require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 4

describe Zendesk::Accounts::Client do
  fixtures :accounts

  let(:account) { accounts(:multiproduct) }
  let(:support_trial_product) { FactoryBot.build(:support_trial_product) }
  let(:support_expired_trial_product) { FactoryBot.build(:support_expired_trial_product) }
  let(:support_not_started_product) { FactoryBot.build(:support_not_started_product) }
  let(:support_free_product) { FactoryBot.build(:support_free_product) }
  let(:support_subscribed_product) { FactoryBot.build(:support_subscribed_product) }
  let(:support_inactive_product) { FactoryBot.build(:support_inactive_product) }
  let(:acme_app_free_product) { FactoryBot.build(:acme_app_free_product) }
  let(:explore_free_product) { FactoryBot.build(:explore_free_product) }
  let(:chat_trial_product) { FactoryBot.build(:chat_trial_product) }
  let(:product) { 'support' }
  let(:account_url) { "#{account_service_url}#{client.send(:account_url)}" }
  # Use high failure threshold to avoid tripping circuit breaker in tests
  let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }

  let(:account_service_url) { 'http://pravda:8888' }

  before { JohnnyFive::MemoryStore.instance.reset!('pravda') }

  describe '#initialize' do
    it 'sets the correct default parameters for options' do
      subject = Zendesk::Accounts::Client.new(account, account.subdomain)
      assert_equal subject.circuit_options, failure_threshold: 10, reset_timeout: 5
      assert_equal subject.timeout, 5
      assert_equal subject.retry_options, {}
    end

    it 'allows options to be configured' do
      circuit_options = {
        failure_threshold: 30,
        reset_timeout: 10
      }

      retry_options = {
        max: 10
      }

      options = {
        circuit_options: circuit_options,
        timeout: 60,
        retry_options: retry_options
      }

      subject = Zendesk::Accounts::Client.new(account, account.subdomain, options)
      assert_equal circuit_options, subject.circuit_options
      assert_equal retry_options, subject.retry_options
      assert_equal 60, subject.timeout
    end

    describe 'when first parameter is not account' do
      let(:statsd_client) { stub_for_statsd }
      subject { Zendesk::Accounts::Client.new(account.id) }

      before do
        ::Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
        statsd_client.expects(:increment).with('account_id_initialization')
      end

      it 'treats this parameter as account id and finds account with it' do
        assert_equal subject.account, account
      end

      it 'sends metrics to datadog' do
        subject
      end
    end
  end

  describe '#zuora_billing_id' do
    let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
    let(:zuora_billing_id) { client.zuora_billing_id }

    describe 'when billing id is present' do
      before { stub_request(:get, account_url).to_return(body: { account: { billing_id: '123' } }.to_json, headers: { content_type: 'application/json; charset=utf-8' }) }

      it 'returns billing id' do
        assert_equal zuora_billing_id, '123'
      end
    end

    describe 'when billing id is not present' do
      before { stub_request(:get, account_url).to_return(body: { account: { billing_id: nil } }.to_json, headers: { content_type: 'application/json; charset=utf-8' }) }

      it 'returns nil' do
        assert_nil zuora_billing_id
      end
    end
  end

  describe '#owner_id' do
    let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
    let(:owner_id) { client.owner_id }

    describe 'when owner id is present' do
      before { stub_request(:get, account_url).to_return(body: { account: { owner_id: 12345 } }.to_json, headers: { content_type: 'application/json; charset=utf-8' }) }

      it 'returns owner id' do
        assert_equal owner_id, 12345
      end
    end

    describe 'when owner id is not present' do
      before { stub_request(:get, account_url).to_return(body: { account: { owner_id: nil } }.to_json, headers: { content_type: 'application/json; charset=utf-8' }) }

      it 'returns nil' do
        assert_nil owner_id
      end
    end
  end

  describe '#abusive' do
    let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
    let(:abusive) { client.abusive }

    describe 'when the account service finds the abusive attribute' do
      let(:abusive_response) { { account: { is_abusive: true } } }
      before { stub_account_response(body: abusive_response) }
      it 'returns is_abusive from the account service' do
        assert_equal abusive, true
      end
    end

    describe 'when the account service does not find the attribute for the account' do
      before { stub_account_response(body: {}) }
      it 'defaults abusive to false' do
        assert_equal abusive, false
      end
    end

    describe 'when the account service throws a faraday error' do
      before do
        Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
        stub_faraday_error(:get, account_url)
      end

      it 'defaults abusive to false' do
        assert_equal abusive, false
      end
    end

    describe 'caching behavior' do
      let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
      let(:statsd_client) { client.send(:statsd_client) }
      let(:abusive_response) { { account: { is_abusive: true } } }

      describe 'when use_cache is false' do
        it 'makes a request to pravda to fetch specific attribute for every call' do
          stub_account_response(body: abusive_response)
          3.times { assert_not_nil client.abusive(use_cache: false) }
          assert_requested(:get, account_url, times: 3)
          statsd_client.expects(:increment).never
        end

        it 'does not populate cache' do
          stub_account_response(body: abusive_response)
          assert_not_nil client.abusive(use_cache: false)
          assert_requested(:get, account_url, times: 1)
        end
      end

      describe 'when use_cache is true' do
        before do
          stub_account_response(body: abusive_response)
        end

        describe 'and an abusive attributes API call was made previously' do
          it 'does not make request to pravda' do
            Zendesk::Accounts::Client.new(account, account.subdomain, client_options).abusive
            statsd_client.expects(:increment).with("accounts.cache", tags: ["cache_hit:true"]).times(3)
            3.times { assert_not_nil client.abusive(use_cache: true) }
          end
        end
      end
    end
  end

  describe '#update_account' do
    let(:client)                  { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
    let(:expected_body)           { { account: { billing_id: 'zuora-abc-123' } } }

    let(:update)                  { client.update_account(billing_id: 'zuora-abc-123') }

    before do
      stub_request(:patch, account_url)
    end

    it 'updates the provided attributes on the account record' do
      update
      assert_requested(:patch, account_url, body: expected_body)
    end

    describe 'when no pravda account exists' do
      before do
        stub_request(:patch, account_url).to_raise(Kragle::ResourceNotFound)
      end

      it 'logs an error' do
        Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
        update
      end
    end
  end

  describe '#products' do
    it 'gets an empty list of products' do
      stub_products_response(:get, products: [])
      products = Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
      assert_equal products.length, 0
    end

    it 'gets products with default subdomain if subdomain is nil' do
      default_url = "#{account_service_url}/api/services/accounts/#{account.id}/products"
      stub_products_response(:get, products: [])
      Zendesk::Accounts::Client.new(account, nil, client_options).products
      assert_requested(:get, default_url)
    end

    it 'gets a trial support product' do
      stub_products_response(:get, products: [support_trial_product])
      products = Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
      assert_equal 1, products.length
      assert_equal :trial, products[0].state
      assert_equal :support, products[0].name
      assert(products[0].active?)
      assert(products[0].trial?)
      assert_equal false, products[0].not_started?
      assert(products[0].started?)
      assert_equal false, products[0].subscribed?
      assert_equal false, products[0].expired?
      assert_equal false, products[0].inactive?
      assert_equal false, products[0].free?
      assert_equal false, products[0].cancelled?
    end

    it 'gets an expired trial support product' do
      stub_products_response(:get, products: [support_expired_trial_product])
      products = Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
      assert_equal 1, products.length
      assert_equal :expired, products[0].state
      assert_equal :support, products[0].name
      assert_equal false, products[0].active?
      assert_equal false, products[0].trial?
      assert_equal false, products[0].not_started?
      assert_equal false, products[0].subscribed?
      assert(products[0].expired?)
      assert_equal false, products[0].inactive?
      assert_equal false, products[0].free?
      assert_equal false, products[0].cancelled?
    end

    it 'gets an un-started support product' do
      stub_products_response(:get, products: [support_not_started_product])
      products = Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
      assert_equal 1, products.length
      assert_equal :not_started, products[0].state
      assert_equal :support, products[0].name
      assert_equal false, products[0].active?
      assert_equal false, products[0].trial?
      assert(products[0].not_started?)
      assert_equal false, products[0].subscribed?
      assert_equal false, products[0].expired?
      assert_equal false, products[0].inactive?
      assert_equal false, products[0].free?
      assert_equal false, products[0].cancelled?
    end

    it 'gets a free support product' do
      stub_products_response(:get, products: [support_free_product])
      products = Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
      assert_equal 1, products.length
      assert_equal :free, products[0].state
      assert_equal :support, products[0].name
      assert(products[0].active?)
      assert_equal false, products[0].trial?
      assert_equal false, products[0].not_started?
      assert_equal false, products[0].subscribed?
      assert_equal false, products[0].expired?
      assert_equal false, products[0].inactive?
      assert(products[0].free?)
      assert_equal false, products[0].cancelled?
    end

    it 'gets an subscribed support product' do
      stub_products_response(:get, products: [support_subscribed_product])
      products = Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
      assert_equal 1, products.length
      assert_equal :subscribed, products[0].state
      assert_equal :support, products[0].name
      assert(products[0].active?)
      assert_equal false, products[0].trial?
      assert_equal false, products[0].not_started?
      assert(products[0].subscribed?)
      assert_equal false, products[0].expired?
      assert_equal false, products[0].inactive?
      assert_equal false, products[0].free?
      assert_equal false, products[0].cancelled?
    end

    it 'gets an un-started support product' do
      stub_products_response(:get, products: [support_not_started_product])
      products = Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
      assert_equal 1, products.length
      assert_equal :not_started, products[0].state
      assert_equal :support, products[0].name
      assert_equal false, products[0].active?
      assert_equal false, products[0].trial?
      assert(products[0].not_started?)
      assert_equal false, products[0].subscribed?
      assert_equal false, products[0].expired?
      assert_equal false, products[0].inactive?
      assert_equal false, products[0].free?
    end

    it 'gets an inactive support product' do
      stub_products_response(:get, products: [support_inactive_product])
      products = Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
      assert_equal 1, products.length
      assert_equal :inactive, products[0].state
      assert_equal :support, products[0].name
      assert_equal false, products[0].active?
      assert_equal false, products[0].trial?
      assert_equal false, products[0].not_started?
      assert_equal false, products[0].subscribed?
      assert_equal false, products[0].expired?
      assert(products[0].inactive?)
      assert_equal false, products[0].free?
      assert_equal false, products[0].cancelled?
    end

    it 'gets a list of active products' do
      stub_products_response(:get, products: [support_trial_product, acme_app_free_product, explore_free_product])
      products = Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
      assert_equal 3, products.length
      assert_equal :trial, products[0].state
      assert_equal :support, products[0].name
      assert(products[0].active?)
      assert_equal :free, products[1].state
      assert_equal :acme_app, products[1].name
      assert(products[1].active?)
      assert_equal :free, products[2].state
      assert_equal :explore, products[2].name
      assert(products[2].active?)
      assert_equal false, products[0].cancelled?
    end

    it 'gets products when include_deleted_account is true' do
      default_url = "#{account_service_url}/api/services/accounts/#{account.id}/products?include_deleted_account=true"
      stub_products_response(:get, {products: []}, true)
      Zendesk::Accounts::Client.new(account, Zendesk::Accounts::Client::DEFAULT_SUBDOMAIN, client_options).products(include_deleted_account: true)
      assert_requested(:get, default_url)
    end

    describe 'too many requests' do
      let(:client) { Zendesk::Accounts::Client.new(account, Zendesk::Accounts::Client::DEFAULT_SUBDOMAIN, client_options) }
      let(:statsd_client) { client.send(:statsd_client) }

      before { stub_request(:get, "#{account_service_url}/api/services/accounts/#{account.id}/products").to_raise(Kragle::TooManyRequests) }

      it 'sends metric to datadog' do
        statsd_client.expects(:increment).with('too_many_requests')
        assert_equal client.products.length, 0
      end
    end
  end

  describe '#chat_products' do
    let(:system_account) { Account.find(Account.system_account_id) }
    it 'gets an empty list of chat products' do
      stub_chat_products_response(:get, products: [])
      chat_products = Zendesk::Accounts::Client.
        new(system_account, Zendesk::Accounts::Client::DEFAULT_SUBDOMAIN, client_options).
        chat_products(10, 0)
      assert_equal chat_products.length, 0
    end

    it 'gets a chat support product' do
      stub_chat_products_response(:get, products: [chat_trial_product])
      chat_products = Zendesk::Accounts::Client.new(system_account, Zendesk::Accounts::Client::DEFAULT_SUBDOMAIN, client_options).chat_products(10, 0)
      assert_equal 1, chat_products.length
      assert_equal :trial, chat_products[0].state
      assert_equal :chat, chat_products[0].name
      assert(chat_products[0].active?)
    end

    describe 'when the account service throws a faraday error' do
      before do
        Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
        stub_faraday_error(:get, "#{account_service_url}/api/services/products?name=chat&limit=10&starting_after=0")
      end
      it 'returns []' do
        chat_products = Zendesk::Accounts::Client.new(system_account, Zendesk::Accounts::Client::DEFAULT_SUBDOMAIN, client_options).chat_products(10, 0)
        assert_equal chat_products.length, 0
      end
    end
  end

  describe '#product' do
    subject { Zendesk::Accounts::Client.new(account, account.subdomain, client_options).product(product) }

    describe 'when the account service finds the product' do
      let(:response) { { product: support_trial_product } }

      before { stub_product_response(product, body: response) }

      it 'returns a Zendesk::Accounts::Product with the correct attributes' do
        assert_equal :trial,   subject.state
        assert_equal :support, subject.name
        assert(subject.active?)
        assert(subject.trial?)
        assert_equal false,    subject.not_started?
        assert_equal false,    subject.subscribed?
        assert_equal false,    subject.expired?
        assert_equal false,    subject.inactive?
        assert_equal false,    subject.free?
      end
    end

    describe 'when the account service does not find the product' do
      before { stub_product_response(product, status: 404) }

      it 'returns nil' do
        assert_nil subject
      end
    end

    describe 'when the account service does not recognize the product' do
      let(:product) { 'invalid-product' }

      before { stub_product_response(product, status: 400) }

      it 'returns nil' do
        assert_nil subject
      end
    end

    describe 'when the account service throws a faraday error' do
      before do
        Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
        stub_faraday_error(:get, "#{account_service_url}/api/services/accounts/#{account.id}/products/#{product}")
      end

      it 'returns nil' do
        assert_nil subject
      end
    end

    describe 'caching behavior' do
      let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
      let(:statsd_client) { client.send(:statsd_client) }
      let(:product_response) { { product: support_trial_product } }
      let(:product_url) { "#{account_service_url}/api/services/accounts/#{account.id}/products/#{support_trial_product[:name]}" }
      let(:products_url) { "#{account_service_url}/api/services/accounts/#{account.id}/products" }

      describe 'when use_cache is false' do
        it 'makes a request to pravda to fetch specific product for every call' do
          stub_product_response(product, body: product_response)
          3.times { assert_not_nil client.product(product, use_cache: false) }
          assert_requested(:get, product_url, times: 3)
          statsd_client.expects(:increment).never
        end

        it 'does not populate cache' do
          statsd_client.expects(:increment).twice
          stub_product_response(product, body: product_response)
          stub_products_response(:get, products: [support_trial_product])
          assert_not_nil client.product(product, use_cache: false)
          assert_not_nil client.product(product, use_cache: true)
          assert_requested(:get, product_url, times: 1)
          assert_requested(:get, products_url, times: 1)
        end
      end

      describe 'when use_cache is true' do
        before do
          stub_products_response(:get, products: [support_trial_product])
        end

        describe 'and a products API call was made previously' do
          it 'does not make request to pravda' do
            Zendesk::Accounts::Client.new(account, account.subdomain, client_options).products
            statsd_client.expects(:increment).with("products.cache", tags: ["cache_hit:true"]).times(3)
            statsd_client.expects(:increment).with("product.cache", tags: ["product_name:#{product}", "cache_hit:true"]).times(3)
            3.times { assert_not_nil client.product(product, use_cache: true) }
            refute_requested(:get, product_url)
          end
        end

        describe 'when multiple calls are made to query different products' do
          it 'makes one request to fetch all products' do
            statsd_client.expects(:increment).with("products.cache", tags: ["cache_hit:false"]).times(1)
            statsd_client.expects(:increment).with("products.cache", tags: ["cache_hit:true"]).times(2)
            statsd_client.expects(:increment).with("product.cache", tags: ["product_name:#{product}", "cache_hit:true"]).times(1)
            statsd_client.expects(:increment).with("product.cache", tags: ["product_name:chat", "cache_hit:true"]).times(1)
            statsd_client.expects(:increment).with("product.cache", tags: ["product_name:explore", "cache_hit:true"]).times(1)
            assert_not_nil client.product(product, use_cache: true)
            assert_nil client.product('explore', use_cache: true)
            assert_nil client.product('chat', use_cache: true)
            refute_requested(:get, product_url)
          end
        end

        it 'makes one request to pravda for multiple calls' do
          statsd_client.expects(:increment).with("products.cache", tags: ["cache_hit:false"]).times(1)
          statsd_client.expects(:increment).with("products.cache", tags: ["cache_hit:true"]).times(2)
          statsd_client.expects(:increment).with("product.cache", tags: ["product_name:#{product}", "cache_hit:true"]).times(3)
          3.times { assert_not_nil client.product(product, use_cache: true) }
          assert_requested(:get, products_url, times: 1)
        end
      end
    end
  end

  describe '#product!' do
    subject { Zendesk::Accounts::Client.new(account, account.subdomain, client_options).product!(product) }

    describe 'when the account service finds the product' do
      let(:response) { { product: support_trial_product } }

      before { stub_product_response(product, body: response) }

      it 'returns a Zendesk::Accounts::Product with the correct attributes' do
        assert_equal :trial,   subject.state
        assert_equal :support, subject.name
        assert(subject.active?)
        assert(subject.trial?)
        assert_equal false,    subject.not_started?
        assert_equal false,    subject.subscribed?
        assert_equal false,    subject.expired?
        assert_equal false,    subject.inactive?
        assert_equal false,    subject.free?
      end
    end

    describe 'when the account service does not find the product' do
      before { stub_product_response(product, status: 404) }

      it 'raises a Kragle::ResourceNotFound error' do
        assert_raise(Kragle::ResourceNotFound) { subject }
      end
    end

    describe 'when the account service does not recognize the product' do
      let(:product) { 'invalid-product' }

      before { stub_product_response(product, status: 400) }

      it 'raises a Kragle::BadRequest error' do
        assert_raise(Kragle::BadRequest) { subject }
      end
    end

    describe 'when the account service throws a faraday error' do
      before do
        Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
        stub_faraday_error(
          :get,
          "#{account_service_url}/api/services/" \
          "accounts/#{account.id}/products/#{product}"
        )
      end

      it 'raises a Faraday::TimeoutError error' do
        assert_raise(Faraday::TimeoutError) { subject }
      end
    end
  end

  describe 'product write APIs' do
    let(:client) do
      Zendesk::Accounts::Client.new(account, account.subdomain, client_options)
    end

    let(:statsd_client) do
      client.send(:statsd_client)
    end

    let(:patch_url_with_deleted) do
      "#{account_service_url}/api/services/accounts/#{account.id}/products/#{support_trial_product[:name]}?include_deleted_account=true"
    end

    let(:patch_url_without_deleted) do
      "#{account_service_url}/api/services/accounts/#{account.id}/products/#{support_trial_product[:name]}?include_deleted_account=false"
    end

    let(:post_url_with_deleted) do
      "#{account_service_url}/api/services/accounts/#{account.id}/products?include_deleted_account=true"
    end

    let(:post_url_without_deleted) do
      "#{account_service_url}/api/services/accounts/#{account.id}/products?include_deleted_account=false"
    end

    let(:patch_url_with_deleted_sku) do
      "#{account_service_url}/api/services/sku/#{account.id}/skus/#{support_trial_product[:name]}?include_deleted_account=true"
    end

    let(:patch_url_without_deleted_sku) do
      "#{account_service_url}/api/services/sku/#{account.id}/skus/#{support_trial_product[:name]}?include_deleted_account=false"
    end

    let(:post_url_with_deleted_sku) do
      "#{account_service_url}/api/services/sku/#{account.id}/skus?include_deleted_account=true"
    end

    let(:post_url_without_deleted_sku) do
      "#{account_service_url}/api/services/sku/#{account.id}/skus?include_deleted_account=false"
    end

    let(:get_url_sku) do
      "#{account_service_url}/api/services/sku/#{account.id}/skus/#{support_trial_product[:name]}"
    end

    let(:headers) do
      { content_type: 'application/json; charset=utf-8' }
    end

    describe '#update_or_create_product!' do
      it 'updates a product' do
        stub_request(:patch, patch_url_without_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_or_create_product_bang", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.update_or_create_product!(support_trial_product[:name], { product: support_trial_product }, context: 'foobar')
      end

      it 'updates a product when include_deleted_account is true' do
        stub_request(:patch, patch_url_with_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_or_create_product_bang", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.update_or_create_product!(support_trial_product[:name], { product: support_trial_product }, include_deleted_account: true, context: 'foobar')
      end

      describe 'when patch fails due to non-existent product record' do
        it 'falls back to post request' do
          stub_request(:patch, patch_url_without_deleted).to_return(body: '', status: 404)
          stub_request(:post, post_url_without_deleted).to_return(body: support_trial_product.to_json, headers: headers)
          statsd_client.expects(:increment).with("update_or_create_product_bang", tags: ["context:foobar", "product_name:support", "result:ok"])

          client.update_or_create_product!(support_trial_product[:name], { product: support_trial_product }, context: 'foobar')

          assert_requested(:patch, patch_url_without_deleted)
          assert_requested(:post, post_url_without_deleted)
        end

        it 'does not handle post request errors' do
          stub_request(:patch, patch_url_without_deleted).to_return(body: '', status: 404)
          stub_faraday_error(:post, post_url_without_deleted)
          statsd_client.expects(:increment).with("update_or_create_product_bang", tags: ["context:foobar", "product_name:support", "result:error", "error:Faraday::TimeoutError"])

          assert_raises Faraday::Error::TimeoutError do
            client.update_or_create_product!(support_trial_product[:name], { product: support_trial_product }, context: 'foobar')
          end

          assert_requested(:patch, patch_url_without_deleted)
          assert_requested(:post, post_url_without_deleted)
        end
      end

      describe 'when patch fails for some other reason' do
        it 'does not handle the error and does not fall back to post request' do
          stub_faraday_error(:patch, patch_url_without_deleted)
          statsd_client.expects(:increment).with("update_or_create_product_bang", tags: ["context:foobar", "product_name:support", "result:error", "error:Faraday::TimeoutError"])

          assert_raises Faraday::Error::TimeoutError do
            client.update_or_create_product!(support_trial_product[:name], { product: support_trial_product }, context: 'foobar')
          end

          assert_requested(:patch, patch_url_without_deleted)
          assert_not_requested(:post, post_url_without_deleted)
        end
      end
    end

    describe '#update_or_create_product' do
      it 'updates a product' do
        stub_request(:patch, patch_url_without_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_or_create_product", tags: ["context:foobar", "product_name:support", "result:ok", "fallback:0"])

        client.update_or_create_product(support_trial_product[:name], { product: support_trial_product }, context: "foobar")
      end

      it 'updates a product when include_deleted_account is true' do
        stub_request(:patch, patch_url_with_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_or_create_product", tags: ["context:foobar", "product_name:support", "result:ok", "fallback:0"])

        client.update_or_create_product(support_trial_product[:name], { product: support_trial_product }, include_deleted_account: true, context: "foobar")
      end

      describe 'when the account service throws a faraday error' do
        it 'handles error and does not fall back to post request' do
          Rails.logger.expects(:error).with(regexp_matches(/Failed to update \(did not fallback to create\) 'support' product record for account_id '\d+'/)).at_least_once
          stub_faraday_error(:patch, patch_url_without_deleted)
          statsd_client.expects(:increment).with("update_or_create_product", tags: ["context:foobar", "product_name:support", "result:error", "error:Faraday::TimeoutError", "fallback:0"])

          client.update_or_create_product(support_trial_product[:name], { product: support_trial_product }, context: "foobar")

          assert_requested(:patch, patch_url_without_deleted)
          assert_not_requested(:post, post_url_without_deleted)
        end
      end

      describe 'when product record does not exist' do
        it 'falls back to post request' do
          stub_request(:patch, patch_url_without_deleted).to_return(body: '', status: 404)
          stub_request(:post, post_url_without_deleted).to_return(body: support_trial_product.to_json, headers: headers)
          statsd_client.expects(:increment).with("update_or_create_product", tags: ["context:foobar", "product_name:support", "result:ok", "fallback:1"])

          client.update_or_create_product(support_trial_product[:name], { product: support_trial_product }, context: "foobar")

          assert_requested(:patch, patch_url_without_deleted)
          assert_requested(:post, post_url_without_deleted)
        end

        it 'handles post request errors' do
          Rails.logger.expects(:error).with(regexp_matches(/Failed to create \(after fallback from update\) 'support' product record for account_id '\d+'/)).at_least_once
          stub_request(:patch, patch_url_without_deleted).to_return(body: '', status: 404)
          stub_faraday_error(:post, post_url_without_deleted)
          statsd_client.expects(:increment).with("update_or_create_product", tags: ["context:foobar", "product_name:support", "result:error", "error:Faraday::TimeoutError", "fallback:1"])

          client.update_or_create_product(support_trial_product[:name], { product: support_trial_product }, context: "foobar")

          assert_requested(:patch, patch_url_without_deleted)
          assert_requested(:post, post_url_without_deleted)
        end
      end
    end

    describe '#update_product!' do
      it 'updates a product' do
        stub_request(:patch, patch_url_without_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_product_bang", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.update_product!(support_trial_product[:name], { product: support_trial_product }, context: "foobar")
      end

      it 'updates a product when include_deleted_account is true' do
        stub_request(:patch, patch_url_with_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_product_bang", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.update_product!(support_trial_product[:name], { product: support_trial_product }, include_deleted_account: true, context: "foobar")
      end
    end

    describe '#update_product' do
      it 'updates a product' do
        stub_request(:patch, patch_url_without_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_product", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.update_product(support_trial_product[:name], { product: support_trial_product }, context: "foobar")
      end

      it 'updates a product when include_deleted_account is true' do
        stub_request(:patch, patch_url_with_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_product", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.update_product(support_trial_product[:name], { product: support_trial_product }, include_deleted_account: true, context: "foobar")
      end

      it 'handles error when the account service throws a faraday error' do
        Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
        stub_faraday_error(:patch, patch_url_without_deleted)
        statsd_client.expects(:increment).with("update_product", tags: ["context:foobar", "product_name:support", "result:error", "error:Faraday::TimeoutError"])

        client.update_product(support_trial_product[:name], { product: support_trial_product }, context: "foobar")
      end
    end

    describe '#create_product!' do
      it 'creates a product' do
        stub_request(:post, post_url_without_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("create_product_bang", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.create_product!(support_trial_product[:name], { product: support_trial_product }, context: "foobar")

        assert_requested(:post, post_url_without_deleted)
      end

      it 'creates a product when include_deleted_account is true' do
        stub_request(:post, post_url_with_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("create_product_bang", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.create_product!(support_trial_product[:name], { product: support_trial_product }, include_deleted_account: true, context: "foobar")
      end
    end

    describe '#create_product' do
      it 'creates a product' do
        stub_request(:post, post_url_without_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("create_product", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.create_product(support_trial_product[:name], { product: support_trial_product }, context: "foobar")

        assert_requested(:post, post_url_without_deleted)
      end

      it 'creates a product when include_deleted_account is true' do
        stub_request(:post, post_url_with_deleted).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("create_product", tags: ["context:foobar", "product_name:support", "result:ok"])

        client.create_product(support_trial_product[:name], { product: support_trial_product }, include_deleted_account: true, context: "foobar")
      end

      describe 'when the account service throws a faraday error' do
        it 'handles error' do
          Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
          stub_faraday_error(:post, post_url_without_deleted)
          statsd_client.expects(:increment).with("create_product", tags: ["context:foobar", "product_name:support", "result:error", "error:Faraday::TimeoutError"])

          client.create_product(support_trial_product[:name], { product: support_trial_product }, context: "foobar")
        end
      end
    end

    describe '#sku!' do
      it 'retireves an sku' do
        stub_request(:get, get_url_sku).to_return(body: support_trial_product.to_json, headers: headers)

        client.sku!(support_trial_product[:name])
      end

      it 'throws an error when an sku cannot be found' do
        stub_request(:get, get_url_sku).to_return(status: 404, body: "", headers: {}).to_raise(Kragle::ResourceNotFound)

        client.sku!(support_trial_product[:name])
      end
    end

    describe '#update_or_create_sku!' do
      it 'updates an sku' do
        stub_request(:patch, patch_url_without_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_or_create_sku_bang", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.update_or_create_sku!(support_trial_product[:name], { sku: support_trial_product }, context: 'foobar')
      end

      it 'updates an sku when include_deleted_account is true' do
        stub_request(:patch, patch_url_with_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_or_create_sku_bang", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.update_or_create_sku!(support_trial_product[:name], { sku: support_trial_product }, include_deleted_account: true, context: 'foobar')
      end

      describe 'when patch fails due to non-existent sku record' do
        it 'falls back to post request' do
          stub_request(:patch, patch_url_without_deleted_sku).to_return(body: '', status: 404)
          stub_request(:post, post_url_without_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
          statsd_client.expects(:increment).with("update_or_create_sku_bang", tags: ["context:foobar", "sku_name:support", "result:ok"])

          client.update_or_create_sku!(support_trial_product[:name], { sku: support_trial_product }, context: 'foobar')

          assert_requested(:patch, patch_url_without_deleted_sku)
          assert_requested(:post, post_url_without_deleted_sku)
        end

        it 'does not handle post request errors' do
          stub_request(:patch, patch_url_without_deleted_sku).to_return(body: '', status: 404)
          stub_faraday_error(:post, post_url_without_deleted_sku)
          statsd_client.expects(:increment).with("update_or_create_sku_bang", tags: ["context:foobar", "sku_name:support", "result:error", "error:Faraday::TimeoutError"])

          assert_raises Faraday::Error::TimeoutError do
            client.update_or_create_sku!(support_trial_product[:name], { sku: support_trial_product }, context: 'foobar')
          end

          assert_requested(:patch, patch_url_without_deleted_sku)
          assert_requested(:post, post_url_without_deleted_sku)
        end
      end

      describe 'when patch fails for some other reason' do
        it 'does not handle the error and does not fall back to post request' do
          stub_faraday_error(:patch, patch_url_without_deleted_sku)
          statsd_client.expects(:increment).with("update_or_create_sku_bang", tags: ["context:foobar", "sku_name:support", "result:error", "error:Faraday::TimeoutError"])

          assert_raises Faraday::Error::TimeoutError do
            client.update_or_create_sku!(support_trial_product[:name], { sku: support_trial_product }, context: 'foobar')
          end

          assert_requested(:patch, patch_url_without_deleted_sku)
          assert_not_requested(:post, post_url_without_deleted_sku)
        end
      end
    end

    describe '#update_or_create_sku' do
      it 'updates a sku' do
        stub_request(:patch, patch_url_without_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_or_create_sku", tags: ["context:foobar", "sku_name:support", "result:ok", "fallback:0"])

        client.update_or_create_sku(support_trial_product[:name], { sku: support_trial_product }, context: "foobar")
      end

      it 'updates a sku when include_deleted_account is true' do
        stub_request(:patch, patch_url_with_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_or_create_sku", tags: ["context:foobar", "sku_name:support", "result:ok", "fallback:0"])

        client.update_or_create_sku(support_trial_product[:name], { sku: support_trial_product }, include_deleted_account: true, context: "foobar")
      end

      describe 'when the account service throws a faraday error' do
        it 'handles error and does not fall back to post request' do
          Rails.logger.expects(:error).with(regexp_matches(/Failed to update \(did not fallback to create\) 'support' sku record for account_id '\d+'/)).at_least_once
          stub_faraday_error(:patch, patch_url_without_deleted_sku)
          statsd_client.expects(:increment).with("update_or_create_sku", tags: ["context:foobar", "sku_name:support", "result:error", "error:Faraday::TimeoutError", "fallback:0"])

          client.update_or_create_sku(support_trial_product[:name], { sku: support_trial_product }, context: "foobar")

          assert_requested(:patch, patch_url_without_deleted_sku)
          assert_not_requested(:post, post_url_without_deleted_sku)
        end
      end

      describe 'when sku record does not exist' do
        it 'falls back to post request' do
          stub_request(:patch, patch_url_without_deleted_sku).to_return(body: '', status: 404)
          stub_request(:post, post_url_without_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
          statsd_client.expects(:increment).with("update_or_create_sku", tags: ["context:foobar", "sku_name:support", "result:ok", "fallback:1"])

          client.update_or_create_sku(support_trial_product[:name], { sku: support_trial_product }, context: "foobar")

          assert_requested(:patch, patch_url_without_deleted_sku)
          assert_requested(:post, post_url_without_deleted_sku)
        end

        it 'handles post request errors' do
          Rails.logger.expects(:error).with(regexp_matches(/Failed to create \(after fallback from update\) 'support' sku record for account_id '\d+'/)).at_least_once
          stub_request(:patch, patch_url_without_deleted_sku).to_return(body: '', status: 404)
          stub_faraday_error(:post, post_url_without_deleted_sku)
          statsd_client.expects(:increment).with("update_or_create_sku", tags: ["context:foobar", "sku_name:support", "result:error", "error:Faraday::TimeoutError", "fallback:1"])

          client.update_or_create_sku(support_trial_product[:name], { sku: support_trial_product }, context: "foobar")

          assert_requested(:patch, patch_url_without_deleted_sku)
          assert_requested(:post, post_url_without_deleted_sku)
        end
      end
    end

    describe '#update_sku!' do
      it 'updates a sku' do
        stub_request(:patch, patch_url_without_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_sku_bang", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.update_sku!(support_trial_product[:name], { sku: support_trial_product }, context: "foobar")
      end

      it 'updates a sku when include_deleted_account is true' do
        stub_request(:patch, patch_url_with_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_sku_bang", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.update_sku!(support_trial_product[:name], { sku: support_trial_product }, include_deleted_account: true, context: "foobar")
      end
    end

    describe '#update_sku' do
      it 'updates a sku' do
        stub_request(:patch, patch_url_without_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_sku", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.update_sku(support_trial_product[:name], { sku: support_trial_product }, context: "foobar")
      end

      it 'updates a sku when include_deleted_account is true' do
        stub_request(:patch, patch_url_with_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("update_sku", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.update_sku(support_trial_product[:name], { product: support_trial_product }, include_deleted_account: true, context: "foobar")
      end

      it 'handles error when the account service throws a faraday error' do
        Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
        stub_faraday_error(:patch, patch_url_without_deleted_sku)
        statsd_client.expects(:increment).with("update_sku", tags: ["context:foobar", "sku_name:support", "result:error", "error:Faraday::TimeoutError"])

        client.update_sku(support_trial_product[:name], { product: support_trial_product }, context: "foobar")
      end
    end

    describe '#create_sku!' do
      it 'creates an sku' do
        stub_request(:post, post_url_without_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("create_sku_bang", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.create_sku!(support_trial_product[:name], { sku: support_trial_product }, context: "foobar")

        assert_requested(:post, post_url_without_deleted_sku)
      end

      it 'creates an sku when include_deleted_account is true' do
        stub_request(:post, post_url_with_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("create_sku_bang", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.create_sku!(support_trial_product[:name], { sku: support_trial_product }, include_deleted_account: true, context: "foobar")

        assert_requested(:post, post_url_with_deleted_sku)
      end
    end

    describe '#create_sku' do
      it 'creates an sku' do
        stub_request(:post, post_url_without_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("create_sku", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.create_sku(support_trial_product[:name], { sku: support_trial_product }, context: "foobar")

        assert_requested(:post, post_url_without_deleted_sku)
      end

      it 'creates an sku when include_deleted_account is true' do
        stub_request(:post, post_url_with_deleted_sku).to_return(body: support_trial_product.to_json, headers: headers)
        statsd_client.expects(:increment).with("create_sku", tags: ["context:foobar", "sku_name:support", "result:ok"])

        client.create_sku(support_trial_product[:name], { sku: support_trial_product }, include_deleted_account: true, context: "foobar")

        assert_requested(:post, post_url_with_deleted_sku)
      end

      describe 'when the account service throws a faraday error' do
        it 'handles error' do
          Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
          stub_faraday_error(:post, post_url_without_deleted_sku)
          statsd_client.expects(:increment).with("create_sku", tags: ["context:foobar", "sku_name:support", "result:error", "error:Faraday::TimeoutError"])

          client.create_sku(support_trial_product[:name], { sku: support_trial_product }, context: "foobar")
        end
      end
    end
  end

  describe '#max_light_agents' do
    let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
    let(:max_light_agents) { 10 }
    let(:max_light_agents_url) do
      /#{account_service_url}\/api\/services\/accounts\/\d+\/products\/light_agents/
    end
    let(:max_light_agents_body) do
      { "product" => { "plan_settings" => { "max_light_agents" => max_light_agents } } }
    end

    it 'requests from account services' do
      request = stub_request(:get, max_light_agents_url).
        to_return(status: 200, body: max_light_agents_body.to_json, headers: { "Content-Type" => "application/json" })
      response = client.max_light_agents
      assert_requested request
      assert_equal max_light_agents, response
    end

    it 'handles error when the account service throws an error' do
      Rails.logger.expects(:add).with(Logger::ERROR, anything, anything).at_least_once
      stub_request(:get, max_light_agents_url).to_raise(Kragle::ResponseError)
      assert_raises Kragle::ResponseError do
        client.max_light_agents
      end
    end
  end

  describe '#expire_product_cache' do
    let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
    let(:response) { { products: [support_trial_product] } }
    let(:url) { "#{account_service_url}/api/services/accounts/#{account.id}/products" }

    it 'expires the cache for the specified product' do
      stub_products_response(:get, response)
      3.times do
        assert_not_nil client.product(product, use_cache: true)
        client.expire_product_cache(product)
      end
      assert_requested(:get, url, times: 3)
    end
  end

  describe '#expire_account_client_account_cache' do
    let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
    let(:abusive_response) { { account: { is_abusive: true } } }

    it 'expires the cache for the account' do
      stub_account_response(body: abusive_response)
      3.times do
        assert_not_nil client.abusive(use_cache: true)
        client.expire_account_client_account_cache
      end
      assert_requested(:get, account_url, times: 3)
    end
  end

  describe '#url_prefix' do
    let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }
    let(:url_prefix) { client.url_prefix }

    it 'fetches account service url from environment (ie. set by consul)' do
      assert_equal account_service_url, url_prefix
    end
  end

  describe '#account_service_host' do
    it 'concatenates the account subdomain and host when forming the host' do
      client = Zendesk::Accounts::Client.new(account, account.subdomain, client_options)
      assert_equal "#{account.subdomain}.zendesk-test.com", client.account_service_host
    end

    it 'downcases the account subdomain arg when forming the host' do
      subdomain = "Sub"
      client = Zendesk::Accounts::Client.new(account, subdomain, client_options)
      assert_equal "#{subdomain.downcase}.zendesk-test.com", client.account_service_host
    end

    it 'downcases the subdomain from the account arg when forming the host' do
      account.subdomain.upcase!
      client = Zendesk::Accounts::Client.new(account, nil, client_options)
      assert_equal "#{account.subdomain.downcase}.zendesk-test.com", client.account_service_host
    end
  end

  describe '#products_url' do
    let(:client) { Zendesk::Accounts::Client.new(account, account.subdomain, client_options) }

    it 'returns the path without the server info' do
      assert_equal "/api/services/accounts/#{account.id}/products", client.products_url
    end
  end

  def stub_products_response(method, body, include_deleted_account = false)
    products_url = "#{account_service_url}/api/services/accounts/#{account.id}/products" + (include_deleted_account ? '?include_deleted_account=true' : '')
    stub_request(method, products_url).
      to_return(body: body.to_json, headers: { content_type: 'application/json; charset=utf-8' })
  end

  def stub_chat_products_response(method, body, limit = 10, starting_after = 0)
    stub_request(method, "#{account_service_url}/api/services/products?name=chat&limit=#{limit}&starting_after=#{starting_after}").
      to_return(body: body.to_json, headers: { content_type: 'application/json; charset=utf-8' })
  end

  def stub_product_response(product, body: {}, status: 200)
    stub_request(:get, "#{account_service_url}/api/services/accounts/#{account.id}/products/#{product}").
      to_return(
        status:  status,
        body:    body.to_json,
        headers: { content_type: 'application/json; charset=utf-8' }
      )
  end

  def stub_account_response(body: {}, status: 200)
    stub_request(:get, "#{account_service_url}/api/services/accounts/#{account.id}").
      to_return(
        status:  status,
        body:    body.to_json,
        headers: { content_type: 'application/json; charset=utf-8' }
      )
  end

  def stub_faraday_error(method, uri)
    stub_request(method, uri).
      to_raise(Faraday::Error::TimeoutError.new("Timeout"))
  end
end
