require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'Account::InvoicedCustomerReport' do
  fixtures :all

  describe "generating reports" do
    before do
      Subscription.any_instance.stubs(invoicing?: true)
    end

    describe ".generate" do
      before do
        @report = filepath
        refute File.exist?(@report)
        Zendesk::Accounts::InvoicedCustomerReport.generate(@report)
      end

      after do
        File.unlink @report
      end

      it "generates a csv file" do
        assert File.exist?(@report)
      end

      it "includes header" do
        actual   = File.readlines(@report).first
        expected = "Zendesk Account ID,Billing Cycle,Plan,Latest Paid Date,Latest Invoiced Date,Max Agents,Subdomain,Hub Account ID\n"
        assert_equal expected, actual
      end

      it "lists all subscription records that are invoicing" do
        actual   = File.readlines(@report)[1..-1].count
        expected = Account.joins(:subscription).count(:all) - 1
        assert_equal expected, actual
      end
    end

    # -- helper methods --

    def filepath
      file = Tempfile.new('report')
      path = file.path
      file.close
      file.unlink
      path
    end
  end
end
