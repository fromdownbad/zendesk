require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

class LocationControllerSupportTestController < ApplicationController
  include Zendesk::Accounts::LocationControllerSupport

  def create
    head :ok
  end
end

class LocationControllerSupportTest < ActionController::TestCase
  tests LocationControllerSupportTestController
  use_test_routes
  fixtures :all

  describe Zendesk::Accounts::LocationControllerSupport do
    describe "#geo_ip_info" do
      let(:sf_location) do
        {
          latitude: 37.775800000000004,
          longitude: -122.4128,
          region: :us,
          country_code: "US",
          country: "United States",
          state: "CA",
          timezone: "America/Los_Angeles",
          city: "San Francisco",
          description: "San Francisco, CA, United States"
        }
      end
      let(:sf_geo_ip_info) do
        {
          latitude: 37.775800000000004,
          longitude: -122.4128,
          region: :us,
          country_code: "US",
          timezone: "America/Los_Angeles",
          city: "San Francisco"
        }
      end

      describe 'when account_creation_use_inferred_ip Arturo is disable' do
        before do
          Arturo.disable_feature!(:account_creation_use_inferred_ip)
        end

        it "uses original_remote_ip" do
          Zendesk::Net::IPTools.expects(:untrusted_ip).once
          Zendesk::GeoLocation.stubs(:locate).returns(sf_location)

          @controller.send(:geo_ip_info)
        end
      end

      describe 'when inferred_ip is available' do
        before do
          @request.params['trial_extras'] = {'Inferred_IP_Address': '1.1.1.1'}
        end

        it "use the inferred ip from trial extras" do
          Zendesk::Net::IPTools.expects(:untrusted_ip).never
          Zendesk::GeoLocation.stubs(:locate).returns(sf_location)

          @controller.send(:geo_ip_info)
        end
      end

      describe "when location is empty hash" do
        it "returns empty hash" do
          Zendesk::GeoLocation.stubs(:locate).returns({})
          assert_equal @controller.send(:geo_ip_info), {}
        end
      end

      describe "when location is nil" do
        it "returns nil" do
          Zendesk::GeoLocation.stubs(:locate).returns(nil)
          assert_nil @controller.send(:geo_ip_info)
        end
      end

      describe "when location is SF" do
        it "returns filtered location" do
          Zendesk::GeoLocation.stubs(:locate).returns(sf_location)

          assert_equal @controller.send(:geo_ip_info), sf_geo_ip_info
        end
      end

      describe "when location contains extra keys" do
        it "returns filtered location" do
          Zendesk::GeoLocation.stubs(:locate).returns(
            sf_location.merge(extra_key: "Useless")
          )

          assert_equal @controller.send(:geo_ip_info), sf_geo_ip_info
        end
      end

      describe "when location contains fewer keys" do
        it "returns filtered location" do
          geo_ip = {
            latitude: 37.775800000000004,
            longitude: -122.4128
          }
          Zendesk::GeoLocation.stubs(:locate).returns(geo_ip)

          assert_equal @controller.send(:geo_ip_info), geo_ip
        end
      end

      describe "when location raises error" do
        it "logs error and retrns nil" do
          raises_exception = -> { raise ArgumentError }
          Zendesk::GeoLocation.stubs(:locate).returns(raises_exception)

          ZendeskExceptions::Logger.expects(:record)
          assert_nil @controller.send(:geo_ip_info)
        end
      end
    end

    describe "#set_region" do
      describe "when trail_extras Inferred_CCode is set" do
        it "returns emea" do
          @request.params['trial_extras'] = {'Inferred_CCode': 'GL'}
          @controller.send(:set_region)
          assert_equal @controller.send(:region), :emea
        end
      end
    end

    describe "#in_restricted_country?" do
      describe "when geo_ip_info is from a restricted country" do
        let(:geo_ip_info) do
          { country_code: "KP" }
        end

        it "returns true" do
          @controller.expects(:geo_ip_info).returns(geo_ip_info)
          assert @controller.send(:in_restricted_country?)
        end
      end

      describe "when geo_ip_info is from a non-restricted country" do
        let(:geo_ip_info) { { country_code: 'SD' } }

        it "returns false" do
          @controller.expects(:geo_ip_info).returns(geo_ip_info)
          refute @controller.send(:in_restricted_country?)
        end
      end

      describe "when geo_ip_info is from a nil country" do
        let(:geo_ip_info) { { } }

        it "returns false" do
          @controller.expects(:geo_ip_info).returns(geo_ip_info)
          refute @controller.send(:in_restricted_country?)
        end
      end
    end

    describe "#in_restricted_zone?" do
      describe "when geo_ip_info is from a restricted zone" do
        let(:geo_ip_info) do
          {
            latitude: 45.0,
            longitude: 35.0
          }
        end

        it "returns true" do
          @controller.expects(:geo_ip_info).twice.returns(geo_ip_info)
          assert @controller.send(:in_restricted_zone?)
        end
      end

      describe "when geo_ip_info is from a non-restricted zone" do
        let(:geo_ip_info) do
          {
            latitude: 50.0,
            longitude: 35.0
          }
        end

        it "returns false" do
          @controller.expects(:geo_ip_info).twice.returns(geo_ip_info)
          refute @controller.send(:in_restricted_zone?)
        end
      end

      describe "when geo_ip_info is from a nil zone" do
        let(:geo_ip_info) { { } }

        it "returns false" do
          @controller.expects(:geo_ip_info).twice.returns(geo_ip_info)
          refute @controller.send(:in_restricted_zone?)
        end
      end
    end

    describe "#in_restricted_city?" do
      describe "when geo_ip_info is from a restricted city" do
        let(:geo_ip_info) do
          { city: "Sevastopol" }
        end

        it "returns true" do
          @controller.expects(:geo_ip_info).returns(geo_ip_info)
          assert @controller.send(:in_restricted_city?)
        end
      end

      describe "when geo_ip_info is from a non-restricted city" do
        let(:geo_ip_info) { { city: 'San Francisco' } }

        it "returns false" do
          @controller.expects(:geo_ip_info).returns(geo_ip_info)
          refute @controller.send(:in_restricted_city?)
        end
      end

      describe "when geo_ip_info is from a nil city" do
        let(:geo_ip_info) { { city: 'San Francisco' } }

        it "returns false" do
          @controller.expects(:geo_ip_info).returns(geo_ip_info)
          refute @controller.send(:in_restricted_city?)
        end
      end
    end

    describe "#check_restricted_jurisdiction!" do
      before do
        @request.account = accounts(:minimum)
        @controller.stubs(set_params_currency: anything)
        @controller.stubs(set_address_and_region: anything)
        login :minimum_agent
      end

      describe "when request originates from a restricted country" do
        let(:geo_ip_info) do
          { country_code: "KP" }
        end

        before do
          @controller.stubs(geo_ip_info: geo_ip_info)
          post :create, format: :json
        end

        it('responds with forbidden') { assert_response :forbidden }

        it "indicates request failure in the response body" do
          payload = JSON.parse(response.body)
          refute payload['success']
          assert_match /prohibited jurisdiction/i, payload['message']
        end
      end

      describe "when request originates from a non-restricted country" do
        let(:geo_ip_info) { { country_code: 'US' } }

        before do
          @controller.stubs(geo_ip_info: geo_ip_info)
          post :create, format: :json
        end

        it('responds with success') { assert_response :success }
      end

      describe "with ofac regulation" do
        describe "when request originates from a restricted zone" do
          let(:geo_ip_info) do
            {
              latitude: 45.0,
              longitude: 35.0
            }
          end

          before do
            @controller.stubs(geo_ip_info: geo_ip_info)
            post :create, format: :json
          end

          it('responds with forbidden') { assert_response :forbidden }

          it "indicates request failure in the response body" do
            payload = JSON.parse(response.body)
            refute payload['success']
            assert_match /prohibited jurisdiction/i, payload['message']
          end
        end

        describe "when request originates from a non-restricted zone" do
          let(:geo_ip_info) do
            {
              latitude: 50.0,
              longitude: 35.0
            }
          end

          before do
            @controller.stubs(geo_ip_info: geo_ip_info)
            post :create, format: :json
          end

          it('responds with success') { assert_response :success }
        end

        describe "when request originates from a restricted city" do
          let(:geo_ip_info) do
            { city: "Sevastopol" }
          end

          before do
            @controller.stubs(geo_ip_info: geo_ip_info)
            post :create, format: :json
          end

          it('responds with forbidden') { assert_response :forbidden }

          it "indicates request failure in the response body" do
            payload = JSON.parse(response.body)
            refute payload['success']
            assert_match /prohibited jurisdiction/i, payload['message']
          end
        end

        describe "when request originates from a non-restricted city" do
          let(:geo_ip_info) { { city: 'San Francisco' } }

          before do
            @controller.stubs(geo_ip_info: geo_ip_info)
            post :create, format: :json
          end

          it('responds with success') { assert_response :success }
        end

        describe "when request originates from nil geo_ip_info" do
          let(:geo_ip_info) { { } }

          before do
            @controller.stubs(geo_ip_info: geo_ip_info)
            post :create, format: :json
          end

          it('responds with success') { assert_response :success }
        end
      end
    end
  end
end
