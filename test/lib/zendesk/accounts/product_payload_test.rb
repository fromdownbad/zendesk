require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Accounts::ProductPayload do
  fixtures :accounts, :subscriptions

  let(:account) { accounts(:minimum) }
  let(:subscription) { account.subscription }
  let(:suite) { false }

  describe '.for_support' do
    describe 'suite is specified' do
      let(:for_support) { Zendesk::Accounts::ProductPayload.for_support(account, suite: suite) }
      let(:expected_hash) do
        {
          product: {
            state: :trial,
            trial_expires_at: subscription.trial_expires_on.to_datetime.iso8601,
            plan_settings: {
              plan_type: subscription.plan_type,
              max_agents: subscription.max_agents,
              light_agents: subscription.has_light_agents?,
              insights: :good_data,
              suite: suite,
              agent_workspace: suite
            }
          }
        }
      end

      it 'returns hash of Account service product payload' do
        assert_equal for_support, expected_hash
      end
    end

    describe 'suite is not specified' do
      let(:for_support) { Zendesk::Accounts::ProductPayload.for_support(account) }
      let(:expected_hash) do
        {
          product: {
            state: :trial,
            trial_expires_at: subscription.trial_expires_on.to_datetime.iso8601,
            plan_settings: {
              plan_type: subscription.plan_type,
              max_agents: subscription.max_agents,
              light_agents: subscription.has_light_agents?,
              insights: :good_data,
              suite: suite,
              agent_workspace: false
            }
          }
        }
      end

      before do
        account.settings.suite_trial = suite
      end

      describe 'account is suite' do
        let(:suite) { true }

        it 'returns hash of Account service product payload' do
          assert_equal expected_hash, for_support
        end

        describe 'account is spp' do
          before do
            account.stubs(:spp?).returns(true)
          end

          it 'sets suite to false' do
            assert_equal false, for_support[:product][:plan_settings][:suite]
          end
        end
      end

      describe 'account is not suite' do
        let(:suite) { false }

        it 'returns hash of Account service product payload' do
          assert_equal for_support, expected_hash
        end
      end

      describe 'account is unset' do
        let(:suite) { nil }
        let(:expected_hash) do
          {
            product: {
              state: :trial,
              trial_expires_at: subscription.trial_expires_on.to_datetime.iso8601,
              plan_settings: {
                plan_type: subscription.plan_type,
                max_agents: subscription.max_agents,
                light_agents: subscription.has_light_agents?,
                insights: :good_data,
                suite: false,
                agent_workspace: false
              }
            }
          }
        end

        it 'defaults suite setting to false' do
          assert_equal for_support, expected_hash
        end
      end
    end

    describe 'with agent workspace setting' do
      let(:expected_hash) do
        {
          product: {
            state: :trial,
            trial_expires_at: subscription.trial_expires_on.to_datetime.iso8601,
            plan_settings: {
              plan_type: subscription.plan_type,
              max_agents: subscription.max_agents,
              light_agents: subscription.has_light_agents?,
              insights: :good_data,
              suite: false,
              agent_workspace: agent_workspace
            }
          }
        }
      end

      before { account.settings.check_group_name_uniqueness = agent_workspace }

      describe 'when not set' do
        let(:agent_workspace) { false }

        it 'defaults agent_workspace plan setting to false' do
          assert_equal expected_hash, Zendesk::Accounts::ProductPayload.for_support(account)
        end
      end

      describe 'when set' do
        let(:agent_workspace) { true }

        it 'sets agent_workspace to true' do
          assert_equal expected_hash, Zendesk::Accounts::ProductPayload.for_support(account)
        end
      end
    end
  end

  describe '.for_social_messaging' do
    let(:actual_hash) { Zendesk::Accounts::ProductPayload.for_social_messaging(account, suite: suite) }
    let(:expected_hash) do
      {
        product: {
          state: :trial,
          trial_expires_at: subscription.trial_expires_on.to_datetime.iso8601,
          plan_settings: {
            whatsapp_numbers_purchased: 0,
            suite: suite
          }
        }
      }
    end

    it 'returns hash of Account service product payload' do
      assert_equal actual_hash, expected_hash
    end
  end
end
