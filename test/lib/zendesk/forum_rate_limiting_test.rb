require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::ForumRateLimiting do
  describe "forum rate limiting" do
    let(:account) { stub(id: 13) }
    let(:settings) { stub }
    let(:user) { stub(id: 42) }
    let(:request) { stub(remote_ip: "1.2.3.4") }
    let(:controller) { stub(current_account: account, current_user: user, request: request) }

    before do
      account.stubs(:settings).returns(settings)
      user.stubs(:is_agent?).returns(false)
      controller.extend(Zendesk::ForumRateLimiting)
      Prop.stubs(:throttle!)
    end

    describe "#rate_limit_web_portal_end_user_content" do
      before do
        settings.stubs(:end_user_comment_rate_limit).returns(3)
      end

      it "throttles if the rate limit for the current user has been reached" do
        Prop.expects(:throttle!).with(:forum_comments, 42, threshold: 3)
        controller.send(:rate_limit_web_portal_end_user_content)
      end

      it "throttles if the rate limit for the IP address has been reached" do
        Prop.expects(:throttle!).with(:forum_comments, [13, "1.2.3.4"], threshold: 3)
        controller.send(:rate_limit_web_portal_end_user_content)
      end

      it "does not throttle if current user is an agent" do
        user.stubs(:is_agent?).returns(true)
        Prop.expects(:throttle!).never

        controller.send(:rate_limit_web_portal_end_user_content)
      end
    end
  end
end
