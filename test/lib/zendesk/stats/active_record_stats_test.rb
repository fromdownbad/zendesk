require_relative "../../../support/test_helper"

SingleCov.not_covered!
# Covered in ar_skipped_callback_metrics_test.rb
SingleCov.covered! uncovered: 16, file: 'lib/zendesk/stats/active_record_stats/stats_subscriber.rb'
SingleCov.covered! file: 'lib/zendesk/stats/active_record_stats.rb'

describe ActiveRecordStats do
  it 'it can subscribe' do
    ActiveSupport::Notifications.stub(:subscribe, :subbed) do
      ActiveRecordStats.subscribe
      assert_equal ActiveRecordStats.subscriber, :subbed
    end
  end

  it 'it can unsubscribe' do
    mock = Minitest::Mock.new
    mock.expect(:call, nil, [:subbed])
    ActiveSupport::Notifications.stub(:unsubscribe, mock) do
      ActiveRecordStats.unsubscribe
    end
    mock.verify
  end

  describe ActiveRecordStats::StatsSubscriber do
    let(:bad_payload) { Minitest::Mock.new }
    let(:statsd_client) { stub_for_statsd }
    let(:payload) { Struct.new(:payload) }
    let(:span) { stub(:span) }

    before(:each) do
      Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
    end

    it 'does not raise' do
      bad_payload.expect(:payload, nil)
      Rails.logger.expects(:warn).with(regexp_matches(/ar_skip_callback/))
      subject.skipped_callback(bad_payload)
      bad_payload.verify
    end

    it 'does not send metrics for internal active_record methods' do
      caller = ['activerecord-x.x/lib/active_record/transactions.rb:291:in `delete']
      event = payload.new(klass: self, method: :delete, caller: caller)
      ActiveRecordStats::StatsSubscriber.new.skipped_callback(event)
    end

    it 'sends metrics for app level active record calls' do
      caller = ['/lib/model.rb:291:in `delete']
      statsd_client.expects(:increment)
      event = payload.new(klass: self, method: :delete, caller: caller)
      ActiveRecordStats::StatsSubscriber.new.skipped_callback(event)
    end

    it 'sends metrics if call has other call to active_record' do
      caller = ['activerecord-x.x/lib/active_record/transactions.rb:291:in `something',
                '/lib/model.rb:291:in `delete']
      statsd_client.expects(:increment)
      event = payload.new(klass: self, method: :delete, caller: caller)
      ActiveRecordStats::StatsSubscriber.new.skipped_callback(event)
    end

    it 'finds the correct caller' do
      caller = [
        '/bundle/klass.rb:291:in `delete',
        '/app/model.rb:200:in `delete'
      ]
      Rails.logger.expects(:info).with(regexp_matches(/model/))

      event = payload.new(klass: self, method: :delete, caller: caller)
      ActiveRecordStats::StatsSubscriber.new.skipped_callback(event)
    end

    it 'build tags from multiple calls' do
      Datadog.tracer.stubs(:active_span).returns(span)

      span.expects(:get_tag).with("zendesk.ar_skip_callback").returns("Existing tag")
      statsd_client.expects(:increment)
      span.expects(:set_tag).with("zendesk.ar_skip_callback", "Existing tag, Klass#delete")

      event = payload.new(klass: "Klass", method: :delete, caller: [])
      ActiveRecordStats::StatsSubscriber.new.skipped_callback(event)
    end
  end
end
