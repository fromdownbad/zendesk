require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 17

describe Zendesk::Stats::TicketBacklog do
  describe "When saving ticket backlog" do
    before do
      @duration = 1.hour
      @time_stamp = Time.parse("01/01/2012 00:00:00")
      StatRollup::Ticket.stubs(:create!)
    end

    describe "by account" do
      before do
        @account_id = 1
        @backlog = 10

        Zendesk::Stats::TicketBacklog.stubs(:ticket_count).returns([[@account_id, @backlog]])
      end
      it "correctly create a new TicketStat" do
        Zendesk::Stats::TicketBacklog.rollup_by_account(@account_id, @duration, @time_stamp)

        assert_received StatRollup::Ticket, :create! do |e|
          e.with(
            account_id: @account_id, duration: @duration, ts: @time_stamp,
            agent_id: 0, organization_id: 0, group_id: 0,
            type: "backlog", value: @backlog, num: nil
          )
        end
      end
    end

    describe "by group" do
      before do
        @account_id = 1
        @backlog1 = 10
        @group1 = 20
        @backlog2 = 20
        @group2 = 30

        Zendesk::Stats::TicketBacklog.stubs(:ticket_count).returns(
          [
            [[@account_id, @group1], @backlog1],
            [[@account_id, @group2], @backlog2]
          ]
        )
      end
    end

    describe "by organization" do
      before do
        @account_id = 1
        @backlog1 = 10
        @org1 = 2
        @backlog2 = 20
        @org2 = 3

        Zendesk::Stats::TicketBacklog.stubs(:ticket_count).returns(
          [
            [[@account_id, @org1], @backlog1],
            [[@account_id, @org2], @backlog2]
          ]
        )
      end
      it "correctly create a new TicketStat" do
        Zendesk::Stats::TicketBacklog.rollup_by_organization(@account_id, @duration, @time_stamp)
        assert_received StatRollup::Ticket, :create! do |e|
          e.with(
            account_id: @account_id, duration: @duration, ts: @time_stamp,
            agent_id: 0, organization_id: @org1, group_id: 0,
            type: "backlog", value: @backlog1, num: nil
          ).once

          e.with(
            account_id: @account_id, duration: @duration, ts: @time_stamp,
            agent_id: 0, organization_id: @org2, group_id: 0,
            type: "backlog", value: @backlog2, num: nil
          ).once
        end
      end
    end
  end
end
