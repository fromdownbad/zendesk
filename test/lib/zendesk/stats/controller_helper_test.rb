require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Stats::ControllerHelper do
  class ThisMockController
    include Zendesk::Stats::ControllerHelper
    def session
      @session ||= {}
    end

    attr_accessor :current_user

    def params
      @params ||= {}
    end

    def flash
      @flash ||= {}
    end
  end

  describe "Search stuff" do
    before do
      @controller = ThisMockController.new
      @controller.params.clear
      @controller.session.clear
      @controller.flash.clear
      @controller.current_user = stub(is_agent?: false, id: 1)
    end

    describe "store_last_search_in_session" do
      it "stores the given query in the session" do
        ts = Time.now

        Timecop.freeze(ts) do
          @controller.send(:store_last_search_in_session, "foo", "portal")
          assert_equal({last_search: {query: "foo", time: Time.now.to_i, clicked: false, origin: "portal"}}, @controller.session)
        end
      end

      it "stores the origin if given" do
        @controller.send(:store_last_search_in_session, "foo", "dropbox")
        assert_equal("dropbox", @controller.session[:last_search][:origin])
      end
    end

    describe "get_last_search_from_session" do
      it "returns the hash if not too old" do
        @controller.send(:store_last_search_in_session, "foo", "portal")
        hash = @controller.send(:get_last_search_from_session)
        assert_equal "foo", hash[:query]
        assert_equal "portal", hash[:origin]
      end

      it "returns nil if the query is too old" do
        @controller.send(:store_last_search_in_session, "foo", "portal")
        Timecop.travel(15.minutes) do
          assert_nil @controller.send(:get_last_search_from_session)
        end
      end
    end

    describe "store_search_stat" do
      describe "with a bad format" do
        it "does not blow up for nil format" do
          @controller.expects(:store_search_string).with(string: "foo", type: "search", user_id: 1, format: '').once
          @controller.send(:store_search_stat, "foo", format: "")
        end
      end

      describe "as an end user" do
        before do
          @controller.expects(:store_search_string).with(string: "foo", type: "search", user_id: 1, format: 'html').once
        end

        it "stores the search string" do
          @controller.send(:store_search_stat, "foo", format: 'html')
        end

        it "stores the search string in the session" do
          @controller.send(:store_search_stat, "foo", format: 'html')
          assert @controller.session[:last_search]
        end

        it "removes :results in the case of a failed search" do
          @controller.flash[:errors] = "hello"
          @controller.send(:store_search_stat, "foo", results: 0, format: 'html')
        end

        # without de-dup, we'd fail the .once expectation
        it "de-dups search from within a miniute of each other" do
          @controller.send(:store_search_stat, "foo", format: 'html')
          @controller.send(:store_search_stat, "foo", format: 'html')
          @controller.send(:store_search_stat, "foo", format: 'html')
          @controller.send(:store_search_stat, "foo", format: 'html')
        end
      end

      describe "on any page other than the first" do
        before do
          @controller.params[:page] = 2
          @controller.expects(:store_search_string).never
        end

        it "does not store a stat" do
          @controller.send(:store_search_stat, "foo", {})
        end
      end

      describe "for cursor based pagination" do
        it "#store_search_string does not raise an exception" do
          @controller.params[:page] = { "size": 3 }
          @controller.send(:store_search_stat, "foo", {})
        end
      end

      describe "as an admin" do
        before do
          @controller.current_user = stub(is_agent?: true, id: 1)
        end

        it "stores the search string with origin -> agent" do
          @controller.expects(:store_search_string).with { |h| h[:origin] == 'agent' }
          @controller.send(:store_search_stat, "foo", format: 'html')
        end

        it "does not store the search string in the session" do
          @controller.expects(:store_search_string).with { |h| h[:origin] == 'agent' }
          @controller.send(:store_search_stat, "foo", format: 'html')
          assert @controller.session[:last_search].nil?
        end
      end
    end

    describe "store_search_click" do
      before do
        @controller.session[:last_search] = {query: "foo", time: Time.now.to_i, clicked: false, origin: 'portal'}
        @expectation = @controller.expects(:store_search_string)
      end

      it "stores a click event" do
        @expectation.with(user_id: 1, type: "click", string: "foo", entry_id: 1, origin: 'portal')
        @controller.send(:store_search_click, entry_id: 1)
      end

      it "does not store a click event for an old search" do
        @expectation.never
        @controller.session[:last_search][:time] -= 15.minutes

        @controller.send(:store_search_click, entry_id: 1)
      end

      it "sets the :clicked property to true" do
        @controller.send(:store_search_click, entry_id: 1)
        assert @controller.session[:last_search][:clicked]
      end

      it "gets the origin property from the session" do
        @controller.session[:last_search][:origin] = 'dropbox'
        @expectation.with(user_id: 1, type: "click", string: "foo", entry_id: 1, origin: 'dropbox')
        @controller.send(:store_search_click, entry_id: 1)
      end
    end

    describe "store_search_ticket_stat" do
      before do
        @controller.params[:ticket_from_search] = "foo"
        @expectation = @controller.expects(:store_search_string)
      end

      it "stores the search string as a merge from a hash" do
        @expectation.with do |hash|
          hash[:ticket_id] == 1 &&
          hash[:type] == 'ticket' &&
          hash[:user_id] == 1 &&
          hash[:string] == 'foo' &&
          hash[:format] == 'html'
        end
        @controller.send(:store_search_ticket_stat, ticket_id: 1, format: 'html')
      end

      it "bails when an empty search string is given" do
        @controller.params[:ticket_from_search] = ''
        @expectation.never
        @controller.send(:store_search_ticket_stat, ticket_id: 1)
      end
    end
  end
end
