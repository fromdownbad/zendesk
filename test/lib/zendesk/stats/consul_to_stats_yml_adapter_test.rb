require_relative '../../../support/test_helper'
require_relative '../../../../lib/zendesk/stats/consul_to_stats_yml_adapter'

SingleCov.covered!

describe Zendesk::Stats::ConsulToStatsYmlAdapter do
  let(:servers) do
    [
      { host: '10.210.165.112', port: 27017, tags: %w[replicaset-13-1 shard-13001 shard-13002 shard-13003 statsdb-production_stats_13001 statsdb-production_stats_13002 statsdb-production_stats_13003] },
      { host: '10.210.151.5',   port: 27017, tags: %w[replicaset-13-2 shard-13004 shard-13005 shard-13006 statsdb-production_stats_13004 statsdb-production_stats_13005 statsdb-production_stats_13006] },
      { host: '10.210.152.228', port: 27017, tags: %w[replicaset-13-3 shard-13007 shard-13008 shard-13009 statsdb-production_stats_13007 statsdb-production_stats_13008 statsdb-production_stats_13009] },
      { host: '10.210.163.227', port: 27017, tags: %w[replicaset-13-4 shard-13010 shard-13011 shard-13012 statsdb-production_stats_13010 statsdb-production_stats_13011 statsdb-production_stats_13012] }
    ]
  end

  let(:stats_yml) do
    {
      'mongo' => {
        'replicasets' => {
          '13-1' => {
            'hosts' => [
              ['10.210.165.112', 27017]
            ]
          },
          '13-2' => {
            'hosts' => [
              ['10.210.151.5', 27017]
            ]
          },
          '13-3' => {
            'hosts' => [
              ['10.210.152.228', 27017]
            ]
          },
          '13-4' => {
            'hosts' => [
              ['10.210.163.227', 27017]
            ]
          },
        },
        'shards' =>  {
          13001 => {
            'replicaset' => '13-1',
            'stats_db' => 'production_stats_13001'
          },
          13002 => {
            'replicaset' => '13-1',
            'stats_db' => 'production_stats_13002'
          },
          13003 => {
            'replicaset' => '13-1',
            'stats_db' => 'production_stats_13003'
          },
          13004 => {
            'replicaset' => '13-2',
            'stats_db' => 'production_stats_13004'
          },
          13005 => {
            'replicaset' => '13-2',
            'stats_db' => 'production_stats_13005'
          },
          13006 => {
            'replicaset' => '13-2',
            'stats_db' => 'production_stats_13006'
          },
          13007 => {
            'replicaset' => '13-3',
            'stats_db' => 'production_stats_13007'
          },
          13008 => {
            'replicaset' => '13-3',
            'stats_db' => 'production_stats_13008'
          },
          13009 => {
            'replicaset' => '13-3',
            'stats_db' => 'production_stats_13009'
          },
          13010 => {
            'replicaset' => '13-4',
            'stats_db' => 'production_stats_13010'
          },
          13011 => {
            'replicaset' => '13-4',
            'stats_db' => 'production_stats_13011'
          },
          13012 => {
            'replicaset' => '13-4',
            'stats_db' => 'production_stats_13012'
          }
        }
      },
      'redis' => {
        'host' => 'localhost:6379'
      },
      'json_output' => {
        'path' => '/var/spool/flume'
      }
    }
  end

  before do
    Zendesk::Configuration.stubs(:servers).returns(servers)
  end

  with_env ZENDESK_STATS_REDIS_HOST: 'localhost:6379' do
    it 'extracts stats db name from server and shard information' do
      assert_equal stats_yml, Zendesk::Stats::ConsulToStatsYmlAdapter.config
    end
  end
end
