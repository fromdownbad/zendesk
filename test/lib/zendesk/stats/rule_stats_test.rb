require_relative "../../../support/test_helper"

SingleCov.not_covered!

describe 'RuleStatsUnit' do
  fixtures :tickets, :rules, :users, :accounts

  describe "When a rule (trigger/automation) acts on a ticket" do
    before do
      @user = users(:minimum_agent)
      @rule = rules(:trigger_notify_requester_of_received_request)
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@user)
    end

    describe "applying the rule's actions" do
      before do
        @rule.apply_actions(@ticket)
      end

      describe "subsequently, saving the ticket" do
        it "stores last affected on ticket save" do
          Zendesk::ClassicStats::RuleUsage.expects(:store).with(
            account_id: @ticket.account.id,
            shard_id: @ticket.account.shard_id, ticket_id: @ticket.id,
            user_id: @user.id, rule_id: @rule.id,
            rule_type: @rule.rule_type
          )
          @ticket.save!
        end
      end
    end

    describe "Ticket saved without any rules applied" do
      before do
        # Making sure old applied rules don't get re-recorded
        @rule.apply_actions(@ticket)
        @ticket.save!
        Zendesk::ClassicStats::RuleUsage.expects(:store).never
      end
      it "stores no stat" do
        @ticket.save!
      end
    end
  end

  describe "When a macro acts on a ticket" do
    before do
      @user = users(:minimum_agent)
      @macro = rules(:macro_incident_escalation)
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@user)
      @ticket.macro_ids = [@macro.id]
    end

    describe "subsequently, saving the ticket" do
      it "stores last applied macro on ticket save" do
        Zendesk::ClassicStats::RuleUsage.expects(:store).with(
          account_id: @ticket.account.id,
          shard_id: @ticket.account.shard_id, ticket_id: @ticket.id,
          user_id: @user.id, rule_id: @macro.id,
          rule_type: 'macro'
        )
        @ticket.save!
      end
    end
  end
end
