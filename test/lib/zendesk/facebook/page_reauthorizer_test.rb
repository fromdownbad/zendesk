require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Facebook::PageReauthorizer do
  fixtures :accounts, :facebook_pages

  before do
    @account = accounts(:minimum)
    Facebook::Page.where(account_id: @account.id).update_all(state: ::Facebook::Page::State::UNAUTHORIZED)
  end

  describe '#run' do
    it 'should go through accounts and call reauthorize_pages' do
      Account.active.pod_local.shard_unlocked.each do |account|
        authorizer = stub
        Zendesk::Facebook::PageReauthorizer.expects(:new).with(account).returns(authorizer)
        authorizer.expects(:reauthorize_pages)
      end
      Zendesk::Facebook::PageReauthorizer.run
    end

    it 'should mark all pages as active' do
      Zendesk::Facebook::PageReauthorizer.run
      Facebook::Page.where(account_id: @account.id).each do |page|
        assert_equal ::Facebook::Page::State::ACTIVE, page.state
      end
    end
  end

  describe '#reauthorize_pages' do
    it 'should go through each page and reauthorize them' do
      authorizer = Zendesk::Facebook::PageReauthorizer.new(@account)
      authorizer.reauthorize_pages
      Facebook::Page.where(account_id: @account.id).each do |page|
        assert_equal ::Facebook::Page::State::ACTIVE, page.state
      end
    end

    it 'should not reauthorize inactive pages' do
      page = facebook_pages(:minimum_facebook_page_1)
      page.update_column(:state, ::Facebook::Page::State::INACTIVE)
      authorizer = Zendesk::Facebook::PageReauthorizer.new(@account)
      authorizer.reauthorize_pages
      assert_equal page.reload.state, ::Facebook::Page::State::INACTIVE
    end
  end
end
