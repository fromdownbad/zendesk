require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::RequestOriginDetails do
  let(:env) do
    {
      'zendesk.account'                        => 'account',
      'action_dispatch.remote_ip'              => 'remote_ip',
      'HTTP_REFERER'                           => 'http://www.referer.com/some/referer?with=info',
      'HTTP_X_FORCE_EXCEPTION_LOCALE'          => 'true',
      'HTTP_X_ZENDESK_LOTUS_VERSION'           => 'lotus_version',
      'HTTP_X_ZENDESK_LOTUS_FEATURE'           => 'lotus_feature',
      'HTTP_X_ZENDESK_LOTUS_INITIAL_USER_ID'   => 'lotus_initial_user_id',
      'HTTP_X_ZENDESK_LOTUS_INITIAL_USER_ROLE' => 'lotus_initial_user_role',
      'HTTP_X_ZENDESK_LOTUS_TAB_ID'            => 'lotus_tab_id',
      'HTTP_X_ZENDESK_LOTUS_UUID'              => 'lotus_uuid',
      'HTTP_X_ZENDESK_LOTUS_EXPIRY_REFETCH'    => 'lotus_expiry_refetch',
      "HTTP_X_ZENDESK_RADAR_SOCKET"            => 'lotus_radar_client_socket',
      "HTTP_X_ZENDESK_RADAR_STATUS"            => 'lotus_radar_client_status',
      'HTTP_X_ZENDESK_APP_ID'                  => 'app_id',
      'HTTP_X_ZENDESK_APP_NAME'                => 'app_name',
      'HTTP_X_ZENDESK_POD_RELAY'               => 'pod_relay',
      "HTTP_X_ZENDESK_CLIENT"                  => 'mobile_client',
      "HTTP_X_ZENDESK_INTEGRATION"             => 'mobile_integration',
      "HTTP_X_ZENDESK_INTEGRATION_VERSION"     => 'mobile_integration_version',
      "HTTP_X_ZENDESK_CLIENT_VERSION"          => 'mobile_client_version',
      "HTTP_X_ZENDESK_RULE_ADMIN_VERSION"      => 'rule_admin_version'
    }
  end
  let(:subject) { Zendesk::RequestOriginDetails.new(env) }

  describe 'with all headers' do
    it 'sets origin details' do
      assert_equal subject.account, 'account'
      assert_equal subject.lotus_version, 'lotus_version'
      assert_equal subject.lotus_feature, 'lotus_feature'
      assert_equal subject.app_id, 'app_id'
      assert_equal subject.app_name, 'app_name'
      assert_equal subject.referer, '/some/referer'
      assert_equal subject.request_source, 'app'
      assert_equal subject.remote_ip, 'remote_ip'
      assert_equal subject.pod_relay, 'pod_relay'
      assert_equal subject.lotus_initial_user_id, 'lotus_initial_user_id'
      assert_equal subject.lotus_initial_user_role, 'lotus_initial_user_role'
      assert_equal subject.lotus_tab_id, 'lotus_tab_id'
      assert_equal subject.lotus_uuid, 'lotus_uuid'
      assert_equal subject.lotus_expiry_refetch, 'lotus_expiry_refetch'
      assert_equal subject.lotus_radar_client_socket, 'lotus_radar_client_socket'
      assert_equal subject.lotus_radar_client_status, 'lotus_radar_client_status'
      assert_equal subject.force_exception_locale, 'true'
      assert_equal subject.mobile_client, 'mobile_client'
      assert_equal subject.mobile_integration, 'mobile_integration'
      assert_equal subject.mobile_integration_version, 'mobile_integration_version'
      assert_equal subject.mobile_client_version, 'mobile_client_version'
      assert_equal subject.rule_admin_version, 'rule_admin_version'
    end
  end

  def it_handles_nil_values(request_origin)
    # api is a catch all for request_source
    assert_equal request_origin.request_source, 'api'
    assert_nil request_origin.account
    assert_nil request_origin.lotus_version
    assert_nil request_origin.lotus_feature
    assert_nil request_origin.app_id
    assert_nil request_origin.app_name
    assert_nil request_origin.referer
    assert_nil request_origin.remote_ip
    assert_nil request_origin.pod_relay
    assert_nil request_origin.lotus_initial_user_id
    assert_nil request_origin.lotus_initial_user_role
    assert_nil request_origin.lotus_tab_id
    assert_nil request_origin.lotus_uuid
    assert_nil request_origin.lotus_expiry_refetch
    assert_nil request_origin.lotus_radar_client_socket
    assert_nil request_origin.lotus_radar_client_status
    assert_nil request_origin.force_exception_locale
    assert_nil request_origin.mobile_client
    assert_nil request_origin.mobile_integration
    assert_nil request_origin.mobile_integration_version
    assert_nil request_origin.mobile_client_version
  end

  describe '#to_h' do
    let(:origin_hash) { subject.to_h }
    it 'exposes attributes as a hash' do
      assert_equal origin_hash[:request_source], 'app'
      assert_equal origin_hash[:lotus_version], 'lotus_version'
    end
  end

  describe '#lotus?' do
    describe 'when request is from lotus' do
      before { env.delete('HTTP_X_ZENDESK_APP_ID') }

      it 'returns true' do
        assert subject.lotus?
      end
    end

    describe 'when request is NOT from lotus' do
      it 'returns false' do
        refute subject.lotus?
      end
    end
  end

  describe '#zendesk_mobile_client?' do
    describe 'when request is from a zendesk mobile client' do
      before do
        env.merge!(
          'HTTP_X_ZENDESK_CLIENT' => 'mobile/ios/apps/support'
        )
      end

      it 'returns true' do
        assert subject.zendesk_mobile_client?
      end
    end

    describe 'when request is NOT from a zendesk mobile client' do
      before do
        env.merge!(
          'HTTP_X_ZENDESK_CLIENT' => 'other'
        )
      end

      it 'returns false' do
        refute subject.zendesk_mobile_client?
      end
    end
  end

  describe 'with blank headers' do
    it 'returns nil values' do
      env.transform_values! { nil }
      it_handles_nil_values(Zendesk::RequestOriginDetails.new(env))
    end
  end

  describe 'with no env' do
    it 'returns nil values' do
      it_handles_nil_values(Zendesk::RequestOriginDetails.new({}))
    end
  end

  describe '#request_source' do
    describe 'with app_id' do
      it 'returns app' do
        assert_equal subject.request_source, 'app'
      end
    end

    describe 'without app_id' do
      before { env.delete('HTTP_X_ZENDESK_APP_ID') }

      describe 'with lotus version' do
        it 'returns lotus' do
          assert_equal subject.request_source, 'lotus'
        end
      end

      describe 'without lotus version' do
        before { env.delete('HTTP_X_ZENDESK_LOTUS_VERSION') }

        it 'returns api' do
          assert_equal subject.request_source, 'api'
        end
      end
    end
  end

  describe '#rule_admin?' do
    describe 'when request is from support-rule-admin' do
      it 'returns true' do
        assert subject.rule_admin?
      end
    end

    describe 'when request is NOT from support-rule-admin' do
      before { env.delete('HTTP_X_ZENDESK_RULE_ADMIN_VERSION') }

      it 'returns false' do
        refute subject.rule_admin?
      end
    end
  end
end
