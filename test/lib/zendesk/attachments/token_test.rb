require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::Attachments::Token do
  fixtures :accounts, :users, :attachments

  describe "new token" do
    let(:attachment) { attachments(:attachment_entry) }
    let(:account) { attachment.account }
    let(:user) { attachment.author }
    let(:token) { Zendesk::Attachments::Token.new(account: account, user: user, attachment: attachment) }

    before { Timecop.freeze }

    it "sets the attachment_id" do
      assert_equal attachment.id, token.claims[:attachment_id]
    end

    it "sets the account_id" do
      assert_equal account.id, token.claims[:account_id]
    end

    it "sets the user_id" do
      assert_equal user.id, token.claims[:user_id]
    end

    it "sets the iat" do
      assert_equal Time.now.to_i, token.claims[:iat]
    end

    it "sets the jti" do
      refute_nil token.claims[:jti]
    end

    it "allows the token to be used only once" do
      refute token.used!, "token has not been used"
      assert token.used!, "token is used"
    end

    it "handles old tokens without a jti" do
      token.claims.delete :jti
      assert token.used!, "token is used since it doesn't have a jti"
    end

    describe "with an anonymous_user" do
      let(:token) { Zendesk::Attachments::Token.new(account: account, user: account.anonymous_user, attachment: attachment) }

      it "sets the user_id to nil" do
        assert_nil token.user_id
      end

      it "sets the user_id to nil after decryption" do
        encrypted = token.encrypt
        new_token = Zendesk::Attachments::Token.new(token: encrypted)

        assert_nil new_token.user_id
      end
    end

    describe "encryption" do
      it "encrypts" do
        assert_not_nil token.encrypt
      end

      it "has 5 segments" do
        assert_equal 5, token.encrypt.split('.').size
      end
    end
  end

  describe "from an existing token" do
    let(:existing_token) { "eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..HON_MY4uqStBzfj1FKdiuQ.qjjPpS484-wWt53cIvh_FjqLeHoeBxfYld3H9xuOA-GtimrGhQ_dCaw2Rs0LV6Gu_CA6A9jGvJWKeLUl063cgMkjZTIV9l42tPEt_Jgvh-APUK-zR_VDkbOlEI4ig5wHDBt7GF63D9Qs_WFOtYTFmeOTKThhqideRvyCzHJvkCpgAs15JxRU2ewY8Nfg50IAGCN0DDMFfFWl21AdRx6y4EaNB6dwUuUtSqxIL4Hg4hETBceJzBM_7JUm3p-bEX2V0BApxi25_LpHxEQeOAWHyg.wfk4zrvdl_nZCRnF4lQvow" }
    let(:issued_at) { Time.at(1422313926) }

    before { Timecop.freeze(issued_at) }

    it "decrypts" do
      token = Zendesk::Attachments::Token.new(token: existing_token)
      assert_equal 90538, token.account_id
      assert_equal 90298, token.attachment_id
      assert_nil token.user_id
    end

    it "accepts a small amount of jitter" do
      Timecop.travel(-25) do
        assert Zendesk::Attachments::Token.new(token: existing_token)
      end
    end

    it "balks at exessive jitter" do
      Timecop.travel(-35) do
        assert_raises(Zendesk::Attachments::Token::InvalidJWE) do
          assert Zendesk::Attachments::Token.new(token: existing_token)
        end
      end
    end

    it "good up until the TTL" do
      Timecop.travel(Zendesk::Attachments::Token::TTL) do
        assert token = Zendesk::Attachments::Token.new(token: existing_token)
        refute token.expired?
      end
    end

    it "decrypts an expired token" do
      Timecop.travel(Zendesk::Attachments::Token::TTL + 1) do
        assert token = Zendesk::Attachments::Token.new(token: existing_token)
        assert token.expired?
      end
    end
  end

  it "doesn't accept an unencrypted token" do
    token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpYXQiOjE0MjIzMzkzOTcsImF0dGFjaG1lbnRfaWQiOjYsImFjY291bnRfaWQiOjF9."
    assert_raises(JSON::JWS::UnexpectedAlgorithm) do
      Zendesk::Attachments::Token.new(token: token)
    end
  end
end
