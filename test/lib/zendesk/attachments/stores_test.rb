require_relative "../../../support/test_helper"
require_relative "../../../support/attachment_test_helper"

SingleCov.covered! uncovered: 12

describe Zendesk::Attachments::Stores do
  let(:path_prefix) { "#{Rails.root}/test/files" }
  let(:attachment) { create_attachment("#{path_prefix}/normal_1.jpg") } # default stores in :fs
  let(:thumbnail) do
    attachment.thumbnails.first
  end
  let(:expirable_attachment) { create_expirable_attachment("#{path_prefix}/normal_1.jpg", accounts(:minimum)) }

  describe '#has_s3_storage' do
    it 'is based on s3 storage in stores' do
      refute attachment.has_s3_storage?
      attachment.stores |= [:s3]
      assert attachment.has_s3_storage?
    end
  end

  describe "#get_s3_storage" do
    before do
      attachment.stubs(:cloud_stores).returns([:s3eu])
    end

    it "returns a class-preferred s3 store if possible" do
      attachment.stores = [:s3eu, :s3, :s3apne, :cf, :fs, :pick_me]
      assert_equal :s3, attachment.get_s3_storage.attachment_options[:store_name]
    end

    it "returns cloud-preferred if no class-preferred" do
      attachment.stores = [:s3eu, :s3apne, :fs]
      assert_equal :s3eu, attachment.get_s3_storage.attachment_options[:store_name]
    end

    it "returns an available s3 store if no preferred" do
      attachment.stores = [:s3apne, :fs]
      assert_equal :s3apne, attachment.get_s3_storage.attachment_options[:store_name]
    end

    it "raises an exception if no s3 stores" do
      attachment.stores = [:fs]
      assert_raises StandardError do
        attachment.get_s3_storage.attachment_options[:store_name]
      end
    end
  end

  describe "synchronize_stores" do
    it "defers to StoresSynchronizer" do
      Zendesk::Stores::StoresSynchronizer.any_instance.
        expects(:add_to_preferred_stores).
        with(attachment).
        once

      attachment.synchronize_stores
    end
  end

  describe "thumbnail?" do
    it "should be false if not a thumbnail" do
      refute attachment.thumbnail?
    end

    it "should be true for a thumbnail" do
      assert thumbnail.thumbnail?
    end

    describe "expirable_attachment" do
      # since it also mixes in Stores
      it "should be false" do
        refute expirable_attachment.thumbnail?
      end
    end
  end

  describe "thumbnail_dimension" do
    it "should detect the thumbnail dimension properly (removes flags)" do
      assert_equal "80x80", attachment.thumbnail_dimension
    end
  end

  describe 'ClassMethods' do
    it 'should define preferred_stores' do
      assert_equal [:s3, :fs].sort, Attachment.preferred_stores
    end

    it 'should define s3_stores' do
      s3_stores = Attachment.attachment_backends.values.
        map { |v| v[:options][:store_name] }.
        select { |sn| sn.to_s.starts_with?('s3') }
      assert_equal s3_stores.sort, Attachment.s3_stores.sort
    end

    it 'should define default_s3_stores' do
      hold = Attachment.attachment_backends[:s3][:options][:default]
      begin
        Attachment.attachment_backends[:s3][:options][:default] = true
        assert_equal [:s3].sort, Attachment.default_s3_stores
      ensure
        Attachment.attachment_backends[:s3][:options][:default] = hold
      end
    end
  end
end
