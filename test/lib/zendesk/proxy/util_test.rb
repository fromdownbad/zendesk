require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Proxy::Util do
  it "nil should be an invalid domain" do
    assert_equal(false, Zendesk::Proxy::Util.is_valid_domain?(nil))
  end

  it "empty string should be an invalid domain" do
    assert_equal(false, Zendesk::Proxy::Util.is_valid_domain?(''))
  end

  it "a generic domain should be valid" do
    assert Zendesk::Proxy::Util.is_valid_domain?('example.com')
  end

  it "localhost should be invalid" do
    assert_equal(false, Zendesk::Proxy::Util.is_valid_domain?('localhost'))
  end

  it "ips in the 10 dot range should be invalid" do
    assert_equal(false, Zendesk::Proxy::Util.is_valid_domain?('10.x.x.x'))
  end

  it "ips in the 127 dot range should be invalid" do
    assert_equal(false, Zendesk::Proxy::Util.is_valid_domain?('127.x.x.x'))
  end

  it "ips in the 172 dot xx range should be invalid" do
    assert(Zendesk::Proxy::Util.is_valid_domain?('172.15.x.x'))
    assert_equal(false, Zendesk::Proxy::Util.is_valid_domain?('172.16.x.x'))
    assert_equal(false, Zendesk::Proxy::Util.is_valid_domain?('172.31.x.x'))
    assert(Zendesk::Proxy::Util.is_valid_domain?('172.32.x.x'))
  end

  it "ips in the 192 dot 168 range should be invalid" do
    assert_equal(false, Zendesk::Proxy::Util.is_valid_domain?('192.168.x.x'))
  end

  it "extracts domain from uris" do
    uri = 'http://example.com/foo/bar'
    assert_equal('example.com', Zendesk::Proxy::Util.extract_domain(uri))
  end

  it "extracts domain from secure uris" do
    uri = 'https://example.com/foo/bar'
    assert_equal('example.com', Zendesk::Proxy::Util.extract_domain(uri))
  end

  it "returns domain if its already a domain" do
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('example.com')
    assert_equal 'www.example.com', Zendesk::Proxy::Util.extract_domain('www.example.com')
  end

  it "does not return domain if uri is badly formatted" do
    assert_nil Zendesk::Proxy::Util.extract_domain('username:password@example:com')
    assert_nil Zendesk::Proxy::Util.extract_domain('username@password:example:com')
    assert_nil Zendesk::Proxy::Util.extract_domain('username@password.example:com')
    assert_nil Zendesk::Proxy::Util.extract_domain('username:127.x.x.x')
    assert_nil Zendesk::Proxy::Util.extract_domain('username@password:127.x.x.x')
    assert_nil Zendesk::Proxy::Util.extract_domain('username:example.com')
    assert_nil Zendesk::Proxy::Util.extract_domain('username@password:example.com')
  end

  it "not return domain unless scheme is http or https" do
    assert_equal '127.x.x.x',       Zendesk::Proxy::Util.extract_domain('http://127.x.x.x')
    assert_equal '127.x.x.x',       Zendesk::Proxy::Util.extract_domain('https://127.x.x.x')
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('http://example.com')
    assert_equal 'www.example.com', Zendesk::Proxy::Util.extract_domain('https://www.example.com')
    assert_nil Zendesk::Proxy::Util.extract_domain('ssh://127.x.x.x')
    assert_nil Zendesk::Proxy::Util.extract_domain('scheme://127.x.x.x')
    assert_nil Zendesk::Proxy::Util.extract_domain('ftp://example.com')
    assert_nil Zendesk::Proxy::Util.extract_domain('ftps://example.com')
    assert_nil Zendesk::Proxy::Util.extract_domain('mailto://www.example.com')
    assert_nil Zendesk::Proxy::Util.extract_domain('scheme://www.example.com')
  end

  it "returns domain if no scheme is provided" do
    assert_equal '127.x.x.x',       Zendesk::Proxy::Util.extract_domain('127.x.x.x')
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('example.com')
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('username:password@example.com')
    assert_equal 'www.example.com', Zendesk::Proxy::Util.extract_domain('www.example.com')
  end

  it "returns domain name if port is included in the uri" do
    assert_equal '127.x.x.x',       Zendesk::Proxy::Util.extract_domain('127.x.x.x:1234')
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('example.com:1234')
    assert_equal 'www.example.com', Zendesk::Proxy::Util.extract_domain('www.example.com:1234')
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('http://example.com:1234')
    assert_equal 'www.example.com', Zendesk::Proxy::Util.extract_domain('https://www.example.com:1234')
  end

  it "returns domain name if credential is included in the uri" do
    assert_equal '127.x.x.x',       Zendesk::Proxy::Util.extract_domain('username:password@127.x.x.x')
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('username:password@example.com')
    assert_equal 'www.example.com', Zendesk::Proxy::Util.extract_domain('username:password@www.example.com')
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('http://username:password@example.com')
    assert_equal 'www.example.com', Zendesk::Proxy::Util.extract_domain('https://username:password@www.example.com')
  end

  it "returns domain name if credential and port is included in the uri" do
    assert_equal '127.x.x.x',       Zendesk::Proxy::Util.extract_domain('username:password@127.x.x.x:1234')
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('username:password@example.com:1234')
    assert_equal 'www.example.com', Zendesk::Proxy::Util.extract_domain('username:password@www.example.com:1234')
    assert_equal 'example.com',     Zendesk::Proxy::Util.extract_domain('http://username:password@example.com:1234')
    assert_equal 'www.example.com', Zendesk::Proxy::Util.extract_domain('https://username:password@www.example.com:1234')
  end

  it "zendesk domains should be invalid" do
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('zendesk.com')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('www.zendesk.com')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('zendesk.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('www.zendesk.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@zendesk.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@www.zendesk.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://zendesk.com')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://www.zendesk.com')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://zendesk.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://www.zendesk.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://username:password@zendesk.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://username:password@www.zendesk.com:1234')
  end

  it "zendesk staging domains should be invalid" do
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('zendesk-staging.com')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('www.zendesk-staging.com')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('zendesk-staging.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('www.zendesk-staging.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@zendesk-staging.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@www.zendesk-staging.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://zendesk-staging.com')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://www.zendesk-staging.com')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://zendesk-staging.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://www.zendesk-staging.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://username:password@zendesk-staging.com:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('http://username:password@www.zendesk-staging.com:1234')
  end

  it "localhost should with credential or port should be invalid" do
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('localhost:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@localhost')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@localhost:1234')
  end

  it "ips in the 10 dot range with credential or port should be invalid" do
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('10.x.x.x:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@10.x.x.x')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@10.x.x.x:1234')
  end

  it "ips in the 127 dot range with credential or port should be invalid" do
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('127.x.x.x:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@127.x.x.x')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@127.x.x.x:1234')
  end

  it "ips in the 172 dot xx range with credential or port should be invalid" do
    assert(Zendesk::Proxy::Util.is_valid_domain?('172.15.x.x:1234'))
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('172.16.x.x:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('172.31.x.x:1234')
    assert(Zendesk::Proxy::Util.is_valid_domain?('172.32.x.x:1234'))
    assert(Zendesk::Proxy::Util.is_valid_domain?('username:password@172.15.x.x'))
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@172.16.x.x')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@172.31.x.x')
    assert(Zendesk::Proxy::Util.is_valid_domain?('username:password@172.32.x.x'))
    assert(Zendesk::Proxy::Util.is_valid_domain?('username:password@172.15.x.x:1234'))
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@172.16.x.x:1234')
    assert_equal false, Zendesk::Proxy::Util.is_valid_domain?('username:password@172.31.x.x:1234')
    assert(Zendesk::Proxy::Util.is_valid_domain?('username:password@172.32.x.x:1234'))
  end
end
