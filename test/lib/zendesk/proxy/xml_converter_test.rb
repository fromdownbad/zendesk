require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 12

describe Zendesk::Proxy::XmlConverter do
  it "tests parser equivalence" do
    strings = [
      '<foo><bar>1</bar><bar>2</bar><goo>j</goo></foo>',
      '<foo></foo>',
      '<bar><foo><too>x</too></foo></bar>'
    ]

    strings.each do |s|
      assert_equal(Hash.from_xml(s), Zendesk::Proxy::XmlConverter.xml_to_hash(s))
    end

    h = { 'foo' => { 'bar' => ['1', '2'], 'goo' => 'j' }}
    x = '<foo><bar>1</bar><bar>2</bar><goo>j</goo></foo>'
    assert_equal(h, Zendesk::Proxy::XmlConverter.xml_to_hash(x))

    h = {"foo" => {"goo" => "a", "bar" => ["1", "2"]}}
    x = "<foo><bar>1</bar><bar>2</bar><goo>a</goo>    \n </foo>"
    assert_equal(h, Zendesk::Proxy::XmlConverter.xml_to_hash(x))
  end

  it "tests harvest" do
    x = FixtureHelper::Files.read('harvest.xml')
    assert_equal(Hash.from_xml(x), Zendesk::Proxy::XmlConverter.xml_to_hash(x))
  end

  it "tests array conversion" do
    x = '<foos type="array"><foo>hep</foo></foos>'
    h = { "foos" => ["hep"] }

    assert_equal Hash.from_xml(x), h
    assert_equal Zendesk::Proxy::XmlConverter.xml_to_hash(x), h
  end

  it "tests highrise" do
    x = FixtureHelper::Files.read('highrise.xml')
    assert_equal(Hash.from_xml(x), Zendesk::Proxy::XmlConverter.xml_to_hash(x))
  end
end
