require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::Proxy::Resource do
  it "tests initialization" do
    params = { domain: 'domain', user: '123456' }

    resource = Zendesk::Proxy::Resource.new do |r|
      r.host       = params[:domain]
      r.userid     = params[:user]
      r.password   = params[:pass]
      r.use_ssl    = params[:use_ssl] == 'true'
      r.media_type = params[:media_type]
      r.cache      = params[:cache_gets] != 'false'
    end

    assert_equal resource.host, params[:domain]
    assert_equal resource.userid, params[:user]
    assert_equal false, resource.use_ssl
    assert resource.cache
  end
end
