require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 25 # TODO: Hash and Array are untested

describe Zendesk::Scrub do
  fixtures :all

  describe "scrubbing" do
    let(:audit) { events(:create_audit_for_minimum_ticket_1) }
    let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }

    before do
      @ticket = tickets(:minimum_1)
      @ticket.status_id = StatusType.DELETED
      @ticket.will_be_saved_by(users(:systemuser))
    end

    describe 'with taggings' do
      before do
        @tag_name = 'freddy'
        @tagging  = @ticket.account.taggings.create(tag_name: @tag_name, taggable: @ticket)
      end

      it 'should update the TagScore to 1' do
        @ticket_2  = tickets(:minimum_2)
        @tagging_2 = @ticket.account.taggings.create(tag_name: @tag_name, taggable: @ticket_2)

        Zendesk::Scrub.scrub_ticket_and_associations(@ticket)
        assert_equal 1, TagScore.where(tag_name: @tag_name).first.score
      end

      it 'should delete taggings and *not* remove the TagScore' do
        assert_equal TagScore.where(tag_name: @tag_name).count, 1
        Zendesk::Scrub.scrub_ticket_and_associations(@ticket)
        assert_equal 0,  @ticket.taggings.count
        assert_equal "", @ticket.current_tags
        assert_equal 1,  TagScore.where(tag_name: @tag_name).count
        assert_equal 0,  TagScore.where(tag_name: "X").count
      end

      it 'should make the ticket scrubbed?' do
        refute @ticket.scrubbed?
        Zendesk::Scrub.scrub_ticket_and_associations(@ticket)
        @ticket.reload
        assert @ticket.scrubbed?
      end
    end

    it "reflects on ticket associations" do
      assert_same_elements expected_associations, Zendesk::Scrub.send(:dependent_associations, @ticket).map(&:name)
    end

    it "updates scrubbable attributes with X" do
      attrs = @ticket.attributes.select { |k, _v| k == "description" }
      Zendesk::Scrub.send(:scrub_attributes, @ticket, attrs, save_record: true, ticket: @ticket)
      @ticket.reload
      assert_equal "X", @ticket.description
    end

    # TODO: @drhenner improve test ... just here to get coverage
    it "does nothing with prevent_deletion_if_churned" do
      Arturo.enable_feature!(:prevent_deletion_if_churned)
      Zendesk::Scrub.scrub_ticket_and_associations(@ticket)
      @ticket.reload
      assert_equal "minimum 1 ticket", @ticket.description
    end

    describe 'with custom fields' do
      before do
        @field = ticket_fields(:field_integer_custom)
        @entry = @ticket.ticket_field_entries.create(ticket_field: @field, value: 18)
      end

      it "updates an numeric custom field with a nil value" do
        scrub_ticket(@ticket)
        assert_equal '0', @entry.reload.value
      end
    end

    describe 'with ticket deflections' do
      before do
        @ticket.ticket_deflection = ticket_deflection
      end

      it 'scrubs ticket deflection enquiry' do
        scrub_ticket(@ticket)
        @ticket.reload
        assert_equal "SCRUBBED", @ticket.ticket_deflection.enquiry
      end
    end

    it "updates the ticket subject with SCRUBBED" do
      scrub_ticket(@ticket)
      @ticket.reload
      assert_equal "SCRUBBED", @ticket.subject
    end

    # this can hopefully go away once we solved all these issues
    it "records yaml errors" do
      Zendesk::Scrub.expects(:scrub_attributes).raises(ActiveRecord::SerializationTypeMismatch)
      assert_raises(ActiveRecord::SerializationTypeMismatch) { scrub_ticket(@ticket) }
    end

    it "records ArgumentError" do
      Zendesk::Scrub.expects(:scrub_attributes).raises(ArgumentError)
      assert_raises(ArgumentError) { scrub_ticket(@ticket) }
    end

    it "updates the voice_comments with Hash data in serialized fields" do
      @ticket.voice_comments.create!(data: { blah: 'Test' }, value_reference: { blah: 'Test' }, is_public: true, account_id: @ticket.account_id, author: users(:minimum_author), audit: audit)
      @ticket.reload
      ticket = scrub_ticket(@ticket)
      ticket.reload
      assert_equal "SCRUBBED", ticket.subject
      assert_equal Hash('X' => 'X'), ticket.voice_comments.first.data
    end

    it "updates the tweets with serialized values" do
      @ticket.tweets.create!(body: ['Pirate Day'], value: ['Arg'], is_public: true, account_id: @ticket.account_id, sender_mth_id: 'value_previous', author: users(:minimum_author), audit: audit)
      ticket = scrub_ticket(@ticket)
      ticket.reload
      assert_equal "SCRUBBED", ticket.subject
      assert_equal ['X'], ticket.tweets.first.value
    end

    it "updates generated timestamp" do
      @archived_ticket = tickets(:minimum_1)
      @archived_ticket.will_be_saved_by(users(:systemuser))
      @archived_ticket.soft_delete!

      archive_and_delete(@archived_ticket)
      @archived_ticket.reload
      generated_timestamp = @archived_ticket.generated_timestamp
      arch_generated_timestamp = @archived_ticket.ticket_archive_stub.generated_timestamp

      ticket = Timecop.travel(2.seconds.from_now) do
        scrub_ticket(@archived_ticket)
      end
      refute_equal arch_generated_timestamp, ticket.ticket_archive_stub.generated_timestamp
      refute_equal generated_timestamp, ticket.generated_timestamp
    end

    it 'is idempotent for serialized fields' do
      @field = ticket_fields(:field_integer_custom)
      @field2 = ticket_fields(:field_textarea_custom)
      @archived_ticket = tickets(:minimum_1)
      @archived_ticket.ticket_field_entries.create!(ticket_field: @field, value: "1111111111118")
      @archived_ticket.ticket_field_entries.create!(ticket_field: @field2, value: "eyeyeyeyeyeyeyooooooo")

      @archived_ticket.will_be_saved_by(users(:systemuser))
      @archived_ticket.save!
      @archived_ticket.reload
      @archived_ticket.will_be_saved_by(users(:systemuser))
      @archived_ticket.soft_delete!

      archive_and_delete(@archived_ticket)
      @archived_ticket.reload

      ticket = scrub_ticket(@archived_ticket)
      assert_equal "SCRUBBED", ticket.subject
      assert_equal "SCRUBBED", ticket.ticket_archive_stub.subject

      assert_equal Zendesk::Scrub::SCRUB_TEXT, ticket.description
      assert_equal ticket.taggings.count, 0
      # all entries are blank or == 0
      assert ticket.ticket_field_entries.map { |entry| entry.value == "0" || entry.value == "X" }.all? { |v| v }

      ticket = scrub_ticket(@archived_ticket)
      ticket.reload

      assert ticket.ticket_field_entries.map { |entry| entry.value == "0" || entry.value == "X" }.all? { |v| v }
    end

    it "scrubs ticket and associations" do
      ticket = scrub_ticket(@ticket)
      assert_equal Zendesk::Scrub::SCRUB_TEXT, ticket.description
      assert_equal 0, ticket.taggings.count
    end

    describe 'audit' do
      let(:ticket) { Ticket.with_deleted { tickets(:minimum_deleted) } }

      describe "cia actor is system user" do
        it "creates a hidden permanent deletion audit" do
          assert_difference 'CIA::Event.count(:all)', 1 do
            CIA.audit(actor: User.system) do
              Zendesk::Scrub.scrub_ticket_and_associations(ticket)
            end

            audit = CIA::Event.last
            assert audit, "No audit was created"
            assert_equal "permanently_destroy", audit.action
            assert_equal User.system, audit.actor
            refute audit.visible?, 'audit should not be visible'
          end
        end
      end

      describe "cia actor is not system user" do
        it "creates a visible permanent deletion audit" do
          assert_difference 'CIA::Event.count(:all)', 1 do
            user = @ticket.account.admins.first
            CIA.audit(actor: user) do
              Zendesk::Scrub.scrub_ticket_and_associations(ticket)
            end

            audit = CIA::Event.last
            assert audit, "No audit was created"
            assert_equal "permanently_destroy", audit.action
            assert_equal user, audit.actor
            assert audit.visible?, 'audit should be visible'
          end
        end
      end
    end

    describe 'domain events' do
      describe_with_and_without_arturo_enabled(:prevent_deletion_if_churned) do
        describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
          let (:domain_event_publisher) { FakeEscKafkaMessage.new }
          before { @ticket.domain_event_publisher = domain_event_publisher }

          it 'publishes an explicit domain event' do
            # Skipping scrub_ticket helper to maintain domain_event_publisher injection
            Zendesk::Scrub.scrub_ticket_and_associations(@ticket)

            events = domain_event_publisher.events.map do |event|
              ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value]).event
            end
            assert_includes events, :ticket_permanently_deleted
          end
        end
      end
    end

    describe 'publish ticket entity' do
      let(:escape_producer) { FakeEscKafkaMessage.new }
      let(:ticket) { tickets(:minimum_1) }

      before do
        ViewsObservers::EscapeProducer.any_instance.stubs(domain_event_publisher: escape_producer)
      end

      describe_with_arturo_enabled(:publish_ticket_on_scrub) do
        it 'publishes entities to escape' do
          escape_producer.expects(:bulk_import!).with(
            includes(
              has_entries(
                account_id: ticket.account.id,
                topic: 'support.views.tickets',
                key: "#{ticket.account_id}/#{ticket.id}",
                value: nil,
                metadata: anything
              )
            )
          )

          Zendesk::Scrub.scrub_ticket_and_associations(ticket)
        end
      end
    end

    describe 'with custom fields' do
      before do
        @field = ticket_fields(:field_integer_custom)
        @entry = @ticket.ticket_field_entries.create(ticket_field: @field, value: 18)
      end

      it "updates an numeric custom field with a nil value" do
        scrub_ticket(@ticket)
        assert_equal '0', @entry.reload.value
      end
    end

    it "scrubs ticket and associations" do
      @recording_sid = "RE5d4706b2a7ce9c6085119352bc3f96c"
      @recording_url = "http://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/#{@recording_sid}"
      @call = mock('Call')
      @call.stubs(:voice_comment_data).returns(recording_url: @recording_url,
                                               recording_duration: "7",
                                               transcription_text: "foo: hi\nbar: bye\n",
                                               transcription_status: "completed",
                                               call_duration: 126,
                                               answered_by_id: users(:minimum_agent).id,
                                               call_id: 7890,
                                               from: '+14155556666',
                                               to: '+14155557777',
                                               started_at: @now,
                                               outbound: false)
      @call.stubs(:account_id).returns(accounts(:minimum).id)

      @field = ticket_fields(:field_integer_custom)
      @archived_ticket = tickets(:minimum_1)
      @archived_ticket.ticket_field_entries.create(ticket_field: @field, value: 18)
      @archived_ticket.add_voice_comment(@call, author_id: accounts(:minimum).agents.last.id,
                                                body: "Call recording")
      @archived_ticket.reload
      @archived_ticket.will_be_saved_by(users(:systemuser))
      @archived_ticket.soft_delete!
      archive_and_delete(@archived_ticket)
      @archived_ticket.reload
      ticket = scrub_ticket(@archived_ticket)

      assert_equal "SCRUBBED", ticket.subject
      assert_equal "X", ticket.description
      assert_equal 5, ticket.status_id
      assert_equal "SCRUBBED", ticket.ticket_archive_stub.subject
      assert_equal "X", ticket.ticket_archive_stub.description
      assert_equal 5, ticket.ticket_archive_stub.status_id

      assert_equal Zendesk::Scrub::SCRUB_TEXT, ticket.description
      assert_equal ticket.taggings.count, 0
      # all entries are blank or == '0'
      assert ticket.ticket_field_entries.map { |entry| entry.value == "0" || entry.value.blank? }.all? { |v| v }
      assert ticket.voice_comments.map { |entry| entry.value == { "X" => "X" } }.all? { |v| v }
    end

    describe 'monitoring performance' do
      let(:span) { stub(:span) }

      it 'sets APM facet tags' do
        span.expects(:set_tag).with(Datadog::Ext::Analytics::TAG_ENABLED, true)
        span.expects(:set_tag).with('zendesk.account_id', @ticket.account.id)
        span.expects(:set_tag).with('zendesk.account_subdomain', @ticket.account.subdomain)
        span.expects(:set_tag).with('zendesk.subpoena', false)
        span.expects(:set_tag).with('scrubber.event_count', any_parameters)
        span.expects(:set_tag).with('scrubber.archived', false)

        ZendeskAPM.expects(:trace).with('scrubber.execute.Tagging', service: 'scrubber-ticket-execution').yields(span)
        ZendeskAPM.expects(:trace).with('scrubber.execute.Comment', service: 'scrubber-ticket-execution').yields(span).at_least_once
        ZendeskAPM.expects(:trace).with('scrubber.execute.TicketMetricSet', service: 'scrubber-ticket-execution').yields(span)
        ZendeskAPM.expects(:trace).with('scrubber.execute.Audit', service: 'scrubber-ticket-execution').yields(span).at_least_once
        ZendeskAPM.expects(:trace).with('scrubber.execute.External', service: 'scrubber-ticket-execution').yields(span).at_least_once
        ZendeskAPM.expects(:trace).with('scrubber.execute.Create', service: 'scrubber-ticket-execution').yields(span).at_least_once
        ZendeskAPM.expects(:trace).with('scrubber.execute.Change', service: 'scrubber-ticket-execution').yields(span).at_least_once
        ZendeskAPM.expects(:trace).with('scrubber.execute.TicketActivity', service: 'scrubber-ticket-execution').yields(span).at_least_once
        ZendeskAPM.expects(:trace).with('scrubber.execute.Ticket', service: 'scrubber-ticket-execution').yields(span)

        ZendeskAPM.expects(:trace).with('scrubber.execute', service: Zendesk::Scrub::APM_SERVICE_NAME).yields(span)

        Zendesk::Scrub.scrub_ticket_and_associations(@ticket)
      end
    end

    def scrub_ticket(ticket)
      ticket = Ticket.with_deleted { Ticket.find(ticket.id) } # so archived? is set if the ticket is archived
      Zendesk::Scrub.scrub_ticket_and_associations(ticket)
      Ticket.with_deleted { Ticket.find(ticket.id) }
    end

    def expected_associations
      %i[
        taggings
        ticket_schedule
        cia_events
        cia_attribute_changes
        latest_comment
        latest_public_comment
        first_comment
        ticket_metric_set
        audits
        collaborations
        comments
        recent_comments
        public_comments
        conversation_items
        events
        redaction_events
        tweets
        ticket_field_entries
        request_token
        unverified_creation
        target_links
        source_links
        followup_target_links
        followup_source_links
        facebook_target_link
        twitter_target_link
        bookmarks
        activities
        skips
        twitter_actions
        voice_comments
        satisfaction_rating_comments
        satisfaction_rating_scores
        satisfaction_ratings
        sla_ticket_policy
        ticket_archive_disqualification
        chat_started_events
        last_few_comments
      ]
    end
  end

  describe "scope" do
    before do
      @account = accounts(:minimum)
      # this ticket should be return
      @ticket1 = delete_and_reload_ticket(tickets(:minimum_1))

      # this ticket is scrubbed and should not return
      @ticket2 = delete_and_reload_ticket(tickets(:minimum_2))
      @ticket2 = scrub_and_reload(@ticket2)

      # this ticket is deleted more than 10 days ago
      @ticket3 = delete_and_reload_ticket(tickets(:minimum_3))
      @ticket3.update_column(:updated_at, 11.days.ago)
    end

    def find_tickets
      Ticket.with_deleted { @account.tickets.deleted_and_not_scrubbed_since(10.days.ago).to_a }
    end

    describe "deleted_and_not_scrubbed" do
      it "returns a ticket that is deleted but not scrubbed" do
        assert_equal [@ticket1.id], find_tickets.map(&:id)
      end
    end

    def delete_and_reload_ticket(ticket)
      CIA.audit(actor: users(:minimum_admin)) do
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.soft_delete!
      end
      ticket.reload
      ticket
    end

    def scrub_and_reload(ticket)
      CIA.audit(actor: User.system) do
        Zendesk::Scrub.scrub_ticket_and_associations(ticket)
      end
      ticket.reload
      ticket
    end
  end
end
