require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 5

describe 'Zendesk::Cms::ImportJob' do
  fixtures :accounts, :users, :translation_locales

  before do
    @minimum_admin = users(:minimum_admin)
  end

  describe "class methods" do
    let(:csv_data) { %("Title","Default Language","Default text","pt-BR text","Variant Status"\r\n"Greeting","British English","Hello","Olá","Current"\r\n"Music","English","Music","Música","Current") }

    before do
      @account = @minimum_admin.account
      @portuguese = translation_locales(:brazilian_portuguese)
      @japanese = translation_locales(:japanese)
      @spanish = translation_locales(:spanish)
      ::Account.any_instance.stubs(:available_languages).returns([@portuguese, @japanese, @spanish])

      fbattrs = {is_fallback: true, nested: true, account_id: @account.id, value: "hi", translation_locale_id: 1 }
      text = ::Cms::Text.create!(name: "Greeting", fallback_attributes: fbattrs, account: @account)
      text.variants.create!(value: "Oi", translation_locale_id: @spanish.id)
      fbattrs = {is_fallback: true, nested: true, value: "music!", translation_locale_id: @portuguese.id}
      text = ::Cms::Text.create!(name: "Music",  fallback_attributes: fbattrs, account: @account)
      text.variants.create!(value: "la musique", translation_locale_id: @japanese.id)

      @token = @account.upload_tokens.create(source: @minimum_admin)
      @token.add_raw_attachment(csv_data, "cms_import.csv").save
    end

    describe "#perform" do
      before { ActionMailer::Base.perform_deliveries = true }

      it "creates a variant and send an email" do
        assert_difference("Cms::Variant.count(:all)") do
          assert_emails_sent { Zendesk::Cms::ImportJob.perform(@account.id, @minimum_admin.id, @token.value) }
        end
      end

      describe "with bad csv data" do
        let(:csv_data) { "\"Title\",\"Default Language\n" }

        it "sends an email communicating the error" do
          Zendesk::Cms::ImportJob.perform(@account.id, @minimum_admin.id, @token.value)
          ActionMailer::Base.deliveries.last.joined_bodies.must_include "Malformed CSV input"
        end
      end
    end
  end
end
