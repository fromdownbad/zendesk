require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Cms::Exporter do
  fixtures :accounts, :translation_locales

  before do
    @account = accounts(:minimum)

    @english = translation_locales(:english_by_zendesk)
    @french = translation_locales(:french)
    @japanese = translation_locales(:japanese)

    Account.any_instance.stubs(:available_languages).returns([@english, @french, @japanese])

    @locales = {@english.locale => @english.id, @french.locale => @french.id, @japanese.locale => @japanese.id}

    @exporter = Zendesk::Cms::Exporter

    @fbattrs = {is_fallback: true, nested: true, value: "export", translation_locale_id: @english.id}

    @text = Cms::Text.create!(name: "Export me", fallback_attributes: @fbattrs, account: @account)
    @text.variants.create!(value: "french-export", translation_locale_id: @french.id)
    @text.variants.create!(value: "japanese-export", translation_locale_id: @japanese.id)
  end

  after do
    File.unlink("minimum.zip")
  end

  describe "#create_csv_files" do
    it "creates a CSV file for each language that exists on the account in the correct format" do
      files = @exporter.build_export(@account)
      Zip::File.open(files) do |zipfile|
        zipfile.size.must_equal 3
        zipfile.each_with_index do |file, index|
          locale = file.name.gsub(".csv", "")
          locale_id = @locales.fetch(locale)
          variant = @text.variants.find_by_translation_locale_id(locale_id)

          header = "\"Title\",\"Default language\",\"Default text\",\"#{locale} text\",\"Variant status\",\"Placeholder\"\n"
          expected = header << "\"#{@text.title}\",\"#{@text.fallback.name}\",\"#{@text.fallback.value}\",\"#{variant.value}\",\"#{variant.status}\",\"#{@text.fully_qualified_identifier}\"\n"

          file.get_input_stream.read.must_equal expected, "File #{index} did not match"
        end
      end
    end

    describe "for multiple CMS Texts" do
      before do
        @fbattrs2 = {is_fallback: true, nested: true, value: "export 2", translation_locale_id: @english.id }
        @text2 = Cms::Text.create!(name: "Export me 2", fallback_attributes: @fbattrs, account: @account)
        @text2.variants.create!(value: "eggsport 2", translation_locale_id: @french.id)
        @text2.variants.create!(value: "sexport 2", translation_locale_id: @japanese.id)
      end

      it "creates multiple rows listing each text" do
        files = @exporter.build_export(@account)
        Zip::File.open(files) do |zipfile|
          zipfile.size.must_equal 3
          zipfile.each_with_index do |file, index|
            locale = file.name.gsub(".csv", "")
            locale_id = @locales.fetch(locale)
            variant = @text.variants.find_by_translation_locale_id(locale_id)

            header = "\"Title\",\"Default language\",\"Default text\",\"#{locale} text\",\"Variant status\",\"Placeholder\"\n"
            row_one = "\"#{@text.title}\",\"#{@text.fallback.name}\",\"#{@text.fallback.value}\",\"#{variant.value}\",\"#{variant.status}\",\"#{@text.fully_qualified_identifier}\"\n"
            variant = @text2.variants.find_by_translation_locale_id(locale_id)
            row_two = "\"#{@text2.title}\",\"#{@text2.fallback.name}\",\"#{@text2.fallback.value}\",\"#{variant.value}\",\"#{variant.status}\",\"#{@text2.fully_qualified_identifier}\"\n"
            expected = header << row_one << row_two

            file.get_input_stream.read.must_equal expected, "File #{index} did not match"
          end
        end
      end
    end

    describe "with CSV macros" do
      before do
        @fbattrs2 = {is_fallback: true, nested: true, value: "export 2", translation_locale_id: @english.id }
        @text3 = Cms::Text.create!(name: "=AND(2>1)", fallback_attributes: @fbattrs, account: @account)
        @text3.variants.create!(value: "eggsport", translation_locale_id: @french.id)
        @text3.variants.create!(value: "sexport 2", translation_locale_id: @japanese.id)
      end

      it "escapes with an apostrophe" do
        files = @exporter.build_export(@account)
        Zip::File.open(files) do |zipfile|
          zipfile.size.must_equal 3
          zipfile.each_with_index do |file, index|
            locale = file.name.gsub(".csv", "")
            locale_id = @locales.fetch(locale)
            variant = @text.variants.find_by_translation_locale_id(locale_id)

            header = "\"Title\",\"Default language\",\"Default text\",\"#{locale} text\",\"Variant status\",\"Placeholder\"\n"
            row_one = "\"#{@text.title}\",\"#{@text.fallback.name}\",\"#{@text.fallback.value}\",\"#{variant.value}\",\"#{variant.status}\",\"#{@text.fully_qualified_identifier}\"\n"
            variant = @text3.variants.find_by_translation_locale_id(locale_id)
            row_two = "\"'#{@text3.title}\",\"#{@text3.fallback.name}\",\"#{@text3.fallback.value}\",\"#{variant.value}\",\"#{variant.status}\",\"#{@text3.fully_qualified_identifier}\"\n"
            expected = header << row_two << row_one

            file.get_input_stream.read.must_equal expected, "File #{index} did not match"
          end
        end
      end
    end

    describe "for CMS Texts which dont have all variants for an account" do
      before do
        @fbattrs2 = {is_fallback: true, nested: true, value: "export 2", translation_locale_id: @english.id }
        @text2 = Cms::Text.create!(name: "Export me 2", fallback_attributes: @fbattrs, account: @account)
        @text2.variants.create!(value: "eggsport 2", translation_locale_id: @french.id)
      end

      it "creates blank rows for variants that don't exist yet" do
        files = @exporter.build_export(@account)
        Zip::File.open(files) do |zipfile|
          variant = @text.variants.find_by_translation_locale_id(@japanese.id)

          header = "\"Title\",\"Default language\",\"Default text\",\"#{@japanese.locale} text\",\"Variant status\",\"Placeholder\"\n"
          row_one = "\"#{@text.title}\",\"#{@text.fallback.name}\",\"#{@text.fallback.value}\",\"#{variant.value}\",\"#{variant.status}\",\"#{@text.fully_qualified_identifier}\"\n"
          row_two = "\"#{@text2.title}\",\"#{@text2.fallback.name}\",\"#{@text2.fallback.value}\",\"\",\"\",\"#{@text2.fully_qualified_identifier}\"\n"
          expected = header << row_one << row_two

          zipfile.read("#{@japanese.locale}.csv").must_equal expected
        end
      end
    end
  end

  describe "#csv_file_data" do
    it "retrieves the data from the file and delete it" do
      files = @exporter.build_export(@account)
      Zip::File.open(files) do |zipfile|
        zipfile.each_with_index do |file, index|
          locale = file.name.gsub(".csv", "")
          locale_id = @locales.fetch(locale)
          variant = @text.variants.find_by_translation_locale_id(locale_id)
          header = "\"Title\",\"Default language\",\"Default text\",\"#{locale} text\",\"Variant status\",\"Placeholder\"\n"
          expected = header << "\"#{@text.title}\",\"#{@text.fallback.name}\",\"#{@text.fallback.value}\",\"#{variant.value}\",\"#{variant.status}\",\"#{@text.fully_qualified_identifier}\"\n"
          file.get_input_stream.read.must_equal expected, "File #{index} did not match"
        end
      end
    end
  end
end
