require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 13

describe 'CmsImporter' do
  fixtures :accounts, :translation_locales

  before do
    @account = accounts(:minimum)
    @account.stubs(:available_languages).returns([stub(locale: "en-US", id: 1), stub(locale: "ja", id: 4), stub(locale: "pt-BR", id: 2), stub(locale: "ja-x-test", id: 5)])

    fbattrs = {is_fallback: true, nested: true, value: "hi", translation_locale_id: 1 }
    text = Cms::Text.create!(name: "Greeting", fallback_attributes: fbattrs, account: @account)
    text.variants.create!(value: "Oi", translation_locale_id: 2)
    fbattrs = {is_fallback: true, nested: true, value: "music!", translation_locale_id: 1 }
    text = Cms::Text.create!(name: "Music", fallback_attributes: fbattrs, account: @account)
    text.variants.create!(value: "la musique", translation_locale_id: 4)

    csv = %("Title","Default Language","Default text","pt-BR text","Variant Status"\r\n"Greeting","British English","Hello","Olá  ","Current"\r\n"Music","English","Music"," Música ","Current")
    csv_new_format = %("Title","Default Language","Default text","pt-BR text","Variant Status","Placeholder"\r\n"Greeting","British English","Hello","Olá  ","Current","dc.greeting"\r\n"Music","English","Music"," Música ","Current","dc.music")
    @import = Zendesk::Cms::Importer.new(@account, csv)
    @import_new_format = Zendesk::Cms::Importer.new(@account, csv_new_format)
  end

  describe '#build_data' do
    it 'parses csv and not raise exception' do
      @import.send(:build_data)
    end
  end

  describe '#language' do
    before { I18n.translation_locale = @import.locale = translation_locales(:spanish) }

    describe 'with valid user_locale' do
      before { @user_locale = translation_locales(:brazilian_portuguese) }

      it 'returns user-localized language name' do
        assert_equal 'espanhol', @import.language(@user_locale)
      end
    end

    describe 'without user_locale' do
      before { @user_locale = nil }

      it 'returns account-localized language name' do
        assert_equal 'español', @import.language(@user_locale)
      end
    end
  end

  describe '#process_columns_names' do
    it 'has valid column names and locale' do
      @import.send(:build_data)
      @import.send(:process_columns_names)
      assert_equal 'pt-BR', @import.locale.locale
      assert_equal 3, @import.language_column_pos
    end

    describe 'with valid locale variations' do
      it 'accepts locale names with different case' do
        csv = %("Title","Default Language","Default text","pt-br text","Variant Status"\r\n"Greeting","British English","Hello","Olá  ","Current"\r\n"Music","English","Music"," Música ","Current")
        @import = Zendesk::Cms::Importer.new(@account, csv)
        @import.send(:build_data)
        @import.send(:process_columns_names)
        assert_equal 'pt-BR', @import.locale.locale
        assert_equal 3, @import.language_column_pos
      end

      describe 'with valid locale standard name' do
        before do
          csv = %("Title","Default Language","Default text","ja-x-test text","Variant Status"\r\n"Greeting","British English","Hello","Olá  ","Current"\r\n"Music","English","Music"," Música ","Current")
          @import = Zendesk::Cms::Importer.new(@account, csv)
          @import.send(:build_data)
        end

        it 'do not raises an error' do
          @import.send(:process_columns_names)
          assert_equal 3, @import.language_column_pos
          assert_equal 'ja-x-test', @import.locale.locale
        end
      end

      it 'does not accept locale names with underscores instead of hyphens' do
        csv = %("Title","Default Language","Default text","pt_BR text","Variant Status"\r\n"Greeting","British English","Hello","Hi","Current"\r\n"Music","English","Music","Jams","Current")
        @import = Zendesk::Cms::Importer.new(@account, csv)
        @import.send(:build_data)
        assert_raises Zendesk::Cms::Importer::ImportError do
          @import.send(:process_columns_names)
        end
      end

      it 'accepts locale names with variations in case and hyphenation' do
        csv = %("Title","Default Language","Default text","pt-br text","Variant Status"\r\n"Greeting","British English","Hello","Olá  ","Current"\r\n"Music","English","Music"," Música ","Current")
        @import = Zendesk::Cms::Importer.new(@account, csv)
        @import.send(:build_data)
        @import.send(:process_columns_names)
        assert_equal 'pt-BR', @import.locale.locale
        assert_equal 3, @import.language_column_pos
      end
    end
  end

  describe '#process_rows' do
    describe 'with valid data' do
      before do
        @import.send(:build_data)
        @import.send(:process_columns_names)
      end

      it 'instantiates Cms::Variant for considered rows and does not strip whitespace' do
        @import.send(:process_rows)
        assert_empty(@import.errors)
        assert_equal 2, @import.records.size
        assert_equal 'Olá  ', @import.records[0].value
        assert_equal ' Música ', @import.records[1].value
      end
    end

    describe 'with invalid data' do
      before do
        # Should ignore lines with no value
        csv = %("Title","Default Language","Default text","pt-BR text","Variant Status"\r\n"No Title 2",,,,\r\n"No Title","British English","Hello","Olá","Current"\r\n"Music","English","Music","Música","Current")
        @invalid_import = Zendesk::Cms::Importer.new(@account, csv)
        @invalid_import.send(:build_data)
        @invalid_import.send(:process_columns_names)
      end

      it 'adds an error' do
        I18n.expects(:t).with('txt.admin.lib.zendesk.cms.importer.title_not_found', title: 'No Title', locale: @account.translation_locale).returns("Title 'No Title' not found")
        @invalid_import.send(:process_rows)
        assert_equal "Title 'No Title' not found", @invalid_import.errors.first
      end
    end

    describe 'with invalid locale' do
      before do
        csv = %("Title","Default Language","Default text","es-abcd12345 text","Variant Status"\r\n"No Title 2",,,,\r\n"No Title","British English","Hello","Olá","Current"\r\n"Music","English","Music","Música","Current")
        @invalid_import = Zendesk::Cms::Importer.new(@account, csv)
        @invalid_import.send(:build_data)
      end

      it 'raises an error' do
        I18n.expects(:t).with('txt.admin.lib.zendesk.cms.importer.expected_column_missing_language_text', locale: @account.translation_locale).returns("Expected column missing: Language text")
        assert_raises(Zendesk::Cms::Importer::ImportError) do
          @invalid_import.send(:process_columns_names)
        end
      end
    end

    describe 'with valid but inactive language' do
      before do
        csv = %("Title","Default Language","Default text","es-419 text","Variant Status"\r\n"No Title 2",,,,\r\n"No Title","British English","Hello","Olá","Current"\r\n"Music","English","Music","Música","Current")
        @invalid_import = Zendesk::Cms::Importer.new(@account, csv)
        @invalid_import.send(:build_data)
      end

      it 'raises an error' do
        I18n.expects(:t).with('txt.admin.lib.zendesk.cms.importer.language_not_active_for_account', locale: @account.translation_locale).returns("Expected column missing: Language text")
        assert_raises(Zendesk::Cms::Importer::ImportError) do
          @invalid_import.send(:process_columns_names)
        end
      end
    end

    describe 'with non-utf8 data' do
      it "coerces data to utf8" do
        csv = %("Title","Default Language","Default text","pt-BR text","Variant Status"\r\n"No Title 2",,,,\r\n"No Title","British English","Hello","Ol\xAD","Current"\r\n"Music","English","Music","Música","Current")
        @invalid_import = Zendesk::Cms::Importer.new(@account, csv)
        assert_equal "Ol�", @invalid_import.send(:build_data)[2][3]
      end
    end

    describe 'with an empty column name' do
      before do
        csv = %("Title","Default Language","Default text","en-US","Variant Status",\r\n"No Title 2",,,,,\r\n"No Title","British English","Hello","Olá","Current",\r\n"Music","English","Music","Música","Current",)
        @invalid_import = Zendesk::Cms::Importer.new(@account, csv)
        @invalid_import.send(:build_data)
      end

      it 'raises an error' do
        I18n.expects(:t).with('txt.admin.lib.zendesk.cms.importer.empty_column_name', locale: @account.translation_locale).returns("Column names cannot be blank")
        assert_raises(Zendesk::Cms::Importer::ImportError) do
          @invalid_import.send(:process_columns_names)
        end
      end
    end
  end

  describe '#save_records' do
    before do
      @import.send(:build_data)
      @import.send(:process_columns_names)
      @import.send(:process_rows)
    end

    it 'creates records' do
      assert_difference("Cms::Variant.count(:all)") do
        @import.send(:save_records)
      end
    end

    it 'updates records' do
      record = @import.records[0]
      variant = Cms::Variant.find_by_id(record.id)
      assert_not_equal variant.value, record.value
      @import.send(:save_records)
      variant = Cms::Variant.find_by_id(record.id)
      assert_equal variant.value, record.value
    end

    it 'updates the updated_at for both the variant and the text' do
      Timecop.freeze
      DC::Synchronizer.expects(:synchronize_snippet).times(@import.records.size)

      @import.send(:save_records)
      @import.records.each do |record|
        assert_equal Time.now, record.updated_at
        assert_equal Time.now, record.text.updated_at
      end
    end
  end
end
