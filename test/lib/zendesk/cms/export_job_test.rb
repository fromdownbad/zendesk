require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 3

describe Zendesk::Cms::ExportJob do
  fixtures :accounts, :users, :translation_locales

  describe "#perform" do
    let(:user) { users(:minimum_admin) }

    before do
      account = user.account
      fbattrs = {is_fallback: true, nested: true, value: "export", translation_locale_id: 1 }
      text = ::Cms::Text.create!(name: "Export me", fallback_attributes: fbattrs, account: account)
      text.variants.create!(value: "eggsport", translation_locale_id: 2)
      ActionMailer::Base.perform_deliveries = true
    end

    it "creates an expirable_attachment and send an email" do
      stub_request(:put, %r{\.amazonaws\.com/data/expirable_attachments/.*/minimum-dc.zip})
      assert_difference("ExpirableAttachment.count") do
        assert_emails_sent { Zendesk::Cms::ExportJob.perform(user.account_id, user.id) }
      end
    end
  end
end
