require_relative "../../support/test_helper"
require 'zendesk_mail'

SingleCov.covered! uncovered: 5

Zendesk::Mail::InboundMessage.class_eval do
  attr_accessor :account
end

describe Zendesk::ReplyParserInstrumentation do
  describe "reply parser instrumentation" do
    let(:statsd) { stub_for_statsd }
    let(:mail_fixture_path) { String }
    let(:mail) { FixtureHelper::Mail.read(mail_fixture_path) }
    let(:locale_id) { 1 }
    let(:client) { Zendesk::Mail::Clients::Unknown }
    let(:delimiter) { "## Please do not write below this line ##" }
    let(:metric) { String }
    let(:options) { Hash }
    let(:subject) do
      Zendesk::ReplyParserInstrumentation.new(
        mail, client, locale_id, delimiter, compare_html: true
      )
    end

    before do
      Zendesk::StatsD::Client.stubs(:new).returns(statsd)
      mail.account = Account.new
    end

    describe "#run_no_delimiter_plain" do
      describe "when content matches" do
        let(:mail_fixture_path) { "replies/gmail/gmail_reply_header.json" }
        let(:client) { Zendesk::Mail::Clients::Gmail }

        it "increments statsd match metric with correct tags" do
          expected_tags = ["client:Gmail", "account_locale:1", "format:plain"]
          statsd.expects(:increment).with("no_delimiter.comparison.match", tags: expected_tags).once
          statsd.expects(:increment).with("no_delimiter.invalid_comparison", tags: expected_tags).once
          subject.run_no_delimiter_plain
        end
      end

      describe "when content does not match" do
        let(:mail_fixture_path) { "fail_mail.json" }

        it "increments statsd mismatch metric with correct tags" do
          expected_tags = ["client:Unknown", "account_locale:1", "format:plain"]
          statsd.expects(:increment).with("no_delimiter.comparison.mismatch", tags: expected_tags).once
          statsd.expects(:increment).with("no_delimiter.invalid_comparison", tags: expected_tags).once
          subject.run_no_delimiter_plain
        end
      end
    end

    describe "#run_no_delimiter_html" do
      let(:expected_tags) { ["client:Unknown", "account_locale:1", "format:html"] }

      describe "when content matches" do
        let(:mail_fixture_path) { "replies/unknown.json" }

        it "increments statsd match metric with correct tags" do
          statsd.expects(:increment).with("no_delimiter.comparison.match", tags: expected_tags).once
          statsd.expects(:increment).with("no_delimiter.invalid_comparison", tags: expected_tags).once
          subject.run_no_delimiter_html
        end
      end

      describe "when content does not match" do
        let(:mail_fixture_path) { "fail_mail.json" }

        it "increments statsd mismatch metric with correct tags" do
          statsd.expects(:increment).with("no_delimiter.comparison.mismatch", tags: expected_tags).once
          statsd.expects(:increment).with("no_delimiter.invalid_comparison", tags: expected_tags).once
          subject.run_no_delimiter_html
        end
      end
    end

    describe "#run_reply_parser_plain" do
      describe "match instrumentation" do
        describe "when content matches" do
          let(:mail_fixture_path) { "replies/gmail/gmail_reply_header.json" }
          let(:client) { Zendesk::Mail::Clients::Gmail }

          it "increments statsd match metric with correct tags" do
            expected_tags = ["client:Gmail", "account_locale:1", "format:plain"]
            statsd.expects(:increment).with("reply_parser.comparison.match", tags: expected_tags).once
            statsd.expects(:increment).with("reply_parser.invalid_comparison", tags: expected_tags).once
            subject.run_reply_parser_plain
          end
        end

        describe "when content does not match" do
          let(:mail_fixture_path) { "fail_mail.json" }

          it "increments statsd mismatch metric with correct tags" do
            expected_tags = ["client:Unknown", "account_locale:1", "format:plain"]
            statsd.expects(:increment).with("reply_parser.comparison.mismatch", tags: expected_tags).once
            statsd.expects(:increment).with("reply_parser.invalid_comparison", tags: expected_tags).once
            subject.run_reply_parser_plain
          end
        end
      end

      describe "timing instrumentation" do
        let(:mail_fixture_path) { "replies/gmail/gmail_reply_header.json" }
        let(:client) { Zendesk::Mail::Clients::Gmail }
        before { Benchmark.stubs(:realtime).returns(0.001) }

        it "benchmarks real time with correct tags" do
          expected_tags = ["client:Gmail", "account_locale:1", "format:plain"]
          statsd.expects(:histogram).with("reply_parser.timing", 1, tags: expected_tags).once
          statsd.expects(:increment).with("reply_parser.invalid_comparison", tags: expected_tags).never # content is nil
          subject.run_reply_parser_plain
        end
      end
    end

    describe "#run_reply_parser_html" do
      describe "match instrumentation" do
        describe "when content matches" do
          let(:mail_fixture_path) { "replies/gmail/gmail_reply_header.json" }
          let(:client) { Zendesk::Mail::Clients::Gmail }

          it "increments statsd match metric with correct tags" do
            expected_tags = ["client:Gmail", "account_locale:1", "format:html"]
            statsd.expects(:increment).with("reply_parser.comparison.match", tags: expected_tags).once
            statsd.expects(:increment).with("reply_parser.invalid_comparison", tags: expected_tags).once
            subject.run_reply_parser_html
          end
        end

        describe "when content does not match" do
          let(:mail_fixture_path) { "fail_mail.json" }

          it "increments statsd mismatch metric with correct tags" do
            expected_tags = ["client:Unknown", "account_locale:1", "format:html"]
            statsd.expects(:increment).with("reply_parser.comparison.mismatch", tags: expected_tags).once
            statsd.expects(:increment).with("reply_parser.invalid_comparison", tags: expected_tags).once
            subject.run_reply_parser_html
          end
        end
      end

      describe "timing instrumentation" do
        let(:mail_fixture_path) { "replies/gmail/gmail_reply_header.json" }
        let(:client) { Zendesk::Mail::Clients::Gmail }
        before { Benchmark.stubs(:realtime).returns(0.001) }

        it "benchmarks real time with correct tags" do
          expected_tags = ["client:Gmail", "account_locale:1", "format:html"]
          statsd.expects(:histogram).with("reply_parser.timing", 1, tags: expected_tags).once
          statsd.expects(:increment).with("reply_parser.invalid_comparison", tags: expected_tags).never # content is nil
          subject.run_reply_parser_html
        end
      end
    end

    describe "zendesk to zendesk parsing" do
      let(:mail_fixture_path) { "replies/gmail/gmail_reply_header.json" }

      before { mail.stubs(from_zendesk?: true) }

      describe "when one delimiter found" do
        it "counts as a match" do
          expected_tags = %w[client:Unknown account_locale:1 format:plain]
          statsd.expects(:increment).with("reply_parser.comparison.match", tags: expected_tags).once
          statsd.expects(:increment).with("reply_parser.invalid_comparison", tags: expected_tags).once
          subject.run_reply_parser_plain
        end
      end

      describe "when more than one delimiter found" do
        before { Zendesk::MtcMpqMigration::Content::Delimiter.any_instance.stubs(occurrences: 2) }

        it "counts as a mismatch" do
          expected_tags = %w[client:Unknown account_locale:1 format:plain]
          statsd.expects(:increment).with("reply_parser.comparison.mismatch", tags: expected_tags).once
          statsd.expects(:increment).with("reply_parser.invalid_comparison", tags: expected_tags).once
          subject.run_reply_parser_plain
        end
      end
    end

    describe "when comparing reply parser text content matches with no delimiter" do
      describe "with html content" do
        let(:client) { Zendesk::Mail::Clients::Gmail }
        let(:expected_tags) { ["client:Gmail", "account_locale:1", "format:html"] }

        describe "when text content matches" do
          let(:mail_fixture_path) { "replies/no_delimiter_parsing.json" }

          it "increments statsd match metric with correct tags" do
            statsd.expects(:increment).with("no_delimiter.text_comparison.match", tags: expected_tags).once
            statsd.expects(:increment).with("no_delimiter.invalid_comparison", tags: expected_tags).once
            subject.run_no_delimiter_html
            subject.run_reply_parser_html
          end
        end

        describe "when text content does not match" do
          let(:mail_fixture_path) { "replies/gmail/gmail_reply_header.json" }

          it "increments statsd match metric with correct tags" do
            statsd.expects(:increment).with("no_delimiter.text_comparison.mismatch", tags: expected_tags).once
            statsd.expects(:increment).with("no_delimiter.invalid_comparison", tags: expected_tags).once
            subject.run_no_delimiter_html
            subject.run_reply_parser_html
          end
        end

        describe "for an email forward" do
          let(:mail_fixture_path) { "replies/gmail/forward.json" }

          it "increments statsd match metric with correct tags" do
            statsd.expects(:increment).with("no_delimiter.text_comparison.match", tags: expected_tags).once
            statsd.expects(:increment).with("no_delimiter.invalid_comparison", tags: expected_tags).once
            subject.run_no_delimiter_html
            subject.run_reply_parser_html
          end
        end
      end

      describe "with plain content" do
        let(:client) { Zendesk::Mail::Clients::Gmail }
        let (:expected_tags) { ["client:Gmail", "account_locale:1", "format:plain"] }

        describe "when text content matches" do
          let(:mail_fixture_path) { "replies/gmail/gmail_reply_header.json" }

          it "increments statsd match metric with correct tags" do
            statsd.expects(:increment).with("no_delimiter.text_comparison.match", tags: expected_tags).once
            statsd.expects(:increment).with("no_delimiter.invalid_comparison", tags: expected_tags).once
            subject.run_no_delimiter_plain
            subject.run_reply_parser_plain
          end
        end
      end
    end
  end
end
