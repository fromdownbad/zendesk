require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 7

describe 'Zendesk::Forums::SpamManagement' do
  fixtures :accounts, :forums, :entries, :posts, :users

  describe "#run" do
    let(:suspended_entry_ids) { [] }
    let(:suspended_post_ids) { [] }
    let(:operation) { nil }

    subject do
      Zendesk::Forums::SpamManagement.new(accounts(:minimum),
        suspended_entry_ids: suspended_entry_ids,
        suspended_post_ids: suspended_post_ids,
        operation: operation)
    end

    describe "with operation: :delete based on a entry" do
      let(:operation) { :delete }
      let(:entry) { entries(:sticky) }
      let(:suspended_entry_ids) { [entry.id] }

      before do
        suspend(entry)
        entries(:technology) # Another entry by minimum_end_user

        Forum.reset_counters(forums(:solutions).id, :entries)
        @initial_entries_count = forums(:solutions).reload.entries_count

        @result = subject.run
      end

      it "is successful" do
        assert @result
      end

      it "deletes that entry and remove the suspended flag" do
        assert Entry.with_deleted { entries(:sticky) }.reload.deleted?
        refute Entry.with_deleted { entries(:sticky) }.reload.suspended?
      end

      it "does not delete other entries from same user" do
        refute entries(:technology).reload.deleted?
      end

      it "updates the forum entries_count properly" do
        # Should be the same count as initial since we are deleting an entry
        # that was already deleted as suspended.
        assert_equal @initial_entries_count, forums(:solutions).reload.entries_count
      end
    end

    describe "with operation: :delete based on a post" do
      let(:operation) { :delete }
      let(:post) { posts(:sticky_reply) }
      let(:suspended_post_ids) { [post.id] }

      before do
        suspend(post)

        Entry.reset_counters(entries(:sticky).id, :posts)
        @initial_posts_count = entries(:sticky).reload.posts_count

        @result = subject.run
      end

      it "is successful" do
        assert @result
      end

      it "deletes that post and remove the suspended flag" do
        assert Post.with_deleted { posts(:sticky_reply) }.reload.deleted?
        refute Post.with_deleted { posts(:sticky_reply) }.reload.suspended?
      end

      it "updates the entries posts_count properly" do
        # Should be the same count as initial since we are deleting a post
        # that was already deleted as suspended.
        assert_equal @initial_posts_count, entries(:sticky).reload.posts_count
      end
    end

    describe "with operation: :not_spam based on a entry" do
      let(:operation) { :not_spam }
      let(:suspended_entry_ids) { [entries(:sticky).id] }

      before do
        suspend(entries(:sticky))

        Forum.reset_counters(forums(:solutions).id, :entries)
        @initial_entries_count = forums(:solutions).reload.entries_count

        @result = subject.run
      end

      it "is successful" do
        assert @result
      end

      it "undeletes it" do
        refute entries(:sticky).reload.deleted?
      end

      it "removes the suspended flag" do
        refute entries(:sticky).reload.suspended?
      end

      it "updates the forum entries_count properly" do
        assert_equal @initial_entries_count + 1, forums(:solutions).reload.entries_count
      end
    end

    describe "with operation: :not_spam based on a post" do
      let(:operation) { :not_spam }
      let(:post) { posts(:sticky_reply) }
      let(:suspended_post_ids) { [post.id] }

      before do
        suspend(post)

        Entry.reset_counters(entries(:sticky).id, :posts)
        @initial_posts_count = entries(:sticky).reload.posts_count

        @result = subject.run
      end

      it "is successful" do
        assert @result
      end

      it "undeletes it" do
        refute posts(:sticky_reply).reload.deleted?
      end

      it "removes the suspended flag" do
        refute posts(:sticky_reply).reload.suspended?
      end

      it "updates the entries posts_count properly" do
        # Should be the same count as initial since we are deleting a post
        # that was already deleted as suspended.
        assert_equal @initial_posts_count + 1, entries(:sticky).reload.posts_count
      end
    end

    describe "with operation: :suspend_authors based on a entry" do
      let(:operation) { :suspend_authors }
      let(:suspended_entry_ids) { [entries(:technology).id] }
      let(:user) { users(:minimum_end_user) }
      let(:post) { posts(:sticky_reply) }

      before do
        @suspended_entry = entries(:technology)
        Watching.create! source: @suspended_entry, user: users(:minimum_end_user), account: users(:minimum_end_user).account
        suspend(@suspended_entry)
        entries(:sticky) # Another entry by minimum_end_user
        suspend(post)

        @result = subject.run
      end

      it "is successful" do
        assert @result
      end

      it "suspends the user" do
        assert user.suspended?
      end

      it "deletes that entry" do
        assert Entry.with_deleted { entries(:technology) }.reload.deleted?
      end

      it "deletes all other entries by that user" do
        assert Entry.with_deleted { entries(:sticky) }.reload.deleted?
      end

      it "deletes all posts" do
        post.reload

        assert post.deleted?
        refute post.suspended?
      end

      it "updates the forum entries_count properly" do
        assert_equal 1, forums(:solutions).entries_count
      end

      it "updates the entries posts_count properly" do
        assert_equal 0, Entry.with_deleted { entries(:ponies) }.reload.posts_count
      end
    end

    describe "with operation: :suspend_authors based on a post" do
      let(:operation) { :suspend_authors }
      let(:post) { posts(:sticky_reply) }
      let(:suspended_post_ids) { [post.id] }
      let(:user) { post.user }

      before do
        suspend(post)

        @other_published_post = Post.create!(body: 'foo', user: post.user, entry: post.entry)

        @other_suspended_post = Post.create!(body: 'bad', user: post.user, entry: post.entry)
        suspend(@other_suspended_post)

        Entry.reset_counters(entries(:sticky).id, :posts)
        @initial_posts_count = entries(:sticky).reload.posts_count

        @result = subject.run
      end

      it "is successful" do
        assert @result
      end

      it "suspends the user" do
        assert user.suspended?
      end

      it "deletes that post" do
        assert Post.with_deleted { post }.reload.deleted?
        refute Post.with_deleted { post }.reload.suspended?
      end

      it "deletes other suspended post" do
        assert @other_suspended_post.reload.deleted?
        refute @other_suspended_post.reload.suspended?
      end

      it "deletes other published post of same user" do
        assert @other_published_post.reload.deleted?
      end

      it "updates the entries posts_count properly" do
        assert_equal @initial_posts_count - 1, entries(:sticky).reload.posts_count
      end
    end

    describe "testing with a string instead of a symbol as operation" do
      let(:operation) { "delete" }

      before do
        @result = subject.run
      end

      it "is successful" do
        assert @result
      end
    end
  end

  private

  def suspend(obj)
    obj.send(:suspend)
    obj.save!
  end
end
