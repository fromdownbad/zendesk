require_relative "../../support/test_helper"
require 'zendesk/inbound_mail'

SingleCov.covered! uncovered: 1

describe Zendesk::Mail::InboundMessage do
  extend ArturoTestHelper

  include MailTestHelper

  fixtures :accounts

  describe 'generate_message_id' do
    let(:inbound_message) { Zendesk::Mail::InboundMessage }
    let(:zendesk_mid_pattern) { /\A<?(\h+_\h+){1}_sprut@zendesk-test.com>?\z/ }

    subject { inbound_message.generate_message_id }

    it 'will generate a standard zendesk message id' do
      assert subject =~ zendesk_mid_pattern
    end
  end

  describe "#sender" do
    def assert_correct_sender(from_address, from_name, reply_to_address, reply_to_name, expected_address, expected_name)
      mail = Zendesk::Mail::InboundMessage.new
      mail.from = Zendesk::Mail::Address.new(address: from_address, name: from_name) if from_address
      mail.reply_to = Zendesk::Mail::Address.new(address: reply_to_address, name: reply_to_name) if reply_to_address

      assert_equal expected_address, mail.sender.address
      assert_equal expected_name, mail.sender.name
    end

    it "pulls a sender from mail with a from header and no reply-to header" do
      assert_correct_sender("from@mail.com", nil, nil, nil, "from@mail.com", "From")
      assert_correct_sender("from@mail.com", "Juan Doe", nil, nil, "from@mail.com", "Juan Doe")
    end

    it "pulls a sender from mail with a reply-to header and no from header" do
      assert_correct_sender(nil, nil, "reply_to@mail.com", nil, "reply_to@mail.com", "Reply To")
      assert_correct_sender(nil, nil, "reply_to@mail.com", "Juan Doe", "reply_to@mail.com", "Juan Doe")
    end

    it "pulls a sender from mail with both from and reply-to headers but different addresses" do
      assert_correct_sender("from@mail.com", nil, "reply_to@mail.com", nil, "reply_to@mail.com", "Reply To")
      assert_correct_sender("from@mail.com", nil, "reply_to@mail.com", "Juan Doe", "reply_to@mail.com", "Juan Doe")
      assert_correct_sender("from@mail.com", "Juan Doe", "reply_to@mail.com", nil, "reply_to@mail.com", "Reply To")
      assert_correct_sender("from@mail.com", "Juan Doe", "reply_to@mail.com", "Juan Doe", "reply_to@mail.com", "Juan Doe")
      assert_correct_sender("from@mail.com", "Juan Doe", "reply_to@mail.com", "Yonatan Doe", "reply_to@mail.com", "Yonatan Doe")
    end

    it "pulls a sender from mail with both from and reply-to headers and identical addresses" do
      assert_correct_sender("same@mail.com", nil, "same@mail.com", nil, "same@mail.com", "Same")
      assert_correct_sender("same@mail.com", nil, "same@mail.com", "Juan Doe", "same@mail.com", "Juan Doe")
      assert_correct_sender("same@mail.com", "Juan Doe", "same@mail.com", nil, "same@mail.com", "Juan Doe")
      assert_correct_sender("same@mail.com", "Juan Doe", "same@mail.com", "Juan Doe", "same@mail.com", "Juan Doe")
      assert_correct_sender("same@mail.com", "Juan Doe", "same@mail.com", "Yonatan Doe", "same@mail.com", "Yonatan Doe")
    end
  end

  describe "ticket identification" do
    let(:mail) { Zendesk::Mail::InboundMessage.new }

    describe "when identifying tickets via encoded ID" do
      let(:encoded_id_1) { "ABC123-DE45" }
      let(:encoded_id_2) { "ZYX987-VU65" }
      let(:encoded_id_3) { "AZB192-YC83" }

      describe "when identifying tickets via the email's Subject header" do
        describe "when the email has no valid encoded IDs in its Subject header" do
          before do
            mail.subject = "Hello, my account is #12345. Help!"
          end

          it "does not find any encoded IDs in the Subject header" do
            assert_empty mail.body_and_subject_encoded_ids
          end
        end

        describe "when the email has a single encoded ID in its Subject header" do
          before do
            mail.subject = "Hello, my account is #12345. Help! [#{encoded_id_1}]"
          end

          it "finds the encoded ID from the Subject header" do
            encoded_ids = mail.body_and_subject_encoded_ids

            assert_equal 1, encoded_ids.size
            assert_equal :subject, encoded_ids.first.location
            assert_equal encoded_id_1, encoded_ids.first.value
          end
        end

        describe "when the email has multiple encoded IDs in its Subject header" do
          before do
            mail.subject = "[#{encoded_id_1}] Hello, my account is #12345. Help! [#{encoded_id_2}]"
          end

          it "finds all of the encoded IDs in the Subject header" do
            first_id = mail.body_and_subject_encoded_ids.detect { |encoded_id| encoded_id.value == encoded_id_1 }
            assert first_id
            assert_equal :subject, first_id.location

            last_id = mail.body_and_subject_encoded_ids.detect { |encoded_id| encoded_id.value == encoded_id_2 }
            assert last_id
            assert_equal :subject, last_id.location

            assert_equal 2, mail.body_and_subject_encoded_ids.size
          end
        end
      end

      # Uses String.casecmp? because Zendesk::Mail::Address downcases all :address attribute strings
      describe "when identifying tickets via the email's recipient headers (To, CC, BCC)" do
        let(:default_host) { accounts(:minimum).default_hosts.first }

        before { mail.account = accounts(:minimum) }

        describe "when there are no encoded IDs in any of the recipient headers" do
          before { mail.to = address("support@example.com") }

          it "does not find an encoded ID in the recipient headers" do
            assert_nil mail.recipients_encoded_id(mail.account)
          end
        end

        describe "when there is a valid encoded ID in one of the recipient headers, but it is not prefixed with '+id'" do
          before { mail.to = address("support#{encoded_id_1}@example.com") }

          it "does not find an encoded ID in the recipient headers" do
            assert_nil mail.recipients_encoded_id(mail.account)
          end
        end

        describe "when there is a +id-formatted encoded ID in one of the recipient headers" do
          before { mail.to = address("support+id#{encoded_id_1}@#{default_host}") }

          it "finds the encoded ID in the recipient header" do
            encoded_id = mail.recipients_encoded_id(mail.account)
            assert encoded_id_1.casecmp?(encoded_id)
          end
        end

        describe "when the account has host mapping configured" do
          let(:email_non_hostmapped_recipients_encoded_id_fix) { true }

          before do
            mail.account = accounts(:minimum)
            mail.account.update_attribute(:host_mapping, "exxxample.com")
            mail.account.stubs(:has_email_non_hostmapped_recipients_encoded_id_fix?).returns(email_non_hostmapped_recipients_encoded_id_fix)
          end

          describe "and hostmapped headers are present" do
            describe "and only a To address is present" do
              before do
                mail.to = address("support+id#{encoded_id_1}@exxxample.com")
              end

              describe_with_arturo_disabled :email_non_hostmapped_recipients_encoded_id_fix do
                let(:email_non_hostmapped_recipients_encoded_id_fix) { false }

                it "selects the encoded ID from the hostmapped To header" do
                  encoded_id = mail.recipients_encoded_id(mail.account)
                  assert encoded_id_1.casecmp?(encoded_id)
                end
              end

              it "does not select the encoded ID from the hostmapped To header" do
                assert_nil mail.recipients_encoded_id(mail.account)
              end
            end

            describe "and the To and CC headers have valid and different +id-formatted encoded IDs" do
              before do
                mail.to = address("support+id#{encoded_id_1}@exxxample.com")
                mail.cc = [address("support+id#{encoded_id_2}@#{default_host}")]
              end

              describe_with_arturo_disabled :email_non_hostmapped_recipients_encoded_id_fix do
                let(:email_non_hostmapped_recipients_encoded_id_fix) { false }

                it "selects the encoded ID from the hostmapped To header" do
                  encoded_id = mail.recipients_encoded_id(mail.account)
                  assert encoded_id_1.casecmp?(encoded_id)
                end
              end

              it "selects the encoded ID from the non-hostmapped CC header" do
                encoded_id = mail.recipients_encoded_id(mail.account)
                assert encoded_id_2.casecmp?(encoded_id)
              end
            end
          end
        end

        describe "when the To, CC, and BCC headers all have valid and different +id-formatted encoded IDs" do
          before do
            mail.to = address("support+id#{encoded_id_1}@#{default_host}")
            mail.cc = [
              address("support+id#{encoded_id_2}@#{default_host}"),
            ]
            mail.bcc = address("support+id#{encoded_id_3}@#{default_host}")
          end

          it "selects the encoded ID from the To header" do
            encoded_id = mail.recipients_encoded_id(mail.account)
            assert encoded_id_1.casecmp?(encoded_id)
          end
        end

        describe "when the CC and BCC headers have valid and different +id-formatted encoded IDs" do
          before do
            mail.to = address("support@#{default_host}")
            mail.cc = [
              address("support+id#{encoded_id_1}@#{default_host}"),
            ]
            mail.bcc = address("support+id#{encoded_id_2}@#{default_host}")
          end

          it "selects the encoded ID from the CC header" do
            encoded_id = mail.recipients_encoded_id(mail.account)
            assert encoded_id_1.casecmp?(encoded_id)
          end
        end
      end

      describe "when identifying tickets via the email's body and/or text parts" do
        describe "when the mail's body and parts are all empty" do
          before do
            mail.body = nil
            mail.parts = []
          end

          it "does not blow up" do
            assert_equal [], mail.body_encoded_ids
          end
        end

        describe "when an encoded ID is present but not enclosed in square brackets" do
          let(:encoded_id_1_no_square_brackets_body) { "Hello! #{encoded_id_1} Thanks!" }

          ["text/html", "text/plain"].each do |content_type|
            describe "when the encoded ID is found inside the email's #{content_type} part" do
              before do
                part = Zendesk::Mail::Part.new.tap do |new_part|
                  new_part.content_type = content_type
                  new_part.body = encoded_id_1_no_square_brackets_body
                end

                mail.parts = [part]
              end

              it "does not find the encoded ID" do
                assert_empty mail.body_encoded_ids
              end
            end

            describe "when the encoded ID is found inside the email's body with content-type #{content_type}" do
              before do
                mail.content_type = content_type
                mail.body = encoded_id_1_no_square_brackets_body
              end

              it "does not find the encoded ID" do
                assert_empty mail.body_encoded_ids
              end
            end
          end
        end

        describe "when an encoded ID is present and enclosed in square brackets" do
          let(:encoded_id_1_square_brackets_body) { "Hello! [#{encoded_id_1}] Thanks!" }

          ["text/html", "text/plain"].each do |content_type|
            describe "when the encoded ID is found inside the email's #{content_type} part" do
              before do
                part = Zendesk::Mail::Part.new.tap do |new_part|
                  new_part.content_type = content_type
                  new_part.body = encoded_id_1_square_brackets_body
                end

                mail.parts = [part]
              end

              it "finds the encoded ID" do
                assert_equal [encoded_id_1], mail.body_encoded_ids
              end
            end

            describe "when the encoded ID is found inside the email's body with content-type #{content_type}" do
              before do
                mail.content_type = content_type
                mail.body = encoded_id_1_square_brackets_body
              end

              it "finds the encoded ID" do
                assert_equal [encoded_id_1], mail.body_encoded_ids
              end
            end
          end
        end

        describe "when multiple encoded IDs are present, each enclosed in square brackets" do
          let(:multiple_encoded_ids_in_square_brackets_body) { "Hello! [#{encoded_id_1}] Thanks! [#{encoded_id_2}] Bye!" }

          ["text/html", "text/plain"].each do |content_type|
            describe "when the encoded ID is found inside the email's #{content_type} part" do
              before do
                part = Zendesk::Mail::Part.new.tap do |new_part|
                  new_part.content_type = content_type
                  new_part.body = multiple_encoded_ids_in_square_brackets_body
                end

                mail.parts = [part]
              end

              it "finds the encoded IDs" do
                assert_equal [encoded_id_1, encoded_id_2], mail.body_encoded_ids
              end
            end

            describe "when the encoded ID is found inside the email's body with content-type #{content_type}" do
              before do
                mail.content_type = content_type
                mail.body = multiple_encoded_ids_in_square_brackets_body
              end

              it "finds the encoded IDs" do
                assert_equal [encoded_id_1, encoded_id_2], mail.body_encoded_ids
              end
            end
          end
        end
      end
    end

    describe "with nice identities" do
      it "has one identity from recipient plus addressing" do
        mail.to = address("mooid007@example.com")
        mail.cc = [
          address("oink+007@example.com"),
          address("support+id12345@example.com"),
          address("bah@example.com")
        ]
        mail.bcc = address("support+id54321@example.com")
        assert_equal "12345", mail.recipients_nice_id(accounts(:minimum))
      end
    end

    describe "with Message-IDs" do
      let(:encoded_id) { "99R900-FE4C" }

      describe "when there are Message-IDs in the References header" do
        it "extracts the References header Message-IDs" do
          mail.references = ["<message_id_1@example.com>", "<message_id_2@example.com>"]
          assert_equal ["<message_id_1@example.com>", "<message_id_2@example.com>"], mail.message_ids
        end
      end

      describe "when there is a Message-ID in the In-Reply-To header" do
        it "extracts the In-Reply-To header Message-ID" do
          mail.in_reply_to = "<message_id_1@example.com>"
          assert_equal ["<message_id_1@example.com>"], mail.message_ids
        end
      end
    end
  end

  describe "processor chain support" do
    let(:mail) { Zendesk::Mail::InboundMessage.new }

    describe "parties" do
      before do
        mail.to << address("to@example.com")
        mail.cc << address("cc@example.com")
        mail.from = address("from@example.com")
      end

      describe "mail without reply to" do
        it "returns with from address" do
          assert_equal %w[cc@example.com from@example.com to@example.com], mail.parties.map(&:address).sort
        end
      end

      describe "mail with reply to" do
        before do
          mail.reply_to = address("reply_to@example.com")
        end

        it "returns without from address" do
          assert_equal %w[cc@example.com reply_to@example.com to@example.com], mail.parties.map(&:address).sort
        end
      end

      describe "mail with same email address in multiple headers" do
        before do
          mail.reply_to = address("reply_to@example.com")
          mail.to << mail.from
          mail.cc << mail.reply_to
        end

        it "returns unique addresses" do
          assert_equal %w[cc@example.com from@example.com reply_to@example.com to@example.com], mail.parties.map(&:address).sort
        end
      end

      describe "mail with nil email address" do
        before do
          mail.to = address(nil)
        end

        it "returns not-nil addresses" do
          assert_equal %w[cc@example.com from@example.com], mail.parties.map(&:address).sort
        end
      end
    end

    describe "#original_recipient" do
      %w[X-Forwarded-For X-Envelope-To X-Orig-To Resent-From].each do |supported_header|
        it "reads the address from #{supported_header}" do
          mail.headers[supported_header] = [address("morten@example.com")]
          original_recipient_address = mail.original_recipient

          assert_equal address("morten@example.com"), original_recipient_address, "Failed on #{supported_header}"
        end
      end
    end

    describe "#smtp_envelope_from" do
      %w[Return-Path Envelope-From From].each do |supported_header|
        it "reads the smtp_envelope_from from #{supported_header}" do
          mail.headers[supported_header] = if supported_header == "From"
            [address("mel@example.com")]
          else
            ["mel@example.com"]
          end
          smtp_envelope_from = mail.smtp_envelope_from

          assert_equal "mel@example.com", smtp_envelope_from, "Failed on #{supported_header}"
        end
      end
    end

    describe "#redacted?" do
      before { refute mail.redacted? }

      it "is true when the mail is redacted" do
        mail.headers['X-Zendesk-Redacted'] = true
        assert mail.redacted?
      end

      it "is true when a mail part is redacted" do
        part = Zendesk::Mail::Part.new
        part.headers['X-Zendesk-Redacted'] = true
        mail.parts << part

        assert mail.redacted?
      end
    end

    describe "#reply?" do
      it "returns true for reply emails" do
        refute mail.reply?

        mail.subject = "Hey there!"
        refute mail.reply?

        mail.subject = "re: Hey there!"
        assert mail.reply?

        mail.subject = "RE: Hey there!"
        assert mail.reply?

        mail.subject = "re:Hey there!"
        assert mail.reply?

        mail.subject = "re:"
        assert mail.reply?

        mail.subject = "SV:"
        assert mail.reply?
      end
    end

    describe "#from_google_apps_group?" do
      it "detects emails from G Suite groups" do
        refute mail.from_google_apps_group?

        mail.headers["X-Google-Expanded"] = ["foo"]
        assert mail.from_google_apps_group?
        mail.headers["X-Google-Expanded"] = nil
        refute mail.from_google_apps_group?

        mail.headers["X-pstn-nxpr"] = ["foo"]
        assert mail.from_google_apps_group?
        mail.headers["X-pstn-nxpr"] = nil
        refute mail.from_google_apps_group?

        mail.headers["X-Google-Group-Id"] = ["foo"]
        assert mail.from_google_apps_group?
        mail.headers["X-Google-Group-Id"] = nil
        refute mail.from_google_apps_group?

        mail.headers["List-Help"] = ["foo"]
        refute mail.from_google_apps_group?
        mail.headers["List-Help"] = ["foo", "google.dk"]
        refute mail.from_google_apps_group?
        mail.headers["List-Help"] = ["foo", "google.dk", "google.com"]
        assert mail.from_google_apps_group?
      end
    end

    describe "#handle_remote_file_deletion" do
      let(:mail) { Zendesk::Mail::InboundMessage.new }
      let(:remote_file) { RemoteFiles::File.new("identifier") }
      let(:remote_url) { "https://cloud.com/files/1234" }

      before { mail.account = accounts(:minimum) }

      describe "with email_switch_off_redis_deletions Arturo enabled" do
        it "increments skip_deletion stats with tag: reason:file_deletions_disabled" do
          mail.statsd_client.expects(:increment).with(:skip_deletion, tags: ["reason:file_deletions_disabled"]).once
          mail.handle_remote_file_deletion(remote_file, remote_file)
        end
      end

      describe_with_arturo_disabled :email_switch_off_redis_deletions do
        describe "when remote file deletion fails because Redis can't be reached" do
          let(:error) { Redis::CannotConnectError }
          before { remote_file.expects(:delete).once.raises(error) }

          it "rescues the exception and increments skip_deletion stats with 'reason' and 'error_type' tags" do
            mail.statsd_client.expects(:increment).with(:skip_deletion, tags: ["reason:exception_raised", "error_type:#{error}"]).once
            mail.handle_remote_file_deletion(remote_file, remote_file)
          end
        end
      end
    end

    describe "#from_zendesk_address?" do
      it "is false if sender is empty" do
        mail.stubs(reply_to: nil)
        refute mail.from_zendesk_address?
      end

      [
        "support@foo.zendesk.com",
        "support@foo.zendesk-test.com",
        "support@zendesk.com",
        "<support@foo.zendesk.com>"
      ].each do |zd_address|
        it "is true when sender is #{zd_address}" do
          mail.stubs(reply_to: Zendesk::Mail::Address.new(address: zd_address))
          assert mail.from_zendesk_address?
        end
      end

      [
        "foo@fake-zendesk.com",
        "foo@example.com",
        "<foo@example.com>",
        "",
        "<>"
      ].each do |non_zd_address|
        it "is false when sender is #{non_zd_address}" do
          mail.stubs(reply_to: Zendesk::Mail::Address.new(address: non_zd_address))
          refute mail.from_zendesk_address?
        end
      end
    end

    describe '#zendesk_message_id?' do
      before do
        mail.account = accounts(:minimum)
        mail.stubs(message_id: message_id)
      end

      [
        "JPVGRER3VJ_5806398f94e7a_9e763f8ebcacd32c1417a5_sprut@#{Zendesk::Configuration.fetch(:host)}",
        "5806398f94e7a_9e763f8ebcacd32c1417a5_sprut@#{Zendesk::Configuration.fetch(:host)}",
        "JPVGRER3VJ@#{Zendesk::Configuration.fetch(:host)}"
      ].each do |zendesk_id|
        describe "when message ID #{zendesk_id} matches known Zendesk outbound message ID patterns" do
          let(:message_id) { zendesk_id }

          it { assert mail.zendesk_message_id? }
        end
      end

      [
        "C0A5C07541E07E4C8E98C61D800D00D7191771@HFWMXMSG07PH.hotels.ad.example.com",
        "b9fa3b126fe8be4e48c1e499ab5b6124@www.example.co.nz",
        "20161020162033.04EA910EF351@in2.pod4.sac1.zdsys.com",
        "5808d4e729baa_54143ff98e0cd31c3324f7@#{Zendesk::Configuration.fetch(:host)}",
        "JPVGRER3VJ_5806398f94e7a_sprut@#{Zendesk::Configuration.fetch(:host)}",
        "010101622224addc-236c79d3-cf30-4da8-b0d6-70c66ce0d133-000000@amazonses.com",
        "0101-236c79d3-cf30-4da8-b0d6-70c66ce0d133-000000@us-west-2.amazonses.com"
      ].each do |non_zendesk_id|
        describe "when message ID #{non_zendesk_id} does not match known Zendesk outbound message ID patterns" do
          let(:message_id) { non_zendesk_id }

          it { refute mail.zendesk_message_id? }
        end
      end
    end
  end
end
