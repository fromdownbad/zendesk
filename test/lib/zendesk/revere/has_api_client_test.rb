require_relative '../../../support/test_helper'

SingleCov.covered!

class RevereDummyClient
  extend Zendesk::Revere::HasApiClient

  class << self
    def test_client
      revere_api_client
    end
  end
end

describe Zendesk::Revere::HasApiClient do
  it "#revere_api_client" do
    Zendesk::Revere::ApiClient.expects(:new).returns(mock)
    assert RevereDummyClient.test_client
  end
end
