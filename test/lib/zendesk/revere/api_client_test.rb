require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 9

describe Zendesk::Revere::ApiClient do
  fixtures :accounts, :users

  before do
    @api_client = ::Zendesk::Revere::ApiClient.new
    @user = users(:minimum_agent)
    stub_request(:post, %r{.*/api/internal/subscribers.json}).to_return(status: 200)
  end

  describe '#update_subscription' do
    describe 'subscribe' do
      it 'subscribe an unsubscribed user' do
        @api_client.update_subscription(@user, true)
        assert_requested(:post, %r{.*/api/internal/subscribers.json},
          body: {
            "subscribers" => [{
              "zendesk_user_id" => @user.id,
              "alert_type" => "EmailAlert",
              "alert_destination" => "minimum_agent@aghassipour.com",
              "pod" => 1,
              "status" => "subscribed",
              "account" => {
                "zendesk_account_id" => @user.account.id,
                "subdomain" => "minimum",
                "uses_talk" => false,
                "uses_help_center" => false,
                "uses_chat" => false
              }
            }]
          })
      end

      it 'uses_chat' do
        Account.any_instance.expects(:zopim_integration).returns(stub)

        @api_client.update_subscription(@user, true)
        assert_requested(:post, %r{.*/api/internal/subscribers.json},
          body: {
            "subscribers" => [hash_including(
              "account" => hash_including(
                "uses_chat" => true
              )
            )]
          })
      end
    end

    describe 'unsubscribe' do
      let!(:delete_req_stub) { stub_request(:delete, %r{/.*/api/internal/accounts/.*/subscribers/.*\.json\?alert_type=EmailAlert}).to_return(status: 200) }

      it 'unsubscribe a subscribed user' do
        @api_client.update_subscription(@user, false)
        assert_requested(delete_req_stub)
      end
    end
  end

  describe '#update_subscription with new params' do
    before do
      Arturo::Feature.create!(phase: 'on', symbol: :new_subscribers_params)
    end

    describe 'subscribe' do
      let!(:stub_api_req) { stub_request(:post, %r{.*/api/internal/subscribers.json}).to_return(status: 200) }

      it 'will take only new params' do
        @api_client.update_subscription(@user, true)
        assert_requested(stub_api_req)
      end
    end

    describe 'unsubscribe' do
      let!(:delete_stub_req) do
        stub_request(:delete, %r{/.*/api/internal/subscribers/delete_from.json}).with(body: { email: @user.email, subdomain: @user.account.subdomain, alert_type: "EmailAlert" }.to_json).to_return(status: 204)
      end

      it 'unsubscribe a subscribed user' do
        @api_client.update_subscription(@user, false)
        assert_requested(delete_stub_req)
      end
    end
  end

  describe '#unsubscribed' do
    let (:unsubscribed_data) do
      [
        {
            zendesk_account_id: 3,
            zendesk_user_id: 4,
            id: 2,
            alert_type: 'EmailAlert',
            alert_destination: 'fake@email.com'
        }
      ]
    end

    it 'returns a list of unsubscribed revere user hashes' do
      s = stub_request(:get, %r{/.*/api/internal/subscribers/unsubscribed.json}).to_return(body: { unsubscribed: unsubscribed_data }.to_json, status: 200, headers: { 'Content-type' => 'application/json' })

      results = @api_client.unsubscribed

      assert_requested s
      assert_equal results.first['zendesk_account_id'], 3
    end

    it 'raises an error on non-200 status code' do
      stub_request(:get, %r{/.*/api/internal/subscribers/unsubscribed.json}).to_return(body: { unsubscribed: [] }.to_json, status: 300, headers: { 'Content-type' => 'application/json' })

      e = assert_raise(RuntimeError) do
        @api_client.unsubscribed
      end
      assert_equal e.message, "Failed to get unsubscribed users from Revere. Status Code (300)"
    end
  end

  describe '#delete_unsubscribed' do
    it 'takes a list of revere ids' do
      stub_request(:delete, %r{/.*/api/internal/subscribers/unsubscribed.json}).with(body: { unsubscribed: [2, 3, 4] }.to_json).to_return(status: 204)
      assert @api_client.delete_unsubscribed([2, 3, 4])
    end

    it 'success on a 204 status code' do
      stub_request(:delete, %r{/.*/api/internal/subscribers/unsubscribed.json}).to_return(status: 204)
      assert @api_client.delete_unsubscribed([])
    end

    it 'raises an error on non-204 status code' do
      stub_request(:delete, %r{/.*/api/internal/subscribers/unsubscribed.json}).to_return(status: 300)
      assert_raise(RuntimeError, "Failed to delete unsubscribed users from Revere. Status Code (300)") do
        @api_client.delete_unsubscribed([])
      end
    end
  end

  describe '#delete_from' do
    it 'will delete user from revere subscription list and return 204 status code' do
      stub_request(:delete, %r{/.*/api/internal/subscribers/delete_from.json}).with(body: { "email": 'example@zendesk.com', "subdomain": "lyft", "alert_type": "EmailAlert"}.to_json).to_return(status: 204)
      assert @api_client.delete_from('example@zendesk.com', 'lyft')
    end

    it 'will return 400 if empty params are sent' do
      stub_request(:delete, %r{/.*/api/internal/subscribers/delete_from.json}).to_return(status: 300)
      assert_raise(RuntimeError, "Failed to delete unsubscribed users from Revere. Status Code (300)") do
        @api_client.delete_from('', '')
      end
    end
  end
end
