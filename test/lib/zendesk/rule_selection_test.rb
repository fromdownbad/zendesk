require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::RuleSelection do
  it 'defines the `AND_JOIN` constant' do
    assert_equal ' AND ', Zendesk::RuleSelection::AND_JOIN
  end

  it 'defines the `OR_JOIN` constant' do
    assert_equal ' OR ', Zendesk::RuleSelection::OR_JOIN
  end
end
