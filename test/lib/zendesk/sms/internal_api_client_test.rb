require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Sms::InternalApiClient do
  fixtures :events

  let(:conn) { Minitest::Mock.new }
  let(:event) { events(:create_audit_for_minimum_ticket_1) }
  let(:account) { accounts(:minimum) }

  describe "#initialize" do
    it 'initializes the connection' do
      Kragle.expects(:new).with('sms', shared_cache: false).returns(conn)
      conn.expect(:url_prefix=, nil, [account.url(ssl: true, mapped: false)])

      Zendesk::Sms::InternalApiClient.new(account)
    end
  end

  describe "#send_notification" do
    let(:statsd_client) { stub_everything('statsd client') }

    before do
      Zendesk::StatsD::Client.stubs(:new).with(anything).returns(statsd_client)
    end

    it 'sends the request' do
      stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/sms/internal/triggers/1/messages").to_return(status: 200)
      response = Zendesk::Sms::InternalApiClient.new(account).send_notification(1, event, {})
      assert_equal 200, response.status
    end

    it 'rescues client errors and instruments them' do
      Zendesk::Sms::InternalApiClient::RESCUABLE_ERRORS.each do |error|
        stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/sms/internal/triggers/1/messages").to_raise(error)
        statsd_client.expects(:increment).with('error', anything)

        Zendesk::Sms::InternalApiClient.new(account).send_notification(1, event, {})
      end
    end

    it 'propagates other errors' do
      stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/sms/internal/triggers/1/messages").to_raise(Kragle::ServiceUnavailable)
      statsd_client.expects(:increment).with('unrescuable_sms_error', anything)

      assert_raise Kragle::ServiceUnavailable do
        Zendesk::Sms::InternalApiClient.new(account).send_notification(1, event, {})
      end
    end

    describe 'sets the timeout when sms_extended_notification_timeout is enabled' do
      let(:options) { stub('options') }
      let(:connection) { stub_everything('connection') }
      let(:request) { stub_everything('request') }

      before do
        Kragle.stubs(:new).returns(connection)
        request.stubs(:options).returns(options)
        connection.stubs(:post).yields(request).returns(200)
      end

      describe_with_arturo_enabled(:sms_extended_notification_timeout) do
        it 'sets the request timeout to 10 seconds' do
          options.expects(:timeout=).with(10)
          Zendesk::Sms::InternalApiClient.new(account).send_notification(1, event, {})
        end
      end

      describe_with_arturo_disabled(:sms_extended_notification_timeout) do
        it 'does not set the timeout' do
          options.expects(:timeout=).never
          Zendesk::Sms::InternalApiClient.new(account).send_notification(1, event, {})
        end
      end
    end
  end

  describe "#send_request_message" do
    let(:statsd_client) { stub_everything('statsd client') }

    before do
      Zendesk::StatsD::Client.stubs(:new).with(anything).returns(statsd_client)
    end

    it 'sends the request' do
      stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/sms/internal/tickets/1/reply").to_return(status: 200)
      response = Zendesk::Sms::InternalApiClient.new(account).send_request_message(1, 1, 'sms', 'hello')
      assert_equal 200, response.status
    end

    it 'rescues timeouts and client errors, instruments them, and returns nil' do
      stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/sms/internal/tickets/1/reply").to_raise(Kragle::BadRequest)
      statsd_client.expects(:increment).with('error', anything)

      assert_nil Zendesk::Sms::InternalApiClient.new(account).send_request_message(1, 1, 'sms', 'hello')
    end

    it 'rescues client errors and instruments them' do
      Zendesk::Sms::InternalApiClient::RESCUABLE_ERRORS.each do |error|
        stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/sms/internal/tickets/1/reply").to_raise(error)
        statsd_client.expects(:increment).with('error', anything)

        Zendesk::Sms::InternalApiClient.new(account).send_request_message(1, 1, 'sms', 'hello')
      end
    end

    it 'propagates other errors' do
      stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/sms/internal/tickets/1/reply").to_raise(Kragle::ServiceUnavailable)
      statsd_client.expects(:increment).with('unrescuable_sms_error', anything)
      assert_raise Kragle::ServiceUnavailable do
        Zendesk::Sms::InternalApiClient.new(account).send_request_message(1, 1, 'sms', 'hello')
      end
    end
  end
end
