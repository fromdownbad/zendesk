require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::OrganizationSubscriptions::Finder do
  fixtures :accounts, :users, :organizations

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:end_user) { users(:minimum_end_user) }
  let(:organization) { organizations(:minimum_organization1) }
  let(:source_type) { Organization.name }
  let(:params) { {} }
  let(:finder) { Zendesk::OrganizationSubscriptions::Finder.new(account, current_user, params) }

  describe "#scope" do
    describe "for end_user" do
      let(:current_user) { end_user }

      it "returns the correct scope" do
        assert_equal current_user.watchings, finder.scope
      end
    end

    describe "for agent" do
      let(:current_user) { agent }

      it "returns the correct scope" do
        assert_equal account.watchings, finder.scope
      end

      describe "with user_id" do
        let(:params) { { user_id: end_user.id } }

        it "returns the correct scope" do
          assert_equal account.users.find(end_user.id).watchings, finder.scope
        end
      end

      describe "with organization_id" do
        let(:params) { { organization_id: organization.id } }

        it "returns the correct scope" do
          assert_equal account.organizations.find(organization.id).watchings, finder.scope
        end
      end
    end

    describe "for system user" do
      let(:current_user) { users(:systemuser) }

      it "returns the correct scope" do
        assert_equal current_user.watchings, finder.scope
      end
    end
  end

  describe "#organization_scope" do
    describe "for end_user" do
      let(:current_user) { end_user }

      it "returns the correct organization_scope" do
        assert_equal current_user.end_user_viewable_organizations, finder.organization_scope
      end
    end

    describe "for agent" do
      let(:current_user) { agent }

      it "returns the correct organization_scope" do
        assert_equal account.organizations, finder.organization_scope
      end
    end
  end
end
