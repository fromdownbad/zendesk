require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 74

class SessionManagementTestController < ActionController::Base
  include Zendesk::SessionManagement

  def index
    head :ok
  end
end

class SessionManagementTest < ActionController::TestCase
  tests SessionManagementTestController
  use_test_routes

  before do
    Arturo.enable_feature! :doorman_header_for_account_logging

    @controller.stubs(:current_account).returns(accounts(:minimum))
    @controller.stubs(:current_brand).returns(accounts(:minimum).brands.first)
    @controller.stubs(:current_route).returns(accounts(:minimum).routes.first)
    @controller.stubs(:authenticated_with_session?).returns(false)
  end

  describe 'with a doorman header that matches current_account' do
    before do
      account = accounts(:minimum)
      @controller.stubs(:parsed_doorman_header).returns(
        'account' => {
          'id' => account.id,
          'is_active' => account.is_active,
          'shard_id' => account.shard_id,
          'subdomain' => account.subdomain,
          'multiproduct' => account.multiproduct,
          'lock_state' => account.lock_state,
          'brand_id' => account.brands.first.id,
          'route_id' => account.routes.first.id
        }
      )
    end

    it 'succesfully increment metrics' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('brand.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('route.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('shard.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('doorman_account_info.match')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.SessionManagementTestController.index', anything)

      get :index
      assert_response :success
    end
  end

  describe 'with a non-matching current_account' do
    before do
      account = accounts(:support)
      @controller.stubs(:parsed_doorman_header).returns(
        'account' => {
          'id' => account.id,
          'is_active' => account.is_active,
          'shard_id' => account.shard_id,
          'subdomain' => account.subdomain,
          'multiproduct' => account.multiproduct,
          'lock_state' => account.lock_state,
          'brand_id' => account.brands.first.id,
          'route_id' => account.routes.first.id
        }
      )
    end

    it 'logs using statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('account.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('doorman_account_info.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.SessionManagementTestController.index', anything)
      get :index
      assert_response :success
    end
  end

  describe 'with a doorman header without an account' do
    before do
      @controller.stubs(:parsed_doorman_header).returns(foo: 'bar')
    end

    it 'logs using statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('account.missing_account')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('doorman_account_info.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.SessionManagementTestController.index', anything)
      get :index
      assert_response :success
    end
  end

  describe 'with a doorman header shard mismatch' do
    before do
      account = accounts(:minimum)
      @controller.stubs(:parsed_doorman_header).returns(
        'account' => {
          'id' => account.id,
          'is_active' => account.is_active,
          'shard_id' => account.shard_id,
          'subdomain' => account.subdomain,
          'multiproduct' => account.multiproduct,
          'lock_state' => account.lock_state,
          'brand_id' => account.brands.first.id,
          'route_id' => account.routes.first.id
        }
      )
      account.shard_id = 10000
    end

    it 'logs using statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('brand.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('route.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('shard.match.false')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('account.attributes.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('doorman_account_info.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.SessionManagementTestController.index', anything)

      get :index
      assert_response :success
    end
  end

  describe 'with a doorman header route mismatch' do
    before do
      account = accounts(:minimum)
      @controller.stubs(:parsed_doorman_header).returns(
        'account' => {
          'id' => account.id,
          'is_active' => account.is_active,
          'shard_id' => account.shard_id,
          'subdomain' => account.subdomain,
          'multiproduct' => account.multiproduct,
          'lock_state' => account.lock_state,
          'brand_id' => account.brands.first.id,
          'route_id' => 10000
        }
      )
    end

    it 'logs using statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('brand.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('route.match.false')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('shard.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('account.attributes.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('doorman_account_info.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.SessionManagementTestController.index', anything)

      get :index
      assert_response :success
    end
  end

  describe 'with a doorman brand mismatch' do
    before do
      account = accounts(:minimum)
      @controller.stubs(:parsed_doorman_header).returns(
        'account' => {
          'id' => account.id,
          'is_active' => account.is_active,
          'shard_id' => account.shard_id,
          'subdomain' => account.subdomain,
          'multiproduct' => account.multiproduct,
          'lock_state' => account.lock_state,
          'brand_id' => 10000,
          'route_id' => account.routes.first.id
        }
      )
    end

    it 'logs using statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('brand.match.false')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('route.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('shard.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('account.attributes.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('doorman_account_info.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.SessionManagementTestController.index', anything)

      get :index
      assert_response :success
    end
  end

  describe 'with a brand is missing from both sides' do
    before do
      account = accounts(:minimum)
      @controller.stubs(:current_brand).returns(nil)
      @controller.stubs(:parsed_doorman_header).returns(
        'account' => {
          'id' => account.id,
          'is_active' => account.is_active,
          'shard_id' => account.shard_id,
          'subdomain' => account.subdomain,
          'multiproduct' => account.multiproduct,
          'lock_state' => account.lock_state,
          'brand_id' => nil,
          'route_id' => account.routes.first.id
        }
      )
    end

    it 'logs using statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('brand.match.empty')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('brand.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('route.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('shard.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('doorman_account_info.match')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.SessionManagementTestController.index', anything)

      get :index
      assert_response :success
    end
  end

  describe 'with a deleted brand mismatch' do
    before do
      account = accounts(:minimum)
      subdomain = "averyverylongsubdomainnamethatgoesonandonandonandisgreaterjkl"
      brand2 = FactoryBot.create(:brand, subdomain: subdomain, id: 5000)
      # no validation on soft delete
      brand2.soft_delete!

      @controller.stubs(:current_brand).returns(brand2)
      @controller.stubs(:parsed_doorman_header).returns(
        'account' => {
          'id' => account.id,
          'is_active' => account.is_active,
          'shard_id' => account.shard_id,
          'subdomain' => account.subdomain,
          'multiproduct' => account.multiproduct,
          'lock_state' => account.lock_state,
          'brand_id' => account.brands.first.id,
          'route_id' => account.routes.first.id
        }
      )
    end

    it 'logs using statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('brand.match.false')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('route.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('shard.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('account.attributes.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('doorman_account_info.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.SessionManagementTestController.index', anything)

      get :index
      assert_response :success
    end
  end

  describe 'with a doorman header subdomain mismatch' do
    before do
      account = accounts(:minimum)
      @controller.stubs(:parsed_doorman_header).returns(
        'account' => {
          'id' => account.id,
          'is_active' => account.is_active,
          'shard_id' => account.shard_id,
          'subdomain' => account.subdomain,
          'multiproduct' => account.multiproduct,
          'lock_state' => account.lock_state,
          'brand_id' => account.brands.first.id,
          'route_id' => account.routes.first.id
        }
      )
      account.subdomain = 'minimal'
    end

    it 'logs using statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('brand.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('route.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('shard.match.true')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('account.attributes.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('doorman_account_info.mismatch')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.SessionManagementTestController.index', anything)

      get :index
      assert_response :success
    end
  end

  describe 'when mismatch_account raises an exception' do
    before do
      @controller.stubs(:parsed_doorman_header).raises(StandardError)
    end

    it 'errors out and logs an exception' do
      Rails.logger.expects(:error).at_least_once
      get :index
    end
  end
end
