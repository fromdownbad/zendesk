require_relative '../../../support/test_helper'

SingleCov.covered!

describe ::Zendesk::Channels::Factory do
  fixtures :accounts, :users, :groups, :memberships, :facebook_pages

  describe '.user_group_ids' do
    it 'returns the group IDs for the user' do
      user = users(:minimum_agent)
      group_id = groups(:minimum_group).id

      assert_equal [group_id], ::Zendesk::Channels::Factory.user_group_ids(user)
    end
  end

  describe '.account_facebook_page_limit' do
    before do
      @account = accounts(:minimum)
    end

    it "is set to 15 by default" do
      assert_equal 15, ::Zendesk::Channels::Factory.account_facebook_page_limit(@account.id)
    end

    it "responds to custom limits" do
      @account.settings.monitored_facebook_page_limit = 50
      @account.settings.save
      assert_equal 50, ::Zendesk::Channels::Factory.account_facebook_page_limit(@account.id)
    end

    describe 'it should return the max out of all limit values' do
      it 'returns default if custom limit is lower' do
        @account.settings.monitored_facebook_page_limit = 5
        @account.settings.save
        assert_equal 15, ::Zendesk::Channels::Factory.account_facebook_page_limit(@account.id)
      end

      it 'returns custom if custom limit is higher' do
        @account.settings.monitored_facebook_page_limit = 80
        @account.settings.save
        assert_equal 80, ::Zendesk::Channels::Factory.account_facebook_page_limit(@account.id)
      end
    end
  end

  describe '.account_facebook_page_limited?' do
    let(:account) { accounts(:minimum) }
    let(:current_count) { Facebook::Page.active.where(account_id: account.id).count(:all) }

    it 'returns true when page count is equal to limit' do
      ::Zendesk::Channels::Factory.expects(:account_facebook_page_limit).returns(current_count)
      assert ::Zendesk::Channels::Factory.account_facebook_page_limited?(account.id)
    end

    it 'returns true when page count is greater than limit' do
      ::Zendesk::Channels::Factory.expects(:account_facebook_page_limit).returns(current_count - 1)
      assert ::Zendesk::Channels::Factory.account_facebook_page_limited?(account.id)
    end

    it 'returns false when page count is less than limit' do
      ::Zendesk::Channels::Factory.expects(:account_facebook_page_limit).returns(current_count + 1)
      refute ::Zendesk::Channels::Factory.account_facebook_page_limited?(account.id)
    end

    it 'only considers active pages' do
      # If there are inactive pages, previous tests have already proven they're not counted
      assert ::Facebook::Page.count(:all) > Facebook::Page.active.count(:all)
    end
  end
end
