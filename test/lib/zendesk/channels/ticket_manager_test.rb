require_relative '../../../support/test_helper'

SingleCov.covered!

describe 'Zendesk::Channels::TicketManager' do
  fixtures :accounts, :tickets, :events, :sequences, :translation_locales
  describe 'TicketManager' do
    before do
      @account = accounts(:minimum)
      @decoration_data = { abc: { def: 'ghi' }}
      @ticket_manager = ::Zendesk::Channels::TicketManager.new(@account, @decoration_data)
    end

    describe '#convert_incoming_comment' do
      before do
        @params = { source_access_token: 123 }
        converted_params = { access_token: 123 }
        @ticket = tickets(:minimum_1)
        @comment = @ticket.comments.last
        @converter = stub
        @converter.stubs(:create_ticket?).returns(true)

        ::Zendesk::Channels::TicketConverter.expects(:new).with(@account, converted_params).returns(@converter)
      end

      describe 'author is not suspended' do
        before do
          author = mock('author', suspended?: false)
          @converter.expects(:author).returns(author)
          @ticket_manager.expects(:create_ticket_and_decoration).with(@converter).returns([@ticket, @comment])
        end

        it 'converts source_access_token param to access_token' do
          @ticket_manager.convert_incoming_comment(@params)
        end

        it 'returns the ticket id, nice id and comment id' do
          assert_equal [@ticket.id, @ticket.nice_id, @comment.id],
            @ticket_manager.convert_incoming_comment(@params)
        end

        describe 'when ticket should be created' do
          before do
            @converter.expects(:create_ticket?).returns(true)
          end

          it 'sets the nice_id before creating the ticket aaa' do
            @ticket_manager.convert_incoming_comment(@params)
            assert_not_nil @params[:nice_id]
          end
        end

        describe 'when comment should be created' do
          before do
            @converter.expects(:create_ticket?).returns(false)
          end

          it 'does not set the nice_id aaa' do
            @ticket_manager.convert_incoming_comment(@params)
            assert_nil @params[:nice_id]
          end
        end
      end

      describe 'when author is suspended' do
        before do
          author = mock('author', id: '123', suspended?: true)
          @converter.expects(:author).twice.returns(author)
        end

        it 'raises permanent error' do
          assert_raises(::Channels::Errors::PermanentError) do
            @ticket_manager.convert_incoming_comment(@params)
          end
        end
      end
    end

    describe '#create_ticket_and_decoration' do
      before do
        @converter = stub
        @ticket = tickets(:minimum_1)
        @comment = @ticket.comments.last
      end

      describe 'normal operation' do
        before do
          @sequence = sequence(:create_ticket_and_decoration)
          @converter.expects(:convert).returns([@ticket, @ticket.comments.last]).in_sequence(@sequence)
          @ticket_manager.expects(:create_decoration).with(@comment).in_sequence(@sequence)
        end

        it 'invokes the TicketConverter and returns the ticket and comment' do
          assert_equal [@ticket, @comment], @ticket_manager.create_ticket_and_decoration(@converter)
        end

        it 'sends metrics' do
          Benchmark.expects(:ms).returns(200.0).yields
          Zendesk::StatsD::Client.any_instance.
            expects(:timing).
            with('create_ticket_and_decoration', 200.0, tags: ['zendesk_version:ci-test'])

          @ticket_manager.create_ticket_and_decoration(@converter)
        end

        describe 'when decorating comment fails' do
          it 'should roll back the transaction' do
            Ticket.expects(:transaction).yields
            @ticket_manager.create_ticket_and_decoration(@converter)
          end
        end
      end

      describe 'exception raised' do
        before do
          @converter.expects(:convert).raises(ActiveRecord::RecordInvalid)
        end

        it 'turns the exception into a PermanentError' do
          assert_raises(::Channels::Errors::PermanentError) do
            @ticket_manager.create_ticket_and_decoration(@converter)
          end
        end
      end

      it 'should wrap ticket and decoration creation in a transaction' do
        Ticket.expects(:transaction)
        @converter.expects(:convert).never
        @ticket_manager.expects(:create_decoration).never
        @ticket_manager.create_ticket_and_decoration(@converter)
      end
    end

    describe '#decorate_outgoing_comment' do
      before do
        @comment = tickets(:minimum_1).comments.last
      end

      it 'finds and decorate the comment' do
        @ticket_manager.expects(:create_decoration).with(@comment)
        @ticket_manager.decorate_outgoing_comment(@comment.id)
      end

      describe 'ticket is deleted' do
        before do
          tickets(:minimum_1).delete
        end

        it 'decorates comment' do
          @ticket_manager.expects(:create_decoration).never
          @ticket_manager.decorate_outgoing_comment(@comment.id)
        end
      end
    end

    describe '#create_decoration' do
      before do
        @event = events(:duplicate_text_for_minimum_ticket_1)
      end

      it 'creates decoration' do
        assert_difference('EventDecoration.count(:all)', 1) do
          decoration = @ticket_manager.create_decoration(@event)
          assert_equal @event.ticket_id, decoration.ticket_id
          assert_equal @event.id, decoration.event_id
          assert_equal Hashie::Mash.new(@decoration_data), decoration.data
        end
      end

      it 'saves the decoration data as a hash' do
        decoration = @ticket_manager.create_decoration(@event)
        assert_equal Hash, decoration.attributes['data'].class
        assert_equal Hash, decoration.attributes['data'][:abc].class
      end
    end

    describe '#create_link_comment' do
      before do
        @target_ticket = tickets(:minimum_1)
        @source_ticket = tickets(:minimum_2)
      end

      describe 'when there is no link before' do
        describe 'when the event_decoration argument is not present' do
          it 'should create a new comment and a new ticket link' do
            assert_difference 'Comment.count(:all)', +1 do
              assert_difference 'TicketLink.count(:all)', +1 do
                assert_no_difference 'EventDecoration.count(:all)' do
                  ::Zendesk::Channels::TicketManager.create_link_comment(
                    @account,
                    'comment text stub',
                    @target_ticket.id,
                    @source_ticket.id
                  )
                end
              end
            end
          end
        end
      end

      describe 'when the event_decoration argument is pressent' do
        it 'should create a new comment, event_decoration for the comment, and a new ticket link' do
          assert_difference 'Comment.count(:all)', +1 do
            assert_difference 'TicketLink.count(:all)', +1 do
              assert_difference 'EventDecoration.count(:all)', +1 do
                ::Zendesk::Channels::TicketManager.create_link_comment(
                  @account,
                  'comment text stub',
                  @target_ticket.id,
                  @source_ticket.id,
                  event: 'decoration'
                )
              end
            end
          end

          # Check the created link has the correct source_id, target_id, and link_type
          assert TicketLink.where(
            source_id: @source_ticket.id,
            target_id: @target_ticket.id,
            link_type: TicketLink::FACEBOOK
          ).exists?

          # Assert the created comment is based on the input text
          comment = Comment.where("value LIKE '%comment text stub%'").first
          assert comment.present?

          # Assert the event_decoration
          assert_equal 'decoration', comment.event_decoration.data[:event]
        end

        # This test causes the ticket save to fail with the `with_rollback` helper. It is possible to test more failure
        # states however the ticket save call is a very slow operation so I'm trying to avoid multiple tests that
        # require it.
        it 'should wrap the new comment, event_decoration, and ticket link in a transaction' do
          assert_no_difference 'Comment.count(:all)' do
            assert_no_difference 'TicketLink.count(:all)' do
              assert_no_difference 'EventDecoration.count(:all)' do
                with_rollback(@target_ticket) do
                  ::Zendesk::Channels::TicketManager.create_link_comment(
                    @account,
                    'comment text stub',
                    @target_ticket.id,
                    @source_ticket.id
                  )
                end
              end
            end
          end
        end
      end

      describe 'if the child_ticket_id is for a deleted ticket' do
        it 'should not create a new comment, event_decoration, nor a new ticket link' do
          assert_no_difference 'Comment.count(:all)' do
            assert_no_difference 'TicketLink.count(:all)' do
              assert_no_difference 'EventDecoration.count(:all)' do
                ::Zendesk::Channels::TicketManager.create_link_comment(
                  @account,
                  'comment text stub',
                  @target_ticket.id,
                  Ticket.maximum(:id) + 1
                )
              end
            end
          end
        end
      end

      describe 'if the parent_ticket_id is for a deleted ticket' do
        it 'should not create a new comment, event_decoration, nor a new ticket link' do
          assert_no_difference 'Comment.count(:all)' do
            assert_no_difference 'TicketLink.count(:all)' do
              assert_no_difference 'EventDecoration.count(:all)' do
                ::Zendesk::Channels::TicketManager.create_link_comment(
                  @account,
                  'comment text stub',
                  Ticket.maximum(:id) + 1,
                  @source_ticket.id
                )
              end
            end
          end
        end
      end

      describe 'if the parent_ticket_id is for an inoperable ticket' do
        it 'should not create a new comment, event_decoration, nor a new ticket link' do
          @target_ticket.will_be_saved_by(@account.owner)
          @target_ticket.update_attributes(status_id: StatusType.CLOSED)
          assert_no_difference 'Comment.count(:all)' do
            assert_no_difference 'TicketLink.count(:all)' do
              assert_no_difference 'EventDecoration.count(:all)' do
                ::Zendesk::Channels::TicketManager.create_link_comment(
                  @account,
                  'comment text stub',
                  @target_ticket.id,
                  @source_ticket.id
                )
              end
            end
          end
        end
      end

      describe 'if there is a link before' do
        before do
          @target_ticket.target_links.build(
            source: @source_ticket,
            target: @target_ticket,
            link_type: TicketLink::FACEBOOK
          ).save
        end

        it 'should not create a new comment, event_decoration, nor a new ticket link' do
          assert_no_difference 'Comment.count(:all)' do
            assert_no_difference 'TicketLink.count(:all)' do
              assert_no_difference 'EventDecoration.count(:all)' do
                ::Zendesk::Channels::TicketManager.create_link_comment(
                  @account,
                  'comment text stub',
                  @target_ticket.id,
                  @source_ticket.id
                )
              end
            end
          end
        end
      end

      describe 'when locale is set at account level' do
        it 'should translate the comment text based on the set locale' do
          @account = accounts(:minimum)
          @account.translation_locale = translation_locales(:japanese)
          ::Zendesk::Channels::TicketManager.create_link_comment(
            @account,
            'comment text stub',
            @target_ticket.id,
            @source_ticket.id
          )
          # Assert the created comment is translated based on specified locale
          comment = @target_ticket.comments.where("value LIKE '%ja_link_to_child_ticket%'").first
          assert comment.present?
        end
      end
    end
  end
end
