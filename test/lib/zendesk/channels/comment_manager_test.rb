require_relative '../../../support/test_helper'

SingleCov.covered!

describe 'Zendesk::Channels::CommentManager' do
  fixtures :accounts, :tickets, :events, :sequences

  describe 'CommentManager' do
    let(:error_params) do
      {
        message: 'test description',
        notification_type: 'error'
      }
    end

    before do
      @account = accounts(:minimum)
      @comment = events(:create_comment_for_minimum_ticket_1)
      @comment_manager = ::Zendesk::Channels::CommentManager.new(@account.id, @comment.id)
    end

    describe '#report_channelback_error' do
      it 'adds an event to the audit for the comment' do
        @comment_manager.report_channelback_error(error_params)
        cbfe = @comment.reload.audit.events.all.find { |evt| evt.is_a?(ChannelBackFailedEvent) }
        assert_equal 'test description', cbfe.value[:description]
      end

      describe 'growls' do
        it 'localizes the growl to the locale of the author of the comment' do
          ::I18n.expects(:with_locale).with(@comment.author.translation_locale)
          @comment_manager.report_channelback_error(error_params)
        end

        it "growls a localized message to the agent via radar of the `notification_type` passed to it" do
          rn = stub('radar notification')
          ::RadarFactory.expects(:create_radar_notification).with(@account, "growl/#{@comment.author_id}").returns(rn)
          rn.expects(:send).with(error_params[:notification_type], error_params[:message])
          @comment_manager.report_channelback_error(error_params)
        end
      end
    end
  end
end
