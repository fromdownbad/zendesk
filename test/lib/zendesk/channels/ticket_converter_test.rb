require_relative '../../../support/test_helper'

SingleCov.covered!

describe 'Zendesk::Channels::TicketConverter' do
  fixtures :accounts, :users, :groups, :sequences, :tickets, :monitored_twitter_handles, :brands, :event_decorations,
    :ticket_fields, :custom_field_options, :tokens, :attachments

  describe 'TicketConverter' do
    before do
      @account = accounts(:minimum)
      Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)

      @params = {
        comment_text: 'abc',
        via_id: ViaType.TWITTER,
        user: {
          id: '153594297',
          id_str: '153594297',
          name: 'john doe',
          screen_name: 'john doe',
          photo_url: 'http://url.com/photo.jpg'
        },
        nice_id: 800,
        source_id: 12345,
        attachments: [tokens(:minimum_upload_token).value]
      }
      stub_request(:get, 'https://api.twitter.com/1.1/users/show.json?user_id=153594297').
        to_return(status: 200, body: read_test_file('twitter_users_show_153594297_1.1.json'))
      stub_request(:get, "http://url.com/photo.jpg")
    end

    def setup_html_body
      @params[:html_body] = '<div>rich_content!</div>'
    end

    describe '#create_ticket?' do
      let(:converter) { ::Zendesk::Channels::TicketConverter.new(@account, @params) }

      describe 'when there is no ticket id' do
        it 'should be true' do
          assert converter.create_ticket?
        end
      end

      describe 'when there is a ticket id' do
        describe 'when the ticket is closed' do
          before do
            @ticket = Ticket.find(converter.convert[0].id)
            @ticket.status_id = StatusType.CLOSED
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!
            assert @ticket.closed?

            @params[:ticket_id] = @ticket.id
          end

          it 'should be true' do
            assert converter.create_ticket?
          end
        end

        describe 'when the ticket is deleted' do
          before do
            @ticket = Ticket.find(converter.convert[0].id)
            @ticket.status_id = StatusType.DELETED
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!
            refute @account.tickets.find_by_id(@ticket.id)

            @params[:ticket_id] = @ticket.id
          end

          it 'should be true' do
            assert converter.create_ticket?
          end
        end

        describe 'when the ticket is open' do
          before do
            @ticket = Ticket.find(converter.convert[0].id)
            @ticket.status_id = StatusType.OPEN
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!
            refute @ticket.closed?

            @params[:ticket_id] = @ticket.id
          end

          it 'should be false' do
            refute converter.create_ticket?
          end
        end
      end
    end

    describe '#convert' do
      describe 'when there is no ticket id' do
        it 'creates ticket and comment' do
          converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
          assert_difference('Ticket.count(:all)', 1) do
            assert_difference('Comment.count(:all)', 1) do
              Comment.any_instance.expects(:channel_source_id=).with(@params[:source_id])
              ticket, comment = converter.convert
              assert_equal 'abc', ticket.subject
              assert_equal 'abc', ticket.description
              assert_equal 'abc', comment.body
              assert_equal '<div class="zd-comment" dir="auto"><p>abc</p></div>', comment.html_body
              assert_equal ViaType.TWITTER, ticket.via_id
              assert_equal @params[:source_id], ticket.via_reference_id
              assert_equal ViaType.TWITTER, comment.via_id
              assert_equal ViaType.TWITTER, comment.audit.via_id
              assert_equal 800, ticket.nice_id
              assert_equal 2, comment.attachments.length
            end
          end
        end

        describe 'when there is html_body in the parameters' do
          before do
            setup_html_body
          end

          it 'creates ticket and comment with html_body' do
            converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
            _ticket, comment = converter.convert
            assert_equal 'rich_content!', comment.body
            assert_equal '<div class="zd-comment" dir="auto"><div>rich_content!</div></div>', comment.html_body
          end
        end

        describe 'non-default brand passed in' do
          before do
            route = @account.routes.create!(subdomain: 'testbrand')
            @brand = @account.brands.create! { |b| b.name = 'Test Brand'; b.route = route }
            @params[:brand_id] = @brand.id
          end

          describe 'and account does not have multiple brands' do
            it 'does not set ticket brand based on source brand' do
              @account.stubs(:has_multiple_active_brands?).returns(false)
              ticket, _comment = ::Zendesk::Channels::TicketConverter.new(@account, @params).convert
              assert_equal @account.default_brand_id, ticket.brand_id
            end
          end

          describe 'and the selected brand is not active' do
            it 'does not set ticket brand based on source brand' do
              @brand.active = false
              @account.stubs(:has_multiple_active_brands?).returns(true)
              ticket, _comment = ::Zendesk::Channels::TicketConverter.new(@account, @params).convert
              assert_equal @account.default_brand_id, ticket.brand_id
            end
          end

          describe 'and account has multiple brands' do
            it 'sets ticket brand based on source brand' do
              @account.stubs(:has_multiple_active_brands?).returns(true)
              ticket, _comment = ::Zendesk::Channels::TicketConverter.new(@account, @params).convert
              assert_equal @params[:brand_id], ticket.brand_id
            end
          end
        end

        describe 'when ticket fields are passed in' do
          before do
            Account.any_instance.stubs(:field_ticket_type).returns(FieldTicketType.new)
            @field_1 = ticket_fields(:field_tagger_custom)
            @field_1_value = custom_field_options(:field_tagger_custom_option_1)
            @field_2 = ticket_fields(:field_text_custom)
          end

          describe 'legitimate ticket fields' do
            before do
              @group = groups(:minimum_group)
              @assignee = users(:minimum_agent)

              @params[:ticket_fields] = [
                { 'id' => 'subject', 'value' => 'test subject' },
                { 'id' => 'description', 'value' => 'test description' },
                { 'id' => 'status', 'value' => 'pending' },
                { 'id' => 'Type', 'value' => 'problem' }, # Note: uppercase, to test case insensitivity
                { 'id' => 'priority', 'value' => 'high' },
                { 'id' => 'group', 'value' => @group.id },
                { 'id' => 'assignee', 'value' => @assignee.id },
                { 'id' => 'tags', 'value' => ['test_tag_1', 'test_tag_2'] },
                { 'id' => @field_1.title, 'value' => @field_1_value.value },
                { 'id' => @field_2.id, 'value' => 'field two value' }
              ]
            end

            it 'sets field values on ticket' do
              converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
              ticket, _comment = converter.convert
              ticket.reload
              assert_equal 'test subject', ticket.subject
              assert_equal 'test description', ticket.description
              assert_equal StatusType.PENDING, ticket.status_id
              assert_equal TicketType.PROBLEM, ticket.ticket_type_id
              assert_equal PriorityType.HIGH, ticket.priority_id
              assert_equal @group, ticket.group
              assert_equal @assignee, ticket.assignee
              assert ticket.current_tags.include?('test_tag_1 test_tag_2')
              assert_equal @field_1_value.value, ticket.ticket_field_entries.where(ticket_field_id: @field_1.id).first.value
              assert_equal 'field two value', ticket.ticket_field_entries.where(ticket_field_id: @field_2.id).first.value
            end
          end

          describe 'disallowed ticket fields' do
            before do
              @params[:ticket_fields] = [
                { 'id' => 'status', 'value' => 'NOT SUPPORTED' },
                { 'id' => 'type', 'value' => 'NOT SUPPORTED' },
                { 'id' => 'priority', 'value' => 'NOT SUPPORTED' },
                { 'id' => 'group', 'value' => -1 },
                { 'id' => 'assignee', 'value' => -2 },
                { 'id' => 'foo', 'value' => 'bar' },
                { 'id' => -3, 'value' => 'baz' }
              ]
            end

            it 'does not set field values on ticket and does not error' do
              converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
              ticket, _comment = converter.convert
              ticket.reload
              assert_equal StatusType.NEW, ticket.status_id
              assert_equal TicketType.find('-'), ticket.ticket_type_id
              assert_equal PriorityType.find('-'), ticket.priority_id
              assert_nil ticket.assignee
              assert_equal [], ticket.ticket_field_entries
            end
          end

          describe 'A mix of allowed and disallowed ticket fields' do
            before do
              @params[:ticket_fields] = [
                { 'id' => 'subject', 'value' => 'test subject' },
                { 'id' => 'description', 'value' => 'test description' },
                { 'id' => 'status', 'value' => 'NOT SUPPORTED' },
                { 'id' => 'type', 'value' => 'NOT SUPPORTED' },
                { 'id' => 'priority', 'value' => 'NOT SUPPORTED' },
                { 'id' => 'group', 'value' => -1 },
                { 'id' => 'assignee', 'value' => -2 },
                { 'id' => 'foo', 'value' => 'bar' },
                { 'id' => -3, 'value' => 'baz' }
              ]
            end

            it 'sets allowed field values on ticket, does not set disallowed values, and does not error' do
              converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
              ticket, _comment = converter.convert
              ticket.reload
              assert_equal 'test subject', ticket.subject
              assert_equal 'test description', ticket.description
              assert_equal StatusType.NEW, ticket.status_id
              assert_equal TicketType.find('-'), ticket.ticket_type_id
              assert_equal PriorityType.find('-'), ticket.priority_id
              assert_nil ticket.assignee
              assert_equal [], ticket.ticket_field_entries
            end
          end
        end

        describe 'the ticket comment is an any channels internal comment' do
          before { Arturo.enable_feature!(:channels_allow_any_channels_internal_notes) }

          let(:any_channels_params) do
            {
              comment_text: 'any channels abc',
              via_id: ViaType.ANY_CHANNEL,
              user: {
                id: '153594297',
                id_str: '153594297',
                name: 'john doe',
                screen_name: 'john doe',
                photo_url: 'http://url.com/photo.jpg'
              },
              nice_id: 800,
              source_id: 12345,
              attachments: [tokens(:minimum_upload_token).value],
              internal_note: true
            }
          end

          it 'creates ticket and private comment' do
            converter = ::Zendesk::Channels::TicketConverter.new(@account, any_channels_params)
            assert_difference('Ticket.count(:all)', 1) do
              assert_difference('Comment.count(:all)', 1) do
                Comment.any_instance.expects(:channel_source_id=).with(@params[:source_id])
                ticket, comment = converter.convert
                assert_equal 'any channels abc', ticket.subject
                assert_equal 'any channels abc', ticket.description
                assert_equal 'any channels abc', comment.body
                assert_equal '<div class="zd-comment" dir="auto"><p>any channels abc</p></div>', comment.html_body
                assert_equal ViaType.ANY_CHANNEL, ticket.via_id
                assert_equal @params[:source_id], ticket.via_reference_id
                assert_equal ViaType.ANY_CHANNEL, comment.via_id
                assert_equal ViaType.ANY_CHANNEL, comment.audit.via_id
                assert_equal 800, ticket.nice_id
                assert_equal 2, comment.attachments.length
                refute comment.is_public
              end
            end
          end
        end
      end

      # See https://zendesk.atlassian.net/browse/CT-1647
      describe 'when the author is an agent' do
        before do
          UserTwitterIdentity.create(account: @account,
                                     user: users(:minimum_agent),
                                     value: @params[:user][:id])
        end

        it 'creates ticket and comment' do
          converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
          assert_difference('Ticket.count(:all)', 1) do
            assert_difference('Comment.count(:all)', 1) do
              ticket, comment = converter.convert
              assert_equal 'abc', ticket.subject
              assert_equal 'abc', ticket.description
              assert_equal ViaType.TWITTER, ticket.via_id
              assert_equal @params[:source_id], ticket.via_reference_id
              assert_equal ViaType.TWITTER, comment.via_id
              assert_equal ViaType.TWITTER, comment.audit.via_id
              assert_equal 800, ticket.nice_id
            end
          end
        end
      end

      describe 'with selected params' do
        before do
          @params[:selected_params] = {}
        end

        describe 'with additional tags' do
          before do
            @params[:selected_params][:additional_tags] = 'tag1,tag2'
          end

          it 'creates ticket with additional tags' do
            converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
            assert_difference('Ticket.count(:all)', 1) do
              assert_difference('Comment.count(:all)', 1) do
                ticket, = converter.convert
                assert_equal 'tag1 tag2', ticket.current_tags
              end
            end
          end
        end

        describe 'with valid params' do
          before do
            @params[:selected_params][:subject] = 'subject'
            @params[:selected_params][:comment] = { value: 'comment', channel_back: true }
          end

          it 'creates ticket and comment' do
            converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
            assert_difference('Ticket.count(:all)', 1) do
              assert_difference('Comment.count(:all)', 1) do
                ticket, comment = converter.convert
                assert_equal 'subject', ticket.subject
                assert_equal 'comment', ticket.description
                assert_equal 'comment', comment.to_s
                assert_equal ViaType.TWITTER, ticket.via_id
                assert_equal @params[:source_id], ticket.via_reference_id
                assert_equal ViaType.TWITTER, comment.via_id
                assert_equal ViaType.TWITTER, comment.audit.via_id
              end
            end
          end
        end

        describe 'when there is a blank subject' do
          before do
            @params[:selected_params][:subject] = ''
            @params[:selected_params][:comment] = { value: 'comment', channel_back: true }
          end

          it 'creates ticket and comment' do
            converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
            assert_difference('Ticket.count(:all)', 1) do
              assert_difference('Comment.count(:all)', 1) do
                ticket, comment = converter.convert
                assert_equal 'abc', ticket.subject
                assert_equal 'comment', ticket.description
                assert_equal ViaType.TWITTER, ticket.via_id
                assert_equal @params[:source_id], ticket.via_reference_id
                assert_equal ViaType.TWITTER, comment.via_id
                assert_equal ViaType.TWITTER, comment.audit.via_id
              end
            end
          end
        end

        describe 'when there is a blank comment' do
          before do
            @params[:selected_params][:subject] = 'subject'
            @params[:selected_params][:comment] = { value: '', channel_back: true }
          end

          it 'creates ticket and comment' do
            converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
            assert_difference('Ticket.count(:all)', 1) do
              assert_difference('Comment.count(:all)', 1) do
                ticket, comment = converter.convert
                assert_equal 'subject', ticket.subject
                assert_equal 'abc', ticket.description
                assert_equal ViaType.TWITTER, ticket.via_id
                assert_equal @params[:source_id], ticket.via_reference_id
                assert_equal ViaType.TWITTER, comment.via_id
                assert_equal ViaType.TWITTER, comment.audit.via_id
              end
            end
          end
        end
      end

      describe 'when there is a ticket id' do
        describe 'when the ticket is deleted' do
          it 'creates a new ticket' do
            converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
            ticket = Ticket.find(converter.convert[0].id)
            ticket.status_id = StatusType.DELETED
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.save!

            @params[:ticket_id] = ticket.id
            @params[:nice_id] = 801
            converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)

            assert_difference('Ticket.count(:all)', 1) do
              assert_difference('Comment.count(:all)', 1) do
                ticket, _comment = converter.convert
                assert_equal 'abc', ticket.subject
                assert_equal 'abc', ticket.description
                assert_equal ViaType.TWITTER, ticket.via_id
                assert_equal @params[:source_id], ticket.via_reference_id
              end
            end
          end
        end

        describe 'when there is an existing ticket' do
          describe 'when the ticket is closed' do
            before do
              converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
              @ticket = Ticket.find(converter.convert[0].id)
              @ticket.status_id = StatusType.CLOSED
              @ticket.will_be_saved_by(users(:minimum_agent))
              @ticket.save!

              @params[:ticket_id] = @ticket.id
              @params[:nice_id] = 801
            end

            it 'creates a new ticket which is a followup to the closed ticket' do
              converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)

              assert_difference('Ticket.count(:all)') do
                assert_difference('Comment.count(:all)', 1) do
                  new_ticket, new_comment = converter.convert
                  assert_equal 'abc', new_ticket.subject
                  assert_equal 'abc', new_ticket.description
                  assert_equal 'abc', new_comment.body
                  assert_equal '<div class="zd-comment" dir="auto"><p>abc</p></div>', new_comment.html_body
                  assert_equal ViaType.TWITTER, new_ticket.via_id
                  assert_equal @ticket, new_ticket.followup_source
                end
              end
            end

            describe 'when there is html_body in the parameters' do
              before do
                setup_html_body
              end

              it 'creates a new ticket which is a followup to the closed ticket' do
                converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
                _new_ticket, new_comment = converter.convert
                assert_equal 'rich_content!', new_comment.body
                assert_equal '<div class="zd-comment" dir="auto"><div>rich_content!</div></div>', new_comment.html_body
              end
            end

            describe 'when parameters disallow followup tickets' do
              it 'does not create a followup ticket' do
                @params[:create_followup_tickets] = false
                converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)

                assert_difference('Ticket.count(:all)') do
                  assert_difference('Comment.count(:all)', 1) do
                    new_ticket, = converter.convert
                    assert_nil new_ticket.followup_source
                  end
                end
              end
            end
          end

          describe 'creates comment' do
            before do
              first_converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
              @params[:ticket_id] = first_converter.convert[0].id
              @params[:attachments] = [tokens(:other_minimum_upload_token).value]
              @converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
            end

            it 'creates comment but not ticket' do
              refute_difference('Ticket.count(:all)') do
                assert_difference('Comment.count(:all)', 1) do
                  ticket, comment = @converter.convert
                  assert_equal ViaType.TWITTER, comment.via_id
                  assert_equal 'abc', comment.body
                  assert_equal '<div class="zd-comment" dir="auto"><p>abc</p></div>', comment.html_body
                  assert_equal ticket, comment.ticket
                  assert comment.is_public
                  assert_equal @params[:source_id], comment.channel_source_id
                  assert_equal 1, comment.attachments.length
                end
              end
            end

            describe 'rich content comment' do
              before do
                setup_html_body
              end

              it 'creates comment with html_body' do
                _ticket, comment = @converter.convert
                assert_equal 'rich_content!', comment.body
                assert_equal '<div class="zd-comment" dir="auto"><div>rich_content!</div></div>', comment.html_body
              end
            end

            describe 'when it is an internal note' do
              before do
                Arturo.enable_feature!(:channels_allow_any_channels_internal_notes)
                @params[:internal_note] = true
              end

              it 'creates comment but not ticket' do
                refute_difference('Ticket.count(:all)') do
                  assert_difference('Comment.count(:all)', 1) do
                    ticket, comment = @converter.convert
                    assert_equal ViaType.TWITTER, comment.via_id
                    assert_equal 'abc', comment.body
                    assert_equal ticket, comment.ticket
                    refute comment.is_public
                    assert_equal @params[:source_id], comment.channel_source_id
                    assert_equal 1, comment.attachments.length
                  end
                end
              end
            end
          end

          describe 'when comment author' do
            before do
              converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
              @ticket, _comment = converter.convert
              @new_params = {
                ticket_id: @ticket.id,
                comment_text: 'def',
                via_id: ViaType.TWITTER,
                type: 'twitter',
                user: {
                  id: '110542029',
                  id_str: '110542029',
                  name: 'jane doe',
                  screen_name: 'jane doe',
                  photo_url: 'http://url.com/photo.jpg'
                }
              }
              stub_request(:get, 'https://api.twitter.com/1.1/users/show.json?user_id=110542029').
                to_return(status: 200, body: read_test_file('twitter_users_show_110542029_1.1.json'))
            end

            describe 'when comment author is not ticket requester' do
              before do
                @user = ::Channels::UserMapper.new(@account, Hashie::Mash.new(@new_params)).find_or_create_user
              end

              describe 'when comment author is CCd on ticket' do
                before do
                  @ticket.collaborations.build(user: @user)
                  @ticket.will_be_saved_by(@user)
                  @ticket.save!
                  @original_collaborators = @ticket.collaborators.dup
                  ::Zendesk::Channels::TicketConverter.new(@account, @new_params).convert
                end

                it 'does not edit ticket collaborators' do
                  assert_equal @original_collaborators, @ticket.reload.collaborators
                end
              end

              describe 'comment author is not CCd on ticket' do
                it 'does not edit ticket collaborators' do
                  @original_collaborators = @ticket.collaborators.dup
                  assert_equal @original_collaborators, @ticket.reload.collaborators
                end
              end

              describe 'when the comment author is an agent' do
                before do
                  monitored_twitter_handles(:minimum_monitored_twitter_handle_1).update_column(:allow_reply, true)
                  event_decorations(:minimum).update_column(:ticket_id, @ticket.id)
                  @user = ::Channels::UserMapper.new(@account, Hashie::Mash.new(@new_params)).find_or_create_user
                  @user.email = 'abc@def.com'
                  @user.roles = Role::AGENT.id
                  @user.save!
                  @user.reload
                end

                it 'should not tweet back' do
                  assert_no_difference('ChannelBackEvent.count(:all)') do
                    Ticket.any_instance.expects(:validate_and_create_channel_back_event).never
                    ::Zendesk::Channels::TicketConverter.new(@account, @new_params).convert
                  end
                end
              end
            end

            describe 'when comment author is ticket requester' do
              before do
                user = @ticket.requester
                user_identity = UserTwitterIdentity.new(user: user,
                                                        value: @new_params[:user][:id],
                                                        is_verified: true)
                user.identities << user_identity
                user.save!
                @original_collaborators = @ticket.collaborators.dup
                ::Zendesk::Channels::TicketConverter.new(@account, @new_params).convert
              end

              it 'does not edit ticket collaborators' do
                assert_equal @original_collaborators, @ticket.reload.collaborators
              end
            end
          end
        end
      end
    end

    describe '#author' do
      it 'initializes and caches the author' do
        converter = ::Zendesk::Channels::TicketConverter.new(@account, @params)
        assert_difference('User.count(:all)', 1) do
          assert converter.author
        end
        assert_no_difference('User.count(:all)', 1) do
          assert converter.author
        end
      end
    end

    describe '#channel' do
      {
        ViaType.TWITTER           => 'twitter',
        ViaType.TWITTER_DM        => 'twitter',
        ViaType.TWITTER_FAVORITE  => 'twitter',
        ViaType.FACEBOOK_POST     => 'facebook',
        ViaType.FACEBOOK_MESSAGE  => 'facebook',
        ViaType.ANY_CHANNEL       => 'any_channel'
      }.each do |via_type, expected|
        describe "via_type is #{via_type}" do
          it "returns #{expected}" do
            @params[:via_id] = via_type
            assert_equal expected, ::Zendesk::Channels::TicketConverter.new(@account, @params).send(:channel)
          end
        end
      end
    end
  end
end
