require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Channels::ErrorAuditor do
  fixtures :events, :tickets

  before do
    @ticket  = tickets(:minimum_1)
    @comment = events(:create_comment_for_minimum_ticket_1)
    @audit = @comment.audit
  end

  describe '#run' do
    let(:permanent) { false }
    let(:error_params) do
      { event_id: @comment.id, message: 'Error message', permanent: permanent, flag: flag }
    end

    describe 'when it is not a permanent error' do
      let(:flag) { nil }

      it 'adds an error event with the given message' do
        @comment.ticket.expects(:deactivate_event_decoration).never
        assert_difference('Error.count(:all)', 1) do
          ::Zendesk::Channels::ErrorAuditor.new(error_params).run
        end

        @error = Error.last
        assert_equal @comment.ticket, @error.ticket
        assert_equal 'Error message', @error.value
        refute @ticket.comments.first.event_decoration.data.inactive
      end
    end

    describe 'when it is a permanent error' do
      let(:permanent) { true }
      let(:flag) { nil }

      it 'adds an error event with the given message and deactivates the event decoration' do
        assert_difference('Error.count(:all)', 1) do
          ::Zendesk::Channels::ErrorAuditor.new(error_params).run
        end

        @error = Error.last
        assert_equal @comment.ticket, @error.ticket
        assert_equal 'Error message', @error.value
        assert_equal @comment, @comment.ticket.comments.first
        assert @ticket.comments.first.event_decoration.data.inactive
      end
    end

    describe 'when a flag is not present' do
      let(:flag) { nil }

      it 'does not add a flag to the audit' do
        ::Zendesk::Channels::ErrorAuditor.new(error_params).run
        assert_nil @audit.reload.value_previous
      end
    end

    describe 'when a invalid flag is present' do
      let(:flag) { -1 }

      it 'does not add a flag to the audit' do
        ::Zendesk::Channels::ErrorAuditor.new(error_params).run
        assert_nil @audit.reload.value_previous
      end
    end

    {
      EventFlagType.FACEBOOK_CLIENT_ERROR_200 => Channels::Constants::FACEBOOK_ERROR_FLAG_TYPE,
      Zendesk::Types::AttachmentErrorType.UPLOAD_HTTP_ERROR => Channels::Constants::ATTACHMENT_ERROR_FLAG_TYPE
    }.each_pair do |flag, type|
      describe "when a #{type} error flag is present" do
        let(:flag) { flag }
        let(:flag_options) do
          { 'type' => type }
        end
        let(:error_params) do
          {
            event_id: @comment.id,
            message: 'Error message',
            permanent: false,
            flag: flag,
            flag_options: flag_options
          }
        end

        it 'adds a flag to the audit' do
          ::Zendesk::Channels::ErrorAuditor.new(error_params).run
          assert JSON.parse(@audit.reload.value_previous)['flags'].member?(flag)
        end
      end
    end
  end
end
