require_relative '../../../support/test_helper'

SingleCov.covered!

describe ::Zendesk::Channels::Utilities do
  fixtures :accounts

  describe '.in_channels_service?' do
    it 'returns false' do
      refute ::Zendesk::Channels::Utilities.in_channels_service?
    end
  end

  describe '.run_channels_job?' do
    describe 'In classic codebase' do
      before do
        ::Zendesk::Channels::Utilities.stubs(:in_channels_service?).returns(false)
      end

      describe 'Arturo flag is not set' do
        before do
          Arturo.disable_feature!(:channels_jobs_run_in_channels_service)
        end

        it 'returns true' do
          assert ::Zendesk::Channels::Utilities.run_channels_job?(accounts(:minimum))
        end
      end

      describe 'Arturo flag is set' do
        before do
          Arturo.enable_feature!(:channels_jobs_run_in_channels_service)
        end

        it 'returns false' do
          refute ::Zendesk::Channels::Utilities.run_channels_job?(accounts(:minimum))
        end
      end
    end

    describe 'Not in classic codebase' do
      before do
        ::Zendesk::Channels::Utilities.stubs(:in_channels_service?).returns(true)
      end

      describe 'Arturo flag is not set' do
        before do
          Arturo.disable_feature!(:channels_jobs_run_in_channels_service)
        end

        it 'returns false' do
          refute ::Zendesk::Channels::Utilities.run_channels_job?(accounts(:minimum))
        end
      end

      describe 'Arturo flag is set' do
        before do
          Arturo.enable_feature!(:channels_jobs_run_in_channels_service)
        end

        it 'returns true' do
          assert ::Zendesk::Channels::Utilities.run_channels_job?(accounts(:minimum))
        end
      end
    end
  end
end
