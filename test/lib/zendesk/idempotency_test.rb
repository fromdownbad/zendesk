require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::Idempotency do
  include Zendesk::Idempotency

  let(:default_expiry) { 15.minutes }
  let(:store_backend) { mock('store backend') }
  let(:exception_logger) { mock('exception logger') }
  let(:proxy) { self.class.idempotent_proxy(store: store_backend) }
  let(:response_store) { mock('response store') }
  let(:error_log) { mock('error log') }

  describe 'for a controller action' do
    let(:idempotency_cache_prefix) { 'prefix' }
    let(:action_name) { 'create' }
    let(:params) { { foo: 'bar' } }
    let(:path) { '/api/v2/tickets' }
    let(:response) { mock('action response').tap { |response| response.stubs(headers: {}) } }
    let(:action) { mock('action') }

    before do
      Zendesk::Idempotency::ResponseStore.
        expects(:new).
        with(backend: store_backend, logger: Zendesk::Idempotency::ErrorLog, expiry: default_expiry).
        returns response_store
    end

    describe 'for idempotent requests' do
      let(:request) do
        mock('request').tap { |request| request.stubs path: path, headers: { 'Idempotency-Key' => 123 }, env: {} }
      end

      it 'stores new action response if not previously stored' do
        response_store.
          expects(:get).
          with('id_res:prefix:create:/api/v2/tickets:40bd001563085fc35165329ea1ff5c5ecbdbbeef').
          returns nil
        action.expects(:call).once.with # no arguments
        response_store.
          expects(:set).
          with(
            'id_res:prefix:create:/api/v2/tickets:40bd001563085fc35165329ea1ff5c5ecbdbbeef',
            response,
            signature: '7a38bf81f383f69433ad6e900d35b3e2385593f76a7b7ab5d4355b8ba41ee24b'
          )

        instance_exec self, action, &proxy

        assert_equal 'miss', response.headers['X-Idempotency-Lookup']
      end

      it 'skips action if response was previously stored' do
        response_store.
          expects(:get).
          with('id_res:prefix:create:/api/v2/tickets:40bd001563085fc35165329ea1ff5c5ecbdbbeef').
          returns ['7a38bf81f383f69433ad6e900d35b3e2385593f76a7b7ab5d4355b8ba41ee24b', status = mock, headers = mock, body = mock]
        response_store.
          expects(:touch).
          with('id_res:prefix:create:/api/v2/tickets:40bd001563085fc35165329ea1ff5c5ecbdbbeef')
        # test the response assignment explicitly
        response.expects(:status=).once.with status
        response.expects(:headers=).once.with headers
        response.expects(:body=).once.with body

        instance_exec self, action, &proxy

        assert(request.env['zendesk.idempotency_hit'])
        assert_equal 'hit', response.headers['X-Idempotency-Lookup']
      end

      it 'checks request parameters digest' do
        response_store.
          expects(:get).
          with('id_res:prefix:create:/api/v2/tickets:40bd001563085fc35165329ea1ff5c5ecbdbbeef').
          returns ['foobar', mock, mock, mock]

        assert_raise Zendesk::Idempotency::ParameterMismatchError do
          instance_exec self, action, &proxy
        end
      end

      it 'does not store action response if an error was raised' do
        response_store.
          expects(:get).
          with('id_res:prefix:create:/api/v2/tickets:40bd001563085fc35165329ea1ff5c5ecbdbbeef').
          returns nil
        action.expects(:call).once.with.raises 'the ceiling'

        assert_raise RuntimeError do
          instance_exec self, action, &proxy
        end
      end
    end

    describe 'for regular requests' do
      let(:request) { mock('request').tap { |request| request.stubs path: path, headers: {} } }

      it 'skips idempotency functionality' do
        action.expects(:call).once.with # no arguments
        instance_exec self, action, &proxy

        refute_includes response.headers, 'X-Idempotency-Lookup'
      end
    end
  end

  describe 'for a controller class' do
    it 'laughs in the face of whining consul' do
      Zendesk::RedisStore.stubs(:client).with.raises 'the ceiling'
      Class.new do
        include AbstractController::Callbacks
        include Zendesk::Idempotency

        around_action idempotent_proxy, only: :foo
      end.new
    end
  end
end
