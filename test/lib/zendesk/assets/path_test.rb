require_relative '../../../support/test_helper'

SingleCov.covered!

describe 'Path' do
  def create_asset(asset_id)
    Photo.new.tap do |asset|
      asset.id = asset_id
      asset.filename = "ohmy.jpg"
    end
  end

  describe "assets uris" do
    describe "ids with fewer than 8 digits" do
      let(:asset_id) { 42 }
      let(:path) do
        asset = create_asset(asset_id)
        Zendesk::Assets::Path.new(asset.path_for_url)
      end

      it "has the correct asset_id" do
        assert_equal asset_id, path.asset_id
      end

      it "has the correct uri" do
        assert_equal "public/system/photos/42/ohmy.jpg", path.uri
      end

      it "has the correct object_type" do
        assert_equal "photos", path.object_type
      end
    end

    describe "ids with fewer than 12 digits" do
      let(:asset_id) { 100_000_042 }
      let(:path) do
        asset = create_asset(asset_id)
        Zendesk::Assets::Path.new(asset.path_for_url)
      end

      it "has the correct asset_id" do
        assert_equal asset_id, path.asset_id
      end

      it "has the correct uri" do
        assert_equal "public/system/photos/100000042/ohmy.jpg", path.uri
      end

      it "has the correct object_type" do
        assert_equal "photos", path.object_type
      end
    end

    describe "ids that have not been partitioned" do
      let(:asset_id) { 111_222_333_444_555 }
      let(:path) do
        asset = create_asset(asset_id)
        Zendesk::Assets::Path.new(asset.path_for_url(nil, raw_id_in_path: true))
      end

      it "has the correct asset_id" do
        assert_equal asset_id, path.asset_id
      end

      it "has the correct uri" do
        assert_equal "public/system/photos/111222333444555/ohmy.jpg", path.uri
      end

      it "has the correct object_type" do
        assert_equal "photos", path.object_type
      end
    end
  end
end
