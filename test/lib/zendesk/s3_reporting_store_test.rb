require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::S3ReportingStore do
  let(:store) { Zendesk::S3ReportingStore.new('foo_report') }

  describe "#fetch" do
    it "reads existing reports" do
      Aws::S3::Resource.any_instance.expects(:bucket).returns(stub(object: s3_object))

      object = store.fetch

      assert_equal 'presigned_url', object.url
      assert_equal 'a_content_type', object.content_type
      assert_equal 'some_content_length', object.content_length
    end

    it "returns nil for missing reports" do
      Aws::S3::Resource.any_instance.expects(:bucket).raises(Aws::S3::Errors::NotFound.new(1, "2"))
      assert_nil store.fetch
    end
  end

  describe "#store!" do
    it "stores" do
      Aws::S3::Client.any_instance.expects(:put_object).returns('foo')
      store.store!('foo')
    end

    it "creates a missing bucket" do
      calls = 0
      Aws::S3::Client.any_instance.expects(:put_object).with do
        if (calls += 1) == 1
          raise Aws::S3::Errors::NoSuchBucket.new('xyz', 'zzz')
        else
          true
        end
      end
      Aws::S3::Client.any_instance.expects(:create_bucket)
      store.store!('foo')
    end
  end

  private

  def s3_object
    stub(
      url: 'a_url',
      url_for: 'a_url',
      content_type: 'a_content_type',
      content_length: 'some_content_length',
      presigned_url: 'presigned_url'
    )
  end
end
