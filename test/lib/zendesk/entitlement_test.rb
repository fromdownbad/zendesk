require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::Entitlement' do
  it "returns the correct string representation of user_seats role" do
    assert_equal Zendesk::Entitlement::ROLES[:support][:agent], "agent"
    assert_equal Zendesk::Entitlement::ROLES[:support][:admin], "admin"
    assert_equal Zendesk::Entitlement::ROLES[:support][:light_agent], "light_agent"
    assert_equal Zendesk::Entitlement::ROLES[:support][:chat_only_agent], "chat_only_agent"
    assert_equal Zendesk::Entitlement::ROLES[:support][:custom], "custom"

    assert_equal Zendesk::Entitlement::ROLES[:guide][:agent], "agent"
    assert_equal Zendesk::Entitlement::ROLES[:guide][:admin], "admin"
    assert_equal Zendesk::Entitlement::ROLES[:guide][:viewer], "viewer"
    assert_equal Zendesk::Entitlement::ROLES[:guide][:light_agent], "light_agent"

    assert_equal Zendesk::Entitlement::ROLES[:chat][:agent], "agent"
    assert_equal Zendesk::Entitlement::ROLES[:chat][:admin], "admin"

    assert_equal Zendesk::Entitlement::ROLES[:explore][:viewer], "viewer"
    assert_equal Zendesk::Entitlement::ROLES[:explore][:studio], "studio"
    assert_equal Zendesk::Entitlement::ROLES[:explore][:admin], "admin"
  end

  it "returns the correct string representation of user_seat types" do
    assert_equal Zendesk::Entitlement::TYPES[:support], "support"
    assert_equal Zendesk::Entitlement::TYPES[:chat], "chat"
    assert_equal Zendesk::Entitlement::TYPES[:explore], "explore"
    assert_equal Zendesk::Entitlement::TYPES[:voice], "voice"
    assert_equal Zendesk::Entitlement::TYPES[:connect], "connect"
  end
end
