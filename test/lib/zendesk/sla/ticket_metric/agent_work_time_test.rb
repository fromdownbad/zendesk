require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Sla::TicketMetric::AgentWorkTime do
  let(:sla_metric) { Zendesk::Sla::TicketMetric::AgentWorkTime }

  describe '.id' do
    it 'returns the SLA metric ID' do
      assert_equal 3, sla_metric.id
    end
  end

  describe '.name' do
    it 'returns the SLA metric name' do
      assert_equal :agent_work_time, sla_metric.name
    end
  end

  describe '.metric' do
    it 'returns the underlying metric name' do
      assert_equal :agent_work_time, sla_metric.metric
    end
  end

  describe '.valid_instance?' do
    describe 'when the instance is valid' do
      it 'returns true' do
        assert(
          (0..100).all? { |instance| sla_metric.valid_instance?(instance) }
        )
      end
    end

    describe 'when the instance is not valid' do
      it 'returns false' do
        refute sla_metric.valid_instance?(-1)
      end
    end
  end
end
