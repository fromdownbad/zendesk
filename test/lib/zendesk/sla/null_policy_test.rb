require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Sla::NullPolicy do
  let(:null_policy) { Zendesk::Sla::NullPolicy }

  describe '.id' do
    it 'returns the highest positive bigint value' do
      assert_equal 256**8 / 2 - 1, null_policy.id
    end
  end

  describe '.title' do
    it 'returns nil' do
      assert_nil null_policy.title
    end
  end

  describe '.policy_metrics' do
    describe '.where' do
      it 'returns itself' do
        assert_equal(
          null_policy.policy_metrics,
          null_policy.policy_metrics.where(metric_id: 1)
        )
      end
    end

    describe '.with_priority' do
      it 'returns itself' do
        assert_equal(
          null_policy.policy_metrics,
          null_policy.policy_metrics.with_priority(1)
        )
      end
    end

    describe '.first' do
      it 'returns nil' do
        assert_nil null_policy.policy_metrics.first
      end
    end
  end

  describe '.priority_metric_map' do
    it 'returns an empty hash' do
      assert_equal({}, null_policy.priority_metric_map)
    end
  end
end
