require_relative '../../../support/test_helper'
require_relative '../../../support/ticket_metric_helper'

SingleCov.covered!

describe Zendesk::Sla::MetricSelection do
  include TicketMetricHelper

  fixtures :accounts,
    :tickets,
    :events

  let(:ticket)  { tickets(:minimum_1) }
  let(:account) { ticket.account }
  let(:policy)  { new_sla_policy }

  let(:metric_selection) { Zendesk::Sla::MetricSelection.new(ticket) }

  before do
    new_sla_policy_metric(
      account:     account,
      policy:      policy,
      metric_id:   Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
      priority_id: ticket.priority_id,
      target:      50
    )
    new_sla_policy_metric(
      account:     account,
      policy:      policy,
      metric_id:   Zendesk::Sla::TicketMetric::AgentWorkTime.id,
      priority_id: ticket.priority_id,
      target:      100
    )
    new_sla_policy_metric(
      account:     account,
      policy:      policy,
      metric_id:   Zendesk::Sla::TicketMetric::FirstReplyTime.id,
      priority_id: ticket.priority_id,
      target:      100
    )
    new_sla_policy_metric(
      account:     account,
      policy:      policy,
      metric_id:   Zendesk::Sla::TicketMetric::FirstReplyTime.id,
      priority_id: ticket.priority_id.succ,
      target:      100
    )

    ticket.create_sla_ticket_policy(
      account: account,
      policy:  policy
    ).tap do |ticket_policy|
      policy.policy_metrics.each do |policy_metric|
        ticket_policy.statuses.create(
          account:       account,
          ticket_policy: ticket_policy,
          policy_metric: policy_metric,
          paused:        false,
          breach_at:     ticket.created_at + policy_metric.target,
          fulfilled_at:  nil
        )
      end
    end

    Sla::TicketPolicy.
      any_instance.
      stubs(:current_statuses).
      returns(
        ticket.sla_ticket_policy.statuses.select do |status|
          status.policy_metric.priority_id == ticket.priority_id
        end
      )
  end

  describe '#all' do
    it 'returns the correct number of statuses' do
      assert_equal 3, metric_selection.all.count
    end

    it 'sorts the metrics' do
      assert_equal(
        %i[requester_wait_time first_reply_time agent_work_time],
        metric_selection.all.map(&:name)
      )
    end

    it 'does not include first_reply_time status by default' do
      ticket.sla_ticket_policy.expects(current_statuses: []).with(false)
      metric_selection.all
    end

    it 'passes the first_reply_time argument to current_statuses' do
      ticket.sla_ticket_policy.expects(current_statuses: []).with(true)
      metric_selection.all(true)
    end
  end

  describe '#most_relevant' do
    it 'returns the most relevant metric' do
      assert_equal :requester_wait_time, metric_selection.most_relevant.name
    end

    describe 'when the most relevant metric is fulfilled' do
      before do
        metric_selection.most_relevant.update_attributes(
          fulfilled_at: ticket.created_at
        )
      end

      it 'excludes that metric' do
        assert_equal :first_reply_time, metric_selection.most_relevant.name
      end
    end

    describe 'when there are no unfulfilled metrics' do
      before do
        Sla::TicketPolicy.any_instance.stubs(:current_statuses).returns([])
      end

      it 'returns the null metric' do
        assert_equal Zendesk::Sla::NullMetric, metric_selection.most_relevant
      end
    end
  end

  describe 'when a ticket is not associated with a policy' do
    before do
      ticket.sla_ticket_policy.destroy

      ticket.reload
    end

    it 'returns the null metric' do
      assert_equal Zendesk::Sla::NullMetric, metric_selection.most_relevant
    end
  end

  describe 'when a ticket is associated with the null policy' do
    before do
      ticket.sla_ticket_policy.destroy

      ticket.build_sla_ticket_policy(account: account).tap do |ticket_policy|
        ticket_policy.sla_policy_id = Zendesk::Sla::NullPolicy.id

        ticket_policy.save!
      end
    end

    it 'returns the null metric' do
      assert_equal Zendesk::Sla::NullMetric, metric_selection.most_relevant
    end
  end
end
