require_relative '../../../support/test_helper'
require_relative '../../../support/ticket_metric_helper'

SingleCov.covered!

describe Zendesk::Sla::PolicyAssignment do
  include TicketMetricHelper

  fixtures :accounts,
    :tickets,
    :users

  let(:account) { accounts(:minimum) }
  let(:agent)   { users(:minimum_agent) }

  let(:policy_assignment) { Zendesk::Sla::PolicyAssignment.new(ticket) }

  let(:ticket) do
    tickets(:minimum_1).tap do |ticket|
      ticket.current_tags = 'match_sla'

      ticket.will_be_saved_by(agent)
    end
  end

  before do
    Account.any_instance.stubs(has_service_level_agreements?: true)
  end

  describe '.persist' do
    describe 'when a new SLA policy has been assigned' do
      let(:policy) { new_sla_policy }

      before do
        ticket.new_sla_policy_id = policy.id

        Zendesk::Sla::PolicyAssignment.persist(ticket)
      end

      it 'persists the assigned policy' do
        assert(
          Sla::TicketPolicy.where(
            account_id:    account.id,
            ticket_id:     ticket.id,
            sla_policy_id: policy.id
          ).one?
        )
      end
    end

    describe 'when a new SLA policy has not been assigned' do
      before do
        ticket.new_sla_policy_id = nil

        Zendesk::Sla::PolicyAssignment.persist(ticket)
      end

      it 'does not persist a policy' do
        assert(
          Sla::TicketPolicy.where(
            account_id: account.id,
            ticket_id:  ticket.id
          ).none?
        )
      end
    end
  end

  describe '#assign' do
    let!(:non_matching_policy) { new_sla_policy(tags: 'no_match') }

    let(:span) { stub(:span) }

    describe 'when the policy has changed' do
      describe 'and there is a matching policy' do
        let!(:matching_policy) { new_sla_policy(position: 2) }

        describe 'and there was not a previously assigned policy' do
          before { policy_assignment.assign }

          it 'assigns the policy' do
            assert_equal matching_policy.id, ticket.new_sla_policy_id
          end

          it 'creates an `sla_policy` audit event' do
            assert(
              ticket.
              audit.
              events.any? { |event| event.value_reference == 'sla_policy' }
            )
          end
        end

        describe 'and there was a previously assigned policy' do
          before do
            ticket.create_sla_ticket_policy(
              account: account,
              policy:  non_matching_policy
            )

            policy_assignment.assign
          end

          it 'assigns the new policy' do
            assert_equal matching_policy.id, ticket.new_sla_policy_id
          end

          it 'creates an `sla_policy` audit event' do
            assert(
              ticket.
              audit.
              events.any? { |event| event.value_reference == 'sla_policy' }
            )
          end
        end

        describe 'and that policy is deleted' do
          before do
            matching_policy.tap(&:soft_delete)

            policy_assignment.assign
          end

          it 'does not assign the deleted policy' do
            assert_nil ticket.new_sla_policy_id
          end

          it 'does not create an `sla_policy` audit event' do
            refute(
              ticket.
              audit.
              events.any? { |event| event.value_reference == 'sla_policy' }
            )
          end
        end
      end

      describe 'and there is not a matching policy' do
        before { policy_assignment.assign }

        it 'does not assign a policy' do
          assert_nil ticket.new_sla_policy_id
        end

        it 'does not create an `sla_policy` audit event' do
          refute(
            ticket.
            audit.
            events.any? { |event| event.value_reference == 'sla_policy' }
          )
        end
      end
    end

    describe 'when the policy has not changed' do
      let!(:matching_policy) { new_sla_policy(position: 2) }

      before do
        ticket.create_sla_ticket_policy(
          account: account,
          policy:  matching_policy
        )

        policy_assignment.assign
      end

      it 'does not assign a policy' do
        assert_nil ticket.new_sla_policy_id
      end

      it 'does not create an `sla_policy` audit event' do
        refute(
          ticket.
          audit.
          events.any? { |event| event.value_reference == 'sla_policy' }
        )
      end
    end
  end
end
