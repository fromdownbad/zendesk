require_relative '../../../support/test_helper'
require_relative '../../../support/ticket_metric_helper'

SingleCov.covered!

describe Zendesk::Sla::MetricsReader do
  include TicketMetricHelper

  fixtures :accounts, :tickets, :users

  let(:account) { accounts(:minimum) }
  let(:end_user) { users(:minimum_end_user) }
  let(:agent)    { users(:minimum_agent) }
  let(:policy)  { new_sla_policy(tags: 'match_sla') }
  let(:ticket)  { tickets(:minimum_1) }
  let(:metrics_reader) { Zendesk::Sla::MetricsReader.new(ticket) }

  before do
    new_sla_policy_metric(
      policy:      policy,
      priority_id: PriorityType.NORMAL,
      metric_id:   Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
      target:      10
    )
    new_sla_policy_metric(
      policy:      policy,
      priority_id: PriorityType.NORMAL,
      metric_id:   Zendesk::Sla::TicketMetric::AgentWorkTime.id,
      target:      10
    )
    new_sla_policy_metric(
      policy:      policy,
      priority_id: PriorityType.NORMAL,
      metric_id:   Zendesk::Sla::TicketMetric::PeriodicUpdateTime.id,
      target:      10
    )
    new_sla_policy_metric(
      policy:      policy,
      priority_id: PriorityType.NORMAL,
      metric_id:   Zendesk::Sla::TicketMetric::PausableUpdateTime.id,
      target:      10
    )

    ticket.events.delete_all
    ticket.metric_events.delete_all

    Account.any_instance.stubs(has_service_level_agreements?: true)

    ticket.update_column(:created_at, Time.utc(2020))

    # Record the measure metric events
    measure_metrics(
      :agent_work_time,
      :pausable_update_time,
      :periodic_update_time,
      :reply_time,
      :requester_wait_time
    )

    tags_updated(after: 0.minutes, to: 'match_sla')
    status_updated(after: 0.minutes, to: StatusType.OPEN)
    comments_added([{after: 0.minutes, by: end_user}])
    priority_updated(after: 6.minutes, to: PriorityType.NORMAL)
    status_updated(after: 10.minutes, to: StatusType.PENDING)
    status_updated(after: 29.minutes, to: StatusType.OPEN)
    comments_added([{after: 40.minutes, by: agent}])
  end

  describe '.metric_statuses' do
    it 'returns statuses for only tracked metrics' do
      assert_equal(
        [
          :agent_work_time,
          :pausable_update_time,
          :periodic_update_time,
          :requester_wait_time
        ],
        metrics_reader.metric_statuses.map(&:name).sort
      )
    end

    describe 'when ticket has no policy' do
      before { ticket.stubs(:sla_ticket_policy).returns(nil) }

      it 'returns empty array' do
        assert_equal([], metrics_reader.metric_statuses)
      end
    end

    describe 'when also tracking `reply_time` metrics' do
      describe '`first_reply_time` and `next_reply_time`' do
        before do
          new_sla_policy_metric(
            policy:      policy,
            priority_id: PriorityType.NORMAL,
            metric_id:   Zendesk::Sla::TicketMetric::FirstReplyTime.id,
            target:      10
          )
          new_sla_policy_metric(
            policy:      policy,
            priority_id: PriorityType.NORMAL,
            metric_id:   Zendesk::Sla::TicketMetric::NextReplyTime.id,
            target:      10
          )
        end

        describe 'and the agent has replied once' do
          it 'returns statuses for tracked metrics excluding `next_reply_time`' do
            assert_equal(
              [
                :agent_work_time,
                :first_reply_time,
                :pausable_update_time,
                :periodic_update_time,
                :requester_wait_time
              ],
              metrics_reader.metric_statuses.map(&:name).sort
            )
          end

          describe 'and the end-user comments again' do
            before { comments_added([{after: 70.minutes, by: end_user}]) }

            it 'returns statuses for tracked metrics including `next_reply_time`' do
              assert_equal(
                [
                  :agent_work_time,
                  :first_reply_time,
                  :next_reply_time,
                  :pausable_update_time,
                  :periodic_update_time,
                  :requester_wait_time
                ],
                metrics_reader.metric_statuses.map(&:name).sort
              )
            end
          end
        end
      end

      describe '`next_reply_time` only' do
        before do
          new_sla_policy_metric(
            policy:      policy,
            priority_id: PriorityType.NORMAL,
            metric_id:   Zendesk::Sla::TicketMetric::NextReplyTime.id,
            target:      10
          )
        end

        describe 'and the agent has replied once' do
          it 'returns statuses for tracked metrics excluding `next_reply_time`' do
            assert_equal(
              [
                :agent_work_time,
                :pausable_update_time,
                :periodic_update_time,
                :requester_wait_time
              ],
              metrics_reader.metric_statuses.map(&:name).sort
            )
          end

          describe 'and the end-user comments again' do
            before { comments_added([{ after: 70.minutes, by: end_user }]) }

            it 'returns statuses for tracked metrics including `next_reply_time`' do
              assert_equal(
                [
                  :agent_work_time,
                  :next_reply_time,
                  :pausable_update_time,
                  :periodic_update_time,
                  :requester_wait_time
                ],
                metrics_reader.metric_statuses.map(&:name).sort
              )
            end
          end
        end
      end
    end
  end
end
