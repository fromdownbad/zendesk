require_relative '../../../support/test_helper'
require_relative '../../../support/ticket_metric_helper'

SingleCov.covered!

describe Zendesk::Sla::MetricStatus do
  include TicketMetricHelper

  fixtures :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }
  let(:metric)  { :reply_time }

  let(:policy) { new_sla_policy }

  let(:metric_status) { Zendesk::Sla::MetricStatus.new(ticket, metric) }

  before do
    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::AgentWorkTime.id,
      target:    10
    )
    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::FirstReplyTime.id,
      target:    10
    )
    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::NextReplyTime.id,
      target:    20
    )
    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::PausableUpdateTime.id,
      target:    10
    )
    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::PeriodicUpdateTime.id,
      target:    10
    )
    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
      target:    10
    )

    assign_policy

    ticket.metric_events.reply_time.activate.create(
      account: account,
      time:    ticket.created_at
    )
    TicketMetric::ApplySla.create(
      ticket:  ticket,
      account: ticket.account,
      metric:  'reply_time',
      time:    ticket.created_at
    )
    ticket.metric_events.reply_time.fulfill.create(
      account: account,
      time:    ticket.created_at + 10.minutes
    )
    ticket.metric_events.reply_time.activate.create(
      account:     account,
      instance_id: 2,
      time:        ticket.created_at + 20.minutes
    )
  end

  describe '.all' do
    before do
      TicketMetric::ApplySla.create(
        ticket:  ticket,
        account: ticket.account,
        metric:  'agent_work_time',
        time:    ticket.created_at
      )
      TicketMetric::ApplySla.create(
        ticket:  ticket,
        account: ticket.account,
        metric:  'pausable_update_time',
        time:    ticket.created_at
      )
      TicketMetric::ApplySla.create(
        ticket:  ticket,
        account: ticket.account,
        metric:  'periodic_update_time',
        time:    ticket.created_at
      )
      TicketMetric::ApplySla.create(
        ticket:      ticket,
        account:     ticket.account,
        metric:      'reply_time',
        instance_id: 2,
        time:        ticket.created_at + 20.minutes
      )
      TicketMetric::ApplySla.create(
        ticket:  ticket,
        account: ticket.account,
        metric:  'requester_wait_time',
        time:    ticket.created_at
      )
    end

    it 'returns statuses for the active, supported metrics' do
      assert_equal(
        %i[
          agent_work_time
          pausable_update_time
          periodic_update_time
          next_reply_time
          requester_wait_time
        ],
        Zendesk::Sla::MetricStatus.all(ticket).
          map(&:policy_metric).
          map(&:metric).
          map(&:name)
      )
    end
  end

  describe '#measured?' do
    describe 'when there is an assigned policy metric' do
      before do
        TicketMetric::ApplySla.create(
          ticket:      ticket,
          account:     ticket.account,
          metric:      'reply_time',
          instance_id: 2,
          time:        ticket.created_at + 20.minutes
        )
      end

      it 'returns true' do
        assert metric_status.measured?
      end
    end

    describe 'when there is not an assigned policy metric' do
      it 'returns false' do
        refute metric_status.measured?
      end
    end
  end

  describe '#fulfilled?' do
    describe "when the last transition is to the 'fulfilled' state" do
      before do
        ticket.metric_events.reply_time.fulfill.create(
          account:     account,
          instance_id: 2,
          time:        ticket.created_at + 30.minutes
        )
      end

      it 'returns true' do
        assert metric_status.fulfilled?
      end
    end

    describe "when the last transition is not to the 'fulfilled' state" do
      before do
        ticket.metric_events.reply_time.pause.create(
          account:     account,
          instance_id: 2,
          time:        ticket.created_at + 30.minutes
        )
      end

      it 'returns false' do
        refute metric_status.fulfilled?
      end
    end
  end

  describe '#paused?' do
    describe "when the last transition is to the 'paused' state" do
      before do
        ticket.metric_events.reply_time.pause.create(
          account:     account,
          instance_id: 2,
          time:        ticket.created_at + 30.minutes
        )
      end

      it 'returns true' do
        assert metric_status.paused?
      end
    end

    describe "when the last transition is not to the 'paused' state" do
      before do
        ticket.metric_events.reply_time.fulfill.create(
          account:     account,
          instance_id: 2,
          time:        ticket.created_at + 30.minutes
        )
      end

      it 'returns false' do
        refute metric_status.paused?
      end
    end
  end

  describe '#breached?' do
    describe 'when there is an active breach' do
      before do
        TicketMetric::ApplySla.create(
          ticket:      ticket,
          account:     ticket.account,
          metric:      'reply_time',
          instance_id: 2,
          time:        ticket.created_at + 20.minutes
        )
        ticket.metric_events.reply_time.breach.create(
          account:     account,
          instance_id: 2,
          time:        ticket.created_at + 40.minutes
        )
      end

      describe 'and it is before the current time' do
        before { Timecop.freeze(ticket.created_at + 50.minutes) }

        it 'returns true' do
          assert metric_status.breached?
        end
      end

      describe 'and it is after the current time' do
        before { Timecop.freeze(ticket.created_at + 30.minutes) }

        it 'returns false' do
          refute metric_status.breached?
        end
      end
    end

    describe 'when there is not an active breach' do
      it 'returns false' do
        refute metric_status.breached?
      end
    end
  end

  describe '#fulfilled_at' do
    describe "when the last transition is to the 'fulfilled' state" do
      let(:last_transition_time) { ticket.created_at + 30.minutes }

      before do
        ticket.metric_events.reply_time.fulfill.create(
          account:     account,
          instance_id: 2,
          time:        last_transition_time
        )
      end

      it 'returns the last transition time' do
        assert_equal last_transition_time, metric_status.fulfilled_at
      end
    end

    describe "when the last transition is not to the 'fulfilled' state" do
      before do
        ticket.metric_events.reply_time.pause.create(
          account:     account,
          instance_id: 2,
          time:        ticket.created_at + 30.minutes
        )
      end

      it 'returns nil' do
        assert_nil metric_status.fulfilled_at
      end
    end
  end

  describe '#breach_at' do
    describe 'when there is an active breach' do
      before do
        TicketMetric::ApplySla.create(
          ticket:      ticket,
          account:     ticket.account,
          metric:      'reply_time',
          instance_id: 2,
          time:        ticket.created_at + 20.minutes
        )
        ticket.metric_events.reply_time.breach.create(
          account:     account,
          instance_id: 2,
          time:        ticket.created_at + 40.minutes
        )
      end

      it 'returns the breach time' do
        assert_equal ticket.created_at + 40.minutes, metric_status.breach_at
      end
    end

    describe 'when there is not an active breach' do
      it 'returns nil' do
        assert_nil metric_status.breach_at
      end
    end
  end

  describe '#policy_metric' do
    before do
      TicketMetric::ApplySla.create(
        ticket:      ticket,
        account:     ticket.account,
        metric:      'reply_time',
        instance_id: 2,
        time:        ticket.created_at + 20.minutes
      )
    end

    it 'returns the policy metric for the most recent active SLA' do
      assert_equal :next_reply_time, metric_status.policy_metric.metric.name
    end
  end

  describe '#first_sla_application' do
    before do
      TicketMetric::ApplySla.create(
        ticket:      ticket,
        account:     ticket.account,
        metric:      'reply_time',
        instance_id: 2,
        time:        ticket.created_at + 20.minutes
      )
    end

    it 'returns the policy metric for the active SLA with instance ID of 1' do
      assert_equal :first_reply_time, metric_status.first_policy_metric.metric.name
    end
  end
end
