require_relative '../../../../support/test_helper'
require_relative '../../../../../lib/zendesk/sla/metrics_reader/metric_event_status'

SingleCov.covered!

describe Zendesk::Sla::MetricEventStatus do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:ticket_metric_events) { ticket.metric_events }
  let(:fulfill_event) do
    ticket_metric_events.requester_wait_time.fulfill.create(
      account: ticket.account,
      time: Time.new(2020)
    )
  end
  let(:pause_event) do
    ticket_metric_events.requester_wait_time.pause.create(
      account: ticket.account,
      time: Time.new(2020)
    )
  end
  let(:breach_event) do
    ticket_metric_events.requester_wait_time.breach.create(
      account: ticket.account,
      time: Time.new(2020)
    )
  end
  let(:event) do
    ticket_metric_events.agent_work_time.activate.create(
      account: ticket.account,
      time: Time.new(2020)
    )
  end

  before(:each) do
    @metric_event_status = Zendesk::Sla::MetricEventStatus.new(event)
  end

  after(:each) do
    ticket.metric_events.destroy_all
  end

  describe '.breach_at' do
    describe 'when metric event `type` is not `breach`' do
      it 'returns nil' do
        assert_nil(@metric_event_status.breach_at)
      end
    end

    describe 'when metric event `type` is `breach`' do
      let(:event) do
        ticket_metric_events.agent_work_time.breach.create(
          account: ticket.account,
          time:    ticket.updated_at - 1.minute
        )
      end

      it 'returns the breach time' do
        assert_equal(event.time, @metric_event_status.breach_at)
      end
    end
  end

  describe '.name' do
    describe 'when `reply_time` metric' do
      let(:event) do
        ticket_metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2020)
        )
      end

      describe 'and `instance_id` is 1' do
        it 'returns `first_reply_time`' do
          assert_equal(:first_reply_time, @metric_event_status.name)
        end
      end

      describe 'and `instance_id` is 2 or greater' do
        let(:event) do
          ticket_metric_events.reply_time.activate.create(
            account: ticket.account,
            instance_id: 2,
            time:    Time.new(2020)
          )
        end

        it 'returns `next_reply_time`' do
          assert_equal(:next_reply_time, @metric_event_status.name)
        end
      end
    end

    describe 'when not `reply_time` metric' do
      it 'returns metric' do
        assert_equal(event.metric.to_sym, @metric_event_status.name)
      end
    end
  end

  describe '.measured?' do
    it 'returns true' do
      assert @metric_event_status.measured?
    end
  end

  describe '.fulfilled?' do
    describe 'when metric event `type` is `fulfill`' do
      let(:event) { fulfill_event }

      it 'returns true' do
        assert @metric_event_status.fulfilled?
      end
    end

    describe 'when metric event `type` is not `fulfill`' do
      it 'returns false' do
        refute @metric_event_status.fulfilled?
      end
    end
  end

  describe '.paused?' do
    describe 'when metric event `type` is `pause`' do
      let(:event) { pause_event }

      it 'returns true' do
        assert @metric_event_status.paused?
      end
    end

    describe 'when metric event `type` is not `pause`' do
      it 'returns false' do
        refute @metric_event_status.paused?
      end
    end
  end

  describe '.stage' do
    describe 'when metric event `type` is `fulfill`' do
      let(:event) { fulfill_event }

      it 'is `achieved`' do
        assert_equal(:achieved, @metric_event_status.stage.name)
      end
    end

    describe 'when metric event `type` is `pause`' do
      let(:event) { pause_event }

      it 'is `paused`' do
        assert_equal(:paused, @metric_event_status.stage.name)
      end
    end

    describe 'when metric event `type` is `breach`' do
      let(:event) { breach_event }

      it 'is `active`' do
        assert_equal(:active, @metric_event_status.stage.name)
      end
    end
  end

  describe '.priority' do
    describe 'when metric event `type` is `fulfill`' do
      let(:event) { fulfill_event }

      it 'is 1' do
        assert_equal(1, @metric_event_status.priority)
      end
    end

    describe 'when metric event `type` is `pause`' do
      let(:event) { pause_event }

      it 'is 2' do
        assert_equal(2, @metric_event_status.priority)
      end
    end

    describe 'when metric event `type` is `breach`' do
      let(:event) { breach_event }

      it 'is 0' do
        assert_equal(0, @metric_event_status.priority)
      end
    end

    describe 'when metric event `type` is not `fulfill`,`pause` or `breach`' do
      let(:event) do
        ticket_metric_events.reply_time.apply_sla.create(
          account: ticket.account,
          time:    Time.new(2020)
        )
      end

      it 'is 3' do
        assert_equal(3, @metric_event_status.priority)
      end
    end
  end

  describe 'when comparing metric events' do
    def sorter(events)
      events.map { |e| Zendesk::Sla::MetricEventStatus.new e }.sort.map(&:event)
    end

    describe 'and there is no event of the same type' do
      let(:events) do
        [fulfill_event, pause_event, breach_event]
      end

      it 'orders them by `breach` > `fulfill` > `pause`' do
        assert_equal([breach_event, fulfill_event, pause_event], sorter(events))
      end
    end

    describe 'and there is more than one event of the same type' do
      let(:other_fulfill) do
        ticket_metric_events.requester_wait_time.fulfill.create(
          account: ticket.account,
          time: Time.new(2021)
        )
      end
      let(:other_pause) do
        ticket_metric_events.requester_wait_time.pause.create(
          account: ticket.account,
          time:    Time.new(2019)
        )
      end
      let(:other_breach) do
        ticket_metric_events.requester_wait_time.breach.create(
          account: ticket.account,
          time:    Time.new(2019)
        )
      end
      let(:events) do
        [fulfill_event, pause_event, breach_event, other_fulfill, other_pause, other_breach]
      end

      it 'orders them by `breach` > `fulfill` > `pause` and time' do
        assert_equal([other_breach, breach_event, fulfill_event, other_fulfill, other_pause, pause_event], sorter(events))
      end
    end
  end
end
