# rubocop:disable Layout/ArgumentAlignment
# rubocop:disable Style/BlockDelimiters
# rubocop:disable Layout/ExtraSpacing
# rubocop:disable Layout/FirstArrayElementIndentation
# rubocop:disable Layout/MultilineHashBraceLayout
# rubocop:disable Layout/MultilineMethodCallBraceLayout
# rubocop:disable Layout/SpaceAroundOperators

require_relative '../../../support/test_helper'
require_relative '../../../support/ticket_metric_helper'

SingleCov.covered! uncovered: 3

describe Zendesk::Sla::PolicyManager do
  include TicketMetricHelper

  fixtures :accounts,
           :users,
           :tickets,
           :ticket_fields

  let(:account)  { accounts(:minimum) }
  let(:ticket)   { tickets(:minimum_1) }
  let(:end_user) { users(:minimum_end_user) }

  let(:placeholder_filter) {
    Definition.build(conditions_all: [
      {field: 'current_tags', operator: 'includes', value: 'blah'}
    ])
  }

  let(:policy_manager) { Zendesk::Sla::PolicyManager.new(account, end_user) }

  describe 'create' do
    before do
      targets = {
        Zendesk::Sla::TicketMetric::FirstReplyTime.id => lambda { |i| 10 * (i + 1) },
        Zendesk::Sla::TicketMetric::RequesterWaitTime.id => lambda { |i| 50 + 10 * (i + 1) }
      }

      policy_metrics = begin
        ['Low', 'normal', PriorityType.HIGH, PriorityType.URGENT].each_with_index.flat_map do |priority, index|
          Zendesk::Sla::TicketMetric.all.map(&:id).map do |metric_id|
            priority_key = priority.is_a?(String) ? :priority : :priority_id
            target       = targets[metric_id].try(:call, index) || 10
            { priority_key => priority, metric_id: metric_id, target: target, business_hours: false }
          end
        end
      end

      @policy_info = {
        title: 'title',
        position: 89,
        filter: {
          all: [
            {
              field:    'current_tags',
              operator: 'includes',
              value:    ['blah'] }
          ]
        },
        policy_metrics: policy_metrics
      }
    end

    it 'creates policy and metrics' do
      assert_difference('Sla::Policy.count(:all)') do
        assert_difference('Sla::PolicyMetric.count(:all)', @policy_info[:policy_metrics].length) do
          policy = policy_manager.create(@policy_info)
          assert policy
          assert_equal 1,              policy.filter.conditions_all.length
          assert_equal 'current_tags', policy.filter.conditions_all[0].source
          assert_equal 'includes',     policy.filter.conditions_all[0].operator
          assert_equal ['blah'],       policy.filter.conditions_all[0].value
          assert_equal [],             policy.filter.conditions_any
        end
      end
    end

    describe 'metrics value as name' do
      before do
        @policy_info[:policy_metrics].each do |pm|
          pmetric      = Zendesk::Sla::TicketMetric.from_id(pm.delete(:metric_id))
          pm[:metric] = pmetric.name
        end
        @total  = @policy_info[:policy_metrics].size
        @policy = policy_manager.create(@policy_info)
      end

      it 'creates metrics' do
        assert_equal @total, @policy.policy_metrics.size
      end
    end

    describe 'metric missing' do
      before do
        @policy_info[:policy_metrics].slice!(-1)
      end

      it 'creates items' do
        assert_difference('Sla::Policy.count(:all)') do
          assert_difference('Sla::PolicyMetric.count(:all)', @policy_info[:policy_metrics].length) do
            policy_manager.create(@policy_info)
          end
        end
      end
    end

    describe 'extra metric' do
      before do
        @policy_info[:policy_metrics] << {
          priority_id:    PriorityType.URGENT,
          metric_id:      Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
          target:         90,
          business_hours: true
        }
      end

      it 'does not create items' do
        refute_difference('Sla::Policy.count(:all)') do
          refute_difference('Sla::PolicyMetric.count(:all)') do
            assert_raises ActiveRecord::RecordInvalid do
              policy_manager.create(@policy_info)
            end
          end
        end
      end
    end

    describe 'metric invalid' do
      before do
        # rubocop:disable Performance/InefficientHashSearch
        policy_metric = @policy_info[:policy_metrics].find { |p| p.keys.include?(:priority_id) }
        # rubocop:enable Performance/InefficientHashSearch

        policy_metric[:priority_id] = -1
      end

      it 'does not create items' do
        refute_difference('Sla::Policy.count(:all)') do
          refute_difference('Sla::PolicyMetric.count(:all)') do
            assert_raises ActiveRecord::RecordInvalid do
              policy_manager.create(@policy_info)
            end
          end
        end
      end
    end

    describe 'policy invalid' do
      before do
        @policy_info[:title] = nil
      end

      it 'does not create items' do
        refute_difference('Sla::Policy.count(:all)') do
          refute_difference('Sla::PolicyMetric.count(:all)') do
            assert_raises ActiveRecord::RecordInvalid do
              policy_manager.create(@policy_info)
            end
          end
        end
      end
    end

    describe 'position' do
      before do
        ids = (1..4).map { |position|
          new_sla_policy(position: position)
        }.map(&:id)

        policy_manager.reorder(ids)
      end

      describe 'position is empty' do
        before do
          @policy_info[:position] = nil
        end

        describe 'policies exists' do
          before { @policy = policy_manager.create(@policy_info) }

          it 'assigns policy to last position' do
            last_position = account.sla_policies.active.last.position
            assert_equal last_position, @policy.reload.position
          end
        end

        describe 'no policies exist' do
          before do
            account.sla_policies.destroy_all
            @policy = policy_manager.create(@policy_info)
          end

          it 'assigns policy to first position' do
            assert_equal 1, @policy.reload.position
          end
        end
      end

      it 'updates position' do
        @policy_info[:position] = 1
        @policy                  = policy_manager.create(@policy_info)
        assert_equal 1, @policy.reload.position
      end

      it 'handles position which is a string' do
        @policy_info[:position] = '90'
        @policy                  = policy_manager.create(@policy_info)
        last_position            = account.sla_policies.active.last.position
        assert_equal last_position, @policy.reload.position
      end

      it 'handles position which is negative' do
        @policy_info[:position] = -1
        @policy                  = policy_manager.create(@policy_info)
        last_position            = account.sla_policies.active.last.position
        assert_equal last_position, @policy.reload.position
      end

      describe 'another policy exists' do
        before do
          @other_policy            = new_sla_policy(position: 5)
          @policy_info[:position] = @other_policy.position
        end

        it 'inserts new policy before existent policy' do
          @policy = policy_manager.create(@policy_info)
          assert_equal @policy.reload.position + 1, @other_policy.reload.position
        end

        describe 'other policy has later position' do
          before do
            @position = @other_policy.position + 10
            @other_policy.update_attributes!(position: @position)
            @policy = policy_manager.create(@policy_info)
          end

          it 'adjusts position of other policy' do
            assert @position > @other_policy.reload.position
          end
        end

        describe "other policy is deleted" do
          before do
            @position = @other_policy.position
            @other_policy.soft_delete
            @policy = policy_manager.create(@policy_info)
          end

          it 'does not adjust position of other policy' do
            assert_equal @position, @other_policy.reload.position
          end
        end

        describe 'gaps in positions' do
          before do
            @other_policy.update_attributes!(position: 13)
            @third_policy  = new_sla_policy(position: 19)
            @fourth_policy = new_sla_policy(position: 90)
            @fifth_policy  = new_sla_policy(position: 10)
            @policies      = [@other_policy, @third_policy, @fourth_policy, @fifth_policy]

            account.sla_policies.active.reject { |p| @policies.include?(p) }.each(&:destroy)
            @policy_info[:position] = 2
            @policy = policy_manager.create(@policy_info)
            @policies << @policy
            @expected = [@fifth_policy, @policy, @other_policy, @third_policy, @fourth_policy]
          end

          it 'closes gaps' do
            assert_equal @expected, @policies.each.map(&:reload).sort_by(&:position)
          end
        end
      end
    end
  end

  describe 'update' do
    before { @policy = new_sla_policy }

    describe 'title' do
      before { policy_manager.update(@policy, title: 'new title') }

      it 'updates the title' do
        assert_equal 'new title', @policy.reload.title
      end
    end

    describe 'description' do
      before { policy_manager.update(@policy, description: 'new description') }

      it 'updates the description' do
        assert_equal 'new description', @policy.reload.description
      end
    end

    describe 'filter' do
      before do
        policy_manager.update(@policy,
          filter: {
            all: [
              {
                field:    'current_tags',
                operator: 'includes',
                value:    ['updated_value']
              }
            ]
          }
        )
      end

      it 'updates the filter' do
        assert_equal(
          Definition.build(
            conditions_all: [
              {
                field:    'current_tags',
                operator: 'includes',
                value:    'updated_value'
              }
            ]
          ),
          @policy.reload.filter
        )
      end
    end

    describe 'policy metrics' do
      before do
        policy_manager.update(@policy,
          policy_metrics: [
            {
              priority_id:    PriorityType.LOW,
              metric_id:      Zendesk::Sla::TicketMetric::FirstReplyTime.id,
              target:         999,
              business_hours: false
            }
          ]
        )
      end

      it 'updates the policy metrics' do
        assert @policy.policy_metrics.map(&:target).include?(999)
      end
    end
  end

  describe 'delete' do
    let(:policy) { new_sla_policy }

    describe 'when policy is valid' do
      before do
        policy_manager.delete(policy.id)

        policy.reload
      end

      it 'soft deletes the policy' do
        assert policy.deleted?
      end
    end

    describe 'when policy is invalid' do
      before do
        policy.update_attribute(:title, nil)

        policy_manager.delete(policy.id)

        policy.reload
      end

      it 'soft deletes the policy' do
        assert policy.deleted?
      end
    end
  end

  describe 'reorder' do
    before do
      2.times do new_sla_policy end

      @policies = account.sla_policies.active.map(&:id)

      policy_manager.reorder(@policies.reverse)
    end

    it 'reorders policies' do
      assert_equal @policies.reverse, account.sla_policies.active.map(&:id)
    end
  end
end

# rubocop:enable Layout/ArgumentAlignment
# rubocop:enable Style/BlockDelimiters
# rubocop:enable Layout/ExtraSpacing
# rubocop:enable Layout/FirstArrayElementIndentation

# rubocop:enable Layout/MultilineHashBraceLayout
# rubocop:enable Layout/MultilineMethodCallBraceLayout

# rubocop:enable Layout/SpaceAroundOperators
