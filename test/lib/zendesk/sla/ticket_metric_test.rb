require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Sla::TicketMetric do
  describe '.supported' do
    it 'returns the supported SLA metric names' do
      assert_equal(
        %i[
          agent_work_time
          first_reply_time
          next_reply_time
          pausable_update_time
          periodic_update_time
          requester_wait_time
        ],
        Zendesk::Sla::TicketMetric.supported
      )
    end
  end

  describe '.all' do
    it 'returns the SLA metrics' do
      assert_equal(
        [
          Zendesk::Sla::TicketMetric::AgentWorkTime,
          Zendesk::Sla::TicketMetric::FirstReplyTime,
          Zendesk::Sla::TicketMetric::NextReplyTime,
          Zendesk::Sla::TicketMetric::PausableUpdateTime,
          Zendesk::Sla::TicketMetric::PeriodicUpdateTime,
          Zendesk::Sla::TicketMetric::RequesterWaitTime
        ],
        Zendesk::Sla::TicketMetric.all
      )
    end
  end

  describe '.from_id' do
    describe 'when the ticket metric is supported' do
      [
        Zendesk::Sla::TicketMetric::AgentWorkTime,
        Zendesk::Sla::TicketMetric::FirstReplyTime,
        Zendesk::Sla::TicketMetric::NextReplyTime,
        Zendesk::Sla::TicketMetric::PausableUpdateTime,
        Zendesk::Sla::TicketMetric::PeriodicUpdateTime,
        Zendesk::Sla::TicketMetric::RequesterWaitTime
      ].each do |sla_metric|
        it 'return the appropriate ticket metric class' do
          assert_equal(
            sla_metric,
            Zendesk::Sla::TicketMetric.from_id(sla_metric.id)
          )
        end
      end
    end

    describe 'when the ticket metric is not supported' do
      let(:metric_id) { 999 }

      it 'return nil' do
        assert_nil Zendesk::Sla::TicketMetric.from_id(metric_id)
      end
    end
  end

  describe '.from_name' do
    describe 'when the SLA metric is supported' do
      let(:metric_name) { :first_reply_time }

      it 'return the appropriate ticket metric class' do
        assert_equal(
          Zendesk::Sla::TicketMetric::FirstReplyTime,
          Zendesk::Sla::TicketMetric.from_name(metric_name)
        )
      end
    end

    describe 'when the SLA metric is not supported' do
      let(:metric_name) { :random_name }

      it 'return nil' do
        assert_nil Zendesk::Sla::TicketMetric.from_name(metric_name)
      end
    end
  end

  describe '.for_metric' do
    describe 'when the underlying metric has multiple SLA metrics' do
      let(:metric) { :reply_time }

      it 'returns the appropriate SLA metrics' do
        assert_equal(
          [
            Zendesk::Sla::TicketMetric::FirstReplyTime,
            Zendesk::Sla::TicketMetric::NextReplyTime
          ],
          Zendesk::Sla::TicketMetric.for_metric(metric)
        )
      end
    end

    describe 'when the underlying metric has one SLA metric' do
      let(:metric) { :requester_wait_time }

      it 'returns the appropriate SLA metric' do
        assert_equal(
          [Zendesk::Sla::TicketMetric::RequesterWaitTime],
          Zendesk::Sla::TicketMetric.for_metric(metric)
        )
      end
    end

    describe 'when the underlying metric is not supported' do
      let(:metric) { :random_metric }

      it 'returns an empty array' do
        assert_equal [], Zendesk::Sla::TicketMetric.for_metric(metric)
      end
    end
  end

  describe '.supported_metric?' do
    describe 'when the metric is supported' do
      let(:metric) { :reply_time }

      it 'returns true' do
        assert Zendesk::Sla::TicketMetric.supported_metric?(metric)
      end
    end

    describe 'when the metric is not supported' do
      let(:metric) { :resolution_time }

      it 'returns false' do
        refute Zendesk::Sla::TicketMetric.supported_metric?(metric)
      end
    end

    describe 'when the metric does not exist' do
      let(:metric) { :invalid_metric }

      it 'returns false' do
        refute Zendesk::Sla::TicketMetric.supported_metric?(metric)
      end
    end
  end
end
