require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Zendesk::Sla::Stage do
  let(:ticket_metric) do
    Class.new do
      attr_writer :measured,
        :fulfilled,
        :paused

      def measured?
        @measured
      end

      def fulfilled?
        @fulfilled
      end

      def paused?
        @paused
      end

      def name?(*)
        false
      end
    end.new
  end

  describe '.for' do
    describe 'when the metric is not measured' do
      before { ticket_metric.measured = false }

      it "returns the 'none' stage" do
        assert_equal :none, Zendesk::Sla::Stage.for(ticket_metric).name
      end
    end

    describe 'when the metric is not fulfilled nor paused' do
      before do
        ticket_metric.measured  = true
        ticket_metric.fulfilled = false
        ticket_metric.paused    = false
      end

      it "returns the 'active' stage" do
        assert_equal :active, Zendesk::Sla::Stage.for(ticket_metric).name
      end
    end

    describe 'when the metric is not fulfilled but is paused' do
      before do
        ticket_metric.measured  = true
        ticket_metric.fulfilled = false
        ticket_metric.paused    = true
      end

      it "returns the 'paused' stage" do
        assert_equal :paused, Zendesk::Sla::Stage.for(ticket_metric).name
      end
    end

    describe 'when the metric is fulfilled' do
      before do
        ticket_metric.measured  = true
        ticket_metric.fulfilled = true
        ticket_metric.paused    = false
      end

      it "returns the 'achieved' stage" do
        assert_equal :achieved, Zendesk::Sla::Stage.for(ticket_metric).name
      end
    end
  end

  describe '#name' do
    it 'returns the name of the stage' do
      assert_equal(
        :achieved,
        Zendesk::Sla::Stage::Achieved.new(ticket_metric).name
      )
    end
  end
end
