require_relative '../../../support/test_helper'
require_relative '../../../support/ticket_metric_helper'

SingleCov.covered!

describe Zendesk::Sla::TicketStatus do
  include TicketMetricHelper

  fixtures :accounts,
    :tickets,
    :events

  let(:ticket)  { tickets(:minimum_1) }
  let(:account) { ticket.account }

  let(:status_creation) do
    events(:create_create_status_id_for_minimum_ticket_1)
  end

  let(:policy) { new_sla_policy }

  let(:calculation_time) { ticket.created_at + 100.minutes }

  let(:sla_status) { Zendesk::Sla::TicketStatus.new(ticket) }

  before do
    Ticket.any_instance.stubs(created_at: status_creation.created_at)

    Account.any_instance.stubs(has_service_level_agreements?: true)

    ticket.metric_events.destroy_all

    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
      target:    10
    )

    assign_policy

    ticket.sla_ticket_policy.statuses.destroy_all

    Timecop.freeze(calculation_time)
  end

  describe '#calculate' do
    describe 'when there is not a relevant metric' do
      before do
        ticket.sla_ticket_policy.cache_statuses

        sla_status.calculate
      end

      it 'sets the breach status to none' do
        assert_equal(
          Zendesk::Sla::TicketStatus::NONE,
          ticket.sla_breach_status
        )
      end
    end

    describe 'when there is a relevant metric' do
      before do
        ticket.metric_events.requester_wait_time.activate.create(
          account: account,
          time:    ticket.created_at
        )

        TicketMetric::ApplySla.create(
          ticket:  ticket,
          account: ticket.account,
          metric:  'requester_wait_time',
          time:    ticket.created_at
        )

        ticket.metric_events.requester_wait_time.breach.create(
          account: account,
          time:    ticket.created_at + 10.minutes
        )
      end

      describe 'and the metric is active' do
        before do
          ticket.sla_ticket_policy.cache_statuses

          sla_status.calculate
        end

        it 'sets the breach status to the breach time' do
          assert_equal(
            (ticket.created_at + 10.minutes).to_i,
            ticket.sla_breach_status
          )
        end
      end

      describe 'and the metric is active and the breach time exceeds the max' do
        before do
          ticket.metric_events.requester_wait_time.breach.destroy_all

          ticket.metric_events.requester_wait_time.breach.create(
            account: account,
            time:    Time.at(4_294_967_293)
          )

          ticket.sla_ticket_policy.cache_statuses

          sla_status.calculate
        end

        it 'sets the breach status to the maximum breach time' do
          assert_equal(
            Zendesk::Sla::TicketStatus::MIN_SPECIAL_STATUS - 1,
            ticket.sla_breach_status
          )
        end
      end

      describe 'and the metric is paused and not breached' do
        before do
          ticket.metric_events.requester_wait_time.breach.destroy_all
          ticket.metric_events.requester_wait_time.pause.create(
            account: account,
            time:    ticket.created_at + 3.minutes
          )

          ticket.sla_ticket_policy.cache_statuses

          sla_status.calculate
        end

        it 'sets the breach status to paused' do
          assert_equal(
            Zendesk::Sla::TicketStatus::PAUSED,
            ticket.sla_breach_status
          )
        end
      end

      describe 'and the metric is paused and breached' do
        before do
          ticket.metric_events.requester_wait_time.pause.create(
            account: account,
            time:    ticket.created_at + 11.minutes
          )

          ticket.sla_ticket_policy.cache_statuses

          sla_status.calculate
        end

        it 'sets the breach status to paused' do
          assert_equal(
            Zendesk::Sla::TicketStatus::PAUSED_BREACHED,
            ticket.sla_breach_status
          )
        end
      end

      describe 'and the status has not changed' do
        it 'does not perform an update' do
          assert_sql_queries 0, /UPDATE `tickets`/ do
            sla_status.calculate
          end
        end
      end
    end
  end

  describe '#next_breach_at' do
    describe 'when set to a time' do
      let(:timestamp) { Time.new(2014, 1, 1) }

      before { ticket.sla_breach_status = timestamp }

      it 'returns the time' do
        assert_equal timestamp, sla_status.next_breach_at
      end
    end

    describe 'when there is no status' do
      before do
        ticket.sla_breach_status = Zendesk::Sla::TicketStatus::NONE
      end

      it 'returns nil' do
        assert_nil sla_status.next_breach_at
      end
    end

    describe 'when paused' do
      before do
        ticket.sla_breach_status = Zendesk::Sla::TicketStatus::PAUSED
      end

      it 'returns nil' do
        assert_nil sla_status.next_breach_at
      end
    end

    describe 'when paused and breached' do
      before do
        ticket.sla_breach_status = Zendesk::Sla::TicketStatus::PAUSED_BREACHED
      end

      it 'returns nil' do
        assert_nil sla_status.next_breach_at
      end
    end
  end

  describe '#metric' do
    it 'return the most relevant metric' do
      assert_equal(
        Zendesk::Sla::MetricSelection.new(ticket).most_relevant,
        sla_status.metric
      )
    end
  end
end
