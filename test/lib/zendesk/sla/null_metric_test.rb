require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Sla::NullMetric do
  let(:null_metric) { Zendesk::Sla::NullMetric }

  describe '.measured?' do
    it 'returns false' do
      refute null_metric.measured?
    end
  end

  describe '.breach_at' do
    it 'returns nil' do
      assert_nil null_metric.breach_at
    end
  end

  describe '.fulfilled_at' do
    it 'returns nil' do
      assert_nil null_metric.fulfilled_at
    end
  end

  describe '.fulfilled?' do
    it 'returns true' do
      assert null_metric.fulfilled?
    end
  end

  describe '.paused?' do
    it 'returns false' do
      refute null_metric.paused?
    end
  end

  describe '.stage' do
    it 'returns the `none` stage' do
      assert_kind_of Zendesk::Sla::Stage::None, null_metric.stage
    end
  end
end
