require_relative "../../../support/test_helper"
require_relative "../../../support/attachment_test_helper"

SingleCov.covered!

describe Zendesk::Stores::StoresSynchronizerHelper.name do
  fixtures :accounts

  let(:att_helper) { Zendesk::Stores::StoresSynchronizerHelper.new(Attachment) }
  let(:brandlogo_helper) { Zendesk::Stores::StoresSynchronizerHelper.new(BrandLogo) }
  let(:known_stores) do
    scoper = ::Technoweenie::AttachmentFu::ScopedStores.method(:scope_store_name)
    [:fs, :s3, :s3eu, :s3apne] | [:fs, :s3, :s3eu, :s3apne].map(&scoper)
  end
  let(:s3_stores) { known_stores.select { |s| s.to_s.starts_with?("s3") } }

  before do
    stub_request(:put, %r{https://zendesk-test.*.amazonaws\.com/.*})
  end

  it "knows Attachment backends" do
    assert_equal [:fs, :s3].sort, att_helper.preferred_stores.sort
    assert_equal known_stores.sort, att_helper.known_stores.sort
    assert_equal s3_stores.sort, att_helper.s3_stores.sort
  end

  it "knows BrandLogo backends" do
    assert_equal [:fs, :s3].sort, brandlogo_helper.preferred_stores.sort
    assert_equal known_stores.sort, brandlogo_helper.known_stores.sort
    assert_equal s3_stores.sort, brandlogo_helper.s3_stores.sort
  end

  describe "#preferred_s3_store" do
    let(:attachment) do
      attachment = Attachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/normal_1.jpg"))
      attachment.stores = [:s3eu, :s3apne, :s3, :fs]
      attachment.account = accounts(:minimum)
      attachment.save!
      attachment
    end

    let(:regional_stores) { [:s3eu] }

    it "picks class-preferred s3 first" do
      assert_equal :s3, att_helper.preferred_s3_store(attachment, regional_stores)
    end

    it "picks regional s3 second" do
      attachment.stores = attachment.stores - [:s3]
      assert_equal :s3eu, att_helper.preferred_s3_store(attachment, regional_stores)
    end

    it "picks available if only one s3 store" do
      attachment.stores = attachment.stores - [:s3, :s3eu]
      assert_equal :s3apne, att_helper.preferred_s3_store(attachment, regional_stores)
    end

    it "picks first out-of-region s3 store if no regional or class preferred" do
      attachment.stores = attachment.stores - [:s3]
      assert_equal :s3eu, att_helper.preferred_s3_store(attachment, [:s3eu1])
    end

    it "returns nil if no s3" do
      attachment.stores = attachment.stores - [:s3apne, :s3, :s3eu]
      assert_nil att_helper.preferred_s3_store(attachment, regional_stores)
    end
  end

  describe "#exists?" do
    let (:fs_attachment) do
      att = create_attachment("#{Rails.root}/test/files/normal_1.jpg")
      att.stores = [:fs]
      att.save!
      att
    end

    let (:s3_attachment) do
      att = create_attachment("#{Rails.root}/test/files/normal_1.jpg")
      att.stores = [:s3]
      att.save!
      att
    end

    let (:fs_brandlogo) do
      logo = create_brand_logo(accounts(:minimum), [:fs])
      logo.save!
      logo
    end

    it "returns false for unrecognized store" do
      fs_attachment.stores += [:cf]
      refute att_helper.exists?(fs_attachment, :cf)
    end

    it "returns false if not in the store" do
      refute att_helper.exists?(fs_attachment, :s3)
      refute att_helper.exists?(s3_attachment, :fs)
    end

    it "will use an exists method if/when there is one" do
      fs_attachment.fs.define_singleton_method(:exists?) { false }
      refute att_helper.exists?(fs_attachment, :fs)
    end

    it 'identifies recs not in :fs' do
      refute att_helper.exists?(s3_attachment, :fs)
    end

    it 'identifies recs not in :s3' do
      refute att_helper.exists?(fs_attachment, :s3)
    end

    it "identifies attachemnts in filesystem" do
      assert att_helper.exists?(fs_attachment, :fs)

      fs_attachment.fs.destroy_file
      refute att_helper.exists?(fs_attachment, :fs)
    end

    it 'identifies brandlogos in filesystem' do
      assert brandlogo_helper.exists?(fs_brandlogo, :fs)

      fs_brandlogo.fs.destroy_file
      refute brandlogo_helper.exists?(fs_brandlogo, :fs)
    end

    it "identifies recs in s3" do
      stub_request(:head, %r{us-west-2\.amazonaws\.com/.*}).
        to_return(status: 200).then.
        to_return(status: 404)

      # stubbed to return false on 2nd call
      assert att_helper.exists?(s3_attachment, :s3)
      refute att_helper.exists?(s3_attachment, :s3)
    end

    it "can skip checking the stores tag" do
      assert att_helper.exists?(fs_attachment, :fs)

      fs_attachment.stores -= [:fs]
      refute att_helper.exists?(fs_attachment, :fs)
      assert att_helper.exists?(fs_attachment, :fs, check_tag: false)
    end

    it "blows up on new backend classes" do
      new_shiny_be = Object.new
      fs_attachment.define_singleton_method(:new_shiny) { new_shiny_be }
      fs_attachment.stores += [:new_shiny]
      assert_raises StandardError do
        att_helper.exists?(fs_attachment, :new_shiny)
      end
    end
  end

  def create_brand_logo(account, stores)
    logo = BrandLogo.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/normal_1.jpg"))
    logo.brand = account.brands.first
    logo.account = account
    logo.stores = stores
    logo.save!
    logo
  end
end
