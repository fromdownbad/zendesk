require_relative "../../../support/test_helper"
require_relative "../../../support/attachment_test_helper"

SingleCov.covered!

# Helper to run over objects for different atta-fu-managed models
module StoresSynchronizerTestHelper
  def describe_for_atta_fu_models(&block)
    describe "for attachments" do
      before do
        @rec = create_attachment("#{Rails.root}/test/files/normal_1.jpg")
      end
      class_eval(&block)
    end

    describe "for brand-logos" do
      before do
        @rec = FactoryBot.create(:brand_logo, filename: "small.png")
      end

      class_eval(&block)
    end

    describe "for voice-uploads" do
      fixtures :voice_uploads

      before do
        @rec = Voice::Upload.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new('test/files/dtmf-1.mp3'))
        @rec.account = Account.last
        @rec.expects(:ensure_valid_audio_file).at_least(1).returns(true)
        @rec.save!
      end

      class_eval(&block)
    end
  end

  def describe_for_models_with_thumbnails(&block)
    before do
      stub_request(:put, %r{us-west-2\.amazonaws.com/.*})
    end

    describe "for attachments" do
      before do
        path_prefix = "#{Rails.root}/test/files"
        @rec = create_attachment("#{path_prefix}/normal_1.jpg")
        assert_equal 1, @rec.thumbnails.size
      end
      class_eval(&block)
    end

    describe "for brand-logos" do
      before do
        @rec = FactoryBot.create(:brand_logo, filename: "small.png")
        assert_equal 2, @rec.thumbnails.size
      end

      class_eval(&block)
    end
  end
end

describe Zendesk::Stores::StoresSynchronizer do
  extend StoresSynchronizerTestHelper

  let(:synchronizer) { Zendesk::Stores::StoresSynchronizer.new }

  let(:invalid_store1) { :nosuchstore }
  let(:invalid_store2) { :fakestore }

  describe "#add_to_preferred_stores" do
    before do
      stub_request(:put, %r{us-west-2\.amazonaws.com/.*})
      stub_request(:put, %r{eu-west-1\.amazonaws.com/.*})
    end

    describe_for_atta_fu_models do
      it "will add secondary stores, after current stores" do
        original_stores = @rec.stores.dup
        refute_includes @rec.stores, :s3
        synchronizer.add_to_preferred_stores(@rec)
        @rec.reload
        assert_includes @rec.stores, :s3
        assert_equal original_stores, @rec.stores.slice(0, original_stores.length)
      end

      it "will not remove unknown stores" do
        @rec.stores = @rec.stores + [invalid_store1]
        synchronizer.add_to_preferred_stores(@rec)
        @rec.reload
        assert_includes @rec.stores, invalid_store1
      end

      it "will not remove non-preferred stores" do
        @rec.stores = @rec.stores + [:s3eu]
        synchronizer.add_to_preferred_stores(@rec)
        @rec.reload
        assert_includes @rec.stores, :s3eu
      end

      it "will not remove unsupported stores" do
        @rec.stores = @rec.stores + Zendesk::Stores::StoresSynchronizer.unsupported_stores
        synchronizer.add_to_preferred_stores(@rec)
        Zendesk::Stores::StoresSynchronizer.unsupported_stores.each { |s| assert_includes @rec.stores, s }
      end

      it "will bail on noop" do
        preferred_stores = @rec.class.attachment_backends.
          select { |_, v| v[:options][:default] || v[:options][:secondary] }.
          map(&:first)

        @rec.stores = @rec.stores + preferred_stores
        original_stores = @rec.stores.dup
        @rec.expects(:save!).never
        synchronizer.add_to_preferred_stores(@rec)
        assert_equal original_stores, @rec.stores
      end
    end

    describe_for_models_with_thumbnails do
      it "will bail if called on a thumbnail" do
        thumb = @rec.thumbnails.first
        thumb.expects(:save!).never
        synchronizer.add_to_preferred_stores(thumb)
      end

      it "will add same stores to thumbnails" do
        synchronizer.add_to_preferred_stores(@rec)
        @rec.reload
        @rec.thumbnails.each do |t|
          assert_equal @rec.stores, t.stores
        end
      end
    end
  end

  describe "#remove_from_unwanted_stores" do
    before do
      stub_request(:put, %r{us-west-2\.amazonaws.com/.*})
      stub_request(:put, %r{eu-west-1\.amazonaws.com/.*})
      stub_request(:delete, %r{us-west-2\.amazonaws.com/.*})
      stub_request(:delete, %r{eu-west-1\.amazonaws.com/.*})
      stub_request(:head, %r{us-west-2\.amazonaws.com/.*})
    end

    describe_for_atta_fu_models do
      it "will remove non-preferred stores" do
        @rec.stores += [:s3eu]
        synchronizer.add_to_preferred_stores(@rec)
        @rec.save
        @rec.reload

        assert_includes @rec.stores, :s3eu

        synchronizer.remove_from_unwanted_stores(@rec)
        refute_includes @rec.stores, :s3eu
      end

      it "will bail on noop" do
        synchronizer.add_to_preferred_stores(@rec)
        @rec.save
        @rec.reload

        original_stores = @rec.stores.dup
        @rec.expects(:save!).never
        synchronizer.remove_from_unwanted_stores(@rec)
        assert_equal original_stores, @rec.stores
      end

      it "will not remove unknown stores" do
        @rec.stores += [invalid_store1]
        synchronizer.remove_from_unwanted_stores(@rec)
        assert_includes @rec.stores, invalid_store1
      end

      it "will not remove stores if not tagged with a preferred stores" do
        @rec.stores = [:s3eu]
        @rec.expects(:save!).never
        assert_raises Zendesk::Stores::NotInPreferredStoreError do
          synchronizer.remove_from_unwanted_stores(@rec)
        end
        assert_includes @rec.stores, :s3eu
      end

      it "will not remove stores if it d.n.e. in its preferred stores" do
        stub_request(:head, %r{us-west-2\.amazonaws.com/.*}).to_return(status: 404)
        @rec.stores = [:s3, :s3eu]
        @rec.expects(:save!).never
        assert_raises Zendesk::Stores::MissingFromPreferredError do
          synchronizer.remove_from_unwanted_stores(@rec)
        end
        assert_includes @rec.stores, :s3eu
      end

      it "will remove unsupported stores" do
        @rec.stores = @rec.stores + Zendesk::Stores::StoresSynchronizer.unsupported_stores # should be [:cf, :cfeu, :xfiles] for test env
        synchronizer.remove_from_unwanted_stores(@rec)
        @rec.reload
        Zendesk::Stores::StoresSynchronizer.unsupported_stores.each { |s| refute_includes @rec.stores, s }
      end

      it "will not remove unsupported store if it is known" do
        @rec.stores = @rec.stores + [:fs]
        @rec.save!
        Zendesk::Stores::StoresSynchronizer.expects(:unsupported_stores).returns([:fs]).once
        @rec.expects(:save!).never
        synchronizer.remove_from_unwanted_stores(@rec)
        assert_includes @rec.stores, :fs
      end
    end

    describe_for_models_with_thumbnails do
      it "will bail if called on a thumbnail" do
        thumb = @rec.thumbnails.first
        thumb.expects(:save!).never
        synchronizer.remove_from_unwanted_stores(thumb)
      end

      it "will remove stores from thumbnails" do
        @rec.stores += [:s3eu]
        synchronizer.add_to_preferred_stores(@rec)
        @rec.save
        @rec.reload

        @rec.thumbnails.each { |thumb| assert_includes thumb.stores, :s3eu }

        synchronizer.remove_from_unwanted_stores(@rec)
        @rec.reload
        @rec.thumbnails.each { |thumb| refute_includes thumb.stores, :s3eu }
      end
    end
  end
end
