require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Stores::Backfill::AccountBackfiller do
  fixtures :accounts

  let(:config) { Zendesk::Stores::Backfill::StoresBackfillConfig.new }
  let(:region_helper) { Zendesk::Stores::Backfill::RegionHelper.new(config) }
  let(:account_id_1) { accounts(:minimum) }

  it "will backfill models for an account" do
    updates = Zendesk::Stores::Backfill::Update.new(44, 22)

    model_backfiller = FakeModelBackfiller.new(
      Attachment.name => updates,
      BrandLogo.name => updates
    )

    region_helper.expects(:account_audits).with(account_id_1).returns(
      [
        create_audit(Attachment.name, false),
        create_audit(Voice::Upload.name, true),
        create_audit(BrandLogo.name, false)
      ]
    )

    backfiller = Zendesk::Stores::Backfill::AccountBackfiller.new(config, region_helper, model_backfiller)

    assert_equal updates + updates, backfiller.backfill_account(account_id_1)
    assert_equal 2, model_backfiller.call_count
    refute model_backfiller.stop_check.call
  end

  it "will skip done accounts" do
    region_helper.expects(:account_audits).with(account_id_1).returns(
      [
        create_audit(Attachment.name, true),
        create_audit(Voice::Upload.name, true),
        create_audit(BrandLogo.name, true)
      ]
    )

    model_backfiller = Object.new
    model_backfiller.expects(:backfill_model).never

    backfiller = Zendesk::Stores::Backfill::AccountBackfiller.new(config, region_helper, model_backfiller)

    assert_equal Zendesk::Stores::Backfill::Update.new, backfiller.backfill_account(account_id_1)
  end

  it "will pass time-based stop_check" do
    update = Zendesk::Stores::Backfill::Update.new(44, 22)
    model_backfiller = FakeModelBackfiller.new(BrandLogo.name => update)

    region_helper.expects(:account_audits).with(account_id_1).returns([create_audit(BrandLogo.name, false)])

    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(account_allowance: 0.25.seconds)

    backfiller = Zendesk::Stores::Backfill::AccountBackfiller.new(config, region_helper, model_backfiller)

    assert_equal update, backfiller.backfill_account(account_id_1)
    refute model_backfiller.stop_check.call
    sleep 0.3 # rubocop:disable Lint/Sleep
    assert model_backfiller.stop_check.call
  end

  def create_audit(name, done)
    audit = StoresBackfillAudit.new
    audit.model_class = name
    audit.max_id = 100
    audit.last_id = done ? 0 : 101
    audit
  end

  class FakeModelBackfiller
    attr_reader :call_count, :stop_check

    def initialize(updates)
      @updates = updates
      @call_count = 0
    end

    def backfill_model(audit, stop_check)
      raise StandardError, "no update for #{audit.model_class}" unless @updates[audit.model_class]
      @call_count += 1
      @stop_check ||= stop_check
      @updates.delete(audit.model_class)
    end
  end
end
