require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Stores::Backfill::RegionHelper do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:backfill_classes) { [Voice::Upload.name, BrandLogo.name, Attachment.name] }

  before do
    attachment = Attachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/normal_1.jpg"))
    attachment.author = account.users.last
    attachment.account = account
    attachment.save!

    upload = Voice::Upload.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new('test/files/dtmf-1.mp3'))
    upload.account = account
    upload.stubs(:ensure_valid_audio_file).returns(true)
    upload.save!

    BrandLogo.delete_all(account_id: account.id)
  end

  it 'Builds region based on preferred stores' do
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(backfill_classes: backfill_classes)
    fetcher = Zendesk::Stores::Backfill::RegionHelper.new(config)
    audits = fetcher.account_audits(account.id)
    assert_equal 3, audits.size
    assert_equal Voice::Upload.name, audits.first.model_class
    assert_equal BrandLogo.name, audits.second.model_class
    assert_equal Attachment.name, audits.third.model_class
    audits.each do |audit|
      assert_equal [:fs, :s3], audit.region, "wrong region for model #{audit.model_class}"
    end
  end
end
