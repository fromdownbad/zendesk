require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Stores::Backfill::StoresBackfillComponent do
  it "works" do
    fake = FakeComponent.new(Zendesk::Stores::Backfill::StoresBackfillConfig.new)
    refute_nil fake.logger
    refute_nil fake.config
    refute_nil fake.statsd
  end

  class FakeComponent < Zendesk::Stores::Backfill::StoresBackfillComponent
  end
end
