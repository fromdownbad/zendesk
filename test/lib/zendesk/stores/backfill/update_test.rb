require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Stores::Backfill::Update do
  let(:progress) { Zendesk::Stores::Backfill::Update.new }

  describe "#nothing_done?" do
    it "only true if not incremented" do
      assert progress.nothing_done?
      progress.increment(false)
      refute progress.nothing_done?
    end

    it "only true if errors not incremented" do
      progress.add_error
      refute progress.nothing_done?
    end

    it "false if initialized with evaluated" do
      p2 = Zendesk::Stores::Backfill::Update.new(2)
      refute p2.nothing_done?
    end

    it "false if added with evaluated" do
      p2 = progress + Zendesk::Stores::Backfill::Update.new(2)
      refute p2.nothing_done?
    end

    it "true if added with another nothing_done?" do
      p2 = progress + progress
      assert p2.nothing_done?
    end
  end

  describe "#increment" do
    it "reflects update" do
      progress.increment(true)
      assert_equal Zendesk::Stores::Backfill::Update.new(1, 1), progress
    end

    it "reflects no update" do
      progress.increment(false)
      assert_equal Zendesk::Stores::Backfill::Update.new(1), progress
    end

    it "is cummulative, but does not count errors" do
      progress.increment(false)
      progress.increment(true)
      progress.add_error
      progress.increment(false)
      progress.increment(false)
      progress.increment(true)
      progress.add_error
      progress.increment(true)
      assert_equal Zendesk::Stores::Backfill::Update.new(6, 3, 2), progress
    end
  end

  describe "#add_error" do
    it "tracks errors separate from eval" do
      progress.add_error
      assert_equal Zendesk::Stores::Backfill::Update.new(0, 0, 1), progress
      progress.increment(false)
      progress.add_error
      progress.increment(false)
      progress.increment(true)
      assert_equal Zendesk::Stores::Backfill::Update.new(3, 1, 2), progress
    end
  end

  describe "#+" do
    it "works" do
      p1 = Zendesk::Stores::Backfill::Update.new(15, 5)
      p2 = Zendesk::Stores::Backfill::Update.new(100, 87, 3)
      p3 = Zendesk::Stores::Backfill::Update.new(7, 0, 4)
      assert_equal 15 + 100 + 7, (p1 + p2 + p3).evaluated
      assert_equal 5 + 87, (p1 + p2 + p3).updated
      assert_equal 3 + 4, (p1 + p2 + p3).erred
      assert_equal 15, p1.evaluated
      assert_equal 100, p2.evaluated
      assert_equal 7, p3.evaluated
      assert_equal 5, p1.updated
      assert_equal 87, p2.updated
      assert_equal 0, p3.updated
      assert_equal 0, p1.erred
      assert_equal 3, p2.erred
      assert_equal 4, p3.erred
    end
  end

  describe "#==" do
    it "works" do
      p1 = Zendesk::Stores::Backfill::Update.new(15, 5)
      assert_equal p1, p1
      assert_equal Zendesk::Stores::Backfill::Update.new(p1.evaluated, p1.updated, p1.erred), p1
      refute_equal Zendesk::Stores::Backfill::Update.new(p1.evaluated + 1, p1.updated, p1.erred), p1
      refute_equal Zendesk::Stores::Backfill::Update.new(p1.evaluated, p1.updated + 1, p1.erred), p1
      refute_equal Zendesk::Stores::Backfill::Update.new(p1.evaluated, p1.updated, p1.erred + 1), p1
    end
  end

  describe '#to_s' do
    it 'works' do
      progress.increment(false)
      progress.increment(true)
      progress.increment(true)
      progress.add_error
      s = progress.to_s
      assert s.include? 'eval:3'
      assert s.include? 'upd:2'
      assert s.include? 'err:1'
    end
  end
end
