require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 1 # missing record exception

describe Zendesk::Stores::Backfill::ModelBackfiller do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  # This is an eager let to force loading of the class
  let!(:synchronizer) { Zendesk::Stores::StoresSynchronizer.new }
  let(:no_progress) { Zendesk::Stores::Backfill::Update.new }

  before do
    stub_request(:get, %r{https://zendesk-test.*.amazonaws.com/.*})
    stub_request(:head, %r{https://zendesk-test.*.amazonaws.com/.*})
    stub_request(:put, %r{https://zendesk-test.*.amazonaws.com/.*})
    stub_request(:delete, %r{https://zendesk-test.*.amazonaws.com/.*})
  end

  it 'bails for done audit (no save)' do
    create_attachment(account, [:s3])
    audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
    audit.done!

    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false)
    backfiller = Zendesk::Stores::Backfill::ModelBackfiller.new(config, synchronizer, nil)

    audit.expects(:save!).never
    assert_equal no_progress, run_backfiller(backfiller, audit)
    assert audit.done?
  end

  it 'bails if starting with no time' do
    create_attachment(account, [:s3])
    audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
    orig_last_id = audit.last_id

    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false)
    backfiller = Zendesk::Stores::Backfill::ModelBackfiller.new(config, synchronizer, nil)
    stop_check = lambda { true }

    audit.expects(:save!).never
    assert_equal no_progress, backfiller.backfill_model(audit, stop_check)
    assert_equal orig_last_id, audit.last_id
  end

  it 'updates audit to done and saves it if no ids selected' do
    rec = create_attachment(account, [:s3])
    audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
    audit.last_id = rec.id
    audit.save!

    refute audit.done?

    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false)
    selector = FakeSelector.new([])
    backfiller = Zendesk::Stores::Backfill::ModelBackfiller.new(config, synchronizer, selector)

    assert_equal no_progress, run_backfiller(backfiller, audit)
    assert audit.done?
  end

  it 'synchronizes to add/remove stores' do
    Zendesk::Stores::StoresSynchronizerHelper.any_instance.stubs(:preferred_stores).returns([:s3eu])

    rec = create_attachment(account, [:s3])
    audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false)

    backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

    verify_update(audit, backfiller, rec, adds: ['s3eu'], removes: ['s3'])
  end

  it 'goes through all the motions except actual sync when dry_run set' do
    Zendesk::Stores::StoresSynchronizerHelper.any_instance.stubs(:preferred_stores).returns([:s3eu])

    rec = create_attachment(account, [:s3])
    audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: true)

    backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

    verify_update(audit, backfiller, rec, adds: ['s3eu'], removes: ['s3'])
  end

  it 'removes recognized unsupported store :cf' do
    Zendesk::Stores::StoresSynchronizerHelper.any_instance.stubs(:preferred_stores).returns([:s3eu])

    rec = create_attachment(account, [:s3eu, :cf])
    audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false)

    backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

    verify_update(audit, backfiller, rec, removes: ['cf'])
  end

  it 'removes recognized unsupported store :cfeu' do
    Zendesk::Stores::StoresSynchronizerHelper.any_instance.stubs(:preferred_stores).returns([:s3eu])

    rec = create_attachment(account, [:s3eu, :cfeu])
    audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false)

    backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

    verify_update(audit, backfiller, rec, removes: ['cfeu'])
  end

  # Assert that no changes happen when the region on the audit only changes to
  # a scoped store or xfiles.
  #
  # [ record_stores, region_stores ]
  scoped_store = Technoweenie::AttachmentFu::ScopedStores.scope_store_name(:s3eu)
  [
    [[:s3eu], [:s3eu]],
    [[:s3eu], [scoped_store]],
    [[scoped_store], [:s3eu]],
    [[:xfiles, :s3eu], [:s3eu]]
  ].each do |record_stores, region_stores|
    it "does not backfill changes from #{record_stores} to #{region_stores}" do
      rec = create_attachment(account, record_stores)
      audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, region_stores)
      config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false)

      setup_no_synchronizer_calls(synchronizer)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_no_update(audit, backfiller, rec)
    end
  end

  it 'does not backfill token attachments' do
    rec = create_attachment(account, [:s3])
    rec.source_type = 'Token'
    rec.save!

    audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false)

    setup_no_synchronizer_calls(synchronizer)

    backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

    verify_no_update(audit, backfiller, rec, reason: rec.source_type)
  end

  it 'errors for rec with unknown store(s)' do
    rec = create_attachment(account, [:s3eu, :xfiles, :funkytown])
    audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false)

    setup_no_synchronizer_calls(synchronizer)

    backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

    backfiller.config.statsd.expects(:increment).with('unknown_stores', tags: %W[model:#{Attachment.name} store:funkytown])
    verify_error(audit, backfiller, reason: 'UnknownStore')
  end

  describe 'for bad-data records' do
    let(:rec) { create_attachment(account, [:s3eu, :xfiles]) }
    let(:audit) { StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3]) }
    let(:config) { Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false) }

    it 'errors on StandardErrors' do
      error = StandardError.new('no bueno')
      setup_synchronizer_error(synchronizer, :add, error)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_error(audit, backfiller, reason: error)
    end

    it 'errors on AWS errors' do
      error = Aws::S3::Errors::NoSuchKey.new(self, 'no good')
      setup_synchronizer_error(synchronizer, :add, error)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_error(audit, backfiller, reason: error)
    end

    it 'errors on NotInPreferredStoreError' do
      error = Zendesk::Stores::NotInPreferredStoreError.new('aw shucks')
      setup_synchronizer_error(synchronizer, :rm, error)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_error(audit, backfiller, reason: error)
    end

    it 'errors on MissingFromPreferredError (unknown cause)' do
      error = Zendesk::Stores::MissingFromPreferredError.new('darn')
      setup_synchronizer_error(synchronizer, :rm, error)
      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_error(audit, backfiller, reason: error)
    end

    it 'continues on if attachment is for deleted, SCRUBBED ticket' do
      attach_ticket(rec) do |tkt|
        tkt.status_id = StatusType.DELETED
        tkt.subject = 'SCRUBBED'
      end

      setup_no_synchronizer_calls(synchronizer)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_no_update(audit, backfiller, rec, reason: 'Scrubbed')
    end

    it 'continues on if attachment is for unclean scrubbed ticket' do
      attach_ticket(rec) do |tkt|
        tkt.status_id = StatusType.ARCHIVED
        tkt.subject = 'I am a scrubby, scruffy ticket'
        stub = TicketArchiveStub.new
        stub.id = tkt.id
        stub.subject = 'SCRUBBED'
        TicketArchiveStub.stubs(:find_by_id).with(stub.id).returns(stub)
      end

      setup_no_synchronizer_calls(synchronizer)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_no_update(audit, backfiller, rec, reason: 'UncleanScrubbed')
    end

    it 'continues on if attachment is for old SuspendedTicket' do
      # changing the source_type re-evaluates the stores during callbacks :(
      rec.update_columns(source_type: 'SuspendedTicket', created_at: Time.now - 300.days)

      setup_no_synchronizer_calls(synchronizer)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_no_update(audit, backfiller, rec, reason: 'SuspendedTicket')
    end

    it 'continues on if attachment is missing data' do
      stub_request(:head, %r{eu-west-1\.amazonaws\.com/#{rec.s3eu.full_filename}}).to_return(status: 404)

      setup_no_synchronizer_calls(synchronizer)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_no_update(audit, backfiller, rec, reason: 'MissingData')
    end

    it 'continues on if thumbnail is missing data' do
      thumb = Attachment.where(parent_id: rec.id).first
      stub_request(:head, %r{eu-west-1\.amazonaws\.com/#{thumb.s3eu.full_filename}}).to_return(status: 404)

      setup_no_synchronizer_calls(synchronizer)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_no_update(audit, backfiller, rec, reason: 'MissingData')
    end

    it 'continues on if stores between rec and thumbnail disagree' do
      thumb = Attachment.where(parent_id: rec.id).first
      thumb.update_attribute(:stores, rec.stores + [:s3apne])

      setup_no_synchronizer_calls(synchronizer)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_no_update(audit, backfiller, rec, reason: 'MismatchTags')
    end

    it 'errors on MissingFromPreferredError if attachment is for newish SuspendedTicket' do
      rec.source_type = 'SuspendedTicket'
      rec.created_at = Time.now - 10.days
      rec.save!

      error = Zendesk::Stores::MissingFromPreferredError.new('new nope')
      setup_synchronizer_error(synchronizer, :rm, error)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_error(audit, backfiller, reason: error)
    end

    it 'errors on MissingFromPreferredError (old closed)' do
      attach_ticket(rec) do |tkt|
        tkt.status_id = StatusType.CLOSED
        tkt.updated_at = Time.new(2016, 12, 31)
        tkt.subject = 'I am an old, closed ticket...fear me!'
      end

      error = Zendesk::Stores::MissingFromPreferredError.new('afeared')
      setup_synchronizer_error(synchronizer, :rm, error)

      backfiller = setup_for_one_rec_run(rec, audit, config, synchronizer)

      verify_error(audit, backfiller, reason: error)
    end
  end

  # Sets up a backfiller and stubs out statsd for a backfill of a single record.  yields the mock statsd so caller
  # can setup expected metrics for the test in question
  def setup_for_one_rec_run(rec, audit, config, synchronizer)
    statsd = Object.new
    config.stubs(:statsd).returns(statsd)

    statsd.expects(:increment).with('model_backfills', tags: %W[model:#{audit.model_class}])

    selector = FakeSelector.new([rec.id])
    Zendesk::Stores::Backfill::ModelBackfiller.new(config, synchronizer, selector)
  end

  def setup_no_synchronizer_calls(synchronizer)
    synchronizer.expects(:add_to_preferred_stores).never
    synchronizer.expects(:remove_from_unwanted_stores).never
  end

  def setup_synchronizer_error(synchronizer, during_call, error)
    if during_call == :add
      synchronizer.expects(:add_to_preferred_stores).raises(error)
      synchronizer.expects(:remove_from_unwanted_stores).never
    else # during remove
      synchronizer.expects(:add_to_preferred_stores).once
      synchronizer.expects(:remove_from_unwanted_stores).raises(error)
    end
  end

  # Verifies that backfill for one updated record is processed correctly
  def verify_update(audit, backfiller, rec, adds: [], removes: [])
    statsd = backfiller.config.statsd
    adds.each { |s| statsd.expects(:increment).with('adds', tags: %W[model:#{audit.model_class} store:#{s}]) }
    removes.each { |s| statsd.expects(:increment).with('removes', tags: %W[model:#{audit.model_class} store:#{s}]) }
    histupds = 1
    histupds += 1 if adds.present?
    histupds += 1 if removes.present?
    statsd.expects(:histogram).times(histupds) # add + remove + model update
    statsd.expects(:increment).with('updates', tags: %W[model:#{audit.model_class}])

    exp_update = Zendesk::Stores::Backfill::Update.new
    exp_update.increment(true)
    exp_last_id = rec.id
    verify_one_rec_processed(exp_update, exp_last_id, backfiller, audit)
  end

  # Verifies that backfill for one non-updated record is processed correctly
  def verify_no_update(audit, backfiller, rec, reason: 'ok')
    backfiller.config.statsd.expects(:increment).with('noops', tags: %W[model:#{audit.model_class} reason:#{reason}])

    exp_update = Zendesk::Stores::Backfill::Update.new
    exp_update.increment(false)
    exp_last_id = rec.id

    verify_one_rec_processed(exp_update, exp_last_id, backfiller, audit)
  end

  # Verifies that backfill that fails on a record is processed correctly
  def verify_error(audit, backfiller, reason:)
    if reason.is_a?(StandardError)
      backfiller.config.statsd.expects(:increment).with('errors', tags: %W[model:#{Attachment.name} reason:#{reason.class.name}])
    else
      backfiller.config.statsd.expects(:increment).with('errors', tags: %W[model:#{Attachment.name} reason:#{reason}])
    end

    exp_update = Zendesk::Stores::Backfill::Update.new
    exp_update.add_error
    exp_last_id = audit.last_id # Errors do not advance the waterline

    verify_one_rec_processed(exp_update, exp_last_id, backfiller, audit)
  end

  def verify_one_rec_processed(exp_update, exp_last_id, backfiller, audit)
    assert_equal exp_update, run_backfiller(backfiller, audit)

    refute audit.done?
    assert audit.id
    assert_equal exp_update.evaluated, audit.total_evaluated
    assert_equal exp_update.updated, audit.total_updated
    assert_equal exp_last_id, audit.last_id
  end

  def run_backfiller(backfiller, audit)
    stop_check = lambda { false }
    backfiller.backfill_model(audit, stop_check)
  end

  def create_attachment(account, stores)
    attachment = Attachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/normal_1.jpg"))
    attachment.author = account.users.last
    attachment.account = account
    attachment.stores = stores
    attachment.created_at = Time.now - 1.day
    attachment.save!
    attachment
  end

  def attach_ticket(rec)
    tkt = Ticket.new
    tkt.id = Random.rand(100000000)
    rec.ticket_id = tkt.id
    yield tkt if block_given?
    rec.save!
    Ticket.stubs(:find_by_id).with(tkt.id).returns(tkt)
  end

  class FakeSelector
    def initialize(ids)
      @ids = ids
    end

    def get_ids(_audit)
      @ids
    end
  end
end
