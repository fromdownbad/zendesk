require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Stores::Backfill::PriorityOrdering do
  let(:ids) { (1..100).to_a.shuffle }
  let(:priority_ids) { (1..100).select { |id| id % 10 == 0 }.shuffle }
  let(:sorter) { Zendesk::Stores::Backfill::PriorityOrdering.new(priority_ids) }

  it "it sorts priority before non-priority" do
    sorted = sorter.sort(ids)
    assert_equal ids.length, sorted.length
    sorted.slice(0, priority_ids.length).each do |pid|
      assert priority_ids.include?(pid), "found non-priority-id #{pid} before priority id"
    end
    assert_equal ids.sort, sorted.sort
  end

  it "handles empty priority" do
    sorter = Zendesk::Stores::Backfill::PriorityOrdering.new([])
    assert_equal ids.sort, sorter.sort(ids).sort
  end

  it "handles empty ids" do
    assert_equal [], sorter.sort([]).sort
  end

  it "it shuffles within priority and non-priority" do
    first = sorter.sort(ids)

    found = false
    100.times do
      sorted = sorter.sort(ids)
      if sorted != first
        found = true
        break
      end
    end

    assert found, "found ids in different order"
  end
end
