require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 4 # after bail in loop

describe Zendesk::Stores::Backfill::StoresBackfiller do
  fixtures :accounts

  let(:feature) { Zendesk::Stores::Backfill::StoresBackfillConfig::DISABLE_FEATURE }
  let(:default_config) { Zendesk::Stores::Backfill::StoresBackfillConfig.new }
  let(:disabled_account_helper) { Zendesk::ShardMover::DisabledAccountHelper.new(feature) }

  it 'will stand up' do
    backfiller = Zendesk::Stores::Backfill::StoresBackfiller.create_backfiller
    # ensures the lambda in create is flexed
    refute_empty backfiller.instance_variable_get(:@accounts_fetcher).call(1, []).to_a
  end

  describe 'for accounts on shard' do
    let(:a_min) { accounts(:minimum) }
    let(:a_sup) { accounts(:support) }
    let(:a_min_upd) { Zendesk::Stores::Backfill::Update.new(55, 33) }
    let(:a_sup_upd) { Zendesk::Stores::Backfill::Update.new(44, 21) }
    let(:backfiller) do
      account_ids = [accounts(:minimum).id, accounts(:support).id]
      priority_ids = []
      account_backfiller = FakeAccountBackfiller.new(a_min.id => a_min_upd, a_sup.id => a_sup_upd)

      setup_backfiller(default_config, account_ids, priority_ids, account_backfiller)
    end

    describe_with_arturo_disabled :stores_backfill do
      it 'will not backfill those accounts' do
        assert_equal Zendesk::Stores::Backfill::Update.new, backfiller.run
      end
    end

    describe_with_arturo_enabled :stores_backfill do
      it 'will backfill those accounts' do
        assert_equal a_min_upd + a_sup_upd, backfiller.run
      end

      it 'will not backfill done accounts' do
        audit = StoresBackfillAudit.new_audit(a_sup.id, Attachment.name, [:s3])
        audit.done!
        audit.save!

        assert_equal a_min_upd, backfiller.run
      end
    end
  end

  describe 'for shard-priority' do
    let(:account_ids) { (1..20).to_a }
    let(:priority_ids) { account_ids.select { |id| id % 4 == 0 } }
    let(:backfiller) do
      account_updates = account_ids.map { |id| [id, Zendesk::Stores::Backfill::Update.new] }.to_h
      account_backfiller = FakeAccountBackfiller.new(account_updates)

      setup_backfiller(default_config, account_ids, priority_ids, account_backfiller, skip_disable_check: true)
    end

    it 'will base shard-priority on incomplete priority accounts' do
      default_config.expects(:weighted_shard_allowance).with(true).returns(default_config.shard_allowance)
      backfiller.run
    end

    it 'will not base shard-priority on completed priority accounts' do
      priority_ids.each do |acct_id|
        audit = StoresBackfillAudit.new_audit(acct_id, Attachment.name, [:s3])
        audit.done!
        audit.save!
      end

      default_config.expects(:weighted_shard_allowance).with(false).returns(default_config.shard_allowance)
      backfiller.run
    end
  end

  describe 'will prioritize locality accounts' do
    let(:account_ids) { (1..20).to_a }
    let(:priority_ids) { (1..20).select { |id| id % 4 == 0 } }
    let(:account_updates) { account_ids.map { |id| [id, Zendesk::Stores::Backfill::Update.new] }.to_h }
    let(:account_backfiller) { FakeAccountBackfiller.new(account_updates) }

    it 'will backfill priority accounts before non-priority accounts' do
      backfiller = setup_backfiller(default_config, account_ids, priority_ids, account_backfiller, skip_disable_check: true)
      backfiller.run

      # Verify priority ids were first backfilled:
      assert_equal priority_ids.sort, account_backfiller.passed_accounts.slice(0, priority_ids.length),
        'priority accts should be backfilled first'
      assert_equal account_ids.sort, account_backfiller.passed_accounts.sort,
        'all accounts should be backfilled'

      # Verify only priority accounts were flagged as priority to the account backfiller:
      passed_priority_flag = account_backfiller.passed_priorities.slice(0, priority_ids.length).uniq
      assert_equal [true], passed_priority_flag, 'priority accounts should be flagged'

      non_priority_count = account_ids.length - priority_ids.length
      passed_non_priority_flag = account_backfiller.passed_priorities.slice(priority_ids.length, non_priority_count).uniq
      assert_equal [false], passed_non_priority_flag, 'non priority accounts should not be flagged'
    end

    it 'will not backfill non-priority accounts if priority weight is 1' do
      config = default_config
      config.priority_weight = 1.0
      backfiller = setup_backfiller(config, account_ids, priority_ids, account_backfiller, skip_disable_check: true)
      backfiller.run

      # Verify priority ids were first backfilled:
      assert_equal priority_ids.sort, account_backfiller.passed_accounts, 'priority accts should be backfilled'

      # Verify priority accounts were flagged as priority to the account backfiller:
      passed_priority_flag = account_backfiller.passed_priorities.uniq
      assert_equal [true], passed_priority_flag, 'priority accounts should be flagged'
    end
  end

  describe 'for accounts with EU/FRA DC addon' do
    let(:eu_account) { accounts(:minimum) }
    let(:de_account) { accounts(:trial) }
    let(:otr_account) { accounts(:support) }

    before do
      eu_account.subscription_feature_addons.create!(
        name: 'eu_data_center', zuora_rate_plan_id: '123'
      )
      de_account.subscription_feature_addons.create!(
        name: 'germany_data_center', zuora_rate_plan_id: '456'
      )
    end

    it 'picks up priority ids in priority region' do
      config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 0.5)
      config.stubs(:in_priority_region).returns(true)

      priority_ids = Zendesk::Stores::Backfill::StoresBackfiller.new(config, nil, nil).send(:fetch_priority_ids)

      assert_includes priority_ids, eu_account.id
      assert_includes priority_ids, de_account.id
      refute_includes priority_ids, otr_account.id
    end

    it 'ignores priority ids in non-priority region' do
      config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 0.5)
      config.stubs(:in_priority_region).returns(false)

      priority_ids = Zendesk::Stores::Backfill::StoresBackfiller.new(config, nil, nil).send(:fetch_priority_ids)

      assert_empty priority_ids
    end
  end

  it 'correctly picks up shards' do
    ActiveRecord::Base.stubs(:shard_names).returns((1..10).to_a)
    workers = (1..3).map do |n|
      Zendesk::Stores::Backfill::StoresBackfiller.create_backfiller(worker_number: n, worker_count: 3)
    end

    # Is there any way to build this more sensibly than:
    # exp = (1..10).map { |n| [ n%3, n ] }.group_by(&:first).values.map { |a| a.flat_map(&:last) }
    exps = [[1, 4, 7, 10], [2, 5, 8], [3, 6, 9]]
    (0..2).each do |n|
      assert_equal n + 1, workers[n].config.worker_number, "Wrong worker number for workers[#{n}]"
      assert_equal exps[n], workers[n].send(:shard_ids).sort, "wrong shards for worker #{n + 1}"
    end
  end

  it 'rotates shards' do
    Random.srand(1234)
    ActiveRecord::Base.stubs(:shard_names).returns((1..100).to_a)
    backfiller = Zendesk::Stores::Backfill::StoresBackfiller.create_backfiller

    found = false
    1000.times do
      found = backfiller.send(:shard_ids)[0] != 1
      break if found
    end

    assert found
  end

  describe_with_arturo_enabled :stores_backfill do
    it 'times out shards' do
      a_sup = accounts(:support)
      a_min = accounts(:minimum)
      a_mult = accounts(:multiproduct)
      a_rpt = accounts(:payment)

      a_sup_upd = Zendesk::Stores::Backfill::Update.new(100, 5)
      a_min_upd = Zendesk::Stores::Backfill::Update.new(50, 50)
      acct_backfiller = FakeAccountBackfiller.new(a_sup.id => [0.01.second, a_sup_upd], a_min.id => [0.1.seconds, a_min_upd])

      account_ids_fetcher = lambda { |_, _| [a_sup.id, a_min.id, a_mult.id, a_rpt.id] }

      config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(shard_allowance: 0.1.seconds)
      backfiller = Zendesk::Stores::Backfill::StoresBackfiller.new(config, account_ids_fetcher, acct_backfiller)
      assert_equal a_sup_upd + a_min_upd, backfiller.run
    end
  end

  describe 'with the kill switch' do
    let(:pending_account) { accounts(:minimum) }
    let(:disabled_account) { accounts(:support) }
    let(:flipped_account) { accounts(:trial) }

    before do
      Zendesk::ShardMover::Accounts.disable_feature(feature, pending_account)
      Zendesk::ShardMover::Accounts.ack_disable_feature(feature, disabled_account)
      Zendesk::ShardMover::Accounts.enable_feature(feature, flipped_account)
    end

    it 'acknowledges disable requests when kill switch is on' do
      config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(disable_nap: 0.1.second)
      config.stubs(:kill_switch_on?).returns(true)

      accounts_fetcher = lambda do |shard_id, _|
        Zendesk::ShardMover::AccountsOnShard.new(disabled_account_helper, shard_id, 0.1.second)
      end

      Zendesk::ShardMover::AccountsOnShard.any_instance.
        stubs(:account_ids).
        returns([pending_account.id, disabled_account.id, flipped_account.id])

      account_backfiller = FakeAccountBackfiller.new({})
      account_backfiller.expects(:backfill_account).never

      backfiller = Zendesk::Stores::Backfill::StoresBackfiller.new(config, accounts_fetcher, account_backfiller)
      backfiller.run

      disabled = disabled_account_helper.disabled_accounts_on_shard
      assert_includes disabled, pending_account.id
      assert_includes disabled, disabled_account.id
      refute_includes disabled, flipped_account.id

      Zendesk::ShardMover::Accounts.disable_feature(feature, flipped_account)
      backfiller.run

      disabled = disabled_account_helper.disabled_accounts_on_shard
      assert_includes disabled, pending_account.id
      assert_includes disabled, disabled_account.id
      assert_includes disabled, flipped_account.id
    end
  end

  def setup_backfiller(config, account_ids, priority_ids, account_backfiller, skip_disable_check: false)
    # Set up a fetcher that will return ids as active ids for shard and use priority_ids as priority account-ids
    id_fetcher = lambda do |sid, pids|
      sorter = Zendesk::Stores::Backfill::PriorityOrdering.new(pids)
      accounts_on_shard = Zendesk::ShardMover::AccountsOnShard.new(
        disabled_account_helper, sid, 1.second, lambda { |acct_ids| sorter.sort(acct_ids) }
      )
      accounts_on_shard.expects(:active_accounts_on_shard).returns(account_ids.to_a)
      accounts_on_shard
    end

    config.stubs(:in_priority_region).returns(true)
    backfiller = Zendesk::Stores::Backfill::StoresBackfiller.new(config, id_fetcher, account_backfiller)
    backfiller.stubs(:fetch_priority_ids).returns(priority_ids)
    backfiller.stubs(:account_disabled?).returns(false) if skip_disable_check
    backfiller
  end

  class FakeAccountBackfiller
    attr_reader :passed_accounts, :passed_priorities

    def initialize(accounts)
      @accounts = accounts
      @passed_accounts = []
      @passed_priorities = []
    end

    def backfill_account(account_id, priority)
      @passed_accounts << account_id
      @passed_priorities << priority

      raise "account #{account_id} not set up in #{self.class.name}" unless @accounts[account_id]

      v = @accounts.delete(account_id)
      return v if v.is_a? Zendesk::Stores::Backfill::Update
      sleep v.first # rubocop:disable Lint/Sleep
      v.second
    end
  end
end
