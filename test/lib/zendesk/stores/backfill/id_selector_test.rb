require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Stores::Backfill::IdSelector do
  fixtures :accounts

  before do
    stub_request(:get, %r{https://zendesk-test.*.amazonaws.com/.*})
    stub_request(:put, %r{https://zendesk-test.*.amazonaws.com/.*})
    stub_request(:delete, %r{https://zendesk-test.*.amazonaws.com/.*})
  end

  let(:account) { accounts(:minimum) }
  let(:config) { Zendesk::Stores::Backfill::StoresBackfillConfig.new(batch_size: 10, xfiles_supporting_models: [Attachment.name]) }
  let(:selector) { Zendesk::Stores::Backfill::IdSelector.new(config) }

  describe "for attachments" do
    describe "without xfiles concern" do
      let(:store) { :s3 }

      before do
        @att_ids = setup_attachments(account, (0..29).map { [store] }).sort
      end

      it "will not select recs in preferred stores" do
        audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3])
        assert_equal [], selector.get_ids(audit)
      end

      it "selects batch of records with highest ids < last_id in desc order" do
        audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])

        exp_batches = build_batches(@att_ids)
        act_batches = get_all_selected(audit, selector)

        exp_batches.each_index do |n|
          assert_equal exp_batches[n], act_batches[n], "batch #{n}"
        end

        audit.done!
        assert_equal [], selector.get_ids(audit)
      end

      it "does not include thumbnails" do
        audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])

        assert_equal @att_ids.size * 2, Attachment.where(account_id: account.id).count
        thumb_ids = Attachment.where(account_id: account.id).where("parent_id is not null").pluck(:id)

        act_batches = get_all_selected(audit, selector)
        act_batches.each do |batch|
          batch.each do |id|
            refute_includes thumb_ids, id
          end
        end
      end

      it "will not select recs that have no stores" do
        audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3eu])
        upd_ids = @att_ids[25..29]
        Attachment.where(id: upd_ids).update_all(stores: '')
        assert_equal @att_ids[15..24].reverse, selector.get_ids(audit)
      end

      it "will not select recs in scoped preferred stores" do
        scoped_store = Technoweenie::AttachmentFu::ScopedStores.linked_store_name(store)
        audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [scoped_store])
        assert_equal [], selector.get_ids(audit)
      end

      describe "stored in a scoped store" do
        let(:store) { Technoweenie::AttachmentFu::ScopedStores.linked_store_name(:s3) }

        it "will not select recs in unscoped preferred stores" do
          audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [:s3])
          assert_equal [], selector.get_ids(audit)
        end
      end
    end

    describe "with xfiles concern" do
      let(:default_store) { :s3 }
      let(:desired_store) { :s3eu }

      before do
        scoped_store = Technoweenie::AttachmentFu::ScopedStores.scope_store_name(desired_store)
        scoped_default = Technoweenie::AttachmentFu::ScopedStores.scope_store_name(default_store)
        stores = {
          2 => [scoped_default],
          6 => [desired_store, :fs, :xfiles],
          7 => [:fs, :xfiles, desired_store],
          8 => [:xfiles, :fs, desired_store],
          12 => [:xfiles, desired_store, default_store],
          13 => [default_store, :xfiles, desired_store],
          16 => [desired_store, :fs],
          17 => [:fs, desired_store],
          19 => [:xfiles, scoped_store], # no-bf if rgn is scoped version of current
          20 => [:xfiles],
          21 => [desired_store], # no-bf if rgn == s3eu
          22 => [default_store],
          23 => [desired_store, scoped_store], # no-bf if rgn is scoped version of current
          25 => [desired_store, :xfiles], # no-bf if rgn == s3eu
          26 => [:xfiles, desired_store], # no-bf if rgn == s3eu
          28 => [default_store, :xfiles],
          29 => [:xfiles, default_store],
        }
        stores.default = [default_store]

        @att_ids = setup_attachments(account, (0..29).map { |i| stores[i] }).sort

        @excludes_region_s3eu = [21, 25, 26].map { |n| @att_ids[n] }
        @excludes_scoped = [19, 23].map { |n| @att_ids[n] }
      end

      it "For 1 store, excludes records in store and optionally in x-files" do
        audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [desired_store])

        exp_batches = build_batches(@att_ids - @excludes_region_s3eu - @excludes_scoped)
        act_batches = get_all_selected(audit, selector)

        exp_batches.each_index do |n|
          assert_equal exp_batches[n], act_batches[n], "batch #{n}"
        end
      end

      it "For 2 stores, does not exclude by store" do
        audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, [desired_store, default_store])

        exp_batches = build_batches(@att_ids)
        act_batches = get_all_selected(audit, selector)

        exp_batches.each_index do |n|
          assert_equal exp_batches[n], act_batches[n], "batch #{n}"
        end
      end
    end

    describe "for voice_uploads" do
      describe "with 1 store" do
        before do
          Voice::Upload.delete_all(account_id: account.id)
          @voice_id = create_voice_upload(account, [:s3]).id
        end

        it "selects recs not in region" do
          audit = StoresBackfillAudit.new_audit(account.id, Voice::Upload.name, [:s3eu])
          assert_equal [@voice_id], selector.get_ids(audit)
        end

        it "skips recs in region" do
          audit = StoresBackfillAudit.new_audit(account.id, Voice::Upload.name, [:s3])
          assert_equal [], selector.get_ids(audit)
        end
      end

      describe "with 2 region stoers" do
        before do
          Voice::Upload.delete_all(account_id: account.id)
          @voice_id = create_voice_upload(account, [:s3, :s3eu]).id
        end

        it "does not exclude by store" do
          audit = StoresBackfillAudit.new_audit(account.id, Voice::Upload.name, [:s3, :s3eu])
          assert_equal [@voice_id], selector.get_ids(audit)
        end
      end
    end

    # repeatedly calls selector and updates audit for each batch of ids
    # until nothing is returned by selector
    def get_all_selected(audit, selector)
      batches = []
      loop do
        batch = selector.get_ids(audit)
        batches << batch
        break if batch.empty?
        audit.update_progress(batch.size, 0, batch.last)
      end
      batches
    end

    def build_batches(exp_ids)
      base = exp_ids.dup.sort
      ret = []
      loop do
        if base.size < 10
          ret << base.reverse
          break
        end
        ret << base.pop(10).reverse
        break if base.empty?
      end
      ret << []
      ret
    end

    # Pulls apart batches of expected vs actual attachment ids
    # to show more details about the records
    def check_batches(exp_batches, act_batches)
      exp_batches.each_index do |n|
        next if exp_batches[n] == act_batches[n]
        sort_map = lambda { |rslt| rslt.to_a.sort { |a, b| b.id <=> a.id }.map { |a| [a.id, a.stores] } }
        exps = sort_map.call(Attachment.where(id: exp_batches[n]))
        acts = sort_map.call(Attachment.where(id: act_batches[n]))
        exps.each_index do |i|
          assert_equal exps[i], acts[i], "batch #{n}, rec #{i} does not match; exp batch ids #{exp_batches[n]} vs actual batch ids #{act_batches[n]}"
        end
      end
    end

    # creates attachment for each store in given array and returns corresponding array of att-ids
    def setup_attachments(account, stores)
      Attachment.delete_all(account_id: account.id)
      stores.map do |store|
        create_attachment(account, store).id
      end
    end

    def create_attachment(account, in_stores)
      stores = in_stores.dup
      need_xfiles = !stores.delete(:xfiles).nil?
      attachment = Attachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/normal_1.jpg"))
      attachment.author = account.users.last
      attachment.account = account

      if stores.present?
        attachment.stores = stores
        attachment.save!
        if need_xfiles
          withxfiles = stores | [:xfiles]
          attachment.update_column(:stores, withxfiles.join(','))
        end
      else # only store was xfiles
        # need a "real" store to save!
        attachment.stores = [:fs]
        attachment.save!
        attachment.update_column(:stores, 'xfiles')
      end

      attachment
    end

    def create_voice_upload(account, stores)
      upload = Voice::Upload.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new('test/files/dtmf-1.mp3'))
      upload.account = account
      upload.stores = stores
      upload.stubs(:ensure_valid_audio_file).returns(true)
      upload.save!
      upload
    end
  end
end
