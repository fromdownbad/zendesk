require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Stores::Backfill::StoresBackfillConfig do
  let(:defaults) { Zendesk::Stores::Backfill::StoresBackfillConfig::DEFAULT_OPTS }
  let(:default_backfill_classes) { Zendesk::Stores::Backfill::StoresBackfillConfig::BACKFILL_CLASSES }
  let(:default_config) { Zendesk::Stores::Backfill::StoresBackfillConfig.new }

  it "initializes correctly" do
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new

    assert_equal false, config.dry_run
    assert_equal false, config.loop
    assert_equal defaults[:worker_count], config.worker_count
    assert_equal default_backfill_classes, config.backfill_classes
    assert_equal [Attachment.name], config.xfiles_supporting_models
    assert_equal 4.minutes, config.account_allowance
  end

  it "overrides default" do
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(dry_run: false,
                                                                 loop: true,
                                                                 backfill_classes: [Attachment.name],
                                                                 account_allowance: 3.minutes)

    assert_equal false, config.dry_run
    assert(config.loop)
    assert_equal defaults[:worker_count], config.worker_count
    assert_equal [Attachment.name], config.backfill_classes
    assert_equal 3.minutes, config.account_allowance

    assert config.statsd.is_a?(Zendesk::StatsD::Client)
  end

  it "limits account_allowance" do
    config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(account_allowance: 5.minutes)
    assert_equal 4.minutes, config.account_allowance
  end

  it "verifies worker assignment" do
    assert_raises ArgumentError do
      Zendesk::Stores::Backfill::StoresBackfillConfig.new(worker_number: -1, worker_count: 5)
    end

    assert_raises ArgumentError do
      Zendesk::Stores::Backfill::StoresBackfillConfig.new(worker_number: 6, worker_count: 5)
    end
  end

  it 'knows it is disabled' do
    Arturo.stubs(:feature_enabled_for_pod?).
      with(:disable_sync_attachments, 1).
      returns(true, true, false)
    assert default_config.kill_switch_on?
    assert default_config.kill_switch_on?
    refute default_config.kill_switch_on?
  end

  describe 'in a region with no need for priority' do
    before do
      pod_id = Zendesk::Configuration.fetch(:pod_id)
      Zendesk::Configuration.stubs(:dig).with(:pods, pod_id, :region).returns("not a priority region I hope")
    end

    it 'cannot be configured to only backfill priority' do
      refute Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 1.0).only_priority?
    end

    it 'does not use priority' do
      refute Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 0.5).using_priority?
      refute Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 0).using_priority?
    end

    it 'uses full allowances for priority and non-priority accounts and shards if not using priority' do
      assert_equal default_config.shard_allowance, default_config.weighted_shard_allowance(false)
      assert_equal default_config.shard_allowance, default_config.weighted_shard_allowance(true)
      assert_equal default_config.account_allowance, default_config.weighted_account_allowance(false)
      assert_equal default_config.account_allowance, default_config.weighted_account_allowance(true)
    end
  end

  describe 'in a priority region' do
    before do
      pod_id = Zendesk::Configuration.fetch(:pod_id)
      Zendesk::Configuration.stubs(:dig).with(:pods, pod_id, :region).returns("emea")
    end

    it 'can be configured to only backfill priority' do
      assert Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 1.0).only_priority?
    end

    it 'bases using priority on priority_weight' do
      assert Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 0.5).using_priority?
      refute Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 0).using_priority?
    end

    it 'bounds priority weighting' do
      assert_equal 0, Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: -0.001).priority_weight
      assert_equal 1, Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 1.001).priority_weight
    end

    it 'weights allowance for shards with priority accounts' do
      assert_equal default_config.shard_allowance, default_config.weighted_shard_allowance(true)
      assert_equal default_config.shard_allowance * (1 - default_config.priority_weight), default_config.weighted_shard_allowance(false)
    end

    it 'weights allowance for priority accounts' do
      assert_equal default_config.account_allowance, default_config.weighted_account_allowance(true)
      assert_equal default_config.account_allowance * (1 - default_config.priority_weight), default_config.weighted_account_allowance(false)
    end

    it 'can zero out allowance' do
      config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(priority_weight: 1)
      assert_equal 0, config.weighted_account_allowance(false)
      assert_equal 0, config.weighted_shard_allowance(false)
    end
  end

  describe 'model classes override' do
    it 'accepts valid model classes' do
      config = Zendesk::Stores::Backfill::StoresBackfillConfig.new(backfill_classes: ['Attachment'])

      assert_equal ['Attachment'], config.backfill_classes
    end

    it 'validates the model classes exist' do
      error = assert_raises(ArgumentError) do
        Zendesk::Stores::Backfill::StoresBackfillConfig.new(backfill_classes: ['FakeClass'])
      end
      assert_match /#{Regexp.quote("Non-existant class found! uninitialized constant FakeClass")}/, error.message
    end
  end
end
