require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 6 # initialize_logger

describe Zendesk::Stores::Backfill::StoresBackfillConfig do
  let(:config) { Zendesk::Stores::Backfill::StoresBackfillConfig.new }
  let(:logger) { Zendesk::Stores::Backfill::StoresBackfillLogger.new(config) }

  it 'can create a logger' do
    assert_not_nil logger.send(:json_logger)
  end

  it 'can log a msg' do
    logger.info("hello from test", x: 'is x', y: 'is not')
  end
end
