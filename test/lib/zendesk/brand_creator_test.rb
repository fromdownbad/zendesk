require_relative "../../support/test_helper"
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered!

describe Zendesk::BrandCreator do
  include MultiproductTestHelper
  fixtures :accounts, :routes, :brands

  let(:account) { accounts(:minimum) }
  let(:brand_creator) { Zendesk::BrandCreator.new(account) }
  let(:brand) { brand_creator.brand }

  describe '#brand' do
    it 'returns initialized brand with route' do
      refute brand.persisted?
      refute brand.route.persisted?
    end

    it 'does not assign attributes to intialized objects' do
      assert_nil brand.name
      assert_nil brand.route.subdomain
    end
  end

  describe '#create!' do
    let(:brand_route) { brand.route }

    describe "for new Support-first account creation" do
      before do
        before_create_state(account)
        brand_creator.create!
      end

      it "creates a default brand and an agent routing route" do
        assert_equal [brand_route], account.routes
        assert_equal [brand], account.brands
        assert_equal brand_route, account.route
        assert_equal brand, account.default_brand
      end

      it "creates the brand and route with default options from account" do
        assert_equal brand_route.subdomain, account.subdomain
        assert_equal brand.name, account.name
        assert_equal_with_nil brand_route.host_mapping, account.host_mapping
        assert_equal Brand::DEFAULT_SIGNATURE_TEMPLATE, brand.signature_template
      end
    end

    describe "for shell account" do
      let(:account) { setup_shell_account(subdomain: 'shell-account', account_name: 'shell account') }
      let(:brand_route) { account.route }
      let(:brand_creator) { Zendesk::BrandCreator.new(account, brand_route) }

      before do
        assert brand_route
        assert account.routes.any?
        refute account.brands.any?
      end

      it 'creates a default brand using existing route' do
        brand_creator.create!
        assert_equal brand, account.default_brand
        assert_equal brand.route, account.route
      end
    end

    describe_with_arturo_enabled :brand_creator_read_from_master do
      let(:account_in_replica) { setup_shell_account(subdomain: 'shell-account-replica', account_name: 'shell account replica') }
      let(:account_in_master) { setup_shell_account(subdomain: 'shell-account-master', account_name: 'shell account master') }
      let(:brand_creator) { Zendesk::BrandCreator.new(account_in_replica) }

      describe 'with default brand' do
        it 'reads account from master and creates a default brand using existing route' do
          Zendesk::BrandCreator.any_instance.expects(:find_account).with(account_in_replica.id).returns(account_in_master)

          brand_creator.create!
          assert_equal brand_creator.brand, account_in_master.default_brand
          assert_equal brand_creator.brand.route, account_in_master.route
        end
      end
    end
  end

  def before_create_state(account)
    Route.without_arsi.delete_all
    Brand.without_arsi.delete_all
    RecipientAddress.without_arsi.delete_all
    account.update_column(:route_id, nil)
  end
end
