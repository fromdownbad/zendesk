require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::AnswerBotService::InternalApiClient do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:api_client) { Zendesk::AnswerBotService::InternalApiClient.new(account) }
  let(:filtered_article_ids) { ["32", "82"] }

  connection_errors = [
    SocketError.new("getaddrinfo: Temporary failure in name resolution"),
    SocketError.new("getaddrinfo: Name or service not known"),
    Net::OpenTimeout,
    Errno::ETIMEDOUT,
    Errno::ECONNRESET,
    Errno::EHOSTUNREACH,
    Faraday::TimeoutError,
    Faraday::Error::TimeoutError,
    OpenSSL::SSL::SSLError
  ]

  describe '#deflect' do
    let(:label_names) { nil }
    let(:deflection_channel_id) { ViaType.MAIL }
    let(:trigger_id) { 123 }
    let(:prediction_response) do
      { 'model_meta' => { 'name' => 'megamike_default', 'version' => '2' },
        'prediction' => [
          {'article_id' => '82', 'brand_id' => '123123', 'score' => 0.824323313970194},
          {'article_id' => '32', 'brand_id' => '123123', 'score' => 0.7658716293400664}
        ]}
    end
    let(:request_body) do
      {
        ticket_id: ticket.nice_id,
        user_id: ticket.requester_id,
        brand_id: ticket.brand_id,
        subject: 'minimum 1 ticket',
        description: 'minimum 1 ticket',
        automatic_answers_threshold: Zendesk::Types::PredictionThresholdType.Balanced,
        label_names: label_names,
        via_id: ticket.via_id,
        deflection_channel_id: deflection_channel_id,
        deflection_channel_reference_id: trigger_id
      }
    end
    let(:deflection_call) do
      api_client.deflect(
        ticket: ticket,
        label_names: label_names,
        deflection_channel_id: deflection_channel_id,
        deflection_channel_reference_id: trigger_id
      )
    end

    before do
      stub_request(:post, 'https://minimum.zendesk-test.com/api/v2/answer_bot/deflection').
        with(body: JSON.dump(request_body)).
        to_return(status: 200, body: prediction_response.to_json, headers: { 'Content-Type' => 'application/json' })
    end

    it 'uses a kragle connection' do
      KragleConnection.expects(:build_for_answer_bot_service).with(account)

      deflection_call
    end

    it 'sends request with the right params' do
      assert_equal prediction_response, deflection_call
    end

    describe 'when Answer Bot Service is unavailable' do
      connection_errors.each do |ex|
        describe "with handled exception #{ex}" do
          it 'returns empty hash' do
            ZendeskExceptions::Logger.expects(:record).never
            api_client.stubs(:connection).raises(ex)

            assert_equal({}, deflection_call)
          end
        end
      end

      describe 'with unhandled exception' do
        it 'pokes exception logger and returns empty hash' do
          ZendeskExceptions::Logger.expects(:record).once
          api_client.expects(:connection).raises(Kragle::BadGateway)

          assert_equal({}, deflection_call)
        end
      end
    end

    describe 'when Answer Bot Service responds with 4xx' do
      def stub_4xx_status(context)
        Rails.logger.stubs(:warn)
        stub_request(:post, 'https://minimum.zendesk-test.com/api/v2/answer_bot/deflection').
          with(body: JSON.dump(request_body)).
          to_return(status: context[:status], body: { error: context[:error] }.to_json, headers: { 'Content-Type' => 'application/json' })
      end

      [
        { status: 422, error: 'invalid',          class: Kragle::UnprocessableEntity },
        { status: 400, error: 'invalid_via_type', class: Kragle::BadRequest          },
        { status: 403, error: 'invalid_plan',     class: Kragle::Forbidden           },
        { status: 404, error: 'ticket_not_found', class: Kragle::ResourceNotFound    },
      ].each do |context|
        it 'returns empty hash' do
          stub_4xx_status(context)
          assert_equal({}, deflection_call)
        end

        it 'logs warning without raising exception' do
          stub_4xx_status(context)
          ZendeskExceptions::Logger.expects(:record).never
          Rails.logger.expects(:warn).with(regexp_matches(%r{ticket_deflection.batch_analytics.client_error -- #{context[:class]} -- }))

          deflection_call
        end
      end
    end
  end

  describe '#delete_embeddings' do
    before do
      stub_request(:delete, "https://minimum.zendesk-test.com/api/v2/answer_bot/embeddings/private/articles/account/#{account.id}/destroy").
        to_return(status: 200)
    end

    it 'sends requests to delete embeddings' do
      assert api_client.delete_embeddings
    end
  end

  describe '#available' do
    let(:deflection_channel_id) { Zendesk::Types::ViaType.ANSWER_BOT_FOR_SDK }
    let(:availability_call) { api_client.available(deflection_channel_id) }

    before do
      stub_request(:get, "https://minimum.zendesk-test.com/api/v2/answer_bot/availability?deflection_channel_id=#{deflection_channel_id}").
        with(body: {}.to_json).
        to_return(status: 200, body: { available: true }.to_json, headers: { 'Content-Type' => 'application/json' })
    end

    it 'uses a Kragle connection' do
      KragleConnection.expects(:build_for_answer_bot_service).with(account)

      availability_call
    end

    describe 'when Answer Bot Service is unavailable' do
      connection_errors.each do |ex|
        describe "with handled exception #{ex}" do
          it 'returns false' do
            api_client.stubs(:connection).raises(ex)
            refute availability_call
          end

          it 'does not log the exception' do
            ZendeskExceptions::Logger.expects(:record).never
            api_client.stubs(:connection).raises(ex)
            availability_call
          end
        end
      end
    end

    describe 'when Answer Bot Service responds with 4xx' do
      def stub_4xx_status(context)
        Rails.logger.stubs(:warn)
        stub_request(:get, "https://minimum.zendesk-test.com/api/v2/answer_bot/availability?deflection_channel_id=65").
          to_return(status: context[:status], body: { error: context[:error] }.to_json, headers: { 'Content-Type' => 'application/json' })
      end

      [
        { status: 422, error: 'invalid',          class: Kragle::UnprocessableEntity },
        { status: 400, error: 'invalid_via_type', class: Kragle::BadRequest          },
        { status: 403, error: 'invalid_plan',     class: Kragle::Forbidden           },
        { status: 404, error: 'ticket_not_found', class: Kragle::ResourceNotFound    },
      ].each do |context|
        it 'returns false' do
          stub_4xx_status(context)
          refute availability_call
        end

        it 'logs warning without raising exception' do
          stub_4xx_status(context)
          ZendeskExceptions::Logger.expects(:record).never
          Rails.logger.expects(:warn).with(regexp_matches(%r{answer_bot.availability.client_error -- #{context[:class]} -- }))

          availability_call
        end
      end
    end

    describe 'when Answer Bot Service raises an error' do
      it 'logs the error' do
        api_client.stubs(:connection).raises(StandardError)
        ZendeskExceptions::Logger.expects(:record)

        availability_call
      end
    end
  end
end
