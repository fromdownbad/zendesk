require_relative "../../../../support/test_helper"
SingleCov.covered!

describe 'Zendesk::PushNotifications::Payloads::APNS' do
  let(:apns) do
    Zendesk::PushNotifications::Payloads::APNS.new(
      alert: 'Hello world!',
      badge: 1,
      extra_payload: {
        ticket_id: 1,
        user_id: 2
      }
    )
  end

  describe '#generate_payload' do
    it 'generates payload' do
      expected_payload = {
        aps: {
          alert: 'Hello world!',
          badge: 1,
          sound: 'notification.aif',
        },
        id: 2,
        tid: 1
      }
      payload = apns.payload

      assert_equal expected_payload, payload
    end

    it 'generates truncated payload if the max payload limit exceeds' do
      silence_warnings do
        Zendesk::PushNotifications::Payloads::APNS.const_set('MAX_PAYLOAD_SIZE', 82)
      end
      expected_payload = {
        aps: {
          alert: 'Hello…',
          badge: 1,
          sound: 'notification.aif',
        },
        id: 2,
        tid: 1
      }
      payload = apns.payload

      assert_equal expected_payload, payload
    end
  end
end
