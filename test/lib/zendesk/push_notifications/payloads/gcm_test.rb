require_relative "../../../../support/test_helper"
SingleCov.covered!

describe 'Zendesk::PushNotifications::Payloads::GCM' do
  let(:gcm_payload) do
    Zendesk::PushNotifications::Payloads::GCM.new(
      alert: 'Hello world!',
      badge: 1,
      extra_payload: {
        ticket_id: 1,
        user_id: 2,
        enable_collapse_key: true
      }
    )
  end

  describe '#generate_payload' do
    it 'generates payload' do
      expected_payload = {
        data: {
          alert: 'Hello world!',
          extra: { tid: 1 }
        }
      }
      payload = gcm_payload.payload

      assert payload.key?(:collapse_key)
      assert_equal expected_payload[:data], payload[:data]
    end

    it 'generates truncated payload if the max payload limit exceeds' do
      silence_warnings do
        Zendesk::PushNotifications::Payloads::GCM.const_set('MAX_PAYLOAD_SIZE', 80)
      end
      expected_payload = {
        data: {
          alert: 'Hello world!',
          extra: { tid: 1 }
        }
      }
      payload = gcm_payload.payload

      assert payload.key?(:collapse_key)
      refute_equal expected_payload[:data], payload[:data]
    end

    it 'generates payload without collapse_key' do
      expected_payload = {
        data: {
          alert: 'Hello world!',
          extra: { tid: 1 }
        }
      }
      other_payload =
        Zendesk::PushNotifications::Payloads::GCM.new(
          alert: 'Hello world!',
          badge: 1,
          extra_payload: {
            ticket_id: 1,
            user_id: 2,
            enable_collapse_keys: false
          }
        )
      payload = other_payload.payload

      assert_equal expected_payload, payload
    end
  end
end
