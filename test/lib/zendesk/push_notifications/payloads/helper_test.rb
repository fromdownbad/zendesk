require_relative "../../../../support/test_helper"
SingleCov.covered!

describe 'Zendesk::PushNotifications::Payloads::Helper' do
  let(:payload_obj) do
    class PayloadObj
      include Zendesk::PushNotifications::Payloads::Helper
    end

    PayloadObj.new
  end
  let(:msg) { 'Hello world!' }
  let(:empty_payload) do
    {
      aps: { alert: '', badge: 1, sound: 'notification.aif' },
      extra: { id: 2, tid: 1 }
    }
  end

  describe '#truncate_message' do
    it 'does not truncate the message if the message is in limits' do
      expected_message = 'Hello world!'
      new_message = payload_obj.send(:truncate_message, msg, empty_payload, 256)
      assert_equal(expected_message, new_message)
    end

    it 'truncates the message when max payload limit exceeds' do
      expected_message = 'Hello'
      new_message = payload_obj.send(:truncate_message, msg, empty_payload, 90)
      assert_equal expected_message, new_message
    end
  end
end
