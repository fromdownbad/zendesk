require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::PushNotifications::Gdpr::GdprFeedbackPublisher' do
  fixtures :users, :accounts

  let(:account)         { accounts(:minimum) }
  let(:admin)           { users(:minimum_admin) }
  let(:deleted_user)    { users(:minimum_end_user2) }

  let(:compliance_deletion_status) do
    FactoryBot.create(:compliance_deletion_status,
      account: account,
      user: deleted_user)
  end

  let(:sns_publisher)  { Zendesk::PushNotifications::Gdpr::GdprSnsFeedbackPublisher.new(topic_arn: 'topic_arn', region: 'aws-west-1', access_key: 'access_key', secret_key: 'secret_key') }
  let(:gdpr_publisher) { Zendesk::PushNotifications::Gdpr::GdprFeedbackPublisher.new(compliance_deletion_status, sns_publisher) }

  let(:gdpr_message) do
    {
      user_id: deleted_user.id,
      account_id: deleted_user.account_id,
      account_subdomain: deleted_user.account.subdomain,
      action: ComplianceDeletionFeedback::COMPLETE,
      application: ComplianceDeletionStatus::CLASSIC,
      pod: deleted_user.account.pod_id
    }
  end

  before do
    ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
  end

  describe('#publish') do
    it 'should call publish on sns_feedback_publisher' do
      sns_publisher.expects(:publish).with(subject: "GDPR Feedback", message: gdpr_message).once

      gdpr_publisher.publish(ComplianceDeletionFeedback::COMPLETE)
    end
  end
end
