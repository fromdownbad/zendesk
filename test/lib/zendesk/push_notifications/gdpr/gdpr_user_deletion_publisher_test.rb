require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::PushNotifications::Gdpr::GdprUserDeletionPublisher' do
  fixtures :users, :accounts

  let(:account)         { accounts(:minimum) }
  let(:admin)           { users(:minimum_admin) }
  let(:deleted_user)    { users(:minimum_end_user2) }

  let(:compliance_deletion_status) do
    FactoryBot.create(:compliance_deletion_status,
      account: account,
      user: deleted_user)
  end

  let(:sns_publisher)  { Zendesk::PushNotifications::Gdpr::GdprSnsUserDeletionPublisher.new(topic_arn: 'topic_arn', region: 'aws-west-1', access_key: 'access_key', secret_key: 'secret_key') }
  let(:gdpr_publisher) { Zendesk::PushNotifications::Gdpr::GdprUserDeletionPublisher.new(compliance_deletion_status, sns_publisher) }

  let(:gdpr_message) do
    {
      user_id: deleted_user.id,
      account_id: deleted_user.account_id,
      account_subdomain: deleted_user.account.subdomain,
      action: ComplianceDeletionStatus::REQUEST_DELETION,
      application: ComplianceDeletionStatus::ALL_APPLICATONS,
      executer_id: admin.id,
      pod: account.pod_id
    }
  end

  before do
    ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
  end

  describe('#publish') do
    it 'should call publish on sns_user_deletion_publisher' do
      sns_publisher.expects(:publish).with(subject: "GDPR Request", message: gdpr_message).once

      gdpr_publisher.publish
    end

    it 'should not call GdprSnsFeedbackPublisher#publish when inactive' do
      sns_publisher.expects(:publish).with(subject: "GDPR Request", message: gdpr_message).never
      account.stubs(:is_active?).returns(false)
      gdpr_publisher.publish
    end
  end
end
