require_relative "../../../../support/test_helper"
require 'aws-sdk-sns'

SingleCov.covered!

describe 'Zendesk::PushNotifications::Gdpr::GdprSnsFeedbackPublisher' do
  fixtures :users, :accounts

  let(:account)         { accounts(:minimum) }
  let(:admin)           { users(:minimum_admin) }
  let(:deleted_user)    { users(:minimum_end_user2) }
  let(:compliance_deletion_status) do
    FactoryBot.create(:compliance_deletion_status,
      account: account,
      user: deleted_user,
      application: ComplianceDeletionStatus::ALL_APPLICATONS,
      action: ComplianceDeletionFeedback::COMPLETE)
  end

  let(:topic_arn)      { 'topic_arn' }
  let(:region)         { 'aws-west-1' }
  let(:access_key)     { 'access_key' }
  let(:secret_key)     { 'secret_key' }
  let(:session_token)  { 'session_token' }
  let(:sns_publisher)  { Zendesk::PushNotifications::Gdpr::GdprSnsFeedbackPublisher.new(topic_arn: topic_arn, region: region, access_key: access_key, secret_key: secret_key, session_token: session_token) }
  let(:gdpr_publisher) { Zendesk::PushNotifications::Gdpr::GdprFeedbackPublisher.new(compliance_deletion_status, sns_publisher) }

  let(:sns_client) { mock }

  let(:gdpr_message) do
    {
      action: ComplianceDeletionFeedback::COMPLETE,
      application: ComplianceDeletionStatus::ALL_APPLICATONS,
      pod: account.pod_id,
      account_subdomain: account.subdomain
    }
  end
  let(:gdpr_message_attributes) do
    {
      application: { string_value: ComplianceDeletionStatus::ALL_APPLICATONS, data_type: "String" },
      pod: { string_value: "POD#{account.pod_id}", data_type: "String" },
      action: { string_value: ComplianceDeletionFeedback::COMPLETE, data_type: "String" },
      account_subdomain: { string_value: account.subdomain, data_type: "String" }
    }
  end
  let(:message_to_sns) do
    {
      default: gdpr_message.to_json,
      APNS_SANDBOX: { aps: gdpr_message.to_json },
      APNS: { aps: gdpr_message.to_json }
    }.to_json
  end

  let(:dev_message) { gdpr_message.merge(development_namespace: Zendesk::Gdpr::Configuration.fetch(:development_namespace)) }
  let(:dev_message_to_sns) do
    {
      default: dev_message.to_json,
      APNS_SANDBOX: { aps: dev_message.to_json },
      APNS: { aps: dev_message.to_json }
    }.to_json
  end

  describe('#publish') do
    before do
      Aws::SNS::Client.stubs(:new).with(
        region: region,
        access_key_id: access_key,
        secret_access_key: secret_key,
        session_token: session_token
      ).returns(sns_client)
    end

    it 'should call publish on sns_client' do
      sns_client.expects(:publish).with(
        topic_arn: topic_arn,
        subject: "GDPR Feedback",
        message: JSON::Schema.stringify(message_to_sns),
        message_structure: 'json',
        message_attributes: gdpr_message_attributes
      ).once

      sns_publisher.publish(message: gdpr_message)
    end

    describe('development ENV') do
      it 'should append a namespace in dev env' do
        Rails.env.stubs(:development?).returns(true)

        sns_client.expects(:publish).with(
          topic_arn: topic_arn,
          subject: "GDPR Feedback",
          message: JSON::Schema.stringify(dev_message_to_sns),
          message_structure: 'json',
          message_attributes: gdpr_message_attributes
        ).once

        sns_publisher.publish(message: gdpr_message)
      end
    end
  end
end
