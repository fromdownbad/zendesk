require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::PushNotifications::Gdpr::GdprSqsFeedbackSubscriber' do
  fixtures :users, :accounts

  let(:account)         { accounts(:minimum) }
  let(:admin)           { users(:minimum_admin) }
  let(:deleted_user)    { users(:minimum_end_user2) }

  let(:subscriber)      { Zendesk::PushNotifications::Gdpr::GdprSqsFeedbackSubscriber.new }
  let(:gdpr_subscriber) { Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.new(subscriber) }
  let(:json_response) do
    {
      user_id:           deleted_user.id,
      account_id:        account.id,
      account_subdomain: account.subdomain,
      action:            ComplianceDeletionStatus::REQUEST_DELETION,
      application:       ComplianceDeletionStatus::ALL_APPLICATONS,
      executer_id:       admin.id
    }.stringify_keys
  end

  let(:sqs_response) do
    response = mock
    response.stubs(:messages).returns([Struct::AwsSQSStruct.new(message_id, receipt_handle, md5_of_body, body)])
    response
  end

  let(:sqs_client) do
    client = mock
    client.stubs(:receive_message).returns(sqs_response)
    client
  end

  let(:body) do
    {
      "Type"      => "Notification",
      "MessageId" => "MessageId--da117057-95c0-5485-a37d-235050ddbcc9",
      "TopicArn"  => "arn:aws:sns:us-west-2:724781030999:staging_us_gdpr_topic",
      "Subject"   => "GDPR Request",
      "Message"   => json_response.to_json
}.to_json
  end

  let(:message_id) { "message_id" }
  let(:receipt_handle) { "receipt_handle" }
  let(:md5_of_body) { "md5_of_body" }

  before do
    Struct.new("AwsSQSStruct", :message_id, :receipt_handle, :md5_of_body, :body)
    # Setup Account to have at least one soft-deleted user
    deleted_user.current_user = admin
    deleted_user.delete!

    subscriber.stubs(:sqs_client).returns(sqs_client)
  end

  describe '#receive_next_message' do
    it 'should return a hash with known keys' do
      _sqs_message, formatted_hash = subscriber.receive_next_message

      assert_equal json_response.keys.sort, formatted_hash.keys.sort
    end

    it 'should return the original message from SQS' do
      sqs_message, _formatted_hash = subscriber.receive_next_message
      assert_equal sqs_response.messages.first, sqs_message
    end
  end
end
