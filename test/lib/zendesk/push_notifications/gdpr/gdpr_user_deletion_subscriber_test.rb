require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::PushNotifications::Gdpr::GdprUserDeletionSubscriber' do
  fixtures :users, :accounts, :role_settings

  describe '#process_queue' do
    let(:account)         { accounts(:minimum) }
    let(:admin)           { users(:minimum_admin) }
    let(:deleted_user)    { users(:minimum_end_user2) }

    let(:subscriber)      { Zendesk::PushNotifications::Gdpr::GdprSqsUserDeletionSubscriber.new }
    let(:gdpr_subscriber) { Zendesk::PushNotifications::Gdpr::GdprUserDeletionSubscriber.new(subscriber) }
    let(:json_response) do
      {
        user_id:           deleted_user.id,
        account_id:        account.id,
        account_subdomain: account.subdomain,
        action:            ComplianceDeletionStatus::REQUEST_DELETION,
        application:       ComplianceDeletionStatus::ALL_APPLICATONS,
        executer_id:       admin.id
      }.stringify_keys
    end

    let(:sqs_response) do
      Struct::AwsSQSStruct.new([Struct::AwsSQSStruct.new(message_id, receipt_handle, md5_of_body, body)], receipt_handle, message_id)
    end

    let(:message_id) { "message_id" }
    let(:receipt_handle) { "receipt_handle" }
    let(:md5_of_body) { "md5_of_body" }
    let(:body) do
      {
        "Type"      => "Notification",
        "MessageId" => "MessageId--da117057-95c0-5485-a37d-235050ddbcc9",
        "TopicArn"  => "arn:aws:sns:us-west-2:724781030999:staging_us_gdpr_topic",
        "Subject"   => "GDPR Request",
        "Message"   => json_response.to_json
      }.to_json
    end

    before do
      Arturo.disable_feature!(:batch_process_gdpr_deletion_requests)
      Struct.new("AwsStruct", :messages, :receipt_handle, :message_id)
      Struct.new("AwsSQSStruct", :message_id, :receipt_handle, :md5_of_body, :body)
      # Setup Account to have at least one soft-deleted user
      deleted_user.current_user = admin
      deleted_user.delete!
    end

    describe 'with a message on SQS' do
      before do
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, json_response])
        # should call delete_message
        subscriber.stubs(:delete_message).with(sqs_response).once
      end

      it 'should call delete_user_from_support' do
        gdpr_subscriber.expects(:delete_user_from_support).with(account, json_response)
        gdpr_subscriber.process_queue
      end

      it 'should call user.ultra_delete!' do
        User.any_instance.expects(:current_user=).with(admin)
        User.any_instance.stubs(:ultra_delete!).returns(true).once

        gdpr_subscriber.process_queue
      end
    end

    describe 'not on a pod_local?' do
      before do
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, json_response])
        subscriber.stubs(:delete_message).with(sqs_response).once
      end

      it 'should not call user.ultra_delete!' do
        Account.any_instance.stubs(:pod_local?).returns(false)
        User.any_instance.expects(:current_user=).with(admin).never
        User.any_instance.stubs(:ultra_delete!).returns(true).never

        gdpr_subscriber.process_queue
      end
    end

    describe 'in development mode' do
      before do
        Rails.env.stubs(:development?).returns(true)
        subscriber.stubs(:delete_message).with(sqs_response).once
      end

      it 'should not call user.ultra_delete! when message is from another developer' do
        new_json_response = json_response.merge!('development_namespace' => 'fredd')
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, new_json_response])
        Account.any_instance.stubs(:pod_local?).returns(true)

        User.any_instance.expects(:current_user=).with(admin).never
        User.any_instance.stubs(:ultra_delete!).returns(true).never

        gdpr_subscriber.process_queue
      end

      it 'should not call user.ultra_delete! when message gdpr.yml is not setup' do
        new_json_response = json_response.merge('development_namespace' => 'your_github_username')
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, new_json_response])
        Account.any_instance.stubs(:pod_local?).returns(true)

        User.any_instance.expects(:current_user=).with(admin).never
        User.any_instance.stubs(:ultra_delete!).returns(true).never

        gdpr_subscriber.process_queue
      end
    end

    describe 'with a message on SQS not for classic' do
      before do
        json_response['application'] = ComplianceDeletionStatus::CHAT
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, json_response])
        # should call delete_message
        subscriber.stubs(:delete_message).with(sqs_response).once
      end

      it 'should call not call delete_user_from_support' do
        gdpr_subscriber.expects(:delete_user_from_support).never
        gdpr_subscriber.process_queue
      end
    end

    it 'should return 0 if there is not a message on SQS' do
      gdpr_subscriber.stubs(:receive_next_message).returns(nil)

      assert_equal 0, gdpr_subscriber.process_queue
    end

    describe 'when there is an unknown error' do
      let(:exception) { RuntimeError.new("Unknown error") }

      before do
        gdpr_subscriber.stubs(:delete_user_from_support).raises(exception)
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, json_response])
      end

      it 'should catch and report the error without deleting the message' do
        tags = ['class:GdprUserDeletionSubscriber', 'error_type:RuntimeError', "account_id:#{account.id}"]
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed_deletions', tags: tags).once
        event_msg = "Deletion request from user_id #{deleted_user.id} account_id #{account.id} failed due #{exception.message}"
        Zendesk::StatsD::Client.any_instance.expects(:event).with('Delete message can not be processed', event_msg, alert_type: 'warning', tags: ['class:GdprUserDeletionSubscriber']).once

        gdpr_subscriber.stubs(:delete_message).once
        error_message = gdpr_subscriber.send(:error_message, json_response, exception)
        ZendeskExceptions::Logger.expects(:record).with(exception, location: gdpr_subscriber, message: error_message, fingerprint: '7e4c21c6a8d417c15b8835998f0fe92732316ab2')
        gdpr_subscriber.process_queue
      end
    end

    describe 'when ultra delete fails' do
      before do
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, json_response])
      end

      it 'should catch and report the error' do
        gdpr_subscriber.stubs(:delete_message).once
        User.any_instance.stubs(:ultra_delete!).returns(false)
        error_message = gdpr_subscriber.send(:error_message, json_response, stub(message: "Ultra Delete failed for user_id #{deleted_user.id}, {}"))
        ZendeskExceptions::Logger.expects(:record).with(anything, location: gdpr_subscriber, message: error_message, fingerprint: '7e4c21c6a8d417c15b8835998f0fe92732316ab2')
        gdpr_subscriber.process_queue
      end
    end

    describe 'with an invalid user_id' do
      before do
        json_response['user_id'] = 999
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, json_response])
      end

      it 'catch and report the error and not ultra delete the user' do
        gdpr_subscriber.stubs(:delete_message).once
        User.any_instance.expects(:ultra_delete!).never
        error_message = gdpr_subscriber.send(:error_message, json_response, stub(message: "Can't find the user that is being logged for deletion. user_id: 999"))
        ZendeskExceptions::Logger.expects(:record).with(anything, location: gdpr_subscriber, message: error_message, fingerprint: '7e4c21c6a8d417c15b8835998f0fe92732316ab2')
        gdpr_subscriber.process_queue
      end
    end

    describe 'with an active user' do
      before do
        json_response['user_id'] = account.owner.id
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, json_response])
      end

      it 'catch and report the error and not ultra delete the user' do
        gdpr_subscriber.stubs(:delete_message).once
        User.any_instance.expects(:ultra_delete!).never
        error_message = gdpr_subscriber.send(:error_message, json_response, stub(message: "Can't delete a user that is active. user_id: #{account.owner.id}"))
        ZendeskExceptions::Logger.expects(:record).with(anything, location: gdpr_subscriber, message: error_message, fingerprint: '7e4c21c6a8d417c15b8835998f0fe92732316ab2')
        gdpr_subscriber.process_queue
      end
    end

    describe 'with an inactive executer' do
      before do
        json_response['executer_id'] = users(:inactive_user).id
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, json_response])
        gdpr_subscriber.stubs(:delete_message).once
      end

      it 'should ultra delete the user using the account owner' do
        User.any_instance.expects(:current_user=).with(account.owner)
        User.any_instance.expects(:ultra_delete!).once

        gdpr_subscriber.process_queue
      end
    end

    describe 'with an executer that has no user deletion permission' do
      before do
        User.any_instance.stubs(:can?).with(:delete, deleted_user).returns(false)
        gdpr_subscriber.stubs(:receive_next_message).returns([sqs_response, json_response])
        gdpr_subscriber.stubs(:delete_message).once
      end

      it 'should ultra delete the user using the account owner' do
        User.any_instance.expects(:current_user=).with(account.owner)
        User.any_instance.expects(:ultra_delete!).once

        gdpr_subscriber.process_queue
      end
    end

    describe "with batch_process_gdpr_deletion_requests" do
      before do
        Arturo.enable_feature!(:batch_process_gdpr_deletion_requests)
      end

      describe 'with an message on SQS in batch' do
        before do
          gdpr_subscriber.stubs(:receive_next_messages).returns([[sqs_response, json_response], [sqs_response, json_response]])
          gdpr_subscriber.stubs(:delete_message).with(sqs_response).twice
        end

        it 'should call delete_user_from_support' do
          gdpr_subscriber.expects(:delete_user_from_support).with(account, json_response).twice
          gdpr_subscriber.process_queue
        end

        it 'should call user.ultra_delete!' do
          User.any_instance.expects(:current_user=).with(admin).twice
          User.any_instance.stubs(:ultra_delete!).returns(true).twice

          gdpr_subscriber.process_queue
        end

        it 'should register metrics for success' do
          User.any_instance.expects(:current_user=).with(admin).twice
          User.any_instance.stubs(:ultra_delete!).returns(true).twice
          ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
          ComplianceDeletionStatus.create_request!(user: deleted_user, executer: admin)

          Zendesk::PushNotifications::Gdpr::GdprUserDeletionSubscriber.statsd_client.expects(:distribution).with do |metric_name, _time|
            metric_name == "time_to_complete_deletion"
          end.twice

          gdpr_subscriber.process_queue
        end

        it 'should register mertrics for fail' do
          User.any_instance.expects(:current_user=).with(admin).twice
          User.any_instance.stubs(:ultra_delete!).raises(StandardError, 'fail').twice
          Zendesk::PushNotifications::Gdpr::GdprUserDeletionSubscriber.statsd_client.expects(:increment).with do |metric_name, _tags|
            metric_name == "failed_deletions"
          end.twice

          Zendesk::PushNotifications::Gdpr::GdprUserDeletionSubscriber.statsd_client.expects(:event).with do |event_name, _message, _alert_type|
            event_name == "Delete message can not be processed"
          end.twice

          gdpr_subscriber.process_queue
        end
      end

      describe "with empty messages on SQS" do
        it 'should return 0 if there is not a message on SQS' do
          gdpr_subscriber.stubs(:receive_next_messages).returns(nil)

          assert_equal 0, gdpr_subscriber.process_queue
        end
      end

      describe 'not on a pod_local?' do
        before do
          gdpr_subscriber.stubs(:receive_next_messages).returns([[sqs_response, json_response], [sqs_response, json_response]])
          gdpr_subscriber.stubs(:delete_message).with(sqs_response).twice
        end

        it 'should not call user.ultra_delete!' do
          Account.any_instance.stubs(:pod_local?).returns(false)
          User.any_instance.expects(:current_user=).with(admin).never
          User.any_instance.stubs(:ultra_delete!).returns(true).never

          gdpr_subscriber.process_queue
        end
      end

      describe 'in development mode' do
        before do
          Rails.env.stubs(:development?).returns(true)
          subscriber.stubs(:delete_message).with(sqs_response).twice
        end

        it 'should not call user.ultra_delete! when message is from another developer' do
          new_json_response = json_response.merge!('development_namespace' => 'fredd')
          gdpr_subscriber.stubs(:receive_next_messages).returns([[sqs_response, new_json_response], [sqs_response, new_json_response]])
          Account.any_instance.stubs(:pod_local?).returns(true)

          User.any_instance.expects(:current_user=).with(admin).never
          User.any_instance.stubs(:ultra_delete!).returns(true).never

          gdpr_subscriber.process_queue
        end

        it 'should not call user.ultra_delete! when message gdpr.yml is not setup' do
          new_json_response = json_response.merge('development_namespace' => 'your_github_username')
          gdpr_subscriber.stubs(:receive_next_messages).returns([[sqs_response, new_json_response], [sqs_response, new_json_response]])
          Account.any_instance.stubs(:pod_local?).returns(true)

          User.any_instance.expects(:current_user=).with(admin).never
          User.any_instance.stubs(:ultra_delete!).returns(true).never

          gdpr_subscriber.process_queue
        end
      end

      describe 'with a message on SQS not for classic' do
        before do
          json_response['application'] = ComplianceDeletionStatus::CHAT
          gdpr_subscriber.stubs(:receive_next_messages).returns([[sqs_response, json_response], [sqs_response, json_response]])
          # should call delete_message
          subscriber.stubs(:delete_message).with(sqs_response).twice
        end

        it 'should call not call delete_user_from_support' do
          gdpr_subscriber.expects(:delete_user_from_support).never
          gdpr_subscriber.process_queue
        end
      end
    end
  end
end
