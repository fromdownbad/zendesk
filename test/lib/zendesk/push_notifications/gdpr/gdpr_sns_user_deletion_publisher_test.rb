require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1 # publish is not covered

describe 'Zendesk::PushNotifications::Gdpr::GdprSnsUserDeletionPublisher' do
  fixtures :users, :accounts

  let(:sns_publisher) { Zendesk::PushNotifications::Gdpr::GdprSnsUserDeletionPublisher.new }

  describe '#initialize' do
    before do
      @current_settings = Zendesk::Gdpr::Configuration.settings.dup

      Zendesk::Gdpr::Configuration.setup do
        {
          'topic_arn' => 'ABC',
          'region' => 'DEF',
          'aws_access_key' => 'GHI',
          'aws_secret_key' => 'JKL'
        }
      end
    end

    after do
      Zendesk::Gdpr::Configuration.setup { @current_settings }
    end

    it 'should default to the values on the ENV' do
      assert_equal 'ABC', sns_publisher.instance_variable_get(:@topic_arn)
      assert_equal 'DEF', sns_publisher.instance_variable_get(:@region)
      assert_equal 'GHI', sns_publisher.instance_variable_get(:@access_key)
      assert_equal 'JKL', sns_publisher.instance_variable_get(:@secret_key)
    end
  end
end
