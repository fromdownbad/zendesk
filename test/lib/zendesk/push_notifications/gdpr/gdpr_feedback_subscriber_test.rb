require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber' do
  fixtures :users, :accounts, :role_settings

  let(:account)         { accounts(:minimum) }
  let(:admin)           { users(:minimum_admin) }
  let(:deleted_user)    { users(:minimum_end_user2) }
  let(:talk_app)        { ComplianceDeletionStatus::TALK }

  let(:subscriber)      { Zendesk::PushNotifications::Gdpr::GdprSqsFeedbackSubscriber.new }
  let(:gdpr_subscriber) { Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.new(subscriber) }
  let(:json_response) do
    {
      user_id:           deleted_user.id,
      account_id:        account.id,
      account_subdomain: account.subdomain,
      action:            ComplianceDeletionFeedback::COMPLETE,
      application:       talk_app
    }.stringify_keys
  end

  let(:message_received_time) { Time.now }
  let(:full_message) do
    message = mock
    # converting to epoch time in milliseconds
    message.stubs(:message_id).returns('12345')
    message.stubs(:receipt_handle).returns('1')
    message.stubs(:attributes).returns('SentTimestamp' => (message_received_time.to_f * 1000).to_i.to_s)
    message
  end

  let(:messages) { [[full_message, json_response]] }

  let(:initial_request_status) do
    FactoryBot.create(
      :compliance_deletion_status,
      account: account,
      user: deleted_user,
      executer: admin
    )
  end

  let(:app_feedback) do
    FactoryBot.create(
      :compliance_deletion_feedback,
      compliance_deletion_status: initial_request_status,
      application: talk_app,
      required: true
    )
  end

  before do
    ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
    # Setup Account to have at least one soft-deleted user
    deleted_user.current_user = admin
    deleted_user.delete!
    initial_request_status
    app_feedback
  end

  describe '#process_queue' do
    it "should call itself if #process_queue at least 1 message" do
      gdpr_subscriber.expects(:process_queue).returns(1, 1, 1, 0).times(4)
      gdpr_subscriber.process_queues
    end

    it "should not call itself if #process_queue returns 0 messages" do
      gdpr_subscriber.expects(:process_queue).returns(0).once
      gdpr_subscriber.process_queues
    end

    describe 'with a message on SQS' do
      before do
        gdpr_subscriber.stubs(:receive_next_messages).returns(messages)
        gdpr_subscriber.expects(:instrument_time_to_process_feedback).once
        gdpr_subscriber.expects(:instrument_application_response_time).once
        gdpr_subscriber.expects(:statsd_feedback_counter).once
        subscriber.expects(:batch_delete_messages).with(messages).once
      end

      it 'should return true to continue processing' do
        assert(gdpr_subscriber.process_queue)
      end

      it 'updates feedback record' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('request_completed', anything).once

        gdpr_subscriber.process_queue

        feedback = ComplianceDeletionFeedback.where(
          account_id: account.id,
          user_id: deleted_user.id,
          application: json_response['application'],
          state: json_response['action']
        ).last

        assert_equal feedback.account_id, account.id
        assert_equal feedback.user_id, deleted_user.id
        assert_equal feedback.pod_id, account.pod_id
        assert_equal feedback.state, ComplianceDeletionFeedback::COMPLETE
        assert_equal feedback.application, ComplianceDeletionStatus::TALK
      end

      describe 'when account moved pods since initial request' do
        before do
          ComplianceDeletionStatus.any_instance.stubs(:publish_to_incomplete!).with([talk_app])
          app_feedback.update_column(:pod_id, account.pod_id + 1)
        end

        it 'redos the request' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('redo_request', tags: ["application:#{talk_app}"]).once
          gdpr_subscriber.process_queue

          assert_equal ComplianceDeletionFeedback::REDO, app_feedback.reload.state
          assert_equal account.pod_id, app_feedback.reload.pod_id
        end
      end
    end

    describe 'with duplicate messages on SQS' do
      let(:messages) { [[full_message, json_response], [full_message, json_response]] }

      before do
        gdpr_subscriber.stubs(:receive_next_messages).returns(messages)
        subscriber.expects(:batch_delete_messages).with(messages).once
      end

      it 'should not create many of records for' do
        gdpr_subscriber.process_queue

        assert_equal 1, ComplianceDeletionFeedback.where(user_id: deleted_user.id, account_id: account.id, application: ComplianceDeletionStatus::TALK, state: ComplianceDeletionFeedback::COMPLETE).count
      end
    end

    it 'should return 0 if there is not a message on SQS' do
      gdpr_subscriber.stubs(:receive_next_messages).returns(nil)

      assert_equal 0, gdpr_subscriber.process_queue
    end

    it "should not call itself if on the wrong pod_local?" do
      gdpr_subscriber.stubs(:receive_next_messages).returns(messages)
      Account.any_instance.expects(:on_shard).never
      subscriber.expects(:batch_delete_messages).with(messages).once
      gdpr_subscriber.expects(:process_message).never
      Account.any_instance.stubs(:pod_local?).returns(false).at_least_once

      gdpr_subscriber.process_queue
    end

    it "should not call itself if on the wrong pod attribute is the wrong pod" do
      messages = [[full_message, json_response.merge('pod' => (account.pod_id + 1))]]
      gdpr_subscriber.stubs(:receive_next_messages).returns(messages)

      Account.any_instance.expects(:on_shard).never
      subscriber.expects(:batch_delete_messages).with(messages).once
      gdpr_subscriber.expects(:process_message).never
      Account.any_instance.stubs(:pod_local?).returns(false).never

      gdpr_subscriber.process_queue
    end

    it "should Log an Exception if on the wrong application attribute passed" do
      messages = [[full_message, json_response.merge!('application' => ComplianceDeletionStatus::ALL_APPLICATONS)]]
      gdpr_subscriber.stubs(:receive_next_messages).returns(messages)

      Account.any_instance.expects(:on_shard).never
      subscriber.expects(:batch_delete_messages).with(messages).once
      gdpr_subscriber.expects(:process_message).never
      Account.any_instance.stubs(:pod_local?).returns(false).never
      Zendesk::StatsD::Client.any_instance.expects(:increment).once

      gdpr_subscriber.process_queue
    end

    describe 'when non-complete action is received' do
      let(:action) { 'started' }
      let(:application) { ComplianceDeletionStatus::TALK }

      let(:json_started_response) do
        {
          account_id:        account.id,
          account_subdomain: account.subdomain,
          action:            action,
          application:       application,
          user_id:           deleted_user.id
        }.stringify_keys
      end

      let(:messages) { [[full_message, json_started_response]] }

      before do
        gdpr_subscriber.stubs(:receive_next_messages).returns(messages)
        subscriber.expects(:batch_delete_messages).with(messages).once
      end

      it 'should record metric' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('invalid_message', tags: ['reason:invalid_action', "action:#{action}", "application:#{application}"]).once
        gdpr_subscriber.process_queue
      end

      it 'should not create compliance_deletion_status feedback record' do
        gdpr_subscriber.expects(:process_message).never
        gdpr_subscriber.process_queue
      end
    end

    describe 'when there is an unknown error' do
      let(:exception) { RuntimeError.new("Unknown error") }

      before do
        gdpr_subscriber.stubs(:process_message).raises(exception)
        gdpr_subscriber.stubs(:receive_next_messages).returns(messages)
      end

      it 'should catch and report the error and also delete the message' do
        tags = ['class:GdprFeedbackSubscriber', 'error_type:RuntimeError', "account_id:#{account.id}"]
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed_deletions', tags: tags).once
        event_msg = "Deletion request from user_id #{deleted_user.id} account_id #{account.id} failed due #{exception.message}"
        Zendesk::StatsD::Client.any_instance.expects(:event).with('Delete message can not be processed', event_msg, alert_type: 'warning', tags: ['class:GdprFeedbackSubscriber']).once

        subscriber.expects(:batch_delete_messages).with(messages).once
        error_message = gdpr_subscriber.send(:error_message, json_response, exception)

        ZendeskExceptions::Logger.expects(:record).with(
          exception,
          location: gdpr_subscriber,
          message: error_message,
          fingerprint: '7e4c21c6a8d417c15b8835998f0fe92732316ab2'
        )
        gdpr_subscriber.process_queue
      end
    end

    describe 'when there is a retryable exception' do
      let(:full_message_two) do
        response = full_message.dup
        response.stubs(:message_id).returns('123')
        response
      end

      let(:json_response_two) do
        {
          user_id:           deleted_user.id,
          account_id:        account.id,
          account_subdomain: account.subdomain,
          action:            ComplianceDeletionFeedback::COMPLETE,
          application:       ComplianceDeletionStatus::TEXT,
        }.stringify_keys
      end

      let(:messages) { [[full_message, json_response], [full_message_two, json_response_two]] }

      before do
        gdpr_subscriber.stubs(:receive_next_messages).returns(messages)
        gdpr_subscriber.stubs(:process_message).returns(nil).then.raises(exception)
      end

      Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber::RETRYABLE_ERRORS.each do |ex|
        describe "a #{ex}" do
          let(:exception) do
            if ex == Seahorse::Client::NetworkingError
              ex.new(RuntimeError.new, 'error')
            elsif ex == Zendesk::Accounts::Locking::LockedException
              ex.new(account)
            else
              ex.new('error')
            end
          end

          it 'should NOT delete the message' do
            subscriber.expects(:batch_delete_messages).with([[full_message, json_response]]).once
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed_deletions', anything).never
            tags = ["error_type:#{exception.class.name}", "account_id:#{account.id}"]
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('retry_feedback', tags: tags).once
            gdpr_subscriber.process_queue
          end
        end
      end

      describe 'a MappedDatabaseExceptions::TemporaryException' do
        # https://github.com/zendesk/zendesk_database_support/blob/master/lib/zendesk_database_support/mapped_exceptions.rb
        let(:exception) { ZendeskDatabaseSupport::MappedDatabaseExceptions::LostConnection.new('error') }

        it 'should NOT delete the message' do
          subscriber.expects(:batch_delete_messages).with([[full_message, json_response]]).once
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed_deletions', anything).never
          tags = ["error_type:ZendeskDatabaseSupport::MappedDatabaseExceptions::LostConnection", "account_id:#{account.id}"]
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('retry_feedback', tags: tags).once
          gdpr_subscriber.process_queue
        end
      end

      describe "a ActiveRecord::StatementInvalid Mysql2::Error: Can't connect to server error" do
        let(:exception) { ActiveRecord::StatementInvalid.new("Mysql2::Error: Can't connect to server") }

        it 'should NOT delete the message' do
          subscriber.expects(:batch_delete_messages).with([[full_message, json_response]]).once
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed_deletions', anything).never
          tags = ["error_type:ActiveRecord::StatementInvalid", "account_id:#{account.id}"]
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('retry_feedback', tags: tags).once
          gdpr_subscriber.process_queue
        end
      end

      describe 'a ActiveRecord::StatementInvalid Mysql2::Error: Unknown MySQL server host error' do
        let(:exception) { ActiveRecord::StatementInvalid.new('Mysql2::Error: Unknown MySQL server host') }

        it 'should NOT delete the message' do
          subscriber.expects(:batch_delete_messages).with([[full_message, json_response]]).once
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed_deletions', anything).never
          tags = ["error_type:ActiveRecord::StatementInvalid", "account_id:#{account.id}"]
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('retry_feedback', tags: tags).once
          gdpr_subscriber.process_queue
        end
      end

      describe 'when other ActiveRecord::StatementInvalid error' do
        let(:exception) { ActiveRecord::StatementInvalid.new('Mysql2::Error: Duplicate entry') }

        it 'should delete the message' do
          subscriber.expects(:batch_delete_messages).with(messages).once
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed_deletions', anything).once
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('retry_feedback', anything).never
          gdpr_subscriber.process_queue
        end
      end
    end

    describe 'with an invalid user_id' do
      before do
        json_response['user_id'] = 999
        gdpr_subscriber.stubs(:receive_next_messages).returns(messages)
      end

      it 'catch and report the error and not ultra delete the user' do
        subscriber.expects(:batch_delete_messages).with(messages).once
        User.any_instance.expects(:ultra_delete!).never
        error_message = gdpr_subscriber.send(:error_message, json_response, stub(message: "Can't find the user that is being logged for deletion. user_id: 999"))
        ZendeskExceptions::Logger.expects(:record).with(
          anything,
          location: gdpr_subscriber,
          message: error_message,
          fingerprint: '7e4c21c6a8d417c15b8835998f0fe92732316ab2'
        )
        gdpr_subscriber.process_queue
      end
    end

    describe 'with an active user' do
      before do
        json_response['user_id'] = account.owner.id
        gdpr_subscriber.stubs(:receive_next_messages).returns(messages)
      end

      it 'catch and report the error and not ultra delete the user' do
        subscriber.expects(:batch_delete_messages).with(messages).once
        User.any_instance.expects(:ultra_delete!).never
        error_message = gdpr_subscriber.send(:error_message, json_response, stub(message: "Can't delete a user that is active. user_id: #{account.owner.id}"))
        ZendeskExceptions::Logger.expects(:record).with(
          anything,
          location: gdpr_subscriber,
          message: error_message,
          fingerprint: '7e4c21c6a8d417c15b8835998f0fe92732316ab2'
        )
        gdpr_subscriber.process_queue
      end
    end
  end

  describe '#valid_message?' do
    it "returns false when application is set to 'all'" do
      msg = json_response.merge('application' => ComplianceDeletionStatus::ALL_APPLICATONS)
      refute gdpr_subscriber.send(:valid_message?, msg)
    end

    it 'returns false when application not in defined list' do
      invalid_app = 'invalid_app'
      assert ComplianceDeletionStatus::APPLICATONS.exclude?(invalid_app)
      msg = json_response.merge('application' => invalid_app)
      refute gdpr_subscriber.send(:valid_message?, msg)
    end

    it 'returns false when action is invalid' do
      msg = json_response.merge('action' => ComplianceDeletionStatus::REQUEST_DELETION)
      refute gdpr_subscriber.send(:valid_message?, msg)
    end

    it 'returns false when action is not complete' do
      msg = json_response.merge('action' => 'started')
      refute gdpr_subscriber.send(:valid_message?, msg)
    end

    it 'returns true when message is valid' do
      assert gdpr_subscriber.send(:valid_message?, json_response)
    end
  end

  describe '#process_message' do
    describe 'when initial feedback' do
      subject { gdpr_subscriber.send(:process_message, account, full_message, json_response) }

      it 'should update compliance_deletion_feedback record' do
        subject
        ComplianceDeletionFeedback.
          complete.
          where(compliance_deletion_status: initial_request_status, application: json_response['application']).
          count.must_equal 1
      end

      it 'should instrument feedback' do
        Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.any_instance.expects(:instrument_time_to_process_feedback).once
        Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.any_instance.expects(:instrument_application_response_time).once
        Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.any_instance.expects(:statsd_feedback_counter).once
        subject
      end

      describe 'when redo record' do
        before do
          app_feedback.redo!
        end

        it 'should update compliance_deletion_feedback record' do
          subject
          ComplianceDeletionFeedback.
            complete.
            where(compliance_deletion_status: initial_request_status, application: json_response['application']).
            count.must_equal 1
        end
      end

      describe 'when feedback record does not exist' do
        before do
          app_feedback.destroy!
        end

        it 'raises ComplianceDeletionError' do
          err = assert_raises(ComplianceDeletionError) do
            subject
          end

          error_msg = "feedback record is missing for application:#{talk_app}, account:#{account.idsub}, user:#{initial_request_status.user_id}"
          assert_equal err.message, error_msg
        end
      end
    end

    describe 'when all missing feedback has been received' do
      subject { gdpr_subscriber.send(:process_message, account, full_message, json_response) }

      it 'should complete initial request' do
        refute initial_request_status.reload.complete?
        subject
        assert initial_request_status.reload.complete?
      end
    end

    describe 'when all missing feedback has NOT been received' do
      subject { gdpr_subscriber.send(:process_message, account, full_message, json_response) }

      let(:feedback_not_complete) do
        FactoryBot.create(
          :compliance_deletion_feedback,
          compliance_deletion_status: initial_request_status,
          application: ComplianceDeletionStatus::TEXT,
          required: true
        )
      end

      before do
        feedback_not_complete
      end

      it 'should NOT complete initial request' do
        refute initial_request_status.reload.complete?
        subject
        refute initial_request_status.reload.complete?
      end
    end

    describe 'when duplicate feedback is received' do
      subject { gdpr_subscriber.send(:process_message, account, full_message, json_response) }
      let(:updated_at_time) { 3.days.ago }

      before do
        Timecop.freeze
        app_feedback.complete!
        app_feedback.update_column(:updated_at, updated_at_time)
      end

      after do
        Timecop.return
      end

      it 'should not update compliance_deletion_feedback record' do
        subject
        assert_equal updated_at_time.to_i, app_feedback.updated_at.to_i
      end

      it 'should only increment statsd_feedback_counter and record feedback process time' do
        Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.any_instance.expects(:instrument_application_response_time).never
        Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.any_instance.expects(:instrument_time_to_process_feedback).once
        Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.any_instance.expects(:statsd_feedback_counter).once
        subject
      end
    end

    describe 'when feedback from wrong pod is received' do
      subject { gdpr_subscriber.send(:process_message, account, full_message, json_response) }

      before do
        app_feedback.update_column(:pod_id, account.pod_id + 1)
        ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).with([talk_app])
      end

      it 'should not complete initial request' do
        refute initial_request_status.reload.complete?
        subject
        refute initial_request_status.reload.complete?
      end

      it 'should set feedback state to redo' do
        assert app_feedback.state == ComplianceDeletionFeedback::PENDING
        subject
        assert app_feedback.reload.redo?
        assert_equal account.pod_id, app_feedback.reload.pod_id
      end
    end
  end

  describe '#instrument_time_to_process_feedback' do
    let(:time_to_process) { 1.minute }
    let(:application) { ComplianceDeletionStatus::TALK }

    before do
      Timecop.freeze
    end

    after do
      Timecop.return
    end

    it 'sends time_to_process_message metrics to statsd' do
      tags = [
        "action:#{ComplianceDeletionFeedback::COMPLETE}",
        "application:#{application}"
      ]

      Zendesk::StatsD::Client.any_instance.expects(:distribution).with('time_to_process_message', time_to_process.to_i, tags: tags).once

      message_received_time = Time.now - time_to_process

      gdpr_subscriber.send(
        :instrument_time_to_process_feedback,
        application: application,
        received_time: message_received_time
      )
    end
  end

  describe '#instrument_application_response_time' do
    let(:application) { ComplianceDeletionStatus::TALK }

    it 'records datadog metric' do
      tags = [
        "application:#{application}",
        "action:#{ComplianceDeletionFeedback::COMPLETE}",
        'repeated:false',
      ]

      Zendesk::StatsD::Client.any_instance.expects(:increment).with('record_feedback', tags: tags).once
      gdpr_subscriber.send(
        :statsd_feedback_counter,
        application: application,
        duplicate: false
      )
    end
  end

  describe '#instrument_application_response_time' do
    subject do
      gdpr_subscriber.send(
        :instrument_application_response_time,
        application: application,
        is_redo: is_redo,
        received_time: message_received_time,
        start_timestamp: initial_request_created_at
      )
    end

    let(:application) { ComplianceDeletionStatus::TALK }
    let(:initial_request_created_at) { initial_request_status.created_at }
    let(:is_redo) { false }
    let(:message_received_time) { initial_request_created_at + time_to_feedback }
    let(:tags) do
      [
        "action:#{ComplianceDeletionFeedback::COMPLETE}",
        "application:#{application}",
        "redo:#{is_redo}",
        "sla_tag:#{sla_tag}"
      ]
    end

    before do
      Zendesk::StatsD::Client.any_instance.expects(:distribution).with('worker_time_to_feedback', time_to_feedback.seconds, tags: tags).once
    end

    describe 'when redo request' do
      let(:is_redo) { true }
      let(:time_to_feedback) { 1.day }
      let(:sla_tag) { 'within_expected' }

      it 'sets redo tag to true' do
        subject
      end
    end

    describe 'when within expected' do
      let(:time_to_feedback) { 1.day }
      let(:sla_tag) { 'within_expected' }

      it 'sends worker_time_to_feedback metrics to statsd with within_expected tag' do
        subject
      end
    end

    describe 'when within sla' do
      let(:time_to_feedback) { 15.days }
      let(:sla_tag) { 'within_sla' }

      it 'sends worker_time_to_feedback metrics to statsd with within_sla tag' do
        subject
      end
    end

    describe 'when outside sla' do
      let(:time_to_feedback) { 31.days }
      let(:sla_tag) { 'outside_sla' }

      it 'sends worker_time_to_feedback metrics to statsd with outside_sla tag' do
        subject
      end
    end
  end
end
