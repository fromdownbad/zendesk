require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'Zendesk::PushNotifications::Gdpr::GdprSqsSubscriber' do
  fixtures :users, :accounts, :role_settings

  describe '#process_queue' do
    let(:account)         { accounts(:minimum) }
    let(:admin)           { users(:minimum_admin) }
    let(:deleted_user)    { users(:minimum_end_user2) }

    let(:subscriber)      { Zendesk::PushNotifications::Gdpr::GdprSqsFeedbackSubscriber.new }
    let(:gdpr_subscriber) { Zendesk::PushNotifications::Gdpr::GdprSqsSubscriber.new }
    let(:json_response) do
      {
        user_id:           deleted_user.id,
        account_id:        account.id,
        account_subdomain: account.subdomain,
        action:            ComplianceDeletionFeedback::COMPLETE,
        application:       ComplianceDeletionStatus::TALK,
        executer_id:       admin.id
      }.stringify_keys
    end

    let(:sqs_response) do
      response = mock
      response.stubs(:messages).returns([Struct::AwsSQSStruct.new(message_id, receipt_handle, md5_of_body, body)])
      response.stubs(:receipt_handle).returns('123456fd')
      response
    end

    let(:sqs_message_response) do
      response = mock
      response.stubs(:messages).returns([Struct::AwsSQSStruct.new(message_id, receipt_handle, md5_of_body, message_body)])
      response.stubs(:receipt_handle).returns('123456fd')
      response
    end

    let(:unparsable_sqs_message_response) do
      response = mock
      response.stubs(:messages).returns([Struct::AwsSQSStruct.new({})])
      response.stubs(:receipt_handle).returns('654321fd')
      response
    end

    let(:empty_sqs_response) do
      response = mock
      response.stubs(:messages).returns([])
      response
    end

    let(:sqs_message) do
      sqsmessage = mock
      sqsmessage.stubs(:receipt_handle).returns('12345')
      sqsmessage
    end

    let(:sqs_client) { mock }

    let(:body) do
      {
        "Type"      => "Notification",
        "MessageId" => "MessageId--da117057-95c0-5485-a37d-235050ddbcc9",
        "TopicArn"  => "arn:aws:sns:us-west-2:724781030999:staging_us_gdpr_topic",
        "Subject"   => "GDPR Request",
        "Message"   => json_response.to_json
      }.to_json
    end

    let(:message_body) do
      {
        "Type"      => "Notification",
        "MessageId" => "MessageId--da117057-95c0-5485-a37d-235050ddbcc9",
        "TopicArn"  => "arn:aws:sns:us-west-2:724781030999:staging_us_gdpr_topic",
        "Subject"   => "GDPR Request",
        "Message"   => {"message" => json_response}.to_json
      }.to_json
    end

    let(:message_id) { "message_id" }
    let(:receipt_handle) { "receipt_handle" }
    let(:md5_of_body) { "md5_of_body" }

    before do
      Struct.new("AwsSQSStruct", :message_id, :receipt_handle, :md5_of_body, :body)
      # Setup Account to have at least one soft-deleted user
      deleted_user.current_user = admin
      deleted_user.delete!

      gdpr_subscriber.stubs(:sqs_client).returns(sqs_client)
    end

    describe '#batch_delete_messages' do
      subject { gdpr_subscriber.batch_delete_messages(messages) }

      let(:sqs_message) do
        sqsmessage = mock
        sqsmessage.stubs(:receipt_handle).returns('12345')
        sqsmessage.stubs(:message_id).returns('1')
        sqsmessage
      end

      let(:successful_sqs_response) do
        response = mock
        response.stubs(:failed).returns([])
        response
      end

      let(:failed_sqs_message) do
        sqsmessage = mock
        sqsmessage.stubs(:id).returns('some_id')
        sqsmessage.stubs(:code).returns('some_code')
        sqsmessage.stubs(:sender_fault).returns(true)
        sqsmessage.stubs(:message).returns('some_message')
        sqsmessage
      end

      let(:failed_sqs_response) do
        response = mock
        response.stubs(:failed).returns([failed_sqs_message])
        response
      end

      let(:messages) do
        Array.new(2) { [sqs_message, json_response] }
      end

      describe 'when no failed messages' do
        it 'calls delete_message_batch on sqs_client' do
          sqs_client.expects(:delete_message_batch).returns(successful_sqs_response).once
          subject
        end
      end

      describe 'failed messages' do
        it 'calls delete_message_batch on sqs_client' do
          sqs_client.expects(:delete_message_batch).returns(failed_sqs_response).once
          subject
        end

        it 'logs the errors' do
          sqs_client.stubs(:delete_message_batch).returns(failed_sqs_response)
          Rails.logger.expects(:error)
          subject
        end
      end
    end

    describe '#receive_next_messages' do
      it "calls receive_message on sqs_client" do
        sqs_client.stubs(:receive_message).returns(sqs_response).once
        gdpr_subscriber.receive_next_messages
      end

      it "calls receive_message on stringy sqs_client" do
        sqs_client.stubs(:receive_message).returns(sqs_message_response, empty_sqs_response).once
        sqs_messages = gdpr_subscriber.receive_next_messages
        _sqs_messaging, json_messaging = sqs_messages.first
        assert_equal json_response.keys.sort, json_messaging.keys.sort
      end

      it 'should call delete_message when you have malformed data' do
        sqs_client.stubs(:receive_message).returns(sqs_response, empty_sqs_response)
        JSON.stubs(:parse).raises(JSON::ParserError)
        gdpr_subscriber.stubs(:delete_message).once
        gdpr_subscriber.receive_next_messages
      end

      describe 'with batches of unparsable data' do
        it 'should call delete_message the bad message & continue processing' do
          sqs_client.stubs(:receive_message).returns(unparsable_sqs_message_response, sqs_message_response, empty_sqs_response)
          gdpr_subscriber.stubs(:delete_message).once
          gdpr_subscriber.receive_next_messages
        end

        it 'should call log that there is a malformed sqs message' do
          sqs_client.stubs(:receive_message).returns(unparsable_sqs_message_response, sqs_message_response, empty_sqs_response)
          gdpr_subscriber.stubs(:delete_message).once
          Rails.logger.expects(:debug).with("Removing malformed message, message: #{Struct::AwsSQSStruct.new({})}")
          gdpr_subscriber.receive_next_messages
        end
      end
    end

    describe '#receive_next_message' do
      it "calls receive_message on sqs_client" do
        sqs_client.stubs(:receive_message).returns(sqs_response).once
        gdpr_subscriber.receive_next_message
      end

      it "calls receive_message on stringy sqs_client" do
        sqs_client.stubs(:receive_message).returns(sqs_message_response).once
        _sqs_messaging, json_messaging = gdpr_subscriber.receive_next_message
        assert_equal json_response.keys.sort, json_messaging.keys.sort
      end

      it 'should call delete_message when you have malformed data' do
        sqs_client.stubs(:receive_message).returns(sqs_response, empty_sqs_response)
        JSON.stubs(:parse).raises(JSON::ParserError)
        gdpr_subscriber.stubs(:delete_message).once
        gdpr_subscriber.receive_next_message
      end
    end

    describe '#delete_message' do
      it "calls delete_message on sqs_client" do
        sqs_client.expects(:delete_message).once
        gdpr_subscriber.delete_message(sqs_message)
      end
    end
  end
end
