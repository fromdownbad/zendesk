require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::PushNotifications::Gdpr::GdprPublisher' do
  fixtures :users, :accounts

  let(:account)         { accounts(:minimum) }
  let(:admin)           { users(:minimum_admin) }
  let(:deleted_user)    { users(:minimum_end_user2) }

  let(:compliance_deletion_status) do
    FactoryBot.create(:compliance_deletion_status,
      account: account,
      user: deleted_user,
      executer: admin)
  end

  let(:sns_publisher)  { Zendesk::PushNotifications::Gdpr::GdprSnsUserDeletionPublisher.new(topic_arn: 'topic_arn', region: 'aws-west-1', access_key: 'access_key', secret_key: 'secret_key') }
  let(:gdpr_publisher) { Zendesk::PushNotifications::Gdpr::GdprPublisher.new(compliance_deletion_status, sns_publisher) }

  before do
    ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
  end

  it 'should raise if the subclass has not Implemented me' do
    assert_raise(ComplianceDeletionError) do
      gdpr_publisher.publish
    end
  end

  it 'should delegate to compliance_deletion_status' do
    %i[user_id account_id account executer_id].each do |method|
      assert_equal compliance_deletion_status.send(method), gdpr_publisher.send(method)
    end
  end

  describe 'publish_to_incomplete!' do
    let(:gdpr_sns_publisher) { mock }
    before do
      Zendesk::PushNotifications::Gdpr::GdprSnsPublisher.stubs(:new).returns(gdpr_sns_publisher)
    end

    it 'should call publish for each application passed in' do
      applications = [ComplianceDeletionStatus::CLASSIC, ComplianceDeletionStatus::TALK]
      gdpr_sns_publisher.expects(:publish).with(
        subject: "GDPR Request for #{ComplianceDeletionStatus::CLASSIC}",
        message: {
          user_id: compliance_deletion_status.user_id,
          account_id: compliance_deletion_status.account_id,
          account_subdomain: compliance_deletion_status.account.subdomain,
          action: ComplianceDeletionStatus::REQUEST_DELETION,
          application: ComplianceDeletionStatus::CLASSIC,
          executer_id: compliance_deletion_status.executer_id,
          pod: compliance_deletion_status.account.pod_id
        }
      ).once

      gdpr_sns_publisher.expects(:publish).with(
        subject: "GDPR Request for #{ComplianceDeletionStatus::TALK}",
        message: {
          user_id: compliance_deletion_status.user_id,
          account_id: compliance_deletion_status.account_id,
          account_subdomain: compliance_deletion_status.account.subdomain,
          action: ComplianceDeletionStatus::REQUEST_DELETION,
          application: ComplianceDeletionStatus::TALK,
          executer_id: compliance_deletion_status.executer_id,
          pod: compliance_deletion_status.account.pod_id
        }
      ).once

      compliance_deletion_status.publish_to_incomplete!(applications)
    end
  end
end
