require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 23

describe 'Zendesk::PushNotifications::Gdpr::GdprUserDeletionSubscriber' do
  fixtures :users, :accounts

  describe '#process_queue' do
    let(:account)         { accounts(:minimum) }
    let(:admin)           { users(:minimum_admin) }
    let(:deleted_user)    { users(:minimum_end_user2) }

    let(:subscriber)      { Zendesk::PushNotifications::Gdpr::GdprSqsUserDeletionSubscriber.new }
    let(:gdpr_subscriber) { Zendesk::PushNotifications::Gdpr::GdprUserDeletionSubscriber.new(subscriber) }
    let(:json_response) do
      {
        user_id:           deleted_user.id,
        account_id:        account.id,
        account_subdomain: account.subdomain,
        action:            ComplianceDeletionStatus::REQUEST_DELETION,
        application:       ComplianceDeletionStatus::ALL_APPLICATONS,
        executer_id:       admin.id
      }.stringify_keys
    end

    before do
      # Setup Account to have at least one soft-deleted user
      deleted_user.current_user = admin
      deleted_user.delete!
    end

    describe '#process_queues' do
      it "should call itself if #process_queue returns at least 1 message" do
        gdpr_subscriber.expects(:process_queue).returns(1, 0).twice
        gdpr_subscriber.process_queues
      end

      it "should stop processing after reaching max_process_age" do
        gdpr_subscriber.stubs(:max_process_age).returns(Time.now - 1.second)
        gdpr_subscriber.expects(:process_queue).returns(1, 1).once
        gdpr_subscriber.process_queues
      end

      it "should not call itself if #process_queue returns 0 messages" do
        gdpr_subscriber.expects(:process_queue).returns(0).once
        gdpr_subscriber.process_queues
      end
    end
  end
end
