require_relative "../../../../support/test_helper"
require 'aws-sdk-sns'

SingleCov.covered! uncovered: 1

describe 'Zendesk::PushNotifications::Hooks::SNS' do
  fixtures :accounts, :tickets, :users, :device_identifiers, :mobile_sdk_apps

  let(:device_identifier) { device_identifiers(:sdk_authenticated) }
  let(:service_name) { 'amazon_sns' }
  let(:statsd_tags) { ['some_tag'] }

  describe '.after_register' do
    it 'on success; saves the endpoint_arn and increment stats' do
      endpoint_arn = 'endpoint_arn'
      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['register:success'] + statsd_tags
      )

      Zendesk::PushNotifications::Hooks::SNS.after_register_success(
        device_identifier.id, endpoint_arn, statsd_tags
      )
      identifier = ::PushNotifications::DeviceIdentifier.find(device_identifier.id)

      assert_equal endpoint_arn, identifier.sns_target_arn
    end

    it 'on failure, log the exception and increment stats' do
      exception = Aws::SNS::Errors::InvalidParameterException.new("PlatformApplicationArn",
        "Invalid parameter: PlatformApplicationArn Reason: An ARN must have at least 6 elements, not 1")

      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['register:failure'] + statsd_tags
      )

      Rails.logger.expects(:error).once

      Zendesk::PushNotifications::Hooks::SNS.after_register_failure(
        device_identifier.id, exception, statsd_tags
      )
    end

    it 'on failure, log the unknown exception and increment stats' do
      exception = StandardError.new("OtherError")

      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['register:failure'] + statsd_tags
      )

      Rails.logger.expects(:error).once

      Zendesk::PushNotifications::Hooks::SNS.after_register_failure(
        device_identifier.id, exception, statsd_tags
      )
    end
  end

  describe '.after_unregister' do
    it 'on success; deletes the device_identifier and increment stats' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['unregister:success'] + statsd_tags
      )

      Zendesk::PushNotifications::Hooks::SNS.after_unregister_success(
        device_identifier.id, statsd_tags
      )

      assert_raises ActiveRecord::RecordNotFound do
        ::PushNotifications::DeviceIdentifier.find(device_identifier.id)
      end
    end

    it 'on failure, log the exception and increment stats' do
      exception = Aws::SNS::Errors::ThrottledException.new("Throttled",
        "Sample throttled message.")

      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['unregister:failure'] + statsd_tags
      )

      Rails.logger.expects(:error).once

      Zendesk::PushNotifications::Hooks::SNS.after_unregister_failure(
        device_identifier.id, exception, statsd_tags
      )
    end

    it 'on failure, log the unknown exception and increment stats' do
      exception = StandardError.new("OtherError")

      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['unregister:failure'] + statsd_tags
      )

      Rails.logger.expects(:error).once

      Zendesk::PushNotifications::Hooks::SNS.after_unregister_failure(
        device_identifier.id, exception, statsd_tags
      )
    end
  end

  describe '.after_push' do
    it 'on success, increment badge_count' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['push:success'] + statsd_tags
      )

      Zendesk::PushNotifications::Hooks::SNS.after_push_success(
        device_identifier.id, "123", statsd_tags
      )

      assert_equal device_identifier.badge_count + 1, ::PushNotifications::DeviceIdentifier.find(device_identifier.id).badge_count
    end

    it 'on failure, log the exception and increment stats' do
      exception = Aws::SNS::Errors::InvalidParameterException.new("TargetArn",
        "Invalid parameter: TargetArn Reason: An ARN must have at least 6 elements, not 1")

      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['push:failure'] + statsd_tags
      )

      Rails.logger.expects(:error).once

      Zendesk::PushNotifications::Hooks::SNS.after_push_failure(
        device_identifier.id, exception, statsd_tags
      )
    end

    it 'on failure with Aws::SNS::Errors::EndpointDisabled type, delete the identifier log the exception and increment stats' do
      exception = Aws::SNS::Errors::EndpointDisabled.new("EndpointDisabled", "Endpoint disabled!")

      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['push:failure'] + statsd_tags
      )

      Rails.logger.expects(:error).once

      Zendesk::PushNotifications::Hooks::SNS.after_push_failure(
        device_identifier.id, exception, statsd_tags
      )

      assert_raise ActiveRecord::RecordNotFound do
        ::PushNotifications::DeviceIdentifier.find(device_identifier.id)
      end
    end

    it 'on failure, log the unknown exception and increment stats' do
      exception = StandardError.new("OtherError")

      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        service_name,
        tags: ['push:failure'] + statsd_tags
      )

      Rails.logger.expects(:error).once

      Zendesk::PushNotifications::Hooks::SNS.after_push_failure(
        device_identifier.id, exception, statsd_tags
      )
    end
  end
end
