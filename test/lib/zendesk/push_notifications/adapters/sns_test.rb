require_relative "../../../../support/test_helper"
require 'aws-sdk-sns'

SingleCov.covered!

describe 'Zendesk::PushNotifications::Adapters::SNS' do
  fixtures :users, :accounts, :mobile_apps

  let(:valid_attributes) do
    {
      token: 'foo',
      device_type: 'iphone',
      account: accounts(:minimum),
    }
  end
  let(:device_identifier) { PushNotifications::DeviceIdentifier.create!(valid_attributes) }
  let(:sns) { Zendesk::PushNotifications::Adapters::SNS.new }
  let(:device_token) { '123456' }
  let(:valid_endpoint_arn) { 'arn:aws:sns:eu-west-1:000000000000:endpoint/APNS/scarlett-inhouse/15a983de-02ec-34ca-9740-9a6ff727ab98' }

  before do
    Zendesk::Configuration.stubs(:fetch).with(:classic_aws_sns_region).returns('akey')
    Zendesk::Configuration.stubs(:fetch).with(:classic_aws_sns_access_key).returns('asecret')
    Zendesk::Configuration.stubs(:fetch).with(:classic_aws_sns_secret_key).returns('eu-west-1')
  end

  describe '#register' do
    let(:platform_application_arn) { 'arn:aws:sns:eu-west-1:000000000000:app/APNS/com.zendesk.agent' }

    before do
      response = Aws::SNS::Types::ListPlatformApplicationsResponse.new
      platform_application = Aws::SNS::Types::PlatformApplication.new
      platform_application.platform_application_arn = [platform_application_arn]
      response.platform_applications = [platform_application]
      Aws::SNS::Client.any_instance.expects(:list_platform_applications).returns(response)
    end

    it 'registers device with valid App ARN' do
      response = Aws::SNS::Types::CreateEndpointResponse.new
      response.endpoint_arn = valid_endpoint_arn
      Aws::SNS::Client.any_instance.expects(:create_platform_endpoint).with(
        platform_application_arn: platform_application_arn,
        token: device_token
      ).returns(response)

      Zendesk::PushNotifications::Hooks::SNS.expects(:after_register_success).with(
        device_identifier.id, response.endpoint_arn, []
      )

      sns.register(
        mobile_app_identifier: 'com.zendesk.agent',
        device_identifier_id: device_identifier.id,
        device_type: 'iphone',
        device_token: device_token
      )
    end

    it 'fails registration with invalid App ARN' do
      exception = Aws::SNS::Errors::InvalidParameter.new("PlatformApplicationArn",
        "Invalid parameter: PlatformApplicationArn Reason: An ARN must have at least 6 elements, not 1")
      Aws::SNS::Client.any_instance.expects(:create_platform_endpoint).with(
        platform_application_arn: nil,
        token: device_token
      ).raises(exception)

      Zendesk::PushNotifications::Hooks::SNS.expects(:after_register_failure).with(
        device_identifier.id, exception, []
      )

      sns.register(
        mobile_app_identifier: 'com.zendesk.agent',
        device_identifier_id: device_identifier.id,
        device_type: 'android',
        device_token: device_token
      )
    end
  end

  describe '#unregister' do
    it 'unregisters device with valid Endpoint ARN' do
      response = Aws::EmptyStructure.new

      Aws::SNS::Client.any_instance.expects(:delete_endpoint).with(
        endpoint_arn: valid_endpoint_arn
      ).returns(response)

      Zendesk::PushNotifications::Hooks::SNS.expects(:after_unregister_success).with(
        device_identifier.id, []
      )

      sns.unregister(device_identifier_id: device_identifier.id, endpoint_arn: valid_endpoint_arn)
    end

    it 'fails unregistration device with invalid Endpoint ARN' do
      exception = Aws::SNS::Errors::InvalidParameter.new("EndpointArn",
        "Invalid parameter: EndpointArn Reason: An ARN must have at least 6 elements, not 1")
      Aws::SNS::Client.any_instance.expects(:delete_endpoint).with(
        endpoint_arn: 'invalid'
      ).raises(exception)

      Zendesk::PushNotifications::Hooks::SNS.expects(:after_unregister_failure).with(
        device_identifier.id, exception, []
      )

      sns.unregister(
        device_identifier_id: device_identifier.id,
        endpoint_arn: 'invalid'
      )
    end
  end

  describe '#push' do
    let(:endpoint_arn) { 'arn:aws:sns:eu-west-1:000000000000:endpoint/APNS/scarlett-inhouse/a7dccf87-66b1-37b3-b39d-17c4a1b50a80' }
    let(:payload) { {aps: {alert: "sample", badge: 9, sound: 'notification.aif'}} }

    it 'sends push notification' do
      response = Aws::SNS::Types::PublishResponse.new
      response.message_id = 'd8549c55-2943-5ccd-9ede-ccafee8a69ec'

      Aws::SNS::Client.any_instance.expects(:publish).with(
        target_arn: endpoint_arn,
        message: {'APNS' => payload.to_json}.to_json,
        message_structure: 'json'
      ).returns(response)

      Zendesk::PushNotifications::Hooks::SNS.expects(:after_push_success).with(
        device_identifier.id, response.message_id, []
      )

      sns.push(
        device_type: 'iphone',
        device_identifier_id: device_identifier.id,
        endpoint_arn: endpoint_arn,
        payload: payload
      )
    end

    it 'does not send push notification if os_type is different than ios' do
      Aws::SNS::Client.any_instance.expects(:publish).with(
        target_arn: endpoint_arn,
        message: {'APNS' => payload.to_json}.to_json,
        message_structure: 'json'
      ).never

      sns.push(
        device_type: 'android',
        device_identifier_id: device_identifier.id,
        endpoint_arn: endpoint_arn,
        payload: payload
      )
    end

    it 'does not send push notification if endpoint_arn is blank' do
      Aws::SNS::Client.any_instance.expects(:publish).with(
        target_arn: '',
        message: {'APNS' => payload.to_json}.to_json,
        message_structure: 'json'
      ).never

      sns.push(
        device_type: 'iphone',
        device_identifier_id: device_identifier.id,
        endpoint_arn: '',
        payload: payload
      )
    end

    it 'fails to send push notification' do
      exception = Aws::SNS::Errors::InvalidParameter.new("TargetArn",
        "Invalid parameter: TargetArn Reason: No endpoint found for the target arn specified")
      Aws::SNS::Client.any_instance.expects(:publish).with(
        target_arn: endpoint_arn,
        message: {'APNS' => payload.to_json}.to_json,
        message_structure: 'json'
      ).raises(exception)

      Zendesk::PushNotifications::Hooks::SNS.expects(:after_push_failure).with(
        device_identifier.id, exception, []
      )

      sns.push(
        device_type: 'iphone',
        device_identifier_id: device_identifier.id,
        endpoint_arn: endpoint_arn,
        payload: payload
      )
    end
  end
end
