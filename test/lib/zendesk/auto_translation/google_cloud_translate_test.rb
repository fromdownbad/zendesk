require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::AutoTranslation::GoogleCloudTranslate do
  let(:locale_en) { ENGLISH_BY_ZENDESK }
  let(:locale_ja) { TranslationLocale.new(name: 'Japanese', locale: 'ja') }
  let(:locale_es) { TranslationLocale.new(name: 'Spanish', locale: 'es') }
  let(:locale_it) { TranslationLocale.new(name: 'Italian', locale: 'it') }

  let(:languages_en) { Zendesk::AutoTranslation::GoogleCloudTranslate.languages(locale_en) }
  let(:languages_ja) { Zendesk::AutoTranslation::GoogleCloudTranslate.languages(locale_ja) }

  it 'returns the list of languages in the provided locale' do
    assert_equal 109, languages_en.length
    assert_equal 109, languages_ja.length
  end

  it 'returns each language item as a hash with code and name' do
    english_en = languages_en.find { |lang| lang[:code] == 'en' }
    english_ja = languages_ja.find { |lang| lang[:code] == 'en' }

    assert_equal 'English', english_en[:name]
    assert_equal '英語', english_ja[:name]
  end

  it 'sorts the list of languages in the provided locale' do
    first_en = languages_en.first[:code]
    first_ja = languages_ja.first[:code]

    assert_not_equal first_en, first_ja
  end

  it 'memoizes the calls so that the list of languages is constructed once per locale' do
    collator_es = ICU::Collation::Collator.new(locale_es.locale)
    collator_it = ICU::Collation::Collator.new(locale_it.locale)

    ICU::Collation::Collator.expects(:new).with(locale_es.locale).returns(collator_es).once
    ICU::Collation::Collator.expects(:new).with(locale_it.locale).returns(collator_it).once

    Zendesk::AutoTranslation::GoogleCloudTranslate.languages(locale_es)
    Zendesk::AutoTranslation::GoogleCloudTranslate.languages(locale_es)

    Zendesk::AutoTranslation::GoogleCloudTranslate.languages(locale_it)
    Zendesk::AutoTranslation::GoogleCloudTranslate.languages(locale_it)
  end
end
