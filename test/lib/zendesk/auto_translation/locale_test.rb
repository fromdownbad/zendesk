require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::AutoTranslation::Locale do
  let(:language_code) { 'ja' }
  let(:locale) { Zendesk::AutoTranslation::Locale.new(language_code) }

  it 'returns the language code via #locale' do
    assert_equal language_code, locale.locale
  end

  it 'inherits methods from the Zendesk::LocalePresentation module' do
    assert_respond_to locale, :localized_name
    assert_respond_to locale, :display_name
  end
end
