require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 10

describe Zendesk::SecurityPolicy::Validation do
  describe "Length" do
    let(:validator) { Zendesk::SecurityPolicy::Validation::Length.new({}) }

    it "denies the password if it's not long enough" do
      validator.minimum = 5
      assert validator.deny?('abc')
    end

    it "accepts the password if it is of the minimum length or longer" do
      validator.minimum = 3
      refute validator.deny?('abc')
      refute validator.deny?('abcd')
    end
  end

  describe "MaxLength" do
    let(:validator) { Zendesk::SecurityPolicy::Validation::MaxLength.new({}) }

    it "denies the password if it's too long" do
      validator.maximum = 128
      assert validator.deny?('1' * 142)
    end

    it "accepts the password if it is shorter than maximum length" do
      validator.maximum = 5
      refute validator.deny?('abc')
      refute validator.deny?('abcd')
    end

    it "accepts the password if it is exactly maximum length" do
      validator.maximum = 5
      refute validator.deny?('abcde')
    end
  end

  describe "DifferenceWithUsername" do
    let(:validator) { Zendesk::SecurityPolicy::Validation::DifferenceWithUsername.new }

    it "denies the password if it's equal to the passed in username" do
      validator.username = "jesper"
      assert validator.deny?("jesper")
    end

    it "accepts the password if it's different than the passed in username" do
      validator.username = "jesper"
      refute validator.deny?("the_mentor")
    end
  end

  describe "AlphanumericComplexity" do
    let(:validator) { Zendesk::SecurityPolicy::Validation::AlphanumericComplexity.new }

    it "denies the password if it doesn't contain a mix of lowercase, uppercase and numbers" do
      assert validator.deny?("test123")
    end

    it "accepts the password if it contains a mix of lower/uppercase characters and numbers" do
      refute validator.deny?("Alph4Numer1c")
    end
  end

  describe "NumberComplexity" do
    let(:validator) { Zendesk::SecurityPolicy::Validation::NumberComplexity.new }

    it "denies the password if it doesn't contain numbers" do
      assert validator.deny?("something")
    end

    it "accepts the password if it contains numbers" do
      refute validator.deny?("something123secure")
    end
  end

  describe "SpecialCharacter" do
    let(:validator) { Zendesk::SecurityPolicy::Validation::SpecialCharacter.new }

    it "denies the password if it doesn't contain special characters" do
      assert validator.deny?("mypassword")
    end

    it "accepts the password if it contains special characters" do
      refute validator.deny?("My!Password#%")
    end
  end

  describe "UniqueHistory" do
    let(:validator) { Zendesk::SecurityPolicy::Validation::UniqueHistory.new }

    it "denies the password if it matches one of the previous ones" do
      validator.history = [Users::Password::BCrypt.create('myoldpassword'), Users::Password::BCrypt.create('myolderpassword')]
      assert validator.deny?(Users::Password::BCrypt.create("myoldpassword"))
    end

    it "accepts the password if it doesn't match one of the previous ones" do
      validator.history = [Users::Password::BCrypt.create('myoldpassword'), Users::Password::BCrypt.create('myolderpassword')]
      refute validator.deny?(Users::Password::BCrypt.create("mynewpassword"))
    end
  end

  describe "LocalPartFromEmail" do
    let(:validator) { Zendesk::SecurityPolicy::Validation::LocalPartFromEmail.new }

    it "accepts the password if it doesn't contain the local part of the email" do
      validator.email = "agent@zendesk.com"
      refute validator.deny?('mypassword')
    end

    it "denies the password if it contains the local part of the email" do
      validator.email = "agent@zendesk.com"
      assert validator.deny?('myagentpassword')
      assert validator.deny?('agentpassword')
      assert validator.deny?('secureagent')
    end
  end

  describe "MaxCharsNumbersInSequence" do
    let(:validator) { Zendesk::SecurityPolicy::Validation::MaxCharsNumbersInSequence.new }

    before { validator.max_consecutive_letters_and_numbers = 4 }

    it "denies the password if it has a sequence of letters longer than the allowed" do
      assert validator.deny?("abcde")
    end

    it "accepts the password if it has a sequence of letters equal to or shorter than the allowed" do
      refute validator.deny?("abcd")
      refute validator.deny?("abc")
    end

    it "accepts the password if it has a sequence of mixed case letters longer than the allowed" do
      refute validator.deny?("ABcdE")
    end

    it "denies the password if it has a sequence of numbers longer than the allowed" do
      assert validator.deny?("12345")
    end

    it "accepts the password if it has a sequence of numbers equal to or shorter than the allowed" do
      refute validator.deny?("1234")
      refute validator.deny?("123")
    end

    it "accepts the password even if it's an ordinal sequence of special characters" do
      refute validator.deny?("!\#$%")
    end
  end
end
