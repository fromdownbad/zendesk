require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 5

class TestField
  include Zendesk::FieldCompare::Base
  attr_reader :value, :account

  def initialize(val, account)
    @value = val
    @account = account
  end
end

describe 'FieldCompareBase' do
  fixtures :accounts

  describe "comparison" do
    before do
      @field = TestField.new(123.45, accounts(:minimum))
    end

    it "'is's should work as expected" do
      assert @field.compare(@field.value, "is", 123.45)
      refute @field.compare(@field.value, "is", nil)

      assert @field.compare(nil, "is", nil)
      assert @field.compare(nil, "is", "")
    end

    it "'is_not's should work as expected" do
      refute @field.compare(@field.value, "is_not", 123.45)
      assert @field.compare(@field.value, "is_not", nil)

      refute @field.compare(nil, "is_not", nil)
      refute @field.compare(nil, "is_not", "")
    end

    it "'less_than's should work as expected" do
      assert @field.compare(@field.value, "less_than", 123.46)
      refute @field.compare(@field.value, "less_than", nil)

      refute @field.compare(nil, "less_than", nil)
    end

    it "'less_than_equal's should work as expected" do
      assert @field.compare(@field.value, "less_than_equal", 123.45)
      refute @field.compare(@field.value, "less_than_equal", nil)

      refute @field.compare(nil, "less_than_equal", nil)
    end

    it "'greater_than's should work as expected" do
      assert @field.compare(@field.value, "greater_than", -123.46)
      refute @field.compare(@field.value, "greater_than", nil)

      refute @field.compare(nil, "greater_than", nil)
    end

    it "'greater_than_equal's should work as expected" do
      assert @field.compare(@field.value, "greater_than_equal", 123.45)
      refute @field.compare(@field.value, "greater_than_equal", nil)

      refute @field.compare(nil, "greater_than_equal", nil)
    end

    it "'includes_words's should work as expected" do
      @field = TestField.new("wombat ram box", accounts(:minimum))
      assert @field.compare(@field.value, "includes_words", "wombat")
      refute @field.compare(@field.value, "includes_words", nil)
    end

    it "'includes_string's should work as expected" do
      @field = TestField.new("wombat is awesome and stuff", accounts(:minimum))
      assert @field.compare(@field.value, "includes_string", "wombat is awesome")
      refute @field.compare(@field.value, "includes_string", nil)
    end
  end
end
