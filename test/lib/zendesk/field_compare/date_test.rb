require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::FieldCompare::Date do
  class DateTestField
    include Zendesk::FieldCompare::Base
    include Zendesk::FieldCompare::Date
    attr_reader :value

    def initialize(val)
      @value = val
    end
  end

  before do
    @field = DateTestField.new("2012-02-18")
  end

  describe "dates" do
    before do
      @theday = Time.parse("2012-02-18")
    end

    describe "#compare" do
      it "'is's should work as expected" do
        assert @field.compare(@field.value, "is", @theday)
        assert @field.compare(@field.value, "is", "2012-02-18")
        refute @field.compare(@field.value, "is", Time.now)
        refute @field.compare(@field.value, "is", nil)

        # if no value set
        assert @field.compare(nil, "is", nil)
        assert @field.compare(nil, "is", "")
        refute @field.compare(nil, "is", Time.now)
      end

      it "'is_not's should work as expected" do
        refute @field.compare(@field.value, "is_not", @theday)
        refute @field.compare(@field.value, "is_not", "2012-02-18")
        assert @field.compare(@field.value, "is_not", Time.now)
        assert @field.compare(@field.value, "is_not", nil)

        # if no value set
        refute @field.compare(nil, "is_not", nil)
        refute @field.compare(nil, "is_not", "")
        assert @field.compare(nil, "is_not", Time.now)
      end

      it "'less_than's should work as expected" do
        assert @field.compare(@field.value, "less_than", @theday.advance(seconds: 1))
        assert @field.compare(@field.value, "less_than", "2012-02-19")
        refute @field.compare(@field.value, "less_than", @theday)
        refute @field.compare(@field.value, "less_than", nil)

        # if no value set
        refute @field.compare(nil, "less_than", nil)
        refute @field.compare(nil, "less_than", Time.now)
      end

      it "'greater_than's should work as expected" do
        assert @field.compare(@field.value, "greater_than", @theday.advance(seconds: -1))
        assert @field.compare(@field.value, "greater_than", "2012-02-17")
        refute @field.compare(@field.value, "greater_than", @theday)
        refute @field.compare(@field.value, "greater_than", nil)

        # if no value set
        refute @field.compare(nil, "greater_than", nil)
        refute @field.compare(nil, "greater_than", Time.now)
      end

      it "'less_than_equal's should work as expected" do
        assert @field.compare(@field.value, "less_than_equal", @theday)
        assert @field.compare(@field.value, "less_than_equal", "2012-02-19")
        refute @field.compare(@field.value, "less_than_equal", @theday.advance(seconds: -1))
        refute @field.compare(@field.value, "less_than_equal", nil)

        # if no value set
        refute @field.compare(nil, "less_than_equal", nil)
        refute @field.compare(nil, "less_than_equal", Time.now)
      end

      it "'greater_than_equal's should work as expected" do
        assert @field.compare(@field.value, "greater_than_equal", @theday)
        assert @field.compare(@field.value, "greater_than_equal", "2012-02-17")
        refute @field.compare(@field.value, "greater_than_equal", @theday.advance(seconds: 1))
        refute @field.compare(@field.value, "greater_than_equal", nil)

        # if no value set
        refute @field.compare(nil, "greater_than_equal", nil)
        refute @field.compare(nil, "greater_than_equal", Time.now)
      end

      it "'within_next_n_days's should work as expected" do
        # |-----|-----|
        # ^           v
        # value just outside 2 day boundary in future
        Timecop.freeze(@theday - 2.days.to_i)

        refute @field.compare(@field.value, "within_next_n_days", 1)
        refute @field.compare(@field.value, "within_next_n_days", 2)
        assert @field.compare(@field.value, "within_next_n_days", 1000)
        refute @field.compare(@field.value, "within_next_n_days", nil)

        # |-----|-----|
        #  ^          v
        # value AT two day boundary in future
        Timecop.freeze(@theday - 2.days.to_i + 1.second) # 2012-02-16 00:00:01

        refute @field.compare(@field.value, "within_next_n_days", 1)
        assert @field.compare(@field.value, "within_next_n_days", 2)
        assert @field.compare(@field.value, "within_next_n_days", 1000)
        refute @field.compare(@field.value, "within_next_n_days", nil)

        # |-----|-----|
        # v           ^
        # value BEFORE two day boundary in past
        Timecop.freeze(@theday + 2.days.to_i)

        refute @field.compare(@field.value, "within_next_n_days", 1)
        refute @field.compare(@field.value, "within_next_n_days", 2)
        refute @field.compare(@field.value, "within_next_n_days", 1000)
        refute @field.compare(@field.value, "within_next_n_days", nil)

        # |-----|----
        # v
        # ^
        # value at NOW
        Timecop.freeze(@theday)

        refute @field.compare(@field.value, "within_next_n_days", 0)
        assert @field.compare(@field.value, "within_next_n_days", 1)
        assert @field.compare(@field.value, "within_next_n_days", 2)
        assert @field.compare(@field.value, "within_next_n_days", 1000)
        refute @field.compare(@field.value, "within_next_n_days", nil)

        # |-----|----
        # v
        #  ^
        # value just before NOW
        Timecop.freeze(@theday + 1.second)

        refute @field.compare(@field.value, "within_next_n_days", 0)
        refute @field.compare(@field.value, "within_next_n_days", 1)
        refute @field.compare(@field.value, "within_next_n_days", 2)
        refute @field.compare(@field.value, "within_next_n_days", 1000)
        refute @field.compare(@field.value, "within_next_n_days", nil)

        refute @field.compare(nil, "within_next_n_days", nil)
        refute @field.compare(nil, "within_next_n_days", 1)
      end

      it "'within_previous_n_days's should work as expected" do
        # |-----|-----|
        # ^           v
        # value AT 2 day boundary in future
        Timecop.freeze(@theday - 2.days.to_i)

        refute @field.compare(@field.value, "within_previous_n_days", 1)
        refute @field.compare(@field.value, "within_previous_n_days", 2)
        refute @field.compare(@field.value, "within_previous_n_days", 1000)
        refute @field.compare(@field.value, "within_previous_n_days", nil)

        # |-----|-----|
        # v           ^
        # value AT 2 day boundary in past
        Timecop.freeze(@theday + 2.days.to_i)

        refute @field.compare(@field.value, "within_previous_n_days", 1)
        assert @field.compare(@field.value, "within_previous_n_days", 2)
        assert @field.compare(@field.value, "within_previous_n_days", 1000)
        refute @field.compare(@field.value, "within_previous_n_days", nil)

        # |-----|-----|-
        # v            ^
        # value just BEFORE 2 day boundary in past
        Timecop.freeze(@theday + 2.days.to_i + 1.second)

        refute @field.compare(@field.value, "within_previous_n_days", 1)
        refute @field.compare(@field.value, "within_previous_n_days", 2)
        assert @field.compare(@field.value, "within_previous_n_days", 1000)
        refute @field.compare(@field.value, "within_previous_n_days", nil)

        # |-----|---
        # v^
        # value just BEFORE now
        Timecop.freeze(@theday + 1.second)

        refute @field.compare(@field.value, "within_previous_n_days", 0)
        assert @field.compare(@field.value, "within_previous_n_days", 1)
        assert @field.compare(@field.value, "within_previous_n_days", 2)
        assert @field.compare(@field.value, "within_previous_n_days", 1000)
        refute @field.compare(@field.value, "within_previous_n_days", nil)

        # |-----|---
        # v
        # ^
        # value AT now
        Timecop.freeze(@theday)

        refute @field.compare(@field.value, "within_previous_n_days", 0)
        refute @field.compare(@field.value, "within_previous_n_days", 1)
        refute @field.compare(@field.value, "within_previous_n_days", 2)
        refute @field.compare(@field.value, "within_previous_n_days", 1000)
        refute @field.compare(@field.value, "within_previous_n_days", nil)

        refute @field.compare(nil, "within_previous_n_days", nil)
        refute @field.compare(nil, "within_previous_n_days", 1)
      end
    end
  end
end
