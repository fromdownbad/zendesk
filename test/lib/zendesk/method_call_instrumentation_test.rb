require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::MethodCallInstrumentation do
  let(:method_name) { :create_chat_agent! }
  let(:exec_context) { stub(caller_locations: trace) }
  let(:trace) { [] }
  let(:source_pattern) { 'app\/services\/provisioning\/before_actions' }

  let(:instrumentation) do
    Zendesk::MethodCallInstrumentation.new(
      method_name: method_name,
      exec_context: exec_context,
      source_pattern: source_pattern
    )
  end

  describe '#perform' do
    let(:version_sha) { SecureRandom.hex }
    let(:statsd_client) { stub_for_statsd }
    let(:perform) { instrumentation.perform }

    before do
      Zendesk::StatsD::Client.stubs(:new).with(namespace: 'method_call_instrumentation').returns(statsd_client)
    end

    describe 'when first matching trace is from expected source' do
      let(:trace) do
        [
          "/opt/zendesk/versions/#{version_sha}/app/services/provisioning/before_actions.rb:32:in `enable_chat_only_role`'",
          "/opt/zendesk/versions/#{version_sha}/app/app/controllers/api/v2/users_controller.rb:731:in `update'`",
        ]
      end

      let(:tags) do
        ['method_name: create_chat_agent!', 'source:app/services/provisioning/before_actions.rb', 'line:32']
      end

      it 'reports change source metrics to Datadog, with project irrelevant paths stripped' do
        statsd_client.expects(:increment).with('create_chat_agent!_called', tags: tags).once
        perform
      end
    end

    describe 'when there is no matching trace found' do
      let(:trace) do
        [
          "/opt/zendesk/versions/#{version_sha}/app/lib/somewhere/some_file.rb:99:in `some_method`'",
          "/opt/zendesk/versions/#{version_sha}/app/app/controllers/api/v2/users_controller.rb:731:in `update'`",
        ]
      end

      let(:tags) do
        ['method_name: create_chat_agent!', 'source:unknown', 'line:']
      end

      it 'reports unknown source to Datadog' do
        statsd_client.expects(:increment).with('create_chat_agent!_called', tags: tags).once
        perform
      end

      it 'logs stack trace' do
        Rails.logger.expects(:info).with("Unexpected call detected for create_chat_agent!, trace: #{trace}")
        perform
      end
    end
  end
end
