require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 9

describe 'Zendesk::Devices::Manager' do
  fixtures :accounts, :users

  describe "Device Manager" do
    before do
      @session = {}
      @account = accounts(:minimum)
      @user = users(:minimum_agent)
      @password_strategy = Zendesk::Auth::Warden::PasswordStrategy.new({})
      @params = { ip: '1.2.3.4', user_agent: 'Mozilla/5.0' }
    end

    describe "Given an untracked strategy" do
      before do
        @basic_strategy = Zendesk::Auth::Warden::MasterTokenStrategy.new({})
        @session[Zendesk::Devices::Manager::STORE_KEY] = "UNCHANGED"
      end

      it "does not track the device" do
        refute_difference 'Device.count(:all)' do
          @manager = Zendesk::Devices::Manager.new(@account, @session, @user, @basic_strategy, @params)
          @result = @manager.track
        end
      end

      it "does not change the session" do
        assert_equal("UNCHANGED", @session[Zendesk::Devices::Manager::STORE_KEY])
      end
    end

    describe "Given an untracked device" do
      before do
        @perform_deliveries = ActionMailer::Base.perform_deliveries
        ActionMailer::Base.perform_deliveries = true
        @manager = Zendesk::Devices::Manager.new(@account, @session, @user, @password_strategy, @params)
      end

      after do
        ActionMailer::Base.perform_deliveries = @perform_deliveries
      end

      describe "tracking the device" do
        before { @result = @manager.track }

        should_change("the number of devices", by: 1) do
          Device.count(:all)
        end

        it "stores a device token in the session" do
          assert @session[Zendesk::Devices::Manager::STORE_KEY]
        end

        it "captures data about the device" do
          assert_equal(@params[:ip], @result.ip)
          assert_equal(@params[:user_agent], @result.user_agent)
        end
      end

      describe "tracking the device without notification" do
        before { @result = @manager.track(notify: false) }

        should_change("the number of devices", by: 1) do
          Device.count(:all)
        end

        it("store a device token in the session") do
          assert @session[Zendesk::Devices::Manager::STORE_KEY]
        end

        should_change("the number of mail deliveries", by: 0) do
          ActionMailer::Base.deliveries.size
        end
      end

      describe "with a JWTStrategy" do
        before do
          @strategy = Zendesk::Auth::Warden::JWTStrategy.new({})
          @manager = Zendesk::Devices::Manager.new(@account, @session, @user, @strategy, @params)
          @manager.track
        end

        should_change("the number of devices", by: 1) do
          Device.count(:all)
        end
      end

      describe "Given a user that wants device notifications" do
        before do
          @user.settings.device_notification = true
        end

        describe "tracking the device" do
          before { @manager.track }

          should_change("the number of mail deliveries", by: 1) do
            ActionMailer::Base.deliveries.size
          end
        end
      end

      describe "Given a user that does not want device notifications" do
        before do
          @user.settings.device_notification = false
        end

        describe "tracking the device" do
          before do
            @user.settings.reload
            @manager.track
          end

          should_change("the number of mail deliveries", by: 0) do
            ActionMailer::Base.deliveries.size
          end
        end
      end
    end

    describe "Given an untracked mobile device" do
      before do
        @manager = Zendesk::Devices::Manager.new(@account, @session, @user, @password_strategy, @params.merge(type: :mobile, token: '7'))
      end

      describe "tracking the device" do
        before { @result = @manager.track }

        should_change("the number of mobile devices", by: 1) do
          MobileDevice.count(:all)
        end

        it "uses given token" do
          assert_equal '7', MobileDevice.last.token
        end
      end
    end

    describe "Given a device" do
      before do
        @device_token = "TOKEN"
        @device = @user.devices.create(@params.merge(account: @account))
        @device.update_attribute(:token, @device_token)
        @cookie = { @user.id.to_s => @device_token }
      end

      describe "Given multiple users on the same device" do
        before do
          @second_user = users(:support_agent)
          @manager = Zendesk::Devices::Manager.new(@account, @session, @user, @password_strategy, @params)
          @result = @manager.track
        end

        describe "With both device tokens in the cookie" do
          before do
            @second_device = @second_user.devices.create(@params.merge(account: @account))
            @second_device.update_attribute(:token, "SECOND_TOKEN")
            @cookie[@second_user.id.to_s] = "SECOND_TOKEN"
            @session[Zendesk::Devices::Manager::STORE_KEY] = @cookie

            refute_difference 'Device.count(:all)' do
              @second_manager = Zendesk::Devices::Manager.new(@account, @session, @second_user, @password_strategy, @params)
              @second_result = @manager.track
            end
          end

          it "does not change the cookie" do
            assert_equal @cookie, @session[Zendesk::Devices::Manager::STORE_KEY]
          end
        end

        describe "With only one device token in the cookie" do
          before do
            @session[Zendesk::Devices::Manager::STORE_KEY] = @cookie

            assert_difference 'Device.count(:all)' do
              @manager = Zendesk::Devices::Manager.new(@account, @session, @second_user, @password_strategy, @params)
              @second_result = @manager.track
            end
          end

          it "stores the device token in the cookie" do
            assert @cookie[@second_user.id.to_s]
          end
        end
      end

      describe "And the same token in the session" do
        before do
          @session[Zendesk::Devices::Manager::STORE_KEY] = @cookie
          refute_difference 'Device.count(:all)' do
            @manager = Zendesk::Devices::Manager.new(@account, @session, @user, @password_strategy, @params)
            @result = @manager.track
          end
        end

        it "returns the matching device" do
          assert_equal(@device, @result)
        end

        it "does not change the session key" do
          assert_equal(@cookie, @session[Zendesk::Devices::Manager::STORE_KEY])
        end

        before_should "register activity on the device" do
          Device.any_instance.expects(:register_activity!)
        end
      end

      describe 'And new ip and user agent info' do
        before do
          @params = { ip: '5.6.7.8', user_agent: 'Chrome/2.0' }
          @session[Zendesk::Devices::Manager::STORE_KEY] = @cookie
          @manager = Zendesk::Devices::Manager.new(@account, @session, @user, @password_strategy, @params)
          @result = @manager.track
        end

        it 'updates device with latest info' do
          assert_equal('5.6.7.8', @device.reload.ip)
          assert_equal('Chrome/2.0', @device.reload.user_agent)
        end
      end

      describe "And a different token in the session" do
        before do
          @session[Zendesk::Devices::Manager::STORE_KEY] = { @user.id.to_s => "TOKEN2" }
          assert_difference 'Device.count(:all)' do
            @manager = Zendesk::Devices::Manager.new(@account, @session, @user, @password_strategy, @params)
            @result = @manager.track
          end
        end

        it "returns a new device" do
          assert_not_equal(@device, @result)
        end

        it "stores the new device token" do
          assert_not_equal("TOKEN2", @session[Zendesk::Devices::Manager::STORE_KEY][@user.id.to_s])
        end
      end
    end
  end
end
