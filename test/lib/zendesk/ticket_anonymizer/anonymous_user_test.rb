require_relative "../../../support/test_helper"

ENV['MAX_TICKETS_PER_ANONYMOUS_USER'] = '2'
SingleCov.covered!

describe Zendesk::TicketAnonymizer::AnonymousUser do
  fixtures :accounts, :tickets
  let(:account) { accounts(:minimum) }
  let(:tickets) { account.tickets }

  describe "anonymous user exists" do
    before do
      account.users.create! do |u|
        u.name = 'Anonymous User'
        u.skip_verification = true
        u.settings.scrubbed_ticket_anonymous_user = true
      end
    end

    it "fetches the anonymous user" do
      anonymous_user = Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account)
      assert_equal 'Anonymous User', anonymous_user.name
    end
  end

  describe "the anonymous user is always the same" do
    let(:anonymous_user) do
      Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account)
    end

    it "should have the same id when the method #find_or_create is called" do
      anonymous_user2 = Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account)
      assert_equal anonymous_user2.id, anonymous_user.id, "Expected the anonymous user ids to match"
    end
  end

  describe 'anonymous user roll over' do
    before do
      Arturo.enable_feature!(:ticket_metadata_deletion_at_close)
      Arturo.enable_feature!(:ticket_user_scrubbing_after_close)
      Account.any_instance.stubs(:has_delete_ticket_metadata_pii_enabled?).returns(true)
      tickets.take(4).each do |ticket|
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(users(:minimum_agent))
        ticket.save!
        ticket_scrubber = Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket)
        CIA.audit actor: User.system do
          ticket_scrubber.scrub_ticket
        end
      end
    end

    it 'creates an extra anonymous user' do
      assert_equal 2, account.scrubbed_ticket_anonymous_users.size
    end

    it 'distribute tickets across anonymous users' do
      assert_equal [2, 2], account.scrubbed_ticket_anonymous_users.map { |u| u.tickets.size }
    end
  end
end
