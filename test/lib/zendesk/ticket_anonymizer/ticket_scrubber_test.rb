require_relative "../../../support/test_helper"
require_relative '../../../support/ticket_test_helper'

SingleCov.covered!

describe Zendesk::TicketAnonymizer::TicketScrubber do
  include TicketTestHelper

  fixtures :accounts, :tickets, :events, :ticket_fields, :event_decorations, :sequences
  let(:account) { accounts(:minimum) }
  let(:anonymous_user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account) }

  describe_with_arturo_enabled :ticket_user_scrubbing_after_close do
    before do
      Arturo.enable_feature!(:ticket_metadata_deletion_at_close)
      account.settings.delete_ticket_metadata_pii = true
      account.settings.save!
    end

    describe "with the account setting :delete_ticket_metadata_pii disabled" do
      let(:ticket) { tickets(:minimum_2) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:requester) { ticket.requester }
      let(:agent) { ticket.submitter }

      before do
        Arturo.enable_feature!(:ticket_metadata_deletion_at_close)
        account.settings.delete_ticket_metadata_pii = false
        account.settings.save!
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(agent)
        ticket.save!(validate: false)
        ticket.reload
      end

      it "does not perform any scrubbing" do
        Zendesk::TicketAnonymizer::TicketScrubber.any_instance.expects(:scrub_pii_in_events).never
        Zendesk::TicketAnonymizer::TicketScrubber.any_instance.expects(:switch_submitter).never
        Zendesk::TicketAnonymizer::TicketScrubber.any_instance.expects(:anonymous_user).never

        ticket_scrubber.scrub_ticket
      end
    end

    describe "scrubbing the requester and submitter" do
      let(:ticket) { tickets(:minimum_2) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:requester) { ticket.requester }
      let(:agent) { ticket.submitter }

      before do
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(agent)
        ticket.save!(validate: false)
        ticket.reload
      end

      describe "when the ticket has an end user as requester and agent as submitter" do
        describe "scrubbing process finished successfully" do
          before do
            @old_time_stamp = ticket.updated_at
            Timecop.freeze 1.second.from_now
            ticket_scrubber.scrub_ticket
            ticket.reload
          end

          it "scrubs the end user" do
            assert_equal anonymous_user.id, ticket.requester.id, "Expected requester to be replaced by the anonymous user"
          end

          it "doesn't scrub the agent" do
            assert_equal agent.id, ticket.submitter.id, "Expected submitter not to be changed"
          end

          it "update ticket timestamp" do
            assert_not_equal @old_time_stamp.to_i, ticket.updated_at.to_i
          end
        end

        describe "scrubbing process failed" do
          before do
            @old_time_stamp = ticket.updated_at
            Timecop.freeze 1.second.from_now
            ticket_scrubber.rescue_errors_in_tests = true
            Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once
            ticket_scrubber.scrub_ticket
            ticket.reload
          end

          it "doesn't changes the requester" do
            assert_equal requester.id, ticket.requester.id
          end

          it "doesn't change the submitter" do
            assert_equal agent.id, ticket.submitter.id
          end

          it "doesn't update ticket timestamp" do
            assert_equal @old_time_stamp.to_i, ticket.updated_at.to_i
          end
        end
      end

      describe "when the ticket has an end user as requester and an end user as submitter" do
        describe "scrubbing process finished successfully" do
          before do
            ticket.will_be_saved_by(agent)
            ticket.submitter_id = requester.id
            ticket.save!(validate: false)
            @old_time_stamp = ticket.updated_at
            Timecop.freeze 1.second.from_now
            ticket_scrubber.scrub_ticket
            ticket.reload
          end

          it "scrubs the end user" do
            assert_equal anonymous_user.id, ticket.requester.id, "Expected requester to be replaced by the anonymous user"
            assert_equal anonymous_user.id, ticket.submitter.id, "Expected submitter to be replaced by the anonymous user"
          end

          it "update ticket timestamp" do
            assert_not_equal @old_time_stamp.to_i, ticket.updated_at.to_i
          end
        end

        describe "scrubbing process failed" do
          before do
            ticket.will_be_saved_by(agent)
            ticket.submitter_id = requester.id
            ticket.save!(validate: false)
            @old_time_stamp = ticket.updated_at
            Timecop.freeze 1.second.from_now
            ticket_scrubber.rescue_errors_in_tests = true
            Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once
            ticket_scrubber.scrub_ticket
            ticket.reload
          end

          it "doesn't scrub the end user" do
            assert_equal requester.id, ticket.requester.id
            assert_equal requester.id, ticket.submitter.id
          end

          it "doesn't update ticket timestamp" do
            assert_equal @old_time_stamp.to_i, ticket.updated_at.to_i
          end
        end
      end

      describe "when the ticket has an agent as requester and an agent as submitter" do
        describe "scrubbing process finished successfully" do
          before do
            ticket.will_be_saved_by(agent)
            ticket.submitter_id = agent.id
            ticket.requester_id = agent.id
            ticket.save!(validate: false)
            @old_time_stamp = ticket.updated_at
            Timecop.freeze 1.second.from_now
            ticket_scrubber.scrub_ticket
            ticket.reload
          end

          it "does not scrub the agent" do
            assert_equal agent.id, ticket.requester.id, "Expected requester not to be replaced by the anonymous user"
            assert_equal agent.id, ticket.submitter.id, "Expected submitter not to be replaced by the anonymous user"
          end

          it "update ticket timestamp" do
            assert_not_equal @old_time_stamp, ticket.updated_at
          end
        end
      end
    end

    describe "scrub author in events" do
      let(:agent) { users(:minimum_agent) }
      let(:ticket) { tickets(:minimum_5) }
      let(:requester) { ticket.requester }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }

      describe "scrubbing process finished succesfully" do
        it "scrubs the end user events" do
          assert_not_equal 0, ticket.events.to_a.count { |event| event.author_id == requester.id }
          assert_equal 0, ticket.events.to_a.count { |event| event.author_id == anonymous_user.id }
          ticket_scrubber.scrub_ticket
          ticket.reload
          assert_equal StatusType.CLOSED, ticket.status_id
          assert_equal 0, ticket.events.to_a.count { |event| event.author_id == requester.id }
          assert_not_equal 0, ticket.events.to_a.count { |event| event.author_id == anonymous_user.id }
        end

        it "deletes metadata from all merge type audits when a ticket is merged into another" do
          source = ticket_create(agent)
          source.will_be_saved_by(agent)
          source.save!
          target = ticket_create(agent)
          target.will_be_saved_by(agent)
          target.save!
          source_scrubber = Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: source)
          target_scrubber = Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: target)

          merger = Zendesk::Tickets::Merger.new(user: agent)
          merge_args = {
            target: target,
            sources: [source],
            target_comment: "#{source.id} has been merged in this ticket",
            source_comment: "Ticket closed and merged in #{target.id}"
          }
          CIA.audit actor: agent do
            merger.merge(merge_args)
            source_scrubber.scrub_ticket
            target_scrubber.scrub_ticket
          end
          source.reload
          target.reload

          end_users_in_source = source.events.select { |event| event.author.is_end_user? }
          end_users_in_target = source.events.select { |event| event.author.is_end_user? }

          assert_equal 0, end_users_in_source.count
          assert_equal 0, end_users_in_target.count
        end

        it "does not scrub agent events" do
          agent_events = ticket.events.to_a.count { |event| event.author_id == agent.id }
          assert_not_equal 0, agent_events
          ticket_scrubber.scrub_ticket
          ticket.reload
          agent_events_after_close = ticket.events.to_a.count { |event| event.author_id == agent.id }
          assert_equal StatusType.CLOSED, ticket.status_id
          assert_equal agent_events, agent_events_after_close
        end

        describe "when the requester is an agent" do
          before do
            ticket.update_column :requester_id, agent.id
            ticket.reload
          end

          it "does not scrub agent events" do
            agent_events = ticket.events.to_a.count { |event| event.author_id == agent.id }
            assert_not_equal 0, agent_events
            ticket_scrubber.scrub_ticket
            ticket.reload
            agent_events_after_close = ticket.events.to_a.count { |event| event.author_id == agent.id }
            assert_equal StatusType.CLOSED, ticket.status_id
            assert_equal agent_events, agent_events_after_close
          end
        end
      end

      describe "scrubbing process failed" do
        it "doesn't scrub the author" do
          requester_events_before_close = ticket.events.to_a.count { |event| event.author_id == requester.id }
          agent_events_before_close = ticket.events.to_a.count { |event| event.author_id == agent.id }
          ticket_scrubber.rescue_errors_in_tests = true
          Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once
          ticket_scrubber.scrub_ticket
          ticket.reload
          assert_equal StatusType.CLOSED, ticket.status_id
          assert_equal requester_events_before_close, ticket.events.to_a.count { |event| event.author_id == requester.id }
          assert_equal agent_events_before_close, ticket.events.to_a.count { |event| event.author_id == agent.id }
          assert_equal 0, ticket.events.to_a.count { |event| event.author_id == anonymous_user.id }
        end
      end
    end

    describe "Scrub recipients in notifications" do
      let(:ticket) { tickets(:minimum_1) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:requester) { ticket.requester }
      let(:agent) { users(:minimum_agent) }
      let(:audit) { events(:create_audit_for_minimum_ticket_1) }
      let(:anonymous_user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: ticket.account) }
      let(:end_user1) { users(:minimum_end_user) }
      let(:end_user2) { users(:minimum_end_user2) }
      let(:event_data) do
        {
          account:    ticket.account,
          ticket:     ticket,
          author:     requester,
          # also insert a random non-existing user id:
          recipients: [users(:minimum_agent), end_user1, end_user2, requester].map(&:id) << 17293491,
          audit:      audit,
          subject:    'The rain in Spain',
          body:       'Stays main in the plain',
        }
      end

      let!(:notifications_with_recipients) do
        types = %w[
          Notification
          NotificationWithCcs
          Cc
          FollowerNotification
          SmsNotification
          AnswerBotNotification
        ]

        types.map! do |type|
          notification = Notification.create!(event_data)
          notification.update_column :type, type

          notification
        end

        ticket.reload.events.where(id: types.map(&:id))
      end

      before do
        ticket.will_be_saved_by(agent)
        ticket.status_id = StatusType.CLOSED
        ticket.save!(validate: false)
      end

      it "switches end users in the recipients" do
        ticket_scrubber.scrub_ticket
        notifications_with_recipients.each do |n|
          refute_includes n.reload.recipients, end_user1.id, "Found the requester id in the notification recipients"
          refute_includes n.reload.recipients, end_user2.id, "Found the requester id in the notification recipients"
          assert_includes n.reload.recipients, anonymous_user.id, "Anonymous user not found in the recipients array"
        end
      end

      describe "when the requester is an agent" do
        before do
          ticket.update_column :requester_id, agent.id
          ticket.reload
        end

        it "does not scrub agent from notifications" do
          ticket_scrubber.scrub_ticket
          notifications_with_recipients.each do |n|
            assert_includes n.reload.recipients, agent.id, "Did not find the agent id in the notification recipients"
            assert_includes n.reload.recipients, anonymous_user.id, "Anonymous user not found in the recipients array"
          end
        end

        it "switches end users in the recipients" do
          ticket_scrubber.scrub_ticket
          notifications_with_recipients.each do |n|
            refute_includes n.reload.recipients, end_user1.id, "Found the requester id in the notification recipients"
            refute_includes n.reload.recipients, end_user2.id, "Found the requester id in the notification recipients"
            assert_includes n.reload.recipients, anonymous_user.id, "Anonymous user not found in the recipients array"
          end
        end
      end
    end

    describe "Scrub voice comments" do
      let(:ticket) { tickets(:minimum_1) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:requester) { ticket.requester }
      let(:agent) { users(:minimum_agent) }
      let(:audit) { events(:create_audit_for_minimum_ticket_1) }
      let(:anonymous_user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: ticket.account) }

      let(:value_previous) do
        {
          recording_url:        "http://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/RE5d4706b2a7ce9c6085119352bc3f96c",
          recording_duration:   "7",
          transcription_text:   "foo: hi\nbar: bye\n",
          transcription_status: "completed",
          call_duration:        126,
          answered_by_id:       requester.id,
          answered_by_name:     requester.name,
          call_id:              7890,
          from:                 "+14155556666",
          to:                   "+14155557777",
          started_at:           "June 04, 2014 05:10:27 AM",
          outbound:             false
        }
      end

      let(:voice_comments) do
        types = %w[
          VoiceApiComment
          VoiceComment
        ]

        types.map! do |type|
          comment = VoiceComment.create!(
            account:    ticket.account,
            ticket:     ticket,
            audit:      audit
          )
          comment.update_column :value_previous, value_previous
          comment.update_column :type, type
          comment
        end
        ticket.reload.events.where(id: types.map(&:id))
      end

      before do
        ticket.will_be_saved_by(agent)
        ticket.status_id = StatusType.CLOSED
        ticket.save!(validate: false)
      end

      describe "scrubbing process finished successfully" do
        it "switches the answered_by_id and answered_by_name in voice_comments" do
          ticket_scrubber.scrub_ticket
          voice_comments.each do |comment|
            assert_equal comment.reload.value_previous[:answered_by_id], anonymous_user.id
            assert_equal comment.reload.value_previous[:answered_by_name], anonymous_user.name
          end
        end

        describe "when the requester is an agent" do
          before do
            ticket.update_column :requester_id, agent.id
            ticket.reload
          end

          it "does not scrub agent from notifications" do
            voice_comments # pre-load the comments
            ticket_scrubber.scrub_ticket
            voice_comments.each do |comment|
              assert_equal comment.reload.value_previous[:answered_by_id], agent.id
              assert_equal comment.reload.value_previous[:answered_by_name], agent.name
            end
          end
        end
      end

      describe "scrubbing process failed" do
        it "doesn't modify any voice comment" do
          voice_comments # pre-load the comments
          ticket_scrubber.rescue_errors_in_tests = true
          Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once
          ticket_scrubber.scrub_ticket
          voice_comments.each do |comment|
            assert_equal comment.reload.value_previous[:answered_by_id], requester.id
            assert_equal comment.reload.value_previous[:answered_by_name], requester.name
          end
        end
      end
    end

    describe "Anonymize ChannelBackEvent" do
      let(:ticket) { tickets(:minimum_1) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:requester) { ticket.requester }
      let(:agent) { users(:minimum_agent) }
      let(:anonymous_user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: ticket.account) }
      let(:audit) { events(:create_audit_for_minimum_ticket_1) }

      let!(:event) do
        event = ChannelBackEvent.new(
          audit: audit,
          source_id: 4,
          account:    ticket.account,
          ticket:     ticket,
          author:     agent,
          value:      {
            external_id: "external_id",
            source: {name: "agent", screen_name: "agent"},
            requester: { name: "end_user", screen_name: "end_user"},
            text: "@end_user reply from agent"
          }
        )
        event.stubs(:initiate_channels_conversion).returns(nil)

        event.mark_for_scrubbing = 1
        event.save!(validate: false)
        event
      end

      before do
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(agent)
        ticket.save!(validate: false)
        ticket_scrubber.scrub_ticket
        ticket.reload
      end

      it 'removes pii from value field' do
        assert_equal "", event.reload.value[:external_id]
        assert_equal "", event.reload.value[:text]
        event.reload.value[:requester].each do |_k, v|
          assert_equal "", v
        end
      end

      it "removes twitter username of the requester from ChannelBackEvent#twitter_text" do
        refute_includes event.reload.twitter_text, 'end_user'
      end
    end

    describe "Scrub Change and Create events" do
      let(:user) { users(:minimum_end_user) }
      let(:user_2) { users(:minimum_end_user2) }
      let(:ticket) { ticket_create(user) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:agent) { users(:minimum_agent) }

      describe "scrubbing process finished successfully" do
        it "replaces the value if the value_reference is 'requester_id' and the requester is a end user" do
          ticket.status_id = StatusType.CLOSED
          ticket.will_be_saved_by(agent)
          ticket.save!(validate: false)
          ticket.reload
          ticket_scrubber.scrub_ticket
          event = ticket.reload.events.where(type: 'Create', value_reference: 'requester_id').first
          assert_equal anonymous_user.id.to_s, event.value
        end

        it "replaces the value if the value_reference is 'requester_id' and the new requester is a end user" do
          ticket.update(requester_id: user_2.id)
          ticket.status_id = StatusType.CLOSED
          ticket.will_be_saved_by(agent)
          ticket.save!(validate: false)
          ticket.reload
          ticket_scrubber.scrub_ticket
          event = ticket.reload.events.where(type: 'Change', value_reference: 'requester_id').first
          assert_equal anonymous_user.id.to_s, event.value
          assert_equal anonymous_user.id.to_s, event.value_previous
        end

        it "does not replace the value on any create/change event if the requester is an Agent" do
          ticket.update(requester_id: agent.id)
          ticket.status_id = StatusType.CLOSED
          ticket.will_be_saved_by(agent)
          ticket.save!(validate: false)
          ticket.reload
          ticket_scrubber.scrub_ticket
          event = ticket.reload.events.where(type: 'Change', value_reference: 'requester_id').last
          assert_equal agent.id.to_s, event.value
          assert_equal anonymous_user.id.to_s, event.value_previous
        end

        describe "Anonymize subject" do
          [ViaType.CHAT, ViaType.SMS, ViaType.PHONE_CALL_INBOUND].each do |via|
            let(:ticket) { ticket_create(user) }
            let(:event) { events(:create_create_for_miniumum_ticket_1) }

            before do
              event.update_column :ticket_id, ticket.id
              ticket.via_id = via
              ticket.status_id = StatusType.CLOSED
              ticket.will_be_saved_by(agent)
            end

            it "replaces subject in ticket created via #{ViaType.to_s(via)}" do
              ticket.save!(validate: false)
              ticket_scrubber.scrub_ticket
              assert_equal I18n.t("txt.ticket_anonymize.redacted_subject"), ticket.reload.subject
            end

            it "replaces the subject in Create events created via #{ViaType.to_s(via)}" do
              ticket.save!(validate: false)
              ticket_scrubber.scrub_ticket
              ticket.reload
              event = ticket.reload.events.where(type: 'Create', value_reference: 'subject').last
              assert_equal I18n.t("txt.ticket_anonymize.redacted_subject"), event.value
              refute event.value_previous
            end

            it "replaces the subject in Change events created via #{ViaType.to_s(via)}" do
              ticket.update_column :subject, 'Subject'
              ticket.subject = 'New Subject'
              ticket.save!(validate: false)
              ticket_scrubber.scrub_ticket
              ticket.reload
              event = ticket.reload.events.where(type: 'Change', value_reference: 'subject').last
              assert_equal I18n.t("txt.ticket_anonymize.redacted_subject"), event.value
              assert_equal I18n.t("txt.ticket_anonymize.redacted_subject"), event.value_previous
            end
          end
        end
      end

      describe "scrubbing process failed" do
        it "does not replace value/previous_value with the anonymous user id" do
          ticket.update(requester_id: user_2.id)
          ticket.status_id = StatusType.CLOSED
          ticket.will_be_saved_by(agent)
          ticket.save!(validate: false)
          ticket.reload
          ticket_scrubber.rescue_errors_in_tests = true
          Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once
          ticket_scrubber.scrub_ticket
          ticket.reload.events.where(type: ['Change', 'Create'], value_reference: 'requester_id').each do |event|
            assert_not_equal anonymous_user.id.to_s, event.value
            assert_not_equal anonymous_user.id.to_s, event.value_previous
          end
        end
      end
    end

    describe "Scrub the audit author after close" do
      let(:ticket) { tickets(:minimum_1) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:audit) { events(:create_audit_for_minimum_ticket_1) }
      let(:agent) { ticket.submitter }
      let(:requester) { ticket.requester }

      before do
        audit.author_id = ticket.requester.id
        audit.save!
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(agent)
        ticket.save!(validate: false)
      end

      describe "scrubbing process finished successfully" do
        it "changes the audit author to anonymous user after close a ticket" do
          ticket_scrubber.scrub_ticket
          assert_equal anonymous_user.id, audit.reload.author_id
        end

        describe "when the requester is an agent" do
          before do
            ticket.update_column :requester_id, agent.id
            ticket.reload
            audit.author_id = ticket.requester.id
            audit.save!
          end

          it "does not scrub agent from notifications" do
            ticket_scrubber.scrub_ticket
            assert_equal agent.id, audit.reload.author_id
          end
        end
      end

      describe "scrubbing process failed" do
        it "doesn't change the audit author after close a ticket" do
          ticket_scrubber.rescue_errors_in_tests = true
          Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once
          ticket_scrubber.scrub_ticket
          assert_equal requester.id, audit.reload.author_id
        end
      end
    end

    describe "scrub pii metadata" do
      let(:agent) { users(:minimum_agent) }
      let(:ticket) { ticket_create(agent) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }

      before do
        ticket.will_be_saved_by(agent)
      end

      describe "scrubbing process finished successfully" do
        it "deletes metadata from all audits and record cia event" do
          ticket.status_id = StatusType.CLOSED
          CIA.audit actor: agent do
            ticket.save!(validate: false)
            ticket_scrubber.scrub_ticket
          end

          expected = {"system" => {"ip_address" => ""}, "custom" => {}}
          ticket.audits.each do |a|
            assert_equal expected, JSON.parse(a.value_previous)
          end
          assert_equal 1, ticket.cia_events.where(action: "scrub_pii_ticket_close").count
        end

        it "deletes emails and attachments" do
          ticket.status_id = StatusType.CLOSED
          ticket.save!(validate: false)

          ticket.expects(:delete_attachments).once
          ticket.expects(:delete_raw_emails).once

          ticket_scrubber.scrub_ticket
        end

        it "doesn't scrub open tickets" do
          ticket.status_id = StatusType.OPEN
          Audit.any_instance.expects(:scrub_metadata!).never
          ticket.save!(validate: false)

          ticket_scrubber.scrub_ticket
        end

        it "deletes metadata from all merge type audits when a ticket is merged into another" do
          source = ticket_create(agent)
          source.will_be_saved_by(agent)
          source.save!
          target = ticket_create(agent)
          target.will_be_saved_by(agent)
          target.save!
          source_scrubber = Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: source)

          merger = Zendesk::Tickets::Merger.new(user: agent)
          merge_args = {
            target: target,
            sources: [source],
            target_comment: "#{source.id} has been merged in this ticket",
            source_comment: "Ticket closed and merged in #{target.id}"
          }
          CIA.audit actor: agent do
            merger.merge(merge_args)
            source_scrubber.scrub_ticket
          end
          source.reload
          target.reload

          expected = {"system" => {"ip_address" => ""}, "custom" => {}}
          source.audits.each do |a|
            assert_equal expected, JSON.parse(a.value_previous)
          end
          assert_equal 1, source.cia_events.where(action: "scrub_pii_ticket_close").count
        end
      end

      describe "scrubbing process failed" do
        it "doesn't delete the metadata" do
          ticket.status_id = StatusType.CLOSED
          CIA.audit actor: agent do
            ticket.save!(validate: false)
            ticket_scrubber.rescue_errors_in_tests = true
            Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once
            ticket_scrubber.scrub_ticket
          end

          expected = {"system" => {"ip_address" => "127.0.0.1"}, "custom" => {}}
          ticket.audits.each do |a|
            assert_equal expected, JSON.parse(a.value_previous)
          end
          assert_equal 0, ticket.cia_events.where(action: "scrub_pii_ticket_close").count
        end

        it "doesn't delete emails and attachments" do
          ticket.status_id = StatusType.CLOSED
          ticket.save!(validate: false)
          ticket.expects(:delete_attachments).never
          ticket.expects(:delete_raw_emails).never
          ticket_scrubber.rescue_errors_in_tests = true
          Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once

          ticket_scrubber.scrub_ticket
        end

        it "doesn't delete the metadata from all merge type audits when a ticket is merged into another" do
          source = ticket_create(agent)
          source.will_be_saved_by(agent)
          source.save!
          target = ticket_create(agent)
          target.will_be_saved_by(agent)
          target.save!
          source_scrubber = Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: source)

          merger = Zendesk::Tickets::Merger.new(user: agent)
          merge_args = {
            target: target,
            sources: [source],
            target_comment: "#{source.id} has been merged in this ticket",
            source_comment: "Ticket closed and merged in #{target.id}"
          }
          CIA.audit actor: agent do
            merger.merge(merge_args)
            source_scrubber.rescue_errors_in_tests = true
            Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once
            source_scrubber.scrub_ticket
          end
          source.reload
          target.reload

          expected = {"system" => {"ip_address" => "127.0.0.1"}, "custom" => {}}
          source.audits.each do |a|
            assert_equal expected, JSON.parse(a.value_previous)
          end
          assert_equal 0, source.cia_events.where(action: "scrub_pii_ticket_close").count
        end
      end
    end

    describe "send metrics to datadog" do
      let(:ticket) { tickets(:minimum_2) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:agent) { ticket.submitter }
      let(:original_requester) { ticket.requester }

      before do
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(agent)
        ticket.submitter_id = original_requester.id
        ticket.save!(validate: false)
        ticket.reload
      end

      it "sends metrics to datadog if the process is completed successfully" do
        Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.expects(:distribution).with do |metric_name, _time|
          metric_name == "time_to_complete_scrubbing"
        end.once

        Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.expects(:time).with do |metric_name, _time|
          metric_name == "time"
        end.once

        ticket_scrubber.scrub_ticket
      end
    end

    describe "anonymize ticket sent via ticket" do
      let(:ticket) { tickets(:minimum_2) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:agent) { ticket.submitter }

      before do
        ticket.via_id = ViaType.TWITTER
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(agent)
        ticket.save!(validate: false)
        event_decorations(:minimum).update_column(:ticket_id, ticket.id)
      end

      it "anonymize requester twitter account" do
        ticket_scrubber.scrub_ticket
        ticket.reload
        expected_value = {"id" => "", "name" => "", "screen_name" => "", "photo_url" => ""}
        ticket.event_decorations.each do |event|
          assert_equal "", event.data.external_id
          assert_equal expected_value, event.data.author.to_hash
        end
      end
    end

    describe "anonymize facebook comment attachments" do
      let(:ticket) { tickets(:minimum_1) }
      let(:event_decoration) { event_decorations(:minimum) }
      let(:comment) { events(:create_comment_for_minimum_ticket_1) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:agent) { ticket.submitter }

      before do
        data = event_decoration.data
        data.links = { a: "b" }
        event_decoration.data = data
        event_decoration.save!
        ticket.via_id = ViaType.FACEBOOK_MESSAGE
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(agent)
        ticket.save!(validate: false)
      end

      it "removes event_decoration attachments" do
        ticket_scrubber.scrub_ticket
        ticket.reload
        empty_hash = {}
        assert_equal empty_hash, comment.event_decoration.data.links.to_hash
      end
    end
  end

  describe_with_arturo_disabled :ticket_user_scrubbing_after_close do
    before do
      Arturo.enable_feature!(:ticket_metadata_deletion_at_close)
      account.settings.delete_ticket_metadata_pii = true
      account.settings.save!
    end

    describe "when scrubbing the requester and submitter" do
      let(:ticket) { tickets(:minimum_2) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:requester) { ticket.requester }
      let(:agent) { ticket.submitter }

      before do
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(agent)
        ticket.save!(validate: false)
        ticket.reload
      end

      describe "ticket with an end user as requester and agent as submitter" do
        describe "scrubbing process finished successfully" do
          before do
            ticket_scrubber.scrub_ticket
            ticket.reload
          end

          it "doesn't changes the requester" do
            assert_equal requester.id, ticket.requester.id
          end

          it "doesn't change the submitter" do
            assert_equal agent.id, ticket.submitter.id
          end
        end
      end

      describe "ticket with an end user as requester and a end user as submitter" do
        describe "scrubbing process finished successfully" do
          before do
            ticket.will_be_saved_by(agent)
            ticket.submitter_id = requester.id
            ticket.save!(validate: false)
            ticket_scrubber.scrub_ticket
            ticket.reload
          end

          it "doesn't change the requester and the submitter" do
            assert_equal requester.id, ticket.requester.id
            assert_equal requester.id, ticket.submitter.id
          end
        end
      end
    end

    describe "scrub author in events" do
      let(:agent) { users(:minimum_agent) }
      let(:ticket) { tickets(:minimum_5) }
      let(:requester) { ticket.requester }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }

      describe "scrubbing process finished succesfully" do
        it "doesn't modify any events after the scrubbing process" do
          ticket_scrubber.scrub_ticket
          ticket.reload
          assert_equal StatusType.CLOSED, ticket.status_id
          assert_not_equal 0, ticket.events.to_a.count { |event| event.author_id == requester.id }
          assert_equal 0, ticket.events.to_a.count { |event| event.author_id == anonymous_user.id }
        end
      end
    end

    describe "Scrub recipients in notifications" do
      let(:ticket) { tickets(:minimum_1) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:requester) { ticket.requester }
      let(:agent) { users(:minimum_agent) }
      let(:audit) { events(:create_audit_for_minimum_ticket_1) }
      let(:anonymous_user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: ticket.account) }
      let(:event_data) do
        {
          account:    ticket.account,
          ticket:     ticket,
          author:     requester,
          recipients: [users(:minimum_agent), users(:minimum_end_user), requester].map(&:id),
          audit:      audit,
          subject:    'The rain in Spain',
          body:       'Stays main in the plain',
        }
      end

      let!(:notifications_with_recipients) do
        types = %w[
          Notification
          NotificationWithCcs
          Cc
          FollowerNotification
          SmsNotification
          AnswerBotNotification
        ]

        types.map! do |type|
          notification = Notification.create!(event_data)
          notification.update_column :type, type

          notification
        end

        ticket.reload.events.where(id: types.map(&:id))
      end

      before do
        ticket.will_be_saved_by(agent)
        ticket.status_id = StatusType.CLOSED
        ticket.save!(validate: false)
      end

      it "does not switch the requester in the recipients" do
        ticket_scrubber.scrub_ticket
        notifications_with_recipients.each do |n|
          assert_includes n.reload.recipients, requester.id, "Found the requester id in the notification recipients"
          refute_includes n.reload.recipients, anonymous_user.id, "Anonymous user not found in the recipients array"
        end
      end
    end

    describe "Scrub voice comments" do
      let(:ticket) { tickets(:minimum_1) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:requester) { ticket.requester }
      let(:agent) { users(:minimum_agent) }
      let(:audit) { events(:create_audit_for_minimum_ticket_1) }
      let(:anonymous_user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: ticket.account) }

      let(:value_previous) do
        {
          recording_url:        "http://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/RE5d4706b2a7ce9c6085119352bc3f96c",
          recording_duration:   "7",
          transcription_text:   "foo: hi\nbar: bye\n",
          transcription_status: "completed",
          call_duration:        126,
          answered_by_id:       requester.id,
          answered_by_name:     requester.name,
          call_id:              7890,
          from:                 "+14155556666",
          to:                   "+14155557777",
          started_at:           "June 04, 2014 05:10:27 AM",
          outbound:             false
        }
      end

      let!(:voice_comments) do
        types = %w[
          VoiceApiComment
          VoiceComment
        ]

        types.map! do |type|
          comment = VoiceComment.create!(
            account:    ticket.account,
            ticket:     ticket,
            audit:      audit
          )
          comment.update_column :value_previous, value_previous
          comment.update_column :type, type
          comment
        end
        ticket.reload.events.where(id: types.map(&:id))
      end

      before do
        ticket.will_be_saved_by(agent)
        ticket.status_id = StatusType.CLOSED
        ticket.save!(validate: false)
      end

      describe "scrubbing process finished successfully" do
        it "does not modify voice_comments" do
          ticket_scrubber.scrub_ticket
          voice_comments.each do |comment|
            assert_equal comment.reload.value_previous[:answered_by_id], requester.id
            assert_equal comment.reload.value_previous[:answered_by_name], requester.name
          end
        end
      end
    end

    describe "Scrub the audit author after close" do
      let(:ticket) { tickets(:minimum_1) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:audit) { events(:create_audit_for_minimum_ticket_1) }
      let(:agent) { ticket.submitter }
      let(:requester) { ticket.requester }

      before do
        audit.author_id = ticket.requester.id
        audit.save!
        ticket.status_id = StatusType.CLOSED
        ticket.will_be_saved_by(agent)
        ticket.save!(validate: false)
      end

      describe "scrubbing process finished successfully" do
        it "does not change the audit author to anonymous user after close a ticket" do
          ticket_scrubber.scrub_ticket
          assert_not_equal anonymous_user.id, audit.reload.author_id
        end
      end
    end

    describe "Scrub Change and Create events" do
      let(:user) { users(:minimum_end_user) }
      let(:user_2) { users(:minimum_end_user2) }
      let(:ticket) { ticket_create(user) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }
      let(:agent) { users(:minimum_agent) }

      describe "scrubbing process finished successfully" do
        it "does not replace value/previous_value with the anonymous user id" do
          ticket.update(requester_id: user_2.id)
          ticket.status_id = StatusType.CLOSED
          ticket.will_be_saved_by(agent)
          ticket.save!(validate: false)
          ticket.reload
          ticket_scrubber.rescue_errors_in_tests = true
          Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:switch_requester).raises(StandardError, 'fail').once
          ticket_scrubber.scrub_ticket
          ticket.reload.events.where(type: ['Change', 'Create'], value_reference: 'requester_id').each do |event|
            assert_not_equal anonymous_user.id.to_s, event.value
            assert_not_equal anonymous_user.id.to_s, event.value_previous
          end
        end
      end
    end

    describe "scrub pii metadata on close" do
      let(:agent) { users(:minimum_agent) }
      let(:ticket) { ticket_create(agent) }
      let(:ticket_scrubber) { Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket) }

      before do
        ticket.will_be_saved_by(agent)
      end

      describe "scrubbing process finished successfully" do
        it "deletes metadata from all audits and record cia event 2" do
          ticket.status_id = StatusType.CLOSED
          CIA.audit actor: agent do
            ticket.save!(validate: false)
            ticket_scrubber.scrub_ticket
          end

          expected = {"system" => {"ip_address" => ""}, "custom" => {}}
          ticket.audits.each do |a|
            assert_equal expected, JSON.parse(a.value_previous)
          end
          assert_equal 1, ticket.cia_events.where(action: "scrub_pii_ticket_close").count
        end

        it "deletes emails and attachments" do
          ticket.status_id = StatusType.CLOSED
          ticket.save!(validate: false)

          ticket.expects(:delete_attachments).once
          ticket.expects(:delete_raw_emails).once

          ticket_scrubber.scrub_ticket
        end

        it "doesn't scrub open tickets" do
          ticket.status_id = StatusType.OPEN
          Audit.any_instance.expects(:scrub_metadata!).never
          ticket.save!(validate: false)

          ticket_scrubber.scrub_ticket
        end

        it "deletes metadata from all merge type audits when a ticket is merged into another" do
          source = ticket_create(agent)
          source.will_be_saved_by(agent)
          source.save!
          target = ticket_create(agent)
          target.will_be_saved_by(agent)
          target.save!
          source_scrubber = Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: source)

          merger = Zendesk::Tickets::Merger.new(user: agent)
          merge_args = {
            target: target,
            sources: [source],
            target_comment: "#{source.id} has been merged in this ticket",
            source_comment: "Ticket closed and merged in #{target.id}"
          }
          CIA.audit actor: agent do
            merger.merge(merge_args)
            source_scrubber.scrub_ticket
          end
          source.reload
          target.reload

          expected = {"system" => {"ip_address" => ""}, "custom" => {}}
          source.audits.each do |a|
            assert_equal expected, JSON.parse(a.value_previous)
          end
          assert_equal 1, source.cia_events.where(action: "scrub_pii_ticket_close").count
        end
      end
    end
  end
end
