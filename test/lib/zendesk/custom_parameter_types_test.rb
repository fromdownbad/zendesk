require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::CustomParameterTypes do
  describe ActiveModel::ForbiddenAttributesProtection do
    let(:error_type) { ActiveModel::ForbiddenAttributesError }

    it "fails with new" do
      assert_raises(error_type) do
        Sharing::Agreement.new(ActionController::Parameters.new(access_key: 'foo'))
      end
    end

    it "fails with attributes=" do
      assert_raises(error_type) do
        Sharing::Agreement.new.attributes = ActionController::Parameters.new(access_key: 'foo')
      end
    end

    it "works with permitted" do
      Sharing::Agreement.new(ActionController::Parameters.new(access_key: 'foo').permit(:access_key))
    end
  end

  describe Zendesk::CustomParameterTypes::ISO8601DateTimeConstraint do
    let(:constraint) { Zendesk::CustomParameterTypes::ISO8601DateTimeConstraint.new }

    describe "when the value is in a proper format" do
      describe "when the value is in seconds since epoch format" do
        it "fails" do
          constraint.value('1406052864').class.must_equal StrongerParameters::InvalidValue
        end
      end

      describe "when the value is in ISO 8601 format" do
        it "return the correct date" do
          assert_equal Time.parse("2012-03-05T11:32:44Z"), constraint.value("2012-03-05T11:32:44Z")
        end

        it "return using utc format" do
          assert constraint.value("2012-03-05T11:32:44Z").utc?
        end

        describe "with a time zone" do
          it "return the correct date" do
            assert_equal Time.parse("2012-03-05T11:32:44-07:00"), constraint.value("2012-03-05T11:32:44-07:00")
          end
        end
      end

      describe "when the value is an empty string" do
        it "fails" do
          constraint.value('').class.must_equal StrongerParameters::InvalidValue
        end
      end
    end

    describe "when the value is not in a proper format" do
      it "fails with a nice message" do
        e = constraint.value('that')
        assert_equal 'It must be an acceptable ISO 8601 date format', e.message
      end

      describe "that Time#parse was accepting" do
        it "fails" do
          ['07/08/1974', '12345', '2014-W15-02'].each do |date|
            constraint.value(date).class.must_equal StrongerParameters::InvalidValue
          end
        end
      end
    end

    describe "when the value is a number" do
      it "fails" do
        e = constraint.value(123456789)
        assert_equal 'It must be an acceptable ISO 8601 date format', e.message
      end
    end

    describe "when the value is nil" do
      it "fails" do
        e = constraint.value(nil)
        assert_equal 'It must be an acceptable ISO 8601 date format', e.message
      end
    end

    describe "when the value is out of range" do
      it "fails" do
        e = constraint.value('8867394')
        assert_equal 'It is not in a valid range', e.message
      end
    end
  end

  describe ActionController::Parameters do
    describe "#ids" do
      let(:ids) { ActionController::Parameters.ids }

      it "return the right constraint" do
        assert_kind_of(StrongerParameters::RegexpConstraint, ids)
      end

      it "match on proper id lists" do
        assert_equal '1,2,3', ids.value('1,2,3')
        assert_equal '1,2,3,', ids.value('1,2,3,')
        assert_equal '1, 2, 3', ids.value('1, 2, 3')
        assert_equal 'suspended, 1', ids.value('suspended, 1')
      end

      it "fails on improper id lists" do
        ['{1,2,3}', 'SUSPENDED', ',1,2'].each do |bad_list|
          ids.value(bad_list).class.must_equal StrongerParameters::InvalidValue
        end
      end
    end

    it "does nothing when nil is passed" do
      Rails.logger.expects(:error).never
      ActionController::Parameters.new(value: nil).permit(value: ActionController::Parameters.integer32)
    end
  end
end
