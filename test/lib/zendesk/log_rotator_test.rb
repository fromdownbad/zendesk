require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Zendesk::LogRotator do
  before do
    @rotator = Zendesk::LogRotator
  end

  after do
    if @rotator.send(:exist?)
      proc = trap('USR1', "IGNORE")
      raise("LogRotator was not cleanly removed: handler source: #{proc.source_location}")
    end
    @rotator.send(:stop) if @rotator.send(:thread).try(:alive?) # something went wrong
  end

  describe "listen" do
    it "reopens logs when a USR1 signal is received" do
      Unicorn::Util.expects(:reopen_logs)
      @rotator.send(:listen)
      assert @rotator.send(:exist?)
      @rotator.send(:pause)
      Process.kill('USR1', Process.pid)
      block until @rotator.send(:signal_queue).any?
      @rotator.send(:stop)
    end

    it "does not reopen logs when no USR1 signal is received" do
      Unicorn::Util.expects(:reopen_logs).never
      @rotator.send(:listen)
      @rotator.send(:stop)
    end
  end

  describe ".exist?" do
    it "is true when something is already subscribed to the signal" do
      trap_signal('USR1') do
        assert(@rotator.send(:exist?))
      end
    end

    it "is false by default" do
      assert_equal false, @rotator.send(:exist?)
    end

    it "does not modify the original handler" do
      assert_equal false, @rotator.send(:exist?)
      assert_equal false, @rotator.send(:exist?)
    end
  end

  describe ".try_listen" do
    before do
      @rotator.instance_variable_set(:@thread, stub(kill: nil, alive?: false))
    end

    it "does not listen when already subscribed to the signal" do
      @rotator.expects(:listen).never
      trap_signal('USR1') do
        @rotator.try_listen
      end
      @rotator.send(:stop)
    end

    it "listens when not already subscribed to the signal" do
      @rotator.expects(:listen)
      @rotator.try_listen
      @rotator.send(:stop)
    end
  end

  def trap_signal(signal, &block)
    original = trap(signal) { puts "Unexpected call. source:#{block.source_location}" }
    yield
  ensure
    trap(signal, original)
  end

  def block
    sleep(0.01) # rubocop:disable Lint/Sleep
  end
end
