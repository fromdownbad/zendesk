require_relative "../../../support/test_helper"
require_relative "../../../support/rack_test_helper"

SingleCov.covered! uncovered: 2

describe 'Zendesk::PermanentCookie::Store' do
  describe "Permanent Cookie" do
    before do
      @cookie_key = '_cookie_test'
      @cookies = ActionDispatch::Cookies::CookieJar.new(nil)
      @store = Zendesk::PermanentCookie::Store.new(@cookie_key, @cookies)
      @key = Zendesk::PermanentCookie::Store::KEYS.first
    end

    describe "Given an empty store" do
      it "reads a value successfully" do
        assert_nil @store[@key]
      end

      it "Writes a value successfully" do
        @store[@key] = 'bar'
        assert @store[@key].present?
      end
    end

    describe "Accessing the cookie" do
      before { @store[@key] = 'bar' }

      it "is able to retrieve that data later" do
        assert_equal('bar', @store[@key])
      end

      it "access same data regardless of string or symbol" do
        assert_equal(@store[@key.to_sym], @store[@key])
      end

      it "only allow whitelisted keys" do
        @store.unstub(:valid_key?)
        assert_raise(ArgumentError) do
          @store[:badkey] = 'bar'
        end
      end

      it "strips invalid keys" do
        @store.data['foo'] = 'bar'
        @store[@key] = 'baz'
        assert_equal [@key], @store.data.keys
      end

      it "overwrites previously set data" do
        @store[@key] = 'baz'
        assert_equal('baz', @store[@key])
      end

      it "stores data in the named key" do
        assert @cookies[@cookie_key]
      end

      describe "with signed data" do
        before do
          @signer = ActiveSupport::MessageVerifier.new(Zendesk::PermanentCookie::Store::COOKIE_SECRETS[0])
        end

        it "datas should be signed" do
          json = @cookies[@cookie_key].to_json
          assert_not_equal @signer.generate(json), @cookies[@cookie_key]
        end

        it "unescapes before unsigning" do
          # signed token: BAhJIhJ7ImZvbyI6ImJhciJ9BjoGRVQ=--3ce8dd873b2f678c80c30a75d100b94f878e211a
          # NOTE: inclusion of '=' in token is key for this test
          @cookies[@cookie_key] = Zendesk::PermanentCookie::Store::DEFAULT_OPTIONS.merge(
            value: CGI.escape(@signer.generate({'foo' => 'bar'}.to_json))
          )
          store = Zendesk::PermanentCookie::Store.new(@cookie_key, @cookies)
          assert_equal("bar", store["foo"])
        end

        it "handles bad data" do
          @cookies[@cookie_key] = Zendesk::PermanentCookie::Store::DEFAULT_OPTIONS.merge(
            value: CGI.escape("bad \xE0 stuff")
          )
          store = Zendesk::PermanentCookie::Store.new(@cookie_key, @cookies)
          assert_nil store["foo"]
        end

        it "is json parseable" do
          JSON.parse @signer.verify(@cookies[@cookie_key])
        end
      end

      describe "with multiple keys" do
        before do
          @signed_messages = Zendesk::PermanentCookie::Store::COOKIE_SECRETS.map do |key|
            mv = ActiveSupport::MessageVerifier.new(key)
            mv.generate({'foo' => 'bar'}.to_json)
          end
        end

        it "can verify all messages" do
          @signed_messages.each do |message|
            @cookies[@cookie_key] = Zendesk::PermanentCookie::Store::DEFAULT_OPTIONS.merge(
              value: CGI.escape(message)
            )
            store = Zendesk::PermanentCookie::Store.new(@cookie_key, @cookies)
            assert_equal("bar", store["foo"])
          end
        end
      end
    end
  end
end
