require_relative "../../../support/test_helper"
require_relative "../../../support/rack_test_helper"

SingleCov.covered! uncovered: 1

describe 'Zendesk::PermanentCookie' do
  fixtures :accounts, :routes

  class MockRequest < FakeRequest
    include Zendesk::PermanentCookie::RequestMixin
  end

  describe "Request Mixin" do
    before do
      @request = MockRequest.new
      @request.env['zendesk.account'] = accounts(:minimum)
      Zendesk::PermanentCookie::Store.any_instance.stubs(:valid_key?).returns(true)
    end

    it 'is included in the environment' do
      refute @request.env[Zendesk::PermanentCookie::ENV_COOKIE_KEY]
      @request.permanent_cookies
      assert @request.env[Zendesk::PermanentCookie::ENV_COOKIE_KEY]
    end

    it 'is accessible' do
      @request.permanent_cookies['foo'] = 'bar'
      env_key = Zendesk::PermanentCookie::ENV_COOKIE_KEY
      store_key = Zendesk::PermanentCookie::COOKIE_KEY
      assert @request.env[env_key].cookies[store_key]
    end

    it 'respects is_ssl_enabled?' do
      Account.any_instance.stubs(is_ssl_enabled?: true)
      assert @request.permanent_cookies.options[:secure]
    end

    it 'defaults secure to false' do
      Account.any_instance.stubs(ssl_should_be_used?: false)
      refute @request.permanent_cookies.options[:secure]
    end
  end
end
