require 'set'
require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 7

describe Zendesk::CustomField::CustomFieldManager do
  fixtures :accounts
  include CustomFieldsTestHelper

  before do
    @account = accounts(:minimum)
    @custom_field = create_user_custom_field!("field_dropdown", "dropdown field", "Dropdown")
    @initial_choices = [
      {name: "testing", value: "test",   position: 0},
      {name: "second",  value: "second", position: 1}
    ]
    @initial_choices.each do |choice|
      @custom_field.dropdown_choices.create(choice)
    end
    @manager = Zendesk::CustomField::CustomFieldManager.new(@account, @account.owner, @custom_field)
  end

  describe ".type" do
    it "returns type specified by argument" do
      assert_equal CustomField::Checkbox, Zendesk::CustomField::CustomFieldManager.type("CustomField::Checkbox")
      assert_equal CustomField::Date, Zendesk::CustomField::CustomFieldManager.type("CustomField::Date")
      assert_equal CustomField::Decimal, Zendesk::CustomField::CustomFieldManager.type("CustomField::Decimal")
      assert_equal CustomField::Integer, Zendesk::CustomField::CustomFieldManager.type("CustomField::Integer")
      assert_equal CustomField::Regexp, Zendesk::CustomField::CustomFieldManager.type("CustomField::Regexp")
      assert_equal CustomField::Text, Zendesk::CustomField::CustomFieldManager.type("CustomField::Text")
      assert_equal CustomField::Textarea, Zendesk::CustomField::CustomFieldManager.type("CustomField::Textarea")
      assert_equal CustomField::Dropdown, Zendesk::CustomField::CustomFieldManager.type("CustomField::Dropdown")
    end

    it "raises UnknownCustomFieldTypeError for unknown type" do
      assert_raise(Zendesk::CustomField::UnknownCustomFieldTypeError) do
        Zendesk::CustomField::CustomFieldManager.type("unknown field name")
      end
    end
  end

  describe "#reorder" do
    before do
      @custom_fields = @account.custom_fields.for_user
      @custom_fields.each do |custom_field|
        assert custom_field.valid?
      end
    end

    it "sets new position values based on parameters" do
      params = @custom_fields.map(&:id).map(&:to_s).reverse
      @manager.reorder(params, "user")

      @account.reload.custom_fields.for_user.reverse.each_with_index do |c, i|
        assert_equal i, c.position
      end
    end

    it "maintains the order of fields not included" do
      not_included = @custom_fields[@custom_fields.size - 2..@custom_fields.size]
      not_included_pos = not_included.map(&:position)
      included = @custom_fields[0..@custom_fields.size - 3].map(&:id).map(&:to_s)
      @manager.reorder(included.reverse, "user")

      @account.reload.custom_fields.for_user.where(id: not_included).to_a.each_with_index do |f, i|
        assert_equal not_included_pos[i] + included.size, f.position
      end
    end

    describe "when there are invalid fields" do
      before do
        @custom_field.dropdown_choices.first.update_attribute(:name, nil)
      end

      it "works" do
        params = @custom_fields.map(&:id).map(&:to_s).reverse
        @manager.reorder(params, "user")

        @account.reload.custom_fields.for_user.reverse.each_with_index do |c, i|
          assert_equal i, c.position
        end
      end
    end
  end

  describe "#update" do
    describe "trying to update without an id" do
      it "adds errors on the model" do
        params = {custom_field_options: [{ name: "Test", value: "test" }]}
        @manager.update(@custom_field, params)
        expected_response = {
          error: "RecordInvalid",
          description: "Record validation errors",
          details: {
            custom_field_options: [
              { error: "BlankValue",
                description: "Custom field options are invalid; you must pass an id parameter when updating custom_field_options. To create a new custom_field_option please pass {id: null}"}
            ]
          }
        }

        refute @custom_field.valid?
        assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(@custom_field)
      end
    end

    describe "updating with ids" do
      before do
        @input = [
          {id: nil, name: "New Option", value: "new_options"},
          {id: @custom_field.dropdown_choices.first.id, name: "updated option", value: "updated_option"},
          {id: @custom_field.dropdown_choices.last.id, name: "updated option 2", value: "updated_option_2"}
        ]
      end

      it "updates name and value on the old field, and add new fields where id is null" do
        params = {custom_field_options: @input}

        @manager.update(@custom_field, params)

        res = @custom_field.dropdown_choices.map do |c|
          {id: c.id, name: c.name, value: c.value}
        end

        assert_equal @input, res
      end

      it "updates name and value on the old field, and add new fields where id is empty string" do
        @input[0][:id] = ""
        params = {custom_field_options: @input}

        @manager.update(@custom_field, params)

        res = @custom_field.dropdown_choices.map do |c|
          {id: c.id, name: c.name, value: c.value}
        end

        @input[0][:id] = nil

        assert_equal @input, res
      end

      it "restores previously inactive fields when value already exists" do
        deleted = @custom_field.dropdown_choices.first
        deleted.soft_delete!

        @input[0][:value] = deleted.value
        @input.delete_at(1)

        @manager.update(@custom_field, custom_field_options: @input)

        res = @custom_field.dropdown_choices.map do |c|
          {id: c.id, name: c.name, value: c.value}
        end

        @input[0][:id] = deleted.id

        assert_equal @input, res
      end

      describe "an update with soft deleted fields" do
        before do
          @deleted_position = 10
          @choice = @custom_field.dropdown_choices.first
          @choice.position = @deleted_position
          @choice.soft_delete!
          @choice.save!
        end

        it "does not re-position soft deleted fields" do
          @input[0][:name] = "Newer Option"
          @manager.update(@custom_field, custom_field_options: @input).save!
          @choice.reload

          assert_equal @choice.position, @deleted_position
        end
      end

      it "re-orders properly" do
        @manager.update(@custom_field, custom_field_options: @input.reverse)

        res = @custom_field.dropdown_choices.map do |c|
          {id: c.id, name: c.name, value: c.value}
        end

        assert_equal @input.reverse, res
      end

      it "deletes them that do not exist" do
        input = @input.clone
        input.delete_at(2)
        input.reverse!

        @manager.update(@custom_field, custom_field_options: input).save!

        assert_equal "updated option", @custom_field.dropdown_choices.first.name
        assert_equal "New Option", @custom_field.dropdown_choices.last.name
        assert_equal 2, @custom_field.dropdown_choices.size
      end
    end

    describe "with an existing custom field option and an id" do
      before do
        @old_updated_at = @custom_field.updated_at
        @option = @custom_field.dropdown_choices.first
        @manager.update(@custom_field,
          custom_field_options: [{ id: @option.id.to_s, name: "changed_name", value: "changed_value"}]).save!
        @option.reload
      end

      it "changes attributes of given option" do
        assert_equal "changed_name", @option.name
        assert_equal "changed_value", @option.value
      end

      it "updates updated_at" do
        assert_not_equal @old_updated_at, @custom_field.updated_at
      end
    end

    describe "with unchanged options" do
      before do
        @initial_updated_at = @custom_field.updated_at
        @manager.update(@custom_field,
          custom_field_options: @custom_field.dropdown_choices.map { |option| { id: option.id.to_s, name: option.name, value: option.value } }).save!
      end

      it "does not change attributes of options" do
        @initial_choices.each_with_index do |initial_choice, index|
          choice = @custom_field.dropdown_choices[index]
          assert_equal initial_choice[:name], choice.name
          assert_equal initial_choice[:value], choice.value
        end
      end

      it "does not update updated_at" do
        assert_equal @initial_updated_at, @custom_field.updated_at
      end
    end

    describe "For a system field" do
      before do
        @system_field = create_user_custom_field!("system::field_dropdown", "txt.dropdown_field", "Dropdown", is_system: true)
        @system_field.dropdown_choices.create(name: "txt.choice1", value: "choice1", position: 1)
        @system_field.dropdown_choices.create(name: "txt.choice2", value: "choice2", position: 2)
        @option = @system_field.dropdown_choices.first
        @manager = Zendesk::CustomField::CustomFieldManager.new(@account, @account.owner, @system_field)
      end

      describe "as a system user" do
        before do
          User.any_instance.stubs(:is_system_user?).returns(true)
          @manager.update(@system_field,
            custom_field_options: [{ id: @option.id.to_s, name: "txt.changed_name_2", value: "changed_value_2"}]).save!
          @option.reload
        end

        it "changes attributes of given option" do
          assert_equal "txt.changed_name_2", @option.name
          assert_equal "changed_value_2", @option.value
        end
      end

      describe "as a non system user" do
        before do
          User.any_instance.stubs(:is_system_user?).returns(false)
          @manager.update(@system_field,
            custom_field_options: [{ id: @option.id.to_s, name: "txt.changed_name_3", value: "changed_value_3"}]).save!
          @option.reload
        end

        it "does not change attributes of given option" do
          assert_not_equal "txt.changed_name_3", @option.name
          assert_not_equal "changed_value_3", @option.value
        end
      end
    end
  end
end
