require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::CustomField::HasManyExtension do
  include CustomFieldsTestHelper
  fixtures :accounts, :subscriptions, :users, :organizations, :credit_cards, :tickets, :organizations

  before do
    # don't use these fixtures
    CustomField::Field.without_arsi.delete_all
    CustomField::Value.without_arsi.delete_all
    @account = accounts(:minimum) # used by helpers below
    @custom_field = create_user_custom_field!("field_1", "Field 1", "Text")
    @custom_field_org = create_organization_custom_field!("field_1", "Field 1", "Text")
    @custom_field_org2 = create_organization_custom_field!("field_22", "Field 1", "Text")
    @user = users(:minimum_end_user)
    @user.custom_field_values.build(field: @custom_field, value: "Foobar")
  end

  it "responds to #key" do
    assert_equal("Foobar", @user.custom_field_values.value_for_key("field_1").value)
  end

  describe "#to_liquid" do
    describe "no custom field with key 'system' exists in the account" do
      before do
        @agent = users(:minimum_admin)
        @ticket = tickets(:minimum_2)
        @ticket.requester = @user
        @custom_user_system_field = create_user_custom_field!("system::zen_sys_user_field", "txt.zentest", "Text", is_system: true)
        @custom_org_system_field = create_organization_custom_field!("system::zen_sys_org_field", "txt.zentest", "Text", is_system: true)
        @user.custom_field_values.build(field: @custom_user_system_field, value: "zenwashere")
        @user.organization.custom_field_values.build(field: @custom_org_system_field, value: "zenwashere")
      end

      it "converts normal user fields" do
        assert_equal "<p></p><div>foo</div><p>Foobar abc Foobar</p>", Zendesk::Liquid::TicketContext.new("<div>foo</div>\n\n{{ticket.requester.custom_fields.field_1}} abc {{ticket.requester.custom_fields.field_1}}", @ticket, @agent, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html]).render
      end

      it "converts system user fields" do
        assert_equal "<p></p><div>foo</div><p>zenwashere abc zenwashere</p>", Zendesk::Liquid::TicketContext.new("<div>foo</div>\n\n{{ticket.requester.custom_fields.system::zen_sys_user_field}} abc {{ticket.requester.custom_fields.system::zen_sys_user_field}}", @ticket, @agent, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html]).render
      end

      it "converts system org fields" do
        assert_equal "<p></p><div>foo</div><p>zenwashere abc zenwashere</p>", Zendesk::Liquid::TicketContext.new("<div>foo</div>\n\n{{ticket.requester.organization.custom_fields.system::zen_sys_org_field}} abc {{ticket.requester.organization.custom_fields.system::zen_sys_org_field}}", @ticket, @agent, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html]).render
      end
    end

    describe "custom field with key 'system' exists in the account" do
      before do
        @agent = users(:minimum_admin)
        @ticket = tickets(:minimum_2)
        @ticket.requester = @user
        @custom_user_field_w_key_system = create_user_custom_field!("system", "zentest system", "Text")
        @custom_user_system_field = create_user_custom_field!("system::zen_sys_user_field", "txt.zentest", "Text", is_system: true)
        @custom_org_system_field = create_organization_custom_field!("system::zen_sys_org_field", "txt.zentest", "Text", is_system: true)
        @user.custom_field_values.build(field: @custom_user_field_w_key_system, value: 'Foobar')
        @user.custom_field_values.build(field: @custom_user_system_field, value: "zenwashere")
        @user.organization.custom_field_values.build(field: @custom_org_system_field, value: "zenwashere")
      end

      it "converts custom user field with key 'system'" do
        assert_equal "<p></p><div>foo</div><p>Foobar abc Foobar</p>", Zendesk::Liquid::TicketContext.new("<div>foo</div>\n\n{{ticket.requester.custom_fields.system}} abc {{ticket.requester.custom_fields.system}}", @ticket, @agent, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html]).render
      end

      it "does not convert system user fields" do
        assert_equal "<p></p><div>foo</div><p> abc </p>", Zendesk::Liquid::TicketContext.new("<div>foo</div>\n\n{{ticket.requester.custom_fields.system::zen_sys_user_field}} abc {{ticket.requester.custom_fields.system::zen_sys_user_field}}", @ticket, @agent, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html]).render
      end
    end
  end

  describe "#as_json" do
    it "responds to as_json with a hash, yeah?" do
      assert_equal({"field_1" => "Foobar"}, @user.custom_field_values.as_json(account: @user.account))
    end

    it "includes custom fields even with nil values" do
      create_user_custom_field!("field_2", "Nothing Here", "Text")
      assert_equal({"field_1" => "Foobar", "field_2" => nil}, @user.custom_field_values.as_json(account: @user.account))
    end

    it "uses custom fields if they are passed in" do
      fields = stub
      fields.expects(:each_with_object).returns([])
      @account.expects(:custom_fields_for_owner).never

      @user.custom_field_values.as_json(custom_fields: fields, account: @user.account)
    end

    it "uses account fields when not given any to use" do
      @user.account.expects(:custom_fields_for_owner).returns([])
      @user.custom_field_values.as_json(account: @user.account)
    end
  end

  describe "#set_field_value" do
    describe "when account has user tags" do
      before do
        @account.settings.has_user_tags = true
        @account.save
      end

      describe "when the account does not have the 'user_and_organization_fields' feature" do
        before { Account.any_instance.stubs(:has_user_and_organization_fields?).returns(false) }

        describe "with a dropdown field" do
          before do
            @cf = create_user_custom_field!("field_2", "Here", "Dropdown")
            @cf.dropdown_choices.build(name: "Something", value: "100")
            @cf.dropdown_choices.build(name: "Something else", value: "200")
            @cf.save!
          end

          it "does not add the tag from the selected dropdown value to the user" do
            refute @user.tags.include?("100")
            @user.custom_field_values.set_field_value(@cf, "100")
            @user.save!
            refute @user.tags.include?("100")
          end
        end

        describe "with a checkbox field" do
          before do
            @cf = create_user_custom_field!("field_2", "Nothing Here", "Checkbox")
            @cf.tag = "nothing_here"
            @cf.save!
          end

          describe "when setting to the value" do
            (CustomField::Checkbox::TRUTHY_VALUES + CustomField::Checkbox::FALSY_VALUES).each do |field_value|
              it "#{field_value} does not add the tag to the user" do
                refute @user.tags.include?("nothing_here")
                @user.custom_field_values.set_field_value(@cf, field_value)
                @user.save!
                refute @user.tags.include?("nothing_here")
              end
            end
          end
        end
      end

      describe "when the account has the 'user_and_organization_fields' feature" do
        before { Account.any_instance.stubs(:has_user_and_organization_fields?).returns(true) }

        describe "with a dropdown field" do
          before do
            @cf = create_user_custom_field!("field_2", "Here", "Dropdown")
            @cf.dropdown_choices.build(name: "Something", value: "100")
            @cf.dropdown_choices.build(name: "Something else", value: "200")
            @cf.save!
          end

          describe "when adding the dropdown value" do
            it "adds the tag from the selected dropdown value to the user" do
              refute @user.tags.include?("100")
              @user.custom_field_values.set_field_value(@cf, "100")
              assert_equal(1, @user.custom_field_values.field_changes.size)
              @user.save!
              assert @user.tags.include?("100")
            end
          end

          describe "when updating the dropdown value to a different value" do
            before do
              @user.custom_field_values.set_field_value(@cf, "200")
              @user.save!
            end

            it "removes the tag from the previously selected dropdown value and adds the tag from the newly selected dropdown value" do
              refute @user.tags.include?("100")
              assert @user.tags.include?("200")
              @user.custom_field_values.set_field_value(@cf, "100")
              @user.save!
              assert @user.tags.include?("100")
              refute @user.tags.include?("200")
              assert_equal(2, @user.custom_field_values.field_changes.size)
            end
          end

          describe "when updating the dropdown value to the same value but the tag was not present" do
            before do
              @user.custom_field_values.set_field_value(@cf, "200")
              @user.save!
              @user.tags = ""
              @user.save!
            end

            it "adds the tag from the dropdown value to the user" do
              refute @user.tags.include?("200")
              @user.custom_field_values.set_field_value(@cf, "200")
              @user.save!
              assert @user.tags.include?("200")
              assert_equal(1, @user.custom_field_values.field_changes.size)
            end
          end

          describe "when removing the dropdown value" do
            before do
              @user.custom_field_values.set_field_value(@cf, "200")
              @user.save!
              assert_equal(1, @user.custom_field_values.field_changes.size)
            end

            it "removes the tag from the previously selected dropdown value" do
              assert @user.tags.include?("200")
              @user.custom_field_values.set_field_value(@cf, nil)
              @user.save!
              refute @user.tags.include?("200")
              assert_equal(2, @user.custom_field_values.field_changes.size)
            end
          end
        end

        describe "with a checkbox field" do
          before do
            @cf = create_user_custom_field!("field_2", "Nothing Here", "Checkbox")
            @cf.tag = "nothing_here"
            @cf.save!
          end

          describe "when setting to the falsy value" do
            describe "when value was previously false and tag was not present" do
              CustomField::Checkbox::FALSY_VALUES.each do |field_value|
                it "#{field_value} does not add the tag to the user" do
                  refute @user.tags.include?("nothing_here")
                  @user.custom_field_values.set_field_value(@cf, field_value)
                  @user.save!
                  refute @user.tags.include?("nothing_here")
                  assert_equal(1, @user.custom_field_values.field_changes.size)
                end
              end
            end

            describe "when value was previously false but tag was present" do
              before do
                @user.tags = "nothing_here"
                @user.save!
              end

              CustomField::Checkbox::FALSY_VALUES.each do |field_value|
                it "#{field_value} does not remove the tag from the user" do
                  assert @user.tags.include?("nothing_here")
                  @user.custom_field_values.set_field_value(@cf, field_value)
                  @user.save!
                  assert @user.tags.include?("nothing_here")
                  assert_equal(1, @user.custom_field_values.field_changes.size)
                end
              end
            end

            describe "when value was previously true and tag was present" do
              before do
                @user.custom_field_values.set_field_value(@cf, true)
                @user.save!
              end

              CustomField::Checkbox::FALSY_VALUES.each do |field_value|
                it "#{field_value} removes the tag from the user" do
                  assert @user.tags.include?("nothing_here")
                  @user.custom_field_values.set_field_value(@cf, field_value)
                  @user.save!
                  refute @user.tags.include?("nothing_here")
                  assert_equal(2, @user.custom_field_values.field_changes.size)
                end
              end
            end
          end

          describe "when setting to the truthy value" do
            describe "when value was previously false and tag was not present" do
              CustomField::Checkbox::TRUTHY_VALUES.each do |field_value|
                it "#{field_value} adds the tag to the user" do
                  refute @user.tags.include?("nothing_here")
                  @user.custom_field_values.set_field_value(@cf, field_value)
                  @user.save!
                  assert @user.tags.include?("nothing_here")
                  assert_equal(1, @user.custom_field_values.field_changes.size)
                end
              end
            end

            describe "when value was previously true but tag was not present" do
              before do
                @user.custom_field_values.set_field_value(@cf, true)
                @user.save!
                @user.tags = ""
                @user.save!
              end

              CustomField::Checkbox::TRUTHY_VALUES.each do |field_value|
                it "#{field_value} adds the tag to the user" do
                  refute @user.tags.include?("nothing_here")
                  @user.custom_field_values.set_field_value(@cf, field_value)
                  @user.save!
                  assert @user.tags.include?("nothing_here")
                  assert_equal(1, @user.custom_field_values.field_changes.size)
                end
              end
            end
          end
        end
      end
    end
  end

  describe "#update_from_hash" do
    describe "when the account does not have the 'user_and_organization_fields' feature" do
      before { Account.any_instance.stubs(:has_user_and_organization_fields?).returns(false) }

      it "does not update update custom field values" do
        create_user_custom_field!("field_2", "Some title", "Checkbox")
        @user.custom_field_values.update_from_hash("field_1" => "foo", "field_2" => "1")
        @user.save!
        @user.reload
        assert_equal({"field_1" => "Foobar", "field_2" => false}, @user.custom_field_values.as_json(account: @user.account))
      end

      it "does not delete keys when the value is nil" do
        create_user_custom_field!("field_2", "Some title", "Checkbox")
        @user.custom_field_values.update_from_hash("field_1" => "foo")
        @user.save!
        cfv_id = @user.custom_field_values.value_for_key("field_1").id

        @user.custom_field_values.update_from_hash("field_1" => nil)

        assert CustomField::Value.find(cfv_id)
        @user.save!
        assert_equal({"field_1" => "Foobar", "field_2" => false }, @user.custom_field_values.as_json(account: @user.account))
        assert CustomField::Value.find_by_id(cfv_id)
      end

      it "does not update a row that exists" do
        @user.custom_field_values.update_from_hash("field_1" => "babar")
        assert_equal({"field_1" => "Foobar"}, @user.custom_field_values.as_json(account: @user.account))
      end

      it "does not create a row when it does not exist" do
        create_custom_field!("field_2", "Nothing Here", "User", "Text")
        @user.custom_field_values.update_from_hash("field_2" => "joe-bob")
        assert_equal({"field_1" => "Foobar", "field_2" => nil}, @user.custom_field_values.as_json(account: @user.account))
      end

      it "does not save the row along with user-save" do
        create_user_custom_field!("field_2", "Nothing Here", "Text")
        @user.custom_field_values.update_from_hash("field_2" => "joe-bob")
        @user.custom_field_values.update_from_hash("field_2" => "joe-bob-briggs")
        @user.save!

        assert_equal({"field_1" => "Foobar", "field_2" => nil}, @user.reload.custom_field_values.as_json(account: @user.account))
        assert_equal 1, @user.custom_field_values.size
      end
    end

    describe "when the account has the 'user_and_organization_fields' feature" do
      before { Account.any_instance.stubs(:has_user_and_organization_fields?).returns(true) }

      it "store/creates values as necessary" do
        @cf2 = create_user_custom_field!("field_2", "Some title", "Checkbox")
        @user.custom_field_values.update_from_hash("field_1" => "foo", "field_2" => "1")
        assert_equal(2, @user.custom_field_values.field_changes.size)
        assert_same_elements(@user.custom_field_values.field_changes, [[@custom_field, "Foobar", "foo"], [@cf2, nil, "1"]])
        @user.save!
        @user.reload
        assert_equal({"field_1" => "foo", "field_2" => true}, @user.custom_field_values.as_json(account: @user.account))
      end

      it "accepts string keys" do
        @cf2 = create_user_custom_field!("field_2", "Some title", "Checkbox")
        @user.custom_field_values.update_from_hash("field_1" => "foo", "field_2" => "1")
        assert_equal(2, @user.custom_field_values.field_changes.size)
        assert_same_elements(@user.custom_field_values.field_changes, [[@custom_field, "Foobar", "foo"], [@cf2, nil, "1"]])
        @user.save!
        @user.reload
        assert_equal({"field_1" => "foo", "field_2" => true}, @user.custom_field_values.as_json(account: @user.account))
      end

      it "drops unknown keys on the floor" do
        @cf2 = create_user_custom_field!("field_2", "Some title", "Checkbox")
        @user.custom_field_values.update_from_hash("field_1" => "foo", "field_2" => "1", "field_not_there" => 5)
        assert_equal(2, @user.custom_field_values.field_changes.size)
        assert_same_elements(@user.custom_field_values.field_changes, [[@custom_field, "Foobar", "foo"], [@cf2, nil, "1"]])
        @user.save!
        @user.reload
        assert_equal({"field_1" => "foo", "field_2" => true}, @user.custom_field_values.as_json(account: @user.account))
      end

      it "deletes keys when the value is nil" do
        create_user_custom_field!("field_2", "Some title", "Checkbox")
        @user.custom_field_values.update_from_hash("field_1" => "foo")
        @user.save!
        cfv_id = @user.custom_field_values.value_for_key("field_1").id
        @user.custom_field_values.update_from_hash("field_1" => nil)
        assert_equal(2, @user.custom_field_values.field_changes.size)

        # ensure the destroy doesn't happen until save
        assert CustomField::Value.find(cfv_id)
        @user.save!

        assert_equal({"field_1" => nil, "field_2" => false }, @user.custom_field_values.as_json(account: @user.account))
        refute CustomField::Value.find_by_id(cfv_id)
      end

      it "updates a row that exists" do
        @user.custom_field_values.update_from_hash("field_1" => "babar")
        assert_equal(1, @user.custom_field_values.field_changes.size)
        assert_equal({"field_1" => "babar"}, @user.custom_field_values.as_json(account: @user.account))
      end

      it "creates a row when it does not exist" do
        create_custom_field!("field_2", "Nothing Here", "User", "Text")
        @user.custom_field_values.update_from_hash("field_2" => "joe-bob")
        assert_equal(1, @user.custom_field_values.field_changes.size)
        assert_equal({"field_1" => "Foobar", "field_2" => "joe-bob"}, @user.custom_field_values.as_json(account: @user.account))
      end

      it "inherits the account-id from the owner" do
        create_custom_field!("field_2", "Nothing Here", "User", "Text")
        @user.custom_field_values.update_from_hash("field_2" => "joe-bob")
        assert_equal(1, @user.custom_field_values.field_changes.size)
        @user.save!
        @user.reload
        val = @user.custom_field_values.value_for_key("field_2")
        assert_equal @user.account_id, val.account_id
      end

      it "saves the row along with user-save" do
        create_user_custom_field!("field_2", "Nothing Here", "Text")
        @user.custom_field_values.update_from_hash("field_2" => "joe-bob")
        @user.custom_field_values.update_from_hash("field_2" => "joe-bob-briggs")
        assert_equal(2, @user.custom_field_values.field_changes.size)
        @user.save!

        assert_equal({"field_1" => "Foobar", "field_2" => "joe-bob-briggs"}, @user.reload.custom_field_values.as_json(account: @user.account))
        assert_equal 2, @user.custom_field_values.size
      end

      describe "with checkbox field" do
        before do
          create_user_custom_field!("field_2", "Some title", "Checkbox")
        end

        describe "when updating from nil to false" do
          before do
            Timecop.travel(1.minute.ago) do
              @user.custom_field_values.update_from_hash("field_2" => nil)
              assert_equal(1, @user.custom_field_values.field_changes.size)
              @user.save!
              @user.reload
              assert_equal false, @user.custom_field_values.as_json(account: @user.account)["field_2"]
            end
          end

          it "does not update updated_at of owner entity" do
            was_updated_at = @user.updated_at
            @user.custom_field_values.update_from_hash("field_2" => false)
            assert_equal(1, @user.custom_field_values.field_changes.size)
            @user.save!
            @user.reload
            assert_equal false, @user.custom_field_values.as_json(account: @user.account)["field_2"]
            assert_equal @user.updated_at, was_updated_at
          end
        end

        describe "when updating from false to nil" do
          before do
            Timecop.travel(1.minute.ago) do
              @user.custom_field_values.update_from_hash("field_2" => false)
              assert_equal(1, @user.custom_field_values.field_changes.size)
              @user.save!
              @user.reload
              assert_equal false, @user.custom_field_values.as_json(account: @user.account)["field_2"]
            end
          end

          it "does not update updated_at of owner entity" do
            was_updated_at = @user.updated_at
            @user.custom_field_values.update_from_hash("field_2" => nil)
            assert_equal(1, @user.custom_field_values.field_changes.size)
            @user.save!
            @user.reload
            assert_equal false, @user.custom_field_values.as_json(account: @user.account)["field_2"]
            assert_equal @user.updated_at, was_updated_at
          end
        end

        describe "when updating from false to true" do
          before do
            Timecop.travel(1.minute.ago) do
              @user.custom_field_values.update_from_hash("field_2" => false)
              assert_equal(1, @user.custom_field_values.field_changes.size)
              @user.save!
              @user.reload
              assert_equal false, @user.custom_field_values.as_json(account: @user.account)["field_2"]
            end
          end

          it "updates updated_at of owner entity" do
            was_updated_at = @user.updated_at
            @user.custom_field_values.update_from_hash("field_2" => true)
            assert_equal(1, @user.custom_field_values.field_changes.size)
            @user.save!
            @user.reload
            assert @user.custom_field_values.as_json(account: @user.account)["field_2"]
            assert_operator @user.updated_at, :>, was_updated_at
          end
        end

        describe "when updating from nil to true" do
          before do
            Timecop.travel(1.minute.ago) do
              @user.custom_field_values.update_from_hash("field_2" => nil)
              @user.save!
              @user.reload
              assert_equal false, @user.custom_field_values.as_json(account: @user.account)["field_2"]
            end
          end

          it "updates updated_at of owner entity" do
            was_updated_at = @user.updated_at
            @user.custom_field_values.update_from_hash("field_2" => true)
            assert_equal(1, @user.custom_field_values.field_changes.size)
            @user.save!
            @user.reload
            assert @user.custom_field_values.as_json(account: @user.account)["field_2"]
            assert_operator @user.updated_at, :>, was_updated_at
          end
        end

        describe "when updating from true to false" do
          before do
            Timecop.travel(1.minute.ago) do
              @user.custom_field_values.update_from_hash("field_2" => true)
              assert_equal(1, @user.custom_field_values.field_changes.size)
              @user.save!
              @user.reload
              assert @user.custom_field_values.as_json(account: @user.account)["field_2"]
            end
          end

          it "updates updated_at of owner entity" do
            was_updated_at = @user.updated_at
            @user.custom_field_values.update_from_hash("field_2" => false)
            assert_equal(1, @user.custom_field_values.field_changes.size)
            @user.save!
            @user.reload
            assert_equal false, @user.custom_field_values.as_json(account: @user.account)["field_2"]
            assert_operator @user.updated_at, :>, was_updated_at
          end
        end

        describe "when updating from true to nil" do
          before do
            Timecop.travel(1.minute.ago) do
              @user.custom_field_values.update_from_hash("field_2" => true)
              assert_equal(1, @user.custom_field_values.field_changes.size)
              @user.save!
              @user.reload
              assert @user.custom_field_values.as_json(account: @user.account)["field_2"]
            end
          end

          it "updates updated_at of owner entity" do
            was_updated_at = @user.updated_at
            @user.custom_field_values.update_from_hash("field_2" => nil)
            assert_equal(1, @user.custom_field_values.field_changes.size)
            @user.save!
            @user.reload
            assert_equal false, @user.custom_field_values.as_json(account: @user.account)["field_2"]
            assert_operator @user.updated_at, :>, was_updated_at
          end
        end

        describe "when updating from true to true" do
          before do
            Timecop.travel(1.minute.ago) do
              @user.custom_field_values.update_from_hash("field_2" => true)
              assert_equal(1, @user.custom_field_values.field_changes.size)
              @user.save!
              @user.reload
              assert @user.custom_field_values.as_json(account: @user.account)["field_2"]
            end
          end

          it "does not update updated_at of owner entity" do
            was_updated_at = @user.updated_at
            @user.custom_field_values.update_from_hash("field_2" => true)
            assert_equal(0, @user.custom_field_values.field_changes.size)
            @user.save!
            @user.reload
            assert @user.custom_field_values.as_json(account: @user.account)["field_2"]
            assert_equal @user.updated_at, was_updated_at
          end
        end
      end

      [
        ["Decimal", 123.45, 456.78],
        ["Date", Time.parse("2012-07-04"), Time.parse("2013-05-05")],
        ["Text", "foo", "bar"],
        ["Dropdown", "100", "200", [{name: "Something", value: "100"}, {name: "Something else", value: "200"}]]
      ].each do |field_type, value_1, value_2, dropdown_choices|
        describe "with #{field_type} field" do
          before do
            create_user_custom_field!("field_2", "something", field_type, dropdown_choices: dropdown_choices)
          end

          describe "when a value is present" do
            before do
              Timecop.travel(1.minute.ago) do
                @user.custom_field_values.update_from_hash("field_2" => value_1)
                @user.save!
                assert_equal(1, @user.custom_field_values.field_changes.size)
                @user.reload
                assert_equal(value_1, @user.custom_field_values.as_json(account: @user.account)["field_2"])
              end
            end

            describe "when updating with the same value" do
              it "does not update updated_at of owner entity" do
                was_updated_at = @user.updated_at
                @user.custom_field_values.update_from_hash("field_2" => value_1)
                @user.save!
                assert_equal(0, @user.custom_field_values.field_changes.size)
                @user.reload
                assert_equal(value_1, @user.custom_field_values.as_json(account: @user.account)["field_2"])
                assert_equal @user.updated_at, was_updated_at
              end
            end

            describe "when updating with a different value" do
              it "updates updated_at of owner entity" do
                was_updated_at = @user.updated_at
                @user.custom_field_values.update_from_hash("field_2" => value_2)
                @user.save!
                assert_equal(1, @user.custom_field_values.field_changes.size)
                @user.reload
                assert_equal(value_2, @user.custom_field_values.as_json(account: @user.account)["field_2"])
                assert_operator @user.updated_at, :>, was_updated_at
              end
            end

            describe "when removing the value" do
              it "updates updated_at of owner entity" do
                was_updated_at = @user.updated_at
                @user.custom_field_values.update_from_hash("field_2" => nil)
                @user.save!
                @user.reload
                assert_nil @user.custom_field_values.as_json(account: @user.account)["field_2"]
                assert_operator @user.updated_at, :>, was_updated_at
              end
            end
          end

          describe "when no value is present" do
            before do
              Timecop.travel(1.minute.ago) do
                @user.touch
                @user.save!
                @user.reload
                assert_nil @user.custom_field_values.as_json(account: @user.account)["field_2"]
              end
            end

            describe "when creating a value" do
              it "updates updated_at of owner entity" do
                was_updated_at = @user.updated_at
                @user.custom_field_values.update_from_hash("field_2" => value_1)
                @user.save!
                assert_equal(1, @user.custom_field_values.field_changes.size)
                @user.reload
                assert_equal(value_1, @user.custom_field_values.as_json(account: @user.account)["field_2"])
                assert_operator @user.updated_at, :>, was_updated_at
              end
            end

            describe "when removing the value" do
              it "does not update updated_at of owner entity" do
                was_updated_at = @user.updated_at
                @user.custom_field_values.update_from_hash("field_2" => nil)
                @user.save!
                assert_equal(1, @user.custom_field_values.field_changes.size)
                @user.reload
                assert_nil @user.custom_field_values.as_json(account: @user.account)["field_2"]
                assert_equal @user.updated_at, was_updated_at
              end
            end
          end
        end
      end

      describe "with a decimal field" do
        before do
          create_user_custom_field!("field_2", "Nothing Here", "Decimal")
        end

        it "whens set as decimal get back decimal" do
          @user.custom_field_values.update_from_hash("field_2" => 123.45)
          @user.save!
          assert_equal(1, @user.custom_field_values.field_changes.size)
          @user.reload
          assert_equal(123.45, @user.custom_field_values.as_json(account: @user.account)["field_2"])
        end

        it "casts String in base 10" do
          @user.custom_field_values.update_from_hash("field_2" => "0123.1230")
          @user.save!
          assert_equal(1, @user.custom_field_values.field_changes.size)
          assert_equal '0000000000123.1230', @user.custom_field_values.value_for_key("field_2").value
          assert_equal(123.1230, @user.custom_field_values.as_json(account: @user.account)["field_2"])
        end
      end

      describe "with an integer field" do
        before do
          create_user_custom_field!("field_2", "Nothing Here", "Integer")
        end

        it "whens set as integer get back integer" do
          @user.custom_field_values.update_from_hash("field_2" => -45)
          @user.save!
          assert_equal(1, @user.custom_field_values.field_changes.size)
          @user.reload
          assert_equal(-45, @user.custom_field_values.as_json(account: @user.account)["field_2"])
        end

        it "casts String in base 10" do
          @user.custom_field_values.update_from_hash("field_2" => "0123")
          @user.save!
          assert_equal(1, @user.custom_field_values.field_changes.size)
          assert_equal '0000000000123', @user.custom_field_values.value_for_key("field_2").value
          assert_equal(123, @user.custom_field_values.as_json(account: @user.account)["field_2"])
        end

        it "casts Integer" do
          @user.custom_field_values.update_from_hash("field_2" => 123)
          @user.save!
          assert_equal(1, @user.custom_field_values.field_changes.size)
          assert_equal '0000000000123', @user.custom_field_values.value_for_key("field_2").value
          assert_equal(123, @user.custom_field_values.as_json(account: @user.account)["field_2"])
        end
      end

      describe "with an integer field with a validation regexp" do
        let(:number_as_integer) { 1234567890123456 }
        let(:number_as_string) { "1234567890123456" }

        before do
          create_user_custom_field!("field_2", "Nothing Here", "Integer", regexp_for_validation: '\A\d{1,16}\z')
        end

        it "allows cast of 16 digit String in base 10" do
          @user.custom_field_values.update_from_hash("field_2" => number_as_string)
          @user.save!
          assert_equal(1, @user.custom_field_values.field_changes.size)
          assert_equal Zendesk::CustomField::NumberPadding.pad(number_as_integer), @user.custom_field_values.value_for_key("field_2").value
          assert_equal(number_as_integer, @user.custom_field_values.as_json(account: @user.account)["field_2"])
        end

        it "allows cast of 16 digit Integer in base 10" do
          @user.custom_field_values.update_from_hash("field_2" => number_as_integer)
          @user.save!
          assert_equal(1, @user.custom_field_values.field_changes.size)
          assert_equal Zendesk::CustomField::NumberPadding.pad(number_as_integer), @user.custom_field_values.value_for_key("field_2").value
          assert_equal(number_as_integer, @user.custom_field_values.as_json(account: @user.account)["field_2"])
        end
      end

      describe "with an date field" do
        before do
          create_user_custom_field!("field_2", "Nothing Here", "Date")
        end

        it "whens set as date get back date" do
          mytime = Time.parse("2012-07-04")
          @user.custom_field_values.update_from_hash("field_2" => mytime)
          @user.save!
          assert_equal(1, @user.custom_field_values.field_changes.size)
          @user.reload
          assert_equal(mytime, @user.custom_field_values.as_json(account: @user.account)["field_2"])
        end
      end

      describe "with a checkbox field" do
        before do
          create_user_custom_field!("field_2", "Nothing Here", "Checkbox")
        end

        describe "gets and sets" do
          field_values = ["true", "false", "1", true]
          as_jsons = [true, false, true, true]

          field_values.each_with_index do |field_value, i|
            it "#{field_value} as #{as_jsons[i]}" do
              @user.custom_field_values.update_from_hash("field_2" => field_value)
              @user.save!
              assert_equal(1, @user.custom_field_values.field_changes.size)
              @user.reload
              assert_equal(as_jsons[i], @user.custom_field_values.as_json(account: @user.account)["field_2"])
            end
          end
        end

        describe "when unset" do
          it "returns as false (not null)" do
            assert_equal false, @user.custom_field_values.as_json(account: @user.account)["field_2"]
          end
        end
      end

      describe "with a dropdown field" do
        before do
          cf = create_user_custom_field!("field_2", "Nothing Here", "Dropdown")
          cf.dropdown_choices.build(name: "Something", value: "100")
          cf.dropdown_choices.build(name: "Something else", value: "200")
          cf.save!
        end

        it "whens set dropdown value get back the dropdown value" do
          @user.custom_field_values.update_from_hash("field_2" => "100")
          @user.save!
          assert_equal(1, @user.custom_field_values.field_changes.size)
          @user.reload
          assert_equal("Something", @user.custom_field_values.as_json(title: true, account: @user.account)["field_2"])
        end
      end
    end
  end
end
