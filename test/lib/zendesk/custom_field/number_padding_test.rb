require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::CustomField::NumberPadding do
  describe "Padding values to strings" do
    it "works for integers" do
      assert_equal("0000000000123", Zendesk::CustomField::NumberPadding.pad(123))
    end

    it "works for negative integers" do
      assert_equal("-000000000123", Zendesk::CustomField::NumberPadding.pad(-123))
    end

    it "works for 0" do
      assert_equal("0000000000000", Zendesk::CustomField::NumberPadding.pad(0))
    end

    it "works for integers passing in left_dig and right_dig" do
      assert_equal("-000123.10", Zendesk::CustomField::NumberPadding.pad(-123.1, 6, 2))
    end

    it "works for floats" do
      assert_equal("0210123456789.1234", Zendesk::CustomField::NumberPadding.pad(210123456789.1234))
    end

    it "works for negative floats" do
      assert_equal("-210123456789.1234", Zendesk::CustomField::NumberPadding.pad(-210123456789.1234))
    end

    it "works for small negative floats" do
      assert_equal("-000000000000.0002", Zendesk::CustomField::NumberPadding.pad(-0.00019))
    end

    it "works for 0.0" do
      assert_equal("0000000000000.0000", Zendesk::CustomField::NumberPadding.pad(0.0))
    end

    it "works for floats passing in left_dig and right_dig" do
      assert_equal("-000123.45", Zendesk::CustomField::NumberPadding.pad(-123.454, 6, 2))
    end
  end
end
