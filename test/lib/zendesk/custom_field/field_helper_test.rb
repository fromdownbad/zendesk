require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::CustomField::FieldHelper do
  before do
    account = accounts(:minimum)
    @field  = account.custom_fields.build(key: "custom_field_foo", title: "title", owner: "User", description: "desc")
  end

  describe "#render_dc_or_i18n" do
    describe "when field isn't system field" do
      before do
        @field.stubs(:is_system?).returns(false)
      end

      describe "asked to render dynamic content" do
        it "renders dynamic content" do
          expects(:render_dynamic_content).returns('dynamic_attribute')
          assert_equal 'dynamic_attribute', render_dc_or_i18n(@field, 'test_attribute')
        end
      end
    end

    describe "when the field is system field" do
      before do
        @field.stubs(:is_system?).returns(true)
        I18n.stubs(:t).with('string_to_translate').returns('translated_string')
      end

      it "renders the translated key" do
        assert_equal 'translated_string', render_dc_or_i18n(@field, 'string_to_translate')
      end
    end
  end
end
