require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::BusinessHoursSupport::Calculation do
  let(:account)  { accounts(:minimum) }
  let(:schedule) do
    account.schedules.create(name: 'Test', time_zone: 'UTC').tap do |schedule|
      schedule.workweek.configure_as_default

      schedule.holiday_calendar.add_holiday(
        name:       'Holiday',
        start_date: '2006-01-26',
        end_date:   '2006-01-26'
      )
    end
  end

  let(:calculation) { Zendesk::BusinessHoursSupport::Calculation.new(schedule) }

  describe '#in_business_hours?' do
    describe 'when the time is in business hours' do
      let(:timestamp) { Time.utc(2006, 1, 2, 9, 30) }

      it 'returns true' do
        assert calculation.in_business_hours?(timestamp)
      end
    end

    describe 'when the time is not in business hours' do
      let(:timestamp) { Time.utc(2006, 1, 2, 8, 30) }

      it 'returns false' do
        refute calculation.in_business_hours?(timestamp)
      end
    end
  end

  describe '#business_minutes_diff' do
    describe 'when the result is a whole number of minutes' do
      let(:time_1) { Time.utc(2006, 1, 2, 7, 25) }
      let(:time_2) { Time.utc(2006, 1, 2, 22, 59) }

      it 'returns the number of business minutes between the times' do
        assert_equal 8 * 60, calculation.business_minutes_diff(time_1, time_2)
      end
    end

    describe 'when the result has fewer than 30 seconds in a partial minute' do
      let(:time_1) { Time.utc(2006, 1, 2, 9, 0) }
      let(:time_2) { Time.utc(2006, 1, 2, 14, 0, 29) }

      it 'rounds down the result to the nearest minute' do
        assert_equal 5 * 60, calculation.business_minutes_diff(time_1, time_2)
      end
    end

    describe 'when the result has 30 or more seconds in the partial minute' do
      let(:time_1) { Time.utc(2006, 1, 2, 9, 0) }
      let(:time_2) { Time.utc(2006, 1, 2, 14, 0, 30) }

      it 'rounds up the result to the nearest minute' do
        assert_equal(
          5 * 60 + 1,
          calculation.business_minutes_diff(time_1, time_2)
        )
      end
    end

    describe 'when there are more than five years between the times' do
      let(:time_1) { Time.utc(2015) }
      let(:time_2) { Time.utc(2025) }

      it 'limits the result to five years' do
        assert_equal(
          calculation.business_minutes_diff(time_1, time_1 + 5.years),
          calculation.business_minutes_diff(time_1, time_2)
        )
      end
    end
  end

  describe '#calendar_hours_ago' do
    describe 'when the number of specified business hours is negative' do
      before { Timecop.freeze(Time.utc(2006, 1, 2, 17)) }

      it 'returns zero' do
        assert_equal 0, calculation.calendar_hours_ago(-4)
      end
    end

    describe 'when the result is a whole number of hours' do
      before { Timecop.freeze(Time.utc(2006, 1, 3, 10)) }

      it 'returns the number of calendar hours' do
        assert_equal 25, calculation.calendar_hours_ago(9)
      end
    end

    describe 'when the result has fewer than 30 minutes in the partial hour' do
      before { Timecop.freeze(Time.utc(2006, 1, 2, 17, 29)) }

      it 'rounds the result down to the nearest hour' do
        assert_equal 1, calculation.calendar_hours_ago(1)
      end
    end

    describe 'when the result has 30 or more minutes in the partial hour' do
      before { Timecop.freeze(Time.utc(2006, 1, 2, 17, 30)) }

      it 'rounds the result up to the nearest hour' do
        assert_equal 2, calculation.calendar_hours_ago(1)
      end
    end

    describe 'when the number of hours is greater than the maximum allowed' do
      before do
        Timecop.freeze(Time.utc(2006, 1, 1, 0, 0))

        schedule.workweek.update(
          [
            {start_time:     0, end_time:  1_440},
            {start_time: 1_440, end_time:  2_880},
            {start_time: 2_880, end_time:  4_320},
            {start_time: 4_320, end_time:  5_760},
            {start_time: 5_760, end_time:  7_200},
            {start_time: 7_200, end_time:  8_640},
            {start_time: 8_640, end_time: 10_080}
          ]
        )
      end

      it 'maxes out at 10,000 business hours' do
        assert_equal 10_000, calculation.calendar_hours_ago(999_999_999)
      end
    end
  end

  describe '#calendar_hours_from_now' do
    describe 'when the number of specified business hours is negative' do
      before { Timecop.freeze(Time.utc(2006, 1, 2, 9)) }

      it 'returns zero' do
        assert_equal 0, calculation.calendar_hours_from_now(-4)
      end
    end

    describe 'when the result is a whole number of hours' do
      before { Timecop.freeze(Time.utc(2006, 1, 2, 16)) }

      it 'returns the number of calendar hours' do
        assert_equal 24, calculation.calendar_hours_from_now(8)
      end
    end

    describe 'when the result has fewer than 30 minutes in the partial hour' do
      before { Timecop.freeze(Time.utc(2006, 1, 2, 8, 31)) }

      it 'rounds the result down to the nearest hour' do
        assert_equal 1, calculation.calendar_hours_from_now(1)
      end
    end

    describe 'when the result has 30 or more minutes in the partial hour' do
      before { Timecop.freeze(Time.utc(2006, 1, 2, 8, 30)) }

      it 'rounds the result up to the nearest hour' do
        assert_equal 2, calculation.calendar_hours_from_now(1)
      end
    end

    describe 'when the number of hours is greater than the maximum allowed' do
      before do
        Timecop.freeze(Time.utc(2007, 1, 1, 0, 0))

        schedule.workweek.update(
          [
            {start_time:     0, end_time:  1_440},
            {start_time: 1_440, end_time:  2_880},
            {start_time: 2_880, end_time:  4_320},
            {start_time: 4_320, end_time:  5_760},
            {start_time: 5_760, end_time:  7_200},
            {start_time: 7_200, end_time:  8_640},
            {start_time: 8_640, end_time: 10_080}
          ]
        )
      end

      it 'maxes out at 10,000 business hours' do
        assert_equal 10_000, calculation.calendar_hours_from_now(999_999_999)
      end
    end
  end

  describe '#first_hour_before' do
    before { Timecop.freeze(Time.utc(2007, 1, 1, 0, 0)) }

    describe 'when a time is specified' do
      let(:timestamp) { Time.utc(2006, 1, 2, 9, 23) }

      it 'returns the first business hour before time' do
        assert_equal(
          Time.utc(2005, 12, 30, 16, 23),
          calculation.first_hour_before(timestamp)
        )
      end
    end

    describe 'when a timestamp is specified' do
      let(:timestamp) { Time.utc(2006, 1, 2, 9, 23).to_i }

      it 'returns the first business hour before time' do
        assert_equal(
          Time.utc(2005, 12, 30, 16, 23),
          calculation.first_hour_before(timestamp)
        )
      end
    end
  end

  describe '#schedule_id' do
    it 'returns the schedule id' do
      assert_equal schedule.id, calculation.schedule_id
    end
  end

  describe '#current_holiday' do
    describe 'when the current time is on a holiday' do
      before { Timecop.freeze(Time.utc(2006, 1, 26)) }

      it 'returns the holiday' do
        assert_equal schedule.holidays.first, calculation.current_holiday
      end
    end

    describe 'when the current time is not on a holiday' do
      before { Timecop.freeze(Time.utc(2006, 1, 27)) }

      it 'returns nil' do
        assert_nil calculation.current_holiday
      end
    end

    describe 'when the current time is on more than one holiday' do
      before do
        Timecop.freeze(Time.utc(2006, 1, 26))

        schedule.holiday_calendar.add_holiday(
          name:       'Another Holiday',
          start_date: '2006-01-26',
          end_date:   '2006-01-26'
        )
      end

      it 'returns the first matching holiday' do
        assert_equal schedule.holidays.first, calculation.current_holiday
      end
    end
  end

  describe '#in_business_hours?' do
    describe 'when the time is in business hours' do
      let(:timestamp) { Time.utc(2006, 1, 2, 10) }

      it 'returns true' do
        assert calculation.in_business_hours?(timestamp)
      end
    end

    describe 'when the time is not in business hours' do
      let(:timestamp) { Time.utc(2006, 1, 1, 10) }

      it 'returns false' do
        refute calculation.in_business_hours?(timestamp)
      end
    end
  end

  describe '#on_holiday?' do
    describe 'when the time is on a holiday' do
      let(:timestamp) { Time.utc(2006, 1, 26) }

      it 'returns true' do
        assert calculation.on_holiday?(timestamp)
      end
    end

    describe 'when the time is not on a holiday' do
      let(:timestamp) { Time.utc(2006, 1, 27) }

      it 'returns false' do
        refute calculation.on_holiday?(timestamp)
      end
    end
  end
end
