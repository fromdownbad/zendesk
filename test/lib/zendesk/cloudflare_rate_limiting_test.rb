require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::CloudflareRateLimiting do
  describe "#rate_limit_helper" do
    let(:controller) { MockController.new }
    let(:headers) { {} }

    before do
      mock_response = mock
      mock_response.stubs(:headers).returns(headers)
      controller.stubs(:response).returns(mock_response)
    end

    describe "mild" do
      it "sets ZENDESK-EP to 10" do
        controller.mild
        assert_equal 10, headers['ZENDESK-EP']
      end
    end

    describe "mild simulate" do
      it "sets ZENDESK-EP to -10" do
        controller.mild_simulate
        assert_equal -10, headers['ZENDESK-EP']
      end
    end

    describe "strong" do
      it "sets ZENDESK-EP to 5" do
        controller.strong
        assert_equal 5, headers['ZENDESK-EP']
      end
    end

    describe "strong simulate" do
      it "sets ZENDESK-EP to -5" do
        controller.strong_simulate
        assert_equal -5, headers['ZENDESK-EP']
      end
    end
  end

  class MockController
    include Zendesk::CloudflareRateLimiting

    def mild
      rate_limit_header(:mild, simulate: false)
    end

    def mild_simulate
      rate_limit_header(:mild, simulate: true)
    end

    def strong
      rate_limit_header(:strong, simulate: false)
    end

    def strong_simulate
      rate_limit_header(:strong, simulate: true)
    end
  end
end
