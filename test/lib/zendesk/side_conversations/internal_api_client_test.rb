require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SideConversations::InternalApiClient do
  fixtures :accounts, :tickets
  let(:account) { accounts(:minimum) }
  let(:api_client) { Zendesk::SideConversations::InternalApiClient.new(account: account) }

  let(:search_url) do
    "http://pod-#{ENV.fetch('ZENDESK_POD_ID')}.collaboration-search.service.consul:3000/accounts/#{account.id}/search_threads?entity_prefix=zen:ticket&from=0&q=potato&size=25"
  end

  describe "#search_side_conversations" do
    let(:options) { {per_page: 25, page: 1} }

    describe "on success" do
      it "gets the results" do
        stub_request(:get, search_url).
          to_return(status: 200, body: JSON.dump(total: 10, results: [{ id: 1 }, { id: 2 }]), headers: { 'Content-Type' => 'application/json' })

        results = api_client.search_side_conversations('potato', options)
        assert_equal({ 'total' => 10, 'results' => [{ 'id' => 1 }, { 'id' => 2 }] }, results)
      end
    end

    describe 'on failure' do
      it 'returns an empty object' do
        stub_request(:get, search_url).
          to_return(status: 401, body: JSON.dump([]), headers: { 'Content-Type' => 'application/json' })

        assert_equal({}, api_client.search_side_conversations('potato', options))
      end

      it 'should return empty object if we can not reach our service' do
        stub_request(:get, search_url).
          to_raise(Kragle::ClientError)

        assert_equal({}, api_client.search_side_conversations('potato', options))
      end
    end
  end
end
