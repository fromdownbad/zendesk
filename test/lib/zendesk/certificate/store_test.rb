require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 18

describe Zendesk::Certificate::Store do
  fixtures :certificate_authorities

  before do
    @store = Zendesk::Certificate::Store.new
    @store.load_from_db
  end

  it "has CA roots" do
    assert_kind_of OpenSSL::X509::Certificate, @store.ca_roots[0]
    assert_equal @store.ca_roots[0].subject.to_s, @store.ca_roots[0].issuer.to_s
  end

  it "has CA intermediates" do
    assert_kind_of OpenSSL::X509::Certificate, @store.ca_intermediates[0]
  end

  it "verifys all root CA certificates" do
    @store.ca_roots.each do |cert|
      ca_chain = @store.chain(cert)
      assert_kind_of Array, ca_chain
      assert ca_chain.empty? || ca_chain.size == 2
      assert @store.verify(ca_chain, cert)
    end
  end

  describe "with roots and intermediates" do
    it "verifys all intermediate certificates" do
      @store.ca_intermediates.each do |cert|
        ca_chain = @store.chain(cert)
        assert_kind_of Array, ca_chain
        assert @store.verify(ca_chain, cert)
      end
    end

    it "verifys a certificate" do
      cert = OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('help.amadorlabs.com.crt'))
      ca_chain = @store.chain(cert)
      assert_kind_of Array, ca_chain
      assert_equal 4, ca_chain.size
      assert @store.verify(ca_chain, cert)
      assert_equal 3, @store.intermediate_chain(ca_chain).size
    end

    it "returns an issuer string (not array) with an intermediate missing and chain not built" do
      cert = OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('help.amadorlabs.com.crt'))
      ca_chain = @store.chain(cert)
      # chain will be missing an intermediate now
      ca_remove = ca_chain[1]
      @store.ca_intermediates.delete(ca_remove)
      ca_chain2 = @store.chain(cert)
      assert_kind_of String, ca_chain2
      assert_equal ca_remove.subject.to_s, ca_chain2
    end

    it "append_intermediates" do
      cert = OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('help.amadorlabs.com.crt'))
      ca_chain = @store.chain(cert)

      # chain will be missing an intermediate now
      ca_remove = ca_chain[1]
      @store.ca_intermediates.delete(ca_remove)

      # verify intermediate missing
      ca_chain2 = @store.chain(cert)
      assert_kind_of String, ca_chain2

      # re-add intermediate
      t = Tempfile.new('removed_intermediate.crt')
      begin
        t.write(ca_remove.to_s)
        t.close
        assert_nil @store.append_intermediate(t.path, cert)
      ensure
        t.close
        t.unlink
      end

      # verify cert chain
      ca_chain = @store.chain(cert)
      assert_kind_of Array, ca_chain
      assert_equal 4, ca_chain.size
      assert @store.verify(ca_chain, cert)
      assert_equal 3, @store.intermediate_chain(ca_chain).size
    end
  end
end
