require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Certificate::Mover do
  fixtures :certificate_ips, :certificates

  let!(:new_ip_hold_duration) { 1.month.from_now - 1.hour }

  before do
    @certificate = certificates(:active1)
    @source_pod_id = 1
    @target_pod_id = 2
    @mover = Zendesk::Certificate::Mover.new @certificate
  end

  describe "#prepare" do
    it "assigns an available IP on the new pod" do
      new_ip = @mover.prepare(pod_id: @target_pod_id)
      assert_equal new_ip.pod_id, @target_pod_id
      assert new_ip.release_at
      assert new_ip.release_at > new_ip_hold_duration
    end

    it "returns the same IP when it already has one in the pod" do
      old_ip = @certificate.certificate_ips.first
      new_ip = @mover.prepare pod_id: old_ip.pod_id
      assert_equal new_ip, old_ip
    end

    it "updates the release for same IP when it already has one in the pod" do
      new_ip = @mover.prepare pod_id: @target_pod_id
      new_ip.schedule_release!(1.day.from_now)

      renew_ip = @mover.prepare pod_id: @target_pod_id
      assert_equal renew_ip, new_ip
      assert renew_ip.release_at
      assert renew_ip.release_at > new_ip_hold_duration
    end

    describe "with an sni certificate" do
      it "creates a sni certificate ip record" do
        @certificate.update_attribute(:sni_enabled, true)
        new_ip = @mover.prepare(pod_id: @target_pod_id)

        assert(new_ip.sni)
        assert_nil new_ip.ip
        assert_equal new_ip.pod_id, @target_pod_id
        assert new_ip.release_at
        assert new_ip.release_at > new_ip_hold_duration
      end
    end
  end

  describe "#swap" do
    it "does not schedule a release_at if target and source pods are the same" do
      new_ip = @mover.swap(@source_pod_id, @source_pod_id)
      assert_nil new_ip.release_at
    end

    it "404 if an IP wasn't already acquired in the target pod (e.g. by prepare)" do
      assert_raises(ActiveRecord::RecordNotFound) { @mover.swap(@source_pod_id, @target_pod_id) }
    end

    describe "with new ip being prepared" do
      let(:old_ip) { certificate_ips(:three) }

      before do
        new_ip = certificate_ips(:two)
        new_ip.update_attribute :certificate_id, @certificate.id
        new_ip.schedule_release!(1.day.from_now)
      end

      it "schedules the old IP to be released" do
        new_ip = @mover.swap(@source_pod_id, @target_pod_id)
        assert old_ip.release_at > 23.hours.from_now
        assert_equal new_ip.old_ip, old_ip.ip
        assert_nil new_ip.release_at
      end

      it "does not record old_ip when moving from aws" do
        old_ip.ip = nil
        old_ip.alias_record = 'xyz'
        old_ip.save!

        new_ip = @mover.swap(@source_pod_id, @target_pod_id)

        assert old_ip.reload.release_at > 23.hours.from_now
        assert_nil new_ip.old_ip
        assert_nil new_ip.release_at
      end
    end
  end

  describe "#rollback" do
    before do
      # after a swap, but the old ip is still associated
      @old_ip = certificate_ips(:three)
      @old_ip.schedule_release!(1.day.from_now)

      @new_ip = certificate_ips(:two)
      @new_ip.old_ip         = @old_ip.ip
      @new_ip.certificate_id = @old_ip.certificate_id
      @new_ip.save!
    end

    it "uses the original IP if still associated" do
      original_ip = @mover.rollback(@source_pod_id, @target_pod_id)
      assert_equal @old_ip.id, original_ip.id
    end

    it "returns the original IP even when target pod does not have an IP yet" do
      original_ip = @mover.rollback(@source_pod_id, 12345)
      assert_equal @old_ip.id, original_ip.id
    end

    it "reclaims the original IP if its available" do
      @old_ip.certificate_id = nil
      @old_ip.release_at = nil
      @old_ip.save!
      original_ip = @mover.rollback(@source_pod_id, @target_pod_id)
      assert_equal @old_ip.id, original_ip.id
    end

    it "finds a new IP on the original pod if original IP is already reassigned" do
      certificates(:temporary).certificate_ips << @old_ip
      @mover.rollback(@source_pod_id, @target_pod_id)
      assert_equal certificate_ips(:one).id, @certificate.certificate_ips.where(pod_id: @source_pod_id).first.id
    end

    it "schedules release the target pod IP and unschedule the source pod IP" do
      original_ip = @mover.rollback(@source_pod_id, @target_pod_id)
      @new_ip.reload
      assert @new_ip.release_at
      assert @new_ip.release_at > new_ip_hold_duration
      assert_nil original_ip.release_at
    end

    describe "when moved from an aws pod" do
      before do
        @old_ip.delete # aws ip was already released
        @new_ip.update_column(:old_ip, nil) # was moved from aws, so no ip address was stored

        # another innocent aws IP that happens to be unassigned (should not happen ...)
        @other = CertificateIp.create! do |ip|
          ip.alias_record = 'xxx'
          ip.pod_id = @source_pod_id
        end
      end

      it "adds a new ip when the old one was already released" do
        @mover.rollback(@source_pod_id, @target_pod_id)
        assert @certificate.certificate_ips.where(pod_id: @source_pod_id).first
      end
    end
  end
end
