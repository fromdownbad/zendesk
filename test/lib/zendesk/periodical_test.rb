require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::Periodical do
  let(:p) { Zendesk::Periodical.new(1) }

  it "executes only periodically" do
    s = Time.now
    count = 0
    while Time.now - s < 2
      Timecop.travel(Time.now + 0.1.seconds)
      p.perform do
        count += 1
      end
    end

    assert(count <= 3)
    assert(count >= 1)
  end

  it "executes always if delayed by more time than the period" do
    count = 0

    4.times do
      p.perform do
        count += 1
      end
      Timecop.travel(Time.now + 1.5.seconds)
    end

    assert(count == 4)
  end
end
