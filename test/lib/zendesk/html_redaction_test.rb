require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::HtmlRedaction do
  let(:text) { "& " }
  let(:body) { "<span> Heart &amp; Soul </span>" }
  let(:swedish_body) { 'Jag är en padda med en sköld.<br><br>Jag är en sköldpadda.' }
  let(:swedish_text) { 'padda med en sköld' }
  let(:japanese_body) { "私はカメです。 私はシールド付きのヒキガエルです。<br><br><br>" }
  let(:japanese_text) { "はシールド付き" }
  let(:russian_body) { "Я черепаха.<br>" }
  let(:russian_text) { "ерепа" }
  let(:mixed_entity_strategy_text) { "Sugar &amp; spice &#x26; everything nice" }
  let(:html_in_the_way) { "lo que la <strong>primavera</strong> hace con los cerezos" }

  describe "#redact" do
    it "replaces escaped ampersand in :body without modifying :body" do
      assert_equal "<span> Heart % Soul </span>", Zendesk::HtmlRedaction.redact(body, text, '%')
      assert_equal "<span> Heart &amp; Soul </span>", body
    end
  end

  describe "#redact!" do
    it "replaces escaped ampersand in :body and modifies :body" do
      assert_equal "<span> Heart ▇ Soul </span>", Zendesk::HtmlRedaction.redact!(body, text)
      assert_equal "<span> Heart ▇ Soul </span>", body
    end

    it "replaces escaped international characters in :body" do
      assert_equal 'Jag är en %%%%% %%% %% %%%%%.<br><br>Jag är en sköldpadda.', Zendesk::HtmlRedaction.redact!(swedish_body, swedish_text, '%')
    end

    it "replaces escaped japanese characters in :body" do
      assert_equal(
        '私はカメです。 私%%%%%%%のヒキガエルです。<br><br><br>',
        Zendesk::HtmlRedaction.redact!(japanese_body, japanese_text, '%')
      )
    end

    it "replaces escaped russian characters in :body" do
      assert_equal(
        'Я ч%%%%%ха.<br>',
        Zendesk::HtmlRedaction.redact!(russian_body, russian_text, '%')
      )
    end

    describe "sad path" do
      it "returns nil when there's nothing to redact" do
        assert_nil(
          Zendesk::HtmlRedaction.redact!("foo", "bar")
        )
      end

      it "won't behave consistently with mixed encoding strategies" do
        assert_equal(
          "Sugar % spice &#x26; everything nice",
          Zendesk::HtmlRedaction.redact!(mixed_entity_strategy_text, "&", "%")
        )
      end

      it "can't deal with HTML in the middle of the redaction target" do
        assert_nil(
          Zendesk::HtmlRedaction.redact!(html_in_the_way, "la primavera")
        )
      end
    end
  end

  describe "#redact_part_or_all!" do
    it "redacts full text iff text is nil and redact_full_comment is set to true" do
      assert_equal "▇▇▇▇▇", Zendesk::HtmlRedaction.redact_part_or_all!(body, nil, true)
    end

    it "does not full redact if text is not nil or redact_full_comment is false" do
      assert_equal "<span> Heart ▇ Soul </span>", Zendesk::HtmlRedaction.redact_part_or_all!(body, text, true)
    end
  end
end
