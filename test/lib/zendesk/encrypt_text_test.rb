# encoding: ascii

require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::EncryptText do
  before do
    @key = Digest::SHA1.hexdigest("12345whatever you like!").byteslice(0, 32)
    @cert = "-----BEGIN CERTIFICATE-----
MIIGKjCCBRKgAwIBAgIRAOR0oea5boVf0nj06xYjT1UwDQYJKoZIhvcNAQEFBQAw
cjELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4G
A1UEBxMHU2FsZm9yZDEaMBgGA1UEChMRQ09NT0RPIENBIExpbWl0ZWQxGDAWBgNV
BAMTD0Vzc2VudGlhbFNTTCBDQTAeFw0xMjA1MTYwMDAwMDBaFw0xMjA4MTQyMzU5
NTlaMFoxITAfBgNVBAsTGERvbWFpbiBDb250cm9sIFZhbGlkYXRlZDERMA8GA1UE
CxMIRnJlZSBTU0wxIjAgBgNVBAMTGXN1cHBvcnQuaGFybGFuc2V5bW91ci5jb20w
ggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCtwq+0OvHHozodzgXsywFx
B+RQs38qH3wI7721Cdh1V9sZc0fe4lWd2HdisBweQx4hhxBDIplfp081VOIOv0hr
6ObnXUTuaRXaVyax2nZ9E13DFHWD5UrYn84aBC98otDZp2h07KmctEpJ6ash4nqf
3BAFsL+ndQgSom/RovPau2nb/Waw17fzOJTVUzqENnL/KBsJCJU3VGZfeagS3w4M
ztJ8dqlE4JbCyaz+GVqNAIqPHUM3lMb/UHZ/jd8U0rj5HISUH7/RlTd334aexiJn
4SfICV5r21dP9f7i3wTkiaWKV/w+MNIn2a3tuFr8kaDTD9VjLFRyUYq7ZzwrPL6d
msb1qiJtjO2prhOlZ76jbWyD0E6o0g5lVKNlUH9S1cLsFLPZlcU3XleeQ2Ip/stD
1zwVCVh/giV297TWjVXRDUg15v3pHiGq3xsiv/9IawgIT66ieETOOtwzeXU64XZh
80e/mzQkoCwIHEBP7YpaxE/Z+G+kJeUpdS5EMOTpm/C/mKzUiTxBC+jP7tlt49zH
DkW5KtmX8dsXHx43dAglJJNUbDIWOqmE5B24vRd3iHJ+eJMH72A2nO463nYvN7Jo
VYErCsmAzDL9PwJ7gcXoz2LPN3tIy9SNl7FAs6baBPl15j8HxXcs+yI3AurqPWlT
OLYm42SVjUVLkpn+3BfWrQIDAQABo4IB0TCCAc0wHwYDVR0jBBgwFoAU2svqrVsI
Xcz//CZUzknlVcY49PgwHQYDVR0OBBYEFEnhecMVydOm0VwYlG2I7fYxXOiHMA4G
A1UdDwEB/wQEAwIFoDAMBgNVHRMBAf8EAjAAMDQGA1UdJQQtMCsGCCsGAQUFBwMB
BggrBgEFBQcDAgYKKwYBBAGCNwoDAwYJYIZIAYb4QgQBMEUGA1UdIAQ+MDwwOgYL
KwYBBAGyMQECAgcwKzApBggrBgEFBQcCARYdaHR0cHM6Ly9zZWN1cmUuY29tb2Rv
LmNvbS9DUFMwOwYDVR0fBDQwMjAwoC6gLIYqaHR0cDovL2NybC5jb21vZG9jYS5j
b20vRXNzZW50aWFsU1NMQ0EuY3JsMG4GCCsGAQUFBwEBBGIwYDA4BggrBgEFBQcw
AoYsaHR0cDovL2NydC5jb21vZG9jYS5jb20vRXNzZW50aWFsU1NMQ0FfMi5jcnQw
JAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmNvbW9kb2NhLmNvbTBDBgNVHREEPDA6
ghlzdXBwb3J0LmhhcmxhbnNleW1vdXIuY29tgh13d3cuc3VwcG9ydC5oYXJsYW5z
ZXltb3VyLmNvbTANBgkqhkiG9w0BAQUFAAOCAQEAh6H0aEHUoBByCm/Cyw2K/whb
61AlAk6C4bdB8jOxE90k3V4djq3ePa3yRNZB7D94KTpQYOPq1uKBxpwPd8NpwYXT
oAtGufowHPNjsCcONIpbODh0fQH8vKeenEZ8OBRn/8gEhrJIu2KmgiLwuvu29kCD
/vGWuDQpt61+L2tj8ZU1rkcoyk96Q01pCTk6jfWxgPMfaWsmLO5g1d1nzXTx0Qu9
yo5XfJb4Dxb5BqjHuISYcumyYRy5dHBL8AaEUwIlq2qA96O0UvBEntI+h554K9YF
Z+YZzyOv6kWGcqDIxfZNtP8F0LkzDmCx/w9IltpRWp4ZMV5Y5xeWZycSNc/zXw==
-----END CERTIFICATE-----"
  end

  it "decrypts encrypted text given the correct key" do
    encrypted_cert = Zendesk::EncryptText.encrypt(@key, @cert)
    assert_equal @cert, Zendesk::EncryptText.decrypt(@key, encrypted_cert)
    # try again
    encrypted_cert = Zendesk::EncryptText.encrypt(@key.reverse, @cert)
    assert_equal @cert, Zendesk::EncryptText.decrypt(@key.reverse, encrypted_cert)
  end

  it "does not decrypt encrypted text given an incorrect key" do
    encrypted_cert = Zendesk::EncryptText.encrypt(@key, @cert)
    decrypted_cert = Zendesk::EncryptText.decrypt(@key.reverse, encrypted_cert)
    assert decrypted_cert.nil?
  end

  it "decrypts legacy messages" do
    assert_equal "hi there", Zendesk::EncryptText.decrypt("12345", "YmNmODM4NDHqPoITCV2zYwt8EOdHQSa/Oa9WC1XuZ2s=\n")
  end
end
