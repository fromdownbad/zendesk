require_relative '../../../support/test_helper'

SingleCov.covered!

class CursorPaginationControllerTest < ActionController::TestCase
  class ApiV2Controller < Api::V2::BaseController
  end

  tests ApiV2Controller

  before do
    @account = @request.account = accounts(:minimum)
  end

  describe 'with_cursor_pagination?' do
    it 'should return true if params include cursor pagination v1 stuff' do
      params = { limit: 10 }
      @controller.stubs(params: params)
      assert(@controller.send(:with_cursor_pagination?))

      params = { cursor: "abc" }
      @controller.stubs(params: params)
      assert(@controller.send(:with_cursor_pagination?))

      params = { limit: 10, per_page: 10 }
      @controller.stubs(params: params)
      assert(@controller.send(:with_cursor_pagination?))

      params = { cursor: "abc", page: 10 }
      @controller.stubs(params: params)
      assert(@controller.send(:with_cursor_pagination?))
    end

    it 'should return false if params dont include cursor pagination params' do
      params = {}
      @controller.stubs(params: params)
      assert_equal false, @controller.send(:with_cursor_pagination?)

      params = { a: 1 }
      @controller.stubs(params: params)
      assert_equal false, @controller.send(:with_cursor_pagination?)

      params = { per_page: 1 }
      @controller.stubs(params: params)
      assert_equal false, @controller.send(:with_cursor_pagination?)
    end
  end

  describe 'with_cursor_pagination_v2?' do
    it 'returns true if params include cursor pagination v2 stuff' do
      params = { page: { size: 10 } }
      @controller.stubs(params: params)
      assert @controller.send(:with_cursor_pagination?)
      assert @controller.send(:with_cursor_pagination_v2?)
      refute @controller.send(:with_cursor_pagination_v1?)

      params = { page: { after: "abc" } }
      @controller.stubs(params: params)
      assert @controller.send(:with_cursor_pagination?)
      assert @controller.send(:with_cursor_pagination_v2?)
      refute @controller.send(:with_cursor_pagination_v1?)

      params = { page: { before: "abc" } }
      @controller.stubs(params: params)
      assert @controller.send(:with_cursor_pagination?)
      assert @controller.send(:with_cursor_pagination_v2?)
      refute @controller.send(:with_cursor_pagination_v1?)

      params = { page: { size: 10, after: "abc" } }
      @controller.stubs(params: params)
      assert @controller.send(:with_cursor_pagination?)
      assert @controller.send(:with_cursor_pagination_v2?)
      refute @controller.send(:with_cursor_pagination_v1?)
    end

    it 'return false if params include cursor pagination v1 stuff' do
      params = { limit: 10 }
      @controller.stubs(params: params)
      refute @controller.send(:with_cursor_pagination_v2?)
      assert @controller.send(:with_cursor_pagination_v1?)

      params = { cursor: "abc" }
      @controller.stubs(params: params)
      refute @controller.send(:with_cursor_pagination_v2?)
      assert @controller.send(:with_cursor_pagination_v1?)
    end

    it 'returns false if params dont include cursor pagination stuff' do
      params = {}
      @controller.stubs(params: params)
      refute @controller.send(:with_cursor_pagination?)
      refute @controller.send(:with_cursor_pagination_v1?)
      refute @controller.send(:with_cursor_pagination_v2?)

      params = { a: 1 }
      @controller.stubs(params: params)
      refute @controller.send(:with_cursor_pagination?)
      refute @controller.send(:with_cursor_pagination_v1?)
      refute @controller.send(:with_cursor_pagination_v2?)

      params = { per_page: 1 }
      @controller.stubs(params: params)
      refute @controller.send(:with_cursor_pagination?)
      refute @controller.send(:with_cursor_pagination_v1?)
      refute @controller.send(:with_cursor_pagination_v2?)

      params = { page: {} }
      @controller.stubs(params: params)
      refute @controller.send(:with_cursor_pagination?)
      refute @controller.send(:with_cursor_pagination_v1?)
      refute @controller.send(:with_cursor_pagination_v2?)
    end
  end

  describe 'paginate_with_cursor' do
    let(:scope) { @account.tickets.reorder(nice_id: :asc) }
    let(:params) { { page: { size: 3 } } }

    it 'calls scope#paginate_with_cursor with the correct parameters' do
      @controller.stubs(params: params)
      scope.expects(:paginate_with_cursor).with(
        cursor_pagination_version: 2,
        page: { size: 3 },
        max_size: Zendesk::CursorPagination::ControllerSupport::CURSOR_PAGINATION_MAX_SIZE
      )
      @controller.send(:paginate_with_cursor, scope)
    end
  end

  describe 'sorted_scope_for_cursor' do
    let(:scope) { @account.tickets.reorder(nice_id: :asc) }
    let(:sortable_fields) do
      {
        'created_at' => :created_at,
        'id' => :nice_id,
        'updated_at' => :updated_at
      }
    end
    let(:params) { { sort: '-created_at' } }

    before { @controller.stubs(params: params) }

    it 'calls order_hash_from_sort with the correct parameters' do
      @controller.expects(:order_hash_for_cursor).with(
        sortable_fields,
        multiple_levels_allowed: true
      )

      @controller.send(
        :sorted_scope_for_cursor,
        scope,
        sortable_fields,
        multiple_levels_allowed: true
      )
    end

    it 'calls order_hash_from_sort with the default parameters' do
      @controller.expects(:order_hash_for_cursor).with(
        Zendesk::CursorPagination::ControllerSupport::DEFAULT_SORTABLE_FIELDS,
        multiple_levels_allowed: false
      )

      @controller.send(:sorted_scope_for_cursor, scope)
    end

    describe 'when there is a sort param' do
      it 'reorders the scope' do
        sorted_scope = @controller.send(
          :sorted_scope_for_cursor,
          scope,
          sortable_fields,
          multiple_levels_allowed: true
        )

        assert_equal scope.reorder(created_at: :desc), sorted_scope
      end
    end

    describe 'when there is no sort param' do
      let(:params) { {} }

      it 'does not reorder the scope' do
        sorted_scope = @controller.send(
          :sorted_scope_for_cursor,
          scope,
          sortable_fields,
          multiple_levels_allowed: true
        )

        assert_equal scope, sorted_scope
      end
    end
  end

  describe 'order_hash_for_cursor' do
    let(:scope) { @account.tickets.reorder(nice_id: :asc) }
    let(:sortable_fields) do
      {
        'created_at' => :created_at,
        'id' => :nice_id,
        'updated_at' => :updated_at
      }
    end

    before { @controller.stubs(params: params) }

    describe 'when there is no sort param' do
      let(:params) { { hello: 1 } }

      it 'returns the correct hash' do
        expected = {}
        assert_equal expected, @controller.send(:order_hash_for_cursor, sortable_fields)
      end
    end

    describe 'when sort param has one of the sortable fields' do
      let(:params) { { sort: 'created_at' } }

      it 'returns the correct hash' do
        expected = { created_at: :asc }
        assert_equal expected, @controller.send(:order_hash_for_cursor, sortable_fields)
      end
    end

    describe 'when sort param has one of the sortable fields in desc mode' do
      let(:params) { { sort: '-created_at' } }

      it 'returns the correct hash' do
        expected = { created_at: :desc }
        assert_equal expected, @controller.send(:order_hash_for_cursor, sortable_fields)
      end
    end

    describe 'when sort param has a field that is not sortable' do
      let(:params) { { sort: '-hello' } }

      it 'raises an exception' do
        err = assert_raises Zendesk::CursorPagination::InvalidPaginationParameter do
          @controller.send(:order_hash_for_cursor, sortable_fields)
        end
        assert_equal "sort is not valid", err.message
      end
    end

    describe 'when sort param is empty' do
      let(:params) { { sort: '' } }

      it 'returns the correct hash' do
        expected = {}
        assert_equal expected, @controller.send(:order_hash_for_cursor, sortable_fields)
      end
    end

    describe 'when sort param has multiple fields' do
      let(:params) { { sort: '-created_at,id' } }

      it 'returns the correct hash when multiple_levels_allowed' do
        expected = { created_at: :desc, nice_id: :asc }
        order_hash = @controller.send(
          :order_hash_for_cursor,
          sortable_fields,
          multiple_levels_allowed: true
        )
        assert_equal expected, order_hash
      end

      it 'raises an exception when no multiple_levels_allowed' do
        err = assert_raises Zendesk::CursorPagination::InvalidPaginationParameter do
          @controller.send(:order_hash_for_cursor, sortable_fields)
        end
        assert_equal "sort is not valid", err.message
      end
    end

    describe 'when sort param has multiple fields and some of them are invalid' do
      let(:params) { { sort: '-created_at,hello' } }

      it 'raises an exception' do
        err = assert_raises Zendesk::CursorPagination::InvalidPaginationParameter do
          @controller.send(:order_hash_for_cursor, sortable_fields)
        end
        assert_equal "sort is not valid", err.message
      end
    end
  end
end
