require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Coders::JSONWithStringFallback do
  let(:subject) { Zendesk::Coders::JSONWithStringFallback }
  describe '.load' do
    describe 'with String' do
      it 'returns the string' do
        subject.load("message").must_equal "message"
      end
    end

    describe 'with JSON' do
      it 'decodes the JSON' do
        subject.load('{"message": "received"}').must_equal("message" => "received")
      end
    end

    describe 'when blank' do
      it 'returns nil' do
        subject.load("").must_be_nil
      end
    end
  end

  describe '.dump' do
    it "encodes to JSON" do
      subject.dump(message: "received").must_equal '{"message":"received"}'
    end
  end
end
