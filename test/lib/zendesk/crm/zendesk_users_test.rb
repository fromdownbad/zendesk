require_relative "../../../support/test_helper"
require 'zendesk/crm/zendesk_users'

SingleCov.covered!

describe Zendesk::Crm::ZendeskUsers do
  fixtures :accounts, :organizations, :users, :organization_memberships

  describe "#update_existing_or_initialize_new_user" do
    before do
      @user = users(:minimum_agent)

      @account = @user.account
    end

    describe "when a user doesn't match the params" do
      before do
        @payload = {
          name: 'New person',
          email: 'new.person.email@zendesk.com'
        }
      end

      it "creates a user" do
        resulting_user, created_new_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)

        assert_equal @payload[:name], resulting_user.name
        assert_equal @payload[:email], resulting_user.email

        assert created_new_user
      end

      describe "and an organisation_id param" do
        before do
          @org_id = organizations(:minimum_organization1).id
          @payload[:organization_id] = @org_id
        end

        describe "with multi-org disabled" do
          before do
            @account.stubs(has_multiple_organizations_enabled?: false)
          end

          it "creates the user with org_id and org_membership" do
            resulting_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)

            assert_equal 1, resulting_user.organization_memberships.size
            assert_equal @org_id, resulting_user.organization_memberships.map(&:organization_id).first
            assert_equal @org_id, resulting_user.organization_id
          end
        end

        describe "with multi-org enabled" do
          before do
            @account.stubs(has_multiple_organizations_enabled?: true)
          end

          it "creates the user with org_id and org_membership" do
            resulting_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)

            assert_equal 1, resulting_user.organization_memberships.size
            assert_equal @org_id, resulting_user.organization_memberships.map(&:organization_id).first
            assert_equal @org_id, resulting_user.organization_id
          end
        end
      end
    end

    it "updates an existing user" do
      payload = {
        name: 'Totally new name',
        email: @user.email
      }

      resulting_user, created_new_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, payload)

      assert_equal payload[:name], resulting_user.name
      refute created_new_user
    end

    describe "for an existing user with multiple organization enabled" do
      describe "with a payload with an organization id" do
        before do
          @other_organization = organizations(:minimum_organization2)
          @payload = {
            email: @user.email,
            organization_id: @other_organization.id
          }
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
        end

        describe "when the user has an existing but different organization membership" do
          before do
            @user.organization_id = organizations(:minimum_organization3).id
            @user.organization_memberships.create!(organization: organizations(:minimum_organization3))
            @user.organization_memberships.create!(organization: organizations(:minimum_organization1))
            @user.save!
          end

          it "creates the new membership but doesn't update the user" do
            existing_name = @user.name

            @payload[:name] = "New name"

            resulting_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)

            assert_equal @user.organization_id, resulting_user.organization_id
            assert_includes resulting_user.organization_ids, @other_organization.id
            assert_equal existing_name, @user.name
          end

          it 'retains other organization memberships' do
            resulting_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)
            assert_includes resulting_user.organization_ids, organizations(:minimum_organization3).id
            assert_includes resulting_user.organization_ids, organizations(:minimum_organization1).id
            assert_includes resulting_user.organization_ids, @other_organization.id
          end
        end

        describe "when the user doesn't have an existing organization membership" do
          it "creates the membership and updates the user" do
            @payload[:name] = "New name"

            resulting_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)

            assert_equal @other_organization.id, resulting_user.organization_id
            assert_equal @payload[:name], resulting_user.name
          end
        end
      end

      describe "for an existing user with multiple organization disabled" do
        describe "with a payload with an organization id" do
          before do
            @other_organization = organizations(:minimum_organization2)
            @payload = {
              email: @user.email,
              organization_id: @other_organization.id
            }
            Account.any_instance.stubs(has_multiple_organizations_enabled?: false)
          end

          describe "when the user belongs to different organization" do
            before do
              @organization = organizations(:minimum_organization3)
              @user.organization_id = @organization.id
              @user.save!
              @organization_membership = @user.organization_memberships.create!(organization: @organization)
            end

            it "update user org id and update membership" do
              @payload[:name] = "New name"

              resulting_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)

              assert_equal @other_organization.id, resulting_user.organization_id
              assert_equal 1, @user.reload.organization_memberships.size
              assert_equal @payload[:name], resulting_user.name
              assert_equal @other_organization.id, @organization_membership.reload.organization_id
            end
          end

          describe "when the user does not have an organization membership" do
            before do
              @user.organization_id = @other_organization.id
              @user.save!
              @user.organization_memberships.destroy_all
            end

            it "creates a new organization membership" do
              resulting_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)

              assert_equal @other_organization.id, resulting_user.organization_id
            end
          end
        end
      end

      describe "with a payload without an organization id" do
        before do
          @payload = {
            email: @user.email,
            name: "New name"
          }
        end

        describe "when the user has an existing organization membership" do
          it "updates the user" do
            resulting_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)

            assert_equal @payload[:name], resulting_user.name
          end
        end

        describe "when the user doesn't have an existing organization membership" do
          before do
            @user.organization_id = nil
            @user.save!
          end

          it "does not update the user" do
            existing_name = @user.name

            resulting_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(@account, @user, @payload)

            assert_equal existing_name, resulting_user.name
          end
        end
      end
    end
  end
end
