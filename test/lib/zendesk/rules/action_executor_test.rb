require_relative '../../../support/test_helper'
require_relative '../../../support/rule'
require_relative '../../../support/rules_test_helper'
require_relative '../../../support/arturo_test_helper'
require_relative '../../../support/collaboration_settings_test_helper'

require 'preload'

SingleCov.covered!

describe 'Zendesk::Rules::ActionExecutor' do
  include CustomFieldsTestHelper
  include RulesTestHelper
  include ArturoTestHelper
  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:rule)    { create_trigger(definition: definition) }

  let(:definition) do
    build_trigger_definition.tap do |definition|
      definition.conditions_all.push(
        DefinitionItem.new('current_tags', 'includes', 'do_not_run')
      )
    end
  end

  let(:via_reference) { {via_id: ViaType.RULE, via_reference_id: rule.id} }

  before { rule.stubs(via_reference: via_reference) }

  resque_inline false

  describe '.execute' do
    let(:comment)              { nil }
    let(:current_user)         { users(:minimum_agent) }
    let(:has_rule_usage_stats) { true }
    let(:ticket)               { tickets(:minimum_1) }
    let(:ticket_join)          { ticket.rule_ticket_joins.last }
    let(:via_id)               { ViaType.WEB_FORM }

    before do
      ticket.account.stubs(has_rule_usage_stats?: has_rule_usage_stats)

      ticket.stubs(via_id: via_id, current_user: current_user, comment: comment)

      Zendesk::Rules::ActionExecutor.execute(rule, ticket, [])
    end

    it 'records the rule execution' do
      assert_equal(
        {
          user_id:   current_user.id,
          rule_id:   rule.id,
          rule_type: rule.rule_type
        },
        ticket.last_executed_rules.last
      )
    end

    describe 'and the account has `rule_usage_stats`' do
      let(:has_rule_usage_stats) { true }

      it 'associates a rule with the ticket' do
        assert_equal rule.id, ticket_join.rule_id
      end
    end

    describe 'and the account does not have `rule_usage_stats`' do
      let(:has_rule_usage_stats) { false }

      it 'does not build a ticket join object' do
        assert_nil ticket_join
      end
    end

    describe 'and the ticket is via SMS' do
      let(:author) { users(:minimum_end_user) }
      let(:via_id) { ViaType.SMS }

      describe 'and there is a comment' do
        let(:comment) { Comment.new(ticket: ticket, author: author) }

        it 'attributes the audit to the comment author' do
          assert_equal author, ticket.audit.author
        end
      end

      describe 'and there is not a comment' do
        let(:comment) { nil }

        it 'does not attribute the audit to the comment author' do
          assert_not_equal author, ticket.audit.author
        end
      end
    end

    describe 'and the ticket is not via SMS' do
      it 'attributes the audit to the current user' do
        assert_equal current_user, ticket.audit.author
      end

      describe 'and there is not a current user' do
        let(:current_user) { nil }

        it 'attributes the audit to the system user' do
          assert_equal User.system, ticket.audit.author
        end
      end
    end
  end

  it 'apply_action should perform share_ticket_action' do
    action = OpenStruct.new(source: 'share_ticket#')
    ticket = tickets(:minimum_1)

    base = build(rule, ticket, action)
    base.expects(:apply_share_ticket_action)
    base.apply
  end

  it 'apply_action should not perform default action when ticket type is unchanged' do
    action = OpenStruct.new(source: 'ticket_type_id', value: ['4'])
    ticket = tickets(:minimum_1)
    ticket.ticket_type_id = 4

    base = build(Trigger.new, ticket, action)
    base.expects(:apply_default_action).never
    base.apply
  end

  it 'share_ticket_action should create a ticket sharing event' do
    ticket = tickets(:minimum_1)
    user = users(:minimum_agent)
    agreement = FactoryBot.create(
      :agreement,
      account: ticket.account,
      direction: :out, status: :accepted
    )

    ticket.will_be_saved_by(user)
    base = build(rule, ticket, DefinitionItem.new('', '', [agreement.id.to_s]))
    base.send(:apply_share_ticket_action)
    event = ticket.audit.events.last

    assert_equal(agreement.id, event.agreement_id)
    assert_equal(ViaType.RULE, event.via_id)
    assert_equal(rule.id, event.via_reference_id)
  end

  it 'share_ticket_action should not create a ticket sharing event for sample tickets' do
    account = accounts(:minimum)
    user = users(:minimum_agent)
    ticket = Ticket.create_test_ticket!(account, user)
    agreement = FactoryBot.create(
      :agreement,
      account: account,
      direction: :out, status: :accepted
    )

    ticket.will_be_saved_by(user)
    base = build(rule, ticket, DefinitionItem.new('', '', [agreement.id.to_s]))
    base.send(:apply_share_ticket_action)

    refute ticket.valid?
    assert ticket.errors.full_messages.include?("Sample tickets can't be shared")
  end

  it 'share_ticket_action should abort if a valid agreement is not present' do
    ticket = tickets(:minimum_1)
    user = users(:minimum_agent)
    agreement = FactoryBot.create(
      :agreement,
      account: ticket.account,
      direction: :out, status: :pending
    )

    ticket.will_be_saved_by(user)
    base = build(rule, ticket, DefinitionItem.new('', '', agreement.id))
    base.send(:apply_share_ticket_action)

    assert_equal(0, ticket.audit.events.size)
  end

  it 'share_ticket_action should abort if a shared ticket already exists' do
    ticket = tickets(:minimum_1)
    user = users(:minimum_agent)
    agreement = FactoryBot.create(
      :agreement,
      account:   ticket.account,
      direction: :out,
      status:    :accepted
    )

    SharedTicket.create!(ticket: ticket, agreement: agreement)
    ticket.reload

    assert ticket.shared_tickets.any? { |st| st.agreement == agreement }

    ticket.will_be_saved_by(user)

    base = build(rule, ticket, DefinitionItem.new('', '', [agreement.id.to_s]))
    base.send(:apply_share_ticket_action)
    assert_equal(0, ticket.audit.events.size)
  end

  it 'apply locale action should be called in apply default action for locale_id' do
    ticket = tickets(:minimum_1)
    ticket.current_user = users(:minimum_agent)

    base = build(
      rule,
      ticket,
      DefinitionItem.new('locale_id#locale_id', 'is', ['2'])
    )

    base.expects(:apply_locale_action).once
    base.stubs(:title).returns('hello')
    base.send(:apply_default_action)
  end

  it 'when changing the ticket_type_id from TASK to something else' do
    Account.any_instance.expects(:has_extended_ticket_types?).returns(true)
    ticket = tickets(:minimum_1)

    # make a trigger that changes ticket type
    definition = Definition.new

    definition.
      actions.
      push(DefinitionItem.new('ticket_type_id', 'is', [TicketType.QUESTION]))

    definition.
      conditions_all.
      push(DefinitionItem.new('status_id', 'is_not', [StatusType.SOLVED]))

    Trigger.create!(
      account:    accounts(:minimum),
      title:      'foo',
      definition: definition,
      owner:      accounts(:minimum)
    )

    # ticket is a task with due date, the trigger should fire
    # and change the ticket type
    ticket.ticket_type_id = TicketType.TASK
    ticket.due_date = Time.now.utc
    ticket.status_id = StatusType.OPEN

    ticket.will_be_saved_by ticket.account.owner
    ticket.save!

    # make sure ticket type changed and we have no due date now.
    assert_equal TicketType.QUESTION, ticket.reload.ticket_type_id
    assert_nil ticket.due_date
  end

  describe 'when applying a macro action' do
    let(:account)     { accounts(:minimum) }
    let(:audit_event) { ticket.audit.events.last }
    let(:locale)      { translation_locales(:english_by_zendesk) }
    let(:rich_ticket) { false }
    let(:requester)   { users(:minimum_end_user) }
    let(:ticket)      { tickets(:minimum_1) }

    let(:rule_with_macro_reference) do
      create_automation(
        definition: Definition.new.tap do |definition|
          definition.conditions_all.push(
            DefinitionItem.new('status_id', 'less_than', [StatusType.CLOSED]),
            DefinitionItem.new('SOLVED', 'is', ['24'])
          )
          definition.actions.push(
            DefinitionItem.new('macro_id_after', nil, macro.id)
          )
        end
      )
    end

    let(:macro) do
      create_macro(
        definition: Definition.new.tap do |definition|
          actions.each { |action| definition.actions.push(action) }
        end
      )
    end

    let(:actions) do
      [
        DefinitionItem.new(
          'priority_id',
          nil,
          Zendesk::Types::PriorityType.HIGH
        )
      ]
    end

    before do
      Arturo.enable_feature!(:rules_can_reference_macros)

      ticket.stubs(rich?: rich_ticket)

      requester.translation_locale = locale

      ticket.requester = requester
      ticket.account   = account

      ticket.will_be_saved_by(requester)

      build(
        rule_with_macro_reference,
        ticket,
        rule_with_macro_reference.definition.actions.first
      ).apply
    end

    it 'executes non-comment actions' do
      assert_equal Zendesk::Types::PriorityType.HIGH, ticket.priority_id
    end

    it 'creates a macro reference audit event' do
      assert_equal 'MacroReference', audit_event.type
    end

    it 'records the `via_id`' do
      assert_equal(
        rule_with_macro_reference.via_reference[:via_id],
        audit_event.via_id
      )
    end

    it 'records the `via_reference_id`' do
      assert_equal(
        rule_with_macro_reference.via_reference[:via_reference_id],
        audit_event.via_reference_id
      )
    end

    describe 'and the macro has a comment action' do
      before do
        Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)
        ticket.stubs(:run_triggers_on_audit).returns(true)
        ticket.save
      end

      describe 'and the ticket is rich' do
        let(:rich_ticket) { true }

        describe 'and there are both types of comment action' do
          let(:actions) do
            [
              DefinitionItem.new('comment_value', nil, 'basic comment'),
              DefinitionItem.new('comment_value_html', nil, 'rich comment')
            ]
          end

          it 'applies the rich comment action' do
            assert_equal 'rich comment', ticket.comments.last.body
          end
        end

        describe 'and there is a basic comment action' do
          let(:actions) do
            [DefinitionItem.new('comment_value', nil, 'basic comment')]
          end

          it 'applies the basic comment action' do
            assert_equal 'basic comment', ticket.comments.last.body
          end
        end

        describe 'and there is a rich comment action' do
          let(:actions) do
            [DefinitionItem.new('comment_value_html', nil, 'rich comment')]
          end

          it 'applies the rich comment action' do
            assert_equal 'rich comment', ticket.comments.last.body
          end

          describe 'and the rich comment contains a link' do
            let(:actions) do
              [
                DefinitionItem.new(
                  'comment_value_html',
                  nil,
                  '<p>Link: https://google.com</p>'
                )
              ]
            end

            it 'renders the link properly' do
              assert_equal(
                %(<div class="zd-comment" dir="auto">) +
                  %(<p>Link: <a href="https://google.com" rel="noreferrer">) +
                  %(https://google.com</a></p></div>),
                ticket.comments.last.html_body
              )
            end
          end
        end
      end

      describe 'and the ticket is not rich' do
        let(:rich_ticket) { false }

        describe 'and there are both types of comment action' do
          let(:actions) do
            [
              DefinitionItem.new('comment_value', nil, 'basic comment'),
              DefinitionItem.new('comment_value_html', nil, 'rich comment')
            ]
          end

          it 'applies the basic comment action' do
            assert_equal 'basic comment', ticket.comments.last.body
          end
        end

        describe 'and there is a basic comment action' do
          let(:actions) do
            [DefinitionItem.new('comment_value', nil, 'basic comment')]
          end

          it 'applies the basic comment action' do
            assert_equal 'basic comment', ticket.comments.last.body
          end
        end

        describe 'and there is a rich comment action' do
          let(:actions) do
            [DefinitionItem.new('comment_value_html', nil, 'rich comment')]
          end

          it 'does not apply a comment action' do
            assert_not_equal 'rich comment', ticket.comments.last.body
          end
        end
      end

      describe 'and comment has a liquid in a non-English language' do
        let(:locale) { translation_locales(:spanish) }

        let(:actions) do
          [
            DefinitionItem.new(
              'comment_value',
              nil,
              'Este comentario es sobre el ticket de tipo ' \
                '{{ticket.ticket_type}}'
            )
          ]
        end

        it 'renders the comment in the proper language' do
          assert_equal(
            'Este comentario es sobre el ticket de tipo Incidente',
            ticket.comments.last.body
          )
        end
      end

      describe 'and there is an existing comment' do
        let(:action) do
          DefinitionItem.new('comment_value', nil, 'private comment')
        end

        before do
          ticket.add_comment(
            is_public:        false,
            author_id:        requester.id,
            via_id:           ViaType.RULE,
            via_reference_id: rule.id
          )

          ticket.set_comment

          build(rule, ticket, action).apply
        end

        it 'does not the override the privacy of the comment' do
          refute ticket.comment.is_public?
        end
      end
    end

    describe 'and the rule is a trigger' do
      let(:rule_with_macro_reference) do
        create_trigger(
          definition: Definition.new.tap do |definition|
            definition.conditions_all.push(
              DefinitionItem.new('status_id', 'less_than', [StatusType.CLOSED])
            )
            definition.actions.push(
              DefinitionItem.new('macro_id_after', nil, macro.id)
            )
          end
        )
      end

      before do
        ticket.macros_to_be_applied_after_save = macros

        build(
          rule_with_macro_reference,
          ticket,
          rule_with_macro_reference.definition.actions.first
        ).apply
      end

      describe 'and `ticket.macros_to_be_applied_after_save` is present' do
        let(:macros) { [123] }

        it 'adds the macro ID to `ticket.macros_to_be_applied_after_save`' do
          assert_includes ticket.macros_to_be_applied_after_save, macro.id
        end
      end

      describe 'and `ticket.macros_to_be_applied_after_save` is not present' do
        let(:macros) { nil }

        it 'adds the macro ID to `ticket.macros_to_be_applied_after_save`' do
          assert_includes ticket.macros_to_be_applied_after_save, macro.id
        end
      end
    end
  end

  describe '#apply_deflection_action' do
    it 'sets deflection rule id on ticket' do
      action = OpenStruct.new(source: 'deflection')
      ticket = tickets(:minimum_1)

      base = build(rule, ticket, action)
      base.apply

      assert_equal ticket.instance_variable_get(:@deflection_rule_id), rule.id
    end
  end

  describe 'tweet_requester' do
    before do
      @action = OpenStruct.new(source: 'tweet_requester', value: 'Tweetaholic')
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.update_column(:requester_id, users(:minimum_end_user).id)
      @ticket.
        stubs(:monitored_twitter_handle).
        returns(monitored_twitter_handles(:minimum_monitored_twitter_handle_1))

      @ticket.requester.identities.twitter.first.update_column(
        :value,
        channels_user_profiles(:minimum_twitter_user_profile_1).external_id
      )

      @ticket.comment = Comment.new(ticket: @ticket)
      @ticket.comment.channel_source_id = 123

      @data = {
        metadata: {
          external_id: 'external_id',
          source: {
            external_id: '106686396330930',
            name:        'Twitter handle'
          },
          text: 'Outgoing text'
        },
        source_id: monitored_twitter_handles(
          :minimum_monitored_twitter_handle_1
        ).id,
        via_id: @ticket.via_id
      }
    end

    describe '#apply_twitter_action' do
      before do
        @action = OpenStruct.new(
          source: 'tweet_requester',
          value:  'Tweetaholic'
        )

        @base = build(Trigger.new, @ticket, @action)
      end

      it 'is called when source is tweet_requester' do
        @base.expects(:apply_twitter_action).once
        @base.apply
      end

      it 'does not create a twitter event when requester has no identity' do
        ::Channels::Converter::ReplyParametersValidator.
          any_instance.
          expects(:validate).
          never

        @ticket.requester.identities.twitter.destroy_all
        refute_difference('ChannelBackEvent.count(:all)') do
          refute_difference('TwitterDmEvent.count(:all)') do
            refute_difference('Tweet.count(:all)') do
              @base.send(:apply_twitter_action)
              @ticket.save!
            end
          end
        end
      end

      describe 'when the user has a twitter identity' do
        before do
          @ticket.update_column(:requester_id, users(:minimum_end_user).id)
        end

        describe 'when called too many times' do
          it 'raises Prop::RateLimited exception' do
            # We want Prop.throttle! to be in the state where it'll throttle
            # on the next call. We'll set the allowed number of calls to 1,
            # then we'll call it once. The next call will be throttled.
            Prop.configure(:twitter_action, threshold: 1, interval: 1.minute)
            Prop.throttle!(:twitter_action, @ticket.account_id)

            assert_raises(Prop::RateLimited) do
              @base.send(:apply_twitter_action)
              @ticket.save!
            end
          end
        end

        describe 'when channel_source_id is not known' do
          it 'does not create a twitter event' do
            refute_difference('ChannelBackEvent.count(:all)') do
              refute_difference('TwitterDmEvent.count(:all)') do
                refute_difference('Tweet.count(:all)') do
                  @ticket.comment.channel_source_id = nil
                  @base.send(:apply_twitter_action)
                  @ticket.save!
                end
              end
            end
          end
        end

        describe 'when channel_source_id is known' do
          describe 'when the ticket is new' do
            it 'creates a channel back event' do
              # Stubbing the reply initiator will stop the channels code from
              # sending a request to twitter to perform a reply.
              ::Channels::Converter::ReplyParametersValidator.
                any_instance.
                expects(:validate).
                returns(@data)

              ::Channels::Converter::ReplyInitiator.any_instance.expects(:reply)

              assert_difference('ChannelBackEvent.count(:all)', 1) do
                @ticket.stubs(:new_record?).returns(true)
                @base.send(:apply_twitter_action)
                @ticket.unstub(:new_record?)
                @ticket.save!
              end
            end
          end

          describe 'when the ticket is not new' do
            it 'does not create a twitter event' do
              refute_difference('ChannelBackEvent.count(:all)') do
                refute_difference('TwitterDmEvent.count(:all)') do
                  refute_difference('Tweet.count(:all)') do
                    @ticket.comment = Comment.new(ticket: @ticket)
                    @ticket.comment.channel_source_id =
                      monitored_twitter_handles(
                        :minimum_monitored_twitter_handle_1
                      ).id
                    @base.send(:apply_twitter_action)
                    @ticket.save!
                  end
                end
              end
            end
          end
        end
      end
    end

    describe 'when the ticket is new' do
      it 'tests tweet requester' do
        @data[:metadata][:text] = '@john_doe hello there'

        ::Channels::Converter::ReplyParametersValidator.
          any_instance.
          expects(:validate).
          returns(@data)

        @ticket.stubs(:new_record?).returns(true)

        new_rule(DefinitionItem.new('tweet_requester', nil, ['hello there'])).
          apply_actions(@ticket)

        assert_equal(
          '@john_doe hello there',
          @ticket.audit.events.last.value[:text]
        )

        assert @ticket.audit.events.last.is_a?(ChannelBackEvent)
      end

      it 'tests dm tweet requester' do
        @data[:metadata][:text] = 'hello there'

        ::Channels::Converter::ReplyParametersValidator.
          any_instance.
          expects(:validate).
          returns(@data)

        @ticket.update_column(:via_id, ViaType.TWITTER_DM)
        @ticket.stubs(:new_record?).returns(true)

        new_rule(DefinitionItem.new('tweet_requester', nil, ['hello there'])).
          apply_actions(@ticket)

        assert_equal 'hello there', @ticket.audit.events.last.value[:text]
        assert @ticket.audit.events.last.is_a?(ChannelBackEvent)

        # Action should send a Tweet, should NEVER send a DM
        ::Channels::TwitterApi::Client.any_instance.expects(:tweet)
        ::Channels::TwitterApi::Client.
          any_instance.
          expects(:direct_message).
          never

        @ticket.unstub(:new_record?)
        @ticket.save!
      end
    end
  end

  describe '#apply_set_schedule_action' do
    let(:author)  { users(:minimum_admin) }
    let(:ticket)  { tickets(:minimum_1) }
    let(:account) { ticket.account }

    let(:schedule_1) do
      account.schedules.create(name: 'schedule_1', time_zone: account.time_zone)
    end
    let(:schedule_2) do
      account.schedules.create(name: 'schedule_2', time_zone: account.time_zone)
    end

    let(:set_schedule) { schedule_2 }

    let(:action) do
      DefinitionItem.new('set_schedule', nil, set_schedule.id.to_s)
    end

    let(:apply_action) { build(rule, ticket, action).apply }

    before { ticket.will_be_saved_by(author) }

    describe 'when the `multiple schedules` feature is enabled' do
      before { account.stubs(:has_multiple_schedules?).returns(true) }

      describe 'and the schedule is already set' do
        before do
          ticket.create_ticket_schedule(
            account:  account,
            schedule: schedule_1
          )
        end

        describe 'and the set schedule changes' do
          before do
            apply_action

            ticket.tap(&:save!).reload
          end

          it 'sets the schedule' do
            assert_equal schedule_2, ticket.schedule
          end

          describe 'the audit event' do
            let(:audit_event) do
              ticket.events.where(type: 'ScheduleAssignment').first
            end

            it 'records the previous schedule' do
              assert_equal schedule_1, audit_event.previous_schedule
            end

            it 'records the new schedule' do
              assert_equal schedule_2, audit_event.new_schedule
            end

            it "records the 'via_id'" do
              assert_equal ViaType.RULE, audit_event.via_id
            end

            it "records the 'via_reference_id'" do
              assert_equal rule.id, audit_event.via_reference_id
            end

            describe 'when set using rule revision' do
              let(:via_reference) do
                {via_id: ViaType.RULE_REVISION, via_reference_id: 1}
              end

              it "records the 'via_id'" do
                assert_equal ViaType.RULE_REVISION, audit_event.via_id
              end

              it "records the 'via_reference_id'" do
                assert_equal 1, audit_event.via_reference_id
              end
            end
          end
        end

        describe 'and the set schedule stays the same' do
          let!(:existing_ticket_schedule) { ticket.ticket_schedule }

          let(:set_schedule) { schedule_1 }

          before do
            apply_action

            ticket.tap(&:save!).reload
          end

          should_not_change 'audit events' do
            ticket.events.where(type: 'ScheduleAssignment').count(:all)
          end

          it 'does not reassign the schedule' do
            assert_equal existing_ticket_schedule, ticket.ticket_schedule
          end
        end
      end

      describe 'and the schedule is not set' do
        before do
          apply_action

          ticket.tap(&:save!).reload
        end

        it 'sets the schedule' do
          assert_equal schedule_2, ticket.schedule
        end

        describe 'the audit event' do
          let(:audit_event) do
            ticket.events.where(type: 'ScheduleAssignment').first
          end

          it 'records the previous schedule' do
            assert_nil audit_event.previous_schedule
          end

          it 'records the new schedule' do
            assert_equal schedule_2, audit_event.new_schedule
          end
        end
      end

      describe 'and the schedule does not exist' do
        before do
          schedule_2.destroy

          apply_action

          ticket.tap(&:save!).reload
        end

        it 'does not set the schedule' do
          assert TicketSchedule.where(ticket_id: ticket.id).none?
        end

        it 'does not create an audit event' do
          assert ticket.events.where(type: 'ScheduleAssignment').none?
        end
      end

      describe 'and the schedule has been soft deleted' do
        before do
          schedule_2.soft_delete

          apply_action

          ticket.tap(&:save!).reload
        end

        it 'does not set the schedule' do
          assert TicketSchedule.where(ticket_id: ticket.id).none?
        end

        it 'does not create an audit event' do
          assert ticket.events.where(type: 'ScheduleAssignment').none?
        end
      end
    end

    describe 'when the `multiple schedules` feature is not enabled' do
      before do
        Arturo.disable_feature!(:multiple_schedules)

        apply_action

        ticket.tap(&:save!).reload
      end

      it 'does not set the schedule' do
        assert TicketSchedule.where(ticket_id: ticket.id).none?
      end

      it 'does not create an audit event' do
        assert ticket.events.where(type: 'ScheduleAssignment').none?
      end
    end
  end

  describe '#apply' do
    let(:author)        { users(:minimum_admin) }
    let(:ticket)        { tickets(:minimum_1) }
    let(:audit_options) { {} }

    let(:apply_action) { build(rule, ticket, action).apply }

    before { ticket.will_be_saved_by(author, audit_options) }

    describe 'when the action source is `assignee_id`' do
      let(:requester)    { users(:minimum_end_user) }
      let(:assignee)     { users(:minimum_agent) }
      let(:action_value) { assignee.id }

      let!(:initial_assignee_id) { ticket.assignee_id }

      let(:action) do
        DefinitionItem.new('assignee_id', nil, action_value)
      end

      before do
        ticket.requester = requester

        apply_action
      end

      it 'sets the assignee' do
        assert_equal assignee.id, ticket.assignee_id
      end

      describe 'and the assignee is invalid' do
        let(:user) { users(:minimum_end_user) }

        it 'does not update the assignee' do
          assert_equal initial_assignee_id, ticket.assignee_id
        end
      end

      describe 'and the action value is `requester_id`' do
        let(:requester)    { users(:minimum_admin) }
        let(:action_value) { 'requester_id' }

        it 'sets the requester as the assignee' do
          assert_equal ticket.requester_id, ticket.assignee_id
        end
      end
    end

    describe 'when the action source is `group_id`' do
      let(:current_user) { users(:with_groups_agent1) }
      let(:ticket)       { tickets(:with_groups_1) }
      let(:group)        { groups(:with_groups_group2) }
      let(:action_value) { group.id }

      let!(:initial_group_id) { ticket.group_id }

      let(:action) do
        DefinitionItem.new('group_id', nil, action_value)
      end

      before do
        ticket.current_user = current_user

        apply_action
      end

      it 'sets the ticket group' do
        assert_equal group.id, ticket.group_id
      end

      describe 'and the action value is `current_groups`' do
        let(:action_value) { 'current_groups' }

        it 'sets the ticket group to the users first group' do
          assert_equal(
            current_user.groups.first.id,
            ticket.group_id
          )
        end

        describe 'and the current user is not an agent' do
          let(:current_user) { users(:with_groups_end_user) }

          it 'unsets the ticket group ID' do
            assert_nil ticket.group_id
          end
        end

        describe 'and the user does not belong to any groups' do
          let(:current_user) do
            users(:with_groups_agent2).tap do |user|
              user.groups.destroy_all
            end
          end

          it 'unsets the ticket group ID' do
            assert_nil ticket.group_id
          end
        end

        describe 'and the user is a system user' do
          let(:user) { User.system }

          it 'does not update the ticket group ID' do
            assert_equal initial_group_id, ticket.group_id
          end
        end
      end
    end

    describe 'when the action source is `ticket_form_id`' do
      let(:ticket_form)         { ticket_forms(:non_default_ticket_form) }
      let(:default_ticket_form) { ticket_forms(:default_ticket_form) }
      let(:action_value)        { ticket_form.id }

      let(:action) do
        DefinitionItem.new('ticket_form_id', nil, action_value)
      end

      before do
        ticket_form.account_id = account.id
        ticket_form.save!
        default_ticket_form.account_id = account.id
        default_ticket_form.save!
        ticket.account.ticket_forms.stubs(default: [default_ticket_form])

        apply_action
      end

      it 'sets the ticket form' do
        assert_equal ticket_form, ticket.ticket_form
      end

      describe 'and the action value is `default_ticket_form`' do
        let(:action_value) { 'default_ticket_form' }

        it 'sets the default ticket form' do
          assert_equal default_ticket_form, ticket.ticket_form
        end
      end
    end

    describe 'when the action source is `satisfaction_score`' do
      let(:action_value)              { SatisfactionType.OFFERED }
      let(:ticket_satisfaction_score) { SatisfactionType.UNOFFERED }

      let(:action) do
        DefinitionItem.new('satisfaction_score', nil, action_value)
      end

      before do
        ticket.satisfaction_score = ticket_satisfaction_score

        apply_action

        ticket.tap(&:save!).reload
      end

      describe 'and the ticket satisfaction score is unoffered' do
        it 'applies the action' do
          assert_equal action_value, ticket.satisfaction_score
        end
      end

      describe 'and the ticket satisfaction score is not unoffered' do
        let(:ticket_satisfaction_score) { SatisfactionType.GOOD }

        it 'does not apply the action' do
          assert_equal ticket_satisfaction_score, ticket.satisfaction_score
        end
      end
    end

    describe 'when the action source is `cc`' do
      let(:user)       { users(:minimum_admin) }
      let(:other_user) { users(:minimum_agent) }

      let(:action_value) { user.id }
      let(:action)       { DefinitionItem.new('cc', nil, action_value) }

      let(:initial_collaborators) { [other_user.id] }
      let(:initial_followers) { [] }
      let(:initial_email_ccs) { [] }

      before do
        if initial_collaborators.present?
          CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
          ticket.set_collaborators = initial_collaborators
        elsif initial_followers.present? || initial_email_ccs.present?
          CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
          ticket.set_followers = initial_followers
          ticket.set_email_ccs = initial_email_ccs
          ticket.save!
          CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
        end
      end

      describe 'when it is applied' do
        describe 'when old collaborations setting is enabled' do
          before do
            account.stubs(is_collaboration_enabled?: true)

            CollaborationSettingsTestHelper.
              disable_all_new_collaboration_settings

            apply_action
          end

          it 'adds the specified user ID as a collaborator' do
            assert_includes(ticket.collaborations.map(&:user_id), action_value)
          end
        end

        describe 'with new CCs and followers settings enabled' do
          before do
            CollaborationSettingsTestHelper.
              enable_all_new_collaboration_settings

            Account.any_instance.stubs(is_collaboration_enabled?: false)
          end

          describe 'and ticket is saved by an agent' do
            let(:author) { users(:minimum_admin) }

            describe 'and action value is `current_user`' do
              let(:action_value) { 'current_user' }

              it 'adds the specified user ID as a collaborator' do
                apply_action
                assert_includes(ticket.collaborations.map(&:user_id), author.id)
              end
            end

            describe 'and action value has agent' do
              let(:user) { users(:minimum_agent) }

              it 'adds the specified user ID as a collaborator' do
                assert_includes(ticket.collaborations.map(&:user_id), action_value)
              end
            end
          end

          describe 'and ticket is saved by an end-user' do
            let(:author) { users(:minimum_end_user) }
            let(:user)   { users(:minimum_end_user) }

            before { apply_action }

            describe 'and action value is `current_user`' do
              let(:action_value) { 'current_user' }

              it 'does not add the specified user ID as a collaborator' do
                refute_includes(ticket.collaborations.map(&:user_id), author.id)
              end
            end

            describe 'and action value has an agent' do
              let(:user)         { users(:minimum_agent) }
              let(:action_value) { user.id.to_s }

              it 'adds the specified user ID as a collaborator' do
                assert_includes(ticket.
                    collaborations.
                    map(&:user_id), action_value.to_i)
              end
            end
          end
        end
      end

      describe 'when apply_collaborator_action? is false' do
        before do
          Zendesk::Rules::ActionExecutor.
            any_instance.
            stubs(apply_collaborator_action?: false)
        end

        before_should 'not update the collaborators' do
          ticket.expects(:set_collaborators).never
        end

        before_should 'not update the followers' do
          ticket.expects(:set_followers).never
        end
      end

      describe 'when the action value is blank' do
        let(:action_value) { nil }

        before { apply_action }

        before_should 'not update the collaborators' do
          ticket.expects(:set_collaborators).never
        end
      end

      describe 'when the action value is already a collaborator' do
        let(:initial_collaborators) { [user.id] }
        let(:action_value)          { user.id }

        before { apply_action }

        before_should 'not update the followers' do
          ticket.expects(:set_followers=).never
        end

        before_should 'not update the collaborators' do
          ticket.expects(:set_collaborators).never
        end
      end

      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        describe 'when the action value is an EMAIL_CC' do
          let(:initial_collaborators) { [] }
          let(:initial_email_ccs) { [{user_id: user.id}] }

          before { apply_action }

          before_should 'updates the followers' do
            ticket.expects(:set_followers=).with([{user_id: user.id}]).once
          end

          before_should 'does not update the collaborators' do
            ticket.expects(:set_collaborators).never
          end
        end

        describe 'when the action value is already a FOLLOWER' do
          let(:initial_collaborators) { [] }
          let(:initial_followers) { [{user_id: user.id}] }

          before { apply_action }

          before_should 'does not update the followers' do
            ticket.expects(:set_followers=).never
          end

          before_should 'does not update the collaborators' do
            ticket.expects(:set_collaborators).never
          end
        end

        describe 'when the action value is already a LEGACY_CC' do
          before { apply_action }

          before_should 'updates the followers' do
            ticket.expects(:set_followers=).with([{user_id: user.id}]).once
          end

          before_should 'does not update the collaborators' do
            ticket.expects(:set_collaborators).never
          end
        end
      end

      describe 'when the action value is a user that has been deleted' do
        let(:ticket_followers_allowed) { false }

        before do
          User.delete(user.id)
          Rails.cache.clear

          Account.
            any_instance.
            stubs(
              has_ticket_followers_allowed_enabled?: ticket_followers_allowed
            )

          apply_action
        end

        before_should 'not update the collaborators' do
          ticket.expects(:set_collaborators=).never
        end

        describe 'ticket followers setting enabled' do
          let(:ticket_followers_allowed) { true }

          before_should 'not update the followers' do
            ticket.expects(:set_followers=).never
          end
        end
      end
    end

    describe 'when the action source is `notification_user`' do
      let(:recipient) { users(:minimum_agent) }

      let(:action_value)   { recipient.id }
      let(:notified_users) { ticket.audit.events.map(&:value).flatten }

      let(:action) do
        DefinitionItem.new('notification_user', 'is', action_value)
      end

      describe 'and the notification is sent from a human' do
        describe 'and the recipient is a human' do
          before { apply_action }

          it 'creates a `Notification` event' do
            assert_equal 'Notification', ticket.audit.events.first.type
          end
        end

        describe 'and the recipient is a machine' do
          before do
            recipient.identities.first.mark_as_machine

            apply_action
          end

          it 'creates a `Notification` event' do
            assert_equal 'Notification', ticket.audit.events.first.type
          end
        end
      end

      describe 'and the notification is sent from a machine' do
        before { author.identities.first.mark_as_machine }

        describe 'and the recipient is a human' do
          before { apply_action }

          it 'creates a `Notification` event' do
            assert_equal 'Notification', ticket.audit.events.first.type
          end
        end

        describe 'and the recipient is a machine' do
          before do
            recipient.identities.first.mark_as_machine

            apply_action
          end

          it 'does not create a `Notification` event' do
            assert_empty ticket.audit.events
          end

          describe 'and the rule is not a Trigger' do
            let(:rule) { Automation.new }

            it 'creates a `Notification` event' do
              assert_equal 'Notification', ticket.audit.events.first.type
            end
          end
        end
      end

      describe 'and notified' do
        before { apply_action }

        it 'records audit event with `RULE` via type' do
          assert_equal ViaType.RULE, ticket.audit.events.first.via_id
        end

        describe 'and the rule supports referencing revisions' do
          let(:via_reference) do
            {via_id: ViaType.RULE_REVISION, via_reference_id: 1}
          end

          it 'records audit event with `RULE_REVISION` via type' do
            assert_equal ViaType.RULE_REVISION, ticket.audit.events.first.via_id
          end
        end
      end

      describe 'and the recipient is deliverable' do
        before do
          recipient.
            identities.
            email.
            first.
            mark_deliverable_state(DeliverableStateType.DELIVERABLE)

          apply_action
        end

        it 'creates a `Notification` event' do
          assert_equal 'Notification', ticket.audit.events.first.type
        end
      end

      describe 'and recipient is not deliverable' do
        before do
          recipient.
            identities.
            email.
            first.
            mark_deliverable_state(DeliverableStateType.UNDELIVERABLE)

          apply_action
        end

        it 'does not create a `Notification` event' do
          assert_empty ticket.audit.events
        end
      end

      describe 'and notifying all agents' do
        let(:agent)        { users(:minimum_agent) }
        let(:action_value) { 'all_agents' }

        describe 'and the agent is active and deliverable' do
          before { apply_action }

          it 'includes the agent' do
            assert_includes notified_users, agent.id
          end
        end

        describe 'and the agent is suspended' do
          before do
            agent.suspend!

            apply_action
          end

          it 'excludes the agent' do
            refute_includes notified_users, agent.id
          end
        end

        describe 'and the agent is not deliverable' do
          before do
            agent.
              identities.
              email.
              first.
              mark_deliverable_state(DeliverableStateType.UNDELIVERABLE)

            apply_action
          end

          it 'excludes the agent' do
            refute_includes notified_users, agent.id
          end
        end
      end

      describe 'and notifying the current user' do
        let(:user)         { users(:minimum_agent) }
        let(:action_value) { 'current_user' }

        before { ticket.current_user = user }

        describe 'and the user is active and deliverable' do
          before { apply_action }

          it 'includes the user' do
            assert_includes notified_users, user.id
          end
        end

        describe 'and the user is suspended' do
          before do
            user.suspend!

            apply_action
          end

          it 'excludes the user' do
            refute_includes notified_users, user.id
          end

          describe_with_and_without_arturo_enabled :email_truncate_notifications_suppressed_for_metadata do
            it 'includes the suppressed users in the audit metadata' do
              assert_equal(
                [user.id],
                ticket.audit.metadata['notifications_suppressed_for']
              )
            end
          end
        end

        describe 'and the user is not deliverable' do
          before do
            user.
              identities.
              email.
              first.
              mark_deliverable_state(DeliverableStateType.UNDELIVERABLE)

            apply_action
          end

          it 'excludes the user' do
            refute_includes notified_users, user.id
          end
        end

        describe 'and the user is not an agent and does not have an email' do
          let(:user) { users(:minimum_end_user) }

          before do
            user.stubs(:email).returns(nil)

            apply_action
          end

          it 'excludes the user' do
            refute_includes notified_users, user.id
          end
        end
      end

      describe 'and notifying the requester' do
        let(:end_user)     { users(:minimum_author) }
        let(:action_value) { 'requester_id' }

        describe 'and the user is active and deliverable' do
          before { apply_action }

          it 'includes the user' do
            assert_includes notified_users, end_user.id
          end
        end

        describe 'and the user is suspended' do
          before do
            end_user.suspend!

            apply_action
          end

          it 'excludes the user' do
            refute_includes notified_users, end_user.id
          end

          describe_with_and_without_arturo_enabled :email_truncate_notifications_suppressed_for_metadata do
            it 'includes the suppressed users in the audit metadata' do
              assert_equal(
                [end_user.id],
                ticket.audit.metadata['notifications_suppressed_for']
              )
            end
          end
        end

        describe 'and the user is not deliverable' do
          before do
            end_user.
              identities.
              email.
              first.
              mark_deliverable_state(DeliverableStateType.UNDELIVERABLE)

            apply_action
          end

          it 'excludes the user' do
            refute_includes notified_users, end_user.id
          end
        end
      end

      describe 'and notifying the requester and CCs' do
        let(:email_cc)     { users(:minimum_end_user) }
        let(:action_value) { 'requester_and_ccs' }

        let(:notification_event) { ticket.audit.events.last }

        let(:undeliverable_state) { DeliverableStateType.UNDELIVERABLE }

        describe 'when `comment_email_ccs_allowed` is enabled' do
          before do
            Account.
              any_instance.
              stubs(has_comment_email_ccs_allowed_enabled?: true)

            Ticket.any_instance.stubs(
              email_ccs: [email_cc],
              persisted_email_cc_user_ids: [email_cc.id]
            )
          end

          describe 'and the update occurred via email' do
            before do
              Audit.any_instance.stubs(:via?).with(:mail).returns(true)
            end

            describe 'and the user is deliverable and not suspended' do
              describe 'when the author is an email CC' do
                let(:author) { email_cc }

                describe 'and the ticket is persisted' do
                  before { apply_action }

                  it 'does not create a notification event' do
                    notification_event.must_be_nil
                  end
                end

                describe 'and the ticket is a new record' do
                  before do
                    Ticket.any_instance.stubs(new_record?: true)
                    Ticket.any_instance.stubs(persisted?: false)
                    apply_action
                  end

                  it 'creates a `NotificationWithCcs` event' do
                    assert_equal 'NotificationWithCcs', notification_event.type
                  end
                end
              end

              describe 'when the author is the requester' do
                let(:author) { ticket.requester }

                describe 'and the ticket is persisted' do
                  before { apply_action }

                  it 'does not create a notification event' do
                    notification_event.must_be_nil
                  end
                end

                describe 'and the ticket is a new record' do
                  before do
                    Ticket.any_instance.stubs(new_record?: true)
                    Ticket.any_instance.stubs(persisted?: false)
                    apply_action
                  end

                  it 'creates a `NotificationWithCcs` event' do
                    assert_equal 'NotificationWithCcs', notification_event.type
                  end
                end
              end

              describe 'when the user is not an email CC or the requester' do
                before { apply_action }

                it 'creates a `NotificationWithCcs` event' do
                  assert_equal 'NotificationWithCcs', notification_event.type
                end

                it 'sets the ticket CCs as the CCs' do
                  assert_equal [email_cc], notification_event.ccs
                end

                it 'sets the ticket requester and CCs as the recipients' do
                  assert_same_elements(
                    [ticket.requester_id, email_cc.id],
                    notification_event.recipients
                  )
                end
              end
            end

            describe 'and the CC is suspended' do
              before do
                email_cc.suspend!

                apply_action
              end

              it 'creates a `NotificationWithCcs` event' do
                assert_equal 'NotificationWithCcs', notification_event.type
              end

              it 'excludes the CC' do
                refute_includes notified_users, email_cc.id
              end

              describe_with_and_without_arturo_enabled :email_truncate_notifications_suppressed_for_metadata do
                it 'includes the suppressed user in the audit metadata' do
                  assert_equal(
                    [email_cc.id],
                    ticket.audit.metadata['notifications_suppressed_for']
                  )
                end
              end
            end

            describe 'and the CC is not deliverable' do
              before do
                email_cc.
                  identities.
                  email.
                  each do |identity|
                    identity.mark_deliverable_state(undeliverable_state)
                  end

                apply_action
              end

              it 'creates a `NotificationWithCcs` event' do
                assert_equal 'NotificationWithCcs', notification_event.type
              end

              it 'excludes the CC' do
                refute_includes notified_users, email_cc.id
              end
              describe_with_and_without_arturo_enabled :email_truncate_notifications_suppressed_for_metadata do
                it 'includes the suppressed user in the audit metadata' do
                  assert_equal(
                    [email_cc.id],
                    ticket.audit.metadata['notifications_suppressed_for']
                  )
                end
              end
            end

            describe "ccs_requester_excluded_public_comments ON" do
              let(:emails) do
                "#{email_cc.email} #{ticket.requester.email}"
              end

              before do
                Account.any_instance.stubs(has_ccs_requester_excluded_public_comments_enabled?: true)
              end

              describe 'and the update occurred via email by the email CC' do
                let(:author) { email_cc }

                before do
                  Audit.any_instance.stubs(:via?).with(:mail).returns(true)
                end

                describe 'and all recipients received the email directly' do
                  before do
                    Ticket.any_instance.stubs(recipients_for_latest_mail: emails)
                    apply_action
                  end

                  it 'does not create a `NotificationWithCcs` event' do
                    refute notification_event
                  end
                end

                describe 'and all recipients did not receive the email directly' do
                  before do
                    Ticket.any_instance.stubs(recipients_for_latest_mail: "support@xyz.zendesk-staging.com")
                    apply_action
                  end

                  it 'creates a `NotificationWithCcs` event' do
                    assert_equal 'NotificationWithCcs', notification_event.type
                  end
                end
              end

              describe 'when comment is private' do
                let(:comment) do
                  Comment.new(ticket: ticket, author: author, is_public: false)
                end

                let(:author) { ticket.requester }

                before do
                  ticket.comment = comment
                  apply_action
                end

                it 'does not create a `NotificationWithCcs` event' do
                  refute notification_event
                end
              end
            end

            describe 'and the CC and requester received the email directly' do
              let(:emails) do
                "#{email_cc.email} #{ticket.requester.email}"
              end

              before do
                Ticket.any_instance.stubs(recipients_for_latest_mail: emails)
              end

              describe 'and the ticket is persisted' do
                before { apply_action }

                it 'does not create a `NotificationWithCcs` event' do
                  refute notification_event
                end

                describe 'and the inbound email string has case variations' do
                  let(:emails) do
                    "#{email_cc.email.upcase} #{ticket.requester.email}"
                  end

                  it 'does not create a `NotificationWithCcs` event' do
                    refute notification_event
                  end
                end
              end

              describe 'and the ticket is a new record' do
                before do
                  Ticket.any_instance.stubs(new_record?: true)
                  Ticket.any_instance.stubs(persisted?: false)
                  apply_action
                end

                it 'creates a `NotificationWithCcs` event' do
                  assert_equal 'NotificationWithCcs', notification_event.type
                end
              end
            end
          end

          describe 'and the update did not occur via email' do
            before do
              Audit.any_instance.stubs(:via?).with(:mail).returns(false)
            end

            describe 'and the user is deliverable and not suspended' do
              describe 'and the action executes' do
                before { apply_action }

                it 'creates a `NotificationWithCcs` event' do
                  assert_equal 'NotificationWithCcs', notification_event.type
                end

                it 'sets the ticket CCs as the CCs' do
                  assert_equal [email_cc], notification_event.ccs
                end

                it 'sets the ticket requester and CCs as the recipients' do
                  assert_same_elements(
                    [ticket.requester_id, email_cc.id],
                    notification_event.recipients
                  )
                end
              end
            end

            describe 'and the CC is suspended' do
              before do
                email_cc.suspend!

                apply_action
              end

              it 'creates a `NotificationWithCcs` event' do
                assert_equal 'NotificationWithCcs', notification_event.type
              end

              it 'excludes the CC' do
                refute_includes notified_users, email_cc.id
              end

              describe_with_and_without_arturo_enabled :email_truncate_notifications_suppressed_for_metadata do
                it 'includes the suppressed user in the audit metadata' do
                  assert_equal(
                    [email_cc.id],
                    ticket.audit.metadata['notifications_suppressed_for']
                  )
                end
              end
            end

            describe 'and the CC is not deliverable' do
              before do
                email_cc.
                  identities.
                  email.
                  each do |identity|
                    identity.mark_deliverable_state(undeliverable_state)
                  end

                apply_action
              end

              it 'creates a `NotificationWithCcs` event' do
                assert_equal 'NotificationWithCcs', notification_event.type
              end

              it 'excludes the CC' do
                refute_includes notified_users, email_cc.id
              end

              describe_with_and_without_arturo_enabled :email_truncate_notifications_suppressed_for_metadata do
                it 'includes the suppressed user in the audit metadata' do
                  assert_equal(
                    [email_cc.id],
                    ticket.audit.metadata['notifications_suppressed_for']
                  )
                end
              end
            end
          end

          describe 'and the update includes a comment' do
            let(:comment) do
              Comment.new(ticket: ticket, author: author, is_public: is_public)
            end

            before do
              ticket.comment = comment

              apply_action
            end

            describe 'and the comment is public' do
              let(:is_public) { true }

              it 'creates a `NotificationWithCcs` event' do
                assert_equal 'NotificationWithCcs', notification_event.type
              end
            end

            describe 'and the comment is not public' do
              let(:is_public) { false }

              it 'does not create a notification event' do
                notification_event.must_be_nil
              end
            end
          end
        end

        describe 'when `comment_email_ccs_allowed` is not enabled' do
          describe 'and the update did not occur via email' do
            before do
              Audit.any_instance.stubs(:via?).with(:mail).returns(false)
              apply_action
            end

            it 'creates a `Notification` event' do
              assert_equal 'Notification', notification_event.type
            end

            it 'includes the requester on the notification' do
              assert_includes notified_users, ticket.requester.id
            end
          end

          describe 'and the update occurred via email' do
            before do
              Audit.any_instance.stubs(:via?).with(:mail).returns(true)
            end

            describe 'and the requester did not receive the email directly' do
              before do
                Zendesk::InboundMail::RecipientsHelper.stubs(
                  latest_email_user_ids: []
                )
              end

              describe 'and trigger executes' do
                before { apply_action }

                it 'creates a `Notification` event' do
                  assert_equal 'Notification', notification_event.type
                end

                it 'includes the requester on the notification' do
                  assert_includes notified_users, ticket.requester.id
                end
              end
            end

            describe 'and the requester received the email directly' do
              before do
                Zendesk::InboundMail::RecipientsHelper.stubs(
                  latest_email_user_ids: [ticket.requester.id]
                )
                apply_action
              end

              it 'does not create a `Notification` event' do
                notification_event.must_be_nil
              end
            end
          end
        end
      end

      describe 'and notifying the assignee' do
        let(:agent)        { users(:minimum_agent) }
        let(:action_value) { 'assignee_id' }

        describe 'and the agent is active and deliverable' do
          before { apply_action }

          it 'includes the agent' do
            assert_includes notified_users, agent.id
          end
        end

        describe 'and the agent is suspended' do
          before do
            agent.suspend!

            apply_action
          end

          it 'excludes the agent' do
            refute_includes notified_users, agent.id
          end

          describe_with_and_without_arturo_enabled :email_truncate_notifications_suppressed_for_metadata do
            it 'includes the suppressed users in the audit metadata' do
              assert_equal(
                [agent.id],
                ticket.audit.metadata['notifications_suppressed_for']
              )
            end
          end
        end

        describe 'and the agent is not deliverable' do
          before do
            agent.
              identities.
              email.
              first.
              mark_deliverable_state(DeliverableStateType.UNDELIVERABLE)

            apply_action
          end

          it 'excludes the agent' do
            refute_includes notified_users, agent.id
          end
        end
      end

      describe 'and notifying the submitter' do
        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            let(:submitter_id) { ticket.submitter_id }
            let(:recipient) { ticket.submitter }
            let(:action_value) { 'all_agents' }
            let(:arturo_enabled) { true }
            let(:comment_is_public) { false }
            let(:comment) { Comment.new(is_public: comment_is_public) }
            let(:ticket_via_reference_id) { ViaType.HELPCENTER }

            before do
              Arturo.set_feature!(:agent_as_end_user, arturo_enabled)
              ticket.update_column(:submitter_id, submitter_id)
              ticket.comment = comment
              ticket.via_id = via_id
              ticket.via_reference_id = ticket_via_reference_id
            end

            describe 'agent as end user case' do
              it 'excludes the user' do
                apply_action
                refute_includes notified_users, ticket.submitter_id
              end
            end

            describe 'ticket does not have comment' do
              let(:comment) { nil }

              it 'includes the user' do
                apply_action
                assert_includes notified_users, ticket.submitter_id
              end
            end

            describe 'ticket comment is public' do
              let(:comment_is_public) { true }

              it 'includes the user' do
                apply_action
                assert_includes notified_users, ticket.submitter_id
              end
            end

            describe ':agent_as_end_user Arturo is not enabled' do
              let(:arturo_enabled) { false }

              it 'includes the user' do
                apply_action
                assert_includes notified_users, ticket.submitter_id
              end
            end

            describe 'ticket is not from HelpCenter' do
              let(:ticket_via_reference_id) { nil }

              it 'includes the user' do
                apply_action
                assert_includes notified_users, ticket.submitter_id
              end
            end

            describe 'ticket submitter is an admin' do
              let(:submitter_id) { users(:minimum_admin).id }

              it 'includes the user' do
                apply_action
                assert_includes notified_users, ticket.submitter_id
              end
            end
          end
        end
      end
    end

    describe 'when the action source is `notification_group`' do
      let(:agent)          { users(:minimum_agent) }
      let(:group)          { groups(:minimum_group) }
      let(:notified_users) { ticket.audit.events.map(&:value).flatten }
      let(:action_value)   { group.id }

      let(:action) do
        DefinitionItem.new('notification_group', 'is', action_value)
      end

      describe 'and the action value is `group_id`' do
        let(:action_value) { 'group_id' }
        let(:ticket)       { tickets(:with_groups_1) }
        let(:group)        { groups(:with_groups_group1) }
        let(:recipients)   do
          [users(:with_groups_agent1), users(:with_groups_agent2)]
        end

        before do
          ticket.stubs(group_id: group.id)

          apply_action
        end

        it 'notifies the group associated with the ticket' do
          assert_equal recipients.map(&:id), notified_users
        end
      end

      describe 'and the group ID does not exist' do
        let(:action_value) { group.id.succ }

        before { apply_action }

        it 'does not create a notification' do
          assert_empty ticket.audit.events
        end
      end

      describe 'and the agent is active and deliverable' do
        before { apply_action }

        it 'includes the agent' do
          assert_includes notified_users, agent.id
        end
      end

      describe 'and the agent is suspended' do
        before do
          agent.suspend!

          apply_action
        end

        it 'excludes the agent' do
          refute_includes notified_users, agent.id
        end
      end

      describe 'and the agent is not deliverable' do
        before do
          agent.
            identities.
            email.
            first.
            mark_deliverable_state(DeliverableStateType.UNDELIVERABLE)

          apply_action
        end

        it 'excludes the agent' do
          refute_includes notified_users, agent.id
        end
      end

      describe 'when the ticket is via mail' do
        let(:audit_options) { {via_id: ViaType.MAIL} }

        describe 'and the user is not a direct recipient' do
          before { apply_action }

          it 'includes the agent' do
            assert_includes notified_users, agent.id
          end
        end

        describe 'and the user is a direct recipient' do
          before do
            ticket.to_and_ccs_for_latest_mail = [agent.email]

            apply_action
          end

          it 'excludes the agent' do
            refute_includes notified_users, agent.id
          end
        end
      end

      describe 'when notified' do
        before { apply_action }

        it 'creates a `Notification` event' do
          assert_equal 'Notification', ticket.audit.events.first.type
        end

        it 'records an audit event with the `RULE` via type' do
          assert_equal ViaType.RULE, ticket.audit.events.first.via_id
        end

        describe 'and the rule supports referencing revisions' do
          let(:via_reference) do
            {via_id: ViaType.RULE_REVISION, via_reference_id: 1}
          end

          it 'records an audit event with the `RULE_REVISION` via type' do
            assert_equal(
              ViaType.RULE_REVISION,
              ticket.audit.events.first.via_id
            )
          end
        end
      end
    end

    describe 'when the action source is `notification_sms_user`' do
      let(:notified_users) { ticket.audit.events.map(&:value).flatten }

      before { ticket.will_be_saved_by(user) }

      describe 'when the notification target is the requester' do
        let(:user) { users(:minimum_author) }
        let(:action) do
          DefinitionItem.new(
            'notification_sms_user',
            'is',
            ['requester_id', 1, 'a message body']
          )
        end

        describe 'and when notified successfully' do
          before { apply_action }

          it 'creates a notification' do
            assert_includes notified_users, user.id
          end

          it 'records audit event with `RULE` via type' do
            assert_equal ViaType.RULE, ticket.audit.events.first.via_id
          end

          describe 'and the rule supports referencing revisions' do
            let(:via_reference) do
              {via_id: ViaType.RULE_REVISION, via_reference_id: 1}
            end

            it 'records audit event with `RULE_REVISION` via type' do
              assert_equal(
                ViaType.RULE_REVISION,
                ticket.audit.events.first.via_id
              )
            end
          end
        end

        describe 'and the user is suspended' do
          before do
            user.suspend!

            apply_action
          end

          it 'does not create a notification' do
            refute_includes notified_users, user.id
          end
        end

        describe 'and the user is missing' do
          # Users should never be missing on an account, but it happens somehow.
          before do
            # in order for destroy through relations to work
            [External, Comment, Create, Change].each do |klass|
              klass.any_instance.stubs(new_record?: true)
            end
            ticket.requester.destroy
            ticket.requester(true)

            apply_action
          end

          it 'does not create a notification' do
            refute_includes notified_users, user.id
          end
        end

        describe 'with fewer than 3 value params' do
          let(:action) do
            DefinitionItem.new(
              'notification_sms_user',
              'is',
              ['requester_id', 1]
            )
          end

          before { apply_action }

          it 'does not create a notification' do
            refute_includes notified_users, user.id
          end
        end
      end

      describe 'when the notification target is the assignee' do
        let(:user) { users(:minimum_agent) }
        let(:action) do
          DefinitionItem.new(
            'notification_sms_user',
            'is',
            ['assignee_id', 1, 'a message body']
          )
        end

        before { apply_action }

        it 'creates a notification' do
          assert_includes notified_users, user.id
        end
      end

      describe 'when the notification target is the current user' do
        let(:user) { users(:minimum_agent) }
        let(:action) do
          DefinitionItem.new(
            'notification_sms_user',
            'is',
            ['current_user', 1, 'a message body']
          )
        end

        before { ticket.current_user = user }

        describe 'when notified successfully' do
          before { apply_action }

          it 'creates a notification' do
            assert_includes notified_users, user.id
          end
        end

        describe 'and the user is not an agent' do
          let(:user) { users(:minimum_end_user) }

          describe 'and the user has a phone number' do
            before do
              user.identities << UserPhoneNumberIdentity.new(
                user:  user,
                value: '1234567'
              )

              apply_action
            end

            it 'creates a notification' do
              assert_includes notified_users, user.id
            end
          end

          describe 'and the user does not have a phone number' do
            before { apply_action }

            it 'excludes the user' do
              refute_includes notified_users, user.id
            end
          end
        end
      end

      describe 'when the notification target is a designated user' do
        let(:user) { users(:minimum_agent) }
        let(:action) do
          DefinitionItem.new(
            'notification_sms_user',
            'is',
            [user.id.to_s, 1, 'a message body']
          )
        end

        before { apply_action }

        it 'creates a notification' do
          assert_includes notified_users, user.id
        end
      end
    end

    describe 'when the action source is `comment_mode_is_public`' do
      let(:is_public) { 'true' }

      let(:action) do
        DefinitionItem.new('comment_mode_is_public', 'is', is_public)
      end

      before { Arturo.enable_feature!(:rules_can_reference_macros) }

      describe 'and a ticket comment is present' do
        before do
          ticket.add_comment(
            body:             'hello',
            author_id:        author.id,
            via_id:           ViaType.RULE,
            via_reference_id: rule.id
          )

          ticket.set_comment

          apply_action
        end

        it 'has not set the default empty comment' do
          refute_empty(ticket.comment)
        end

        describe 'and the action value is `true`' do
          it 'sets `ticket.comment.is_public` to `true`' do
            assert ticket.comment.is_public
          end
        end

        describe 'and the action value is `false`' do
          let(:is_public) { 'false' }

          it 'sets `ticket.comment.is_public` to `false`' do
            refute ticket.comment.is_public
          end
        end
      end

      describe 'and a ticket comment is not present' do
        before do
          ticket.comment = nil

          apply_action
        end

        it 'sets a default empty comment' do
          assert ticket.comment && ticket.comment.empty?
        end

        describe 'and the action value is `true`' do
          it 'sets `ticket.comment.is_public` to `true`' do
            assert ticket.comment.is_public
          end
        end

        describe 'and the action value is `false`' do
          let(:is_public) { 'false' }

          it 'sets `ticket.comment.is_public` to `false`' do
            refute ticket.comment.is_public
          end
        end
      end

      describe_with_arturo_disabled :rules_can_reference_macros do
        let!(:original_ticket) { ticket.dup }

        before { apply_action }

        it 'does not apply the action to the ticket comment' do
          assert_equal_with_nil ticket.comment, original_ticket.comment
        end
      end
    end

    describe 'when the action source is `locale_id`' do
      let(:new_locale)    { translation_locales(:french) }
      let(:new_locale_id) { new_locale.id }
      let(:requester)     { users(:minimum_agent) }

      let!(:initial_ticket_locale)    { ticket.translation_locale }
      let!(:initial_requester_locale) { requester.translation_locale }

      let(:action) do
        DefinitionItem.new('locale_id', nil, new_locale_id)
      end

      describe 'and an action is applied' do
        before do
          ticket.requester = requester

          apply_action
        end

        it 'updates the ticket locale' do
          assert_equal new_locale, ticket.translation_locale
        end

        it 'updates the requester locale' do
          assert_equal new_locale, ticket.requester.translation_locale
        end

        describe 'and the new locale matches the requester locale' do
          let(:new_locale) { requester.translation_locale }

          it 'does not update the ticket locale' do
            assert_equal initial_ticket_locale, ticket.translation_locale
          end

          it 'does not update the requester locale' do
            assert_equal(
              initial_requester_locale,
              ticket.requester.translation_locale
            )
          end
        end
      end

      describe 'and the new locale does not exist' do
        let(:new_locale_id) { -99 }

        describe 'and an action is applied' do
          it 'does not update the ticket locale' do
            assert_raises(ActiveRecord::RecordNotFound) do
              ticket.requester = requester

              apply_action
            end
          end

          it 'does not update the requester locale' do
            assert_raises(ActiveRecord::RecordNotFound) do
              ticket.requester = requester

              apply_action
            end
          end
        end
      end
    end
  end

  describe '#apply_sms_group_notification_actions' do
    let(:author)         { users(:minimum_admin) }
    let(:ticket)         { tickets(:minimum_1) }
    let(:notified_users) { ticket.audit.events.map(&:value).flatten }
    let(:recipient)      { users(:minimum_agent) }

    let(:apply_action) do
      build(rule, ticket, action).apply
    end

    before do
      ticket.will_be_saved_by(author)
    end

    describe 'sms notifications' do
      let(:agent)    { users(:minimum_agent) }
      let(:end_user) { users(:minimum_author) }

      describe 'a rule that notifies a group' do
        let(:action) do
          DefinitionItem.new(
            'notification_sms_group',
            'is',
            ['group_id', 1, 'a message body']
          )
        end

        it 'excludes suspended agents from its list of recipients' do
          agent.suspend!
          apply_action
          refute_includes notified_users, agent.id
        end

        it 'excludes agents who have a truthy suspension setting from its list of recipients' do
          agent.suspended = 1
          agent.save!
          apply_action
          refute_includes notified_users, agent.id
        end

        it 'includes non-suspended and deliverable agents in its list of recipients' do
          apply_action
          assert_includes notified_users, agent.id
        end

        describe 'when notified' do
          before { apply_action }

          it 'records audit event with `RULE` via type' do
            assert_equal ViaType.RULE, ticket.audit.events.first.via_id
          end

          describe 'and the rule supports referencing revisions' do
            let(:via_reference) do
              {via_id: ViaType.RULE_REVISION, via_reference_id: 1}
            end

            it 'records audit event with `RULE_REVISION` via type' do
              assert_equal(
                ViaType.RULE_REVISION,
                ticket.audit.events.first.via_id
              )
            end
          end
        end

        describe 'with fewer than 3 value params' do
          let(:action) do
            DefinitionItem.new('notification_sms_group', 'is', ['group_id', 1])
          end

          before { apply_action }

          it 'does not create a notification' do
            assert_empty ticket.audit.events
          end
        end
      end
    end
  end

  describe '#apply_messaging_csat_action' do
    let(:author) { users(:minimum_admin) }
    let(:ticket) { tickets(:minimum_1) }
    let(:apply_action) do
      new_rule(DefinitionItem.new(
        'notification_messaging_csat', 'is', ['requester_id']
      )).apply_actions(ticket.reload)
    end

    around do |t|
      Timecop.freeze { t.call }
    end

    describe 'Ticket via: native_messaging' do
      before do
        # ticket created via native_messaging
        ticket.will_be_saved_by(author, via_id: ViaType.NATIVE_MESSAGING)
        ChatStartedEvent.new(ticket: ticket, created_at: 2.days.ago, value: {'chat_id' => '123', 'visitor_id' => '456'}).tap(&:apply!)

        # ticket updated via email
        ticket.will_be_saved_by(author, via_id: ViaType.WEB_FORM)
        ticket.comment = Comment.new(ticket: ticket, author: author, account: accounts(:minimum), body: "test", via_id: ViaType.WEB_FORM, created_at: 1.day.ago, is_public: true)
        ticket.save!
      end

      describe 'public comment after the last chat_started' do
        it 'does not create messaging_csat event' do
          apply_action
          assert_empty ticket.audit.events
        end
      end

      describe 'no public comment after the last chat_started' do
        before do
          # ticket updated via native_messaging second time
          ticket.will_be_saved_by(author, via_id: ViaType.NATIVE_MESSAGING)
          @chat_started = ChatStartedEvent.new(ticket: ticket, created_at: 1.hour.ago, value: {'chat_id' => '321', 'visitor_id' => '456'}).tap(&:apply!)
        end

        it 'create messaging_csat event & update satisfaction' do
          api_response = stub(success?: true, body: '')
          Zopim::InternalApiClient.any_instance.expects(:send_messaging_csat).returns(api_response)

          apply_action
          assert_equal SatisfactionType.OFFERED, ticket.satisfaction_score
          assert ticket.audit.events.last.is_a?(MessagingCsatEvent)
          assert_equal @chat_started.id, ticket.audit.events.last.chat_started_id
        end

        it 'create messaging_csat event only' do
          api_response = stub(success?: false, body: 'Visitor without Messaging data')
          Zopim::InternalApiClient.any_instance.expects(:send_messaging_csat).returns(api_response)

          apply_action
          assert_equal SatisfactionType.UNOFFERED, ticket.satisfaction_score
          assert ticket.audit.events.last.is_a?(MessagingCsatEvent)
          assert_equal @chat_started.id, ticket.audit.events.last.chat_started_id
        end
      end
    end

    describe 'Ticket via: chat' do
      before do
        # ticket created via chat
        ticket.will_be_saved_by(author, via_id: ViaType.CHAT)
        ChatStartedEvent.new(ticket: ticket, created_at: 2.days.ago, value: {'chat_id' => '123', 'visitor_id' => '456'}).tap(&:apply!)
      end

      it 'does not create messaging_csat event' do
        apply_action
        assert_empty ticket.audit.events
      end
    end

    describe 'Ticket does not have public comment or chat started event' do
      before do
        ticket.stubs(events: [])
      end

      it 'does not create messaging_csat event' do
        apply_action
        assert_empty ticket.audit.events
      end
    end
  end

  describe 'from rule' do
    let(:ticket) { tickets(:minimum_1) }

    before do
      @account = accounts(:minimum)
    end

    it 'tests set property' do
      assert_equal PriorityType.Normal, ticket.priority_id

      new_rule(DefinitionItem.new('priority_id', nil, PriorityType.HIGH)).
        apply_actions(ticket)

      assert_equal PriorityType.HIGH, ticket.priority_id

      new_rule(DefinitionItem.new('priority_id', nil, PriorityType.HIGH)).
        apply_actions(ticket)
    end

    describe 'tests set invalid property' do
      before do
        new_rule(DefinitionItem.new('priority_id', nil, 0)).
          apply_actions(ticket)
      end

      it 'check the priority was not reset' do
        # Priority was not reset
        assert_equal PriorityType.NORMAL, ticket.priority_id
      end
    end

    it 'tests set brand to inactive brand' do
      original_brand = ticket.brand

      inactive_brand = FactoryBot.create(
        :brand,
        name: 'wombat',
        active: false,
        account_id: ticket.account_id
      )

      new_rule(DefinitionItem.new('brand_id', nil, inactive_brand.id)).
        apply_actions(ticket)

      # Brand was not set to inactive brand
      assert_equal original_brand.id, ticket.brand_id
    end

    it 'tests set assignee to current user' do
      assert_equal users(:minimum_agent), ticket.assignee

      new_rule(DefinitionItem.new('assignee_id', nil, 'current_user')).
        apply_actions(ticket)

      # Can't assign to system user
      assert_equal users(:minimum_agent), ticket.assignee

      ticket.current_user = users(:minimum_admin)

      new_rule(DefinitionItem.new('assignee_id', nil, 'current_user')).
        apply_actions(ticket)

      assert_equal users(:minimum_admin), ticket.assignee
    end

    it 'tests set assignee and group to current assignee and group' do
      ticket = tickets(:with_groups_1)
      ticket.current_user = users(:with_groups_agent1)

      assert_equal(
        [users(:with_groups_agent2), groups(:with_groups_group1)],
        [ticket.assignee, ticket.group]
      )

      new_rule(DefinitionItem.new('assignee_id', nil, 'current_user')).
        apply_actions(ticket)

      assert_equal(
        [users(:with_groups_agent1), groups(:with_groups_group1)],
        [ticket.assignee, ticket.group]
      )
    end

    it 'tests set tags' do
      new_rule(DefinitionItem.new('set_tags', nil, 'wrench monkey')).
        apply_actions(ticket)
      assert_equal 'monkey wrench', ticket.current_tags
    end

    it 'tests add tags' do
      assert_equal 'hello', ticket.current_tags

      new_rule(DefinitionItem.new('current_tags', nil, 'kitty')).
        apply_actions(ticket)

      assert_equal 'hello kitty', ticket.current_tags
      ticket.reset_delta_changes!

      new_rule(DefinitionItem.new('current_tags', nil, 'kitty')).
        apply_actions(ticket)

      assert_empty(ticket.audit.events)
      # assert !ticket.delta_changes?
    end

    it 'tests remove tags' do
      assert_equal 'hello', ticket.current_tags

      new_rule(DefinitionItem.new('remove_tags', nil, 'hello')).
        apply_actions(ticket)

      assert_nil ticket.current_tags
    end

    describe 'when there are custom ticket field actions' do
      let(:ticket) { tickets(:minimum_1) }

      describe 'and an action is applied' do
        let(:field) do
          FieldText.create!(account: account, title: 'text field')
        end

        let(:rule) do
          new_rule(
            DefinitionItem.new("ticket_fields_#{field.id}", nil, field_value)
          )
        end

        before do
          rule.apply_actions(ticket)
        end

        describe 'and the field value is truthy' do
          let(:field_value) { '1' }

          it 'records the ticket field entry as true' do
            assert_equal 'true', ticket.ticket_field_entries.last.value
          end
        end

        describe 'and the field value is falsey' do
          let(:field_value) { nil }

          it 'records the ticket field entry as false' do
            assert_equal 'false', ticket.ticket_field_entries.last.value
          end
        end
      end

      describe 'and the field is a multiselect' do
        let(:multiselect) do
          FieldMultiselect.create!(
            account: account,
            title: 'Multiselect Wombat',
            custom_field_options: [
              CustomFieldOption.new(name: 'Wombats', value: 'wombats'),
              CustomFieldOption.new(name: 'Giraffes', value: 'giraffes'),
              CustomFieldOption.new(
                name: 'Tazzmanian Devils',
                value: 'tazzies'
              ),
              CustomFieldOption.new(name: 'Lemurs', value: 'lemurs')
            ]
          )
        end

        let(:field_entry) do
          ticket.ticket_field_entries.detect do |entry|
            entry.ticket_field_id == multiselect.id
          end
        end

        let(:tazzie_option) do
          ticket.account.custom_field_options.detect do |option|
            option.value == 'tazzies'
          end
        end

        before do
          ticket.fields = {multiselect.id => %w[wombats giraffes]}
          ticket.will_be_saved_by(users(:minimum_admin))
          ticket.save!
        end

        it 'initializes the field entry values' do
          assert_equal 'giraffes wombats', field_entry.value
        end

        it 'initalizes the current tags' do
          assert_equal 'giraffes hello wombats', ticket.current_tags
        end

        describe 'and an action is applied' do
          before do
            new_rule(
              DefinitionItem.new(
                "ticket_fields_#{multiselect.id}",
                nil,
                tazzie_option.id
              )
            ).apply_actions(ticket)
          end

          it 'adds the new entry' do
            assert_equal 'giraffes tazzies wombats', field_entry.value
          end

          it 'adds the new tags' do
            assert_equal 'giraffes hello tazzies wombats', ticket.current_tags
          end
        end
      end

      describe 'and the field is a tagger' do
        let(:tagger_id) { ticket_fields(:field_tagger_custom).id }
        let(:user)      { users(:minimum_agent) }

        let(:rule) do
          new_rule(
            DefinitionItem.new(
              "ticket_fields_#{tagger_id}",
              nil,
              custom_field_option_id
            )
          )
        end

        let(:custom_field_option_id) do
          custom_field_options(:field_tagger_custom_option_2).id
        end

        let(:change_event) do
          ticket.audits.last.events.find do |event|
            event.value_reference == 'current_tags'
          end
        end

        describe 'when the custom field is not set' do
          before do
            rule.apply_actions(ticket)

            ticket.save
          end

          it 'records a change event' do
            assert_instance_of Change, change_event
          end

          it 'records the previous value' do
            assert_equal 'hello', change_event.value_previous
          end

          it 'records the new value' do
            assert_equal 'hello not_bad', change_event.value
          end
        end

        describe 'when a custom field is set' do
          before do
            ticket.will_be_saved_by(user)

            ticket.fields = {
              tagger_id => custom_field_options(
                :field_tagger_custom_option_3
              ).value
            }

            ticket.save
          end

          describe 'and the custom field is changed' do
            let(:custom_field_option_id) do
              custom_field_options(:field_tagger_custom_option_1).id
            end

            before do
              rule.apply_actions(ticket)

              ticket.save
            end

            it 'records the previous value' do
              assert_equal 'hello hilarious', change_event.value_previous
            end

            it 'records the new value' do
              assert_equal 'booring hello', change_event.value
            end
          end

          describe 'and the custom field is deleted' do
            let(:custom_field_option_id) { nil }

            before do
              rule.apply_actions(ticket)

              ticket.save
            end

            it 'records the previous value' do
              assert_equal 'hello hilarious', change_event.value_previous
            end

            it 'records the new value' do
              assert_equal 'hello', change_event.value
            end
          end
        end
      end

      describe 'and the field is a date' do
        let(:date_field) do
          FieldDate.create!(
            account: account,
            title: 'Custom wombat date',
            is_active: true
          )
        end

        let(:ticket_date_field) do
          ticket.ticket_field_entries.detect do |f|
            f.ticket_field_id == date_field.id
          end
        end

        let(:change_event) do
          ticket.audits.last.events.find do |event|
            event.value_reference == date_field.id.to_s
          end
        end

        let(:rule) do
          new_rule(
            DefinitionItem.new(
              "ticket_fields_#{date_field.id}",
              'is',
              %w[specific_date 2014-02-18]
            )
          )
        end

        before do
          rule.apply_actions(ticket)

          ticket.save
        end

        it 'sets custom date' do
          assert_equal '2014-02-18', ticket_date_field.value
        end

        it 'records a change event' do
          assert_instance_of Change, change_event
        end

        it 'records the previous value' do
          assert_nil change_event.value_previous
        end

        it 'records the new value' do
          assert_equal '2014-02-18', change_event.value
        end

        describe 'when an integer value is set' do
          let(:rule) { create_trigger(title: 'panda', definition: definition) }

          let(:definition) do
            Definition.new.tap do |definition|
              definition.conditions_all.push(
                DefinitionItem.new('current_tags', 'includes', 'abc')
              )
              definition.actions.push(
                DefinitionItem.new(
                  "ticket_fields_#{date_field.id}",
                  'is',
                  %w[days_from_now 3]
                )
              )
            end
          end

          it 'sets the integer to custom date field' do
            assert_equal(
              3.days.
                from_now.
                in_time_zone(@account.time_zone).
                strftime('%Y-%m-%d'),
              ticket_date_field.value
            )
          end
        end
      end

      describe 'and the field is a checkbox' do
        before do
          @checkbox_id = ticket_fields(:field_checkbox_custom).id
        end

        describe 'sets correct checkbox custom field values and tags' do
          before do
            @block = proc do
              # True
              new_rule(
                DefinitionItem.new(
                  "ticket_fields_#{@checkbox_id}",
                  nil,
                  definition_enable
                )
              ).apply_actions(ticket)

              assert_equal 'hello im_set', ticket.current_tags

              assert_equal(
                '1',
                ticket.
                  ticket_field_entries.
                  detect { |f| f.ticket_field_id == @checkbox_id }.
                  value
              )

              ticket.save

              tags_event = ticket.
                audits.
                last.
                events.
                find_by_value_reference('current_tags')

              assert_equal 'hello im_set', tags_event.value

              # False
              new_rule(
                DefinitionItem.new(
                  "ticket_fields_#{@checkbox_id}",
                  nil,
                  definition_disable
                )
              ).apply_actions(ticket)

              ticket.save!
              assert_equal 'hello', ticket.current_tags

              assert_equal(
                '0',
                ticket.
                  ticket_field_entries.
                  reload.
                  detect { |f| f.ticket_field_id == @checkbox_id }.
                  value
              )

              ticket.save

              tags_event = ticket.
                audits.
                last.
                events.
                find_by_value_reference('current_tags')

              assert_equal 'hello', tags_event.value
            end
          end

          describe 'when using Integer' do
            let(:definition_enable)  { 1 }
            let(:definition_disable) { 0 }

            it 'sets the correct values' do
              @block.call
            end
          end

          describe 'when using Boolean' do
            let(:definition_enable)  { 'true' }
            let(:definition_disable) { 'false' }

            it 'sets the correct values' do
              @block.call
            end
          end
        end
      end
    end

    describe 'and the source is a custom field' do
      let(:external_change) { ticket.external_changes.last }
      let(:field_key)       { 'field_decimal' }
      let(:field_title)     { 'decimal field' }
      let(:field_type)      { 'Decimal' }
      let(:result_1)        { '0000000000123.4500' }
      let(:result_2)        { '-000000000010.0000' }
      let(:value_1)         { '123.45' }
      let(:value_2)         { '-10.0' }

      describe 'and the field is a custom user field' do
        let(:action_source) { "requester.custom_fields.#{field_key}" }

        let(:custom_field) do
          create_user_custom_field!(field_key, field_title, field_type)
        end

        let(:field) do
          ticket.requester.custom_field_values.value_for_key(field_key)
        end

        before { custom_field.save! }

        describe 'and the requester is a foreign user' do
          before do
            User.any_instance.stubs(foreign?: true)

            new_rule(DefinitionItem.new(action_source, nil, true)).
              apply_actions(ticket)

            ticket.save!
          end

          it 'does not update the ticket field' do
            assert_nil(
              ticket.
                reload.
                requester.
                custom_field_values.
                value_for_key(field_key)
            )
          end
        end

        describe 'and an action is applied' do
          before do
            new_rule(DefinitionItem.new(action_source, nil, value_1)).
              apply_actions(ticket)
          end

          it 'sets the external change value to the field value' do
            assert_equal field.value, external_change.value
          end

          it 'records the previous external change value' do
            assert_nil external_change.value_previous
          end

          it 'matches the external field change to the ticket' do
            assert_equal(
              field.info_for_external_change_event[:info],
              external_change.value_reference[:info]
            )
          end

          describe 'and an action is applied with a different value' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, value_2)).
                apply_actions(ticket)
            end

            it 'sets the external change value to the field value' do
              assert_equal field.value, external_change.value
            end

            it 'records the previous external change value' do
              assert_equal result_1, external_change.value_previous
            end

            it 'matches the external field change to the ticket' do
              assert_equal(
                field.info_for_external_change_event[:info],
                external_change.value_reference[:info]
              )
            end
          end

          describe 'and an action is applied that unsets the custom field' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, nil)).
                apply_actions(ticket)
            end

            it 'removes the field' do
              assert_nil field
            end

            it 'clears the external change value' do
              assert_nil external_change.value
            end

            it 'records the previous external change value' do
              assert_equal result_1, external_change.value_previous
            end
          end

          describe_with_arturo_enabled :set_active_only_custom_fields do
            let(:new_value) { "0000000000006.3020" }

            describe 'and custom field is inactive' do
              before do
                custom_field.is_active = false;
                custom_field.save!

                new_rule(DefinitionItem.new(action_source, nil, new_value)).apply_actions(ticket)
              end

              it 'does not apply change' do
                refute_equal new_value, external_change.value
              end
            end

            describe 'and custom field is active' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, new_value)).apply_actions(ticket)
              end

              it 'applies change' do
                assert_equal new_value, external_change.value
              end
            end
          end

          describe_with_arturo_disabled :set_active_only_custom_fields do
            let(:new_value) { "0000000000006.3020" }

            describe 'and custom field is inactive' do
              before do
                custom_field.is_active = false;
                custom_field.save!

                new_rule(DefinitionItem.new(action_source, nil, new_value)).apply_actions(ticket)
              end

              it 'applies change' do
                assert_equal new_value, external_change.value
              end
            end
          end
        end

        describe 'and the field type is checkbox' do
          let(:field_key)   { 'field_checkbox' }
          let(:field_title) { 'checkbox field' }
          let(:field_type)  { 'Checkbox' }

          describe 'and an action is applied' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, true)).
                apply_actions(ticket)
            end

            it 'sets the proper field value' do
              assert_equal '1', field.value
            end

            describe 'and an action is applied that unsets the checkbox' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, false)).
                  apply_actions(ticket)
              end

              it 'removes the field' do
                assert_nil field
              end
            end
          end
        end

        describe 'and the field type is decimal' do
          describe 'and an action is applied' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, value_1)).
                apply_actions(ticket)
            end

            it 'sets the proper field value' do
              assert_equal result_1, field.value
            end

            describe 'and an action is applied with a different value' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, value_2)).
                  apply_actions(ticket)
              end

              it 'sets the proper field value' do
                assert_equal result_2, field.value
              end
            end
          end
        end

        describe 'and the field type is dropdown' do
          let(:choice_1_id)    { custom_field.dropdown_choices.first.id.to_s }
          let(:choice_2_id)    { custom_field.dropdown_choices.second.id.to_s }
          let(:choice_1_value) { 'choice_1' }
          let(:choice_2_value) { 'choice_2' }
          let(:field_key)      { 'field_dropdown' }
          let(:field_title)    { 'dropdown field' }
          let(:field_type)     { 'Dropdown' }

          let(:custom_field) do
            create_user_custom_field!(
              field_key,
              field_title,
              field_type
            ).tap do |dropdown|
              dropdown.dropdown_choices.build(
                name: 'Choice 1',
                value: choice_1_value
              )

              dropdown.dropdown_choices.build(
                name: 'Choice 2',
                value: choice_2_value
              )
            end
          end

          describe 'and an action is applied' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, choice_2_value)).
                apply_actions(ticket)
            end

            it 'sets the proper field value' do
              assert_equal choice_2_id, field.value
            end

            describe 'and an action is applied with a different value' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, choice_1_value)).
                  apply_actions(ticket)
              end

              it 'sets the proper field value' do
                assert_equal choice_1_id, field.value
              end
            end

            describe 'and an action is applied that unsets the custom field' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, nil)).
                  apply_actions(ticket)
              end

              it 'records the previous external change value' do
                assert_equal choice_2_id, external_change.value_previous
              end
            end
          end
        end
      end

      describe 'and the field is a custom organization field' do
        let(:action_source) do
          "requester.organization.custom_fields.#{field_key}"
        end

        let(:custom_field) do
          create_organization_custom_field!(field_key, field_title, field_type)
        end

        let(:field) do
          ticket.
            requester.
            organization.
            custom_field_values.
            value_for_key(field_key)
        end

        before do
          ticket.requester.update_attribute(
            :organization,
            organizations(:minimum_organization1)
          )

          custom_field.save!
        end

        describe 'and an action is applied' do
          before do
            new_rule(DefinitionItem.new(action_source, nil, value_1)).
              apply_actions(ticket)
          end

          it 'sets the external change value to the field value' do
            assert_equal field.value, external_change.value
          end

          it 'records the previous external change value' do
            assert_nil external_change.value_previous
          end

          it 'matches the external field change to the ticket' do
            assert_equal(
              field.info_for_external_change_event[:info],
              external_change.value_reference[:info]
            )
          end

          describe 'and an action is applied with a different value' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, value_2)).
                apply_actions(ticket)
            end

            it 'sets the external change value to the field value' do
              assert_equal field.value, external_change.value
            end

            it 'records the previous external change value' do
              assert_equal result_1, external_change.value_previous
            end

            it 'matches the external field change to the ticket' do
              assert_equal(
                field.info_for_external_change_event[:info],
                external_change.value_reference[:info]
              )
            end
          end

          describe 'and an action is applied that unsets the custom field' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, nil)).
                apply_actions(ticket)
            end

            it 'removes the field' do
              assert_nil field
            end

            it 'clears the external change value' do
              assert_nil external_change.value
            end

            it 'records the previous external change value' do
              assert_equal result_1, external_change.value_previous
            end
          end

          describe_with_arturo_enabled :set_active_only_custom_fields do
            let(:new_value) { "0000000000006.3020" }

            describe 'and custom field is inactive' do
              before do
                custom_field.is_active = false;
                custom_field.save!

                new_rule(DefinitionItem.new(action_source, nil, new_value)).apply_actions(ticket)
              end

              it 'does not apply change' do
                refute_equal new_value, external_change.value
              end
            end

            describe 'and custom field is active' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, new_value)).apply_actions(ticket)
              end

              it 'applies change' do
                assert_equal new_value, external_change.value
              end
            end
          end

          describe_with_arturo_disabled :set_active_only_custom_fields do
            let(:new_value) { "0000000000006.3020" }

            describe 'and custom field is inactive' do
              before do
                custom_field.is_active = false;
                custom_field.save!

                new_rule(DefinitionItem.new(action_source, nil, new_value)).apply_actions(ticket)
              end

              it 'applies change' do
                assert_equal new_value, external_change.value
              end
            end
          end
        end

        describe 'and the field type is checkbox' do
          let(:field_key)   { 'field_checkbox' }
          let(:field_title) { 'checkbox field' }
          let(:field_type)  { 'Checkbox' }

          describe 'and an action is applied' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, true)).
                apply_actions(ticket)
            end

            it 'sets the proper field value' do
              assert_equal '1', field.value
            end

            describe 'and an action is applied that unsets the checkbox' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, nil)).
                  apply_actions(ticket)
              end

              it 'removes the field' do
                assert_nil field
              end
            end
          end
        end

        describe 'and the field type is decimal' do
          describe 'and an action is applied' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, value_1)).
                apply_actions(ticket)
            end

            it 'sets the proper field value' do
              assert_equal result_1, field.value
            end

            describe 'and an action is applied with a different value' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, value_2)).
                  apply_actions(ticket)
              end

              it 'sets the proper field value' do
                assert_equal result_2, field.value
              end
            end
          end
        end

        describe 'and the field type is dropdown' do
          let(:choice_1_id)    { custom_field.dropdown_choices.first.id.to_s }
          let(:choice_2_id)    { custom_field.dropdown_choices.second.id.to_s }
          let(:choice_1_value) { 'choice_1' }
          let(:choice_2_value) { 'choice_2' }
          let(:field_key)      { 'field_dropdown' }
          let(:field_title)    { 'dropdown field' }
          let(:field_type)     { 'Dropdown' }

          let(:custom_field) do
            create_organization_custom_field!(
              field_key,
              field_title,
              field_type
            ).tap do |dropdown|
              dropdown.dropdown_choices.build(
                name: 'Choice 1',
                value: choice_1_value
              )

              dropdown.dropdown_choices.build(
                name: 'Choice 2',
                value: choice_2_value
              )
            end
          end

          describe 'and an action is applied' do
            before do
              new_rule(DefinitionItem.new(action_source, nil, choice_2_value)).
                apply_actions(ticket)
            end

            it 'sets the proper field value' do
              assert_equal choice_2_id, field.value
            end

            describe 'and an action is applied with a different value' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, choice_1_value)).
                  apply_actions(ticket)
              end

              it 'sets the proper field value' do
                assert_equal choice_1_id, field.value
              end
            end

            describe 'and an action is applied that unsets the custom field' do
              before do
                new_rule(DefinitionItem.new(action_source, nil, nil)).
                  apply_actions(ticket)
              end

              it 'records the previous external change value' do
                assert_equal choice_2_id, external_change.value_previous
              end
            end
          end
        end
      end
    end

    describe 'when action is `notification_target`' do
      let(:trigger) { create_trigger(title: 'panda', definition: definition) }
      let(:ticket)  { tickets(:minimum_1) }

      let(:definition) do
        Definition.new.tap do |definition|
          definition.conditions_all.push(
            DefinitionItem.new('status_id', 'is', StatusType.CLOSED)
          )
          definition.actions.push(
            DefinitionItem.new(
              'notification_target',
              nil,
              targets(:twitter_valid).id
            )
          )
        end
      end

      before do
        trigger.stubs(via_reference: {via_id: ViaType.RULE, id: trigger.id})

        trigger.apply_actions(ticket)
      end

      it 'enqueues a target job' do
        TargetJob.expects(:enqueue)

        ticket.will_be_saved_by(users(:minimum_agent))
        ticket.save!
      end

      it 'records an `External` audit event' do
        assert_equal 'External', ticket.audit.events.last.type
      end
    end

    describe_with_arturo_enabled :email_comment_placeholder_detected_metric do
      # it "records a metric" do
      #   action_executor_statsd_client.expects(:increment).with('action_executor.comment_placeholder_detected',tags: instance_of(Hash)).once
      # end

      it "logs a message" do
        Rails.logger.expects(:info).with(regexp_matches(/\Asome info/)).once
        base = build(rule, ticket, action)
        base.apply
        #ticket.apply
      end
    end

    # describe_with_arturo_disabled :email_comment_placeholder_detected_metric do
    #   it "does not record a metric" do
    #     action_executor_statsd_client.expects(:increment).with('action_executor.comment_placeholder_detected',tags: instance_of(Hash)).never
    #   end
    #
    #   it "does not log anything" do
    #     Rails.logger.expects(:info).with(regexp_matches(/\Asome info/)).never
    #   end
    # end

    def action_executor_statsd_client
    @action_executor_statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'action executor')
    end
  end

  private

  def build(rule, ticket, action)
    Zendesk::Rules::ActionExecutor.new(rule, ticket, action)
  end
end
