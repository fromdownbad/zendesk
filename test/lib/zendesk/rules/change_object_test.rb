require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Zendesk::Rules::ChangeObject do
  include TestSupport::Rule::DiffHelper

  let(:change)  { '+' }
  let(:content) { 'content' }

  describe '#initialize' do
    it 'raises an error if the `change` argument is not supported' do
      e = assert_raises(ArgumentError) { change_object('x', content) }

      assert_match(/`change` must be one of: =, \+, -/, e.message)
    end
  end

  describe '#==' do
    it 'returns false if compared to an object of a different class' do
      refute change_object(change, content) == Class.new.new
    end

    it 'returns true if compared to a object that is a child class' do
      assert(
        change_object(change, content) ==
          Class.new(Zendesk::Rules::ChangeObject).new(change, content)
      )
    end

    it 'returns true if the `change` and `content` attributes are equal' do
      # rubocop:disable Lint/UselessComparison
      assert change_object(change, content) == change_object(change, content)
      # rubocop:enable Lint/UselessComparison
    end

    it 'returns false if the `change` attributes are different' do
      refute change_object(change, content) == change_object('-', content)
    end

    it 'returns false if the `content` attributes are different' do
      refute change_object(change, content) == change_object(change, 'new')
    end
  end

  describe '#to_h' do
    it 'returns a serialized hash containing the `change` attribute' do
      assert_equal change, change_object(change, content).to_h[:change]
    end

    it 'returns a serialized hash containing the `content` attribute' do
      assert_equal content, change_object(change, content).to_h[:content]
    end
  end
end
