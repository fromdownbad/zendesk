require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::Rules::Categories::RuleUpdates do
  include TestSupport::Rule::Helper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:tuple) { Zendesk::Rules::Categories::RuleUpdates.partition([item]) }

  describe '#partition' do
    let!(:category_a) { create_category(position: 1) }
    let!(:category_b) { create_category(position: 2) }
    let!(:trigger_1) { create_trigger(position: 1, rules_category_id: category_a.id) }
    let!(:trigger_2) { create_trigger(position: 2, rules_category_id: category_a.id, active: false) }

    describe 'and there are updates without position' do
      let(:item) { {id: trigger_1.id, active: false} }

      it 'is included in the positionless updates response tuple' do
        assert_includes tuple[0], item
      end
    end

    describe 'and there are updates with position' do
      let(:item) { {id: trigger_1.id, position: 5} }

      it 'is included in the position updates response tuple' do
        assert_includes tuple[1], item
      end
    end

    describe 'and there are positional updates without position values' do
      describe 'when reactivating a rule' do
        let(:item) { {id: trigger_2.id, active: true} }

        it 'is included in the position updates response tuple' do
          sliced_item = item.slice(:id)

          assert_includes tuple[1], sliced_item
        end
      end
    end
  end
end
