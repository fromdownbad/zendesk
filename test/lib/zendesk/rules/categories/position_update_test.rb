require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::Rules::Categories::PositionUpdate do
  include TestSupport::Rule::Helper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:categories) { RuleCategory.where(account: account).order(:position) }

  describe '#update_positions' do
    before do
      RuleCategory.destroy_all

      @category_1 = create_category(position: 1)
      @category_2 = create_category(position: 2)
      @category_3 = create_category(position: 3)

      @sql_queries = sql_queries do
        Zendesk::Rules::Categories::PositionUpdate.update_positions(updates, Trigger.name, account)
      end
    end

    describe 'when updating multiple categories' do
      let(:updates) do
        [
          {'id' => @category_1.id, 'position' => 3},
          {'id' => @category_3.id, 'position' => 2}
        ]
      end

      it 'updates the positions properly' do
        assert_equal [@category_2.id, @category_3.id, @category_1.id], categories.map(&:id)
      end
    end

    describe 'when there are multiple updates with same position' do
      let(:updates) do
        [
          {'id' => @category_1.id, 'position' => 3},
          {'id' => @category_2.id, 'position' => 3}
        ]
      end

      it 'only respects the last update with that position' do
        assert_equal [@category_1.id, @category_3.id, @category_2.id], categories.map(&:id)
      end
    end

    describe 'when there are multiple updates with same ID' do
      let(:updates) do
        [
          {'id' => @category_1.id, 'position' => 3},
          {'id' => @category_1.id, 'position' => 2}
        ]
      end

      it 'only respects the last update with that ID' do
        assert_equal [@category_2.id, @category_1.id, @category_3.id], categories.map(&:id)
      end
    end

    describe 'when the update position is out of bounds' do
      let(:updates) do
        [
          {'id' => @category_2.id, 'position' => -420},
          {'id' => @category_1.id, 'position' => 10_000_000}
        ]
      end

      it 'adds those rules to the end in order of position' do
        assert_equal [@category_3.id, @category_2.id, @category_1.id], categories.map(&:id)
      end
    end
  end
end
