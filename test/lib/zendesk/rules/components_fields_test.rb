require_relative '../../../support/test_helper'

SingleCov.not_covered!

describe 'ComponentsFields' do
  fixtures :accounts,
    :ticket_fields,
    :custom_field_options,
    :translation_locales,
    :users

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_agent)
    @context = Zendesk::Rules::Context.new(
      @account,
      @user,
      rule_type: :view,
      component_type: :condition,
      condition_type: :all
    )

    @validation_err = {}
  end

  describe '#component_types : selection, grouping, sorting' do
    describe 'for targets' do
      [
        'refute PICKCONDITION                     for selection,  grouping, sorting',

        'assert NOGROUP                           for grouping',
        'refute NOGROUP                           for selection,  sorting',

        'assert subject                           for selection,  grouping, sorting',
        'refute subject                           for condition',

        'assert submitter_id                      for selection,  grouping, sorting',
        'refute submitter_id                      for condition',

        'assert ticket_type_id                    for selection,  grouping, sorting',
        'assert priority_id                       for selection,  grouping, sorting',
        'assert group_id                          for selection,  grouping, sorting',
        'assert requester_id                      for selection,  grouping, sorting',
        'assert assignee_id                       for selection,  grouping, sorting',
        'assert organization_id                   for selection,  grouping, sorting',

        'refute current_tags                      for selection,  grouping, sorting',
        'refute description_includes_word         for selection,  grouping, sorting',

        'assert via_id                            for selection,  grouping, sorting',

        'refute resolution_time                   for selection,  grouping, sorting',
        'refute current_via_id                    for selection,  grouping, sorting',
        'refute recipient                         for selection,  grouping, sorting',

        'assert satisfaction_score                for selection,  grouping, sorting',

        'refute new                               for selection,  grouping, sorting',
        'refute open                              for selection,  grouping, sorting',
        'refute pending                           for selection,  grouping, sorting',
        'refute hold                              for selection,  grouping, sorting',
        'refute solved                            for selection,  grouping, sorting',
        'refute closed                            for selection,  grouping, sorting',

        'assert assigned_at                       for selection,  grouping, sorting',
        'assert updated_at                        for selection,  grouping, sorting',
        'assert requester_updated_at              for selection,  grouping, sorting',
        'assert assignee_updated_at               for selection,  grouping, sorting',
        'assert due_date                          for selection,  grouping, sorting',

        'refute until_due_date                    for selection,  grouping, sorting',
        'refute until_sla_next_breach_at          for selection,  grouping, sorting',
        'refute update_type                       for selection,  grouping, sorting',
        'refute subject_includes_word             for selection,  grouping, sorting',
        'refute comment_is_public                 for selection,  grouping, sorting',
        'refute comment_includes_word             for selection,  grouping, sorting',
        'refute reopens                           for selection,  grouping, sorting',
        'refute replies                           for selection,  grouping, sorting',
        'refute agent_stations                    for selection,  grouping, sorting',
        'refute group_stations                    for selection,  grouping, sorting',
        'refute first_reply                       for selection,  grouping, sorting',
        'refute first_resolution                  for selection,  grouping, sorting',
        'refute full_resolution                   for selection,  grouping, sorting',
        'refute agent_wait                        for selection,  grouping, sorting',
        'refute requester_wait                    for selection,  grouping, sorting',
        'refute on_hold_time_in_minutes           for selection,  grouping, sorting',
        'refute in_business_hours                 for selection,  grouping, sorting',
        'refute group_is_set_with_no_assignee     for selection,  grouping, sorting',
        'refute received_from,                    for selection,  grouping, sorting',
        'refute sent_to,                          for selection,  grouping, sorting',
        'refute collaboration_thread,             for selection,  grouping, sorting',

        'assert nice_id                           for selection,  sorting',
        'refute nice_id                           for grouping',

        'assert updated_by_type_id                for selection,  grouping, sorting',
        'refute updated_by_type_id                for condition',

        'assert created_at                        for selection,  grouping, sorting',
        'refute created_at                        for condition',

        'assert solved_at                         for selection,  grouping, sorting',
        'refute solved_at                         for condition',

        'assert locale_id                         for selection,  grouping, sorting',

        'refute requester_twitter_followers_count for selection,  grouping, sorting',
        'refute requester_twitter_statuses_count  for selection,  grouping, sorting',
        'refute requester_twitter_verified        for selection,  grouping, sorting',
        'refute role                              for selection,  grouping, sorting',

        # with features

        'with    ticket_forms                assert ticket_form_id     for selection,  grouping,  sorting',
        'without ticket_forms                refute ticket_form_id     for selection,  grouping,  sorting',

        'with    multibrand                  assert brand_id           for selection,  grouping,  sorting',
        'without multibrand                  refute brand_id           for selection,  grouping,  sorting',

        'with    service_level_agreements assert sla_next_breach_at for selection,  sorting',
        'with    service_level_agreements refute sla_next_breach_at for grouping',
        # refute
        'without service_level_agreements refute sla_next_breach_at for selection,  grouping,  sorting',

        'with    prefer_lotus                refute score              for condition,  grouping',
        'without prefer_lotus                refute score              for condition,  grouping',
        'with    prefer_lotus                refute score              for selection',
        'without prefer_lotus                refute score              for selection',
        'with    prefer_lotus                refute score              for sorting',
        'without prefer_lotus                refute score              for sorting',

        'with    prefer_lotus                refute status_id          for selection',
        'without prefer_lotus                refute status_id          for selection',
        'with    prefer_lotus                assert status_id          for grouping,   sorting',
        'without prefer_lotus                assert status_id          for grouping,   sorting'

      ].each do |predicate|
        if predicate.starts_with?('with')
          preposition, property, claim, field, = predicate.split
          component_types = predicate.split[5..-1].join(' ').split(', ')
        else
          claim, field, = predicate.split
          component_types = predicate.split[3..-1].join(' ').split(', ')
        end

        component_types.each do |component_type|
          str = "for type #{component_type}, for field #{field}"
          str = "#{preposition} #{property} enabled, " + str if preposition

          describe str do
            before do
              @context.options[:component_type] = component_type.to_sym

              setup_property(preposition, property) if preposition
            end

            qualifier = claim == 'assert' ? '' : 'not'
            it "#{qualifier}s contain #{field} target" do
              target_def = get_def(field)

              send(
                claim,
                target_def,
                "failed: #{preposition} #{property} #{claim} failed " \
                  "for #{field} for #{component_type.to_sym}"
              )
            end
          end
        end
      end
    end

    describe 'with custom fields #2' do
      [
        # FieldDate
        'assert active   ticket FieldDate date1 for selection, sorting',
        'refute inactive ticket FieldDate date1 for grouping',

        # FieldCheckbox
        'assert active   ticket FieldCheckbox checkbox1 for selection, grouping, sorting',
        'refute inactive ticket FieldCheckbox checkbox1 for selection, grouping, sorting',

        # FieldInteger
        'assert active   ticket FieldInteger integer1 for selection, grouping, sorting',
        'refute active   ticket FieldInteger integer1 for condition',
        'refute inactive ticket FieldInteger integer1 for selection, grouping, sorting, condition',

        # FieldDecimal
        'assert active   ticket FieldDecimal decimal1 for selection, grouping, sorting',
        'refute active   ticket FieldDecimal decimal1 for condition',
        'refute inactive ticket FieldDecimal decimal1 for selection, grouping, sorting, condition',

        # FieldText
        'assert active   ticket FieldText text1 for selection, grouping, sorting',
        'refute active   ticket FieldText text1 for condition',
        'refute inactive ticket FieldText text1 for selection, grouping, sorting, condition',

        # FieldRegexp
        'assert active   ticket FieldRegexp regexp1 for selection, grouping, sorting',
        'refute active   ticket FieldRegexp regexp1 for condition',
        'refute inactive ticket FieldRegexp regexp1 for selection, grouping, sorting, condition',

        # FieldTagger
        'assert active   ticket FieldTagger tagger1 for selection, grouping, sorting',
        'refute inactive ticket FieldTagger tagger1 for selection, grouping, sorting',

        # FieldTextarea
        'assert active   ticket FieldTextarea textarea1 for selection, grouping, sorting',
        'refute inactive ticket FieldTextarea textarea1 for selection, grouping, sorting',

        # CustomField::Text
        'refute active   user         cf_Text text1 for selection, grouping, sorting',
        'refute inactive user         cf_Text text1 for selection, grouping, sorting',
        'refute active   organization cf_Text text1 for selection, grouping, sorting',
        'refute inactive organization cf_Text text1 for selection, grouping, sorting',

        # CustomField::Decimal
        'refute active   user         cf_Decimal decimal1 for selection, grouping, sorting',
        'refute inactive user         cf_Decimal decimal1 for selection, grouping, sorting',
        'refute active   organization cf_Decimal decimal1 for selection, grouping, sorting',
        'refute inactive organization cf_Decimal decimal1 for selection, grouping, sorting',

        # CustomField::Integer
        'refute active   user         cf_Integer integer1 for selection, grouping, sorting',
        'refute inactive user         cf_Integer integer1 for selection, grouping, sorting',
        'refute active   organization cf_Integer integer1 for selection, grouping, sorting',
        'refute inactive organization cf_Integer integer1 for selection, grouping, sorting',

        # CustomField::Dropdown
        'refute active   user         cf_Dropdown dropdown1 for selection, grouping, sorting',
        'refute inactive user         cf_Dropdown dropdown1 for selection, grouping, sorting',
        'refute active   organization cf_Dropdown dropdown1 for selection, grouping, sorting',
        'refute inactive organization cf_Dropdown dropdown1 for selection, grouping, sorting',

        # CustomField::Date
        'refute active   user         cf_Date date1 for selection, grouping, sorting',
        'refute inactive user         cf_Date date1 for selection, grouping, sorting',
        'refute active   organization cf_Date date1 for selection, grouping, sorting',
        'refute inactive organization cf_Date date1 for selection, grouping, sorting',

        # CustomField::Checkbox
        'refute active   user         cf_Checkbox checkbox1 for selection, grouping, sorting',
        'refute inactive user         cf_Checkbox checkbox1 for selection, grouping, sorting',
        'refute active   organization cf_Checkbox checkbox1 for selection, grouping, sorting',
        'refute inactive organization cf_Checkbox checkbox1 for selection, grouping, sorting',

        # CustomField::Regexp
        'refute active   user         cf_Regexp regexp1 for selection, grouping, sorting',
        'refute inactive user         cf_Regexp regexp1 for selection, grouping, sorting',
        'refute active   organization cf_Regexp regexp1 for selection, grouping, sorting',
        'refute inactive organization cf_Regexp regexp1 for selection, grouping, sorting'

      ].each do |predicate|
        if predicate.starts_with?('with')
          preposition, property, claim, field_state, field_type, field_klass, field_name = predicate.split
          component_types = predicate.split[8..-1].join(' ').split(', ')
        else
          claim, field_state, field_type, field_klass, field_name, _field = predicate.split
          component_types = predicate.split[6..-1].join(' ').split(', ')
        end

        component_types.each do |component_type|
          field_str = "#{field_state} #{field_type} #{field_klass}: " \
            "#{field_name}"
          str = "for #{component_type}, for field: #{field_str}"
          str = "#{preposition} #{property} enabled, " + str if preposition

          describe str do
            before do
              @context.options[:component_type] = component_type.to_sym

              setup_property(preposition, property)
              @field_title = "#{field_state}-#{field_type}-#{field_name}"
              setup_field(field_klass, field_state, field_type)
            end

            qualifier = claim == 'assert' ? '' : 'not'

            it "#{qualifier}s contain #{@field_title} target" do
              target_def = get_def("field_title:#{@field_title}")

              send(
                claim,
                target_def,
                "failed: #{preposition} #{property} #{claim} failed for " \
                  " #{@field_title} for #{component_type.to_sym}"
              )
            end
          end
        end
      end
    end
  end

  private

  def setup_property(preposition, property)
    return unless preposition && preposition == 'with'

    if property == 'prefer_lotus'
      @account.settings.prefer_lotus = true
    else
      has_property = "has_#{property}?".to_sym
      @account.stubs(has_property => true)
    end
  end

  def setup_field(field_klass, field_state, field_type)
    case field_klass
    when 'FieldDate',
      'FieldInteger',
      'FieldDecimal',
      'FieldText',
      'FieldTextarea'

      klass = field_klass.constantize
      klass.create!(
        account: @account,
        is_active: (field_state == 'active'),
        title: @field_title
      )
    when 'FieldRegexp'
      FieldRegexp.create!(
        account: @account,
        is_active: (field_state == 'active'),
        title: @field_title,
        regexp_for_validation: "'[^']*'"
      )
    when 'FieldCheckbox'
      FieldCheckbox.create!(
        account: @account,
        is_active: (field_state == 'active'),
        title: @field_title,
        tag: 'tag1'
      )
    when 'FieldTagger'
      FieldTagger.create!(
        account: @account,
        is_active: (field_state == 'active'),
        title: @field_title,
        custom_field_options: [
          CustomFieldOption.new(name: 'foo', value: 'foo')
        ]
      )
    else
      klass = 'CustomField::' + /cf_(.+)/.match(field_klass)[1]
      klass = klass.constantize
      cf = klass.create(
        account: @account,
        key: field_klass.downcase.to_s,
        is_active: (field_state == 'active'),
        title: @field_title,
        owner: field_type.capitalize
      )

      if klass == CustomField::Dropdown
        cf.dropdown_choices.build(name: 'Choice 1', value: 'choice_1')
      elsif klass == CustomField::Regexp
        cf.regexp_for_validation = "'[^']*'"
      end
      cf.save!
    end
  end

  def generate_defs
    ZendeskRules::Definitions::Conditions.definitions_as_json(@context)
  end

  def parse_form(form_name)
    if (m = /field_title:(.+)/.match(form_name))
      [:title_for_field, m[1]]
    else
      [:value, form_name.to_s]
    end
  end

  def get_def(form_name)
    key, value = parse_form(form_name)

    generate_defs.detect do |d|
      d[key] == value
    end
  end
end
