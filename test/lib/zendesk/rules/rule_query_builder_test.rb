require_relative '../../../support/test_helper'
require_relative '../../../support/business_hours_test_helper'
require_relative '../../../support/rule_query_builder_test_helper'

SingleCov.covered!

describe Zendesk::Rules::RuleQueryBuilder do
  include CustomFieldsTestHelper
  include BusinessHoursTestHelper
  include RuleQueryBuilderTestHelper

  fixtures :accounts,
    :tickets,
    :users,
    :groups,
    :memberships,
    :ticket_fields,
    :custom_field_options,
    :organizations,
    :organization_memberships

  before do
    # don't use these fixtures
    CustomField::Field.without_arsi.delete_all
    CustomField::Value.without_arsi.delete_all
  end

  describe '#build_query' do
    let(:account) { accounts(:with_groups) }

    let(:empty_view) do
      View.new.tap do |v|
        v.account    = account
        v.definition = Definition.new
        v.output     = Output.create(columns: View::COLUMNS)
      end
    end

    it 'adds an :equals restriction on account_id' do
      builder = Zendesk::Rules::RuleQueryBuilder.
        new(account, users(:with_groups_admin), empty_view)

      query = builder.build_query
      compiled = query.compile(:sql)

      assert_equal(1, compiled[:account_id].size)
      assert_equal(
        ZendeskRules::RuleQuery::Restriction.
          new(:account_id).
          in([users(:with_groups_admin) .account_id, -1000]),
        compiled[:account_id][0]
      )
    end

    describe 'for admins' do
      before do
        builder = Zendesk::Rules::RuleQueryBuilder.
          new(account, users(:with_groups_admin), empty_view)

        @query = builder.build_query
      end

      it 'does not add any other restrictions' do
        assert_equal(2, @query.compile(:sql).size)
      end
    end

    describe 'for non-restricted agents' do
      before do
        builder = Zendesk::Rules::RuleQueryBuilder.
          new(accounts(:minimum), users(:minimum_agent), empty_view)

        @query = builder.build_query
      end

      it 'does not add any other restrictions' do
        assert_equal(2, @query.compile(:sql).size)
      end
    end

    describe 'for group restricted agents' do
      before do
        @user   = users(:with_groups_agent_groups_restricted)
        builder = Zendesk::Rules::RuleQueryBuilder.
          new(accounts(:with_groups), @user, empty_view)

        @query = builder.build_query
      end

      it 'adds an any restriction on group_ids and requester_id' do
        assert_includes(
          @query.compile(:sql).to_sql,
          format(
            '(tickets.requester_id = %d OR tickets.group_id = %d)',
            @user.id,
            @user.group_ids.first
          )
        )
      end
    end

    describe 'for organization restricted agents' do
      let(:user)    { users(:with_groups_agent_organization_restricted) }
      let(:builder) do
        Zendesk::Rules::RuleQueryBuilder.new(account, user, empty_view)
      end
      let(:query) { builder.build_query }
      let(:sql)   { query.compile(:sql).to_sql }

      describe 'with multiple organizations' do
        before { account.stubs(has_multiple_organizations_enabled?: true) }

        it 'adds an any restriction on organization_ids and requester_id' do
          sql = query.compile(:sql).to_sql

          assert_includes(
            sql,
            format('tickets.organization_id = %d', user.organization_ids.first)
          )

          assert_includes(sql, format('tickets.requester_id = %d', user.id))
        end

        it 'adds an invalid condition if user has no organizations' do
          user.stubs(organization_ids: [])

          assert_includes(sql, 'tickets.organization_id = 0')
        end
      end

      describe 'without multiple organizations' do
        before { account.stubs(has_multiple_organizations_enabled?: false) }

        it 'adds an any restriction on organization_id and requester_id' do
          user.organization_id = 1234

          assert_includes(
            sql,
            format('tickets.organization_id = %d', user.organization_id)
          )
          assert_includes(sql, format('tickets.requester_id = %d', user.id))
        end

        it 'adds an invalid condition if user has no organization' do
          user.organization_id = nil
          account.stubs(has_multiple_organizations_enabled?: true)
          user.stubs(:organization_ids).returns []
          assert_includes(sql, 'tickets.organization_id = 0')
        end
      end
    end

    describe 'for assigned restricted agents' do
      before do
        @user   = users(:with_groups_agent_assigned_restricted)
        builder = Zendesk::Rules::RuleQueryBuilder.
          new(accounts(:with_groups), @user, empty_view)

        @query = builder.build_query
      end

      it 'adds an any restriction on assignee_id and requester_id' do
        any_of_group = @query.compile(:sql)['_any_of'][0]
        assert_equal(2, any_of_group.size)
        assert_includes(
          any_of_group[:assignee_id],
          ZendeskRules::RuleQuery::Restriction.new(:assignee_id).eq(@user.id)
        )
        assert_includes(
          any_of_group[:requester_id],
          ZendeskRules::RuleQuery::Restriction.new(:requester_id).eq(@user.id)
        )
      end
    end
  end

  describe '.query_for' do
    let(:account) { accounts(:minimum) }
    let(:user)    { users(:with_groups_admin) }

    let(:definition) do
      Definition.new.tap do |defn|
        defn.conditions_all << definition_item
      end
    end

    let(:rule) do
      Automation.new(definition: definition).tap do |automation|
        automation.account = account
      end
    end

    let(:query) { Zendesk::Rules::RuleQueryBuilder.query_for(rule, user) }

    describe 'with the `has been offered to or rated by requester` condition' do
      let(:definition_item) do
        DefinitionItem.new(
          'satisfaction_score',
          'is',
          SatisfactionType.UNOFFERED
        )
      end

      it 'generates the proper query' do
        assert_includes query.compile(:sql).to_sql, 'satisfaction_score = 0'
      end
    end

    describe 'with the `satisfaction reason is Some other reason` condition' do
      let(:definition_item) do
        DefinitionItem.new(
          'satisfaction_reason_code',
          'is',
          Satisfaction::Reason::OTHER
        )
      end

      it 'generates the proper query' do
        assert_includes(
          query.compile(:sql).to_sql,
          'satisfaction_reason_code = 100'
        )
      end
    end

    describe 'with the `satisfaction probability less than 0.9` condition' do
      let(:definition_item) do
        DefinitionItem.new(
          'satisfaction_probability',
          'less_than',
          '0.9'
        )
      end

      it 'generates the proper query' do
        assert_includes(
          query.compile(:sql).to_sql,
          "`satisfaction_probability` < '0.9'"
        )
      end
    end

    describe 'with compound statements like `group_is_set_with_no_assignee`' do
      let(:definition) do
        Definition.new.tap do |defn|
          defn.conditions_all << DefinitionItem.new('status_id', 'is', 3)
          defn.conditions_any << DefinitionItem.new('priority_id', 'is', 1)
          defn.conditions_any << DefinitionItem.new(
            'group_is_set_with_no_assignee',
            'is',
            ''
          )
        end
      end

      let(:rule) do
        View.new(definition: definition).tap do |view|
          view.output  = Output.create(columns: View::COLUMNS)
          view.account = account
        end
      end

      it 'generates the proper query with ORs' do
        assert_includes(
          query.compile(:sql).to_sql,
          'AND tickets.status_id = 3 AND ((tickets.assignee_id IS NULL AND ' \
            "tickets.group_id = #{user.groups.first.id}) OR " \
            'tickets.priority_id = 1)'
        )
      end
    end
  end

  describe '.compiled_query_with_frozen_time_for' do
    it 'generates a proper query where time conditions are not a factor' do
      definition = Definition.new.tap do |defn|
        defn.conditions_all << DefinitionItem.new(
          'until_sla_next_breach_at',
          'greater_than',
          '4'
        )
        defn.conditions_all << DefinitionItem.new(
          'until_sla_next_breach_at',
          'less_than',
          '6'
        )
      end

      rule = Automation.new(definition: definition)
      rule.account = accounts(:minimum)

      compiled_query = Zendesk::Rules::RuleQueryBuilder.
        compiled_query_with_frozen_time_for(rule, users(:with_groups_admin))

      sql = compiled_query.to_sql
      # Always the same values, no matter when this test runs
      assert_includes(
        sql,
        'sla_breach_status <= 21600 AND sla_breach_status > 14400'
      )
    end
  end

  describe '#definition_item_condition' do
    describe 'As SQL querying on' do
      before do
        @account = accounts(:minimum)
        @current_user = @account.owner
        @definition_item = DefinitionItem.new('REPLACE_IN_TEST', 'is', '5')
        Timecop.freeze '2010-03-09 22:25:01 UTC'
      end

      describe 'sla next breach at' do
        let(:account) { accounts(:minimum) }

        before do
          @definition_item.source = 'sla_next_breach_at'
          @now = Time.new(2010, 3, 9, 22, 25, 1, '+00:00').to_i
        end

        before do
          Account.any_instance.stubs(business_hours_active?: true)

          add_schedule_intervals(
            account.schedules.create(
              name:      'Schedule',
              time_zone: account.time_zone
            )
          )
        end

        describe 'less_than_business_hours' do
          before do
            @definition_item.operator = 'less_than_business_hours'
            Zendesk::BusinessHoursSupport::Calculation.
              any_instance.
              expects(:calendar_hours_ago).
              with(5).
              returns(6)

            @hours_ago = Time.new(2010, 3, 9, 16, 25, 1, '+00:00').to_i
          end

          it 'query tickets that breached fewer than 5 business hours ago' do
            assert_equal(
              "(sla_breach_status <= #{@now} AND " \
                "sla_breach_status > #{@hours_ago})",
              generate_sql
            )
          end
        end
      end

      describe 'until sla next breach at' do
        let(:account) { accounts(:minimum) }

        before do
          @definition_item.source = 'until_sla_next_breach_at'

          Account.any_instance.stubs(business_hours_active?: true)

          add_schedule_intervals(
            account.schedules.create(
              name:      'Schedule',
              time_zone: account.time_zone
            )
          )
        end

        describe 'greater_than_business_hours' do
          before do
            @definition_item.operator = 'greater_than_business_hours'

            Zendesk::BusinessHoursSupport::Calculation.
              any_instance.
              expects(:calendar_hours_from_now).
              with(5).
              returns(6)
          end

          it 'queries for next breach at more than X business hours from now' do
            assert_equal(
              '(sla_breach_status < ' \
                "#{Zendesk::Sla::TicketStatus::MIN_SPECIAL_STATUS} AND " \
                'sla_breach_status > ' \
                "#{Time.new(2010, 3, 10, 4, 25, 1, '+00:00').to_i})",
              generate_sql
            )
          end
        end

        describe 'less_than_business_hours' do
          before do
            @definition_item.operator = 'less_than_business_hours'
            Zendesk::BusinessHoursSupport::Calculation.
              any_instance.
              expects(:calendar_hours_from_now).
              with(5).
              returns(6)
          end

          it 'queries for next breach at less than X business hours from now' do
            hours_from_now = Time.new(2010, 3, 10, 4, 25, 1, '+00:00').to_i
            now            = Time.new(2010, 3, 9, 22, 25, 1, '+00:00').to_i

            assert_equal(
              "(sla_breach_status <= #{hours_from_now} AND " \
                "sla_breach_status > #{now})",
              generate_sql
            )
          end
        end

        describe 'is_business_hours' do
          before do
            @definition_item.operator = 'is_business_hours'
          end

          it 'queries tickets with next breach X business hours from now' do
            assert_equal(
              "(sla_breach_status <= #{Time.utc(2010, 3, 10, 13, 25, 1).to_i}" \
                ' AND sla_breach_status > ' \
                "#{Time.utc(2010, 3, 10, 12, 25, 1).to_i})",
              generate_sql
            )
          end
        end
      end

      describe 'business hours' do
        let(:account) { accounts(:minimum) }

        before do
          Account.any_instance.stubs(business_hours_active?: true)

          add_schedule_intervals(
            account.schedules.create(
              name:      'Schedule',
              time_zone: account.time_zone
            )
          )
        end

        it 'querys tickets created X hours ago' do
          @definition_item.source   = 'NEW'
          @definition_item.operator = 'greater_than_business_hours'
          @definition_item.value = '5'
          expected_sql = "tickets.created_at < '2010-03-09 11:25:01'"
          assert_equal expected_sql, generate_sql
        end

        describe 'is conditions' do
          before do
            @definition_item.source   = 'NEW'
            @definition_item.operator = 'is_business_hours'
            @definition_item.value    = '12'
          end

          it 'returns business hours range 0' do
            # Monday 14:18 (as account in UTC+1)
            Timecop.freeze '2010-11-08 13:18 UTC'

            assert_equal(
              "(tickets.created_at <= '2010-11-05 09:18:00' AND " \
                "tickets.created_at > '2010-11-05 08:18:00')",
              generate_sql
            )
          end

          it 'returns non-business hours range 1' do
            # Monday 13:18 (as account in UTC+1)
            Timecop.freeze '2010-11-08 12:18 UTC'

            assert_equal(
              "(tickets.created_at <= '2010-11-05 08:18:00' AND " \
                "tickets.created_at > '2010-11-04 15:18:00')",
              generate_sql
            )
          end

          it 'returns business hours range 2' do
            # Monday 12:18 (as account in UTC+1)
            Timecop.freeze '2010-11-08 11:18 UTC'

            assert_equal(
              "(tickets.created_at <= '2010-11-04 15:18:00' AND " \
                "tickets.created_at > '2010-11-04 14:18:00')",
              generate_sql
            )
          end

          it 'returns business hours range 3' do
            # Monday 11:18 (as account in UTC+1)
            Timecop.freeze '2010-11-08 10:18 UTC'
            assert_equal(
              "(tickets.created_at <= '2010-11-04 14:18:00' AND " \
                "tickets.created_at > '2010-11-04 13:18:00')",
              generate_sql
            )
          end
        end
      end

      describe 'due_date' do
        let(:account) { accounts(:minimum) }

        before do
          @definition_item.source = 'due_date'
          # Due date are always saved as full hours (account noon time)
          Timecop.freeze(Time.new(2015, 'jan', 15, 8, 0).utc)

          Account.any_instance.stubs(business_hours_active?: true)

          add_schedule_intervals(
            account.schedules.create(
              name:      'Schedule',
              time_zone: account.time_zone
            )
          )
        end

        describe 'is' do
          it 'queries tickets that were due X hours ago' do
            assert_equal(
              '(tickets.ticket_type_id = 4 AND (tickets.due_date ' \
                "<= '2015-01-15 03:00:00' AND tickets.due_date > " \
                "'2015-01-15 02:00:00'))",
              generate_sql
            )
          end

          it 'queries nothing when definition value is missing' do
            @definition_item.value = ' '
            assert_nil generate_sql
          end
        end

        describe 'less_than' do
          before { @definition_item.operator = 'less_than' }

          it 'queries tickets that were due 5 hours ago' do
            assert_equal(
              '(tickets.ticket_type_id = 4 AND (tickets.due_date <= ' \
                "'2015-01-15 08:00:00' AND tickets.due_date > " \
                "'2015-01-15 03:00:00'))",
              generate_sql
            )
          end
        end

        describe 'less_than_business_hours' do
          before do
            @definition_item.operator = 'less_than_business_hours'

            Zendesk::BusinessHoursSupport::Calculation.
              any_instance.
              expects(:calendar_hours_ago).
              with(5).
              returns(6)
          end

          it 'query tickets that were due less than 5 business hours ago' do
            assert_equal(
              '(tickets.ticket_type_id = 4 AND (tickets.due_date <= ' \
                "'2015-01-15 08:00:00' AND tickets.due_date > " \
                "'2015-01-15 02:00:00'))",
              generate_sql
            )
          end
        end
      end

      describe 'until_due_date' do
        before do
          @definition_item.source = 'until_due_date'
          # Due date are always saved as full hours (account noon time)
          Timecop.freeze(Time.new(2015, 'jan', 15, 8, 0).utc)
        end

        describe 'with business hours' do
          let(:account) { accounts(:minimum) }

          before do
            Account.any_instance.stubs(business_hours_active?: true)

            add_schedule_intervals(
              account.schedules.create(
                name:      'Schedule',
                time_zone: account.time_zone
              )
            )
          end

          describe 'greater_than_business_hours' do
            before do
              @definition_item.operator = 'greater_than_business_hours'

              Zendesk::BusinessHoursSupport::Calculation.
                any_instance.
                expects(:calendar_hours_from_now).
                with(5).
                returns(6)
            end

            it 'queries for tickets due more than X business hours from now' do
              assert_equal(
                '(tickets.ticket_type_id = 4 AND tickets.due_date > ' \
                  "'2015-01-15 14:00:00')",
                generate_sql
              )
            end
          end

          describe 'less_than_business_hours' do
            before do
              @definition_item.operator = 'less_than_business_hours'

              Zendesk::BusinessHoursSupport::Calculation.
                any_instance.
                expects(:calendar_hours_from_now).
                with(5).
                returns(6)
            end

            it 'queries for tickets due fewer than X business hours from now' do
              assert_equal(
                '(tickets.ticket_type_id = 4 AND (tickets.due_date <= ' \
                  "'2015-01-15 14:00:00' AND tickets.due_date > " \
                  "'2015-01-15 08:00:00'))",
                generate_sql
              )
            end
          end

          describe 'is_business_hours' do
            before do
              @definition_item.operator = 'is_business_hours'
            end

            it 'querys tickets that are due 5 business hours from now' do
              assert_equal(
                '(tickets.ticket_type_id = 4 AND (tickets.due_date <= ' \
                  "'2015-01-15 13:00:00' AND tickets.due_date > " \
                  "'2015-01-15 12:00:00'))",
                generate_sql
              )
            end
          end
        end
      end
    end
  end

  describe 'View.incoming' do
    let(:account) { accounts(:with_groups) }
    let(:user)    { users(:with_groups_admin) }
    let(:query)   do
      Zendesk::Rules::RuleQueryBuilder.
        query_for(View.incoming(user), user)
    end

    it 'adds group_id is NULL' do
      query.compile(:sql).to_sql.must_include 'tickets.group_id IS NULL'
    end
  end

  describe 'while using sla_breach_status in condition' do
    let(:account) { accounts(:with_groups) }

    let(:view) do
      View.new.tap do |v|
        v.account = account
        v.definition = Definition.new
        v.definition.conditions_all << DefinitionItem.new(
          'requester_id',
          'is',
          2
        )
        v.output = Output.create({})
        v.output.order = 'sla_next_breach_at'
        v.output.order_asc = false
        v.output.group = 'created_at'
        v.output.type = 'table'
      end
    end

    it 'removes duplicated created_at field' do
      builder = Zendesk::Rules::RuleQueryBuilder.
        new(account, users(:with_groups_admin), view)

      query = builder.build_query
      compiled = query.compile(:sql)

      assert_equal(
        [
          '`tickets`.`created_at` DESC', '`tickets`.`sla_breach_status` DESC',
          '`tickets`.`nice_id` DESC'
        ],
        compiled.order
      )

      assert_equal(
        [
          '`tickets`.`created_at`',
          '`tickets`.`sla_breach_status`',
          '`tickets`.`nice_id`'
        ],
        compiled.order_fields
      )

      compiled = query.compile(:mql)
      assert_equal(
        [
          {field: 'created_at', desc: true},
          {field: 'sla_breach_status', desc: true},
          {field: 'nice_id', desc: true}
        ],
        compiled.order
      )

      assert_equal(
        %w[created_at sla_breach_status nice_id],
        compiled.order_fields
      )
    end
  end
end
