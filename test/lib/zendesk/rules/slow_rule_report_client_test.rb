require_relative '../../../support/test_helper'
require_relative '../../../support/rules_test_helper'

SingleCov.covered! uncovered: 1

describe Zendesk::Rules::SlowRuleReportClient do
  fixtures :accounts, :rules

  let(:account) { accounts(:minimum) }
  let(:rule) { rules(:view_for_minimum_agent) }
  let(:headers) { {'Content-Type' => 'application/json'} }

  before do
    stub_request(:post, /slow_rule_reports\/create/).
      to_return(status: 200, body: {'count' => 1}.to_json, headers: headers)

    stub_request(:post, /slow_rule_reports\/show/).
      to_return(status: 200, body: {'count' => 1}.to_json, headers: headers)

    stub_request(:post, /slow_rule_reports\/show_many/).
      to_return(status: 200, body: {'1' => {'count' => 0}}.to_json, headers: headers)

    stub_request(:post, /slow_rule_reports\/destroy/).
      to_return(status: 200, body: {'deleted_count' => 1}.to_json, headers: headers)

    stub_request(:post, /slow_rule_reports\/destroy_all/).
      to_return(status: 200, body: {'deleted_count' => 1}.to_json, headers: headers)

    stub_request(:post, /slow_rule_reports\/all/).
      to_return(
        status: 200,
        body: {'occam/slow_rule/v1/1/1/1563914686' => {'count' => 0}}.to_json,
        headers: headers
      )
  end

  let(:client) do
    Zendesk::Rules::SlowRuleReportClient.new(account)
  end

  describe 'slow rule report endpoints' do
    describe '#create_report' do
      describe 'request is successful' do
        let(:limits) { {limits: {count: 1}} }
        let(:limits) { {} }
        let(:response) { {'count' => 1} }
        it 'creates report for a rule and adds audit report' do
          assert_difference 'CIA::Event.count', 1 do
            assert_equal response, client.create_report(rule, limits)
          end
        end
      end

      describe 'request is not successful' do
        let(:error_msg) do
          "Needs a limits hash like {'find': 0, 'count': 1}"
        end

        before do
          stub_request(:post, /slow_rule_reports\/create/).
            to_return(
              status: 200,
              body: "Needs a limits hash like {'find': 0, 'count': 1}",
              headers: headers
            )
        end

        it 'throws error' do
          assert_raises Occam::Client::ClientError, error_msg do
            client.create_report(rule, {})
          end
        end
      end
    end

    describe '#show_report' do
      let(:response) { {'count' => 1} }
      it 'shows report for a rule' do
        assert_equal response, client.show_report(rule)
      end
    end

    describe '#show_many' do
      let(:rules_param) { [rules(:view_for_minimum_agent)] }
      let(:response) { {'1' => {'count' => 0}} }
      it 'shows report for the rules' do
        assert_equal response, client.show_many_report(rules_param)
      end
    end

    describe '#destroy_report' do
      describe 'request is successful' do
        let(:response) { {'deleted_count' => 1} }

        it 'destroys report of the rule and adds audit report' do
          assert_difference 'CIA::Event.count', 1 do
            assert_equal response, client.destroy_report(rule)
          end
        end
      end
    end

    describe '#destroy_all_report' do
      describe 'request is successful' do
        let(:response) { {'deleted_count' => 1} }

        it 'destroys all the report for the account and adds audit report' do
          assert_difference 'CIA::Event.count', 1 do
            assert_equal response, client.destroy_all
          end
        end
      end
    end

    describe '#all_reports' do
      let(:response) do
        {'occam/slow_rule/v1/1/1/1563914686' => {'count' => 0}}
      end

      it 'shows all report for the account' do
        assert_equal response, client.all_reports
      end
    end
  end
end
