require_relative '../../../support/test_helper'
require_relative '../../../support/rules_test_helper'

SingleCov.covered!

describe Zendesk::Rules::BrokenOccamTicketCount do
  let(:now) { Time.now }
  let(:count) { 8 }
  subject do
    Zendesk::Rules::BrokenOccamTicketCount.new(123)
  end

  it 'instantiates with default values' do
    assert_equal 123, subject.rule_id
    assert_equal ::View::BROKEN_COUNT, subject.value
    assert(subject.fresh)
    assert_equal now.to_s, subject.updated_at
    assert_equal 'poll', subject.refresh
    assert_equal Zendesk::Rules::BrokenOccamTicketCount::POLL_WAIT, subject.poll_wait

    assert_equal '...', subject.pretty_print
    assert_equal false, subject.is_updating?
    assert subject.send(:age) > 0
  end
end
