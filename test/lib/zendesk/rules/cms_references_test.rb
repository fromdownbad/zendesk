require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe 'Zendesk::Rules::CmsReferences' do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :users,
    :rules

  let(:account) { accounts(:minimum) }

  describe '#create_cms_references' do
    let(:admin) { users(:with_groups_admin) }

    let!(:cms_text) do
      account.cms_texts.create(
        name:                'Text',
        fallback_attributes: {
          is_fallback:           true,
          nested:                true,
          account_id:            1,
          value:                 'The fallback value',
          translation_locale_id: 1
        }
      )
    end

    describe 'when a rule is updated' do
      let(:action_subject) { 'notification_user' }

      let(:definition) { build_trigger_definition }
      let(:rule)       { create_trigger(definition: definition) }

      before do
        rule.definition = build_trigger_definition.tap do |definition|
          definition.actions.push(
            DefinitionItem.new(action_subject, nil, [action_value])
          )
        end
      end

      describe 'and it does not contain a CMS reference' do
        let(:action_value) { 'test' }

        before { rule.save! }

        before_should 'call `create_cms_references`' do
          rule.expects(:create_cms_references).at_least_once
        end

        it 'does not create a CMS rule reference' do
          refute cms_text.references.find_by_rule_id(rule.id).present?
        end

        should_not_change 'the number of CMS references' do
          cms_text.references.count
        end

        should_not_change 'the number of rule references' do
          rule.rule_references.count
        end
      end

      describe 'and it contains a CMS reference' do
        let(:action_value) { '{{dc.text}}' }

        describe 'and the action is a reference action' do
          let(:action_subject) { 'notification_user' }

          before { rule.save! }

          it 'creates a CMS rule reference' do
            assert cms_text.references.find_by_rule_id(rule.id).present?
          end

          should_change('the number of CMS references', by: 1) do
            cms_text.references.count
          end

          should_change('the number of rule references', by: 1) do
            rule.rule_references.count
          end
        end

        describe 'and the action is not a reference action' do
          let(:action_subject) { 'set_tags' }

          should_not_change 'the number of CMS references' do
            cms_text.references.count
          end

          should_not_change 'the number of rule references' do
            rule.rule_references.count
          end
        end
      end

      describe 'and it already contained a CMS reference' do
        let(:definition) do
          Definition.new.tap do |definition|
            definition.conditions_all.push(
              DefinitionItem.new('status_id', 'is', 1)
            )
            definition.actions.push(
              DefinitionItem.new('notification_user', nil, ['{{dc.text}}'])
            )
          end
        end

        describe 'and it still contains CMS references' do
          let(:action_value) { 'Test: {{dc.text}}' }

          before { rule.save! }

          should_not_change 'the number of CMS references' do
            cms_text.references.count
          end

          should_not_change 'the number of rule references' do
            rule.rule_references.count
          end

          it 'has a CMS reference' do
            assert cms_text.references.any?
          end
        end

        describe 'and it no longer contains CMS references' do
          let(:action_value) { 'Test value' }

          before { rule.save! }

          should_change('the number of CMS references', by: -1) do
            cms_text.references.count
          end

          should_change('the number of rule references', by: -1) do
            rule.rule_references.count
          end
        end
      end
    end
  end
end
