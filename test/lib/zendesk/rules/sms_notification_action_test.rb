require_relative '../../../support/test_helper'
require_relative '../../../support/rule'
require_relative '../../../support/rules_test_helper'
require 'preload'

SingleCov.covered! uncovered: 1

describe Zendesk::Rules::SmsNotificationAction do
  include TestSupport::Rule::Helper

  fixtures :tickets,
    :users,
    :translation_locales,
    :user_identities,
    :monitored_twitter_handles,
    :targets,
    :rules,
    :organizations,
    :ticket_fields,
    :custom_field_options,
    :channels_user_profiles,
    :account_settings,
    :organization_memberships,
    :brands

  resque_inline false

  describe '.valid_definition_item?' do
    let(:action_value) { ['assignee_id', 1, 'message body'] }
    let(:item)         { DefinitionItem.new(source, nil, action_value) }

    describe 'when passed a definition item' do
      %w[notification_sms_user notification_sms_group].each do |source|
        describe "and the source is `#{source}`" do
          let(:source) { source }

          it 'returns true' do
            assert(
              Zendesk::Rules::SmsNotificationAction.valid_definition_item?(item)
            )
          end
        end
      end

      describe 'with a source that is not whitelisted' do
        let(:source) { 'nope' }

        it 'returns false' do
          refute(
            Zendesk::Rules::SmsNotificationAction.valid_definition_item?(item)
          )
        end
      end

      describe 'and 3 values in its value array' do
        let(:source)       { 'notification_sms_user' }
        let(:action_value) { ['assignee_id', 1, 'message body'] }

        it 'returns true' do
          assert(
            Zendesk::Rules::SmsNotificationAction.valid_definition_item?(item)
          )
        end
      end

      describe 'and when notification body is empty' do
        let(:source)       { 'notification_sms_user' }
        let(:action_value) { ['assignee_id', 1, ''] }

        it 'returns false' do
          refute(
            Zendesk::Rules::SmsNotificationAction.valid_definition_item?(item)
          )
        end
      end

      describe 'and does NOT have 3 values in its value array' do
        let(:source)       { 'notification_sms_user' }
        let(:action_value) { ['assignee_id', 1] }

        it 'returns false' do
          refute(
            Zendesk::Rules::SmsNotificationAction.valid_definition_item?(item)
          )
        end
      end
    end
  end

  describe '#apply' do
    let(:rule)           { create_trigger }
    let(:author)         { users(:minimum_admin) }
    let(:ticket)         { tickets(:minimum_1) }
    let(:notified_users) { ticket.audit.events.map(&:value).flatten }
    let(:system_user)    { users(:systemuser) }
    let(:agent)          { users(:minimum_agent) }
    let(:account)        { accounts(:minimum) }
    let(:via_reference)  { {via_id: ViaType.RULE, via_reference_id: 1} }

    let(:action) do
      Zendesk::Rules::SmsNotificationAction.new(
        ticket:          ticket,
        user:            ticket.current_user,
        rule:            rule,
        phone_number_id: 1,
        body:            'test'
      )
    end

    before do
      rule.stubs(via_reference: via_reference)

      ticket.will_be_saved_by(author)
    end

    describe 'when an agent is not suspended and is deliverable' do
      before { action.apply(agent.id) }

      it 'includes the agent in the list of recipients' do
        assert_includes notified_users, agent.id
      end
    end

    describe 'when an agent is suspended' do
      before do
        agent.suspend!
        action.apply(agent.id)
      end

      it 'excludes the agents from the list of recipients' do
        refute_includes notified_users, agent.id
      end
    end

    describe 'when an agent has a truthy suspension setting' do
      before do
        agent.suspended = 1
        agent.save!

        action.apply(agent.id)
      end

      it 'excludes the agents from the list of recipients' do
        refute_includes notified_users, agent.id
      end
    end

    describe 'when there is a system user' do
      before { action.apply([agent.id, system_user.id]) }

      it 'does not include them in the list of recipients' do
        refute_includes notified_users, system_user.id
      end
    end

    describe 'when applied' do
      before { action.apply(agent.id) }

      it 'records audit event with `RULE`' do
        assert_equal ViaType.RULE, ticket.audit.events.last.via_id
      end

      describe 'when executed with rule revision' do
        let(:via_reference) do
          {via_id: ViaType.RULE_REVISION, via_reference_id: 1}
        end

        it 'records audit event with `RULE_REVISION`' do
          assert_equal ViaType.RULE_REVISION, ticket.audit.events.last.via_id
        end
      end
    end
  end
end
