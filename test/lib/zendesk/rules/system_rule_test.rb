require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::SystemRule do
  fixtures :accounts,
    :users,
    :subscriptions

  let(:account) { accounts(:minimum) }

  describe 'when the rule has a feature identifer' do
    before do
      @rule = Rule.new(
        title:              'Rule with feature identifier',
        definition:         Definition.new,
        owner:              account,
        is_active:          true,
        feature_identifier: 'YammerIntegration'
      )
    end

    it 'is a system rule' do
      assert @rule.is_system_rule?
    end

    it 'does not be a system rule' do
      @rule.update_attribute(:feature_identifier, '0')

      refute @rule.is_system_rule?
    end

    it 'is readonly' do
      @rule.update_attribute(:is_locked, true)

      assert_raise ActiveRecord::ReadOnlyRecord do
        @rule.update_attribute(:title, "This shouldn't be possible")
      end
    end

    it 'is deletable' do
      @rule.update_attribute(:is_locked, true)

      assert @rule.destroy
    end
  end

  describe 'when the rule does not have a feature identifier' do
    before do
      @rule = Rule.new(
        title:      'Rule without a feature identifier',
        definition: Definition.new,
        owner:      account,
        is_active:  true
      )
    end

    it 'does not be a system rule' do
      refute @rule.is_system_rule?
    end
  end
end
