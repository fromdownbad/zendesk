require_relative '../../../support/test_helper'
require_relative '../../../support/rule'
require_relative '../../../support/collaboration_settings_test_helper'

SingleCov.covered! uncovered: 35

describe Zendesk::Rules::MacroApplication do
  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:actions) { [] }
  let(:ticket)  { nil }
  let(:user)    { users(:minimum_admin) }

  let(:macro) do
    create_macro(
      definition: Definition.new.tap do |definition|
        definition.actions.concat(actions)
      end
    )
  end

  let(:macro_application) do
    Zendesk::Rules::MacroApplication.new(macro, user, ticket)
  end

  def self.follower_or_collaborations_tests
    describe 'when there are multiple actions with the source `cc`' do
      let(:actions) do
        [
          DefinitionItem.new('cc', 'is', users(:minimum_admin).id),
          DefinitionItem.new('cc', 'is', users(:minimum_agent).id)
        ]
      end

      it 'returns a list of all the users' do
        assert_same_elements(
          [users(:minimum_admin).id, users(:minimum_agent).id],
          subject.map { |user| user[:id] }
        )
      end

      it "presents all the users' names" do
        names = [users(:minimum_admin), users(:minimum_agent)].map do |user|
          # Any action can be used to call #lookup_name.
          actions.sample.lookup_name(account.agents, user.id)
        end

        assert_same_elements names, subject.map { |user| user[:name] }
      end

      describe "when an `id` in an action's source is not a valid user" do
        let(:actions) { [DefinitionItem.new('cc', 'is', -1)] }

        it 'does not add any users' do
          assert_nil subject
        end
      end

      describe 'when there are duplicate users in cc action values' do
        let(:actions) do
          [
            DefinitionItem.new('cc', 'is', user.id),
            DefinitionItem.new('cc', 'is', user.id)
          ]
        end

        it 'returns a uniq list of all the users' do
          assert_equal [user.id], [subject.first[:id]]
        end
      end
    end
  end

  def self.collaboration_cced_tests
    let(:actions) { [DefinitionItem.new('cc', 'is', user.id)] }

    describe 'when a ticket is present' do
      let(:ticket) { tickets(:minimum_1) }

      describe 'when a user in a cc action value is a collaborator' do
        before { ticket.collaborations.build(user: user) }

        it 'marks the user as `cced`' do
          assert subject.first[:cced]
        end
      end

      describe 'when a user in a cc action value is a not a collaborator' do
        before { ticket.collaborations.clear }

        it 'does not mark the user as `cced`' do
          refute subject.first[:cced]
        end
      end
    end

    describe 'when a ticket is not present' do
      let(:ticket) { nil }

      it 'does not mark the user as `cced`' do
        refute subject.first[:cced]
      end
    end
  end

  describe '#run' do
    let(:comment) { macro_application.run['comment'] }

    describe 'when the macro has a Markdown comment' do
      let(:actions) { [DefinitionItem.new('comment_value', nil, ['Hi there'])] }

      it 'has the proper keys' do
        assert_equal %w[scoped_value value], comment.keys
      end

      it 'has the correct comment value' do
        assert_equal 'Hi there', comment['value']
      end

      it 'has the correct scoped value' do
        assert_equal [['channel:all', 'Hi there']], comment['scoped_value']
      end

      describe 'and it contains a link' do
        let(:ticket) { tickets(:minimum_1) }
        let(:link)   { ticket.url(for_agent: true) }

        let(:actions) do
          [
            DefinitionItem.new(
              'comment_value',
              nil,
              ['Link: {{ticket.link}}']
            )
          ]
        end

        it 'renders the comment value properly' do
          assert_equal "Link: #{link}", comment['value']
        end
      end
    end

    describe 'when the macro has a Markdown comment with a channel' do
      let(:actions) do
        [
          DefinitionItem.new(
            'comment_value',
            nil,
            ['channel:all', 'Hi there']
          )
        ]
      end

      it 'has the proper keys' do
        assert_equal %w[scoped_value value], comment.keys
      end

      it 'has the correct comment value' do
        assert_equal 'Hi there', comment['value']
      end

      it 'has the correct scoped value' do
        assert_equal [['channel:all', 'Hi there']], comment['scoped_value']
      end
    end

    describe 'when the macro has multiple Markdown comments' do
      let(:macro) do
        account.macros.build(
          definition: Definition.new.tap do |definition|
            definition.actions.concat(actions)
          end
        )
      end

      let(:actions) do
        [
          DefinitionItem.new('comment_value', nil, ['Hi there']),
          DefinitionItem.new('comment_value', nil, ['Bye bye birdie'])
        ]
      end

      before { macro.save!(validate: false) }

      it 'has the proper keys' do
        assert_equal %w[scoped_value value], comment.keys
      end

      it 'has the correct comment value' do
        assert_equal 'Hi thereBye bye birdie', comment['value']
      end

      it 'has the correct scoped value' do
        assert_equal(
          [['channel:all', 'Hi there'], ['channel:all', 'Bye bye birdie']],
          comment['scoped_value']
        )
      end
    end

    describe 'when the macro has an HTML comment' do
      let(:ticket) { tickets(:minimum_1) }

      let(:actions) do
        [DefinitionItem.new('comment_value_html', nil, ['<p>Hi there</p>'])]
      end

      it 'has the proper keys' do
        assert_equal %w[html_value], comment.keys
      end

      it 'has the correct comment value' do
        assert_equal '<p>Hi there</p>', comment['html_value']
      end

      describe 'and it contains a link' do
        let(:link) { ticket.url(for_agent: true) }

        let(:actions) do
          [
            DefinitionItem.new(
              'comment_value_html',
              nil,
              ['<p>Link: {{ticket.link}}</p>']
            )
          ]
        end

        it 'renders the comment value properly' do
          assert_equal(
            "<p>Link: <a href=\"#{link}\" rel=\"noreferrer\">#{link}</a></p>",
            comment['html_value']
          )
        end
      end

      describe 'and it contains an email address' do
        let(:email) { user.email }

        let(:actions) do
          [
            DefinitionItem.new(
              'comment_value_html',
              nil,
              ['<p>Email: {{current_user.email}}</p>']
            )
          ]
        end

        it 'renders the comment value properly' do
          assert_equal(
            "<p>Email: <a href=\"mailto:#{email}\">#{email}</a></p>",
            comment['html_value']
          )
        end
      end

      describe 'and it contains dynamic content' do
        let(:locale) { translation_locales(:english_by_zendesk) }

        let(:actions) do
          [
            DefinitionItem.new(
              'comment_value_html',
              nil,
              ['<p>DC:</p><p>{{dc.test_content}}</p>']
            )
          ]
        end

        before do
          Cms::Text.create!(
            name:                'test_content',
            account:             account,
            fallback_attributes: {
              is_fallback:           true,
              nested:                true,
              value:                 cms_value,
              translation_locale_id: locale.id
            }
          )
        end

        describe 'and the dynamic content contains multiple lines' do
          let(:cms_value) { "Line 1\n\n\nLine 2\nLine 3" }

          it 'renders the comment value properly' do
            assert_equal(
              '<p>DC:</p><p>Line 1<br /><br />Line 2<br />Line 3</p>',
              comment['html_value']
            )
          end
        end

        describe 'and the dynamic content contains markdown' do
          let(:cms_value) { "### Separate lines\n**YO**" }

          it 'renders the comment value properly' do
            assert_equal(
              '<p>DC:</p><p><h3 dir="auto">Separate lines</h3><br />' \
                '<strong>YO</strong></p>',
              comment['html_value']
            )
          end
        end

        describe 'and the dynamic content is nested' do
          let(:cms_value) { "Test\n{{dc.test_content}}" }

          it 'renders the comment value properly' do
            assert_equal(
              '<p>DC:</p><p>Test<br />Test<br />Test<br />Test<br />Test</p>',
              comment['html_value']
            )
          end
        end

        describe_with_arturo_disabled(:macro_render_dc_markdown) do
          let(:cms_value) { "Line 1\n\n\nLine 2\nLine 3" }

          it 'renders the comment value using the simple formatter' do
            assert_equal(
              '<p>DC:</p><p><p>Line 1</p><p>Line 2<br />Line 3</p></p>',
              comment['html_value']
            )
          end
        end
      end

      describe 'and the ticket is not present' do
        let(:ticket) { nil }

        describe_with_arturo_enabled(:dc_context_for_macro_application) do
          let(:actions) do
            [DefinitionItem.new('comment_value_html', nil, ['<p>Hi there</p>'])]
          end

          it 'has the proper keys' do
            assert_equal %w[html_value], comment.keys
          end

          it 'has the correct comment value' do
            assert_equal '<p>Hi there</p>', comment['html_value']
          end
        end

        describe 'and the macro contains dynamic content' do
          let(:locale) { translation_locales(:english_by_zendesk) }

          let(:actions) do
            [
              DefinitionItem.new(
                'comment_value_html',
                nil,
                ['<p>DC:</p><p>{{dc.test_content}}</p>']
              )
            ]
          end

          before do
            Cms::Text.create!(
              name:                'test_content',
              account:             account,
              fallback_attributes: {
                is_fallback:           true,
                nested:                true,
                value:                 cms_value,
                translation_locale_id: locale.id
              }
            )
          end

          describe_with_arturo_enabled(:dc_context_for_macro_application) do
            describe 'and the dynamic content contains multiple lines' do
              let(:cms_value) { "Line 1\n\n\nLine 2\nLine 3" }

              it 'renders the comment value properly' do
                assert_equal(
                  '<p>DC:</p><p>Line 1<br /><br />Line 2<br />Line 3</p>',
                  comment['html_value']
                )
              end
            end

            describe 'and the dynamic content contains markdown' do
              let(:cms_value) { "### Separate lines\n**YO**" }

              it 'renders the comment value properly' do
                assert_equal(
                  '<p>DC:</p><p><h3 dir="auto">Separate lines</h3><br />' \
                    '<strong>YO</strong></p>',
                  comment['html_value']
                )
              end
            end

            describe 'and the dynamic content is nested' do
              let(:cms_value) { "Test\n{{dc.test_content}}" }

              it 'renders the comment value properly' do
                assert_equal(
                  '<p>DC:</p><p>Test<br />Test<br />Test<br />Test<br />Test</p>',
                  comment['html_value']
                )
              end
            end
          end

          describe_with_arturo_disabled(:dc_context_for_macro_application) do
            let(:cms_value) { "Line 1\n\n\nLine 2\nLine 3" }

            it 'returns the macro comment as-is' do
              assert_equal(
                '<p>DC:</p><p>{{dc.test_content}}</p>',
                comment['html_value']
              )
            end
          end
        end
      end
    end

    describe 'when applying a macro with comment action to a ticket' do
      let(:ticket) { tickets(:minimum_1) }

      let(:actions) do
        [
          DefinitionItem.new(
            'comment_value_html',
            nil,
            ['Hi {{ticket.requester}}, sup']
          )
        ]
      end

      it 'evaluates the placeholders' do
        assert_equal 'Hi Author Minimum, sup', comment['html_value']
      end
    end

    describe 'when the macro also has a comment-mode action' do
      let(:actions) do
        [
          DefinitionItem.new('comment_value_html', nil, ['Hi there']),
          DefinitionItem.new('comment_mode_is_public', nil, [true])
        ]
      end

      it 'has the proper keys' do
        assert_equal %w[is_public html_value], comment.keys
      end

      it 'has the correct comment mode' do
        assert(comment['is_public'])
      end

      describe 'and the comment-mode is public' do
        let(:actions) do
          [DefinitionItem.new('comment_mode_is_public', nil, ['true'])]
        end

        it 'returns with no change if the user is a light agent' do
          user.stubs(:can?).with(:publicly, Comment).returns(false)
          assert_nil comment
        end
      end

      describe 'and the comment-mode is private' do
        let(:actions) do
          [DefinitionItem.new('comment_mode_is_public', nil, ['false'])]
        end
        let(:ticket) { tickets(:minimum_1) }

        describe 'and the ticket is new' do
          before { ticket.stubs(new_record?: true) }

          describe 'and without the `first_comment_private` setting enabled' do
            before do
              account.expects(is_first_comment_private_enabled: false)
            end

            it 'returns with no change' do
              assert_nil comment
            end
          end

          describe 'and with the `first_comment_private` setting enabled' do
            before do
              ticket.stubs(new_record?: true)
              account.expects(is_first_comment_private_enabled: true)
            end

            it 'returns the private comment mode' do
              assert_equal 'false', comment['is_public']
            end
          end
        end

        describe 'and the ticket is not new' do
          before { ticket.stubs(new_record?: false) }

          it 'returns the private comment mode' do
            assert_equal 'false', comment['is_public']
          end
        end
      end
    end

    describe 'when the macro does not have a comment and has a comment mode' do
      let(:actions) do
        [DefinitionItem.new('comment_mode_is_public', nil, [false])]
      end

      it 'has the proper keys' do
        assert_equal %w[is_public], comment.keys
      end

      it 'has the correct comment mode' do
        refute comment['is_public']
      end
    end

    describe 'when the macro has a multiselect ticket field action' do
      let(:ticket) { tickets(:minimum_1) }
      let(:multiselect_wombat_field) do
        FieldMultiselect.create!(
          account: accounts(:minimum),
          title: 'Multiselect Wombat',
          custom_field_options: [
            CustomFieldOption.new(name: 'Wombats', value: 'wombats'),
            CustomFieldOption.new(name: 'Giraffes', value: 'giraffes'),
            CustomFieldOption.new(name: 'Tazzmanian Devils', value: 'tazzies'),
            CustomFieldOption.new(name: 'Lemurs', value: 'lemurs')
          ]
        )
      end
      let(:tf_entry) do
        ticket.
          ticket_field_entries.
          detect { |t| t.ticket_field_id == multiselect_wombat_field.id }
      end
      let(:wombat_option) do
        ticket.account.custom_field_options.detect { |o| o.value == 'wombats' }
      end
      let(:tazzie_option) do
        ticket.account.custom_field_options.detect { |o| o.value == 'tazzies' }
      end
      let(:actions) do
        [
          DefinitionItem.new(
            "ticket_fields_#{multiselect_wombat_field.id}",
            nil,
            wombat_option.id
          )
        ]
      end

      before do
        ticket.fields = {multiselect_wombat_field.id => %w[lemurs giraffes]}
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!
      end

      it 'adds the requested value to the field and returns the new complete value of the field' do
        assert_equal 'giraffes lemurs', tf_entry.value

        assert_equal(
          %w[giraffes lemurs wombats],
          macro_application.
            run["ticket_fields_#{multiselect_wombat_field.id}"]
        )
      end

      describe 'with many actions on the multiselect field' do
        let(:actions) do
          [
            DefinitionItem.new(
              "ticket_fields_#{multiselect_wombat_field.id}",
              nil,
              wombat_option.id
            ),
            DefinitionItem.new(
              "ticket_fields_#{multiselect_wombat_field.id}",
              nil,
              tazzie_option.id
            )
          ]
        end

        it 'adds multiple requested values to the field and returns the new complete value of the field' do
          assert_equal 'giraffes lemurs', tf_entry.value
          assert_equal(
            %w[giraffes lemurs wombats tazzies],
            macro_application.
              run["ticket_fields_#{multiselect_wombat_field.id}"]
          )
        end
      end
    end

    describe 'respecting active and inactive ticket fields' do
      let(:ticket) { tickets(:minimum_1) }
      let(:tagger_field) do
        FieldTagger.create!(
          account: accounts(:minimum),
          title: 'FieldTagger test',
          custom_field_options: [
            CustomFieldOption.new(name: 'name_a', value: 'value_a'),
            CustomFieldOption.new(name: 'name_b', value: 'value_b')
          ]
        )
      end
      let(:tf_entry) do
        ticket.
          ticket_field_entries.
          detect { |entry| entry.ticket_field_id == tagger_field.id }
      end

      let(:action_option) do
        ticket.
          account.
          custom_field_options.detect { |option| option.value == 'value_b' }
      end

      let(:actions) do
        [
          DefinitionItem.new(
            "ticket_fields_#{tagger_field.id}",
            nil,
            action_option.id
          )
        ]
      end

      let!(:local_macro_application) do
        Zendesk::Rules::MacroApplication.new(macro, user, ticket)
      end

      before do
        ticket.fields = {tagger_field.id => 'value_a'}
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!
      end

      it 'with an active ticket field, the value is updated' do
        assert_equal(
          'value_b',
          local_macro_application.run["ticket_fields_#{tagger_field.id}"]
        )
      end

      it 'with an inactive ticket field, the value is not updated' do
        tagger_field.toggle!
        assert_empty(local_macro_application.run)
      end
    end
  end

  describe 'when the macro has a side_conversation' do
    let(:ticket) { tickets(:minimum_1) }

    let(:sc_subject) { 'The subject' }
    let(:sc_message) { 'The message' }
    let(:sc_recipients) { 'agent1@example.org, agent2@example.org' }
    let(:sc_content_type) { 'text/plain' }
    let(:actions) do
      [
        DefinitionItem.new(
          'side_conversation',
          nil,
          [sc_subject, sc_message, sc_recipients, sc_content_type]
        )
      ]
    end

    before do
      account.stubs(has_side_conversation_macros?: true)
      account.stubs(has_side_conversation_slack_macros?: true)
    end

    it 'returns side_conversation containing the right values' do
      result = macro_application.run

      assert_equal sc_recipients, result['side_conversation']['recipients']
      assert_equal sc_subject, result['side_conversation']['subject']
      assert_equal sc_message, result['side_conversation']['message']
      assert_equal sc_content_type, result['side_conversation']['content_type']
    end

    describe 'when the content_type is text/html' do
      let(:sc_content_type) { 'text/html' }
      let(:locale) { translation_locales(:english_by_zendesk) }

      it 'calls render with the right attributes' do
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_recipients, ticket, user, true, locale).once
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_subject, ticket, user, true, locale).once
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_message, ticket, user, true, locale, Mime[:text], rich_comment: true).once
        macro_application.run
      end
    end

    describe 'when the content_type is text/plain' do
      let(:sc_content_type) { 'text/plain' }
      let(:locale) { translation_locales(:english_by_zendesk) }

      it 'calls render with the right attributes' do
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_recipients, ticket, user, true, locale).once
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_subject, ticket, user, true, locale).once
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_message, ticket, user, true, locale, Mime[:text], rich_comment: false).once
        macro_application.run
      end
    end
  end

  describe 'when the macro has a side_conversation_slack item' do
    let(:ticket) { tickets(:minimum_1) }

    let(:sc_message) { 'The message' }
    let(:sc_channel) { 'general' }
    let(:sc_content_type) { 'text/plain' }
    let(:actions) do
      [
        DefinitionItem.new(
          'side_conversation_slack',
          nil,
          [sc_message, sc_channel, sc_content_type]
        )
      ]
    end

    before do
      account.stubs(has_side_conversation_macros?: true)
      account.stubs(has_side_conversation_slack_macros?: true)
    end

    it 'returns side_conversation containing the right values' do
      result = macro_application.run

      assert_equal sc_channel, result['side_conversation_slack']['channel']
      assert_equal sc_message, result['side_conversation_slack']['message']
      assert_equal sc_content_type, result['side_conversation_slack']['content_type']
      assert_nil result['side_conversation_slack']['subject']
      assert_nil result['side_conversation_slack']['recipients']
    end

    describe 'when values contain liquid placeholders' do
      let(:sc_recipients) { '{{current_user.email}}' }
      let(:sc_subject) { 'title: {{ticket.title}}' }
      let(:sc_message) { 'title again: {{ticket.title}}' }
      let(:actions) do
        [
          DefinitionItem.new(
            'side_conversation',
            nil,
            [sc_subject, sc_message, sc_recipients, sc_content_type]
          )
        ]
      end

      it 'replaces the placeholder with the real deal' do
        result = macro_application.run
        assert_equal user.email, result['side_conversation']['recipients']
        subject_actual = result['side_conversation']['subject']
        assert_equal "title: #{ticket.subject}", subject_actual
        message_actual = result['side_conversation']['message']
        assert_equal "title again: #{ticket.subject}", message_actual
      end
    end

    describe 'when the content_type is text/html' do
      let(:sc_content_type) { 'text/html' }
      let(:locale) { translation_locales(:english_by_zendesk) }

      it 'calls render with the right attributes' do
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_channel, ticket, user, true, locale).once
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_message, ticket, user, true, locale, Mime[:text], rich_comment: true).once
        macro_application.run
      end
    end

    describe 'when the content_type is text/plain' do
      let(:sc_content_type) { 'text/plain' }
      let(:locale) { translation_locales(:english_by_zendesk) }

      it 'calls render with the right attributes' do
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_channel, ticket, user, true, locale).once
        Zendesk::Liquid::TicketContext.expects(:render).with(sc_message, ticket, user, true, locale, Mime[:text], rich_comment: false).once
        macro_application.run
      end
    end
  end

  describe 'when the macro has a brand_id' do
    let(:default_brand) { account.default_brand }
    let(:active_brand)  do
      FactoryBot.create(:brand, name: 'active', active: true)
    end
    let(:inactive_brand) do
      FactoryBot.create(:brand, name: 'inactive', active: false)
    end

    before { account.stubs(has_multibrand?: true) }

    describe 'and the brand is valid' do
      let(:actions) do
        [DefinitionItem.new('brand_id', nil, [active_brand.id])]
      end

      it 'uses that brand_id' do
        assert_equal active_brand.id, macro_application.run['brand_id']
      end
    end

    describe 'and the brand is inactive' do
      let(:actions) do
        [DefinitionItem.new('brand_id', nil, [inactive_brand.id])]
      end

      it 'uses the default brand id' do
        assert_equal default_brand.id, macro_application.run['brand_id']
      end
    end

    describe 'and the brand_id is invalid' do
      let(:actions) { [DefinitionItem.new('brand_id', nil, [999])] }

      it 'uses the default brand id' do
        assert_equal default_brand.id, macro_application.run['brand_id']
      end
    end
  end

  describe 'tagging permissions' do
    let(:ticket)  { tickets(:minimum_1) }
    let(:actions) { [DefinitionItem.new('set_tags', nil, 'distag')] }

    describe 'light agents' do
      let(:user) { users(:minimum_agent) }
      before do
        user.stubs(:has_permission_set?).returns(true)
        permission_set = PermissionSet.
          initialize_light_agent(accounts(:minimum))
        user.stubs(:permission_set).returns(permission_set)
        user.stubs(:is_light_agent?).returns(true)
      end

      it 'allows macro tag applications for new tickets' do
        ticket.stubs(:new_record?).returns(true)

        assert_equal({'current_tags' => 'distag'}, macro_application.run)
      end

      it 'denies macro tag applications for non-new tickets' do
        assert_equal({}, macro_application.run)
      end

      describe 'when the ticket has the same requester' do
        before { ticket.stubs(:requester).returns(user) }

        it 'allows macro tag applications' do
          assert_equal({'current_tags' => 'distag'}, macro_application.run)
        end
      end

      describe 'when the ticket has a different requester' do
        before { ticket.stubs(:requester).returns(user.dup) }

        it 'denies macros tag applications' do
          assert_equal({}, macro_application.run)
        end
      end

      describe 'bulk ticket updates' do
        let(:ticket) { nil }

        it 'allows bulk_ticket update request' do
          assert_equal({'current_tags' => 'distag'}, macro_application.run)
        end
      end
    end

    describe 'system users' do
      let(:user) { users(:systemuser) }

      it 'allows macro tag applications regardless' do
        ticket.stubs(:requester).returns(user.dup)
        user.stubs(:permission_set).returns(nil)

        assert_equal({'current_tags' => 'distag'}, macro_application.run)
      end
    end

    describe 'regular agents' do
      let(:user) { users(:minimum_agent) }

      it 'allows macro tag applications if they have the permission' do
        user.stubs(:has_permission_set?).returns(true)
        permission_set = PermissionSet.new.tap do |ps|
          ps.permissions.enable(:edit_ticket_tags)
        end
        user.stubs(:permission_set).returns(permission_set)
        ticket.stubs(:requester).returns(user.dup)

        assert_equal({'current_tags' => 'distag'}, macro_application.run)
      end

      it 'denies macro tag applications if they dont have the permission' do
        user.stubs(:has_permission_set?).returns(true)
        permission_set = PermissionSet.new.tap do |ps|
          ps.permissions.disable(:edit_ticket_tags)
        end
        user.stubs(:permission_set).returns(permission_set)
        ticket.stubs(:requester).returns(user.dup)

        assert_equal({}, macro_application.run)
      end
    end

    describe_with_arturo_enabled(:email_ccs) do
      describe_with_arturo_setting_enabled(
        :follower_and_email_cc_collaborations
      ) do
        describe_with_arturo_setting_enabled(:ticket_followers_allowed) do
          let(:subject) { macro_application.run['followers_list'] }
          let(:actions) { [DefinitionItem.new('cc', 'is', user.id)] }

          follower_or_collaborations_tests

          describe 'when a ticket is present' do
            let(:ticket) { tickets(:minimum_1) }

            describe 'when a user in an cc action value is a follower' do
              before do
                ticket.stubs(followers: [user])
              end

              it 'marks the user as `following`' do
                assert subject.first[:following]
              end
            end

            describe 'when a user in an cc action value is not a follower' do
              before { ticket.stubs(followers: []) }

              it 'does not mark the user as `following`' do
                refute subject.first[:following]
              end
            end
          end

          describe 'when the ticket is not present' do
            let(:ticket) { nil }

            it 'does not mark the user as `following`' do
              refute subject.first[:following]
            end
          end
        end

        # In order to create a CC rule either `is_collaboration_enabled?` or
        # `has_ticket_followers_allowed_enabled?` must true. This turns on the
        # former to test when the later is disabled.
        describe_with_arturo_enabled(:collaboration_enabled_enabled) do
          describe_with_arturo_setting_disabled(:ticket_followers_allowed) do
            let(:actions) { [DefinitionItem.new('cc', 'is', user.id)] }

            it 'returns output without users' do
              assert_empty(
                macro_application.run.slice(
                  'followers_list', 'collaborator_list'
                )
              )
            end
          end
        end
      end
    end

    describe_with_arturo_disabled(:email_ccs) do
      let(:subject) { macro_application.run['collaborator_list'] }

      follower_or_collaborations_tests
      collaboration_cced_tests
    end

    describe_with_arturo_setting_disabled(
      :follower_and_email_cc_collaborations
    ) do
      let(:subject) { macro_application.run['collaborator_list'] }

      follower_or_collaborations_tests
      collaboration_cced_tests
    end
  end

  describe 'when the macro actions contain ticket.ccs placeholder' do
    let(:ticket) { tickets(:minimum_1) }
    let(:locale) { translation_locales(:english_by_zendesk) }
    let(:comment_template) do
      '{% for user in ticket.ccs %}' \
      '  <p>{{user.email}}</p>' \
      '{% endfor %}'
    end

    let(:cc_name_pattern) do
      /minimum_admin@aghassipour\.com|minimum_end_user@aghassipour\.com/x
    end

    let(:actions) do
      [
        DefinitionItem.new(
          'comment_value_html',
          nil,
          [comment_template]
        )
      ]
    end

    describe 'when ticket is not present' do
      let(:ticket) { nil }

      describe_with_arturo_disabled(:dc_context_for_macro_application) do
        it 'returns the comment template as-is' do
          assert_equal(
            comment_template,
            macro_application.run['comment']['html_value']
          )
        end
      end

      describe_with_arturo_enabled(:dc_context_for_macro_application) do
        # rendered comment will be empty because comment template
        # loops through ticket ccs, which is nil without a present ticket
        let(:rendered_comment_template) { '' }

        it 'returns the rendered comment template' do
          assert_equal(
            rendered_comment_template,
            macro_application.run['comment']['html_value']
          )
        end
      end
    end

    describe 'when ticket is present' do
      before do
        ticket.collaborations << Collaboration.new(
          collaborator_type: CollaboratorType.FOLLOWER,
          user:              users(:minimum_admin)
        )

        ticket.collaborations << Collaboration.new(
          collaborator_type: CollaboratorType.EMAIL_CC,
          user:              users(:minimum_end_user)
        )

        ticket.will_be_saved_by(users(:minimum_admin))
      end

      describe 'and ccs are unsaved' do
        it 'renders email of each cc' do
          assert_match(
            cc_name_pattern,
            macro_application.run['comment']['html_value']
          )
        end
      end

      describe 'and ccs are saved' do
        before { ticket.save! }

        it 'renders email of each cc' do
          assert_match(
            cc_name_pattern,
            macro_application.run['comment']['html_value']
          )
        end
      end
    end
  end
end
