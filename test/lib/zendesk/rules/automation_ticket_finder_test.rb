require_relative '../../../support/test_helper'
require 'zendesk_rules/errors'

SingleCov.covered! uncovered: 2

describe Zendesk::Rules::AutomationTicketFinder do
  fixtures :rules, :tickets, :cf_fields

  let(:subject) do
    Zendesk::Rules::AutomationTicketFinder.new(automation: automation)
  end

  let(:automation) { rules(:automation_for_custom_user_model) }

  describe '#find_tickets' do
    # Cover the slow code path
    before { subject.stubs(:log_slow_find).returns(0) }

    it 'finds tickets that match the automation' do
      Zendesk::Rules::RuleExecuter.
        any_instance.
        stubs(:find_tickets_with_occam_client).
        returns([tickets(:minimum_1), tickets(:minimum_2)])

      tickets = subject.find_tickets
      tickets.size.must_equal 2
      tickets.first.must_be_instance_of Ticket

      subject.found.must_equal 2
      subject.select_time.must_be :>, 0.0
    end

    describe 'for an automation with broken references' do
      before { cf_fields(:decimal1).delete }

      it 'deactivates itself and reraises the exception' do
        -> { subject.find_tickets }.must_raise ZendeskRules::InvalidComponent
        automation.reload.is_active?.must_equal false
      end
    end
  end
end
