require_relative '../../../support/test_helper'
SingleCov.covered! uncovered: 1

describe 'Zendesk::Rules::ViewsRateLimiter' do
  fixtures :accounts,
    :users,
    :rules

  let(:account) { accounts(:minimum) }
  let(:agent)   { users(:minimum_agent) }
  let(:klass)   { Zendesk::Rules::ViewsRateLimiter }
  let(:span)    { stub(add_tag: true) }
  let(:handle)  { :views_limit_find_requests }
  let(:first_throttled) { false }
  let(:error) do
    Prop::RateLimited.new(
      handle: handle,
      cache_key: '1',
      first_throttled: false,
      description: 'desc',
      interval: 1.minute,
      strategy: stub(threshold_reached: '')
    )
  end
  let(:origin)  { stub_everything(request_source: 'app') }
  let(:limiter) { klass.limiter_for(:view_find, account, agent, origin, 1) }

  let(:statsd_client) { Zendesk::StatsD::Client.new(namespace: 'views') }
  before do
    Datadog.tracer.stubs(:active_span).returns(span)
    limiter.stubs(:statsd_client).returns(statsd_client)
  end

  describe '.limiter_for' do
    it 'returns a rate limiter with right attributes' do
      assert_kind_of(klass, limiter)
    end

    it 'has the correct attributes' do
      assert_equal limiter.handle, handle
      assert_equal limiter.account, account
      assert_equal limiter.user, agent
      assert_equal limiter.resource_id, 1
      assert_equal limiter.origin, origin
    end
  end

  describe '#to_s' do
    it 'returns the object inspect' do
      assert_match(/ViewsRateLimiter/, limiter.to_s)
    end
  end

  describe '#throttle!' do
    describe 'with arturo disabled' do
      before { Arturo.expects(:feature_enabled_for?).with(handle, account).returns(false) }

      it 'returns early' do
        Prop.expects(:throttle!).returns(false).never
        assert_nil limiter.throttle!
      end
    end

    describe 'with arturo enabled' do
      before { Arturo.expects(:feature_enabled_for?).with(handle, account).returns(true) }

      describe_with_arturo_enabled :views_enforce_rate_limits do
        it 'throttles the request' do
          Prop.expects(:throttle!).returns(false)
          limiter.throttle!
        end
      end

      describe_with_arturo_disabled :views_enforce_rate_limits do
        it 'only checks if request should be throttled' do
          Prop.expects(:throttle)
          limiter.throttle!
        end

        describe 'with first throttled' do
          before { Prop.expects(:throttle).returns(:first_throttled) }

          it 'reports with first throttled' do
            limiter.expects(:report).with(true)
            limiter.throttle!
          end
        end

        describe 'with further rate limited requests' do
          before { Prop.expects(:throttle).returns(true) }

          it 'reports with first throttled' do
            limiter.expects(:report).with(false)
            limiter.throttle!
          end
        end
      end
    end
  end

  describe '#report' do
    let(:tags) do
      [
        "handle:#{handle}",
        "first_throttled:#{first_throttled}",
        'request_source:app',
        "status:#{status}"
      ]
    end

    it 'adds trace tags' do
      span.expects(:set_tag).with('zendesk.rate_limited.handle', handle)
      span.expects(:set_tag).with('zendesk.rate_limited.count', 0)
      span.expects(:set_tag).with('zendesk.rate_limited.first_throttled', false)
      limiter.report(first_throttled)
    end

    describe_with_arturo_enabled :views_enforce_rate_limits do
      let(:status) { 429 }

      it 'tracks metrics with 429 status' do
        limiter.stubs(:add_tracer_tags)
        statsd_client.expects(:increment).with('rate_limited', tags: tags)
        limiter.report(first_throttled)
      end
    end

    describe_with_arturo_disabled :views_enforce_rate_limits do
      let(:status) { 200 }

      it 'tracks metrics with 200 status' do
        limiter.stubs(:add_tracer_tags)
        statsd_client.expects(:increment).with('rate_limited', tags: tags)
        limiter.report(first_throttled)
      end
    end

    it 'logs' do
      limiter.stubs(:add_tracer_tags)
      Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/))
      limiter.report(first_throttled)
    end

    it 'yields the limiter' do
      limiter.stubs(:add_tracer_tags)
      limiter.report(first_throttled) do |lim|
        assert_equal lim, limiter
      end
    end

    it 'does NOT raise errors' do
      limiter.stubs(:add_tracer_tags).raises(StandardError.new('limiter_error'))
      ZendeskExceptions::Logger.expects(:record)
      limiter.report(first_throttled)
    end
  end

  describe '#raise_rate_limited?' do
    before { limiter.stubs(:exception).returns(first_throttled) }

    describe 'when origin is lotus' do
      let(:origin) { stub_everything(lotus?: true) }

      describe_with_arturo_enabled :views_enforce_rate_limits do
        describe_with_arturo_enabled :views_rate_limit_lotus do
          it 'returns true' do
            assert limiter.raise_rate_limited?
          end
        end

        describe_with_arturo_disabled :views_rate_limit_lotus do
          it 'returns false' do
            refute limiter.raise_rate_limited?
          end
        end
      end
      describe_with_arturo_disabled :views_enforce_rate_limits do
        it 'returns false' do
          refute limiter.raise_rate_limited?
        end
      end
    end

    describe 'when origin is a zendesk mobile client' do
      let(:origin) do
        stub_everything(lotus?: false, zendesk_mobile_client?: true)
      end

      describe_with_arturo_enabled :views_enforce_rate_limits do
        it 'returns false' do
          refute limiter.raise_rate_limited?
        end
      end
      describe_with_arturo_disabled :views_enforce_rate_limits do
        it 'returns false' do
          refute limiter.raise_rate_limited?
        end
      end
    end

    describe 'when origin is a non-zendesk mobile client' do
      let(:origin) do
        stub_everything(lotus?: false, zendesk_mobile_client?: false)
      end

      describe_with_arturo_enabled :views_enforce_rate_limits do
        it 'returns true' do
          assert limiter.raise_rate_limited?
        end
      end
      describe_with_arturo_disabled :views_enforce_rate_limits do
        it 'returns false' do
          refute limiter.raise_rate_limited?
        end
      end
    end

    describe 'when origin is api but NOT mobile client' do
      let(:origin) { stub_everything(lotus?: false) }

      describe_with_arturo_enabled :views_enforce_rate_limits do
        it 'returns true' do
          assert limiter.raise_rate_limited?
        end
      end
      describe_with_arturo_disabled :views_enforce_rate_limits do
        it 'returns false' do
          refute limiter.raise_rate_limited?
        end
      end
    end

    describe 'when origin is not lotus' do
      let(:origin) { stub_everything(lotus?: false) }

      describe_with_arturo_enabled :views_enforce_rate_limits do
        it 'returns true' do
          assert limiter.raise_rate_limited?
        end
      end
      describe_with_arturo_disabled :views_enforce_rate_limits do
        it 'returns false' do
          refute limiter.raise_rate_limited?
        end
      end
    end
  end
end
