require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Context::CustomFieldComponents do
  fixtures :accounts,
    :brands,
    :cf_fields,
    :ticket_fields,
    :translation_locales

  let(:klass) do
    Class.new do
      include Zendesk::Rules::Context::CustomFieldComponents

      def initialize(account, user, validating, executing)
        @account    = account
        @user       = user
        @validating = validating
        @executing  = executing
      end

      attr_reader :account, :user, :validating, :executing
    end
  end

  let(:account) { accounts(:minimum) }
  let(:user)    { users(:minimum_agent) }

  let(:validating) { false }
  let(:executing)  { false }

  let(:japanese)        { translation_locales(:japanese) }
  let(:fallback_locale) { translation_locales(:english_by_zendesk) }

  let(:create_cms_text) do
    Cms::Text.create!(
      account:             account,
      name:                'Welcome Wombat',
      fallback_attributes: {
        is_fallback:           true,
        nested:                true,
        value:                 'The fallback value',
        translation_locale_id: fallback_locale.id
      }
    ).tap do |text|
      text.variants.create!(
        active:                true,
        translation_locale_id: japanese.id,
        value:                 'Japanese Value!'
      )
    end
  end

  let(:subject) { klass.new(account, user, validating, executing) }

  describe '#ticket_custom_fields' do
    let(:ticket_field) { ticket_fields(:field_tagger_custom) }

    let(:resolved_field) do
      subject.
        ticket_custom_fields.
        find { |field| field.id == ticket_field.id }
    end

    before do
      user.account.locale_id = japanese.id

      create_cms_text
    end

    it 'returns the custom ticket fields' do
      assert_equal(
        account.ticket_fields.custom.active.map(&:id).sort,
        subject.ticket_custom_fields.map(&:id).sort
      )
    end

    it 'renders the title with dynamic content' do
      assert_equal 'Fun factor Japanese Value!', resolved_field.title
    end

    it 'renders the custom field options with dynamic content' do
      assert_equal(
        'Japanese Value!',
        resolved_field.custom_field_options.last.name
      )
    end

    describe 'when validating' do
      let(:validating) { true }

      it 'returns the custom ticket fields' do
        assert_equal(
          account.ticket_fields.custom.active.map(&:id).sort,
          subject.ticket_custom_fields.map(&:id).sort
        )
      end

      it 'does not render the dynamic content' do
        assert_equal 'Fun factor {{dc.welcome_wombat}}', resolved_field.title
      end

      it 'does not render the custom field options with dynamic content' do
        assert_equal(
          '{{dc.welcome_wombat}}',
          resolved_field.custom_field_options.last.name
        )
      end
    end

    describe 'when executing' do
      let(:executing) { true }

      it 'returns the custom ticket fields' do
        assert_equal(
          account.ticket_fields.custom.active.map(&:id).sort,
          subject.ticket_custom_fields.map(&:id).sort
        )
      end

      it 'does not render the dynamic content' do
        assert_equal 'Fun factor {{dc.welcome_wombat}}', resolved_field.title
      end

      it 'does not render the custom field options with dynamic content' do
        assert_equal(
          '{{dc.welcome_wombat}}',
          resolved_field.custom_field_options.last.name
        )
      end
    end
  end

  describe '#user_custom_fields' do
    let!(:user_field) do
      cf_fields(:text1).tap do |field|
        field.title = 'Fun factor {{dc.welcome_wombat}}'
        field.save!
      end
    end

    let(:resolved_title) do
      subject.
        user_custom_fields.
        find { |field| field.id == user_field.id }.
        title
    end

    before do
      user.account.locale_id = japanese.id

      create_cms_text
    end

    it 'returns the custom user fields' do
      assert_equal(
        account.custom_fields.for_user.active.map(&:id).sort,
        subject.user_custom_fields.map(&:id).sort
      )
    end

    it 'renders the dynamic content' do
      assert_equal 'Fun factor Japanese Value!', resolved_title
    end

    describe 'when the account does not have user and organization fields' do
      before { account.stubs(has_user_and_organization_fields?: false) }

      it 'returns an empty array' do
        assert_empty subject.user_custom_fields
      end
    end

    describe 'when validating' do
      let(:validating) { true }

      it 'returns the custom user fields' do
        assert_equal(
          account.custom_fields.for_user.active.map(&:id).sort,
          subject.user_custom_fields.map(&:id).sort
        )
      end

      it 'does not render the dynamic content' do
        assert_equal 'Fun factor {{dc.welcome_wombat}}', resolved_title
      end
    end

    describe 'when executing' do
      let(:executing) { true }

      it 'returns the custom user fields' do
        assert_equal(
          account.custom_fields.for_user.active.map(&:id).sort,
          subject.user_custom_fields.map(&:id).sort
        )
      end

      it 'does not render the dynamic content' do
        assert_equal 'Fun factor {{dc.welcome_wombat}}', resolved_title
      end
    end
  end

  describe '#organization_custom_fields' do
    let!(:org_field) do
      account.custom_fields.build(
        key:   'field',
        title: 'Fun factor {{dc.welcome_wombat}}',
        owner: 'Organization'
      ).tap do |field|
        field.type = 'Text'
        field.save!
      end
    end

    let(:resolved_title) do
      subject.
        organization_custom_fields.
        find { |field| field.id == org_field.id }.
        title
    end

    before do
      user.account.locale_id = japanese.id

      create_cms_text
    end

    it 'returns the custom organization fields' do
      assert_equal(
        account.custom_fields.for_organization.active.map(&:id).sort,
        subject.organization_custom_fields.map(&:id).sort
      )
    end

    it 'renders the dynamic content' do
      assert_equal 'Fun factor Japanese Value!', resolved_title
    end

    describe 'when the account does not have user and organization fields' do
      before { account.stubs(has_user_and_organization_fields?: false) }

      it 'returns an empty array' do
        assert_empty subject.organization_custom_fields
      end
    end

    describe 'when validating' do
      let(:validating) { true }

      it 'returns the custom organization fields' do
        assert_equal(
          account.custom_fields.for_organization.active.map(&:id).sort,
          subject.organization_custom_fields.map(&:id).sort
        )
      end

      it 'does not render the dynamic content' do
        assert_equal 'Fun factor {{dc.welcome_wombat}}', resolved_title
      end
    end

    describe 'when executing' do
      let(:executing) { true }

      it 'returns the custom organization fields' do
        assert_equal(
          account.custom_fields.for_organization.active.map(&:id).sort,
          subject.organization_custom_fields.map(&:id).sort
        )
      end

      it 'does not render the dynamic content' do
        assert_equal 'Fun factor {{dc.welcome_wombat}}', resolved_title
      end
    end
  end

  describe '#find_ticket_field' do
    let(:ticket_field) { ticket_fields(:field_tagger_custom) }

    it 'returns the ticket field' do
      assert ticket_field, subject.find_ticket_field(ticket_field.id)
    end

    describe 'when the ticket field does not exist' do
      it 'returns nil' do
        assert_nil subject.find_ticket_field(0)
      end
    end
  end

  describe '#find_user_custom_field' do
    let(:user_field) { cf_fields(:dropdown1) }

    it 'returns the custom user field' do
      assert user_field, subject.find_user_custom_field(user_field.key)
    end

    describe 'when the account does not have user and organization fields' do
      before { account.stubs(has_user_and_organization_fields?: false) }

      it 'returns false' do
        refute subject.find_user_custom_field(user_field.key)
      end
    end

    describe 'when the ticket field does not exist' do
      it 'returns nil' do
        assert_nil subject.find_user_custom_field('')
      end
    end
  end

  describe '#find_organization_custom_field' do
    let(:org_field) { cf_fields(:org_checkbox1) }

    it 'returns the custom organization field' do
      assert org_field, subject.find_organization_custom_field(org_field.key)
    end

    describe 'when the account does not have user and organization fields' do
      before { account.stubs(has_user_and_organization_fields?: false) }

      it 'returns false' do
        refute subject.find_organization_custom_field(org_field.key)
      end
    end

    describe 'when the custom organization field does not exist' do
      it 'returns nil' do
        assert_nil subject.find_organization_custom_field('')
      end
    end
  end

  describe '#accessed_fields' do
    let(:ticket_field) { ticket_fields(:field_tagger_custom) }
    let(:user_field)   { cf_fields(:dropdown1) }
    let(:org_field)    { cf_fields(:org_checkbox1) }

    before do
      subject.find_ticket_field(ticket_field.id)
      subject.find_user_custom_field(user_field.key)
      subject.find_organization_custom_field(org_field.key)
    end

    it 'returns the accessed fields' do
      assert_equal(
        {
          'ticket_custom_fields' => [
            {
              'id'   => ticket_field.id,
              'type' => ticket_field.type,
              'tag'  => ticket_field.tag
            }
          ],
          'user_custom_fields' => [
            {
              'id'   => user_field.id,
              'type' => user_field.type,
              'key'  => user_field.key,
              'tag'  => user_field.tag
            }
          ],
          'organization_custom_fields' => [
            {
              'id'   => org_field.id,
              'type' => org_field.type,
              'key'  => org_field.key,
              'tag'  => org_field.tag
            }
          ]
        },
        subject.accessed_fields
      )
    end
  end
end
