require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Zendesk::Rules::ThrottledRuleTicketFinder do
  fixtures :accounts, :rules, :users, :tickets

  def throttled?(params = {caller: 'v2'})
    Zendesk::Rules::ThrottledRuleTicketFinder.send(
      :throttled?,
      @rule,
      @user,
      Zendesk::Rules::ExecutionOptions.new(@rule, @rule.account, params)
    )
  end

  def exceed_throttle
    key = Zendesk::Rules::ThrottledRuleTicketFinder.
      send(:cache_key, @rule, @user, options)

    assert Prop.throttle(:rule_execution_time_limit, key, increment: 9_999_999)
  end

  def query_should_take(time, results = [])
    Zendesk::Rules::ThrottledRuleTicketFinder.stubs(measure: [results, time])
  end

  before do
    @rule = rules(:view_my_working_tickets)
    @user = users(:minimum_agent)
    @query = Zendesk::Rules::RuleQueryBuilder.
      query_for(@rule, @user).
      compile(:sql)
  end

  let(:default_params) { {caller: 'v2'} }
  let(:params)         { default_params }
  let(:options)        do
    Zendesk::Rules::ExecutionOptions.new(@rule, @rule.account, params)
  end

  describe 'throw a stat after execution' do
    before do
      Zendesk::Rules::ThrottledRuleTicketFinder.expects(:throttled?)

      Zendesk::Rules::ThrottledRuleTicketFinder.stubs(benchmark: [[], 10])

      Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options)
    end

    before_should 'throw a stat' do
      Zendesk::Rules::ThrottledRuleTicketFinder.
        send(:statsd_client).
        expects(:histogram).
        with('execution_time', 10)
    end
  end

  describe 'when a rule has a force_key attribute' do
    let(:params) { default_params.merge(group: 'status') }

    describe 'with a valid index' do
      let(:force_key) do
        'index_tickets_on_account_id_and_status_id_and_created_at'
      end

      it 'is valid' do
        assert_predicate @rule, :valid_index_for_force_key?
      end

      it 'pass the correct options and FORCE KEY when force_key is specified' do
        @rule.force_key = force_key

        @rule.save!

        Ticket::ActiveRecord_AssociationRelation.
          any_instance.
          expects(:from).
          with("tickets FORCE KEY(`#{force_key}`)").
          returns(Ticket.all)

        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)
      end
    end

    describe 'with an invalid index' do
      before do
        @bad_rule = @rule.dup
      end

      it 'does not be valid' do
        @bad_rule.force_key = 'foo'

        assert_equal 'foo', @bad_rule.force_key
        refute_predicate @bad_rule, :valid_index_for_force_key?
      end

      it 'does not allow me to save an invalid index to a rule' do
        @bad_rule.force_key = 'foo'

        refute_predicate @bad_rule, :valid?
      end

      it 'does not allow me to run a rule with an invalid force_key index' do
        @bad_rule.update_attribute(:force_key, 'foo')

        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@bad_rule, @user, @query, options)

        refute_predicate @bad_rule, :valid_index_for_force_key?
      end
    end
  end

  describe '.find' do
    it 'finds tickets' do
      # found something
      tickets = Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options)

      refute_predicate tickets, :empty?

      # not cached
      Ticket::ActiveRecord_AssociationRelation.
        any_instance.
        expects(:all).
        returns([])
      # @rule.account.tickets.expects(:all).returns []
      Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options)
    end

    it 'pass :include to the finder when cached' do
      options.stubs(:includes).returns(nil)

      exceed_throttle

      Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options) # fill cache

      options.stubs(:includes).returns(:requester)

      sql_queries = assert_sql_queries(2) do
        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)
      end

      assert_includes sql_queries[0], '`tickets`'
      assert_includes sql_queries[1], '`users`'
    end

    # Since prop does not support floats -> infinite requests below 1 second
    it 'uses integers for throttling' do
      Prop.expects(:throttle).with do |_scope, _key, options|
        assert_equal(123, options[:increment])

        true
      end

      query_should_take(0.12345678)

      Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options)
    end

    describe 'with a grouping' do
      before { @rule.output.group = 'hello' }

      it 'is not enabled' do
        assert(
          Zendesk::Rules::ThrottledRuleTicketFinder.
            send(:disabled?, options, @rule)
        )
      end
    end

    describe 'with an ids_only execution' do
      let(:params) { default_params.merge(ids_only: true) }
      it 'selects only ticket ids' do
        tickets = Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)

        assert_equal tickets.first.attributes.keys, ['id']
      end

      it 'returns full ticket objects anyway, for some ugly reason' do
        tickets = Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)

        assert_kind_of Ticket, tickets.first
        assert_equal %w[id], tickets.first.attributes.keys
      end
    end

    it 'finds tickets without user' do
      refute_predicate(
        Zendesk::Rules::ThrottledRuleTicketFinder.find(@rule, nil, @query, options),
        :empty?
      )
    end

    describe 'when throttle was reached' do
      before do
        Zendesk::Rules::ThrottledRuleTicketFinder.stubs(throttled?: true)
      end

      it 'returns cached results' do
        # hits db since there is no cache
        tickets = Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options.dup)

        refute_predicate tickets, :empty?

        Ticket.expects(:find_by_sql).with do |sql|
          sql.where_sql =~ /`tickets`.`id` = \d+/
        end.returns([])

        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)
      end

      it 'does not return cached results if not api call' do
        # hits db since there is no cache
        tickets = Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options.dup)

        refute_predicate tickets, :empty?

        options = Zendesk::Rules::ExecutionOptions.new(@rule, @rule.account, {})

        # now is cached. but don't pull from cache since not api call
        # use metrics instrumentation to evaluate
        statsd_client = Zendesk::Rules::ThrottledRuleTicketFinder.send(:statsd_client)
        statsd_client.expects(:increment).with('fetch', tags: %w[mode:disabled]).once

        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)
      end

      it 'returns fresh tickets even when cached' do
        time = 1.month.ago

        # hits db since there is no cache
        tickets = Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options.dup)

        tickets.first.update_columns(updated_at: time)

        tickets = Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)

        assert_equal time.to_i, tickets.first.updated_at.to_i
      end

      it 'sorts tickets by cached order' do
        # this test relied on a different version of the rule --
        # the query was a nearly empty query, but the rule was legit.
        # I don't know.  stop asking.
        # It will be a nice day when this all goes into the garbage.  i think.
        @query.delete(:assignee_id)

        original_tickets = Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options.dup)

        assert_operator 2, '<=', original_tickets.size

        # return incorrect order from db lookup
        Ticket.expects(:find_by_sql).returns(original_tickets.reverse)

        tickets = Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)

        assert_equal tickets.map(&:id).sort, original_tickets.map(&:id).sort
      end
    end

    it 'starts caching if the throttle is reached' do
      Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options.dup)

      Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options.dup)

      # giant runtime -> reaches throttle
      query_should_take(999_999)

      Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options.dup)

      # now cached
      assert throttled?, 'expected to be throttled? but was not'

      Rails.cache.expects(:fetch).returns([])

      Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options)
    end

    # race condition with checking throttle?
    it 'does not raise when throttle! raises inside of unthrottled' do
      # giant runtime -> reaches throttle
      query_should_take(999_999, [:foo])
      assert(
        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options).present?
      )

      # simulate other thread that got throttled? to false while it has reached
      # the threshold in the mean time
      Rails.cache.expects(:fetch).never

      Zendesk::Rules::ThrottledRuleTicketFinder.stubs(throttled?: false)

      assert_predicate(
        Zendesk::Rules::ThrottledRuleTicketFinder.find(@rule, @user, @query, options),
        :present?
      )
    end

    it 'does not create a throttle for preview' do
      @rule = @user.account.views.new
      @rule.definition = Definition.new
      @rule.output = Output.create(columns: View::COLUMNS)

      query_should_take(999_999)

      Prop.expects(:throttle!).never

      Zendesk::Rules::ThrottledRuleTicketFinder.
        find(@rule, @user, @query, options)
    end

    describe '#cached_by_throttled_rule_ticket_finder' do
      it 'does not set when not cached' do
        exceed_throttle

        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)

        refute_predicate @rule, :cached_by_throttled_rule_ticket_finder
      end

      it 'does not set when not throttled or cached' do
        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)

        refute_predicate @rule, :cached_by_throttled_rule_ticket_finder
      end

      it 'sets when cached' do
        exceed_throttle

        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)

        refute_predicate @rule, :cached_by_throttled_rule_ticket_finder

        Zendesk::Rules::ThrottledRuleTicketFinder.
          find(@rule, @user, @query, options)

        assert_predicate @rule, :cached_by_throttled_rule_ticket_finder
      end
    end
  end

  describe '.throttled?' do
    describe 'when the throttle is not reached' do
      it 'returns false' do
        refute throttled?, 'is throttled'
      end
    end

    describe 'when throttled is reached' do
      before { exceed_throttle }

      it 'is throttled' do
        assert throttled?, 'expected to be throttled? but was not'
      end

      it 'is throttled for offset 0' do
        assert throttled?(page: 1, paginate: true, per_page: 50), 'expected to be throttled? but was not'
      end

      it 'is not throttled for offset > 0' do
        refute throttled?(page: 2, paginate: true, per_page: 50), 'is throttled?'
      end

      it 'is not throttled for different limit' do
        refute throttled?(limit: 1), 'is throttled?'
      end

      it 'is not throttled for different order' do
        refute throttled?(order: 'id'), 'is throttled?'
      end
    end

    describe 'and the rule is not a view' do
      before do
        @rule = rules(:automation_close_ticket)

        exceed_throttle
      end

      it 'is not throttled' do
        refute throttled?, 'is throttled?'
      end
    end
  end
end
