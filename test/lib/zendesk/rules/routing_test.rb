require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe 'Zendesk::Rules::Routing' do
  include TestSupport::Rule::Helper

  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:user)    { users(:minimum_admin) }

  before do
    account.settings.skill_based_filtered_views = [view.id]
    account.settings.save!
    account.settings.reload
  end

  after do
    View.destroy_all
  end

  describe '#update_skill_based_filtered_views' do
    describe 'when a skill-based filtered view is updated' do
      before do
        view.save!
      end

      describe 'and the view is active' do
        describe 'and the view is deleted' do
          let(:view) do
            create_view(owner: account, active: true).tap(&:soft_delete!)
          end

          it 'changes `skill_based_filtered_views` account settings' do
            refute account.settings.skill_based_filtered_views.include?(
              view.id
            )
          end
        end

        describe 'and the view is not deleted' do
          describe 'and the view is shared' do
            let(:view) { create_view(owner: account, active: true) }

            it "doesn't change `skill_based_filtered_views` account settings" do
              assert account.settings.skill_based_filtered_views.include?(
                view.id
              )
            end
          end

          describe 'and the view is not shared' do
            let(:view) { create_view(owner: user, active: true) }

            it 'changes `skill_based_filtered_views` account settings' do
              refute account.settings.skill_based_filtered_views.include?(
                view.id
              )
            end
          end
        end
      end

      describe 'and the view is not active' do
        let(:view) { create_view(owner: account, active: false) }

        it 'changes `skill_based_filtered_views` account settings' do
          refute account.settings.skill_based_filtered_views.include?(view.id)
        end
      end
    end

    describe 'when a non skill-based filtered view is updated' do
      before do
        non_skill_based_filtered_view.save!
      end

      let(:view) { create_view(owner: account, active: true) }
      let(:non_skill_based_filtered_view) do
        create_view(owner: account, active: false)
      end

      it 'does not change `skill_based_filtered_views` account settings' do
        assert account.settings.skill_based_filtered_views.include?(view.id)
      end
    end
  end
end
