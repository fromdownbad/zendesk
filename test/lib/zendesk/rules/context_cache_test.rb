require_relative '../../../support/test_helper'
require_relative '../../../support/sharing_test_helper'

SingleCov.covered!

describe Zendesk::Rules::ContextCache do
  include SharingTestHelper

  fixtures :accounts,
    :brands,
    :groups,
    :organizations,
    :rules,
    :ticket_forms,
    :users,
    :custom_statuses,
    :translation_locales

  let(:account) { accounts(:minimum) }
  let(:user)    { users(:minimum_agent) }

  let(:described_class) { Zendesk::Rules::ContextCache }
  let(:validating)  { false }
  let(:executing)   { false }

  let(:context_cache) { described_class.new(account, user) }

  it 'includes custom field components' do
    assert_includes(
      described_class.included_modules,
      Zendesk::Rules::Context::CustomFieldComponents
    )
  end

  describe '#accepted_outbound_sharing_agreements' do
    let!(:sharing_agreement) do
      create_valid_agreement(
        status_id: Sharing::Agreement::STATUS_MAP[:accepted]
      )
    end

    it 'returns the name and ID for the accepted outbound sharing agreements' do
      assert_equal(
        [[sharing_agreement.name, sharing_agreement.id]],
        context_cache.accepted_outbound_sharing_agreements
      )
    end
  end

  describe '#active_brands' do
    let(:brand) { brands(:minimum) }

    it 'returns the name and ID of the active brands' do
      assert_equal [[brand.name, brand.id]], context_cache.active_brands
    end
  end

  describe "#active_custom_statuses" do
    let(:japanese)        { translation_locales(:japanese) }
    let(:fallback_locale) { translation_locales(:english_by_zendesk) }

    let(:create_cms_text) do
      Cms::Text.create!(
        account:             account,
        name:                'Welcome Wombat',
        fallback_attributes: {
          is_fallback:           true,
          nested:                true,
          value:                 'The fallback value',
          translation_locale_id: fallback_locale.id
        }
      ).tap do |text|
        text.variants.create!(
          active:                true,
          translation_locale_id: japanese.id,
          value:                 'Japanese Value!'
        )
      end
    end
    before do
      user.account.locale_id = japanese.id

      create_cms_text
    end

    describe "return the list of active statuses" do
      let(:custom_status_with_dc) { custom_statuses(:custom_status_with_dc) }

      let(:active_custom_statuses) do
        account.custom_statuses.active.
          pluck(:agent_label, :id, :system_status_id).
          map do |agent_label, id|
          [
            Zendesk::Liquid::DcContext.render(agent_label, account, user, 'text/plain', nil, user.dc_cache),
            id
          ]
        end
      end

      it 'returns the agent_label, id, system_status_id of the active custom statuses' do
        assert_equal active_custom_statuses, context_cache.active_custom_statuses
      end
    end
  end

  describe '#active_groups' do
    let(:group) { groups(:minimum_group) }

    it 'returns the name and ID of active groups' do
      assert_equal [[group.name, group.id]], context_cache.active_groups
    end
  end

  describe '#active_shared_macros' do
    let(:macro) { rules(:macro_incident_escalation) }

    it 'returns the title and ID of the shared active macros for the account' do
      assert_equal [[macro.title, macro.id]], context_cache.active_shared_macros
    end
  end

  describe '#active_ticket_forms' do
    let(:account)       { accounts(:support) }
    let(:ticket_form_1) { ticket_forms(:default_ticket_form) }
    let(:ticket_form_2) { ticket_forms(:non_default_ticket_form) }

    it 'returns the name and ID of the active ticket forms' do
      assert_equal(
        [
          [ticket_form_1.name, ticket_form_1.id],
          [ticket_form_2.name, ticket_form_2.id]
        ],
        context_cache.active_ticket_forms
      )
    end
  end

  describe '#agents' do
    it 'returns the agents for the account' do
      assert_equal(
        account.agents.active.pluck(:name, :id),
        context_cache.agents
      )
    end
  end

  describe '#assignable_agents' do
    it 'returns the assignable agents for the account' do
      assert_equal(
        account.assignable_agents.active.pluck(:name, :id),
        context_cache.assignable_agents
      )
    end
  end

  describe '#current_organization_ids' do
    before do
      account.stubs(has_multiple_organizations_enabled?: multiple_organizations)
    end

    describe 'when multiple organizations is enabled' do
      let(:multiple_organizations) { true }
      let(:organization_ids)       { [1, 2] }

      before { user.stubs(organization_ids: organization_ids) }

      it "returns the user's organization IDs" do
        assert_equal organization_ids, context_cache.current_organization_ids
      end
    end

    describe 'when multiple organizations is disabled' do
      let(:multiple_organizations) { false }
      let(:organization_id)        { 123 }

      before { user.stubs(organization_id: organization_id) }

      it "returns the user's organization ID" do
        assert_equal [organization_id], context_cache.current_organization_ids
      end
    end
  end

  describe '#default_schedule_id' do
    before { account.schedules.create(name: 'A', time_zone: 'UTC') }

    it 'returns the ID of the default schedule' do
      assert_equal account.schedule.id, context_cache.default_schedule_id
    end
  end

  describe '#default_ticket_form_id' do
    let(:account)             { accounts(:support) }
    let(:default_ticket_form) { ticket_forms(:default_ticket_form) }

    it 'returns the ID of the default ticket form' do
      assert_equal default_ticket_form.id, context_cache.default_ticket_form_id
    end
  end

  describe '#integration_service_instance_map' do
    let!(:ris) do
      ::Channels::AnyChannel::RegisteredIntegrationService.new(
        name:           'ris',
        search_name:    'ris',
        external_id:    1,
        manifest_url:   'manifest_url',
        admin_ui_url:   'admin_ui_url'
      ).tap do |ris|
        ris.account = account
        ris.save!
      end
    end

    let(:isis) do
      [].tap do |instances|
        2.times do |isi_index|
          isi = ::Channels::AnyChannel::IntegrationServiceInstance.new(
            registered_integration_service: ris,
            name:                           "isi #{isi_index}",
            metadata:                       '{}',
            state:
              ::Channels::AnyChannel::IntegrationServiceInstance::State::ACTIVE
          ).tap do |new_isi|
            new_isi.account = account
            new_isi.save!
          end

          instances << {'id' => isi.id, 'name' => isi.name}
        end
      end
    end

    it 'returns map of RIS ID to ISI IDs' do
      assert_equal(
        {ris.id => isis},
        context_cache.integration_service_instance_map
      )
    end
  end

  describe '#monitored_facebook_page_map' do
    before do
      Facebook::Page.destroy_all

      @mfp = Facebook::Page.create(
        graph_object_id: '123',
        access_token:    'abc',
        account:         account,
        state:           Facebook::Page::State::ACTIVE
      )

      Facebook::Page.create(
        graph_object_id: '234',
        access_token:    'bcd',
        account:         account,
        state:           Facebook::Page::State::FRESH
      )
    end

    it 'returns map of visible MFP ID to name' do
      assert_equal(
        {@mfp.id => @mfp.name},
        context_cache.monitored_facebook_page_map
      )
    end
  end

  describe '#monitored_twitter_handle_map' do
    before do
      MonitoredTwitterHandle.destroy_all

      @mth = MonitoredTwitterHandle.create!(
        access_token:        '<token>',
        secret:              '<secret>',
        account:             account,
        twitter_user_id:     1,
        twitter_screen_name: 'test name',
        state:               MonitoredTwitterHandle::State::ACTIVE
      )

      MonitoredTwitterHandle.create!(
        access_token:        '<token 2>',
        secret:              '<secret 2>',
        account:             account,
        twitter_user_id:     2,
        twitter_screen_name: 'test name 2',
        state:               MonitoredTwitterHandle::State::UNLINKED
      )
    end

    it 'returns map of visible MTH ID to twitter screen name' do
      assert_equal(
        {@mth.id => @mth.twitter_screen_name},
        context_cache.monitored_twitter_handle_map
      )
    end
  end

  describe '#organization_list' do
    let(:organization_1) { organizations(:minimum_organization1) }
    let(:organization_2) { organizations(:minimum_organization2) }

    let(:account_organizations) do
      [organization_1, organization_2].map do |organization|
        [organization.name, organization.id]
      end
    end

    before do
      account.stubs(fetch_organizations_for_lists: account_organizations)
    end

    it 'returns the account organizations' do
      assert_equal account_organizations, context_cache.organization_list
    end

    describe 'when user is not specified' do
      let(:user) { nil }

      it 'returns the account organizations' do
        assert_equal account_organizations, context_cache.organization_list
      end
    end

    describe 'when the user is an organization restricted agent' do
      before do
        user.stubs(:agent_restriction?).with(:organization).returns(true)

        user.stubs(organization_id: organization_2.id)
      end

      it 'returns the account organizations the user is a member of' do
        assert_equal(
          [[organization_2.name, organization_2.id]],
          context_cache.organization_list
        )
      end

      describe 'and the user is an admin' do
        before { user.stubs(is_admin?: true) }

        it 'returns the account organizations' do
          assert_equal account_organizations, context_cache.organization_list
        end
      end
    end
  end

  describe '#organization_list_size' do
    let(:organization_list) { [1, 2, 3, 4, 5] }

    before do
      account.stubs(fetch_organizations_for_lists: organization_list)
    end

    it 'returns the size of the organization list' do
      assert_equal organization_list.size, context_cache.organization_list_size
    end
  end

  describe '#recipient_emails' do
    let(:emails) { ['test@example.com'] }

    before { account.stubs(recipient_emails: emails) }

    it 'returns the recipient emails for the account' do
      assert_equal emails, context_cache.recipient_emails
    end
  end

  describe '#registered_integration_service_map' do
    let!(:ris) do
      ::Channels::AnyChannel::RegisteredIntegrationService.new(
        name:           'ris',
        search_name:    'ris',
        external_id:    1,
        manifest_url:   'manifest_url',
        admin_ui_url:   'admin_ui_url'
      ).tap do |ris|
        ris.account = account
        ris.save!
      end
    end

    it 'returns map of annotated ID to name' do
      assert_equal(
        {"any_channel:#{ris.id}" => ris.name},
        context_cache.registered_integration_service_map
      )
    end
  end

  describe '#requester_agents' do
    it 'returns the requester agents for the account' do
      assert_equal(
        account.requester_agents.active.pluck(:name, :id),
        context_cache.requester_agents
      )
    end
  end

  describe '#satisfaction_reasons' do
    let(:reasons) { [stub('satisfaction reason')] }

    before { account.stubs(satisfaction_reasons: reasons) }

    it 'returns the satisfaction reasons for the account' do
      assert_equal reasons, context_cache.satisfaction_reasons
    end
  end

  describe '#schedules' do
    before do
      account.stubs(has_multiple_schedules?: true)

      account.schedules.create(name: 'A', time_zone: 'UTC').soft_delete
      account.schedules.create(name: 'B', time_zone: 'UTC')
      account.schedules.create(name: 'C', time_zone: 'UTC')
    end

    it 'returns values for all of the active schedules' do
      assert_equal 2, context_cache.schedules.count
    end

    it 'returns the name and ID of the active schedules' do
      assert_equal(
        account.schedules.active.pluck(:name, :id),
        context_cache.schedules
      )
    end
  end

  describe '#sharing_agreements' do
    let!(:sharing_agreement) do
      create_valid_agreement(
        status_id: Sharing::Agreement::STATUS_MAP[:accepted]
      )
    end

    it 'returns the sharing agreements' do
      assert_equal [sharing_agreement], context_cache.sharing_agreements.to_a
    end
  end
end
