require_relative '../../../support/test_helper'
require_relative '../../../support/rules_test_helper'

SingleCov.covered!

describe Zendesk::Rules::OccamTicketCount do
  let(:now) { Time.now }
  let(:count) { 8 }
  subject do
    Zendesk::Rules::OccamTicketCount.new(
      id: 123, count: count, fresh: true, updated_at: now.to_s, channel: 'bcd',
      refresh: 'poll', poll_wait: 30
    )
  end

  it 'instantiates' do
    assert_equal 123,      subject.rule_id
    assert_equal 8,        subject.value
    assert(subject.fresh)
    assert_equal now.to_s, subject.updated_at
    assert_equal 'poll',   subject.refresh
    assert_equal 'bcd',    subject.channel
    assert_equal 30,       subject.poll_wait

    assert_equal '8', subject.pretty_print
    assert_equal false, subject.is_updating?
    assert subject.send(:age) > 0
  end

  describe 'pretty_print' do
    describe 'age less than a minute' do
      let(:count) { 444 }

      it 'returns value' do
        assert_equal '444', subject.pretty_print
      end
    end

    describe 'age between 1 and 5 minutes and value less than 10' do
      let(:now) { Time.now - 2.minutes }

      it 'returns value' do
        assert_equal '8', subject.pretty_print
      end
    end

    describe 'age between 1 and 5 minutes and value more than 10' do
      let(:now) { Time.now - 2.minutes }
      let(:count) { 111 }

      it 'approximates' do
        assert_equal '~111', subject.pretty_print
      end
    end

    describe 'age more than 10 minutes and value less than 10' do
      let(:now) { Time.now - 11.minutes }

      it 'approximates' do
        assert_equal '~8', subject.pretty_print
      end
    end

    describe 'age between 5 and 10 minutes and value less than 100' do
      let(:now) { Time.now - 6.minutes }
      let(:count) { 89 }

      it 'rounds and approximates' do
        assert_equal '~90', subject.pretty_print
      end
    end

    describe 'age more than 10 minutes and value less than 100' do
      let(:now) { Time.now - 11.minutes }
      let(:count) { 89 }

      it 'rounds and approximates' do
        assert_equal '~90', subject.pretty_print
      end
    end

    describe 'age more than 10 minutes and value more than 100' do
      let(:now) { Time.now - 11.minutes }
      let(:count) { 333 }

      it 'rounds and approximates' do
        assert_equal '~300', subject.pretty_print
      end
    end
  end
end
