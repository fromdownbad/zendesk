require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

require_relative '../../../../../lib/zendesk/rules/trigger/revision_diff'

SingleCov.covered!

describe Zendesk::Rules::Trigger::RevisionDiff do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper

  let(:trigger) do
    create_trigger(
      account: accounts(:minimum),
      definition: build_trigger_definition(
        conditions_all: [build_definition_item],
        conditions_any: [build_definition_item],
        actions:        [build_definition_item]
      )
    )
  end

  let(:source)        { create_revision(trigger) }
  let(:target)        { create_revision(trigger) }
  let(:revision_diff) { create_trigger_revision_diff(source, target) }

  describe '#source_id' do
    it 'returns the `nice_id` of the source revision' do
      assert_equal source.nice_id, revision_diff.source_id
    end
  end

  describe '#target_id' do
    it 'returns the `nice_id` of the target revision' do
      assert_equal target.nice_id, revision_diff.target_id
    end
  end

  describe '#title' do
    it 'returns a change set' do
      assert_equal [change_object('=', trigger.title)], revision_diff.title
    end
  end

  describe '#description' do
    it 'returns a change set' do
      assert_equal(
        [change_object('=', trigger.description)],
        revision_diff.description
      )
    end
  end

  describe '#active?' do
    it 'returns a change set' do
      assert_equal(
        [change_object('=', trigger.is_active?)],
        revision_diff.active?
      )
    end
  end

  describe '#conditions_any' do
    it 'returns a change set' do
      assert_equal(
        [change_object('=', trigger.definition.conditions_any.first)],
        revision_diff.conditions_any
      )
    end
  end

  describe '#conditions_all' do
    it 'returns a change set' do
      assert_equal(
        [change_object('=', trigger.definition.conditions_all.first)],
        revision_diff.conditions_all
      )
    end
  end

  describe '#actions' do
    it 'returns a change set' do
      assert_equal(
        [change_object('=', trigger.definition.actions.first)],
        revision_diff.actions
      )
    end
  end
end
