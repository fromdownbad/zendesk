require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Trigger::TriggerSnapshotSerialization do
  describe '#dump' do
    let(:subject) do
      Zendesk::Rules::Trigger::TriggerSnapshotSerialization.new.dump(object)
    end

    describe 'when the value is a string' do
      let(:object) { 'x' }

      it 'returns the string' do
        assert_equal object, subject
      end
    end

    describe 'when the value is not a string' do
      let(:object) { [] }

      it 'returns a YAML serialized string' do
        assert_equal YAML.dump(object), subject
      end
    end
  end
end
