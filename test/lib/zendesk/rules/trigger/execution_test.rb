require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::Rules::Trigger::Execution do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :tickets

  let(:account)  { accounts(:minimum) }
  let(:events)   { [] }
  let(:ticket)   { tickets(:minimum_1) }
  let(:triggers) { [] }

  let(:described_class) { Zendesk::Rules::Trigger::Execution }

  let(:subject) do
    described_class.new(
      account:  account,
      events:   events,
      ticket:   ticket,
      triggers: triggers
    )
  end

  describe '.execute' do
    let(:ticket_sharing_triggers) { [new_trigger(title: 'ticket_sharing')] }
    let(:prediction_triggers)     { [new_trigger(title: 'prediction')] }

    let(:active_triggers) do
      [new_trigger(title: 'active 1'), new_trigger(title: 'active 2')]
    end

    let(:execution_type) { :default }

    let(:audit)  { Audit.new(account: account, ticket: ticket) }
    let(:events) { [Change.new(audit: audit)] }

    let(:trigger_execution) { stub('trigger execution') }

    let(:span)        { stub(:span) }
    let(:update_type) { 'Update' }

    before do
      account.stubs(
        fetch_active_non_update_triggers: ticket_sharing_triggers,
        fetch_active_prediction_triggers: prediction_triggers,
        fetch_active_triggers:            active_triggers
      )
      span.stubs(parent: stub(service: 'foo'))

      ticket.status_id = StatusType.PENDING

      span.expects(:set_tag).with(Datadog::Ext::Analytics::TAG_ENABLED, true)
      span.expects(:set_tag).with('zendesk.account_id', account.id)

      span.
        expects(:set_tag).
        with('zendesk.account_subdomain', account.subdomain)

      span.expects(:set_tag).with('zendesk.ticket.id', ticket.id)
      span.expects(:set_tag).with('zendesk.parent_span_service', 'foo')

      span.
        expects(:set_tag).
        with('zendesk.ticket.update_type', update_type)

      ZendeskAPM.
        expects(:trace).
        with(
          'trigger.run_loop.execution',
          service: Trigger::APM_SERVICE_NAME
        ).
        yields(span)
    end

    describe 'when invoked' do
      before do
        span.
          expects(:set_tag).
          with('zendesk.trigger.count', active_triggers.length)

        span.
          expects(:set_tag).
          with('zendesk.trigger.execution_type', :default)

        described_class.execute(
          account:        account,
          events:         events,
          execution_type: execution_type,
          ticket:         ticket
        )
      end

      before_should 'execute the active triggers' do
        described_class.
          expects(:new).
          with(
            account:  account,
            events:   events,
            triggers: active_triggers,
            ticket:   ticket
          ).
          returns(trigger_execution)

        trigger_execution.expects(:execute)
      end
    end

    describe 'when the ticket is new' do
      let(:update_type) { 'Create' }

      before do
        ticket.stubs(new_record?: true)

        span.
          expects(:set_tag).
          with('zendesk.trigger.count', active_triggers.length)

        span.
          expects(:set_tag).
          with('zendesk.trigger.execution_type', :default)

        described_class.execute(
          account:        account,
          events:         events,
          execution_type: execution_type,
          ticket:         ticket
        )
      end

      before_should 'execute the active triggers' do
        described_class.
          expects(:new).
          with(
            account:  account,
            events:   events,
            triggers: active_triggers,
            ticket:   ticket
          ).
          returns(trigger_execution)

        trigger_execution.expects(:execute)
      end
    end

    describe 'when the execution type is `ticket_sharing_create`' do
      let(:execution_type) { :ticket_sharing_create }

      before do
        span.
          expects(:set_tag).
          with('zendesk.trigger.count', ticket_sharing_triggers.length)

        span.
          expects(:set_tag).
          with('zendesk.trigger.execution_type', :ticket_sharing_create)

        described_class.execute(
          account:        account,
          events:         events,
          execution_type: execution_type,
          ticket:         ticket
        )
      end

      before_should 'execute the active non update triggers' do
        described_class.
          expects(:new).
          with(
            account:  account,
            events:   events,
            triggers: ticket_sharing_triggers,
            ticket:   ticket
          ).
          returns(trigger_execution)

        trigger_execution.expects(:execute)
      end
    end

    describe 'when the execution type is `satisfaction_prediction`' do
      let(:execution_type) { :satisfaction_prediction }

      before do
        span.
          expects(:set_tag).
          with('zendesk.trigger.count', prediction_triggers.length)

        span.
          expects(:set_tag).
          with('zendesk.trigger.execution_type', :satisfaction_prediction)

        described_class.execute(
          account:        account,
          events:         events,
          execution_type: execution_type,
          ticket:         ticket
        )
      end

      before_should 'execute the active prediction triggers' do
        described_class.
          expects(:new).
          with(
            account:  account,
            events:   events,
            triggers: prediction_triggers,
            ticket:   ticket
          ).
          returns(trigger_execution)

        trigger_execution.expects(:execute)
      end
    end
  end

  describe '#execute' do
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    before do
      Zendesk::Rules::Trigger::RunState.stubs(new: run_state)
    end

    it 'returns the run state' do
      assert_equal run_state, subject.execute
    end

    describe 'when invoked' do
      let(:triggers) do
        [
          stub(id: 1, routing_action: false,  account: account),
          stub(id: 2, routing_action: 'both', account: account)
        ]
      end

      before do
        triggers.each do |trigger|
          trigger.expects(:fire).returns(:mismatch, nil)
        end

        subject.execute
      end

      before_should 'log the result of the trigger run' do
        run_state.
          expects(:log_results).
          with(ticket.id, triggers.length, ticket)
      end
    end

    describe 'when there are discarded triggers' do
      let(:discarded_trigger) do
        stub(id: 1, routing_action: false, account: account)
      end

      let(:expected_trigger) do
        stub(id: 3, routing_action: false, account: account)
      end

      let(:triggers) { [discarded_trigger, expected_trigger] }

      before do
        run_state.discard!(discarded_trigger)

        expected_trigger.stubs(:fire).once.returns([:permanent_mismatch, nil])

        subject.execute
      end

      before_should 'not fire the discarded triggers' do
        discarded_trigger.expects(:fire).never
      end
    end

    describe 'when there are applied triggers' do
      let(:applied_trigger) do
        stub(id: 2, routing_action: false, account: account)
      end

      let(:expected_trigger) do
        stub(id: 3, routing_action: false, account: account)
      end

      let(:triggers) { [applied_trigger, expected_trigger] }

      before do
        run_state.applied!(applied_trigger)

        expected_trigger.stubs(:fire).once.returns([:permanent_mismatch, nil])

        subject.execute
      end

      before_should 'not fire the applied triggers' do
        applied_trigger.expects(:fire).never
      end
    end

    describe 'when a trigger returns an aborted status' do
      let(:trigger_a) { stub(id: 1) }
      let(:trigger_b) { stub(id: 2) }
      let(:triggers)  { [trigger_a, trigger_b] }

      before do
        trigger_a.stubs(fire: [:aborted, nil])

        subject.execute
      end

      before_should 'not execute subsequent triggers' do
        trigger_b.expects(:fire).never
      end

      it 'return an aborted run state' do
        assert run_state.aborted?
      end
    end

    describe 'when a trigger returns a permanent mismatch status' do
      let(:trigger)  { stub(id: 1) }
      let(:triggers) { [trigger] }

      before do
        trigger.stubs(:fire).returns([:permanent_mismatch, nil])

        subject.execute
      end

      before_should 'mark the trigger as discarded' do
        run_state.expects(:discard!).with(trigger)
      end
    end

    describe 'when a trigger returns the mismatch status' do
      let(:trigger)  { stub(id: 1, routing_action: false, account: account) }
      let(:triggers) { [trigger] }

      before do
        trigger.stubs(:fire).returns([:mismatch, []])

        subject.execute
      end

      before_should 'not mark the trigger as applied' do
        run_state.expects(:applied!).never
      end

      before_should 'not mark the trigger as discarded' do
        run_state.expects(:applied!).never
      end

      before_should 'not abort the execution' do
        run_state.expects(:aborted!).never
      end
    end

    describe 'when a trigger returns a match status' do
      let(:trigger_1) { stub(id: 1, routing_action: false, account: account) }
      let(:trigger_2) { stub(id: 2, routing_action: false, account: account) }
      let(:triggers)  { [trigger_1, trigger_2] }

      before do
        trigger_2.stubs(:fire).returns([:match, changes])
      end

      describe 'and it is executed' do
        let(:changes) { [] }

        before do
          trigger_1.stubs(:fire).returns([:mismatch, changes])

          subject.execute
        end

        before_should 'mark the trigger as applied' do
          run_state.expects(:applied!).with(trigger_2)
        end
      end

      describe 'and there are changes' do
        let(:audit)   { Audit.new(account: account, ticket: ticket) }
        let(:changes) { [Change.new(audit: audit)] }

        before { subject.execute }

        before_should 're-execute the loop' do
          trigger_1.expects(:fire).returns([:mismatch, nil]).twice
        end
      end

      describe 'and there are no changes' do
        let(:changes) { [] }

        before { subject.execute }

        before_should 'not re-execute the loop' do
          trigger_1.expects(:fire).returns([:mismatch, nil]).once
        end
      end
    end
  end
end
