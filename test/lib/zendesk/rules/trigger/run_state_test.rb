require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Trigger::RunState do
  fixtures :accounts,
    :tickets

  let(:described_class) { Zendesk::Rules::Trigger::RunState }

  let(:subject) { described_class.new }

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }
  let(:trigger) { stub(id: 2, account: account, routing_action: false) }

  describe '#aborted?' do
    describe 'when the trigger run has been aborted' do
      before { subject.aborted! }

      it 'returns true' do
        assert subject.aborted?
      end
    end

    describe 'when the trigger run has not been aborted' do
      it 'returns false' do
        refute subject.aborted?
      end
    end
  end

  describe '#applied?' do
    describe 'when the trigger has been applied' do
      before { subject.applied!(trigger) }

      it 'returns true' do
        assert subject.applied?(trigger)
      end
    end

    describe 'when the trigger has not been applied' do
      it 'returns false' do
        refute subject.applied?(trigger)
      end
    end
  end

  describe '#discarded?' do
    describe 'when the trigger has been discarded' do
      before { subject.discard!(trigger) }

      it 'returns true' do
        assert subject.discarded?(trigger)
      end
    end

    describe 'when the trigger has not been discarded' do
      it 'returns false' do
        refute subject.discarded?(trigger)
      end
    end
  end

  describe '#log_results' do
    describe 'when the ticket is new' do
      before do
        ticket.stubs(new_record?: true)

        subject.log_results(123, 1, ticket)
      end

      before_should 'record the update type as `Create`' do
        Zendesk::StatsD::Client.
          any_instance.
          expects(:increment).
          with(
            'trigger_execution',
            tags: %w[routing_action:false update_type:Create]
          )
      end
    end

    describe 'when the ticket is not new' do
      before do
        ticket.stubs(new_record?: false)

        subject.log_results(123, 1, ticket)
      end

      before_should 'record the update type as `Change`' do
        Zendesk::StatsD::Client.
          any_instance.
          expects(:increment).
          with(
            'trigger_execution',
            tags: %w[routing_action:false update_type:Change]
          )
      end
    end

    describe 'when a routing action has been applied' do
      let(:trigger) { stub(id: 2, account: account, routing_action: 'both') }

      before do
        subject.applied!(trigger)

        subject.log_results(123, 1, ticket)
      end

      before_should 'record the routing action' do
        Zendesk::StatsD::Client.
          any_instance.
          expects(:increment).
          with(
            'trigger_execution',
            tags: %w[routing_action:both update_type:Change]
          )
      end
    end

    describe 'when no ticket is provided' do
      before { subject.log_results(123, 1) }

      before_should 'record the update type as `Unknown`' do
        Zendesk::StatsD::Client.
          any_instance.
          expects(:increment).
          with(
            'trigger_execution',
            tags: %w[routing_action:false update_type:Unknown]
          )
      end
    end
  end

  describe '#memoize' do
    let(:key_1) { :key_1 }
    let(:key_2) { :key_2 }

    let(:result_1) { stub('result 1') }
    let(:result_2) { stub('result 2') }
    let(:result_3) { stub('result 3') }

    before { subject.memoize(key_1) { result_1 } }

    describe 'when the key has not been memoized before' do
      let!(:yielded_result) do
        subject.memoize(key_2) { result_2 }
      end

      before_should 'increment checks' do
        subject.expects(:increment_checks).once
      end

      it 'returns the yielded result' do
        assert_equal result_2, yielded_result
      end
    end

    describe 'when the key has been memoized before' do
      let!(:memoized_result) do
        subject.memoize(key_1) { result_3 }
      end

      before_should 'increment checks elided' do
        subject.expects(:increment_checks_elided).once
      end

      it 'returns the memoized result' do
        assert_equal result_1, memoized_result
      end
    end
  end

  describe '#increment_match_by_source' do
    status = ['status_id', 'status_id']
    requester_cf = ['requester.custom_fields.foo', 'requester_custom_fields']
    other_cf = ['bar.custom_fields.foo', 'other_custom_fields']
    ticket_field = ['ticket_fields_1234', 'ticket_fields']

    [status, requester_cf, other_cf, ticket_field].each do |source, key|
      describe "when the source is #{source}" do
        before do
          subject.match_time_by_source[key] = 0.123
          subject.checks_by_source[key] = 1

          subject.increment_match_by_source(source, 0.1)
        end

        it "increments the number of checks for #{key}" do
          assert_equal 2, subject.checks_by_source[key]
        end

        it "increments the match time for that #{key}" do
          assert_in_delta 0.223, subject.match_time_by_source[key]
        end
      end
    end
  end
end
