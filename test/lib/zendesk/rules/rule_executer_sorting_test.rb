require_relative '../../../support/test_helper'

SingleCov.not_covered!

describe 'RuleExecuterSorting' do
  fixtures :accounts,
    :users,
    :groups,
    :memberships,
    :organizations,
    :subscriptions,
    :tickets,
    :ticket_fields,
    :ticket_field_entries,
    :events,
    :rules

  let(:account) { accounts(:minimum) }

  let(:rule) do
    View.new(title: 'some view', owner: account).tap do |view|
      view.definition = Definition.new.tap do |definition|
        definition.conditions_all.push(
          DefinitionItem.new('status_id', 'less_than', [StatusType.CLOSED])
        )
      end

      view.output = Output.create(
        columns: %i[requester assignee submitter group organization]
      )

      # field and type are defined in lower let() blocks
      if field
        if type == :order
          view.output.order = field
        else
          view.output.type  = 'table'
          view.output.group = field
        end
      end
    end
  end

  let(:user)     { users(:minimum_agent) }
  let(:executer) { Zendesk::Rules::RuleExecuter.new(rule, user, {}) }
  let(:query)    { executer.query }

  describe 'for grouping and sorting' do
    {
      nil => {order: 'tickets.nice_id DESC'},
      # special case
      :status => {order: 'tickets.status_id DESC'},
      :subject => {
        order: "IFNULL(NULLIF(TRIM(tickets.subject), ''), " \
                 'tickets.description) DESC'
      },
      :nice_id => {order: 'tickets.nice_id DESC'},
      :submitter => {
        order: 'submitter.name DESC',
        joins: 'LEFT JOIN users AS submitter ON tickets.submitter_id = ' \
                 'submitter.id'
      },
      :requester => {
        order: 'requester.name DESC',
        joins: 'LEFT JOIN users AS requester ON tickets.requester_id = ' \
                 'requester.id'
      },
      :assignee => {
        order: 'assignee.name DESC',
        joins: 'LEFT JOIN users AS assignee ON tickets.assignee_id = ' \
                 'assignee.id'
      },
      :priority => {order: 'tickets.priority_id DESC'},
      :type => {order: 'tickets.ticket_type_id DESC'},
      :group => {
        order: 'group.name DESC',
        joins: 'LEFT JOIN groups AS group ON tickets.group_id = group.id'
      },
      :organization => {
        order: 'organization.name DESC',
        joins: 'LEFT JOIN organizations AS organization ON ' \
                 'tickets.organization_id = organization.id'
      },
      :updated_by_type => {order: 'tickets.updated_by_type_id DESC'},
      :satisfaction_score => {order: 'tickets.satisfaction_score DESC'},
      :satisfaction_reason_code => {
        order: 'tickets.satisfaction_reason_code DESC'
      },
      :via => {order: 'tickets.via_id DESC'},
      :created => {order: 'tickets.created_at DESC'},
      :updated => {order: 'tickets.updated_at DESC'},
      :solved => {order: 'tickets.solved_at DESC'},
      [:due_date, :due_at] => {order: 'tickets.due_date DESC'},
      :assigned => {order: 'tickets.assigned_at DESC'},
      [:requester_updated_at,
       :updated_requester] => {order: 'tickets.requester_updated_at DESC'},
      [:assignee_updated_at,
       :updated_assignee] => {order: 'tickets.assignee_updated_at DESC'},
      :current_tags => {order: 'tickets.current_tags DESC'},
      :recipient => {order: 'tickets.recipient DESC'},
      [:locale_id, :locale] => {
        order: 'FIELD(IFNULL(tickets.locale_id, 1), 1) DESC'
      },
      :ticket_form => {order: 'tickets.ticket_form_id DESC'},
      :brand => {
        order: 'brand.name DESC',
        joins: 'LEFT JOIN brands AS brand ON tickets.brand_id = brand.id'
      },
      :sla_next_breach_at => {
        order: 'tickets.sla_breach_status DESC, tickets.created_at DESC'
      },
      :status_updated_at => {order: 'tickets.status_updated_at DESC'},
      :satisfaction_probability => {
        order: 'ticket_predictions.satisfaction_probability DESC',
        joins: 'LEFT JOIN ticket_predictions ON tickets.id = ' \
                 'ticket_predictions.ticket_id'
      }
    }.each do |key, expected|
      fields = Array(key)
      fields.each do |field|
        field_str = field.nil? ? 'nil' : field.to_s

        %i[group order].each do |type|
          describe "for field #{field_str} as output.#{type}" do
            let(:type)  { type }
            let(:field) { field }

            it 'adds join and order correctly' do
              # nice id is always added at the end
              expected_order = [
                expected[:order],
                'tickets.nice_id DESC'
              ].uniq.join(', ')

              expected_joins = expected[:joins] || ''

              assert_equal expected_order, query_orders
              assert_equal expected_joins, query_joins
            end
          end
        end
      end
    end
  end

  describe 'status with hold enabled' do
    before do
      account.stubs(use_status_hold?: true)
    end

    let(:field) { :status_id }
    let(:type)  { :order }

    it 'uses field ordering' do
      assert_equal(
        'FIELD(tickets.status_id, ' \
           "#{Zendesk::Types::StatusType.order.join(', ')}) DESC, " \
           'tickets.nice_id DESC',
        query_orders
      )

      assert_equal '', query_joins
    end
  end

  describe 'for grouping and sorting, custom fields' do
    {
      'FieldDate date1' => {
        order: 'custom_ticket_field_<id>.value DESC',
        join:  'LEFT JOIN ticket_field_entries AS custom_ticket_field_<id> ' \
                 'ON tickets.id = custom_ticket_field_<id>.ticket_id AND ' \
                 'custom_ticket_field_<id>.ticket_field_id = <id>'
      },
      'FieldCheckbox checkbox1' => {
        order: 'custom_ticket_field_<id>.value DESC',
        join:  'LEFT JOIN ticket_field_entries AS custom_ticket_field_<id> ' \
                 'ON tickets.id = custom_ticket_field_<id>.ticket_id AND ' \
                 'custom_ticket_field_<id>.ticket_field_id = <id>'
      },
      'FieldTagger tagger1' => {
        order: "IFNULL(custom_ticket_field_<id>.value, '') DESC",
        join:  'LEFT JOIN ticket_field_entries AS custom_ticket_field_<id> ' \
                 'ON tickets.id = custom_ticket_field_<id>.ticket_id AND ' \
                 'custom_ticket_field_<id>.ticket_field_id = <id>'
      },
      'FieldRegexp regexp1' => {
        order: 'custom_ticket_field_<id>.value DESC',
        join:  'LEFT JOIN ticket_field_entries AS custom_ticket_field_<id> ' \
                 'ON tickets.id = custom_ticket_field_<id>.ticket_id AND ' \
                 'custom_ticket_field_<id>.ticket_field_id = <id>'
      },
      'FieldText text1' => {
        order: "IFNULL(custom_ticket_field_<id>.value, '') DESC",
        join:  'LEFT JOIN ticket_field_entries AS custom_ticket_field_<id> ' \
                 'ON tickets.id = custom_ticket_field_<id>.ticket_id AND ' \
                 'custom_ticket_field_<id>.ticket_field_id = <id>'
      },
      'FieldTextarea textarea1' => {
        order: 'custom_ticket_field_<id>.value DESC',
        join:  'LEFT JOIN ticket_field_entries AS custom_ticket_field_<id> ' \
                 'ON tickets.id = custom_ticket_field_<id>.ticket_id AND ' \
                 'custom_ticket_field_<id>.ticket_field_id = <id>'
      },
      'FieldInteger integer1' => {
        order: 'IFNULL(CAST(custom_ticket_field_<id>.value AS SIGNED ' \
                 'INTEGER), CAST(-~0 AS SIGNED INTEGER)) DESC',
        join:  'LEFT JOIN ticket_field_entries AS custom_ticket_field_<id> ' \
                 'ON tickets.id = custom_ticket_field_<id>.ticket_id AND ' \
                 'custom_ticket_field_<id>.ticket_field_id = <id>'
      },
      'FieldDecimal decimal1' => {
        order: 'IFNULL(custom_ticket_field_<id>.value * 1, CAST(-~0 AS ' \
                 'DECIMAL(10,4))) DESC',
        join:  'LEFT JOIN ticket_field_entries AS custom_ticket_field_<id> ' \
                 'ON tickets.id = custom_ticket_field_<id>.ticket_id AND ' \
                 'custom_ticket_field_<id>.ticket_field_id = <id>'
      }
    }.each do |key, expected|
      klass_name, fname = key.split

      %i[group order].each do |type|
        describe "for field type #{klass_name}, #{fname} as output.#{type}" do
          let(:custom_field) { setup_field(klass_name, fname) }
          let(:field)        { custom_field.id.to_s.to_sym }
          let(:type)         { type }

          it 'adds join and order correctly' do
            expected_order = [
              expected[:order].gsub('<id>', custom_field.id.to_s),
              'tickets.nice_id DESC'
            ].uniq.join(', ')

            expected_joins = expected[:join].
              gsub('<id>', custom_field.id.to_s) || ''

            assert_equal expected_order, query_orders
            assert_equal expected_joins, query_joins
          end
        end
      end
    end
  end

  describe 'user/org custom fields' do
    [
      'User         CustomField::Text     cf_text100     for            order',
      'User         CustomField::Decimal  cf_decimal100  for            order',
      'User         CustomField::Integer  cf_int100      for            order',
      'User         CustomField::Checkbox cf_cb100       for            order',
      'User         CustomField::Date     cf_date100     for            order',
      'User         CustomField::Regexp   cf_regexp100   for            order',
      'User         CustomField::Dropdown cf_dropdown100 for            order',
      'Organization CustomField::Text     cf_text100     for            order',
      'Organization CustomField::Decimal  cf_decimal100  for            order',
      'Organization CustomField::Integer  cf_int100      for            order',
      'Organization CustomField::Checkbox cf_cb100       for            order',
      'Organization CustomField::Date     cf_date100     for            order',
      'Organization CustomField::Regexp   cf_regexp100   for            order',
      'Organization CustomField::Dropdown cf_dropdown100 for            order',
      'User         CustomField::Text     cf_text101     for            group',
      'User         CustomField::Decimal  cf_decimal101  for            group',
      'User         CustomField::Integer  cf_int101      for            group',
      'User         CustomField::Checkbox cf_cb101       for            group',
      'User         CustomField::Date     cf_date101     for            group',
      'User         CustomField::Regexp   cf_regexp101   for            group',
      'User         CustomField::Dropdown cf_dropdown101 for            group',
      'Organization CustomField::Text     cf_text101     for            group',
      'Organization CustomField::Decimal  cf_decimal101  for            group',
      'Organization CustomField::Integer  cf_int101      for            group',
      'Organization CustomField::Checkbox cf_cb101       for            group',
      'Organization CustomField::Date     cf_date101     for            group',
      'Organization CustomField::Regexp   cf_regexp101   for            group',
      'Organization CustomField::Dropdown cf_dropdown101 for            group'
    ].each do |stmt|
      describe "with #{stmt}" do
        field_type, field_klass, _, _, type = stmt.split

        let(:org_field) do
          setup_user_org_field(field_klass, field_type)
        end

        let(:field) { org_field.id.to_s.to_sym }
        let(:type)  { type.to_sym }

        it 'does not add any query ordering or joins' do
          assert_equal '', query_joins
          assert_equal 'tickets.nice_id DESC', query_orders
        end
      end
    end
  end

  def setup_field(klass_name, field_name)
    klass = klass_name.constantize
    account = accounts(:minimum)

    args = {account: account, is_active: true, title: field_name}

    case klass_name
    when 'FieldCheckbox'
      args.merge(tag: 'tag1')
    when 'FieldTagger'
      args[:custom_field_options] = [
        CustomFieldOption.new(name: 'foo', value: 'foo')
      ]
    when 'FieldRegexp'
      args[:regexp_for_validation] = "'[^']*'"
    when %w[FieldDate FieldInteger FieldDecimal FieldText FieldTextarea]
      nil
    end
    field = klass.create!(args)

    field
  end

  def setup_user_org_field(field_klass, field_type)
    klass = field_klass.constantize

    field_name = (field_type + '_' + klass.name.downcase.gsub('::', '_')).
      downcase

    custom_field = klass.create(
      account:   accounts(:minimum),
      key:       field_name,
      is_active: true,
      title:     "#{field_name}_1",
      owner:     field_type.capitalize
    )

    case field_klass
    when 'CustomField::Dropdown'
      custom_field.dropdown_choices.build(name: 'Choice 1', value: 'choice_1')
    when 'CustomField::Regexp'
      custom_field.regexp_for_validation = "'[^']*'"
    end
    custom_field.save!

    custom_field
  end

  def build_query(field: nil, type: :order); end

  def query_orders
    query.order.join(', ').delete('`')
  end

  def query_joins
    query.sql_joins.join(', ').delete('`')
  end
end
