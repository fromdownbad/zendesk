require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 5

describe Zendesk::Rules::UserViewInitializer do
  fixtures :rules

  subject { {} }

  before do
    @account  = accounts(:minimum)
    @view     = rules(:minimum_custom_fields_view)
    @key      = :user_view
    @instance = Zendesk::Rules::UserViewInitializer.new(
      @account,
      subject.with_indifferent_access,
      @key
    )
  end

  describe '#normalize_params' do
    it('handle no params') { assert true }

    describe 'with active' do
      subject { {user_view: {active: true}} }

      it 'converts to is_active' do
        assert @instance.params[:user_view][:is_active]
        assert_nil @instance.params[:user_view][:active]
      end
    end
  end

  describe 'all' do
    describe 'definition' do
      subject do
        {
          user_view: {
            all: [{field: 'is_active', operator: 'is', value: 'true'}]
          }
        }
      end

      it 'removes the params' do
        assert_nil @instance.params[:user_view][:all]
      end

      it 'adds them to the definition' do
        item = @instance.params[:user_view][:definition].conditions_all.first

        assert_equal 'is_active', item.source
        assert_equal 'is', item.operator
        assert_equal 'true', item.value
      end
    end
  end

  describe 'any' do
    describe 'definition' do
      subject do
        {
          user_view: {
            all: [{field: 'is_active', operator: 'is', value: 'true'}]
          }
        }
      end

      it 'removes the params' do
        assert_nil @instance.params[:user_view][:any]
      end

      it 'adds them to the definition' do
        item = @instance.params[:user_view][:definition].conditions_all.first

        assert_equal 'is_active', item.source
        assert_equal 'is', item.operator
        assert_equal 'true', item.value
      end
    end
  end

  describe 'restriction' do
    describe 'with parameters' do
      subject { {user_view: {restriction: {id: '123', type: 'User'}}} }

      it 'removes restriction' do
        assert_nil @instance.params[:user_view][:restriction]
      end

      it 'sets owner_type' do
        assert_equal 'User', @instance.params[:user_view][:owner_type]
      end

      it 'sets owner_id' do
        assert_equal 123, @instance.params[:user_view][:owner_id]
      end
    end

    describe 'with invalid type' do
      subject { {} }

      it 'raises argument error' do
        assert_raises(
          Zendesk::Rules::UserViewInitializer::InvalidArgument,
          I18n.t(
            'txt.api.v2.user_views.restriction.errors.type',
            type: 'Hello'
          )
        ) do
          Zendesk::Rules::UserViewInitializer.new(
            @account,
            {user_view: {restriction: {id: '123', type: 'hello'}}},
            @key
          )
        end
      end
    end

    describe 'with account' do
      subject { {} }

      it 'raises argument error' do
        assert_raises(
          Zendesk::Rules::UserViewInitializer::InvalidArgument,
          I18n.t(
            'txt.api.v2.user_views.restriction.errors.type',
            type: 'Account'
          )
        ) do
          Zendesk::Rules::UserViewInitializer.new(
            @account,
            {user_view: {restriction: {id: '1', type: 'Account'}}},
            @key
          )
        end
      end
    end
  end

  describe 'output' do
    describe 'with parameters' do
      subject do
        {
          user_view: {
            execution: {columns: %i[id name created_at organization_id]}
          }
        }
      end

      it 'creates an output' do
        assert_equal(
          %i[id name created_at organization_id],
          @instance.params[:user_view][:output].columns
        )
      end

      it 'ignores sorting even if it is by id' do
        subject do
          {
            user_view: {
              execution: {
                columns: %i[id name created_at organization_id],
                sort:    {id: :id, order: :asc}
              }
            }
          }
        end

        assert_nil @instance.params[:user_view][:output].sort
      end
    end
  end

  describe '#find_or_build_view' do
    describe 'with an :id' do
      describe 'that is invalid' do
        subject { {id: '9259634529'} }

        it 'raises ActiveRecord::RecordNotfound' do
          assert_raises ActiveRecord::RecordNotFound do
            @instance.send(:find_or_build_view)
          end
        end
      end

      describe 'a valid one' do
        subject { {id: @view.id} }

        it 'returns it if you please' do
          assert_equal @view, @instance.send(:find_or_build_view)
        end
      end

      describe 'with attributes' do
        subject { {id: @view.id, user_view: {title: 'New Title'}} }

        it 'updates the attributes' do
          view = @instance.send(:find_or_build_view)

          assert view.title_changed?
          assert_equal 'New Title', view.title
        end
      end
    end

    describe 'with no id' do
      it 'builds a new record' do
        assert @instance.send(:find_or_build_view).new_record?
      end

      describe 'with attributes' do
        subject { {user_view: {title: 'Whabba, Whalla, Wanna'}} }

        it 'sets them' do
          view = @instance.send(:find_or_build_view)

          assert_equal 'Whabba, Whalla, Wanna', view.title
        end
      end
    end
  end
end
