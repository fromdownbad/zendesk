require_relative '../../../support/test_helper'
require_relative '../../../support/collaboration_settings_test_helper'

SingleCov.not_covered!

describe 'ComponentsActions' do
  fixtures :accounts,
    :translation_locales

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_agent)
    @context = Zendesk::Rules::Context.
      new(@account, @user, component_type: :action)
  end

  def self.should_be_available_for(def_name, *types)
    types.each do |type|
      it "is available for #{type}" do
        assert definition(def_name, type)
      end
    end
  end

  def self.should_not_be_available_for(def_name, *types)
    types.each do |type|
      it "does not be available for #{type}" do
        refute definition(def_name, type)
      end
    end
  end

  describe 'subject action' do
    should_be_available_for('subject', :macro)
    should_not_be_available_for('subject', :trigger, :automation)
  end

  describe 'status_id action' do
    it 'includes hold if the account supports it' do
      @account.stubs(:use_status_hold?).returns(true)
      d = definition('status_id', :trigger)

      assert d
      assert_includes(
        d[:values][:list],
        title: 'On-hold',
        value: StatusType.HOLD
      )
    end

    it 'does not include NEW' do
      definition_titles('status_id', :trigger).wont_include 'New'
    end

    it 'does not include CLOSED for macros' do
      definition_titles('status_id', :macro).wont_include 'Closed'
    end
  end

  describe 'ticket_form_id action' do
    before do
      @account.stubs(:has_ticket_forms?).returns(true)

      params = {
        ticket_form: {
          name:             'My Wombat Form',
          display_name:     'My Wombat Form',
          default:          true,
          end_user_visible: true,
          position:         1
        }
      }

      Zendesk::TicketForms::Initializer.new(@account, params).ticket_form.save!
      @account.ticket_forms.reload
    end

    describe "when the account doesn't have ticket forms feature" do
      before { @account.stubs(:has_ticket_forms?).returns(false) }

      should_not_be_available_for(
        'ticket_form_id',
        :trigger,
        :automation,
        :macro
      )
    end

    describe 'when the account does have ticket forms feature' do
      should_be_available_for('ticket_form_id', :trigger, :automation, :macro)

      it 'includes all ticket forms' do
        @account.ticket_forms.each do |form|
          definition_titles('ticket_form_id', :trigger).must_include form.name
        end
      end

      it 'includes the default ticket form option for trigger' do
        definition_titles('ticket_form_id', :trigger).
          must_include('(default form)')
      end

      it 'includes the default ticket form option for macro' do
        definition_titles('ticket_form_id', :macro).
          must_include('(default form)')
      end

      it 'includes the default ticket form option for automation' do
        d = definition('ticket_form_id', :automation)

        assert d[:values][:list].detect { |v| v[:title] == '(default form)' }
      end
    end
  end

  describe 'brand_id action' do
    before do
      @account.stubs(:has_multibrand?).returns(true)
      @brand = FactoryBot.create(:brand, account_id: @account.id)
      @account.brands.reload
    end

    describe "when the account doesn't have brands feature" do
      before { @account.stubs(:has_multibrand?).returns(false) }

      should_not_be_available_for('brand_id', :trigger, :automation, :macro)
    end

    describe 'when the account does have brands feature' do
      should_be_available_for('brand_id', :trigger, :macro)
      should_not_be_available_for('brand_id', :automation)

      it 'includes all brands' do
        @account.brands.each do |brand|
          definition_titles('brand_id', :trigger).must_include brand.name
        end
      end
    end
  end

  describe 'priority_id action' do
    it 'includes all the priorities except reset' do
      values = definition_values('priority_id', :trigger)
      PriorityType.list.each do |_key, value|
        if value.zero?
          values.wont_include value
        else
          values.must_include value
        end
      end
    end
  end

  describe 'ticket_type_id action' do
    it 'includes all the types except reset' do
      values = definition_values('ticket_type_id', :trigger)
      PriorityType.list.each do |_key, value|
        if value.zero?
          values.wont_include value
        else
          values.must_include value
        end
      end
    end
  end

  describe 'group_id action' do
    it 'includes active groups' do
      @account.groups.each do |group|
        definition('group_id', :trigger)[:values][:list].
          must_include(title: group.name, value: group.id)
      end
    end

    it 'includes current_groups in macro context' do
      assert_includes definition_values('group_id', :macro), 'current_groups'
    end
  end

  describe 'assignee_id action' do
    it 'includes assignable agents' do
      User.assignable(@account).each do |user|
        definition('assignee_id', :trigger)[:values][:list].
          must_include(title: user.name, value: user.id)
      end
    end

    it 'includes the current_user value for macros and triggers' do
      assert_includes definition_values('assignee_id', :macro), 'current_user'
      assert_includes definition_values('assignee_id', :trigger), 'current_user'
    end

    it 'includes the requester value for triggers' do
      assert_includes definition_values('assignee_id', :trigger), 'requester_id'
    end
  end

  %w[set_tags current_tags remove_tags].each do |field|
    describe "#{field} action" do
      it 'is included' do
        definition(field, :trigger)[:values][:type].must_equal 'text'
      end
    end
  end

  describe 'comment_value' do
    should_be_available_for('comment_value', :macro)
  end

  describe 'comment_mode_is_public action' do
    should_be_available_for('comment_mode_is_public', :macro)

    it 'includes true/false selections' do
      assert_equal(
        %w[true false],
        definition_values('comment_mode_is_public', :macro)
      )
    end
  end

  describe 'notification_user action' do
    should_be_available_for('notification_user', :trigger, :automation)
    should_not_be_available_for('notification_user', :macro)

    it 'includes requester_id and assignee_id' do
      values = definition_values('notification_user', :trigger)
      assert_includes values, 'requester_id'
      assert_includes values, 'assignee_id'
    end

    it 'includes current_user for triggers' do
      assert_includes(
        definition_values('notification_user', :trigger),
        'current_user'
      )
    end

    it 'includes all_agents for triggers' do
      assert_includes(
        definition_values('notification_user', :trigger),
        'all_agents'
      )
    end
  end

  describe 'notification_group action' do
    should_be_available_for('notification_group', :trigger, :automation)
    should_not_be_available_for('notification_group', :macro)

    it 'includes group_id value (assigned group)' do
      assert_includes(
        definition_values('notification_group', :trigger),
        'group_id'
      )
    end

    it 'includes active groups' do
      @account.groups.each do |g|
        assert_includes definition_values('notification_group', :trigger), g.id
      end
    end
  end

  describe 'notification_target action' do
    before do
      @target = UrlTarget.create!(
        title:     'Url Target',
        account:   @account,
        is_active: true,
        url:       'http://www.example.com/a?foo=bar',
        attribute: 'message',
        method:    'GET'
      )
    end

    should_be_available_for('notification_target', :trigger, :automation)
    should_not_be_available_for('notification_target', :macro)

    it 'only be available when there are targets in the account' do
      @account.targets.map(&:destroy)
      @account.reload
      refute definition('notification_target', :trigger)
    end
  end

  describe 'tweet_requester action' do
    [
      [true, 'pre_deprecation'],
      [true, 'post_deprecation'],
      [false, 'pre_deprecation'],
      [false, 'post_deprecation']
    ].each do |arturo, account_creation_time|
      describe "when tweet_requester_action arturo is #{arturo} and account was created #{account_creation_time}" do
        let(:deprecation_date) do
          ZendeskRules::Definitions::Actions::FEATURE_DEPRECATION_DATE
        end

        before do
          Account.any_instance.stubs(has_tweet_requester_action?: arturo)

          created_at = if account_creation_time == 'pre_deprecation'
            deprecation_date - 1
          else
            deprecation_date + 1
          end

          Account.any_instance.stubs(created_at: created_at)
        end

        should_not_be_available_for('tweet_requester', :macro)
        if arturo == false && account_creation_time == 'post_deprecation'
          should_not_be_available_for('tweet_requester', :trigger, :automation)
        else
          should_be_available_for('tweet_requester', :trigger, :automation)
        end
      end
    end
  end

  describe 'add cc action' do
    should_be_available_for('cc', :trigger, :automation, :macro)
    it 'includes all agents' do
      @account.agents.each do |a|
        assert_includes definition_values('cc', :trigger), a.id
        assert_includes definition_values('cc', :macro), a.id
      end
    end

    it 'includes (current_user) in trigger and macro context' do
      assert_includes definition_values('cc', :trigger), 'current_user'
      assert_includes definition_values('cc', :macro), 'current_user'
    end

    describe 'without collaboration enabled' do
      before do
        @account.stubs(is_collaboration_enabled?: false)
      end

      describe 'with CCs/Followers settings enabled' do
        before do
          CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
        end

        should_be_available_for('cc', :trigger)
      end

      describe 'with CCs/Followers settings disabled' do
        before do
          CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
        end

        should_not_be_available_for('cc', :trigger)
      end
    end
  end

  describe 'share ticket action' do
    before do
      @account.stubs(has_ticket_sharing_triggers?: true)

      @agreement = FactoryBot.create(
        :agreement,
        remote_url: 'example.com',
        status:     :accepted,
        direction:  :out,
        account:    @account
      )
    end

    should_be_available_for('share_ticket', :trigger, :automation)
    should_not_be_available_for('share_ticket', :macro)

    describe "when the accout doesn't have ticket sharing triggers" do
      before { @account.stubs(has_ticket_sharing_triggers?: false) }

      should_not_be_available_for('share_ticket', :trigger)
    end

    it 'lists the sharing agreements' do
      assert_equal [@agreement.id], definition_values('share_ticket', :trigger)
    end
  end

  describe 'set locale_id action' do
    before do
      @account.subscription.stubs(has_individual_language_selection?: true)
    end

    describe 'with more than 1 language' do
      before do
        @account.allowed_translation_locales << TranslationLocale.
          where(locale: 'pt-BR').
          first
      end

      should_be_available_for('locale_id', :trigger)

      it 'includes available languages' do
        assert_equal 2, definition_values('locale_id', :trigger).size
      end
    end
  end

  describe 'macro_id_after action' do
    describe 'when rules can reference macros' do
      before do
        @account.stubs(:has_rules_can_reference_macros?).returns(true)
      end

      should_be_available_for('macro_id_after', :trigger, :automation)

      it 'includes active macros' do
        assert_equal(
          @account.shared_macros.active.map(&:id),
          definition_values('macro_id_after', :trigger)
        )
      end
    end
  end

  describe 'satisfaction_score action' do
    describe_with_arturo_enabled :customer_satisfaction do
      describe_with_arturo_setting_enabled :customer_satisfaction do
        should_be_available_for('satisfaction_score', :trigger, :automation)

        it 'includes just the OFFERED value' do
          assert_equal(
            [SatisfactionType.OFFERED],
            definition_values('satisfaction_score', :trigger)
          )
        end
      end
    end
  end

  describe 'ticket custom date field action' do
    before do
      @date_field = FieldDate.create!(
        account: @account,
        title:   'Custom wombat date',
        is_active:  true
      )
    end

    it 'is present when date_custom_field is enabled #1' do
      assert_equal(
        'custom_date',
        definition("ticket_fields_#{@date_field.id}", :trigger)[:values][:type]
      )
    end
  end

  describe 'a checkbox' do
    it 'is settable as an action in triggers and macros' do
      checkbox = FieldCheckbox.
        create!(account: @account, tag: 'bar', title: 'massive')

      assert definition_values("ticket_fields_#{checkbox.id}", :trigger)
      assert definition_values("ticket_fields_#{checkbox.id}", :macro)
    end
  end

  %w[user organization].each do |type|
    Zendesk::CustomField::CustomFieldManager.
      type_map.
      except('tagger').
      each do |field_type, klass_name|
        describe "#{type} custom field rules" do
          before do
            @base_key = type == 'user' ? 'requester' : 'organization'

            klass = klass_name.constantize

            params = {
              account: @account,
              key:     "cf_#{field_type}",
              title:   "CF #{field_type}",
              owner:   type.capitalize
            }

            params[:regexp_for_validation] = '.*' if field_type == 'regexp'

            active_field   = klass.new(params)
            inactive_field = klass.new(
              params.merge(
                key:       "cf_#{field_type}_inactive",
                title:     "CF #{field_type} (inactive)",
                is_active: false
              )
            )

            if field_type == 'dropdown'
              active_field.
                dropdown_choices.
                build(name: 'Choice 1', value: 'choice_1')

              inactive_field.
                dropdown_choices.
                build(name: 'Choice 2', value: 'choice_2')
            end

            active_field.save!
            inactive_field.save!
          end

          describe "#{field_type} fields" do
            before do
              @field_name = "cf_#{field_type.downcase}"
            end

            %i[trigger automation].each do |rule_type|
              describe "in #{rule_type} rule_type" do
                before { @context.options[:rule_type] = rule_type }

                it 'is available if active' do
                  assert(
                    definition(
                      "#{@base_key}.custom_fields.#{@field_name}",
                      rule_type
                    )
                  )
                end

                it 'is not available if inactive' do
                  refute(
                    definition(
                      "#{@base_key}.custom_fields.#{@field_name}_inactive",
                      rule_type
                    )
                  )
                end
              end

              describe 'and without user_and_organization_fields feature' do
                before do
                  @account.stubs(has_user_and_organization_fields?: false)
                end

                it 'does not return a definition' do
                  refute(
                    definition(
                      "#{@base_key}.custom_fields.#{@field_name}",
                      rule_type
                    )
                  )
                end
              end
            end
          end
        end
      end
  end

  private

  def generate_definitions(rule_type)
    @context.options[:rule_type] = rule_type
    ZendeskRules::Definitions::Actions.definitions_as_json(@context)
  end

  def definition_values(form_name, rule_type, _key = :value)
    definition(form_name, rule_type)[:values][:list].map { |v| v[:value] }
  end

  def definition_titles(form_name, rule_type)
    definition(form_name, rule_type)[:values][:list].map { |v| v[:title] }
  end

  def definition(form_name, rule_type)
    generate_definitions(rule_type).detect do |d|
      d[:value] == form_name.to_s
    end
  end
end
