require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Zendesk::Rules::PositionUpdate do
  include TestSupport::Rule::Helper

  fixtures :users, :groups

  let(:described_class) { Zendesk::Rules::PositionUpdate }

  let(:user)    { users(:minimum_admin) }
  let(:account) { user.account }
  let(:group)   { groups(:minimum_group) }

  let(:context) { Zendesk::RuleSelection::Context::View.new(user) }

  let(:scope) do
    Zendesk::RuleSelection::Scope.new(
      context,
      Zendesk::RuleSelection::Filter.new(
        context,
        user_filter: Zendesk::RuleSelection::UserFilter::Shared.new(context)
      )
    ).sorted
  end

  before do
    Rule.destroy_all

    create_view(title: 'AA', owner: account, active: true,  position: 1)
    create_view(title: 'BA', owner: account, active: false, position: 2)
    create_view(title: 'ZC', owner: account, active: true,  position: 4)
    create_view(title: 'RQ', owner: account, active: true,  position: 7)
    create_view(title: 'MN', owner: account, active: false, position: 10)
    create_view(title: 'QQ', owner: account, active: true,  position: 11)

    create_view(title: 'PH', owner: group, active: true,  position: 8)
    create_view(title: 'FD', owner: group, active: false, position: 5)

    create_view(title: 'LG', owner: user, active: true,  position: 3)
    create_view(title: 'RT', owner: user, active: false, position: 7)
    create_view(title: 'VK', owner: user, active: true,  position: 8)

    account.subscription.stubs(has_group_rules?: true)
    account.subscription.stubs(has_personal_rules?: true)
  end

  describe '.perform' do
    let(:shared_rules) do
      scope.map { |rule| {title: rule.title, position: rule.position} }
    end

    let(:personal_rules) do
      user.views.map { |rule| {title: rule.title, position: rule.position} }
    end

    let(:updates) do
      [
        {id: Rule.where(title: 'AA').first.id, position: 3},
        {id: Rule.where(title: 'MN').first.id, position: 2},
        {id: Rule.where(title: 'RT').first.id, position: 1}
      ]
    end

    before { described_class.perform(context, updates) }

    describe 'when user has `manage-shared` permission' do
      let(:user) { users(:minimum_admin) }

      it "updates shared rules' positions" do
        assert_equal(
          [
            {title: 'BA', position: 1},
            {title: 'MN', position: 2},
            {title: 'AA', position: 3},
            {title: 'ZC', position: 4},
            {title: 'FD', position: 5},
            {title: 'RQ', position: 6},
            {title: 'PH', position: 7},
            {title: 'QQ', position: 8}
          ],
          shared_rules
        )
      end

      it "updates personal rules' positions" do
        assert_equal(
          [
            {title: 'RT', position: 1},
            {title: 'LG', position: 2},
            {title: 'VK', position: 3}
          ],
          personal_rules
        )
      end
    end

    describe 'when user does not have `manage-shared` permission' do
      let(:user) { users(:minimum_agent) }

      it "does not update shared rules' positions" do
        assert_equal(
          [
            {title: 'AA', position: 1},
            {title: 'BA', position: 2},
            {title: 'ZC', position: 4},
            {title: 'FD', position: 5},
            {title: 'RQ', position: 7},
            {title: 'PH', position: 8},
            {title: 'MN', position: 10},
            {title: 'QQ', position: 11}
          ],
          shared_rules
        )
      end

      it "updates personal rules' position" do
        assert_equal(
          [
            {title: 'RT', position: 1},
            {title: 'LG', position: 2},
            {title: 'VK', position: 3}
          ],
          personal_rules
        )
      end
    end
  end

  describe '#perform' do
    before do
      @sql_queries = sql_queries do
        described_class.new(scope, updates).perform
      end
    end

    describe 'when updating multiple rules' do
      let(:updates) do
        [
          {id: Rule.where(title: 'AA').first.id, position: 3},
          {id: Rule.where(title: 'MN').first.id, position: 2}
        ]
      end

      it 'updates the positions properly' do
        assert_equal %w[BA MN AA ZC FD RQ PH QQ], scope.map(&:title)
      end
    end

    describe 'when there are multiple updates with same position' do
      let(:updates) do
        [
          {id: Rule.where(title: 'QQ').first.id, position: 5},
          {id: Rule.where(title: 'ZC').first.id, position: 5}
        ]
      end

      it 'only respects the last update with that position' do
        assert_equal %w[AA BA FD RQ ZC PH MN QQ], scope.map(&:title)
      end
    end

    describe 'when there are multiple updates with same ID' do
      let(:updates) do
        [
          {id: Rule.where(title: 'ZC').first.id, position: 3},
          {id: Rule.where(title: 'ZC').first.id, position: 2},
          {id: Rule.where(title: 'FD').first.id, position: 7}
        ]
      end

      it 'only respects the last update with that ID' do
        assert_equal %w[AA ZC BA RQ PH MN FD QQ], scope.map(&:title)
      end
    end

    describe 'when the update position is out of bounds' do
      let(:updates) do
        [
          {id: Rule.where(title: 'RQ').first.id, position: -420},
          {id: Rule.where(title: 'BA').first.id, position: 10_000_000},
          {id: Rule.where(title: 'AA').first.id, position: 99_999}
        ]
      end

      it 'adds those rules to the end in order of position' do
        assert_equal %w[ZC FD PH MN QQ RQ AA BA], scope.map(&:title)
      end
    end

    describe 'when there are updates with rules outside of scope' do
      let(:updates) do
        [
          {id: Rule.where(title: 'QQ').first.id, position: 5},
          {id: Rule.where(title: 'VK').first.id, position: 3}
        ]
      end

      it 'does not include them in result' do
        assert_equal %w[AA BA ZC FD QQ RQ PH MN], scope.map(&:title)
      end
    end

    describe 'when there are no valid updates' do
      let(:updates) { [] }

      it 'does not perform an update' do
        @sql_queries.join("\n").wont_include 'UPDATE `rules`'
      end
    end

    describe 'when a rule does not change position' do
      let(:updates) { [{id: Rule.where(title: 'QQ').first.id, position: 3}] }

      it 'is not updated' do
        @sql_queries.
          join("\n").
          must_match(/UPDATE `rules`.*FIELD\(id, NULL/)
      end
    end
  end
end
