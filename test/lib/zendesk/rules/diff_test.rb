require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

require 'simplediff-ruby'

SingleCov.covered!

describe Zendesk::Rules::Diff do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper

  extend TestSupport::Rule::Helper
  extend TestSupport::Rule::DiffHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:source)  { create_trigger(title: 'title 1') }
  let(:target)  { create_trigger(title: 'title 2') }

  let(:null_rule) do
    Rule.new.tap do |rule|
      rule.definition = Definition.new
      rule.is_active  = nil
    end
  end

  let(:rule_diff) do
    Zendesk::Rules::Diff.new(source: source, target: target)
  end

  describe '#source_id' do
    it 'returns the `id` of the source' do
      assert_equal source.id, rule_diff.source_id
    end
  end

  describe '#target_id' do
    it 'returns the `id` of the target' do
      assert_equal target.id, rule_diff.target_id
    end
  end

  describe '#title' do
    describe 'when the title did not change' do
      let(:source) { create_trigger(title: 'title') }
      let(:target) { create_trigger(title: 'title') }

      it 'returns a change set representing no changes were made' do
        assert_equal [change_object('=', 'title')], rule_diff.title
      end
    end

    describe 'when the title changed' do
      let(:source) { create_trigger(title: 'old title') }
      let(:target) { create_trigger(title: 'new title') }

      it 'returns a change set representing the change' do
        assert_equal(
          [change_object('-', 'old title'), change_object('+', 'new title')],
          rule_diff.title
        )
      end
    end
  end

  describe '#description' do
    describe 'when the source and target descriptions were not defined' do
      let(:source) { create_trigger(description: nil) }
      let(:target) { create_trigger(description: nil) }

      it 'returns an empty change set' do
        assert_equal [], rule_diff.description
      end
    end

    describe 'when a description was added to the target' do
      let(:source) { create_trigger(description: nil) }
      let(:target) { create_trigger(description: 'description') }

      it 'returns a change set representing the addition' do
        assert_equal(
          [change_object('+', 'description')],
          rule_diff.description
        )
      end
    end

    describe 'when a description was removed from the target' do
      let(:source) { create_trigger(description: 'description') }
      let(:target) { create_trigger(description: nil) }

      it 'returns a change set representing the removal' do
        assert_equal(
          [change_object('-', 'description')],
          rule_diff.description
        )
      end
    end

    describe 'when the description was changed in the target' do
      let(:source) { create_trigger(description: 'old description') }
      let(:target) { create_trigger(description: 'new description') }

      it 'returns a change set representing the change' do
        assert_equal(
          [
            change_object('-', 'old description'),
            change_object('+', 'new description')
          ],
          rule_diff.description
        )
      end
    end

    describe 'when the description was unchanged in the target' do
      let(:source) { create_trigger(description: 'description') }
      let(:target) { create_trigger(description: 'description') }

      it 'returns a change set representing no changes were made' do
        assert_equal(
          [change_object('=', 'description')],
          rule_diff.description
        )
      end
    end
  end

  describe '#active?' do
    describe 'when the source and target active state was not defined' do
      let(:source) { null_rule }
      let(:target) { null_rule }

      it 'returns an empty change set' do
        assert_equal [], rule_diff.active?
      end
    end

    describe 'when the active state was added in the target' do
      let(:source) { null_rule }
      let(:target) { create_trigger(active: true) }

      it 'returns a change set representing the addition' do
        assert_equal [change_object('+', true)], rule_diff.active?
      end
    end

    describe 'when the active state was removed in the target' do
      let(:source) { create_trigger(active: true) }
      let(:target) { null_rule }

      it 'returns a change set representing the removal' do
        assert_equal [change_object('-', true)], rule_diff.active?
      end
    end

    describe 'when the active state changed in the target' do
      let(:source) { create_trigger(active: true) }
      let(:target) { create_trigger(active: false) }

      it 'returns a change set representing the change' do
        assert_equal(
          [change_object('-', true), change_object('+', false)],
          rule_diff.active?
        )
      end
    end

    describe 'when the active state did not change in the target' do
      let(:source) { create_trigger(active: true) }
      let(:target) { create_trigger(active: true) }

      it 'returns a change set representing no changes were made' do
        assert_equal [change_object('=', true)], rule_diff.active?
      end
    end
  end

  describe 'definition item methods' do
    condition_1 = {field: 'status_id', operator: 'is', value: 1}
    condition_2 = {field: 'status_id', operator: 'is', value: 2}
    condition_3 = {field: 'status_id', operator: 'is', value: 3}
    condition_4 = {field: 'status_id', operator: 'is', value: 4}

    [
      {
        change:   'empty',
        source:   [],
        target:   [],
        expected: []
      },
      {
        change:   'addition',
        source:   [],
        target:   [condition_1],
        expected: [change_object('+', build_definition_item(condition_1))]
      },
      {
        change:   'addition of multiple items',
        source:   [],
        target:   [condition_1, condition_2],
        expected: [
          change_object('+', build_definition_item(condition_1)),
          change_object('+', build_definition_item(condition_2))
        ]
      },
      {
        change:   'removal',
        source:   [condition_1],
        target:   [],
        expected: [change_object('-', build_definition_item(condition_1))]
      },
      {
        change:   'removal of multiple items',
        source:   [condition_1, condition_2],
        target:   [],
        expected: [
          change_object('-', build_definition_item(condition_1)),
          change_object('-', build_definition_item(condition_2))
        ]
      },
      {
        change:   'unchanged',
        source:   [condition_1],
        target:   [condition_1],
        expected: [change_object('=', build_definition_item(condition_1))]
      },
      {
        change:   'appended',
        source:   [condition_1],
        target:   [condition_1, condition_2],
        expected: [
          change_object('=', build_definition_item(condition_1)),
          change_object('+', build_definition_item(condition_2))
        ]
      },
      {
        change:   'prepended',
        source:   [condition_1],
        target:   [condition_2, condition_1],
        expected: [
          change_object('+', build_definition_item(condition_2)),
          change_object('=', build_definition_item(condition_1))
        ]
      },
      {
        change:   'deleted',
        source:   [condition_1, condition_2],
        target:   [condition_1],
        expected: [
          change_object('=', build_definition_item(condition_1)),
          change_object('-', build_definition_item(condition_2))
        ]
      },
      {
        change:   'changed',
        source:   [condition_1],
        target:   [condition_2],
        expected: [
          change_object('-', build_definition_item(condition_1)),
          change_object('+', build_definition_item(condition_2))
        ]
      },
      {
        change:   'changed certain items',
        source:   [condition_1, condition_2, condition_3],
        target:   [condition_1, condition_4],
        expected: [
          change_object('=', build_definition_item(condition_1)),
          change_object('-', build_definition_item(condition_2)),
          change_object('-', build_definition_item(condition_3)),
          change_object('+', build_definition_item(condition_4))
        ]
      }
    ].each do |test|
      describe "when the state change is: #{test[:change]}" do
        if test[:source].empty?
          let(:source) { null_rule }
        else
          let(:source) do
            items = test[:source].map { |args| build_definition_item(args) }

            create_trigger(
              definition: build_trigger_definition(
                conditions_all: items,
                conditions_any: items,
                actions:        items
              )
            )
          end
        end

        if test[:target].empty?
          let(:target) { null_rule }
        else
          let(:target) do
            items = test[:target].map { |args| build_definition_item(args) }

            create_trigger(
              definition: build_trigger_definition(
                conditions_all: items,
                conditions_any: items,
                actions:        items
              )
            )
          end
        end

        describe '#conditions_all' do
          it 'returns a change set representing the change' do
            assert_equal(test[:expected], rule_diff.conditions_all)
          end
        end

        describe '#conditions_any' do
          it 'returns a change set representing the change' do
            assert_equal(test[:expected], rule_diff.conditions_any)
          end
        end

        describe '#actions' do
          it 'returns a change set representing the change' do
            assert_equal(test[:expected], rule_diff.actions)
          end
        end
      end
    end
  end
end
