require_relative '../../../support/test_helper'
require_relative '../../../support/business_hours_test_helper'

SingleCov.covered! uncovered: 47

describe Zendesk::Rules::RuleExecuter do
  fixtures :all

  let(:count_from_occam) { 0 }

  before do
    @minimum_account = accounts(:minimum)
    @minimum_agent   = users(:minimum_agent)

    stub_occam_count(count_from_occam)
  end

  describe '.find_tickets' do
    describe 'given a valid rule' do
      let(:options) do
        Zendesk::Rules::ExecutionOptions.
          new(@rule, @rule.account, is_api: false)
      end

      before do
        @rule = View.new(
          title:  'Bad View',
          owner:  accounts(:minimum),
          output: Output.create(columns: View::COLUMNS)
        )

        @rule.definition = Definition.new.tap do |definition|
          definition.conditions_all.push(
            DefinitionItem.new('status_id', 'less_than', StatusType.CLOSED)
          )
        end

        @rule.output = Output.create({})
        @rule.save!
      end

      it 'queries occam' do
        Zendesk::Rules::RuleExecuter.
          any_instance.
          expects(:find_tickets_with_occam_client).
          returns([])

        @result = Zendesk::Rules::RuleExecuter.find_tickets(
          @rule,
          accounts(:minimum).owner,
          paginate: true
        )
      end

      it 'adds virtual attributes to results' do
        stub_occam_find([tickets(:minimum_1).id])

        Zendesk::Rules::RuleExecuter.
          any_instance.
          expects(:add_virtual_attributes_to_tickets!)

        @result = Zendesk::Rules::RuleExecuter.find_tickets(
          @rule,
          accounts(:minimum).owner,
          paginate: true
        )
      end

      describe 'for out of range ticket queries' do
        before do
          page_size = 9

          stub_occam_find([tickets(:minimum_1).id])

          @result = Zendesk::Rules::RuleExecuter.find_tickets(
            @rule,
            accounts(:minimum).owner,
            paginate: true,
            per_page: page_size,
            page:     1
          )

          @total_tickets = @result.size
          @cached_count  = @rule.ticket_count(
            accounts(:minimum).owner,
            refresh: false
          )

          stub_occam_find([])

          # lets try an out of range query i.e ask for page which does not exist
          @result = Zendesk::Rules::RuleExecuter.find_tickets(
            @rule,
            accounts(:minimum).owner,
            paginate: true,
            per_page: page_size,
            page:     2
          )

          @cached_count = @rule.ticket_count(
            accounts(:minimum).owner,
            refresh: false
          )
        end

        it 'does not update cache ticket count' do
          assert @total_tickets > @result.size
          assert_equal @result.size, 0
          assert_equal @cached_count.value, @total_tickets
        end
      end

      describe 'with a rule that joins' do
        before do
          @rule.definition.conditions_all.push(
            DefinitionItem.new(
              'requester.custom_fields.decimal1',
              'greater_than',
              '0.1'
            ),
            DefinitionItem.new(
              'requester.custom_fields.decimal1',
              'less_than',
              '0.3'
            )
          )

          stub_occam_find([])
        end

        it 'works' do
          Zendesk::Rules::RuleExecuter.
            new(@rule, @rule.account.owner).
            find_tickets
        end
      end
    end
  end

  describe '.count_tickets' do
    describe 'given an impossible rule' do
      let(:count_from_occam) { 0 }
      before do
        @rule = View.new(
          title: 'Bad View',
          owner: accounts(:minimum),
          output: Output.create(columns: View::COLUMNS)
        )

        @rule.definition = Definition.new.tap do |definition|
          definition.conditions_all.push(
            DefinitionItem.new('status_id', 'is', [1]),
            DefinitionItem.new('status_id', 'is', [2])
          )
        end

        @rule.save!

        @result = Zendesk::Rules::RuleExecuter.
          count_tickets(@rule, accounts(:minimum).owner)
      end

      it 'returns zero as result' do
        assert_equal(0, @result)
      end
    end

    describe 'when retreiving archived tickets' do
      let(:rule) do
        new_rule(
          DefinitionItem.new('assignee_id', 'is', 'current_user')
        ).tap do |rule|
          rule.include_archived_tickets = include_archived
        end
      end

      let(:query) do
        Zendesk::Rules::RuleQueryBuilder.
          query_for(rule, @minimum_agent).
          compile(:sql)
      end

      let(:options) do
        Zendesk::Rules::ExecutionOptions.
          new(rule, rule.account, is_api: false)
      end

      describe 'when rule#include_archived_ticket set to true' do
        let(:include_archived) { true }

        it 'sets proper options for occam request' do
          assert options.occam_query_params[:options][:with_archived]
        end
      end

      describe 'when rule#include_archived_ticket set to false' do
        let(:include_archived) { false }

        it 'sets proper options for occam request' do
          refute options.occam_query_params[:options][:with_archived]
        end
      end
    end
  end

  describe 'with occam' do
    let(:params) { {occam: true} }
    let(:occam_client) { Zendesk::Rules::OccamClient.any_instance }
    let(:executer) do
      Zendesk::Rules::RuleExecuter.new(rule, rule.account.owner, params)
    end
    let(:rule) do
      View.new(
        title:  'Some View',
        owner:  accounts(:minimum),
        output: Output.create(columns: View::COLUMNS)
      ).tap do |view|
        view.definition = Definition.new.tap do |definition|
          definition.conditions_all.push(
            DefinitionItem.new('status_id', 'is', StatusType.OPEN)
          )
        end

        view.output = Output.create({})
      end
    end

    describe 'circuit breaking' do
      after { JohnnyFive::MemoryStore.instance.reset_all! }
      before do
        rule.id = 1 # we need a rule ID.

        Zendesk::Rules::OccamClient.
          any_instance.
          stubs(:find).
          raises(Occam::Client::ClientConnectionError, 'ops')

        Zendesk::Rules::OccamClient.
          any_instance.
          stubs(:count).
          raises(Occam::Client::ClientConnectionError, 'ops')

        executer.stubs(:circuit_options).
          returns(failure_threshold: 1, reset_timeout: 1)
      end

      describe_with_arturo_enabled :views_circuit_breaker do
        it 'raises CircuitTrippedError for find' do
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.find_tickets
          end
          # open
          assert_raise(JohnnyFive::CircuitTrippedError) do
            executer.find_tickets
          end
          assert_raise(JohnnyFive::CircuitTrippedError) do
            executer.find_tickets
          end

          Timecop.travel(2)

          # closed
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.find_tickets
          end
          # open
          assert_raise(JohnnyFive::CircuitTrippedError) do
            executer.find_tickets
          end

          Timecop.return
        end

        describe 'with circuit key' do
          let(:account) { accounts(:minimum) }
          let(:origin) { '' }

          let(:find_tickets) do
            Zendesk::Rules::RuleExecuter.find_tickets(
              rule,
              account.owner,
              paginate: true,
              per_page: 1,
              page:     2,
              caller: origin
            )
          end

          describe 'when the caller is recognized' do
            let(:origin) { Zendesk::Rules::View::ViewQueue::PLAY_TICKETS_VERIFICATION }

            it 'uses the key scoped to caller' do
              Zendesk::Rules::OccamClient.
                any_instance.
                expects(:with_circuit).
                with(['occam_circuit', account.id, rule.id, 'find', origin].join(':')).
                returns({'ids' => []})

              find_tickets
            end
          end

          describe 'when the caller is not recognized' do
            let(:origin) { 'something-not-known' }

            it 'uses the key scoped to caller' do
              Zendesk::Rules::OccamClient.
                any_instance.
                expects(:with_circuit).
                with(['occam_circuit', account.id, rule.id, 'find'].join(':')).
                returns({'ids' => []})

              find_tickets
            end
          end
        end
      end

      describe_with_arturo_disabled :views_circuit_breaker do
        it 'does NOT use a circuit breaker for find' do
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.find_tickets
          end
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.find_tickets
          end
        end

        it 'does NOT use a circuit breaker for count' do
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.count_tickets
          end
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.count_tickets
          end
        end
      end
    end

    describe 'on failure' do
      before do
        Zendesk::Rules::OccamClient.
          any_instance.
          stubs(:find).
          returns('error' => 'Error', 'message' => 'occam error')

        executer.stubs(method_to_test).with do
          fail Occam::Client::ClientConnectionError, StandardError.new
        end
      end

      describe 'count' do
        let(:method_to_test) { :count_tickets_with_occam_client }

        it 'should not fallback to classic execution if occam fails' do
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.count_tickets
          end
        end
      end

      describe 'find' do
        let(:method_to_test) { :find_tickets_with_occam_client }

        it 'should not fallback to classic execution if occam fails' do
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.find_tickets
          end
        end
      end
    end
  end

  describe '.add_virtual_attributes_to_tickets' do
    before do
      @custom_field = ticket_fields(:field_text_custom)

      @rule = View.new(
        title:  'Bad View',
        owner:  accounts(:minimum),
        output: Output.create(columns: View::COLUMNS)
      )

      @rule.definition = Definition.new.tap do |definition|
        definition.conditions_all.push(
          DefinitionItem.new('status_id', 'less_than', [StatusType.CLOSED])
        )
      end

      @rule.output = Output.create(
        columns: [
          :requester,
          :assignee,
          :submitter,
          :group,
          :organization,
          @custom_field.id.to_s.to_sym
        ]
      )

      @rule.save!

      @tickets = accounts(:minimum).tickets

      Zendesk::Rules::RuleExecuter.
        new(@rule, nil, {}).
        send(:add_virtual_attributes_to_tickets!, @tickets)

      assert(@tickets.any?)
    end

    it 'adds user names as virtual attributes' do
      assert @tickets.any? { |ticket| ticket.requester.present? }
      assert @tickets.any? { |ticket| ticket.assignee.present? }
      assert @tickets.any? { |ticket| ticket.submitter.present? }

      @tickets.each do |ticket|
        assert ticket.attributes.key?('req_name')
        assert_equal ticket.requester.try(:name), ticket['req_name']

        assert ticket.attributes.key?('assignee_name')
        assert_equal_with_nil(
          ticket.assignee.try(:name),
          ticket['assignee_name']
        )

        assert ticket.attributes.key?('submitter_name')
        assert_equal_with_nil(
          ticket.submitter.try(:name),
          ticket['submitter_name']
        )
      end
    end

    it 'adds group name as virtual attribute' do
      assert @tickets.any? { |ticket| ticket.group.present? }

      @tickets.each do |ticket|
        assert ticket.attributes.key?('group_name')
        assert_equal_with_nil ticket.group.try(:name), ticket['group_name']
      end
    end

    it 'adds organization name as virtual attribute' do
      assert @tickets.any? { |ticket| ticket.organization.present? }

      @tickets.each do |ticket|
        assert ticket.attributes.key?('organization_name')
        assert_equal_with_nil(
          ticket.organization.try(:name),
          ticket['organization_name']
        )
      end
    end

    it 'adds custom fields as virtual attributes' do
      assert @tickets.any? { |ticket| ticket.ticket_field_entries.any? }

      virtual_attribute_name = "field_#{@custom_field.id}"

      @tickets.each do |ticket|
        custom_value = ticket.
          ticket_field_entries.
          find_by_ticket_field_id(@custom_field.
          id).try(:value) || ''

        assert ticket.attributes.key?(virtual_attribute_name)
        assert_equal custom_value, ticket[virtual_attribute_name]
      end
    end
  end

  describe '#expected_sql_from_query' do
    let(:view) { rules(:view_unassigned_to_group) }
    let(:executer) do
      Zendesk::Rules::RuleExecuter.new(view, view.account.owner)
    end

    it 'requests the sql query from occam' do
      executer.occam.expects(:sql)
      executer.expected_sql_from_query
    end
  end

  describe '#expected_sql_count_from_query' do
    let(:view) { rules(:view_unassigned_to_group) }
    let(:executer) do
      Zendesk::Rules::RuleExecuter.new(view, view.account.owner)
    end

    it 'requests the sql_count query from occam' do
      executer.occam.expects(:sql_count)
      executer.expected_sql_count_from_query
    end
  end

  describe '#watchability' do
    let(:rule) do
      new_rule(DefinitionItem.new('assignee_id', 'is', 'current_user'))
    end

    let(:rule_executer) do
      Zendesk::Rules::RuleExecuter.new(rule, rule.account.owner, {})
    end

    before do
      Zendesk::Rules::RuleQueryBuilder.
        stubs(:query_for).
        returns(stub('raw query', cacheability: {cacheable: watchable}))
    end

    describe 'when the query is cachable' do
      let(:watchable) { true }

      it 'is watchable' do
        assert rule_executer.watchability[:cacheable]
      end
    end

    describe 'when the query is not cachable' do
      let(:watchable) { false }

      it 'is not watchable' do
        refute rule_executer.watchability[:cacheable]
      end
    end
  end

  describe 'given comparisons of strings, should not fail on optimize' do
    before do
      Arturo.enable_feature!(:date_custom_field)
      @account = accounts(:minimum)
      @field_date = FieldDate.create!(
        account: @account,
        is_active:  true,
        title:   'date1'
      )

      @rule = View.new(
        title:  'Comparison View',
        owner:  accounts(:minimum),
        output: Output.create(columns: View::COLUMNS)
      )
      @rule.definition = Definition.new.tap do |definition|
        definition.conditions_all.push(
          DefinitionItem.new('status_id', 'less_than', StatusType.CLOSED),
          DefinitionItem.new(
            "ticket_fields_#{@field_date.id}",
            'greater_than_equal',
            '2014-01-01'
          ),
          DefinitionItem.new(
            "ticket_fields_#{@field_date.id}",
            'less_than_equal',
            '2015-01-01'
          )
        )
      end
      @rule.output = Output.create({})
      @rule.save!

      stub_occam_find([tickets(:minimum_1).id])
    end

    it 'does not fail' do
      assert(
        Zendesk::Rules::RuleExecuter.find_tickets(
          @rule,
          accounts(:minimum).owner,
          paginate: true
        )
      )
    end

    should_eventually 'generate correct SQL' do
      q = Zendesk::Rules::RuleQueryBuilder.
        query_for(@rule, users(:minimum_agent))

      sql = "`custom_ticket_field_#{@field_date.id}`.`value` <= " \
        "'2015-01-01' AND `custom_ticket_field_#{@field_date.id}`.`value` " \
        ">= '2014-01-01'"
      # currently will generate < and > instead of <= and >=,
      # hence the should_eventually

      assert(q.to_sql.include?(sql), "query:#{q.to_sql} vs #{sql}")
    end
  end

  describe 'tag_evaluation_optimizer' do
    let(:tag_scores) do
      {
        'common'           => 100,
        'common_indeed'    => 100,
        'very_rare'        => 2,
        'very_rare_indeed' => 2,
        'rare'             => 20,
        'very_very_rare'   => 1
      }
    end

    let(:view) do
      View.new(
        title:  't',
        owner:  accounts(:minimum),
        output: Output.create(columns: View::COLUMNS)
      ).tap do |rule|
        rule.id = 1000
        rule.definition = Definition.new.tap do |definition|
          tags_for_conditions.each do |op, value|
            definition.conditions_all.push(
              DefinitionItem.new('current_tags', op, value)
            )
          end
        end
      end
    end

    let(:rule_executer) do
      Zendesk::Rules::RuleExecuter.new(view, view.account.owner)
    end

    let(:optimized_query) do
      rule_executer.query
    end

    let(:optimized_query_tag_list) do
      optimized_query[:current_tags].map(&:value).map(&:terms)
    end

    before do
      # disable the other two optimizers
      ZendeskRules::TagsJoinOptimizer.blocked [1_000]
      ZendeskRules::TagsLikeOptimizer.blocked [1_000]

      tag_scores.each do |tag, score|
        TagScore.create!(
          account:  accounts(:minimum),
          tag_name: tag,
          score:    score
        )
      end
    end

    describe 'with a list of tags' do
      let(:tags_for_conditions) { [%w[is common], %w[is very_rare]] }

      it 'sorts the rarer tag to the front' do
        assert_equal 'very_rare', optimized_query_tag_list.first.first
      end
    end

    describe 'with a group of tags' do
      let(:tags_for_conditions) do
        [%w[is common], ['includes', 'very_rare very_very_rare']]
      end

      it 'sorts a group of rarer tags to the front' do
        assert_equal(
          %w[very_rare very_very_rare],
          optimized_query_tag_list.first
        )
      end
    end

    describe 'with groups of tags with same scores' do
      let(:tags_for_conditions) do
        [
          ['includes', 'common_indeed common'],
          ['includes', ' very_rare_indeed very_rare']
        ]
      end

      it 'sorts by terms within' do
        assert_equal(
          %w[very_rare very_rare_indeed],
          optimized_query_tag_list.first
        )

        assert_equal %w[common common_indeed], optimized_query_tag_list.last
      end
    end

    describe 'with no tag_score rows' do
      let(:tags_for_conditions) { [%w[is very_rare], %w[is common]] }

      before do
        TagScore.all.each(&:destroy)
      end

      it 'does not optimize' do
        assert_equal %w[very_rare common], optimized_query_tag_list.flatten
      end
    end

    describe 'with a missing tag_score row' do
      let(:tags_for_conditions) do
        [%w[is very_rare], %w[is rare]]
      end

      before do
        TagScore.where(tag_name: 'rare').first.destroy
      end

      it 'treats it the same as zero' do
        assert_equal %w[rare very_rare], optimized_query_tag_list.flatten
      end
    end

    describe 'with same tag_score' do
      let(:tags_for_conditions) { [%w[is very_rare_indeed], %w[is very_rare]] }

      before do
        TagScore.where(tag_name: 'rare').first.destroy
      end

      it 'sorts by the term' do
        assert_equal(
          %w[very_rare very_rare_indeed],
          optimized_query_tag_list.flatten
        )
      end
    end

    describe 'with is_not' do
      let(:tags_for_conditions) do
        [%w[is_not very_rare], %w[is_not common]]
      end

      it 'puts common is_not conditons first' do
        assert_equal %w[common very_rare], optimized_query_tag_list.flatten
      end
    end

    describe 'with not_includes' do
      let(:tags_for_conditions) do
        [%w[is_not very_rare], ['not_includes', %w[rare common]]]
      end

      it 'sorts by the most-common tag, putting it first in the regexp' do
        assert_equal(
          [%w[common rare], %w[very_rare]],
          optimized_query_tag_list
        )
      end
    end
  end

  def new_rule(condition, options = {})
    definition = Definition.new
    definition.send(options[:type] || :actions).push(condition)
    definition.conditions_all.push(condition) if condition
    View.new(
      definition: definition,
      owner: accounts(:minimum),
      output: Output.create(columns: View::COLUMNS)
    )
  end
end
