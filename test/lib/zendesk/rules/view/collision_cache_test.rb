require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 2

describe Zendesk::Rules::View::CollisionCache do
  describe '#collisions_cache' do
    let(:fake_redis) { FakeRedis::Redis.new }

    let(:collisions_cache) do
      Zendesk::Rules::View::CollisionCache.new(
        fake_redis,
        'collision-cache-test-key'
      )
    end

    describe '#upsert' do
      before do
        collisions_cache.upsert(123)
      end

      it 'inserts a new item' do
        assert_equal [123], collisions_cache.read
      end

      describe 'when item exists' do
        before do
          collisions_cache.upsert(123)
        end

        it 'updates the existing item' do
          assert_equal [123], collisions_cache.read
        end
      end

      it 'sets expiry' do
        assert fake_redis.ttl('collision-cache-test-key')
      end
    end

    describe '#read' do
      describe 'when there are items passed expiry' do
        before do
          collisions_cache.upsert(124)
        end

        it 'trims the cache on read' do
          Timecop.travel(21.seconds.from_now) do
            collisions_cache.upsert(125)

            assert_equal [125], collisions_cache.read
          end
        end
      end

      describe 'when there are no items present' do
        it 'returns an empty array' do
          assert_equal [], collisions_cache.read
        end
      end
    end
  end
end
