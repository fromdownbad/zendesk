require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 7

describe Zendesk::Rules::View::ViewQueue do
  fixtures :accounts, :users, :rules, :tickets

  describe '#pop' do
    let(:tickets_from_occam) { view_tickets.map(&:id) }
    let(:span)  { stub(get_tag: 'tag', set_tag: {}, finish: {}) }
    let(:view)  { rules(:view_unassigned_to_group) }
    let(:admin) { users(:minimum_admin) }
    let(:agent) { users(:minimum_agent) }

    let(:view_tickets) do
      [tickets(:minimum_6), tickets(:minimum_5), tickets(:minimum_4)]
    end

    before do
      ZendeskAPM.stubs(:trace).returns(span)
      Zendesk::Rules::View::ViewQueue.stubs(store: FakeRedis::Redis.new)

      Zendesk::Rules::RuleExecuter.any_instance.
        stubs(:find_tickets_with_occam_client).
        returns(tickets_from_occam)

      @queue1 = Zendesk::Rules::View::ViewQueue.new(view, admin, rewind: true)
      @queue2 = Zendesk::Rules::View::ViewQueue.new(view, agent, rewind: true)

      Zendesk::AgentCollision.stubs(:ticket_viewed_by_user_ids).returns []

      view_tickets.each { |t| Zendesk::RedisStore.client.del("Zendesk-ViewQueue-#{t.id}") } # reset locks
    end

    describe_with_and_without_arturo_enabled :view_queue_prevent_removing_seen_tickets_from_shared_queue do
      describe 'with view id present' do
        it 'sets apm tag with view id' do
          span.expects(:set_tag).with('zendesk.view_id', view.id)

          @queue1.pop
        end
      end

      describe 'with view id not present - previews' do
        let(:view) { View.incoming(agent) }

        it 'sets apm tag with view id as preview' do
          span.expects(:set_tag).with('zendesk.view_id', "preview")

          @queue1.pop
        end
      end

      it "returns nil after working off all tickets" do
        assert_equal 0, view_tickets.index(@queue1.pop)

        # Why mock again?
        # return empty results as this is mocking call to find_tickets which is
        # suppose to return nothing as there are no more new tickets in the view
        # as the above request has already served all available tickets
        Zendesk::Rules::RuleExecuter.any_instance.
          stubs(:find_tickets_with_occam_client).
          returns([])
        assert_nil view_tickets.index(@queue1.pop)
      end

      it 'goes through the skipped tickets' do
        @queue1.stubs(:skipped).returns([view_tickets.map(&:id)])
        @queue1.stubs(:ticket_with_lock).returns(view_tickets[0])

        assert_equal view_tickets[0], @queue1.pop
      end

      it 'invokes occam client with valid params' do
        view.expects(:find_tickets).with(
          anything,
          rewind: true,
          caller: 'play-tickets-refill',
          ids_only: true,
          paginate: true,
          per_page: 1000,
          page: 1,
          timeout: 30.seconds
        )

        @queue1.pop
      end

      it 'does not return viewed ticket' do
        Zendesk::AgentCollision.stubs(:ticket_viewed_by_user_ids).with(view_tickets[0]).returns [123]
        assert_equal 1, view_tickets.index(@queue1.pop)
      end

      describe 'when ticket is not available' do
        before do
          Zendesk::Rules::View::ViewQueue.
            any_instance.
            stubs(:ticket_with_lock).
            returns(nil, tickets(:minimum_5))
        end

        it 'skips the ticket' do
          assert_equal tickets(:minimum_5), @queue1.pop
        end
      end

      it 'locks tickets' do
        assert_equal 0, view_tickets.index(@queue1.pop)
        assert_equal admin.id, Zendesk::RedisStore.client.get("Zendesk-ViewQueue-#{view_tickets.first.id}").to_i
        refute Zendesk::RedisStore.client.get("Zendesk-ViewQueue-#{view_tickets.second.id}")
      end

      describe 'when acquiring lock fails' do
        let(:tickets_from_occam) { [view_tickets.first.id] }

        before { @queue1.stubs(acquire_ticket_lock: false) }

        it 'skips the ticket' do
          Zendesk::Rules::RuleExecuter.any_instance.
            stubs(:find_tickets_with_occam_client).
            returns([])

          assert_nil @queue1.pop
        end
      end

      it 'returns ticket locked by me' do
        ticket = @queue1.pop
        view.stubs(:find_tickets).returns(view_tickets.map(&:id))

        queue = Zendesk::Rules::View::ViewQueue.new(view, admin, rewind: true)
        assert_equal ticket, queue.pop
      end

      describe 'when ticket gets removed from view' do
        let(:all_tickets) { view_tickets.map { @queue1.pop } }
        before do
          assert_includes view.find_tickets(admin), view_tickets[1].id

          # Simulate removal of ticket from the view mock occam to return
          # all tickets except the one that was moved out of the queue.
          # TODO: we need to return [] the second time to prevent an infinite loop,
          # this will be refined when we refactor the tests.
          Zendesk::Rules::RuleExecuter.any_instance.
            stubs(:find_tickets_with_occam_client).
            returns(view_tickets.map(&:id) - [view_tickets[1].id], [])
        end

        it 'does not return tickets that no longer match the view' do
          assert_not_includes all_tickets, view_tickets[1].id
        end
      end

      it 'returns ticket viewed by me' do
        Zendesk::AgentCollision.stubs(:ticket_viewed_by_user_ids).returns [admin.id]
        assert_equal 0, view_tickets.index(@queue1.pop)
      end

      it 'does not loop' do
        Zendesk::AgentCollision.stubs(:ticket_viewed_by_user_ids).returns [123]
        Zendesk::Rules::RuleExecuter.any_instance.
          stubs(:find_tickets_with_occam_client).
          returns([])

        assert_nil view_tickets.index(@queue1.pop)
      end

      describe 'when there are no tickets pushed' do
        before do
          view.stubs(find_tickets: view_tickets.map(&:id))
          # Perform an initial pop to populate the user queue cache
          # This is because this test wants to valid behavior of the cache
          # when an existing queue gets empty due to which we will eventually
          # push nil to the value of cache key (when refreshing the ticket)
          @queue1.pop

          # Now the queue is ready, we have a non-empty queue and we wont reload
          # from occam, instead when we pop this time, we will first try to
          # refresh tickets. lets simulate occam retuning no tickets, this will
          # cause us to push nil value to the key in refresh_tickets method
          view.
            stubs(:find_tickets_with_ids).
            with(admin, anything, anything).
            returns([])

          # Peform another pop to have the user_cache_queue set with nil value
          @queue1.pop
        end

        it 'ignores empty entries when reading' do
          # Verify that when we read the shared_cached_queue we ignore the nil value
          # and we get back an empty array instead of an array with `0` in it
          assert_empty @queue1.send(:shared_cached_queue)
        end
      end

      describe 'when there are tickets pushed' do
        let(:cache_key) { @queue1.instance_variable_get('@cached_queue_key') }

        before do
          Timecop.freeze

          view.stubs(find_tickets: view_tickets.map(&:id))
          # Perform an initial pop to populate the user queue cache
          #
          # This is because this test wants to valid behavior of the cache
          # when an existing queue values are refreshed due to which we will
          # eventually push new values to cache key (when refreshing tickets)
          @queue1.pop
        end

        after { Timecop.return }

        it 'doesnt alter the ttl of cache key' do
          current_ttl = Zendesk::RedisStore.client.ttl(cache_key)
          # Pop again and this time the redis key should be updated without TTL
          # being updated
          @queue1.pop
          # the TTL of the key should not be changed
          assert_equal current_ttl, Zendesk::RedisStore.client.ttl(cache_key)
        end
      end

      describe 'when radar throws an error' do
        before do
          Zendesk::AgentCollision.stubs(:ticket_viewed_by_user_ids).raises(StandardError.new)

          ZendeskExceptions::Logger.stubs(:record)
        end

        it 'rescue and returns the ticket' do
          assert @queue1.pop
        end
      end

      describe 'previous collisions' do
        describe 'when there is a previous collision' do
          before do
            @queue1.stubs(:play_span).returns(span)
            @queue1.send(:update_collisions, view_tickets[0].id)
          end

          it 'skips that ticket' do
            assert_not_equal view_tickets[0].id, @queue1.pop.id
          end
        end

        describe 'when the previous collision is not within the time window' do
          before do
            @queue1.stubs(:play_span).returns(span)
            @queue1.send(:update_collisions, view_tickets[0].id)
          end

          it 'does not skip that ticket' do
            Timecop.travel(21.seconds.from_now) do
              assert_equal view_tickets[0].id, @queue1.pop.id
            end
          end
        end
      end

      describe_with_arturo_enabled :skill_based_view_filters do
        let(:matching_ticket) { tickets(:minimum_4) }

        before do
          Zendesk::Routing::TicketSkillMatcher.
            any_instance.
            stubs(:matching_tickets).
            returns([matching_ticket.id])
        end

        describe 'when the view is a filtered view' do
          before do
            admin.account.settings.skill_based_filtered_views = [view.id]

            admin.account.settings.save!
          end

          it 'returns tickets that match user skills' do
            assert_equal matching_ticket, @queue1.pop
          end

          describe 'when the number of tickets in view is greater than MAX_TICKETS_PER_FETCH' do
            before do
              Zendesk::Rules::View::ViewQueue.const_set(:MAX_TICKETS_PER_FETCH, 1)

              # Simulate call to occam with specific id
              Zendesk::Rules::RuleExecuter.any_instance.
                stubs(:find_tickets_with_occam_client).
                returns([matching_ticket.id])
            end

            after do
              Zendesk::Rules::View::ViewQueue.const_set(:MAX_TICKETS_PER_FETCH, 1000)
            end

            it 'returns matched tickets from having to make multiple fetches' do
              assert_equal matching_ticket, @queue1.pop
            end
          end
        end

        describe 'when the view is not a filtered view' do
          it 'does not filter the tickets based on user skills' do
            assert_equal view_tickets.first, @queue1.pop
          end
        end
      end

      describe_with_arturo_disabled :skill_based_view_filters do
        it 'does not filter the tickets based on user skills' do
          assert_equal view_tickets.first, @queue1.pop
        end
      end

      describe '#ticket_with_lock' do
        describe 'when the ticket no longer exists' do
          let(:ticket_id)    { view_tickets.first.id }
          let(:tickets_stub) { stub }

          let(:subject) { @queue1.send(:ticket_with_lock, ticket_id) }

          before do
            Account.any_instance.stubs(tickets: tickets_stub)

            tickets_stub.stubs(:find).raises(ActiveRecord::RecordNotFound)
          end

          it 'logs proper message' do
            Rails.logger.expects(:info).with(
              "ViewQueue: Ticket not found: #{ticket_id},"\
              "account: #{admin.account.id},"\
              "user: #{admin.id}"
            )

            subject
          end
        end

        describe 'when there is an agent collision' do
          before do
            Zendesk::AgentCollision.
              stubs(:ticket_viewed_by_user_ids).
              with(view_tickets[0]).
              returns [123]
          end

          it 'skips that ticket' do
            assert_not_equal view_tickets[0].id, @queue1.pop.id
          end
        end
      end

      describe '#ticket_in_filtered_views?' do
        before do
          admin.account.stubs(has_skill_based_view_filters?: true)
        end

        describe_with_arturo_enabled :filtered_views_discrepancy do
          let(:matching_ticket) { tickets(:minimum_4) }

          before do
            Zendesk::Routing::TicketSkillMatcher.
              any_instance.
              stubs(:matching_tickets).
              returns([matching_ticket.id])
          end

          describe 'when the view is a filtered view' do
            before do
              admin.account.settings.skill_based_filtered_views = [view.id]

              admin.account.settings.save!
            end

            describe 'and the ticket belongs to view' do
              it 'returns true' do
                assert(
                  @queue1.send(:ticket_in_filtered_views?, tickets(:minimum_4).id)
                )
              end
            end
          end
        end

        describe 'when the view is not a filtered view' do
          describe 'and the ticket belongs to view' do
            it 'returns true' do
              assert(
                @queue1.send(:ticket_in_filtered_views?, tickets(:minimum_4))
              )
            end
          end
        end

        describe_with_arturo_disabled :filtered_views_discrepancy do
          it 'returns true' do
            assert @queue1.send(:ticket_in_filtered_views?, tickets(:minimum_4))
          end
        end
      end
    end
  end
end
