require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::View::Output do
  fixtures :accounts, :users

  let(:account)         { accounts(:minimum) }
  let(:user)            { account.owner }
  let(:described_class) { Zendesk::Rules::View::Output }

  let(:definition) do
    {title_for_field: 'Title', output_key: 'id'}
  end

  describe '.for_definition' do
    let(:output) do
      described_class.for_definition(
        definition,
        account: account,
        user:    user
      )
    end

    it 'returns a new `View::Output` object' do
      assert output.instance_of? Zendesk::Rules::View::Output
    end

    it 'has a value' do
      assert_equal 'id', output.value
    end
  end

  describe '#title' do
    let(:output) do
      described_class.new(definition, account: account, user: user)
    end

    before do
      Zendesk::Liquid::DcContext.
        stubs(:render).
        with('Title', account, account.owner).
        returns('Title text')
    end

    it 'returns the title' do
      assert_equal 'Title text', output.title
    end
  end
end
