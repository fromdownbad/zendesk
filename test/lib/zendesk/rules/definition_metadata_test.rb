require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::DefinitionMetadata do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:account_url) { account.url(mapped: false, ssl: true) }

  let(:definition_metadata) do
    Zendesk::Rules::DefinitionMetadata.new(account, user)
  end

  describe '#item_key' do
    describe 'when the subject is `organization_id`' do
      let(:subject) { 'organization_id' }

      it 'returns the item key' do
        assert_equal(
          'organization',
          definition_metadata.item_key(subject)
        )
      end
    end

    describe 'when the subject is not supported' do
      let(:subject) { 'unsupported' }

      it 'returns nil' do
        assert_nil definition_metadata.item_key(subject)
      end
    end
  end

  describe '#collection_key' do
    describe 'when the subject is `organization_id`' do
      let(:subject) { 'organization_id' }

      it 'returns the collection key' do
        assert_equal(
          'organizations',
          definition_metadata.collection_key(subject)
        )
      end
    end

    describe 'when the subject is not supported' do
      let(:subject) { 'unsupported' }

      it 'returns nil' do
        assert_nil definition_metadata.collection_key(subject)
      end
    end
  end

  describe '#phone_numbers' do
    let(:body) do
      {
        'phone_numbers' => [
          {'id' => 123, 'nickname' => 'Jenn', 'display_number' => '(867-5309)'},
          {'id' => 123, 'nickname' => nil, 'display_number' => '(867-5309)'}
        ]
      }
    end

    let(:voice_client) { mock('voice client') }

    before do
      Zendesk::Voice::InternalApiClient.
        stubs(:new).
        with(account.subdomain, timeout: 10.seconds).
        returns(voice_client)
    end

    describe 'when there is no error' do
      before do
        voice_client.
          stubs(:get).
          with(
            '/api/v2/channels/voice/phone_numbers',
            sms_enabled:  true,
            minimal_mode: true
          ).
          returns(mock('response', body: body))
      end

      it 'returns the sms_enabled phone numbers' do
        assert_equal(
          body['phone_numbers'].map do |number|
            if number['nickname']
              {
                value: number['id'],
                title: "#{number['nickname']} #{number['display_number']}"
              }
            else
              {value: number['id'], title: number['display_number']}
            end
          end,
          definition_metadata.phone_numbers.map(&:to_h)
        )
      end

      it 'does not return null values for nameless phone numbers' do
        assert definition_metadata.phone_numbers.map(&:title).all?
      end
    end

    describe 'when there is an error' do
      before do
        voice_client.
          stubs(:get).
          with(
            '/api/v2/channels/voice/phone_numbers',
            sms_enabled:  true,
            minimal_mode: true
          ).
          raises(StandardError)
      end

      it 'returns an empty set of phone numbers' do
        assert_equal [], definition_metadata.phone_numbers
      end
    end
  end
end
