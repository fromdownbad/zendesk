require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::ControllerSupport::DefinitionParams do
  describe 'simple' do
    before do
      @params = {
        sets: {
          '1' => {
            conditions: {
              '0' => {
                'operator' => 'less_than',
                'value'    => {'0' => '2'},
                'source'   => 'status_id'
              },
              '1' => {
                'operator' => 'is',
                'value'    => {'0' => 'current_groups'},
                'source'   => 'group_id'
              },
              '2' => {
                'operator' => 'is_not',
                'value'    => {'0' => ''},
                'source'   => 'group_id'
              }
            }
          }
        }
      }

      @definition_params =
        Zendesk::Rules::ControllerSupport::DefinitionParams.new(@params)
    end

    it 'lookups actions via the :actions key' do
      assert_nil @params[:actions]
      @params[:sets]['1'][:actions] = @params[:sets]['1'][:conditions]

      assert @definition_params[:actions]
    end

    it 'lookups conditions_all via the :conditions_all key' do
      assert @definition_params[:conditions_all]
    end

    it 'lookups conditions_any via the :conditions_any key' do
      assert_nil @params[:actions]

      @params[:sets]['2'] = {conditions: @params[:sets]['1'][:conditions]}

      assert @definition_params[:conditions_any]
    end

    it 'returns nil for unsupported keys' do
      @params[:hello] = 'hi'
      assert_nil @definition_params[:hello]
    end

    it 'returns nil when missing supported keys' do
      @params[:sets]['1'].delete(:conditions)
      assert_nil @definition_params[:conditions_all]
    end

    it 'returns the items sorted by position' do
      sorted_items = [
        {
          'operator' => 'less_than',
          'value'    => {'0' => '2'},
          'source'   => 'status_id'
        },
        {
          'operator' => 'is',
          'value'    => {'0' => 'current_groups'},
          'source'   => 'group_id'
        },
        {
          'operator' => 'is_not',
          'value'    => {'0' => ''},
          'source'   => 'group_id'
        }
      ]

      assert_equal sorted_items, @definition_params[:conditions_all]
    end
  end

  describe 'Large Definitions' do
    before do
      @params = {
        sets: {
          '1' => {
            actions: {
              '10' => {
                'source'   => 'subject',
                'operator' => 'is',
                'value'    => {'0' => '112'}
              },
              '12' => {
                'source'   => 'remove_tags',
                'operator' => 'is',
                'value'    => {'0' => 's'}
              },
              '3' => {
                'source'   => 'comment_value',
                'operator' => 'is',
                'value'    => {'0' => 'd'}
              },
              '4' => {
                'source'   => 'comment_mode_is_public',
                'operator' => 'is',
                'value'    => {'0' => 'true'}
              },
              '8' => {
                'source'   => 'ticket_fields_131',
                'operator' => 'is',
                'value'    => {'0' => '11'}
              },
              '6' => {
                'source'   => 'ticket_fields_121',
                'operator' => 'is',
                'value'    => {'0' => 'true'}
              },
              '7' => {
                'source'   => 'status_id',
                'operator' => 'is',
                'value'    => {'0' => '1'}
              },
              '5' => {
                'source'   => 'ticket_form_id',
                'operator' => 'is',
                'value'    => {'0' => '6'}
              },
              '9' => {
                'source'   => 'priority_id',
                'operator' => 'is',
                'value'    => {'0' => '3'}
              },
              '1' => {
                'source'   => 'ticket_type_id',
                'operator' => 'is',
                'value'    => {'0' => '1'}
              },
              '11' => {
                'source'   => 'group_id',
                'operator' => 'is',
                'value'    => {'0' => '6'}
              },
              '2' => {
                'source'   => 'assignee_id',
                'operator' => 'is',
                'value'    => {'0' => 'requester_id'}
              },
              '13' => {
                'source'   => 'set_tags',
                'operator' => 'is',
                'value'    => {'0' => 'a'}
              },
              '14' => {
                'source'   => 'current_tags',
                'operator' => 'is',
                'value'    => {'0' => 'd'}
              }
            }
          }
        }
      }

      @definition_params =
        Zendesk::Rules::ControllerSupport::DefinitionParams.new(@params)
    end

    it 'returns the items sorted by numeric position position' do
      sorted_items = [
        {
          'source'   => 'ticket_type_id',
          'operator' => 'is',
          'value'    => {'0' => '1'}
        },
        {
          'source'   => 'assignee_id',
          'operator' => 'is',
          'value'    => {'0' => 'requester_id'}
        },
        {
          'source'   => 'comment_value',
          'operator' => 'is',
          'value'    => {'0' => 'd'}
        },
        {
          'source'   => 'comment_mode_is_public',
          'operator' => 'is',
          'value'    => {'0' => 'true'}
        },
        {
          'source'   => 'ticket_form_id',
          'operator' => 'is',
          'value'    => {'0' => '6'}
        },
        {
          'source'   => 'ticket_fields_121',
          'operator' => 'is',
          'value'    => {'0' => 'true'}
        },
        {
          'source'   => 'status_id',
          'operator' => 'is',
          'value'    => {'0' => '1'}
        },
        {
          'source'   => 'ticket_fields_131',
          'operator' => 'is',
          'value'    => {'0' => '11'}
        },
        {
          'source'   => 'priority_id',
          'operator' => 'is',
          'value'    => {'0' => '3'}
        },
        {
          'source'   => 'subject',
          'operator' => 'is',
          'value'    => {'0' => '112'}
        },
        {
          'source'   => 'group_id',
          'operator' => 'is',
          'value'    => {'0' => '6'}
        },
        {
          'source'   => 'remove_tags',
          'operator' => 'is',
          'value'    => {'0' => 's'}
        },
        {
          'source'   => 'set_tags',
          'operator' => 'is',
          'value'    => {'0' => 'a'}
        },
        {
          'source'   => 'current_tags',
          'operator' => 'is',
          'value'    => {'0' => 'd'}
        }
      ]

      assert_equal sorted_items, @definition_params[:actions]
    end
  end
end
