require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 27

describe Zendesk::Rules::ExecutionOptions do
  let(:rule) do
    View.new(title: 'A View', owner: accounts(:minimum)).tap do |v|
      v.definition = Definition.new
      v.definition.conditions_all << DefinitionItem.new('status_id', 'is', 2)
      v.output = Output.create({})
      v.output.order = 'created_at'
      v.output.order_asc = true
      v.output.group = 'status'
      v.output.type = 'table'
      v.save!
    end
  end

  let(:params) { {} }

  let(:options) do
    Zendesk::Rules::ExecutionOptions.new(rule, rule.account, params)
  end
  let(:request_options_keys) do
    %i[lotus_version referer request_source app_id].sort
  end

  describe 'occam_query_params' do
    describe 'preview type' do
      let(:preview_type) { options.occam_query_params[:rule][:preview_type] }

      describe 'incoming type' do
        let(:rule) do
          View.incoming(users(:minimum_agent))
        end
        it 'is incoming type' do
          assert_equal preview_type, 'incoming'
        end
      end

      describe 'my tickets' do
        let(:rule) do
          View.my(users(:minimum_agent))
        end
        it 'is my tickets type' do
          assert_equal preview_type, 'my'
        end
      end

      describe 'my group tickets' do
        let(:rule) do
          View.my_groups(users(:minimum_agent))
        end
        it 'is my group tickets type' do
          assert_equal preview_type, 'my_groups'
        end
      end
    end
  end

  describe 'ordering' do
    describe 'with no params' do
      it "uses the rule's output order" do
        assert_equal 'created_at', options.order
      end

      it 'falls back to nice_id' do
        rule.output.order = nil
        assert_equal :nice_id, options.order
      end

      it "uses the rule's group" do
        assert_equal 'status', options.group
      end
    end

    describe 'with an invalid order param' do
      let(:params) { {order: 'flkxj kljfl 3333'} }

      it 'declines to use it' do
        assert_equal 'created_at', options.order
      end
    end

    describe 'with an automation' do
      let(:rule) { Automation.new }

      it 'does not exist' do
        assert_nil options.order
      end
    end

    describe 'order direction' do
      describe 'with an :order param' do
        let(:params) { {order: 'requester'} }

        it 'honors the `:desc` parameter' do
          params[:desc] = '1'
          assert_equal :desc, options.order_direction
        end

        it 'defaults to ascending if :desc is left off' do
          rule.output.order_asc = false
          assert_equal :asc, options.order_direction
        end
      end
    end
  end

  describe 'with a valid order param' do
    let(:params) { {order: 'requester'} }

    it 'honors it' do
      assert_equal 'requester', options.order
    end

    it 'excludes any grouping' do
      assert_nil options.group
    end
  end

  describe 'watch?' do
    describe 'with the feature flag on' do
      before { Arturo.enable_feature!(:hanlon_watch_count) }

      it 'should be true' do
        assert options.watch?
      end
    end

    describe 'with the feature flag off' do
      before { Arturo.disable_feature!(:hanlon_watch_count) }

      it 'should be false' do
        refute options.watch?
      end
    end
  end

  describe ':request_origin' do
    let(:request_origin) do
      Zendesk::RequestOriginDetails.new(
        'HTTP_X_ZENDESK_LOTUS_VERSION' => 1,
        'HTTP_X_ZENDESK_APP_ID' => 2
      )
    end
    let(:request_origin_result_hash) do
      {
        request_source: 'app',
        lotus_version:  1,
        app_id:         2
      }
    end

    describe 'with request origin in rule' do
      before do
        rule.request_origin = request_origin
      end

      it 'sets request_origin option' do
        assert_equal options.request_origin, request_origin_result_hash
      end

      describe 'without app_id' do
        let(:request_origin) do
          Zendesk::RequestOriginDetails.new(
            'HTTP_X_ZENDESK_LOTUS_VERSION' => 1
          )
        end
        let(:request_origin_result_hash) do
          {
            request_source: 'lotus',
            lotus_version:  1
          }
        end

        it 'does not send app_id key' do
          assert_equal options.request_origin, request_origin_result_hash
        end
      end
    end

    describe 'without request_origin in rule' do
      describe 'with request_origin in params' do
        let(:options) do
          Zendesk::Rules::ExecutionOptions.new(
            rule,
            rule.account,
            params.merge(request_origin: request_origin)
          )
        end

        it 'sets request_origin option' do
          assert_equal options.request_origin, request_origin_result_hash
        end
      end

      describe 'without request_origin in params' do
        before do
          rule.request_origin = nil
        end

        it 'does not set request_origin option' do
          assert_equal({}, options.request_origin)
        end
      end
    end
  end

  describe 'without rule' do
    let(:request_origin) { Zendesk::RequestOriginDetails.new }
    let(:options) do
      Zendesk::Rules::ExecutionOptions.new(nil, accounts(:minimum), params)
    end

    let(:params) do
      {simulate_slow: true, timeout: 30, request_origin: request_origin}
    end

    before do
      Arturo.enable_feature! :hanlon_cache
      Arturo.enable_feature! :hanlon_watch_count
      Arturo.enable_feature! :hanlon_parity_check
    end

    it 'retuns some options' do
      assert options.hanlon_cache?
      assert options.watch?
      assert_equal 30, options.timeout
      assert options.simulate_slow
      assert options.hanlon_parity_check?
    end
  end
end
