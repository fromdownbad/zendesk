require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Zendesk::Rules::DehydratedTicket do
  fixtures :rules,
    :tickets

  let(:automation) { rules(:automation_close_ticket) }
  let(:ticket)     { tickets(:minimum_4) }
  let(:ticket_ids_from_occam) { [ticket.id] }

  before do
    Zendesk::Rules::RuleExecuter.
      any_instance.
      stubs(:find_tickets_with_occam_client).
      returns(ticket_ids_from_occam)
  end

  describe '#dehydrate_tickets' do
    it 'converts tickets' do
      dehydrated = Zendesk::Rules::DehydratedTicket.dehydrate_tickets([ticket])
      dehydrated.length.must_equal 1
      dehydrated.first.nice_id.must_equal ticket.nice_id
      dehydrated.first.generated_timestamp.must_equal(
        ticket.generated_timestamp.to_i
      )
    end
  end

  describe '#hydrate_tickets_for_automation' do
    let(:dehydrated) do
      Zendesk::Rules::DehydratedTicket.dehydrate_tickets([ticket])
    end

    describe 'generated_timestamp is bumped, but it matches the automation still' do
      before do
        ticket.will_be_saved_by(User.system)
        ticket.save
      end

      it 'still includes the ticket' do
        tickets = Zendesk::Rules::DehydratedTicket.
          hydrate_tickets_for_automation(dehydrated, automation)

        tickets.length.must_equal 1
        tickets.first.id.must_equal ticket.id
      end

      it 'logs tickets that still match automation conditions after rehydration' do
        Rails.logger.expects(:info).with(
          "Tickets that still match automation conditions after rehydration | automation id: #{automation.id}, # tickets: 1, tickets by id: #{ticket.id}"
        ).at_least_once

        Zendesk::Rules::DehydratedTicket.any_instance.stubs(:still_matches_automation?).returns(true)

        Zendesk::Rules::DehydratedTicket.
          hydrate_tickets_for_automation(dehydrated, automation)
      end
    end

    describe 'ticket no longer matches automation' do
      let(:ticket_ids_from_occam) { [] }

      before do
        ticket.will_be_saved_by(User.system)
        ticket.status_id = 4
        ticket.save
      end

      it 'excludes the ticket' do
        tickets = Zendesk::Rules::DehydratedTicket.
          hydrate_tickets_for_automation(dehydrated, automation)

        tickets.length.must_equal 0
      end
    end

    it 'serializes correctly' do
      dehydrated.first.as_json.must_equal [ticket.id, ticket.nice_id, ticket.generated_timestamp.to_i]
    end
  end
end
