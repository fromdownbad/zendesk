require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Definitions do
  fixtures :accounts,
    :groups,
    :translation_locales,
    :users

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:context_cache) { stub('context cache') }
  let(:dc_cache)      { stub('dc_cache') }
  let(:rule_context)  { stub('rule context') }
  let(:rule_type)     { :trigger }

  let(:definitions) do
    Zendesk::Rules::Definitions.new(
      account:   account,
      user:      user,
      rule_type: rule_type
    )
  end

  before do
    Zendesk::DynamicContent::AccountContent.
      stubs(cache: dc_cache)

    Zendesk::Rules::Context.
      stubs(:new).
      with(account, user, context_options).
      returns(rule_context)

    Zendesk::Rules::ContextCache.
      stubs(:new).
      with(account, user).
      returns(context_cache)
  end

  describe '#actions' do
    let(:context_options) do
      {
        cache:          context_cache,
        component_type: :action,
        dc_cache:       dc_cache,
        rule_type:      rule_type
      }
    end

    let(:action_definitions) do
      [
        {value: 'status', title: 'Status', values: {type: 'list', list: []}},
        {value: 'type',   title: 'Type',   values: {type: 'list', list: []}}
      ]
    end

    before do
      ZendeskRules::Definitions::Actions.
        stubs(:definitions_as_json).
        with(rule_context).
        returns(
          [
            {value: 'PICKACTION', title: '-- Click to select action. --'},
            *action_definitions
          ]
        )
    end

    describe 'when executed' do
      before { definitions.actions }

      before_should 'construct an action from each definition' do
        action_definitions.each do |definition|
          Zendesk::Rules::Action.
            expects(:for_definition).
            with(definition, account: account)
        end
      end

      it 'omits the `PICKACTION` action' do
        refute(
          definitions.actions.any? { |action| action.subject == 'PICKACTION' }
        )
      end
    end
  end

  describe '#conditions_all' do
    let(:context_options) do
      {
        cache:          context_cache,
        component_type: :condition,
        condition_type: :all,
        dc_cache:       dc_cache,
        rule_type:      :trigger
      }
    end

    let(:condition_definitions) do
      [
        {
          value: 'priority_id',
          title: 'Priority',
          values: {type: 'list', list: []},
          operators: [
            {value: :is,     title: 'Is'},
            {value: :is_not, title: 'Is not'}
          ]
        }
      ]
    end

    before do
      ZendeskRules::Definitions::Conditions.
        stubs(:definitions_as_json).
        with(rule_context).
        returns(
          [
            {
              value: 'PICKCONDITION',
              title: '-- Click to select condition. --'
            },
            *condition_definitions
          ]
        )
    end

    describe 'when executed' do
      before { definitions.conditions_all }

      before_should 'construct a condition from each definition' do
        condition_definitions.each do |definition|
          Zendesk::Rules::Condition.
            expects(:for_definition).
            with(definition, account: account)
        end
      end

      it 'omits the `PICKCONDITION` condition' do
        refute(
          definitions.conditions_all.any? do |condition|
            condition.subject == 'PICKCONDITION'
          end
        )
      end
    end
  end

  describe '#conditions_any' do
    let(:context_options) do
      {
        cache:          context_cache,
        component_type: :condition,
        condition_type: :any,
        dc_cache:       dc_cache,
        rule_type:      :trigger
      }
    end

    let(:condition_definitions) do
      [
        {
          value: 'priority_id',
          title: 'Priority',
          values: {type: 'list', list: []},
          operators: [
            {value: :is,     title: 'Is'},
            {value: :is_not, title: 'Is not'}
          ]
        }
      ]
    end

    before do
      ZendeskRules::Definitions::Conditions.
        stubs(:definitions_as_json).
        with(rule_context).
        returns(
          [
            {
              value: 'PICKCONDITION',
              title: '-- Click to select condition. --'
            },
            *condition_definitions
          ]
        )
    end

    describe 'when executed' do
      before { definitions.conditions_any }

      before_should 'construct a condition from each definition' do
        condition_definitions.each do |definition|
          Zendesk::Rules::Condition.
            expects(:for_definition).
            with(definition, account: account)
        end
      end

      it 'omits the `PICKCONDITION` condition' do
        refute(
          definitions.conditions_any.any? do |condition|
            condition.subject == 'PICKCONDITION'
          end
        )
      end
    end
  end

  describe '#view_output' do
    let(:context_options) do
      {
        cache:          context_cache,
        component_type: :selection,
        rule_type:      :view
      }
    end

    let(:view_output_definitions) do
      [{output_key: 'something', title_for_field: 'some value title'}]
    end

    before do
      ZendeskRules::Definitions::Conditions.
        stubs(:definitions_as_json).
        with(rule_context).
        returns(view_output_definitions)
    end

    describe 'when executed' do
      before { definitions.view_output }

      before_should 'construct a view output from each definition' do
        view_output_definitions.each do |definition|
          Zendesk::Rules::View::Output.
            expects(:for_definition).
            with(definition, account: account, user: account.owner)
        end
      end
    end
  end

  describe '#groupables' do
    let(:context_options) do
      {
        cache:          context_cache,
        component_type: :grouping,
        rule_type:      :view
      }
    end

    let(:view_output_definitions) do
      [{output_key: 'something', title_for_field: 'some value title'}]
    end

    before do
      ZendeskRules::Definitions::Conditions.
        stubs(:definitions_as_json).
        with(rule_context).
        returns(view_output_definitions)
    end

    describe 'when executed' do
      before { definitions.groupables }

      before_should 'construct a view output from each definition' do
        view_output_definitions.each do |definition|
          Zendesk::Rules::View::Output.
            expects(:for_definition).
            with(definition, account: account, user: account.owner)
        end
      end
    end
  end

  describe '#sortables' do
    let(:context_options) do
      {
        cache:          context_cache,
        component_type: :sorting,
        rule_type:      :view
      }
    end

    let(:view_output_definitions) do
      [{output_key: 'something', title_for_field: 'some value title'}]
    end

    before do
      ZendeskRules::Definitions::Conditions.
        stubs(:definitions_as_json).
        with(rule_context).
        returns(view_output_definitions)
    end

    describe 'when executed' do
      before { definitions.sortables }

      before_should 'construct a view output from each definition' do
        view_output_definitions.each do |definition|
          Zendesk::Rules::View::Output.
            expects(:for_definition).
            with(definition, account: account, user: account.owner)
        end
      end
    end
  end
end
