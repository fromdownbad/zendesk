require_relative '../../../support/test_helper'
require_relative '../../../support/business_hours_test_helper'
require_relative '../../../support/rule_query_builder_test_helper'

SingleCov.not_covered!

# TODO: replace generated sql/joins assertions, instead should check that
# generates expected query as json (sent to Occam)
describe 'RuleQueryBuilderCustomUserFields' do
  include CustomFieldsTestHelper
  include BusinessHoursTestHelper
  include RuleQueryBuilderTestHelper

  fixtures :accounts,
    :tickets,
    :users,
    :groups,
    :memberships,
    :ticket_fields,
    :custom_field_options,
    :organizations,
    :organization_memberships

  describe 'Custom user and organization fields' do
    before do
      # don't use these fixtures.  why?  don't know.
      CustomField::Field.without_arsi.delete_all
      CustomField::Value.without_arsi.delete_all

      @account = accounts(:minimum)
    end

    def cf_table_name(field_id, _increment = 1)
      "cfv_#{field_id}"
    end

    describe 'decimal fields' do
      before do
        # Decimal
        @decimal1_id = create_user_custom_field!(
          'decimal1',
          'Whatever',
          'Decimal'
        ).id

        @decimal1_tbl = cf_table_name(@decimal1_id)

        @decimal1_tbl_join = "JOIN `cf_values` AS `#{@decimal1_tbl}` ON " \
          "`tickets`.`requester_id` = `#{@decimal1_tbl}`.`owner_id` AND " \
          "`#{@decimal1_tbl}`.`cf_field_id` = #{@decimal1_id}"

        @decimal1_tbl_leftjoin = 'LEFT ' + @decimal1_tbl_join

        @decimal2_id = create_organization_custom_field!(
          'decimal2',
          'Whatever',
          'Decimal'
        ).id
      end

      it 'handles user decimal fields' do
        query = get_query_for_conditions(
          'requester.custom_fields.decimal1',
          'greater_than_equal',
          '123'
        )

        assert_equal(
          "`#{@decimal1_tbl}`.`value` >= '0000000000123.0000'",
          query.to_sql
        )

        assert_equal [@decimal1_tbl_join], query.sql_joins
      end

      it 'handles blank user decimal fields' do
        query = get_query_for_conditions(
          'requester.custom_fields.decimal1',
          'is',
          ''
        )

        assert_equal "`#{@decimal1_tbl}`.`value` IS NULL", query.to_sql
        assert_equal [@decimal1_tbl_leftjoin], query.sql_joins
      end

      it 'handles non-blank Integer user decimal fields with is_not' do
        query = get_query_for_conditions(
          'requester.custom_fields.decimal1',
          'is_not',
          '123'
        )

        assert_equal(
          "(`#{@decimal1_tbl}`.`value` != '0000000000123.0000' OR " \
            "`#{@decimal1_tbl}`.`value` IS NULL)",
          query.to_sql
        )

        assert_equal [@decimal1_tbl_leftjoin], query.sql_joins
      end

      it 'handles non-blank Float user decimal fields with is_not' do
        query = get_query_for_conditions(
          'requester.custom_fields.decimal1',
          'is_not',
          '123.456'
        )

        assert_equal(
          "(`#{@decimal1_tbl}`.`value` != '0000000000123.4560' OR " \
            "`#{@decimal1_tbl}`.`value` IS NULL)",
          query.to_sql
        )

        assert_equal [@decimal1_tbl_leftjoin], query.sql_joins
      end

      it 'handles non-blank user decimal fields with is_not' do
        query = get_query_for_conditions(
          'requester.custom_fields.decimal1',
          'is_not',
          ''
        )

        assert_equal "`#{@decimal1_tbl}`.`value` IS NOT NULL", query.to_sql
        assert_equal [@decimal1_tbl_leftjoin], query.sql_joins
      end

      it 'handles nil user decimal fields with is_not' do
        query = get_query_for_conditions(
          'requester.custom_fields.decimal1',
          'is_not',
          nil
        )

        assert_equal "`#{@decimal1_tbl}`.`value` IS NOT NULL", query.to_sql
        assert_equal [@decimal1_tbl_leftjoin], query.sql_joins
      end

      it 'handles the `not_present` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.decimal1',
          'not_present',
          nil
        )

        assert_includes query.to_sql, "`#{@decimal1_tbl}`.`value` IS NULL"
        assert query.sql_joins.first =~ /^LEFT JOIN/
      end

      it 'handles organization fields' do
        @decimal2_tbl = cf_table_name(@decimal2_id, 1)

        query = get_query_for_conditions(
          'organization.custom_fields.decimal2',
          'is_not',
          '0000000000000.0000'
        )

        assert_equal(
          "(`#{@decimal2_tbl}`.`value` != '0000000000000.0000' OR " \
            "`#{@decimal2_tbl}`.`value` IS NULL)",
          query.to_sql
        )

        assert_equal(
          query.sql_joins,
          [
            "LEFT JOIN `cf_values` AS `#{@decimal2_tbl}` ON " \
              "`tickets`.`organization_id` = `#{@decimal2_tbl}`.`owner_id` " \
              "AND `#{@decimal2_tbl}`.`cf_field_id` = #{@decimal2_id}"
          ]
        )
      end

      it 'handles both fields at once' do
        @decimal2_tbl = cf_table_name(@decimal2_id, 2)

        query = get_query_for_conditions(
          'requester.custom_fields.decimal1',
          'greater_than_equal',
          '0000000000123.0000',
          'organization.custom_fields.decimal2',
          'is_not',
          '0000000000000.0000'
        )

        sa = "((`#{@decimal2_tbl}`.`value` != '0000000000000.0000' OR " \
          "`#{@decimal2_tbl}`.`value` IS NULL) AND " \
          "`#{@decimal1_tbl}`.`value` >= '0000000000123.0000')"

        assert_equal(
          sa.gsub(/\(|\)/, '').split(' AND ').sort,
          query.to_sql.gsub(/\(|\)/, '').split(' AND ').sort
        )

        assert_includes(
          query.sql_joins,
          "LEFT JOIN `cf_values` AS `#{@decimal2_tbl}` ON " \
            "`tickets`.`organization_id` = `#{@decimal2_tbl}`.`owner_id` AND " \
            "`#{@decimal2_tbl}`.`cf_field_id` = #{@decimal2_id}"
        )
      end
    end

    describe 'integer fields' do
      before do
        # Integer
        @integer1_id = create_user_custom_field!(
          'integer1',
          'Whatever',
          'Integer'
        ).id

        @integer1_tbl = cf_table_name(@integer1_id)

        @integer1_tbl_join = "JOIN `cf_values` AS `#{@integer1_tbl}` ON " \
          "`tickets`.`requester_id` = `#{@integer1_tbl}`.`owner_id` AND " \
          "`#{@integer1_tbl}`.`cf_field_id` = #{@integer1_id}"

        @integer1_tbl_leftjoin = 'LEFT ' + @integer1_tbl_join

        @integer2_id = create_organization_custom_field!(
          'integer2',
          'Whatever',
          'Integer'
        ).id
      end

      it 'handles user integer fields' do
        query = get_query_for_conditions(
          'requester.custom_fields.integer1',
          'greater_than_equal',
          '123'
        )

        assert_equal(
          "`#{@integer1_tbl}`.`value` >= '0000000000123'",
          query.to_sql
        )

        assert_equal [@integer1_tbl_join], query.sql_joins
      end

      it 'handles blank user integer fields' do
        query = get_query_for_conditions(
          'requester.custom_fields.integer1',
          'is',
          ''
        )

        assert_equal "`#{@integer1_tbl}`.`value` IS NULL", query.to_sql
        assert_equal [@integer1_tbl_leftjoin], query.sql_joins
      end

      it 'handles non-blank Integer user integer fields with is_not' do
        query = get_query_for_conditions(
          'requester.custom_fields.integer1',
          'is_not',
          '123'
        )

        assert_equal(
          "(`#{@integer1_tbl}`.`value` != '0000000000123' OR " \
            "`#{@integer1_tbl}`.`value` IS NULL)",
          query.to_sql
        )

        assert_equal [@integer1_tbl_leftjoin], query.sql_joins
      end

      it 'handles non-blank user integer fields with is_not' do
        query = get_query_for_conditions(
          'requester.custom_fields.integer1',
          'is_not',
          ''
        )

        assert_equal "`#{@integer1_tbl}`.`value` IS NOT NULL", query.to_sql
        assert_equal [@integer1_tbl_leftjoin], query.sql_joins
      end

      it 'handles nil user integer fields with is_not' do
        query = get_query_for_conditions(
          'requester.custom_fields.integer1',
          'is_not',
          nil
        )

        assert_equal "`#{@integer1_tbl}`.`value` IS NOT NULL", query.to_sql
        assert_equal [@integer1_tbl_leftjoin], query.sql_joins
      end

      it 'handles the `not_present` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.integer1',
          'not_present',
          nil
        )

        assert_includes query.to_sql, "`#{@integer1_tbl}`.`value` IS NULL"
        assert query.sql_joins.first =~ /^LEFT JOIN/
      end

      it 'handles organization fields' do
        @integer2_tbl = cf_table_name(@integer2_id, 1)

        query = get_query_for_conditions(
          'organization.custom_fields.integer2',
          'is_not',
          '0000000000000'
        )

        assert_equal(
          "(`#{@integer2_tbl}`.`value` != '0000000000000' OR " \
            "`#{@integer2_tbl}`.`value` IS NULL)",
          query.to_sql
        )

        assert_includes(
          query.sql_joins,
          "LEFT JOIN `cf_values` AS `#{@integer2_tbl}` ON " \
            "`tickets`.`organization_id` = `#{@integer2_tbl}`.`owner_id` AND " \
            "`#{@integer2_tbl}`.`cf_field_id` = #{@integer2_id}"
        )
      end

      it 'handles both fields at once' do
        @integer2_tbl = cf_table_name(@integer2_id, 2)

        query = get_query_for_conditions(
          'requester.custom_fields.integer1',
          'greater_than_equal',
          '0000000000123',
          'organization.custom_fields.integer2',
          'is_not',
          '0000000000000'
        )

        sa = "((`#{@integer2_tbl}`.`value` != '0000000000000' OR " \
          "`#{@integer2_tbl}`.`value` IS NULL) AND " \
          "`#{@integer1_tbl}`.`value` >= '0000000000123')"

        assert_equal(
          sa.gsub(/\(|\)/, '').split(' AND ').sort,
          query.to_sql.gsub(/\(|\)/, '').split(' AND ').sort
        )

        assert_includes(
          query.sql_joins,
          "LEFT JOIN `cf_values` AS `#{@integer2_tbl}` ON " \
            "`tickets`.`organization_id` = `#{@integer2_tbl}`.`owner_id` AND " \
            "`#{@integer2_tbl}`.`cf_field_id` = #{@integer2_id}"
        )
      end
    end

    describe 'date fields' do
      before do
        @date1_id = create_user_custom_field!('date1', 'Whatever', 'Date').id

        @date_tbl = cf_table_name(@date1_id)

        @date_value_left = format('`%s`.`value`', @date_tbl)
      end

      it 'handles blank user date fields' do
        # invalid value for lteq operation (nil)
        assert_raise(RuntimeError) do
          get_query_for_conditions(
            'requester.custom_fields.date1',
            'less_than_equal',
            ''
          )
        end
      end

      it 'handles user date fields' do
        query = get_query_for_conditions(
          'requester.custom_fields.date1',
          'less_than_equal',
          '2012-01-01'
        )

        assert_includes(
          query.to_sql,
          format("%s <= '2012-01-01'", @date_value_left)
        )

        assert query.sql_joins.first =~ /^JOIN/
      end

      it 'handles the `present` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.date1',
          'present',
          nil
        )

        assert_includes query.to_sql, format('%s IS NOT NULL', @date_value_left)

        assert query.sql_joins.first =~ /^JOIN/
      end

      it 'handles the `not_present` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.date1',
          'not_present',
          nil
        )

        assert_includes(
          query.to_sql,
          format('%s IS NULL', @date_value_left)
        )

        assert query.sql_joins.first =~ /^LEFT JOIN/
      end

      it 'handles the `within_previous_n_days` operator' do
        Timecop.freeze

        query = get_query_for_conditions(
          'requester.custom_fields.date1',
          'within_previous_n_days',
          '5'
        )

        assert_includes(
          query.to_sql,
          format(
            "%s >= '#{5.days.ago.to_s(:db)}' AND %s <= " \
              "'#{Time.now.to_s(:db)}'",
            @date_value_left,
            @date_value_left
          )
        )
      end

      it 'handles the `within_next_n_days` operator' do
        Timecop.freeze

        query = get_query_for_conditions(
          'requester.custom_fields.date1',
          'within_next_n_days',
          '5'
        )

        assert_includes(
          query.to_sql,
          format(
            "%s <= '#{5.days.from_now.to_s(:db)}' AND " \
              "%s >= '#{Time.now.to_s(:db)}'",
            @date_value_left,
            @date_value_left
          )
        )
      end
    end

    describe 'checkbox fields' do
      before do
        # Checkbox
        @checkbox_id = create_user_custom_field!(
          'checkbox1',
          'Whatever',
          'Checkbox'
        ).id

        @checkbox_tbl = cf_table_name(@checkbox_id)

        @checkbox_join =
          "JOIN `cf_values` AS `#{@checkbox_tbl}` ON " \
          "`tickets`.`requester_id` = `#{@checkbox_tbl}`.`owner_id` AND " \
          "`#{@checkbox_tbl}`.`cf_field_id` = #{@checkbox_id}"

        @checkbox_leftjoin = 'LEFT ' + @checkbox_join
      end

      it 'handles user checkbox fields where right hand side is 1' do
        query = get_query_for_conditions(
          'requester.custom_fields.checkbox1',
          'is',
          '1'
        )

        assert_includes query.to_sql, "`#{@checkbox_tbl}`.`value` = '1'"
        assert_equal [@checkbox_join], query.sql_joins
      end

      it 'handles user checkbox fields where right hand side is `true`' do
        query = get_query_for_conditions(
          'requester.custom_fields.checkbox1',
          'is',
          'true'
        )

        assert_includes query.to_sql, "`#{@checkbox_tbl}`.`value` = '1'"
        assert_equal [@checkbox_join], query.sql_joins
      end

      it 'handles user checkbox fields where right hand side is 0' do
        query = get_query_for_conditions(
          'requester.custom_fields.checkbox1',
          'is',
          '0'
        )

        assert_includes query.to_sql, "`#{@checkbox_tbl}`.`value` IS NULL"
        assert_equal [@checkbox_leftjoin], query.sql_joins
      end

      it 'handles user checkbox fields where right hand side is false' do
        query = get_query_for_conditions(
          'requester.custom_fields.checkbox1',
          'is',
          'false'
        )

        assert_includes query.to_sql, "`#{@checkbox_tbl}`.`value` IS NULL"
        assert_equal [@checkbox_leftjoin], query.sql_joins
      end

      it 'handles user checkbox fields with right hand side is nil' do
        query = get_query_for_conditions(
          'requester.custom_fields.checkbox1',
          'is',
          nil
        )

        assert_includes query.to_sql, "`#{@checkbox_tbl}`.`value` IS NULL"
        assert_equal [@checkbox_leftjoin], query.sql_joins
      end

      it 'handles user checkbox fields with is_not 1' do
        query = get_query_for_conditions(
          'requester.custom_fields.checkbox1',
          'is_not',
          '1'
        )

        assert_includes(
          query.to_sql,
          "(`#{@checkbox_tbl}`.`value` != '1' OR `#{@checkbox_tbl}`.`value` " \
            'IS NULL)'
        )

        assert_equal [@checkbox_leftjoin], query.sql_joins
      end

      it 'handles user checkbox fields with is_not 0' do
        query = get_query_for_conditions(
          'requester.custom_fields.checkbox1',
          'is_not',
          '0'
        )

        assert_includes query.to_sql, "`#{@checkbox_tbl}`.`value` IS NOT NULL"
        assert_equal [@checkbox_leftjoin], query.sql_joins
      end

      it 'handles user checkbox fields with is_not nil' do
        query = get_query_for_conditions(
          'requester.custom_fields.checkbox1',
          'is_not',
          nil
        )

        assert_includes query.to_sql, "`#{@checkbox_tbl}`.`value` IS NOT NULL"
        assert_equal [@checkbox_leftjoin], query.sql_joins
      end
    end

    describe 'dropdown fields' do
      before do
        @dropdown = CustomField::Dropdown.new(
          account: accounts(:minimum),
          owner:   'User',
          key:     'dropdown',
          title:   'dropdown field'
        )

        @dropdown.dropdown_choices.build(name: 'choice 1', value: 'choice_1')
        @dropdown.dropdown_choices.build(name: 'choice 2', value: 'choice_2')
        @dropdown.save!

        @dropdown_tbl = cf_table_name(@dropdown.id)
      end

      it 'handles the `present` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.dropdown',
          'present',
          nil
        )

        assert_includes(
          query.to_sql,
          "`#{@dropdown_tbl}`.`value` IS NOT NULL",
          query.to_sql
        )

        # assert not left join
        assert_match(/^JOIN/, query.sql_joins.first)
      end

      it 'handles the `not_present` operaetor' do
        query = get_query_for_conditions(
          'requester.custom_fields.dropdown',
          'not_present',
          nil
        )

        assert_includes(
          query.to_sql,
          "`#{@dropdown_tbl}`.`value` IS NULL",
          query.to_sql
        )

        assert query.sql_joins.first =~ /^LEFT JOIN/
      end

      it 'handles the `is` operator' do
        choice_id = @dropdown.dropdown_choices.first.id

        query = get_query_for_conditions(
          'requester.custom_fields.dropdown',
          'is',
          choice_id
        )

        assert_includes(
          query.to_sql,
          "`#{@dropdown_tbl}`.`value` = '#{choice_id}'"
        )

        assert query.sql_joins.first =~ /^JOIN/
      end

      it 'handles the `is not` operator' do
        choice_id = @dropdown.dropdown_choices.first.id

        query = get_query_for_conditions(
          'requester.custom_fields.dropdown',
          'is_not',
          choice_id
        )

        assert_includes(
          query.to_sql,
          "`#{@dropdown_tbl}`.`value` != '#{choice_id}'"
        )

        assert query.sql_joins.first =~ /^LEFT JOIN/
      end

      it "removes the condition if given a value that's not there anymore" do
        query = get_query_for_conditions(
          'requester.custom_fields.dropdown',
          'is',
          12_345_678
        )

        assert_empty(query.sql_joins)
      end
    end

    describe 'text fields' do
      before do
        @text_field = CustomField::Text.new(
          account: accounts(:minimum),
          owner:   'User',
          key:     'text_field',
          title:   'text_field field'
        )

        @text_field.save!

        @text_field_tbl = cf_table_name(@text_field.id)
      end

      it 'handles the `present` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.text_field',
          'present',
          nil
        )

        assert_includes(
          query.to_sql,
          "`#{@text_field_tbl}`.`value` IS NOT NULL",
          query.to_sql
        )

        # assert not left join
        assert_match(/^JOIN/, query.sql_joins.first)
      end

      it 'handles the `not_present` operaetor' do
        query = get_query_for_conditions(
          'requester.custom_fields.text_field',
          'not_present',
          nil
        )

        assert_includes(
          query.to_sql,
          "`#{@text_field_tbl}`.`value` IS NULL", query.to_sql
        )

        assert query.sql_joins.first =~ /^LEFT JOIN/
      end

      it 'handles the `is` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.text_field',
          'is',
          'abcd'
        )

        assert_includes query.to_sql, "`#{@text_field_tbl}`.`value` = 'abcd'"
        assert query.sql_joins.first =~ /^JOIN/
      end

      it 'handles the `is not` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.text_field',
          'is_not',
          'abcd'
        )

        assert_includes query.to_sql, "`#{@text_field_tbl}`.`value` != 'abcd'"
        assert query.sql_joins.first =~ /^LEFT JOIN/
      end
    end

    describe 'regexp fields' do
      before do
        @regexp_field = CustomField::Regexp.new(
          account: accounts(:minimum),
          owner:   'User',
          key:     'regexp_field',
          title:   'regexp_field field'
        )

        @regexp_field.regexp_for_validation = "'[^']*'"

        @regexp_field.save!

        @text_field_tbl = cf_table_name(@regexp_field.id)
      end

      it 'handles the `present` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.regexp_field',
          'present',
          nil
        )

        assert_includes(
          query.to_sql,
          "`#{@text_field_tbl}`.`value` IS NOT NULL", query.to_sql
        )

        # assert not left join
        assert_match(/^JOIN/, query.sql_joins.first)
      end

      it 'handles the `not_present` operaetor' do
        query = get_query_for_conditions(
          'requester.custom_fields.regexp_field',
          'not_present',
          nil
        )

        assert_includes(
          query.to_sql,
          "`#{@text_field_tbl}`.`value` IS NULL", query.to_sql
        )

        assert query.sql_joins.first =~ /^LEFT JOIN/
      end

      it 'handles the `is` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.regexp_field',
          'is',
          'abcd'
        )

        assert_includes query.to_sql, "`#{@text_field_tbl}`.`value` = 'abcd'"
        assert query.sql_joins.first =~ /^JOIN/
      end

      it 'handles the `is not` operator' do
        query = get_query_for_conditions(
          'requester.custom_fields.regexp_field',
          'is_not',
          'abcd'
        )

        assert_includes query.to_sql, "`#{@text_field_tbl}`.`value` != 'abcd'"
        assert query.sql_joins.first =~ /^LEFT JOIN/
      end
    end
  end
end
