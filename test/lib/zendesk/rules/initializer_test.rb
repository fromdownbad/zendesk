require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Initializer do
  fixtures :accounts,
    :rules,
    :users

  let(:account) { accounts(:minimum) }

  let(:scope) { mock('scope') }
  let(:key)   { :view }

  let(:initializer) do
    Zendesk::Rules::Initializer.new(
      account,
      scope,
      params.with_indifferent_access,
      key
    )
  end

  describe '#normalize_params' do
    describe 'when there are no parameters' do
      let(:params) { {} }

      it 'does not error' do
        assert initializer
      end
    end

    describe 'with `active`' do
      describe 'true' do
        let(:params) { {view: {active: true}} }

        it 'sets `is_active`' do
          assert(initializer.params[:view][:is_active])
        end

        it 'removes `active`' do
          assert_nil initializer.params[:view][:active]
        end
      end

      describe 'false' do
        let(:params) { {view: {active: false}} }

        it 'sets `is_active`' do
          assert_equal false, initializer.params[:view][:is_active]
        end

        it 'removes `active`' do
          assert_nil initializer.params[:view][:active]
        end
      end
    end

    describe 'output' do
      describe 'with parameters' do
        let(:params) { {view: {output: {columns: %i[status type]}}} }

        it 'creates an output' do
          assert_equal(
            %i[status type],
            initializer.params[:view][:output].columns
          )
        end
      end

      describe 'without an ID' do
        let(:params) { {view: {title: '123'}} }

        it 'uses the default output' do
          assert_equal(
            Output.default.to_yaml,
            initializer.params[:view][:output].to_yaml
          )
        end
      end

      describe 'with an ID' do
        let(:params) { {id: '123', view: {title: '123'}} }

        it 'does not add an output' do
          assert_nil initializer.params[:view][:output]
        end
      end
    end

    describe 'category_id' do
      let(:params) { { view: { category_id: 123 } } }

      it 'removes the category_id key' do
        refute initializer.params[:view][:category_id]
      end

      describe_with_arturo_setting_enabled :trigger_categories_api do
        it 'sets rules_category_id' do
          assert_equal 123, initializer.params[:view][:rules_category_id]
        end
      end

      describe_with_arturo_setting_disabled :trigger_categories_api do
        it 'does not set rules_category_id' do
          refute initializer.params[:view][:rules_category_id]
        end
      end
    end

    %w[any all actions].each do |defn|
      describe "definition - #{defn}" do
        let(:params) do
          {view: {defn => [{field: 'status', operator: 'is', value: 'closed'}]}}
        end

        let(:item) { initializer.params[:view][:definition].items.first }

        it 'removes the parameter key' do
          assert_nil initializer.params[:view][defn]
        end

        it 'adds to the definition' do
          assert_equal 'status_id', item.source
          assert_equal 'is', item.operator
          assert_equal [Zendesk::Types::StatusType.CLOSED.to_s], item.value
          assert_equal Zendesk::Types::StatusType.CLOSED.to_s, item.first_value
        end
      end
    end

    describe 'definition items' do
      let(:params) do
        {
          view: {
            all: [{field: 'new', operator: 'is', value: 1.hour.ago.as_json}]
          }
        }
      end

      let(:item) { initializer.params[:view][:definition].items.first }

      it 'is mapped' do
        assert_equal 'NEW', item.source
        assert_equal 1, item.first_value
      end
    end

    describe 'nested definition items' do
      let(:params) do
        {
          view: {
            conditions: {
              all: [{field: 'new', operator: 'is', value: 1.hour.ago.as_json}]
            }
          }
        }
      end

      let(:item) { initializer.params[:view][:definition].items.first }

      it 'is mapped' do
        assert_equal 'NEW', item.source
        assert_equal 1, item.first_value
      end
    end

    describe 'without conditions' do
      let(:params) { {view: {active: true}} }

      it 'does not set the definition' do
        assert_nil initializer.params[:view][:definition]
      end
    end

    describe '`restriction`' do
      describe 'with null' do
        let(:params) { {view: {restriction: nil}} }

        it 'removes `restriction`' do
          assert_nil initializer.params[:view][:restriction]
        end

        it 'sets the owner' do
          assert_equal account, initializer.params[:view][:owner]
        end
      end

      describe 'with parameters' do
        let(:params) { {view: {restriction: {id: '123', type: 'User'}}} }

        it 'removes `restriction`' do
          assert_nil initializer.params[:view][:restriction]
        end

        it 'sets `owner_type`' do
          assert_equal 'User', initializer.params[:view][:owner_type]
        end

        it 'sets `owner_id`' do
          assert_equal '123', initializer.params[:view][:owner_id]
        end

        describe 'with a lowercase type' do
          let(:params) { {view: {restriction: {id: '123', type: 'user'}}} }

          it 'sets normalized type' do
            assert_equal 'User', initializer.params[:view][:owner_type]
          end
        end

        describe 'with a forbidden type' do
          before { params[:view][:restriction][:type] = 'Subscription' }

          it 'raises an error' do
            assert_raise(ArgumentError) { initializer }
          end
        end
      end

      describe 'with an ID' do
        let(:params) { {id: '123', view: {title: '123'}} }

        it 'does not add an owner' do
          assert_nil initializer.params[:view][:owner]
        end
      end

      describe 'without an ID' do
        let(:params) { {view: {title: '123'}} }

        it 'defaults to the account' do
          assert_equal account, initializer.params[:view][:owner]
        end
      end

      describe 'when the rule is a macro' do
        let(:key) { :macro }

        describe 'and the restriction includes `ids`' do
          let(:params) { {macro: {restriction: {ids: [1, 2], type: 'Group'}}} }

          it 'sets the owner ID' do
            assert_equal 1, initializer.params[:macro][:owner_id]
          end
        end

        describe 'and the restriction includes `id`' do
          let(:params) { {macro: {restriction: {id: 123, type: 'Group'}}} }

          it 'sets the owner ID' do
            assert_equal 123, initializer.params[:macro][:owner_id]
          end
        end
      end

      describe 'when the rule is a view' do
        let(:params) do
          {view: {restriction: {id: 1, ids: [2, 3], type: 'Group'}}}
        end

        it 'sets the owner ID' do
          assert_equal 2, initializer.params[:view][:owner_id]
        end

        describe_with_arturo_disabled :multiple_group_views_writes do
          it 'sets the owner ID' do
            assert_equal 1, initializer.params[:view][:owner_id]
          end
        end
      end
    end
  end

  describe 'when initializing a rule' do
    let(:user) { users(:minimum_admin) }

    let(:initialized_rule) { initializer.rule }

    let(:scope) { user.aggregate_views(types: %i[shared personal]) }

    describe 'with an ID' do
      describe 'that is invalid' do
        let(:params) { {id: '1231234123'} }

        it 'raises `ActiveRecord::RecordNotFound`' do
          assert_raises(ActiveRecord::RecordNotFound) { initialized_rule }
        end
      end

      describe 'that is valid' do
        let(:rule) { rules(:view_my_working_tickets) }

        let(:params) { {id: rule.id} }

        it 'returns the rule' do
          assert_equal rule, initialized_rule
        end

        describe 'with attributes' do
          let(:group) { groups(:minimum_group) }

          let(:params) do
            {id: rule.id, view: {title: 'New Title', group_id: group.id}}
          end

          it 'updates the attributes' do
            assert initialized_rule.title_changed?
            assert_equal 'New Title', initialized_rule.title
            assert_equal group.id, initialized_rule.group_id
          end
        end
      end
    end

    describe 'without an ID' do
      let(:params) { {} }

      it 'builds a new record' do
        assert initialized_rule.new_record?
      end

      describe 'with attributes' do
        let(:group) { groups(:minimum_group) }
        let(:params) { {view: {title: 'Hello', group_id: group.id}} }

        it 'sets the attributes' do
          assert_equal 'Hello', initialized_rule.title
          assert_equal group.id, initialized_rule.group_id
        end
      end
    end

    describe 'with a restriction' do
      describe 'when the rule is a view' do
        describe 'and the type is Group' do
          let(:params) do
            {view: {restriction: {id: 1, ids: [2, 3], type: 'Group'}}}
          end

          it 'sets `owner_id`' do
            assert_equal 2, initialized_rule.owner_id
          end

          it 'sets `group_owner_ids`' do
            assert_equal [2, 3], initialized_rule.group_owner_ids
          end

          describe_with_arturo_disabled :multiple_group_views_writes do
            it 'sets `owner_id`' do
              assert_equal 1, initialized_rule.owner_id
            end

            it 'sets `group_owner_ids` to nil' do
              assert_nil initialized_rule.group_owner_ids
            end
          end
        end
      end

      describe 'when the rule is a macro' do
        let(:key)   { :macro }
        let(:scope) { user.shared_and_personal_macros }

        describe 'and the type is Group' do
          let(:params) do
            {macro: {restriction: {id: 1, ids: [2, 3], type: 'Group'}}}
          end

          it 'sets `owner_id`' do
            assert_equal 2, initialized_rule.owner_id
          end

          it 'sets `group_owner_ids`' do
            assert_equal [2, 3], initialized_rule.group_owner_ids
          end
        end
      end
    end
  end

  describe '#map_definition_items' do
    let(:params) { {} }

    it 'calls `Api::V2::Rules::ConditionMappings.map_definition_item!`' do
      items = [stub, stub]

      items.each do |param|
        Api::V2::Rules::ConditionMappings.expects(
          :map_definition_item!
        ).once.with(account, param)
      end

      initializer.send(:map_definition_items, items)
    end
  end

  describe '#rule' do
    let(:params) { {} }

    describe 'with deprecated rule components' do
      let(:params) do
        {
          view: {
            title:      'new title',
            conditions: {
              all: [{field: 'group_id', operator: 'is', value: 'group_id'}]
            }
          }
        }
      end

      let(:user) { users(:minimum_admin) }

      let(:scope) { user.aggregate_views(types: %i[shared personal]) }

      it 'removes the deprecated components' do
        assert_equal [], initializer.rule.definition.conditions_all
      end
    end
  end
end
