require_relative '../../../support/test_helper'
require_relative '../../../support/rules_test_helper'
require_relative '../../../support/sharing_test_helper'

SingleCov.not_covered!

describe 'ComponentsConditions' do
  include SharingTestHelper

  fixtures :accounts,
    :ticket_fields,
    :custom_field_options,
    :translation_locales,
    :users

  before do
    @account = accounts(:minimum)
    @user = users(:minimum_agent)
    @context = Zendesk::Rules::Context.new(
      @account,
      @user,
      rule_type: :trigger,
      component_type: :condition,
      condition_type: :all
    )
    @validation_err = {}
  end

  describe 'status_id rule' do
    it 'does not include hold status if the account does not support it' do
      refute @account.use_status_hold?
      definition = get_def('status_id')

      refute(
        definition[:values][:list].
          detect { |h| h[:value] == StatusType.HOLD }
      )
    end

    it 'includes NEW in trigger context' do
      @context.options[:rule_type] = :trigger
      definition = get_def('status_id')

      assert(
        definition[:values][:list].
          detect { |h| h[:value] == StatusType.NEW }
      )
    end

    it 'does not be available for reports' do
      @context.options[:rule_type] = :report

      refute get_def('status_id')
    end

    it 'does not be available for sla policies' do
      @context.options[:rule_type] = :sla_policy

      refute get_def('status_id')
    end

    it 'validates as in the list' do
      assert valid_definition?('status_id', :is, StatusType.CLOSED)
      refute valid_definition?('status_id', :is, 43_908)
      assert_equal :invalid_value, @validation_err[:error]
    end

    it 'validates not less-than-NEW, not greater-than SOLVED' do
      refute valid_definition?('status_id', :less_than, StatusType.NEW)
      assert_equal :invalid_value, @validation_err[:error]
      refute valid_definition?('status_id', :greater_than, StatusType.CLOSED)
      assert_equal :invalid_value, @validation_err[:error]
    end

    it "accepts 'changed', []" do
      assert(
        valid_definition?('status_id', :changed, []),
        "error is #{@validation_err.inspect}"
      )
    end
  end

  describe 'priority_id rule' do
    it 'includes all the priorities as values' do
      definition = get_def('priority_id')

      assert_equal(
        PriorityType.list.map { |i| i[0] },
        definition[:values][:list].map { |h| h[:title] }
      )
    end

    it 'validates not less-than-nothing, not greater than high' do
      refute valid_definition?('priority_id', :less_than, '')
      assert_equal :invalid_value, @validation_err[:error]
      refute(
        valid_definition?('priority_id', :greater_than, PriorityType.URGENT)
      )
      assert_equal :invalid_value, @validation_err[:error]
    end

    it 'does not be available for sla policies' do
      @context.options[:rule_type] = :sla_policy

      refute get_def('priority_id')
    end
  end

  describe 'group_id rule' do
    it 'includes current_groups for views' do
      @context.options[:rule_type] = :view
      definition = get_def('group_id')

      assert(
        definition[:values][:list].detect { |h| h[:value] == 'current_groups' }
      )
    end

    it "includes the account's active groups" do
      definition = get_def('group_id')

      @account.groups.each do |group|
        assert(
          definition[:values][:list].
            include?(title: group.name, value: group.id),
          "rule did not include #{group}"
        )
      end
    end

    it 'is present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy

      assert get_def('group_id')
    end
  end

  describe 'organization_id rule' do
    describe 'with a few orgs' do
      before do
        5.times do |i|
          @account.organizations.create!(name: "org#{i}")
        end
      end

      it "includes an account's organization as a list" do
        definition = get_def('organization_id')

        assert_equal(
          @account.organizations.map(&:id),
          definition[:values][:list].map { |h| h[:value] }.compact
        )
      end

      it 'validates that the org is in the list' do
        refute valid_definition?('organization_id', :is, 123_412_341_234)
        assert_equal :invalid_value, @validation_err[:error]
        assert(
          valid_definition?(
            'organization_id',
            :is,
            @account.organizations.first.id
          )
        )
      end

      it 'is present for SLA Policies' do
        @context.options[:rule_type] = :sla_policy

        assert get_def('organization_id')
      end
    end

    describe 'when over the autocomplete limit' do
      before do
        101.times do |i|
          @account.organizations.create!(name: "org#{i}")
        end
      end

      it 'validates that the org exists in the account' do
        refute valid_definition?('organization_id', :is, 123_412_341_234)
        assert_equal :invalid_value, @validation_err[:error]
        assert(
          valid_definition?(
            'organization_id',
            :is,
            @account.organizations.first.id
          )
        )
      end

      it 'supports a blank organization field' do
        assert valid_definition?('organization_id', :is, '')
        assert valid_definition?('organization_id', :is_not, '')
      end
    end
  end

  describe 'requester_id rule' do
    it 'accepts any valid user in the account' do
      assert(
        valid_definition?('requester_id', :is, @account.end_users.first.id)
      )

      assert(
        valid_definition?(
          'requester_id',
          :is,
          @account.admins.first.id.to_s
        )
      )

      assert valid_definition?('requester_id', :is, @account.admins.first.id)
      refute valid_definition?('requester_id', :is, 123_456_789)
      refute valid_definition?('requester_id', :is, 'alskdjflaksjd')
      refute valid_definition?('requester_id', :is, -1)
    end

    it 'accepts current_user in trigger and view context' do
      @context.options[:rule_type] = :trigger
      assert valid_definition?('requester_id', :is, 'current_user')
      @context.options[:rule_type] = :view
      assert valid_definition?('requester_id', :is, 'current_user')
      @context.options[:rule_type] = :automation
      refute valid_definition?('requester_id', :is, 'current_user')
    end

    it 'accepts assignee_id in all but report context' do
      %i[trigger view automation].each do |rule_type|
        @context.options[:rule_type] = rule_type

        assert valid_definition?('requester_id', :is, 'assignee_id')
      end
    end

    describe 'null values' do
      it 'does not be included in the list' do
        values = get_def('requester_id')[:values][:list]
        assert !values.empty?

        refute values.any? { |v| v[:value].nil? }
      end

      it 'is quietly allowed to validate (for now)' do
        assert valid_definition?('requester_id', :is, '')
      end
    end

    it 'is present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      assert get_def('requester_id')
    end
  end

  describe 'tags rule' do
    it 'validates the input with the tagging regexp' do
      refute valid_definition?('current_tags', :is, '!!!! $$D+')
      assert_equal :invalid_tag_value, @validation_err[:error]
      assert valid_definition?('current_tags', :is, '1234, aslkj, becalkj_jj')
    end

    it 'is present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy

      assert get_def('current_tags')
    end
  end

  describe 'description_includes_word condition' do
    before do
      @context.options[:rule_type] = :view
    end

    it "validates that there's no more than 3 words in an `includes` operator" do
      refute(
        valid_definition?('description_includes_word', :includes, '5 4 3 2 1')
      )

      assert_equal :too_many_words, @validation_err[:error]

      assert(
        valid_definition?('description_includes_word', :includes, '5 4 3')
      )
    end
  end

  describe 'received at rule' do
    it "includes the account's recipient addresses in the list" do
      emails = get_def('recipient')[:values][:list].map { |p| p[:value] }

      @account.recipient_addresses.pluck(:email).each do |email|
        assert emails.include?(email)
      end
    end

    it 'includes any email addresses in the context :recipients parameter' do
      existing_recipient_value = 'rules_current_recipient_email@example.com'
      @context.options[:recipients] = [existing_recipient_value]
      addresses = get_def('recipient')[:values][:list]
      assert(
        addresses.
          map { |address| address[:value] }.
          include?(existing_recipient_value)
      )
    end

    it 'does not include duplicate email addresses' do
      existing_recipient_value = @account.default_recipient_address.email
      @context.options[:recipients] = [existing_recipient_value]
      addresses = get_def('recipient')[:values][:list]
      assert_equal(
        1,
        addresses.
          map { |address| address[:value] }.
          select { |address| address == existing_recipient_value }.
          size
      )
    end

    it 'is present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      assert get_def('recipient')
    end
  end

  describe 'satisfaction rule' do
    describe_with_arturo_enabled :customer_satisfaction do
      describe_with_arturo_setting_enabled :customer_satisfaction do
        it 'is present' do
          assert get_def('satisfaction_score')
        end
      end
    end

    describe_with_arturo_disabled :customer_satisfaction do
      it 'is not present' do
        refute get_def('satisfaction_score')
      end
    end
  end

  describe 'ticket form rule' do
    describe 'ticket forms enabled' do
      before do
        @account.stubs(:has_ticket_forms?).returns(true)
        Zendesk::TicketForms::Initializer.new(
          @account,
          ticket_form: {
            name: 'Default',
            display_name: 'default name',
            default: true,
            end_user_visible: true,
            position: 1
          }
        ).
          ticket_form.
          save!
      end

      it 'is present' do
        assert get_def('ticket_form_id')
      end

      it 'is present for SLA Policies' do
        @context.options[:rule_type] = :sla_policy
        assert get_def('ticket_form_id')
      end
    end

    it 'is not present when disabled' do
      @account.stubs(:has_ticket_forms?).returns(false)
      refute get_def('ticket_form_id')
    end
  end

  describe 'multi brand rule' do
    describe 'multibrand enabled' do
      before do
        @account.stubs(:has_multibrand?).returns(true)
        FactoryBot.create(:brand, account_id: @account.id)
      end

      it 'is present' do
        assert get_def('brand_id')
      end

      it 'is present for SLA Policies' do
        @context.options[:rule_type] = :sla_policy
        assert get_def('brand_id')
      end
    end

    it 'is not present when multi brand is disabled' do
      @account.stubs(:has_multibrand?).returns(false)
      refute get_def('brand_id')
    end
  end

  describe 'ticket_type rule' do
    it 'is present' do
      assert get_def('ticket_type_id')
    end

    it 'is present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      assert get_def('ticket_type_id')
    end
  end

  describe 'assignee_id rule' do
    it 'is present' do
      assert get_def('assignee_id')
    end

    it 'is present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      assert get_def('assignee_id')
    end
  end

  describe 'custom dates rule' do
    before do
      @date_field = FieldDate.create!(
        account: @account,
        title: 'Custom wombat date',
        is_active: true
      )
    end

    it 'custom date field is always enabled' do
      assert get_def("ticket_fields_#{@date_field.id}")
    end

    it 'is not present when component_type is an action' do
      @context.options[:component_type] = :action

      refute get_def("ticket_fields_#{@date_field.id}")
    end
  end
  basic_operators      = [:is, :is_not, :present, :not_present]
  slas_basic_operators = [:is, :is_not]
  inequality_operators = [:less_than, :less_than_equal, :greater_than,
                          :greater_than_equal]
  includes_operators   = [:includes_words, :not_includes_words,
                          :includes_string, :not_includes_string]
  within_operators     = [:within_next_n_days, :within_previous_n_days]

  # type -> context -> operator list map
  self::OPERATOR_MAP = {
    'Text' => {
      trigger:    basic_operators + includes_operators,
      automation: basic_operators,
      sla_policy: slas_basic_operators
    },
    'Decimal' => {
      trigger:    basic_operators + inequality_operators,
      automation: basic_operators + inequality_operators,
      sla_policy: slas_basic_operators + inequality_operators
    },
    'Integer' => {
      trigger:    basic_operators + inequality_operators,
      automation: basic_operators + inequality_operators,
      sla_policy: slas_basic_operators + inequality_operators
    },
    'Date' => {
      trigger:    basic_operators + inequality_operators + within_operators,
      automation: basic_operators + inequality_operators + within_operators,
      sla_policy: slas_basic_operators + inequality_operators
    },
    'Dropdown' => {
      trigger:    basic_operators,
      automation: basic_operators,
      sla_policy: slas_basic_operators
    },
    'Checkbox' => {
      trigger:    [:is],
      automation: [:is],
      sla_policy: [:is]
    }
  }.freeze

  %w[ticket user organization].each do |type|
    describe "#{type} custom field rules" do
      describe 'taggers' do
        before do
          @english = translation_locales(:english_by_zendesk)
          @fbattrs = {
            is_fallback: true,
            nested: true,
            value: 'The fallback value',
            translation_locale_id: @english.id
          }
          @cms_params = {
            account: @account,
            name: 'Welcome Wombat',
            fallback_attributes: @fbattrs
          }
          @cms_text = Cms::Text.create!(@cms_params)
          set_key(type)
        end

        it 'is included for triggers in an any/all context' do
          @context.options[:rule_type] = :trigger
          @context.options[:condition_type] = :all
          assert get_def(@key)

          @context.options[:rule_type] = :trigger
          @context.options[:condition_type] = :any
          assert get_def(@key)
        end

        it 'is included for automations etc in the all/any(custom field is enabled) context' do
          @context.options[:rule_type] = :automation
          @context.options[:condition_type] = :all
          assert get_def(@key)

          @context.options[:condition_type] = :any
          assert get_def(@key)
        end

        it 'is included for sla policies in the any/all context' do
          @context.options[:rule_type] = :sla_policy

          @context.options[:condition_type] = :all
          assert get_def(@key)

          @context.options[:condition_type] = :any
          assert get_def(@key)
        end

        it 'has the right inclusion for views, macros and reports' do
          [:view, :macro, :report].each do |rule_type|
            @context.options[:rule_type] = rule_type
            @context.options[:condition_type] = :all
            if type == 'ticket'
              assert get_def(@key)
            else
              refute get_def(@key)
            end
          end
        end

        if type == 'ticket'
          it 'does not include the nil value for views' do
            @context.options[:rule_type] = :view
            @context.options[:condition_type] = :all
            defs = get_def(@key)[:values][:list]
            assert defs.first[:title] != '-'
          end

          it 'does not include the nil value for automations' do
            @context.options[:rule_type] = :automation
            @context.options[:condition_type] = :all
            defs = get_def(@key)[:values][:list]
            assert defs.first[:title] != '-'
          end
        end

        it 'adds drop-downs as name/value pairs' do
          definition = get_def(@key)
          assert_equal '-', definition[:values][:list].first[:title]
          expected = @tagger.
            custom_field_options.
            map do |cf|
              Zendesk::Liquid::DcContext.
                render(cf.name, @account, @account.owner)
            end

          assert_equal(
            expected,
            definition[:values][:list][1..-1].map { |d| d[:title] }
          )
        end

        it 'evaluates dynamic context' do
          definition = get_def(@key)
          assert_equal(
            'The fallback value',
            definition[:values][:list].last[:title]
          )
        end
      end

      describe 'custom dates' do
        before do
          Arturo.enable_feature!(:date_custom_field)
          @date_field = FieldDate.create!(
            account: @account,
            title: 'wombat',
            is_active: true
          )
          @key = "ticket_fields_#{@date_field.id}"
        end

        if type == 'ticket'
          it 'is included for triggers in an any/all context' do
            @context.options[:rule_type] = :trigger
            @context.options[:condition_type] = :all
            assert get_def(@key)

            @context.options[:rule_type] = :trigger
            @context.options[:condition_type] = :any
            assert get_def(@key)
          end

          it 'does not include `is_not` and `not_present` operators for views' do
            @context.options[:rule_type] = :view
            @context.options[:condition_type] = :all
            operators = get_def(@key)[:operators].map { |op| op[:value] }
            assert_equal(
              %i[
                is
                less_than
                less_than_equal
                greater_than
                greater_than_equal
                within_previous_n_days
                within_next_n_days
              ],
              operators
            )
          end

          it 'does not include `is_not` and `not_present` operators for automations' do
            @context.options[:rule_type] = :automation
            @context.options[:condition_type] = :all
            operators = get_def(@key)[:operators].map { |op| op[:value] }
            assert_equal(
              %i[
                is
                less_than
                less_than_equal
                greater_than
                greater_than_equal
                within_previous_n_days
                within_next_n_days
              ],
              operators
            )
          end
        end

        it 'is included in `all` context for SLA Policies' do
          @context.options[:rule_type] = :sla_policy
          assert get_def(@key)
        end

        it 'validates as YYYY-MM-DD' do
          assert valid_definition?(@key, :is, '2012-03-03')
          refute valid_definition?(@key, :is, '0')
          assert_equal :invalid_date, @validation_err[:error]

          refute valid_definition?(@key, :is, '2012-15-15')
          assert_equal :invalid_date, @validation_err[:error]
        end

        it 'validates specific date as date when used in automation' do
          assert valid_definition?(@key, :is, %w[specific_date 2012-03-03])
          refute valid_definition?(@key, :is, %w[specific_date 7])
        end

        it 'validates days from now as integer when used in automation' do
          assert valid_definition?(@key, :is, %w[days_from_now 7])
          refute valid_definition?(@key, :is, %w[days_from_now 2012-03-03])
        end
      end
    end

    next unless %w[user organization].include?(type)

    describe "with custom #{type} fields" do
      before do
        @base_key = type == 'user' ? 'requester' : 'organization'

        # create a field for each type
        (self.class::OPERATOR_MAP.keys - ['Dropdown']).each do |field_type|
          klass = "CustomField::#{field_type}".constantize
          klass.create!(
            account: @account,
            key: "cf_#{field_type.downcase}",
            title: "CF #{field_type}",
            owner: type.capitalize
          )
          klass.create!(
            account: @account,
            key: "cf_#{field_type.downcase}_inactive",
            title: "CF #{field_type} (inactive)",
            owner: type.capitalize,
            is_active: false
          )
        end

        dropdown = CustomField::Dropdown.new(
          account: @account,
          key: 'cf_dropdown',
          title: 'CF dropdown',
          owner: type.capitalize
        )

        dropdown.dropdown_choices.build(name: 'Choice 1', value: 'choice_1')
        dropdown.save!

        dropdown = CustomField::Dropdown.new(
          account: @account,
          key: 'cf_dropdown_inactive',
          title: 'CF dropdown (inactive)',
          owner: type.capitalize,
          is_active: false
        )

        dropdown.dropdown_choices.build(name: 'Choice 2', value: 'choice_2')
        dropdown.save!
      end

      describe 'and without user_and_organization_fields feature' do
        before do
          @account.stubs(:has_user_and_organization_fields?).returns(false)
        end

        it 'does not return a definition' do
          assert_nil get_def("#{@base_key}.custom_fields.cf_integer")
        end

        it 'is not a valid definition' do
          refute(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_integer",
              :is,
              '13243.0'
            )
          )
        end
      end

      self::OPERATOR_MAP.keys.each do |field_type|
        describe "#{field_type} fields" do
          before do
            @field_name = "cf_#{field_type.downcase}"
          end
          [:trigger, :automation, :sla_policy].each do |context|
            describe "in #{context} context" do
              before { @context.options[:rule_type] = context }

              it 'is available if active' do
                assert get_def("#{@base_key}.custom_fields.#{@field_name}")
              end

              it 'does not be available if inactive' do
                assert_nil(
                  get_def("#{@base_key}.custom_fields.#{@field_name}_inactive")
                )
              end

              it 'includes these operators' do
                assert_operators(
                  "#{@base_key}.custom_fields.#{@field_name}",
                  self.class::OPERATOR_MAP[field_type][context]
                )
              end
            end
          end
        end
      end

      describe 'dropdown fields' do
        before do
          @definition = get_def("#{@base_key}.custom_fields.cf_dropdown")
        end

        it 'validates presence operators' do
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_decimal",
              'present'
            )
          )
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_decimal",
              'not_present'
            )
          )
        end

        describe 'when operator set to present' do
          it 'returns an empty list of values' do
            operator = @definition[:operators].
              detect { |op| op[:value] == :present }

            assert_equal operator[:values], []
          end
        end

        describe 'when operator set to not present' do
          it 'returns an empty list of values' do
            operator = @definition[:operators].
              detect { |op| op[:value] == :not_present }

            assert_equal operator[:values], []
          end
        end
      end

      describe 'decimal fields' do
        it 'validates presence operators' do
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_decimal",
              'present'
            )
          )
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_decimal",
              'not_present'
            )
          )
        end

        it 'validates their args numerically' do
          @context.options[:condition_type] = :all
          @context.options[:rule_type] = :trigger
          refute(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_decimal",
              :greater_than,
              'ablkjasd'
            )
          )
          assert_equal :invalid_number, @validation_err[:error]

          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_decimal",
              :greater_than,
              '10.001'
            )
          )
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_decimal",
              :greater_than,
              '10'
            )
          )
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_decimal",
              :greater_than,
              '.232'
            )
          )
        end
      end

      describe 'integer fields' do
        it 'validates presence operators' do
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_integer",
              'present'
            )
          )
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_integer",
              'not_present'
            )
          )
        end

        it 'validates as integers' do
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_integer",
              :is,
              '13243'
            )
          )
          refute(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_integer",
              :is,
              '13243.0'
            )
          )
          assert_equal :invalid_number, @validation_err[:error]
          refute(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_integer",
              :is,
              'abc 13243.0'
            )
          )
          assert_equal :invalid_number, @validation_err[:error]
        end
      end

      describe 'date fields' do
        it 'validates presence operators' do
          assert(
            valid_definition?("#{@base_key}.custom_fields.cf_date", 'present')
          )
          assert(
            valid_definition?(
              "#{@base_key}.custom_fields.cf_date",
              'not_present'
            )
          )
        end

        it 'labels generic numeric operators as date operators' do
          definition = get_def("#{@base_key}.custom_fields.cf_date")
          operators = definition[:operators]

          assert_equal :less_than, operators[4][:value]
          assert_equal :less_than_equal, operators[5][:value]
          assert_equal :greater_than, operators[6][:value]
          assert_equal :greater_than_equal, operators[7][:value]

          assert_equal(
            I18n.t('txt.admin.models.rules.rule_dictionary.before_date_label'),
            operators[4][:title]
          )
          assert_equal(
            I18n.t(
              'txt.admin.models.rules.rule_dictionary.before_or_on_date_label'
            ),
            operators[5][:title]
          )
          assert_equal(
            I18n.t('txt.admin.models.rules.rule_dictionary.after_date_label'),
            operators[6][:title]
          )
          assert_equal(
            I18n.t(
              'txt.admin.models.rules.rule_dictionary.after_or_on_date_label'
            ),
            operators[7][:title]
          )
        end
      end
    end
  end

  describe 'via types' do
    before do
      @listed_types = [
        ViaType.WEB_FORM,
        ViaType.DROPBOX,
        ViaType.MAIL,
        ViaType.CHAT,
        ViaType.TWITTER,
        ViaType.TWITTER_DM,
        ViaType.TWITTER_FAVORITE,
        ViaType.VOICEMAIL,
        ViaType.PHONE_CALL_INBOUND,
        ViaType.PHONE_CALL_OUTBOUND,
        ViaType.SMS,
        ViaType.GET_SATISFACTION,
        ViaType.WEB_WIDGET,
        ViaType.WEB_SERVICE,
        ViaType.RULE,
        ViaType.TOPIC,
        ViaType.CLOSED_TICKET,
        ViaType.MOBILE_SDK,
        ViaType.MOBILE,
        ViaType.TICKET_SHARING,
        ViaType.FACEBOOK_POST,
        ViaType.FACEBOOK_MESSAGE,
        ViaType.HELPCENTER,
        ViaType.API_PHONE_CALL_INBOUND,
        ViaType.API_PHONE_CALL_OUTBOUND,
        ViaType.API_VOICEMAIL,
        ViaType.SATISFACTION_PREDICTION,
        ViaType.ANSWER_BOT_FOR_WEB_WIDGET
      ]
    end

    it 'is a valid definition for the defined list' do
      @listed_types.each do |via_id|
        assert(
          valid_definition?('via_id', :is, [via_id]),
          "via_id #{via_id} should be valid"
        )
      end

      (ViaType.fields - @listed_types).each do |via_id|
        refute(
          valid_definition?('via_id', :is, [via_id]),
          "via_id #{via_id} should not be valid"
        )
      end

      refute valid_definition?('via_id', :is, ['web'])
    end

    it 'is present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      assert get_def('via_id')
    end
  end

  describe 'update via' do
    it 'is present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      assert get_def('current_via_id')
    end
  end

  describe 'in business hours' do
    before do
      @account.stubs(:business_hours_active?).returns(true)
    end

    it 'is present for triggers' do
      assert get_def('in_business_hours')
    end

    it 'is not present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      refute get_def('in_business_hours')
    end
  end

  describe 'update type' do
    it 'is present for triggers' do
      assert get_def('update_type')
    end

    it 'is not present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      refute get_def('update_type')
    end
  end

  describe 'subject includes word' do
    before do
      @account.field_subject.update_attribute(:is_active, true)
      assert @account.field_subject.is_active?
    end

    it 'should be present for triggers' do
      assert get_def('subject_includes_word')
    end

    %i[view automation report macro sla_policy].each do |rule_type|
      describe "rule type is #{rule_type}" do
        it 'should not be available' do
          @context.options[:rule_type] = rule_type
          refute get_def('subject_includes_word')
        end
      end
    end

    describe 'when Ticket `subject` field is inactive' do
      before do
        @account.field_subject.update_attribute(:is_active, false)
      end

      it 'should not be present for triggers' do
        refute get_def('subject_includes_word')
      end
    end
  end

  describe 'collaboration_thread' do
    before do
      Arturo.enable_feature!(:ticket_threads_eap)
      @account.settings.ticket_threads = true
      @account.settings.save!
    end

    it 'should be present for triggers' do
      assert get_def('collaboration_thread')
    end

    %i[view automation report macro sla_policy].each do |rule_type|
      describe "rule type is #{rule_type}" do
        it 'is not available' do
          @context.options[:rule_type] = rule_type
          refute get_def('collaboration_thread')
        end
      end
    end

    describe "when Collaboration Threads aren't enabled" do
      before do
        @account.settings.ticket_threads = false
        @account.settings.save!
      end

      it 'should not be present for triggers' do
        refute get_def('collaboration_thread')
      end
    end
  end

  describe 'ticket is public' do
    it 'is present for triggers' do
      assert get_def('ticket_is_public')
    end

    it 'is present for Views' do
      @context.options[:rule_type] = :view
      assert get_def('ticket_is_public')
    end

    it 'is present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      assert get_def('ticket_is_public')
    end

    it 'is present for Automations' do
      @context.options[:rule_type] = :automation
      assert get_def('ticket_is_public')
    end

    it 'is NOT present for Reports' do
      @context.options[:rule_type] = :report
      refute get_def('ticket_is_public')
    end
  end

  describe 'comment is public' do
    it 'is present for triggers' do
      assert get_def('comment_is_public')
    end

    it 'is not present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      refute get_def('comment_is_public')
    end
  end

  describe 'comment includes word' do
    it 'is present for triggers' do
      assert get_def('comment_includes_word')
    end

    it 'is not present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      refute get_def('comment_includes_word')
    end
  end

  describe 'requester language' do
    describe 'account has multiple languages' do
      before do
        english = translation_locales(:english_by_zendesk)
        japanese = translation_locales(:japanese)
        @account.stubs(:available_languages).returns([english, japanese])

        @context = Zendesk::Rules::Context.new(
          @account,
          users(:minimum_end_user),
          rule_type: :trigger,
          component_type: :condition,
          condition_type: :all
        )
      end

      it 'is present for triggers' do
        assert get_def('locale_id')
      end

      it 'is not present for SLA Policies' do
        @context.options[:rule_type] = :sla_policy
        refute get_def('locale_id')
      end
    end

    describe 'account does not have multiple languages' do
      it 'is not present for triggers' do
        refute get_def('locale_id')
      end
    end
  end

  describe 'requester twitter followers' do
    it 'is present for triggers' do
      assert get_def('requester_twitter_followers_count')
    end

    it 'is not present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      refute get_def('requester_twitter_followers_count')
    end
  end

  describe 'requester twitter statuses' do
    it 'is present for triggers' do
      assert get_def('requester_twitter_statuses_count')
    end

    it 'is not present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      refute get_def('requester_twitter_statuses_count')
    end
  end

  describe 'verified by twitter' do
    it 'is present for triggers' do
      assert get_def('requester_twitter_verified')
    end

    it 'is not present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      refute get_def('requester_twitter_verified')
    end
  end

  describe 'role' do
    it 'is present for triggers' do
      assert get_def('role')
    end

    it 'is not present for SLA Policies' do
      @context.options[:rule_type] = :sla_policy
      refute get_def('role')
    end
  end

  describe 'hours since conditions' do
    before do
      @account.stubs(:has_service_level_agreements?).returns(true)
    end

    %w[
      NEW
      OPEN
      PENDING
      SOLVED
      CLOSED
      assigned_at
      updated_at
      requester_updated_at
      assignee_updated_at
      due_date
      until_due_date
      sla_next_breach_at
      until_sla_next_breach_at
    ].each do |cond|
      describe cond do
        it 'is available for views and automations in ALL context' do
          @context.options[:condition_type] = :all
          @context.options[:rule_type] = :view
          assert get_def(cond)
          @context.options[:rule_type] = :automation
          assert get_def(cond)
        end

        it 'does not be available for sla policies in ALL context' do
          @context.options[:condition_type] = :all
          @context.options[:rule_type] = :sla_policy
          refute get_def(cond)
        end

        it 'does not be available in ANY context' do
          @context.options[:condition_type] = :any
          @context.options[:rule_type] = :view
          refute get_def(cond)
          @context.options[:rule_type] = :automation
          refute get_def(cond)
          @context.options[:rule_type] = :sla_policy
          refute get_def(cond)
        end

        it 'uses the calendar/business hours operators when available' do
          @account.stubs(:business_hours_active?).returns(true)
          @context.options[:condition_type] = :all
          @context.options[:rule_type] = :view
          d = get_def(cond)
          assert_equal(
            %i[
              less_than
              greater_than
              is
              less_than_business_hours
              greater_than_business_hours
              is_business_hours
            ],
            d[:operators].map { |o| o[:value] }
          )
        end

        it 'validates its value as numeric' do
          @context.options[:condition_type] = :all
          @context.options[:rule_type] = :view
          refute valid_definition?(cond, :greater_than, 'ablkjasd')
          assert_equal :invalid_number, @validation_err[:error]

          refute valid_definition?(cond, :greater_than, '10.4')
          assert_equal :invalid_number, @validation_err[:error]

          assert valid_definition?(cond, :greater_than, '10')
        end
      end
    end

    %w[
      first_reply_time_in_minutes
      first_resolution_time_in_minutes
      full_resolution_time_in_minutes
      agent_wait_time_in_minutes
      requester_wait_time_in_minutes
    ].each do |cond|
      describe cond do
        it 'is available for reporting, if extended metrics is available' do
          @context.options[:condition_type] = :all
          @context.options[:rule_type] = :report

          @account.stubs(:has_extended_ticket_metrics?).returns(true)
          assert get_def(cond)
          @account.stubs(:has_extended_ticket_metrics?).returns(false)
          refute get_def(cond)
        end
      end
    end

    describe 'on_hold_time_in_minutes' do
      it 'is available for reports if account uses on hold' do
        @context.options[:condition_type] = :all
        @context.options[:rule_type] = :report

        @account.stubs(:has_extended_ticket_metrics?).returns(true)
        @account.stubs(:use_status_hold?).returns(true)
      end
    end
  end

  describe 'resolution_time condition' do
    it 'is availble for reports' do
      @context.options[:rule_type] = :report
      assert get_def('resolution_time')
    end
  end

  describe 'extended metrics' do
    %w[reopens replies agent_stations group_stations].each do |cond|
      describe "#{cond} condition" do
        describe 'account has extended metrics' do
          before do
            @account.stubs(:extended_ticket_metrics_visible?).returns(true)
          end
          [:trigger, :report].each do |rule_type|
            describe "rule type is #{rule_type}" do
              it 'is available' do
                @context.options[:rule_type] = rule_type
                assert get_def(cond)
              end
            end
          end

          [:view, :automation, :sla_policy].each do |rule_type|
            describe "rule type is #{rule_type}" do
              it 'does not be available' do
                @context.options[:rule_type] = rule_type
                refute get_def(cond)
              end
            end
          end
        end

        it 'is unavailable when account does not have extended metrics' do
          @account.stubs(:extended_ticket_metrics_visible?).returns(false)
          refute get_def(cond)
        end
      end
    end
  end

  describe 'hidden rules' do
    before do
      @context.options[:rule_type] = :view
    end

    it 'does not be included in the json blob' do
      assert(
        generate_defs.
          detect { |d| d[:value] == 'group_is_set_with_no_assignee' }.nil?
      )
    end

    it 'is included in the json blob when asked for' do
      defs = ZendeskRules::Definitions::Conditions.
        definitions_as_json(@context, include_hidden: true)

      assert defs.detect { |d| d[:value] == 'group_is_set_with_no_assignee' }
    end
  end

  describe 'created rule' do
    before do
      @key = :exact_created_at
    end

    describe 'when the account has the SLA feature' do
      before do
        @account.stubs(:has_service_level_agreements?).returns(true)
      end

      it 'is available for sla policies in ANY/ALL context' do
        @context.options[:rule_type] = :sla_policy

        @context.options[:condition_type] = :all
        assert get_def(@key)

        @context.options[:condition_type] = :any
        assert get_def(@key)
      end

      it 'does not be available for views, automations, and triggers in ALL context' do
        @context.options[:condition_type] = :all
        [:view, :automation, :trigger].each do |rule_type|
          @context.options[:rule_type] = rule_type
          refute get_def(@key)
        end
      end

      it 'does not be available in ANY context' do
        @context.options[:condition_type] = :any
        @context.options[:rule_type] = :view
        refute get_def(@key)
        @context.options[:rule_type] = :automation
        refute get_def(@key)
      end

      describe 'with SLA policy' do
        before do
          @context.options[:condition_type] = :all
          @context.options[:rule_type] = :sla_policy
        end

        it 'does not include `is_not` and `not_present` operators for views' do
          operators = get_def(@key)[:operators].map { |op| op[:value] }

          assert_equal(
            %i[less_than less_than_equal greater_than greater_than_equal],
            operators
          )
        end

        it 'validates as YYYY-MM-DD hh:mm' do
          assert valid_definition?(@key, :less_than, '2012-03-03 00:00')
          refute valid_definition?(@key, :less_than, '2012-03-03')
          refute valid_definition?(@key, :less_than, '0')
          assert_equal :invalid_date, @validation_err[:error]
        end
      end
    end

    describe 'when the account does not have the SLA feature' do
      before do
        @account.stubs(:has_service_level_agreements?).returns(false)
      end

      it 'does not be available sla policies in ALL context' do
        @context.options[:condition_type] = :all
        @context.options[:rule_type] = :sla_policy
        refute get_def(@key)
      end

      it 'does not be available in ANY context' do
        @context.options[:condition_type] = :any
        @context.options[:rule_type] = :sla_policy
        refute get_def(@key)
      end
    end
  end

  describe 'sharing agreement rule' do
    it 'condition received_from are present' do
      @account.sharing_agreements = [
        valid_agreement(status: :accepted, direction: :in)
      ]

      [:view, :automation].each do |type|
        @context.options[:rule_type] = type
        assert get_def(:received_from)
        refute get_def(:sent_to)
      end
    end

    it 'condition sent_to are present' do
      @account.sharing_agreements = [
        valid_agreement(status: :accepted, direction: :out)
      ]

      [:view, :automation].each do |type|
        @context.options[:rule_type] = type
        refute get_def(:received_from)
        assert get_def(:sent_to)
      end
    end

    it 'empty agreements will not enable condition' do
      @account.sharing_agreements = []
      [:view, :automation].each do |type|
        @context.options[:rule_type] = type
        refute get_def(:received_from)
        refute get_def(:sent_to)
      end
    end
  end

  private

  def set_key(type) # rubocop:disable Naming/AccessorMethodName
    case type
    when 'ticket'
      @tagger = @account.active_taggers.first
      @key = "ticket_fields_#{@tagger.id}"
    when 'user', 'organization'
      @tagger = @account.
        custom_fields.
        dropdowns.
        send("for_#{type}").
        build(key: "#{type}_dropdown", title: '{{dc.welcome_wombat}}')

      @tagger.custom_field_options.build(
        name:     '{{dc.welcome_wombat}}',
        value:    'welcome_wombat',
        position: 0
      )

      @tagger.save!

      tagger_type = type == 'user' ? 'requester' : 'organization'
      @key = "#{tagger_type}.custom_fields.#{@tagger.key}"
    end
  end

  def generate_defs
    ZendeskRules::Definitions::Conditions.definitions_as_json(@context)
  end

  def get_def(form_name)
    generate_defs.detect do |d|
      d[:value] == form_name.to_s
    end
  end

  def get_operators(form_name)
    d = get_def(form_name)
    assert(d)
    d[:operators].map { |o| o[:value] }
  end

  def assert_operators(form_name, asserted_ops)
    rule_ops = get_operators(form_name)
    assert(rule_ops)
    assert_equal asserted_ops.sort, rule_ops.sort
  end

  def valid_definition?(target, operator, value = nil)
    definition_list = ZendeskRules::Definitions::Conditions.
      definition_list(@context)

    definition_list.
      valid_definition?(target, operator, value, @validation_err)
  end
end
