require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Zendesk::Rules::PositionUpdateWithCategories do
  include TestSupport::Rule::Helper

  fixtures :users, :groups

  let(:described_class) { Zendesk::Rules::PositionUpdateWithCategories }

  let(:user)    { users(:minimum_admin) }
  let(:account) { user.account }
  let(:group)   { groups(:minimum_group) }

  let(:context) { Zendesk::RuleSelection::Context::Trigger.new(user) }

  let(:scope) { account.triggers_with_category_position.active }

  before do
    Rule.destroy_all

    @category_1 = create_category(position: 1)
    @category_2 = create_category(position: 2)
    @category_3 = create_category(position: 3)

    create_trigger(title: 'AA', position: 1, rules_category_id: @category_1.id)
    create_trigger(title: 'BA', position: 2, rules_category_id: @category_1.id)
    create_trigger(title: 'ZC', position: 4, rules_category_id: @category_1.id)
    create_trigger(title: 'RQ', position: 7, rules_category_id: @category_1.id)
    create_trigger(title: 'MN', position: 10, rules_category_id: @category_1.id)
    create_trigger(title: 'QQ', position: 11, rules_category_id: @category_1.id)

    create_trigger(title: 'FF', position: 4, rules_category_id: @category_1.id, active: false)
    create_trigger(title: 'FH', position: 20, rules_category_id: @category_1.id, active: false)
    create_trigger(title: 'FG', position: 1, rules_category_id: @category_1.id, active: false)

    create_trigger(title: 'PH', position: 8, rules_category_id: @category_2.id)
    create_trigger(title: 'FD', position: 5, rules_category_id: @category_2.id)

    create_trigger(title: 'LG', position: 3, rules_category_id: @category_3.id)
    create_trigger(title: 'RT', position: 7, rules_category_id: @category_3.id)
    create_trigger(title: 'VK', position: 8, rules_category_id: @category_3.id)
  end

  describe_with_arturo_setting_enabled :trigger_categories_api do
    describe '.perform' do
      before do
        @sql_queries = sql_queries do
          described_class.perform(context, updates)
        end
      end

      describe 'when updating multiple rules in the same category' do
        let(:updates) do
          [
            {id: Rule.where(title: 'AA').first.id, position: 3},
            {id: Rule.where(title: 'MN').first.id, position: 2}
          ]
        end

        it 'updates the positions properly' do
          assert_equal %w[BA MN AA ZC RQ QQ], scope.where(rules_category_id: @category_1.id).map(&:title)
        end

        it 'enforces sequential position values' do
          assert_equal (1..6).to_a, scope.where(rules_category_id: @category_1.id).map(&:position)
        end

        it 'selects only for active rules' do
          @sql_queries.join("\n").must_include '`is_active` = 1'
        end

        it 'does not adhere to ordering queries defined in scope' do
          @sql_queries.join("\n").wont_include 'INNER JOIN'
        end
      end

      describe 'when updating multiple rules in multiple categories' do
        let(:updates) do
          [
            {id: Rule.where(title: 'AA').first.id, position: 3},
            {id: Rule.where(title: 'MN').first.id, position: 2},
            {id: Rule.where(title: 'PH').first.id, position: 1},
            {id: Rule.where(title: 'LG').first.id, position: 9}
          ]
        end

        it 'updates the positions properly' do
          assert_equal %w[BA MN AA ZC RQ QQ], scope.where(rules_category_id: @category_1.id).map(&:title)
          assert_equal %w[PH FD], scope.where(rules_category_id: @category_2.id).map(&:title)
          assert_equal %w[RT VK LG], scope.where(rules_category_id: @category_3.id).map(&:title)
        end

        it 'enforces sequential position values' do
          assert_equal (1..6).to_a, scope.where(rules_category_id: @category_1.id).map(&:position)
          assert_equal (1..2).to_a, scope.where(rules_category_id: @category_2.id).map(&:position)
          assert_equal (1..3).to_a, scope.where(rules_category_id: @category_3.id).map(&:position)
        end

        it 'selects only for active rules' do
          @sql_queries.join("\n").must_include '`is_active` = 1'
        end

        it 'does not adhere to ordering queries defined in scope' do
          @sql_queries.join("\n").wont_include 'INNER JOIN'
        end
      end

      describe 'when there are multiple updates with same position within the same category' do
        let(:updates) do
          [
            {id: Rule.where(title: 'QQ').first.id, position: 5},
            {id: Rule.where(title: 'ZC').first.id, position: 5}
          ]
        end

        it 'only respects the last update with that position' do
          assert_equal %w[AA BA RQ MN ZC QQ], scope.where(rules_category_id: @category_1.id).map(&:title)
        end

        it 'enforces sequential position values' do
          assert_equal (1..6).to_a, scope.where(rules_category_id: @category_1.id).map(&:position)
        end
      end

      describe 'when there are multiple updates with same ID within the same category' do
        let(:updates) do
          [
            {id: Rule.where(title: 'ZC').first.id, position: 3},
            {id: Rule.where(title: 'ZC').first.id, position: 2},
            {id: Rule.where(title: 'FD').first.id, position: 7}
          ]
        end

        it 'only respects the last update with that ID' do
          assert_equal %w[AA ZC BA RQ MN QQ], scope.where(rules_category_id: @category_1.id).map(&:title)
        end

        it 'enforces sequential position values' do
          assert_equal (1..6).to_a, scope.where(rules_category_id: @category_1.id).map(&:position)
        end
      end

      describe 'when the update position is out of bounds' do
        let(:updates) do
          [
            {id: Rule.where(title: 'RQ').first.id, position: -420},
            {id: Rule.where(title: 'BA').first.id, position: 10_000_000},
            {id: Rule.where(title: 'AA').first.id, position: 99_999}
          ]
        end

        it 'adds those rules to the end in order of position' do
          assert_equal %w[ZC MN QQ RQ AA BA], scope.where(rules_category_id: @category_1.id).map(&:title)
        end

        it 'enforces sequential position values' do
          assert_equal (1..6).to_a, scope.where(rules_category_id: @category_1.id).map(&:position)
        end
      end

      describe 'when there are no valid updates' do
        let(:updates) { [] }

        it 'does not perform an update' do
          @sql_queries.join("\n").wont_include 'UPDATE `rules`'
        end
      end

      describe 'when a rule does not change position' do
        let(:updates) { [{id: Rule.where(title: 'AA').first.id, position: 1}] }

        it 'is not updated' do
          @sql_queries.
            join("\n").
            must_match(/UPDATE `rules`.*FIELD\(id, NULL/)
        end
      end
    end
  end
end
