require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Action do
  fixtures :accounts,
    :cf_fields

  let(:account) { accounts(:minimum) }

  describe '.for_definition' do
    let(:definition) do
      {
        value: 'status',
        title: 'Status',
        values: {
          type: 'list',
          list: [
            {value: 'open',   title: 'Open'},
            {value: 'closed', title: 'Closed'}
          ]
        },
        group: 'ticket'
      }
    end

    let(:trigger_action) do
      Zendesk::Rules::Action.for_definition(definition, account: account)
    end

    it 'has a subject' do
      assert_equal 'status', trigger_action.subject
    end

    it 'has a title' do
      assert_equal 'Status', trigger_action.title
    end

    it 'has values' do
      assert_equal(
        [{value: 'open', title: 'Open'}, {value: 'closed', title: 'Closed'}],
        trigger_action.values.map do |action_value|
          {value: action_value.value, title: action_value.title}
        end
      )
    end

    it 'has a type' do
      assert_equal 'list', trigger_action.type
    end

    it 'has a group' do
      assert_equal 'ticket', trigger_action.group
    end

    it 'returns if it is repeatable' do
      refute trigger_action.repeatable?
    end

    it 'does not have metadata' do
      refute trigger_action.metadata.present?
    end
  end

  describe 'when initialized' do
    let(:source) { 'ticket_form_id' }
    let(:type)   { 'list' }
    let(:list)   { [] }

    let(:trigger_action) do
      Zendesk::Rules::Action.new(
        OpenStruct.new(
          source: source,
          title:  'title',
          type:   type,
          group:  'ticket',
          list:   list
        ),
        account: account
      )
    end

    it 'has a title' do
      assert_equal 'title', trigger_action.title
    end

    it 'has a group' do
      assert_equal 'ticket', trigger_action.group
    end

    it 'has a type' do
      assert_equal 'list', trigger_action.type
    end

    it 'is not repeatable' do
      refute trigger_action.repeatable?
    end

    describe '#subject' do
      describe 'when it does not have an attribute mapping' do
        let(:source) { 'status' }

        it 'returns the value' do
          assert_equal source, trigger_action.subject
        end
      end

      describe 'when it has an attribute mapping' do
        let(:source) { 'ticket_type_id' }

        it 'returns the ticket attribute name' do
          assert_equal 'type', trigger_action.subject
        end
      end

      describe 'when it is a custom ticket field' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { "ticket_fields_#{field.id}" }

        it 'returns the ticket attribute name' do
          assert_equal "custom_fields_#{field.id}", trigger_action.subject
        end
      end

      describe 'when it has the value `cc`' do
        let(:source) { 'cc' }

        it 'returns `cc`' do
          assert_equal 'cc', trigger_action.subject
        end

        describe_with_arturo_setting_enabled(
          :follower_and_email_cc_collaborations
        ) do
          it 'returns `follower`' do
            assert_equal 'follower', trigger_action.subject
          end
        end
      end
    end

    describe '#type' do
      let(:type) { 'text' }

      describe 'when it is not a tag-based action' do
        let(:source) { 'status_id' }

        it 'returns the specified type' do
          assert_equal type, trigger_action.type
        end
      end

      describe 'when it is a `current_tags` action' do
        let(:source) { 'current_tags' }

        it 'returns `tags`' do
          assert_equal 'tags', trigger_action.type
        end
      end

      describe 'when it is a `remove_tags` action' do
        let(:source) { 'remove_tags' }

        it 'returns `tags`' do
          assert_equal 'tags', trigger_action.type
        end
      end

      describe 'when it is a `set_tags` action' do
        let(:source) { 'set_tags' }

        it 'returns `tags`' do
          assert_equal 'tags', trigger_action.type
        end
      end
    end

    describe '#metadata' do
      describe 'when the action does not contain metadata' do
        let(:source) { 'status_id' }

        it 'is not present' do
          refute trigger_action.metadata.present?
        end
      end

      describe 'when it is a `notification_sms_user` action' do
        let(:source) { 'notification_sms_user' }

        it 'returns the relevant metadata' do
          assert_equal %i[phone_numbers], trigger_action.metadata
        end
      end

      describe 'when it is a `notification_sms_group` action' do
        let(:source) { 'notification_sms_group' }

        it 'returns the relevant metadata' do
          assert_equal %i[phone_numbers], trigger_action.metadata
        end
      end
    end

    describe '#nullable?' do
      describe 'when the action is not nullable' do
        let(:source) { 'status_id' }

        it 'returns false' do
          refute trigger_action.nullable?
        end
      end

      describe 'when it is an `assignee_id` action' do
        let(:source) { 'assignee_id' }

        it 'returns true' do
          assert trigger_action.nullable?
        end
      end

      describe 'when it is a `group_id` action' do
        let(:source) { 'group_id' }

        it 'returns true' do
          assert trigger_action.nullable?
        end
      end

      describe 'when it is a custom ticket field action' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { "ticket_fields_#{field.id}" }

        it 'returns true' do
          assert trigger_action.nullable?
        end
      end

      describe 'when it is a custom user field action' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { 'requester.custom_fields.user_dropdown_field' }

        it 'returns true' do
          assert trigger_action.nullable?
        end
      end

      describe 'when it is a custom organization field action' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { 'organization.custom_fields.custom_org_drop_down_field' }

        it 'returns true' do
          assert trigger_action.nullable?
        end
      end
    end

    describe '#values' do
      describe 'when the list includes an empty item' do
        let(:list) do
          [
            {value: 0, title: '-'},
            {value: 1, title: 'Option 1'},
            {value: 2, title: 'Option 2'},
            {value: 3, title: 'Option 3'}
          ]
        end

        describe 'and the action is not nullable' do
          it 'removes the empty item' do
            assert_equal(
              [
                {value: '1', title: 'Option 1'},
                {value: '2', title: 'Option 2'},
                {value: '3', title: 'Option 3'}
              ],
              trigger_action.values.map do |item|
                {value: item.value, title: item.title}
              end
            )
          end
        end

        describe 'and the action is nullable' do
          let(:source) { 'assignee_id' }

          it 'includes a null item' do
            assert_equal(
              [
                {value: '__NULL__', title: '-'},
                {value: '1',        title: 'Option 1'},
                {value: '2',        title: 'Option 2'},
                {value: '3',        title: 'Option 3'}
              ],
              trigger_action.values.map do |item|
                {value: item.value, title: item.title}
              end
            )
          end
        end
      end

      describe 'when the list items have an `enabled` key' do
        let(:list) do
          [
            {value: 1, title: 'Option 1', enabled: true},
            {value: 2, title: 'Option 2', enabled: false},
            {value: 3, title: 'Option 3', enabled: false},
            {value: 4, title: 'Option 4', enabled: true}
          ]
        end

        it 'uses the specified `enabled` values' do
          assert_equal(
            [
              {value: '1', title: 'Option 1', enabled: true},
              {value: '2', title: 'Option 2', enabled: false},
              {value: '3', title: 'Option 3', enabled: false},
              {value: '4', title: 'Option 4', enabled: true}
            ],
            trigger_action.values.map do |action_value|
              {
                value:   action_value.value,
                title:   action_value.title,
                enabled: action_value.enabled?
              }
            end
          )
        end
      end

      describe 'when the list items do not have the `enabled` key' do
        let(:source) { 'status_id' }

        let(:list) do
          [
            {value: 1, title: 'Open'},
            {value: 2, title: 'Pending'},
            {value: 3, title: 'Solved'}
          ]
        end

        it 'enables all of the items' do
          assert_equal(
            [
              {value: 'open',    title: 'Open',    enabled: true},
              {value: 'pending', title: 'Pending', enabled: true},
              {value: 'solved',  title: 'Solved',  enabled: true}
            ],
            trigger_action.values.map do |action_value|
              {
                value:   action_value.value,
                title:   action_value.title,
                enabled: action_value.enabled?
              }
            end
          )
        end

        describe 'and it is a `priority` action' do
          let(:source) { 'priority_id' }

          let(:list) do
            [
              {value: 1, title: 'Low priority'},
              {value: 2, title: 'Normal priority'},
              {value: 3, title: 'High priority'},
              {value: 4, title: 'Urgent priority'}
            ]
          end

          describe 'and the account has extended ticket priorities' do
            before { account.stubs(has_extended_ticket_priorities?: true) }

            it 'enables all of the items' do
              assert_equal(
                [
                  {value: 'low',    title: 'Low priority',    enabled: true},
                  {value: 'normal', title: 'Normal priority', enabled: true},
                  {value: 'high',   title: 'High priority',   enabled: true},
                  {value: 'urgent', title: 'Urgent priority', enabled: true}
                ],
                trigger_action.values.map do |action_value|
                  {
                    value:   action_value.value,
                    title:   action_value.title,
                    enabled: action_value.enabled?
                  }
                end
              )
            end
          end

          describe 'and the account does not have extended ticket priorities' do
            before { account.stubs(has_extended_ticket_priorities?: false) }

            it 'enables the basic priority items' do
              assert_equal(
                [
                  {value: 'low',    title: 'Low priority',    enabled: false},
                  {value: 'normal', title: 'Normal priority', enabled: true},
                  {value: 'high',   title: 'High priority',   enabled: true},
                  {value: 'urgent', title: 'Urgent priority', enabled: false}
                ],
                trigger_action.values.map do |action_value|
                  {
                    value:   action_value.value,
                    title:   action_value.title,
                    enabled: action_value.enabled?
                  }
                end
              )
            end
          end

          describe 'and the account does not have active field priorities' do
            before { account.field_priority.stubs(is_active?: false) }

            it 'disables all of the priority types' do
              assert_equal(
                [
                  {value: 'low',    title: 'Low priority',    enabled: false},
                  {value: 'normal', title: 'Normal priority', enabled: false},
                  {value: 'high',   title: 'High priority',   enabled: false},
                  {value: 'urgent', title: 'Urgent priority', enabled: false}
                ],
                trigger_action.values.map do |action_value|
                  {
                    value:   action_value.value,
                    title:   action_value.title,
                    enabled: action_value.enabled?
                  }
                end
              )
            end
          end
        end
      end

      describe 'and the action values are formatted' do
        describe 'and it is a `target` action' do
          let(:type) { 'target' }

          let(:list) do
            [
              {
                value:        1,
                title:        'Plain',
                v2:           false,
                method:       'get',
                content_type: 'sup'
              },
              {
                value:        2,
                title:        'Query',
                v2:           true,
                method:       'get',
                content_type: 'yo'
              },
              {
                value:        3,
                title:        'JSON',
                v2:           true,
                method:       'post',
                content_type: 'application/json'
              },
              {
                value:        4,
                title:        'XML',
                v2:           true,
                method:       'patch',
                content_type: 'application/xml'
              },
              {
                value:        5,
                title:        'Form',
                v2:           true,
                method:       'put',
                content_type: 'application/x-www-form-urlencoded'
              },
              {
                value:        6,
                title:        'None',
                v2:           true,
                method:       'post',
                content_type: 'non-formatted'
              }
            ]
          end

          it 'returns the formatted values' do
            assert_equal(
              [
                {value: '1', title: 'Plain', format: :plain},
                {value: '2', title: 'Query', format: :query},
                {value: '3', title: 'JSON',  format: :json},
                {value: '4', title: 'XML',   format: :xml},
                {value: '5', title: 'Form',  format: :form},
                {value: '6', title: 'None',  format: :none}
              ],
              trigger_action.values.map do |action_value|
                {
                  value:  action_value.value,
                  title:  action_value.title,
                  format: action_value.format
                }
              end
            )
          end
        end

        describe 'and it is a `custom_date` action' do
          let(:type) { 'custom_date' }

          it 'returns the formatted values' do
            assert_equal(
              [
                {
                  value: 'specific_date',
                  title: 'Set to a specific date',
                  format: 'date'
                },
                {
                  value: 'days_from_now',
                  title: 'Set to a number of days from now',
                  format: 'text'
                }
              ],
              trigger_action.values.map do |action_value|
                {
                  value:  action_value.value,
                  title:  action_value.title,
                  format: action_value.format
                }
              end
            )
          end
        end
      end

      describe 'and the action values are not formatted' do
        let(:source) { 'brand_id' }

        let(:list) do
          [{value: 1, title: 'Option 1'}, {value: 2, title: 'Option 2'}]
        end

        it 'returns the non-formatted values' do
          assert_equal(
            [
              {value: '1', title: 'Option 1', format: nil},
              {value: '2', title: 'Option 2', format: nil}
            ],
            trigger_action.values.map do |action_value|
              {
                value:  action_value.value,
                title:  action_value.title,
                format: action_value.format
              }
            end
          )
        end
      end
    end
  end
end
