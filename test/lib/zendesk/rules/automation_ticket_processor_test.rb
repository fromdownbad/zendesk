require_relative '../../../support/test_helper'
require_relative '../../../support/business_hours_test_helper'

SingleCov.covered!

describe Zendesk::Rules::AutomationTicketProcessor do
  include BusinessHoursTestHelper

  fixtures :rules,
    :tickets,
    :ticket_fields,
    :events

  let(:subject) do
    Zendesk::Rules::AutomationTicketProcessor.new(automation: automation)
  end

  let(:automation) { rules(:automation_close_ticket) }
  let(:ticket)     { tickets(:minimum_1) }

  describe '#process_tickets' do
    describe 'applicable ticket' do
      # Cover the slow code path
      before { subject.stubs(:log_slow_per_ticket_apply).returns(0) }

      it 'applies actions to the ticket' do
        subject.process_tickets([ticket])
        ticket.reload
        ticket.status_id.must_equal StatusType.CLOSED
      end

      it 'sets APM tags' do
        span = stub(:span)
        automation.stubs(:apply_actions)

        span.
          expects(:set_tag).
          with('zendesk.ticket.id', ticket.id)

        span.
          expects(:set_tag).
          with('zendesk.account_id', ticket.account_id)

        span.
          expects(:set_tag).
          with('zendesk.account_subdomain', ticket.account.subdomain)

        ZendeskAPM.
          expects(:trace).
          with(
            'automation.apply_actions',
            service: Rule::APM_SERVICE_NAME
          ).
          yields(span)

        ticket.audit = Audit.new

        subject.process_tickets([ticket])
      end

      describe 'logging how many tickets were processed and changed' do
        let(:ticket_2) { tickets(:minimum_2) }

        before do
          automation.stubs(:apply_actions)
          ticket.audit = Audit.new
          ticket.audit.events << events(:create_external_for_minimum_ticket_1)
          ticket.stubs(:save).returns(true)

          ticket_2.audit = Audit.new
          ticket_2.stubs(:save).returns(true)

          Rails.logger.expects(:warn).with(regexp_matches(/SLOW APPLY: Automation/))
        end

        describe_with_arturo_disabled :parallel_automations do
          it 'includes that the account does not have parallel automations' do
            Rails.logger.expects(:warn).with(
              "Automation execution | automation id: #{automation.id}, account.has_parallel_automations?: false, # tickets processed: 2, # tickets changed: 1"
            )

            subject.process_tickets([ticket, ticket_2])
          end
        end

        describe_with_arturo_enabled :parallel_automations do
          it 'includes that the account has parallel automations' do
            Rails.logger.expects(:warn).with(
              "Automation execution | automation id: #{automation.id}, account.has_parallel_automations?: true, # tickets processed: 2, # tickets changed: 1"
            )

            subject.process_tickets([ticket, ticket_2])
          end
        end
      end
    end

    describe 'when a ticket reaches 100 automation audits' do
      before do
        ticket.audits.create!(
          author:           User.system,
          via_id:           ViaType.RULE,
          via_reference_id: automation.id
        )

        subject.max_automations_per_ticket = 0
      end
      after { subject.max_automations_per_ticket = 100 }

      describe 'when the action solves the ticket' do
        before do
          automation.definition.actions.clear

          automation.
            definition.
            actions.
            push(DefinitionItem.new('status_id', 'is', ['3']))
        end

        it 'creates an error event on the last audit' do
          subject.process_tickets([ticket])

          assert ticket.audits.last.events.any? { |event|
            event.is_a?(::TranslatableError)
          }
        end

        it 'does not process the ticket' do
          subject.process_tickets([ticket])
          subject.changed.must_equal 0
        end
      end

      describe 'when the action closes the ticket' do
        it 'processes the ticket' do
          subject.process_tickets([ticket])
          subject.changed.must_equal 1
        end
      end
    end

    describe 'with definitions based on last updated' do
      let(:update_time) { Time.parse('2013-10-30 08:03:17 UTC') }

      before do
        add_schedule_intervals(
          ticket.account.schedules.create(
            name:      'Schedule',
            time_zone: ticket.account.time_zone
          )
        )

        automation.definition.actions.reject! do |m|
          m.source == 'status_id'
        end

        automation.definition.conditions_all.replace(
          [
            DefinitionItem.new('updated_at', 'is_business_hours', ['3']),
            DefinitionItem.new('status_id', 'is_not', ['4'])
          ]
        )
        automation.save

        audit = ticket.audits.last # our automation based audit
        @audit_2 = audit.dup       # our non-automation based audit

        audit.created_at = update_time + 3.hours + 10.minutes
        audit.via_id = ViaType.RULE
        audit.via_reference_id = automation.id
        audit.save(validate: false)

        @audit_2.created_at = update_time + 1.hours + 10.minutes
        @audit_2.save(validate: false)
        ticket.update_columns(updated_at: update_time + 2.hours)
      end

      it 'returns the proper updated_at' do
        subject = Zendesk::Rules::AutomationTicketProcessor.
          new(automation: automation)

        subject.send(:last_audit_not_for_this_automation, ticket).
          must_equal(@audit_2)
      end

      it 'acts only once for a given set of tickets' do
        sum_changed = 0

        (1..12).each do |number|
          Timecop.freeze(update_time + number.hours) do
            subject = Zendesk::Rules::AutomationTicketProcessor.
              new(automation: automation)

            subject.process_tickets([ticket])
            sum_changed += subject.changed
          end
        end

        sum_changed.must_equal 1
      end

      describe 'private tickets' do
        before do
          ticket.update_column(:is_public, false)
        end

        it 'does not send private ticket automated emails to end users' do
          ticket.audits.create!(
            author:           User.system,
            via_id:           ViaType.RULE,
            via_reference_id: automation.id
          )

          sum_changed = 0

          (1..12).each do |number|
            Timecop.freeze(update_time + number.hours) do
              subject = Zendesk::Rules::AutomationTicketProcessor.
                new(automation: automation)

              subject.process_tickets([ticket])
              sum_changed += subject.changed
            end
          end

          sum_changed.must_equal 0
        end
      end
    end

    describe 'business hours conditions' do
      let(:automation) do
        definition = Definition.new
        definition.conditions_all.replace(
          [
            DefinitionItem.new('NEW', 'is_business_hours', 8),
            DefinitionItem.new('status_id', 'less_than', [3])
          ]
        )
        definition.actions.replace(
          [
            DefinitionItem.new(
              'notification_user',
              'is',
              [users(:minimum_agent), 'h', 'h']
            )
          ]
        )
        Automation.create!(
          account:    accounts(:minimum),
          title:      'test',
          definition: definition,
          owner:      accounts(:minimum)
        )
      end

      # Friday 0700 - minimum account is utc + 1 hour
      let(:update_time) { Time.utc(2010, 'nov', 5, 6, 0) }

      before do
        Account.any_instance.stubs(:business_hours_active?).returns(true)
        add_schedule_intervals(
          ticket.account.schedules.create(
            name:      'Schedule',
            time_zone: ticket.account.time_zone
          )
        )

        ticket.update_columns(created_at: update_time, updated_at: update_time)
      end

      it 'only processes tickets once within the same non-biz-hour period' do
        # Friday 21:00 - biz hours since creation is 8 hours
        Timecop.freeze(update_time + 14.hours) do
          subject = Zendesk::Rules::AutomationTicketProcessor.
            new(automation: automation)

          subject.process_tickets([ticket])
          assert_equal 1, subject.changed
        end

        # Friday 22:00 - biz hours since creation is STILL 8 hours
        Timecop.freeze(update_time + 15.hours) do
          subject = Zendesk::Rules::AutomationTicketProcessor.
            new(automation: automation)

          subject.process_tickets([ticket])
          assert_equal 0, subject.changed
        end
      end
    end
  end

  describe '#fetch_automation_audit_count' do
    before do
      # Chaff, should not match
      ticket.status_id = StatusType.OPEN
      ticket.add_comment(body: 'Setting this to open...', is_public: true)
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!

      # Audit from Zendesk::Rules::ActionExecutor, should match
      ticket.status_id = StatusType.SOLVED
      ticket.add_comment(body: 'Setting this to solved...', is_public: true)

      ticket.will_be_saved_by(
        users(:systemuser),
        via_id:           ViaType.RULE,
        via_reference_id: automation.id
      )

      ticket.save!

      # Don't rely on purging specific key names used by
      # #fetch_automation_audit_count
      Rails.cache.clear
    end

    it 'fetchs count of system created audits' do
      assert_equal 1, subject.send(:fetch_automation_audit_count, ticket)
    end
  end
end
