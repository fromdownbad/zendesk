require_relative '../../../support/test_helper'
require_relative '../../../support/channels_test_helper'

SingleCov.covered!

describe Zendesk::Rules::Context do
  include ChannelsTestHelper

  fixtures :accounts,
    :brands,
    :ticket_fields,
    :ticket_forms,
    :translation_locales,
    :users,
    :targets,
    :organizations

  let(:described_class) { Zendesk::Rules::Context }

  let(:account) { accounts(:minimum) }
  let(:user)    { users(:minimum_agent) }

  let(:executing)  { stub('executing') }
  let(:validating) { stub('validating') }

  let(:context_options) do
    {
      rule_type:      :view,
      component_type: :condition,
      executing:      executing,
      validating:     validating
    }
  end

  let(:context) { described_class.new(account, user, context_options) }

  %i[
    accepted_outbound_sharing_agreements
    accessed_fields
    active_brands
    active_groups
    active_shared_macros
    active_ticket_forms
    agents
    assignable_agents
    current_organization_ids
    default_schedule_id
    default_ticket_form_id
    find_organization_custom_field
    find_ticket_field
    find_user_custom_field
    integration_service_instance_map
    monitored_facebook_page_map
    monitored_twitter_handle_map
    organization_custom_fields
    organization_list
    organization_list_size
    recipient_emails
    registered_integration_service_map
    requester_agents
    satisfaction_reasons
    schedules
    sharing_agreements
    ticket_custom_fields
    user_custom_fields
    active_custom_statuses
  ].each do |cached_item|
    describe "##{cached_item}" do
      let(:cache)        { stub('context cache') }
      let(:cached_value) { stub('cached value') }

      before { cache.stubs(cached_item => cached_value) }

      describe 'when a cache is specified' do
        let(:context_options) do
          {rule_type: :view, component_type: :condition, cache: cache}
        end

        it 'returns the cached value' do
          assert_equal cached_value, context.public_send(cached_item)
        end
      end

      describe 'when no cache is specified' do
        before do
          Zendesk::Rules::ContextCache.
            expects(:new).
            with(account, user, executing: executing, validating: validating).
            returns(cache)
        end

        it 'returns the cached value' do
          assert_equal cached_value, context.public_send(cached_item)
        end
      end
    end
  end

  %i[
    condition_type
    validating
    executing
    end_user_id
  ].each do |option|
    describe "##{option}" do
      let(:option_value)    { stub('option value') }
      let(:context_options) { {option => option_value} }

      it 'delegates to the options hash' do
        assert_equal option_value, context.public_send(option)
      end
    end
  end

  describe '#edit_rule_mode?' do
    let(:edit_rule_mode?) { stub('edit rule mode') }
    let(:context_options) { {edit_rule_mode?: edit_rule_mode?} }

    it 'delegates to the options hash' do
      assert_equal edit_rule_mode?, context.edit_rule_mode?
    end

    describe 'when it is not specified as an option' do
      let(:context_options) { {} }

      it 'returns false' do
        refute context.edit_rule_mode?
      end
    end
  end

  describe '#recipients' do
    let(:recipients)      { stub('recipients') }
    let(:context_options) { {recipients: recipients} }

    it 'delegates to the options hash' do
      assert_equal recipients, context.recipients
    end

    describe 'when it is not specified as an option' do
      let(:context_options) { {} }

      it 'returns an empty array' do
        assert_equal [], context.recipients
      end
    end
  end

  %i[
    has_groups?
    has_multibrand?
    has_ticket_forms?
    has_service_level_agreements?
    csat_reason_code_enabled?
    extended_ticket_metrics_visible?
    business_hours_active?
    has_multiple_schedules?
    has_ticket_sharing_triggers?
    has_rules_can_reference_macros?
    is_collaboration_enabled?
    has_tweet_requester_action?
    has_organizations?
    has_satisfaction_prediction_enabled?
    has_automatic_answers_enabled?
    has_custom_date_grouping?
    has_skill_based_ticket_routing?
    has_skill_based_ticket_routing_v2?
    has_ticket_followers_allowed_enabled?
    has_email_ccs?
    has_follower_and_email_cc_collaborations_enabled?
    has_side_conversation_macros?
    has_side_conversation_slack_macros?
    use_status_hold?
    has_custom_statuses_in_views?
  ].each do |feature|
    describe "##{feature}" do
      let(:return_value) { stub('return value') }

      before { account.stubs(feature => return_value) }

      it 'delegates to account' do
        assert_equal return_value, context.public_send(feature)
      end
    end
  end

  describe '#account_created_at' do
    it 'returns the account created at time in UTC' do
      assert_equal account.created_at.utc, context.account_created_at
    end
  end

  describe '#current_user_group_ids' do
    describe 'when a user is specified' do
      let(:group_ids) { [1, 2] }

      before do
        user.stubs(group_ids: group_ids)
      end

      it 'returns the group IDs for the current user' do
        assert_equal group_ids, context.current_user_group_ids
      end

      describe 'and the user does not belong to any groups' do
        let(:group_ids) { [] }

        it 'returns nil' do
          assert_nil context.current_user_group_ids
        end
      end
    end

    describe 'when no user is specified' do
      let(:user) { nil }

      it 'returns nil' do
        assert_nil context.current_user_group_ids
      end
    end
  end

  describe '#current_user_skills' do
    describe 'when a user is specified' do
      let(:expected_skills) { ["11ea5cab6081e5d295a137743f79ff8e", "11ea620e7103a64eadda6753a7e2f051"] }

      before do
        user.stubs(:id).returns(2)
        user.stubs(:account_id).returns(1)
      end

      it 'returns skills for the current user' do
        assert_equal expected_skills, context.current_user_skills
      end
    end

    describe 'when no user is specified' do
      let(:user) { nil }

      it 'returns nil' do
        assert_nil context.current_user_skills
      end
    end
  end

  describe '#current_role' do
    it 'returns the user role' do
      assert_equal user.role.downcase, context.current_role
    end
  end

  describe '#ticket_access' do
    it 'returns the user ticket access' do
      assert_equal user.ticket_access, context.ticket_access
    end
  end

  describe '#find_organization_by_id' do
    let(:organization_stub) { stub('organization') }
    let(:id)                { 1 }

    before do
      account.
        organizations.
        stubs(:find_by_id).
        with(id).
        returns(organization_stub)
    end

    it 'returns the organization' do
      assert_equal organization_stub, context.find_organization_by_id(id)
    end
  end

  describe '#host_name' do
    it 'returns the unmapped host name' do
      assert_equal account.host_name, context.host_name
    end
  end

  describe '#satisfaction_enabled?' do
    describe_with_arturo_enabled :customer_satisfaction do
      describe_with_arturo_setting_enabled :customer_satisfaction do
        it 'returns true' do
          assert context.satisfaction_enabled?
        end
      end

      describe_with_arturo_setting_disabled :customer_satisfaction do
        it 'returns false' do
          refute context.satisfaction_enabled?
        end
      end
    end

    describe_with_arturo_disabled :customer_satisfaction do
      it 'returns false' do
        refute context.satisfaction_enabled?
      end
    end
  end

  describe '#messaging_csat_enabled?' do
    describe 'has native_messaging_csat' do
      before { account.stubs(has_native_messaging_csat?: true) }

      it { assert context.messaging_csat_enabled? }
    end

    describe 'no native_messaging_csat' do
      before { account.stubs(has_native_messaging_csat?: false) }

      it { refute context.messaging_csat_enabled? }
    end
  end

  describe '#localized_languages' do
    describe 'when user is specified' do
      let(:english)  { translation_locales(:english_by_zendesk) }
      let(:japanese) { translation_locales(:japanese) }
      let(:spanish)  { translation_locales(:spanish) }

      let(:languages_list) { [english, japanese, spanish] }

      let(:expected_language) { [spanish.localized_name(display_in: spanish), spanish.id] }
      let(:expected_list) do
        [
          [spanish.localized_name(display_in: spanish), spanish.id],
          [english.localized_name(display_in: spanish), english.id],
          [japanese.localized_name(display_in: spanish), japanese.id]
        ]
      end

      before do
        account.stubs(available_languages: languages_list)
        user.stubs(translation_locale: spanish) # Localized to Spanish
      end

      it 'returns the localized language in the user locale' do
        assert_includes context.localized_languages, expected_language # 'español'
      end

      it 'returns sorted language list based in the user locale' do
        assert_equal expected_list, context.localized_languages # 'español', 'inglés (Estados Unidos)', 'japonés'
      end
    end

    describe 'when user is not specified' do
      let(:user) { nil }

      it 'returns an empty array' do
        assert_equal [], context.localized_languages
      end
    end
  end

  describe '#available_languages' do
    let(:locale) { translation_locales(:english_by_zendesk) }

    before { account.stubs(available_languages: [locale]) }

    it 'returns the name and ID of the locales' do
      assert_equal(
        [[locale.name, locale.id]],
        context.available_languages
      )
    end
  end

  describe '#default_language_id' do
    let(:locale) { translation_locales(:english_by_zendesk) }

    before { account.stubs(translation_locale: locale) }

    it 'returns the ID of the account translation locale' do
      assert_equal locale.id, context.default_language_id
    end
  end

  describe '#current_user_id' do
    it 'returns the ID of the current user' do
      assert_equal user.id, context.current_user_id
    end

    describe 'when user is not specified' do
      let(:user) { nil }

      it 'returns false' do
        refute context.current_user_id
      end
    end
  end

  describe '#active_targets' do
    let(:active_targets) do
      [
        targets(:flowdock_without_encrypted_credentials),
        targets(:twitter_without_encrypted_credentials),
        targets(:url_v2_without_encrypted_credentials),
        targets(:email_valid),
        targets(:email_valid2),
        targets(:twitter_valid),
        targets(:url_valid),
        targets(:url_valid_with_placeholders),
        targets(:url_v2)
      ]
    end

    it 'returns the active targets for the account' do
      assert_equal(
        active_targets.map do |target|
          {
            title:           target.title,
            id:              target.id,
            include_message: target.is_message_supported?,
            v2:              target.is_a?(UrlTargetV2),
            method:          target.settings[:method],
            content_type:    target.settings[:content_type]
          }
        end,
        context.active_targets
      )
    end
  end

  describe '#find_user' do
    let(:user_stub) { stub('user', name: 'name', id: id) }
    let(:id)        { 1 }

    before do
      account.
        users.
        stubs(:find).
        with(id).
        returns(user_stub)
    end

    it 'returns the user' do
      assert_equal(
        {name: user_stub.name, id: user_stub.id},
        context.find_user(id)
      )
    end
  end

  describe '#user_exists?' do
    describe 'when the user exists' do
      let(:user) { users(:minimum_agent) }

      it 'returns true' do
        assert context.user_exists?(user.id)
      end
    end

    describe 'when the user does not exist' do
      it 'returns false' do
        refute context.user_exists?(0)
      end
    end
  end

  describe '#time_zones' do
    it 'returns a list of time zones' do
      assert_equal ActiveSupport::TimeZone.all.count, context.time_zones.count
    end

    it 'formats the entries correctly' do
      assert(
        context.time_zones.include?(
          ['(GMT-11:00) American Samoa', 'American Samoa']
        )
      )
    end
  end

  describe '#schedule_calculations' do
    before do
      Account.any_instance.stubs(has_multiple_schedules?: true)

      account.
        schedules.
        create(name: 'A', time_zone: 'UTC').
        tap(&:soft_delete).
        update_column(:created_at, Time.utc(2010))

      account.schedules.create(name: 'B', time_zone: 'UTC').update_column(
        :created_at,
        Time.utc(2012)
      )
      account.schedules.create(name: 'C', time_zone: 'UTC').update_column(
        :created_at,
        Time.utc(2011)
      )
    end

    it 'returns the correct objects' do
      assert(
        context.schedule_calculations.all? do |calculation|
          calculation.is_a?(Zendesk::BusinessHoursSupport::Calculation)
        end
      )
    end

    it 'returns calculations for active schedules ordered by creation time' do
      assert_equal(
        account.schedules.active.map(&:id).sort.reverse,
        context.schedule_calculations.map(&:schedule_id)
      )
    end
  end

  describe '#first_business_hour_before' do
    let(:before_time) { Time.now }
    let(:result_stub) { stub('result') }

    before do
      Timecop.freeze

      account.
        stubs(:time_before_business_hours).
        with(before_time, 1).
        returns(result_stub)
    end

    it 'delegates to `time_before_business_hours`' do
      assert_equal result_stub, context.first_business_hour_before(before_time)
    end
  end

  describe '#calendar_hours_ago' do
    let(:number_of_hours) { 7 }
    let(:result_stub)     { stub('result') }

    before do
      Timecop.freeze

      account.
        stubs(:calendar_hours_ago).
        with(number_of_hours).
        returns(result_stub)
    end

    it 'delegates to `calendar_hours_ago`' do
      assert_equal result_stub, context.calendar_hours_ago(number_of_hours)
    end
  end

  describe '#calendar_hours_from_now' do
    let(:number_of_hours) { 7 }
    let(:result_stub)     { stub('result') }

    before do
      Timecop.freeze

      account.
        stubs(:calendar_hours_from_now).
        with(number_of_hours).
        returns(result_stub)
    end

    it 'delegates to `calendar_hours_from_now`' do
      assert_equal result_stub, context.calendar_hours_from_now(number_of_hours)
    end
  end

  describe '#targets_enabled?' do
    let(:targets_enabled?) { stub('targets enabled') }

    before { account.stubs(has_targets?: targets_enabled?) }

    it 'returns whether the account has targets' do
      assert_equal targets_enabled?, context.targets_enabled?
    end
  end

  describe '#has_ticket_collaboration_threads?' do
    describe 'when side conversations are enabled' do
      before do
        Arturo.enable_feature!(:ticket_threads_eap)
        account.settings.ticket_threads = true
        account.settings.save!
      end

      it 'returns true' do
        assert context.has_ticket_collaboration_threads?
      end
    end

    describe 'when side conversations are disabled' do
      before do
        account.settings.ticket_threads = false
        account.settings.save!
      end

      it 'returns true' do
        refute context.has_ticket_collaboration_threads?
      end
    end
  end

  describe "#has_side_conversations_tickets?" do
    describe 'when tickets feature is enabled' do
      before do
        Arturo.enable_feature!(:ticket_threads_eap)
        Arturo.enable_feature!(:lotus_feature_side_conversations_ticket_channel)
        account.settings.side_conversations_tickets = true
        account.settings.save!
      end

      it 'returns true' do
        assert context.has_side_conversations_tickets?
      end
    end

    describe 'when tickets feature is disabled' do
      before do
        Arturo.enable_feature!(:ticket_threads_eap)
        Arturo.enable_feature!(:lotus_feature_side_conversations_ticket_channel)
        account.settings.side_conversations_tickets = false
        account.settings.save!
      end

      it 'returns false' do
        refute context.has_side_conversations_tickets?
      end
    end
  end

  describe '#has_subject_field?' do
    describe 'when `subject` field is active' do
      before do
        account.field_subject.stubs(:is_active?).returns(true)
      end

      it 'returns true' do
        assert context.has_subject_field?
      end
    end

    describe 'when `subject` field is not active' do
      before do
        account.field_subject.stubs(:is_active?).returns(false)
      end

      it 'returns false' do
        refute context.has_subject_field?
      end
    end
  end

  describe '#has_sms_triggers?' do
    describe 'when the account has sms' do
      before do
        account.expects(:voice_feature_enabled?).with(:sms).returns true
      end

      it 'returns true' do
        assert context.has_sms_triggers?
      end
    end

    describe 'when the account does NOT have sms' do
      before do
        account.expects(:voice_feature_enabled?).with(:sms).returns false
      end

      it 'returns false' do
        refute context.has_sms_triggers?
      end
    end
  end

  describe '#unlimited_rule?' do
    let(:context) do
      Zendesk::Rules::Context.new(
        account,
        user,
        rule_type:      rule_type,
        component_type: :condition
      )
    end

    describe 'when the rule is a trigger' do
      let(:rule_type) { :trigger }

      describe 'and the account has limited triggers' do
        before { account.stubs(has_unlimited_triggers?: false) }

        it 'returns false' do
          refute context.unlimited_rule?
        end
      end

      describe 'and the account has unlimited triggers' do
        before { account.stubs(has_unlimited_triggers?: true) }

        it 'returns true' do
          assert context.unlimited_rule?
        end
      end
    end

    describe 'when the rule is not a trigger' do
      let(:rule_type) { :automation }

      it 'returns true' do
        assert context.unlimited_rule?
      end
    end
  end

  describe '#edit_rule_mode?' do
    it 'is set to true when the rule is being edited' do
      context = Zendesk::Rules::Context.new(
        account,
        user,
        rule_type:       :view,
        component_type:  :condition,
        edit_rule_mode?: true
      )

      assert context.edit_rule_mode?
    end

    it 'is set to false when it is a new rule' do
      context = Zendesk::Rules::Context.new(
        account,
        user,
        rule_type:       :view,
        component_type:  :condition,
        edit_rule_mode?: false
      )

      refute context.edit_rule_mode?
    end

    it 'is set to false by default if no value set' do
      context = Zendesk::Rules::Context.new(
        account,
        user,
        rule_type:      :view,
        component_type: :condition
      )

      refute context.edit_rule_mode?
    end
  end

  describe 'organizations list returned based on user role' do
    it 'returns all account orgs for admin' do
      @admin_user = users(:minimum_admin)

      context = Zendesk::Rules::Context.new(
        account,
        @admin_user,
        rule_type:      :view,
        component_type: :condition
      )

      assert_equal context.organization_list_size, account.organizations.size
    end

    it 'returns all account orgs for admin with org restrictions set' do
      @admin_user = users(:minimum_admin)
      # looks at possibility that user was upgraded to admin with a
      # pre-existing restriction
      # RoleRestrictionType.fuzzy_find(:organizations)
      @admin_user.restriction_id = 2

      context = Zendesk::Rules::Context.new(
        account,
        @admin_user,
        rule_type:      :view,
        component_type: :condition
      )

      assert_equal(
        context.organization_list_size,
        account.organizations.size
      )
    end

    it 'returns all account orgs for agent with no org restrictions' do
      @agent_user = users(:minimum_agent)

      context = Zendesk::Rules::Context.new(
        account,
        @agent_user,
        rule_type:      :view,
        component_type: :condition
      )

      assert_equal(
        context.organization_list_size,
        account.organizations.size
      )
    end

    it 'returns just agent organization(s) when restricted' do
      @groups_account = accounts(:with_groups)
      @groups_account.organizations << organizations(:minimum_organization1)
      @agent_user = users(:with_groups_agent_organization_restricted)

      context = Zendesk::Rules::Context.new(
        @groups_account,
        @agent_user,
        rule_type:      :view,
        component_type: :condition
      )

      assert_equal(
        context.organization_list_size,
        @agent_user.organizations.size
      )
    end
  end

  describe '#workspaces' do
    before do
      account.stubs(has_contextual_workspaces?: true)

      account.workspaces.create(
        title:       'Workspace 1 title',
        description: 'Workspace 1 description'
      )

      account.workspaces.create(
        title:       'Workspace 2 title',
        description: 'Workspace 2 description'
      )
    end

    it 'returns the list of workspaces belonging to the account' do
      assert_equal context.workspaces.count, account.workspaces.count
    end
  end
end
