require_relative '../../../support/test_helper'
require_relative '../../../support/rules_test_helper'
require_relative "../../../support/arturo_slider_helper"

SingleCov.covered! uncovered: 5

describe Zendesk::Rules::OccamClient do
  include ArturoSliderHelper

  fixtures :ticket_fields,
    :cf_fields

  before do
    Account.
      any_instance.
      stubs(has_user_and_organization_fields?: true)
  end

  let(:account)    { accounts(:minimum) }
  let(:user)       { users(:minimum_agent) }
  let(:some_ids)   { [1, 2] }
  let(:some_count) { 2 }
  let(:count_many_response) do
    {id: 123, count: 5, fresh: true, updated_at: now, sha: 'abc'}
  end
  let(:now) { Time.now.to_s }

  let(:expected_sql) do
    'SELECT `tickets`.* ' \
    'FROM `tickets` ' \
    'WHERE (tickets.status_id IN (0, 1, 2, 3, 4) ' \
    'AND tickets.account_id IN (1, -1000)) ' \
    'ORDER BY `tickets`.`nice_id` ASC,`tickets`.`nice_id` DESC'
  end

  let(:headers) { {'Content-Type' => 'application/json'} }

  before do
    stub_request(:post, /sql/).
      to_return(status: 200, body: {'sql' => expected_sql}.to_json, headers: headers)
    stub_request(:post, /find/).
      to_return(status: 200, body: {'ids' => some_ids}.to_json, headers: headers)
    stub_request(:post, /count/).
      to_return(status: 200, body: {'count' => some_count}.to_json, headers: headers)
    stub_request(:post, /count_many/).
      to_return(status: 200, body: {'counts' => [count_many_response]}.to_json, headers: headers)
  end

  let(:output) do
    Output.create(
      group:     :status,
      group_asc: true,
      order:     :id,
      order_asc: true,
      type:      'table'
    )
  end
  let(:ticket_field) { ticket_fields(:field_integer_custom) }
  let(:user_field)   { cf_fields(:checkbox1) }
  let(:org_field)    { cf_fields(:org_integer1) }

  let(:all) do
    [
      ['priority', 'is', 1],
      ['OPEN', 'less_than', 2],
      ["requester.custom_fields.#{user_field.key}",   'present', 'true'],
      ["organization.custom_fields.#{org_field.key}", 'present', 'true']
    ]
  end

  let(:any) do
    [
      ['ticket_form', 'is', 123],
      ['requester_id', 'is', 5]
    ]
  end

  let(:rule) do
    Rule.new(
      definition: Definition.new.tap do |definition|
        all.each do |arr|
          definition.conditions_all.push(DefinitionItem.new(*arr))
        end

        any.each do |arr|
          definition.conditions_any.push(DefinitionItem.new(*arr))
        end
      end
    ).tap do |rule|
      rule.output       = output
      rule.output.order = ticket_field.id.to_s
      rule.type         = 'View'
      rule.account      = account
      rule.updated_at   = Time.now
    end
  end

  let(:context) { Zendesk::Rules::Context.new(account, user, executing: true) }

  let(:execution_options) do
    Zendesk::Rules::ExecutionOptions.new(rule, rule.account, {})
  end

  let(:query) do
    Zendesk::Rules::RuleQueryBuilder.
      query_for(rule, user, execution_options, context)
  end

  let(:client) do
    Zendesk::Rules::OccamClient.new
  end

  let(:expected_account) do
    {
      'id'        => account.id,
      'shard_id'  => account.shard_id,
      'subdomain' => account.subdomain
    }
  end

  let(:expected_any) do
    [
      ['ticket_form_id', 'is', [123]],
      ['requester_id', 'is', 5]
    ]
  end

  let(:expected_context) do
    {
      'ticket_custom_fields' => [
        {
          'id'   => ticket_field.id,
          'type' => ticket_field.type,
          'tag'  => ticket_field.tag
        }
      ],
      'user_custom_fields' => [
        {
          'id'   => user_field.id,
          'type' => 'Checkbox',
          'key'  => user_field.key,
          'tag'  => nil
        }
      ],
      'organization_custom_fields' => [
        {
          'id'   => org_field.id,
          'type' => 'Integer',
          'tag'  => nil,
          'key'  => org_field.key
        }
      ],
      'use_status_hold?' => false,
      'available_languages' => account.available_languages.map do |language|
        [language.name, language.id]
      end,
      'default_language_id' => 1
    }
  end

  describe 'open timeout configuration' do
    with_env(ZENDESK_OCCAM_OPEN_TIMEOUT: 0.1) do
      it 'initializes the client with open timeout' do
        assert_equal client.default_settings[:open_timeout], 0.1
      end
    end
  end

  describe 'with occam' do
    let(:params) do
      client.build_params(user.id, rule, query, context, execution_options)
    end

    let(:expected_order) do
      ['status asc', "#{ticket_field.id} asc", 'nice_id desc']
    end

    let(:expected_options) do
      {
        cache:               false,
        watch:               false,
        timeout:             nil,
        simulate_slow:       nil,
        hanlon_parity_check: false,
        request_origin:      { 'caller' => nil }
      }.stringify_keys
    end

    describe 'when creating the client' do
      let(:conditions_all) { params[:query][:all] }
      let(:priority_condition) do
        conditions_all.detect { |field, _, _| field == 'priority_id' }
      end
      let(:and_condition) do
        conditions_all.detect { |field, _, _| field == 'AND' }
      end

      it 'correctly generates the params' do
        assert_equal expected_account, params[:account]
        assert_equal user.id,          params[:user_id]
        assert_nil params[:query][:rule][:id]
        assert_equal 'View', params[:query][:rule][:type]

        assert_not_nil params[:query][:rule][:updated_at]

        assert priority_condition
        assert_equal ['priority_id', 'is', 1], priority_condition

        field, value = and_condition
        assert_equal 2,     and_condition.size
        assert_equal 'AND', field
        assert_equal 2,     value.size
        assert_equal ['status_id', 'is', 1], value.first
        # not checking the time related condition.

        assert_equal expected_any, params[:query][:any]
        assert_equal expected_order, params[:query][:order]
        assert_equal expected_options, params[:options]
      end

      it 'generates a minimal version of the params' do
        expected = {
          # ticket_field is the sort term
          'ticket_custom_fields' => [
            {
              'id'   => ticket_field.id,
              'type' => ticket_field.type,
              'tag'  => ticket_field.tag
            }
          ],
          'user_custom_fields' => [
            {
              'id'   => user_field.id,
              'type' => 'Checkbox',
              'key'  => user_field.key,
              'tag'  => nil
            }
          ],
          'organization_custom_fields' => [
            {
              'id'   => org_field.id,
              'type' => 'Integer',
              'tag'  => nil,
              'key'  => org_field.key
            }
          ]
        }

        expected.each do |key, value|
          assert_equal value, params['context'][key]
        end
      end
    end

    it 'is able to fetch sql' do
      assert_equal expected_sql, client.sql(params)['sql']
    end

    it 'is able to fetch tickets with find' do
      response = {'ids' => some_ids}

      assert_equal response, client.find(params)
    end

    it 'is able to fetch counts' do
      response = {'count' => some_count}

      assert_equal response, client.count(params)
    end

    describe 'count many' do
      let(:views) { [rule] }

      let(:params) do
        client.send(
          :build_count_many_params,
          execution_options,
          views,
          account,
          user,
          {}
        )
      end

      let(:exected_query_options) do
        {
          with_archived: false,
          since: nil,
          find_time: nil,
          force_key: 'forced_key'
        }.stringify_keys
      end

      subject do
        client.count_many(execution_options, views, account, user, {})
      end

      it 'correctly generates the params' do
        rule.force_key = 'forced_key'
        query = params[:queries].first

        assert_equal expected_account, params[:account]
        assert_equal user.id,          params[:user_id]
        assert_equal expected_options, params[:options]
        assert_equal expected_context, params[:context]

        assert_nil query[:rule][:id]
        assert_equal 'View', query[:rule][:type]
        assert_not_nil query[:rule][:updated_at]

        assert_equal expected_any,          query[:any]
        assert_equal expected_order,        query[:order]
        assert_equal exected_query_options, query[:options]

        priority_condition = query[:all].
          detect { |field, _, _| field == 'priority_id' }

        assert_equal ['priority_id', 'is', 1], priority_condition

        and_condition = query[:all].detect { |field, _, _| field == 'AND' }
        field, value = and_condition
        assert_equal 2,     and_condition.size
        assert_equal 'AND', field
        assert_equal 2,     value.size
        assert_equal ['status_id', 'is', 1], value.first
        # not checking the time related condition.
      end

      it 'fetches result' do
        ticket_count = subject['counts'].first
        assert_equal 123, ticket_count['id']
        assert_equal 5, ticket_count['count']
      end
    end
  end
end
