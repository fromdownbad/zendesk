require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Macro::Action do
  fixtures :accounts,
    :cf_fields

  let(:account) { accounts(:minimum) }

  describe '.for_definition' do
    let(:definition) do
      {
        value: 'status',
        title: 'Status',
        values: {
          type: 'list',
          list: [
            {value: 'open',   title: 'Open'},
            {value: 'closed', title: 'Closed'}
          ]
        },
        group: 'ticket'
      }
    end

    let(:macro_action) do
      Zendesk::Rules::Macro::Action.for_definition(definition, account: account)
    end

    it 'has a subject' do
      assert_equal 'status', macro_action.subject
    end

    it 'has a title' do
      assert_equal 'Status', macro_action.title
    end

    it 'has values' do
      assert_equal(
        [{value: 'open', title: 'Open'}, {value: 'closed', title: 'Closed'}],
        macro_action.values.map do |action_value|
          {value: action_value.value, title: action_value.title}
        end
      )
    end

    it 'has a type' do
      assert_equal 'list', macro_action.type
    end

    it 'has a group' do
      assert_equal 'ticket', macro_action.group
    end

    it 'returns if it is repeatable' do
      refute macro_action.repeatable?
    end

    it 'does not have metadata' do
      refute macro_action.metadata.present?
    end
  end

  describe 'when initialized' do
    let(:source) { 'ticket_form_id' }
    let(:type)   { 'list' }
    let(:list)   { [] }

    let(:macro_action) do
      Zendesk::Rules::Macro::Action.new(
        OpenStruct.new(
          source: source,
          title:  'title',
          type:   type,
          group:  'ticket',
          list:   list
        ),
        account: account
      )
    end

    it 'has a title' do
      assert_equal 'title', macro_action.title
    end

    it 'has a group' do
      assert_equal 'ticket', macro_action.group
    end

    it 'has a type' do
      assert_equal 'list', macro_action.type
    end

    it 'does not have metadata' do
      refute macro_action.metadata.present?
    end

    describe '#subject' do
      describe 'when it does not have an attribute mapping' do
        let(:source) { 'status' }

        it 'returns the value' do
          assert_equal source, macro_action.subject
        end
      end

      describe 'when it has an attribute mapping' do
        let(:source) { 'ticket_type_id' }

        it 'returns the ticket attribute name' do
          assert_equal 'type', macro_action.subject
        end
      end

      describe 'when it is a custom ticket field' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { "ticket_fields_#{field.id}" }

        it 'returns the ticket attribute name' do
          assert_equal "custom_fields_#{field.id}", macro_action.subject
        end
      end

      describe 'when it has the value `cc`' do
        let(:source) { 'cc' }

        it 'returns `cc`' do
          assert_equal 'cc', macro_action.subject
        end

        describe_with_arturo_setting_enabled(
          :follower_and_email_cc_collaborations
        ) do
          it 'returns `follower`' do
            assert_equal 'follower', macro_action.subject
          end
        end
      end
    end

    describe '#type' do
      let(:type) { 'text' }

      describe 'when it is not a tag-based action' do
        let(:source) { 'status_id' }

        it 'returns the specified type' do
          assert_equal type, macro_action.type
        end
      end

      describe 'when it is a `current_tags` action' do
        let(:source) { 'current_tags' }

        it 'returns `tags`' do
          assert_equal 'tags', macro_action.type
        end
      end

      describe 'when it is a `remove_tags` action' do
        let(:source) { 'remove_tags' }

        it 'returns `tags`' do
          assert_equal 'tags', macro_action.type
        end
      end

      describe 'when it is a `set_tags` action' do
        let(:source) { 'set_tags' }

        it 'returns `tags`' do
          assert_equal 'tags', macro_action.type
        end
      end
    end

    describe '#nullable?' do
      describe 'when the action is not nullable' do
        let(:source) { 'status_id' }

        it 'returns false' do
          refute macro_action.nullable?
        end
      end

      describe 'when it is an `assignee_id` action' do
        let(:source) { 'assignee_id' }

        it 'returns true' do
          assert macro_action.nullable?
        end
      end

      describe 'when it is a `group_id` action' do
        let(:source) { 'group_id' }

        it 'returns true' do
          assert macro_action.nullable?
        end
      end

      describe 'when it is a custom ticket field action' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { "ticket_fields_#{field.id}" }

        it 'returns true' do
          assert macro_action.nullable?
        end
      end
    end

    describe '#repeatable?' do
      describe 'when the action is not repeatable' do
        let(:source) { 'subject' }

        it 'returns false' do
          refute macro_action.repeatable?
        end
      end

      describe 'when it is a `cc` action' do
        let(:source) { 'cc' }

        it 'returns true' do
          assert macro_action.repeatable?
        end
      end

      describe 'when it is a `set_tags` action' do
        let(:source) { 'set_tags' }

        it 'returns true' do
          assert macro_action.repeatable?
        end
      end

      describe 'when it is a `current_tags` action' do
        let(:source) { 'current_tags' }

        it 'returns true' do
          assert macro_action.repeatable?
        end
      end

      describe 'when the action has a `multiselect_list` type' do
        let(:source) { 'ticket_fields_1' }
        let(:type)   { 'multiselect_list' }

        it 'returns true' do
          assert macro_action.repeatable?
        end
      end
    end

    describe '#values' do
      let(:list) do
        [
          {value: 1, title: 'Option 1'},
          {value: 2, title: 'Option 2'},
          {value: 3, title: 'Option 3'}
        ]
      end

      it 'returns non-formatted values' do
        assert_equal(
          [
            {value: '1', title: 'Option 1', format: nil},
            {value: '2', title: 'Option 2', format: nil},
            {value: '3', title: 'Option 3', format: nil}
          ],
          macro_action.values.map do |item|
            {value: item.value, title: item.title, format: item.format}
          end
        )
      end

      describe 'when the list includes an empty item' do
        let(:list) do
          [
            {value: 0, title: '-'},
            {value: 1, title: 'Option 1'},
            {value: 2, title: 'Option 2'},
            {value: 3, title: 'Option 3'}
          ]
        end

        describe 'and the action is not nullable' do
          it 'removes the empty item' do
            assert_equal(
              [
                {value: '1', title: 'Option 1'},
                {value: '2', title: 'Option 2'},
                {value: '3', title: 'Option 3'}
              ],
              macro_action.values.map do |item|
                {value: item.value, title: item.title}
              end
            )
          end
        end

        describe 'and the action is nullable' do
          let(:source) { 'assignee_id' }

          it 'includes a null item' do
            assert_equal(
              [
                {value: '__NULL__', title: '-'},
                {value: '1',        title: 'Option 1'},
                {value: '2',        title: 'Option 2'},
                {value: '3',        title: 'Option 3'}
              ],
              macro_action.values.map do |item|
                {value: item.value, title: item.title}
              end
            )
          end
        end
      end

      describe 'when the list items have an `enabled` key' do
        let(:list) do
          [
            {value: 1, title: 'Option 1', enabled: true},
            {value: 2, title: 'Option 2', enabled: false},
            {value: 3, title: 'Option 3', enabled: false},
            {value: 4, title: 'Option 4', enabled: true}
          ]
        end

        it 'uses the specified `enabled` values' do
          assert_equal(
            [
              {value: '1', title: 'Option 1', enabled: true},
              {value: '2', title: 'Option 2', enabled: false},
              {value: '3', title: 'Option 3', enabled: false},
              {value: '4', title: 'Option 4', enabled: true}
            ],
            macro_action.values.map do |action_value|
              {
                value:   action_value.value,
                title:   action_value.title,
                enabled: action_value.enabled?
              }
            end
          )
        end
      end

      describe 'when the list items do not have the `enabled` key' do
        let(:source) { 'status_id' }

        let(:list) do
          [
            {value: 1, title: 'Open'},
            {value: 2, title: 'Pending'},
            {value: 3, title: 'Solved'}
          ]
        end

        it 'enables all of the items' do
          assert_equal(
            [
              {value: 'open',    title: 'Open',    enabled: true},
              {value: 'pending', title: 'Pending', enabled: true},
              {value: 'solved',  title: 'Solved',  enabled: true}
            ],
            macro_action.values.map do |action_value|
              {
                value:   action_value.value,
                title:   action_value.title,
                enabled: action_value.enabled?
              }
            end
          )
        end

        describe 'and it is a `priority` action' do
          let(:source) { 'priority_id' }

          let(:list) do
            [
              {value: 1, title: 'Low priority'},
              {value: 2, title: 'Normal priority'},
              {value: 3, title: 'High priority'},
              {value: 4, title: 'Urgent priority'}
            ]
          end

          describe 'and the account has extended ticket priorities' do
            before { account.stubs(has_extended_ticket_priorities?: true) }

            it 'enables all of the items' do
              assert_equal(
                [
                  {value: 'low',    title: 'Low priority',    enabled: true},
                  {value: 'normal', title: 'Normal priority', enabled: true},
                  {value: 'high',   title: 'High priority',   enabled: true},
                  {value: 'urgent', title: 'Urgent priority', enabled: true}
                ],
                macro_action.values.map do |action_value|
                  {
                    value:   action_value.value,
                    title:   action_value.title,
                    enabled: action_value.enabled?
                  }
                end
              )
            end
          end

          describe 'and the account does not have extended ticket priorities' do
            before { account.stubs(has_extended_ticket_priorities?: false) }

            it 'enables the basic priority items' do
              assert_equal(
                [
                  {value: 'low',    title: 'Low priority',    enabled: false},
                  {value: 'normal', title: 'Normal priority', enabled: true},
                  {value: 'high',   title: 'High priority',   enabled: true},
                  {value: 'urgent', title: 'Urgent priority', enabled: false}
                ],
                macro_action.values.map do |action_value|
                  {
                    value:   action_value.value,
                    title:   action_value.title,
                    enabled: action_value.enabled?
                  }
                end
              )
            end
          end

          describe 'and the account does not have active field priorities' do
            before { account.field_priority.stubs(is_active?: false) }

            it 'disables all of the priority types' do
              assert_equal(
                [
                  {value: 'low',    title: 'Low priority',    enabled: false},
                  {value: 'normal', title: 'Normal priority', enabled: false},
                  {value: 'high',   title: 'High priority',   enabled: false},
                  {value: 'urgent', title: 'Urgent priority', enabled: false}
                ],
                macro_action.values.map do |action_value|
                  {
                    value:   action_value.value,
                    title:   action_value.title,
                    enabled: action_value.enabled?
                  }
                end
              )
            end
          end
        end
      end
    end
  end
end
