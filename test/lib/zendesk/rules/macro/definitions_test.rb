require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Macro::Definitions do
  fixtures :accounts,
    :groups,
    :translation_locales,
    :users

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:definitions) { Zendesk::Rules::Macro::Definitions.new(account, user) }

  describe '#actions' do
    let(:action_context) { stub('action context') }
    let(:dc_cache)       { stub('dc_cache') }

    let(:action_definitions) do
      [
        {value: 'subject', title: 'Subject', values: {type: 'list', list: []}},
        {value: 'status',  title: 'Status',  values: {type: 'list', list: []}},
        {value: 'type',    title: 'Type',    values: {type: 'list', list: []}}
      ]
    end

    let(:context_options) do
      {component_type: :action, rule_type: :macro, dc_cache: dc_cache}
    end

    before do
      Zendesk::DynamicContent::AccountContent.
        stubs(cache: dc_cache)

      Zendesk::Rules::Context.
        stubs(:new).
        with(account, user, context_options).
        returns(action_context)

      ZendeskRules::Definitions::Actions.
        stubs(:definitions_as_json).
        with(action_context).
        returns(
          [
            {value: 'PICKACTION', title: '-- Click to select action. --'},
            *action_definitions
          ]
        )

      definitions.actions
    end

    before_should 'construct an action from each definition' do
      action_definitions.each do |definition|
        Zendesk::Rules::Macro::Action.
          expects(:for_definition).
          with(definition, account: account)
      end
    end

    it 'omits the `PICKACTION` action' do
      refute(
        definitions.actions.any? { |action| action.subject == 'PICKACTION' }
      )
    end
  end
end
