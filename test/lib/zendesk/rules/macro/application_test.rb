require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::Rules::Macro::Application do
  include TestSupport::Rule::Helper

  fixtures :all

  before do
    stub_request(:put, %r{\.amazonaws\.com/data/rule_attachments/.*})
    stub_request(:delete, %r{\.amazonaws\.com/data/rule_attachments/.*})
  end

  let(:account) { accounts(:minimum) }
  let(:macro)   { create_macro }
  let(:ticket)  { tickets(:minimum_1) }
  let(:user)    { users(:minimum_admin) }
  let(:for_chat) { false }

  let(:filename)                 { 'screenshot.jpg' }
  let(:rule_attachment_filename) { 'small.png' }
  let(:file_path)                { "#{Rails.root}/test/files/normal_1.jpg" }

  let!(:rule_attachment) do
    RuleAttachment.create! do |ra|
      ra.account_id    = account.id
      ra.user_id       = users(:minimum_agent).id
      ra.uploaded_data = fixture_file_upload(rule_attachment_filename)
      ra.filename      = rule_attachment_filename
    end
  end

  let(:macro_application) do
    Zendesk::Rules::Macro::Application.new(
      macro:  macro,
      ticket: ticket,
      user:   user,
      for_chat: for_chat
    )
  end

  before { rule_attachment.associate(macro) }

  describe '#result' do
    let(:application_run) { stub('application run') }

    before do
      Zendesk::Rules::MacroApplication.
        stubs(:new).
        with(macro, user, ticket).
        returns(stub('macro application', run: application_run))
    end

    it 'returns the result of applying the macro to the ticket' do
      assert_equal application_run, macro_application.result
    end
  end

  describe '#upload_token' do
    before { UploadToken.without_arsi.delete_all }

    describe 'when the macro does not have attachments' do
      before { RuleAttachment.destroy_all }

      it 'returns nil' do
        assert_nil macro_application.upload_token
      end
    end

    describe_with_arturo_enabled :polaris do
      describe "and applying macro for a chat" do
        it 'sets target as ticket' do
          macro_application_for_chat = Zendesk::Rules::Macro::Application.new(
            macro:  macro,
            ticket: ticket,
            user:   user,
            for_chat: true
          )
          macro_application_for_chat.upload_token
          assert_equal ticket, macro_application_for_chat.upload_token.target
        end
      end

      describe "and applying macro for a non-chat ticket" do
        it 'sets target as user' do
          macro_application.upload_token
          assert_equal user, macro_application.upload_token.target
        end
      end
    end

    describe_with_arturo_disabled :polaris do
      it 'sets target as user' do
        macro_application.upload_token
        assert_equal user, macro_application.upload_token.target
      end
    end

    describe_with_arturo_disabled :log_macro_attachments do
      describe 'and the macro has attachments' do
        it 'does not log anything' do
          Rails.logger.expects(:info).never

          macro_application.upload_token
        end
      end
    end

    describe_with_arturo_enabled :log_macro_attachments do
      describe 'and the macro has attachments' do
        before { macro_application.upload_token }

        before_should 'instrument the macro attachment application' do
          Zendesk::StatsD::Client.
            any_instance.
            expects(:increment).
            with(
              'attachment:apply',
              tags: %W[macro:#{macro.id} attachment-count:1]
            ).
            once
          Zendesk::StatsD::Client.
            any_instance.
            stubs(:increment).
            with('saved', has_key(:tags))
        end

        before_should 'log info about the macro attachment application' do
          Rails.logger.expects(:info).with(
            "Apply macro attachment for macro: #{macro.id} | "\
            "account: #{account.id}, "\
            "user: #{user.id}, "\
            "ticket: #{ticket.id}, "\
            'attachment-count: 1'
          ).once
        end

        should_change('upload tokens', by: 1) do
          UploadToken.count(:all)
        end

        should_change('attachments', by: 1) do
          Attachment.originals.count(:all)
        end

        it 'returns an upload token' do
          assert_equal UploadToken, macro_application.upload_token.class
        end

        it 'sets the upload token source' do
          assert_equal account, macro_application.upload_token.source
        end

        it 'sets the upload token target' do
          assert_equal user, macro_application.upload_token.target
        end

        it 'adds the attachments to the upload token' do
          assert_equal(
            rule_attachment_filename,
            macro_application.upload_token.attachments.first.filename
          )
        end
      end
    end
  end
end
