require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Macro::Actions do
  fixtures :accounts,
    :groups,
    :users

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:macro_actions) { Zendesk::Rules::Macro::Actions.new(account, user) }

  describe '#definitions' do
    let(:attributes) do
      action[:values][:list].map { |value| value[:value] }
    end

    it 'presents the values' do
      assert_equal(
        %w[
          subject
          status
          priority
          type
          group_id
          assignee_id
          set_tags
          current_tags
          remove_tags
          cc
          comment_value
          comment_value_html
          comment_mode_is_public
          custom_fields_10045
          custom_fields_6746
        ],
        macro_actions.definitions.map { |definition| definition[:value] }
      )
    end

    describe 'when presenting the `status` action' do
      let(:action) { macro_actions.definitions[1] }

      it 'sets the value to `status`' do
        assert_equal 'status', action[:value]
      end

      it 'is not repeatable' do
        refute action[:repeatable]
      end

      describe 'and the account uses the hold status' do
        before { Account.any_instance.stubs(use_status_hold?: true) }

        it 'formats the action attributes as names' do
          assert_equal %w[open pending hold solved], attributes
        end
      end

      describe 'and the account does not use the hold status' do
        before { Account.any_instance.stubs(use_status_hold?: false) }

        it 'formats the action attributes as names' do
          assert_equal %w[open pending solved], attributes
        end
      end
    end

    describe 'when presenting the `priority` action' do
      let(:action) { macro_actions.definitions[2] }

      it 'sets the value to `priority`' do
        assert_equal 'priority', action[:value]
      end

      it 'does not include the empty value' do
        refute_includes attributes, ''
      end

      it 'formats the action attributes as names' do
        assert_equal %w[low normal high urgent], attributes
      end

      it 'is not repeatable' do
        refute action[:repeatable]
      end

      describe 'and the priority field is enabled' do
        before do
          # Do not stub account directly because it needs to be dumpable
          FieldPriority.any_instance.stubs(is_active?: true)
        end

        describe 'and the account has extended ticket priorities' do
          before do
            # Do not stub account directly because it needs to be dumpable
            Account.any_instance.stubs(has_extended_ticket_priorities?: true)
          end

          it 'enables all priorities' do
            assert action[:values][:list].all? { |item| item[:enabled] }
          end
        end

        describe 'and the account does not have extended ticket priorities' do
          before do
            # Do not stub account directly because it needs to be dumpable
            Account.any_instance.stubs(has_extended_ticket_priorities?: false)
          end

          it 'only enables the basic priorities' do
            assert_equal(
              %w[normal high],
              action[:values][:list].each_with_object([]) do |item, items|
                items << item[:value] if item[:enabled]
              end
            )
          end
        end
      end

      describe 'and the priority field is disabled' do
        before do
          # Do not stub account directly because it needs to be dumpable
          FieldPriority.any_instance.stubs(is_active?: false)
        end

        it 'does not enable any priorities' do
          refute action[:values][:list].any? { |item| item[:enabled] }
        end
      end
    end

    describe 'when presenting the `type` action' do
      let(:action) { macro_actions.definitions[3] }

      it 'sets the value to `type`' do
        assert_equal 'type', action[:value]
      end

      it 'formats the action attributes as names' do
        assert_equal(
          %w[question incident problem task],
          attributes
        )
      end

      it 'is not repeatable' do
        refute action[:repeatable]
      end
    end

    describe 'when presenting the `add cc` action' do
      let(:action) { macro_actions.definitions[9] }

      it 'sets the value to `cc`' do
        assert_equal 'cc', action[:value]
      end

      it 'returns `current_user` as the first attribute' do
        assert_equal 'current_user', attributes.first
      end

      it 'returns valid user IDs for the remaining attributes' do
        attributes.drop(1).each do |id|
          assert User.find_by_id(id).present?
        end
      end

      it 'is repeatable' do
        assert action[:repeatable]
      end
    end

    describe 'when presenting the `comment_value` action' do
      let(:action) { macro_actions.definitions[10] }

      it 'sets the value to `comment_value`' do
        assert_equal 'comment_value', action[:value]
      end

      it 'sets the channel values' do
        assert_equal %w[channel:all channel:web], attributes
      end

      it 'is not repeatable' do
        refute action[:repeatable]
      end
    end

    describe 'when presenting the `set_tags` action' do
      let(:action) do
        macro_actions.definitions.find { |action| action[:value] == 'set_tags' }
      end

      it 'sets the value to `set_tags`' do
        assert_equal 'set_tags', action[:value]
      end

      it 'sets the type to `text`' do
        assert_equal 'text', action[:values][:type]
      end

      it 'sets the title' do
        assert_equal 'Set tags', action[:title]
      end

      it 'is repeatable' do
        assert action[:repeatable]
      end
    end

    describe 'when presenting the `current_tags` action' do
      let(:action) do
        macro_actions.
          definitions.
          find { |action| action[:value] == 'current_tags' }
      end

      it 'sets the value to `current_tags`' do
        assert_equal 'current_tags', action[:value]
      end

      it 'sets the type to `text`' do
        assert_equal 'text', action[:values][:type]
      end

      it 'sets the title' do
        assert_equal 'Add tags', action[:title]
      end

      it 'is repeatable' do
        assert action[:repeatable]
      end
    end

    describe 'when presenting the `remove_tags` action' do
      let(:action) do
        macro_actions.
          definitions.
          find { |action| action[:value] == 'remove_tags' }
      end

      it 'sets the value to `remove_tags`' do
        assert_equal 'remove_tags', action[:value]
      end

      it 'sets the type to `text`' do
        assert_equal 'text', action[:values][:type]
      end

      it 'sets the title' do
        assert_equal 'Remove tags', action[:title]
      end

      it 'is not repeatable' do
        refute action[:repeatable]
      end
    end

    describe 'when presenting a multi-select custom field action' do
      let(:action) do
        macro_actions.definitions.find do |definition|
          definition[:values][:type] == 'multiselect_list'
        end
      end

      before do
        FieldMultiselect.create!(
          account:              account,
          title:                'Multiselect Wombat',
          custom_field_options: [
            CustomFieldOption.new(name: 'Wombats',           value: 'wombats'),
            CustomFieldOption.new(name: 'Giraffes',          value: 'giraffes'),
            CustomFieldOption.new(name: 'Tazzmanian Devils', value: 'tazzies'),
            CustomFieldOption.new(name: 'Lemurs',            value: 'lemurs')
          ]
        )
      end

      it 'sets the attributes properly' do
        assert_equal %w[wombats giraffes tazzies lemurs], attributes
      end

      it 'is repeatable' do
        assert action[:repeatable]
      end
    end
  end
end
