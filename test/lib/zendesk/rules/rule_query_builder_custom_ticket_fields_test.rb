require_relative '../../../support/test_helper'
require_relative '../../../support/business_hours_test_helper'
require_relative '../../../support/rule_query_builder_test_helper'
require 'zendesk_rules/rule_query/restriction_group'

SingleCov.not_covered!

describe 'RuleQueryBuilderCustomTicketFields' do
  include CustomFieldsTestHelper
  include BusinessHoursTestHelper
  include RuleQueryBuilderTestHelper

  fixtures :accounts,
    :tickets,
    :users,
    :groups,
    :memberships,
    :ticket_fields,
    :custom_field_options,
    :organizations,
    :organization_memberships

  describe 'Custom ticket fields' do
    before do
      @account           = accounts(:minimum)
      @definition_item   = DefinitionItem.new('REPLACE_IN_TEST', 'is', '5')
      @restriction_group = ZendeskRules::RuleQuery::RestrictionGroup.new
      Timecop.freeze '2010-03-09 22:25:01 UTC'
    end

    describe 'taggers' do
      before do
        @tagger    = ticket_fields(:field_tagger_custom)
        @tagger_id = @tagger.id
        @custom_field_option =
          custom_field_options(:field_tagger_custom_option_2)
      end

      it 'does nothing for missing field' do
        @definition_item.source   = 'ticket_fields_123456'
        @definition_item.operator = 'is'
        @definition_item.value    =
          custom_field_options(:field_tagger_custom_option_2).id

        assert_nil generate_sql
      end

      it 'should look for tag' do
        @definition_item.source   = "ticket_fields_#{@tagger_id}"
        @definition_item.operator = 'is'
        @definition_item.value    =
          custom_field_options(:field_tagger_custom_option_2).id

        custom_field_value =
          custom_field_options(:field_tagger_custom_option_2).value

        assert_equal(
          "tickets.current_tags REGEXP '(^| )(#{custom_field_value})($| +)'",
          generate_sql
        )
      end

      it 'should not look for tag for missing custom field option' do
        @definition_item.source   = "ticket_fields_#{@tagger_id}"
        @definition_item.operator = 'is'
        @definition_item.value    = '2222'
        assert_nil generate_sql
      end

      it 'does nothing for an inactive field' do
        @tagger.is_active = false
        @tagger.save!

        query = get_query_for_conditions(
          "ticket_fields_#{@tagger_id}",
          'is',
          custom_field_options(:field_tagger_custom_option_2).id
        )

        assert query.to_sql !~ /cfv/
        assert_equal [], query.joins
      end
    end

    describe 'checkboxes' do
      before do
        @field_checkbox = ticket_fields(:field_checkbox_custom)
        @definition_item.source   = "ticket_fields_#{@field_checkbox.id}"
        @definition_item.operator = 'is'
      end

      it 'should look for checkbox tag' do
        @definition_item.value = true
        assert_equal(
          "tickets.current_tags REGEXP '(^| )(#{@field_checkbox.tag})($| +)'",
          generate_sql
        )
      end

      it 'should look for non-inclusion of checkbox tag' do
        @definition_item.value = false
        assert_equal(
          '(tickets.current_tags IS NULL OR NOT tickets.current_tags ' \
            "REGEXP '(^| )(#{@field_checkbox.tag})($| +)')",
          generate_sql
        )
      end

      it 'ignores checkboxes with a nil tag' do
        @definition_item.value = true
        @field_checkbox.tag    = nil
        @field_checkbox.save!

        assert_nil generate_sql
      end

      it 'ignores checkboxes with an empty tag' do
        @definition_item.value = true
        @field_checkbox.tag    = ''
        @field_checkbox.save!

        assert_nil generate_sql
      end
    end

    describe 'FieldDate' do
      let(:id_query) { 'tickets.status_id IN (0, 1, 2, 6, 3, 4)' }

      before do
        Arturo.enable_feature!(:date_custom_field)

        @definition_item   = nil
        @custom_date_field = FieldDate.create!(
          account: accounts(:minimum),
          title:   'Custom wombat date',
          is_active:  true
        )
      end

      it 'generates correct join table statement for the query' do
        query = get_query_for_conditions(
          "ticket_fields_#{@custom_date_field.id}",
          'is',
          '2014-02-18'
        )

        table_name = "custom_ticket_field_#{@custom_date_field.id}"
        assert_equal(
          [
            "JOIN `ticket_field_entries` AS `#{table_name}` ON " \
              "`tickets`.`id` = `#{table_name}`.`ticket_id` AND " \
              "`#{table_name}`.`ticket_field_id` = #{@custom_date_field.id}"
          ],
          query.sql_joins
        )
      end

      it 'generates correct query for `is` operator' do
        query = get_query_for_conditions(
          "ticket_fields_#{@custom_date_field.id}",
          'is',
          '2014-02-18'
        )

        assert_equal(
          "`custom_ticket_field_#{@custom_date_field.id}`.`value` = " \
            "'2014-02-18'",
          query.to_sql
        )
      end

      it 'generates correct query for `less_than_equal` operator' do
        query = get_query_for_conditions(
          "ticket_fields_#{@custom_date_field.id}",
          'less_than_equal',
          '2014-02-18'
        )

        assert_equal(
          "`custom_ticket_field_#{@custom_date_field.id}`.`value` <= " \
            "'2014-02-18'",
          query.to_sql
        )
      end

      it 'generates correct query for `greater_than_equal` operator' do
        query = get_query_for_conditions(
          "ticket_fields_#{@custom_date_field.id}",
          'greater_than_equal',
          '2014-02-18'
        )

        assert_equal(
          "`custom_ticket_field_#{@custom_date_field.id}`.`value` >= " \
            "'2014-02-18'",
          query.to_sql
        )
      end

      it 'generates correct query for `less_than` operator' do
        query = get_query_for_conditions(
          "ticket_fields_#{@custom_date_field.id}",
          'less_than',
          '2014-02-18'
        )

        assert_equal(
          "`custom_ticket_field_#{@custom_date_field.id}`.`value` < " \
            "'2014-02-18'",
          query.to_sql
        )
      end

      it 'does not try to optimize queries with multiple operators' do
        table_alias = "custom_ticket_field_#{@custom_date_field.id}"

        query = get_query_for_conditions(
          "ticket_fields_#{@custom_date_field.id}",
          'less_than',
          '2014-02-18',
          "ticket_fields_#{@custom_date_field.id}",
          'greater_than',
          '2014-01-18'
        )

        assert_equal(
          "(`#{table_alias}`.`value` < '2014-02-18' AND " \
            "`#{table_alias}`.`value` > '2014-01-18')",
          query.to_sql
        )

        assert_equal(
          [
            "JOIN `ticket_field_entries` AS `#{table_alias}` ON " \
              "`tickets`.`id` = `#{table_alias}`.`ticket_id` AND " \
              "`#{table_alias}`.`ticket_field_id` = #{@custom_date_field.id}"
          ],
          query.sql_joins
        )
      end

      it 'generates correct query for `greater_than` operator' do
        query = get_query_for_conditions(
          "ticket_fields_#{@custom_date_field.id}",
          'greater_than',
          '2014-02-18'
        )

        assert_equal(
          "`custom_ticket_field_#{@custom_date_field.id}`.`value` > " \
            "'2014-02-18'",
          query.to_sql
        )
      end

      it 'generates correct query for `within_previous_n_days` operator' do
        Timecop.freeze

        query = get_query_for_conditions(
          "ticket_fields_#{@custom_date_field.id}",
          'within_previous_n_days',
          '5'
        )

        assert_includes(
          query.to_sql,
          "`custom_ticket_field_#{@custom_date_field.id}`.`value` >= " \
            "'#{5.days.ago.to_s(:db)}' AND " \
            "`custom_ticket_field_#{@custom_date_field.id}`.`value` " \
            "<= '#{Time.now.to_s(:db)}'",
          query.to_sql
        )
      end

      it 'generates correct query for `within_next_n_days` operator' do
        Timecop.freeze

        query = get_query_for_conditions(
          "ticket_fields_#{@custom_date_field.id}",
          'within_next_n_days',
          '5'
        )

        assert_includes(
          query.to_sql,
          "`custom_ticket_field_#{@custom_date_field.id}`.`value` <= " \
            "'#{5.days.from_now.to_s(:db)}' AND " \
            "`custom_ticket_field_#{@custom_date_field.id}`.`value` >= " \
            "'#{Time.now.to_s(:db)}'",
          query.to_sql
        )
      end
    end
  end
end
