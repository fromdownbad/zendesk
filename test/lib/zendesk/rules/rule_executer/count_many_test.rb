require_relative '../../../../support/test_helper'
require_relative '../../../../support/business_hours_test_helper'

SingleCov.covered!

describe Zendesk::Rules::RuleExecuter::CountMany do
  fixtures :rules
  before do
    Timecop.freeze
    Zendesk::StatsD::Client.any_instance.stubs(:increment)
  end
  after { Timecop.return }

  def assert_broken_counts(count_tickets)
    count_tickets.each do |rule_count|
      next if %w[deleted suspended].include?(rule_count.rule_id)

      assert rule_count.is_a?(Zendesk::Rules::OccamTicketCount)
      assert_equal ::View::BROKEN_COUNT, rule_count.value
      assert(rule_count.fresh)
      assert_equal Time.now.to_s, rule_count.updated_at
      assert_equal 'poll', rule_count.refresh
      assert_equal Zendesk::Rules::BrokenOccamTicketCount::POLL_WAIT, rule_count.poll_wait
    end
  end

  def assert_report_error(name)
    Zendesk::StatsD::Client.any_instance.expects(:increment).
      with('rules.view_api_occam_error', tags: ['action:count_many', "error:#{name}"])
  end

  let(:occam_client) { Zendesk::Rules::OccamClient.any_instance }
  let(:account)      { accounts(:minimum) }
  let(:views)        { [rules(:view_my_working_tickets), rules(:view_assigned_working_tickets)] }
  let(:user)         { account.agents.first }

  let(:count_tickets) do
    executer.perform
  end

  let(:executer) do
    Zendesk::Rules::RuleExecuter::CountMany.new(
      account:           account,
      views:             views,
      user:              user,
      include_deleted:   true,
      include_suspended: true,
      params:            {}
    )
  end

  describe '.count_many' do
    let(:now) { Time.now.to_s }

    describe 'occam responds with cache' do
      let(:occam_response) do
        counts = views.map do |rule|
          {
            id:         rule.id,
            count:      5,
            fresh:      true,
            updated_at: now,
            refresh:    'poll',
            poll_wait:  0
          }
        end

        {'counts' => counts.map(&:stringify_keys)}
      end

      before { occam_client.stubs(post: occam_response) }

      it 'returns OccamTicketCount instances' do
        assert_equal 4, count_tickets.count

        count_tickets.each do |rule_count|
          next if %w[deleted suspended].include?(rule_count.rule_id)

          assert rule_count.is_a?(Zendesk::Rules::OccamTicketCount)
          assert_equal 5, rule_count.value
          assert(rule_count.fresh)
          assert_equal now, rule_count.updated_at
          assert_equal 'poll', rule_count.refresh
          assert_equal 0, rule_count.poll_wait
        end
      end

      describe_with_arturo_enabled :views_disable_count_many do
        it 'returns broken OccamTicketCount instances' do
          assert_equal 4, count_tickets.count
          assert_broken_counts(count_tickets)
        end

        it 'logs the event' do
          Rails.logger.expects(:error).with("[views][count_many] CountMany disabled for Account #{account.id}")
          count_tickets
        end

        it 'sends datadog metrics' do
          assert_report_error('count_many_disabled_error')
          count_tickets
        end
      end

      it 'includes suspended and deleted tickets counts' do
        rule_count = count_tickets.find { |ct| ct.rule_id == 'deleted' }
        assert(rule_count.fresh)
        assert_equal now, rule_count.updated_at
        assert_equal 'poll', rule_count.refresh
        assert_equal 60, rule_count.poll_wait

        rule_count = count_tickets.find { |ct| ct.rule_id == 'suspended' }
        assert(rule_count.fresh)
        assert_equal now, rule_count.updated_at
        assert_equal 'poll', rule_count.refresh
        assert_equal 60, rule_count.poll_wait
      end
    end

    describe 'occam responds with error' do
      let(:occam_response) { {'error' => 'ValidationError'} }

      before do
        occam_client.stubs(:post).returns(occam_response)
      end

      it 'returns empty results' do
        assert_empty(count_tickets)
      end
    end

    describe 'circuit breaking' do
      after { JohnnyFive::MemoryStore.instance.reset_all! }
      before do
        Zendesk::Rules::OccamClient.
          any_instance.
          stubs(:count_many).
          raises(Occam::Client::ClientConnectionError, Faraday::Error::TimeoutError.new)

        # let it fail only once
        executer.stubs(:circuit_options).returns(failure_threshold: 1, reset_timeout: 1)
      end

      describe_with_arturo_enabled :views_count_many_circuit_breaker do
        it 'returns broken counts' do
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.perform
          end
          # open
          assert_broken_counts(executer.perform)
          assert_broken_counts(executer.perform)

          Timecop.freeze(2)
          # closed
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.perform
          end
          # open
          assert_broken_counts(executer.perform)
        end

        it 'reports open circuit' do
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.perform
          end
          # open
          assert_report_error('circuit_tripped_error')
          executer.perform
        end
      end

      describe_with_arturo_disabled :views_count_many_circuit_breaker do
        it 'does NOT use a circuit breaker for count_many' do
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.perform
          end
          # A second exception means the circuit breaker didn't kick in.
          # It would recover from this error and present broken counts with a 200 status
          assert_raise(Occam::Client::ClientConnectionError) do
            executer.perform
          end
        end
      end
    end

    describe 'when there is a client error' do
      [
        Occam::Client::ClientError,
        Occam::Client::ClientConnectionError,
        Occam::Client::ClientTimeoutError,
        Occam::Client::QueryTimeoutError
      ].each do |error_class|
        describe "and the error type is `#{error_class}`" do
          before do
            occam_client.stubs(:post).raises(error_class.new(StandardError.new))
          end

          it 'raises the specified error type' do
            assert_raises(error_class) { count_tickets.empty? }
          end
        end
      end
    end
  end
end
