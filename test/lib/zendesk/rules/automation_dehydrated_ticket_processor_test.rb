require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::AutomationDehydratedTicketProcessor do
  fixtures :rules, :tickets

  let(:subject) do
    Zendesk::Rules::AutomationDehydratedTicketProcessor.
      new(automation: automation)
  end

  let(:automation) { rules(:automation_close_ticket) }
  let(:ticket)     { tickets(:minimum_1) }

  let(:dehydrated_ticket) do
    Zendesk::Rules::DehydratedTicket.new_from_ticket(ticket)
  end

  describe '#process_tickets' do
    it 'rehydrates and processes the ticket' do
      subject.process_tickets([dehydrated_ticket])
      subject.changed.must_equal 1
      ticket.tap(&:reload).status_id.must_equal StatusType.CLOSED
    end
  end
end
