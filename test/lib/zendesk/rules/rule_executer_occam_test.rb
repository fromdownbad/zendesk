require_relative '../../../support/test_helper'

SingleCov.not_covered!

describe Zendesk::Rules::RuleExecuter do
  fixtures :accounts,
    :users,
    :groups,
    :memberships,
    :organizations,
    :subscriptions,
    :tickets,
    :ticket_fields,
    :ticket_field_entries,
    :events,
    :rules

  before do
    @minimum_account = accounts(:minimum)
    @minimum_agent = users(:minimum_agent)
    Arturo.enable_feature! :occam_rule_execution
  end

  let(:options)       { {is_api: false} }
  let(:rule)          { rules(:view_your_unsolved_tickets) }
  let(:empty_set)     { {'ids' => []} }
  let(:some_ids)      { [tickets(:minimum_1).id, tickets(:minimum_2).id] }
  let(:occam_result)  { {'ids' => some_ids} }
  let(:empty_count)   { {'count' => 0} }
  let(:some_count)    { {'count' => 2} }
  let(:user)          { accounts(:minimum).owner }
  let(:find_tickets)  do
    Zendesk::Rules::RuleExecuter.find_tickets(rule, user, paginate: true)
  end
  let(:count_tickets) { Zendesk::Rules::RuleExecuter.count_tickets(rule, user) }

  describe '.find_tickets' do
    describe 'given a possible rule' do
      it 'calls Occam' do
        Zendesk::Rules::OccamClient.
          any_instance.
          expects(:find).
          returns(empty_set)

        find_tickets
      end

      describe 'with some results' do
        before do
          Zendesk::Rules::OccamClient.
            any_instance.
            stubs(:find).
            returns(occam_result)
        end

        it 'adds virtual attributes to results' do
          cf_id = ticket_fields(:field_text_custom).id

          rule.output = Output.create(
            columns: [
              :requester,
              :assignee,
              :submitter,
              :group,
              :organization,
              cf_id.to_s.to_sym
            ]
          )

          ticket = find_tickets.first

          assert ticket.attributes["field_#{cf_id}"]
        end

        it 'returns the ticket in order of ids' do
          assert_equal([1, 2], find_tickets.map(&:nice_id))
        end

        it 'hydrates the tickets' do
          fields = %i[
            score
            description
            requester
            created
            type
            priority
            nice_id
            group
            status
            type
          ].map { |field| ZendeskRules::RuleField.lookup(field) }

          associations = fields.map(&:association).compact.uniq

          ticket = find_tickets.first
          ActiveRecord::Base.connection.expects(:execute).never
          associations.each do |association|
            ticket.send(association)
          end
          %i[photo identities taggings groups].each do |rel|
            ticket.requester.send(rel)
          end
        end

        describe 'with archived ticket results' do
          before do
            ticket = Ticket.find some_ids.first
            ticket.status_id = StatusType.CLOSED
            ticket.will_be_saved_by(ticket.account.owner)
            ticket.save!
            ticket.archive!

            rule.stubs(:can_find_tickets_on_archive?).returns(true)
          end

          it 'finds archived tickets and preserves nice_id order' do
            tickets = find_tickets
            assert_equal some_ids, tickets.map(&:id)
          end
        end
      end

      describe 'with unexpected results' do
        before do
          Zendesk::Rules::OccamClient.
            any_instance.
            stubs(:find).
            returns(occam_result)
        end

        describe 'RuleInvalid' do
          let(:occam_result) do
            {'error' => 'AttributeError', 'message' => '間違い'}
          end

          it 'raises UnprocessableEntity' do
            assert_raises Zendesk::Rules::RuleExecuter::UnprocessableEntity do
              find_tickets
            end
          end
        end

        describe 'other errors' do
          let(:occam_result) { {'error' => 'なに？', 'message' => '間違い'} }

          it 'raises UnknownOccamError' do
            assert_raises Zendesk::Rules::RuleExecuter::UnknownOccamError do
              find_tickets
            end
          end
        end
      end

      describe 'with bogus results' do
        let(:some_ids) { [tickets(:minimum_1).id, 123_123_123_123] }

        it 'logs a warning' do
          Zendesk::Rules::OccamClient.
            any_instance.
            stubs(:find).
            returns(occam_result)

          Rails.logger.expects(:warn)

          find_tickets
        end
      end

      describe 'for out of range ticket queries' do
        let(:page_size) { 9 }

        before do
          Zendesk::Rules::OccamClient.
            any_instance.
            expects(:find).
            returns(occam_result)

          @result = Zendesk::Rules::RuleExecuter.find_tickets(
            rule,
            user,
            paginate: true,
            per_page: page_size,
            page:     1
          )

          @total_tickets = @result.size
          @cached_count = rule.ticket_count(user, refresh: false)

          # lets try an out of range query i.e ask for page which does not
          # exist
          Zendesk::Rules::OccamClient.
            any_instance.
            expects(:find).
            returns(empty_set)

          @result = Zendesk::Rules::RuleExecuter.find_tickets(
            rule,
            user,
            paginate: true,
            per_page: page_size,
            page:     2
          )

          @cached_count = rule.ticket_count(user, refresh: false)
        end

        it 'does not update cache ticket count' do
          assert @total_tickets > @result.size
          assert_equal @result.size, 0
          assert_equal @cached_count.value, @total_tickets
        end
      end

      describe 'with ids_only option' do
        before do
          Zendesk::Rules::OccamClient.
            any_instance.
            stubs(:find).
            returns(occam_result)
        end

        it 'does not hydrate the tickets' do
          ticket_id = Zendesk::Rules::RuleExecuter.
            find_tickets(rule, user, ids_only: true).first

          assert ticket_id.is_a?(Integer)
        end
      end
    end

    describe 'given an impossible rule' do
      before do
        Zendesk::Rules::OccamClient.
          any_instance.
          stubs(:find).
          returns(empty_set)
      end

      it 'returns an empty result' do
        assert_empty(find_tickets)
      end
    end
  end

  describe '.count_tickets' do
    describe 'given an impossible rule' do
      before do
        Zendesk::Rules::OccamClient.
          any_instance.
          expects(:count).
          returns(empty_count)
      end

      it 'returns zero as result' do
        assert_equal 0, count_tickets
      end
    end

    describe 'given a rule with some count' do
      before do
        Zendesk::Rules::OccamClient.
          any_instance.
          expects(:count).
          returns(some_count)
      end

      it 'returns the count as result' do
        assert_equal(2, count_tickets)
      end
    end
  end
end
