require_relative '../../../support/test_helper'
require_relative '../../../support/business_hours_test_helper'
require_relative '../../../support/rule'

SingleCov.covered! uncovered: 121

describe Zendesk::Rules::Match do
  include BusinessHoursTestHelper
  include TestSupport::Rule::Helper

  fixtures :all

  before { Arturo.enable_feature!(:memoizing_schedules_in_triggers) }

  describe 'when it is included' do
    class TestRule
      include Zendesk::Rules::Match
    end

    let(:subject) { TestRule.new }

    let(:ticket)    { tickets(:minimum_1) }
    let(:events)    { ticket.events }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    describe '#match?' do
      let(:match?) { stub('match') }

      before do
        Zendesk::Rules::Match.
          stubs(:match?).
          with(subject, ticket, events, run_state).
          returns(match?)
      end

      it 'delegates to the match module' do
        assert_equal match?, subject.match?(ticket, events, run_state)
      end
    end

    describe '#condition_match?' do
      let(:condition_match?) { stub('condition match') }

      before do
        Zendesk::Rules::Match.
          stubs(:condition_match?).
          with(subject, ticket, events, run_state).
          returns(condition_match?)
      end

      it 'delegates to the match module' do
        assert_equal(
          condition_match?,
          subject.condition_match?(ticket, events, run_state)
        )
      end
    end
  end

  describe '.condition_match?' do
    let(:ticket)    { tickets(:minimum_1) }
    let(:events)    { ticket.events }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    let(:rule) do
      new_trigger(account: ticket.account, definition: definition)
    end

    let(:definition) do
      Definition.new.tap do |definition|
        definition.conditions_all.push(*conditions_all)
        definition.conditions_any.push(*conditions_any)
      end
    end

    let(:conditions_all) do
      [
        DefinitionItem.new('priority_id', 'is', PriorityType.LOW),
        DefinitionItem.new('status_id', 'is', StatusType.OPEN)
      ]
    end

    let(:conditions_any) do
      [
        DefinitionItem.new('priority_id', 'is_not', PriorityType.URGENt),
        DefinitionItem.new('status_id', 'is_not', StatusType.CLOSED)
      ]
    end

    let(:condition_match?) do
      Zendesk::Rules::Match.condition_match?(rule, ticket, events, run_state)
    end

    describe 'when every `all` condition matches' do
      before do
        conditions_all.each do |condition|
          Zendesk::Rules::Match.
            expects(:instance_matches_condition?).
            with(ticket, condition, events, run_state).
            returns(true)
        end
      end

      describe 'and there are no `any` conditions' do
        let(:conditions_any) { [] }

        it 'returns match' do
          assert_equal :match, condition_match?
        end

        describe 'and it is evaluated' do
          before { condition_match? }

          before_should 'log the trigger match' do
            Zendesk::Rules::Match.
              expects(:trigger_log).
              with(rule, ticket, :match, :blank)
          end
        end
      end

      describe 'and an `any` condition matches' do
        before do
          Zendesk::Rules::Match.
            expects(:instance_matches_condition?).
            with(ticket, conditions_any.first, events, run_state).
            returns(false)

          Zendesk::Rules::Match.
            expects(:instance_matches_condition?).
            with(ticket, conditions_any.last, events, run_state).
            returns(true)
        end

        it 'returns match' do
          assert_equal :match, condition_match?
        end

        describe 'and it is evaluated' do
          before { condition_match? }

          before_should 'log the trigger match' do
            Zendesk::Rules::Match.
              expects(:trigger_log).
              with(rule, ticket, :match, conditions_any.last)
          end
        end
      end

      describe 'and no `any` conditions match' do
        before do
          conditions_any.each do |condition|
            Zendesk::Rules::Match.
              expects(:instance_matches_condition?).
              with(ticket, condition, events, run_state).
              returns(false)
          end
        end

        it 'returns mismatch' do
          assert_equal :mismatch, condition_match?
        end

        describe 'and it is evaluated' do
          before { condition_match? }

          before_should 'log the trigger mismatch' do
            Zendesk::Rules::Match.
              expects(:trigger_log).
              with(rule, ticket, :mismatch)
          end
        end
      end
    end

    describe 'when an `all` condition does not match' do
      before do
        Zendesk::Rules::Match.
          stubs(:instance_matches_condition?).
          with(ticket, conditions_all.first, events, run_state).
          returns(true)

        Zendesk::Rules::Match.
          stubs(:instance_matches_condition?).
          with(ticket, conditions_all.last, events, run_state).
          returns(false)
      end

      describe 'and it is immutable' do
        let(:conditions_all) do
          [
            DefinitionItem.new('priority_id', 'is', PriorityType.LOW),
            DefinitionItem.new('via_id', 'is', ViaType.WEB_FORM)
          ]
        end

        it 'returns permanent mismatch' do
          assert_equal :permanent_mismatch, condition_match?
        end

        describe 'and it is evaluated' do
          before { condition_match? }

          before_should 'log a blank trigger execution' do
            Zendesk::Rules::Match.
              expects(:trigger_log).
              with(rule, ticket, :permanent_mismatch, conditions_all.last)
          end
        end
      end

      describe 'and it is not immutable' do
        it 'returns mismatch' do
          assert_equal :mismatch, condition_match?
        end

        describe 'and it is evaluated' do
          before { condition_match? }

          before_should 'log a blank trigger execution' do
            Zendesk::Rules::Match.
              expects(:trigger_log).
              with(rule, ticket, :mismatch, conditions_all.last)
          end
        end
      end
    end

    describe 'when there are no `all` conditions' do
      let(:conditions_all) { [] }

      describe 'and there are no `any` conditions' do
        let(:conditions_any) { [] }

        it 'returns mismatch' do
          assert_equal :match, condition_match?
        end

        describe 'and it is evaluated' do
          before { condition_match? }

          before_should 'log a blank trigger execution' do
            Zendesk::Rules::Match.
              expects(:trigger_log).
              with(rule, ticket, :match, :blank)
          end

          before_should 'not invoke instance_matches_condition' do
            Zendesk::Rules::Match.expects(:instance_matches_condition?).never
          end
        end
      end

      describe 'and an `any` condition matches' do
        before do
          Zendesk::Rules::Match.
            expects(:instance_matches_condition?).
            with(ticket, conditions_any.first, events, run_state).
            returns(false)

          Zendesk::Rules::Match.
            expects(:instance_matches_condition?).
            with(ticket, conditions_any.last, events, run_state).
            returns(true)
        end

        it 'returns match' do
          assert_equal :match, condition_match?
        end

        describe 'and it is evaluated' do
          before { condition_match? }

          before_should 'log the trigger match' do
            Zendesk::Rules::Match.
              expects(:trigger_log).
              with(rule, ticket, :match, conditions_any.last)
          end
        end
      end

      describe 'and no `any` conditions match' do
        before do
          conditions_any.each do |condition|
            Zendesk::Rules::Match.
              expects(:instance_matches_condition?).
              with(ticket, condition, events, run_state).
              returns(false)
          end
        end

        it 'returns mismatch' do
          assert_equal :mismatch, condition_match?
        end

        describe 'and it is evaluated' do
          before { condition_match? }

          before_should 'log the trigger mismatch' do
            Zendesk::Rules::Match.
              expects(:trigger_log).
              with(rule, ticket, :mismatch)
          end
        end
      end
    end
  end

  describe '.instance_matches_condition?' do
    let(:ticket)    { tickets(:minimum_1) }
    let(:events)    { ticket.events }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    let(:match_condition?) { stub('match condition') }

    let(:condition) do
      DefinitionItem.new('priority_id', 'is', PriorityType.LOW)
    end

    before do
      Rule.
        stubs(:match_condition?).
        with(ticket, condition, run_state, events).
        returns(match_condition?)
    end

    describe 'when the condition is memoizable' do
      let(:condition) { DefinitionItem.new('current_tags', 'is', 'tag yo') }

      let!(:instance_matches_condition?) do
        Zendesk::Rules::Match.
          instance_matches_condition?(ticket, condition, events, run_state)
      end

      before_should 'memoize the result' do
        run_state.
          expects(:memoize).
          with(condition.memoization_key(ticket))
      end

      it 'returns whether the condition matches' do
        assert_equal match_condition?, instance_matches_condition?
      end
    end

    describe 'when the condition is not memoizable' do
      let!(:instance_matches_condition?) do
        Zendesk::Rules::Match.
          instance_matches_condition?(ticket, condition, events, run_state)
      end

      before_should 'not memoize the result' do
        run_state.expects(:memoize).never
      end

      before_should 'increment checks on the run state' do
        run_state.expects(:increment_checks).once
      end

      it 'returns whether the condition matches' do
        assert_equal match_condition?, instance_matches_condition?
      end
    end

    describe 'when there is no run state' do
      let(:run_state) { nil }

      let(:instance_matches_condition?) do
        Zendesk::Rules::Match.
          instance_matches_condition?(ticket, condition, events, run_state)
      end

      it 'returns whether the condition matches' do
        assert_equal match_condition?, instance_matches_condition?
      end
    end
  end

  describe '.match?' do
    let(:ticket)    { tickets(:minimum_1) }
    let(:events)    { ticket.events }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }
    let(:rule)      { new_trigger(account: ticket.account) }

    before do
      Zendesk::Rules::Match.
        stubs(:condition_match?).
        with(rule, ticket, events, run_state).
        returns(condition_match)
    end

    describe 'when the condition matches the rule' do
      let(:condition_match) { :match }

      it 'returns true' do
        assert Zendesk::Rules::Match.match?(rule, ticket, events, run_state)
      end
    end

    describe 'when the condition does not match the rule' do
      let(:condition_match) { :mismatch }

      it 'returns false' do
        refute Zendesk::Rules::Match.match?(rule, ticket, events, run_state)
      end
    end
  end

  describe '.trigger_log' do
    let(:rule)      { new_trigger(account: ticket.account) }
    let(:ticket)    { tickets(:minimum_1) }
    let(:status)    { :mismatch }
    let(:condition) do
      DefinitionItem.new('priority_id', 'is', PriorityType.LOW)
    end

    before { Timecop.freeze }

    describe 'when trigger logging is enabled' do
      before do
        ticket.account.stubs(has_trigger_logging?: true)

        Zendesk::Rules::Match.trigger_log(rule, ticket, status, condition)
      end

      before_should 'log the trigger' do
        TRIGGER_LOG.
          expects(:info).
          with(
            [
              "{ account: #{ticket.account_id}",
              "timestamp: #{Time.now.to_json}",
              "trigger: #{rule.id}",
              "ticket_id: #{ticket.id}",
              "nice_id: #{ticket.nice_id}",
              "status: #{status.to_json}",
              "condition: #{condition.to_json}}"
            ].join(', ')
          )
      end
    end

    describe 'when trigger logging is disabled' do
      before do
        ticket.account.stubs(has_trigger_logging?: false)

        Zendesk::Rules::Match.trigger_log(rule, ticket, status, condition)
      end

      before_should 'not log the trigger' do
        TRIGGER_LOG.expects(:info).never
      end
    end
  end

  describe '.match_condition?' do
    let(:ticket) { tickets(:minimum_1) }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    describe 'with a `brand` condition' do
      let(:brand)           { brands(:minimum) }
      let(:condition_value) { brand.id }

      let(:condition) do
        DefinitionItem.new('brand_id', 'is', condition_value)
      end

      before { ticket.stubs(brand_id: brand.id) }

      it 'matches the brand ID in the ticket' do
        assert Rule.match_condition?(ticket, condition, run_state)
      end

      describe 'and the brand ID in the condition is different' do
        let(:condition_value) { brands(:minimum_sdk).id }

        it 'does not match' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the ticket has no brand ID' do
        let(:default_brand)   { brands(:minimum_sdk) }
        let(:condition_value) { default_brand.id }

        before do
          ticket.stubs(brand_id: nil)

          ticket.account.stubs(default_brand_id: condition_value)
        end

        it 'matches the default brand ID' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'with a `satisfaction_score` condition' do
      let(:score) { SatisfactionType.GOOD }

      let(:condition) do
        DefinitionItem.new('satisfaction_score', 'is', score)
      end

      before { ticket.stubs(satisfaction_score: score) }

      describe_with_arturo_setting_enabled :customer_satisfaction do
        it 'matches' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe_with_arturo_setting_disabled :customer_satisfaction do
        it 'does not match' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'with a `satisfaction_probability` condition' do
      describe_with_arturo_setting_enabled(:satisfaction_prediction) do
        let(:probability) { '0.7' }

        let(:condition) do
          DefinitionItem.new(
            'satisfaction_probability',
            'less_than',
            probability
          )
        end

        before { ticket.stubs(satisfaction_probability: 0.6) }

        it 'matches' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end

        describe 'and the ticket does not have a satisfaction probability' do
          let(:probability) { nil }

          before { ticket.stubs(satisfaction_probability: nil) }

          it 'does not match' do
            refute Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end
    end

    describe 'with a `satisfaction_reason_code` condition' do
      let(:account) { ticket.account }
      let(:reason)  { Satisfaction::Reason::OTHER }

      let(:condition) do
        DefinitionItem.new('satisfaction_reason_code', 'is', reason)
      end

      before do
        account.stubs(csat_reason_code_enabled?: true)

        ticket.stubs(satisfaction_reason_code: reason)
      end

      it 'matches' do
        assert Rule.match_condition?(ticket, condition, run_state)
      end

      describe 'and `csat_reason_code` is not enabled' do
        before { account.stubs(csat_reason_code_enabled?: false) }

        it 'does not match' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'with a ticket form condition' do
      let(:ticket_form)     { ticket_forms(:non_default_ticket_form) }
      let(:condition_value) { ticket_form.id }

      let(:condition) do
        DefinitionItem.new('ticket_form_id', 'is', condition_value)
      end

      before { ticket.stubs(ticket_form_id: ticket_form.id) }

      it 'matches the ticket form ID in the ticket' do
        assert Rule.match_condition?(ticket, condition, run_state)
      end

      describe 'and the ticket form ID in the condition is different' do
        let(:condition_value) { ticket_forms(:minimum_ticket_form).id }

        it 'does not match' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the ticket has no ticket form ID' do
        let(:default_ticket_form) { ticket_forms(:default_ticket_form) }
        let(:condition_value)     { default_ticket_form.id }

        before do
          ticket.stubs(ticket_form_id: nil)

          ticket.account.ticket_forms.stubs(default: [default_ticket_form])
        end

        it 'matches the default ticket form ID' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'with a ticket due date condition' do
      let(:condition) do
        DefinitionItem.new('ticket_due_date', 'is', condition_value)
      end

      describe 'and the ticket has a due date' do
        before do
          ticket.stubs(due_date: Time.zone.parse('2016-02-10 15:30:45'))
        end

        describe 'and the condition value is `true`' do
          let(:condition_value) { 'true' }

          it 'matches' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end
        end

        describe 'and the condition value is `false`' do
          let(:condition_value) { 'false' }

          it 'does not match' do
            refute Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end

      describe 'and the ticket does not have a due date' do
        before { ticket.stubs(due_date: nil) }

        describe 'and the condition value is `true`' do
          let(:condition_value) { 'true' }

          it 'does not match' do
            refute Rule.match_condition?(ticket, condition, run_state)
          end
        end

        describe 'and the condition value is `false`' do
          let(:condition_value) { 'false' }

          it 'matches' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end
    end

    describe 'with a ticket privacy condition' do
      let(:condition) do
        DefinitionItem.new('ticket_is_public', 'is', condition_value)
      end

      describe 'and the ticket is public' do
        before { ticket.stubs(is_public?: true) }

        describe 'and the condition value is `public`' do
          let(:condition_value) { 'public' }

          it 'matches' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end
        end

        describe 'and the condition value is `private`' do
          let(:condition_value) { 'private' }

          it 'does not match' do
            refute Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end

      describe 'and the ticket is private' do
        before { ticket.stubs(is_public?: false) }

        describe 'and the condition value is `public`' do
          let(:condition_value) { 'public' }

          it 'does not match' do
            refute Rule.match_condition?(ticket, condition, run_state)
          end
        end

        describe 'and the condition value is `private`' do
          let(:condition_value) { 'private' }

          it 'matches' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end
    end

    describe 'with a schedule condition' do
      let(:account) { ticket.account }

      let(:schedule) do
        account.schedules.create(name: 'Schedule', time_zone: account.time_zone)
      end

      describe 'and the condition is `schedule_id`' do
        describe 'and the account does not have the `multiple schedules` feature' do
          before { account.stubs(has_multiple_schedules?: false) }

          it 'returns false' do
            refute Rule.match_condition?(
              ticket,
              DefinitionItem.new('schedule_id', 'is', nil),
              run_state
            )
          end
        end

        describe 'and the account has the `multiple schedules` feature' do
          before { account.stubs(has_multiple_schedules?: true) }

          describe 'and the ticket does not have an assigned schedule' do
            describe "and using the 'is' operator" do
              it 'does not match a schedule not assigned to the ticket' do
                refute Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('schedule_id', 'is', schedule.id),
                  run_state
                )
              end

              it 'matches nil' do
                assert Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('schedule_id', 'is', nil),
                  run_state
                )
              end
            end

            describe 'and using the `is_not` operator' do
              it 'matches a schedule not assigned to the ticket' do
                assert Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('schedule_id', 'is_not', schedule.id),
                  run_state
                )
              end

              it 'does not match nil' do
                refute Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('schedule_id', 'is_not', nil),
                  run_state
                )
              end
            end
          end

          describe 'and the ticket has an assigned schedule' do
            before do
              ticket.
                create_ticket_schedule(account: account, schedule: schedule)
            end

            describe "and using the 'is' operator" do
              it "matches the ticket's schedule" do
                assert Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('schedule_id', 'is', schedule.id),
                  run_state
                )
              end

              it 'does not match a different schedule' do
                refute Rule.match_condition?(
                  ticket,
                  DefinitionItem.new(
                    'schedule_id',
                    'is',
                    account.schedules.create(
                      name:      'Different schedule',
                      time_zone: account.time_zone
                    ).id
                  ),
                  run_state
                )
              end

              it 'does not match nil' do
                refute Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('schedule_id', 'is', nil),
                  run_state
                )
              end
            end

            describe "and using the 'is_not' operator" do
              it "does not match the ticket's schedule" do
                refute Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('schedule_id', 'is_not', schedule.id),
                  run_state
                )
              end

              it 'matches a different schedule' do
                assert Rule.match_condition?(
                  ticket,
                  DefinitionItem.new(
                    'schedule_id',
                    'is_not',
                    account.schedules.create(
                      name:      'Different schedule',
                      time_zone: account.time_zone
                    ).id
                  ),
                  run_state
                )
              end

              it 'matches nil' do
                assert Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('schedule_id', 'is_not', nil),
                  run_state
                )
              end
            end
          end
        end
      end

      describe 'and the condition is `within_schedule`' do
        before do
          Time.zone = schedule.time_zone

          add_schedule_intervals(schedule)

          Timecop.freeze(Time.zone.local(2006, 1, 4, 16, 0))
        end

        describe 'and the `multiple schedules` feature is not enabled' do
          before do
            Account.any_instance.stubs(has_multiple_schedules?: false)
          end

          it 'does not match' do
            refute Rule.match_condition?(
              ticket,
              DefinitionItem.new('within_schedule', nil, schedule.id),
              run_state
            )
          end
        end

        describe 'and the `multiple schedules` feature is enabled' do
          before do
            Account.any_instance.stubs(has_multiple_schedules?: true)
          end

          describe 'and the schedule does not exist' do
            it 'does not match' do
              refute Rule.match_condition?(
                ticket,
                DefinitionItem.new('within_schedule', nil, schedule.id.succ),
                run_state
              )
            end
          end

          describe 'and the schedule is soft deleted' do
            before { schedule.soft_delete }

            it 'does not match' do
              refute Rule.match_condition?(
                ticket,
                DefinitionItem.new('within_schedule', nil, schedule.id),
                run_state
              )
            end
          end

          describe 'and the schedule exists' do
            describe "and the current time is in the schedule's hours" do
              it 'matches' do
                assert Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('within_schedule', nil, schedule.id),
                  run_state
                )
              end
            end

            describe "and the current time is not in the schedule's hours" do
              before do
                Timecop.freeze(Time.zone.local(2006, 1, 4, 20, 0))
              end

              it 'does not match' do
                refute Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('within_schedule', nil, schedule.id),
                  run_state
                )
              end
            end
          end
        end
      end
    end

    describe 'with a via_id condition' do
      before do
        @account = accounts(:minimum)
      end

      describe 'via_id condition is not AnyChannel' do
        before do
          @ticket = stub(
            'ticket',
            via_id:       Zendesk::Types::ViaType.WEB_FORM,
            current_user: mock('user'),
            account:      @account
          )
        end

        describe 'it matches the via_id in the ticket' do
          it 'returns true' do
            assert(
              Rule.match_condition?(
                @ticket,
                DefinitionItem.new(
                  'via_id',
                  'is',
                  Zendesk::Types::ViaType.WEB_FORM
                ),
                run_state
              )
            )
          end
        end

        describe 'does not match the via_id in the ticket' do
          it 'returns false' do
            refute(
              Rule.match_condition?(
                @ticket,
                DefinitionItem.new(
                  'via_id',
                  'is',
                  Zendesk::Types::ViaType.WEB_SERVICE
                ),
                run_state
              )
            )
          end
        end
      end

      describe 'via_id condition is AnyChannel' do
        before do
          @ticket = stub(
            'ticket',
            via_id:           Zendesk::Types::ViaType.ANY_CHANNEL,
            via_reference_id: 123,
            current_user:     mock('user'),
            account:          @account,
            account_id:       @account.id
          )
        end

        describe 'value is not in `any_channel` format' do
          it 'returns match' do
            assert(
              Rule.match_condition?(
                @ticket,
                DefinitionItem.new(
                  'via_id',
                  'is',
                  Zendesk::Types::ViaType.ANY_CHANNEL
                ),
                run_state
              )
            )
          end
        end

        describe 'value is in `any_channel` format' do
          describe 'no matching IntegrationServiceInstances found' do
            it 'returns false' do
              refute(
                Rule.match_condition?(
                  @ticket,
                  DefinitionItem.new('via_id', 'is', 'any_channel:0'),
                  run_state
                )
              )
            end
          end

          describe 'IntegrationServiceInstances found' do
            before do
              isi_ids = []
              2.times do |isi_index|
                isi_ids << ::Channels::AnyChannel::IntegrationServiceInstance.
                  new(
                    registered_integration_service_id: 234,
                    name: "isi #{isi_index}",
                    metadata: '{}',
                    state: ::Channels::AnyChannel::IntegrationServiceInstance::State::ACTIVE
                  ).tap do |isi|
                    isi.account = @account
                    isi.save!
                  end.id
              end

              @ticket = stub(
                'ticket',
                via_id:           Zendesk::Types::ViaType.ANY_CHANNEL,
                via_reference_id: isi_ids[1],
                current_user:     mock('user'),
                account:          @account,
                account_id:       @account.id
              )
            end

            describe 'an IntegrationServiceInstances matches' do
              it 'returns true' do
                assert(
                  Rule.match_condition?(
                    @ticket,
                    DefinitionItem.new('via_id', 'is', 'any_channel:234'),
                    run_state
                  )
                )
              end
            end

            describe 'no IntegrationServiceInstances matches' do
              it 'returns false' do
                refute(
                  Rule.match_condition?(
                    @ticket,
                    DefinitionItem.new('via_id', 'is', 'any_channel:345'),
                    run_state
                  )
                )
              end
            end
          end
        end
      end
    end

    describe 'with a `via_subtype_id` condition' do
      let(:account) { accounts(:minimum) }

      let(:any_channel_ticket) do
        stub(
          'ticket',
          via_id:           Zendesk::Types::ViaType.ANY_CHANNEL,
          via_reference_id: 123,
          current_user:     mock('user'),
          account:          account
        )
      end

      let(:twitter_ticket) do
        stub(
          'ticket',
          via_id:           Zendesk::Types::ViaType.TWITTER,
          via_reference_id: 123,
          current_user:     mock('user'),
          account:          account
        )
      end

      let(:twitter_dm_ticket) do
        stub(
          'ticket',
          via_id:           Zendesk::Types::ViaType.TWITTER_DM,
          via_reference_id: 123,
          current_user:     mock('user'),
          account:          account
        )
      end

      let(:twitter_favorite_ticket) do
        stub(
          'ticket',
          via_id:           Zendesk::Types::ViaType.TWITTER_FAVORITE,
          via_reference_id: 123,
          current_user:     mock('user'),
          account:          account
        )
      end

      let(:facebook_post_ticket) do
        stub(
          'ticket',
          via_id:           Zendesk::Types::ViaType.FACEBOOK_POST,
          via_reference_id: 123,
          current_user:     mock('user'),
          account:          account
        )
      end

      let(:facebook_message_ticket) do
        stub(
          'ticket',
          via_id:           Zendesk::Types::ViaType.FACEBOOK_MESSAGE,
          via_reference_id: 123,
          current_user:     mock('user'),
          account:          account
        )
      end

      Struct.new(
        'ViaSubtypeTest',
        :ticket_var,
        :via_type,
        :via_reference_id,
        :operator,
        :expected
      )
      [
        Struct::ViaSubtypeTest.new(:any_channel_ticket, Zendesk::Types::ViaType.ANY_CHANNEL, 123, 'is', true),
        Struct::ViaSubtypeTest.new(:any_channel_ticket, Zendesk::Types::ViaType.ANY_CHANNEL, 234, 'is', false),
        Struct::ViaSubtypeTest.new(:any_channel_ticket, Zendesk::Types::ViaType.WEB_FORM, 123, 'is', false),
        Struct::ViaSubtypeTest.new(:any_channel_ticket, Zendesk::Types::ViaType.WEB_FORM, 234, 'is', false),
        Struct::ViaSubtypeTest.new(:any_channel_ticket, Zendesk::Types::ViaType.ANY_CHANNEL, 123, 'is_not', false),
        Struct::ViaSubtypeTest.new(:any_channel_ticket, Zendesk::Types::ViaType.ANY_CHANNEL, 234, 'is_not', true),
        Struct::ViaSubtypeTest.new(:any_channel_ticket, Zendesk::Types::ViaType.WEB_FORM, 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:any_channel_ticket, Zendesk::Types::ViaType.WEB_FORM, 234, 'is_not', true),
        Struct::ViaSubtypeTest.new(:twitter_ticket, 'TWITTER', 123, 'is', true),
        Struct::ViaSubtypeTest.new(:twitter_ticket, 'TWITTER', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:twitter_ticket, 'WEB_FORM', 123, 'is', false),
        Struct::ViaSubtypeTest.new(:twitter_ticket, 'WEB_FORM', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:twitter_ticket, 'TWITTER', 123, 'is_not', false),
        Struct::ViaSubtypeTest.new(:twitter_ticket, 'TWITTER', 234, 'is_not', true),
        Struct::ViaSubtypeTest.new(:twitter_ticket, 'WEB_FORM', 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:twitter_ticket, 'WEB_FORM', 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:twitter_dm_ticket, 'TWITTER', 123, 'is', true),
        Struct::ViaSubtypeTest.new(:twitter_dm_ticket, 'TWITTER', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:twitter_dm_ticket, 'WEB_FORM', 123, 'is', false),
        Struct::ViaSubtypeTest.new(:twitter_dm_ticket, 'WEB_FORM', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:twitter_dm_ticket, 'TWITTER', 123, 'is_not', false),
        Struct::ViaSubtypeTest.new(:twitter_dm_ticket, 'TWITTER', 234, 'is_not', true),
        Struct::ViaSubtypeTest.new(:twitter_dm_ticket, 'WEB_FORM', 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:twitter_dm_ticket, 'WEB_FORM', 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:twitter_favorite_ticket, 'TWITTER', 123, 'is', true),
        Struct::ViaSubtypeTest.new(:twitter_favorite_ticket, 'TWITTER', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:twitter_favorite_ticket, 'WEB_FORM', 123, 'is', false),
        Struct::ViaSubtypeTest.new(:twitter_favorite_ticket, 'WEB_FORM', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:twitter_favorite_ticket, 'TWITTER', 123, 'is_not', false),
        Struct::ViaSubtypeTest.new(:twitter_favorite_ticket, 'TWITTER', 234, 'is_not', true),
        Struct::ViaSubtypeTest.new(:twitter_favorite_ticket, 'WEB_FORM', 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:twitter_favorite_ticket, 'WEB_FORM', 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:facebook_post_ticket, 'FACEBOOK', 123, 'is', true),
        Struct::ViaSubtypeTest.new(:facebook_post_ticket, 'FACEBOOK', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:facebook_post_ticket, 'WEB_FORM', 123, 'is', false),
        Struct::ViaSubtypeTest.new(:facebook_post_ticket, 'WEB_FORM', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:facebook_post_ticket, 'FACEBOOK', 123, 'is_not', false),
        Struct::ViaSubtypeTest.new(:facebook_post_ticket, 'FACEBOOK', 234, 'is_not', true),
        Struct::ViaSubtypeTest.new(:facebook_post_ticket, 'WEB_FORM', 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:facebook_post_ticket, 'WEB_FORM', 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:facebook_message_ticket, 'FACEBOOK', 123, 'is', true),
        Struct::ViaSubtypeTest.new(:facebook_message_ticket, 'FACEBOOK', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:facebook_message_ticket, 'WEB_FORM', 123, 'is', false),
        Struct::ViaSubtypeTest.new(:facebook_message_ticket, 'WEB_FORM', 234, 'is', false),
        Struct::ViaSubtypeTest.new(:facebook_message_ticket, 'FACEBOOK', 123, 'is_not', false),
        Struct::ViaSubtypeTest.new(:facebook_message_ticket, 'FACEBOOK', 234, 'is_not', true),
        Struct::ViaSubtypeTest.new(:facebook_message_ticket, 'WEB_FORM', 123, 'is_not', true),
        Struct::ViaSubtypeTest.new(:facebook_message_ticket, 'WEB_FORM', 123, 'is_not', true)
      ].each do |test_case|
        describe "'#{test_case.operator}' match for #{test_case.ticket_var}" do
          describe "it #{test_case.expected ? 'matches' : 'does not match'} via_type #{test_case.via_type} and via_reference_id #{test_case.via_reference_id}" do
            it "returns #{test_case.expected}" do
              ticket = send(test_case.ticket_var)
              assert_equal(
                test_case.expected,
                Rule.match_condition?(
                  ticket,
                  DefinitionItem.new(
                    'via_subtype_id',
                    test_case.operator,
                    "#{test_case.via_type}:#{test_case.via_reference_id}"
                  ),
                  run_state
                )
              )
            end
          end
        end
      end
    end

    describe 'with instrumentation' do
      let(:brand)           { brands(:minimum) }
      let(:condition_value) { brand.id }
      let(:condition) do
        DefinitionItem.new('brand_id', 'is', condition_value)
      end

      before do
        Benchmark.stubs(realtime: 0.05)
        ticket.stubs(brand_id: brand.id)
      end

      describe_with_arturo_enabled :instrument_trigger_matching do
        before { Rule.match_condition?(ticket, condition, run_state) }

        before_should 'call #increment_match_by_source on the run state' do
          Zendesk::Rules::Trigger::RunState.any_instance.expects(:increment_match_by_source).with('brand_id', 0.05)
        end
      end

      describe_with_arturo_disabled :instrument_trigger_matching do
        before { Rule.match_condition?(ticket, condition, run_state) }

        before_should 'not call #increment_match_by_source on the run state' do
          Zendesk::Rules::Trigger::RunState.any_instance.expects(:increment_match_by_source).never
        end
      end
    end
  end

  describe 'requester_time_zone' do
    let(:ticket) { tickets(:minimum_1) }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    before { ticket.requester.stubs(:time_zone).returns('Hawaii') }

    describe 'when using the `is` operator' do
      it 'matches the same time zone' do
        assert Rule.match_condition?(
          ticket,
          DefinitionItem.new('requester_time_zone', 'is', 'Hawaii'),
          run_state
        )
      end

      it 'does not match a different time zone' do
        refute Rule.match_condition?(
          ticket,
          DefinitionItem.new('requester_time_zone', 'is', 'Copenhagen'),
          run_state
        )
      end
    end

    describe 'when using the `is not` operator' do
      it 'does not match the same time zone' do
        refute Rule.match_condition?(
          ticket,
          DefinitionItem.new('requester_time_zone', 'is_not', 'Hawaii'),
          run_state
        )
      end

      it 'matches a different time zone' do
        assert Rule.match_condition?(
          ticket,
          DefinitionItem.new('requester_time_zone', 'is_not', 'Copenhagen'),
          run_state
        )
      end
    end
  end

  describe 'with the `collaboration_thread` condition' do
    let(:ticket)        { tickets(:minimum_1) }
    let(:agent)         { users(:minimum_agent) }
    let(:thread_events) { %w[created closed reopened reply] }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    describe 'CollabThreadCreated event' do
      before do
        event = CollabThreadCreated.new
        ticket.will_be_saved_by(agent, via_id: ViaType.WEB_SERVICE)
        ticket.audit.events << event
      end

      describe 'with the `is` operator' do
        it 'matches created' do
          assert Rule.match_condition?(
            ticket,
            DefinitionItem.new('collaboration_thread', 'is', 'created'),
            run_state
          )
        end

        it 'does not match other events' do
          (thread_events - ['created']).each do |event|
            refute Rule.match_condition?(
              ticket,
              DefinitionItem.new('collaboration_thread', 'is', event),
              run_state
            )
          end
        end
      end
    end

    describe 'CollabThreadClosed event' do
      before do
        event = CollabThreadClosed.new
        ticket.will_be_saved_by(agent, via_id: ViaType.WEB_SERVICE)
        ticket.audit.events << event
      end

      describe 'with the `is` operator' do
        it 'matches closed' do
          assert Rule.match_condition?(
            ticket,
            DefinitionItem.new('collaboration_thread', 'is', 'closed'),
            run_state
          )
        end

        it 'does not match other events' do
          (thread_events - ['closed']).each do |event|
            refute Rule.match_condition?(
              ticket,
              DefinitionItem.new('collaboration_thread', 'is', event),
              run_state
            )
          end
        end
      end
    end

    describe 'CollabThreadReopened event' do
      before do
        event = CollabThreadReopened.new
        ticket.will_be_saved_by(agent, via_id: ViaType.WEB_SERVICE)
        ticket.audit.events << event
      end

      describe 'with the `is` operator' do
        it 'matches reopened' do
          assert Rule.match_condition?(
            ticket,
            DefinitionItem.new('collaboration_thread', 'is', 'reopened'),
            run_state
          )
        end

        it 'does not match other events' do
          (thread_events - ['reopened']).each do |event|
            refute Rule.match_condition?(
              ticket,
              DefinitionItem.new('collaboration_thread', 'is', event),
              run_state
            )
          end
        end
      end
    end

    describe 'CollabThreadReply event' do
      before do
        event = CollabThreadReply.new
        ticket.will_be_saved_by(agent, via_id: ViaType.WEB_SERVICE)
        ticket.audit.events << event
      end

      describe 'with the `is` operator' do
        it 'matches reply' do
          assert Rule.match_condition?(
            ticket,
            DefinitionItem.new('collaboration_thread', 'is', 'reply'),
            run_state
          )
        end

        it 'does not match other events' do
          (thread_events - ['reply']).each do |event|
            refute Rule.match_condition?(
              ticket,
              DefinitionItem.new('collaboration_thread', 'is', event),
              run_state
            )
          end
        end
      end
    end
  end

  describe 'with a `role` condition' do
    let(:ticket) { tickets(:minimum_1) }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    describe 'current user is system user and not using "is_not" operator' do
      before do
        ticket.current_user = stub('current user', is_system_user?: true)
      end

      it 'returns false' do
        refute Rule.match_condition?(
          ticket,
          DefinitionItem.new('role', 'some_operator', 'some_value'),
          run_state
        )
      end
    end

    [true, false].each do |arturo_state|
      describe ":agent_as_end_user Arturo #{arturo_state ? 'on' : 'off'}" do
        before do
          Arturo.set_feature!(:agent_as_end_user, arturo_state)
        end

        current_user_roles = [Role::END_USER, Role::AGENT]
        current_user_roles.each do |current_role|
          other_roles = current_user_roles - [current_role]
          current_role_name = current_role.name.downcase.snakecase

          describe "when current user is #{current_role}" do
            before do
              ticket.current_user = stub('current user')
              ticket.current_user.stubs(
                is_agent?:    current_role == Role::AGENT,
                is_end_user?: current_role == Role::END_USER,
                is_admin?:    false,
                is_system_user?:    false,
                id: ticket.submitter_id
              )
            end

            describe 'and using the `is` operator' do
              it 'matches the same requester role' do
                assert Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('role', 'is', current_role_name),
                  run_state
                )
              end

              other_roles.each do |other_role|
                other_role_name = other_role.name.downcase.snakecase

                it 'does not match a different role' do
                  refute Rule.match_condition?(
                    ticket,
                    DefinitionItem.new('role', 'is', other_role_name),
                    run_state
                  )
                end
              end
            end

            describe 'and using the `is_not` operator' do
              other_roles.each do |other_role|
                other_role_name = other_role.name.downcase.snakecase

                it 'matches a different role' do
                  assert Rule.match_condition?(
                    ticket,
                    DefinitionItem.new('role', 'is_not', other_role_name),
                    run_state
                  )
                end
              end

              it 'does not match the same role' do
                refute Rule.match_condition?(
                  ticket,
                  DefinitionItem.new('role', 'is_not', current_role_name),
                  run_state
                )
              end
            end
          end
        end

        describe 'matching current user ID' do
          before do
            ticket.current_user = stub('current user')
            ticket.current_user.stubs(
              is_agent?:    false,
              is_end_user?: true,
              is_admin?:    false,
              is_system_user?:    false,
              id: ticket.submitter_id
            )
          end

          it 'returns true or false depending on matching' do
            submitter_id = ticket.submitter_id.to_s
            other_user_id = (ticket.submitter_id + 1).to_s

            assert Rule.match_condition?(
              ticket,
              DefinitionItem.new('role', 'is', submitter_id),
              run_state
            )

            refute Rule.match_condition?(
              ticket,
              DefinitionItem.new('role', 'is_not', submitter_id),
              run_state
            )

            refute Rule.match_condition?(
              ticket,
              DefinitionItem.new('role', 'is', other_user_id),
              run_state
            )

            assert Rule.match_condition?(
              ticket,
              DefinitionItem.new('role', 'is_not', other_user_id),
              run_state
            )
          end
        end
      end
    end

    describe 'helpcenter ticket created by agent' do
      [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
        describe "via_id is #{via_id}" do
          before do
            ticket.current_user = stub('current user')
            ticket.current_user.stubs(
              is_agent?:    true,
              is_end_user?: false,
              is_admin?:    false,
              is_system_user?:    false,
              id: ticket.submitter_id
            )
            ticket.via_id = via_id
            ticket.via_reference_id = ViaType.HELPCENTER
          end

          describe ':agent_as_end_user Arturo enabled' do
            before do
              Arturo.enable_feature!(:agent_as_end_user)
            end

            it 'treats agent as an end user' do
              assert Rule.match_condition?(
                ticket,
                DefinitionItem.new('role', 'is', 'end_user'),
                run_state
              )

              refute Rule.match_condition?(
                ticket,
                DefinitionItem.new('role', 'is', 'agent'),
                run_state
              )

              refute Rule.match_condition?(
                ticket,
                DefinitionItem.new('role', 'is_not', 'end_user'),
                run_state
              )

              assert Rule.match_condition?(
                ticket,
                DefinitionItem.new('role', 'is_not', 'agent'),
                run_state
              )
            end
          end

          describe ':agent_as_end_user Arturo disabled' do
            before do
              Arturo.disable_feature!(:agent_as_end_user)
            end

            it 'does not treat agent as an end user' do
              refute Rule.match_condition?(
                ticket,
                DefinitionItem.new('role', 'is', 'end_user'),
                run_state
              )

              assert Rule.match_condition?(
                ticket,
                DefinitionItem.new('role', 'is', 'agent'),
                run_state
              )

              assert Rule.match_condition?(
                ticket,
                DefinitionItem.new('role', 'is_not', 'end_user'),
                run_state
              )

              refute Rule.match_condition?(
                ticket,
                DefinitionItem.new('role', 'is_not', 'agent'),
                run_state
              )
            end
          end
        end
      end
    end
  end

  describe 'with a `requester_role` condition' do
    let(:ticket) { tickets(:minimum_1) }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    requester_roles = [Role::END_USER.id, Role::ADMIN.id, Role::AGENT.id]

    requester_roles.each do |current_role|
      other_roles = requester_roles - [current_role]

      describe "when requester is #{current_role}" do
        before do
          ticket.requester.stubs(
            is_agent?:    current_role == Role::AGENT.id,
            is_end_user?: current_role == Role::END_USER.id,
            is_admin?:    current_role == Role::ADMIN.id
          )
        end

        describe 'and using the `is` operator' do
          it 'matches the same requester role' do
            assert Rule.match_condition?(
              ticket,
              DefinitionItem.new('requester_role', 'is', current_role),
              run_state
            )
          end

          other_roles.each do |other_role|
            it 'does not match a different requester_role' do
              refute Rule.match_condition?(
                ticket,
                DefinitionItem.new('requester_role', 'is', other_role),
                run_state
              )
            end
          end
        end

        describe 'and using the `is_not` operator' do
          other_roles.each do |other_role|
            it 'matches a different requester role' do
              assert Rule.match_condition?(
                ticket,
                DefinitionItem.new('requester_role', 'is_not', other_role),
                run_state
              )
            end
          end

          it 'does not match the same requester role' do
            refute Rule.match_condition?(
              ticket,
              DefinitionItem.new('requester_role', 'is_not', current_role),
              run_state
            )
          end
        end
      end
    end
  end

  describe 'with an `in_business_hours` condition' do
    let(:ticket) { tickets(:minimum_1) }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    let(:definition_item) do
      DefinitionItem.new('in_business_hours', 'is', business_hours_value)
    end

    before do
      Account.any_instance.stubs(business_hours_active?: true)

      ticket.create_ticket_schedule(
        account:    ticket.account,
        schedule:   ticket.account.schedules.create(
          name:      'Schedule',
          time_zone: 'UTC'
        ).tap do |schedule|
          schedule.workweek.configure_as_default
        end
      )
    end

    describe 'when the value is set to true' do
      let(:business_hours_value) { 'true' }

      describe 'and the current time is in business hours' do
        before { Timecop.freeze(Time.utc(2006, 1, 2, 9)) }

        it 'matches' do
          assert Rule.match_condition?(ticket, definition_item, run_state)
        end
      end

      describe 'and the current time is not in business hours' do
        before { Timecop.freeze(Time.utc(2006, 1, 2, 8)) }

        it 'does not match' do
          refute Rule.match_condition?(ticket, definition_item, run_state)
        end
      end
    end

    describe 'when the value is set to false' do
      let(:business_hours_value) { 'false' }

      describe 'and the current time is in business hours' do
        before { Timecop.freeze(Time.utc(2006, 1, 2, 9)) }

        it 'does not match' do
          refute Rule.match_condition?(ticket, definition_item, run_state)
        end
      end

      describe 'and the current time is not in business hours' do
        before { Timecop.freeze(Time.utc(2006, 1, 2, 8)) }

        it 'matches' do
          assert Rule.match_condition?(ticket, definition_item, run_state)
        end
      end
    end
  end

  describe 'with an `on_holiday` condition' do
    let(:ticket) { tickets(:minimum_1) }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    let(:definition_item) do
      DefinitionItem.new('on_holiday', 'is', on_holiday_value)
    end

    before do
      Account.any_instance.stubs(business_hours_active?: true)

      ticket.create_ticket_schedule(
        account:    ticket.account,
        schedule:   ticket.account.schedules.create(
          name:      'Schedule',
          time_zone: 'UTC'
        ).tap do |schedule|
          schedule.workweek.configure_as_default

          schedule.holiday_calendar.add_holiday(
            name:       'Holiday',
            start_date: '2006-01-26',
            end_date:   '2006-01-26'
          )
        end
      )
    end

    describe 'when the value is set to true' do
      let(:on_holiday_value) { 'true' }

      describe 'and the current time is on a holiday' do
        before { Timecop.freeze(Time.utc(2006, 1, 26)) }

        it 'matches' do
          assert Rule.match_condition?(ticket, definition_item, run_state)
        end
      end

      describe 'and the current time is not on a holiday' do
        before { Timecop.freeze(Time.utc(2006, 1, 27)) }

        it 'does not match' do
          refute Rule.match_condition?(ticket, definition_item, run_state)
        end
      end
    end

    describe 'when the value is set to false' do
      let(:on_holiday_value) { 'false' }

      describe 'and the current time is on a holiday' do
        before { Timecop.freeze(Time.utc(2006, 1, 26)) }

        it 'does not match' do
          refute Rule.match_condition?(ticket, definition_item, run_state)
        end
      end

      describe 'and the current time is not on a holiday' do
        before { Timecop.freeze(Time.utc(2006, 1, 27)) }

        it 'matches' do
          assert Rule.match_condition?(ticket, definition_item, run_state)
        end
      end
    end
  end

  describe 'with a `subject_includes_word` condition' do
    let(:ticket)   { tickets(:minimum_1) }
    let(:operator) { 'is' }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    let(:condition) do
      DefinitionItem.new('subject_includes_word', operator, condition_value)
    end

    before { ticket.stubs(subject: subject) }

    describe 'when using the `is` operator' do
      let(:operator) { 'is' }
      let(:subject)  { 'foo' }

      describe 'and the subject contains the condition value' do
        let(:condition_value) { subject }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the subject does not contain the condition value' do
        let(:condition_value) { 'bar' }

        it 'returns false' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when using the `is_not` operator' do
      let(:operator) { 'is_not' }
      let(:subject)  { 'foo' }

      describe 'and the subject contains the condition value' do
        let(:condition_value) { subject }

        it 'returns false' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the subject does not contain the condition value' do
        let(:condition_value) { 'bar' }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when using the `includes` operator' do
      let(:operator) { 'includes' }
      let(:subject)  { 'foo bar' }

      describe 'and the subject includes any part of the condition value' do
        %w[foo bar].each do |word|
          let(:condition_value) { "#{word} other words" }

          it 'returns true' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end

      describe 'and the subject does not include any of the condition value' do
        let(:condition_value) { 'baz other words' }

        it 'returns false' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when using the `not_includes` operator' do
      let(:operator) { 'not_includes' }
      let(:subject)  { 'foo bar' }

      describe 'and the subject includes any part of the condition value' do
        %w[foo bar].each do |word|
          let(:condition_value) { "#{word} other words" }

          it 'returns false' do
            refute Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end

      describe 'and the subject does not include any of the condition value' do
        let(:condition_value) { 'baz other words' }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when using an invalid operator' do
      let(:condition_value) { 'body' }
      let(:operator)        { 'is_awesome' }
      let(:subject)         { 'Body' }

      describe 'and the subject includes any part of the condition value' do
        let(:condition_value) { 'body other words' }

        it 'falls back to the `includes` behavior' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the subject does not include any of the condition value' do
        let(:condition_value) { 'baz other words' }

        it 'falls back to the `includes` behavior' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when the ticket subject is blank' do
      let(:condition_value) { 'foo' }
      let(:subject)         { nil }

      describe 'and using the `is` operator' do
        let(:operator) { 'is' }

        it 'returns false' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'when using the `is_not` operator' do
        let(:operator) { 'is_not' }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'when using the `includes` operator' do
        let(:operator) { 'includes' }

        it 'returns false' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'when using the `not_includes` operator' do
        let(:operator) { 'not_includes' }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when the subject includes special characters' do
      let(:condition_value) { 'срочно' }
      let(:subject)         { 'Срочно' }

      it 'matches `Срочно`' do
        assert Rule.match_condition?(ticket, condition, run_state)
      end
    end
  end

  describe 'with a `comment_includes_word` condition' do
    let(:ticket)   { tickets(:minimum_1) }
    let(:operator) { 'is' }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    let(:condition) do
      DefinitionItem.new('comment_includes_word', operator, condition_value)
    end

    let(:comment) do
      Comment.new(account: ticket.account, ticket: ticket).tap do |comment|
        comment.send(:value=, body)
      end
    end

    before { ticket.stubs(comment: comment) }

    describe 'when using the `is` operator' do
      let(:operator) { 'is' }
      let(:body)     { 'foo' }

      describe 'and the comment contains the condition value' do
        let(:condition_value) { body }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the comment does not contain the condition value' do
        let(:condition_value) { 'bar' }

        it 'returns false' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when using the `is_not` operator' do
      let(:operator) { 'is_not' }
      let(:body)     { 'foo' }

      describe 'and the comment contains the condition value' do
        let(:condition_value) { body }

        it 'returns false' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the comment does not contain the condition value' do
        let(:condition_value) { 'bar' }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when using the `includes` operator' do
      let(:operator) { 'includes' }

      describe 'and the comment includes any part of the condition value' do
        let(:body) { 'foo bar' }

        %w[foo bar].each do |word|
          let(:condition_value) { "#{word} other words" }

          it 'returns true' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end

      describe 'and the comment does not include any of the condition value' do
        let(:condition_value) { 'foo other words' }
        let(:body) { 'fooé bar' }

        describe_with_arturo_enabled :rule_match_non_ascii_characters do
          it 'returns false' do
            refute Rule.match_condition?(ticket, condition, run_state)
          end
        end

        describe_with_arturo_disabled :rule_match_non_ascii_characters do
          it 'returns true' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end
    end

    describe 'when using the `not_includes` operator' do
      let(:operator) { 'not_includes' }
      let(:body)     { 'foo bar' }

      describe 'and the comment includes any part of the condition value' do
        %w[foo bar].each do |word|
          let(:condition_value) { "#{word} other words" }

          it 'returns false' do
            refute Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end

      describe 'and the comment does not include any of the condition value' do
        let(:condition_value) { 'baz other words' }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when using an invalid operator' do
      let(:body)            { 'Body' }
      let(:condition_value) { 'body' }
      let(:operator)        { 'is_awesome' }

      describe 'and the comment includes any part of the condition value' do
        let(:condition_value) { 'body other words' }

        it 'falls back to the `includes` behavior' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the comment does not include any of the condition value' do
        let(:condition_value) { 'baz other words' }

        it 'falls back to the `includes` behavior' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when there is no comment' do
      let(:comment)         { nil }
      let(:condition_value) { 'test' }

      describe 'and the operator is negating' do
        let(:operator) { 'is_not' }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the operator is not negating' do
        let(:operator) { 'is' }

        it 'returns false' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when the comment body includes markdown' do
      let(:body)            { 'foo_bar **foobar** _lol_ m*a*s*h' }
      let(:condition_value) { body }

      describe 'and it is a rich comment' do
        before { comment.stubs(rich?: true) }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and it is a basic comment' do
        before { comment.stubs(rich?: false) }

        it 'returns true' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when the comment body has html comments' do
      let(:body) do
        '<div class=\"zd-comment\">\n<style><!--priority--></style>\n' \
          '<div>Body <!--priority-->text</div>\n</div>'
      end

      let(:condition_value) { 'priority' }

      let(:comment) do
        Comment.new(account: ticket.account, ticket: ticket).tap do |comment|
          comment.send(:value=, body)
          comment.send(:format=, 'rich')
        end
      end

      it 'does not match word in comment in style tags' do
        refute Rule.match_condition?(ticket, condition, run_state)
      end
    end

    describe 'when the comment body contains HTML' do
      let(:body) do
        '<br><b>foo_bar</b> <i>foobar</i> lol <b>mash</b><br>'
      end

      let(:condition_value) { 'foo_bar foobar lol mash' }

      let(:comment) do
        Comment.new(account: ticket.account, ticket: ticket).tap do |comment|
          comment.send(:value=, body)
          comment.send(:format=, 'rich')
        end
      end

      it 'matches conditions based on the plain value' do
        assert Rule.match_condition?(ticket, condition, run_state)
      end

      describe 'and the condition value contains special HTML characters' do
        let(:body)            { "<br>&lt;test&gt;'^&lt;[]&amp;</br>" }
        let(:condition_value) { "<test>'^<[]&" }

        it 'matches conditions based on the plain value' do
          assert Rule.match_condition?(ticket, condition, run_state)
        end
      end

      describe 'and the comment includes non-breaking space entities' do
        let(:body) do
          '<div>foo&nbsp;<font style=\"font-weight: bold;\">bar</font> ' \
            '<b>baz</b></div>'
        end

        let(:condition_value) { 'foo bar baz' }

        it 'does not match conditions based on the plain value' do
          refute Rule.match_condition?(ticket, condition, run_state)
        end
      end
    end

    describe 'when the comment includes special characters' do
      let(:condition_value) { 'Срочно' }
      let(:body)            { 'срочно' }

      it 'matches special characters' do
        assert Rule.match_condition?(ticket, condition, run_state)
      end
    end

    describe 'when the ticket is new' do
      let(:body) { 'Body' }

      before { ticket.stubs(new_record?: true) }

      describe 'and the ticket has a subject' do
        before { ticket.stubs(subject: 'subject') }

        describe 'and the subject includes the condition value' do
          let(:condition_value) { 'subject' }

          it 'returns true' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end

          describe 'and the operator is negating' do
            let(:operator) { 'is_not' }

            it 'returns false' do
              refute Rule.match_condition?(ticket, condition, run_state)
            end
          end
        end

        describe 'and the comment body includes the condition value' do
          let(:condition_value) { 'body' }

          it 'returns true' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end

          describe 'and the operator is negating' do
            let(:operator) { 'is_not' }

            it 'returns false' do
              refute Rule.match_condition?(ticket, condition, run_state)
            end
          end
        end

        describe 'and both include the condition value' do
          let(:condition_value) { 'subject' }
          let(:body)            { 'Subject' }

          it 'returns true' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end

          describe 'and the operator is negating' do
            let(:operator) { 'is_not' }

            it 'returns false' do
              refute Rule.match_condition?(ticket, condition, run_state)
            end
          end
        end

        describe 'and neither include the condition value' do
          let(:condition_value) { 'test' }

          it 'returns false' do
            refute Rule.match_condition?(ticket, condition, run_state)
          end

          describe 'and the operator is negating' do
            let(:operator) { 'is_not' }

            it 'returns true' do
              assert Rule.match_condition?(ticket, condition, run_state)
            end
          end
        end
      end

      describe 'and the ticket has a subject with special HTML characters' do
        before { ticket.stubs(subject: '<subject>') }

        describe 'and the condition contains the subject' do
          let(:condition_value) { '<subject>' }

          it 'returns true' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end

          describe 'and the first comment is rich' do
            let(:comment) do
              Comment.new(account: ticket.account).tap do |comment|
                comment.send(:value=, '<br>No match</br>')
                comment.send(:format=, 'rich')
              end
            end

            it 'returns true' do
              assert Rule.match_condition?(ticket, condition, run_state)
            end
          end
        end
      end

      describe 'and the ticket does not have a subject' do
        before { ticket.stubs(subject: nil) }

        describe 'and the comment body contains the condition value' do
          let(:condition_value) { 'body' }

          it 'returns true' do
            assert Rule.match_condition?(ticket, condition, run_state)
          end
        end
      end
    end
  end
  describe 'text_includes_words' do
    let(:account) { accounts(:minimum) }

    describe 'when checking words in asian languages without spaces' do
      describe_with_arturo_disabled :pattern_matching_without_spaces_for_thai do
        [
          ['no matter what is here', 'here'],
          ['скан и что', 'скан'], # Cyrillic
          ['ベサニー・ヒルズと高森昭は東京に住んでいます。', '東京'], # Japanese
          ['제 눈에 안경이다', '눈'], # Korean
          ['北京在中国北方', '北京'], # Chinese
          ['ในน้ำมีปลา ในนามีข้าว', 'ปลา'] # Thai
        ].each do |text, word|
          it 'matches words' do
            assert(
              Rule.text_includes_words?(text, word, account),
              "expected '#{text}' to include '#{word}'"
            )
          end
        end
      end

      describe_with_arturo_enabled :pattern_matching_without_spaces_for_thai do
        [
          ['ในน้ำมีปลา ในนามีข้าว', 'ปลา'], # Thai without word breaks
          ['น์', 'น'], # Thai consonant with consonant vowel thai
          # Thai "advantage" doesn't match "crash"
          # (https://support.zendesk.com/agent/tickets/3216951)
          ['สรัฐพงค์', 'รัฐ'],
          ['text', nil]
        ].each do |text, word|
          it 'exludes thai from partially matching words' do
            refute(
              Rule.text_includes_words?(text, word, account),
              "expected '#{text}' to not include '#{word}'"
            )
          end
        end
      end
    end
  end

  describe 'Rule.interval' do
    describe 'with parameters' do
      parameters_to_test = [
        ['updated_at', 'is', %w[3], true],
        ['updated_at', 'greater_than', %w[4], false],
        ['updated_at', 'less_than', %w[3], false],
        ['updated_at', 'is_business_hours', %w[3], false],
        ['updated_at', 'greater_than_business_hours', %w[3], false],
        ['updated_at', 'less_than_business_hours', %w[3], true]
      ]
      parameters_to_test.each do |set|
        it "returns #{set[3]} for #{set[0]} #{set[1]} #{set[2].first} hours ago" do
          Timecop.freeze(Time.parse('2013-10-31 4:00:00 UTC')) do
            ticket = tickets(:minimum_1)

            Account.any_instance.stubs(business_hours_active?: true)

            add_schedule_intervals(
              ticket.account.schedules.create(
                name:      'Schedule',
                time_zone: ticket.account.time_zone
              )
            )

            ticket.expects(:updated_at).returns(3.hours.ago - 10.minutes)
            source, operator, value, result = set

            assert_equal(
              result,
              Automation.
                send(:interval, ticket, source, operator, value.first)
            )
          end
        end
      end
    end
  end

  describe 'multiselect ticket field' do
    let(:account)             { accounts(:minimum) }
    let(:current_field_value) { 'purple_wombat blue_wombat' }
    let(:run_state) { Zendesk::Rules::Trigger::RunState.new }

    let(:included_value_id) do
      account.custom_field_options.find_by_value('purple_wombat').id
    end
    let(:included_value_id2) do
      account.custom_field_options.find_by_value('blue_wombat').id
    end
    let(:not_included_value_id) do
      account.custom_field_options.find_by_value('pink_wombat').id
    end
    let(:multiselect) do
      FieldMultiselect.
        new(account: accounts(:minimum), title: 'Multiselect Wombat')
    end

    let(:ticket)  { tickets(:minimum_1) }
    let(:ticket2) { tickets(:minimum_2) }

    before do
      multiselect.
        custom_field_options.
        build(name: 'Purple Wombat', value: 'purple_wombat')

      multiselect.
        custom_field_options.
        build(name: 'Blue Wombat', value: 'blue_wombat')

      multiselect.
        custom_field_options.
        build(name: 'Pink Wombat', value: 'pink_wombat')

      multiselect.
        custom_field_options.
        build(name: 'Just Blue', value: 'blue')

      multiselect.save!

      ticket.
        ticket_field_entries.
        build(value: 'purple_wombat blue_wombat', ticket_field: multiselect)

      ticket2.
        ticket_field_entries.
        build(value: '', ticket_field: multiselect)
    end

    describe 'when using the `includes` operator' do
      it 'matches words' do
        assert(
          Rule.match_condition?(
            ticket,
            DefinitionItem.new(
              "ticket_fields_#{multiselect.id}",
              'includes',
              included_value_id.to_s
            ),
            run_state
          )
        )

        assert(
          Rule.match_condition?(
            ticket,
            DefinitionItem.new(
              "ticket_fields_#{multiselect.id}",
              'includes',
              included_value_id2.to_s
            ),
            run_state
          )
        )
      end

      it 'does not match a missing word' do
        refute(
          Rule.match_condition?(
            ticket,
            DefinitionItem.new(
              "ticket_fields_#{multiselect.id}",
              'includes',
              not_included_value_id.to_s
            ),
            run_state
          )
        )
      end
    end

    describe 'when using the `not_includes` operator' do
      it 'does not match matching words' do
        refute(
          Rule.match_condition?(
            ticket,
            DefinitionItem.new(
              "ticket_fields_#{multiselect.id}",
              'not_includes',
              included_value_id.to_s
            ),
            run_state
          )
        )

        refute(
          Rule.match_condition?(
            ticket,
            DefinitionItem.new(
              "ticket_fields_#{multiselect.id}",
              'not_includes',
              included_value_id2.to_s
            ),
            run_state
          )
        )
      end

      it 'matches missings word' do
        assert(
          Rule.match_condition?(
            ticket,
            DefinitionItem.new(
              "ticket_fields_#{multiselect.id}",
              'not_includes',
              not_included_value_id.to_s
            ),
            run_state
          )
        )
      end
    end

    describe 'when using the `present` operator' do
      it 'matches if the field has any value' do
        assert Rule.match_condition?(
          ticket,
          DefinitionItem.new("ticket_fields_#{multiselect.id}", 'present', ''),
          run_state
        )
      end
      it 'does not match if the field has no value' do
        refute Rule.match_condition?(
          ticket2,
          DefinitionItem.new("ticket_fields_#{multiselect.id}", 'present', ''),
          run_state
        )
      end
    end

    describe 'when using the `not present` operator' do
      it 'matches if the field has no value' do
        assert Rule.match_condition?(
          ticket2,
          DefinitionItem.new(
            "ticket_fields_#{multiselect.id}",
            'not_present',
            ''
          ),
          run_state
        )
      end

      it 'does not match if the field has a value' do
        refute Rule.match_condition?(
          ticket,
          DefinitionItem.new(
            "ticket_fields_#{multiselect.id}",
            'not_present',
            ''
          ),
          run_state
        )
      end
    end
  end
end
