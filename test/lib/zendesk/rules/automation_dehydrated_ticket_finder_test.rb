require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::AutomationDehydratedTicketFinder do
  fixtures :rules, :tickets

  let(:subject)    do
    Zendesk::Rules::AutomationDehydratedTicketFinder.new(automation: automation)
  end

  let(:automation) { rules(:automation_close_ticket) }
  let(:ticket)     { tickets(:minimum_4) }

  before do
    Zendesk::Rules::RuleExecuter.
      any_instance.
      expects(:find_tickets_with_occam_client).
      returns(Array(ticket))
  end

  describe '#find_tickets' do
    it 'returns dehydrated tickets' do
      dehydrated_tickets = subject.find_tickets
      dehydrated_tickets.size.must_equal 1

      dehydrated_tickets.
        first.
        must_be_instance_of(Zendesk::Rules::DehydratedTicket)

      dehydrated_tickets.first.nice_id.must_equal ticket.nice_id

      dehydrated_tickets.
        first.
        generated_timestamp.
        must_equal(ticket.generated_timestamp.to_i)
    end
  end
end
