require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::NullAppInstallation do
  describe '.as_json' do
    it 'returns nil' do
      assert_nil Zendesk::Rules::NullAppInstallation.as_json
    end
  end

  describe '.present?' do
    it 'returns false' do
      refute Zendesk::Rules::NullAppInstallation.present?
    end
  end
end
