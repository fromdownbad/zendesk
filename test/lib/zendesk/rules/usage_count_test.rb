require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Zendesk::Rules::UsageCount do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }

  let(:rule) { Macro.all.first }

  let!(:usage_count) { Zendesk::Rules::UsageCount.new(rule) }

  before do
    Timecop.freeze

    Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

    Macro.destroy_all

    create_macro(title: 'Macro with usages')

    [30.minutes.ago, 10.hours.ago, 4.days.ago, 29.days.ago].each do |time|
      Timecop.travel(time) do
        record_usage(type: :macro, id: rule.id)
      end
    end
  end

  describe '#hourly' do
    describe 'when it has not been set' do
      it 'calculates the hourly usage' do
        assert_equal 1, usage_count.hourly
      end
    end

    describe 'when it has been set' do
      before { usage_count.hourly = 6 }

      it 'returns the value' do
        assert_equal 6, usage_count.hourly
      end
    end
  end

  describe '#daily' do
    describe 'when it has not been set' do
      it 'calculates the daily usage' do
        assert_equal 2, usage_count.daily
      end
    end

    describe 'when it has been set' do
      before { usage_count.daily = 6 }

      it 'returns the value' do
        assert_equal 6, usage_count.daily
      end
    end
  end

  describe '#weekly' do
    describe 'when it has not been set' do
      it 'calculates the weekly usage' do
        assert_equal 3, usage_count.weekly
      end
    end

    describe 'when it has been set' do
      before { usage_count.weekly = 6 }

      it 'returns the value' do
        assert_equal 6, usage_count.weekly
      end
    end
  end

  describe '#monthly' do
    describe 'when it has not been set' do
      it 'calculates the monthly usage' do
        assert_equal 4, usage_count.monthly
      end
    end

    describe 'when it has been set' do
      before { usage_count.monthly = 6 }

      it 'returns the value' do
        assert_equal 6, usage_count.monthly
      end
    end
  end
end
