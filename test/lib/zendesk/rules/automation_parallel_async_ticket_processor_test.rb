require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::AutomationParallelAsyncTicketProcessor do
  fixtures :rules, :tickets

  let(:subject) do
    Zendesk::Rules::AutomationParallelAsyncTicketProcessor.
      new(automation: automation)
  end

  let(:automation) { rules(:automation_close_ticket) }
  let(:ticket)     { tickets(:minimum_4) }

  describe '#execute' do
    it 'returns an AutomationResult with audit ids' do
      AccountAutomationParallelExecutionJob.
        expects(:enqueue).
        with(
          automation.account_id,
          automation.id,
          anything,
          anything
        ) do |_, _, dehyrdated_ticket_batch, _|
          dehyrdated_ticket_batch.must_equal([ticket])
        end

      subject.process_tickets([ticket])
      subject.queued_audit_ids.size.must_equal 1
    end
  end
end
