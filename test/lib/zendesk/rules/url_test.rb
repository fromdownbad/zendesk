require_relative '../../../support/test_helper'
require 'rack/mock'

SingleCov.covered!

describe Zendesk::Rules::Url do
  fixtures :rules,
    :users,
    :tickets,
    :groups,
    :ticket_fields,
    :custom_field_options,
    :accounts

  let(:url) { build_url('/rules?filter=triggers') }

  describe 'supported_rule_type?' do
    %w[automations triggers views].each do |rule_type|
      describe "when rule type is #{rule_type}" do
        before { url.stubs(:rule_type).returns(rule_type) }

        it 'returns true' do
          assert url.supported_rule_type?
        end
      end
    end

    describe 'when rule type is not valid' do
      before { url.stubs(rule_type: 'fancymacros') }

      it 'returns false' do
        refute url.supported_rule_type?
      end
    end
  end

  describe 'new path' do
    %w[automations triggers views].each do |rule_type|
      describe "when rule type is #{rule_type}" do
        it 'points to the specific type' do
          assert_equal(
            "/rules/#{rule_type}",
            build_url("/rules?filter=#{rule_type}").new_path
          )
        end
      end
    end

    describe 'when the rule type is not valid' do
      let(:url) { build_url('/rules?filter=fancymacros') }

      it 'is the same as current path' do
        assert_equal '/rules', url.new_path
      end
    end
  end

  describe '#rule_type' do
    let(:url) { build_url('/rules?filter=trigger') }

    it 'pluralizes singular types' do
      assert_equal 'triggers', url.rule_type
    end

    it 'is detected via the rule.type param' do
      url = build_url('/rules?rule[type]=triggers')
      assert_equal 'triggers', url.rule_type
    end

    it 'is detected via actions specific to that type' do
      url = build_url('/rules/search')
      assert_equal 'views', url.rule_type
    end

    describe 'detecting by record type' do
      it "is detected from the record's type when a record is found" do
        rule = rules(:trigger_notify_requester_of_received_request)
        url  = build_url("/rules/#{rule.id}")

        assert_equal 'triggers', url.rule_type
      end

      it 'is blank when no record is found' do
        rule = rules(:trigger_notify_requester_of_received_request)
        url  = build_url("/rules/#{rule.id}")
        rule.destroy

        assert_equal '', url.rule_type
      end

      it 'is blank when no account is present' do
        rule = rules(:trigger_notify_requester_of_received_request)
        url  = build_url("/rules/#{rule.id}")
        @env['zendesk.account'] = nil

        assert_equal '', url.rule_type
      end
    end
  end

  protected

  def build_url(path)
    @env = Rack::MockRequest.env_for(path)
    @env['zendesk.account'] = accounts(:minimum)
    Zendesk::Rules::Url.new(@env)
  end
end
