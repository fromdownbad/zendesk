require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Rules::Condition do
  fixtures :accounts,
    :cf_fields

  let(:account) { accounts(:minimum) }

  describe '.for_definition' do
    let(:definition) do
      {
        value: 'status',
        title: 'Status',
        group: 'ticket',
        values: {
          type: 'list',
          list: [
            {value: 'open',   title: 'Open'},
            {value: 'closed', title: 'Closed'}
          ]
        },
        operators: [
          {value: :is,     title: 'Is'},
          {value: :is_not, title: 'Is not'}
        ]
      }
    end

    let(:trigger_condition) do
      Zendesk::Rules::Condition.
        for_definition(definition, account: account)
    end

    it 'has a subject' do
      assert_equal 'status', trigger_condition.subject
    end

    it 'has a title' do
      assert_equal 'Status', trigger_condition.title
    end

    it 'has values' do
      assert_equal(
        [{value: 'open', title: 'Open'}, {value: 'closed', title: 'Closed'}],
        trigger_condition.values.map do |condition_value|
          {value: condition_value.value, title: condition_value.title}
        end
      )
    end

    it 'has a type' do
      assert_equal 'list', trigger_condition.type
    end

    it 'has a group' do
      assert_equal 'ticket', trigger_condition.group
    end

    it 'has operators' do
      assert_equal(
        [{value: :is, title: 'Is'}, {value: :is_not, title: 'Is not'}],
        trigger_condition.operators.map do |condition_operator|
          {value: condition_operator.value, title: condition_operator.title}
        end
      )
    end

    it 'returns if it is repeatable' do
      refute trigger_condition.repeatable?
    end

    it 'returns if it is nullable' do
      refute trigger_condition.nullable?
    end

    it 'does not have metadata' do
      refute trigger_condition.metadata.present?
    end
  end

  describe 'when initialized' do
    let(:source)        { 'ticket_form_id' }
    let(:type)          { 'list' }
    let(:list)          { [] }
    let(:operator_list) { [] }

    let(:trigger_condition) do
      Zendesk::Rules::Condition.new(
        OpenStruct.new(
          source:        source,
          title:         'title',
          type:          type,
          group:         'ticket',
          list:          list,
          operator_list: operator_list
        ),
        account: account
      )
    end

    it 'has a group' do
      assert_equal 'ticket', trigger_condition.group
    end

    it 'has a type' do
      assert_equal 'list', trigger_condition.type
    end

    it 'is not repeatable' do
      refute trigger_condition.repeatable?
    end

    describe '#subject' do
      describe 'when it does not have an attribute mapping' do
        let(:source) { 'brand_id' }

        it 'returns the value' do
          assert_equal source, trigger_condition.subject
        end
      end

      describe 'when it has an attribute mapping' do
        let(:source) { 'ticket_type_id' }

        it 'returns the ticket attribute name' do
          assert_equal 'type', trigger_condition.subject
        end
      end

      describe 'when it is a custom ticket field' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { "ticket_fields_#{field.id}" }

        it 'returns the ticket attribute name' do
          assert_equal "custom_fields_#{field.id}", trigger_condition.subject
        end
      end

      describe 'when it is a `via_id` condition' do
        let(:source) { 'via_id' }

        it 'returns the value' do
          assert_equal source, trigger_condition.subject
        end
      end
    end

    describe '#title' do
      describe 'when it does not have a title mapping' do
        let(:source) { 'brand_id' }

        it 'returns the title' do
          assert_equal 'title', trigger_condition.title
        end
      end

      describe 'when it is a `comment_includes_word` action' do
        let(:source) { 'comment_includes_word' }

        it 'returns the mapped title' do
          assert_equal 'Comment text', trigger_condition.title
        end
      end

      describe 'when it is a `comment_is_public` action' do
        let(:source) { 'comment_is_public' }

        it 'returns the mapped title' do
          assert_equal 'Comment', trigger_condition.title
        end
      end

      describe 'when it is a `requester_twitter_statuses_count` action' do
        let(:source) { 'requester_twitter_statuses_count' }

        it 'returns the mapped title' do
          assert_equal 'Number of tweets', trigger_condition.title
        end
      end

      describe 'when it is a `subject_includes_word` action' do
        let(:source) { 'subject_includes_word' }

        it 'returns the mapped title' do
          assert_equal 'Subject text', trigger_condition.title
        end
      end

      describe 'when it is an `update_type` action' do
        let(:source) { 'update_type' }

        it 'returns the mapped title' do
          assert_equal 'Ticket', trigger_condition.title
        end
      end
    end

    describe '#type' do
      let(:type) { 'text' }

      describe 'when it is not a tag-based condition' do
        let(:source) { 'status_id' }

        it 'returns the specified type' do
          assert_equal type, trigger_condition.type
        end
      end

      describe 'when it is a `current_tags` condition' do
        let(:source) { 'current_tags' }

        it 'returns `tags`' do
          assert_equal 'tags', trigger_condition.type
        end
      end
    end

    describe '#metadata' do
      describe 'when the condition does not have metadata' do
        let(:source) { 'status_id' }

        it 'is not present' do
          refute trigger_condition.metadata.present?
        end
      end

      describe 'when it is an `autocomplete` condition' do
        let(:type) { 'autocomplete' }

        it 'returns the relevant metadata' do
          assert_equal(
            %i[collection_key item_key],
            trigger_condition.metadata
          )
        end
      end
    end

    describe '#nullable?' do
      describe 'when the condition is not nullable' do
        let(:source) { 'status_id' }

        it 'returns false' do
          refute trigger_condition.nullable?
        end
      end

      describe 'when it is an `assignee_id` condition' do
        let(:source) { 'assignee_id' }

        it 'returns true' do
          assert trigger_condition.nullable?
        end
      end

      describe 'when it is a `group_id` condition' do
        let(:source) { 'group_id' }

        it 'returns true' do
          assert trigger_condition.nullable?
        end
      end

      describe 'when it is an `organization_id` condition' do
        let(:source) { 'organization_id' }

        describe 'and it is a list type condition' do
          let(:type) { 'list' }

          it 'returns true' do
            assert trigger_condition.nullable?
          end
        end

        describe 'and it is an autocomplete type condition' do
          let(:type) { 'autocomplete' }

          it 'returns true' do
            assert trigger_condition.nullable?
          end
        end
      end

      describe 'when it is a `priority` condition' do
        let(:source) { 'priority_id' }

        it 'returns true' do
          assert trigger_condition.nullable?
        end
      end

      describe 'when it is a `ticket_type_id` condition' do
        let(:source) { 'ticket_type_id' }

        it 'returns true' do
          assert trigger_condition.nullable?
        end
      end

      describe 'when it is a custom ticket field condition' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { "ticket_fields_#{field.id}" }

        describe 'and it is a list type condition' do
          let(:type) { 'list' }

          it 'returns true' do
            assert trigger_condition.nullable?
          end
        end

        describe 'and it is not a list type condition' do
          let(:type) { 'text' }

          it 'returns false' do
            refute trigger_condition.nullable?
          end
        end
      end

      describe 'when it is a custom user field condition' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { 'requester.custom_fields.user_dropdown_field' }

        describe 'and it is a list type condition' do
          let(:type) { 'list' }

          it 'returns true' do
            assert trigger_condition.nullable?
          end
        end

        describe 'and it is not a list type condition' do
          let(:type) { 'text' }

          it 'returns false' do
            refute trigger_condition.nullable?
          end
        end
      end

      describe 'when it is a custom organization field condition' do
        let(:field)  { cf_fields(:dropdown1) }
        let(:source) { 'organization.custom_fields.custom_org_drop_down_field' }

        describe 'and it is a list type condition' do
          let(:type) { 'list' }

          it 'returns true' do
            assert trigger_condition.nullable?
          end
        end

        describe 'and it is not a list type condition' do
          let(:type) { 'text' }

          it 'returns false' do
            refute trigger_condition.nullable?
          end
        end
      end
    end

    describe '#values' do
      describe 'when the list includes an empty item' do
        let(:list) do
          [
            {value: 0, title: '-'},
            {value: 1, title: 'Option 1'},
            {value: 2, title: 'Option 2'},
            {value: 3, title: 'Option 3'}
          ]
        end

        describe 'and the condition is not nullable' do
          let(:source) { 'brand_id' }

          it 'removes the empty item' do
            assert_equal(
              [
                {value: '1', title: 'Option 1'},
                {value: '2', title: 'Option 2'},
                {value: '3', title: 'Option 3'}
              ],
              trigger_condition.values.map do |item|
                {value: item.value, title: item.title}
              end
            )
          end
        end

        describe 'and the condition is nullable' do
          let(:source) { 'assignee_id' }

          it 'includes a null item' do
            assert_equal(
              [
                {value: '__NULL__', title: '-'},
                {value: '1',        title: 'Option 1'},
                {value: '2',        title: 'Option 2'},
                {value: '3',        title: 'Option 3'}
              ],
              trigger_condition.values.map do |item|
                {value: item.value, title: item.title}
              end
            )
          end
        end
      end

      describe 'when the list items have an `enabled` key' do
        let(:list) do
          [
            {value: 1, title: 'Option 1', enabled: true},
            {value: 2, title: 'Option 2', enabled: false},
            {value: 3, title: 'Option 3', enabled: false},
            {value: 4, title: 'Option 4', enabled: true}
          ]
        end

        it 'uses the specified `enabled` values' do
          assert_equal(
            [
              {value: '1', title: 'Option 1', enabled: true},
              {value: '2', title: 'Option 2', enabled: false},
              {value: '3', title: 'Option 3', enabled: false},
              {value: '4', title: 'Option 4', enabled: true}
            ],
            trigger_condition.values.map do |condition_value|
              {
                value:   condition_value.value,
                title:   condition_value.title,
                enabled: condition_value.enabled?
              }
            end
          )
        end
      end

      describe 'when the list items do not have the `enabled` key' do
        let(:list) do
          [
            {value: 1, title: 'Option 1'},
            {value: 2, title: 'Option 2'},
            {value: 3, title: 'Option 3'}
          ]
        end

        it 'enables all of the items' do
          assert_equal(
            [
              {value: '1', title: 'Option 1', enabled: true},
              {value: '2', title: 'Option 2', enabled: true},
              {value: '3', title: 'Option 3', enabled: true}
            ],
            trigger_condition.values.map do |condition_value|
              {
                value:   condition_value.value,
                title:   condition_value.title,
                enabled: condition_value.enabled?
              }
            end
          )
        end
      end

      describe 'when it is a `via_id` condition' do
        let(:source) { 'via_id' }

        let(:list) do
          [
            {value: ViaType.WEB_FORM.to_s,      title: 'Web form'},
            {value: ViaType.CLOSED_TICKET.to_s, title: 'Closed ticket'},
            {value: ViaType.SMS.to_s,           title: 'Text'}
          ]
        end

        it 'returns the values' do
          assert_equal(
            list,
            trigger_condition.values.map do |condition_value|
              {value: condition_value.value, title: condition_value.title}
            end
          )
        end
      end

      describe 'when it is a `current_via_id` condition' do
        let(:source) { 'current_via_id' }

        let(:list) do
          [
            {value: ViaType.WEB_FORM.to_s,      title: 'Web form'},
            {value: ViaType.CLOSED_TICKET.to_s, title: 'Closed ticket'},
            {value: ViaType.SMS.to_s,           title: 'Text'}
          ]
        end

        it 'returns the values' do
          assert_equal(
            list,
            trigger_condition.values.map do |condition_value|
              {value: condition_value.value, title: condition_value.title}
            end
          )
        end
      end

      describe 'when it is a `satisfaction_score` condition' do
        let(:source) { 'satisfaction_score' }

        let(:list) do
          [
            {
              value: SatisfactionType.GOOD.to_s,
              title: 'Good'
            },
            {
              value: SatisfactionType.BADWITHCOMMENT.to_s,
              title: 'Bad with comment'
            }
          ]
        end

        it 'returns the values' do
          assert_equal(
            [
              {value: 'good',             title: 'Good'},
              {value: 'bad_with_comment', title: 'Bad with comment'}
            ],
            trigger_condition.values.map do |condition_value|
              {value: condition_value.value, title: condition_value.title}
            end
          )
        end
      end
    end

    describe '#operators' do
      let(:operator_list) do
        [{value: :is, title: 'Is'}, {value: :is_not, title: 'Is not'}]
      end

      it 'returns the operators' do
        assert_equal(
          [
            {value: :is,     title: 'Is',     terminal: false},
            {value: :is_not, title: 'Is not', terminal: false}
          ],
          trigger_condition.operators.map do |condition_operator|
            {
              value:    condition_operator.value,
              title:    condition_operator.title,
              terminal: condition_operator.terminal?
            }
          end
        )
      end

      describe 'when a terminal operator is present' do
        let(:operator_list) do
          [{value: :is_present, title: 'Is present', values: []}]
        end

        it 'indicates the terminal operators' do
          assert_equal(
            [{value: :is_present, title: 'Is present', terminal: true}],
            trigger_condition.operators.map do |condition_operator|
              {
                value:    condition_operator.value,
                title:    condition_operator.title,
                terminal: condition_operator.terminal?
              }
            end
          )
        end
      end
    end
  end
end
