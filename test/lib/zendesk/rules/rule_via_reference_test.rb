require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Api::V2::Tickets::RuleViaPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:rule)    { create_trigger }
  let(:user)    { users(:minimum_admin_not_owner) }

  let(:rule_via_reference) do
    Zendesk::Rules::RuleViaReference.new(user: user, rule: rule)
  end

  describe '#deleted?' do
    it 'returns whether the rule is deleted' do
      assert_equal rule.deleted?, rule_via_reference.deleted?
    end
  end

  describe '#title' do
    it 'returns the rule title' do
      assert_equal rule.title, rule_via_reference.title
    end

    describe 'when the rule is soft deleted' do
      before { rule.soft_delete }

      it 'returns the rule title with a `(deleted)` suffix' do
        assert_equal "#{rule.title} (deleted)", rule_via_reference.title
      end
    end

    describe 'when the rule is hard deleted' do
      let(:rule) { nil }

      it 'returns the title for an unknown rule' do
        assert_equal 'Unknown rule (deleted)', rule_via_reference.title
      end
    end
  end

  describe '#rule_type' do
    it 'returns the rule type' do
      assert_equal rule.rule_type, rule_via_reference.rule_type
    end
  end

  describe '#rule_id' do
    it 'returns the rule type' do
      assert_equal rule.id, rule_via_reference.rule_id
    end
  end

  describe '#viewable?' do
    describe 'when the user can view the rule' do
      it 'returns true' do
        assert rule_via_reference.viewable?
      end
    end

    describe 'when user cannot view the rule' do
      before { user.stubs(:can?).with(:view, rule).returns(false) }

      it 'returns false' do
        refute rule_via_reference.viewable?
      end
    end

    describe 'when the rule is deleted' do
      before { rule.soft_delete }

      it 'returns false' do
        refute rule_via_reference.viewable?
      end
    end
  end
end
