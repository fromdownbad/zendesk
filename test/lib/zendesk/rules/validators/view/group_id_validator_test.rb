require_relative '../../../../../support/test_helper'
require_relative '../../../../../support/rules_test_helper'

SingleCov.covered!

describe Zendesk::Rules::Validators::View::GroupIdValidator do
  fixtures :accounts, :rules, :groups

  let(:view)              { rules(:active_view_for_minimum_group) }
  let(:group)             { groups(:minimum_group) }
  let(:group_2)           { groups(:support_group) }
  let(:valid_group_ids)   { [group, group_2].map(&:id) }
  let(:invalid_group_ids) { [10_002, 10_234] }

  before do
    GroupView.destroy_all
    group_2.update_columns(is_active: 1)
  end

  describe_with_arturo_enabled :multiple_group_views_writes do
    describe 'when update view with valid group ids' do
      it 'saves view with no error' do
        view.group_owner_ids = valid_group_ids
        assert view.valid?
      end
    end

    describe 'when update view with invalid group ids' do
      let(:error_messsage) do
        I18n.t(
          'txt.admin.models.rules.rule.invalid_group_ids',
          group_ids: invalid_group_ids.join(', ')
        )
      end

      describe 'when all group ids are invalid' do
        before do
          view.group_owner_ids = invalid_group_ids
        end

        it 'fails validation' do
          refute view.valid?
        end

        it 'adds an error message' do
          view.valid?
          assert_equal(view.errors.messages[:base], [error_messsage])
        end
      end

      describe 'when some group ids are invalid' do
        before do
          view.group_owner_ids = [group.id, 10_002, 10_234]
        end

        it 'fails validation' do
          refute view.valid?
        end

        it 'adds an error message' do
          view.valid?
          assert_equal(view.errors.messages[:base], [error_messsage])
        end
      end
    end
  end

  describe_with_arturo_disabled :multiple_group_views_writes do
    describe 'when update view with valid group ids' do
      it 'saves view with no errors' do
        view.group_owner_ids = valid_group_ids
        assert view.valid?
      end
    end

    describe 'when update view with invalid group ids' do
      it 'saves view with no errors' do
        view.group_owner_ids = invalid_group_ids
        assert view.valid?
      end
    end
  end
end
