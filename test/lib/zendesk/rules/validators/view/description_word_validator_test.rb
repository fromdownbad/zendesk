require_relative '../../../../../support/test_helper'
require_relative '../../../../../support/rules_test_helper'

SingleCov.covered!

describe Zendesk::Rules::Validators::View::DescriptionWordValidator do
  include RulesTestHelper
  fixtures :accounts, :rules
  let(:account) { accounts(:minimum) }

  describe '#too_many_description_word_conditions?' do
    describe 'with 2 description word conditions' do
      let(:rule) do
        build_rule(
          type: 'View',
          conditions_all: [
            ['status_id', 'is', [StatusType.NEW]],
            %w[description_includes_word is anything]
          ],
          conditions_any: [
            %w[description_includes_word includes nothing']
          ]
        )
      end
      it 'saves without errors' do
        assert rule.save
      end
    end
    describe 'with MORE THAN 2 description word conditions' do
      let(:rule) do
        build_rule(
          type: 'View',
          conditions_all: [
            ['status_id', 'is', [StatusType.NEW]],
            %w[description_includes_word is anything],
            %w[description_includes_word includes something]
          ],
          conditions_any: [
            %w[description_includes_word not_includes nothing]
          ]
        )
      end
      describe_with_arturo_enabled :views_strict_description_count_validation do
        it 'rejects rules with more than 2 description word conditions' do
          refute rule.save
          assert_match(/No more than two/, rule.errors.full_messages.join)
        end
      end

      describe_with_arturo_disabled :views_strict_description_count_validation do
        it 'saves without errors' do
          assert rule.save
        end
      end
    end
  end
end
