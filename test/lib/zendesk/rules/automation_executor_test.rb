require_relative '../../../support/test_helper'
require_relative '../../../support/business_hours_test_helper'

SingleCov.covered! uncovered: 2

describe Zendesk::Rules::AutomationExecutor do
  include BusinessHoursTestHelper

  fixtures :all

  def create_automation(conditions_all, conditions_any, actions)
    definition = Definition.new

    Array(conditions_all).each do |condition|
      definition.conditions_all.push(condition)
    end

    Array(conditions_any).each do |condition|
      definition.conditions_any.push(condition)
    end

    definition.actions.push(actions)

    Automation.new(
      account:    accounts(:minimum),
      definition: definition,
      title:      'Wobmat automation',
      owner:      accounts(:minimum)
    )
  end

  describe '#execute' do
    before do
      @automation = rules(:automation_close_ticket)
    end

    it 'executes applies actions to matching tickets' do
      tickets(:minimum_4).status_id.must_equal StatusType.SOLVED

      Zendesk::Rules::RuleExecuter.
        any_instance.
        stubs(:find_tickets_with_occam_client).
        returns([tickets(:minimum_4)])

      result = Zendesk::Rules::AutomationExecutor.
        new(automation: rules(:automation_close_ticket)).
        execute

      result.changed.must_equal 1
      tickets(:minimum_4).tap(&:reload).status_id.must_equal StatusType.CLOSED
    end

    # The following are tests are for logic not explicitly handled in the
    # Automation* classes, and may be more appropriately relocated deeper
    # in the rules system.

    describe 'due date conditions' do
      def create_tasks(times)
        times.each_with_index do |due, index|
          ticket = tickets("minimum_#{index + 1}")
          ticket.update_column(:ticket_type_id, TicketType.TASK)
          ticket.update_column(:due_date, due)
        end
      end

      def create_automation(source, hours)
        definition = Definition.new
        definition.conditions_all.replace(
          [
            DefinitionItem.new(source, 'less_than', [hours]),
            DefinitionItem.new(
              'current_tags',
              'not_includes',
              ['until_due_date']
            ),
            DefinitionItem.new('status_id', 'less_than', [3])
          ]
        )
        definition.actions.replace(
          [DefinitionItem.new('current_tags', 'is', ['until_due_date'])]
        )

        Automation.create!(
          account:    accounts(:minimum),
          title:      'due date test',
          definition: definition,
          owner:      accounts(:minimum)
        )
      end

      describe 'until' do
        [1, 2].each do |offset_hours|
          before do
            @now = Time.use_zone(accounts(:minimum).time_zone) do
              Time.zone.local(2015, 'jan', 13, 12, 0).utc
            end

            hours = offset_hours.hours
            create_tasks([@now + hours, @now + hours - 30.minutes, @now])

            @automation = create_automation('until_due_date', offset_hours)

            Zendesk::Rules::RuleExecuter.
              any_instance.
              stubs(:find_tickets_with_occam_client).
              returns([tickets(:minimum_1), tickets(:minimum_2)])
          end

          it "only executes for tickets due between before now and less than ** or equal to ** #{offset_hours} hour(s)" do
            # now < {field} <= x.hours.from_now
            Timecop.freeze(@now) do
              assert_equal(
                2,
                Zendesk::Rules::AutomationExecutor.
                  new(automation: @automation).
                  execute.
                  changed
              )
            end
          end
        end
      end

      describe 'since' do
        [1, 2].each do |offset_hours|
          before do
            @now = Time.use_zone(accounts(:minimum).time_zone) do
              Time.zone.local(2015, 'jan', 13, 12, 0).utc
            end

            hours = offset_hours.hours
            create_tasks([@now - hours, @now - hours + 30.minutes, @now])

            @automation = create_automation('due_date', offset_hours)

            Zendesk::Rules::RuleExecuter.
              any_instance.
              stubs(:find_tickets_with_occam_client).
              returns([tickets(:minimum_1), tickets(:minimum_2)])
          end

          it "only executes for tickets due since now and less than #{offset_hours} hour(s)" do
            # x.hours.ago < {field} <= now
            Timecop.freeze(@now) do
              assert_equal(
                2,
                Zendesk::Rules::AutomationExecutor.
                  new(automation: @automation).
                  execute.
                  changed
              )
            end
          end
        end
      end
    end

    describe 'custom user/org fields in conditions' do
      before do
        # extra ticket user minimum_end_user2
        user = users(:minimum_end_user2)

        @ticket_7 = Ticket.new(
          requester:   user,
          account:     user.account,
          subject:     'ticket subject',
          description: 'testing ticket',
          status_id:   1
        )

        Ticket.record_timestamps = false

        # automation matches tickets > 1 minute old
        @ticket_7.updated_at = Time.now.utc.advance(minutes: -2)

        Ticket.record_timestamps = true
        @ticket_7.will_be_saved_by(user)
        @ticket_7.save!

        @before = Time.now.utc.advance(seconds: -1)
        @automation = rules(:automation_for_custom_user_model)

        Zendesk::Rules::RuleExecuter.
          any_instance.
          stubs(:find_tickets_with_occam_client).
          returns([tickets(:minimum_2), tickets(:minimum_6), @ticket_7])
      end

      it 'changes three tickets' do
        assert_equal(
          3,
          Zendesk::Rules::AutomationExecutor.
            new(automation: @automation).
            execute.
            changed
        )

        tickets = [
          tickets(:minimum_2),
          tickets(:minimum_6),
          Ticket.find(@ticket_7.id)
        ]

        tickets.each do |tic|
          assert_operator @before, :<=, tic.updated_at.utc
          assert_equal 2, tic.status_id
        end

        # requester.custom_fields.decimal1
        users = [users(:minimum_end_user), users(:minimum_end_user2)]
        users.each do |user|
          user.reload

          assert_equal(
            '0000000000123.4560',
            user.custom_field_values.value_for_key('decimal1').value
          )
        end

        # requester.organization.custom_fields.decimal1
        orgs = [organizations(:minimum_organization1)]
        orgs.each do |org|
          org.reload

          assert_equal(
            '-000000000009.9900',
            org.custom_field_values.value_for_key('decimal1').value
          )
        end
      end

      # Should only happen when multi org is enabled
      describe 'for a ticket with different org and requester default org' do
        before do
          field = @ticket_7.
            account.
            custom_fields.
            where(owner: 'Organization', key: 'decimal1').
            first

          @ticket_7.requester.organization =
            organizations(:minimum_organization2)

          @ticket_7.
            requester.
            organization.
            custom_field_values.
            set_field_value(field, '20.2')

          @ticket_7.requester.organization.save!
          @ticket_7.requester.save!

          Ticket.update_all(status_id: 2)
          assert(@ticket_7.update_columns(
                   organization_id: organizations(:minimum_organization1).id,
                   status_id: 1
                 ))

          assert_equal(
            organizations(:minimum_organization1),
            @ticket_7.reload.organization
          )

          assert_equal(
            organizations(:minimum_organization2),
            @ticket_7.requester.reload.organization
          )

          Zendesk::Rules::RuleExecuter.
            any_instance.
            stubs(:find_tickets_with_occam_client).
            returns([@ticket_7])
        end

        it 'assigns values to the correct locations' do
          assert_equal(
            1,
            Zendesk::Rules::AutomationExecutor.
              new(automation: @automation).
              execute.
              changed
          )

          assert_equal 2, @ticket_7.reload.status_id

          assert_equal(
            '-000000000009.9900',
            @ticket_7.
              organization.
              custom_field_values.
              value_for_key('decimal1').
              value
          )

          assert_equal(
            '0000000000123.4560',
            @ticket_7.
              requester.
              custom_field_values.
              value_for_key('decimal1').
              value
          )

          assert_equal(
            '0000000000020.2000',
            @ticket_7.
              requester.
              organization.
              custom_field_values.
              value_for_key('decimal1').
              value
          )
        end
      end
    end

    describe 'custom date ticket field in condition' do
      before do
        @custom_date_field = FieldDate.create!(
          title:   'My custom wombat field',
          is_active:  true,
          account: accounts(:minimum)
        )

        conditions = [
          DefinitionItem.new(
            "ticket_fields_#{@custom_date_field.id}",
            'is',
            '2014-02-18'
          ),
          DefinitionItem.new(
            'status_id',
            'is',
            StatusType.OPEN
          )
        ]

        pending_action = DefinitionItem.new(
          'status_id',
          'is',
          StatusType.PENDING
        )

        @automation = create_automation(conditions, nil, pending_action)
        @automation.save!
        @ticket = tickets(:minimum_2)

        @ticket.ticket_field_entries.create!(
          value:           '2014-02-18',
          ticket_field_id: @custom_date_field.id
        )

        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.save!
        @ticket.reload

        Zendesk::Rules::RuleExecuter.
          any_instance.
          stubs(:find_tickets_with_occam_client).
          returns([@ticket])
      end

      it 'changes ticket' do
        assert_equal StatusType.OPEN, @ticket.status_id

        assert_equal(
          1,
          Zendesk::Rules::AutomationExecutor.
            new(automation: @automation).
            execute.
            changed
        )

        @ticket.reload
        assert_equal StatusType.PENDING, @ticket.status_id
      end
    end
  end
end
