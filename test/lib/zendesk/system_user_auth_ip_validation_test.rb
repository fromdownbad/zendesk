require_relative '../../support/test_helper'
require 'zendesk/system_user/authorizes_subsystem_users'

SingleCov.covered!

class SystemUserAuthIPValidationController < ActionController::Base
  include Zendesk::Auth::Warden::ControllerMixin
  include Zendesk::Auth::AuthenticatedSessionMixin
  include Zendesk::SystemUser::AuthorizesSubsystemUsers
  include Zendesk::SystemUserAuthIPValidation

  allow_subsystem_user :classic

  def index
    render plain: 'success'
  end
end

class SystemUserAuthIPValidationTest < ActionController::TestCase
  before do
    ZendeskAPM.stubs(:enabled?).returns(false)
  end

  tests SystemUserAuthIPValidationController
  use_test_routes

  describe '#validate_system_user_ip' do
    describe '`sys_auth_sec_inc_sep_2018_enable_logging` arturo disabled' do
      before do
        login('minimum_admin')
        Zendesk::StatsD::Client.any_instance.expects(:event).never
        get :index
      end

      it 'allows all requests' do
        assert_response :success
      end
    end

    describe '`sys_auth_sec_inc_sep_2018_enable_logging` arturo enabled' do
      before do
        Arturo.enable_feature!(:sys_auth_sec_inc_sep_2018_enable_logging)
        Arturo::Feature.create!(
          symbol: 'sys_auth_sec_inc_sep_2018_whitelist',
          phase: 'on',
          deployment_percentage: 0,
          external_beta_subdomains: %w[2.2.2.1 1.1.1.0/16]
        )
      end

      describe 'a request from a non-system user' do
        before do
          login('minimum_admin')
          Zendesk::StatsD::Client.any_instance.expects(:event).never
          get :index
        end

        it 'allows the request' do
          assert_response :success
        end

        it 'does not log request info' do
          Rails.logger.expects(:warn).never
        end
      end

      as_a_subsystem_user(user: 'classic', account: :minimum) do
        it 'allows the request' do
          get :index
          Zendesk::StatsD::Client.any_instance.expects(:event).never
          assert_response :success
        end

        describe 'and the ip address is known' do
          before do
            request.env['REMOTE_ADDR'] = '2.2.2.1'
            Zendesk::StatsD::Client.any_instance.expects(:event).never
          end

          describe '`sys_auth_sec_inc_sep_2018_enable_blocking` arturo disabled' do
            it 'allows the request' do
              get :index
              assert_response :success
            end
          end

          describe '`sys_auth_sec_inc_sep_2018_enable_blocking` arturo enabled' do
            before do
              Arturo.enable_feature!(:sys_auth_sec_inc_sep_2018_enable_blocking)
              Zendesk::StatsD::Client.any_instance.expects(:event).never
              get :index
            end

            it 'does not block' do
              assert_response :success
            end
          end
        end

        describe 'and the ip address is unknown' do
          before do
            request.env['REMOTE_ADDR'] = '3.3.3.3'
          end

          describe '`sys_auth_sec_inc_sep_2018_enable_blocking` arturo disabled' do
            before do
              Zendesk::StatsD::Client.any_instance.expects(:event).once
              get :index
            end

            it 'allows the request' do
              assert_response :success
            end
          end

          describe '`sys_auth_sec_inc_sep_2018_enable_blocking` arturo enabled' do
            before do
              Arturo.enable_feature!(:sys_auth_sec_inc_sep_2018_enable_blocking)
              Zendesk::StatsD::Client.any_instance.expects(:event).once
              get :index
            end

            it 'blocks the request with a 403' do
              assert_response 403
            end

            it 'includes a message that the request is unauthorized' do
              assert_equal 'Request must originate from an authorized source', response.body.strip
            end
          end
        end
      end
    end
  end
end
