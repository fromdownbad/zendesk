require_relative "../../support/test_helper"

SingleCov.covered!

describe 'ZendeskDatadogFailureReporter' do
  describe Zendesk::DatadogFailureReporter do
    it "reports failures" do
      backend = Resque::Failure.backend.new(RuntimeError.new, {}, "my-queue", "class" => "Zendesk::Maintenance::Jobs::SatisfactionRatingIntentionJob")
      ::Datadog::Statsd.any_instance.expects(:send_stats).with('zendesk.classic.resque.errors', 1, 'c',
        tags: ['queue:my-queue', 'job:SatisfactionRatingIntentionJob', 'exception_class:RuntimeError'])
      backend.save
    end
  end
end
