require_relative '../../../support/test_helper'

# SingleCov.covered!

describe Zendesk::HtmlEmail::Button do
  describe '#render' do
    let(:href) { 'https://www.redfishbluefish.com' }
    let(:stroke) { '#efefef' }
    let(:fill) { '#666666' }
    let(:color) { 'white' }
    let(:text) { 'Thing 1' }
    let(:width) { '88px' }
    let(:style) { 'margin: 0px 5px;' }

    let(:button) { Zendesk::HtmlEmail::Button.render(href: href, stroke: stroke, fill: fill, color: color, text: text, width: width, style: style) }

    describe 'generates a html button link' do
      let(:rendered) { Nokogiri::HTML(button).css('a').to_s }

      it 'with a href' do
        assert_includes rendered, href
      end

      it 'with border style' do
        assert_includes rendered, "border:1px solid #{stroke};"
      end

      it 'with background-color style' do
        assert_includes rendered, "background-color:#{fill};"
      end

      it 'with color style' do
        assert_includes rendered, "color:#{color};"
      end

      it 'with text' do
        assert_includes rendered, text
      end

      it 'with width' do
        assert_includes rendered, "width:#{width}"
      end

      it 'with additional style appended' do
        assert_includes rendered, style
      end
    end

    describe 'generates a formatted microsoft vector graphic link' do
      let(:rendered) { Nokogiri::HTML(button).xpath("//comment()").to_s }

      it 'with a href' do
        assert_includes rendered, href
      end

      it 'with strokecolor style' do
        assert_includes rendered, "strokecolor=\"#{stroke}\""
      end

      it 'with fillcolor style' do
        assert_includes rendered, "fillcolor=\"#{fill}\""
      end

      it 'with color style' do
        assert_includes rendered, "color:#{color};"
      end

      it 'with text' do
        assert_includes rendered, text
      end

      it 'with additional style appended' do
        assert_includes rendered, style
      end
    end

    describe 'default values if none given' do
      let(:stroke) { '#cccccc' }
      let(:fill) { '#78a300' }
      let(:color) { '#555555' }
      let(:button) { Zendesk::HtmlEmail::Button.render }
      let(:rendered) { Nokogiri::HTML(button).css('a') }

      it 'href defaults to an empty string' do
        assert_empty rendered.first['href']
      end

      it "border color defaults to #cccccc" do
        assert_includes rendered.to_s, "border:1px solid #{stroke};"
      end

      it "background-color defaults to #78a300" do
        assert_includes rendered.to_s, "background-color:#{fill};"
      end

      it "color defaults to dark #555555" do
        assert_includes rendered.to_s, "color:#{color};"
      end

      it 'text defaults to an empty string' do
        assert_empty rendered.css('span').text
      end

      it 'width defaults to 150px' do
        assert_includes rendered.to_s, "width:150px;"
      end
    end
  end
end
