require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::HtmlEmail::RelativeToAbsoluteUrlConverter do
  let(:host) { 'support.zendesk.com' }

  describe 'when html is correct' do
    let(:pre_content) { "<a href='/hc/en-us/whatever'>crap goes here</a><img src='hc/en-us/a_hot_picture.png' /> " }
    let(:content) { Zendesk::HtmlEmail::RelativeToAbsoluteUrlConverter.convert(pre_content, host) }

    it "should convert anchor tags to full urls" do
      assert content.include?('https://support.zendesk.com/hc/en-us/whatever')
    end

    it "should convert image tags to full urls" do
      assert content.include?('https://support.zendesk.comhc/en-us/a_hot_picture.png')
    end
  end

  describe 'when html is not correct' do
    let(:pre_content) { "<body><a href='/hc/en-us/whatever'> I can't do html yo.</a> <img src='hc/en-us/a_hot_picture.png' /> " }
    let(:post_content) { "<body><a href=\"https://support.zendesk.com/hc/en-us/whatever\"> I can't do html yo.</a> <img src=\"https://support.zendesk.comhc/en-us/a_hot_picture.png\"> </body>" }
    let(:content) { Zendesk::HtmlEmail::RelativeToAbsoluteUrlConverter.convert(pre_content, host) }

    it "and nokogiri can parse the html" do
      assert_equal post_content, content
    end

    it "and nokogiri throws an exception" do
      Nokogiri::HTML::DocumentFragment.expects(:parse).throws(StandardError)
      assert_equal pre_content, content
    end
  end

  describe 'when an anchor tag does not contain a href attribute' do
    let(:pre_content) { "<a>No href to be found</a>" }
    let(:content) { Zendesk::HtmlEmail::RelativeToAbsoluteUrlConverter.convert(pre_content, host) }

    it "skips converting the tag" do
      assert_equal pre_content, content
    end
  end

  describe 'when an image tag does not contain a src attribute' do
    let(:pre_content) { "<img>No src to be found" }
    let(:content) { Zendesk::HtmlEmail::RelativeToAbsoluteUrlConverter.convert(pre_content, host) }

    it "skips converting the tag" do
      assert_equal pre_content, content
    end
  end
end
