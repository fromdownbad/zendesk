require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Tickets::UrlBuilder do
  let(:account) do
    Account.new do |a|
      a.subdomain = "acme"
      a.settings.help_center_state = :disabled
      a.settings.prefer_lotus = false
      a.subscription = Subscription.new
      a.host_mapping = "support.acme.com"
      a.settings.web_portal_state = "disabled"
    end
  end
  let(:account_route) { FactoryBot.create(:route, subdomain: account.subdomain, host_mapping: account.host_mapping) }
  let(:brand) { FactoryBot.create(:brand, subdomain: "wombats") }
  let(:ticket) { stub("ticket", nice_id: 42, brand: brand) }
  let(:user) { stub("user", is_agent?: false) }

  before do
    Zendesk::Routes::UrlGenerator.any_instance.stubs(hosted_ssl?: true)
    account.route = account_route
  end

  let(:builder) { Zendesk::Tickets::UrlBuilder.new(account: account, user: user) }

  describe "for agent" do
    before { user.stubs(:is_agent?).returns(true) }

    it "returns the agent ticket page" do
      assert_equal "https://acme.zendesk-test.com/agent/tickets/42", url
    end

    it "returns the ticket page even when help center is enabled" do
      user.stubs(:is_agent?).returns(true)
      Account.any_instance.stubs(help_center_enabled?: true)
      assert_equal "https://acme.zendesk-test.com/agent/tickets/42", url
    end

    describe "when autolinking and ticket is nil" do
      let(:ticket) { nil }

      it "returns no ticket id in path for the page" do
        assert_equal "agent/tickets", path
      end
    end
  end

  describe "for end-user" do
    describe "on an account with the guide product" do
      before do
        user.stubs(:is_agent?).returns(false)
        account.stubs(:help_center_enabled?).returns(true)
      end

      it "returns a branded URL for the request page" do
        assert_equal "https://wombats.zendesk-test.com/hc/requests/42", url
      end

      it "returns a non-branded request url if the ticket has no brand" do
        ticket.stubs(:brand).returns(nil)
        assert_equal "https://support.acme.com/hc/requests/42", url
      end

      it "returns a URL for the request page when lotus is enabled" do
        account.settings.prefer_lotus = true
        assert_equal "https://wombats.zendesk-test.com/hc/requests/42", url
      end

      describe "when params are passed in the options" do
        it "append those parameters to the url" do
          assert_equal "https://wombats.zendesk-test.com/hc/requests/42?intention=1", url(params: {intention: 1})
        end

        describe "when autolinking and the ticket is nil" do
          let(:ticket) { nil }

          it "not append those parameters to the url" do
            assert_equal "hc/requests", path(params: {intention: 1})
          end
        end
      end
    end

    describe "without guide" do
      it "cannot build a url" do
        url.must_be_nil
      end
    end
  end

  def url(options = {})
    builder.url_for(ticket, options)
  end

  def path(options = {})
    builder.path_for(ticket, options)
  end
end
