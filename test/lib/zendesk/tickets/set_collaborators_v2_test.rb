require_relative "../../../support/test_helper"
require_relative "../../../support/collaboration_settings_test_helper"
require_relative '../../../support/agent_test_helper'
require 'zendesk/tickets/set_collaborators_v2'

SingleCov.covered! uncovered: 5

describe Zendesk::Tickets::SetCollaboratorsV2 do
  include AgentTestHelper

  fixtures :accounts, :tickets, :users, :user_identities

  before do
    Account.any_instance.stubs(:has_email_ccs_transform_collaborations?).returns(false)

    @account = accounts(:minimum)
    @account.cc_blacklist = "abcd@gmail.com hansen@google.com"
    @account.save!

    @ticket = tickets(:minimum_1)
    @ticket.will_be_saved_by(submitter)
  end

  def set_email_ccs
    @ticket.set_email_ccs = email_ccs_data
    @ticket.save!
  end

  let(:submitter) { users(:minimum_agent) }

  describe "#set_followers=" do
    let(:agent_user) { users(:minimum_agent) }
    let(:admin_user) { users(:minimum_admin) }
    let(:end_user) { users(:minimum_end_user) }

    describe "with follower_and_email_cc_collaborations setting enabled" do
      describe "with ticket_followers enabled" do
        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: true,
            email_ccs_setting: true
          ).apply
        end

        describe "when followers data contains user id" do
          let(:followers_data) do
            [
              { user_id: agent_user.id, action: :put },
              { user_id: admin_user.id, action: :put }
            ]
          end

          before do
            assert_empty @ticket.followers
            assert_empty @ticket.email_ccs
          end

          it "sets followers" do
            @ticket.set_followers = followers_data
            @ticket.save!
            assert_equal 2, @ticket.followers.size
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>", @ticket.current_collaborators
          end

          describe "and users are blocklisted" do
            before do
              @ticket.account.cc_blacklist = "#{@ticket.account.cc_blacklist} #{agent_user.email} #{admin_user.email}"
            end

            it "does not add the users as followers" do
              @ticket.set_followers = followers_data
              @ticket.save!
              assert_empty @ticket.followers
              assert_nil @ticket.current_collaborators
            end
          end
        end

        describe "when followers data contains user id and email address" do
          let(:followers_data) do
            [
              { user_id: agent_user.id, email: agent_user.email },
              { user_id: admin_user.id, email: admin_user.email }
            ]
          end

          before do
            assert_empty @ticket.followers
            @ticket.set_followers = followers_data
            @ticket.save!
          end

          it "sets followers" do
            assert_equal 2, @ticket.followers.size
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when followers data contains users who are suspended" do
          let(:followers_data) do
            [
              { email: agent_user.email, action: :put },
              { user_id: admin_user.id, email: admin_user.email, action: :put }
            ]
          end

          it "adds them as email_ccs" do
            agent_user.suspend!
            assert agent_user.suspended?
            assert_empty @ticket.collaborations
            assert_empty @ticket.followers
            @ticket.set_followers = followers_data
            @ticket.save!

            assert_equal 2, @ticket.collaborations.reload.size
            assert_equal 2, @ticket.followers.size
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when followers data contains existing collaborators who are now suspended" do
          let(:followers_data) do
            [
              { email: agent_user.email, action: :put },
              { user_id: admin_user.id, email: admin_user.email, action: :put }
            ]
          end

          it "does not remove the suspended collaborators" do
            [agent_user, admin_user].each do |user|
              @ticket.collaborations.create(user: user, collaborator_type: CollaboratorType.FOLLOWER)
            end
            assert_equal 2, @ticket.collaborations.size
            assert_equal 2, @ticket.followers.size
            @ticket.set_followers = followers_data
            @ticket.save!

            agent_user.suspend!
            assert agent_user.suspended?
            @ticket.set_followers = followers_data
            @ticket.save!
            assert_equal 2, @ticket.collaborations.reload.size
            assert_equal 2, @ticket.followers.size
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when followers data does not contain existing collaboration with type of FOLLOWER" do
          let(:followers_data) do
            [
              { user_id: admin_user.id, email: admin_user.email }
            ]
          end

          before do
            @ticket.collaborations.create!(user: agent_user, collaborator_type: CollaboratorType.FOLLOWER)
            @ticket.collaborations.create!(user: end_user, collaborator_type: CollaboratorType.EMAIL_CC)
            assert_equal 1, @ticket.followers.size
            @ticket.set_followers = followers_data
            @ticket.save!
          end

          it "does not remove the missing followers" do
            assert_equal 2, @ticket.followers.size
          end

          it "includes data from all collaborations" do
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>, "\
              "minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when followers data contains user id that isn't already a collaborator and is set to delete" do
          let(:followers_data) do
            [
              { user_id: admin_user.id, action: :delete }
            ]
          end

          before do
            @ticket.set_followers = followers_data
            @ticket.save!
          end

          it "does not save any followers" do
            assert_empty @ticket.followers
          end
        end

        describe "when existing current_collaborators does not have email addresses" do
          let(:followers_data) do
            [
              { user_id: admin_user.id, email: admin_user.email }
            ]
          end

          before do
            @ticket.collaborations.create!(user: agent_user, collaborator_type: CollaboratorType.FOLLOWER)
            @ticket.update_column(:current_collaborators, "Agent Minimum")
            @ticket.set_followers = followers_data
            @ticket.save!
          end

          it "does not set emails on new current_collaborators" do
            assert_equal 2, @ticket.followers.size
            assert_equal "Admin Man, Agent Minimum", @ticket.current_collaborators
          end
        end

        describe "when follower data contains email and name for non-existing user" do
          let(:followers_data) do
            [
              { email: "joe@example.com", name: "Joe Plum" },
              { email: "jane@example.com", name: "Jane Plum" }
            ]
          end

          before do
            assert_empty @ticket.followers
            @ticket.set_followers = followers_data
            @ticket.save!
          end

          it "does not set followers" do
            assert_empty @ticket.followers
          end
        end

        describe 'when followers data contains end users' do
          let(:end_user)       { users(:minimum_end_user) }
          let(:followers_data) { [{ user_id: end_user.id, action: :put }] }
          let(:ticket)         { tickets(:minimum_1) }

          before do
            ticket.set_followers = followers_data

            ticket.will_be_saved_by(submitter)
            ticket.save!
          end

          it 'does not add them to ticket collaborations' do
            assert_empty ticket.collaborations
          end
        end

        describe 'when there are existing collaborations' do
          let(:user)   { users(:minimum_end_user) }
          let(:ticket) { tickets(:minimum_1) }

          before do
            ticket.collaborations.create!(
              user: user,
              collaborator_type: CollaboratorType.FOLLOWER
            )
          end

          it 'ticket contains collaborations' do
            refute_empty ticket.collaborations
          end

          describe 'and there is request to remove them' do
            let(:followers_data) { [{ user_id: end_user.id, action: :delete }] }

            before do
              ticket.set_followers = followers_data

              ticket.will_be_saved_by(submitter)
              ticket.save!
            end

            it 'does not add them to ticket collaborations' do
              assert_empty ticket.collaborations
            end
          end
        end

        describe "when a collaboration is created without a user" do
          let(:collaboration_without_user) do
            Collaboration.new(
              ticket: @ticket,
              collaborator_type: CollaboratorType.FOLLOWER
            )
          end

          let(:error_message) do
            'Invalid user in update_current_collaborators ' \
            "for account_id: #{@ticket.account.id} " \
            "in ticket_id: #{@ticket.id} for collaboration: #{collaboration_without_user.id} "\
            "with collaboration_type: #{collaboration_without_user.collaborator_type}"
          end

          before do
            @ticket.stubs(in_flight_collaborations: [collaboration_without_user])
            Rails.logger.stubs(:info)
          end

          it "sets followers without raising a NoMethodError" do
            @ticket.set_followers = []
          end

          it "logs that the user is invalid" do
            Rails.logger.expects(:info).with(error_message)
            @ticket.set_followers = []
          end
        end
      end

      describe "with ticket_followers disabled" do
        let(:followers_data) do
          [
            { user_id: agent_user.id, action: :put },
            { user_id: admin_user.id, action: :put }
          ]
        end

        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: false,
            email_ccs_setting: true
          ).apply
        end

        it { assert_empty @ticket.collaborations.followers }

        describe "when force_collaboration mode is disabled" do
          before do
            @ticket.set_followers = followers_data
            @ticket.save!
          end

          it "does not set follower collaborators" do
            assert_empty @ticket.collaborations.followers
          end

          it "does not update current_collaborators" do
            assert_nil @ticket.current_collaborators
          end
        end

        describe "when force_collaboration mode is enabled" do
          before do
            @ticket.force_collaboration = true
            @ticket.set_followers = followers_data
            @ticket.save!
          end

          it "sets follower collaborators with correct type" do
            assert_equal 2, @ticket.collaborations.followers.size
          end

          it "ticket should still act like followers are empty" do
            assert_empty @ticket.followers
          end

          it "updates current_collaborators" do
            assert @ticket.current_collaborators.include? "#{agent_user.name} <#{agent_user.email}>"
            assert @ticket.current_collaborators.include? "#{admin_user.name} <#{admin_user.email}>"
          end
        end
      end
    end

    describe "with follower_and_email_cc_collaborations setting disabled" do
      let(:followers_data) do
        [
          { user_id: agent_user.id, action: :put },
          { user_id: admin_user.id, action: :put }
        ]
      end

      describe "with force_collaboration disabled" do
        before { @ticket.force_collaboration = false }

        it "does not set followers" do
          Ticket.any_instance.expects(:update_collaborators).with(collaborators: "#{agent_user.id},#{admin_user.id}")
          @ticket.set_followers = followers_data
          @ticket.save
          assert_empty @ticket.collaborations.followers
        end
      end

      describe "with force_collaboration enabled" do
        before { @ticket.force_collaboration = true }

        describe_with_arturo_setting_enabled :ticket_followers_allowed do
          it "sets followers" do
            @ticket.set_followers = followers_data
            assert_equal 2, @ticket.collaborations.size
            assert @ticket.collaborations.all? { |c| c.collaborator_type == CollaboratorType.FOLLOWER }
          end
        end

        describe_with_arturo_setting_disabled :ticket_followers_allowed do
          it "still sets followers" do
            @ticket.set_followers = followers_data
            assert_equal 2, @ticket.collaborations.size
            assert @ticket.collaborations.all? { |c| c.collaborator_type == CollaboratorType.FOLLOWER }
          end
        end
      end
    end
  end

  describe "#set_email_ccs=" do
    let(:agent_user) { users(:minimum_agent) }
    let(:admin_user) { users(:minimum_admin) }
    let(:end_user) { users(:minimum_end_user) }

    describe "with follower_and_email_cc_collaborations setting enabled" do
      describe "with comment_email_ccs enabled" do
        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: true,
            email_ccs_setting: true
          ).apply
        end

        describe "when email_ccs data contains user name" do
          describe "when the name is bigger than 2 letters" do
            let(:email_ccs_data) do
              [
                { email: 'secret.agent.m@example.com', name: 'Agent M', action: :put }
              ]
            end

            before { set_email_ccs }

            it "sets email_ccs including the user name & email" do
              assert_equal "Agent M <secret.agent.m@example.com>", @ticket.current_collaborators
            end
          end

          describe "when the name is less than 2 letters" do
            let(:email_ccs_data) do
              [
                { email: 'secret.agent.m@example.com', name: 'M', action: :put }
              ]
            end

            before { set_email_ccs }

            it "sets email_ccs but includes 'Unknown' as part of the user name" do
              assert_equal "Unknown M <secret.agent.m@example.com>", @ticket.current_collaborators
            end
          end
        end

        describe "when email_ccs data does not contain a user name" do
          describe "when Zendesk::Mail::Address.parse fails to create an Address object with non-nil :name attribute" do
            let(:email_ccs_data) do
              [
                { email: 'secret.agent.m@example.com', action: :put }
              ]
            end

            before do
              Zendesk::Mail::Address.any_instance.stubs(:name).returns(nil)
              Zendesk::StatsD::Client.any_instance.stubs(:increment)
              Zendesk::StatsD::Client.any_instance.expects(:increment).with("collaborators.new_user_data_name_nil").once

              set_email_ccs
            end

            it "sets emails_ccs and and gives the new user the name 'Unknown'" do
              assert_equal "Unknown <secret.agent.m@example.com>", @ticket.current_collaborators
            end
          end
        end

        describe "when email_ccs data contains user id" do
          let(:email_ccs_data) do
            [
              { user_id: agent_user.id, action: :put },
              { user_id: admin_user.id, action: :put },
              { user_id: end_user.id, action: :put }
            ]
          end

          before do
            assert_empty @ticket.email_ccs
          end

          it "sets email_ccs" do
            set_email_ccs
            assert_equal 3, @ticket.email_ccs.size
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>, "\
              "minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end

          describe "and users are blocklisted" do
            before do
              @ticket.account.cc_blacklist = "#{@ticket.account.cc_blacklist} #{agent_user.email} #{admin_user.email} #{end_user.email}"
            end

            it "does not add the users as email ccs" do
              set_email_ccs
              assert_empty @ticket.email_ccs
              assert_nil @ticket.current_collaborators
            end
          end
        end

        describe "when email_ccs data contains user id and email address" do
          let(:email_ccs_data) do
            [
              { user_id: agent_user.id, email: agent_user.email, action: :put },
              { user_id: admin_user.id, email: admin_user.email, action: :put },
              { user_id: end_user.id, email: end_user.email, action: :put }
            ]
          end

          before do
            assert_empty @ticket.email_ccs
            set_email_ccs
          end

          it "sets email_ccs" do
            assert_equal 3, @ticket.email_ccs.size
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>, "\
              "minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when email_ccs data contains users who are suspended" do
          let(:users_to_suspend) { [agent_user, end_user] }
          let(:email_ccs_data) do
            [
              { email: agent_user.email, action: :put },
              { user_id: admin_user.id, email: admin_user.email, action: :put },
              { email: end_user.email, action: :put }
            ]
          end

          it "adds them as email_ccs" do
            users_to_suspend.each(&:suspend!)

            assert_empty @ticket.collaborations
            assert_empty @ticket.email_ccs
            set_email_ccs

            assert_equal 3, @ticket.collaborations.reload.size
            assert_equal 3, @ticket.email_ccs.size
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>, "\
              "minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when email_ccs data contains data about existing collaborations" do
          let(:email_ccs_data) do
            [
              { email: agent_user.email.upcase, action: :put },
              { user_id: admin_user.id, action: :put },
              { email: end_user.email, action: :put }
            ]
          end

          before do
            [agent_user, admin_user, end_user].each do |user|
              @ticket.collaborations.create(user: user, collaborator_type: CollaboratorType.EMAIL_CC)
            end
            assert_equal 3, @ticket.collaborations.size
            assert_equal 3, @ticket.email_ccs.size
            set_email_ccs
          end

          it "does not create duplicate collaborations" do
            assert_equal 3, @ticket.collaborations.reload.size
            assert_equal 3, @ticket.email_ccs.size
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>, "\
              "minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when email_ccs data contains existing collaborators who are now suspended" do
          let(:email_ccs_data) do
            [
              { email: agent_user.email, action: :put },
              { user_id: admin_user.id, email: admin_user.email, action: :put },
              { email: end_user.email, action: :put }
            ]
          end

          it "does not remove the suspended collaborators" do
            [agent_user, admin_user, end_user].each do |user|
              @ticket.collaborations.create(user: user, collaborator_type: CollaboratorType.EMAIL_CC)
            end
            assert_equal 3, @ticket.collaborations.size
            assert_equal 3, @ticket.email_ccs.size
            set_email_ccs

            agent_user.suspend!
            assert agent_user.suspended?
            set_email_ccs
            assert_equal 3, @ticket.collaborations.reload.size
            assert_equal 3, @ticket.email_ccs.size
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>, "\
              "minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when email_ccs data contains new users" do
          let(:email_ccs_data) do
            [
              { email: 'bobby@example.com', action: :put },
              { email: 'janet@example.com', name: 'Janet Jackson', action: :put }
            ]
          end

          before do
            refute @ticket.account.find_user_by_email('bobby@example.com')
            refute @ticket.account.find_user_by_email('janet@example.com')
            assert_empty @ticket.email_ccs
          end

          describe "and account settings have ticket collaborations disabled" do
            before { @ticket.account.stubs(has_comment_email_ccs_allowed_enabled?: false) }

            it "sets no email_ccs or collaborators" do
              set_email_ccs
              assert_empty @ticket.email_ccs
              assert_nil @ticket.current_collaborators
            end

            it "adds an error" do
              set_email_ccs
              assert @ticket.audits.last.events.any? do |event|
                event.to_s.include?("This user does not have permission to create new user records")
              end
            end
          end

          describe "and account settings have ticket collaborations enabled" do
            describe "and the submitter is an end user" do
              let(:submitter) { users(:minimum_author) }

              describe "and account is open" do
                before { @ticket.account.stubs(is_open?: true) }

                describe "and only agents can add collaborators is OFF" do
                  before { @ticket.account.is_collaborators_addable_only_by_agents = false }

                  it "sets new email_ccs and collaborators" do
                    set_email_ccs
                    assert_equal 2, @ticket.email_ccs.size
                    assert_equal "Bobby <bobby@example.com>, Janet Jackson <janet@example.com>", @ticket.current_collaborators
                  end

                  describe "when email_ccs data marks a nonexistent user email address for removal" do
                    let(:email_ccs_data) { [{ email: 'tony@example.com', action: :delete }] }

                    before do
                      refute @ticket.account.find_user_by_email('tony@example.com')
                      assert_empty @ticket.email_ccs
                    end

                    it "sets no email_ccs or collaborators" do
                      set_email_ccs
                      assert_empty @ticket.email_ccs
                      assert_nil @ticket.current_collaborators
                    end
                  end

                  describe "but not all new users are permitted" do
                    let(:email_ccs_data) do
                      [
                        { email: "hansen@google.com", action: :put },
                        { email: "janet@example.com", name: "Janet Jackson", action: :put }
                      ]
                    end

                    it "sets valid email_ccs and collaborators" do
                      set_email_ccs
                      assert_equal 1, @ticket.email_ccs.size
                      assert_equal "Janet Jackson <janet@example.com>", @ticket.current_collaborators
                    end
                  end
                end

                describe "and only agents can add collaborators is ON" do
                  before { @ticket.account.is_collaborators_addable_only_by_agents = true }

                  it "sets new email_ccs and collaborators" do
                    set_email_ccs
                    assert_equal 2, @ticket.email_ccs.size
                    assert_equal "Bobby <bobby@example.com>, Janet Jackson <janet@example.com>", @ticket.current_collaborators
                  end
                end
              end

              describe "when account is not open" do
                before { @ticket.account.stubs(is_open?: false) }

                it "sets no email_ccs or collaborators" do
                  set_email_ccs
                  assert_empty @ticket.email_ccs
                  assert_nil @ticket.current_collaborators
                end

                it "adds an error" do
                  set_email_ccs
                  assert @ticket.audits.last.events.any? do |event|
                    event.to_s.include?("This user does not have permission to create new user records")
                  end
                end
              end
            end
          end

          describe "when the submitter is an agent" do
            let(:submitter) { users(:minimum_agent) }

            it "sets email_ccs and collaborators" do
              set_email_ccs
              assert_equal 2, @ticket.email_ccs.size
              assert_equal "Bobby <bobby@example.com>, Janet Jackson <janet@example.com>", @ticket.current_collaborators
            end

            describe "but not all new users are permitted" do
              let(:email_ccs_data) do
                [
                  { email: "hansen@google.com", action: :put },
                  { email: "janet@example.com", name: "Janet Jackson", action: :put }
                ]
              end

              it "sets valid email_ccs and collaborators" do
                set_email_ccs
                assert_equal 1, @ticket.email_ccs.size
                assert_equal "Janet Jackson <janet@example.com>", @ticket.current_collaborators
              end
            end

            describe "the agent does not have permissions to create new users" do
              before do
                @permission_set = PermissionSet.new
                submitter.account.stubs(:has_permission_sets?).returns(true)
                submitter.stubs(:permission_set).returns(@permission_set)
                submitter.permission_set.permissions.end_user_profile = "readonly"
              end

              it "does not set email_ccs or collaborators" do
                set_email_ccs
                assert_empty @ticket.email_ccs
                assert_nil @ticket.current_collaborators
              end

              it "adds a translatable error event" do
                set_email_ccs
                assert @ticket.audits.last.events.any? do |event|
                  event.to_s.include?("This user does not have permission to create new user records")
                end
              end
            end
          end
        end

        describe 'when email_ccs data contains users without public comment access' do
          let(:light_agent)    { create_light_agent }
          let(:email_ccs_data) { [{ user_id: light_agent.id, action: :put }] }

          subject { set_email_ccs }

          describe_with_arturo_disabled :email_ccs_light_agents_v2 do
            it 'does not add them to ticket collaborations' do
              subject
              assert_empty @ticket.collaborations
            end
          end

          describe_with_arturo_enabled :email_ccs_light_agents_v2 do
            describe_with_arturo_setting_disabled :light_agent_email_ccs_allowed do
              it 'does not add them to ticket collaborations' do
                subject
                assert_empty @ticket.collaborations
              end
            end

            describe_with_arturo_setting_enabled :light_agent_email_ccs_allowed do
              it 'adds them to ticket collaborations' do
                subject
                assert_equal 1, @ticket.email_ccs.size
              end
            end
          end
        end

        describe "when email_ccs data does not contain existing collaboration with type of EMAIL_CC" do
          let(:email_ccs_data) do
            [
              { user_id: admin_user.id, email: admin_user.email, action: :put }
            ]
          end

          before do
            @ticket.collaborations.create!(user: end_user, collaborator_type: CollaboratorType.EMAIL_CC)
            @ticket.collaborations.create!(user: agent_user, collaborator_type: CollaboratorType.FOLLOWER)
            assert_equal 1, @ticket.email_ccs.size
            set_email_ccs
          end

          it "does not remove the missing email ccs" do
            assert_equal 2, @ticket.email_ccs.size
          end

          it "includes data from all collaborations" do
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>, "\
              "minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when email_ccs data contains user id that isn't already a collaborator and is set to delete" do
          let(:email_ccs_data) do
            [
              { user_id: admin_user.id, action: :delete }
            ]
          end

          before { set_email_ccs }

          it "does not save any email ccs" do
            assert_empty @ticket.email_ccs
          end
        end

        describe "when existing current_collaborators does not have email addresses" do
          let(:email_ccs_data) do
            [
              { user_id: admin_user.id, email: admin_user.email, action: :put }
            ]
          end

          before do
            @ticket.collaborations.create!(user: agent_user, collaborator_type: CollaboratorType.EMAIL_CC)
            @ticket.update_column(:current_collaborators, "Agent Minimum")
            set_email_ccs
          end

          it "does not set emails on new current_collaborators" do
            assert_equal 2, @ticket.email_ccs.size
            assert_equal "Admin Man, Agent Minimum", @ticket.current_collaborators
          end
        end
      end

      describe "with comment_email_ccs disabled" do
        let(:email_ccs_data) do
          [
            { user_id: agent_user.id, action: :put },
            { user_id: admin_user.id, action: :put }
          ]
        end

        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: true,
            email_ccs_setting: false
          ).apply
        end

        it { assert_empty @ticket.collaborations.email_ccs }

        describe "when force_collaboration mode is disabled" do
          before { set_email_ccs }

          it "does not set email_cc collaborators" do
            assert_empty @ticket.collaborations.email_ccs
          end

          it "does not update current_collaborators" do
            assert_nil @ticket.current_collaborators
          end
        end

        describe "when force_collaboration mode is enabled" do
          before do
            @ticket.force_collaboration = true
            set_email_ccs
          end

          it "sets email_cc collaborators with correct type" do
            assert_equal 2, @ticket.collaborations.email_ccs.size
          end

          it "ticket should still act like email_ccs are empty" do
            assert_empty @ticket.email_ccs
          end

          it "updates current_collaborators" do
            assert @ticket.current_collaborators.include? "#{agent_user.name} <#{agent_user.email}>"
            assert @ticket.current_collaborators.include? "#{admin_user.name} <#{admin_user.email}>"
          end
        end
      end
    end

    describe "with follower_and_email_cc_collaborations setting disabled" do
      let(:email_ccs_data) do
        [
          { user_id: agent_user.id, action: :put },
          { user_id: admin_user.id, action: :put }
        ]
      end

      describe "with force_collaboration disabled" do
        before { @ticket.force_collaboration = false }

        it "does not set email_ccs" do
          Ticket.any_instance.expects(:update_collaborators).with(collaborators: "#{agent_user.id},#{admin_user.id}")
          @ticket.set_email_ccs = email_ccs_data
          @ticket.save
          assert_empty @ticket.collaborations
        end
      end

      describe "with force_collaboration enabled" do
        before { @ticket.force_collaboration = true }

        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it "sets email_ccs" do
            @ticket.set_email_ccs = email_ccs_data
            assert_equal 2, @ticket.collaborations.size
            assert @ticket.collaborations.all? { |c| c.collaborator_type == CollaboratorType.EMAIL_CC }
          end
        end

        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          it "still sets email_ccs" do
            @ticket.set_email_ccs = email_ccs_data
            assert_equal 2, @ticket.collaborations.size
            assert @ticket.collaborations.all? { |c| c.collaborator_type == CollaboratorType.EMAIL_CC }
          end
        end
      end
    end
  end
end
