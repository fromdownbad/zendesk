require_relative "../../../support/test_helper"
require 'zendesk/tickets/recent_ticket_manager'

SingleCov.covered! uncovered: 1

describe Zendesk::Tickets::RecentTicketManager do
  fixtures :tickets, :users

  before do
    @ticket = tickets(:minimum_1)
    @user = users(:minimum_agent)
    @store = stub("store", read: [@ticket.id], write: nil)

    @manager = Zendesk::Tickets::RecentTicketManager.new(@user, store: @store)
  end

  describe "initialization" do
    it "raises an ArgumentError is user is nil" do
      assert_raise ArgumentError do
        Zendesk::Tickets::RecentTicketManager.new(nil, store: stub)
      end
    end
  end

  describe "recent_tickets" do
    describe "with a ticket" do
      before do
        @tickets = @manager.recent_tickets
      end

      before_should "read from the store" do
        @store.expects(:read)
      end

      it "returns a list" do
        assert_equal [@ticket], @tickets
      end
    end

    describe "with a list of tickets" do
      subject { [tickets(:minimum_2), @ticket, tickets(:minimum_3)] }

      before do
        @store.stubs(:read).returns(subject.map(&:id))
        @tickets = @manager.recent_tickets
      end

      it "returns them in order" do
        assert_equal subject, @tickets
      end
    end
  end

  describe "remove_ticket" do
    before do
      @manager.remove_ticket(@ticket)
    end

    before_should "remove ticket id" do
      @store.expects(:write).with([])
    end
  end

  describe "add_ticket" do
    describe "with no entries" do
      before do
        @store.stubs(:read).returns([])
        @manager.add_ticket(@ticket)
      end

      before_should "add ticket" do
        @store.expects(:write).with([@ticket.id])
      end
    end

    describe "with existing entries in the list" do
      before do
        @store.stubs(:read).returns([1, @ticket.id, 2])
        @manager.add_ticket(@ticket)
      end

      before_should "unshift new ticket" do
        @store.expects(:write).with([@ticket.id, 1, 2])
      end
    end

    describe "when the list is full" do
      before do
        @store.stubs(:read).returns((1..5).to_a)
        @manager.add_ticket(@ticket)
      end

      before_should "write new ticket ids" do
        @store.expects(:write).with([@ticket.id, 1, 2, 3, 4])
      end
    end
  end
end
