require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Tickets::RequesterDataParser do
  include Zendesk::Tickets::RequesterDataParser

  describe "parsing requester_data" do
    it "returns nil when blank or not a string" do
      assert_nil parse_requester_data("")
      assert_nil parse_requester_data(nil)
      assert_nil parse_requester_data([])
      assert_nil parse_requester_data([-1])
      assert_nil parse_requester_data(["arf"])
      assert_nil parse_requester_data(-1)
    end

    it "returns hash with name and e-mail" do
      hash = { email: "joe.smith@host.com", name: "Joe Smith" }
      assert_equal hash, parse_requester_data("Joe Smith <joe.smith@host.com>")
    end

    it "returns hash with name and e-mail when only e-mail is present" do
      hash = { email: "joe.smith@host.com", name: "Joe Smith" }
      assert_equal hash, parse_requester_data("joe.smith@host.com")
    end

    it "returns nil when no email is present" do
      assert_nil parse_requester_data("Joe")
      assert_nil parse_requester_data("Joe Doe")
      assert_nil parse_requester_data(name: "Joe Smith")
    end

    it "resolves a hash" do
      hash = { email: "joe.smith@host.com", name: "Joe Smith" }
      assert_equal hash, parse_requester_data(hash.merge(hello: "Key"))
    end
  end
end
