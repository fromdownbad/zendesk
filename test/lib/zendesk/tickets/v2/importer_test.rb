require_relative "../../../../support/test_helper"
require 'zendesk/tickets/v2/importer'

SingleCov.covered!

describe Zendesk::Tickets::V2::Importer do
  fixtures :all

  let(:user) { users(:minimum_agent) }
  let(:account) { user.account }

  before do
    Timecop.freeze(Date.today)
    @importer = Zendesk::Tickets::V2::Importer.new(account: account, user: user)
  end

  describe "importing a ticket with comments" do
    before do
      attrs = ticket_attrs(
        created_at: 3.weeks.ago,
        updated_at: 2.days.ago,
        solved_at: 3.days.ago,
        tags: "foo bar",
        comments: [
          comment_attrs(value: "comment 1"),
          comment_attrs(value: "comment 2", author_id: user.id, is_public: nil, public: false)
        ]
      )

      @sql_queries = sql_queries do
        @ticket = @importer.import(attrs).reload
      end
    end

    it "does not make too many sql queries" do
      assert_sql_queries 0..130
    end

    it "correctly set the author id of the first comment" do
      @ticket.comments.first.author_id.must_equal @ticket.submitter.id
    end
  end

  private

  def assert_same_time(expected, actual)
    assert_equal expected.to_i, actual.to_i, "#{expected.inspect} expected but was\n#{actual.inspect}"
  end

  def ticket_attrs(options = {})
    tickets(:minimum_4).attributes.symbolize_keys!.update(options).
      except(:account_id, :nice_id)
  end

  def comment_attrs(options = {})
    {
      value: "some comment",
      author_id: users(:minimum_end_user).id,
      is_public: true,
      created_at: 2.weeks.ago
    }.update(options)
  end
end
