require_relative "../../../../support/test_helper"
require 'zendesk/tickets/v2/initializer'

SingleCov.covered! uncovered: 1

describe 'Zendesk::Tickets::V2::Initializer' do
  fixtures :all

  describe "default submitter" do
    before do
      @submitter = users(:minimum_admin)
      @requester = users(:minimum_end_user)

      @ticket_initializer = Zendesk::Tickets::V2::Initializer.new(
        accounts(:minimum),
        @submitter,
        ticket: {
          subject: "hi",
          requester: { email: @requester.email }
        },
        comment: { value: "OMGWTFBBQ", is_public: false }
      )

      @ticket = @ticket_initializer.ticket
    end

    it "sets the submitter to the requester" do
      assert_equal @ticket.requester, @ticket.submitter
    end

    it "sets the comment author to the submitter" do
      assert_equal @ticket.comment.author, @ticket.submitter
    end
  end

  describe "ticket subject" do
    before { @submitter = users(:minimum_admin) }

    describe "with raw_subject present" do
      before do
        params = {ticket: {subject: "hi", raw_subject: "{{foo}} bar"}}
        ticket_initializer = Zendesk::Tickets::V2::Initializer.new(accounts(:minimum), @submitter, params)
        @ticket = ticket_initializer.ticket
      end

      it "overwrites the subject if raw subject is present" do
        assert_equal "{{foo}} bar", @ticket.subject
      end
    end

    describe "with raw_subject not present" do
      before do
        params = {ticket: {subject: "hi"}}
        ticket_initializer = Zendesk::Tickets::V2::Initializer.new(accounts(:minimum), @submitter, params)
        @ticket = ticket_initializer.ticket
      end

      it "does not overwrite the subject if raw_subject is not present" do
        assert_equal "hi", @ticket.subject
      end
    end

    describe "with raw_subject as integer" do
      before do
        params = { ticket: { raw_subject: 123456 } }
        ticket_initializer = Zendesk::Tickets::V2::Initializer.new(accounts(:minimum), @submitter, params)
        @ticket = ticket_initializer.ticket
      end

      it "converts raw_string with integer to a string" do
        assert @ticket.subject.is_a?(String)
      end
    end

    describe "with raw_subject as boolean" do
      before do
        params = { ticket: { raw_subject: true } }
        ticket_initializer = Zendesk::Tickets::V2::Initializer.new(accounts(:minimum), @submitter, params)
        @ticket = ticket_initializer.ticket
      end

      it "converts raw_string with boolean to a string" do
        assert @ticket.subject.is_a?(String)
      end
    end
  end

  describe "#add_flags" do
    let(:account) { accounts(:minimum) }
    let(:user) { account.owner }

    before do
      Arturo.enable_feature!(:on_behalf_of_user_flag)
    end

    describe "when the comment is posted on behalf of an admin by an admin" do
      before do
        params = {
          ticket: {
            comment: {
              value: "Halps.",
              author_id: users(:minimum_admin_not_owner).id
            }
          }
        }

        @ticket_initializer = Zendesk::Tickets::V2::Initializer.new(account, user, params)

        @ticket = @ticket_initializer.ticket
      end

      it "adds flags" do
        assert_equal [11], @ticket.audit.metadata[:flags]
      end
    end

    describe "when the comment is posted on behalf of an agent by an agent" do
      let(:user) { users(:minimum_agent) }

      before do
        params = {
          ticket: {
            comment: {
              value: "Halps.",
              author_id: users(:minimum_admin).id
            }
          }
        }

        @ticket_initializer = Zendesk::Tickets::V2::Initializer.new(account, user, params)

        @ticket = @ticket_initializer.ticket
      end

      it "adds flags" do
        assert_equal [11], @ticket.audit.metadata[:flags]
      end
    end

    describe "when the comment is posted on behalf of an admin by an agent" do
      let(:user) { users(:minimum_agent) }

      before do
        params = {
          ticket: {
            comment: {
              value: "Halps.",
              author_id: users(:minimum_admin).id
            }
          }
        }

        @ticket_initializer = Zendesk::Tickets::V2::Initializer.new(account, user, params)

        @ticket = @ticket_initializer.ticket
      end

      it "adds flags" do
        assert_equal [11], @ticket.audit.metadata[:flags]
      end
    end

    describe "when the comment is posted on behalf of an agent by an admin" do
      before do
        params = {
          ticket: {
            comment: {
              value: "Halps.",
              author_id: users(:minimum_agent).id
            }
          }
        }

        @ticket_initializer = Zendesk::Tickets::V2::Initializer.new(account, user, params)

        @ticket = @ticket_initializer.ticket
      end

      it "does not add flag" do
        assert_nil @ticket.audit.metadata[:flags]
      end
    end

    describe "when the comment is posted on behalf of an end-user" do
      before do
        params = {
          ticket: {
            comment: {
              value: "Halps.",
              author_id: users(:minimum_end_user).id
            }
          }
        }

        @ticket_initializer = Zendesk::Tickets::V2::Initializer.new(account, user, params)

        @ticket = @ticket_initializer.ticket
      end

      it "does not add flag" do
        assert_nil @ticket.audit.metadata[:flags]
      end
    end

    describe "when the ticket is submitted with a submitter_id" do
      before do
        params = {
          ticket: {
            submitter_id: users(:minimum_admin_not_owner).id,
            comment: {
              value: "Halps."
            }
          }
        }

        @ticket_initializer = Zendesk::Tickets::V2::Initializer.new(account, user, params)

        @ticket = @ticket_initializer.ticket
      end

      it "adds flags" do
        assert_equal [11], @ticket.audit.metadata[:flags]
      end
    end
  end
end
