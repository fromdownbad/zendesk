require_relative "../../../../support/test_helper"
require 'zendesk/tickets/anonymous/initializer'

SingleCov.covered! uncovered: 1

describe Zendesk::Tickets::Anonymous::Initializer do
  fixtures :custom_field_options, :tickets, :ticket_fields

  let(:account) { accounts(:minimum) }
  let(:user) { account.anonymous_user }

  let(:initializer) do
    Zendesk::Tickets::Anonymous::Initializer.new(account, user, request: request)
  end

  let(:request) { {} }

  before do
    @account = accounts(:minimum)
  end

  describe "#build_suspended_ticket (prefix:_)" do
    let(:ticket) { tickets(:minimum_1) }
    let(:options) { {} }
    let(:suspended_ticket) { initializer.send(:build_suspended_ticket, @account, ticket, options) }

    it "locale_ids equal the ticket's requester locale_id" do
      skip "FIXME"
      assert ticket.requester.locale_id
      assert_equal ticket.requester.locale_id,
        suspended_ticket.properties[:locale_id]
    end

    describe "if FieldPriority is active, visible in portal, editable in portal" do
      before do
        field = ticket_fields(:minimum_field_priority)
        field.is_active = true
        field.is_visible_in_portal = true
        field.is_editable_in_portal = true
        field.save!
      end

      it "priority_ids equal the ticket's priority_id" do
        assert_equal ticket.priority_id,
          suspended_ticket.properties[:priority_id]
      end
    end

    it "priority_ids is nil" do
      assert_nil suspended_ticket.properties[:priority_id]
    end

    describe "if FieldTicketType is active, visible in portal, editable in portal" do
      before do
        field = ticket_fields(:minimum_field_ticket_type)
        field.is_active = true
        field.is_visible_in_portal = true
        field.is_editable_in_portal = true
        field.save!
      end

      it "ticket_type_ids equal the ticket's ticket_type_id" do
        assert_equal ticket.ticket_type_id,
          suspended_ticket.properties[:ticket_type_id]
      end
    end

    describe "with ticket form set" do
      before do
        ticket.ticket_form_id = 2
      end

      it "ticket_form_ids equal the ticket's ticket_form_id" do
        assert_equal 2, suspended_ticket.properties[:ticket_form_id]
      end
    end

    describe "with brand set" do
      before do
        ticket.brand_id = 1
      end

      it "brand_ids equal the ticket's brand_id" do
        assert_equal 1, suspended_ticket.properties[:brand_id]
      end
    end

    it "ticket_type_ids is nil" do
      assert_nil suspended_ticket.properties[:ticket_type_id]
    end

    describe "ticket field entries" do
      before do
        ticket.ticket_field_entries.build(ticket_field_id: ticket_fields(:field_tagger_custom).id, value: custom_field_options(:field_tagger_custom_option_1).value)
        ticket.ticket_field_entries.build(ticket_field_id: ticket_fields(:field_checkbox_custom).id, value: 0)
      end

      it "has two ticket field entries" do
        assert_equal 2, suspended_ticket.properties[:fields].size
      end

      it "has field tagger" do
        assert_equal custom_field_options(:field_tagger_custom_option_1).value,
          suspended_ticket.properties[:fields][ticket_fields(:field_tagger_custom).id]
      end

      it "has field checkbox" do
        assert_equal '0', suspended_ticket.properties[:fields][ticket_fields(:field_checkbox_custom).id]
      end
    end
  end

  describe "#build_suspended_ticket" do
    let(:request) do
      {
      comment: { value: "test" },
      subject: "test",
      requester: "test@test.com"
    }
    end

    describe "SIGNUP_REQUIRED" do
      before do
        @account.stubs(is_signup_required?: true)
      end

      it "suspends on SIGNUP_REQUIRED" do
        assert_equal SuspensionType.SIGNUP_REQUIRED,
          initializer.anonymous_ticket.cause
      end

      describe "with required fields missing" do
        let(:request) do
          { requester: "test@test.com" }
        end
        before do
          sub_field = account.ticket_fields.find_by_type("FieldSubject")
          sub_field.is_required_in_portal = true
          sub_field.is_active = true
          sub_field.save!
          des_field = account.ticket_fields.find_by_type("FieldDescription")
          des_field.is_required_in_portal = true
          sub_field.is_active = true
          des_field.save!
        end

        it "should return ticket field missing validation errors" do
          exception = assert_raise(Zendesk::Tickets::Anonymous::TicketInvalid) do
            initializer.anonymous_ticket
          end
          assert_equal "Validation failed: Enter your request: cannot be blank, Subject: cannot be blank", exception.message
        end
      end
    end

    describe "Requester is Suspended User" do
      let(:request) do
        {
          comment: { value: "test" },
          subject: "test",
          requester: @suspended_user.email
      }
      end
      before do
        @suspended_user = users(:minimum_end_user)
        @suspended_user.suspend
      end

      describe "and all required fields filled" do
        it "should build a suspended ticket" do
          assert_equal SuspensionType.SPAM, initializer.anonymous_ticket.cause
          assert_equal SuspendedTicket, initializer.anonymous_ticket.class
        end
      end

      describe "and required field is missing" do
        let(:request) do
          { requester: @suspended_user.email }
        end
        before do
          sub_field = account.ticket_fields.find_by_type("FieldSubject")
          sub_field.is_required_in_portal = true
          sub_field.is_active = true
          sub_field.save!
          des_field = account.ticket_fields.find_by_type("FieldDescription")
          des_field.is_required_in_portal = true
          sub_field.is_active = true
          des_field.save!
        end

        it "should return ticket field missing validation errors" do
          exception = assert_raise(Zendesk::Tickets::Anonymous::TicketInvalid) do
            initializer.anonymous_ticket
          end
          assert_equal "Validation failed: Enter your request: cannot be blank, Subject: cannot be blank", exception.message
        end
      end
    end
  end

#   Related to is_anonymous_user? switch inside #ticket
#   If !is_anonymous_user? all roads lead back to returning @ticket
#   but, that's for the next iteration
#   describe "without an anonymous user" do
#     let(:user) { users(:minimum_agent) }
#     let(:request) {{ description: "test", subject: "test" }}
#
#     before do
#       initializer.anonymous_ticket
#     end
#
#     before_should "not apply anonymous changes" do
#       initializer.expects(:apply_anonymous_changes).never
#     end
#   end

  describe "#identity" do
    let(:request) do
      {
      comment: { value: "test" },
      subject: "test"
    }
    end

    it "finds with email" do
      request[:requester_data] = users(:minimum_agent).email
      assert initializer.identity
    end

    it "finds with email even if it has extra spaces" do
      request[:requester_data] = "  #{users(:minimum_agent).email}  "
      assert initializer.identity
    end

    it "finds with hash" do
      request[:requester_data] = {email: users(:minimum_agent).email}
      assert initializer.identity
    end

    it "does not blow up if email is invalid" do
      request[:requester_data] = {email: '?'}
      assert_nil initializer.identity
    end
  end

  describe "untrusted comments" do
    describe "without an identity" do
      let(:request) do
        {
        comment: { value: "test" },
        subject: "test",
        requester: "test@test.com"
      }
      end

      describe "that needs verification" do
        before do
          @account.stubs(is_signup_required?: true)
        end

        describe "valid ticket" do
          it "returns a suspended ticket" do
            assert_instance_of SuspendedTicket, initializer.anonymous_ticket
          end
        end

        describe "invalid ticket" do
          before do
            Ticket.any_instance.stubs(valid?: false)
          end

          it "raises TicketInvalid" do
            assert_raise Zendesk::Tickets::Anonymous::TicketInvalid do
              initializer.anonymous_ticket
            end
          end
        end
      end

      describe "that doesn't need verification" do
        before do
          @account.stubs(is_signup_required?: false)
        end

        it "returns the ticket" do
          assert_instance_of Ticket, initializer.anonymous_ticket
        end

        it "is flagged" do
          assert initializer.anonymous_ticket.audit.flagged?
        end

        it "is not trusted" do
          refute initializer.anonymous_ticket.audit.trusted?
        end
      end
    end

    describe "with an identity" do
      let(:request) do
        {
        comment: { value: "test" },
        subject: "test",
        requester: users(:minimum_agent).email
      }
      end

      describe "that needs verification" do
        it "returns the ticket" do
          assert_instance_of Ticket, initializer.anonymous_ticket
        end

        it "is untrusted" do
          refute initializer.anonymous_ticket.audit.trusted?
        end
      end

      describe "that doesn't need verification" do
        let(:user) { users(:minimum_agent) }

        it "returns the ticket" do
          assert_instance_of Ticket, initializer.anonymous_ticket
        end

        it "is not trusted" do
          refute initializer.anonymous_ticket.audit.trusted?
        end
      end
    end
  end

  describe "no untrusted comments" do
    describe "without an identity" do
      let(:request) do
        {
        comment: { value: "test" },
        subject: "test",
        requester: "test@test.com"
      }
      end

      describe "that doesn't need verification" do
        before do
          @account.stubs(is_signup_required?: false)
        end

        it "returns the ticket" do
          assert_instance_of Ticket, initializer.anonymous_ticket
        end
      end

      describe "that needs verification" do
        before do
          @account.stubs(is_signup_required?: true)
        end

        describe "valid ticket" do
          it "suspends the ticket" do
            assert_instance_of SuspendedTicket, initializer.anonymous_ticket
          end
        end

        describe "invalid ticket" do
          before do
            Ticket.any_instance.stubs(valid?: false)
          end

          it "raises TicketInvalid" do
            assert_raise Zendesk::Tickets::Anonymous::TicketInvalid do
              initializer.anonymous_ticket
            end
          end
        end
      end
    end

    describe "with an identity" do
      let(:request) do
        {
        comment: { value: "test" },
        subject: "test",
        requester: users(:minimum_agent).email
      }
      end

      describe "that needs verification" do
        it "returns the ticket" do
          assert_instance_of Ticket, initializer.anonymous_ticket
        end
      end

      describe "that doesn't need verification" do
        let(:user) { users(:minimum_agent) }

        it "returns the ticket" do
          assert_instance_of Ticket, initializer.anonymous_ticket
        end
      end
    end
  end

  describe "with an unverified identity and a user without any other verified identities" do
    let(:identity) { user_identities(:minimum_end_user) }
    let(:request) do
      {
        comment: { value: "test" },
        subject: "test",
        requester: identity.value
      }
    end

    before { identity.user.identities.each { |identity| identity.update_attribute(:is_verified, false) } }

    describe "in account that needs verification" do
      before { @account.stubs(is_signup_required?: true) }

      it "returns the ticket" do
        assert_instance_of Ticket, initializer.anonymous_ticket
      end

      it "suspends the ticket when the requester has no existing tickets" do
        identity.user.tickets.delete_all
        assert_instance_of SuspendedTicket, initializer.anonymous_ticket
      end
    end

    describe "in account that doesn't need verification/sign up required" do
      before { @account.stubs(is_signup_required?: false) }

      it "returns the ticket" do
        assert_instance_of Ticket, initializer.anonymous_ticket
      end

      it "still returns the ticket even if there are no tickets under the user" do
        identity.user.tickets.delete_all
        assert_instance_of Ticket, initializer.anonymous_ticket
      end
    end
  end

  describe "client info via metadata on a http requets" do
    let(:anonymous_user) { account.anonymous_user }
    let(:initializer) do
      Zendesk::Tickets::Anonymous::Initializer.new(
        account,
        anonymous_user,
        { ticket: @ticket_params },
        request_params: @request_params
      )
    end
    before do
      account.stubs(is_signup_required?: true)

      @request_params = Zendesk::Tickets::HttpRequestParameters.new({})
      @request_params.stubs(:format).returns(Mime[:json])

      # Some datacenter ip we don't want to track
      @request_params.stubs(:remote_ip).returns("127.0.0.1")
      # The user agent from the middleman client not the real user.
      @request_params.stubs(:user_agent).returns("Middleman")
      @request_params.stubs(:internal_client?).returns(true)

      @ticket_params = {
        subject: 'I like these ponies',
        comment: { value: "Halps." },
        requester: { email: "noone@example.com", name: "Pony King" },
        system_metadata: { client: "Firefox with Ponies", ip_address: '192.168.1.77' }
      }
    end

    it "sets user_agent and ip from metadata in #client when is a request" do
      ticket = initializer.anonymous_ticket
      assert_equal "Client: Firefox with Ponies\nIP address: 192.168.1.77", ticket.client
    end

    it "leaves user_agent and ip as they are if no system metadata value are available" do
      @ticket_params.delete(:system_metadata)
      ticket = initializer.anonymous_ticket
      assert_equal "Client: Middleman\nIP address: 127.0.0.1", ticket.client
    end
  end
end
