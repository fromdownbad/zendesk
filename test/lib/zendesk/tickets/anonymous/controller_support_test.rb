require_relative "../../../../support/test_helper"
require "zendesk/tickets/anonymous/controller_support"

SingleCov.covered! uncovered: 6

class Zendesk::Tickets::Anonymous::ControllerSupportTest < ActionController::TestCase
  tests(Class.new(ApplicationController) do
    include Zendesk::Tickets::ControllerSupport
    include Zendesk::Tickets::Anonymous::ControllerSupport
  end)

  describe Zendesk::Tickets::Anonymous::ControllerSupport do
    as_an_agent do
      let(:ticket) { Ticket.new }

      it "has an anonymous initializer" do
        assert_instance_of Zendesk::Tickets::Anonymous::Initializer,
          @controller.send(:anonymous_ticket_initializer)
      end

      before do
        @controller.send(:anonymous_ticket_initializer).stubs(anonymous_ticket: ticket)
      end

      describe "suspended ticket" do
        let(:ticket) { SuspendedTicket.new }

        it "always return a suspended ticket" do
          assert_equal ticket, @controller.send(:initialized_anonymous_ticket)
        end
      end

      describe "ticket" do
        before { @controller.expects(:ensure_access_to_ticket).with(ticket).returns(access) }

        describe "ensure_access -> true" do
          let(:access) { true }

          it "returns ticket if allowed" do
            assert_equal ticket, @controller.send(:initialized_anonymous_ticket)
          end
        end

        describe "ensure_access -> false" do
          let(:access) { false }

          it "returns nil if not allowed" do
            assert_nil @controller.send(:initialized_anonymous_ticket)
          end
        end
      end
    end
  end
end
