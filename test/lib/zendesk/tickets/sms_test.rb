require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe 'Tickets::Sms' do
  fixtures :tickets, :events, :users, :user_identities,
    :sequences, :channels_brands

  before do
    @account = accounts(:minimum)
    @agent = users(:minimum_agent)
    @minimum_end_user = users(:minimum_end_user)
    @ticket = tickets(:minimum_1)
    @ticket.via_id = Zendesk::Types::ViaType.Sms
    @ticket.comment = Comment.new(
      is_public: true,
      body: 'Outgoing text',
      ticket: @ticket,
      author: @agent
    )
    @phone_identity = user_identities(:minimum_search_user_phone)
    @phone_identity.update_attribute :user_id, @ticket.requester.id

    Account.any_instance.stubs(:voice_feature_enabled?).with(:sms).returns true
  end

  describe '#should_create_sms_response?' do
    it 'returns true when all of the conditions for creating an SMS response are true' do
      @ticket.should_create_sms_response?.must_equal true
    end

    it 'returns false when sms is disabled' do
      Account.any_instance.stubs(:voice_feature_enabled?).with(:sms).returns false

      @ticket.should_create_sms_response?.must_equal false
    end

    it 'returns false when comment via_id is the same as ticket via_id' do
      @ticket.comment.via_id = @ticket.via_id

      @ticket.should_create_sms_response?.must_equal false
    end

    it 'returns false when ticket via_id is not SMS' do
      @ticket.via_id = Zendesk::Types::ViaType.Web_form

      @ticket.should_create_sms_response?.must_equal false
    end

    it 'returns false when reply is internal' do
      @ticket.comment.is_public = false

      @ticket.should_create_sms_response?.must_equal false
    end

    it 'returns false when comment author is not an agent' do
      @ticket.comment.author = @minimum_end_user

      @ticket.should_create_sms_response?.must_equal false
    end
  end

  describe '#validate_and_create_sms_response' do
    it 'post an SMS creation request to the SMS service' do
      sms_body = Zendesk::Liquid::TicketContext.render(
        @ticket.comment.body,
        @ticket,
        @ticket.comment.author,
        false,
        @ticket.requester.translation_locale
      )

      ::Sms::RequestMessageJob.expects(:enqueue).with(
        @account.id,
        @ticket.id,
        @ticket.comment.author_id,
        sms_body
      )

      @ticket.validate_and_create_sms_response
    end
  end
end
