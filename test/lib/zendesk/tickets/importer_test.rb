require_relative "../../../support/test_helper"
require 'zendesk/tickets/importer'

SingleCov.covered! uncovered: 2

describe Zendesk::Tickets::Importer do
  fixtures :all

  let(:user) { users(:minimum_agent) }
  let(:account) { user.account }

  before do
    Timecop.freeze(Date.today)
    @importer = Zendesk::Tickets::Importer.new(account: account, user: user)
  end

  describe "passing nil as the argument" do
    it "raises ArgumentError" do
      assert_raise(Zendesk::Tickets::Importer::InvalidParams) { @importer.import(nil) }
    end
  end

  describe "importing a ticket" do
    it "does not execute any triggers" do
      Trigger.any_instance.expects(:execute).never
      @importer.import(ticket_attrs(comments: [comment_attrs, comment_attrs]))
    end

    it "does not cause any cc notifications" do
      TicketNotifier.expects(:notify).never
      @importer.import(ticket_attrs(collaborators: [user.id, { name: "Kjeld", email: "kjeld@example.org" }]))
    end
  end

  describe "importing a ticket without comments" do
    it "creates a ticket" do
      assert_difference 'Ticket.count(:all)' do
        @ticket = @importer.import(ticket_attrs).reload
      end

      assert_not_nil @ticket
    end

    describe "without updated_at" do
      before do
        @ticket = @importer.import(ticket_attrs(updated_at: nil)).reload
      end

      it "sets updated_at to ticket created_at" do
        assert_same_time @ticket.created_at, @ticket.updated_at
      end
    end
  end

  describe "importing a ticket with a description" do
    before do
      attrs = ticket_attrs(
        description: "FIRST POST",
        created_at: 3.weeks.ago,
        comments: [comment_attrs(value: "comment 1")]
      )

      @ticket = @importer.import(attrs).reload
    end

    it "adds 2 comments in total" do
      assert_equal 2, @ticket.comments.count(:all)
    end

    it "adds the description as the first comment" do
      assert_equal "FIRST POST", @ticket.comments.first.body
    end

    it "adds the description with the proper created_at" do
      assert_equal @ticket.created_at, @ticket.audits.first.created_at
    end

    it "stills add the other comment" do
      assert_equal "comment 1", @ticket.comments.second.body
    end
  end

  describe "importing a ticket with comments and a via_id" do
    before do
      attrs = ticket_attrs(
        created_at: 3.weeks.ago,
        updated_at: 2.days.ago,
        solved_at: 3.days.ago,
        tags: "foo bar",
        via: { channel: ViaType.CHAT },
        comments: [
          comment_attrs(value: "comment 1"),
          comment_attrs(value: "comment 2", author_id: user.id, public: false),
          comment_attrs(value: "comment 2", via_id: ViaType.MAIL),
        ]
      )

      @ticket = @importer.import(attrs).reload
    end

    it "sets the ticket via_id" do
      assert_equal ViaType.CHAT, @ticket.via_id
    end

    it "sets the first comment via_id" do
      assert_equal ViaType.CHAT, @ticket.comments[0].via_id
    end

    it "does not set the second comment via_id" do
      assert_equal ViaType.WEB_SERVICE, @ticket.comments[1].via_id
    end

    it "does not set the third comment via_id from params" do
      assert_equal ViaType.WEB_SERVICE, @ticket.comments[2].via_id
    end
  end

  describe "importing a ticket without a via_id" do
    before do
      attrs = ticket_attrs(
        created_at: 3.weeks.ago,
        updated_at: 2.days.ago,
        solved_at: 3.days.ago,
        tags: "foo bar",
        comments: [comment_attrs(value: "comment 1")]
      )

      attrs.delete(:via_id)

      @ticket = @importer.import(attrs).reload
    end

    it "sets the default via_id to WEB_SERVICE" do
      assert_equal ViaType.WEB_SERVICE, @ticket.via_id
    end
  end

  describe "importing a ticket with comments" do
    let(:updated_at) { 2.days.ago }

    before do
      attrs = ticket_attrs(
        created_at: 3.weeks.ago,
        updated_at: updated_at,
        solved_at: 3.days.ago,
        tags: "foo bar",
        comments: [
          comment_attrs(value: "comment 1"),
          comment_attrs(value: "comment 2", author_id: user.id, public: false)
        ]
      )

      @sql_queries = sql_queries do
        @ticket = @importer.import(attrs).reload
      end
    end

    it "does not make too many sql queries" do
      assert_sql_queries 0..130 # why?
    end

    it "creates a ticket" do
      assert @ticket
    end

    it "sets the tags" do
      assert_equal %w[bar foo], @ticket.tag_array.sort
    end

    it "correctly set the author id of the first comment" do
      assert_equal @ticket.requester.id, @ticket.comments.first.author_id
    end

    it "correctly set the author id of the last comment" do
      assert_equal user.id, @ticket.comments.last.author_id
    end

    it "makes comment the publicity specified by the comment params" do
      assert_equal false, @ticket.comments.find_by_value("comment 2").is_public?
    end

    it "makes comments public by default unless specified in the comment params" do
      assert(@ticket.comments.find_by_value("comment 1").is_public?)
    end

    it "adds the comments to the ticket" do
      # The description is added as a comment as well.
      assert_equal 3, @ticket.comments.count(:all)
    end

    it "preserves the ticket's creation time" do
      assert_same_time 3.weeks.ago, @ticket.created_at
    end

    describe "with updated_at" do
      it "preserves the ticket's update time" do
        assert_same_time updated_at, @ticket.updated_at
      end
    end

    describe "without updated_at" do
      let(:updated_at) { nil }

      it "sets updated_at to last comments created_at" do
        assert_same_time @ticket.comments.last.created_at, @ticket.updated_at
      end
    end

    it "preserves the ticket's solved time" do
      assert_same_time 3.days.ago, @ticket.solved_at
    end

    it "preserves the comments' creation time" do
      assert_same_time 2.weeks.ago, @ticket.comments.second.created_at
      assert_same_time 2.weeks.ago, @ticket.comments.third.created_at
    end

    it "preserves the audits' creation time" do
      assert_same_time 2.weeks.ago, @ticket.comments.second.audit.created_at
      assert_same_time 2.weeks.ago, @ticket.comments.third.audit.created_at
    end
  end

  describe "importing ticket with no description" do
    let(:ticket_attributes) do
      ticket_attrs(
        description: nil,
        created_at: 3.weeks.ago,
        updated_at: 2.days.ago,
        solved_at: 3.days.ago,
        comments: comments
      )
    end

    before do
      @ticket = @importer.import(ticket_attributes).reload
    end

    describe "with first comment private and second public" do
      let(:comments) do
        [
          comment_attrs(value: "comment 1", public: false),
          comment_attrs(value: "comment 2", public: true)
        ]
      end

      it "makes the ticket public" do
        assert @ticket.is_public?
      end
    end

    describe "with first comment public and second private" do
      let(:comments) do
        [
          comment_attrs(value: "comment 1", public: true),
          comment_attrs(value: "comment 2", public: false)
        ]
      end

      it "makes the ticket public" do
        assert @ticket.is_public?
      end
    end

    describe "with both comments public" do
      let(:comments) do
        [
          comment_attrs(value: "comment 1", public: true),
          comment_attrs(value: "comment 2", public: true)
        ]
      end

      it "makes the ticket public" do
        assert @ticket.is_public?
      end
    end

    describe "with both comments private" do
      let(:comments) do
        [
          comment_attrs(value: "comment 1", public: false),
          comment_attrs(value: "comment 2", public: false)
        ]
      end

      it "makes the ticket private" do
        assert @ticket.is_private?
      end
    end
  end

  describe "import a ticket with empty comments" do
    it "raises an exception" do
      assert_raise Zendesk::Tickets::Importer::InvalidParams do
        @importer.import(ticket_attrs(
          comments: [{ value: nil, created_at: 2.weeks.ago }]
        ))
      end
    end

    it "raises an exception if comment params is empty" do
      assert_raise Zendesk::Tickets::Importer::InvalidParams do
        @importer.import(ticket_attrs(
          comments: { author_id: "290123986", created_at: "2012-12-12T20:45:18Z", value: "this is a test", public: true}
        ))
      end
    end
  end

  describe "tickets with invalid created_at" do
    describe "created_at in the future" do
      it "raises an exception" do
        error = assert_raise(Zendesk::Tickets::Importer::InvalidParams) do
          attrs = ticket_attrs(
            description: "should raise an exception",
            created_at: 1.minute.from_now,
            comments: [comment_attrs(value: "comment")]
          )
          @ticket = @importer.import(attrs)
        end
        assert_equal error.message, "Created at is invalid. It cannot be in the future"
      end
    end

    describe "created_at in the future with a String object" do
      it "raises an exception" do
        assert_raise(Zendesk::Tickets::Importer::InvalidParams) do
          attrs = ticket_attrs(
            description: "should raise an exception",
            created_at: 1.minute.from_now.to_s,
            comments: [comment_attrs(value: "comment")]
          )
          @ticket = @importer.import(attrs)
        end
      end
    end
  end

  describe "tickets with invalid updated_at" do
    describe "updated_at in the future" do
      it "raises an exception" do
        error = assert_raise(Zendesk::Tickets::Importer::InvalidParams) do
          attrs = ticket_attrs(
            description: "should raise an exception",
            created_at: 1.minute.ago,
            updated_at: 1.minute.from_now,
            comments: [comment_attrs(value: "comment")]
          )
          @ticket = @importer.import(attrs)
        end
        assert_equal error.message, "Updated at is invalid. It cannot be in the future"
      end
    end

    describe "updated_at in the future with a String object" do
      it "raises an exception" do
        assert_raise(Zendesk::Tickets::Importer::InvalidParams) do
          attrs = ticket_attrs(
            description: "should raise an exception",
            created_at: 1.minute.ago.to_s,
            updated_at: 1.minute.from_now.to_s,
            comments: [comment_attrs(value: "comment")]
          )
          @ticket = @importer.import(attrs)
        end
      end
    end
  end

  describe "tickets with invalid solved_at" do
    describe "solved_at in the future" do
      it "raises an exception" do
        error = assert_raise(Zendesk::Tickets::Importer::InvalidParams) do
          attrs = ticket_attrs(
            description: "should raise an exception",
            created_at: 1.minute.ago,
            solved_at: 1.minute.from_now,
            comments: [comment_attrs(value: "comment")]
          )
          @ticket = @importer.import(attrs)
        end
        assert_equal error.message, "Solved at is invalid. It cannot be in the future"
      end
    end

    describe "solved_at in the future with a String object" do
      it "raises an exception" do
        assert_raise(Zendesk::Tickets::Importer::InvalidParams) do
          attrs = ticket_attrs(
            description: "should raise an exception",
            created_at: 1.minute.ago.to_s,
            solved_at: 1.minute.from_now.to_s,
            comments: [comment_attrs(value: "comment")]
          )
          @ticket = @importer.import(attrs)
        end
      end
    end
  end

  describe "tickets with comments with invalid created_at" do
    describe "created_at in the future" do
      it "raises an exception" do
        error = assert_raise(Zendesk::Tickets::Importer::InvalidParams) do
          attrs = ticket_attrs(
            description: "should raise an exception",
            comments: [comment_attrs(value: "comment", created_at: Time.now + 1.minute)]
          )
          @ticket = @importer.import(attrs)
        end
        assert_equal error.message, "Comment is invalid. created_at cannot be pre 1970 or in the future"
      end
    end

    describe "created_at before Unix epoch with a Time object" do
      it "raises an exception" do
        assert_raise(Zendesk::Tickets::Importer::InvalidParams) do
          attrs = ticket_attrs(
            description: "should raise an exception",
            comments: [comment_attrs(value: "comment", created_at: Time.at(-1))]
          )
          @ticket = @importer.import(attrs)
        end
      end
    end

    describe "created_at before Unix epoch with a String object" do
      it "raises an exception" do
        assert_raise(Zendesk::Tickets::Importer::InvalidParams) do
          attrs = ticket_attrs(
            description: "should raise an exception",
            comments: [comment_attrs(value: "comment", created_at: "0001-01-01 00:00:00 UTC")]
          )
          @ticket = @importer.import(attrs)
        end
      end
    end
  end

  describe "importing a ticket comment with a null author id" do
    before do
      attrs = ticket_attrs(
        comments: [comment_attrs(value: "zen comment", author_id: nil)]
      )
      @ticket = @importer.import(attrs)
    end

    it "sets the comment author id to the importing user id" do
      assert_equal user.id, @ticket.comments.last.author_id
    end
  end

  describe "importing a ticket with a comment with an invalid author id" do
    it "raises an exception" do
      error = assert_raise Zendesk::Tickets::Importer::InvalidParams do
        attrs = ticket_attrs(
          created_at: 3.weeks.ago,
          updated_at: 2.days.ago,
          solved_at: 3.days.ago,
          tags: "alpha beta",
          via: { channel: ViaType.CHAT },
          comments: [
            comment_attrs(value: "comment 1"),
            comment_attrs(value: "comment 2", author_id: 9999, public: false)
          ]
        )
        @importer.import(attrs)
      end
      assert_equal error.message, "Author is invalid"
    end
  end

  describe "importing a ticket without a description" do
    describe "without comments" do
      it "raises an exception" do
        assert_raise Zendesk::Tickets::Importer::InvalidParams do
          @importer.import(ticket_attrs(description: nil))
        end
      end
    end

    describe "with a comment" do
      before do
        @ticket = @importer.import(ticket_attrs(
          description: nil,
          comments: [comment_attrs]
        ))
      end

      it "sets the description" do
        assert_equal @ticket.comments.first.body, @ticket.description
      end

      it "adds a comment" do
        assert_equal 1, @ticket.comments.count(:all)
      end

      it "makes the comment public by default" do
        assert @ticket.comments.first.is_public?
      end
    end

    describe "with a private comment" do
      before do
        @ticket = @importer.import(ticket_attrs(
          description: nil,
          comments: [comment_attrs(public: false)]
        ))
      end

      it "makes the comment private" do
        refute @ticket.comments.first.is_public?
      end
    end

    describe "with multiple comments" do
      before do
        @ticket = @importer.import(ticket_attrs(
          description: nil,
          comments: subject
        ))
      end

      describe "all with dates" do
        subject do
          [
            comment_attrs(value: "test1", created_at: 10.days.ago),
            comment_attrs(value: "test2", created_at: 45.days.ago)
          ]
        end

        it "sets the description" do
          assert_equal "test2", @ticket.description
        end

        it "adds two comments" do
          assert_equal 2, @ticket.comments.count(:all)
        end
      end

      describe "some without dates" do
        subject do
          [
            comment_attrs(value: "test2", created_at: nil),
            comment_attrs(value: "test1"),
            comment_attrs(value: "test3", created_at: nil)
          ]
        end

        it "sets the description" do
          assert_equal "test1", @ticket.description
        end

        it "adds three comments" do
          assert_equal 3, @ticket.comments.count(:all)
        end
      end

      describe "none with dates" do
        subject do
          [
            comment_attrs(value: "test1", created_at: nil),
            comment_attrs(value: "test2", created_at: nil)
          ]
        end

        it "sets the description" do
          assert_equal "test1", @ticket.description
        end

        it "adds two comments" do
          assert_equal 2, @ticket.comments.count(:all)
        end
      end

      describe "without specifying publicity for the comments" do
        subject do
          [
            comment_attrs(value: "test1", created_at: 10.days.ago),
            comment_attrs(value: "test2", created_at: 45.days.ago)
          ]
        end

        it "makes the comments public by default" do
          assert_equal 2, @ticket.comments.count(:all)
          @ticket.comments.each { |comment| assert comment.is_public? }
        end
      end

      describe "specifying publicity for the comments" do
        subject do
          [
            comment_attrs(value: "test1", created_at: 10.days.ago, public: true),
            comment_attrs(value: "test2", created_at: 45.days.ago, public: false)
          ]
        end

        it "sets the given publicity" do
          assert_equal 2, @ticket.comments.count(:all)
          refute @ticket.comments.first.is_public?
          assert @ticket.comments.second.is_public?
        end
      end
    end
  end

  describe "importing a ticket with a comment with two attachments" do
    it "adds the attachments to the comment" do
      @ticket = @importer.import(ticket_attrs(comments: [comment_attrs(uploads: "minimum_upload_token")])).reload

      comment = @ticket.comments.last

      assert_equal 2, comment.attachments.size
      assert comment.attachments.include?(attachments(:foreign_file_attachment))
      assert comment.attachments.include?(attachments(:upload_token_attachment))
    end
  end

  describe "setting the created_at value on a ticket import" do
    it "sets the created_at to now if given `nil`" do
      ticket = @importer.import(ticket_attrs(created_at: nil)).reload

      assert_same_time Time.now, ticket.created_at
    end

    it "sets the created_at to now if given an empty string" do
      ticket = @importer.import(ticket_attrs(created_at: "")).reload

      assert_same_time Time.now, ticket.created_at
    end

    it "sets the created_at to now if given a non-Time String" do
      ticket = @importer.import(ticket_attrs(created_at: "I like turtles")).reload

      assert_same_time Time.now, ticket.created_at
    end

    it "sets the created_at to the given value if it's after Unix epoch" do
      y2k = Time.new(2000, 1, 1)
      ticket = @importer.import(ticket_attrs(created_at: y2k.to_s)).reload

      assert_same_time y2k, ticket.created_at
    end

    it "sets the created_at to Unix epoch if given a value before Unix epoch" do
      ticket = @importer.import(ticket_attrs(created_at: "0001-01-01 00:00:00 UTC")).reload

      assert_same_time Time.at(0), ticket.created_at
    end
  end

  describe "setting the updated_at value on a ticket import" do
    describe "when created_at is not explicitly set" do
      it "sets the updated_at to created_at if given nil" do
        ticket = @importer.import(ticket_attrs(updated_at: nil)).reload

        assert_same_time ticket.created_at, ticket.updated_at
      end

      it "sets the updated_at to created_at if given an empty string" do
        ticket = @importer.import(ticket_attrs(updated_at: "")).reload

        assert_same_time ticket.created_at, ticket.updated_at
      end

      it "sets the updated_at to created_at if given a non-Time String" do
        ticket = @importer.import(ticket_attrs(updated_at: "Gotta catch em all")).reload

        assert_same_time ticket.created_at, ticket.updated_at
      end

      it "sets the updated_at to the given value if it's after Unix epoch" do
        y2k = Time.new(2000, 1, 1)
        ticket = @importer.import(ticket_attrs(updated_at: y2k.to_s)).reload

        assert_same_time y2k, ticket.updated_at
      end

      it "sets the updated_at to Unix epoch if given a value before Unix epoch" do
        ticket = @importer.import(ticket_attrs(updated_at: "0001-01-01 00:00:00 UTC")).reload

        assert_same_time Time.at(0), ticket.updated_at
      end
    end

    describe "when created_at is explicitly set" do
      it "sets the updated_at to created_at if given nil" do
        ticket = @importer.import(ticket_attrs(created_at: 1.year.ago, updated_at: nil)).reload

        assert_same_time ticket.created_at, ticket.updated_at
      end

      it "sets the updated_at to created_at if given an empty string" do
        ticket = @importer.import(ticket_attrs(created_at: 1.year.ago, updated_at: "")).reload

        assert_same_time ticket.created_at, ticket.updated_at
      end

      it "sets the updated_at to created_at if given a non-Time String" do
        ticket = @importer.import(ticket_attrs(created_at: 1.year.ago, updated_at: "So say we all")).reload

        assert_same_time ticket.created_at, ticket.updated_at
      end

      it "sets the updated_at to the given value if it's after Unix epoch" do
        y2k = Time.new(2000, 1, 1)
        ticket = @importer.import(ticket_attrs(created_at: 1.year.ago, updated_at: y2k.to_s)).reload

        assert_same_time y2k, ticket.updated_at
      end

      it "sets the updated_at to Unix epoch if given a value before Unix epoch" do
        ticket = @importer.import(ticket_attrs(created_at: 1.year.ago, updated_at: "0001-01-01 00:00:00 UTC")).reload

        assert_same_time Time.at(0), ticket.updated_at
      end
    end
  end

  describe "importing a ticket with multiple rich text comments" do
    before do
      attrs = ticket_attrs(
        comments: [
          {
            html_body: "<div class=\"zd-comment\"><b>This is a HTML comment</b></div>",
            author_id: users(:minimum_end_user).id,
            created_at: 2.weeks.ago
          },
          {
            html_body: "<div class=\"zd-comment\"><b>This is another HTML comment</b></div>",
            author_id: users(:minimum_end_user).id,
            created_at: 2.weeks.ago
          }
        ]
      )

      @ticket = @importer.import(attrs).reload
    end

    it "creates the ticket" do
      assert_not_nil @ticket
    end

    it "adds the comments with rich text" do
      # The description is added as a comment as well.
      assert_equal 3, @ticket.comments.count(:all)
      assert_equal "**This is another HTML comment**", @ticket.comments.last.to_s
    end
  end

  describe "importing a closed ticket" do
    before do
      attrs = ticket_attrs(
        status_id: Zendesk::Types::StatusType.CLOSED,
        comments: [comment_attrs, comment_attrs]
      )

      @ticket = @importer.import(attrs).reload
    end

    it "creates the ticket" do
      assert_not_nil @ticket
    end

    it "adds the comments" do
      # The description is added as a comment as well.
      assert_equal 3, @ticket.comments.count(:all)
    end

    it "creates a closed ticket" do
      assert @ticket.closed?
    end
  end

  describe "importing with invalid parameters" do
    it "raises an ActiveRecord::RecordInvalid" do
      assert_raise Zendesk::Tickets::Importer::InvalidParams do
        @importer.import(subject: nil)
      end
    end
  end

  describe "importing a ticket with a rating" do
    let(:attrs) do
      ticket_attrs(
        status_id: Zendesk::Types::StatusType.SOLVED,
        created_at: 3.weeks.ago,
        updated_at: 2.days.ago,
        solved_at: 3.days.ago
      )
    end

    it "rates the ticket" do
      attrs[:satisfaction_rating] = { score: "bad", comment: "Terrible" }

      assert_difference "Satisfaction::Rating.count(:all)", +1 do
        @ticket = @importer.import(attrs).reload
      end
      assert_equal SatisfactionType.BADWITHCOMMENT, @ticket.satisfaction_rating.score
      assert_equal SatisfactionType.BADWITHCOMMENT, Satisfaction::Rating.last.score
    end

    it "does not rate the ticket when there is no score" do
      attrs[:satisfaction_rating] = { score: "oops", comment: "Terrible" }
      refute_difference "Ticket.count(:all)" do
        assert_raise(Zendesk::Tickets::Importer::InvalidParams) { @importer.import(attrs) }
      end
    end
  end

  describe "importing ticket with metadata within comments" do
    let(:metadata) { [{ "data" => "first comment" }, { "data" => "second comment" }] }

    before do
      @ticket = @importer.import(ticket_attrs(
        description: nil,
        comments: [
          comment_attrs(value: "test1", metadata: metadata[0]),
          comment_attrs(value: "test2", metadata: metadata[1])
        ]
      )).reload

      @custom_metadata = @ticket.audits.map(&:metadata).map { |m| m[:custom] }
    end

    it "adds metadata to each comment audit" do
      assert_equal metadata, @custom_metadata
    end

    describe "when metadata is not a hash" do
      let(:metadata) { ["first comment", ""] }

      it "does not add metadata to the audit" do
        assert_equal [{}, {}], @custom_metadata
      end
    end
  end

  describe "with archive_immediately" do
    before do
      importer = Zendesk::Tickets::Importer.new(account: account, user: user, archive_immediately: true)

      attrs = ticket_attrs(
        status_id: Zendesk::Types::StatusType.CLOSED,
        comments: [comment_attrs, comment_attrs]
      )

      ticket = importer.import(attrs)
      @archived_ticket = Ticket.find_by_id(ticket.id)
    end

    it "creates the ticket" do
      assert_not_nil @archived_ticket
    end

    it "adds the comments" do
      # The description is added as a comment as well.
      assert_equal 3, @archived_ticket.comments.length
    end

    it "creates an archived ticket" do
      assert @archived_ticket.archived?
    end
  end

  private

  def assert_same_time(expected, actual)
    assert_equal expected.to_i, actual.to_i, "#{expected.inspect} expected but was\n#{actual.inspect}"
  end

  def ticket_attrs(options = {})
    tickets(:minimum_4).attributes.symbolize_keys!.update(options).
      except(:account_id, :nice_id)
  end

  def comment_attrs(options = {})
    {
      value: "some comment",
      author_id: users(:minimum_end_user).id,
      created_at: 2.weeks.ago
    }.update(options)
  end
end
