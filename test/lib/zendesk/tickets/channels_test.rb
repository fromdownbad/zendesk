require_relative "../../../support/test_helper"
require_relative "../../../support/channels_test_helper"
require "zendesk/tickets/channels"

SingleCov.covered! uncovered: 1

describe "Tickets::Channels" do
  fixtures :tickets, :events, :monitored_twitter_handles, :users, :user_identities,
    :event_decorations, :facebook_pages, :sequences, :channels_brands

  include ChannelsTestHelper

  def setup_ticket
    @ticket = tickets(:minimum_1)
    @ticket.via_id = ViaType.FACEBOOK_POST
    @ticket.comment = Comment.new(
      is_public: true,
      body: "Outgoing text",
      channel_source_id: 1,
      ticket: @ticket
    )
    @ticket.will_be_saved_by(users(:minimum_agent))
  end

  def setup_facebook_ticket
    @source = @page

    @ticket = tickets(:minimum_1)
    event_decoration = @ticket.comments.first.event_decoration
    data = event_decoration.data
    data.source.zendesk_id = @source.id
    event_decoration.update_column(:data, data)

    @ticket = tickets(:minimum_1)
    @ticket.comment = Comment.new(
      is_public: true,
      body: "ABC",
      channel_source_id: @source.id
    )
    @ticket.will_be_saved_by(users(:minimum_agent))
    @ticket.update_column(:requester_id, users(:minimum_end_user).id)
    @ticket.stubs(:via_facebook?).returns(true)
  end

  def setup_twitter_ticket(comment_is_public = true)
    @source = @mth
    @ticket = tickets(:minimum_1)
    @ticket.comment = Comment.new(
      is_public: comment_is_public,
      body: "ABC",
      channel_source_id: @source.id,
      ticket: @ticket
    )
    @ticket.will_be_saved_by(users(:minimum_agent))
    @ticket.update_column(:requester_id, users(:minimum_end_user).id)

    @ticket.update_column(:via_id, Zendesk::Types::ViaType.TWITTER)

    MonitoredTwitterHandle.any_instance.stubs(:can_reply?).returns(true)
    @external_id = 110542029
    @ticket.requester.identities.twitter.first.update_column(:value, @external_id)
    @payload = read_json("twitter_users_show_110542029_1.1.json")
    @twitter_request =
      stub_request(:get, "https://api.twitter.com/1.1/users/show.json?user_id=#{@external_id}").
        to_return(status: 200, body: @payload.to_json)
  end

  describe Zendesk::Tickets::Channels do
    before do
      Channels::Converter::ReplyInitiator.any_instance.stubs(:reply) # fails otherwise and triggers a exception record

      @account = accounts(:minimum)

      @mth = monitored_twitter_handles(:minimum_monitored_twitter_handle_1)
      @page = facebook_pages(:minimum_facebook_page_1)
      @ticket = tickets(:minimum_1)
      @ticket.update_column(:requester_id, users(:minimum_end_user).id)
    end

    describe "#twitter_source" do
      before do
        setup_twitter_ticket
      end

      describe "Via Type is TWITTER" do
        it "returns an active monitored twitter handle" do
          assert_equal @mth, @ticket.twitter_source
        end

        it "returns nil if the requested handle is inactive" do
          @source.deactivate!
          assert_nil @ticket.twitter_source
        end
      end

      describe "Via Type is TWITTER_DM" do
        before do
          @ticket.via_id = ViaType.TWITTER_DM
          @dm_source = monitored_twitter_handles(:minimum_monitored_twitter_handle_2)
          event_decoration = @ticket.comments.first.event_decoration
          event_decoration.data = { source: { zendesk_id: @dm_source.id} }
          event_decoration.save!
        end

        it "returns an active monitored twitter handle" do
          assert_equal @dm_source, @ticket.twitter_source
        end

        it "returns nil if the requested handle is inactive" do
          @dm_source.deactivate!
          assert_nil @ticket.twitter_source
        end
      end
    end

    describe "#twitter_ticket_source" do
      before do
        setup_twitter_ticket
      end

      it "returns false if not via twitter" do
        @ticket.expects(:via_twitter?).returns(false)
        refute @ticket.twitter_ticket_source
      end

      it "returns ticket_source if via twitter" do
        @ticket.expects(:via_twitter?).returns(true)
        @ticket.expects(:ticket_source).returns(@source)
        assert_equal @source, @ticket.twitter_ticket_source
      end
    end

    describe "#ticket_source" do
      before do
        setup_twitter_ticket

        @ticket.stubs(:ticket_source_id).returns(@source.id)
      end

      it "returns an active monitored twitter handle defined by ticket_source_id" do
        assert_equal @source, @ticket.ticket_source
      end

      it "returns an active monitored twitter handle defined by ticket_source_id when another one is passed in" do
        @other_source = monitored_twitter_handles(:minimum_monitored_twitter_handle_2)
        @ticket.comment.channel_source_id = @other_source.id
        assert_equal @source, @ticket.ticket_source
      end

      describe "when the action passed in is not 'follows!'" do
        describe "when the source is deactivated" do
          before do
            @source.deactivate!
          end

          it "returns the first active mth with can_reply? if the requested handle is inactive" do
            expected = MonitoredTwitterHandle.where(account_id: @ticket.account_id).active.find(&:can_reply?)
            assert_equal expected, @ticket.ticket_source
          end
        end
      end

      describe "when the action passed in is 'follows!'" do
        describe "when the source is deactivated" do
          before do
            @source.deactivate!
          end

          it "returns the first active mth if the requested handle is inactive" do
            expected = MonitoredTwitterHandle.where(account_id: @ticket.account_id).active.first
            assert_equal expected, @ticket.ticket_source
          end
        end
      end
    end

    describe "#ticket_source_id" do
      before do
        setup_twitter_ticket(false)
      end

      it "returns the source with event decorations" do
        decoration = EventDecoration.first
        decoration.update_attributes!(data: { source: { zendesk_id: @source.id } })
        assert_equal @source.id, @ticket.ticket_source_id
      end

      it "returns the latest source when there are multiple event decorations" do
        @other_source = monitored_twitter_handles(:minimum_monitored_twitter_handle_2)
        @ticket.save!
        @ticket.comments.last.create_event_decoration(data: { source: { zendesk_id: @other_source.id } })
        assert_equal @other_source.id, @ticket.ticket_source_id
      end

      it "returns nil if there are no event decorations" do
        EventDecoration.destroy_all
        refute @ticket.ticket_source_id
      end
    end

    describe '#handle_reply_options' do
      before do
        setup_twitter_ticket
      end

      describe 'comment.reply_option is not present' do
        describe 'should_create_sms_response? returns true' do
          before do
            @ticket.expects(:should_create_sms_response?).returns(true)
          end

          it 'performs pre-existing logic (calls create_sms_response)' do
            @ticket.expects(:create_channel_back_event)
            @ticket.expects(:create_sms_response)
            @ticket.handle_reply_options
          end
        end

        describe 'should_create_sms_response? returns false' do
          before do
            @ticket.expects(:should_create_sms_response?).returns(false)
          end

          it 'performs pre-existing logic (does not call create_sms_response)' do
            @ticket.expects(:create_channel_back_event)
            @ticket.expects(:create_sms_response).never
            @ticket.handle_reply_options
          end
        end
      end

      describe 'comment.reply_option is present' do
        before do
          @ticket.comment.reply_option = { test: 'value' }
        end

        it 'raises an exception' do
          assert_raises StandardError do
            @ticket.handle_reply_options
          end
        end
      end
    end

    describe "#validate_and_create_channel_back_event" do
      before do
        @validator_data = {
          metadata: {
            external_id: "external_id",
            source: {
              external_id: "106686396330930",
              name: "Facebook page"
            },
            text: "Corrected outgoing text"
          },
          source_id: @page.id,
          via_id: @ticket.via_id
        }

        setup_ticket

        @parameter_validator = stub('parameter validator')
        ::Channels::Converter::ReplyParametersValidator.expects(:new).returns(@parameter_validator)
      end

      describe 'when data is valid' do
        before do
          @parameter_validator.expects(:validate).returns(@validator_data)
        end

        it "should call the reply factory and append channel back event" do
          assert_difference("ChannelBackEvent.count(:all)") do
            @ticket.save!
          end
          event = ChannelBackEvent.last
          assert_equal @account, event.account
          assert_equal @ticket, event.ticket
          assert_equal @validator_data[:metadata], event.value
          assert_equal @page.id, event.source_id
          assert_equal @ticket.via_id, event.via_id
          refute event.new_thread
        end

        it "should append channel back event with the passed in via_id for rules" do
          # Stubbing the reply initiator will stop the channels code from
          # sending a request to twitter to perform a reply.
          ::Channels::Converter::ReplyInitiator.any_instance.expects(:reply)

          @ticket.via_id = ViaType.WEB_FORM
          @ticket.validate_and_create_channel_back_event(update_via_id: ViaType.RULE)
          @ticket.save!
          event = ChannelBackEvent.last
          assert_equal @account, event.account
          assert_equal @ticket, event.ticket
          assert_equal @validator_data[:metadata], event.value
          assert_equal @page.id, event.source_id
          assert_equal ViaType.RULE, event.via_id
          assert event.new_thread
        end

        it "should update the comment text to match the reply_data" do
          comment = @ticket.comment
          original_body = comment.body
          @ticket.via_id = ViaType.WEB_FORM
          @ticket.validate_and_create_channel_back_event(update_via_id: ViaType.WEB_FORM)
          # The comment body should be replaced with the value returned by the validator
          assert_equal @validator_data[:metadata][:text], comment.body
          # The original comment body (which might contain placeholders) should get stored as the "intermediate value"
          assert_equal original_body, comment.intermediate_value
        end
      end

      describe 'when data is invalid' do
        it "should add errors if the factory returns any" do
          @parameter_validator.expects(:validate).returns(error: "Reply validation failed")
          refute @ticket.save
          assert_equal [:base, "Reply validation failed"], @ticket.errors.first
        end
      end
    end

    describe "#reply_parameters_validator" do
      before do
        setup_ticket
      end

      it "should initialize a Channels reply factory" do
        factory = stub
        ::Channels::Converter::ReplyParametersValidator.expects(:new).with(
          account_id: @account.id,
          ticket_id: @ticket.id,
          via_id: ViaType.TWITTER,
          source_id: @ticket.comment.channel_source_id,
          requester_id: @ticket.requester.id,
          text: "Outgoing text"
        ).returns(factory)
        assert_equal factory, @ticket.reply_parameters_validator("Outgoing text", ViaType.TWITTER)
      end

      it "should initialize successfully" do
        factory = @ticket.reply_parameters_validator("Outgoing text", ViaType.TWITTER)
        assert_equal ::Channels::Converter::ReplyParametersValidator, factory.class
        assert_equal @account, factory.account
        assert_equal @ticket.id, factory.ticket_id
        assert_equal @ticket.comment.channel_source_id, factory.source_id
        assert_equal ViaType.TWITTER, factory.via_id
        assert_equal "Outgoing text", factory.text
      end
    end
  end
end
