require_relative "../../../support/test_helper"
require 'zendesk/tickets/facebook'

SingleCov.covered!

describe 'Tickets::Facebook' do
  fixtures :accounts, :users, :subscriptions, :sequences, :tickets, :facebook_pages

  before do
    @account = accounts(:minimum)
    @agent   = users(:minimum_agent)
    @page    = facebook_pages(:minimum_facebook_page_1)
    @ticket  = tickets(:minimum_1)
    @ticket.update_column(:via_id, ViaType.FACEBOOK_POST)
  end

  describe 'facebook_update?' do
    before do
      @comment_defaults = {
        account: @ticket.account, body: "New comment", channel_back: "1", is_public: true, author: @agent
      }
    end

    it 'returns true if the update is via facebook' do
      [ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE].each do |via_id|
        @ticket.will_be_saved_by(@agent, via_id: via_id)
        assert @ticket.facebook_update?
      end
    end

    it 'returns false if the ticket is not via facebook' do
      @ticket.update_column(:via_id, ViaType.TWITTER)
      refute @ticket.facebook_update?
    end
  end

  it "returns proper via type" do
    assert @ticket.via_facebook?
    @ticket.via_id = ViaType.FACEBOOK_MESSAGE
    assert @ticket.via_facebook?
    @ticket.via_id = ViaType.WEB_FORM
    refute @ticket.via_facebook?
  end
end
