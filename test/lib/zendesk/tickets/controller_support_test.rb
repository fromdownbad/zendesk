require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 46

class TicketsControllerTest < ActionController::TestCase
  class ApiV2TicketsController < Api::V2::BaseController
    include Zendesk::Tickets::ControllerSupport
  end

  tests ApiV2TicketsController

  before do
    @account = @request.account = accounts(:minimum)
  end

  describe 'sort_tickets_for_cursor' do
    let (:tickets) { @account.tickets }

    before { @controller.stubs(params: params) }

    describe 'when sort parameter is omitted and sorting defaults to id' do
      let (:params) { {} }
      let (:expected_scope) { @account.tickets.use_index(Ticket::ACCOUNT_AND_NICE_ID_INDEX).reorder({nice_id: :asc}) }

      it 'returns the correct scope' do
        assert_equal expected_scope, @controller.send(:sort_tickets_for_cursor, tickets)
      end
    end

    describe 'when sort parameter is id DESC' do
      let (:params) { { sort: '-id' } }
      let (:expected_scope) { @account.tickets.use_index(Ticket::ACCOUNT_AND_NICE_ID_INDEX).reorder({nice_id: :desc}) }

      it 'returns the correct scope' do
        assert_equal expected_scope, @controller.send(:sort_tickets_for_cursor, tickets)
      end
    end

    describe 'when sort parameter is updated_at ASC' do
      let(:params) { { sort: 'updated_at' } }
      let(:expected_scope) { @account.tickets.use_index(Ticket::ACCOUNT_AND_STATUS_AND_UPDATED_INDEX).reorder({updated_at: :asc, id: :asc}) }

      it 'returns the correct scope' do
        assert_equal expected_scope, @controller.send(:sort_tickets_for_cursor, tickets)
      end
    end

    describe 'when sort parameter is updated_at DESC' do
      let(:params) { { sort: '-updated_at' } }
      let(:expected_scope) { @account.tickets.use_index(Ticket::ACCOUNT_AND_STATUS_AND_UPDATED_INDEX).reorder({updated_at: :desc, id: :desc}) }

      it 'returns the correct scope' do
        assert_equal expected_scope, @controller.send(:sort_tickets_for_cursor, tickets)
      end
    end

    describe 'when sort parameter has multiple fields' do
      let(:params) { { sort: '-created_at,id' } }

      it 'raises an exception' do
        err = assert_raises Zendesk::CursorPagination::InvalidPaginationParameter do
          @controller.send(:sort_tickets_for_cursor, tickets)
        end
        assert_equal "sort is not valid", err.message
      end
    end

    describe 'when sort parameter has invalid fields' do
      let(:params) { { sort: '-invalid' } }

      it 'raises an exception' do
        err = assert_raises Zendesk::CursorPagination::InvalidPaginationParameter do
          @controller.send(:sort_tickets_for_cursor, tickets)
        end
        assert_equal "sort is not valid", err.message
      end
    end
  end
end
