require_relative "../../../support/test_helper"
require_relative "../../../support/collaboration_settings_test_helper"
require 'zendesk/tickets/collaboration_support'

SingleCov.covered!

describe Zendesk::Tickets::CollaborationSupport do
  fixtures :accounts, :tickets

  describe "#legacy_or_modern_collaboration_enabled?" do
    let(:ticket) { tickets(:minimum_1) }
    before { CollaborationSettingsTestHelper.disable_all_new_collaboration_settings }

    describe "with new collaboration off" do
      before { ticket.account.expects(has_follower_and_email_cc_collaborations_enabled?: true).at_least_once }

      describe "when neither email_ccs nor follower setting is on" do
        before do
          ticket.account.expects(has_comment_email_ccs_allowed_enabled?: false)
          ticket.account.expects(has_ticket_followers_allowed_enabled?: false)
        end

        it { refute ticket.legacy_or_modern_collaboration_enabled? }
      end

      describe "when comment email_ccs setting is on" do
        before { ticket.account.expects(has_comment_email_ccs_allowed_enabled?: true) }

        it { assert ticket.legacy_or_modern_collaboration_enabled? }
      end

      describe "when ticket followers setting is on" do
        before do
          ticket.account.expects(has_comment_email_ccs_allowed_enabled?: false)
          ticket.account.expects(has_ticket_followers_allowed_enabled?: true)
        end

        it { assert ticket.legacy_or_modern_collaboration_enabled? }
      end
    end

    describe "with new collaboration off" do
      before do
        ticket.account.expects(has_follower_and_email_cc_collaborations_enabled?: false)
        ticket.account.expects(is_collaboration_enabled?: :blargh)
      end

      it { assert_equal :blargh, ticket.legacy_or_modern_collaboration_enabled? }
    end
  end
end
