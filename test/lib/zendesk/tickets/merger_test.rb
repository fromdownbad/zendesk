require_relative "../../../support/test_helper"
require_relative "../../../support/attachment_test_helper"

SingleCov.covered! uncovered: 5

describe Zendesk::Tickets::Merger do
  fixtures :all

  resque_inline false

  describe "#merge" do
    before do
      ActionMailer::Base.deliveries = []
      @user = users(:minimum_agent)
      @source = tickets(:minimum_2)
      @target = tickets(:minimum_3)
      @different_requester_source = tickets(:minimum_1)
      @source_comment = "Ticket closed and merged in #{@target.id}"
      @target_comment = "#{@source.id} has been merged in this ticket"
      @merger = Zendesk::Tickets::Merger.new(user: @user)
      create_private_tickets
    end

    describe 'social tickets' do
      describe 'merging from a social ticket' do
        before do
          @source.update_column(:via_id, ViaType.FACEBOOK_POST)
        end

        it 'should not channel back' do
          assert_no_difference('ChannelBackEvent.count(:all)') do
            merge
          end
        end
      end

      describe 'merging into a social ticket' do
        before do
          @target.update_column(:via_id, ViaType.FACEBOOK_POST)
        end

        it 'should not channel back' do
          assert_no_difference('ChannelBackEvent.count(:all)') do
            merge
          end
        end
      end

      describe 'merging from and into a social ticket' do
        before do
          @source.update_column(:via_id, ViaType.FACEBOOK_POST)
          @target.update_column(:via_id, ViaType.FACEBOOK_POST)
        end

        it 'should not channel back' do
          assert_no_difference('ChannelBackEvent.count(:all)') do
            merge
          end
        end
      end
    end

    describe "audits" do
      describe "with one source" do
        before { merge(target_is_public: false) }

        it "sets the audit value_references correctly" do
          assert_equal "source", @source.audits.last.value_reference
          assert_equal "target", @target.audits.last.value_reference
        end

        it "sets the value of target ticket to the source ticket id" do
          assert_equal @source.nice_id, @target.audits.last.value.to_i
        end
      end

      describe "with multiple sources" do
        before do
          @sources = [@source, tickets(:minimum_4)]
          merge(target_is_public: false, sources: @sources)
        end
        it "sets the audit value_references correctly" do
          @sources.each do |source|
            assert_equal "source", source.audits.last.value_reference
          end
          assert_equal "target", @target.audits.last.value_reference
        end

        it "sets the value of target ticket to comma separated list of merged ticket ids" do
          assert_equal @sources.map(&:nice_id).join(','), @target.audits.last.value
        end
      end
    end

    describe "ticket domain events" do
      describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
        let (:domain_event_publisher) { FakeEscKafkaMessage.new }
        before { @source.domain_event_publisher = domain_event_publisher }

        it "publishes a TicketMerged domain event" do
          merge
          events = domain_event_publisher.events.map do |event|
            ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value])
          end
          # Note a merging publishes several other events, including:
          # [:comment_added, :status_changed, :tags_changed, :agent_assignment_changed, :ticket_merged]
          assert_includes events.map(&:event), :ticket_merged
        end
      end
    end

    describe "when latest public comment is a voice comment" do
      before do
        @source.current_user = @user
        assert_not_equal @source.latest_public_comment.class.name, "VoiceComment"
        call = stub(voice_comment_data: {from: '', to: '', public: true}, account_id: @user.account_id)
        @source.comment = @source.add_voice_comment(call, body: "ramen", is_public: true, via_id: ViaType.PHONE_CALL_INBOUND)
        @source.will_be_saved_by @user
        @source.save && @source.reload
        merge(target_is_public: true)
      end

      it "makes a voice comment on the target" do
        assert_equal "VoiceComment", @target.reload.comments.last.class.name
      end

      it "updates target with target comment" do
        assert_equal "67806 has been merged in this ticket", @target.latest_public_comment.to_s
      end
    end

    describe "when source comments have attachments" do
      before do
        @source.current_user = @user
      end

      describe "when the source of the attachment is private" do
        before do
          @source.add_comment(body: "private attachment comment", is_public: false, via_id: ViaType.WEB_SERVICE)
          @source.will_be_saved_by(@user)
          @source.save

          @private_attachment = create_attachment(File.join(Rails.root, 'test/files/hello.html'), @user)
          @private_attachment.source = @source.comments.last
          @private_attachment.save
        end

        describe "and the target comment is public" do
          it "creates a new private comment to hold the attachment" do
            assert_difference("@target.comments.size", 2) do
              merge(target_is_public: true)
            end

            refute @target.reload.comments.last.is_public
            assert_equal 1, @target.comments.last.attachments.size
          end
        end

        describe "and the target comment is private" do
          it "does not create a new private comment to hold the attachment" do
            assert_difference("@target.comments.size", 1) do
              merge(target_is_public: false)
            end

            refute @target.reload.comments.last.is_public
            assert_equal 1, @target.comments.last.attachments.size
          end
        end
      end

      describe "when the source of the attachment is public" do
        before do
          @source.add_comment(body: "public attachment comment", is_public: true, via_id: ViaType.WEB_SERVICE)
          @source.will_be_saved_by(@user)
          @source.save

          @private_attachment = create_attachment(File.join(Rails.root, 'test/files/hello.html'), @user)
          @private_attachment.source = @source.comments.last
          @private_attachment.save
        end

        describe "and the target comment is public" do
          it "does not create a new private comment to hold the attachment" do
            assert_difference("@target.comments.size", 1) do
              merge(target_is_public: true)
            end

            assert @target.reload.comments.last.is_public
            assert_equal 1, @target.comments.last.attachments.size
          end
        end

        describe "and the target comment is private" do
          it "does not create a new comment to hold the attachment" do
            assert_difference("@target.comments.size", 1) do
              merge(target_is_public: false)
            end

            refute @target.reload.comments.last.is_public
            assert_equal 1, @target.comments.last.attachments.size
          end
        end
      end

      describe "when there are both private and public attachments" do
        before do
          @source.add_comment(body: "private attachment comment", is_public: false, via_id: ViaType.WEB_SERVICE)
          @source.will_be_saved_by(@user)
          @source.save

          @private_attachment = create_attachment(File.join(Rails.root, 'test/files/hello.html'), @user)
          @private_attachment.source = @source.comments.last
          @private_attachment.save

          @source.add_comment(body: "public attachment comment", is_public: true, via_id: ViaType.WEB_SERVICE)
          @source.will_be_saved_by(@user)
          @source.save

          @public_attachment = create_attachment(File.join(Rails.root, 'test/files/hello.html'), @user)
          @public_attachment.source = @source.comments.last
          @public_attachment.save
        end

        describe "and the target comment is public" do
          it "creates a new private comment to hold the private attachments" do
            assert_difference("@target.comments.size", 2) do
              merge(target_is_public: true)
            end

            refute @target.reload.comments.last.is_public
            assert_equal 1, @target.comments.last.attachments.size

            assert @target.comments[-2].is_public
            assert_equal 1, @target.comments.last.attachments.size
          end
        end

        describe "and the target comment is private" do
          it "does not create a new comment to hold both private and public attachments on the one private comment" do
            assert_difference("@target.comments.size", 1) do
              merge(target_is_public: false)
            end

            refute @target.reload.comments.last.is_public
            assert_equal 2, @target.comments.last.attachments.size
          end
        end
      end

      describe "which is missing or for which a copy cannot be created" do
        before do
          Attachment.stubs(:from_attachment).returns(nil)

          @source.add_comment(body: "private attachment comment", is_public: false, via_id: ViaType.WEB_SERVICE)
          @source.will_be_saved_by(@user)
          @source.save

          @private_attachment = create_attachment(File.join(Rails.root, 'test/files/hello.html'), @user)
          @private_attachment.source = @source.comments.last
          @private_attachment.save
          merge(target_is_public: true)
        end

        it "is not included in target and does not fail the merge" do
          refute @target.comments.any? { |c| c.attachments.any? }
        end
      end

      describe "which raises an exception on copy" do
        before do
          Attachment.stubs(:from_attachment).raises(IOError)

          @source.add_comment(body: "private attachment comment", is_public: false, via_id: ViaType.WEB_SERVICE)
          @source.will_be_saved_by(@user)
          @source.save

          @private_attachment = create_attachment(File.join(Rails.root, 'test/files/hello.html'), @user)
          @private_attachment.source = @source.comments.last
          @private_attachment.save
        end

        it "is repackaged as MergeFailed" do
          assert_raise(Zendesk::Tickets::Merger::MergeFailed) do
            merge(target_is_public: true)
          end
        end
      end

      describe "which do not have a store" do
        before do
          @source.add_comment(body: "no store attachment comment", is_public: false, via_id: ViaType.WEB_SERVICE)
          @source.will_be_saved_by(@user)
          @source.save

          @private_attachment = create_attachment(File.join(Rails.root, 'test/files/hello.html'), @user)
          @private_attachment.source = @source.comments.last
          @private_attachment.save
          @private_attachment.update_column(:stores, '')
          merge(target_is_public: true)
        end

        it "it does not include the attachment w/o a store" do
          refute @target.comments.last.attachments.any? { |a| a.stores.empty? }
        end
      end
    end

    describe "with same requester" do
      describe "is a public source ticket and private target ticket" do
        before do
          refute @private_target.is_public
          ActionMailer::Base.perform_deliveries = true
          merge(target: @private_target, sources: [@source])
        end

        should_change("the number of Notifications", by: 0) { Notification.count(:all) }
        should_change("the number of Comments", by: 2) { Comment.count(:all) }
        should_change("the number of Audits", by: 2) { Audit.count(:all) }
        should_change("the number of mail deliveries", by: 0) { ActionMailer::Base.deliveries.size }

        it "makes private merge comments by default on both the source and the target" do
          refute @private_target.comments.last.is_public
          refute @source.comments.last.is_public
        end
      end

      describe "is a public ticket" do
        before do
          ActionMailer::Base.perform_deliveries = true
          merge
        end

        should_change("the number of Notifications", by: 2) { Notification.count(:all) }
        should_change("the number of Comments", by: 2) { Comment.count(:all) }
        should_change("the number of Audits", by: 2) { Audit.count(:all) }
        should_change("the number of mail deliveries", by: 2) { ActionMailer::Base.deliveries.size }

        it "creates audits with ViaType.MERGE" do
          assert_equal ViaType.MERGE, @source.audits.last.via_id
          assert_equal ViaType.MERGE, @target.audits.last.via_id
        end

        it "closes @source" do
          assert_equal "Closed", @source.status
          assert_equal "Pending", @target.status
        end

        it "adds 'closed_by_merge' tag to @source" do
          assert @source.current_tags.include?('closed_by_merge')
        end

        it "assigns source to @user" do
          assert_equal @user, @source.assignee
        end

        it "creates public comments in @source and @target" do
          assert @source.comments.last.is_public?
          assert @target.comments.last.is_public?
          assert_equal @source_comment, @source.comments.last.body
          assert_equal @target_comment, @target.comments.last.body
        end

        it "sends mails to the right recipients" do
          assert_equal [@source.requester.email], ActionMailer::Base.deliveries[0].to
          assert_equal [@source.requester.email], ActionMailer::Base.deliveries[1].to
          assert ActionMailer::Base.deliveries[0].joined_bodies.include?(@source_comment)
          assert ActionMailer::Base.deliveries[1].joined_bodies.include?(@target_comment)
        end
      end
    end

    describe "where target has an assignee and group" do
      before do
        @source.assignee = nil
        @source.group = nil
        @target.assignee = users(:minimum_admin)
        @target.group = groups(:minimum_group)
        @target.assignee.groups = [@target.group.id]
        merge
      end

      it "assigns ticket to target's user and group" do
        assert_equal @source.assignee, @target.assignee
        assert_equal @source.group, @target.group
      end
    end

    describe "where source and target has no assignee or group and current user has no groups" do
      before do
        @source.assignee = nil
        @source.group = nil
        @target.assignee = nil
        @target.group = nil
        @user.memberships.clear
        merge
      end

      it "assigns ticket to the account default agent and group" do
        assert_equal @source.group, @user.account.groups.first
        assert_equal @source.assignee, @user.account.agents.first
      end
    end

    describe "with current agent in ticket group" do
      before do
        @user.groups = [groups(:minimum_group).id]
        @source.update_attribute(:group_id, groups(:minimum_group).id)
        merge
      end
      should_change("the number of Comments", by: 2) { Comment.count(:all) }
      it "does not reset the group" do
        assert_equal groups(:minimum_group), @source.group
      end
    end

    describe "with current agent not in ticket group" do
      before do
        @group = groups(:inactive_group)
        @group.update_attribute(:is_active, true)
        @user.current_user = @user
        @source.will_be_saved_by(@source.submitter)
        @source.update_attribute(:group_id, @group.id)
        refute @user.groups.include?(@source.group)
        merge
      end
      should_change("the number of Comments", by: 2) { Comment.count(:all) }
      it "changes group to one of the assignee groups" do
        assert_not_equal @group, @source.group
        assert @user.groups.include?(@source.group)
      end
    end

    describe "with private target comment on a private ticket" do
      before do
        ActionMailer::Base.perform_deliveries = true
        merge(target: @private_target, sources: [@private_source], source_is_public: false)
      end
      should_change("the number of Comments", by: 2) { Comment.count(:all) }
      should_change("the number of Notifications", by: 0) { Notification.count(:all) } # should this be ZERO???
      should_change("the number of mail deliveries", by: 0) { ActionMailer::Base.deliveries.size }
      it "creates private comment in @source and private comment in @target" do
        refute @private_source.comments.last.is_public?
        refute @private_target.comments.last.is_public?
        assert_equal @source_comment, @private_source.comments.last.body
        assert_equal @target_comment, @private_target.comments.last.body
      end
    end

    describe "with private source comment on a public ticket" do
      before do
        ActionMailer::Base.perform_deliveries = true
        merge(source_is_public: false)
      end
      should_change("the number of Comments", by: 2) { Comment.count(:all) }
      should_change("the number of Notifications", by: 1) { Notification.count(:all) }
      should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }
      it "creates private comment in @source and public in @target" do
        refute @source.comments.last.is_public?
        assert @target.comments.last.is_public?
        assert_equal @source_comment, @source.comments.last.body
        assert_equal @target_comment, @target.comments.last.body
      end
    end

    describe "with source and target private comments" do
      before do
        ActionMailer::Base.perform_deliveries = true
        merge(target_is_public: false, source_is_public: false)
      end
      should_change("the number of Comments", by: 2) { Comment.count(:all) }
      should_not_change("the number of Notifications") { Notification.count(:all) }
      should_not_change("the number of mail deliveries") { ActionMailer::Base.deliveries.size }
      it "creates private comments in @source and @target" do
        refute @source.comments.last.is_public?
        refute @target.comments.last.is_public?
        assert_equal @source_comment, @source.comments.last.body
        assert_equal @target_comment, @target.comments.last.body
      end
    end

    describe "with source in invalid state" do
      before do
        ActionMailer::Base.perform_deliveries = true
        @source.stubs(:valid?).returns(false)
        assert_raises(Zendesk::Tickets::Merger::MergeFailed) { merge }
      end
      should_not_change("the number of Notifications") { Notification.count(:all) }
      should_not_change("the number of mail deliveries") { ActionMailer::Base.deliveries.size }
      should_not_change("the number of Comments") { Comment.count(:all) }
    end

    describe "with target requester in source collaborators" do
      before do
        @source = tickets(:minimum_1)
        @source.collaborator_ids = [@target.requester.id]
        merge
      end
      should_change("the number of Comments", by: 2) { Comment.count(:all) }
    end

    describe "linking merge tickets" do
      describe "merging a valid source ticket" do
        before do
          merge
        end
        should_change("the number of TicketLinks", by: 1) { TicketLink.count(:all) }
      end

      describe "merging an invalid source ticket" do
        before do
          @source.stubs(:valid?).returns(false)
          assert_raises(Zendesk::Tickets::Merger::MergeFailed) { merge }
        end
        should_not_change("the number of TicketLinks") { TicketLink.count(:all) }
      end
    end

    describe "#extract_collaborator_ids" do
      before { create_agent_tickets }
      describe_with_arturo_enabled :email_ccs do
        before do
          # Set target to have different requester than source tickets since we won't add a source requester if it's the same as the target requester
          @target.requester = users(:minimum_end_user2)
          @target.will_be_saved_by(@user)
          @target.save!
          @target.reload
        end

        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it "adds current email_ccs and followers on sources to target" do
            current_follower_user = users(:minimum_admin_not_owner)
            current_email_cc_user = users(:minimum_author)
            @source.collaborations << Collaboration.new(collaborator_type: CollaboratorType.EMAIL_CC, user: current_email_cc_user)
            @source.collaborations << Collaboration.new(collaborator_type: CollaboratorType.FOLLOWER, user: current_follower_user)
            @merger.instance_variable_set(:@target, @target)
            @merger.instance_variable_set(:@sources, [@source])
            @merger.send(:extract_collaborator_ids)
            assert @merger.instance_variable_get(:@email_cc_ids).include?(current_email_cc_user.id)
            assert @merger.instance_variable_get(:@follower_ids).include?(current_follower_user.id)
          end

          it "adds all source requester ids to email_ccs list" do
            Account.any_instance.stubs(:has_agent_email_ccs_become_followers_enabled?).returns(false)
            @merger.instance_variable_set(:@target, @target)
            @merger.instance_variable_set(:@sources, [@source, @different_requester_source])
            @merger.send(:extract_collaborator_ids)
            assert_equal [@source.requester.id, @different_requester_source.requester.id].sort, @merger.instance_variable_get(:@email_cc_ids)
          end

          describe_with_arturo_setting_enabled :agent_email_ccs_become_followers do
            it "also adds any source requesters that are agents onto the followers list" do
              @merger.instance_variable_set(:@target, @target)
              @merger.instance_variable_set(:@sources, [@source, @agent_source, @admin_source])
              @merger.send(:extract_collaborator_ids)
              assert_equal [@agent_source.requester.id, @admin_source.requester.id].sort, @merger.instance_variable_get(:@follower_ids)
              assert_equal [@source.requester.id, @agent_source.requester.id, @admin_source.requester.id].sort, @merger.instance_variable_get(:@email_cc_ids)
            end

            it "does nothing if all source requesters are end users" do
              assert @source.requester.is_end_user?
              assert @different_requester_source.requester.is_end_user?
              @merger.instance_variable_set(:@target, @target)
              @merger.instance_variable_set(:@sources, [@source, @different_requester_source])
              @merger.send(:extract_collaborator_ids)
              assert_equal [@source.requester.id, @different_requester_source.requester.id].sort, @merger.instance_variable_get(:@email_cc_ids)
            end
          end

          describe_with_arturo_setting_disabled :agent_email_ccs_become_followers do
            it "does not add any source requesters that are agents on to the followers list" do
              @merger.instance_variable_set(:@target, @target)
              @merger.instance_variable_set(:@sources, [@source, @agent_source, @admin_source])
              @merger.send(:extract_collaborator_ids)
              assert_empty @merger.instance_variable_get(:@follower_ids)
            end
          end
        end

        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          it "adds current email_ccs and followers on sources to target" do
            current_follower_user = users(:minimum_admin_not_owner)
            current_email_cc_user = users(:minimum_author)
            @source.collaborations << Collaboration.new(collaborator_type: CollaboratorType.EMAIL_CC, user: current_email_cc_user)
            @source.collaborations << Collaboration.new(collaborator_type: CollaboratorType.FOLLOWER, user: current_follower_user)
            @merger.instance_variable_set(:@target, @target)
            @merger.instance_variable_set(:@sources, [@source])
            @merger.send(:extract_collaborator_ids)
            assert @merger.instance_variable_get(:@email_cc_ids).include?(current_email_cc_user.id)
            assert @merger.instance_variable_get(:@follower_ids).include?(current_follower_user.id)
          end

          describe_with_arturo_setting_disabled :ticket_followers_allowed do
            it "does not add source requesters to followers list even if ALL source requesters are agents" do
              @merger.instance_variable_set(:@target, @target)
              @merger.instance_variable_set(:@sources, [@admin_source, @agent_source])
              @merger.send(:extract_collaborator_ids)
              assert_empty @merger.instance_variable_get(:@follower_ids) & [@source.requester.id, @agent_source.requester.id]
            end

            describe "with legacy CCs enabled" do
              before { Account.any_instance.stubs(is_collaboration_enabled?: true) }

              it "adds source requesters to collaborators list" do
                @merger.instance_variable_set(:@target, @target)
                @merger.instance_variable_set(:@sources, [@source, @different_requester_source])
                @merger.send(:extract_collaborator_ids)
                assert_equal [@source.requester.id, @different_requester_source.requester.id].sort, @merger.instance_variable_get(:@collaborator_ids)
              end
            end
          end

          describe_with_arturo_setting_enabled :ticket_followers_allowed do
            it "does not add source requesters to follower list if some source requesters are end users" do
              assert @source.requester.is_end_user?
              @merger.instance_variable_set(:@target, @target)
              @merger.instance_variable_set(:@sources, [@source, @agent_source])
              @merger.send(:extract_collaborator_ids)
              assert_empty @merger.instance_variable_get(:@follower_ids) & [@source, @agent_source]
            end

            it "adds source requesters to followers list if ALL source requesters are agents" do
              @merger.instance_variable_set(:@target, @target)
              @merger.instance_variable_set(:@sources, [@admin_source, @agent_source])
              @merger.send(:extract_collaborator_ids)
              assert_equal [@admin_source.requester.id, @agent_source.requester.id].sort, @merger.instance_variable_get(:@follower_ids)
            end
          end
        end
      end

      describe_with_arturo_disabled :email_ccs do
        it "copies all requesters of source tickets to target as legacy collaborators" do
          @merger.instance_variable_set(:@target, @target)
          @merger.instance_variable_set(:@sources, [@agent_source, @different_requester_source])
          @merger.send(:extract_collaborator_ids)
          assert_equal [@agent_source.requester.id, @different_requester_source.requester.id].sort, @merger.instance_variable_get(:@follower_ids)
        end

        it "if a source ticket has the same requester as the target ticket, it does not add it as a legacy collaborator" do
          assert_equal @target.requester.id, @source.requester.id

          @merger.instance_variable_set(:@target, @target)
          @merger.instance_variable_set(:@sources, [@source, @different_requester_source])
          @merger.send(:extract_collaborator_ids)
          assert_equal [@different_requester_source.requester.id], @merger.instance_variable_get(:@follower_ids)
        end
      end

      it "does not contain duplicate user entries" do
        @source.collaborations << Collaboration.new(collaborator_type: CollaboratorType.EMAIL_CC, user: users(:minimum_author))
        @source.collaborations << Collaboration.new(collaborator_type: CollaboratorType.EMAIL_CC, user: users(:minimum_author))
        @merger.instance_variable_set(:@sources, [@source])
        @merger.instance_variable_set(:@target, @target)
        @merger.send(:extract_collaborator_ids)

        assert_equal 1, @merger.instance_variable_get(:@email_cc_ids).select { |id| id == users(:minimum_author).id }.size
      end
    end

    describe "Setting followers/email_ccs/collaborators during merge" do
      before { create_agent_tickets }
      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        it "if a legacy_cc is present on a source ticket it copies all requesters and followers of source tickets to target as legacy collaborators" do
          @source.collaborations << Collaboration.new(collaborator_type: CollaboratorType.LEGACY_CC, user: @legacy_agent)
          # Set target to have different requester than source tickets
          @target.requester = users(:minimum_end_user2)
          @target.will_be_saved_by(@user)
          @target.save!

          merge(target: @target, sources: [@source, @different_requester_source])
          all_collabs = [@source.requester_id, @different_requester_source.requester_id] | @source.follower_ids | @different_requester_source.follower_ids | @target.follower_ids
          assert_equal all_collabs.sort, @target.collaborators.map(&:id).sort
          # The legacy_cc should have been transformed into an email_cc since its an end_user
          assert @target.email_cc_ids.include?(@source.requester_id)
        end

        it "if a source ticket has the same requester as the target ticket, it does not add it as a collaborator" do
          @source.requester = users(:minimum_end_user2)
          @target.requester = users(:minimum_end_user2)
          merge(target: @target, sources: [@source, @different_requester_source])
          all_collabs = [@different_requester_source.requester_id] | @source.follower_ids | @different_requester_source.follower_ids | @target.follower_ids
          assert_equal all_collabs.sort, @target.collaborators.map(&:id).sort
        end

        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          describe_with_arturo_setting_disabled :agent_email_ccs_become_followers do
            it "adds all source requester_ids and source email_cc_ids to email_ccs on the target" do
              merge(target: @target, sources: [@different_requester_source])
              email_ccs = [@different_requester_source.requester_id] | @source.email_cc_ids | @target.email_cc_ids
              assert_equal email_ccs.sort, @target.email_cc_ids.sort
            end
          end

          describe_with_arturo_setting_enabled :agent_email_ccs_become_followers do
            it "adds any source requesters that are agents onto the followers list but doesn't add end users as followers" do
              merge(target: @target, sources: [@different_requester_source, @agent_source, @admin_source])
              assert @target.follower_ids.include?(@agent_source.requester_id)
              assert @target.follower_ids.include?(@admin_source.requester_id)
              refute @target.follower_ids.include?(@different_requester_source.requester_id)
            end
          end
        end

        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          it "does not add source requesters to email_ccs list on target" do
            merge(target: @target, sources: [@different_requester_source])
            assert_empty @target.email_cc_ids
          end

          describe_with_arturo_setting_disabled :ticket_followers_allowed do
            it "does not add source requesters to followers list even if ALL source requesters are agents" do
              merge(target: @target, sources: [@admin_source, @agent_source])
              refute @target.follower_ids.include?(@admin_source.requester_id)
              refute @target.follower_ids.include?(@agent_source.requester_id)
            end
          end

          describe_with_arturo_setting_enabled :ticket_followers_allowed do
            it "does not add source requesters to follower list if some source requesters are end users" do
              @source.requester = users(:minimum_end_user2)
              merge(target: @target, sources: [@source, @agent_source])
              refute @target.follower_ids.include?(@source.requester_id)
              refute @target.follower_ids.include?(@agent_source.requester_id)
            end

            it "adds source requesters to followers list if ALL source requesters are agents" do
              merge(target: @target, sources: [@admin_source, @agent_source])
              assert @target.follower_ids.include?(@admin_source.requester_id)
              assert @target.follower_ids.include?(@agent_source.requester_id)
            end
          end
        end
      end

      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        describe_with_arturo_disabled :email_ccs do
          it "copies all requesters of source tickets to target as legacy collaborators" do
            merge(target: @target, sources: [@agent_source, @different_requester_source])
            all_collabs = [@agent_source.requester_id, @different_requester_source.requester_id] | @agent_source.follower_ids | @different_requester_source.follower_ids | @target.follower_ids
            assert_equal all_collabs.sort, @target.collaborators.map(&:id).sort
          end
        end
      end
    end
  end

  describe "#different_requesters_not_allowed?" do
    before do
      @user = users(:minimum_agent)
      @source1 = tickets(:minimum_1)
      @account = @source1.account
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it "returns true when is_collaboration is disabled" do
        Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
        assert Zendesk::Tickets::Merger.different_requesters_not_allowed?(@account, [@source1])
      end
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
        describe_with_arturo_setting_enabled :ticket_followers_allowed do
          before do
            @source2 = tickets(:minimum_3)
            @source2.requester = users(:minimum_admin)
            @source2.will_be_saved_by(users(:minimum_admin))
            @source2.save!
          end

          it "returns true when source requesters are not ALL agents" do
            assert Zendesk::Tickets::Merger.different_requesters_not_allowed?(@account, [@source1, @source2])
          end

          it "returns false when source requesters are ALL agents" do
            @source1.requester = users(:minimum_agent)
            @source1.will_be_saved_by(users(:minimum_admin))
            @source1.save!

            refute Zendesk::Tickets::Merger.different_requesters_not_allowed?(@account, [@source1, @source2])
          end
        end

        describe_with_arturo_setting_disabled :ticket_followers_allowed do
          it "returns true" do
            assert Zendesk::Tickets::Merger.different_requesters_not_allowed?(@account, [@source1])
          end
        end
      end

      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        it "returns false" do
          refute Zendesk::Tickets::Merger.different_requesters_not_allowed?(@account, [@source1])
        end
      end
    end
  end

  def merge(options = {})
    defaults = {
      target: @target,
      sources: [@source],
      target_comment: @target_comment,
      source_comment: @source_comment
    }
    merge_args = defaults.merge(options)
    @merger.merge(merge_args)
    merge_args[:sources].first.reload
    merge_args[:target].reload
  end

  def create_private_tickets
    @private_source = Zendesk::Tickets::Initializer.new(accounts(:minimum), @user, ticket: {
      comment: { public: false, value: "Private source wombat ticket" }
      }).ticket
    @private_source.save!
    @private_target = Zendesk::Tickets::Initializer.new(accounts(:minimum), @user, ticket: {
      comment: { public: false, value: "Private target wombat ticket" }
      }).ticket
    @private_target.save!
    @private_target.reload
    @private_source.reload
    refute @private_source.is_public
    refute @private_target.is_public
  end

  def create_agent_tickets
    @agent_source = tickets(:minimum_6)
    @agent_source.requester = users(:minimum_agent)
    @agent_source.will_be_saved_by(@user)
    @agent_source.save!

    @admin_source = tickets(:minimum_4)
    @admin_source.requester = users(:minimum_admin)
    @admin_source.will_be_saved_by(@user)
    @admin_source.save!

    @legacy_agent = users(:minimum_admin_not_verified)
    @legacy_agent.name = 'Legacy'
    @legacy_agent.save!
  end
end
