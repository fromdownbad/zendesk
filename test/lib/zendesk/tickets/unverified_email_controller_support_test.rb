require_relative "../../../support/test_helper"
require "zendesk/tickets/unverified_email_controller_support"

SingleCov.covered!

class Zendesk::Tickets::UnverifiedEmailControllerSupportTest < ActionController::TestCase
  fixtures :accounts, :users

  tests(Class.new(ApplicationController) do
    include Zendesk::Tickets::UnverifiedEmailControllerSupport

    def self.controller_name
      "test"
    end
  end)

  describe "#ensure_end_user_email_is_verified" do
    TestResponse = if RAILS4
      ActionController::TestResponse
    else
      ActionDispatch::TestResponse
    end

    as_an_end_user do
      describe "when end user has no unverified emails" do
        before { @controller.response = TestResponse.new }

        it('raises no error') do
          @controller.send(:ensure_end_user_email_is_verified)
          @controller.response.status.must_equal(200)
        end
      end

      describe "when end user has unverified emails" do
        let(:account) { accounts(:minimum) }
        let(:current_user) { users(:minimum_end_user) }
        let(:unverified_email_identity) do
          UserEmailIdentity.create(
            user: current_user,
            value: "EnduserUnverified@zendesk.com",
            account: account,
            is_verified: false
          )
        end

        before do
          @controller.response = TestResponse.new
          unverified_email_identity
        end

        it('raises "Unverified email address" error') do
          @controller.send(:ensure_end_user_email_is_verified)

          @controller.response.status.must_equal(403)
          @controller.response.body.must_include("Forbidden")
          @controller.response.body.must_include("Unverified email address")
        end

        describe_with_arturo_enabled :unverified_ticket_creations do
          describe "when end user created tickets using the unverified email" do
            let(:ticket) { current_user.tickets.first }

            before do
              ticket.create_unverified_creation(
                account: account,
                user: current_user,
                user_identity: unverified_email_identity,
                from_address: unverified_email_identity.value
              )
            end

            it('raises "Unverified email address" error') do
              @controller.send(:ensure_end_user_email_is_verified)

              @controller.response.status.must_equal(403)
              @controller.response.body.must_include("Forbidden")
              @controller.response.body.must_include("Unverified email address")
            end
          end

          describe "when end user didn't create tickets using the unverified email" do
            it('raises no error') do
              @controller.send(:ensure_end_user_email_is_verified)
              @controller.response.status.must_equal(200)
            end
          end
        end
      end
    end
  end
end
