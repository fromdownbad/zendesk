require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'CommentsInitializer' do
  fixtures :accounts, :users

  let(:user) { users(:minimum_admin) }

  describe "comment initialization" do
    describe "normalizes the public parameter passed in" do
      before do
        @comment_initializer = Zendesk::Tickets::Comments::Initializer.new(user, value: "Halps.", public: subject)
      end

      [true, '1', 'true'].each do |truthy|
        describe "public => #{truthy} (#{truthy.class})" do
          subject { truthy }

          it "sets comment to public" do
            assert @comment_initializer.comment[:is_public]
          end
        end
      end

      [false, '0', 'false'].each do |falsy|
        describe "public => #{falsy} (#{falsy.class})" do
          subject { falsy }

          it "sets comment to private" do
            assert_equal false, @comment_initializer.comment[:is_public]
          end
        end
      end
    end

    describe 'body size checking' do
      let(:body) { '' }
      let(:initializer) { Zendesk::Tickets::Comments::Initializer.new(user, body: body) }

      describe 'and the body is < 64kb' do
        let(:body) { 'hello' }

        describe_with_arturo_enabled :block_large_comment_body_on_initialize do
          it 'succeeds' do
            assert_equal body, initializer.normalize[:body]
          end
        end

        describe_with_arturo_disabled :block_large_comment_body_on_initialize do
          it 'succeeds' do
            assert_equal body, initializer.normalize[:body]
          end
        end
      end

      describe 'and the body is > 64kb' do
        let(:body) { 'a' * 65.kilobytes }

        describe_with_arturo_enabled :block_large_comment_body_on_initialize do
          it 'errors' do
            assert_raises Event::ValueTooLarge do
              initializer.normalize
            end
          end
        end

        describe_with_arturo_disabled :block_large_comment_body_on_initialize do
          it 'succeeds' do
            assert_equal body, initializer.normalize[:body]
          end
        end
      end
    end
  end
end
