require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 4

class Zendesk::Tickets::Comments::EmailCommentSupportTest < ActionController::TestCase
  class EmailCommentTestController < ApplicationController
    include Zendesk::Tickets::Comments::EmailCommentSupport
  end

  tests EmailCommentTestController

  describe Zendesk::Tickets::Comments::EmailCommentSupport do
    fixtures :accounts, :users, :tickets

    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }
    let(:comment) { ticket.comments.first }

    as_an_agent do
      describe '#create_dev_comment_on_ticket' do
        let(:kragle_client) { stub }
        let(:success_response) { stub(success?: true, body: response_body.to_json) }
        let(:kragle_response) { success_response }
        let(:request_params) { { message_id: "<message_id:1234.com>", id: comment.id, ticket_id: ticket.nice_id, json_email_identifier: "email_identifier", raw_email_identifier: "raw_email_identifier"} }

        before do
          @controller.stubs(:params).returns(request_params)
          @controller.stubs(:internal_api_client).returns(kragle_client)
        end

        it 'creates a ticket comment' do
          kragle_client.expects(:put).with("api/v2/tickets/#{ticket.nice_id}", anything).once
          @controller.send(:create_dev_comment_on_ticket, ticket.nice_id)
        end
      end

      describe '#should_report_mail_rendering_issue?' do
        before do
          @controller.stubs(:comment).returns(comment)

          if value_previous.present?
            comment.stubs(:audit).returns(Audit.create!(
              author: ticket.requester,
              via_id: ViaType.WEB_FORM,
              ticket: ticket,
              value_previous: value_previous
            ))
          end
        end

        describe 'when a z1 ticket does not exist for the ticket comment' do
          let(:value_previous) { { custom: {} }.to_json }

          it 'returns true' do
            assert @controller.send(:should_report_mail_rendering_issue?)
          end
        end

        describe 'when a z1 ticket exists for the ticket comment' do
          let(:value_previous) { { custom: { z1_request_url: 'sample_z1_request_url' } }.to_json }

          it 'returns false' do
            refute @controller.send(:should_report_mail_rendering_issue?)
          end
        end

        describe 'when there is an issue parsing the comment audit' do
          let(:value_previous) { nil }

          it 'returns true' do
            assert @controller.send(:should_report_mail_rendering_issue?)
          end
        end
      end

      describe '#report_mail_rendering_issue' do
        let(:kragle_client) { stub }
        let(:success_response) { stub(success?: true, body: response_body.to_json) }
        let(:kragle_response) { success_response }
        let(:request_params) { {id: comment.id, ticket_id: ticket.nice_id, description: "description", json_email_identifier: "email_identifier", raw_email_identifier: "raw_email_identifier"} }

        before do
          @controller.stubs(:params).returns(request_params)
          @controller.stubs(:internal_api_client).returns(kragle_client)
        end

        it 'creates a support ticket with the corrrect data' do
          kragle_client.expects(:post).with('api/v2/requests', request: {subject: "Reply Parser Issue - account_id: #{account.id}, ticket_id: #{request_params[:ticket_id]}, comment_id: #{request_params[:id]}",
                                                                         comment: {body: "There is an reply parser issue with the following ticket comment:\n\naccount_id: #{account.id}\n\nticket_id: #{request_params[:ticket_id]}\n\ncomment_id: #{request_params[:id]}\n\ndescription"},
                                                                         requester: users(:minimum_agent)}).once
          @controller.send(:report_mail_rendering_issue)
        end
      end

      describe "#update_audit_metadata!" do
        before { @controller.stubs(:comment).returns(account.events.where(type: "Comment").first) }

        let(:metadata) { { z1_request_url: 'https://support.zendesk-test.com/hc/requests/12345' } }

        it "calls #set_custom_metadata! on the comment audit" do
          Audit.any_instance.expects(:set_custom_metadata!).with(metadata).once
          @controller.send(:update_audit_metadata!, metadata)
        end
      end

      describe '#prepare_full_body_from_raw_email' do
        let(:remote_json) do
          {
            processing_state: {
              content: {
                html_quoted: "<style>.inner { border: none; }</style><div class=\"inner\">html body json data from external storage (s3)</div>",
                plain_quoted: "plain body json data from external storage (s3)"
              }
            }
          }.to_json
        end

        before do
          comment.update_column(:via_id, ViaType.MAIL)
          @controller.stubs(:comment).returns(comment)
        end

        describe "when full message body json is nil" do
          before do
            Zendesk::Mailer::RawEmailAccess.any_instance.stubs(full_message_body_json: nil)
            @controller.stubs(:current_account).returns(account)
          end

          it "raises an exception" do
            assert_raises ActiveRecord::RecordNotFound do
              @controller.send(:prepare_full_body_from_raw_email)
            end
          end
        end

        describe 'with rich content in emails disabled' do
          let(:expected_full_comment) do
            "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\">plain body json data from external storage (s3)</div>"
          end
          let(:rich_content_in_emails) { false }

          before do
            Zendesk::Mailer::RawEmailAccess.any_instance.stubs(full_message_body_json: remote_json)
            account.settings.rich_content_in_emails = rich_content_in_emails
            account.save!
            @controller.stubs(:current_account).returns(account)
          end

          it 'retrieves full email body' do
            full_comment = @controller.send(:prepare_full_body_from_raw_email)
            assert_equal expected_full_comment, full_comment
          end
        end

        describe 'with rich content in emails enabled' do
          let(:expected_comment) do
            "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><div class=\"inner\" style=\"border: none\">html body json data from external storage (s3)</div></div>"
          end
          let(:rich_content_in_emails) { true }

          before do
            Zendesk::Mailer::RawEmailAccess.any_instance.stubs(full_message_body_json: remote_json)
            account.settings.rich_content_in_emails = rich_content_in_emails
            account.save!
            @controller.stubs(:current_account).returns(account)
          end

          describe 'when it is a rich comment' do
            before do
              comment.stubs(:rich?).returns(true)
            end

            it 'retrieves full email body with CSS inline' do
              full_comment = @controller.send(:prepare_full_body_from_raw_email)
              assert_equal expected_comment, full_comment
            end
          end

          describe 'when it is not a rich comment' do
            let(:expected_comment) do
              "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">plain body json data from external storage (s3)</p></div>"
            end

            before do
              comment.stubs(:rich?).returns(false)
            end

            it 'retrieves full email body' do
              full_comment = @controller.send(:prepare_full_body_from_raw_email)
              assert_equal expected_comment, full_comment
            end
          end
        end
      end
    end
  end
end
