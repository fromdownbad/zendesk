require_relative "../../../support/test_helper"
require 'zendesk/tickets/initializer'
require_relative "../../../support/collaboration_settings_test_helper"
require_relative "../../../support/agent_test_helper"

SingleCov.covered! uncovered: 22

describe Zendesk::Tickets::Initializer do
  include AgentTestHelper

  fixtures :all

  describe "Standard ticket initialization" do
    describe "empty comment" do
      before do
        @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
          assignee_id: users(:minimum_agent).id,
          collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
          comment: "",
          metadata: { key: "value" }
        })
        @ticket = @ticket_initializer.ticket
      end

      it "does not add a comment" do
        assert_nil @ticket.comment
      end
    end

    describe "sets comment publicity" do
      describe "and no publicity value set by the user" do
        it "sets comment publicity to false if ticket is not public and request is via api or mail" do
          @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
            assignee_id: users(:minimum_agent).id,
            is_public: false,
            via_id: ViaType.WEB_SERVICE,
            collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
            comment: { value: "Hi wombat", public: nil },
            metadata: { key: "value" }
          }, from_api: true)

          refute(@ticket_initializer.ticket.comment.is_public)
        end

        it "sets comment publicity to true if the ticket is public" do
          @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
            assignee_id: users(:minimum_agent).id,
            is_public: true,
            via_id: ViaType.WEB_SERVICE,
            collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
            comment: { value: "Hi wombat", public: nil },
            metadata: { key: "value" }
          }, from_api: true)

          assert(@ticket_initializer.ticket.comment.is_public)
        end
      end

      it "sets comment publicity to value passed in by the user" do
        @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
          assignee_id: users(:minimum_agent).id,
          is_public: false,
          collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
          comment: { value: "Hi wombat", public: true },
          metadata: { key: "value" }
        })

        assert(@ticket_initializer.ticket.comment.is_public)
      end
    end

    describe "ticket subject as integer" do
      before do
        @ticket_initializer = Zendesk::Tickets::Initializer.new(
          accounts(:minimum),
          users(:minimum_admin),
          ticket: { subject: 123456 }
        )
        @ticket = @ticket_initializer.ticket
      end

      it "normalizes integer value to string" do
        assert @ticket.subject.is_a?(String)
      end
    end

    describe "ticket subject as bool" do
      before do
        @ticket_initializer = Zendesk::Tickets::Initializer.new(
          accounts(:minimum),
          users(:minimum_admin),
          ticket: { subject: true }
        )
        @ticket = @ticket_initializer.ticket
      end

      it "normalizes boolean value to string" do
        assert @ticket.subject.is_a?(String)
      end
    end

    describe "collaborator_ids" do
      before do
        @requester = users(:minimum_admin)

        @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), @requester, ticket: {
          assignee_id: users(:minimum_agent).id,
          collaborator_ids: [users(:minimum_author).id],
          comment: { value: "Halps." }
        })

        @ticket = @ticket_initializer.ticket
      end

      it "changes current_collaborators" do
        assert @ticket.current_collaborators_changed?
      end

      it "sets the collaborators" do
        assert_equal 1, @ticket.collaborations.size
        assert_equal users(:minimum_author).id, @ticket.collaborations.first.user_id
      end
    end

    describe "recipient" do
      let(:original_ticket) { tickets(:minimum_1) }
      let(:requester) { users(:minimum_admin) }
      let(:params) do
        {
          id: original_ticket.nice_id,
          ticket: {
            recipient: "foo@bar.com",
            assignee_id: users(:minimum_agent).id,
            collaborator_ids: [users(:minimum_author).id],
            comment: { value: "Halps." }
          }
        }
      end

      describe "when not a light agent" do
        it "does not delete the original_recipient_address parameter" do
          ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), requester, params)
          assert_equal "foo@bar.com", ticket_initializer.ticket.original_recipient_address
        end
      end

      describe "as a light agent" do
        before { requester.stubs(:is_light_agent?).returns(true) }
        it "deletes the original_recipient_address parameter" do
          ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), requester, params)
          refute_equal "foo@bar.com", ticket_initializer.ticket.original_recipient_address
        end
      end
    end

    describe "ticket_form_id" do
      before do
        @ticket_form_id = ticket_forms(:default_ticket_form).id

        account = accounts(:minimum)
        account.stubs(:has_ticket_forms?).returns(has_ticket_forms)

        ticket_initializer = Zendesk::Tickets::Initializer.new(account, users(:minimum_admin), ticket: {
          ticket_form_id: @ticket_form_id
        })
        @ticket = ticket_initializer.ticket
      end

      describe "the account has ticket_froms feature" do
        let(:has_ticket_forms) { true }

        it "sets ticket_form_id to the ticket" do
          assert_equal @ticket_form_id, @ticket.ticket_form_id
        end
      end

      describe "the account has no ticket_forms feature" do
        let(:has_ticket_forms) { false }

        it "does not set ticket_form_id to the ticket" do
          assert_nil @ticket.ticket_form_id
        end
      end
    end

    describe "as a followup ticket" do
      let(:default_followup_ticket_params) do
        {
        via_followup_source_id: source_ticket.nice_id,
        assignee_id: users(:minimum_agent).id,
        comment: "",
        metadata: { key: "value" },
        description: 'asdf'
      }
      end

      let(:source_ticket) do
        source_ticket = tickets(:minimum_1)
        source_ticket.set_collaborators = [users(:minimum_end_user).id]
        source_ticket.status_id = StatusType.CLOSED
        source_ticket.will_be_saved_by(users(:minimum_admin))
        source_ticket.save!
        source_ticket
      end

      def create_new_followup_ticket(ticket_params = {})
        ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: ticket_params.merge(default_followup_ticket_params))
        ticket = ticket_initializer.ticket
        ticket.save!
        ticket
      end

      describe "when via_followup_source_id is present" do
        describe "when via_id is one of VOICE_VIA_TYPES" do
          describe_with_arturo_enabled :fix_voice_via_override_on_followup do
            it "sets the followup ticket via_id to closed ticket" do
              ticket = create_new_followup_ticket(via_id: ViaType.VOICEMAIL)
              assert_equal ViaType.CLOSED_TICKET, ticket.via_id
            end
          end

          describe_with_arturo_disabled :fix_voice_via_override_on_followup do
            it "does not let followup ticket via_id override voice via_id" do
              ticket = create_new_followup_ticket(via_id: ViaType.VOICEMAIL)
              refute_equal ViaType.CLOSED_TICKET, ticket.via_id
            end
          end
        end

        # example: TicketImporter sets via:channel params by default
        describe "when via: channel is set in ticket params" do
          describe "with :override_via_for_followups arturo enabled" do
            before { Arturo.enable_feature!(:override_via_for_followups) }
            it "sets the followup ticket via_id to closed ticket" do
              ticket = create_new_followup_ticket(via: { channel: ViaType.WEB_SERVICE })
              assert_equal ViaType.CLOSED_TICKET, ticket.via_id
              assert_equal ViaType.WEB_SERVICE, ticket.original_via_id
            end
          end

          describe "with :override_via_for_followups arturo disabled" do
            before { Arturo.disable_feature!(:override_via_for_followups) }
            it "does not override the user set via_id with followup via_id" do
              ticket = create_new_followup_ticket(via: { channel: ViaType.WEB_SERVICE })
              assert_equal ViaType.WEB_SERVICE, ticket.via_id
              assert_equal ViaType.WEB_FORM, ticket.original_via_id # WEB_FORM is the default via_id
            end
          end
        end
      end

      describe "when source ticket has CollaboratorType.LEGACY_CC collaborators" do
        describe_with_and_without_arturo_setting_enabled :follower_and_email_cc_collaborations do
          describe_with_and_without_arturo_enabled :email_ccs_for_followps_via_api do
            it "copies collaborators from source when collaborators param is not present" do
              ticket = create_new_followup_ticket
              assert_equal [users(:minimum_end_user).id], ticket.collaborators.map(&:id)
            end

            it "does not copy collaborators from source when collaborators param is nil" do
              ticket = create_new_followup_ticket(collaborators: nil)
              assert_equal [], ticket.collaborators.map(&:email)
            end

            it "uses new collaborators when collaborators param present" do
              ticket = create_new_followup_ticket(collaborators: 'asdf@example.com')
              assert_equal ['asdf@example.com'], ticket.collaborators.map(&:email)
            end
          end
        end
      end

      describe "when source ticket has CollaboratorType.EMAIL_CC and CollaboratorType.FOLLOWER collaborators" do
        let(:end_user) { users(:minimum_end_user) }
        let(:agent) { users(:minimum_agent) }
        let(:admin) { users(:minimum_admin) }

        let!(:source_ticket) do
          source_ticket = tickets(:minimum_1)
          source_ticket.collaborations = []
          source_ticket.collaborations.build(user_id: agent.id, collaborator_type: CollaboratorType.FOLLOWER)
          source_ticket.collaborations.build(user_id: end_user.id, collaborator_type: CollaboratorType.EMAIL_CC)
          source_ticket.status_id = StatusType.CLOSED
          source_ticket.will_be_saved_by(admin)

          # Need to save this with master CCs and Followers setting enabled so no transformation occurs
          # (this is enabled/stubbed here, but stubbed over in the before blocks for the following tests)
          source_ticket.account.stubs(has_follower_and_email_cc_collaborations_enabled?: true)
          source_ticket.save!

          source_ticket
        end

        describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
          it "copies email_ccs from source when email_ccs param is not present" do
            ticket = create_new_followup_ticket
            assert ticket.collaborators.map(&:id).include? end_user.id
          end

          it "does not copy email_ccs or followers from source when email_ccs param is nil" do
            ticket = create_new_followup_ticket(email_ccs: nil)
            assert_equal [], ticket.collaborators.map(&:id)
          end

          it "uses email_ccs param when present" do
            ticket = create_new_followup_ticket(email_ccs: [{ user_email: 'asdf@example.com'.upcase, action: :put }])
            assert_equal ['asdf@example.com'].to_set, ticket.collaborators.map(&:email).to_set
          end

          it "copies followers from source when followers param is not present" do
            ticket = create_new_followup_ticket
            assert ticket.collaborators.map(&:id).include? agent.id
          end

          it "does not copy followers or email_ccs from source when followers param is nil" do
            ticket = create_new_followup_ticket(followers: nil)
            assert_equal [], ticket.collaborators.map(&:id)
          end

          it "uses followers param when present" do
            minimum_admin = users(:minimum_admin)
            followers_params = source_ticket.user_ids_to_collaborators_hashes([minimum_admin.id])
            ticket = create_new_followup_ticket(followers: followers_params)
            assert_equal [minimum_admin.email].to_set, ticket.collaborators.map(&:email).to_set
          end

          it "uses collaborators param when present" do
            minimum_admin = users(:minimum_admin)
            ticket = create_new_followup_ticket(collaborators: [minimum_admin.id])
            assert_equal [minimum_admin.email], ticket.collaborators.map(&:email)
          end
        end

        describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
          it "copies end user collaborators from source" do
            ticket = create_new_followup_ticket
            assert ticket.collaborators.map(&:id).include? end_user.id
          end

          it "ignores email_ccs param when present and nil, copies email_ccs from source" do
            ticket = create_new_followup_ticket(email_ccs: nil)
            assert ticket.collaborators.map(&:id).include? end_user.id
          end

          it "ignores email_ccs param when present, copies email_ccs from source" do
            ticket = create_new_followup_ticket(email_ccs: [{ email: 'asdf@example.com', action: :put }])
            refute ticket.collaborators.map(&:email).include? 'asdf@example.com'
            assert ticket.collaborators.map(&:id).include? end_user.id
          end

          it "copies agent collaborators from source" do
            ticket = create_new_followup_ticket
            assert ticket.collaborators.map(&:id).include? agent.id
          end

          it "ignores followers param when present and nil, copies followers from source" do
            ticket = create_new_followup_ticket(followers: nil)
            assert ticket.collaborators.map(&:id).include? agent.id
          end

          it "converts new followers when present, uses existing collaborators" do
            admin = users(:minimum_admin)
            followers_params = source_ticket.user_ids_to_collaborators_hashes([admin.id])
            ticket = create_new_followup_ticket(followers: followers_params)
            assert_equal [admin.id, agent.id, end_user.id].to_set, ticket.collaborators.map(&:id).to_set
          end

          it "uses collaborators param when present" do
            minimum_admin = users(:minimum_admin)
            ticket = create_new_followup_ticket(collaborators: [minimum_admin.id])
            assert_equal [minimum_admin.email], ticket.collaborators.map(&:email)
          end
        end
      end

      it "sets via_id and original_via_id" do
        ticket = create_new_followup_ticket
        assert_equal ViaType.CLOSED_TICKET, ticket.via_id
        assert_equal ViaType.WEB_FORM, ticket.original_via_id
      end

      describe "audits" do
        before do
          source_ticket = tickets(:minimum_5)
          @field_id = source_ticket.ticket_field_entries.first.ticket_field_id

          params = {
            via_followup_source_id: source_ticket.nice_id,
            assignee_id: users(:minimum_agent).id,
            comment: "",
            description: 'something something'
          }

          ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: params)
          @ticket = ticket_initializer.ticket
          @ticket.save!
        end

        it "has an audit for ticket field creation events" do
          assert_includes @ticket.audits.first.events.collect(&:type), "Create"
        end

        it "has a create audit event for a set ticket field in source ticket" do
          assert_includes @ticket.audits.first.events.collect(&:value_reference), @field_id.to_s
        end
      end
    end

    describe "setting a comment to a string" do
      before do
        @requester = users(:minimum_admin)

        @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), @requester, ticket: {
          assignee_id: users(:minimum_agent).id,
          collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
          comment: "hooby do",
          metadata: { key: "value" },
          system_metadata: { client: "hello" },
          brand_id: 6
        })
      end

      it "makes comment a hash if it is a string" do
        assert_equal Comment, @ticket_initializer.ticket.comment.class
        assert_equal "hooby do", @ticket_initializer.ticket.description
      end
    end

    describe "rich comments" do
      let(:requester) { users(:minimum_admin) }

      before do
        @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), requester,
          ticket: {
            comment: { body: "New Comment", html_body: "New <strong>Comment</strong>" }
          })

        @ticket_initializer.ticket.save!
      end

      it "stores the rich comment" do
        comment = @ticket_initializer.ticket.comments.first

        assert_equal "rich", comment.format
        assert_equal "New <strong>Comment</strong>", comment.send(:value)
      end

      it "stores the plain text version of the comment as description" do
        assert_equal "New **Comment**", @ticket_initializer.ticket.description
      end
    end

    describe "for agents" do
      before do
        @requester = users(:minimum_admin)
        @account = accounts(:minimum)

        @ticket_initializer = Zendesk::Tickets::Initializer.new(
          @account,
          @requester,
          ticket: {
            assignee_id: users(:minimum_agent).id,
            collaborators: [users(:minimum_author).id, {name: "Kjeld", email: "kjeld@example.org"}],
            comment: {value: "Halps."},
            metadata: {key: "value"},
            system_metadata: {client: "hello"},
            brand_id: 6
          }
        )
      end

      describe "when the request is internal" do
        before do
          @ticket_initializer.stubs(:internal_client?).returns(true)
          @ticket = @ticket_initializer.ticket
        end

        it "sets metadata and system metadata on the audit" do
          metadata = @ticket.audit.metadata.to_hash
          assert_equal({ "system" => { "client" => "hello" }, "custom" => { "key" => "value" }}, metadata)
        end
      end

      describe "for any request" do
        before { @ticket = @ticket_initializer.ticket }

        it "returns a Ticket" do
          assert_equal Ticket, @ticket.class
        end

        it "sets attributes" do
          assert_equal users(:minimum_agent).id, @ticket[:assignee_id]
        end

        before_should "sets followup source" do
          Ticket.any_instance.expects(:set_followup_source).at_least_once.with(users(:minimum_admin))
        end

        it "sets the requester to the user that created the ticket" do
          assert_equal @requester, @ticket.requester
        end

        it "sets the submitter to the requester" do
          assert_equal @ticket.requester, @ticket.submitter
        end

        it "sets the collaborators" do
          assert_equal 2, @ticket.collaborations.size
          assert_includes @ticket.collaborations.map(&:user_id), users(:minimum_author).id
        end

        it "sets metadata on the audit, but not system metadata" do
          metadata = @ticket.audit.metadata.to_hash
          assert_equal({ "system" => {}, "custom" => { "key" => "value" }}, metadata)
        end

        it "sets brand on ticket" do
          assert_equal 6, @ticket.brand_id
        end

        describe "when ticket_followers_allowed and comment_email_ccs_allowed are enabled" do
          before do
            CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
          end

          describe "with collaborators, email_ccs, and followers params present" do
            let(:ticket) do
              Zendesk::Tickets::Initializer.new(
                @account,
                @requester,
                ticket: collaborators_params
              ).ticket
            end

            describe "with email_ccs and collaborators" do
              let(:collaborators_params) do
                {
                  email_ccs: [{user_email: "foo@bar.biz", user_name: "Bill Nye"}],
                  collaborators: [users(:minimum_author).id]
                }
              end

              it { assert_empty ticket.collaborations.select(&:legacy_cc?) }
              it { assert_equal 1, ticket.email_ccs.size }
            end

            describe "with followers and collaborators" do
              let(:collaborators_params) do
                {
                  followers: [{user_id: users(:minimum_agent).id, action: :put}],
                  collaborators: [users(:minimum_author).id]
                }
              end

              it { assert_empty ticket.collaborations.select(&:legacy_cc?) }
              it { assert_equal 1, ticket.followers.size }
            end

            describe "with only collaborators" do
              let(:collaborators_params) { {collaborators: [users(:minimum_author).id]} }

              it { assert_equal 1, ticket.collaborations.select(&:legacy_cc?).size }
            end
          end

          describe "when collaborator params are nil" do
            let(:ticket) do
              Zendesk::Tickets::Initializer.new(
                @account,
                @requester,
                ticket: { followers: nil, email_ccs: nil }
              ).ticket
            end

            it { assert_empty ticket.followers }
            it { assert_empty ticket.email_ccs }
            it { assert_empty ticket.collaborations }
          end

          describe "with email_ccs having new users" do
            before do
              @ticket_initializer = Zendesk::Tickets::Initializer.new(
                @account,
                @requester,
                ticket: {
                  subject: 'abc',
                  description: 'def',
                  priority_id: Zendesk::Types::PriorityType.HIGH,
                  ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                  assignee_id: users(:minimum_agent).id,
                  organization_id: organizations(:minimum_organization1).id,
                  email_ccs: email_ccs_value,
                  created_at: @created_at
                },
                comment: {value: "OMGWTFBBQ", is_public: false},
                email: 'brand_new_user@example.com'
              )
              @ticket = @ticket_initializer.ticket
              assert @ticket
            end

            describe "with email_ccs in array format" do
              let(:email_ccs_value) { [{user_email: "foo@bar.biz", user_name: "Bill Nye"}] }

              it { assert_equal 1, @ticket.email_ccs.size }
              it { assert_equal 1, @ticket.collaborations.size }

              describe "with email_ccs having bad data" do
                let(:email_ccs_value) do
                  [
                    { user_email: "foo@bar.biz", action: "blah" },
                    { user_email: "bar@foo.gov", action: -1 },
                    { user_email: "bar@foo.gov", action: false },
                  ]
                end

                it { assert_equal 3, @ticket.email_ccs.size }
                it { assert_equal 3, @ticket.collaborations.size }
              end
            end
          end

          describe_with_arturo_enabled :email_ccs_update_for_private_comment_fix do
            describe "adding a private comment with email_ccs" do
              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                      subject: 'abc123',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs_value,
                      created_at: @created_at,
                      comment: {value: "OMGWTFBBQ", public: false}
                  },
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              let(:email_ccs_value) { [{user_email: "foo123@bar.biz", user_name: "Bill Nye"}] }

              it 'ignores the email_ccs' do
                assert_equal 0, @ticket.email_ccs.size
              end
            end

            describe "adding a public comment with email_ccs" do
              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                      subject: 'abc123',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs_value,
                      created_at: @created_at,
                      comment: {value: "OMGWTFBBQ", public: true}
                  },
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              let(:email_ccs_value) { [{user_email: "foo123@bar.biz", user_name: "Bill Nye"}] }

              it 'adds the email_ccs' do
                assert_equal 1, @ticket.email_ccs.size
              end
            end

            describe "adding email_ccs without a comment" do
              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                      subject: 'abc123',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs_value,
                      created_at: @created_at
                  },
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              let(:email_ccs_value) { [{user_email: "foo123@bar.biz", user_name: "Bill Nye"}] }

              it 'adds the email_ccs' do
                assert_equal 1, @ticket.email_ccs.size
              end
            end

            describe "when private comment is declared outside the ticket params" do
              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                      subject: 'abc123',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs_value,
                      created_at: @created_at
                  },
                  comment: {value: "OMGWTFBBQ", public: false},
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              let(:email_ccs_value) { [{user_email: "foo123@bar.biz", user_name: "Bill Nye"}] }

              it 'does not add the email_ccs' do
                assert_equal 0, @ticket.email_ccs.size
              end
            end

            describe "when public comment is declared outside the ticket params" do
              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                      subject: 'abc123',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs_value,
                      created_at: @created_at
                  },
                  comment: {value: "OMGWTFBBQ", public: true},
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              let(:email_ccs_value) { [{user_email: "foo123@bar.biz", user_name: "Bill Nye"}] }

              it 'adds the email_ccs' do
                assert_equal 1, @ticket.email_ccs.size
              end
            end

            describe "when comment is a string declared outside the ticket params" do
              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                      subject: 'abc123',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs_value,
                      created_at: @created_at
                  },
                  comment: "lalala",
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              let(:email_ccs_value) { [{user_email: "foo123@bar.biz", user_name: "Bill Nye"}] }

              it 'adds the email_ccs' do
                assert_equal 1, @ticket.email_ccs.size
              end
            end

            describe "when comment is a string declared inside the ticket params" do
              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                      subject: 'abc123',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs_value,
                      comment: "lalala",
                      created_at: @created_at
                  },
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              let(:email_ccs_value) { [{user_email: "foo123@bar.biz", user_name: "Bill Nye"}] }

              it 'adds the email_ccs' do
                assert_equal 1, @ticket.email_ccs.size
              end
            end
          end

          describe_with_arturo_disabled :email_ccs_update_for_private_comment_fix do
            describe "adding a private comment with email_ccs" do
              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                      subject: 'abc123',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs_value,
                      created_at: @created_at,
                      comment: {value: "OMGWTFBBQ", public: false},
                  },
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              let(:email_ccs_value) { [{user_email: "foo123@bar.biz", user_name: "Bill Nye"}] }

              it 'adds the email_ccs' do
                assert_equal 1, @ticket.email_ccs.size
              end
            end

            describe "adding a public comment with email_ccs" do
              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                      subject: 'abc123',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs_value,
                      created_at: @created_at,
                      comment: {value: "OMGWTFBBQ", is_public: false},
                  },
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              let(:email_ccs_value) { [{user_email: "foo123@bar.biz", user_name: "Bill Nye"}] }

              it 'adds the email_ccs' do
                assert_equal 1, @ticket.email_ccs.size
              end
            end
          end

          describe "when preventing single API calls from adding more than the maximum number of email_ccs at once" do
            before do
              Zendesk::Tickets::Initializer.any_instance.stubs(max_email_ccs_limit: 2)
            end

            describe "when the net difference between email_ccs puts and deletes is below the maximum number of email_ccs" do
              email_ccs = [
                { user_email: "wang.zhihuan@tang.gov", action: "put" },
                { user_email: "bai.juyi@tang.gov", action: "put" },
                { user_email: "zhang.jiuling@tang.gov", action: "delete" }
              ]

              before do
                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  @account,
                  @requester,
                  ticket: {
                    subject: 'abc',
                    description: 'def',
                    priority_id: Zendesk::Types::PriorityType.HIGH,
                    ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                    assignee_id: users(:minimum_agent).id,
                    organization_id: organizations(:minimum_organization1).id,
                    email_ccs: email_ccs,
                    created_at: @created_at
                  },
                  comment: {value: "OMGWTFBBQ", is_public: false},
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              it "does not raise an exception and adds the email_ccs to the ticket" do
                assert_equal 2, @ticket.email_ccs.size
                assert_equal 2, @ticket.collaborations.size
              end
            end

            describe "when the API call attempts to add more than the maximum number of email_ccs at once" do
              email_ccs = [
                { user_email: "wang.zhihuan@tang.gov", action: "put" },
                { user_email: "bai.juyi@tang.gov", action: "put" },
                { user_email: "zhang.jiuling@tang.gov", action: "put" }
              ]

              it "raises an exception" do
                assert_raise(ZendeskApi::Controller::Errors::TooManyValuesError) do
                  Zendesk::Tickets::Initializer.new(
                    @account,
                    @requester,
                    ticket: {
                      subject: 'abc',
                      description: 'def',
                      priority_id: Zendesk::Types::PriorityType.HIGH,
                      ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                      assignee_id: users(:minimum_agent).id,
                      organization_id: organizations(:minimum_organization1).id,
                      email_ccs: email_ccs,
                      created_at: @created_at
                    },
                    comment: {value: "OMGWTFBBQ", is_public: false},
                    email: 'brand_new_user@example.com'
                  )
                end
              end
            end
          end

          describe "with followers and email_ccs params containing same user" do
            before do
              @ticket_initializer = Zendesk::Tickets::Initializer.new(
                @account,
                @requester,
                ticket: {
                  subject: 'abc',
                  description: 'def',
                  priority_id: Zendesk::Types::PriorityType.HIGH,
                  ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                  assignee_id: users(:minimum_agent).id,
                  organization_id: organizations(:minimum_organization1).id,
                  followers: followers_value,
                  email_ccs: email_ccs_value,
                  created_at: @created_at
                },
                comment: {value: "OMGWTFBBQ", is_public: false},
                email: 'brand_new_user@example.com'
              )
              @ticket = @ticket_initializer.ticket
              assert @ticket
            end

            describe "with email_ccs and followers in array format" do
              let(:email_ccs_value) { [{ user_id: users(:minimum_agent).id, action: :put }] }
              let(:followers_value) { [{ user_id: users(:minimum_agent).id, action: :put }] }

              it { assert_equal 1, @ticket.followers.size }
              it { assert_equal 1, @ticket.email_ccs.size }
              it { assert_equal 2, @ticket.collaborations.size }

              describe "with followers having bad data" do
                let(:email_ccs_value) { [] }
                let(:followers_value) do
                  [
                    { user_id: users(:minimum_agent).id, action: "blah" },
                    { user_id: users(:minimum_admin).id, action: -1 },
                    { user_id: "lkajsdfasdfs", action: false },
                  ]
                end

                it { assert_equal 2, @ticket.followers.size }
                it { assert_equal 2, @ticket.collaborations.size }
              end
            end
          end

          describe "with followers removing a user and email_ccs adding the same user" do
            let(:agent) { users(:minimum_agent) }
            let(:end_user) { users(:minimum_end_user) }

            before do
              @ticket = tickets(:minimum_1)
              @ticket.account = @account
              @ticket.save

              @ticket.collaborations.create(user: agent, collaborator_type: CollaboratorType.FOLLOWER)
              @ticket.collaborations.create(user: end_user, collaborator_type: CollaboratorType.EMAIL_CC)

              @ticket.collaborations.reload

              assert_equal 1, @ticket.followers.size
              assert_equal 1, @ticket.email_ccs.size
              assert_equal 2, @ticket.collaborations.size

              ticket_initializer = Zendesk::Tickets::Initializer.new(
                @account,
                @requester,
                id: @ticket.nice_id,
                ticket: {
                  subject: 'abc',
                  description: 'def',
                  priority_id: Zendesk::Types::PriorityType.HIGH,
                  ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                  assignee_id: users(:minimum_agent).id,
                  organization_id: organizations(:minimum_organization1).id,
                  followers: followers_value,
                  email_ccs: email_ccs_value,
                  created_at: @created_at
                },
                comment: {value: "OMGWTFBBQ", is_public: false},
                email: 'brand_new_user@example.com'
              )
              @ticket = ticket_initializer.ticket
              assert @ticket
              @ticket.save # verifying that collaborations are removed requires saving the ticket
            end

            describe "with email_ccs and followers in array format" do
              let(:followers_value) { [{ user_email: agent.email, action: :delete }] }
              let(:email_ccs_value) { [{ user_email: agent.email, action: :put }] }

              it { assert_equal 0, @ticket.followers.size }
              it { assert_equal 2, @ticket.email_ccs.size }
              it { assert_equal 2, @ticket.collaborations.size }
            end
          end
        end

        describe "when follower_and_email_cc_collaborations is not enabled" do
          let(:ticket) do
            Zendesk::Tickets::Initializer.new(
              @account,
              @requester,
              ticket: collaborators_params
            ).ticket
          end

          before do
            CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
          end

          describe "when a legacy CC field is present" do
            let(:collaborators_params) do
              {
                email_ccs: [{user_email: "foo@bar.biz", user_name: "Bill Nye"}],
                collaborators: [users(:minimum_author).id]
              }
            end

            it { assert_empty ticket.email_ccs }
            it { assert_equal 1, ticket.collaborations.select(&:legacy_cc?).size }
          end

          describe "when a legacy CC field is not present" do
            let(:collaborators_params) do
              {
                email_ccs: [
                  {user_email: "foo@bar.biz", user_name: "Bill Nye"},
                  {user_email: "user@email.com"}
                ],
                followers: [{user_id: users(:minimum_agent).id, action: :put}]
              }
            end

            it { assert_empty ticket.email_ccs }
            it { assert_empty ticket.followers }
            it { assert_equal 3, ticket.collaborations.select(&:legacy_cc?).size }

            it "handles email formats with a given name" do
              assert_includes ticket.collaborations.map { |collaboration|
                collaboration.user.name
              }, "Bill Nye"
            end
          end
        end
      end
    end

    describe "for anonymous users" do
      let(:requester) { nil }
      let(:email) { 'brand_new_user@example.com' }

      before do
        @created_at = 5.minutes.ago
        @ticket_initializer = Zendesk::Tickets::Initializer.new(
          accounts(:minimum),
          accounts(:minimum).anonymous_user,
          ticket: {
            subject: 'abc',
            description: 'def',
            priority_id: Zendesk::Types::PriorityType.HIGH,
            ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
            assignee_id: users(:minimum_agent).id,
            created_at: @created_at,
            requester: requester
          },
          comment: {value: "OMGWTFBBQ", is_public: false},
          email: email
        )
        @ticket = @ticket_initializer.ticket
        assert @ticket
      end

      it 'builds a ticket with a new requester' do
        assert @ticket.requester
        assert @ticket.requester.new_record?
        assert_equal 'brand_new_user@example.com', @ticket.requester.email
      end

      it 'sets the submitter to the requester' do
        assert_equal @ticket.requester, @ticket.submitter
      end

      it "does not set the priority" do
        assert_equal 0, @ticket.priority_id
      end

      it "does not set the ticket type" do
        assert_equal 0, @ticket.ticket_type_id
      end

      it "does not set the assignee" do
        assert_nil @ticket.assignee
      end

      it "does not set the specified created_at" do
        assert_not_equal @created_at, @ticket.created_at
      end

      it "does not allow private comments" do
        assert @ticket.comment.is_public
      end

      describe 'with requester param' do
        let(:email) { nil }
        let(:japanese) { translation_locales(:japanese) }
        let(:requester) { { name: 'Really New', email: 'really-new@example.com', locale: japanese.locale } }

        before do
          Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
          Account.any_instance.stubs(:available_languages).returns([japanese, ENGLISH_BY_ZENDESK])
        end

        it 'builds a ticket with a new requester' do
          assert @ticket.requester
          assert @ticket.requester.new_record?
          assert_equal 'really-new@example.com', @ticket.requester.email
          assert_equal 'Really New', @ticket.requester.name
          assert_equal japanese.id, @ticket.requester.locale_id
        end
      end
    end

    describe "for end-users" do
      let(:account) { accounts(:minimum) }
      let(:ccs_followers_enabled) { false }

      before do
        if ccs_followers_enabled
          CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
        else
          CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
        end

        @created_at = 5.minutes.ago
        @ticket_initializer = Zendesk::Tickets::Initializer.new(
          account,
          users(:minimum_end_user),
          ticket: {
            subject: 'abc',
            description: 'def',
            priority_id: Zendesk::Types::PriorityType.HIGH,
            ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
            assignee_id: users(:minimum_agent).id,
            organization_id: organizations(:minimum_organization1).id,
            collaborators: [users(:minimum_author).id],
            created_at: @created_at
          },
          comment: {value: "OMGWTFBBQ", is_public: false},
          email: 'brand_new_user@example.com'
        )
        @ticket = @ticket_initializer.ticket
        assert @ticket
      end

      it 'sets the end user as the requester' do
        assert_equal users(:minimum_end_user), @ticket.requester
      end

      it 'sets the submitter to the requester' do
        assert_equal @ticket.requester, @ticket.submitter
      end

      it 'sets the organization' do
        assert_equal @ticket.organization, organizations(:minimum_organization1)
      end

      it "does not set the priority" do
        assert_equal 0, @ticket.priority_id
      end

      it "does not set the ticket type" do
        assert_equal 0, @ticket.ticket_type_id
      end

      it "does not set the assignee" do
        assert_nil @ticket.assignee
      end

      it "does not set the specified created_at" do
        assert_not_equal @created_at, @ticket.created_at
      end

      it "does not allow private comments" do
        assert @ticket.comment.is_public
      end

      it "sets the collaborators" do
        assert_equal 1, @ticket.collaborations.size
        assert_includes @ticket.collaborations.map(&:user_id), users(:minimum_author).id
      end

      describe "when collaborators_addable_only_by_agents setting is true" do
        let(:account) do
          set_account_collaborators_addable_only_by_agents
        end

        describe "with CCs/Followers settings disabled" do
          it "does not set collaborators" do
            assert_equal 0, @ticket.collaborations.size
          end
        end

        describe "with CCs/Followers settings enabled" do
          let(:ccs_followers_enabled) { true }

          it "sets collaborators" do
            assert_equal 1, @ticket.collaborations.size
            assert_includes @ticket.collaborations.map(&:user_id), users(:minimum_author).id
          end
        end
      end

      # TODO: The "collaborators addable only by agents" setting should always be disabled for
      # CCs/Followers so this shouldn't matter too much going forward but we'll leave it for
      # now to ensure that we are actually ignoring it.
      [true, false].each do |collaborators_addable_only_by_agents_enabled|
        describe "when collaborators_addable_only_by_agents setting is #{collaborators_addable_only_by_agents_enabled ? 'enabled' : 'disabled'}" do
          if collaborators_addable_only_by_agents_enabled
            let(:account) { set_account_collaborators_addable_only_by_agents }
          end

          [true, false].each do |comment_email_ccs_allowed_enabled|
            describe "with email_ccs when follower_and_email_cc_collaborations enabled and comment_email_ccs_allowed #{comment_email_ccs_allowed_enabled ? 'enabled' : 'disabled'}" do
              before do
                CollaborationSettingsTestHelper.new(
                  feature_arturo: true,
                  feature_setting: true,
                  followers_setting: true,
                  email_ccs_setting: comment_email_ccs_allowed_enabled
                ).apply

                @ticket_initializer = Zendesk::Tickets::Initializer.new(
                  account,
                  users(:minimum_end_user),
                  ticket: {
                    subject: 'abc',
                    description: 'def',
                    priority_id: Zendesk::Types::PriorityType.HIGH,
                    ticket_type_id: Zendesk::Types::TicketType.PROBLEM,
                    assignee_id: users(:minimum_agent).id,
                    organization_id: organizations(:minimum_organization1).id,
                    email_ccs: [user_id: users(:minimum_author).id],
                    created_at: @created_at
                  },
                  comment: {value: "OMGWTFBBQ", is_public: false},
                  email: 'brand_new_user@example.com'
                )
                @ticket = @ticket_initializer.ticket
                assert @ticket
              end

              it "#{comment_email_ccs_allowed_enabled ? 'does' : 'does not'} use email_ccs data" do
                expected_collaborations_count = comment_email_ccs_allowed_enabled ? 1 : 0
                assert_equal expected_collaborations_count, @ticket.collaborations.size
              end
            end
          end
        end
      end

      describe 'when CCs/Followers is on and legacy collaborator params are sent' do
        let(:collaborators) { [users(:minimum_agent).id] }
        let(:ticket) do
          Zendesk::Tickets::Initializer.new(
            account,
            users(:minimum_end_user),
            ticket: { collaborators: collaborators }
          ).ticket
        end
        let(:ccs_followers_enabled) { true }

        it 'sets the collaborators as email_ccs' do
          assert_equal 1, ticket.email_ccs.size
        end

        it 'does not set collaborators as followers' do
          assert_empty ticket.followers
        end

        describe 'and the collaborators exceeds the max email cc limit' do
          before do
            Zendesk::Tickets::Initializer.any_instance.stubs(max_email_ccs_limit: 2)
          end

          let(:collaborators) { ['1@email.com', '2@email.com', '3@email.com'] }

          it 'raises an exception' do
            assert_raise(ZendeskApi::Controller::Errors::TooManyValuesError) do
              ticket
            end
          end
        end
      end
    end

    describe "for light agents and when CCs/Followers is enabled" do
      let(:account) { accounts(:minimum) }
      before do
        CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
      end

      describe 'when legacy collaborator params are sent' do
        let(:light_agent) { create_light_agent }
        let(:collaborators) { [users(:minimum_agent).id, users(:minimum_end_user).id] }
        let(:user) { light_agent }
        let(:initializer) do
          Zendesk::Tickets::Initializer.new(
            account,
            user,
            ticket: { collaborators: collaborators }
          )
        end
        let(:ticket) { initializer.ticket }
        subject { ticket }

        it 'normalizes collaborators as follower types and does not make changes to CCs' do
          Zendesk::Tickets::Initializer.any_instance.expects(:normalize_legacy_collaborators_with_follower_type)
          Zendesk::Tickets::Initializer.any_instance.expects(:normalize_legacy_collaborators_with_email_cc_type).never
          subject
        end
      end
    end

    describe "via info" do
      before do
        @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
          assignee_id: users(:minimum_agent).id,
          collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
          comment: "",
          metadata: { key: "value" },
          via: {
            channel: Zendesk::Types::ViaType.WEB_FORM,
            source: Zendesk::Types::ViaType.HELPCENTER
          }
        })
        @ticket = @ticket_initializer.ticket
      end

      it "sets via_id and via_reference_id" do
        assert_equal Zendesk::Types::ViaType.WEB_FORM, @ticket.via_id
        assert_equal Zendesk::Types::ViaType.HELPCENTER, @ticket.via_reference_id
      end
    end
  end

  describe "Ticket lookup" do
    before do
      @account = accounts(:minimum)
      @user    = @account.owner
    end

    describe "by id" do
      before do
        @ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: 123)
      end

      it "looks up the ticket by nice_id on the account" do
        @account.tickets.expects(:find_by_nice_id!).with(123, {}).returns(tickets(:minimum_2))
        assert_equal tickets(:minimum_2), @ticket_initializer.ticket
      end

      it "raises RecordNotFound when the id is not found" do
        assert_raise ActiveRecord::RecordNotFound do
          @ticket_initializer.ticket
        end
      end
    end

    describe "by suspended ticket" do
      before do
        @suspended_ticket = mock('suspended_ticket', to_ticket: tickets(:minimum_2))
        @account.suspended_tickets.expects(:find).with(123).returns(@suspended_ticket)
        @ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @user, suspended: 123)
        @ticket = @ticket_initializer.ticket
      end

      it "looks up the suspended ticket by id on the account" do
        assert_equal tickets(:minimum_2), @ticket
      end

      it "produces a ticket with a reference to the suspended ticket" do
        assert_equal @suspended_ticket, @ticket.suspended_ticket
      end
    end

    it "does not change via_id" do
      ticket = tickets(:minimum_2)
      ticket.update_attribute(:via_id, ViaType.MAIL)
      assert_equal ViaType.MAIL, ticket.reload.via_id
      ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: ticket.nice_id, ticket: { via_id: ViaType.WEB_FORM })
      assert_equal ViaType.MAIL, ticket_initializer.ticket.via_id
    end

    it "does not change assignee_id if nil to empty string" do
      ticket = tickets(:minimum_2)
      ticket.update_attribute(:assignee_id, nil)
      assert_nil ticket.reload.assignee_id
      ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: ticket.nice_id, ticket: { assignee_id: '' })
      assert_equal({}, ticket_initializer.ticket.delta_changes)
    end

    describe "if from_api specified" do
      it "sets audit via_id to WEB_SERVICE" do
        ticket = tickets(:minimum_2)
        ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: ticket.nice_id, ticket: { via_id: ViaType.WEB_FORM, status_id: StatusType.PENDING }, from_api: true)
        ticket_initializer.ticket.save!
        assert_equal ViaType.WEB_SERVICE, ticket_initializer.ticket.audits.last.via_id
      end

      it "does not set audit via_id to WEB_SERVICE if from_api not present" do
        ticket = tickets(:minimum_2)
        ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: ticket.nice_id, ticket: { via_id: ViaType.WEB_FORM, status_id: StatusType.PENDING })
        ticket_initializer.ticket.save!
        assert_equal ViaType.WEB_FORM, ticket_initializer.ticket.audits.last.via_id
      end

      it "audit via_id defaults to WEB_SERVICE if via channel is invalid" do
        ticket = tickets(:minimum_2)
        ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: ticket.nice_id, ticket: { via: { channel: "web" }, status_id: StatusType.PENDING }, from_api: true)
        ticket_initializer.send(:statsd_client).expects(:increment).with('channel.via.invalid').once
        ticket_initializer.ticket.save!
        assert_equal Zendesk::Types::ViaType.WEB_SERVICE, ticket_initializer.ticket.audits.last.via_id
      end
    end

    describe "with a specified created_at" do
      before do
        Timecop.freeze
        initializer = Zendesk::Tickets::Initializer.new(
          @account,
          @user,
          id: tickets(:minimum_2).nice_id,
          ticket: {created_at: 2.days.ago},
          comment: {value: 'foo'}
        )
        @ticket = initializer.ticket
      end

      it "does not update the ticket's created_at" do
        assert_not_equal 2.days.ago, @ticket.created_at
      end

      it "does not set the created_at of the comment" do
        assert_nil @ticket.comment.created_at
      end
    end

    describe "with a specified read-only attributes" do
      before do
        initializer = Zendesk::Tickets::Initializer.new(
          @account,
          @user,
          id: tickets(:minimum_2).nice_id,
          submitter_id: 123456,
          ticket: {description: 'updated description'},
          comment: {value: 'foo'}
        )
        @ticket = initializer.ticket
      end

      it "does not update the ticket's description" do
        assert_not_equal 'updated description', @ticket.description
      end

      it "does not update the ticket's submitter_id" do
        assert_not_equal 123456, @ticket.submitter_id
      end
    end

    describe "by an end-user" do
      before do
        @user = users(:minimum_end_user)
        ticket = tickets(:minimum_2)
        ticket.will_be_saved_by(@account.owner)
        ticket.update_attribute(:assignee_id, @account.owner_id)
        @ticket_initializer = Zendesk::Tickets::Initializer.new(
          @account,
          @user,
          id: ticket.nice_id,
          comment: { value: "OMGWTFBBQ", is_public: false },
          email: 'brand_new_user@example.com',
          ticket: {
            requester_id: users(:minimum_admin),
            requester_data: users(:minimum_admin).email,
            requester_name: users(:minimum_admin).name,
            solved: true
          }
        )
        @ticket = @ticket_initializer.ticket
      end

      it "does not allow setting the requester" do
        assert_equal @user, @ticket.requester
        assert_equal @user.id, @ticket.requester_id
      end

      it "does not allow private comments" do
        assert @ticket.comment.is_public
      end

      it "allows solving the ticket" do
        assert_equal Zendesk::Types::StatusType.SOLVED, @ticket.status_id
      end
    end
  end

  describe "New tickets" do
    before do
      @account = accounts(:minimum)
      @user    = @account.owner
    end

    describe "without parameters" do
      before do
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, {})
        @ticket = initializer.ticket
        assert @ticket.new_record?
      end

      it "belongs to the account" do
        assert_equal @account, @ticket.account
      end

      it "has the user as the requester" do
        assert_equal @user, @ticket.requester
      end

      it "has a via_id" do
        assert_equal ViaType.WEB_FORM, @ticket.via_id
      end
    end

    describe "with a via_id of ViaType.LOTUS" do
      before do
        @initializer = Zendesk::Tickets::Initializer.new(@account, @user, via_id: ViaType.LOTUS)
      end

      describe "when requesting JSON" do
        before do
          @initializer.stubs(:request_params).returns(stub(xml_or_json_format: true))
          @ticket = @initializer.ticket
        end

        it "sets the via_id to ViaType.WEB_FORM" do
          assert_equal ViaType.WEB_FORM, @ticket.via_id
        end
      end
    end

    describe "with a forum entry" do
      before do
        @account.field_subject.stubs(:is_active?).returns(true)
        @entry = @account.entries.first
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, entry_id: @entry.id)
        @ticket = initializer.ticket
        assert @ticket.new_record?
      end

      it "has a reference to the entry" do
        assert_equal @entry.id, @ticket.entry_id
      end

      it "has the entry title as the subject" do
        assert_equal @entry.title, @ticket.subject
      end
    end

    describe "with a specified description" do
      before do
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, ticket: { description: "foo" })
        @ticket = initializer.ticket
        @ticket.save!
        @ticket.reload
        @comment = @ticket.comments.first
      end

      it "has the specified description" do
        assert_equal 'foo', @ticket.description
      end

      it "has a comment" do
        assert_equal 1, @ticket.comments.size
      end

      it "has a comment with the description as the body" do
        assert_equal 'foo', @comment.body
      end

      it "has a comment with the requester as the author" do
        assert_equal @ticket.requester, @comment.author
      end
    end

    describe "with an invalid status_id" do
      it "doesn't allow setting ticket status to archived" do
        assert_raises Zendesk::UnknownAttributeError do
          Zendesk::Tickets::Initializer.new(@account, @user, ticket: { description: "foo", status: "archived"})
        end
      end
    end

    describe "with a comment" do
      describe "with empty uploads" do
        it "does not break with empty uploads" do
          initializer2 = Zendesk::Tickets::Initializer.new(@account, @user, comment: { uploads: [""], value: "foo"})
          ticket = initializer2.ticket
          assert_nil ticket.comment.uploads
        end
      end

      describe "with duplicate upload tokens" do
        before do
          @attach1 = attachments(:upload_token_attachment)
          @attach1.save
          @token = @attach1.source.value
          @initializer = Zendesk::Tickets::Initializer.new(@account, @user, ticket: { subject: "bloo", comment: { uploads: [@token, @token, @token], value: "foo"}})
        end

        it "has only unique uploads" do
          assert_equal 1, @initializer.ticket.comment.uploads.size
        end

        it "has a comment event with attachment after save" do
          @initializer.ticket.save
          ticket = @initializer.ticket
          events = ticket.audits.first.events.select { |e| e.is_a?(Comment) }
          assert events.first.trusted?
          assert_equal 1, events.first.uploads.size
        end

        it "does not save with invalid tokens" do
          @initializer = Zendesk::Tickets::Initializer.new(@account, @user, ticket: { subject: "bloo", comment: { uploads: ["something_bogus", @token, @token], value: "foo"}})
          refute @initializer.ticket.valid?
        end
      end

      describe "without privacy info" do
        before do
          initializer = Zendesk::Tickets::Initializer.new(@account, @user, comment: { value: 'foo' })
          @ticket = initializer.ticket
          assert @ticket.new_record?
        end

        it "has the comment value as a description" do
          assert_equal 'foo', @ticket.description
        end

        it "has a public comment" do
          assert @ticket.comment.is_public
        end
      end

      describe "that is private" do
        before do
          initializer = Zendesk::Tickets::Initializer.new(@account, @user, comment: { value: 'foo', is_public: false })
          @ticket = initializer.ticket
          assert @ticket.new_record?
        end

        it "has the comment value as a description" do
          assert_equal 'foo', @ticket.description
        end

        it "has a private comment" do
          refute @ticket.comment.is_public
        end
      end

      describe "that is public" do
        before do
          initializer = Zendesk::Tickets::Initializer.new(@account, @user, comment: { value: 'foo', is_public: true })
          @ticket = initializer.ticket
          assert @ticket.new_record?
        end

        it "has the comment value as a description" do
          assert_equal 'foo', @ticket.description
        end

        it "has a public comment" do
          assert @ticket.comment.is_public
        end

        describe "with an agent that only can make private comments" do
          before do
            @user.stubs(:can?).returns(false)
            initializer = Zendesk::Tickets::Initializer.new(@account, @user, comment: { value: 'foo', is_public: true })
            @ticket = initializer.ticket
            assert @ticket.new_record?
          end

          it "has a private comment" do
            refute @ticket.comment.is_public
          end
        end
      end
    end

    describe "with a specified requester" do
      let(:requester_data) do
        {
          requester_email: users(:minimum_search_user).email,
          requester_name: users(:minimum_search_user).name
        }
      end

      before do
        @author = users(:minimum_agent)
        @minimum_search_user = users(:minimum_search_user)
        assert_not_equal @author, @user
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        describe_with_arturo_setting_enabled :agent_can_change_requester do
          describe 'when `requester_data` is present' do
            it 'sets the requester from `requester_data`' do
              update_ticket_requester(requester_data)

              assert_equal @minimum_search_user, @ticket.requester
            end
          end

          describe 'when `requester_data` is absent' do
            it 'sets the requester from the specified `requester_id`' do
              update_ticket_requester

              assert_equal @author, @ticket.requester
            end
          end
        end

        describe_with_arturo_setting_disabled :agent_can_change_requester do
          describe 'when `requester_data` is present' do
            it 'does not update the requester' do
              update_ticket_requester(requester_data)

              assert_not_equal @minimum_search_user, @ticket.requester
              assert_not_equal @author, @ticket.requester
            end
          end

          describe 'when `requester_data` is absent' do
            it 'does not update the requester' do
              update_ticket_requester

              assert_not_equal @author, @ticket.requester
            end
          end
        end
      end

      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        describe_with_arturo_setting_enabled :collaboration_enabled do
          describe 'when `requester_data` is present' do
            it 'sets the requester from `requester_data`' do
              update_ticket_requester(requester_data)

              assert_equal @minimum_search_user, @ticket.requester
            end
          end

          describe 'when `requester_data` is absent' do
            it 'sets the requester from the specified `requester_id`' do
              update_ticket_requester

              assert_equal @author, @ticket.requester
            end
          end
        end

        describe_with_arturo_setting_disabled :collaboration_enabled do
          describe 'when `requester_data` is present' do
            it 'does not update the requester' do
              update_ticket_requester(requester_data)

              assert_not_equal @minimum_search_user, @ticket.requester
              assert_not_equal @author, @ticket.requester
            end
          end

          describe 'when `requester_data` is absent' do
            it 'does not update the requester' do
              update_ticket_requester

              assert_not_equal @author, @ticket.requester
            end
          end
        end
      end

      describe "by id" do
        before do
          initializer = Zendesk::Tickets::Initializer.new(@account, @user, ticket: { description: 'foo', requester_id: @author.id })
          @ticket = initializer.ticket
          @ticket.save!
          @ticket.reload
          @comment = @ticket.comments.first
        end

        it "has the comment author as the requester" do
          assert_equal @author, @ticket.requester
        end

        it "creates a comment with that requester as the author" do
          assert_equal 1, @ticket.comments.size
          assert_equal @author, @comment.author
        end

        it "has the agent as the submitter" do
          assert_equal @user, @ticket.submitter
        end
      end

      describe "by email, name and with locale" do
        before do
          initializer = Zendesk::Tickets::Initializer.new(
            @account,
            @user,
            ticket: {
              description: 'foo',
              requester: { email: "new_email@example.com", name: "New Email User", locale_id: translation_locales(:brazilian_portuguese).id }
            }
          )

          User.any_instance.expects(:should_clear_locale?).returns(false)

          @ticket = initializer.ticket
          @ticket.save!
          @ticket.reload
        end

        it "sets the email" do
          assert_equal "new_email@example.com", @ticket.requester.email
        end

        it "sets the name" do
          assert_equal "New Email User", @ticket.requester.name
        end

        it "sets the locale id" do
          assert_equal translation_locales(:brazilian_portuguese).id, @ticket.requester.reload.locale_id
        end
      end

      describe "by email" do
        before do
          initializer = Zendesk::Tickets::Initializer.new(@account, @user, ticket: { description: 'foo', requester_email: @author.email })
          @ticket = initializer.ticket
          @ticket.save!
          @ticket.reload
          @comment = @ticket.comments.first
        end

        it "has the comment author as the requester" do
          assert_equal @author, @ticket.requester
        end

        it "creates a comment with the requester as the author" do
          assert_equal 1, @ticket.comments.size
          assert_equal @author, @comment.author
        end

        it "has the agent as the submitter" do
          assert_equal @user, @ticket.submitter
        end
      end

      describe "by email for a new user where they are also CC'd #1" do
        before do
          initializer = Zendesk::Tickets::Initializer.new(@account, @user, ticket: { description: 'foo', requester_email: 'n00b@example.com', set_collaborators: ["n00b@example.com"]})
          @ticket = initializer.ticket
          @ticket.save!
          @ticket.reload
          @comment = @ticket.comments.first
        end

        it "works" do
          assert_equal 1, @ticket.comments.size
          assert_equal @ticket.requester.email, "n00b@example.com"
          assert_equal @ticket.collaborations.size, 1
          assert_equal @ticket.collaborations.first.user_id, @ticket.requester.id
        end
      end
    end

    describe "with a specified created_at" do
      before do
        travel_to Time.now
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, ticket: {created_at: 2.days.ago}, comment: { value: 'foo' })
        @ticket = initializer.ticket
      end

      after { travel_back }

      it "creates a ticket with the specified created_at" do
        assert_equal 2.days.ago, @ticket.created_at
      end

      it "creates a comment with the specified created_at" do
        assert_equal 2.days.ago, @ticket.comment.created_at
      end
    end
  end

  describe "ticket tags" do
    before do
      @account = accounts(:minimum)
      @user    = @account.owner
      @ticket  = tickets(:minimum_2)
      @tags    = @ticket.tag_array
    end

    describe "when account does not have ticket_tagging settings enabled" do
      let(:initializer) do
        Zendesk::Tickets::Initializer.new(@account, @user, id: @ticket.nice_id, ticket: {
          tags: 'abc',
          set_tags: 'def',
          additional_tags: 'ghi',
          remove_tags: 'jkl',
          via_id: ViaType.WEB_FORM
        })
      end

      before do
        @account.settings.stubs(ticket_tagging?: false)
      end

      describe_with_arturo_enabled :log_updating_ticket_tags_without_ticket_tagging do
        it "logs the necessary information" do
          span = stub(:span)
          span.expects(:set_tag).with(Datadog::Ext::Analytics::TAG_ENABLED, true)
          span.expects(:set_tag).with('zendesk.account_id', @account.id)
          span.expects(:set_tag).with('zendesk.account_subdomain', @account.subdomain)
          span.expects(:set_tag).with('zendesk.ticket_tagging_fix', @account.has_ticket_tagging_fix?)
          span.expects(:set_tag).with('zendesk.user_can_edit_tags', true)
          span.expects(:set_tag).with('zendesk.new_record', @ticket.new_record?)
          span.expects(:set_tag).with('zendesk.ticket_params.via_id', ViaType.WEB_FORM)
          span.expects(:set_tag).with('zendesk.ticket_params.tags', 'abc')
          span.expects(:set_tag).with('zendesk.ticket_params.set_tags', 'def')
          span.expects(:set_tag).with('zendesk.ticket_params.additional_tags', 'ghi')
          span.expects(:set_tag).with('zendesk.ticket_params.remove_tags', 'jkl')
          span.expects(:set_tag).with('zendesk.ticket_params.adjusted_tags', true)

          ZendeskAPM.expects(:trace).
            with('without-ticket-tagging', service: 'tickets-initializer').
            yields(span)

          assert_instance_of Ticket, initializer.ticket
        end
      end

      describe_with_arturo_disabled :log_updating_ticket_tags_without_ticket_tagging do
        it "doesn't log anything" do
          ZendeskAPM.expects(:trace).
            with('without-ticket-tagging', service: 'tickets-initializer').
            never

          assert_instance_of Ticket, initializer.ticket
        end
      end
    end

    describe "when user can edit tags" do
      before do
        @user.stubs(:can?).returns(true)
      end

      it "allows setting tags" do
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: @ticket.nice_id, ticket: { set_tags: 'abc'})
        ticket = initializer.ticket
        ticket.save!
        assert_equal ['abc'], ticket.reload.tag_array
      end

      it "allows adding a tag" do
        assert @tags.any?
        new_tags = @tags + ['additional_tag']
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: @ticket.nice_id, ticket: { additional_tags: 'additional_tag' })
        ticket = initializer.ticket
        ticket.save!
        assert_equal new_tags.sort, ticket.reload.tag_array
      end

      it "allows adding multiple tags" do
        assert @tags.any?
        additional_tags = ['additional_tag1', 'additional_tag2']
        new_tags = @tags + additional_tags
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: @ticket.nice_id, ticket: { additional_tags: additional_tags.join(' ') })
        ticket = initializer.ticket
        ticket.save!
        assert_equal new_tags.sort, ticket.reload.tag_array
      end

      it "allows removing tags" do
        assert @tags.any?
        new_tags = @tags - [@tags[0]]
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: @ticket.nice_id, ticket: { remove_tags: @tags[0]})
        ticket = initializer.ticket
        ticket.save!
        assert_equal new_tags, ticket.reload.tag_array
      end
    end

    describe "when user can not edit tags" do
      before do
        @user.stubs(:can?).returns(false)
      end

      it "does not allow setting tags" do
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: @ticket.nice_id, ticket: { set_tags: 'abc'})
        ticket = initializer.ticket
        ticket.save!
        assert_equal @tags, ticket.reload.tag_array
      end

      it "does not allow removing tags" do
        assert @tags.any?
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: @ticket.nice_id, ticket: { remove_tags: @tags[0]})
        ticket = initializer.ticket
        ticket.save!
        assert_equal @tags, ticket.reload.tag_array
      end
    end
  end

  describe "ticket attribute values" do
    before do
      @account = accounts(:minimum)
      @user    = @account.owner
      @ticket  = tickets(:minimum_2)
    end

    describe "when user can edit attribute values" do
      before do
        @user.stubs(:can?).returns(true)
      end

      it "allows setting attribute values" do
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: @ticket.nice_id, ticket: { attribute_value_ids: ['abc', 'def']})
        ticket = initializer.ticket
        assert_equal ['abc', 'def'], ticket.instance_variable_get(:@attribute_value_ids)
      end
    end

    describe "when user cannot edit attribute values" do
      before do
        @user.stubs(:can?).returns(false)
      end

      it "does not allow setting attribute values" do
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, id: @ticket.nice_id, ticket: { attribute_value_ids: ['abc', 'def']})
        ticket = initializer.ticket
        assert_nil ticket.instance_variable_get(:@attribute_value_ids)
      end
    end
  end

  describe "satisfaction probability" do
    describe "the account doesn't have satsifaction_prediicton" do
      before do
        Arturo.disable_feature!(:satisfaction_prediction)
        @account = accounts(:minimum)
        @user    = @account.owner
        @ticket  = tickets(:minimum_2)
      end

      it "doesn't save a value when passed satisfaction probability" do
        probability = 0.91029
        ticket_changes = { id: @ticket.nice_id, ticket: { satisfaction_probability: probability } }
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, ticket_changes)
        ticket = initializer.ticket

        assert ticket.save
        assert_nil ticket.satisfaction_probability
      end
    end

    describe "the account has satisfaction_prediction" do
      before do
        Arturo.enable_feature!(:satisfaction_prediction)
        @account = accounts(:minimum)
        @account.subscription.send(:write_attribute, :plan_type, SubscriptionPlanType.EXTRALARGE)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
        @account.settings.satisfaction_prediction = true
        @user = @account.owner
        @ticket = tickets(:minimum_2)
      end

      it "saves the passed satisfaction_probability" do
        probability = 0.91029
        ticket_changes = { id: @ticket.nice_id, ticket: { satisfaction_probability: probability } }
        initializer = Zendesk::Tickets::Initializer.new(@account, @user, ticket_changes)
        ticket = initializer.ticket

        ticket.save
        assert_equal probability, ticket.satisfaction_probability
      end
    end
  end

  describe "ticket fields" do
    before do
      @account = accounts(:minimum)
      @ticket_hash = {
        ticket: {
          assignee_id: users(:minimum_agent).id,
          collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
          comment: { value: "Halps." },
          metadata: { key: "value" }
        }
      }

      @ticket_hash[:ticket].merge!(fields: subject)
    end

    describe "with an end user" do
      subject { { ticket_fields(:field_tagger_custom).id => "not_bad" } }

      before do
        @requester = users(:minimum_end_user)
      end

      describe "with a valid ticket field" do
        before do
          @ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @requester, @ticket_hash)
          @ticket = @ticket_initializer.ticket
        end

        it "adds the ticket field" do
          assert_equal ["not_bad"], @ticket.ticket_field_entries.map(&:value)
        end
      end

      describe "with an invalid ticket field" do
        before do
          @ticket_field = ticket_fields(:field_tagger_custom)
          @ticket_field.is_editable_in_portal = false
          @ticket_field.save!

          @ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @requester, @ticket_hash)
          @ticket = @ticket_initializer.ticket
        end

        it "does not add the ticket field" do
          assert_empty(@ticket.ticket_field_entries)
        end
      end
    end

    describe "normalization" do
      before do
        @requester = users(:minimum_admin)
        @ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @requester, @ticket_hash)
        @ticket = @ticket_initializer.ticket
      end

      describe "single ticket field" do
        describe "with single id and value keys" do
          subject { { id: ticket_fields(:field_text_custom).id, value: "abc" } }

          it "sets the ticket field" do
            assert_equal ["abc"], @ticket.ticket_field_entries.collect(&:value)
          end
        end

        describe "with single id and value keys in an array" do
          subject { [{ id: ticket_fields(:field_text_custom).id, value: "abc" }] }

          it "sets the ticket field" do
            assert_equal ["abc"], @ticket.ticket_field_entries.collect(&:value)
          end
        end

        describe "with id/value key/value pair" do
          subject do
            { ticket_fields(:field_text_custom).id => "abc",
              ticket_fields(:field_textarea_custom).id => "def" }
          end

          it "sets the ticket field" do
            assert_equal ["abc", "def"], @ticket.ticket_field_entries.collect(&:value).sort
          end
        end

        describe "with id/value key/value pair in an array" do
          subject { [{ ticket_fields(:field_text_custom).id => "abc"}] }

          it "sets the ticket field" do
            assert_equal ["abc"], @ticket.ticket_field_entries.collect(&:value).sort
          end
        end

        describe "with an invalid type" do
          subject { "wtf" }

          it "sets the ticket field" do
            assert_equal [], @ticket.ticket_field_entries.collect(&:value).sort
          end
        end

        describe "with mixed content" do
          subject do
            { :id => ticket_fields(:field_text_custom).id, :value => "abc",
              ticket_fields(:field_textarea_custom).id => "def" }
          end

          it "sets the ticket field" do
            assert_equal ["abc"], @ticket.ticket_field_entries.collect(&:value).sort
          end
        end
      end

      describe "multiple ticket fields" do
        describe "with id and value as keys" do
          subject do
            [
              { id: ticket_fields(:field_text_custom).id, value: "abc" },
              { id: ticket_fields(:field_textarea_custom).id, value: "def" }
            ]
          end

          it "sets the ticket fields" do
            assert_equal ["abc", "def"], @ticket.ticket_field_entries.collect(&:value).sort
          end
        end

        describe "with id/value key/value pair in an array" do
          subject do
            [{ ticket_fields(:field_text_custom).id => "abc"},
             { ticket_fields(:field_textarea_custom).id => "def" }]
          end

          it "sets the ticket field" do
            assert_equal ["abc", "def"], @ticket.ticket_field_entries.collect(&:value).sort
          end
        end

        describe "with invalid content" do
          subject { ['abc', 'def'] }

          it "sets the ticket fields" do
            assert_equal [], @ticket.ticket_field_entries.collect(&:value)
          end
        end

        describe "with mixed content" do
          subject do
            [{ id: ticket_fields(:field_text_custom).id, value: "abc"},
             { ticket_fields(:field_textarea_custom).id => "def" }]
          end

          it "sets the ticket field" do
            assert_equal ["abc", "def"], @ticket.ticket_field_entries.collect(&:value).sort
          end
        end
      end

      describe "no fields" do
        describe "with empty array" do
          subject { [] }

          it "does not set fields" do
            assert_equal [], @ticket.ticket_field_entries.collect(&:value)
          end
        end

        describe "with nil" do
          subject { nil }

          it "does not set fields" do
            assert_equal [], @ticket.ticket_field_entries.collect(&:value)
          end
        end
      end
    end
  end

  describe "ticket properties" do
    let(:account) { accounts(:minimum) }
    let(:user) { account.owner }
    let(:ticket) { tickets(:minimum_2) }
    let(:ticket_field) { ticket_fields(:field_textarea_custom) }

    describe "when user can edit properties" do
      before { user.stubs(:can?).with(anything, anything).returns(true) }

      it "allows setting fields" do
        initializer = Zendesk::Tickets::Initializer.new(account, user,
          id: ticket.nice_id,
          ticket: {
            fields: [{ ticket_field.id => "abc"}]
          })
        updated_ticket = initializer.ticket
        assert_equal "abc", updated_ticket.ticket_field_entries.detect { |tfe| tfe.ticket_field_id == ticket_field.id }&.value
      end
    end

    describe "when user can't edit properties" do
      before do
        user.stubs(:can?).with(anything, anything).returns(true)
        user.stubs(:can?).with(:edit_properties, anything).returns(false)
      end

      it "doesn't allow setting system fields on existing tickets" do
        initializer = Zendesk::Tickets::Initializer.new(account, user,
          id: ticket.nice_id,
          ticket: {
            status_id: StatusType.PENDING,
            priority_id: Zendesk::Types::PriorityType.HIGH,
            ticket_type_id: Zendesk::Types::TicketType.PROBLEM
          })
        updated_ticket = initializer.ticket
        refute_equal updated_ticket.status_id, StatusType.PENDING
        refute_equal updated_ticket.priority_id, Zendesk::Types::PriorityType.HIGH
        refute_equal updated_ticket.ticket_type_id, Zendesk::Types::TicketType.PROBLEM
      end

      it "allows setting system fields on new tickets" do
        initializer = Zendesk::Tickets::Initializer.new(account, user,
          ticket: {
            priority_id: Zendesk::Types::PriorityType.HIGH,
            ticket_type_id: Zendesk::Types::TicketType.PROBLEM
          })
        new_ticket = initializer.ticket
        assert_equal new_ticket.priority_id, Zendesk::Types::PriorityType.HIGH
        assert_equal new_ticket.ticket_type_id, Zendesk::Types::TicketType.PROBLEM
      end

      it "doesn't allow setting fields on existing tickets" do
        initializer = Zendesk::Tickets::Initializer.new(account, user,
          id: ticket.nice_id,
          ticket: {
            fields: [{ ticket_field.id => "abc"}]
          })
        updated_ticket = initializer.ticket
        assert_nil updated_ticket.ticket_field_entries.detect { |tfe| tfe.ticket_field_id == ticket_field.id }&.value
      end

      it "allows setting fields on new tickets" do
        initializer = Zendesk::Tickets::Initializer.new(account, user,
          ticket: {
            fields: [{ ticket_field.id => "abc"}]
          })
        new_ticket = initializer.ticket
        assert_equal "abc", new_ticket.ticket_field_entries.detect { |tfe| tfe.ticket_field_id == ticket_field.id }&.value
      end
    end
  end

  describe "custom fields" do
    before do
      @account = accounts(:minimum)
      setup_custom_fields(@account)
    end

    it "has a writable field" do
      assert_equal @writable_custom_field.id, @account.ticket_fields.find(@writable_custom_field.id).id
      assert @writable_custom_field.is_editable_in_portal
    end

    it "has a readonly field" do
      assert_equal @read_only_custom_field.id, @account.ticket_fields.find(@read_only_custom_field.id).id
      refute @read_only_custom_field.is_editable_in_portal
    end

    describe "given an agent" do
      before do
        @ticket = create_ticket_with_custom_fields(@account, users(:minimum_agent))
      end

      it "sets a writable field" do
        entry = @ticket.ticket_field_entries.detect { |e| e.ticket_field_id == @writable_custom_field.id }
        assert_not_nil entry
        assert_equal "zen", entry.value
      end

      it "sets a readonly field" do
        entry = @ticket.ticket_field_entries.detect { |e| e.ticket_field_id == @read_only_custom_field.id }
        assert_not_nil entry
        assert_equal "desk", entry.value
      end
    end

    describe "given a light agent" do
      before do
        user = users(:minimum_agent)
        user.permission_set = user.account.permission_sets.create!(name: I18n.t("txt.default.roles.light_agent.name"), role_type: PermissionSet::Type::LIGHT_AGENT)
        user.permission_set.permissions.set(PermissionSet::LightAgent.configuration)
        user.stubs(:can?).with(anything, anything)
        user.stubs(:can?).with(:edit_properties, anything).returns(false)
        @ticket = create_ticket_with_custom_fields(@account, user)
      end

      it "sets a writable field" do
        entry = @ticket.ticket_field_entries.detect { |e| e.ticket_field_id == @writable_custom_field.id }
        assert_not_nil entry
        assert_equal "zen", entry.value
      end

      it "sets a readonly field" do
        entry = @ticket.ticket_field_entries.detect { |e| e.ticket_field_id == @read_only_custom_field.id }
        assert_not_nil entry
        assert_equal "desk", entry.value
      end
    end

    describe "given an end-user" do
      before do
        @ticket = create_ticket_with_custom_fields(@account, users(:minimum_end_user))
      end

      it "sets a writable field" do
        entry = @ticket.ticket_field_entries.detect { |e| e.ticket_field_id == @writable_custom_field.id }
        assert_not_nil entry
        assert_equal "zen", entry.value
      end

      it "does not set a readonly field" do
        refute @ticket.ticket_field_entries.detect { |e| e.ticket_field_id == @read_only_custom_field.id }
      end
    end
  end

  describe "via_id" do
    before do
      @account = accounts(:minimum)
      @user    = @account.owner
      @request_params = Zendesk::Tickets::HttpRequestParameters.new({})
      @request_params.stubs(
        remote_ip: '127.0.0.1',
        user_agent: 'Internet Explorer 1.0',
        url: 'http://example.com/some/path',
        method: 'http-method',
        headers: { }
      )
      @params = {ticket: {}}
    end

    describe "from an API request" do
      before { @request_params.stubs(:xml_or_json_format).returns(true) }

      describe "without a via_id" do
        before do
          @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
        end
        it("is ViaType.WEB_SERVICE") { assert_equal ViaType.WEB_SERVICE, @ticket.audit.via_id }
      end

      describe "with a via_id of ViaType.CHAT" do
        before do
          @params[:ticket][:via_id] = ViaType.CHAT
          @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
        end
        it("is ViaType.CHAT") { assert_equal ViaType.CHAT, @ticket.audit.via_id }
      end

      describe "with a via_id of ViaType.MOBILE_SDK" do
        before do
          @params[:ticket][:via_id] = ViaType.MOBILE_SDK
          @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
        end
        it("has ViaType.MOBILE_SDK") { assert_equal ViaType.MOBILE_SDK, @ticket.audit.via_id }
      end

      describe "with a via_id of ViaType.GET_SATISFACTION" do
        before do
          @params[:ticket][:via_id] = ViaType.GET_SATISFACTION
          @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
        end
        it("has ViaType.GET_SATISFACTION") { assert_equal ViaType.GET_SATISFACTION, @ticket.audit.via_id }
      end

      describe "with a via_id of ViaType.WEB_FORM" do
        before do
          @params[:ticket][:via_id] = ViaType.WEB_FORM
          @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
        end
        it("has ViaType.WEB_SERVICE") { assert_equal ViaType.WEB_SERVICE, @ticket.audit.via_id }
      end

      describe "with a via_id of ViaType.API_VOICEMAIL" do
        before do
          @params[:ticket][:via_id] = ViaType.API_VOICEMAIL
        end

        describe "with via channel of ViaType.API_VOICEMAIL" do
          before do
            @params[:ticket][:via] = { channel: ViaType.API_VOICEMAIL }
            @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
          end

          it "is ViaType.API_VOICEMAIL" do
            assert_equal ViaType.API_VOICEMAIL, @ticket.audit.via_id
          end
        end
      end

      describe "with a via_id of ViaType.SATISFACTION_PREDICTION" do
        before do
          @params[:ticket][:via_id] = ViaType.SATISFACTION_PREDICTION
          @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
        end

        it("has ViaType.SATISFACTION_PREDICTION") { assert_equal ViaType.SATISFACTION_PREDICTION, @ticket.audit.via_id }

        describe "with an internal client" do
          before do
            @request_params.stubs(internal_client?: true)
            @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
          end

          it("has ViaType.SATISFACTION_PREDICTION") { assert_equal ViaType.SATISFACTION_PREDICTION, @ticket.audit.via_id }
        end
      end

      describe "with a via_id of ViaType.WEB_WIDGET" do
        before do
          @params[:ticket][:via_id] = ViaType.WEB_WIDGET
          @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
        end
        it("has ViaType.WEB_WIDGET") { assert_equal ViaType.WEB_WIDGET, @ticket.audit.via_id }
      end

      describe "with a user_agent of HelpCenter" do
        before do
          @request_params.stubs(internal_client?: true)
          @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
        end

        it("has ViaType.WEB_FORM") { assert_equal ViaType.WEB_FORM, @ticket.audit.via_id }
      end

      describe "with a user_agent of mobile agent apps" do
        before do
          @request_params.stubs(:user_agent).returns("Zendesk for iPhone")
          @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
        end

        it("has ViaType.MOBILE") { assert_equal ViaType.MOBILE, @ticket.audit.via_id }
      end
    end

    describe "from an HTML request" do
      before do
        @request_params.stubs(:xml_or_json_format).returns(false)
        @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
      end
      it("has ViaType.WEB_FORM") { assert_equal ViaType.WEB_FORM, @ticket.audit.via_id }
    end

    describe "with a specified forum entry" do
      before do
        @request_params.stubs(:xml_or_json_format).returns(false)
        @params[:entry_id] = @account.entries.first.id
        @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
      end
      it("has ViaType.TOPIC") { assert_equal ViaType.TOPIC, @ticket.audit.via_id }
    end

    describe "in a follow-up" do
      before do
        @params[:ticket][:via_followup_source_id] = tickets(:minimum_5).nice_id
        @request_params.stubs(:xml_or_json_format).returns(false)
        @ticket = Zendesk::Tickets::Initializer.new(@account, @user, @params, request_params: @request_params).ticket
      end

      it("has ViaType.CLOSED_TICKET") { assert_equal ViaType.CLOSED_TICKET, @ticket.audit.via_id }

      it "sets the via_reference_id" do
        assert_equal tickets(:minimum_5).id, @ticket.audit.via_reference_id
      end
    end
  end

  describe "Follow-up ticket initialization" do
    describe "for agents" do
      before do
        @account = accounts(:minimum)
        @user = @account.owner
        @request_params = stub('request_params', user_agent: 'Internet Explorer 1.0', remote_ip: '127.0.0.1')

        ticket_params
      end

      it "sets the new requester" do
        assert_equal @ticket.requester.id, users(:minimum_search_user).id
      end

      it "sets the new CCs" do
        assert_equal 2, @ticket.collaborations.size
        assert_includes @ticket.collaborations.map(&:user_id), users(:minimum_author).id
      end

      it "has the new subject" do
        assert_equal @ticket.subject, "Updated subject for follow-up ticket"
      end

      it "has the new comment" do
        assert_equal @ticket.description, "Follow-up comment data."
      end
    end

    describe "for end-users" do
      before do
        @account = accounts(:minimum)
        @user = users(:minimum_end_user)
        @request_params = stub('request_params', user_agent: 'Internet Explorer 1.0', remote_ip: '127.0.0.1', has_x_on_behalf_of_header?: false)

        ticket_params
      end

      it "does not change requester" do
        assert_equal false, @ticket.requester.changed?
      end

      it "has the new subject" do
        assert_equal @ticket.subject, "Updated subject for follow-up ticket"
      end

      it "has the new comment" do
        assert_equal @ticket.description, "Follow-up comment data."
      end
    end
  end

  describe "sharing sample tickets" do
    let(:account) { accounts(:minimum) }
    let(:admin) { users(:minimum_admin) }
    let(:agreement) { FactoryBot.create(:agreement, account_id: account.id) }
    let(:sharing_agreement_ids) { [{ id: agreement.id }] }
    let(:sharing_agreements) { { sharing_agreements: sharing_agreement_ids } }
    let(:ticket) { Ticket.create_test_ticket!(account, admin) }

    before do
      stub_request(:any, %r{http://example.com/sharing/tickets})

      @ticket_initializer = Zendesk::Tickets::Initializer.new(
        account,
        admin,
        id: ticket.nice_id,
        ticket: {
          comment: {
            value: "Halps."
          }
        }.merge(sharing_agreements)
      )

      @updated_ticket = @ticket_initializer.ticket
    end

    it "shows an error" do
      refute @updated_ticket.valid?
      assert @updated_ticket.errors.full_messages.include?("Sample tickets can't be shared")
    end
  end

  describe "sharing agreements" do
    before do
      stub_request(:any, %r{http://example.com/sharing/tickets})

      @account = accounts(:minimum)
      @requester = users(:minimum_admin)

      ticket = tickets(:minimum_1)
      ticket.shared_tickets.create!(agreement_id: agreement.id)

      @ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @requester,         id: ticket.nice_id,
                                                                                            ticket: {
          comment: { value: "Halps." }
        }.merge(sharing_agreements))

      @ticket = @ticket_initializer.ticket
      @ticket.save!
      @ticket.reload
    end

    def agreement
      FactoryBot.create(:agreement, account_id: @account.id)
    end

    describe "with no sharing agreements passed" do
      let(:sharing_agreements) { {} }

      it "does not touch shared_tickets" do
        assert_equal 1, @ticket.shared_tickets.size
      end
    end

    describe "with a key" do
      let(:sharing_agreements) do
        { sharing_agreements: [
          { id: agreement.id, custom_fields: { abc: :def } },
          { custom_fields: { hi: :hello } },
          { id: agreement.id }
        ] }
      end

      it "only adds valid sharing agreements" do
        ids = sharing_agreements[:sharing_agreements].map { |a| a[:id] }.compact
        assert_equal ids, @ticket.shared_tickets.map(&:agreement_id)
      end
    end

    describe "with sharing_agreement_ids" do
      let(:sharing_agreements) { { sharing_agreement_ids: [agreement.id, agreement.id] } }

      it "adds sharing agreements" do
        assert_equal 2, @ticket.shared_tickets.size
        assert_equal sharing_agreements[:sharing_agreement_ids], @ticket.shared_tickets.map(&:agreement_id)
      end
    end

    describe "with no ids in sharing_agreement_ids " do
      let(:sharing_agreements) { { sharing_agreement_ids: nil } }

      it "removes sharing agreements" do
        assert_equal 0, @ticket.shared_tickets.size
      end
    end
  end

  describe "with safe_update" do
    before do
      @account = accounts(:minimum)
      @requester = users(:minimum_admin)
      @original_ticket = tickets(:minimum_1)
      @params = {
        id: @original_ticket.nice_id,
        ticket: {
          assignee_id: users(:minimum_agent).id,
          updated_stamp: @original_ticket.updated_at
        }
      }
    end

    it "sets safe_update_timestamp with safe_update" do
      @params[:ticket][:safe_update] = true
      ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @requester, @params)
      ticket_initializer.ticket.safe_update_timestamp.must_equal @original_ticket.updated_at
    end

    it "does not set safe_update_timestamp without safe_update" do
      ticket_initializer = Zendesk::Tickets::Initializer.new(@account, @requester, @params)
      ticket_initializer.ticket.safe_update_timestamp.must_be_nil
    end
  end

  describe "with additional_collaborators" do
    let(:account) { accounts(:minimum) }
    let(:ticket) do
      ticket = tickets(:minimum_1)
      ticket.set_collaborators = [users(:minimum_end_user).id]
      ticket.will_be_saved_by(users(:minimum_admin))
      ticket.save!
      ticket
    end
    let(:ccs_followers_enabled) { false }

    before do
      if ccs_followers_enabled
        CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
      else
        CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
      end

      @params = {
        id: ticket.nice_id,
        ticket: { additional_collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }, email: "mini@example.com"] }
      }
      @initialized_ticket = Zendesk::Tickets::Initializer.new(account, requester, @params).ticket
    end

    describe "for agents" do
      let(:requester) { users(:minimum_agent) }

      it "adds collaborators" do
        expected_result = "Author Minimum <minimum_author@aghassipour.com>, Kjeld <kjeld@example.org>, Mini <mini@example.com>, minimum_end_user <minimum_end_user@aghassipour.com>"

        @initialized_ticket.current_collaborators.must_equal expected_result
      end
    end

    describe "for end_users" do
      let(:requester) { users(:minimum_end_user) }

      it "adds collaborators" do
        @initialized_ticket.current_collaborators.must_equal "Author Minimum <minimum_author@aghassipour.com>, Kjeld <kjeld@example.org>, Mini <mini@example.com>, minimum_end_user <minimum_end_user@aghassipour.com>"
      end

      describe "when collaborators_addable_only_by_agents setting is true" do
        let(:account) do
          set_account_collaborators_addable_only_by_agents
        end

        describe "with CCs/Followers settings disabled" do
          it "does not add collaborators" do
            @initialized_ticket.current_collaborators.must_equal "minimum_end_user <minimum_end_user@aghassipour.com>"
          end
        end

        describe "with CCs/Followers settings enabled" do
          let(:ccs_followers_enabled) { true }

          it "adds collaborators" do
            @initialized_ticket.current_collaborators.must_equal "Author Minimum <minimum_author@aghassipour.com>, Kjeld <kjeld@example.org>, Mini <mini@example.com>, minimum_end_user <minimum_end_user@aghassipour.com>"
          end
        end
      end
    end
  end

  describe "closed zendesks" do
    before do
      @account = accounts(:minimum)
      @account.is_open = false
      @account.save!
      @ticket_params = {
        ticket: {
          requester_id: users(:minimum_end_user),
          collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
          comment: { value: "Halps." },
          metadata: { key: "value" },
          system_metadata: { client: "hello" },
          brand_id: 6
        }
      }
    end

    describe "for agents" do
      before do
        @user = users(:minimum_agent)
      end

      it "adds new collaborators" do
        assert_equal initialized_ticket.current_collaborators, "Author Minimum <minimum_author@aghassipour.com>, Kjeld <kjeld@example.org>"
      end
    end

    describe "for end-users" do
      before do
        @user = users(:minimum_end_user)
      end

      it "does not add new collaborators" do
        assert_equal initialized_ticket.current_collaborators, "Author Minimum <minimum_author@aghassipour.com>"
      end
    end

    def initialized_ticket
      Zendesk::Tickets::Initializer.new(@account, @user, @ticket_params).ticket
    end
  end

  describe "Voice API ticket" do
    describe "with a voice_comment" do
      before do
        @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
          voice_comment: {
            from: "+14045555404",
            to: "+18005555800",
            recording_url: "http://domain.com/Accounts/AC97090f4127e39f6ba124883058a01f41/Recordings/CA0accec2ffe51a84bb7f4344e03b8b97a.mp3",
            call_duration: 50,
            answered_by_id: 6,
            transcription_text: "a transcription",
            started_at: "2013-06-24 15:31:44 +0000",
            location: "location",
          }
        })
        @ticket = @ticket_initializer.ticket
      end

      it "does not add a comment" do
        assert_not_nil @ticket.comment
      end
    end
  end

  describe "request params" do
    let(:ticket_params) { {ticket: {}} }

    before do
      @initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_agent), ticket_params)
      @initializer.ticket
      @request_params = @initializer.instance_variable_get(:@request_params)
    end

    describe "without request_params" do
      it "should have request_params as an object" do
        assert @request_params.is_a? Zendesk::Tickets::HttpRequestParameters
      end
    end

    describe "with request params inside ticket" do
      let(:ticket_params) { {ticket: {request: {user_agent: "xxx"}}} }

      it "should have request_params as an object" do
        assert @request_params.is_a? Zendesk::Tickets::HttpRequestParameters
        assert_equal "xxx", @request_params.user_agent
      end
    end
  end

  describe "submitter_id parameter is user from another account" do
    let(:account) { accounts(:minimum) }
    let(:other_account) { accounts(:extra_large) }
    let(:non_account_user) { users(:minimum_end_user) }
    let(:admin) { users(:minimum_admin) }

    before do
      non_account_user.update_column(:account_id, other_account.id)
      @created_at = 5.minutes.ago
      @ticket_initializer = Zendesk::Tickets::Initializer.new(
        account,
        admin,
        ticket: {
          subject: 'abc',
          description: 'def',
          submitter_id: non_account_user.id,
          created_at: @created_at
        }
      )
      @ticket = @ticket_initializer.ticket
    end

    it "sets submitter_id to default submitter" do
      assert_equal @ticket.submitter_id, admin.id
    end
  end

  describe "contextual_workspace parameter" do
    let(:account) { accounts(:minimum) }

    it "creates a contextual workspace audit event and attributes the audit to the user and not the system" do
      Account.any_instance.stubs(:has_contextual_workspaces?).returns(true)
      Account.any_instance.stubs(:has_contextual_workspaces_in_js?).returns(true)
      tde_workspace_params = { id: 1, title: 'refunds_workspace' }
      Ticket.any_instance.expects(:add_contextual_workspace_audit_event).with(tde_workspace_params).once

      ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
          assignee_id: users(:minimum_agent).id,
          collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
          comment: "",
          metadata: { key: "value" },
          tde_workspace: tde_workspace_params
        })

      ticket = ticket_initializer.ticket

      refute_equal User.system, ticket.audit.author_id
    end
  end

  private

  def update_ticket_requester(requester_data = {})
    ticket = tickets(:minimum_2)
    params = { id: ticket.nice_id, ticket: { requester_id: @author.id} }
    params[:ticket].merge!(requester_data) if requester_data.present?
    initializer = Zendesk::Tickets::Initializer.new(@account, @user, params)

    @ticket = initializer.ticket
    @ticket.save!
    @ticket.reload
  end

  def ticket_params
    @params = {ticket: {}}

    @params[:ticket] = {
      via_followup_source_id: tickets(:minimum_5).nice_id,
      requester_data: users(:minimum_search_user).email,
      set_collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
      subject: "Updated subject for follow-up ticket"
    }
    @params[:comment] = { value: "Follow-up comment data.", is_public: false }

    @request_params.stubs(:xml_or_json_format).returns(false)
    @request_params.stubs(:user_agent).returns("A test case!")
    @ticket = Zendesk::Tickets::Initializer.new(accounts(:minimum), @user, @params, request_params: @request_params).ticket
  end

  def setup_custom_fields(account)
    tm = Zendesk::Tickets::TicketFieldManager.new(account)
    @writable_custom_field = tm.build(title: "test-writable", title_in_portal: "test-writable", is_visible_in_portal: true, is_editable_in_portal: true)
    @writable_custom_field.save!
    @read_only_custom_field = tm.build(title: "test-writable", title_in_portal: "test-writable", is_visible_in_portal: true, is_editable_in_portal: false)
    @read_only_custom_field.save!
  end

  def create_ticket_with_custom_fields(account, user)
    ticket_initializer = Zendesk::Tickets::Initializer.new(account, user, ticket: {
      assignee_id: users(:minimum_agent).id,
      collaborators: [users(:minimum_author).id, { name: "Kjeld", email: "kjeld@example.org" }],
      comment: "this is a test",
      custom_fields: [{id: @writable_custom_field.id, value: "zen"}, {id: @read_only_custom_field.id, value: "desk"}],
      metadata: { key: "value" }
    })
    ticket_initializer.ticket
  end

  def set_account_collaborators_addable_only_by_agents
    account = accounts(:minimum)
    account.settings.collaborators_addable_only_by_agents = true
    account
  end
end
