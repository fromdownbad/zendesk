require_relative "../../../support/test_helper"

SingleCov.covered!

class TestMetricSetPagination
end

PaginationOption = Struct.new(
  :current_page,
  :next_page,
  :previous_page,
  :per_page,
  :total_entries
)

describe Zendesk::Tickets::MetricSetsPagination do
  let(:metric_sets)        { TestMetricSetPagination.new }
  let(:pagination_options) { PaginationOption.new(1, 2, 3, 4, 5) }

  before do
    metric_sets.extend(Zendesk::Tickets::MetricSetsPagination)
  end

  describe 'add_pagination_options' do
    before do
      metric_sets.add_pagination_options(pagination_options)
    end

    it 'sets current_page' do
      assert_equal(1, metric_sets.current_page)
    end

    it 'sets next_page' do
      assert_equal(2, metric_sets.next_page)
    end

    it 'sets previous_page' do
      assert_equal(3, metric_sets.previous_page)
    end

    it 'sets per_page' do
      assert_equal(4, metric_sets.per_page)
    end

    it 'sets total_entries' do
      assert_equal(5, metric_sets.total_entries)
    end
  end
end
