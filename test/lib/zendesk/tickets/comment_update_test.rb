require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Tickets::CommentUpdate do
  class BulkUpdateDummyClass
    include Zendesk::Tickets::CommentUpdate
  end

  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_5) }

  describe ".can_bulk_update_comments" do
    before do
      @dummy = BulkUpdateDummyClass.new
      ticket.comment = Comment.new(body: "blah", author: account.owner, is_public: false)
    end

    describe 'with light agent' do
      before do
        @light_agent = User.create! do |user|
          user.account = account
          user.name = "Prince Light"
          user.email = "prince@example.org"
          user.roles = Role::AGENT.id
        end

        Account.any_instance.stubs(:has_light_agents?).returns(true)
        BulkUpdateDummyClass.any_instance.stubs(:current_user).returns(@light_agent)
        User.any_instance.stubs(:can?).with(:only_create_ticket_comments, ticket).returns(true)
      end

      it "returns true for light_agents bulk updating just private comments" do
        assert(@dummy.can_bulk_update_comments(ticket))
      end

      it "returns false for light_agents editing the ticket" do
        ticket.subject = "Boo"
        refute @dummy.can_bulk_update_comments(ticket)
      end

      it "returns false for light_agents bulk updating without comments" do
        ticket.comment = nil
        refute @dummy.can_bulk_update_comments(ticket)
      end
    end
  end
end
