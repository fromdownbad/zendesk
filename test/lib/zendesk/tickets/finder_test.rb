require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Tickets::Finder do
  fixtures :accounts, :users, :organizations

  let(:account) { accounts(:minimum) }
  let(:external_id) { 123 }
  let(:organization) { organizations(:minimum_organization1) }
  let(:admin) { users(:minimum_admin) }
  let(:agent) { users(:minimum_agent) }
  let(:current_user) { admin }
  let(:params) { {} }
  let(:finder) { Zendesk::Tickets::Finder.new(account, current_user, params) }

  describe "#tickets" do
    it "returns the correct scope" do
      assert_equal account.tickets.to_sql, finder.tickets.to_sql
    end

    describe "with organization_id" do
      let(:params) { { organization_id: organization.id } }

      it "returns the correct scope" do
        assert_equal organization.tickets.to_sql, finder.tickets.to_sql
      end
    end

    describe "with external_id" do
      let(:params) { { external_id: external_id } }

      it "returns the correct scope" do
        scope = account.tickets.where(external_id: external_id)

        assert_equal scope.to_sql, finder.tickets.to_sql
      end
    end

    describe "for agent" do
      let(:current_user) { agent }

      it "returns the correct scope" do
        assert_equal account.tickets.for_user(agent).to_sql, finder.tickets.to_sql
      end
    end

    describe "for system user" do
      let(:current_user) { users(:systemuser) }

      it "returns the correct scope" do
        assert_equal account.tickets.to_sql, finder.tickets.to_sql
      end
    end
  end
end
