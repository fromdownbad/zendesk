require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Tickets::ArchiveFinders do
  describe 'find' do
    describe 'with an invalidly long id' do
      it 'raises RecordNotFound' do
        proc { Ticket.find(10**20) }.must_raise(ActiveRecord::RecordNotFound)
      end
    end
  end
end
