require_relative "../../../support/test_helper"
require_relative "../../../support/api_test_helper"
require 'zendesk/tickets/ticket_field_manager'

SingleCov.covered!

describe Zendesk::Tickets::TicketFieldManager do
  include ApiTestHelper

  fixtures :all

  before do
    @account = accounts(:minimum)
    @manager = Zendesk::Tickets::TicketFieldManager.new(@account)
  end

  describe "#update" do
    subject { { custom_field_options: [{ name: "Test", value: "  test  " }] } }
    let(:expected_field_options) { [{ name: "Test", value: "test" }] }

    before do
      @ticket_field = ticket_fields(:field_tagger_custom)
    end

    describe "with existing custom field options" do
      before do
        @ticket_field.custom_field_options.create(name: "testing", value: "test")
        @manager.update(@ticket_field, subject).save!
        @custom_field_options = @ticket_field.custom_field_options.map { |cfo| { name: cfo.name, value: cfo.value } }
      end

      it "replaces custom field options" do
        assert_equal expected_field_options, @custom_field_options
      end

      describe "with non-ascii characters" do
        # If case is not correctly handled here, this cause a  KeyConstraintViolation because
        # we will try to insert a new record without removing the old record instead of updating it.
        before do
          @new_option = @ticket_field.custom_field_options.create!(name: "non ascii", value: "Верификация_селфи")
          @new_option.update_column(:value, "Верификация_селфи") # this will get downcased in the create call above, reset caps

          @manager.update(
            @ticket_field,
            custom_field_options: [{ name: "non ascii", value: "верификация_селфи" }]
          ).save!
        end

        it "updates the existing option" do
          assert_equal @new_option.id, @ticket_field.reload.custom_field_options.last.id
        end

        it "saves the new value" do
          assert_equal "верификация_селфи", @new_option.reload.value
        end
      end
    end

    describe "without existing custom field options" do
      before do
        @ticket_field.custom_field_options.clear
        @manager.update(@ticket_field, subject).save!
        @custom_field_options = @ticket_field.custom_field_options.map { |cfo| { name: cfo.name, value: cfo.value } }
      end

      it "adds custom field option" do
        assert_equal expected_field_options, @custom_field_options
      end
    end

    describe "re-adding a deleted field" do
      before do
        @custom_field_options = @ticket_field.custom_field_options.map { |cfo| { name: cfo.name, value: cfo.value } }
        @ticket_field.custom_field_options.first.soft_delete!
        @custom_field_options.rotate! # move deleted choice to end of list
      end

      it "undeletes and adds back in position specified in custom_field_options list" do
        @manager.update(@ticket_field, custom_field_options: @custom_field_options).save!
        field_after_update = @ticket_field.custom_field_options.where(value: 'booring').first
        assert_equal @custom_field_options.count - 1, field_after_update.position
      end

      describe "with non-ascii characters" do
        describe_with_and_without_arturo_enabled :special_chars_in_custom_field_options do
          # If case is not correctly handled here, this cause a  KeyConstraintViolation because
          # we will try to insert a new record without removing the old record instead of updating it.
          before do
            @new_option = @ticket_field.custom_field_options.create!(name: "non ascii", value: "Верификация_селфи")
            @new_option.soft_delete!

            # value will get downcased in the create call above, reset caps
            # this also clears enhanced_value to mimic the case where an account was added to special_chars_in_custom_field_options
            # and had a deleted field that they are now trying to re-add
            @new_option.update_columns(value: "Верификация_селфи", enhanced_value: nil)

            @manager.update(
              @ticket_field,
              custom_field_options: [{ name: "non ascii", value: "верификация_селфи" }]
            ).save!
          end

          it "updates the existing option" do
            assert_equal @new_option.id, @ticket_field.reload.custom_field_options.last.id
          end

          it "saves the new value" do
            assert_equal "верификация_селфи", @new_option.reload.value
          end
        end
      end
    end

    describe "without custom field option value" do
      subject { { custom_field_options: [{ name: "testing" }] } }

      it "returns errors" do
        expected_response = {
          error: "RecordInvalid",
          description: "Record validation errors",
          details: {
            custom_field_options: [
              { error: "BlankValue", description: "Custom field options must have a tag" }
            ]
          }
        }

        assert_api_raise(ActiveRecord::RecordInvalid, expected_response) do
          @manager.update(@ticket_field, subject).save!
        end
      end
    end

    describe "without custom field option value" do
      subject { { custom_field_options: [{ name: "testing" }] } }

      it "returns errors" do
        expected_response = {
          error: "RecordInvalid",
          description: "Record validation errors",
          details: {
            custom_field_options: [
              { error: "BlankValue", description: "Custom field options must have a tag" }
            ]
          }
        }

        assert_api_raise(ActiveRecord::RecordInvalid, expected_response) do
          @manager.update(@ticket_field, subject).save!
        end
      end
    end

    describe "without custom field option name" do
      subject { { custom_field_options: [{ value: "test" }] } }

      it "returns errors" do
        expected_response = {
          error: "RecordInvalid",
          description: "Record validation errors",
          details: {
            field_options: [
              { error: "BlankValue", description: "Field options: must have a title" }
            ]
          }
        }
        assert_api_raise(ActiveRecord::RecordInvalid, expected_response) do
          @manager.update(@ticket_field, subject).save!
        end
      end
    end

    describe "with duplicate custom field option values" do
      subject { { custom_field_options: [{ name: "Test", value: "test" }, { name: "Test2", value: "test" }] } }

      before do
        @original = @ticket_field.custom_field_options.dup
      end

      it "keeps existing custom field options and return errors" do
        expected_response = {
          error: "RecordInvalid",
          description: "Record validation errors",
          details: {
            field_options: [
              { description: "Field options: must each have a unique tag. The tag <strong>test</strong> is currently used more than once." }
            ]
          }
        }
        assert_api_raise(ActiveRecord::RecordInvalid, expected_response) do
          @manager.update(@ticket_field, subject).save!
        end
        assert_equal @original, @ticket_field.custom_field_options.reload
      end
    end

    describe "with an existing unchanged custom field option" do
      subject { { custom_field_options: [{ id: @option.id.to_s, name: @option.name, value: @option.value}] } }

      before do
        @initial_options = @ticket_field.custom_field_options.map { |option| { name: option.name, value: option.value } }
        @initial_updated_at = @ticket_field.updated_at
        @manager.update(@ticket_field, @initial_options).save!
      end

      it "does not change attributes of given option" do
        @initial_options.each_with_index do |initial_option, index|
          option = @ticket_field.custom_field_options[index]
          assert_equal initial_option[:name], option.name
          assert_equal initial_option[:value], option.value
        end
      end

      it "does not changed updated_at" do
        assert_equal @initial_updated_at, @ticket_field.updated_at
      end
    end

    describe "with custom field options with mixed case value" do
      subject { { custom_field_options: [{ name: "new name", value: "MixedCaseValue"}] } }

      before do
        @ticket_field.custom_field_options.clear
        @ticket_field.custom_field_options.create(name: "testing", value: "MixedCaseValue")
        @option = @ticket_field.custom_field_options.first
      end

      it "downcases value" do
        assert_equal "mixedcasevalue", @option.value
      end

      it "replaces custom field options" do
        old_id = @option.id
        @manager.update(@ticket_field, subject).save!
        @option.reload
        assert_equal "new name", @option.name
        assert_equal old_id, @option.id
      end
    end

    describe "when updating with id" do
      describe "with duplicate custom field option ids" do
        subject { { custom_field_options: [{ id: @option.id.to_s, name: "Test1", value: "test1" }, { id: @option.id.to_s, name: "Test2", value: "test2" }] } }
        let(:expected_field_options) { [{ name: "Test1", value: "test1" }, { name: "Test2", value: "test2" }] }

        before do
          @option = @ticket_field.custom_field_options.first
        end

        it "ignores the id's and updates by value" do
          @manager.update(@ticket_field, subject).save!
          assert_equal expected_field_options, @ticket_field.custom_field_options.reload.map { |cfo| { name: cfo.name, value: cfo.value } }
        end
      end

      describe "with unknown custom field option id" do
        subject { { custom_field_options: [{ id: "-1", name: "testing", value: "test" }] } }
        let(:expected_field_options) { [{ name: "testing", value: "test" }] }

        it "ignores the id and updates by value" do
          @manager.update(@ticket_field, subject).save!
          assert_equal expected_field_options, @ticket_field.custom_field_options.reload.map { |cfo| { name: cfo.name, value: cfo.value } }
        end
      end
    end

    describe "with an existing custom field option and an id" do
      subject { { custom_field_options: [{ id: @option.id.to_s, name: "testing", value: "test"}] } }

      before do
        @original_updated_at = @ticket_field.updated_at
        @option = @ticket_field.custom_field_options.first
        @manager.update(@ticket_field, subject).save!
        @resulting_option = @ticket_field.custom_field_options.reload.first
      end

      it "replaced the original option with a new one" do
        assert_equal "testing", @resulting_option .name
        assert_equal "test", @resulting_option.value
      end

      it "updates updated_at" do
        assert_not_equal @original_updated_at, @ticket_field.updated_at
      end
    end
  end

  describe "map" do
    it "handles invalid options" do
      [nil, [], ""].each do |type|
        assert_instance_of Hash, @manager.map(type)
      end
    end

    it "maps shorthand type to class name" do
      result = @manager.map(type: "tagger")
      assert_equal "FieldTagger", result[:type]
    end

    it "does not map shorthand type that doesn't exist" do
      result = @manager.map(type: "raget")
      assert_equal "raget", result[:type]
    end
  end

  it "builds a new ticket field with the given params" do
    ticket_field = @manager.build(title: "Cowboy")
    assert_equal "Cowboy", ticket_field.title
  end

  it "uses the :type param to determine the ticket field type" do
    ticket_field = @manager.build(type: "FieldDate")
    assert ticket_field.is_a?(FieldDate)
  end

  it "supports short hand field type references" do
    ticket_field = @manager.build(type: "date")
    assert ticket_field.is_a?(FieldDate)
  end

  it "accepts both 'partialcreditcard' and 'partial_credit_card' for shorthand type" do
    ticket_field_1 = @manager.build(type: "partialcreditcard")
    assert ticket_field_1.is_a?(FieldPartialCreditCard)

    ticket_field_2 = @manager.build(type: "partial_credit_card")
    assert ticket_field_2.is_a?(FieldPartialCreditCard)
  end

  it "uses FieldText if no :type param is given" do
    ticket_field = @manager.build
    assert ticket_field.is_a?(FieldText)
  end

  it "sets the account on the ticket field" do
    ticket_field = @manager.build
    assert_equal @account, ticket_field.account
  end

  it "converts required to is_required" do
    ticket_field = @manager.build(active: true)
    assert ticket_field.is_active?
    ticket_field = @manager.build(active: false)
    refute ticket_field.is_active?
  end

  it "raises an UnknownCustomFieldTypeError if the :type param is invalid" do
    assert_raise(Zendesk::CustomField::UnknownCustomFieldTypeError) { @manager.build(type: "Wookie") }
  end
end
