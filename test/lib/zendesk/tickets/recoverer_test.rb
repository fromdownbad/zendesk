require_relative '../../../support/test_helper'
require 'zendesk/tickets/recoverer'

SingleCov.covered!

describe Zendesk::Tickets::Recoverer do
  fixtures :accounts,
    :subscriptions,
    :account_property_sets,
    :users,
    :suspended_tickets,
    :ticket_fields,
    :sequences,
    :user_identities,
    :tickets

  let(:recoverer) { Zendesk::Tickets::Recoverer.new(account: account, user: user) }
  let(:suspended_ticket) { suspended_tickets(:minimum_unknown_author) }
  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:statsd_client) { Zendesk::StatsD::Client.any_instance }
  let(:logger) { Rails.logger }

  before do
    statsd_client.stubs(:increment)
    logger.stubs(:info)
  end

  # AKA "recover_many"
  describe "#recover" do
    describe "when no options are provided" do
      let(:recover_as) { :authors }

      describe "on success" do
        describe_with_and_without_arturo_enabled(:email_metrics_on_suspended_tickets_recovery) do
          it "recovers a suspended ticket" do
            ticket = recoverer.recover([suspended_ticket])
            refute_nil ticket
          end
        end

        describe_with_arturo_enabled(:email_metrics_on_suspended_tickets_recovery) do
          it "logs the success" do
            logger.expects(:info).with("Recovery finished on 1 SuspendedTickets for '#{account&.subdomain}' account, recovered as #{recover_as}")
            statsd_client.expects(:increment).with("recovery_finished", tags: ["caller:recover_many", "recover_as:#{recover_as}"])

            recoverer.recover([suspended_ticket])
          end
        end

        describe "creates related models" do
          before { recoverer.recover([suspended_ticket]) }

          should_create :user
          should_create :ticket
        end

        it "destroys the suspended ticket" do
          recoverer.recover([suspended_ticket])
          assert_nil SuspendedTicket.find_by_id(suspended_ticket.id)
        end
      end

      describe "on failure" do
        before { SuspendedTicket.any_instance.stubs(recoverable?: false) }

        describe_with_and_without_arturo_enabled(:email_metrics_on_suspended_tickets_recovery) do
          it "doesn't destroy the suspended ticket" do
            assert_raise Zendesk::Tickets::Recoverer::RecoveryFailed do
              recoverer.recover([suspended_ticket])
            end.message.must_include "Failed to import"

            refute_nil SuspendedTicket.find_by_id(suspended_ticket.id)
          end
        end

        describe_with_arturo_enabled(:email_metrics_on_suspended_tickets_recovery) do
          it "profiles the failure" do
            logger.expects(:info).with("Recovery failed on 1/1 SuspendedTickets for '#{account&.subdomain}' account, recovered as #{recover_as}")
            statsd_client.expects(:increment).with("recovery_failed", tags: ["caller:recover_many", "recover_as:#{recover_as}"])

            assert_raise Zendesk::Tickets::Recoverer::RecoveryFailed do
              recoverer.recover([suspended_ticket])
            end
          end
        end

        it "doesn't create a ticket" do
          refute_difference('Ticket.count(:all)') do
            assert_raise Zendesk::Tickets::Recoverer::RecoveryFailed do
              recoverer.recover([suspended_ticket])
            end
          end
        end
      end
    end

    describe "when flags are present on the suspended ticket" do
      before do
        suspended_ticket.add_flags(AuditFlags.new([EventFlagType.OTHER_USER_UPDATE]), nil)
        suspended_ticket.save!

        @ticket = recoverer.recover([suspended_ticket])
      end

      it "preserves the flags" do
        assert_equal @ticket.first.audits.last.flags, AuditFlags.new([EventFlagType.OTHER_USER_UPDATE])
      end
    end

    describe "when options[:recover_as_user] is not provided" do
      it "recovers the suspended ticket" do
        assert_difference('account.tickets.count(:all)', 1) do
          recoverer.recover([suspended_ticket])
        end
      end

      describe "given multiple suspended tickets" do
        before do
          @selected = ["foo@bar.com", "Foo@bar.com"].map do |email|
            account.suspended_tickets.create!(
              subject: "test #{email}",
              recipient: "support@dev.localhost",
              from_name: "Flew Blar",
              from_mail: email,
              cause: SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE,
              content: "I am an apple #{email}"
            )
          end
        end

        it "only the selected tickets should be recovered (A)" do
          assert_difference('Ticket.count(:all)', 1) do
            recoverer.recover([@selected.first])
          end

          assert_equal 1, account.find_user_by_email('foo@bar.com').tickets.count(:all)
        end

        it "only the selected tickets should be recovered (B)" do
          assert_difference('Ticket.count(:all)', 2) do
            recoverer.recover(@selected)
          end

          assert_equal 2, account.find_user_by_email('foo@bar.com').tickets.count(:all)
        end
      end
    end

    describe "when options[:recover_as_user] is provided" do
      describe_with_and_without_arturo_enabled(:email_metrics_on_suspended_tickets_recovery) do
        it "recovers the suspended ticket" do
          assert_difference('user.tickets.count(:all)', 1) do
            recoverer.recover([suspended_ticket], recover_as_user: user)
          end
        end
      end

      describe "other suspended tickets" do
        before do
          @suspended_ticket_two = account.suspended_tickets.create!(
            subject: "test",
            recipient: "support@dev.localhost",
            from_name: "me",
            from_mail: "someone@else.com",
            cause: SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE,
            content: "I am an apple"
          )
        end

        it "recovers both suspended tickets" do
          assert_difference('user.tickets.count(:all)', 2) do
            recoverer.recover([suspended_ticket, @suspended_ticket_two], recover_as_user: user)
          end
        end
      end
    end
  end

  # AKA "recover_single"
  describe "#recover_ticket" do
    let(:recover_as) { :user }

    describe "on success" do
      describe_with_and_without_arturo_enabled(:email_metrics_on_suspended_tickets_recovery) do
        it "recovers a suspended ticket" do
          ticket = recoverer.recover_ticket(suspended_ticket, user)
          assert !ticket.nil?
          assert ticket.is_a?(Ticket)
        end
      end

      describe_with_arturo_enabled(:email_metrics_on_suspended_tickets_recovery) do
        it "logs the success" do
          logger.expects(:info).with("Recovery finished on #{suspended_ticket.id} SuspendedTicket for '#{account&.subdomain}' account, recovered as #{recover_as}")
          statsd_client.expects(:increment).with("recovery_finished", tags: ["caller:recover_single", "recover_as:#{recover_as}"])

          recoverer.recover_ticket(suspended_ticket, user)
        end
      end
    end

    describe "on failure" do
      before { SuspendedTicket.any_instance.stubs(recoverable?: false) }

      describe_with_and_without_arturo_enabled(:email_metrics_on_suspended_tickets_recovery) do
        it "raises an exception when the suspended ticket isn't recoverable" do
          assert_raise Zendesk::Tickets::Recoverer::RecoveryFailed do
            recoverer.recover_ticket(suspended_ticket, user)
          end.message.must_include "Failed to import"
        end
      end

      describe_with_arturo_enabled(:email_metrics_on_suspended_tickets_recovery) do
        it "profiles the failure" do
          logger.expects(:info).with("Recovery failed on #{suspended_ticket.id} SuspendedTicket for '#{account&.subdomain}' account, recovered as #{recover_as}")
          statsd_client.expects(:increment).with("recovery_failed", tags: ["caller:recover_single", "recover_as:#{recover_as}"])

          assert_raise Zendesk::Tickets::Recoverer::RecoveryFailed do
            recoverer.recover_ticket(suspended_ticket, user)
          end
        end
      end
    end
  end
end
