require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'Ticket::ProblemIncident' do
  describe "ProblemIncident" do
    fixtures :accounts, :subscriptions, :tickets, :users, :groups, :sequences, :user_identities, :memberships, :account_property_sets, :attachments

    before do
      Account.any_instance.stubs(:has_extended_ticket_types?).returns(true)
      @ticket = Ticket.new
    end

    describe "#incident_must_link_to_problem" do
      before do
        @admin = users(:minimum_admin)
        @end_user = users(:minimum_end_user)

        @linked_ticket = tickets(:minimum_2)
        @linked_ticket.will_be_saved_by(@admin)

        @incident_ticket = tickets(:minimum_1)
        @incident_ticket.requester = @end_user
        @incident_ticket.ticket_type_id = TicketType.INCIDENT
        @incident_ticket.problem_id = @linked_ticket.nice_id
        @incident_ticket.will_be_saved_by(@admin)
      end

      describe "when being updated by an agent" do
        it "links properly to a problem ticket" do
          @linked_ticket.ticket_type_id = TicketType.PROBLEM
          @linked_ticket.save!

          assert @incident_ticket.save
        end

        it "displays an error when linking to non-problem tickets" do
          @linked_ticket.ticket_type_id = TicketType.QUESTION
          @linked_ticket.save!

          refute @incident_ticket.save
          assert_includes @incident_ticket.errors.full_messages, I18n.t("activerecord.errors.models.ticket.attributes.problem_id.invalid")
        end

        it "does not display an error when updating via email" do
          Ticket.any_instance.stubs(:current_via_id).returns(ViaType.MAIL)

          @linked_ticket.ticket_type_id = TicketType.QUESTION
          @linked_ticket.save!

          assert @incident_ticket.save
        end

        it "does not display an error when updating via rule" do
          Ticket.any_instance.stubs(:current_via_id).returns(ViaType.RULE)

          @linked_ticket.ticket_type_id = TicketType.QUESTION
          @linked_ticket.save!

          assert @incident_ticket.save
        end
      end

      describe "when being updated by an end-user" do
        it "does not display an error when linking to non-problem tickets" do
          @linked_ticket.ticket_type_id = TicketType.QUESTION
          @linked_ticket.save!
          @incident_ticket.save(validate: false)

          @incident_ticket.add_comment(body: 'halp')
          @incident_ticket.will_be_saved_by(@end_user)
          assert @incident_ticket.save!
        end
      end
    end

    describe "#will_process_incidents_in_background?" do
      describe "when the system user is processing a ticket" do
        before do
          @ticket.expects(:current_user).returns(User.system)
          @ticket.expects(:incidents).never
        end

        it "returns false" do
          assert_equal false, @ticket.will_process_incidents_in_background?
        end
      end

      describe "when a non-system user is processing a ticket" do
        before do
          @ticket.expects(:current_user).returns(User.new)
        end

        it "returns true of the ticket has more than Ticket::INLINE_INCIDENT_PROCESSING_LIMIT incidents" do
          @ticket.expects(:incidents).returns(stub(working: stub(count: Ticket::INLINE_INCIDENT_PROCESSING_LIMIT + 1)))
          assert(@ticket.will_process_incidents_in_background?)
        end

        it "returns false of the ticket has less than ProblemIncident::INLINE_INCIDENT_PROCESSING_LIMIT incidents" do
          @ticket.expects(:incidents).returns(stub(working: stub(count: Ticket::INLINE_INCIDENT_PROCESSING_LIMIT - 1)))
          assert_equal false, @ticket.will_process_incidents_in_background?
        end
      end
    end

    describe "#will_solve_linked_tickets?" do
      before do
        @ticket.current_user   = User.new { |u| u.roles = Role::AGENT.id }
        @ticket.ticket_type_id = TicketType.PROBLEM
      end
      describe "with solved status" do
        before do
          @ticket.status_id = StatusType.SOLVED
          @ticket.stubs(:previous_changes).returns(status_id: [0, 3])
        end

        it "solves linked tickets" do
          assert @ticket.will_solve_linked_tickets?
        end

        it "returns false if current_user is not an agent" do
          @ticket.current_user = User.new
          assert_equal false, @ticket.will_solve_linked_tickets?
        end

        it "returns false if status is not solved" do
          @ticket.status_id = StatusType.OPEN
          assert_equal false, @ticket.will_solve_linked_tickets?
        end

        it "returns false if type is not a problem" do
          @ticket.ticket_type_id = TicketType.INCIDENT
          assert_equal false, @ticket.will_solve_linked_tickets?
        end

        it "returns false if it was solved before" do
          @ticket.stubs(:previous_changes).returns({})
          assert_equal false, @ticket.will_solve_linked_tickets?
        end
      end

      describe "with closed status" do
        before do
          @ticket.status_id = StatusType.CLOSED
        end

        it "does not solve linked tickets" do
          refute @ticket.will_solve_linked_tickets?
        end
      end
    end

    describe "#solve_or_enqueue_linked_incidents" do
      describe "when #will_solve_linked_tickets? returns false" do
        before do
          @ticket.expects(:will_solve_linked_tickets?).returns(false)
        end

        it "shoulds not do anything " do
          @ticket.expects(:comment).never
          @ticket.solve_or_enqueue_linked_incidents
        end
      end

      describe "when #will_solve_linked_tickets? returns true" do
        before do
          @ticket.expects(:will_solve_linked_tickets?).returns(true)
          @ticket.current_user    = User.new { |u| u.roles = Role::AGENT.id }
          @ticket.current_user.id = 123
          @ticket.id              = 456
          @ticket.account_id      = 789
        end

        describe "and will_process_incidents_in_background? returns true" do
          before do
            @ticket.expects(:will_process_incidents_in_background?).returns(true)
          end

          it "enqueues the work" do
            SolveIncidentsJob.expects(:enqueue).with(@ticket.account_id, @ticket.current_user.id, problem: @ticket.id)
            @ticket.save_intermediate_comment_value
            @ticket.solve_or_enqueue_linked_incidents
          end

          it "enqueues the work with comment data when available" do
            @ticket.comment = Comment.new(body: "Mjallo", is_public: false)
            SolveIncidentsJob.expects(:enqueue).with(@ticket.account_id, @ticket.current_user.id, problem: @ticket.id, value: "Mjallo", is_public: false, format: nil)
            @ticket.save_intermediate_comment_value
            @ticket.solve_or_enqueue_linked_incidents
          end

          describe "when the comment has an image and a thumb of the image as attachments" do
            before do
              @ticket.comment = Comment.new(body: "Mjallo", is_public: false)
              Comment.any_instance.stubs(attachments: [attachments(:attachment_post), attachments(:attachment_post_thumb)])
            end

            it "enqueues the work with the non thumbnailed attachment" do
              SolveIncidentsJob.expects(:enqueue).with(@ticket.account_id, @ticket.current_user.id, problem: @ticket.id, value: "Mjallo", is_public: false, attachment_ids: [attachments(:attachment_post).id], format: nil)
              @ticket.save_intermediate_comment_value
              @ticket.solve_or_enqueue_linked_incidents
            end
          end
        end

        describe "and will_process_incidents_in_background? returns false" do
          before do
            @ticket.expects(:will_process_incidents_in_background?).returns(false)
          end

          it "executes the work right away" do
            SolveIncidentsJob.expects(:work).with(@ticket.account_id, @ticket.current_user.id, problem: @ticket)
            @ticket.save_intermediate_comment_value
            @ticket.solve_or_enqueue_linked_incidents
          end
        end
      end
    end

    describe "when a save rolls back" do
      before do
        @account = accounts(:minimum)
        @current_user = users(:minimum_admin)

        @problem = @account.tickets.new(
          requester: @current_user,
          description: "Some hard problem",
          ticket_type_id: TicketType.PROBLEM
        )

        @problem.will_be_saved_by(@current_user)
        @problem.save!
      end

      it "ensures no solve incidents job is queued" do
        SolveIncidentsJob.expects(:enqueue).never

        with_rollback(@problem) { @problem.save! }
      end
    end

    describe "scenario" do
      before do
        @account    = accounts(:with_groups)
        @agent1     = users(:with_groups_agent1)
        @agent2     = users(:with_groups_agent2)
        @group1     = groups(:with_groups_group1)
        @group2     = groups(:with_groups_group2)
        @requester1 = users(:with_groups_end_user)
        @requester2 = @account.users.create!(name: "Other Requester", email: "other@example.org")
      end

      it "solves link incidents and support dynamic liquid template evaluation" do
        # Submit problem
        problem = @account.tickets.new(
          requester: @requester1,
          description: "Some hard problem",
          ticket_type_id: TicketType.PROBLEM
        )

        problem.will_be_saved_by(@agent1)
        problem.save!

        assert_equal @agent1.id, problem.submitter_id
        assert_equal @requester1.id, problem.requester_id
        assert_equal ViaType.WEB_SERVICE, problem.via_id

        problem.linked_id = problem.id

        problem.will_be_saved_by(@agent1)
        problem.save!
        assert problem.linked_id.blank?

        # Submit incident 1
        incident1 = Ticket.new(
          requester: @requester1, description: "Incident 1", linked_id: problem.id,
          via_id: ViaType.WEB_FORM, ticket_type_id: TicketType.INCIDENT, account: @account
        )

        incident1.will_be_saved_by(@agent1)
        incident1.save!

        assert_equal incident1.problem, problem
        assert incident1.group.blank?
        assert incident1.assignee.blank?

        # Submit incident 2
        incident2 = Ticket.new(
          requester: @requester2, description: "Incident 2", linked_id: problem.id,
          via_id: ViaType.WEB_FORM, ticket_type_id: TicketType.INCIDENT, group: @group2,
          account: @account
        )

        incident2.will_be_saved_by(@agent2)
        incident2.save!

        assert_equal incident2.problem, problem
        assert_equal incident2.group, @group2
        assert incident2.assignee.blank?

        # 2 incidents have now been attached to the problem
        assert_equal 2, problem.reload.incidents.length

        # Solve problem
        problem.status_id = StatusType.SOLVED
        problem.assignee = @agent1
        problem.group    = @group1

        problem.add_comment(body: "Dear {{ticket.requester.name}} your ticket {{ticket.id}} is now solved", is_public: true)
        problem.will_be_saved_by(@agent1)
        problem.save!

        # Check incidents. Since agent1 is only in group1, we expect the incidents to be in group1 now
        assert_equal [true, true], problem.linked_incidents_summary.collect { |s| s[:success] }

        [incident1, incident2].each do |incident|
          incident.reload

          assert_equal 2, incident.comments.length
          assert_equal "Dear #{incident.requester.name} your ticket #{incident.nice_id} is now solved", incident.comments.last.body
          assert_equal ViaType.LINKED_PROBLEM, incident.comments.last.via_id
          assert_equal problem.id, incident.comments.last.via_reference_id
          assert_equal StatusType.SOLVED, incident.status_id
          assert_equal problem.group, incident.group
          assert_equal problem.assignee, incident.assignee
        end
      end
    end
  end
end
