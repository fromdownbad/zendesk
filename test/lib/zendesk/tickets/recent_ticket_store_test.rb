require_relative "../../../support/test_helper"
require 'zendesk/tickets/recent_ticket_store'

SingleCov.covered!

describe Zendesk::Tickets::RecentTicketStore do
  let(:nice_id) { [{nice_id: 42}] }

  def user
    @user ||= stub("user", id: 42, account_id: 44)
  end

  def store
    @store ||= Zendesk::Tickets::RecentTicketStore.new(user)
  end

  def key
    "recent-tickets-for-#{user.account_id}-#{user.id}-v2"
  end

  describe "#read" do
    it "reads from the Rails cache with a key based on the user id" do
      Rails.cache.write(key, nice_id)
      assert_equal nice_id, store.read
    end

    it "returns an empty array if there is no entry in the cache" do
      assert_equal [], store.read
    end
  end

  describe "#write" do
    it "writes to the Rails cache and logs" do
      Rails.logger.expects(:debug).with("RecentTicketStore write succeeded #{key}:#{nice_id}")

      store.write(nice_id)
      assert_equal nice_id, Rails.cache.read(key)
    end

    it "logs if the write fails" do
      Rails.cache.stubs(:write).returns(false)
      Rails.logger.expects(:debug).with("RecentTicketStore write failed #{key}:#{nice_id}")

      store.write(nice_id)
    end

    it "deletes the entry if the new list is empty" do
      Rails.cache.write(key, nice_id)
      store.write([])
      refute Rails.cache.exist?(key)
    end
  end
end
