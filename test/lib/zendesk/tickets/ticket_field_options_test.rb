require_relative "../../../support/test_helper"
require 'zendesk/tickets/ticket_field_options'

SingleCov.covered! uncovered: 1

describe Zendesk::Tickets::TicketFieldOptions do
  fixtures :ticket_fields

  before do
    @account = accounts(:minimum)
    @subject = Object.new
    @subject.extend(Zendesk::Tickets::TicketFieldOptions)
  end

  describe "#get_priorities_for" do
    before do
      @excludes = [PriorityType.find!("-"), PriorityType.LOW, PriorityType.URGENT]
    end

    it "returns ticket priority options for account" do
      assert_equal PriorityType.list(@excludes), @subject.get_priorities_for(@account)
    end

    it "returns extended ticket priority options for accounts with extended ticket priorities" do
      Account.any_instance.stubs(has_extended_ticket_priorities?: true)
      @excludes -= [PriorityType.LOW, PriorityType.URGENT]
      assert_equal PriorityType.list(@excludes), @subject.get_priorities_for(@account)
    end
  end

  describe "#get_priorities_for_v2" do
    before do
      @excludes = [PriorityType.find!("-"), PriorityType.LOW, PriorityType.URGENT]
    end

    it "returns ticket priority options for account" do
      assert_equal [["Normal", "normal"], ["High", "high"]], @subject.get_priorities_for_v2(@account)
    end

    it "returns extended ticket priority options for accounts with extended ticket priorities" do
      Account.any_instance.stubs(has_extended_ticket_priorities?: true)
      @excludes -=  [PriorityType.LOW, PriorityType.URGENT]
      assert_equal [["Low", "low"], ["Normal", "normal"], ["High", "high"], ["Urgent", "urgent"]], @subject.get_priorities_for_v2(@account)
    end
  end

  describe "#get_status_for" do
    before do
      @excludes = [StatusType.NEW, StatusType.CLOSED, StatusType.DELETED, StatusType.HOLD]
    end

    it "returns ticket status options for account" do
      assert_equal StatusType.list(@excludes), @subject.get_status_for(@account)
    end

    it "returns ticket status options including on-hold for accounts with on-hold enabled" do
      Account.any_instance.stubs(use_status_hold?: true)
      @excludes.delete(StatusType.HOLD)
      assert_equal StatusType.list(@excludes), @subject.get_status_for(@account)
    end
  end

  describe "#get_status_for_v2" do
    before do
      @excludes = [StatusType.NEW, StatusType.CLOSED, StatusType.DELETED, StatusType.HOLD]
    end

    it "returns ticket status options for account" do
      assert_equal [["Open", "open"], ["Pending", "pending"], ["Solved", "solved"]], @subject.get_status_for_v2(@account)
    end

    it "returns ticket status options including on-hold for accounts with on-hold enabled" do
      Account.any_instance.stubs(use_status_hold?: true)
      @excludes.delete(StatusType.HOLD)
      assert_equal [["Open", "open"], ["Pending", "pending"], ["On-hold", "hold"], ["Solved", "solved"]], @subject.get_status_for_v2(@account)
    end
  end

  describe "#get_ticket_types_for" do
    before do
      @excludes = [TicketType.find!("-")]
    end

    it "returns ticket type options for account" do
      assert_equal TicketType.list(@excludes), @subject.get_ticket_types_for(@account)
    end
  end

  describe "#get_ticket_types_for_v2" do
    before do
      @excludes = [TicketType.find!("-")]
    end

    it "returns ticket type options for account" do
      assert_equal [["Question", "question"], ["Incident", "incident"], ["Problem", "problem"], ["Task", "task"]], @subject.get_ticket_types_for_v2(@account)
    end
  end
end
