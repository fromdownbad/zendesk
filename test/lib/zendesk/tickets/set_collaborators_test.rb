require_relative "../../../support/test_helper"
require_relative "../../../support/collaboration_settings_test_helper"
require 'zendesk/tickets/set_collaborators'

SingleCov.covered!

describe Zendesk::Tickets::SetCollaborators do
  fixtures :accounts, :tickets, :users, :user_identities

  let(:agent) { users(:minimum_agent) }
  let(:admin_agent) { users(:minimum_admin) }
  let(:end_user) { users(:minimum_end_user) }
  let(:end_user_2) { users(:minimum_end_user2) }
  let(:end_user_3) { users(:minimum_author) }
  let(:end_user_4) { users(:minimum_search_user) }

  before do
    Account.any_instance.stubs(:has_email_ccs_transform_collaborations?).returns(false)

    @account = accounts(:minimum)
    @account.cc_blacklist = "abcd@gmail.com hansen@google.com"
    @account.save!
    @account.update_attributes(settings: { collaborators_maximum: 5 })

    @ticket = tickets(:minimum_1)
    @ticket.will_be_saved_by(agent)
  end

  def create_collaboration(ticket, user, collaborator_type)
    ticket.collaborations.create(
      ticket: ticket, user: user, collaborator_type: collaborator_type
    )
    ticket.send(:update_current_collaborators)
  end

  def collaboration_exists?(ticket, user, collaborator_type)
    collaboration_in_association = ticket.collaborations.any? do |c|
      c.user_id == user.id && c.collaborator_type == collaborator_type
    end

    collaboration_in_current_collaborators = ticket.current_collaborators&.include? user.name

    collaboration_in_association && collaboration_in_current_collaborators
  end

  describe "#set_collaborators=" do
    [true, false].each do |enabled|
      describe "with follower_and_email_cc_collaborations setting #{enabled ? "enabled" : "disabled"}" do
        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: enabled,
            feature_setting: enabled,
            followers_setting: enabled,
            email_ccs_setting: enabled
          ).apply
        end

        it "does not set emails on current_collaborators when old data did not have emails" do
          @ticket.set_collaborators = end_user.id.to_s
          @ticket.save!
          @ticket.update_column(:current_collaborators, "minimum_end_user")

          @ticket.set_collaborators = "#{end_user.id},#{admin_agent.id}"
          @ticket.will_be_saved_by(@ticket.requester)
          @ticket.save!
          assert_equal "Admin Man, minimum_end_user", @ticket.current_collaborators
        end

        describe "when setting collaborators using emails with numbers in them" do
          before do
            @identity = agent.identities.email.first
            @identity.update_attribute(:value, "foo123@foo.com")
            @ticket.set_collaborators = "foo123@foo.com"
            @ticket.save!
          end

          it "properly deals with emails with numbers in them" do
            assert_equal 1, @ticket.followers.size
          end
        end

        describe "when setting collaborators with emails with capital letters in them" do
          before do
            user_identities(:minimum_agent).update_attribute(:value, "Minimum_agent@aghassipour.com")
            @ticket.set_collaborators = "Minimum_agent@aghassipour.com"
            @ticket.save!
          end

          it "properly deals with them" do
            assert_equal 1, @ticket.followers.size
            assert_equal 1, @ticket.collaborations.size
          end
        end

        describe "when invalid user-ids being passed in" do
          before do
            @ticket.set_collaborators = "12345667"
            @ticket.save!
          end

          it "does not create collaborations using those values" do
            assert_empty @ticket.collaborations
          end
        end

        describe "when blacklisted users are set as collaborators" do
          before do
            blacklisted_address = "hansen@google.com"
            good_user_address_1 = "foo@bar.com"
            good_user_address_2 = end_user.email
            collaborators_to_add = [good_user_address_1, blacklisted_address, good_user_address_2].join(', ')

            assert_empty @ticket.collaborators
            @ticket.set_collaborators = collaborators_to_add
            @ticket.save!
          end

          it "does not add collaborations with blacklisted users" do
            assert_equal 2, @ticket.collaborations.reload.size
            assert_equal 2, @ticket.email_ccs.size if enabled
            assert_equal "Foo <foo@bar.com>, minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when adding invalid users as collaborators" do
          before do
            assert_empty @ticket.collaborations
            @ticket.set_collaborators = "ase@base, #{end_user.email}"
            @ticket.save!
          end

          it "does not add collaborations with invalid users" do
            assert_equal 1, @ticket.collaborations.reload.size
            assert_equal 1, @ticket.email_ccs.size if enabled
            assert_equal "minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when adding users of different types as collaborators" do
          before do
            assert_empty @ticket.collaborations
            user_id = end_user.id
            hash_with_email_and_name = { name: 'test-user', email: 'foo@example.com' }
            hash_with_email = { email: "foo@bar.com" }
            @ticket.set_collaborators = [user_id, hash_with_email_and_name, hash_with_email]
            @ticket.save!
          end

          it "handles all valid types properly" do
            assert_equal [end_user.email, 'foo@example.com', 'foo@bar.com'].sort, @ticket.collaborations.map(&:user).map(&:email).sort
          end
        end

        describe "when adding duplicate references to a user as collaborators" do
          before do
            assert_empty @ticket.collaborations
            @ticket.set_collaborators = "foo@bar.com, foo@bar.com, #{end_user.id}, #{end_user.id}, #{end_user.email}"
            @ticket.save!
            @ticket.collaborations.reload
          end

          it "does not add multiple collaborations per user" do
            assert_equal ['foo@bar.com', end_user.email].sort, @ticket.collaborators.map(&:email).sort
          end
        end

        describe "when replacing a user that already exists as a collaborator" do
          before do
            assert_empty @ticket.collaborations
            @ticket.collaborations.create(user_id: agent.id, collaborator_type: CollaboratorType.LEGACY_CC)
            @ticket.save!

            @ticket.set_collaborators = "foo@bar.com, #{end_user.id}, #{agent.email}"
            @ticket.will_be_saved_by(agent)
            @ticket.save!
            @ticket.collaborations.reload
          end

          it "does not add multiple collaborations per user" do
            assert_equal ['foo@bar.com', end_user.email, agent.email].sort, @ticket.collaborators.map(&:email).sort
          end
        end

        describe 'when a collaborator is removed' do
          let(:collaborator) { agent }

          before do
            @ticket.collaborations.create(
              user_id:           collaborator.id,
              collaborator_type: CollaboratorType.LEGACY_CC
            )

            @ticket.save!

            @ticket.set_collaborators = []
          end

          describe 'and it is saved' do
            before do
              @ticket.will_be_saved_by(collaborator)
              @ticket.save!
            end

            it 'removes the initial collaborator' do
              refute_includes(
                @ticket.reload.collaborations.map(&:user_id),
                collaborator.id
              )
            end
          end

          describe 'and the collaborator is added back again' do
            let(:other_user) { admin_agent }

            before do
              @ticket.set_collaborators = [collaborator.id, admin_agent.id]

              @ticket.will_be_saved_by(collaborator)
              @ticket.save!
            end

            it 'still removes the collaborator' do
              refute_includes(
                @ticket.reload.collaborations.map(&:user_id),
                collaborator.id
              )
            end
          end
        end

        describe "when removing agents and end-users as collaborators" do
          before do
            @ticket.collaborations.create(user_id: end_user.id, collaborator_type: CollaboratorType.EMAIL_CC)
            @ticket.collaborations.create(user_id: agent.id, collaborator_type: CollaboratorType.FOLLOWER)
            @ticket.collaborations.create(user_id: admin_agent.id, collaborator_type: CollaboratorType.FOLLOWER)
          end

          it "does not perform n+1 delete sql calls when removing collaborators" do
            assert_sql_queries 1, /DELETE/ do
              @ticket.set_collaborators = []
              @ticket.will_be_saved_by(agent)
              @ticket.save!
            end
            assert_empty @ticket.collaborations.reload
          end
        end

        describe "when setting same collaborators in a different order" do
          before do
            @ticket.set_collaborators = [agent.id, admin_agent.id]
            @ticket.will_be_saved_by(agent)
            @ticket.save!
            @ticket.set_collaborators = [admin_agent.id, agent.id]
          end

          it "isn't dirty" do
            assert_empty @ticket.changed, "Expected no dirty attributes. Changes: #{@ticket.changes.inspect}"
          end
        end

        it "doesn't bomb when passed nil or -1 in ID list" do
          @ticket.set_collaborators = [nil, -1]
        end

        unless enabled

          it "handles boundary conditions consistently" do
            min = 1
            max = enabled ? 200 : 10
            @ticket.set_collaborators = (min..max).map { |i| "email#{i}@example.com" }
            @ticket.will_be_saved_by(agent)
            @ticket.save!

            min = 1
            max = enabled ? 100 : 5
            expected_current_collaborators = (min..max).map { |i| "Email#{i} <email#{i}@example.com>" }
            assert_equal expected_current_collaborators.sort, @ticket.reload.current_collaborators.split(', ').sort

            min = enabled ? 101 : 6
            max = enabled ? 200 : 10
            @ticket.set_collaborators = (min..max).map { |i| "email#{i}@example.com" }
            @ticket.will_be_saved_by(agent)
            @ticket.save!

            min = enabled ? 101 : 6
            max = enabled ? 200 : 10
            expected_current_collaborators = (min..max).map { |i| "Email#{i} <email#{i}@example.com>" }
            assert_equal expected_current_collaborators.sort, @ticket.reload.current_collaborators.split(', ').sort

            # Don't override the existing collaborators if already at maxn
            min = 1
            max = enabled ? 200 : 10
            @ticket.set_collaborators = (min..max).map { |i| "email#{i}@example.com" }
            @ticket.will_be_saved_by(agent)
            @ticket.save!

            min = enabled ? 101 : 6
            max = enabled ? 200 : 10
            expected_current_collaborators = (min..max).map { |i| "Email#{i} <email#{i}@example.com>" }
            assert_equal expected_current_collaborators.sort, @ticket.reload.current_collaborators.try(:split, ', ').try(:sort)

            # For the collaborations that are getting removed, allow that many new collaborations
            min = 1
            max = enabled ? 199 : 9
            @ticket.set_collaborators = (min..max).map { |i| "email#{i}@example.com" }
            @ticket.will_be_saved_by(agent)
            @ticket.save!

            min = enabled ? 101 : 6
            max = enabled ? 199 : 9
            expected_current_collaborators = (min..max).map { |i| "Email#{i} <email#{i}@example.com>" }
            expected_current_collaborators << "Email1 <email1@example.com>"
            assert_equal expected_current_collaborators.sort, @ticket.reload.current_collaborators.split(', ').sort

            min = enabled ? 151 : 7
            max = enabled ? 300 : 15
            @ticket.set_collaborators = (min..max).map { |i| "email#{i}@example.com" }
            @ticket.will_be_saved_by(agent)
            @ticket.save!

            min = enabled ? 151 : 7
            max = enabled ? 250 : 11
            expected_current_collaborators = (min..max).map { |i| "Email#{i} <email#{i}@example.com>" }
            assert_equal expected_current_collaborators.sort, @ticket.reload.current_collaborators.split(', ').sort
          end
        end

        describe "when agent has readonly user permissions" do
          before do
            Access::Policies::UserPolicy.any_instance.stubs(:create_new?).returns(false)
            @ticket.set_collaborators = "poopthe@cat.com, #{end_user.email}"
            @ticket.save!
          end

          it "does not create users" do
            assert_equal [end_user.email], @ticket.collaborators.map(&:email)
            assert @ticket.audits.last.events.first.to_s.include?("poopthe@cat.com")
          end
        end

        describe "with followers and email_ccs" do
          before do
            @ticket.set_collaborators = "foo@bar.com, #{end_user.email}"
            @ticket.save!

            assert_equal 2, @ticket.collaborations.reload.size

            expected_followers_count = enabled ? 0 : 2
            assert_equal expected_followers_count, @ticket.followers.size

            expected_email_ccs_count = enabled ? 2 : 0
            assert_equal expected_email_ccs_count, @ticket.email_ccs.size

            @ticket.will_be_saved_by(agent)
          end

          it "removes when setting empty array" do
            @ticket.set_collaborators = []
            @ticket.save!
            assert_empty @ticket.collaborations.reload
            assert_empty @ticket.followers
            assert_empty @ticket.email_ccs
          end

          it "removes when setting nil" do
            @ticket.set_collaborators = nil
            @ticket.save!
            assert_empty @ticket.collaborations.reload
            assert_empty @ticket.followers
            assert_empty @ticket.email_ccs
          end

          it "updates properly" do
            @ticket.set_collaborators = [admin_agent.id, end_user.email, {name: 'test-user', email: 'foo@example.com'}]
            @ticket.save!
            @ticket.collaborations.reload
            assert_equal [admin_agent.email, end_user.email, 'foo@example.com'].sort, @ticket.collaborations.map(&:user).map(&:email).sort
          end

          it "updates properly with duplicate collaborators" do
            existing_collaboration_user_ids = @ticket.collaborations.map(&:user_id)
            collaborators = existing_collaboration_user_ids.dup << admin_agent.email
            @ticket.set_collaborators = collaborators
            collaborators << { name: 'test-user', email: 'foo@example.com' }
            @ticket.set_collaborators = collaborators
            @ticket.save!
            @ticket.collaborations.reload

            expected_collaborators_count = 4
            assert_equal expected_collaborators_count, @ticket.collaborations.size

            expected_collaborator_email_addresses = [
              admin_agent.email,
              'foo@bar.com',
              end_user.email,
              'foo@example.com'
            ]
            assert_equal expected_collaborator_email_addresses.sort, @ticket.collaborators.map(&:email).sort

            expected_followers_count = enabled ? 1 : 4
            assert_equal expected_followers_count, @ticket.followers.size

            expected_email_ccs_count = enabled ? 3 : 0
            assert_equal expected_email_ccs_count, @ticket.email_ccs.size
          end

          it "removes existing follower if blacklisted" do
            # reset blacklist
            @account.cc_blacklist = nil
            @account.save
            @ticket.account(true)

            # add some new collaborators which will then be blacklisted
            @ticket.set_collaborators = "#{users(:photo_user).id}, hansen@google.com, #{end_user.email}"
            @ticket.save!
            @ticket.collaborations.reload

            @account.cc_blacklist = "hansen@google.com #{users(:photo_user).email}"
            @account.save!
            @ticket.account(true)

            @ticket.set_collaborators = "#{users(:photo_user).id}, hansen@google.com, #{end_user.email}"
            @ticket.will_be_saved_by(@ticket.requester)
            @ticket.save!

            assert_equal "minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
            assert_equal [end_user.email], @ticket.collaborators.map(&:email)
          end

          it "removes followers if they don't exist" do
            deleted_user_collaboration = @ticket.collaborations.first
            deleted_user_collaboration.user_id = nil

            @ticket.set_collaborators = "foo@bar.com, hansen@google.com, #{end_user.email}"
            @ticket.will_be_saved_by(@ticket.requester)
            @ticket.save!

            @ticket.collaborations.reload

            refute_includes @ticket.collaborators, deleted_user_collaboration
          end
        end

        describe "when the emails are ill-formatted" do
          before do
            assert_empty @ticket.collaborations
            @ticket.set_collaborators = "foo@bar.com,barfoo.com,bar@"
            @ticket.save!
          end

          it "skips the bad ones" do
            assert_equal 1, @ticket.collaborations.reload.size
          end
        end
      end
    end

    describe "with follower_and_email_cc_collaborations setting enabled" do
      describe "with ticket_followers setting enabled and comment_email_ccs setting enabled" do
        before do
          Account.any_instance.stubs(:has_email_ccs_transform_collaborations?).returns(true)

          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: true,
            email_ccs_setting: true
          ).apply
        end

        describe "when setting agent and end-user as collaborator" do
          before do
            assert_empty @ticket.collaborators
            @ticket.set_collaborators = "#{end_user.id},#{admin_agent.id}"
            @ticket.save!
            @ticket.collaborations.reload
          end

          it "sets agent users as followers" do
            assert_equal 1, @ticket.followers.size
            assert_equal 1, @ticket.collaborations.where(collaborator_type: CollaboratorType.FOLLOWER).count
          end

          it "sets end users as email ccs" do
            assert_equal 1, @ticket.email_ccs.size
            assert_equal 1, @ticket.collaborations.where(collaborator_type: CollaboratorType.EMAIL_CC).count
          end

          it "sets the current_collaborators to everyone" do
            assert_equal "Admin Man <minimum_admin@aghassipour.com>, minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          end
        end

        describe "when specifying collaborator_type" do
          subject { ticket.set_collaborators_with_type(collaboration_users, collaborator_type) }

          let(:email_cc_user) { end_user }
          let(:collaboration_users) { [email_cc_user.id] }
          let(:ticket) { @ticket }
          let(:collaborator_type) { CollaboratorType.EMAIL_CC }

          it "creates a new collaboration with the specified type" do
            ticket.collaborations.must_be_empty
            subject
            assert_equal collaborator_type, ticket.collaborations.first.collaborator_type
          end

          describe "when a collaboration already exists for the user with the same collaborator_type" do
            before { create_collaboration(ticket, email_cc_user, collaborator_type) }

            it "does not add a duplicate collaboration" do
              assert collaboration_exists?(ticket, email_cc_user, collaborator_type)
              assert_equal 1, ticket.collaborations.size
              subject
              assert collaboration_exists?(ticket, email_cc_user, collaborator_type)
              assert_equal 1, ticket.collaborations.size
            end
          end

          describe "when a collaboration already exists for the user with a different collaborator_type" do
            before { create_collaboration(ticket, email_cc_user, CollaboratorType.FOLLOWER) }

            it "creates a new collaboration with the specified type" do
              refute collaboration_exists?(ticket, email_cc_user, CollaboratorType.EMAIL_CC)
              subject
              assert collaboration_exists?(ticket, email_cc_user, CollaboratorType.EMAIL_CC)
            end

            it "does not remove the existing collaboration of the other type" do
              assert collaboration_exists?(ticket, email_cc_user, CollaboratorType.FOLLOWER)
              subject
              assert collaboration_exists?(ticket, email_cc_user, CollaboratorType.FOLLOWER)
            end
          end

          describe "when a collaboration already exists for a different user with a different collaborator_type" do
            let(:follower_user) { agent }

            before { create_collaboration(ticket, follower_user, CollaboratorType.FOLLOWER) }

            describe_with_arturo_enabled :email_fix_current_collaborators_set_collaborators do
              it "creates a new collaboration with the specified type" do
                refute collaboration_exists?(ticket, email_cc_user, CollaboratorType.EMAIL_CC)
                subject
                assert collaboration_exists?(ticket, email_cc_user, CollaboratorType.EMAIL_CC)
              end

              it "does not remove the existing collaboration of the other type" do
                assert collaboration_exists?(ticket, follower_user, CollaboratorType.FOLLOWER)
                subject
                assert collaboration_exists?(ticket, follower_user, CollaboratorType.FOLLOWER)
              end
            end

            describe_with_arturo_disabled :email_fix_current_collaborators_set_collaborators do
              it "creates a new collaboration with the specified type" do
                refute collaboration_exists?(ticket, email_cc_user, CollaboratorType.EMAIL_CC)
                subject
                assert collaboration_exists?(ticket, email_cc_user, CollaboratorType.EMAIL_CC)
              end

              it "removes the existing collaboration of the other type" do
                assert collaboration_exists?(ticket, follower_user, CollaboratorType.FOLLOWER)
                subject
                refute collaboration_exists?(ticket, follower_user, CollaboratorType.FOLLOWER)
              end
            end
          end
        end
      end

      describe "with ticket_followers setting enabled and comment_email_ccs setting disabled" do
        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: true,
            email_ccs_setting: false
          ).apply

          assert_empty @ticket.collaborators
          @ticket.set_collaborators = ['foo100@example.com', end_user.id, admin_agent.id, agent.id]
          @ticket.save!
        end

        it "sets agent users as followers" do
          assert_equal 2, @ticket.followers.size
          assert_equal [admin_agent.id, agent.id].sort, @ticket.followers.map(&:id).sort
        end

        it "drops end users and new users" do
          assert_empty @ticket.email_ccs
        end
      end

      describe "with ticket_followers setting disabled and comment_email_ccs setting enabled" do
        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: false,
            email_ccs_setting: true
          ).apply

          assert_empty @ticket.collaborators
          @ticket.set_collaborators = ['foo100@example.com', end_user.id, admin_agent.id, agent.id]
          @ticket.save!
        end

        # Is this what we decided?
        it "drops agent users" do
          assert_empty @ticket.followers
        end

        it "sets end users and new users as email ccs" do
          assert_equal 2, @ticket.email_ccs.size
          assert_equal [end_user.email, 'foo100@example.com'].sort, @ticket.email_ccs.map(&:email).sort
        end
      end

      describe "with ticket_followers setting disabled and comment_email_ccs setting disabled" do
        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: false,
            email_ccs_setting: false
          ).apply

          assert_empty @ticket.collaborators
          @ticket.set_collaborators = ['foo100@example.com', end_user.id, admin_agent.id, agent.id]
          @ticket.save!
        end

        it "drops all collaborators" do
          assert_empty @ticket.collaborators
        end

        it "drops all agent users" do
          assert_empty @ticket.followers
        end

        it "drops all end users and new users" do
          assert_empty @ticket.email_ccs
        end
      end
    end

    describe "with follower_and_email_cc_collaborations setting disabled" do
      before do
        CollaborationSettingsTestHelper.new(
          feature_arturo: false,
          feature_setting: false,
          followers_setting: false,
          email_ccs_setting: false
        ).apply
      end

      it "sets collaborators" do
        assert_empty @ticket.collaborators
        @ticket.set_collaborators = "#{end_user.id},#{admin_agent.id}"
        @ticket.save!
        assert_equal 2, @ticket.collaborators.reload.size
        assert_equal "Admin Man <minimum_admin@aghassipour.com>, minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
      end

      it "followers are equivalent to collaborators (when follower_and_email_cc_collaborations setting is disabled)" do
        assert_empty @ticket.collaborators
        @ticket.set_collaborators = "#{end_user.id},#{admin_agent.id}"
        @ticket.save!
        assert_equal 2, @ticket.followers.reload.size
      end

      it "does not allow duplicates" do
        assert_empty @ticket.collaborators
        @ticket.set_collaborators = "#{end_user.id},#{admin_agent.id},#{admin_agent.id}"
        @ticket.save!
        assert_equal 2, @ticket.collaborators.reload.size
        assert_equal "Admin Man <minimum_admin@aghassipour.com>, minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
      end

      it "does not set emails when old data did not have emails" do
        @ticket.set_collaborators = end_user.id.to_s
        @ticket.save!
        @ticket.update_column(:current_collaborators, "minimum_end_user")

        @ticket.set_collaborators = "#{end_user.id},#{admin_agent.id}"
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
        assert_equal "Admin Man, minimum_end_user", @ticket.current_collaborators
      end

      it "limits the number of end-users while allowing any number of agents" do
        @account.update_attributes(settings: { collaborators_maximum: 2 })
        @ticket.set_collaborators = [agent.id, "foo@bar.com", "hansen@google.com", end_user.email, admin_agent.id]
        @ticket.save!
        @ticket.collaborators.reload
        assert_equal [agent.email, admin_agent.email, end_user.email, 'foo@bar.com'].sort, @ticket.collaborators.map(&:email).sort
      end

      describe "with collaborators" do
        before do
          @ticket.set_collaborators = "foo@bar.com, hansen@google.com, #{end_user.email}"
          @ticket.save!
          assert_equal 2, @ticket.collaborators.reload.size
          @ticket.will_be_saved_by(agent)
        end

        it "removes when setting empty array" do
          @ticket.set_collaborators = []
          @ticket.save!
          assert_empty @ticket.collaborators.reload
        end
      end

      describe "when passed emails" do
        it "sets followers" do
          assert_empty @ticket.collaborators
          @ticket.set_collaborators = "#{end_user.email},foo@bar.com"
          @ticket.save!
          assert_equal 2, @ticket.collaborators.reload.size
          assert_equal 2, @ticket.followers.size
          assert_equal "Foo <foo@bar.com>, minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
          assert_not_nil(new_user = @ticket.account.find_user_by_email("foo@bar.com"))
          assert new_user.is_end_user?
        end
      end

      describe "when setting more than the maximum number of collaborators" do
        before do
          assert_empty @ticket.collaborators
          @ticket.set_collaborators = "foo@bar.com,bar@foo.com,fum@baz.com,baz@foo.com,xyzzy@foo.com,fam@xyzzy.com"
          @ticket.save!
          @ticket.collaborations.reload
        end

        it "does not add more than collaborators_maximum new users" do
          assert_equal @ticket.account.settings.collaborators_maximum, @ticket.collaborators.size
          assert_equal @ticket.account.settings.collaborators_maximum, @ticket.followers.size
        end

        describe "when number of collaborations is already at maximum" do
          before do
            @ticket.set_collaborators = (@ticket.follower_ids + ["foo1@example.com", "foo2@example.com", "foo3@example.com"]).join(",")
            @ticket.will_be_saved_by(@ticket.requester)
            @ticket.save!
            @ticket.collaborations.reload
          end

          it "does not exceed the maximum collaborators count" do
            assert_equal @ticket.account.settings.collaborators_maximum, @ticket.collaborators.size
            assert_equal @ticket.account.settings.collaborators_maximum, @ticket.followers.size
          end
        end
      end

      describe "enforces collaborators_maximum" do
        before do
          @account.update_attributes(settings: { collaborators_maximum: 1 })
          @account.save!
        end

        it "limits number of end user collaborators" do
          @ticket.set_collaborators = "#{end_user_3.id}, #{end_user_2.id}"

          @ticket.save!
          assert_equal 1, @ticket.collaborators.reload.size
          assert_equal 1, @ticket.followers.size
          assert_equal "Author Minimum <minimum_author@aghassipour.com>", @ticket.current_collaborators

          @ticket.additional_collaborators = end_user_2.id.to_s
          @ticket.will_be_saved_by(agent)
          @ticket.save!
          assert_equal 1, @ticket.collaborators.reload.size
          assert_equal 1, @ticket.followers.size
          assert_equal "Author Minimum <minimum_author@aghassipour.com>", @ticket.current_collaborators
        end

        it "allows unlimited agents as followers" do
          @ticket.set_collaborators = "#{end_user_2.id},#{admin_agent.id}"
          @ticket.save!
          assert_equal 2, @ticket.collaborators.reload.size
          assert_equal 2, @ticket.followers.size
          assert_equal "Admin Man <minimum_admin@aghassipour.com>, minimum_end_user2 <>", @ticket.current_collaborators

          @ticket.additional_collaborators = users(:minimum_admin_not_owner).id.to_s
          @ticket.will_be_saved_by(agent)
          @ticket.save!
          assert_equal 3, @ticket.collaborators.reload.size
          assert_equal 3, @ticket.followers.size
          assert_equal "Admin Man <minimum_admin@aghassipour.com>, Not owner man <minimum_admin_not_owner@aghassipour.com>, minimum_end_user2 <>", @ticket.current_collaborators
        end
      end
    end
  end

  describe "#additional_collaborators=" do
    before do
      @ticket.collaborations.build(user: admin_agent)
      @ticket.will_be_saved_by(agent)
      @ticket.save!
    end

    it "adds collaborators" do
      @ticket.additional_collaborators = end_user.id.to_s
      @ticket.will_be_saved_by(agent)
      @ticket.save!
      assert_equal 2, @ticket.collaborators.reload.size
      assert_equal "Admin Man <minimum_admin@aghassipour.com>, minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
    end

    it "doesn't add duplicate collaborators" do
      @ticket.additional_collaborators = end_user.id.to_s
      @ticket.additional_collaborators = [admin_agent.id.to_s, end_user.id.to_s]
      @ticket.will_be_saved_by(agent)
      @ticket.save!
      assert_equal 2, @ticket.collaborators.reload.size
      assert_equal "Admin Man <minimum_admin@aghassipour.com>, minimum_end_user <minimum_end_user@aghassipour.com>", @ticket.current_collaborators
    end
  end

  describe "#filtered_collaborators" do
    it "doesn't perform n+1 queries when filtering collaborators" do
      @ticket.set_collaborators = [agent.id]
      @ticket.will_be_saved_by(agent)
      @ticket.save!

      collaborator_list = [
        agent.id,
        admin_agent.id,
        agent.email,
        admin_agent.email,
        "foo@example.com",
        "hansen@google.com"
      ]

      assert_sql_queries 2, /user_identities/ do
        assert_sql_queries 1, /users/ do
          @ticket.send(:filtered_collaborators, list: collaborator_list, update_type: 'replace')
        end
      end
    end

    it "works when there is no collaborator" do
      result = @ticket.send(:filtered_collaborators, list: [], update_type: 'add', collaboration_user_ids: [])
      assert_equal [[], []], result
    end
  end

  describe "#set_collaborators_with_type" do
    let(:string_or_array_of_ids_or_emails) { [end_user.id, 'foo@example.com'] }
    let(:collaborator_type) { CollaboratorType.EMAIL_CC }
    let(:ticket) { @ticket }

    subject do
      ticket.set_collaborators_with_type(
        string_or_array_of_ids_or_emails, collaborator_type
      )
    end

    it 'calls update_collaborators with collaborators and collaborator_type' do
      ticket.expects(:update_collaborators).with(collaborators: string_or_array_of_ids_or_emails, collaborator_type: collaborator_type)
      subject
    end

    describe 'replacing existing collaborations for user and type' do
      let(:string_or_array_of_ids_or_emails) { [admin_agent.id] }

      before do
        create_collaboration(ticket, agent, CollaboratorType.FOLLOWER)
        create_collaboration(ticket, agent, CollaboratorType.EMAIL_CC)
        create_collaboration(ticket, agent, CollaboratorType.LEGACY_CC)
      end

      describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
        describe 'adding followers' do
          let(:collaborator_type) { CollaboratorType.FOLLOWER }

          it 'calls update_collaborators with collaborators and collaborator_type' do
            ticket.expects(:update_collaborators).with(collaborators: string_or_array_of_ids_or_emails, collaborator_type: collaborator_type)
            subject
          end

          it 'marks the FOLLOWER collaboration for removal' do
            subject # FOLLOWER, EMAIL_CC, LEGACY_CC (agent), FOLLOWER (admin_agent)
            ticket.save! # FOLLOWER (agent) is removed

            assert_equal 3, ticket.collaborations.size
            collaboration_exists?(ticket, agent, CollaboratorType.EMAIL_CC)
            collaboration_exists?(ticket, agent, CollaboratorType.LEGACY_CC)
            collaboration_exists?(ticket, admin_agent, CollaboratorType.FOLLOWER)
          end
        end

        describe 'adding email CCs' do
          let(:collaborator_type) { CollaboratorType.EMAIL_CC }

          it 'calls update_collaborators with collaborators and collaborator_type' do
            ticket.expects(:update_collaborators).with(collaborators: string_or_array_of_ids_or_emails, collaborator_type: collaborator_type)
            subject
          end

          it 'marks the EMAIL_CC collaboration for removal' do
            subject # FOLLOWER, EMAIL_CC, LEGACY_CC (agent), FOLLOWER (admin_agent)
            ticket.save! # EMAIL_CC (agent) is removed

            assert_equal 3, ticket.collaborations.size
            collaboration_exists?(ticket, agent, CollaboratorType.FOLLOWER)
            collaboration_exists?(ticket, agent, CollaboratorType.LEGACY_CC)
            collaboration_exists?(ticket, admin_agent, CollaboratorType.FOLLOWER)
          end
        end

        describe 'adding legacy CCs' do
          let(:collaborator_type) { CollaboratorType.LEGACY_CC }

          it 'calls update_collaborators with collaborators and collaborator_type' do
            ticket.expects(:update_collaborators).with(collaborators: string_or_array_of_ids_or_emails, collaborator_type: collaborator_type)
            subject
          end

          it 'marks the LEGACY_CC collaboration for removal' do
            subject # FOLLOWER, EMAIL_CC, LEGACY_CC (agent), FOLLOWER (admin_agent)
            ticket.save! # LEGACY_CC (agent) is removed

            assert_equal 3, ticket.collaborations.size
            collaboration_exists?(ticket, agent, CollaboratorType.FOLLOWER)
            collaboration_exists?(ticket, agent, CollaboratorType.EMAIL_CC)
            collaboration_exists?(ticket, admin_agent, CollaboratorType.FOLLOWER)
          end
        end
      end
    end
  end

  describe "#additional_collaborators_with_type" do
    let(:string_or_array_of_ids_or_emails) { [end_user.id, 'foo@example.com'] }
    let(:collaborator_type) { CollaboratorType.EMAIL_CC }
    let(:ticket) { @ticket }

    it "calls update_collaborators with collaborators, update_type, and collaborator_type" do
      Ticket.any_instance.expects(:update_collaborators).with(collaborators: string_or_array_of_ids_or_emails, update_type: 'add', collaborator_type: collaborator_type)
      ticket.additional_collaborators_with_type(string_or_array_of_ids_or_emails, collaborator_type)
    end
  end

  describe "#set_collaborators_with_email_cc_type" do
    before do
      CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
      @ticket.set_collaborators_with_email_cc_type = [agent.id, end_user.id, end_user_2.id]
      @ticket.save!
    end

    it "sets collaborators as email_ccs" do
      assert_equal 3, @ticket.email_ccs.size
      assert_empty @ticket.followers
    end
  end

  describe "#additional_collaborators_with_email_cc_type" do
    before do
      CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
      @ticket.collaborations.build(user: end_user)
      @ticket.additional_collaborators_with_email_cc_type = agent.id.to_s
      @ticket.save!
    end

    it "adds collaborators as email_ccs" do
      assert_equal 2, @ticket.email_ccs.size
      assert_empty @ticket.followers
    end
  end

  describe "#set_collaborators_with_follower_type" do
    before do
      CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
    end

    # This test checks for collaborations being added as the method does not verify the role itself.
    it "builds collaborations before saving with follower type" do
      @ticket.set_collaborators_with_follower_type = [agent.id, admin_agent.id, end_user.id]
      assert_equal 3, @ticket.collaborations.size
      assert collaboration_exists?(@ticket, agent, CollaboratorType.FOLLOWER)
      assert collaboration_exists?(@ticket, admin_agent, CollaboratorType.FOLLOWER)
      assert collaboration_exists?(@ticket, end_user, CollaboratorType.FOLLOWER)
    end
  end

  describe "#additional_collaborators_with_follower_type" do
    before do
      CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
    end

    # This test checks for additional collaborations being added as the method does not verify the role itself.
    it "adds additional collaborators as followers before saving" do
      create_collaboration(@ticket, agent, CollaboratorType.FOLLOWER)
      @ticket.additional_collaborators_with_follower_type = [admin_agent.id.to_s, end_user.id.to_s]
      assert_equal 3, @ticket.collaborations.size
      assert collaboration_exists?(@ticket, agent, CollaboratorType.FOLLOWER)
      assert collaboration_exists?(@ticket, admin_agent, CollaboratorType.FOLLOWER)
      assert collaboration_exists?(@ticket, end_user, CollaboratorType.FOLLOWER) # end user gets added to collaborations
    end
  end

  describe "#find_or_build_new_user" do
    let(:ticket) { @ticket }
    let(:requester) { @ticket.requester }

    subject { ticket.send(:find_or_build_new_user, new_user_data) }

    describe 'when the user data has the same email as the requester' do
      let(:new_user_data) do
        { name: requester.name, email: requester.email }
      end

      it 'returns the requester' do
        ticket.expects(:build_new_user).never
        assert_equal requester, subject
      end
    end

    describe 'when the user data has the an email that matches a non-primary email of the requester' do
      let!(:non_primary_identity) do
        requester.identities.email.create! value: 'alternate@example.com', priority: 2
      end
      let(:new_user_data) do
        { name: requester.name, email: non_primary_identity.value }
      end

      it 'returns the requester' do
        ticket.expects(:build_new_user).never
        assert_equal requester, subject
      end
    end

    describe 'when the user data contains information that does not match an existing user' do
      let(:new_user_data) do
        { name: 'Other Guy', email: 'other@example.com' }
      end

      it 'builds a new user' do
        ticket.expects(:build_new_user).with(new_user_data).once
        refute_equal requester, subject
      end
    end
  end

  describe "enforces collaborators_maximum" do
    let(:collaborators_maximum) { 1 }
    let(:subdomain) { @account.subdomain }

    before do
      @account.settings.collaborators_maximum = collaborators_maximum
      @account.save!
    end

    it "limits number of end user collaborators" do
      @ticket.set_collaborators = "#{users(:minimum_author).id}, #{end_user_2.id}"
      @ticket.save!
      assert_equal 1, @ticket.collaborators.reload.size
      assert_equal 1, @ticket.followers.size
      assert_equal "Author Minimum <minimum_author@aghassipour.com>", @ticket.current_collaborators

      @ticket.additional_collaborators = end_user_2.id.to_s
      @ticket.will_be_saved_by(agent)
      @ticket.save!
      assert_equal 1, @ticket.collaborators.reload.size
      assert_equal 1, @ticket.followers.size
      assert_equal "Author Minimum <minimum_author@aghassipour.com>", @ticket.current_collaborators
    end

    it "allows unlimited agents as followers" do
      @ticket.set_collaborators = "#{end_user_2.id},#{admin_agent.id}"
      @ticket.save!
      assert_equal 2, @ticket.collaborators.reload.size
      assert_equal 2, @ticket.followers.size
      assert_equal "Admin Man <minimum_admin@aghassipour.com>, minimum_end_user2 <>", @ticket.current_collaborators

      @ticket.additional_collaborators = users(:minimum_admin_not_owner).id.to_s
      @ticket.will_be_saved_by(agent)
      @ticket.save!
      assert_equal 3, @ticket.collaborators.reload.size
      assert_equal 3, @ticket.followers.size
      assert_equal "Admin Man <minimum_admin@aghassipour.com>, Not owner man <minimum_admin_not_owner@aghassipour.com>, minimum_end_user2 <>", @ticket.current_collaborators
    end

    describe 'when some collaborators were skipped' do
      let(:total_attempted_ccs) { 3 }
      let(:skipped) { [end_user_2.id, end_user.id] }
      let(:id) { @ticket.id }

      subject do
        # 1 collaborator is allowed, 3 were attempted.
        @ticket.set_collaborators = "#{end_user.id},#{end_user_4.id},#{end_user_2.id}"
        @ticket.will_be_saved_by(agent)
        @ticket.save!

        assert_equal 1, @ticket.collaborators.reload.size
        assert_equal 1, @ticket.followers.size
      end

      before { Rails.logger.stubs(:warn) }
      before { @ticket.stubs(collaborators_limit: 2) }

      it 'logs the error and the list of skipped collaborations' do
        Rails.logger.expects(:warn).with("More than #{collaborators_maximum} collaborators requested on ticket #{subdomain}/#{id}")
        Rails.logger.expects(:warn).with("Skipped: #{skipped}")
        subject
      end

      it 'logs the total attempted ccs and increments the statd metric' do
        Zendesk::StatsD::Client.any_instance.stubs(:increment)
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('collaborators.exceeding_ccs_limit').once
        Rails.logger.expects(:warn).with("Attempting to exceed 2 collaborators. #{total_attempted_ccs} collaborators requested on ticket #{subdomain}/#{id}")
        subject
      end
    end
  end
end
