require_relative '../../../support/test_helper'

SingleCov.covered!

describe "Zendesk::Permissions" do
  describe "PermissionsAgentClient" do
    let(:authz_path) { '/v1/data/permissions/allow' }
    let(:allow_scopes_path) { '/v1/data/permissions/allow_scopes' }
    let(:account) { { id: 2, shard_id: 1 } }
    let(:user) { { permission_set_id: 1 } }
    let(:resource_scopes) { ["tickets:read"] }
    let(:permissions_agent_decision_id) { '35771ef6-1dc9-42d1-aa4c-d120f4b84a75' }
    let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }
    let(:permissions_agent_client) { Zendesk::ExternalPermissions::PermissionsAgentClient.new(client_options) }
    let(:permissions_agent_base_url) { permissions_agent_client.agent_url }

    describe "when authz request is made to opa" do
      it 'should succeed and return result' do
        request = permissions_agent_client.request(account, user, resource_scopes)
        stub_request = stub_request(:post, permissions_agent_base_url + authz_path).with(
            body: { input: { account: account, user: user, resource_scopes: resource_scopes } }
          ).to_return(status: 200, body: { decision_id: permissions_agent_decision_id, result: true }.to_json, headers: { "Content-Type" => "application/json" })
        stub_response = permissions_agent_client.allow!(request)
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['decision_id'], permissions_agent_decision_id
        assert_equal stub_response_body['result'], true
      end

      it 'raises ResponseError' do
        request = '{ input }'
        stub_request = stub_request(:post, permissions_agent_base_url + authz_path).to_raise(Kragle::ResponseError)
        assert_raises Kragle::ResponseError do
          permissions_agent_client.allow!(request)
        end
        assert_requested stub_request, times: 1
      end

      it 'raises ClientError' do
        request = '{ input }'
        stub_request = stub_request(:post, permissions_agent_base_url + authz_path).to_raise(Faraday::ClientError)
        assert_raises Faraday::ClientError do
          permissions_agent_client.allow!(request)
        end
        assert_requested stub_request, times: 1
      end
    end

    describe "when allow_scopes request is made to opa" do
      it 'should succeed and return result' do
        request = permissions_agent_client.request(account, user, resource_scopes)
        stub_request = stub_request(:post, permissions_agent_base_url + allow_scopes_path).with(
            body: { input: { account: account, user: user, resource_scopes: resource_scopes } }
          ).to_return(status: 200, body: { decision_id: permissions_agent_decision_id, result: resource_scopes }.to_json, headers: { "Content-Type" => "application/json" })
        stub_response = permissions_agent_client.allow_scopes!(request)
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['decision_id'], permissions_agent_decision_id
        assert_equal stub_response_body['result'].first, resource_scopes.first
      end

      it 'raises ResponseError' do
        request = '{ input }'
        stub_request = stub_request(:post, permissions_agent_base_url + allow_scopes_path).to_raise(Kragle::ResponseError)
        assert_raises Kragle::ResponseError do
          permissions_agent_client.allow_scopes!(request)
        end
        assert_requested stub_request, times: 1
      end

      it 'raises ClientError' do
        request = '{ input }'
        stub_request = stub_request(:post, permissions_agent_base_url + allow_scopes_path).to_raise(Faraday::ClientError)
        assert_raises Faraday::ClientError do
          permissions_agent_client.allow_scopes!(request)
        end
        assert_requested stub_request, times: 1
      end
    end
  end
end
