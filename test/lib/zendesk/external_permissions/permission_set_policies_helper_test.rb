require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 2

describe "Zendesk::Permissions" do
  describe "PermissionSetPoliciesHelper" do
    let(:account) { accounts(:minimum) }
    let(:subdomain) { 'test' }
    let(:user_id) { 10011 }
    let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }
    let(:permissions_set_policies_helper) { Zendesk::ExternalPermissions::PermissionSetPoliciesHelper.new(subdomain, user_id, client_options) }
    let(:subdomain_policies_url) { permissions_set_policies_helper.policies_url }
    let(:ps_permissions) { {"manage_ticket_fields" => "0", "manage_ticket_forms" => "0", "manage_user_fields" => "1", "manage_organization_fields" => "0", "manage_contextual_workspaces" => "0"} }

    before do
      @permission_set = PermissionSet.new(account: account, name: 'my name')
      @permission_set.save!
      Account.any_instance.stubs(:has_permissions_custom_role_policy_dual_save?).returns(true)
    end

    describe "when policy_service_permissions is provided" do
      let(:ps_id) { '1234' }
      let(:ps_name) { 'test' }

      before do
        PermissionSet.any_instance.stubs(:id).returns(ps_id)
        PermissionSet.any_instance.stubs(:name).returns(ps_name)
      end

      it "calls permissions service to create policy when given permission set" do
        request_body = { policy: { name: ps_name, subject: { id: ps_id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'user_fields', scopes: ['manage'] }] } }
        stub_request = stub_request(:post, subdomain_policies_url).with(body: request_body).to_return(status: 200)
        response = permissions_set_policies_helper.create_policy!(@permission_set, ps_permissions)
        assert response.success?
        assert_requested stub_request, times: 1
      end
    end

    describe "handle update" do
      let(:permissions_service_failure_handler) { lambda { |_| } }
      let(:update_permission_set_handler) { lambda { |_| } }

      it "correctly handles on permissions service success" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_update_policy_request = stub_request(:put, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_return(status: 200)
        update_permission_set_handler.expects(:call)
        permissions_service_failure_handler.expects(:call).never
        permissions_set_policies_helper.handle_update!(@permission_set, update_permission_set_handler, permissions_service_failure_handler, ps_permissions)
        assert_requested stub_get_policy_request, times: 1
        assert_requested stub_update_policy_request, times: 1
      end

      it "correctly handles on permissions service failure" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_raise(Kragle::ServerError)
        update_permission_set_handler.expects(:call).never
        permissions_service_failure_handler.expects(:call)
        permissions_set_policies_helper.handle_update!(@permission_set, update_permission_set_handler, permissions_service_failure_handler, ps_permissions)
        assert_requested stub_get_policy_request, times: 1
      end

      it "correctly handles 3XX" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_update_policy_request = stub_request(:put, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_return(status: 300)
        update_permission_set_handler.expects(:call).never
        permissions_service_failure_handler.expects(:call)
        permissions_set_policies_helper.handle_update!(@permission_set, update_permission_set_handler, permissions_service_failure_handler, ps_permissions)
        assert_requested stub_get_policy_request, times: 1
        assert_requested stub_update_policy_request, times: 1
      end
    end

    it "calls permissions service to update policy when given permission set if associated policy exists" do
      stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
          status: 200,
          body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'user_fields', scopes: ['manage'] }] }] }.to_json,
          headers: { "Content-Type" => "application/json" }
        )
      stub_update_policy_request = stub_request(:put, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_return(status: 200)
      response = permissions_set_policies_helper.update_policy!(@permission_set, ps_permissions)
      assert response.success?
      assert_requested stub_get_policy_request, times: 1
      assert_requested stub_update_policy_request, times: 1
    end

    it "calls permissions service to create policy when given permission set if associated policy does not exist" do
      stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
          status: 200,
          body: { policies: [] }.to_json,
          headers: { "Content-Type" => "application/json" }
        )
      stub_create_policy_request = stub_request(:post, subdomain_policies_url).to_return(
          status: 200,
          body: { policy: { id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'user_fields', scopes: ['manage'] }] } }.to_json,
          headers: { "Content-Type" => "application/json" }
        )
      response = permissions_set_policies_helper.update_policy!(@permission_set, ps_permissions)
      assert response.success?
      assert_requested stub_get_policy_request, times: 1
      assert_requested stub_create_policy_request, times: 1
    end

    describe "handle delete" do
      let(:permissions_service_failure_handler) { lambda { |_| } }
      let(:delete_permission_set_handler) { lambda { |_| } }

      it "correctly handles on permissions service success - policy found" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_delete_policy_request = stub_request(:delete, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_return(status: 204)
        delete_permission_set_handler.expects(:call)
        permissions_service_failure_handler.expects(:call).never
        permissions_set_policies_helper.handle_delete!(@permission_set, delete_permission_set_handler, permissions_service_failure_handler)
        assert_requested stub_get_policy_request, times: 1
        assert_requested stub_delete_policy_request, times: 1
      end

      it "correctly handles on permissions service success - policy not found" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
          status: 200,
          body: { policies: [] }.to_json,
          headers: { "Content-Type" => "application/json" }
        )
        delete_permission_set_handler.expects(:call)
        permissions_service_failure_handler.expects(:call).never
        permissions_set_policies_helper.handle_delete!(@permission_set, delete_permission_set_handler, permissions_service_failure_handler)
        assert_requested stub_get_policy_request, times: 1
      end

      it "correctly handles permissions service resource_not_found" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_raise(Kragle::ResourceNotFound)
        delete_permission_set_handler.expects(:call)
        permissions_service_failure_handler.expects(:call).never
        permissions_set_policies_helper.handle_delete!(@permission_set, delete_permission_set_handler, permissions_service_failure_handler)
        assert_requested stub_get_policy_request, times: 1
      end

      it "correctly handles on permissions service failure" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_raise(Kragle::ServerError)
        delete_permission_set_handler.expects(:call).never
        permissions_service_failure_handler.expects(:call)
        permissions_set_policies_helper.handle_delete!(@permission_set, delete_permission_set_handler, permissions_service_failure_handler)
        assert_requested stub_get_policy_request, times: 1
      end

      it "correctly handles 3XX" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_delete_policy_request = stub_request(:delete, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_return(status: 300)
        delete_permission_set_handler.expects(:call).never
        permissions_service_failure_handler.expects(:call)
        permissions_set_policies_helper.handle_delete!(@permission_set, delete_permission_set_handler, permissions_service_failure_handler)
        assert_requested stub_get_policy_request, times: 1
        assert_requested stub_delete_policy_request, times: 1
      end
    end
  end
end
