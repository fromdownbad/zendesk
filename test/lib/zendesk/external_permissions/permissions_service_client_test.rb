require_relative '../../../support/test_helper'

SingleCov.covered!

describe "Zendesk::Permissions" do
  describe "PermissionsServiceClient" do
    let(:policies_path) { '/api/v2/policies' }
    let(:subdomain) { 'test' }
    let(:user_id) { 10011 }
    let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }
    let(:permissions_service_client) { Zendesk::ExternalPermissions::PermissionsServiceClient.new(subdomain, user_id, client_options) }
    let(:subdomain_policies_url) { "#{permissions_service_client.service_url}#{policies_path}" }
    let(:subdomain_policies_default_url) { "#{subdomain_policies_url}/default" }

    describe 'POST /api/v2/policies' do
      it('should create policy') do
        policy_id = '01EHD5H0YHC81MYDR45Y1R1SYF'
        request_body = { policy: { name: 'test_policy', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] } }
        stub_request = stub_request(:post, subdomain_policies_url).with(
            body: request_body
          ).to_return(
            status: 200,
            body: { policy: { id: policy_id, name: 'test_policy', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] } }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_response = permissions_service_client.create_policy!(request_body)
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['policy']['id'], policy_id
      end

      it 'raises ResponseError' do
        request_body = { policy: { name: 'test_policy'} }
        stub_request = stub_request(:post, subdomain_policies_default_url).to_raise(Kragle::ResponseError)
        assert_raises Kragle::ResponseError do
          permissions_service_client.create_default_policy!(request_body)
        end
        assert_requested stub_request, times: 1
      end

      it 'raises ClientError' do
        request_body = { policy: { name: 'test_policy'} }
        stub_request = stub_request(:post, subdomain_policies_default_url).to_raise(Faraday::ClientError)
        assert_raises Faraday::ClientError do
          permissions_service_client.create_default_policy!(request_body)
        end
        assert_requested stub_request, times: 1
      end
    end

    describe 'POST /api/v2/policies/default' do
      it('should create policy') do
        policy_id = '01EHD5H0UHC81MYDR45Y1R1SYF'
        request_body = { policy: { name: 'test_default_policy', type: 'billing_admin', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] } }
        stub_request = stub_request(:post, subdomain_policies_default_url).with(
            body: request_body
          ).to_return(
            status: 200,
            body: { policy: { id: policy_id, name: 'test_default_policy', type: 'billing_admin', subject: { id: '1', type: 'custom_role' }, statements: [] } }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_response = permissions_service_client.create_default_policy!(request_body)
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['policy']['id'], policy_id
      end

      it 'raises ResponseError' do
        request_body = { policy: { name: 'test_policy'} }
        stub_request = stub_request(:post, subdomain_policies_url).to_raise(Kragle::ResponseError)
        assert_raises Kragle::ResponseError do
          permissions_service_client.create_policy!(request_body)
        end
        assert_requested stub_request, times: 1
      end

      it 'raises ClientError' do
        request_body = { policy: { name: 'test_policy'} }
        stub_request = stub_request(:post, subdomain_policies_url).to_raise(Faraday::ClientError)
        assert_raises Faraday::ClientError do
          permissions_service_client.create_policy!(request_body)
        end
        assert_requested stub_request, times: 1
      end
    end

    describe 'GET /api/v2/policies' do
      it('should get first page (default page size) of policies') do
        stub_request = stub_request(:get, "#{subdomain_policies_url}?page[size]=100").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_response = permissions_service_client.get_policies!
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['policies'][0]['id'], '01EHD5H0YHC81MYDR45Y1R1SYF'
      end

      it('should get requested page size of policies') do
        stub_request = stub_request(:get, "#{subdomain_policies_url}?page[size]=1").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_response = permissions_service_client.get_policies!({ page_size: 1 })
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['policies'][0]['id'], '01EHD5H0YHC81MYDR45Y1R1SYF'
      end

      it('should get requested page size of policies after provided id') do
        stub_request = stub_request(:get, "#{subdomain_policies_url}?page[size]=1&page[after]=01EHD5H0YHC81MYDR45Y1R1SYE").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_response = permissions_service_client.get_policies!({ page_size: 1, page_after: '01EHD5H0YHC81MYDR45Y1R1SYE' })
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['policies'][0]['id'], '01EHD5H0YHC81MYDR45Y1R1SYF'
      end

      it 'raises ResponseError' do
        stub_request = stub_request(:get, "#{subdomain_policies_url}?page[size]=100").to_raise(Kragle::ResponseError)
        assert_raises Kragle::ResponseError do
          permissions_service_client.get_policies!
        end
        assert_requested stub_request, times: 1
      end

      it 'raises ClientError' do
        stub_request = stub_request(:get, "#{subdomain_policies_url}?page[size]=100").to_raise(Faraday::ClientError)
        assert_raises Faraday::ClientError do
          permissions_service_client.get_policies!
        end
        assert_requested stub_request, times: 1
      end

      it('should get a policy by subject id') do
        stub_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=1&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_response = permissions_service_client.get_policies!({ subject_id: 1, subject_type: 'custom_role' })
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['policies'][0]['id'], '01EHD5H0YHC81MYDR45Y1R1SYF'
      end
    end

    describe 'GET /api/v2/policies/{id}' do
      it('should get a policy by provided id') do
        policy_id = '01EHD5H0YHC81MYDR45Y1R1SYF'
        stub_request = stub_request(:get, "#{subdomain_policies_url}/#{policy_id}").to_return(
            status: 200,
            body: { policy: { id: policy_id, name: 'test_policy', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] } }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_response =  permissions_service_client.get_policy!(policy_id)
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['policy']['id'], policy_id
      end

      it 'raises ResponseError' do
        policy_id = '01EHKXSZTGCRW6J01Z6TQBYDNW'
        stub_request = stub_request(:get, "#{subdomain_policies_url}/#{policy_id}").to_raise(Kragle::ResponseError)
        assert_raises Kragle::ResponseError do
          permissions_service_client.get_policy!(policy_id)
        end
        assert_requested stub_request, times: 1
      end

      it 'raises ClientError' do
        policy_id = '01EHKXSZTGCRW6J01Z6TQBYDNW'
        stub_request = stub_request(:get, "#{subdomain_policies_url}/#{policy_id}").to_raise(Faraday::ClientError)
        assert_raises Faraday::ClientError do
          permissions_service_client.get_policy!(policy_id)
        end
        assert_requested stub_request, times: 1
      end
    end

    describe 'PUT /api/v2/policies/{id}' do
      it('should update policy') do
        policy_id = '01EHD5H0YHC81MYDR45Y1R1SYF'
        request_body = { policy: { name: 'test_policy', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['write'] }] } }
        stub_request = stub_request(:put, "#{subdomain_policies_url}/#{policy_id}").with(
            body: request_body
          ).to_return(
            status: 200,
            body: { policy: { id: policy_id, name: 'test_policy', subject: { id: '1', type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['write'] }] } }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_response = permissions_service_client.update_policy!(policy_id, request_body)
        assert_requested stub_request
        assert(stub_response.success?)
        stub_response_body = JSON.parse(stub_response.body.to_json)
        assert_equal stub_response_body['policy']['id'], policy_id
      end

      it 'raises ResponseError' do
        policy_id = '01EHKXSZTGCRW6J01Z6TQBYDNW'
        request_body = { policy: { name: 'test_policy'} }
        stub_request = stub_request(:put, "#{subdomain_policies_url}/#{policy_id}").to_raise(Kragle::ResponseError)
        assert_raises Kragle::ResponseError do
          permissions_service_client.update_policy!(policy_id, request_body)
        end
        assert_requested stub_request, times: 1
      end

      it 'raises ClientError' do
        policy_id = '01EHKXSZTGCRW6J01Z6TQBYDNW'
        request_body = { policy: { name: 'test_policy'} }
        stub_request = stub_request(:put, "#{subdomain_policies_url}/#{policy_id}").to_raise(Faraday::ClientError)
        assert_raises Faraday::ClientError do
          permissions_service_client.update_policy!(policy_id, request_body)
        end
        assert_requested stub_request, times: 1
      end
    end

    describe 'DELETE /api/v2/policies/{id}' do
      it('should delete policy') do
        policy_id = '01EHD5H0YHC81MYDR45Y1R1SYF'
        stub_request = stub_request(:delete, "#{subdomain_policies_url}/#{policy_id}").to_return(status: 204)
        stub_response = permissions_service_client.delete_policy!(policy_id)
        assert_requested stub_request
        assert(stub_response.success?)
      end

      it 'raises ResponseError' do
        policy_id = '01EHKXSZTGCRW6J01Z6TQBYDNW'
        stub_request = stub_request(:delete, "#{subdomain_policies_url}/#{policy_id}").to_raise(Kragle::ResponseError)
        assert_raises Kragle::ResponseError do
          permissions_service_client.delete_policy!(policy_id)
        end
        assert_requested stub_request, times: 1
      end

      it 'raises ClientError' do
        policy_id = '01EHKXSZTGCRW6J01Z6TQBYDNW'
        stub_request = stub_request(:delete, "#{subdomain_policies_url}/#{policy_id}").to_raise(Faraday::ClientError)
        assert_raises Faraday::ClientError do
          permissions_service_client.delete_policy!(policy_id)
        end
        assert_requested stub_request, times: 1
      end
    end
  end
end
