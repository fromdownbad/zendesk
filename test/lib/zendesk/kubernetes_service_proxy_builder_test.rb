require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::KubernetesServiceProxyBuilder do
  let(:consul_url) { ENV['CONSUL_HTTP_ADDR'] || 'localhost:8500' }
  let(:dc_url) { "http://#{consul_url}/v1/kv/global/pod/1/dc?raw" }
  let(:service_name) { 'satisfaction-prediction-app' }

  describe '#service_url' do
    it 'builds the service proxy' do
      stub_request(:get, dc_url).to_return(body: "my-dc")
      actual_url = Zendesk::KubernetesServiceProxyBuilder.service_url(service_name, 1)
      actual_url.must_equal "#{service_name}.pod-1.svc.my-dc.zdsystest.com:4080"
    end
  end

  describe '#datacenter_for_pod' do
    let(:pod_id) { 1 }
    let(:dc_name) { 'use1' }

    it 'fetches dc from consul' do
      stub_request(:get, dc_url).to_return(body: dc_name)

      actual_dc = Zendesk::KubernetesServiceProxyBuilder.datacenter_for_pod(pod_id)
      actual_dc.must_equal dc_name
    end

    it "fails when datacenter fetch fails" do
      stub_request(:get, dc_url).to_raise(Timeout::Error)

      assert_raises(Faraday::TimeoutError) do
        Zendesk::KubernetesServiceProxyBuilder.datacenter_for_pod(pod_id)
      end
    end
  end
end
