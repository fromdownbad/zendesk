require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SatisfactionPredictionApp::InternalApiClient do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:client) { Zendesk::SatisfactionPredictionApp::InternalApiClient.new(account) }

  describe "#fetch_satisfaction_prediction_available" do
    it "makes a GET request to prediction API to check for prediction availability" do
      stub_request(:get, "http://pod-1.satisfaction-prediction-app.service.consul:3000/api/v2/satisfaction_prediction/private/prediction/account/#{account.id}/available").
        to_return(body: JSON.dump(available: true))

      assert client.fetch_satisfaction_prediction_available
    end
  end
end
