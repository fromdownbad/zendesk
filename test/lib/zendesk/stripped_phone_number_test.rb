require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::StrippedPhoneNumber do
  let(:subject) { Zendesk::StrippedPhoneNumber }

  describe '.strip' do
    it 'returns a phone number with formatting stripped' do
      value = '+1 (202) 803-7065'
      assert_equal subject.strip(formatted_number: value), '+12028037065'
    end

    it 'returns a phone number (extension) with formatting stripped' do
      value = '+1 (202) 803 7065 x 123'
      assert_equal subject.strip(formatted_number: value), '+12028037065x123'
    end

    describe 'when given nil' do
      it 'returns the original value' do
        value = nil
        assert_nil subject.strip(formatted_number: value), nil
      end
    end

    describe 'when given a non-string' do
      it 'returns the original value' do
        value = 12028037065
        assert_equal subject.strip(formatted_number: value), 12028037065
      end
    end

    # standard formtted charachters = [-| ( | ) |.|  ]
    describe 'when given a value that contains non-standard formtted charachters' do
      it 'returns the original value' do
        value = '+1 1800-CALL-NOW'
        assert_equal subject.strip(formatted_number: value), '+1 1800-CALL-NOW'
      end
    end
  end
end
