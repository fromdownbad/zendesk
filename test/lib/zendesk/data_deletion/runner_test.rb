require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 16

describe Zendesk::DataDeletion::Runner do
  describe 'setting the environment variable' do
    it 'sets the constant from the environment variable' do
      Zendesk::DataDeletion::Runner::MAX_ACTIVE_DELETIONS.must_equal Integer(ENV['DATA_DELETION_RUNNER_MAX_ACTIVE_DELETIONS'])
    end
  end

  describe "with active audit" do
    let(:audit) do
      DataDeletionAudit.build_for_cancellation.tap do |audit|
        audit.account_id = 1
        audit.save!
        audit.start!
      end
    end
    describe "without kill_switch flipped" do
      before do
        Arturo.disable_feature! :account_deletion_kill_switch
      end

      it "processes active jobs" do
        Zendesk::DataDeletion::AccountDeletionManager.expects(:work).with(audit.id)
        subject.run_once
      end

      it "starts enqueued jobs" do
        Zendesk::DataDeletion::AccountDeletionManager.expects(:work).with(audit.id)
        subject.run_once
      end

      it "rescues errors" do
        Zendesk::DataDeletion::AccountDeletionManager.expects(:work).with(audit.id).raises(StandardError, 'whoops')
        subject.run_once
      end
    end

    describe "with kill_switch flipped" do
      it "doesn't start enqueued jobs" do
        Arturo.enable_feature! :account_deletion_kill_switch
        Zendesk::DataDeletion::AccountDeletionManager.expects(:work).with(audit.id).never
        subject.run_once
      end
    end
  end

  it "limits the number of active managed jobs" do
    (Zendesk::DataDeletion::Runner::MAX_ACTIVE_DELETIONS + 1).times do
      audit = DataDeletionAudit.build_for_cancellation
      audit.account_id = 1
      audit.save!
      audit.enqueue!
    end

    subject.deletions_to_run.size.must_equal Zendesk::DataDeletion::Runner::MAX_ACTIVE_DELETIONS
  end
end
