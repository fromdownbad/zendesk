require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::DataDeletion::AccountDeletionManager do
  let(:job_class) { Zendesk::DataDeletion::AccountDeletionManager }

  before do
    @account = accounts(:minimum)
    ActiveRecord::Base.connection.execute <<-MYSQL
    UPDATE `subscriptions`
    SET
      `manual_discount` = 0,
      `is_trial` = 0,
      `churned_on` = '#{130.days.ago.to_s(:db)}',
      `updated_at` = '#{Time.now.to_s(:db)}'
    WHERE
      `subscriptions`.`id` = #{@account.subscription.id}
    MYSQL
    @account.update_column(:deleted_at, 40.days.ago)

    @audit = @account.data_deletion_audits.build_for_cancellation
    @audit.created_at = 40.days.ago
    @audit.started_at = Time.now
    @audit.save!

    Zendesk::Maintenance::Jobs::BaseDataDeleteJob.stubs(:enqueue_in).returns(true)
  end

  describe '.stages' do
    # Adding stages requires you to add ordered tests below
    #
    it 'should define stages' do
      assert_equal [:independent, :before_db, :db, :after_db], job_class.stages
    end
  end

  describe '.work' do
    it 'should not work on accounts with arturo feature' do
      Account.any_instance.stubs(:has_prevent_deletion_if_churned?).returns(true)
      @audit.start!

      error = -> { job_class.work(@audit.id) }.must_raise(Zendesk::DataDeletion::PermanentError)
      error.message.must_equal "Account deletion prevented by arturo feature: prevent_deletion_if_churned"
    end

    it 'should work on started audits' do
      @audit.start!

      job_class.work(@audit.id)
    end

    it 'should work on enqueued audits' do
      @audit.enqueue!
      job_class.work(@audit.id)
      @audit.job_audits.count.must_equal 6
      @audit.job_audits.first.status.must_equal 'enqueued'
    end

    it 'should not work on finished audits' do
      @audit.finish!
      error = -> { job_class.work(@audit.id) }.must_raise(Zendesk::DataDeletion::PermanentError)
      error.message.must_equal "Audit status must be 'enqueued' or 'started' (is \"finished\")"
    end

    it 'should not work on failed audits' do
      @audit.fail!(StandardError.new('test'))
      error = -> { job_class.work(@audit.id) }.must_raise(Zendesk::DataDeletion::PermanentError)
      error.message.must_equal "Audit status must be 'enqueued' or 'started' (is \"failed\")"
    end

    describe 'for temporary errors' do
      it 'raises but retains state so it will be retried' do
        @audit.start!
        DataDeletionAudit.stubs(:find).raises(Timeout::Error)
        -> { job_class.work(@audit.id) }.must_raise(Timeout::Error)
        DataDeletionAudit.unstub(:find)
        @audit.reload
        @audit.status.must_equal 'started'
      end
    end

    describe 'for moved' do
      before { DataDeletionAudit.any_instance.stubs(:reason).returns('moved') }

      it 'raises when the account is being cleaned from its current shard' do
        DataDeletionAudit.any_instance.stubs(:shard_id).returns(ActiveRecord::Base.current_shard_id)
        Account.any_instance.stubs(:shard_id).returns(ActiveRecord::Base.current_shard_id)
        error = -> { job_class.work(@audit.id) }.must_raise(Zendesk::DataDeletion::PermanentError)
        error.message.must_equal "Account is currently active on shard 1, can't erase"
      end

      it 'raises when no shard_id is present on the audit' do
        DataDeletionAudit.any_instance.stubs(:shard_id).returns(nil)
        error = -> { job_class.work(@audit.id) }.must_raise(Zendesk::DataDeletion::InvalidAudit)
        error.message.must_equal "No shard_id present on an account move deletion"
      end
    end

    it 'should not work on restored accounts' do
      @account.deleted_at = nil
      @account.save!
      error = -> { job_class.work(@audit.id) }.must_raise(Zendesk::DataDeletion::PermanentError)
      error.message.must_equal "Cannot remove an account unless it has been soft deleted"
    end

    describe 'on enqueued audits' do
      before do
        @audit.enqueue!
      end

      describe 'when all jobs have completed' do
        before do
          job_class.stubs(:job_complete?).returns(true)
        end

        it 'should finish the audit if all jobs have completed' do
          job_class.work(@audit.id)

          assert_equal 'finished', @audit.reload.status
        end

        it 'should log the audit duration' do
          Zendesk::StatsD::Client.any_instance.expects(:distribution).with('data_deletion_audit_duration', anything, anything).once
          job_class.work(@audit.id)
        end
      end

      describe 'on started audits' do
        before do
          job_class.expects(:enqueue_stage).with(@audit, :independent).once
        end

        it 'should change status to started' do
          job_class.work(@audit.id)

          assert_equal 'started', @audit.reload.status
        end

        it 'should enqueue jobs for independent stage first' do
          job_class.expects(:stage_complete?).with(@audit, :independent).returns(false)

          job_class.work(@audit.id)
        end

        describe 'after independent stage completes' do
          before do
            job_class.expects(:stage_complete?).with(@audit, :independent).returns(true)
            job_class.expects(:enqueue_stage).with(@audit, :before_db).once
          end

          it 'should enqueue jobs for before_db stage after independent stage' do
            job_class.expects(:stage_complete?).with(@audit, :before_db).returns(false)

            job_class.work(@audit.id)
          end

          describe 'after before_db stage completes' do
            before do
              job_class.expects(:stage_complete?).with(@audit, :before_db).returns(true)
              job_class.expects(:enqueue_stage).with(@audit, :db).once
            end

            it 'should enqueue jobs for db stage after before_db stage' do
              job_class.expects(:stage_complete?).with(@audit, :db).returns(false)

              job_class.work(@audit.id)
            end
          end
        end
      end
    end
  end

  describe '.job_klasses' do
    subject { job_class.job_klasses }

    it 'should exclude the base participant job' do
      assert_not_includes subject, Zendesk::Maintenance::Jobs::BaseDataDeletionParticipantJob
    end

    it 'should not include jobs that do not belong to a stage' do
      Zendesk::Maintenance::Jobs::FaultyJob = Class.new(Zendesk::Maintenance::Jobs::BaseDataDeleteJob)
      assert_not_includes subject, Zendesk::Maintenance::Jobs::FaultyJob
    end

    it 'should only include jobs with valid stages' do
      assert(subject.all? { |jk| [:independent, :before_db, :db, :after_db].include?(jk.stage) })
    end
  end

  describe 'jobs' do
    it 'should return independent job classes' do
      independent_jobs = job_class.job_klasses_for_stage(:independent)

      assert_equal 6, independent_jobs.size
      assert_includes independent_jobs, Zendesk::Maintenance::Jobs::RemoveCustomResourcesJob
      assert_includes independent_jobs, Zendesk::Maintenance::Jobs::RemoveHCAttachmentsJob
      assert_includes independent_jobs, Zendesk::Maintenance::Jobs::RemoveReferenceParticipantDataJob
      assert_includes independent_jobs, Zendesk::Maintenance::Jobs::RemoveS3InboundEmailJob
      assert_includes independent_jobs, Zendesk::Maintenance::Jobs::RemoveSunshineEventsProfilesJob
      assert_includes independent_jobs, Zendesk::Maintenance::Jobs::RemoveSellDataJob
    end

    it 'should return before_db job classes' do
      before_db_jobs = job_class.job_klasses_for_stage(:before_db)

      assert_equal 12, before_db_jobs.size
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RevokeExternalEmailCredentialsJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemoveAttachmentsFromCloudJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemoveRiakDataJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemoveGooddataIntegrationJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemoveExportArtefactsJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemoveLegionDataJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemovePigeonDataJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemoveVoiceDataJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemoveCollaborationDataJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemoveExploreDataJob
      assert_includes before_db_jobs, Zendesk::Maintenance::Jobs::RemoveEmbeddingsAfterDeleteJob
    end

    it 'should return db job classes' do
      db_jobs = job_class.job_klasses_for_stage(:db)

      assert_equal 2, db_jobs.size
      assert_includes db_jobs, Zendesk::Maintenance::Jobs::RemoveShardDataJob
      assert_includes db_jobs, Zendesk::Maintenance::Jobs::RemoveCertificatesJob
    end
  end

  describe '.enqueue_stage' do
    before { job_class.stubs(:job_klasses_for_stage).with(:db).returns([Zendesk::Maintenance::Jobs::RemoveShardDataJob]) }

    describe 'when no jobs have been enqueued' do
      it 'should enqueue a job if there are no active jobs' do
        Zendesk::Maintenance::Jobs::RemoveShardDataJob.expects(:enqueue_in).once
        job_class.enqueue_stage(@audit, :db)
        @audit.job_audits.where(job: 'Zendesk::Maintenance::Jobs::RemoveShardDataJob').count.must_equal 1
      end

      it 'should not log job duration' do
        job_class.expects(:record_job_duration).never
        job_class.enqueue_stage(@audit, :db)
      end
    end

    describe 'when there is a running job' do
      before do
        @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
      end

      it 'should not enqueue a job' do
        Zendesk::Maintenance::Jobs::RemoveShardDataJob.expects(:enqueue_in).never
        job_class.enqueue_stage(@audit, :db)
        @audit.job_audits.where(job: 'Zendesk::Maintenance::Jobs::RemoveShardDataJob').count.must_equal 1
      end

      it 'should log job duration' do
        Zendesk::StatsD::Client.any_instance.expects(:distribution).with('jobs.job_duration', anything, anything).once
        job_class.enqueue_stage(@audit, :db)
      end
    end

    describe 'when all job audits are finished' do
      it 'should not log job duration' do
        job = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
        job.finish!
        job_class.expects(:record_job_duration).never
        job_class.enqueue_stage(@audit, :db)
      end
    end

    it 'should re-enqueue the active job if it job has timed out' do
      job = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
      job.start!
      job.update_attribute(:heartbeat_at, 10.minutes.ago)
      Zendesk::Maintenance::Jobs::RemoveShardDataJob.expects(:enqueue_in).once
      job_class.enqueue_stage(@audit, :db)
      @audit.job_audits.where(job: 'Zendesk::Maintenance::Jobs::RemoveShardDataJob').count.must_equal 1
    end

    describe 'when all job attempts have failed' do
      it 'should create and enqueue a replacement job with a delay if attempts have all failed' do
        job = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
        job.fail!(StandardError.new('test'))
        Zendesk::Maintenance::Jobs::RemoveShardDataJob.expects(:enqueue_in).once
        job_class.enqueue_stage(@audit, :db)
        @audit.job_audits.where(job: 'Zendesk::Maintenance::Jobs::RemoveShardDataJob').count.must_equal 2
      end

      it 'should only leave 2 jobs in the db, one active and the latest failure' do
        Timecop.freeze(Time.now - 2.hours) do
          @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap do |job_audit|
            job_audit.status = 'failed'
            job_audit.save!
          end
        end

        Timecop.freeze(Time.now - 1.hour) do
          @second_fail_job_audit = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap do |job_audit|
            job_audit.status = 'failed'
            job_audit.save!
          end
        end

        Zendesk::Maintenance::Jobs::RemoveShardDataJob.expects(:enqueue_in).once
        job_class.enqueue_stage(@audit, :db)

        job_audits = @audit.job_audits.where(job: 'Zendesk::Maintenance::Jobs::RemoveShardDataJob')
        job_audits.count.must_equal 2
        assert_equal job_audits.first, @second_fail_job_audit
        assert job_audits.second.enqueued?
      end

      it 'should not call cleanup when only 1 failed job' do
        @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap do |job_audit|
          job_audit.status = 'failed'
          job_audit.save!
        end

        Zendesk::Maintenance::Jobs::RemoveShardDataJob.expects(:enqueue_in).once
        DataDeletionAuditJob.expects(:delete_failed_audit_jobs).never
        job_class.enqueue_stage(@audit, :db)
      end
    end
  end

  describe '.stage_complete?' do
    it 'should return true if all jobs in a stage are complete' do
      job_class.stubs(:job_complete?).returns(true)

      job_class.stage_complete?(@audit, :before_db)
    end

    it 'should return false if any jobs in a stage are not complete' do
      job_class.stubs(:job_klasses).returns([Zendesk::Maintenance::Jobs::RemoveRiakDataJob, Zendesk::Maintenance::Jobs::RemoveAttachmentsFromCloudJob])
      job_class.stubs(:job_complete?).with(@audit, Zendesk::Maintenance::Jobs::RemoveRiakDataJob).returns(true)
      job_class.stubs(:job_complete?).with(@audit, Zendesk::Maintenance::Jobs::RemoveAttachmentsFromCloudJob).returns(false)

      job_class.stage_complete?(@audit, :before_db)
    end
  end

  describe 'moved accounts' do
    it 'sets failure if deleting form current shard' do
      audit = @account.data_deletion_audits.new
      audit.reason = 'moved'
      audit.shard_id = @account.shard_id
      audit.save!
      error = -> { job_class.work(audit.id) }.must_raise(Zendesk::DataDeletion::PermanentError)
      error.message.must_equal "Account is currently active on shard 1, can't erase"
      audit.reload
      audit.status.must_equal 'failed'
    end
  end

  describe '.job_complete?' do
    before do
      job = DataDeletionAuditJob.new(
        account_id: @account.id,
        shard_id: @account.shard_id,
        job: Zendesk::Maintenance::Jobs::RemoveRiakDataJob.name
      )
      @audit.job_audits << job
      job.fail!(StandardError.new('test'))
    end

    it 'should return true if any attempt finishes' do
      job = DataDeletionAuditJob.new(
        account_id: @account.id,
        shard_id: @account.shard_id,
        job: Zendesk::Maintenance::Jobs::RemoveRiakDataJob.name
      )
      @audit.job_audits << job
      job.finish!

      assert job_class.job_complete?(@audit, Zendesk::Maintenance::Jobs::RemoveRiakDataJob)
    end

    it 'should return false if all attempts have not finished' do
      job = DataDeletionAuditJob.new(
        account_id: @account.id,
        shard_id: @account.shard_id,
        job: Zendesk::Maintenance::Jobs::RemoveRiakDataJob.name
      )
      @audit.job_audits << job
      job.start!

      refute job_class.job_complete?(@audit, Zendesk::Maintenance::Jobs::RemoveRiakDataJob)
    end
  end

  describe '.record_job_duration' do
    subject { job_class.record_job_duration(job_audits) }

    before do
      Timecop.freeze
    end

    let(:job_audit_class) { job_class.job_klasses_for_stage(:independent).first }
    let(:job_duration) { 1.hour }
    let(:job_audits) do
      @audit.update_attribute(:started_at, Time.now - job_duration)
      job_audit = @audit.build_job_audit_for_klass(job_audit_class)
      job_audit.fail!(Exception.new('some_exception'))
      @audit.attempts_by_job(job_audit_class)
    end

    after do
      Timecop.return
    end

    it 'should record the job duration to statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:distribution).with('jobs.job_duration', job_duration.to_i, tags: ["audit_id:#{@audit.id}", "job:#{job_audits.first.job.demodulize}"])
      subject
    end
  end

  describe '.record_audit_duration' do
    subject { job_class.record_audit_duration(@audit) }

    describe 'when audit is still running' do
      it 'does not log the duration' do
        Zendesk::StatsD::Client.any_instance.expects(:distribution).with('data_deletion_audit_duration', anything, anything).never
        subject
      end
    end

    describe 'when audit is finished' do
      let(:audit_duration) { rand(1..40).days }

      before do
        @audit.start!
        Timecop.travel(@audit.started_at + audit_duration) do
          @audit.finish!
        end
        Zendesk::StatsD::Client.any_instance.expects(:distribution).with('data_deletion_audit_duration', audit_duration.to_i, tags: ["audit_id:#{@audit.id}", "sla_tag:#{sla_tag}"]).once
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('data_deletion_audit_duration_inc', tags: ["audit_id:#{@audit.id}", "sla_tag:#{sla_tag}", "slo_tag:#{slo_tag}"]).once
      end

      describe 'when within expected time' do
        let(:audit_duration) { 6.days.to_i }
        let(:sla_tag) { :within_expected }
        let(:slo_tag) { :normal }

        it 'logs duration with within expected tag' do
          subject
        end
      end

      describe 'when within sla' do
        let(:audit_duration) { job_class::DELETION_SLA.to_i }
        let(:sla_tag) { :within_sla }
        let(:slo_tag) { :warning }

        it 'logs duration with within sla tag' do
          subject
        end
      end

      describe 'when outside sla' do
        let(:audit_duration) { (job_class::DELETION_SLA + 1.second).to_i }
        let(:sla_tag) { :outside_sla }
        let(:slo_tag) { :critical }

        it 'logs duration with outside sla tag' do
          subject
        end
      end
    end
  end
end
