require_relative "../../../support/job_helper"

SingleCov.covered!

describe Zendesk::DataDeletion::AccountDeletionKillSwitch do
  describe "#kill_switch_flipped?" do
    let(:obj_to_test) { Class.new { include Zendesk::DataDeletion::AccountDeletionKillSwitch }.new }

    it "feature active" do
      Arturo.enable_feature! :account_deletion_kill_switch
      assert obj_to_test.kill_switch_flipped?
    end

    it "feature set to off" do
      Arturo.disable_feature! :account_deletion_kill_switch
      refute obj_to_test.kill_switch_flipped?
    end

    it "checks per pod" do
      Arturo::Feature.create!(
        symbol: :account_deletion_kill_switch,
        phase: 'external_beta',
        external_beta_subdomains: '+pod1'
      )
      assert obj_to_test.kill_switch_flipped?
    end
  end
end
