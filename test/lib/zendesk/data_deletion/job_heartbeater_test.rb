require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Zendesk::DataDeletion::JobHeartbeater do
  let(:subject) { Zendesk::DataDeletion::JobHeartbeater.new(job_audit, initial_heartbeat) }
  let(:job_audit) { DataDeletionAuditJob.create! job: 'RemoveFooJob', account_id: 1, heartbeat_at: initial_heartbeat }
  let(:initial_heartbeat) { 1.minute.ago }

  before { subject.interval = 0.01 }

  describe '#with_poll' do
    it 'heartbeats the job' do
      subject.poll
      job_audit.reload
      job_audit.heartbeat_at.must_be :>, initial_heartbeat
    end

    it 'heartbeats in the background for the requested interval' do
      # Thread timing is not deterministic. Using a 2x margin of variation.
      # Locally, this averaged 88 with a stddev of 1.4
      job_audit.expects(:optimistic_heartbeat!).times((50..150))
      Zendesk::StatsD::Client.any_instance.expects(:gauge).with('data_deletion_heartbeat', anything, tags: ["job:#{job_audit.job}"]).times((50..150))
      subject.with_poll do
        sleep 1 # rubocop:disable Lint/Sleep
      end
    end

    # Classic uses transactional_fixtures, which causes MySQL to deadlock if
    # two separate connections with two separate transactions try heartbeating
    # the same record. This is hacked using stubs, sorry.
    it 'aborts if another thread heartbeats' do
      # Initial value is for the first poll in the main thread
      job_audit.expects(:optimistic_heartbeat!).with(any_parameters).returns(Time.now).then.raises(DataDeletionAuditJob::JobCollision).at_least_once
      Zendesk::DataDeletion::JobHeartbeater.expects(:exit_now!).at_least_once

      subject.with_poll do
        sleep 0.1 # rubocop:disable Lint/Sleep
      end
    end

    it 'starts a thread and shuts it down before returning' do
      base_thread_count = Thread.list.length
      subject.with_poll do
        Thread.list.length.must_equal base_thread_count + 1
      end
      Thread.list.length.must_equal base_thread_count
    end
  end
end
