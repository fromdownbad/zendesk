require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::DataDeletion::ParticipantClient do
  it 'returns a Faraday connection with the default adapter' do
    connection = Zendesk::DataDeletion::ParticipantClient.new('test', shared_cache: false)
    assert_instance_of(Faraday::Connection, connection)
    assert_includes(connection.builder.handlers, Faraday::Adapter::NetHttp)
  end

  it 'returns a Faraday connection with the specified adapter' do
    connection = Zendesk::DataDeletion::ParticipantClient.new('test', shared_cache: false) do |kragle|
      kragle.adapter :typhoeus
    end

    assert_instance_of(Faraday::Connection, connection)
    assert_includes(connection.builder.handlers, Faraday::Adapter::Typhoeus)
    refute_includes(connection.builder.handlers, Faraday::Adapter::NetHttp)
  end

  describe 'when testing account data deletion system user' do
    def mac_token(token)
      /MAC id="#{token}", ts="[0-9]{10}", nonce="[a-z0-9]+", mac=".+"/
    end

    subject do
      service.get('/hello') do |env|
        [200, headers, JSON.dump('auth' => env.request_headers.fetch('Authorization'))]
      end
    end

    let(:token) { Zendesk::Configuration.dig :system_user_auth, :account_data_deletion, :oauth }
    let(:service) { Faraday::Adapter::Test::Stubs.new }
    let(:headers) { { 'Content-Type' => 'application/json' } }
    let(:connection) do
      Zendesk::DataDeletion::ParticipantClient.client(:test_service, retry_options: { max: 3 }) do |faraday|
        faraday.adapter :test, service
      end
    end

    it 'sets the Authorization header to a signed token with ADD system user' do
      subject

      response = connection.get('/hello')
      assert_match(mac_token(token), response.body.fetch('auth'))
    end

    it 'does not change Kragle oauth secret' do
      subject

      oauth_secret = Zendesk::Configuration.dig :system_user_auth, :zendesk, :oauth
      assert_equal Kragle.oauth_secret, oauth_secret
    end
  end
end
