require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'AddonService' do
  let(:account) { accounts(:minimum) }

  describe '#execute!' do
    describe 'when no add-ons are present in zuora or subscription_feature_addons table' do
      let(:addon_service) do
        Zendesk::Features::AddonService.new(account, {})
      end

      it 'does not remove, update, or add new add-ons' do
        addon_service.expects(:remove_addons).never
        addon_service.expects(:update_addons).never
        addon_service.expects(:add_new_addons).never
        assert_nil addon_service.execute!
      end
    end

    describe 'when removing an add-on' do
      let(:addon_service) do
        Zendesk::Features::AddonService.new(account, {})
      end

      describe 'when the account previously purchased the add-on' do
        before do
          account.subscription_feature_addons.create(
            name: 'light_agents',
            zuora_rate_plan_id: 'rpid_1'
          )
        end

        it 'removes the add-on from the subscription_feature_addons table' do
          assert_equal 1, SubscriptionFeatureAddon.where(account_id: account.id).count(:all)
          addon_service.execute!
          assert_equal 0, SubscriptionFeatureAddon.where(account_id: account.id).count(:all)
        end
      end

      describe 'when the account has a boosted add-on on it' do
        let(:boost) do
          SubscriptionFeatureAddon.where(account_id: account.id, name: 'light_agents').first
        end

        before do
          account.subscription_feature_addons.create(
            name: 'light_agents',
            boost_expires_at: 10.days.from_now
          )
        end

        it 'should NOT remove the boosted add-ons' do
          addon_service.execute!
          assert_equal boost, SubscriptionFeatureAddon.where(account_id: account.id).first
        end
      end
    end

    describe 'when updating an add-on' do
      let(:addon_service) do
        Zendesk::Features::AddonService.new(
          account,
          'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' }
        )
      end

      describe 'when the account already has the add-on' do
        before do
          account.subscription_feature_addons.create(
            name: 'light_agents',
            zuora_rate_plan_id: 'rate_plan_id'
          )
        end

        it 'updates the updated_at column and keeps the add-on' do
          SubscriptionFeatureAddon.any_instance.expects(:update_attributes).with(
            zuora_rate_plan_id: 'rate_plan_id',
            boost_expires_at: nil,
            quantity: nil,
            expires_at: nil,
            starts_at: nil
          )
          addon_service.execute!
        end
      end

      describe 'purchased add-on should replace boosted add-on of same name' do
        let(:actual) do
          SubscriptionFeatureAddon.where(account_id: account.id, name: 'light_agents').first
        end

        before do
          account.subscription_feature_addons.create(
            name: 'light_agents',
            boost_expires_at: 10.days.from_now
          )
        end

        it 'should replace the boosted add-on with a purchased add-on' do
          addon_service.execute!
          assert_nil actual.boost_expires_at
          assert_equal 'rate_plan_id', actual.zuora_rate_plan_id
        end
      end

      describe 'multiple temporary zendesk agents addons' do
        let(:two_month_expiration) { 2.months.from_now }
        let(:three_month_expiration) { 3.months.from_now }
        let(:start_date) { Time.zone.now }
        let(:addon_service) do
          Zendesk::Features::AddonService.new(account, 'temporary_zendesk_agents' =>
            [
              {
                zuora_rate_plan_id: 'rpid_1',
                quantity: 10,
                expires_at: three_month_expiration,
                starts_at: start_date
              },
              {
                zuora_rate_plan_id: 'rpid_2',
                quantity: 40,
                expires_at: two_month_expiration,
                starts_at: start_date
              }
            ])
        end

        before do
          account.subscription_feature_addons.create(
            [{
              name: 'temporary_zendesk_agents',
              zuora_rate_plan_id: 'rpid_1',
              quantity: 10,
              expires_at: three_month_expiration,
              starts_at: start_date
            },
             {
               name: 'temporary_zendesk_agents',
               zuora_rate_plan_id: 'rpid_2',
               quantity: 30,
               expires_at: 1.month.from_now,
               starts_at: start_date
             }]
          )
        end

        it 'should update the correct addon' do
          addon_service.execute!
          updated = SubscriptionFeatureAddon.where(zuora_rate_plan_id: 'rpid_2').first
          assert_equal 40, updated.quantity
          assert_equal two_month_expiration.to_s, updated.expires_at.to_s
        end

        it 'should NOT update the wrong addon' do
          addon_service.execute!
          unchanged = SubscriptionFeatureAddon.where(zuora_rate_plan_id: 'rpid_1').first
          assert_equal 10, unchanged.quantity
          assert_equal three_month_expiration.to_s, unchanged.expires_at.to_s
        end
      end
    end

    describe 'when new add-on purchase' do
      let(:actual) do
        SubscriptionFeatureAddon.where(account_id: account.id, name: 'light_agents').first
      end
      let(:addon_service) do
        Zendesk::Features::AddonService.new(
          account,
          'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' }
        )
      end

      it 'should create a subscription_feature_addons record' do
        assert_equal 0, SubscriptionFeatureAddon.where(account_id: account.id).count(:all)
        addon_service.execute!
        assert_equal 'light_agents', actual.name
        assert_equal 'rate_plan_id', actual.zuora_rate_plan_id
        assert_equal 1, SubscriptionFeatureAddon.where(account_id: account.id).count(:all)
      end

      describe 'multiple temporary zendesk agent addons' do
        let(:addon_service) do
          Zendesk::Features::AddonService.new(
            account,
            'temporary_zendesk_agents' =>
              [
                {
                  zuora_rate_plan_id: 'rp_id1',
                  quantity: 10,
                  expires_at: 3.months.from_now,
                  starts_at: Time.zone.now
                },
                {
                  zuora_rate_plan_id: 'rp_id2',
                  quantity: 30,
                  expires_at: 1.month.from_now,
                  starts_at: Time.zone.now
                }
              ]
          )
        end

        it 'should create all instances' do
          assert_equal 0, SubscriptionFeatureAddon.all.count
          addon_service.execute!
          assert_equal 2, SubscriptionFeatureAddon.all.count
        end
      end

      describe 'temporary zendesk agents active now' do
        let(:addon_service) do
          Zendesk::Features::AddonService.new(
            account,
            'temporary_zendesk_agents' =>
              [{
                zuora_rate_plan_id: 'rp_id',
                quantity: 10,
                expires_at: 3.months.from_now,
                starts_at: Time.zone.now
              }]
          )
        end

        it 'should set status to SubscriptionFeatureAddon::STATUS[:started]' do
          addon_service.execute!
          assert_equal SubscriptionFeatureAddon::STATUS[:started], account.subscription_feature_addons.first.status_id
        end
      end

      describe 'temporary zendesk agents active in the future' do
        let(:addon_service) do
          Zendesk::Features::AddonService.new(
            account,
            'temporary_zendesk_agents' =>
              [{
                zuora_rate_plan_id: 'rp_id',
                quantity: 10,
                expires_at: 6.months.from_now,
                starts_at: 3.months.from_now
              }]
          )
        end

        it 'should not set status' do
          addon_service.execute!
          assert_nil account.subscription_feature_addons.first.status
        end
      end
    end

    describe 'combo action: new, update, remove' do
      let(:light_agent_actual) do
        SubscriptionFeatureAddon.where(account_id: account.id, name: 'light_agents').first
      end
      let(:high_volume_api_actual) do
        SubscriptionFeatureAddon.where(account_id: account.id, name: 'high_volume_api').first
      end
      let(:addon_service) do
        Zendesk::Features::AddonService.new(
          account,
          'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' },
          'high_volume_api' => { zuora_rate_plan_id: 'rate_plan_id2' }
        )
      end

      before do
        account.subscription_feature_addons.create(
          [
            {
              name: 'high_volume_api',
              boost_expires_at: 10.days.from_now
            },
            {
              name: 'nps',
              zuora_rate_plan_id: 'nps_rate_plan_id'
            }
          ]
        )
        addon_service.execute!
      end

      it 'should add light_agent add-on to subscription_feature_addons' do
        assert_equal 'light_agents', light_agent_actual.name
        assert_equal 'rate_plan_id', light_agent_actual.zuora_rate_plan_id
      end

      it 'should remove nps add-on from subscription_feature_addons' do
        assert_nil SubscriptionFeatureAddon.where(account_id: account.id, name: 'nps').first
      end

      it 'should update high_volume_api from boost to purchased' do
        assert_equal 'high_volume_api', high_volume_api_actual.name
        assert_equal 'rate_plan_id2', high_volume_api_actual.zuora_rate_plan_id
        assert_nil high_volume_api_actual.boost_expires_at
      end
    end
  end

  describe '#proposed_addons' do
    let(:addon_service) do
      Zendesk::Features::AddonService.new(account, {})
    end

    describe 'when no add-ons' do
      it 'returns an empty array' do
        assert_empty(addon_service.proposed_addons)
      end
    end

    describe 'when add-on from zuora' do
      let(:addon_service) do
        Zendesk::Features::AddonService.new(account, 'light_agents' => { zuora_rate_plan_id: 'rpid_1' })
      end
      it 'returns an array with the zuora add-on name' do
        assert_equal [:light_agents], addon_service.proposed_addons
      end
    end

    describe 'when boosting add-ons' do
      before do
        account.subscription_feature_addons.create(
          [
            {
              name: 'nps',
              boost_expires_at: 10.days.from_now
            },
            {
              name: 'light_agents',
              zuora_rate_plan_id: 'rate_plan_id'
            }
          ]
        )
        account.subscription_feature_addons.reload
      end

      it 'returns all addons by name' do
        assert_equal [:light_agents, :nps], addon_service.proposed_addons.sort
      end
    end

    describe 'when zuora and boosted add-ons' do
      before do
        account.subscription_feature_addons.create(
          name: 'nps',
          boost_expires_at: 10.days.from_now
        )
      end

      describe 'when two distinct add-ons' do
        let(:addon_service) do
          Zendesk::Features::AddonService.new(account, 'light_agents' => { zuora_rate_plan_id: 'rpid_1' })
        end

        it 'returns boosted and zuora add-ons' do
          assert_equal [:light_agents, :nps], addon_service.proposed_addons
        end
      end

      describe 'when zuora replaces an existing boosted add-on' do
        let(:addon_service) do
          Zendesk::Features::AddonService.new(account, 'nps' => { zuora_rate_plan_id: 'rpid_1' })
        end

        it 'returns boosted and zuora add-ons' do
          assert_equal [:nps], addon_service.proposed_addons
        end
      end
    end
  end
end
