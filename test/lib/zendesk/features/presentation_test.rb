require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Features::Presentation do
  include Zendesk::Features::Presentation

  describe '#readable_name' do
    describe 'when name is not in READABLE_TRANSLATIONS' do
      describe 'when name is nil' do
        subject { readable_name('') }

        it 'is empty string' do
          assert_equal '', subject
        end
      end

      describe 'when name is string' do
        subject { readable_name('advanced_security') }

        it 'returns humanized name' do
          assert_equal 'Advanced security', subject
        end
      end

      describe 'when name is symbol' do
        subject { readable_name(:advanced_security) }

        it 'returns humanized name' do
          assert_equal 'Advanced security', subject
        end
      end
    end

    describe 'when name is in READABLE_TRANSLATIONS' do
      describe 'when name is nps' do
        subject { readable_name('nps') }

        it 'is "NPS"' do
          assert_equal 'NPS', subject
        end
      end

      describe 'when name is high_volume_api' do
        subject { readable_name('high_volume_api') }

        it 'is "high_volume_api"' do
          assert_equal 'High volume API', subject
        end
      end
    end
  end
end
