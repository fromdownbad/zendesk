require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Features::CatalogService do
  let(:account)               { accounts(:minimum) }
  let(:legacy_catalog)        { Zendesk::Features::Catalogs::LegacyCatalog }
  let(:catalog)               { Zendesk::Features::Catalogs::Catalog }
  let(:patagonia_ga_datetime) { Zendesk::Features::CatalogService::PATAGONIA_GA_DATETIME }

  describe '.new' do
    subject { Zendesk::Features::CatalogService.new(account) }

    describe 'and the subscription is in trial but NOT under patagonia' do
      it 'returns the legacy catalog' do
        assert_equal legacy_catalog, subject.current_catalog
      end
    end

    describe 'and the subscription is in trial WITH patagonia' do
      before do
        account.subscription.update_column(:pricing_model_revision,
          ZBC::Zendesk::PricingModelRevision::PATAGONIA)
      end

      it 'returns the latest catalog' do
        assert_equal catalog, subject.current_catalog
      end
    end

    describe 'with an existing Zuora subscription' do
      let(:pre_patagonia_pricing_model) do
        ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE
      end

      let(:post_patagonia_pricing_model) do
        ZBC::Zendesk::PricingModelRevision::PATAGONIA
      end

      describe 'with a legacy pricing model' do
        let(:zuora_subscription) do
          stub(
            pricing_model_revision: pre_patagonia_pricing_model,
            approval_required:      false
          )
        end

        before do
          account.subscription.stubs(:zuora_subscription).
            returns(zuora_subscription)
        end

        it 'returns the legacy catalog' do
          assert_equal legacy_catalog, subject.current_catalog
        end

        describe 'and a plan-level boost' do
          before do
            account.on_shard do
              account.create_feature_boost!(
                valid_until: 2.days.from_now,
                boost_level:  5
              )
            end
          end

          it 'returns the latest catalog' do
            assert_equal catalog, subject.current_catalog
          end
        end
      end

      describe 'with the latest pricing model' do
        let(:zuora_subscription) do
          stub(
            pricing_model_revision: post_patagonia_pricing_model,
            approval_required:      false
          )
        end

        before do
          account.subscription.expects(:zuora_subscription).
            returns(zuora_subscription)
        end

        it 'returns the latest catalog' do
          assert_equal catalog, subject.current_catalog
        end
      end

      describe 'in pending approval due to change in pricing model' do
        let(:zuora_subscription) do
          stub(
            pricing_model_revision: post_patagonia_pricing_model,
            approval_required:      true
          )
        end

        before do
          account.subscription.expects(:zuora_subscription).
            returns(zuora_subscription)
        end

        it 'returns the legacy catalog' do
          assert_equal legacy_catalog, subject.current_catalog
        end
      end
    end

    describe 'with an existing boost' do
      before do
        account.create_feature_boost(
          valid_until: 15.days.from_now,
          boost_level: 4
        )
      end

      describe 'before patagonia GA datetime' do
        before do
          account.feature_boost.stubs(updated_at: patagonia_ga_datetime - 1.day)
        end

        it 'should use the legacy catalog' do
          assert_equal legacy_catalog, subject.current_catalog
        end
      end

      describe 'after patagonia GA datetime' do
        it 'should use the patagonia catalog' do
          assert account.feature_boost.updated_at > patagonia_ga_datetime
          assert_equal catalog, subject.current_catalog
        end
      end
    end
  end
end
