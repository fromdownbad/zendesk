require_relative '../../../../support/test_helper'
require_relative '../../../../support/features_helpers'

SingleCov.covered!

describe Zendesk::Features::Catalogs::LegacyCatalog do
  include FeaturesHelpers

  let(:extra_large_features) do
    [
      :agent_collision,
      :agent_display_names,
      :api_limit_700_rpm_legacy,
      :apps_private,
      :apps_public,
      :audit_log,
      :bcc_archiving,
      :business_hours,
      :categorized_forums,
      :chat,
      :cms,
      :collision_chat,
      :community,
      :community_forums,
      :conditional_fields_app,
      :credit_card_sanitization,
      :crm_integrations,
      :cti_integrations,
      :custom_dkim_domain,
      :custom_security_policy,
      :custom_session_timeout,
      :customer_satisfaction,
      :customizable_help_center_themes,
      :dc_in_templates,
      :extended_ticket_metrics,
      :forum_statistics,
      :gooddata_advanced_analytics,
      :gooddata_hourly_synchronization,
      :group_rules,
      :groups,
      :hc_manage_requests,
      :hc_user_profiles,
      :help_center_analytics,
      :help_center_article_labels,
      :help_center_configurable_spam_filter,
      :help_center_content_moderation,
      :help_center_google_analytics,
      :help_center_unlimited_categories,
      :host_mapping,
      :hosted_ssl,
      :hourly_incremental_export,
      :incremental_export,
      :individual_language_selection,
      :individual_time_zone_selection,
      :internal_help_center,
      :ip_restrictions,
      :jwt,
      :knowledge_bank_list_management,
      :light_agents,
      :macro_preview,
      :macro_search,
      :multibrand,
      :multiple_organizations,
      :multiple_schedules,
      :nps_surveys,
      :organizations,
      :pathfinder_app,
      :pci,
      :permission_sets,
      :personal_rules,
      :play_tickets,
      :play_tickets_advanced,
      :report_feed,
      :reporting_leaderboard,
      :rule_analysis,
      :rule_usage_stats,
      :saml,
      :sandbox,
      :satisfaction_dashboard,
      :satisfaction_prediction,
      :screencasts_for_tickets,
      :sdk_manage_requests,
      :search_statistics,
      :service_level_agreements,
      :skill_based_attribute_ticket_mapping,
      :skill_based_ticket_routing,
      :status_hold,
      :tag_phrases_for_forum_entries,
      :targets,
      :ticket_forms,
      :ticket_sharing_triggers,
      :trigger_revision_history,
      :twitter_saved_search,
      :unlimited_automations,
      :unlimited_multibrand,
      :unlimited_triggers,
      :unlimited_views,
      :user_and_organization_fields,
      :user_views,
      :user_xml_export,
      :view_csv_export,
      :voice_business_hours,
      :xml_export,
      :zopim_chat,
      :contextual_workspaces
    ]
  end

  let(:large_features) do
    [
      :agent_collision,
      :agent_display_names,
      :api_limit_700_rpm_legacy,
      :apps_private,
      :apps_public,
      :business_hours,
      :categorized_forums,
      :chat,
      :cms,
      :community,
      :community_forums,
      :credit_card_sanitization,
      :crm_integrations,
      :cti_integrations,
      :custom_dkim_domain,
      :custom_security_policy,
      :custom_session_timeout,
      :customer_satisfaction,
      :customizable_help_center_themes,
      :dc_in_templates,
      :extended_ticket_metrics,
      :forum_statistics,
      :gooddata_advanced_analytics,
      :group_rules,
      :groups,
      :hc_manage_requests,
      :hc_user_profiles,
      :help_center_analytics,
      :help_center_article_labels,
      :help_center_configurable_spam_filter,
      :help_center_content_moderation,
      :help_center_google_analytics,
      :help_center_unlimited_categories,
      :host_mapping,
      :hosted_ssl,
      :incremental_export,
      :individual_language_selection,
      :individual_time_zone_selection,
      :internal_help_center,
      :ip_restrictions,
      :jwt,
      :knowledge_bank_list_management,
      :macro_search,
      :multiple_organizations,
      :nps_surveys,
      :organizations,
      :personal_rules,
      :play_tickets,
      :report_feed,
      :reporting_leaderboard,
      :rule_usage_stats,
      :saml,
      :sandbox,
      :satisfaction_dashboard,
      :screencasts_for_tickets,
      :sdk_manage_requests,
      :search_statistics,
      :service_level_agreements,
      :status_hold,
      :targets,
      :twitter_saved_search,
      :unlimited_automations,
      :unlimited_triggers,
      :unlimited_views,
      :user_and_organization_fields,
      :user_views,
      :user_xml_export,
      :view_csv_export,
      :voice_business_hours,
      :xml_export,
      :zopim_chat
    ]
  end

  let(:medium_features) do
    [
      :agent_collision,
      :api_limit_700_rpm_legacy,
      :apps_private,
      :apps_public,
      :categorized_forums,
      :chat,
      :community,
      :community_forums,
      :crm_integrations,
      :cti_integrations,
      :custom_dkim_domain,
      :customer_satisfaction,
      :customizable_help_center_themes,
      :groups,
      :hc_manage_requests,
      :help_center_configurable_spam_filter,
      :help_center_google_analytics,
      :help_center_unlimited_categories,
      :host_mapping,
      :jwt,
      :organizations,
      :personal_rules,
      :play_tickets,
      :reporting_leaderboard,
      :satisfaction_dashboard,
      :sdk_manage_requests,
      :status_hold,
      :targets,
      :twitter_saved_search,
      :unlimited_automations,
      :unlimited_triggers,
      :unlimited_views,
      :user_and_organization_fields,
      :user_views,
      :view_csv_export,
      :zopim_chat
    ]
  end

  let(:small_features) do
    [
      :agent_collision,
      :api_limit_700_rpm_legacy,
      :apps_private,
      :apps_public,
      :chat,
      :crm_integrations,
      :cti_integrations,
      :custom_dkim_domain,
      :groups,
      :hc_manage_requests,
      :help_center_unlimited_categories,
      :jwt,
      :organizations,
      :play_tickets,
      :reporting_leaderboard,
      :sdk_manage_requests,
      :targets,
      :twitter_saved_search,
      :unlimited_automations,
      :unlimited_triggers,
      :unlimited_views,
      :user_and_organization_fields,
      :zopim_chat
    ]
  end

  let(:inbox_features) do
    [
      :groups,
      :inbox,
      :individual_language_selection,
      :targets,
      :unlimited_automations,
      :unlimited_triggers,
      :unlimited_views,
      :user_and_organization_fields
    ]
  end

  let(:small_addons) do
    [
      :social_messaging,
      :talk_cti_partner,
      :temporary_zendesk_agents,
      :whatsapp_phone_number
    ]
  end

  let(:medium_addons) do
    [
      :social_messaging,
      :talk_cti_partner,
      :temporary_zendesk_agents,
      :whatsapp_phone_number
    ]
  end

  let(:large_addons) do
    [
      :enterprise_productivity_pack,
      :eu_data_center,
      :germany_data_center,
      :high_volume_api,
      :light_agents,
      :limited_multibrand,
      :premier_support,
      :priority_support,
      :social_messaging,
      :support_collaboration,
      :talk_cti_partner,
      :temporary_zendesk_agents,
      :us_data_center,
      :whatsapp_phone_number
    ]
  end

  let(:extra_large_addons) do
    [
      :advanced_security,
      :eu_data_center,
      :germany_data_center,
      :high_volume_api,
      :premier_support,
      :premium_sandbox_production,
      :premium_sandbox_partial,
      :premium_sandbox_metadata,
      :priority_support,
      :social_messaging,
      :support_collaboration,
      :talk_cti_partner,
      :temporary_zendesk_agents,
      :us_data_center,
      :whatsapp_phone_number
    ]
  end

  let(:catalog) { get_catalog(:legacy) }

  describe 'for Enterprise customer' do
    let(:expected_features) { extra_large_features.sort }

    it 'returns the correct features' do
      assert_equal expected_features, get_features(:legacy, SubscriptionPlanType.ExtraLarge, :feature)
    end
  end

  describe 'for Plus customer' do
    let(:expected_features) { large_features.sort }

    it 'returns the correct features' do
      assert_equal expected_features, get_features(:legacy, SubscriptionPlanType.Large, :feature)
    end
  end

  describe 'for Regular customer' do
    let(:expected_features) { medium_features.sort }

    it 'returns the correct features' do
      assert_equal expected_features, get_features(:legacy, SubscriptionPlanType.Medium, :feature)
    end
  end

  describe 'for Starter customer' do
    let(:expected_features) { small_features.sort }

    it 'returns the correct features' do
      assert_equal expected_features, get_features(:legacy, SubscriptionPlanType.Small, :feature)
    end
  end

  describe 'for Inbox customer' do
    let(:expected_features) { inbox_features.sort }

    it 'returns the correct features' do
      assert_equal expected_features, get_features(:legacy, SubscriptionPlanType.Inbox, :feature)
    end
  end

  describe 'all features' do |_variable|
    let(:expected_plans) { [SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge, SubscriptionPlanType.Inbox] }

    it 'should have valid plan types' do
      catalog.features.each do |f|
        f.plans.each do |_plan_type, plans|
          plans.each do |plan|
            catalog.plans.must_include plan
          end
        end
      end
    end

    it 'should return the correct plans' do
      assert_equal expected_plans.sort, catalog.plans.sort
    end

    it 'should be defined in the catalog, even deprecated ones' do
      # IMPORTANT: This number should ONLY increment.
      #
      # When deprecating a feature, please move its definition to the
      # "Deprecated Features" section (top of the catalog).
      assert_equal 123, catalog.features.count
    end
  end

  describe 'trialable' do
    let(:non_trialable) { [:hosted_ssl] }

    it 'list should not include non trialable features' do
      trialable_features = get_catalog(:legacy).features_for_plan(SubscriptionPlanType.Large, :trial).map(&:name)
      non_trialable.each do |f|
        trialable_features.wont_include f
      end
    end
  end

  describe 'boostable' do
    let(:non_boostable) do
      [
        :advanced_security,
        :enterprise_productivity_pack,
        :eu_data_center,
        :germany_data_center,
        :high_volume_api,
        :hosted_ssl,
        :limited_multibrand,
        :premier_support,
        :priority_support,
        :satisfaction_dashboard,
        :social_messaging,
        :status_hold,
        :talk_cti_partner,
        :temporary_zendesk_agents,
        :us_data_center,
        :whatsapp_phone_number
      ]
    end
    let(:boostable_features) { get_catalog(:legacy).boostable_features_for_plan(SubscriptionPlanType.Large).map(&:name) }

    it 'should not include blacklisted features' do
      non_boostable.each do |f|
        boostable_features.wont_include f
      end
    end

    it 'has 68 boostable features' do
      assert_equal 68, boostable_features.count
    end
  end

  describe 'Bundles' do
    describe 'light_agents' do
      let(:light_agents_features) do
        [
          :ticket_threads
        ]
      end
      let(:actual_light_agents_features) do
        Zendesk::Features::Catalogs::Catalog.features_for_bundle(:light_agents).map(&:name).sort
      end

      it 'returns the features that make up each bundle' do
        assert_equal light_agents_features, actual_light_agents_features
      end
    end

    describe 'support_collaboration' do
      let(:support_collaboration_features) do
        [
          :light_agents,
          :ticket_threads
        ]
      end
      let(:actual_support_collaboration_features) do
        Zendesk::Features::Catalogs::Catalog.features_for_bundle(:support_collaboration).map(&:name).sort
      end

      it 'returns the features that make up each bundle' do
        assert_equal support_collaboration_features, actual_support_collaboration_features
      end
    end
  end

  describe 'Addons' do
    describe 'for Enterprise customer' do
      let(:expected_features) { extra_large_addons.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:legacy, SubscriptionPlanType.ExtraLarge, :addon)
      end
    end

    describe 'for Plus customer' do
      let(:expected_features) { large_addons.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:legacy, SubscriptionPlanType.Large, :addon)
      end
    end

    describe 'for Regular customer' do
      let(:expected_features) { medium_addons.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.Medium, :addon)
      end
    end

    describe 'for Starter customer' do
      let(:expected_features) { small_addons.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.Small, :addon)
      end
    end

    describe 'for Inbox customer' do
      it 'returns the correct features' do
        assert_empty(get_features(:legacy, SubscriptionPlanType.Inbox, :addon))
      end
    end
  end

  describe 'enterprise_productivity_pack bundle' do
    let(:enterprise_productivity_pack_features) do
      %i[conditional_fields_app pathfinder_app ticket_forms]
    end

    it 'returns the bundled features' do
      actual = Zendesk::Features::Catalogs::LegacyCatalog.
        features_for_bundle(:enterprise_productivity_pack).map(&:name)

      assert_equal enterprise_productivity_pack_features, actual
    end
  end
end
