require_relative '../../../../support/test_helper'
require_relative '../../../../support/features_helpers'

SingleCov.covered!

describe 'Catalog' do
  include FeaturesHelpers

  let(:extra_large_features) do
    [
      :agent_collision,
      :agent_display_names,
      :api_limit_700_rpm,
      :apps_private,
      :apps_public,
      :audit_log,
      :bcc_archiving,
      :business_hours,
      :categorized_forums,
      :chat,
      :cms,
      :collision_chat,
      :community,
      :community_forums,
      :conditional_fields_app,
      :credit_card_sanitization,
      :crm_integrations,
      :cti_integrations,
      :custom_dkim_domain,
      :custom_security_policy,
      :custom_session_timeout,
      :customer_satisfaction,
      :customizable_help_center_themes,
      :dc_in_templates,
      :extended_ticket_metrics,
      :forum_statistics,
      :gooddata_advanced_analytics,
      :gooddata_hourly_synchronization,
      :group_rules,
      :groups,
      :hc_manage_requests,
      :hc_user_profiles,
      :help_center_analytics,
      :help_center_article_labels,
      :help_center_configurable_spam_filter,
      :help_center_content_moderation,
      :help_center_google_analytics,
      :help_center_unlimited_categories,
      :host_mapping,
      :hosted_ssl,
      :hourly_incremental_export,
      :incremental_export,
      :individual_language_selection,
      :individual_time_zone_selection,
      :internal_help_center,
      :ip_restrictions,
      :jwt,
      :knowledge_bank_list_management,
      :limited_multibrand,
      :macro_preview,
      :macro_search,
      :multiple_organizations,
      :multiple_schedules,
      :organizations,
      :pathfinder_app,
      :pci,
      :permission_sets,
      :personal_rules,
      :play_tickets,
      :play_tickets_advanced,
      :report_feed,
      :reporting_leaderboard,
      :rule_analysis,
      :rule_usage_stats,
      :saml,
      :sandbox,
      :satisfaction_dashboard,
      :satisfaction_prediction,
      :screencasts_for_tickets,
      :sdk_manage_requests,
      :search_statistics,
      :service_level_agreements,
      :skill_based_attribute_ticket_mapping,
      :skill_based_ticket_routing,
      :status_hold,
      :tag_phrases_for_forum_entries,
      :targets,
      :ticket_forms,
      :ticket_sharing_triggers,
      :trigger_revision_history,
      :unlimited_automations,
      :unlimited_triggers,
      :unlimited_views,
      :user_and_organization_fields,
      :user_xml_export,
      :view_csv_export,
      :voice_business_hours,
      :xml_export,
      :zopim_chat,
      :contextual_workspaces
    ]
  end

  let(:large_features) do
    [
      :agent_collision,
      :agent_display_names,
      :api_limit_400_rpm,
      :apps_private,
      :apps_public,
      :business_hours,
      :categorized_forums,
      :chat,
      :cms,
      :community,
      :community_forums,
      :credit_card_sanitization,
      :crm_integrations,
      :cti_integrations,
      :custom_dkim_domain,
      :custom_security_policy,
      :custom_session_timeout,
      :customer_satisfaction,
      :customizable_help_center_themes,
      :dc_in_templates,
      :extended_ticket_metrics,
      :forum_statistics,
      :gooddata_advanced_analytics,
      :group_rules,
      :groups,
      :hc_manage_requests,
      :hc_user_profiles,
      :help_center_analytics,
      :help_center_article_labels,
      :help_center_configurable_spam_filter,
      :help_center_content_moderation,
      :help_center_google_analytics,
      :help_center_unlimited_categories,
      :host_mapping,
      :hosted_ssl,
      :incremental_export,
      :individual_language_selection,
      :individual_time_zone_selection,
      :internal_help_center,
      :jwt,
      :knowledge_bank_list_management,
      :macro_search,
      :multiple_organizations,
      :organizations,
      :personal_rules,
      :play_tickets,
      :report_feed,
      :reporting_leaderboard,
      :rule_usage_stats,
      :saml,
      :satisfaction_dashboard,
      :screencasts_for_tickets,
      :sdk_manage_requests,
      :search_statistics,
      :service_level_agreements,
      :status_hold,
      :targets,
      :unlimited_automations,
      :unlimited_triggers,
      :unlimited_views,
      :user_and_organization_fields,
      :user_xml_export,
      :view_csv_export,
      :voice_business_hours,
      :xml_export,
      :zopim_chat
    ]
  end

  let(:medium_features) do
    [
      :agent_collision,
      :api_limit_200_rpm,
      :apps_public,
      :categorized_forums,
      :chat,
      :community_forums,
      :crm_integrations,
      :cti_integrations,
      :custom_dkim_domain,
      :customizable_help_center_themes,
      :group_rules,
      :groups,
      :hc_manage_requests,
      :help_center_configurable_spam_filter,
      :help_center_google_analytics,
      :help_center_unlimited_categories,
      :host_mapping,
      :jwt,
      :organizations,
      :personal_rules,
      :play_tickets,
      :reporting_leaderboard,
      :sdk_manage_requests,
      :status_hold,
      :targets,
      :unlimited_automations,
      :unlimited_triggers,
      :unlimited_views,
      :user_and_organization_fields,
      :view_csv_export,
      :zopim_chat
    ]
  end

  let(:small_features) do
    [
      :agent_collision,
      :chat,
      :custom_dkim_domain,
      :groups,
      :help_center_unlimited_categories,
      :zopim_chat
    ]
  end

  let(:inbox_features) do
    [
      :groups,
      :inbox,
      :targets,
      :individual_language_selection,
      :unlimited_automations,
      :unlimited_triggers,
      :unlimited_views,
      :user_and_organization_fields
    ]
  end

  let(:small_addons) do
    [
      :social_messaging,
      :talk_cti_partner,
      :temporary_zendesk_agents,
      :whatsapp_phone_number
    ]
  end

  let(:medium_addons) do
    [
      :social_messaging,
      :talk_cti_partner,
      :temporary_zendesk_agents,
      :whatsapp_phone_number
    ]
  end

  let(:large_addons) do
    [
      :enterprise_productivity_pack,
      :eu_data_center,
      :germany_data_center,
      :high_volume_api,
      :light_agents,
      :limited_multibrand,
      :nps,
      :premier_support,
      :priority_support,
      :social_messaging,
      :support_collaboration,
      :talk_cti_partner,
      :temporary_zendesk_agents,
      :us_data_center,
      :whatsapp_phone_number
    ]
  end

  let(:extra_large_addons) do
    [
      :advanced_security,
      :eu_data_center,
      :germany_data_center,
      :high_volume_api,
      :light_agents,
      :nps,
      :premier_support,
      :premium_sandbox_production,
      :premium_sandbox_partial,
      :premium_sandbox_metadata,
      :priority_support,
      :social_messaging,
      :support_collaboration,
      :talk_cti_partner,
      :temporary_zendesk_agents,
      :unlimited_multibrand,
      :us_data_center,
      :whatsapp_phone_number
    ]
  end

  describe 'Features' do
    describe 'for Enterprise customer' do
      let(:expected_features) { extra_large_features.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.ExtraLarge, :feature)
      end
    end

    describe 'for Professional customer' do
      let(:expected_features) { large_features.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.Large, :feature)
      end
    end

    describe 'for Team customer' do
      let(:expected_features) { medium_features.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.Medium, :feature)
      end
    end

    describe 'for Essential customer' do
      let(:expected_features) { small_features.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.Small, :feature)
      end
    end

    describe 'for Inbox customer' do
      let(:expected_features) { inbox_features.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.Inbox, :feature)
      end
    end

    describe 'all features' do
      let(:catalog) { get_catalog(:catalog) }

      it 'should have valid plan types' do
        catalog.features.each do |f|
          f.plans.each do |_plan_type, plans|
            plans.each do |plan|
              catalog.plans.must_include plan
            end
          end
        end
      end

      it 'should not be both included in and an addon for the same plan' do
        catalog.features.each do |f|
          f.send(:valid_no_redundancy)
          f.errors.must_be_empty
        end
      end

      it 'should be defined in the catalog, even deprecated ones' do
        # IMPORTANT: This number should ONLY increment.
        #
        # When deprecating a feature, please move its definition to the
        # "Deprecated Features" section (top of the catalog).
        assert_equal 122, catalog.features.count
      end
    end
  end

  describe 'Addons' do
    describe 'for Enterprise customer' do
      let(:expected_features) { extra_large_addons.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.ExtraLarge, :addon)
      end
    end

    describe 'for Professional customer' do
      let(:expected_features) { large_addons.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.Large, :addon)
      end
    end

    describe 'for Team customer' do
      let(:expected_features) { medium_addons.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.Medium, :addon)
      end
    end

    describe 'for Essential customer' do
      let(:expected_features) { small_addons.sort }

      it 'returns the correct features' do
        assert_equal expected_features, get_features(:catalog, SubscriptionPlanType.Small, :addon)
      end
    end

    describe 'for Inbox customer' do
      it 'returns the correct features' do
        assert_empty(get_features(:catalog, SubscriptionPlanType.Inbox, :addon))
      end
    end
  end

  describe 'Bundles' do
    let(:expected_bundles) do
      [
        :advanced_security,
        :enterprise_productivity_pack,
        :light_agents,
        :nps,
        :support_collaboration
      ]
    end

    it 'returns the bundles in the catalog' do
      assert_equal expected_bundles, get_features(:catalog, nil, :bundle)
    end

    describe 'advanced_security bundle' do
      let(:advanced_security_features) do
        [
          :drp,
          :encryption_at_rest,
          :hipaa
        ]
      end
      let(:actual_advanced_security_features) do
        Zendesk::Features::Catalogs::Catalog.features_for_bundle(:advanced_security).map(&:name).sort
      end

      it 'returns the features that make up each bundle' do
        assert_equal advanced_security_features, actual_advanced_security_features
      end
    end

    describe 'enterprise_productivity_pack bundle' do
      let(:enterprise_productivity_pack_features) do
        [
          :conditional_fields_app,
          :pathfinder_app,
          :ticket_forms
        ]
      end
      let(:actual_enterprise_productivity_pack_features) do
        Zendesk::Features::Catalogs::Catalog.features_for_bundle(:enterprise_productivity_pack).map(&:name).sort
      end

      it 'returns the features that make up each bundle' do
        assert_equal enterprise_productivity_pack_features, actual_enterprise_productivity_pack_features
      end
    end

    describe 'nps bundle' do
      let(:nps_features) do
        [
          :nps_surveys,
          :user_views
        ]
      end
      let(:actual_nps_features) do
        Zendesk::Features::Catalogs::Catalog.features_for_bundle(:nps).map(&:name).sort
      end

      it 'returns the features that make up each bundle' do
        assert_equal nps_features, actual_nps_features
      end
    end

    describe 'support_collaboration' do
      let(:support_collaboration_features) do
        [
          :light_agents,
          :ticket_threads
        ]
      end
      let(:actual_support_collaboration_features) do
        Zendesk::Features::Catalogs::Catalog.features_for_bundle(:support_collaboration).map(&:name).sort
      end

      it 'returns the features that make up each bundle' do
        assert_equal support_collaboration_features, actual_support_collaboration_features
      end
    end

    describe 'all bundles' do
      let(:catalog) { get_catalog(:catalog) }

      it 'should have at least one plan specified as an addon' do
        catalog.bundles.each do |b|
          b.send(:valid_bundle_is_assigned)
          b.errors.must_be_empty
        end
      end

      it 'should specify valid features' do
        catalog.bundles.each do |b|
          catalog.features_for_bundle(b.name).count.must_equal b.bundled_features.count
        end
      end

      it 'should not be included in plans' do
        catalog.bundles.each do |b|
          b.plans[:included_for].must_be_empty
        end
      end
    end
  end

  describe 'Plans' do
    let(:actual_plans) { [SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge, SubscriptionPlanType.Inbox] }
    it 'should be specified in the catalog' do
      get_catalog(:catalog).plans.wont_be_empty
    end

    it 'should return the correct plans' do
      assert_equal actual_plans.sort, get_catalog(:catalog).plans.sort
    end
  end

  describe 'trialable' do
    let(:non_trialable) { [:hosted_ssl, :temporary_zendesk_agents] }

    it 'list should not include blacklisted features' do
      trialable_features = get_catalog(:catalog).features_for_plan(SubscriptionPlanType.Large, :trial).map(&:name)
      non_trialable.each do |f|
        trialable_features.wont_include f
      end
    end
  end

  describe 'boostable' do
    let(:non_boostable) do
      [
        :advanced_security,
        :enterprise_productivity_pack,
        :eu_data_center,
        :germany_data_center,
        :high_volume_api,
        :hosted_ssl,
        :light_agents,
        :limited_multibrand,
        :premier_support,
        :priority_support,
        :satisfaction_dashboard,
        :social_messaging,
        :status_hold,
        :talk_cti_partner,
        :temporary_zendesk_agents,
        :us_data_center,
        :whatsapp_phone_number
      ]
    end
    let(:boostable_features) { get_catalog(:catalog).boostable_features_for_plan(SubscriptionPlanType.Large).map(&:name) }

    it 'should not include blacklisted features' do
      non_boostable.each do |f|
        boostable_features.wont_include f
      end
    end

    it 'has 63 boostable features' do
      assert_equal 63, boostable_features.count
    end
  end

  describe 'groups' do
    def self.test_group_members(group_name, *members)
      describe "has_#{group_name}?" do
        members.each do |f|
          it "returns true if has_#{f}? is true" do
            # Don't put in a before block, won't work
            subscription.stubs(:feature_present?).returns(false)
            subscription.stubs(:feature_present?).with(f).returns(true)
            assert subscription.send("has_#{group_name}?")
          end
        end

        it 'returns false if none are true' do
          subscription.stubs(:feature_present?).returns(false)
          refute subscription.send("has_#{group_name}?")
        end
      end
    end

    let(:subscription) { Subscription.new }

    test_group_members(:api, :api_limit_200_rpm, :api_limit_400_rpm, :api_limit_700_rpm, :api_limit_700_rpm_legacy)
    test_group_members(:manage_requests, :hc_manage_requests, :sdk_manage_requests)
    test_group_members(:multibrand, :unlimited_multibrand, :limited_multibrand)
  end
end
