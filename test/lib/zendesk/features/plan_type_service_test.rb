require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'PlanTypeService' do
  let(:account) { accounts(:minimum) }
  let(:subscription) { account.subscription }
  let(:plan_type_service) { Zendesk::Features::PlanTypeService.new(account) }

  describe '#proposed_plan_type' do
    describe 'when account is a trial' do
      let(:addon_service) do
        Zendesk::Features::AddonService.new(account, {})
      end

      it "does not remove, update, or add new addons" do
        addon_service.expects(:remove_addons).never
        addon_service.expects(:update_addons).never
        addon_service.expects(:add_new_addons).never
        assert_nil addon_service.execute!
      end
      before do
        subscription.stubs(:is_trial?).returns(true)
      end
      it 'returns the subscription plan_type' do
        assert_equal subscription.plan_type, plan_type_service.proposed_plan_type
      end
    end

    describe 'when account is paid customer' do
      let(:plan_type_service) { Zendesk::Features::PlanTypeService.new(account, SubscriptionPlanType.Small) }
      describe 'without an active plan type boost' do
        before do
          subscription.stubs(:is_trial?).returns(false)
        end
        it 'returns the zuora plan_type' do
          assert_equal SubscriptionPlanType.Small, plan_type_service.proposed_plan_type
        end
      end

      describe 'with an active boost that is higher than the zuora_plan type' do
        let(:feature_boost) { stub(boost_level: 4, is_active?: true) }
        before do
          subscription.stubs(:is_trial?).returns(false)
          account.stubs(:feature_boost).returns(feature_boost)
        end
        it 'returns the zuora plan_type' do
          assert_equal SubscriptionPlanType.ExtraLarge, plan_type_service.proposed_plan_type
        end
      end
    end

    describe 'when account boosts the plan type' do
      let(:feature_boost) { stub(boost_level: 4, is_active?: true) }
      before do
        subscription.stubs(:is_trial?).returns(false)
        account.stubs(:feature_boost).returns(feature_boost)
      end
      it 'returns the higher of the subscription plan_type or the boosted level' do
        assert_equal SubscriptionPlanType.ExtraLarge, plan_type_service.proposed_plan_type
      end
    end
  end
end
