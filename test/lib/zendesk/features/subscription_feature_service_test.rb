require_relative "../../../support/test_helper"
require "zendesk_feature_framework"

SingleCov.covered!

describe Zendesk::Features::SubscriptionFeatureService do
  def self.startup
    # need to persist initial feature set
    Zendesk::Features::SubscriptionFeatureService.new(account).execute!
  end

  let(:account) { accounts(:minimum) }

  let(:subscription_feature_service) do
    Zendesk::Features::SubscriptionFeatureService.new(account)
  end

  let(:subscription) { subscription_feature_service.send(:subscription) }

  describe '#subscription' do
    it 'attempts to read from the DB first' do
      Subscription.expects(:find_by_account_id).returns(true).once
      account.expects(:subscription).never
      assert subscription_feature_service.send(:subscription)
    end

    it 'falls back to the version in the accounts object' do
      Subscription.expects(:find_by_account_id).returns(nil).once
      account.expects(:subscription).returns(true).once
      assert subscription_feature_service.send(:subscription)
    end
  end

  describe '#execute!' do
    before do
      Subscription.any_instance.stubs(find_by_account_id: account.subscription)
    end

    it 'returns immediately if the account is the system account' do
      account.stubs(:id).returns(-1)
      assert_nil Zendesk::Features::SubscriptionFeatureService.new(account).execute!
    end

    it 'returns immediately if no features are changed' do
      assert Zendesk::Features::SubscriptionFeatureService.new(account).execute!
      assert_nil Zendesk::Features::SubscriptionFeatureService.new(account,
        fast_execute: true).execute!
    end

    it 'returns immediately if the account does not have Support' do
      Zendesk::Features::SubscriptionFeatureService.any_instance.stubs(:subscription).returns(nil)
      assert_nil Zendesk::Features::SubscriptionFeatureService.new(account).execute!
    end

    describe 'when using Legacy Catalog' do
      let(:current_features) do
        [
          stub(name: 'feat1'),
          stub(name: 'feat2')
        ]
      end
      let(:feature_changes) do
        {
          additions: [],
          removals: [],
          unchanged: []
        }
      end
      let(:addon_service) { stub(proposed_addons: []) }
      let(:plan_type_service) { stub(proposed_plan_type: 3) }
      let(:plan_change_calculator) do
        stub(
          changes: feature_changes,
          override: nil
        )
      end

      before do
        subscription.stubs(:active_features).returns(current_features)
        Zendesk::Features::PlanTypeService.stubs(:new).returns(plan_type_service)
        Zendesk::Features::AddonService.stubs(:new).returns(addon_service)
      end

      it 'properly invalidates the subscription features cache' do
        ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
          [:feat1, :feat2],
          3,
          [],
          Zendesk::Features::Catalogs::LegacyCatalog,
          {}
        ).returns(plan_change_calculator)

        Rails.cache.expects(:delete).at_least(1)
        assert subscription_feature_service.execute!
      end

      describe 'with only paid account' do
        before do
          subscription.stubs(:is_trial?).returns(false)
        end

        it 'calls feature_changes correctly' do
          ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
            [:feat1, :feat2],
            3,
            [],
            Zendesk::Features::Catalogs::LegacyCatalog,
            {}
          ).returns(plan_change_calculator)
          assert subscription_feature_service.execute!
        end

        describe 'when on pricing model 0' do
          before do
            Subscription.any_instance.stubs(:pricing_model_revision).returns(0)
            account.settings.stubs(:personal_rules?).returns(false)
          end

          describe 'with proposed_plan_type 1' do
            let(:plan_type_service) { stub(proposed_plan_type: 1) }
            before do
              Subscription.any_instance.stubs(:plan_type).returns(1)
            end
            it 'calls feature_changes with overrides' do
              ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                [:feat1, :feat2],
                1,
                [],
                Zendesk::Features::Catalogs::LegacyCatalog,
                {}
              ).returns(plan_change_calculator)
              plan_change_calculator.expects(:override).with(:forums2_toggle, true)
              plan_change_calculator.expects(:override).with(:host_mapping, true)
              plan_change_calculator.expects(:override).with(:view_csv_export, true)
              assert subscription_feature_service.execute!
            end
          end

          (2..4).each do |p|
            describe "on plan_type #{p}" do
              let(:plan_type_service) { stub(proposed_plan_type: p) }
              it "calls feature_changes with correct override for plan_type #{p}" do
                ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                  [:feat1, :feat2],
                  p,
                  [],
                  Zendesk::Features::Catalogs::LegacyCatalog,
                  {}
                ).returns(plan_change_calculator)
                plan_change_calculator.expects(:override).with(:forums2_toggle, true)
                assert subscription_feature_service.execute!
              end
            end
          end

          describe 'with personal rules settings set to true' do
            let(:plan_type_service) { stub(proposed_plan_type: 5) }

            before do
              Subscription.any_instance.stubs(:plan_type).returns(5)
              account.settings.stubs(:personal_rules?).returns(true)
            end

            it 'calls feature_changes with overrides' do
              ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                [:feat1, :feat2],
                5,
                [],
                Zendesk::Features::Catalogs::LegacyCatalog,
                {}
              ).returns(plan_change_calculator)
              plan_change_calculator.expects(:override).with(:personal_rules, true)
              assert subscription_feature_service.execute!
            end
          end
        end

        describe 'when on pricing model 1' do
          before do
            Subscription.any_instance.stubs(:pricing_model_revision).returns(1)
          end
          describe 'with proposed_plan_type 1' do
            let(:plan_type_service) { stub(proposed_plan_type: 1) }

            it 'calls feature_changes with overrides' do
              ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                [:feat1, :feat2],
                1,
                [],
                Zendesk::Features::Catalogs::LegacyCatalog,
                {}
              ).returns(plan_change_calculator)
              plan_change_calculator.expects(:override).with(:forums2_toggle, true)
              plan_change_calculator.expects(:override).with(:host_mapping, true)
              plan_change_calculator.expects(:override).with(:view_csv_export, true)
              assert subscription_feature_service.execute!
            end
          end

          (2..5).each do |p|
            describe "on plan_type #{p}" do
              let(:plan_type_service) { stub(proposed_plan_type: p) }
              it "calls feature_changes with correct override for plan_type #{p}" do
                ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                  [:feat1, :feat2],
                  p,
                  [],
                  Zendesk::Features::Catalogs::LegacyCatalog,
                  {}
                ).returns(plan_change_calculator)
                plan_change_calculator.expects(:override).with(:forums2_toggle, true)
                assert subscription_feature_service.execute!
              end
            end
          end
        end

        describe 'when on pricing model 4' do
          before do
            Subscription.any_instance.stubs(:pricing_model_revision).returns(4)
          end
          (1..4).each do |plan_type|
            describe "when plan_type #{plan_type}" do
              let(:plan_type_service) { stub(proposed_plan_type: plan_type) }
              it "never calls override for plan_type #{plan_type}" do
                ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                  [:feat1, :feat2],
                  plan_type,
                  [],
                  Zendesk::Features::Catalogs::LegacyCatalog,
                  {}
                ).returns(plan_change_calculator)
                plan_change_calculator.expects(:override).never
                assert subscription_feature_service.execute!
              end
            end
          end

          describe 'with proposed_plan_type 5' do
            let(:plan_type_service) { stub(proposed_plan_type: 5) }

            it 'calls feature_changes with overrides' do
              ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                [:feat1, :feat2],
                5,
                [],
                Zendesk::Features::Catalogs::LegacyCatalog,
                {}
              ).returns(plan_change_calculator)
              plan_change_calculator.expects(:override).with(:host_mapping, true)
              assert subscription_feature_service.execute!
            end
          end

          describe 'with grandfathered host mapping setting set to true' do
            let(:plan_type_service) { stub(proposed_plan_type: 4) }

            before do
              account.settings.stubs(:grandfathered_host_mapping?).returns(true)
            end

            it 'calls feature_changes with overrides' do
              ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                [:feat1, :feat2],
                4,
                [],
                Zendesk::Features::Catalogs::LegacyCatalog,
                {}
              ).returns(plan_change_calculator)
              plan_change_calculator.expects(:override).with(:host_mapping, true)
              assert subscription_feature_service.execute!
            end
          end
        end

        describe 'when on pricing model 5' do
          before do
            Subscription.any_instance.stubs(:pricing_model_revision).returns(5)
          end

          (1..5).each do |plan_type|
            describe "when plan_type #{plan_type}" do
              let(:plan_type_service) { stub(proposed_plan_type: plan_type) }

              it "never calls override for plan_type #{plan_type}" do
                ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                  [:feat1, :feat2],
                  plan_type,
                  [],
                  Zendesk::Features::Catalogs::LegacyCatalog,
                  {}
                ).returns(plan_change_calculator)
                plan_change_calculator.expects(:override).never
                assert subscription_feature_service.execute!
              end
            end
          end

          describe 'with grandfathered host mapping setting set to true' do
            let(:plan_type_service) { stub(proposed_plan_type: 4) }

            before do
              account.settings.stubs(:grandfathered_host_mapping?).returns(true)
            end

            it 'calls feature_changes with overrides' do
              ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                [:feat1, :feat2],
                4,
                [],
                Zendesk::Features::Catalogs::LegacyCatalog,
                {}
              ).returns(plan_change_calculator)
              plan_change_calculator.expects(:override).with(:host_mapping, true)
              assert subscription_feature_service.execute!
            end
          end

          describe 'with help center grandfather ga set to true' do
            let(:plan_type_service) { stub(proposed_plan_type: 1) }

            before do
              account.stubs(:has_help_center_grandfather_ga?).returns(true)
            end

            it 'calls feature_changes with overrides' do
              ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
                [:feat1, :feat2],
                1,
                [],
                Zendesk::Features::Catalogs::LegacyCatalog,
                {}
              ).returns(plan_change_calculator)
              plan_change_calculator.expects(:override).with(:help_center_google_analytics, true)
              assert subscription_feature_service.execute!
            end
          end
        end

        describe 'when on pricing model 7' do
          before do
            Subscription.any_instance.stubs(:pricing_model_revision).returns(7)

            ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
              [:feat1, :feat2],
              3,
              [],
              Zendesk::Features::Catalogs::Catalog,
              {}
            ).returns(plan_change_calculator)
          end

          it 'does not call help_center_ga_override' do
            subscription_feature_service.expects(:help_center_ga_override).never
            subscription_feature_service.execute!
          end
        end

        describe 'settings-based overrides' do
          before do
            ZendeskFeatureFramework::PlanChangeCalculator.stubs(:new).returns(plan_change_calculator)
          end

          describe 'grandfathered_web_portal' do
            describe 'when true' do
              before do
                account.settings.stubs(:grandfathered_web_portal?).returns(true)
              end

              it 'should enable community and custom themes features' do
                plan_change_calculator.expects(:override).with(:community, true)
                plan_change_calculator.expects(:override).with(:customizable_help_center_themes, true)
                plan_change_calculator.expects(:override).with(:internal_help_center, true)
                assert subscription_feature_service.execute!
              end
            end

            describe 'when false' do
              it 'should NOT enable community and custom themes features' do
                plan_change_calculator.expects(:override).never
                assert subscription_feature_service.execute!
              end
            end
          end
        end

        describe 'support_collaboration overrides' do
          let(:subscription_feature_service) do
            Zendesk::Features::SubscriptionFeatureService.new(account, 0)
          end

          before do
            ZendeskFeatureFramework::PlanChangeCalculator.stubs(:new).returns(plan_change_calculator)
          end

          describe 'grandfathered_support_enterprise' do
            describe 'when plan type is legacy Enterprise' do
              before do
                Zendesk::Features::SubscriptionFeatureService.any_instance.
                  stubs(:proposed_plan_type).
                  returns(SubscriptionPlanType.ExtraLarge)
                Zendesk::Features::SubscriptionFeatureService.any_instance.
                  stubs(:subscribed_for_support_collaboration?).returns(false)
              end

              it 'should disable support collaboration features' do
                plan_change_calculator.expects(:override).with(:ticket_threads, false)
                assert subscription_feature_service.execute!
              end
            end

            describe 'when plan type is NOT legacy Enterprise' do
              it 'should NOT enable support collaboration features' do
                plan_change_calculator.expects(:override).never
                assert subscription_feature_service.execute!
              end
            end

            describe 'when plan type is legacy Enterprise and has support_collaboration addon' do
              before do
                Zendesk::Features::SubscriptionFeatureService.any_instance.
                  stubs(:proposed_plan_type).
                  returns(SubscriptionPlanType.ExtraLarge)
                account.subscription.stubs(:is_elite?).returns(false)
                account.subscription_feature_addons.
                  stubs(find_by_name: stub(active?: true))
              end

              it 'should NOT override features' do
                plan_change_calculator.expects(:override).never
                assert subscription_feature_service.execute!
              end
            end

            describe 'when plan type is legacy Enterprise Elite and has support_collaboration addon' do
              before do
                Zendesk::Features::SubscriptionFeatureService.any_instance.
                  stubs(:proposed_plan_type).
                  returns(SubscriptionPlanType.ExtraLarge)
                account.subscription.stubs(:is_elite?).returns(true)
                account.subscription_feature_addons.
                  stubs(find_by_name: stub(active?: true))
              end

              it 'should NOT enable support collaboration features' do
                plan_change_calculator.expects(:override).never
                assert subscription_feature_service.execute!
              end
            end
          end
        end

        describe 'subscription cache key' do
          before do
            @before_cache_key = subscription.cache_key
          end

          let(:plan_type_service) { stub(proposed_plan_type: 2) }

          it 'updates the cache key' do
            ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
              [:feat1, :feat2],
              2,
              [],
              Zendesk::Features::Catalogs::LegacyCatalog,
              {}
            ).returns(plan_change_calculator)
            plan_change_calculator.expects(:override).never
            assert subscription_feature_service.execute!
            after_cache_key = subscription.reload.cache_key

            assert(@before_cache_key != after_cache_key, "Before : #{@before_cache_key}, After : #{after_cache_key}")
          end
        end

        describe '#pricing_model' do
          describe 'when zuora_pricing_model_revision is present' do
            let(:subscription_feature_service) do
              Zendesk::Features::SubscriptionFeatureService.new(account, 0)
            end

            it 'returns zuora pricing model' do
              assert_equal subscription_feature_service.zuora_pricing_model, subscription_feature_service.send(:pricing_model)
            end
          end

          describe 'when not called from zuora' do
            it 'returns the subscriptions pricing_model revision' do
              assert_equal subscription.pricing_model_revision, subscription_feature_service.send(:pricing_model)
            end
          end
        end
      end

      describe 'with trial account' do
        before do
          subscription.stubs(:is_trial?).returns(true)
        end

        it 'calls feature_changes correctly' do
          ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
            [:feat1, :feat2],
            3,
            [],
            Zendesk::Features::Catalogs::LegacyCatalog,
            trial: true
          ).returns(plan_change_calculator)
          assert subscription_feature_service.execute!
        end
      end

      describe 'with boosted plan_type on account' do
        let(:feature_boost) do
          stub(
            is_active?: true,
            boost_level: 4,
            updated_at: (Zendesk::Features::CatalogService::PATAGONIA_GA_DATETIME - 1.day)
          )
        end
        let(:plan_type_service) { stub(proposed_plan_type: 4) }
        before do
          subscription.stubs(:is_trial?).returns(false)
          account.stubs(:feature_boost).returns(feature_boost)
        end

        it 'calls feature_changes correctly' do
          ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
            [:feat1, :feat2],
            4,
            [],
            Zendesk::Features::Catalogs::LegacyCatalog,
            boosted_plan: true
          ).returns(plan_change_calculator)
          assert subscription_feature_service.execute!
        end
      end

      describe 'the feature_hash' do
        let(:current_features) do
          [
            stub(name: 'jwt'),
            stub(name: 'groups')
          ]
        end
        let(:feature_changes) do
          {
            additions: [:light_agents],
            removals: [:agent_display_names, :unlimited_multibrand],
            unchanged: [:chat]
          }
        end

        let(:features) { subscription.features }
        let(:light_agents_feature) { features.where(name: "light_agents").first }
        let(:agent_display_names_feature) { features.where(name: "agent_display_names").first }
        let(:unlimited_multibrand_feature) { features.where(name: "unlimited_multibrand").first }

        before do
          ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).with(
            [:jwt, :groups],
            3,
            [],
            Zendesk::Features::Catalogs::LegacyCatalog,
            {}
          ).returns(plan_change_calculator)
        end

        it 'calls feature_changes correctly' do
          assert subscription_feature_service.execute!
        end

        it 'sets the features correctly in the subscription_feature table' do
          subscription_feature_service.execute!
          assert_equal "1", light_agents_feature.value
          assert_equal "0", agent_display_names_feature.value
          assert_equal "0", unlimited_multibrand_feature.value
        end

        it 'calls feature change triggers' do
          ::Zendesk::Features::Change::LightAgentsChange.any_instance.expects(:upgrade)
          ::Zendesk::Features::Change::UnlimitedMultibrandChange.any_instance.expects(:downgrade)
          assert subscription_feature_service.execute!
        end

        describe 'skip feature triggers' do
          let(:subscription_feature_service) do
            Zendesk::Features::SubscriptionFeatureService.new(account, nil, nil, {}, true)
          end

          it 'does not call feature change triggers' do
            ::Zendesk::Features::Change::LightAgentsChange.any_instance.expects(:upgrade).never
            ::Zendesk::Features::Change::UnlimitedMultibrandChange.any_instance.expects(:downgrade).never
            assert subscription_feature_service.execute!
          end
        end
      end
    end

    describe 'when using Patagonia Catalog' do
      before do
        Zendesk::Features::CatalogService.any_instance.stubs(:current_catalog).
          returns(Zendesk::Features::Catalogs::Catalog)
      end

      describe 'medium account' do
        let(:features) { account.subscription.active_features }

        it 'upgrade sets the features correctly' do
          num_med_features = features.count
          Zendesk::Features::SubscriptionFeatureService.new(account, nil, SubscriptionPlanType.Large).execute!
          num_lg_features = features.count
          expected_num_lg_features = Zendesk::Features::Catalogs::Catalog.features_for_plan(SubscriptionPlanType.Large).count

          assert_not_equal num_med_features, num_lg_features
          assert_equal expected_num_lg_features, num_lg_features
        end

        it 'downgrade sets the features correctly' do
          num_med_features = features.count
          Zendesk::Features::SubscriptionFeatureService.new(account, nil, SubscriptionPlanType.Small).execute!
          num_sm_features = features.count
          expected_num_sm_features = Zendesk::Features::Catalogs::Catalog.features_for_plan(SubscriptionPlanType.Small).count

          assert_not_equal num_med_features, num_sm_features
          assert_equal expected_num_sm_features, num_sm_features
        end
      end

      describe 'extra_large account' do
        let(:el_account) { accounts(:extra_large) }

        before do
          UpdateAppsFeatureJob.stubs(:enqueue)
          # need to persist initial feature set
          Zendesk::Features::SubscriptionFeatureService.new(el_account).execute!
        end

        it 'purchasing addons sets the features correctly' do
          assert_nil el_account.subscription.active_features.find { |f| f.name == 'light_agents' }
          Zendesk::Features::SubscriptionFeatureService.new(el_account, nil, nil, 'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' }).execute!
          assert_not_nil el_account.subscription.active_features.find { |f| f.name == 'light_agents' }
        end

        it 'removing addons sets the features correctly' do
          Zendesk::Features::SubscriptionFeatureService.new(el_account, nil, nil, 'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' }).execute!
          assert_not_nil el_account.subscription.active_features.find { |f| f.name == 'light_agents' }
          Zendesk::Features::SubscriptionFeatureService.new(el_account, nil, nil, {}).execute!
          assert_nil el_account.subscription.active_features.find { |f| f.name == 'light_agents' }
        end

        describe 'sets correct features for trial account' do
          before do
            el_account.subscription.features.destroy_all
            el_account.subscription.update_column(:is_trial, true)
            assert_nil el_account.subscription.active_features.find { |f| f.name == 'hosted_ssl' }
            Zendesk::Features::SubscriptionFeatureService.new(el_account).execute!
          end

          it 'does not have non-trialable features' do
            assert_nil el_account.subscription.active_features.find { |f| f.name == 'hosted_ssl' }
          end

          it 'has trialable features' do
            assert_not_nil el_account.subscription.active_features.find { |f| f.name == 'host_mapping' }
          end
        end

        describe 'sets correct features for trial syncing to a paid account' do
          before do
            el_account.subscription.features.destroy_all
            el_account.subscription.stubs(:is_trial?).returns(true)
            assert_nil el_account.subscription.active_features.find { |f| f.name == 'hosted_ssl' }
            Zendesk::Features::SubscriptionFeatureService.new(el_account, 7, SubscriptionPlanType.ExtraLarge).execute!
          end

          it 'has hosted_ssl a non-trialable features' do
            assert_not_nil el_account.subscription.active_features.find { |f| f.name == 'hosted_ssl' }
          end

          it 'has host_mapping a trialable features' do
            assert_not_nil el_account.subscription.active_features.find { |f| f.name == 'host_mapping' }
          end
        end
      end
    end

    describe 'when large_account using Pre Patagonia' do
      let(:large_account) do
        account.subscription.update_attributes(
          pricing_model_revision: ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE,
          plan_type: SubscriptionPlanType.Large
        )
        account
      end

      before do
        UpdateAppsFeatureJob.stubs(:enqueue)
        Zendesk::Features::SubscriptionFeatureService.new(large_account).execute!
      end

      it 'purchasing addons sets the features correctly' do
        assert_nil large_account.subscription.active_features.find { |f| f.name == 'light_agents' }
        Zendesk::Features::SubscriptionFeatureService.new(large_account, nil, nil, 'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' }).execute!
        assert_not_nil large_account.subscription.active_features.find { |f| f.name == 'light_agents' }
        assert_not_nil large_account.subscription.active_features.find { |f| f.name == 'ticket_threads' }
      end
    end

    describe 'SP&P override' do
      let(:feature_changes) do
        {
          additions: [],
          removals: [],
          unchanged: []
        }
      end
      let(:plan_change_calculator) do
        stub(changes: feature_changes, override: nil)
      end

      before do
        Zendesk::Features::Overrides::SimplePricePackaging.stubs(:changes_for).returns({ ticket_threads: true, light_agents: false })
      end

      describe 'when the acccount is SPP' do
        let(:zuora_billing_id) { 1234 }

        before do
          Account.any_instance.stubs(:spp?).returns(true)
          subscription_feature_service.stubs(:account_client).returns(OpenStruct.new(zuora_billing_id: zuora_billing_id))
        end

        it 'applies the SPP changes' do
          subscription_feature_service.execute!
        end

        describe '#changes' do
          let(:feature_changes) do
            {
              additions: [:able, :baker],
              removals:  [:echo, :foxtrot]
            }
          end
          let(:spp_addon_changes) do
            {
              charlie: true,
              delta: false
            }
          end

          before do
            subscription_feature_service.stubs(:feature_changes).returns(feature_changes)
            subscription_feature_service.stubs(:spp_addon_changes).returns(spp_addon_changes)
            subscription_feature_service.expects(:feature_setup).with({
              able: true,
              baker: true,
              charlie: true,
              delta: false,
              echo: false,
              foxtrot: false
            })
          end

          it 'augments and does not replace the default feature changes' do
            subscription_feature_service.execute!
          end
        end

        describe 'and the enterprise_productivity_pack is enabled' do
          before do
            Zendesk::Features::Overrides::SimplePricePackaging.stubs(:changes_for).returns({ enterprise_productivity_pack: true })
          end

          describe 'when the enterprise_productivity_pack addon has already been added' do
            before do
              addon = SubscriptionFeatureAddon.new
              addon.account_id = account.id
              addon.boost_expires_at = 10.days.from_now.to_s
              addon.name = 'enterprise_productivity_pack'
              addon.save!
              subscription_feature_service.execute!
            end

            it 'does not add another enterprise_productivity_pack addon' do
              assert account.subscription_feature_addons.count == 1
              assert account.subscription_feature_addons[0].name == 'enterprise_productivity_pack'
            end
          end

          describe 'when the enterprise_productivity_pack addon has not been added' do
            before do
              subscription_feature_service.execute!
            end

            it 'adds an enterprise_productivity_pack addon' do
              assert account.subscription_feature_addons.count == 1
              assert account.subscription_feature_addons[0].name == 'enterprise_productivity_pack'
            end
          end
        end

        describe 'and the enterprise_productivity_pack is not enabled' do
          before do
            Zendesk::Features::Overrides::SimplePricePackaging.stubs(:changes_for).returns({ enterprise_productivity_pack: false })
            account.subscription_feature_addons.expects(:create!).never
          end

          it 'the enterprise_productivity_pack addon is not created' do
            subscription_feature_service.execute!
          end
        end

        describe 'and account services return a nil zuora_billing_id' do
          let(:zuora_billing_id) { nil }

          describe 'and the enterprise_productivity_pack is enabled' do
            before do
              Zendesk::Features::Overrides::SimplePricePackaging.stubs(:changes_for).returns({ enterprise_productivity_pack: true })
              account.subscription_feature_addons.expects(:create!).with(
                name: 'enterprise_productivity_pack',
                zuora_rate_plan_id: 1
              ).twice
            end

            it 'the enterprise_productivity_pack addon is created with a non-nil rate_plan_id' do
              subscription_feature_service.execute!
            end
          end

          describe 'and the enterprise_productivity_pack is not enabled' do
            before do
              Zendesk::Features::Overrides::SimplePricePackaging.stubs(:changes_for).returns({ enterprise_productivity_pack: false })
              account.subscription_feature_addons.expects(:create!).never
            end

            it 'the enterprise_productivity_pack addon is not created' do
              subscription_feature_service.execute!
            end
          end
        end
      end

      describe 'when an SPP addon is boosted' do
        before do
          Account.any_instance.stubs(:spp?).returns(true)
          Zendesk::Features::Overrides::SimplePricePackaging.stubs(:changes_for).returns({ high_volume_api: true })
          account.subscription_feature_addons.create!(boost_expires_at: 10.days.from_now.to_s, name: 'high_volume_api')
        end

        it 'adds the addon' do
          assert account.subscription_feature_addons.count == 1
          assert account.subscription_feature_addons[0].name == 'high_volume_api'
        end

        describe 'and then deactivated' do
          before do
            Zendesk::Features::Overrides::SimplePricePackaging.stubs(:changes_for).returns({ high_volume_api: false })
            subscription_feature_service.execute!
          end

          it 'removes the addon' do
            assert_empty account.subscription_feature_addons
          end
        end
      end

      describe 'when the acccount is not SPP' do
        before do
          Account.any_instance.stubs(:spp?).returns(false)
          ZendeskFeatureFramework::PlanChangeCalculator.expects(:new).returns(plan_change_calculator)
        end

        it 'does not apply the SPP changes' do
          plan_change_calculator.expects(:override).with(:foo, true).never
          plan_change_calculator.expects(:override).with(:bar, false).never
          subscription_feature_service.execute!
        end
      end
    end
  end

  describe '#create_sandbox_features!' do
    describe 'legacy sandbox PMR 2 plan_type 3' do
      let(:sandbox) { account.reset_sandbox }

      it 'has the same features as the master account' do
        assert_equal account.subscription.active_features.map(&:name), sandbox.subscription.active_features.map(&:name)
      end
    end

    describe 'patagonia sandbox PricingModelRevision::PATAGONIA SubscriptionPlanType.ExtraLarge' do
      let(:extra_large_account) do
        account.subscription.update_attributes(
          pricing_model_revision: ZBC::Zendesk::PricingModelRevision::PATAGONIA,
          plan_type: SubscriptionPlanType.ExtraLarge
        )
        account
      end

      let(:sandbox) { extra_large_account.reset_sandbox }

      it 'has the same features as the master account' do
        assert_equal account.subscription.active_features.map(&:name), sandbox.subscription.active_features.map(&:name)
      end
    end
  end
end
