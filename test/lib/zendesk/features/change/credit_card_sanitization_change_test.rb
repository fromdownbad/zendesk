require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe CreditCardSanitizationChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      before do
        @account.settings.credit_card_redaction = true
        @account.settings.save!
        Subscription.any_instance.expects(:has_credit_card_sanitization?).returns(true)
        @change = CreditCardSanitizationChange.new(@account.subscription)
      end

      it 'sets credit card redaction setting to false' do
        @account.subscription.expects(:has_credit_card_sanitization?).returns(false)
        @change.execute
        refute @account.settings.credit_card_redaction
      end
    end
  end
end
