require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

module Zendesk::Features::Change
  describe UnlimitedRulesChange do
    fixtures :subscriptions, :rules

    before do
      @subscription = subscriptions(:minimum)
      Subscription.any_instance.expects(:has_unlimited_rules?).returns(true)
      UnlimitedRulesChange.any_instance.stubs(:class_name).returns('Rule')
    end

    describe 'downgrade' do
      before do
        @change = UnlimitedRulesChange.new(@subscription)
      end

      it 'should run downgrade' do
        Subscription.any_instance.expects(:has_unlimited_rules?).returns(false)
        @change.expects(:deactivate_rules)
        @change.expects(:create_default_rules)
        @change.execute
      end
    end

    describe 'deactivate_rules' do
      it 'should deactivate all the accounts rules' do
        assert !@subscription.account.rules.active.empty?
        UnlimitedRulesChange.new(@subscription).send(:deactivate_rules)
        assert_empty(@subscription.account.rules.active)
      end
    end

    describe 'create_default_rules' do
      it 'should load default rules for the account' do
        Accounts::Base.any_instance.expects(:load_rule_class_from_yaml).with('Rule')
        UnlimitedRulesChange.new(@subscription).send(:create_default_rules)
      end
    end
  end
end
