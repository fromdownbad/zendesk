require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe TicketFormsChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'upgrade' do
      before do
        assert_empty @account.ticket_forms

        Subscription.any_instance.
          expects(:has_ticket_forms?).
          at_least_once.
          returns(false)
        @change = TicketFormsChange.new(@account.subscription)
      end

      it 'creates a default ticket_form' do
        @account.subscription.
          expects(:has_ticket_forms?).
          at_least_once.
          returns(true)
        @change.execute
        assert_equal 1, @account.ticket_forms.count(:all)
      end

      describe 'sandbox account' do
        before { @account.expects(:is_sandbox?).returns(true) }

        it 'does not create a ticket_form' do
          @account.subscription.
            expects(:has_ticket_forms?).
            at_least_once.
            returns(true)
          @change.execute
          TicketForm.expects(:init_default_form).with(@account).never
        end
      end

      describe 'existing default ticket_form on account' do
        before do
          TicketForm.init_default_form(@account).save!

          Subscription.any_instance.
            expects(:has_ticket_forms?).
            at_least_once.
            returns(false)
          @change = TicketFormsChange.new(@account.subscription)
        end

        it 'does not create a ticket_form' do
          @account.subscription.
            expects(:has_ticket_forms?).
            at_least_once.
            returns(true)
          @change.execute
          TicketForm.expects(:init_default_form).with(@account).never
        end
      end
    end
  end
end
