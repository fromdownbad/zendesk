require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

module Zendesk::Features::Change
  describe FeatureChange do
    fixtures :subscriptions

    class TestChange < FeatureChange
    end

    before do
      @subscription = subscriptions(:minimum)
    end

    describe 'upgrade' do
      before do
        Subscription.any_instance.expects(:has_test?).returns(false)
        @change = TestChange.new(@subscription)
      end

      it 'should run' do
        Subscription.any_instance.expects(:has_test?).returns(true)
        @change.expects(:upgrade)
        @change.expects(:downgrade).never
        @change.execute
      end
    end

    describe 'downgrade' do
      before do
        Subscription.any_instance.expects(:has_test?).returns(true)
        @change = TestChange.new(@subscription)
      end

      it 'should run' do
        Subscription.any_instance.expects(:has_test?).returns(false)
        @change.expects(:downgrade)
        @change.expects(:upgrade).never
        @change.execute
      end
    end

    describe 'no change' do
      [true, false].each do |has_feature|
        describe "if has_test returns #{has_feature}" do
          before do
            Subscription.any_instance.expects(:has_test?).returns(has_feature)
            @change = TestChange.new(@subscription)
          end

          it 'should run downgrade' do
            Subscription.any_instance.expects(:has_test?).returns(has_feature)
            @change.expects(:downgrade).never
            @change.expects(:upgrade).never
            @change.execute
          end
        end
      end
    end
  end
end
