require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe MultipleOrganizationsChange do
    fixtures :accounts

    let(:account) { accounts(:minimum) }
    let(:change)  { MultipleOrganizationsChange.new(account.subscription) }

    describe 'downgrade' do
      it 'should retrieve the latest version for the account' do
        account_id = account.id
        RemoveRegularOrganizationMembershipsJob.stubs(:enqueue)
        Account.expects(:find).with(account_id, nil).returns(account)
        change.downgrade
      end

      describe 'when the account has non-default organization memberships' do
        let(:organization_memberships) { stub(regular: [stub]) }

        before do
          account.stubs(organization_memberships: organization_memberships)
        end

        it 'enqueues RemoveRegularOrganizationMembershipsJob' do
          Account.stubs(find: account)
          RemoveRegularOrganizationMembershipsJob.expects(:enqueue).with(account.id)
          change.downgrade
        end
      end

      describe 'when the account does not have any non-default organization memberships' do
        let(:organization_memberships) { stub(regular: []) }

        before do
          account.stubs(organization_memberships: organization_memberships)
        end

        it 'does not enqueue RemoveRegularOrganizationMembershipsJob' do
          Account.stubs(find: account)
          RemoveRegularOrganizationMembershipsJob.expects(:enqueue).never
          change.downgrade
        end
      end
    end
  end
end
