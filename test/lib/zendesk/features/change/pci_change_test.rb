require_relative '../../../../support/test_helper'

SingleCov.covered!

module Zendesk::Features::Change
  describe PciChange do
    fixtures :accounts, :tickets

    let(:account)               { accounts(:minimum) }
    let(:subscription)          { account.subscription }

    subject { PciChange.new(subscription) }

    before do
      @ticket_field = FieldPartialCreditCard.create!(account: account, title: 'credit card field')
      @ticket_field_entry = TicketFieldEntry.create!(
        ticket_field: @ticket_field,
        ticket: tickets(:minimum_1),
        value: 'XXXXXXXXXXXX1234',
        account: account
      )
    end

    describe '#downgrade' do
      before do
        subject.downgrade
        @ticket_field.reload
        @ticket_field_entry.reload
      end

      it 'deactivates any credit card fields' do
        assert_equal false, @ticket_field.is_active
      end

      it "doesn't remove existing entries" do
        assert @ticket_field_entry
      end
    end

    describe '#upgrade' do
      before do
        subject.downgrade
        subject.upgrade
        @ticket_field.reload
        @ticket_field_entry.reload
      end

      it 'does not activate any credit card fields' do
        assert_equal false, @ticket_field.is_active
      end
    end
  end
end
