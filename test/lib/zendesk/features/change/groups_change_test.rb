require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Features::Change::GroupsChange do
  fixtures :subscriptions

  let(:subscription) { subscriptions(:minimum) }
  let(:account)      { subscription.account }
  let(:change)       { Zendesk::Features::Change::GroupsChange.new(subscription) }
  let(:expire_keys)  { %i[views rules rule_analysis] }
  let(:expiry)       { mock('Zendesk::RadarExpiry') }

  before do
    Zendesk::RadarExpiry.stubs(:get).with(account).returns(expiry)
    expiry.stubs(:expire_radar_cache_key)
    Arturo.enable_feature! :agent_workspace_for_support_only_trial
  end

  describe '#upgrade' do
    before { change.upgrade }

    before_should 'expire radar `views` cache key' do
      expiry.expects(:expire_radar_cache_key).with(:views)
    end

    before_should 'busts view cache' do
      account.expects(:expire_scoped_cache_key).at_least(3).with do |key|
        expire_keys.include? key
      end
    end

    before_should 'not log with append_attributes' do
      Rails.logger.expects(:append_attributes).never
    end

    describe 'for zuora managed account' do
      before_should 'queues job to update agent workspace availability' do
        account.stubs(:zuora_managed?).returns(true)
        SetAgentWorkspaceAvailabilityJob.expects(:enqueue).with(account.id, true)
      end

      before_should 'does not queue job if trial' do
        account.stubs(:zuora_managed?).returns(false)
        SetAgentWorkspaceAvailabilityJob.expects(:enqueue).never
      end
    end
  end

  describe '#downgrade' do
    before { change.downgrade }

    before_should 'expire radar `views` cache key' do
      expiry.expects(:expire_radar_cache_key).with(:views)
    end

    before_should 'enqueue DowngradeAgentGroupsAccesJob' do
      DowngradeAgentGroupsAccessJob.expects(:enqueue).with(account.id)
    end

    before_should 'enqueue AddAllAgentsToDefaultGroupJob' do
      AddAllAgentsToDefaultGroupJob.expects(:enqueue).with(account.id)
    end

    before_should 'busts view cache' do
      account.expects(:expire_scoped_cache_key).at_least(3).with do |key|
        expire_keys.include? key
      end
    end

    before_should 'not log with append_attributes' do
      Rails.logger.expects(:append_attributes).never
    end

    describe 'for zuora managed account' do
      before_should 'queues job to update agent workspace availability' do
        account.stubs(:zuora_managed?).returns(true)
        SetAgentWorkspaceAvailabilityJob.expects(:enqueue).with(account.id, false)
      end

      before_should 'does not queue job if trial' do
        account.stubs(:zuora_managed?).returns(false)
        SetAgentWorkspaceAvailabilityJob.expects(:enqueue).never
      end
    end
  end
end
