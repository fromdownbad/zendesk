require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe CustomerSatisfactionChange do
    fixtures :accounts, :rules

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      before do
        @account.settings.customer_satisfaction = true
        @account.settings.save!
        @csat_view = rules(:active_view_for_minimum_account)
        @csat_view.title = '{{zd.recently_rated_tickets}}'
        @csat_view.save!
        @change = CustomerSatisfactionChange.new(@account.subscription)
      end

      it 'deactivates customer satisfaction and view' do
        @change.downgrade
        Rails.cache.clear
        @account.reload
        refute @account.settings.customer_satisfaction?
        refute @account.views.where(title: '{{zd.recently_rated_tickets}}', is_active: true).first
      end
    end
  end
end
