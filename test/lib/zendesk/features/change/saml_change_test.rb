require_relative '../../../../support/test_helper'

SingleCov.covered!

module Zendesk::Features::Change
  describe SamlChange do
    fixtures :accounts, :remote_authentications

    let(:account)               { accounts(:minimum) }
    let(:subscription)          { account.subscription }
    let(:remote_authentication) { remote_authentications(:minimum_remote_authentication) }

    subject { SamlChange.new(subscription) }

    describe '#downgrade' do
      before do
        Account.any_instance.stubs(has_saml?: true, has_jwt?: true)

        remote_authentication.update_attributes!(
          auth_mode: RemoteAuthentication::SAML,
          account: account,
          is_active: true,
          fingerprint: '44:D2:9D:98:49:66:27:30:3A:67:A2:5D:97:62:31:65:57:9F:57:D1'
        )
        account.role_settings.update_attributes!(
          agent_remote_login: true,
          end_user_remote_login: true,
          agent_zendesk_login: false,
          end_user_zendesk_login: false
        )
      end

      it 'deactivates SAML if present' do
        assert account.remote_authentications.active.saml.first.is_active
        subject.downgrade
        account.reload

        refute account.remote_authentications.active.saml.first.present?, 'SAML should be deactivated'
      end

      describe 'with security_settings_for_all_accounts arturo on' do
        before { Arturo.enable_feature!(:security_settings_for_all_accounts) }

        it 'SAML is not deactivated' do
          assert account.remote_authentications.active.saml.first.is_active
          subject.downgrade
          account.reload

          assert account.remote_authentications.active.saml.first.is_active
        end
      end

      it "enables zendesk login if there are no other active remote authentications" do
        subject.downgrade
        account.reload

        assert_equal false, account.role_settings.agent_remote_login
        assert_equal false, account.role_settings.end_user_remote_login
        assert(account.role_settings.agent_zendesk_login)
        assert(account.role_settings.end_user_zendesk_login)
      end

      it "leaves remote authentication enabled if there are other active remote authentications" do
        remote_authentication.dup.update_attributes!(
          auth_mode: RemoteAuthentication::JWT,
          account: account,
          is_active: true
        )

        subject.downgrade
        account.reload

        assert(account.role_settings.agent_remote_login)
        assert_equal false, account.role_settings.agent_zendesk_login

        assert(account.role_settings.end_user_remote_login)
        assert_equal false, account.role_settings.end_user_zendesk_login
      end

      it "doesn't disable other logins methods" do
        account.role_settings.update_attributes!(
          agent_remote_login: true,
          end_user_remote_login: false,
          agent_zendesk_login: false,
          end_user_zendesk_login: false,
          end_user_google_login: true
        )
        subject.downgrade
        account.reload

        assert_equal false, account.role_settings.agent_remote_login
        assert(account.role_settings.agent_zendesk_login)

        assert(account.role_settings.end_user_google_login)
        assert_equal false, account.role_settings.end_user_zendesk_login
      end
    end
  end
end
