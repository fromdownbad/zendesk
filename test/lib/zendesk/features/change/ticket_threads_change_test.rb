require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe TicketThreadsChange do
    fixtures :all

    let(:account) { accounts(:minimum) }
    subject { TicketThreadsChange.new(account.subscription) }

    describe 'upgrade' do
      before do
        Subscription.any_instance.stubs(has_ticket_threads?: true)
        subject.upgrade
      end

      it 'activated the collaboration product on the account' do
        assert_requested(
          :patch,
          "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products/support?include_deleted_account=false",
          body: '{"product":{"plan_settings":{"side_conversations":true}}}'
        )
      end
    end

    describe 'downgrade' do
      before do
        Subscription.any_instance.stubs(has_ticket_threads?: false)
        subject.downgrade
      end

      it 'activated the collaboration product on the account' do
        assert_requested(
          :patch,
          "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products/support?include_deleted_account=false",
          body: '{"product":{"plan_settings":{"side_conversations":false}}}'
        )
      end
    end
  end
end
