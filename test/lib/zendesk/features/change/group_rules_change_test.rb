require_relative '../../../../support/test_helper'

SingleCov.covered!

module Zendesk::Features::Change
  describe GroupRulesChange do
    fixtures :accounts, :groups, :rules

    let(:account) { accounts(:minimum) }
    let(:group)   { groups(:minimum_group) }
    let(:rule)    { rules(:view_cloud) }

    subject       { GroupRulesChange.new(account.subscription) }

    describe '#downgrade' do
      before do
        Subscription.any_instance.expects(:has_group_rules?).returns(true)
        @subject = GroupRulesChange.new(account.subscription)
        rule.update_attributes!(owner_id: group.id, owner_type: 'Group')
      end

      it 'change any rules with a group owner to be owned by the account instead' do
        assert_equal group.id, rule.owner_id
        Subscription.any_instance.expects(:has_group_rules?).returns(false)
        @subject.execute
        assert_equal account.id, rule.reload.owner_id, 'the rule owner should be changed to the account'
      end
    end
  end
end
