require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe LightAgentsChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'upgrade' do
      before do
        assert_nil @account.permission_sets.find_by_role_type(PermissionSet::Type::LIGHT_AGENT)

        Subscription.any_instance.stubs(has_light_agents?: false)
        @change = LightAgentsChange.new(@account.subscription)
        AccountProductFeatureSyncJob.expects(:enqueue).with(@account.id, 'light_agents', 'light_agents')
      end

      it 'creates light agent permission set' do
        Subscription.any_instance.stubs(has_light_agents?: true)
        @change.execute
        refute_nil @account.permission_sets.find_by_role_type(PermissionSet::Type::LIGHT_AGENT)
      end
    end

    describe 'downgrade' do
      before do
        @permission_set = PermissionSet.create_light_agent!(@account)

        @user = @permission_set.users.create! do |user|
          user.name = "Ingo"
          user.email = "the.ingo@example.org"
          user.roles = Role::AGENT.id
          user.account = @account
        end

        Subscription.any_instance.stubs(has_light_agents?: true)
        @change = LightAgentsChange.new(@account.subscription)
        AccountProductFeatureSyncJob.expects(:enqueue).with(@account.id, 'light_agents', 'light_agents')
      end

      it 'downgrade light agents to end users' do
        Subscription.any_instance.stubs(has_light_agents?: false)
        @change.execute
        assert @user.reload.is_end_user?
      end

      it 'destroys the light agent permission set' do
        Subscription.any_instance.stubs(has_light_agents?: false)
        @change.execute
        assert_nil @account.reload.light_agent_permission_set
      end

      it 'does not error when Light Agent permission set does not exist' do
        Subscription.any_instance.stubs(has_light_agents?: false)
        @account.light_agent_permission_set.destroy
        @account.reload
        @change.execute
        assert_nil @account.reload.light_agent_permission_set
      end
    end
  end
end
