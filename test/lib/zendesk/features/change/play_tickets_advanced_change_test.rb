require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Features::Change::PlayTicketsAdvancedChange do
  fixtures :accounts,
    :users

  let(:account) { accounts(:minimum) }
  let(:agent)   { users(:minimum_agent) }

  let(:feature_change) do
    Zendesk::Features::Change::PlayTicketsAdvancedChange.new(
      account.subscription
    )
  end

  describe '#downgrade' do
    describe 'when downgrading the `view_access` permission' do
      let(:permission_set) do
        build_valid_permission_set(agent.account).tap(&:save)
      end

      before do
        agent.tap { |a| a.permission_set = permission_set }.save!

        permission_set.tap do |set|
          set.permissions.view_access = view_access_permission
        end.save!

        feature_change.downgrade

        permission_set.reload
      end

      describe 'and the permission is set to `playonly`' do
        let(:view_access_permission) { 'playonly' }

        it 'sets the `view_access` permission to `readonly`' do
          assert_equal 'readonly', permission_set.permissions.view_access
        end
      end

      describe 'and the permission is set to another value' do
        let(:view_access_permission) { 'full' }

        it 'leaves the permission untouched' do
          assert_equal 'full', permission_set.permissions.view_access
        end
      end
    end
  end
end
