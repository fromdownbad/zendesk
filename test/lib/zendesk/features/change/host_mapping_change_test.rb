require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe HostMappingChange do
    fixtures :accounts, :certificates

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      before do
        @change = HostMappingChange.new(@account.subscription)
        @account.subscription.expects(:has_host_mapping?).returns(false)
      end

      describe 'when host_mapping is nil' do
        it 'does not change host mapping on account' do
          @account.expects(:update_attribute).with(:host_mapping, nil).never
          @change.execute
          assert_nil @account.host_mapping
        end
      end

      describe 'when host_mapping exists' do
        before do
          @account.update_attribute(:host_mapping, "test")
        end

        it 'sets host mapping on account to nil' do
          @change.execute
          assert_nil @account.host_mapping
        end
      end

      describe 'with active certificates' do
        before do
          certificates(:active1).update_attribute(:account_id, @account.id)
        end

        it 'revokes the certificates' do
          @account.certificates.active.count(:all).must_equal 1
          @change.execute
          @account.certificates.active.count(:all).must_equal 0
          certificates(:active1).reload.state.must_equal 'revoked'
        end

        it 'removes the IPM' do
          cert = @account.certificates.active.first
          cert.create_ipm_alert!(text: 'your cert is expired')
          cert.save!
          refute_nil cert.reload.ipm_alert_id

          @change.execute

          assert_nil cert.reload.ipm_alert_id
        end
      end
    end
  end
end
