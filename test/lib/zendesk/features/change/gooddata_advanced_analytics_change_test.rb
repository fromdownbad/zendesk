require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe GooddataAdvancedAnalyticsChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      before do
        Subscription.any_instance.expects(:has_gooddata_advanced_analytics?).returns(true)
        @change = GooddataAdvancedAnalyticsChange.new(@account.subscription)
      end

      it 'destroys gooddata integration' do
        @account.subscription.expects(:has_gooddata_advanced_analytics?).returns(false)
        Zendesk::Gooddata::IntegrationProvisioning.expects(:destroy_for_account).with(@account)
        @change.execute
      end
    end
  end
end
