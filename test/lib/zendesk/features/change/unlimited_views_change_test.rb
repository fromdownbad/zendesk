require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Features::Change::UnlimitedViewsChange do
  fixtures :subscriptions, :translation_locales

  describe '#downgrade' do
    let(:subscription)  { subscriptions(:minimum) }
    let(:account)       { subscription.account }
    let(:locale)        { translation_locales(:spanish) }
    let(:views)         { account.views }
    let(:rules_file)    { File.open(::Accounts::Base::DEFAULT_RULES_PATH) }
    let(:default_rules) { YAML.load(rules_file) }

    let(:default_titles) do
      default_rules.each_with_object([]) do |rule, titles|
        if rule['class_name'] == 'View' && rule['attributes']['is_active'] == '1'
          titles << rule['attributes']['title']
        end
      end.sort
    end

    before do
      account.locale_id = locale.id
      account.save!

      Zendesk::Features::Change::UnlimitedViewsChange.new(
        subscription
      ).downgrade
    end

    it 'creates default views' do
      assert_equal default_titles, views.active.pluck(:title).sort
    end
  end
end
