require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe TalkCtiPartnerChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
      @change  = TalkCtiPartnerChange.new(@account.subscription)
      Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
    end

    describe 'upgrade' do
      describe "when account has no voice_partner_edition_account" do
        it 'creates a voice_partner_edition_account' do
          assert_difference 'Voice::PartnerEditionAccount.where(account_id: @account.id).count', 1 do
            @change.upgrade
          end
        end
      end

      describe "when account has voice_partner_edition_account" do
        it 'activates the existing voice_partner_edition_account' do
          FactoryBot.create(:voice_partner_edition_account, account_id: @account.id, active: false)

          @change.upgrade
          assert @account.voice_partner_edition_account.active
        end
      end
    end

    describe 'downgrade' do
      describe "when account has a regular active voice_partner_edition_account" do
        it 'should be marked as inactive' do
          FactoryBot.create(:voice_partner_edition_account, account_id: @account.id)

          @change.downgrade
          refute @account.voice_partner_edition_account.active
        end
      end

      describe "when account has a legacy active voice_partner_edition_account" do
        it 'should not be marked as inactive' do
          FactoryBot.create(
            :voice_partner_edition_account,
            account_id: @account.id,
            plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:legacy]
          )

          @change.downgrade
          assert @account.voice_partner_edition_account.active
        end
      end
    end
  end
end
