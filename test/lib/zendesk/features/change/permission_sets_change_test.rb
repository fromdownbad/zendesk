require_relative '../../../../support/test_helper'
require_relative '../../../../support/suite_test_helper'
require_relative '../../../../support/multiproduct_test_helper'

SingleCov.covered!

describe Zendesk::Features::Change::PermissionSetsChange do
  include MultiproductTestHelper
  include SuiteTestHelper
  fixtures :accounts, :users, :translation_locales

  let(:account) { accounts(:minimum) }
  let(:agent)   { users(:minimum_agent) }

  let(:feature_change) do
    Zendesk::Features::Change::PermissionSetsChange.new(account.subscription)
  end

  describe '#upgrade' do
    it 'add the default permission sets' do
      account.expects(:add_default_permission_sets)
      feature_change.upgrade
    end

    it 'syncs account talk entitlements' do
      Omnichannel::TalkAccountSyncJob.expects(:enqueue).with(account_id: account.id)
      feature_change.upgrade
    end

    it "uses the account's language settings for custom roles" do
      ::I18n.locale = translation_locales(:japanese)
      feature_change.upgrade
      assert_equal account.permission_sets.first.name, 'Advisor'
    end

    describe 'with existing permission sets' do
      let(:default_sets) { account.add_default_permission_sets }
      let(:light_agent) { PermissionSet.create_light_agent!(account) }

      before do
        default_sets
        light_agent
      end

      it 'syncs all default sets to Pravda' do
        feature_change.stubs(:update_pravda_agent_role)
        default_sets.each do |permission_set|
          Omnichannel::RoleSyncJob.expects(:enqueue).with(
            account_id: account.id,
            permission_set_id: permission_set.id,
            role_name: "custom_#{permission_set.id}",
            enabled: true,
            context: 'updated_plan'
          )
        end
        feature_change.upgrade
      end
    end
  end

  describe '#downgrade' do
    let!(:default_sets) { account.add_default_permission_sets }
    let!(:light_agent) { PermissionSet.create_light_agent!(account) }

    it 'calls the map_role_permissions_to_agents method on the subscription' do
      account.subscription.expects(:map_role_permissions_to_agents)

      feature_change.downgrade
    end

    it 'syncs account talk entitlements' do
      Omnichannel::TalkAccountSyncJob.expects(:enqueue).with(account_id: account.id)
      feature_change.downgrade
    end

    it 'syncs account guide entitlements' do
      Omnichannel::GuideAccountSyncJob.expects(:enqueue).with(account_id: account.id)
      feature_change.downgrade
    end

    it 'deactives default sets in Pravda' do
      feature_change.stubs(:update_pravda_agent_role)
      default_sets.each do |permission_set|
        Omnichannel::RoleSyncJob.expects(:enqueue).with(
          account_id: account.id,
          permission_set_id: permission_set.id,
          role_name: "custom_#{permission_set.id}",
          enabled: false,
          context: 'updated_plan'
        )
      end
      feature_change.downgrade
    end
  end
end
