require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe CommunityForumsChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      before do
        @change = CommunityForumsChange.new(@account.subscription)
      end

      it 'updates the forums display_type_id' do
        @account.subscription.expects(:has_community_forums?).returns(false)
        @change.execute
        @account.forums.each do |f|
          assert_equal ForumDisplayType::ARTICLES.id, f.display_type_id
        end
      end
    end
  end
end
