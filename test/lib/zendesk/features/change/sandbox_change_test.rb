require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe SandboxChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      before do
        Account.any_instance.stubs(:sandbox_master).returns(@account)
        assert @account.reset_sandbox
        @account.reload
        @change = SandboxChange.new(@account.subscription)
      end

      it 'destroys the sandbox' do
        @account.expects(:destroy_sandbox).once
        @change.downgrade
      end
    end
  end
end
