require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe CategorizedForumsChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      before do
        @change = CategorizedForumsChange.new(@account.subscription)
      end

      it 'destroys all categories on account' do
        @account.subscription.expects(:has_categorized_forums?).returns(false)
        @change.execute
        assert_empty(@account.categories)
      end
    end
  end
end
