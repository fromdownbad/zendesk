require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe MultibrandChange do
    fixtures :subscriptions

    before do
      @subscription = subscriptions(:minimum)
      Subscription.any_instance.stubs(:has_unlimited_multibrand?).returns(true)
      10.times { FactoryBot.create(:brand, account_id: @subscription.account_id) }
      @subscription.account.update_attributes(default_brand: @subscription.account.brands.active.last)
    end

    describe 'deactivate_all_brands' do
      before do
        @subscription.account.update_attributes(name: 'wombat')
        @subscription.account.update_attributes(default_brand: @subscription.account.route.brand)
        MultibrandChange.new(@subscription).send(:deactivate_all_brands)
      end

      it 'should deactivate all but 1 brand' do
        assert_equal 1, @subscription.account.brands.active.count(:all)
      end

      it 'should keep the agent route brand activated' do
        assert_equal @subscription.account.route.brand, @subscription.account.brands.active.first
      end

      it 'should name the account the same as the agent route brand if the account name was different' do
        assert_equal @subscription.account.route.brand.name, @subscription.account.name
      end
    end

    describe 'help center state adjustment' do
      before do
        @subscription.account.update_attributes(default_brand: @subscription.account.route.brand)
      end

      describe 'when deactivating brands' do
        # in order of precedence:
        it 'enabled account Help Center state when a brand HC is enabled' do
          HelpCenter.stubs(:find_by_brand_id).returns(nil)
          HelpCenter.
            stubs(:find_by_brand_id).
            with(@subscription.account.route.brand.id).
            returns(HelpCenter.new(id: 42, state: 'enabled'))

          MultibrandChange.new(@subscription).send(:deactivate_all_brands)

          assert_equal :enabled, @subscription.account.help_center_state
        end

        it 'restricts account Help Center state when a brand HC is restricted' do
          HelpCenter.stubs(:find_by_brand_id).returns(nil)
          HelpCenter.
            stubs(:find_by_brand_id).
            with(@subscription.account.route.brand.id).
            returns(HelpCenter.new(id: 42, state: 'restricted'))

          MultibrandChange.new(@subscription).send(:deactivate_all_brands)

          assert_equal :restricted, @subscription.account.help_center_state
        end

        it 'disables account Help Center state when a brand HC is archived' do
          HelpCenter.stubs(:find_by_brand_id).returns(nil)
          HelpCenter.
            stubs(:find_by_brand_id).
            with(@subscription.account.route.brand.id).
            returns(HelpCenter.new(id: 42, state: 'archived'))

          MultibrandChange.new(@subscription).send(:deactivate_all_brands)

          assert_equal :disabled, @subscription.account.help_center_state
        end
      end

      describe 'when upgrading feature' do
        before do
          @subscription.account.update_attributes(default_brand: @subscription.account.route.brand)
          MultibrandChange.new(@subscription).send(:deactivate_all_brands)
        end

        # in order of precedence:
        it 'enabled account Help Center state when a brand HC is enabled' do
          HelpCenter.stubs(:find_by_brand_id).returns(nil)
          HelpCenter.
            stubs(:find_by_brand_id).
            with(@subscription.account.brands.first.id).
            returns(HelpCenter.new(id: 42, state: 'enabled'))

          MultibrandChange.new(@subscription).send(:upgrade)

          assert_equal :enabled, @subscription.account.help_center_state
        end

        it 'restricts account Help Center state when a brand HC is restricted' do
          HelpCenter.stubs(:find_by_brand_id).returns(nil)
          HelpCenter.
            stubs(:find_by_brand_id).
            with(@subscription.account.brands.second.id).
            returns(HelpCenter.new(id: 42, state: 'restricted'))

          MultibrandChange.new(@subscription).send(:upgrade)

          assert_equal :restricted, @subscription.account.help_center_state
        end

        it 'disables account Help Center state when all brand HCs is archived' do
          HelpCenter.stubs(:find_by_brand_id).returns(nil)
          HelpCenter.
            stubs(:find_by_brand_id).
            with(@subscription.account.brands.third.id).
            returns(HelpCenter.new(id: 42, state: 'archived'))

          MultibrandChange.new(@subscription).send(:upgrade)

          assert_equal :disabled, @subscription.account.help_center_state
        end
      end
    end

    describe 'fix_agent_route' do
      it 'should set the default brand to the agent route brand' do
        refute_equal @subscription.account.default_brand, @subscription.account.route.brand
        MultibrandChange.new(@subscription).send(:fix_agent_route)
        assert_equal @subscription.account.default_brand, @subscription.account.route.brand
      end
    end
  end
end
