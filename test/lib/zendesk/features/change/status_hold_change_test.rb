require_relative '../../../../support/test_helper'

SingleCov.covered!

module Zendesk::Features::Change
  describe StatusHoldChange do
    fixtures :accounts
    let(:account) { accounts(:minimum) }
    subject { StatusHoldChange.new(account.subscription) }

    describe '#downgrade' do
      it 'enqueue a job to open tickets previously on hold' do
        account.subscription.expects(:has_status_hold?).returns(false)
        OpenTicketsOnHoldJob.expects(:enqueue).with(account.id)
        subject.execute
      end
    end
  end
end
