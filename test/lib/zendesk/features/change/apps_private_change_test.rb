require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe AppsPrivateChange do
    fixtures :subscriptions

    before do
      @subscription = subscriptions(:minimum)
      @change = AppsPrivateChange.new(@subscription)
      Rails.logger.stubs(:info)
    end

    describe 'upgrade' do
      it 'logs change' do
        Rails.logger.expects(:info).with("Account #{@subscription.account.id} gaining Apps private feature")
        @change.upgrade
      end

      it 'enqueues job to update feature on apps market' do
        UpdateAppsFeatureJob.expects(:enqueue).with(@subscription.account.id, 'apps_private', false, true)
        @change.upgrade
      end
    end

    describe 'downgrade' do
      it 'logs change' do
        Rails.logger.expects(:info).with("Account #{@subscription.account.id} losing Apps private feature")
        @change.downgrade
      end

      it 'enqueues job to update feature on apps market' do
        UpdateAppsFeatureJob.expects(:enqueue).with(@subscription.account.id, 'apps_private', true, false)
        @change.downgrade
      end
    end
  end
end
