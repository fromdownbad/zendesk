require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe UnlimitedAutomationsChange do
    fixtures :subscriptions

    before do
      @subscription = subscriptions(:minimum)
    end

    describe 'downgrade' do
      before do
        Subscription.any_instance.expects(:has_unlimited_automations?).returns(true)
        @change = UnlimitedAutomationsChange.new(@subscription)
      end

      it 'should run downgrade and end up with default automations' do
        Subscription.any_instance.expects(:has_unlimited_automations?).returns(false)
        @change.execute
        YAML.load(File.open(::Accounts::Base::DEFAULT_RULES_PATH)).each do |rule|
          next unless rule['class_name'] == 'Automation' && rule['attributes']['is_active'] == '1'
          assert @subscription.account.automations.active.where(title: I18n.t(rule['attributes']['title'])).first
        end
      end
    end
  end
end
