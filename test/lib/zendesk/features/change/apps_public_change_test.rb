require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe AppsPublicChange do
    fixtures :subscriptions

    before do
      @subscription = subscriptions(:minimum)
      @change = AppsPublicChange.new(@subscription)
      Rails.logger.stubs(:info)
    end

    describe 'upgrade' do
      it 'logs change' do
        Rails.logger.expects(:info).with("Account #{@subscription.account.id} gaining Apps public feature")
        @change.upgrade
      end

      it 'enqueues job to update feature on apps market' do
        UpdateAppsFeatureJob.expects(:enqueue).with(@subscription.account.id, 'apps_public', false, true)
        @change.upgrade
      end
    end

    describe 'downgrade' do
      it 'logs change' do
        Rails.logger.expects(:info).with("Account #{@subscription.account.id} losing Apps public feature")
        @change.downgrade
      end

      it 'enqueues job to update feature on apps market' do
        UpdateAppsFeatureJob.expects(:enqueue).with(@subscription.account.id, 'apps_public', true, false)
        @change.downgrade
      end
    end
  end
end
