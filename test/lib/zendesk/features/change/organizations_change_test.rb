require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe OrganizationsChange do
    fixtures :subscriptions

    before do
      @subscription = subscriptions(:minimum)
      Subscription.any_instance.expects(:has_organizations?).returns(true)
    end

    describe 'downgrade' do
      before do
        @change = OrganizationsChange.new(@subscription)
      end

      it 'enqueues job to downgrade agent and end user organization access' do
        Subscription.any_instance.expects(:has_organizations?).returns(false)
        DowngradeOrganizationsAccessJob.expects(:enqueue).with(@subscription.account.id)
        @change.execute
      end
    end
  end
end
