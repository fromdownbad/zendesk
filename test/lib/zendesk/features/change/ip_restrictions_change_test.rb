require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe IpRestrictionsChange do
    fixtures :accounts

    describe 'downgrade' do
      before do
        account.settings.ip_restriction_enabled = true
        account.settings.save!
        assert account.settings.ip_restriction_enabled?
        Subscription.any_instance.expects(:has_ip_restrictions?).returns(true)
        @change = IpRestrictionsChange.new(account.subscription)
      end

      describe 'non-multiproduct account' do
        let(:account) { accounts(:minimum) }

        it 'sets ip restrictions enabled setting to false' do
          account.subscription.expects(:has_ip_restrictions?).returns(false)
          @change.execute
          refute account.settings.ip_restriction_enabled?
        end

        describe 'with security_settings_for_all_accounts arturo on' do
          before { Arturo.enable_feature!(:security_settings_for_all_accounts) }

          it 'does not set ip restrictions enabled setting to false' do
            account.subscription.expects(:has_ip_restrictions?).returns(false)
            @change.execute
            assert account.settings.ip_restriction_enabled?
          end
        end
      end

      describe 'multiproduct account' do
        let(:account) { accounts(:multiproduct) }

        describe 'has ip_restriction feature' do
          before { account.stubs(:has_ip_restriction?).returns(true) }

          it 'does not set ip restrictions enabled setting to false' do
            account.subscription.expects(:has_ip_restrictions?).returns(false)
            @change.execute
            assert account.settings.ip_restriction_enabled?
          end
        end

        describe 'does not have ip_restriction feature' do
          before { account.stubs(:has_ip_restriction?).returns(false) }

          it 'sets ip restrictions enabled setting to false' do
            account.subscription.expects(:has_ip_restrictions?).returns(false)
            @change.execute
            refute account.settings.ip_restriction_enabled?
          end

          describe 'with security_settings_for_all_accounts arturo on' do
            before { Arturo.enable_feature!(:security_settings_for_all_accounts) }

            it 'does not set ip restrictions enabled setting to false' do
              account.subscription.expects(:has_ip_restrictions?).returns(false)
              @change.execute
              assert account.settings.ip_restriction_enabled?
            end
          end
        end
      end
    end
  end
end
