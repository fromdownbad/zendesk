require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe IndividualLanguageSelectionChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      it 'enqueues the reset end users locale job' do
        @change = IndividualLanguageSelectionChange.new(@account.subscription)
        ResetEndUsersLocaleJob.expects(:enqueue).with(@account.id)
        @change.downgrade
      end
    end
  end
end
