require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe CustomSessionTimeoutChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      before do
        @change = CustomSessionTimeoutChange.new(@account.subscription)
      end

      describe 'when session timeout is not default' do
        before do
          @account.settings.session_timeout = 5
          @account.settings.save

          refute @account.settings.session_timeout == @account.settings.default(:session_timeout)
        end

        it 'sets session timeout to default' do
          @change.downgrade
          assert_equal @account.settings.session_timeout, @account.settings.default(:session_timeout)
        end

        describe 'with security_settings_for_all_accounts arturo on' do
          before { Arturo.enable_feature!(:security_settings_for_all_accounts) }

          it 'session timeout remains non default' do
            @change.downgrade
            refute @account.settings.session_timeout == @account.settings.default(:session_timeout)
          end
        end
      end
    end
  end
end
