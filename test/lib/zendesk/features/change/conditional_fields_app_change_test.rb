require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe ConditionalFieldsAppChange do
    fixtures :subscriptions

    before do
      @subscription = subscriptions(:minimum)
      @change = ConditionalFieldsAppChange.new(@subscription)
      Rails.logger.stubs(:info)
    end

    describe 'upgrade' do
      it 'logs change' do
        Rails.logger.expects(:info).with("Account #{@subscription.account.id} gaining Conditional fields app feature")
        @change.upgrade
      end

      it 'enqueues job to update feature on apps market' do
        UpdateAppsFeatureJob.expects(:enqueue).with(@subscription.account.id, 'conditional_fields_app', false, true)
        @change.upgrade
      end
    end

    describe 'downgrade' do
      it 'logs change' do
        Rails.logger.expects(:info).with("Account #{@subscription.account.id} losing Conditional fields app feature")
        @change.downgrade
      end

      it 'enqueues job to update feature on apps market' do
        UpdateAppsFeatureJob.expects(:enqueue).with(@subscription.account.id, 'conditional_fields_app', true, false)
        @change.downgrade
      end
    end
  end
end
