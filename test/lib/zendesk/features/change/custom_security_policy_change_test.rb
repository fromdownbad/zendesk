require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe CustomSecurityPolicyChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      before do
        @change = CustomSecurityPolicyChange.new(@account.subscription)
      end

      describe 'when agent has custom security policy' do
        before do
          @account.role_settings.update_attribute(:agent_security_policy_id, Zendesk::SecurityPolicy::Custom.id)
          assert_equal Zendesk::SecurityPolicy::Custom.id, @account.role_settings.agent_security_policy_id
        end

        describe 'with security_settings_for_all_accounts arturo on' do
          before { Arturo.enable_feature!(:security_settings_for_all_accounts) }

          it 'policy remains custom' do
            @change.downgrade
            assert_equal Zendesk::SecurityPolicy::Custom.id, @account.role_settings.agent_security_policy_id
          end
        end

        describe 'with security_settings_for_all_accounts arturo off' do
          before { Arturo.disable_feature!(:security_settings_for_all_accounts) }

          it 'policy changes to high' do
            @change.downgrade
            assert_equal Zendesk::SecurityPolicy::High.id, @account.role_settings.agent_security_policy_id
          end
        end
      end

      describe 'when agent security policy level is custom' do
        let(:agent_security_policy) { stub(level?: :custom) }
        before do
          @account.expects(:agent_security_policy).returns(agent_security_policy)
        end

        it 'sets customer satisfaction enabled setting to false' do
          @change.downgrade
          assert_equal Zendesk::SecurityPolicy::High.id, @account.role_settings.agent_security_policy_id
        end
      end

      describe 'when end user security policy leve is custom' do
        let(:end_user_security_policy) { stub(level?: :custom) }
        before do
          @account.expects(:end_user_security_policy).returns(end_user_security_policy)
        end

        it 'sets customer satisfaction enabled setting to false' do
          @change.downgrade
          assert_equal Zendesk::SecurityPolicy::High.id, @account.role_settings.end_user_security_policy_id
        end
      end
    end
  end
end
