require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe IndividualTimeZoneSelectionChange do
    fixtures :accounts

    before do
      @account = accounts(:minimum)
    end

    describe 'downgrade' do
      it 'enqueues the align user time zones job' do
        @change = IndividualTimeZoneSelectionChange.new(@account.subscription)
        AlignUserTimeZonesJob.expects(:enqueue).with(@account.id)
        @change.downgrade
      end
    end
  end
end
