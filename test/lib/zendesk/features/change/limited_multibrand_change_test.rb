require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe LimitedMultibrandChange do
    fixtures :subscriptions

    before do
      @subscription = subscriptions(:minimum)
      Subscription.any_instance.stubs(:has_limited_multibrand?).returns(true)
      3.times { FactoryBot.create(:brand, account_id: @subscription.account_id) }
    end

    describe 'downgrade' do
      describe 'for accounts losing limited_multibrand' do
        before do
          @change = LimitedMultibrandChange.new(@subscription)
          Subscription.any_instance.expects(:has_limited_multibrand?).returns(false)
        end

        it 'should run downgrade' do
          @change.expects(:fix_agent_route)
          @change.expects(:deactivate_all_brands)
          @change.execute
        end

        it 'should run downgrade and end up with only 1 active brand which is the default and agent brand' do
          assert @subscription.account.brands.active.count(:all) > 1
          @change.execute
          assert_equal 1, @subscription.account.brands.active.count(:all)
          assert_equal @subscription.account.route.brand, @subscription.account.brands.active.first
          assert_equal @subscription.account.default_brand, @subscription.account.brands.active.first
        end

        it 'should not deactivate any brands if the account is also gaining unlimited_multibrand' do
          Subscription.any_instance.expects(:has_unlimited_multibrand?).returns(true)
          active_brands = @subscription.account.brands.active
          @change.expects(:fix_agent_route).never
          @change.expects(:deactivate_all_brands).never
          @change.execute
          assert_equal active_brands.count(:all), @subscription.account.brands.active.count(:all)
        end
      end
    end
  end
end
