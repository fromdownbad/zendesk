require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe UnlimitedMultibrandChange do
    fixtures :subscriptions

    before do
      @subscription = subscriptions(:minimum)
      Subscription.any_instance.stubs(:has_unlimited_multibrand?).returns(true)
      10.times { FactoryBot.create(:brand, account_id: @subscription.account_id) }
      @subscription.account.update_attributes(default_brand: @subscription.account.brands.active.last)
    end

    describe 'downgrade' do
      describe 'for accounts with limited_multibrand and unlimited_multibrand features' do
        before do
          @subscription.stubs(:has_limited_multibrand?).returns(true)
          @change = UnlimitedMultibrandChange.new(@subscription)
        end

        it 'should run downgrade' do
          Subscription.any_instance.expects(:has_unlimited_multibrand?).returns(false)
          @change.expects(:deactivate_extra_brands)
          @change.execute
        end

        it 'should run downgrade and end up with only 5 active brands' do
          Subscription.any_instance.expects(:has_unlimited_multibrand?).returns(false)
          assert @subscription.account.brands.active.count(:all) > 5
          @change.execute
          assert_equal 5, @subscription.account.brands.active.count(:all)
        end
      end

      describe 'for accounts with unlimited_multibrand and without limited_multibrand' do
        before do
          @subscription.stubs(:has_limited_multibrand?).returns(false)
          @change = UnlimitedMultibrandChange.new(@subscription)
        end

        it 'should run downgrade' do
          Subscription.any_instance.expects(:has_unlimited_multibrand?).returns(false)
          @change.expects(:fix_agent_route)
          @change.expects(:deactivate_all_brands)
          @change.execute
        end

        it 'should run downgrade and end up with only 1 active brand' do
          Subscription.any_instance.expects(:has_unlimited_multibrand?).returns(false)
          assert @subscription.account.brands.active.count(:all) > 5
          @change.execute
          assert_equal 1, @subscription.account.brands.active.count(:all)
        end
      end
    end

    describe 'deactivate_extra_brands' do
      before do
        UnlimitedMultibrandChange.new(@subscription).send(:deactivate_extra_brands)
      end

      it 'should deactivate all but 5 brands' do
        assert_equal 5, @subscription.account.brands.active.count(:all)
      end

      it 'should not deactivate the agent brand' do
        assert_includes @subscription.account.brands.active, @subscription.account.route.brand
      end

      it 'should not deactivate the default brand' do
        assert_includes @subscription.account.brands.active, @subscription.account.default_brand
      end

      it 'should keep active the 5 oldest brands including agent and default brands' do
        oldest = @subscription.account.brands.sort_by(&:created_at).slice(0..2)
        oldest.each do |brand|
          assert_includes @subscription.account.brands.active, brand
        end
        assert_includes @subscription.account.brands.active, @subscription.account.default_brand
        assert_includes @subscription.account.brands.active, @subscription.account.route.brand
      end
    end
  end
end
