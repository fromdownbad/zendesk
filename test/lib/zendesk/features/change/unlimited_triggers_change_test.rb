require_relative "../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Features::Change
  describe UnlimitedTriggersChange do
    fixtures :subscriptions

    def trigger_uses_group?(rule)
      rule['attributes']['definition'].conditions_all.map(&:source).include?('group_id')
    end

    before do
      @subscription = subscriptions(:minimum)
      @account = @subscription.account
      @change = UnlimitedTriggersChange.new(@subscription)
    end

    describe 'downgrade' do
      before do
        Subscription.any_instance.stubs(:has_unlimited_triggers?).returns(false)
        Subscription.any_instance.stubs(:has_groups?).returns(false)
      end

      it 'should run downgrade' do
        @change.expects(:deactivate_rules)
        @change.expects(:create_default_rules)
        @change.execute
      end

      it 'should run downgrade and end up with default triggers' do
        @change.execute
        YAML.load(File.open(::Accounts::Base::DEFAULT_RULES_PATH)).each do |rule|
          next unless rule['class_name'] == 'Trigger' && rule['attributes']['is_active'] == '1'
          next if rule['class_name'] == 'Trigger' && trigger_uses_group?(rule)
          assert @account.triggers.active.where(title: I18n.t(rule['attributes']['title'])).first
        end
      end

      it 'does not add inactive triggers' do
        count_of_triggers_before_downgrade = @account.triggers.count
        @change.execute
        assert_equal count_of_triggers_before_downgrade, @account.triggers.inactive.count
      end
    end
  end
end
