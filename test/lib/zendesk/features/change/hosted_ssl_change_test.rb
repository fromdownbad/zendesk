require_relative "../../../../support/test_helper"

SingleCov.covered!
module Zendesk::Features::Change
  describe HostedSslChange do
    fixtures :accounts, :certificates, :certificate_ips

    let(:account) { accounts(:minimum) }
    let(:active_certificate) { account.certificates.active.first }
    let(:change) { HostedSslChange.new(account.subscription) }

    describe '#upgrade' do
      before do
        assert_empty account.certificates.active
        change.upgrade
      end

      it "does nothing" do
        assert_empty account.certificates.active.reload
      end
    end

    describe '#downgrade' do
      describe 'with an active ip hosted certificate' do
        before do
          certificates(:active1).update_attribute(:account_id, account.id)
          refute_nil active_certificate
          assert_equal false, active_certificate.sni_enabled
          refute_empty active_certificate.certificate_ips
        end

        it 'converts the certificate to SNI' do
          change.downgrade

          assert_equal 'active', active_certificate.reload.state

          assert(active_certificate.sni_enabled)
          assert_equal 1, active_certificate.certificate_ips.size
          assert(active_certificate.certificate_ips[0].sni)
          assert_nil active_certificate.certificate_ips[0].ip
        end
      end

      describe 'with an active sni hosted certificate' do
        before do
          certificates(:active_sni).update_attribute(:account_id, account.id)
          refute_nil active_certificate

          change.downgrade
        end

        it 'does nothing' do
          assert_equal 'active', active_certificate.reload.state
          assert(active_certificate.sni_enabled)
        end
      end

      describe 'without a certificate' do
        before do
          assert_empty account.certificates.active
          change.downgrade
        end

        it 'does nothing' do
          assert_empty account.certificates.active.reload
        end
      end
    end
  end
end
