require_relative '../../../../support/test_helper'
require_relative '../../../../support/features_helpers'

SingleCov.covered!

module Zendesk::Features::Overrides
  describe SimplePricePackaging do
    let(:account) { accounts(:minimum) }
    before do
      account.stubs(:spp?).returns(true)
    end

    describe 'getting state of addons from Account service' do
      let(:current_features_from_account_service) do
        {
          enterprise_productivity_pack: true,
          high_volume_api:              false,
          light_agents:                 false,
          limited_multibrand:           true,
          hipaa:                        false,
          skill_based_ticket_routing:   true,
          ticket_threads:               true,
          unlimited_multibrand:         false,
          us_data_center:               true
        }
      end
      let(:current_features_in_classic) do
        [
          :light_agents,
          :skill_based_ticket_routing,
          :ticket_threads
        ]
      end
      let(:expected_changes) do
        {
          enterprise_productivity_pack: true,
          high_volume_api: false,
          light_agents: false,
          limited_multibrand: true,
          hipaa: false,
          unlimited_multibrand: false,
          us_data_center: true
        }
      end
      before do
        Zendesk::Features::Overrides::SimplePricePackaging.stubs(:current_features_in_classic).returns(current_features_in_classic)
        Zendesk::Features::Overrides::SimplePricePackaging.stubs(:current_features_from_account_service).returns(current_features_from_account_service)
      end

      it 'calculates actual changes to state of features in Classic' do
        assert_equal expected_changes, Zendesk::Features::Overrides::SimplePricePackaging.changes_for(account)
      end

      describe 'features in account service and Classic but proposed changes turn feature off' do
        let(:current_features_from_account_service) do
          {
            limited_multibrand: true
          }
        end
        let(:proposed_changes) do
          {
            limited_multibrand: false
          }
        end
        let(:current_features_in_classic) do
          [
            :limited_multibrand
          ]
        end
        let(:expected_changes) do
          {
            limited_multibrand: true
          }
        end

        it 'calculates actual changes to state of features in Classic' do
          assert_equal expected_changes, Zendesk::Features::Overrides::SimplePricePackaging.changes_for(account, proposed_features: proposed_changes)
        end
      end
    end

    describe '.changes_for' do
      let(:actual_result) { Zendesk::Features::Overrides::SimplePricePackaging.changes_for(account) }
      let(:products) do
        [
          Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed')
        ]
      end

      before do
        account.stubs(:products).returns(products)
      end

      describe 'no addons' do
        let(:products) do
          [
            Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed')
          ]
        end

        it 'sets everything to disabled' do
          assert_empty actual_result, "No features should be overriden"
        end
      end

      describe 'skills based routing' do
        let(:feature_override) { 'skill_based_ticket_routing' }
        let(:active) { true }
        let(:products) do
          [
            Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed'),
            Zendesk::Accounts::Product.new(name: 'skills_based_routing', state: 'subscribed', active: active)
          ]
        end

        it 'grants the addon' do
          assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
        end

        describe 'expired' do
          let(:active) { false }

          it 'disables the addon' do
            refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
          end
        end
      end

      describe 'ticket threads' do
        let(:feature_override) { 'ticket_threads' }
        let(:active) { true }
        let(:products) do
          [
            Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed'),
            Zendesk::Accounts::Product.new(name: 'side_conversations', state: 'subscribed', active: active)
          ]
        end

        it 'grants the addon' do
          assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
        end

        describe 'expired' do
          let(:active) { false }

          it 'disables the addon' do
            refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
          end
        end
      end

      describe 'light agents' do
        let(:feature_override) { 'light_agents' }
        let(:active) { true }
        let(:products) do
          [
            Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed'),
            Zendesk::Accounts::Product.new(name: 'light_agents', state: 'subscribed', active: active)
          ]
        end

        it 'grants the addon' do
          assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
        end

        describe 'expired' do
          let(:active) { false }

          it 'disables the addon' do
            refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
          end
        end
      end

      describe 'data center locality' do
        let(:active) { true }
        let(:products) do
          [
            Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed'),
            Zendesk::Accounts::Product.new(name: 'data_locality', state: 'subscribed', active: active, plan_settings: {datacenter_location: dc_location})
          ]
        end

        describe 'us location' do
          let(:feature_override) { 'us_data_center' }
          let(:dc_location) { 'data-locality-us' }

          it 'grants the addon' do
            assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
          end

          describe 'expired' do
            let(:active) { false }

            it 'disables the addon' do
              refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
            end
          end
        end

        describe 'eu location' do
          let(:feature_override) { 'eu_data_center' }
          let(:dc_location) { 'data-locality-eu' }

          it 'grants the addon' do
            assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
          end

          describe 'expired' do
            let(:active) { false }

            it 'disables the addon' do
              refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
            end
          end
        end

        describe 'germany location' do
          let(:feature_override) { 'germany_data_center' }
          let(:dc_location) { 'data-locality-germany' }

          it 'grants the addon' do
            assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
          end

          describe 'expired' do
            let(:active) { false }

            it 'disables the addon' do
              refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
            end
          end
        end
      end

      describe 'multibrand' do
        let(:active) { true }
        let(:products) do
          [
            Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed'),
            Zendesk::Accounts::Product.new(name: 'multibrand', state: 'subscribed', active: active, plan_settings: {max_brands: max_brands})
          ]
        end

        describe 'unlimited' do
          let(:feature_override) { 'unlimited_multibrand' }
          let(:max_brands) { 300 }

          it 'grants the addon' do
            assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
          end

          describe 'expired' do
            let(:active) { false }

            it 'disables the addon' do
              refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
            end
          end
        end

        describe 'limited' do
          let(:feature_override) { 'limited_multibrand' }
          let(:max_brands) { 5 }

          it 'grants the addon' do
            assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
          end

          describe 'expired' do
            let(:active) { false }

            it 'disables the addon' do
              refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
            end
          end
        end
      end

      describe 'hipaa' do
        let(:feature_override) { 'hipaa' }
        let(:active) { true }
        let(:products) do
          [
            Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed'),
            Zendesk::Accounts::Product.new(name: 'hipaa', state: 'subscribed', active: active)
          ]
        end

        it 'grants the addon' do
          assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
        end

        describe 'expired' do
          let(:active) { false }

          it 'disables the addon' do
            refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
          end
        end
      end

      describe 'productivity pack' do
        let(:feature_override) { 'enterprise_productivity_pack' }
        let(:active) { true }
        let(:products) do
          [
            Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed'),
            Zendesk::Accounts::Product.new(name: 'productivity_pack', state: 'subscribed', active: active)
          ]
        end

        it 'grants the addon' do
          assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
        end

        describe 'expired' do
          let(:active) { false }

          it 'disables the addon' do
            refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
          end
        end
      end

      describe 'high volume api' do
        let(:feature_override) { 'high_volume_api' }
        let(:active) { true }
        let(:products) do
          [
            Zendesk::Accounts::Product.new(name: 'support', state: 'subscribed'),
            Zendesk::Accounts::Product.new(name: 'high_volume_api', state: 'subscribed', active: active)
          ]
        end

        it 'grants the addon' do
          assert actual_result[feature_override.to_sym], "#{feature_override} was not enabled"
        end

        describe 'expired' do
          let(:active) { false }

          it 'disables the addon' do
            refute actual_result[feature_override.to_sym], "#{feature_override} was not disabled"
          end
        end
      end

      describe 'not an SP&P account' do
        before do
          account.stubs(:spp?).returns(false)
        end

        it 'sets everything to disabled' do
          assert_empty actual_result
        end
      end
    end
  end
end
