require_relative '../../support/test_helper'
require 'maxminddb'

SingleCov.covered!

describe Zendesk::GeoLocation do
  fixtures :accounts, :users

  describe 'configuration' do
    it 'has configuration for geo location data file downdload URL' do
      assert_not_nil Zendesk::Configuration.dig!(:geo_location, :download_url)
    end
  end

  describe '.update_data!' do
    let(:download_url) { Zendesk::Configuration.dig!(:geo_location, :download_url) }

    before do
      payload = "some bytes"
      str_out = StringIO.new
      gz_out = Zlib::GzipWriter.new(str_out)
      gz_out.write(payload)
      gz_out.finish
      compressed_bytes = str_out.string
      stub_request(:get, download_url).to_return(body: compressed_bytes)
      str_out.rewind
      @gz_reader = Zlib::GzipReader.wrap(str_out)
      StringIO.any_instance.stubs(:unlink)
    end

    it 'downloads the latest gzipped db and writes it to the data file' do
      Zlib::GzipReader.expects(:wrap).returns(@gz_reader)
      Gem::Package::TarReader.expects(:new).with(@gz_reader)
      Zendesk::GeoLocation.update_data!
    end
  end

  describe '.locate' do
    describe 'on locatable IPs' do
      it 'returns location data' do
        location = Zendesk::GeoLocation.locate('98.210.110.15')
        assert_equal(
          {
            city: 'San Francisco',
            country_code: 'US',
            country: 'United States',
            description: 'San Francisco, CA, United States',
            region: :us,
            state: 'CA',
            timezone: 'America/Los_Angeles'
          },
          location.except(:latitude, :longitude)
        )
        assert_in_delta 37.758700000000005, location[:latitude], 0.1
        assert_in_delta -122.43809999999999, location[:longitude], 0.1
      end
    end

    describe 'with unsanitized IP addresses' do
      it 'sanitizes IPs and returns location data' do
        location = Zendesk::GeoLocation.locate('098.210.110.15')
        assert_equal(
          {
            city: 'San Francisco',
            country_code: 'US',
            country: 'United States',
            description: 'San Francisco, CA, United States',
            region: :us,
            state: 'CA',
            timezone: 'America/Los_Angeles'
          },
          location.except(:latitude, :longitude)
        )
        assert_in_delta 37.758700000000005, location[:latitude], 0.1
        assert_in_delta -122.43809999999999, location[:longitude], 0.1
      end
    end

    describe 'with invalid IP addresses which raise IPAddr::* exceptions' do
      it 'returns nil' do
        ZendeskExceptions::Logger.expects(:record).never
        assert_nil(Zendesk::GeoLocation.locate('019.07.31'))
      end
    end

    describe 'with bad data' do
      it 'returns nil' do
        assert_nil(Zendesk::GeoLocation.locate(nil))
      end
    end

    describe 'on local IPs' do
      it 'returns nil' do
        assert_nil(Zendesk::GeoLocation.locate('127.0.0.1'))
      end
    end

    describe 'on IPs that return cities with latin characters' do
      it 'returns city name in utf-8' do
        location = Zendesk::GeoLocation.locate('201.233.136.42')
        assert_equal 'BogotÃ¡', location[:city]
      end
    end

    it 'does not leak exceptions from the GeoIP library' do
      exception = StandardError.new
      Zendesk::GeoLocation.send(:geo_ip).expects(:lookup).raises(exception)
      ZendeskExceptions::Logger.expects(:record).with do |e, params|
        assert_equal(exception, e)
        assert_equal(Zendesk::GeoLocation, params[:location])
      end
      location = Zendesk::GeoLocation.locate('98.210.110.15')
      assert_nil location
    end
  end

  describe 'locate_ticket_audit!' do
    describe 'on an audit without an ip address' do
      before { @audit = Audit.new }

      describe 'and a unlocated author' do
        before do
          @audit.author = User.new
          Zendesk::GeoLocation.locate_ticket_audit!(@audit)
        end

        it 'does not touch the audit' do
          refute @audit.metadata[:system].key?(:location)
          refute @audit.metadata[:system].key?(:latitude)
          refute @audit.metadata[:system].key?(:longitude)
        end
      end

      describe 'and a located author' do
        before do
          @audit.author = User.new
          @audit.author.settings.location  = 'Palo Alto'
          @audit.author.settings.latitude  = 1.0
          @audit.author.settings.longitude = 2.0
          Zendesk::GeoLocation.locate_ticket_audit!(@audit)
        end

        it 'adds location metadata' do
          assert_equal 'Palo Alto', @audit.metadata[:system][:location]
          assert_equal 1.0, @audit.metadata[:system][:latitude]
          assert_equal 2.0, @audit.metadata[:system][:longitude]
        end
      end
    end

    describe 'on an audit with a client with an IP' do
      describe 'that is locatable' do
        before do
          @audit = Audit.new
          @audit.metadata[:system][:ip_address] = '98.210.110.15'
          Zendesk::GeoLocation.locate_ticket_audit!(@audit)
        end
        it 'adds location metadata' do
          assert_equal 'San Francisco, CA, United States', @audit.metadata[:system][:location]
          assert_in_delta 37.758700000000005, @audit.metadata[:system][:latitude], 0.1
          assert_in_delta -122.43809999999999, @audit.metadata[:system][:longitude], 0.1
        end
      end
      describe 'that is local' do
        before do
          @audit = Audit.new
          @audit.metadata[:system][:ip_address] = '127.0.0.1'
          Zendesk::GeoLocation.locate_ticket_audit!(@audit)
        end
        it 'does not touch the audit' do
          refute @audit.metadata[:system].key?(:location)
          refute @audit.metadata[:system].key?(:latitude)
          refute @audit.metadata[:system].key?(:longitude)
        end
      end
    end
  end

  describe 'locate_user!' do
    before { @user = users(:minimum_admin) }

    describe 'for a user without a location' do
      before do
        @user.settings.set(location: nil, latitude: nil, longitude: nil)
      end

      it 'sets the location, longitude, and latitude settings of the user' do
        @user.settings.expects(:save)
        Zendesk::GeoLocation.locate_user!(@user, '98.210.110.15')
        assert_equal 'San Francisco, CA, United States', @user.settings.location
        assert_in_delta 37.758700000000005, @user.settings.latitude, 0.1
        assert_in_delta -122.43809999999999, @user.settings.longitude, 0.1
      end
    end

    describe 'for a user with a different location' do
      before do
        @user.settings.set(location: 'Somewhere', latitude: 0, longitude: 0)
      end

      it 'sets the location, longitude, and latitude settings of the user' do
        @user.settings.expects(:save)
        Zendesk::GeoLocation.locate_user!(@user, '98.210.110.15')
        assert_equal 'San Francisco, CA, United States', @user.settings.location
        assert_in_delta 37.758700000000005, @user.settings.latitude, 0.1
        assert_in_delta -122.43809999999999, @user.settings.longitude, 0.1
      end
    end

    describe 'for a user with unchanged location' do
      before do
        @user.settings.set(location: 'San Francisco, CA, United States', latitude: 37.7506, longitude: -122.4121)
      end

      it 'sets the location, longitude, and latitude settings of the user' do
        @user.settings.expects(:save).never
        Zendesk::GeoLocation.locate_user!(@user, '98.210.110.15')
      end
    end

    describe 'for a user with changed location but unchanged description' do
      before do
        @user.settings.set(location: 'San Francisco, CA, United States', latitude: 37.7643, longitude: -122.3942)
      end

      it 'sets the location, longitude, and latitude settings of the user' do
        @user.settings.expects(:save)
        Zendesk::GeoLocation.locate_user!(@user, '98.210.110.15')
        assert_equal 'San Francisco, CA, United States', @user.settings.location
        assert_in_delta 37.758700000000005, @user.settings.latitude, 0.1
        assert_in_delta -122.43809999999999, @user.settings.longitude, 0.1
      end
    end
  end

  describe '.fill_region' do
    it 'sets the region to us' do
      location = { longitude: -50 }
      expected_location = { longitude: -50, region: :us }
      Zendesk::GeoLocation.send(:fill_region, location)
      assert_equal expected_location, location
    end

    it 'sets the region to emea' do
      location = { longitude: 20 }
      expected_location = location.merge(region: :emea)
      Zendesk::GeoLocation.send(:fill_region, location)
      assert_equal expected_location, location
    end

    it 'sets the region to apac' do
      location = { longitude: 150 }
      expected_location = location.merge(region: :apac)
      Zendesk::GeoLocation.send(:fill_region, location)
      assert_equal expected_location, location
    end
  end

  describe '.fill_region_with_country' do
    it 'sets the region to us' do
      location = { country_code: 'US' }
      expected_location = { country_code: 'US', region: :us }
      Zendesk::GeoLocation.send(:fill_region_with_country, location)
      assert_equal expected_location, location
    end

    it 'sets the region to emea' do
      location = { country_code: 'RU' }
      expected_location = { country_code: 'RU', region: :emea }
      Zendesk::GeoLocation.send(:fill_region_with_country, location)
      assert_equal expected_location, location
    end

    it 'sets the region to apac' do
      location = { country_code: 'CN' }
      expected_location = { country_code: 'CN', region: :apac }
      Zendesk::GeoLocation.send(:fill_region_with_country, location)
      assert_equal expected_location, location
    end

    it 'sets the region to fra1' do
      location = { country_code: 'FR' }
      expected_location = { country_code: 'FR', region: :fra1 }
      Zendesk::GeoLocation.send(:fill_region_with_country, location)
      assert_equal expected_location, location
    end
  end

  describe 'for the system user' do
    before do
      @user = users(:systemuser)
    end
    it 'does not locate it' do
      @user.settings.expects(:save).never
      Zendesk::GeoLocation.locate_user!(@user, '98.210.110.15')
    end
  end
end
