require_relative "../../support/test_helper"
require_relative "../../support/consul_lookup_support"

SingleCov.covered!

describe Zendesk::AccountReadiness do
  include ConsulLookupSupport

  let(:account_id) { 1 }
  let(:subdomain) { 'test' }
  let(:products_activated) { ['chat'] }
  let(:source) { 'account_readiness_test' }
  let(:statsd_client) { Rails.application.config.statsd.client }
  let(:account_readiness) { Zendesk::AccountReadiness.new(account_id: account_id, subdomain: subdomain, products_activated: products_activated, source: source) }
  let(:tags) { ["source:#{source}"] }

  ENV['ZENDESK_DOORMAN_SECRET'] = 'e426213646b9ea0956755b6f6185c31f46cc71d9a410c66ea08d2c517068369334adedb441fb1341342c6d5d4b4fd4745c42090bb6a08e0c582c0a8653a3999e'

  def stub_account(id: account_id, active: true)
    JSON.dump('account' => { 'id' => id, 'is_active' => active })
  end

  def stub_products(products)
    JSON.dump('products' => products)
  end

  def stub_product(name: 'chat', active: true)
    {'name' => name, 'active' => active, 'state' => 'trial'}
  end

  def stub_doorman_jwt(jwt_claims)
    JWT.encode(jwt_claims, ENV['ZENDESK_DOORMAN_SECRET'])
  end

  def doorman_jwt_claims(account_active: true)
    timestamp = Time.now.to_i
    exp = timestamp + 300

    {
        "iat" => timestamp,
        "exp" => exp,
        "via" => "system_user",
        "ip_restriction" => "no_restriction",
        "system_user" => { "name" => "zendesk" },
        "account" => {
            "id" => 107,
            "is_active" => account_active,
            "route_id" => 68,
            "shard_id" => 1,
            "subdomain" => "acme-393806",
            "multiproduct" => true,
            "lock_state" => 0,
            "products" => { "acme_app" => { "state" => "trial", "plan" => "0" }},
            "plans" => { "acme_app" => "0" }
        },
        "user" => {"id" => -1, "role" => nil, "locale" => "en-US", "timezone" => "Etc/UTC", "entitlements" => {}, "owner" => false }
    }
  end

  def stub_account_request(status: 200, body: stub_account)
    stub_request(:get, /\/api\/services\/accounts\/\d+/).
      to_return(status: status, body: body, headers: {'Content-Type' => 'application/json'})
  end

  def stub_products_request(status: 200, body:)
    stub_request(:get, /\/api\/services\/accounts\/\d+\/products$/).
      to_return(status: status, body: body, headers: {'Content-Type' => 'application/json'})
  end

  def stub_doorman_request(status: 200, auth_response: 200, doorman_jwt: stub_doorman_jwt(doorman_jwt_claims))
    stub_request(:get, /\/doorman\/v1\/auth/).
      to_return(status: status, headers: { 'X-Zendesk-Doorman-Auth-Response' => auth_response.to_s, 'X-Zendesk-Doorman' => doorman_jwt })
  end

  def stub_classic_account(active: true, serviceable: true)
    Account.any_instance.stubs(:is_active?).returns(active)
    Account.any_instance.stubs(:is_serviceable?).returns(serviceable)
  end

  before do
    Arturo.enable_feature!(:voltron_account_setup_disable_account_cache)
    stub_account_service_lookup
    stub_auth_service_lookup
    stub_account_request
    stub_products_request(body: stub_products([stub_product(name: 'chat', active: true)]))
    stub_doorman_request
    stub_classic_account
  end

  describe '#readiness_check' do
    it 'returns completed response if all checks pass' do
      assert_equal "complete", account_readiness.readiness_check
    end

    it 'records metrics when all checks pass' do
      statsd_client.expects(:increment).with('account_setup.state.complete', tags: tags).once
      account_readiness.readiness_check
    end

    describe 'when readiness check has already completed once' do
      it 'returns a complete status without performing checks again' do
        Rails.cache.expects(:read).with('1-setup-controller-readiness-check').returns(true)
        Rails.cache.expects(:write).never
        assert_equal "complete", account_readiness.readiness_check
      end
    end
  end

  describe '#doorman_ready?' do
    let(:tags) { ['cause:doorman', "source:#{source}"] }

    describe 'when doorman check has a host returning a non-200 X-Zendesk-Doorman-Auth-Response header' do
      it 'returns a pending state' do
        stub_doorman_request(auth_response: 401)
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end
    end

    describe 'when doorman check has a host returning a doorman jwt with an account that is not active' do
      it 'returns a pending state' do
        stub_doorman_request(doorman_jwt: stub_doorman_jwt(doorman_jwt_claims(account_active: false)))
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end
    end

    describe 'when doorman check calls a host that already returned a successful check (cached)' do
      let(:doorman_host_cached_result) { "1.0.0.11:12080" }
      let(:doorman_host_not_cached_result) { "1.0.0.12:12080" }
      let(:doorman_host_not_cached_result_2) { "1.0.0.13:12080" }

      it 'skips the host' do
        Rails.cache.stubs(:read).returns(nil)
        Rails.cache.expects(:read).with("1-setup-controller-readiness-check").returns(nil)
        Rails.cache.expects(:read).with("1-setup-controller-doorman-status-#{doorman_host_cached_result}").returns(true)
        Rails.cache.expects(:read).with("1-setup-controller-doorman-status-#{doorman_host_not_cached_result}").returns(nil)
        Rails.cache.expects(:read).with("1-setup-controller-doorman-status-#{doorman_host_not_cached_result_2}").returns(nil)
        Rails.cache.expects(:read).with(regexp_matches(/^1-setup-controller-account.*$/)).returns(nil).times(1)
        Rails.cache.expects(:read).with(regexp_matches(/^1-setup-controller-products.*$/)).returns(nil).times(1)
        assert_equal "complete", account_readiness.readiness_check
        assert_not_requested :get, "http://#{doorman_host_cached_result}/doorman/v1/auth"
        assert_requested :get, "http://#{doorman_host_not_cached_result}/doorman/v1/auth"
        assert_requested :get, "http://#{doorman_host_not_cached_result_2}/doorman/v1/auth"
      end
    end

    describe 'when doorman check has a host that returns a non-200 response code' do
      it 'returns a pending state' do
        stub_doorman_request(status: 503)
        assert_equal "pending", account_readiness.readiness_check
      end
    end

    describe 'when doorman check has a host returning an invalid doorman jwt ' do
      it 'returns a pending state' do
        stub_doorman_request(doorman_jwt: '')
        assert_equal "pending", account_readiness.readiness_check
      end
    end
  end

  describe '#account_ready?' do
    let(:tags) { ['cause:account', "source:#{source}"] }

    it 'does not disable caching by default' do
      Arturo.disable_feature!(:voltron_account_setup_disable_account_cache)
      stub_account_request(body: stub_account(active: false))
      account_readiness.readiness_check
      assert_requested :get, /api\/services\/accounts\/1\?disable_cache=false/
    end

    it 'disables caching when voltron_account_setup_disable_account_cache feature is enabled' do
      Arturo.enable_feature!(:voltron_account_setup_disable_account_cache)
      stub_account_request(body: stub_account(active: false))
      account_readiness.readiness_check
      assert_requested :get, /api\/services\/accounts\/1\?disable_cache=true/
    end

    describe 'when account check has a host returning an account that is not active' do
      it 'returns a pending state' do
        stub_account_request(body: stub_account(active: false))
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end
    end

    describe 'when account check calls a host that already returned a successful check (cached)' do
      let(:account_host_cached_result) { "1.0.0.21:12080" }

      it 'skips the host' do
        Rails.cache.stubs(:read).returns(nil)
        Rails.cache.expects(:read).with("1-setup-controller-readiness-check").returns(nil)
        Rails.cache.expects(:read).with("1-setup-controller-account-status-#{account_host_cached_result}").returns(true)
        Rails.cache.expects(:read).with(regexp_matches(/^1-setup-controller-doorman.*$/)).returns(nil).times(1)
        Rails.cache.expects(:read).with(regexp_matches(/^1-setup-controller-products.*$/)).returns(nil).times(1)
        assert_equal "complete", account_readiness.readiness_check
        assert_not_requested :get, "http://#{account_host_cached_result}/api/services/accounts/1"
      end
    end

    describe 'when account check has a host that returns a non-200 response code' do
      it 'returns a pending state' do
        stub_account_request(status: 503)
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end
    end
  end

  describe("#products_ready?") do
    let(:tags) { ['cause:products', "source:#{source}"] }

    describe 'when product check is not returning all products activated during account creation' do
      it 'returns a pending state' do
        stub_products_request(body: stub_products([]))
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end
    end

    describe 'when product check is not returning all products activated during account creation with an active state' do
      it 'returns a pending state' do
        stub_products_request(body: stub_products([stub_product(name: 'chat', active: false)]))
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end
    end

    describe 'when product check already returned a successful check (cached)' do
      let(:product_host_cached_result) { "1.0.0.21:12080" }

      it 'skips the host' do
        Rails.cache.stubs(:read).returns(nil)
        Rails.cache.expects(:read).with("1-setup-controller-readiness-check").returns(nil)
        Rails.cache.expects(:read).with("1-setup-controller-products-status-#{product_host_cached_result}").returns(true)
        Rails.cache.expects(:read).with(regexp_matches(/^1-setup-controller-doorman.*$/)).returns(nil).times(3)
        Rails.cache.expects(:read).with(regexp_matches(/^1-setup-controller-account.*$/)).returns(nil).times(1)
        assert_equal "complete", account_readiness.readiness_check
        assert_not_requested :get, "http://#{product_host_cached_result}/api/services/accounts/1/products"
      end
    end

    describe 'when product check host returns a non-200 response code' do
      it 'returns a pending state' do
        error_body = JSON.dump('errors' => [{'title' => 'Service Unavailable Error', 'detail' => 'Service unavailable'}])
        stub_products_request(status: 503, body: error_body)
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end
    end

    describe 'when product check returns more active products then there were products activated' do
      it 'returns completed response' do
        stub_products_request(body: stub_products([stub_product(name: 'chat', active: true), stub_product(name: 'guide', active: true)]))
        assert_equal "complete", account_readiness.readiness_check
      end
    end
  end

  describe '#classic_account_ready?' do
    let(:tags) { ['cause:classic_account', "source:#{source}"] }

    describe 'when classic account is not found' do
      it 'returns a pending state' do
        Account.stubs(:find).returns(nil)
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end
    end

    describe 'classic account check' do
      it 'returns a pending state when account is not active' do
        Account.any_instance.expects(:clear_kasket_cache!).once
        stub_classic_account(active: false)
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end

      it 'returns a pending state when account is not serviceable' do
        Account.any_instance.expects(:clear_kasket_cache!).once
        stub_classic_account(serviceable: false)
        statsd_client.expects(:increment).with('account_setup.state.pending', tags: tags).once
        assert_equal "pending", account_readiness.readiness_check
      end
    end
  end
end
