require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'Params' do
  before do
    @params = Zendesk::Search::Params.new({})
  end

  it "has default options" do
    default_options = { sort_mode: :relevance, per_page: 15,
                        type: nil, hl: nil }
    assert_equal default_options, @params.to_options
  end

  describe "by updated options" do
    it "is supported" do
      @params.params[:by_updated] = '1'

      assert_equal :desc,       @params.to_options[:sort_mode]
      assert_equal :updated_at, @params.to_options[:order]
    end

    it "takes precendence over custom options" do
      @params.params.merge!(by_updated: '1', sort: 'asc', order_by: 'status')

      assert_equal :desc,       @params.to_options[:sort_mode]
      assert_equal :updated_at, @params.to_options[:order]
    end

    it "does not be used when encountering falsey values" do
      @params.params.merge!(by_updated: '0', sort: 'asc', order_by: 'status')

      assert_equal :asc,       @params.to_options[:sort_mode]
      assert_equal :status_id, @params.to_options[:order]
    end

    it "does not be used on blank values" do
      @params.params.merge!(by_updated: '', sort: 'asc', order_by: 'status')

      assert_equal :asc,       @params.to_options[:sort_mode]
      assert_equal :status_id, @params.to_options[:order]
    end
  end

  describe "per page" do
    it "defaults to the minimum allowed" do
      assert_equal false, @params.params.key?(:per_page)
      assert_equal 15,    @params.to_options[:per_page]
    end

    it "uses API minimum when too low" do
      @params.params[:per_page] = 0
      assert_equal 15, @params.to_options[:per_page]
    end

    it "does not override when within bounds" do
      @params.params[:per_page] = 30
      assert_equal 30, @params.to_options[:per_page]
    end

    it "uses API max when too high" do
      @params.params[:per_page] = 100
      assert_equal 50, @params.to_options[:per_page]
    end
  end

  describe "sort mode" do
    it "uses relevance by default" do
      assert_equal false,      @params.params.key?(:sort)
      assert_equal :relevance, @params.to_options[:sort_mode]
    end

    it "does not allow arbitrary values" do
      @params.params[:sort] = 'anything'
      assert_equal :relevance, @params.to_options[:sort_mode]
    end

    it "supports asc" do
      @params.params[:sort] = 'asc'
      assert_equal :asc, @params.to_options[:sort_mode]
    end

    it "supports desc" do
      @params.params[:sort] = 'desc'
      assert_equal :desc, @params.to_options[:sort_mode]
    end
  end

  describe "order" do
    it "does not allow arbitrary values" do
      @params.params[:order_by] = 'awesome'
      assert_equal false, @params.to_options.key?(:order)
    end

    it "converts ticket type to id" do
      @params.params[:order_by] = 'ticket_type'
      assert_equal :ticket_type_id, @params.to_options[:order]
    end

    it "converts priority to id" do
      @params.params[:order_by] = 'priority'
      assert_equal :priority_id, @params.to_options[:order]
    end

    it "converts status to id" do
      @params.params[:order_by] = 'status'
      assert_equal :status_id, @params.to_options[:order]
    end
  end

  describe "updates options accordingly for a CBP endpoint" do
    before do
      @params.params[:action] = "export"
      @params.params[:page] = { after: "blah", size: 1 }
      @params.params[:filter] = { type: "ticket" }
    end

    it "adds CBP params to options" do
      assert_equal "blah", @params.to_options[:page_after]
      assert_equal 1, @params.to_options[:page_size]
      assert_equal "ticket", @params.to_options[:filter_type]
    end
  end
end
