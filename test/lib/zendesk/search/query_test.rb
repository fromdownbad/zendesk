require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 21

describe 'Query' do
  describe "A query" do
    before do
      @user  = stub('User')
      @query = Zendesk::Search::Query.new('hi')
    end

    it "ignores extra whitespace" do
      query = Zendesk::Search::Query.new(' hi ')
      assert_equal 'hi', query.to_s
    end

    it "includes API per page options by default" do
      assert_equal({ per_page: 15 }, @query.options)
    end

    describe "being executed" do
      before do
        @user = users(:minimum_agent)
      end

      it "provides search results" do
        Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.empty)
        query   = Zendesk::Search::Query.new('hi')
        result  = query.execute(@user)

        assert_equal Zendesk::Search::Results, result.class
        assert_equal result, query.result
      end

      describe_with_arturo_enabled :exclude_articles_from_search_results do
        it "passes the exclude_articles option to SS" do
          query = Zendesk::Search::Query.new('hello', account: @user.account)
          Zendesk::Search.expects(:search).with(@user, 'hello',
            per_page: 15,
            without: {type: :article},
            arturo_flags: 'exclude_articles_from_search_results')

          query.execute(@user)
        end
      end

      describe_with_arturo_disabled :exclude_articles_from_search_results do
        it "does not pass the exclude_articles option to SS" do
          query = Zendesk::Search::Query.new('hello', account: @user.account)
          Zendesk::Search.expects(:search).with(@user, 'hello',
            per_page: 15,
            without: {type: :article})

          query.execute(@user)
        end
      end

      it "calls the help center user segments access control endpoint" do
        Zendesk::Search.expects(:search).
          with(@user, 'hi', per_page: 3, page: 1, include_helpcenter: true, without: {type: :entry},
                            user_segments: ['111', '126', '\\-1']).
          at_least_once.
          returns(Zendesk::Search::Results.create(1, 1, 1) { |pager| pager.replace(['id' => 1]) })

        connection = Faraday.new do |builder|
          builder.response :json
          builder.adapter :test do |stubs|
            stubs.get("/hc/api/internal/users/#{@user.id}/access/user_segments.json") { [200, {}, '{"user_segment_ids": [111,126]}'] }
          end
        end

        query = Zendesk::Search::Query.new('hi', account: @user.account)
        query.per_page = 3
        query.page = 1
        query.include_helpcenter = true
        query.stubs(:connection).returns(connection)
        result = query.execute(@user)

        assert_equal [{'id' => 1}], result
      end

      it "performs searches with the given options" do
        @query.page      = 1
        @query.per_page  = 3
        @query.sort_mode = :relevance
        @query.order     = :asc
        @query.use_guide_search_for_articles = true
        @query.include_side_conversations = true
        @query.include_side_conversations_count = false

        Zendesk::Search.expects(:search).with(@user, 'hi',
          page: 1,
          per_page: 3,
          sort_mode: :relevance,
          order: :asc,
          without: {type: :article},
          use_guide_search_for_articles: true,
          include_side_conversations: true,
          include_side_conversations_count: false)
        @query.execute(@user)
      end

      it "does not execute search results for blank queries" do
        query = Zendesk::Search::Query.new('')
        Zendesk::Search.expects(:search).never
        query.execute(@user)
        assert query.result.is_a?(Zendesk::Search::Results)
      end

      it "executes search results for blank queries with a type specified" do
        query = Zendesk::Search::Query.new('')
        query.type = :people
        Zendesk::Search.expects(:search)
        query.execute(@user)
      end

      it "uses error handling" do
        @query.expects(:with_error_handling)
        @query.execute(@user)
      end
    end
  end
end
