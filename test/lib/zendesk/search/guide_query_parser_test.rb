require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 0

describe Zendesk::Search::GuideQueryParser do
  describe 'Parsing error' do
    it 'is invalid if there\'s any errors during parsing' do
      query = 'fails miserably created:wow'
      Zendesk::Search::GuideQueryParser.any_instance.expects(:parse_date_params).raises(StandardError.new('fail'))
      ZendeskExceptions::Logger.expects(:record)

      parser = Zendesk::Search::GuideQueryParser.new(query)
      refute parser.valid?
    end

    it 'is invalid if query terms are missing' do
      query = 'brand:foo sort:asc '

      parser = Zendesk::Search::GuideQueryParser.new(query)
      refute parser.valid?
    end
  end

  describe 'Query' do
    it 'gets the query when no options are passed' do
      query = 'hello how are you'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_query = parser.query
      assert_equal query, expected_query
    end

    it 'gets the query when options are passed in between the query terms' do
      query = 'hello sort:asc potato why'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_query = 'hello potato why'
      assert_equal expected_query, parser.query
    end

    it 'gets the query when options are passed around the query terms' do
      query = 'sort:asc hello potato why order_by:created'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_query = 'hello potato why'
      assert_equal expected_query, parser.query
    end

    it 'parses as a query, invalid, look-alike params' do
      query = 'hello sort: brand: win: bogus: foo:bar'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_query = parser.query
      assert_equal query, expected_query
    end

    it 'removes user/organization/tickets properties from the query' do
      query = 'hello via:api cc:hello submitter:adrian recipient:you commented:me solved:2018-10-01 name:no-name world'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_query = parser.query
      assert_equal 'hello world', expected_query
    end
  end

  describe 'Params' do
    it 'gets nothing when no params are passed' do
      query = 'hello how are you'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_query = parser.query
      assert_equal query, expected_query
      assert_empty parser.params
    end

    it 'gets the params in between the query terms' do
      query = 'hello sort:asc potato why'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_params = ['sort_order=asc']
      assert_equal expected_params, parser.params
    end

    it 'gets the params with query terms in between' do
      query = 'sort:asc hello potato why order_by:created'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_query = ['sort_order=asc', 'sort_by=created_at']
      assert_equal expected_query, parser.params
    end

    it 'removes times from date formats' do
      query = 'hello updated:2018-12-01T12:00:00-08:00'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_query = ['updated_at=2018-12-01']
      assert_equal expected_query, parser.params
    end

    it 'handles invalid date formats' do
      query = 'hello updated:gibberish00'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      assert_equal 'hello', parser.query
      assert_empty parser.params
    end

    describe "relative dates" do
      it 'ignores minutes and hours' do
        parser = Zendesk::Search::GuideQueryParser.new('hello created>10minutes')
        assert parser.valid?
        assert_empty parser.params

        parser = Zendesk::Search::GuideQueryParser.new('hello created>5hours')
        assert parser.valid?
        assert_empty parser.params
      end

      it 'handles days' do
        query = 'hello created>10days'

        parser = Zendesk::Search::GuideQueryParser.new(query)
        assert parser.valid?

        expected_query = ["created_after=#{Date.today - 10}"]
        assert_equal expected_query, parser.params
      end

      it 'handles weeks' do
        query = 'hello created>52weeks'

        parser = Zendesk::Search::GuideQueryParser.new(query)
        assert parser.valid?

        expected_query = ["created_after=#{Date.today - 52.weeks}"]
        assert_equal expected_query, parser.params
      end

      it 'handles months' do
        query = 'hello created>2months'

        parser = Zendesk::Search::GuideQueryParser.new(query)
        assert parser.valid?

        expected_query = ["created_after=#{Date.today - 2.months}"]
        assert_equal expected_query, parser.params
      end

      it 'handles years' do
        query = 'hello updated:2years'

        parser = Zendesk::Search::GuideQueryParser.new(query)
        assert parser.valid?

        expected_query = ["updated_at=#{Date.today - 2.years}"]
        assert_equal expected_query, parser.params
      end
    end
  end

  describe 'Brand' do
    it 'returns no brands when none are in the query' do
      query = 'sort:asc hello potato why order_by:created'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      assert_empty parser.brands
    end

    it 'returns the brands in the middle of the sentence' do
      query = 'sort:asc hello potato brand:banana why order_by:created'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_brands = ['banana']
      assert_equal expected_brands, parser.brands
    end

    it 'returns multiple brands spread around the sentence' do
      query = 'sort:asc brand:gordian hello potato brand:banana why order_by:created brand:search'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_brands = ['gordian', 'banana', 'search']
      assert_equal expected_brands, parser.brands
    end

    it 'returns quoted brands containing spaces' do
      query = 'brand:gordian potato brand:banana test brand:"Potato Salad" order_by:created brand:search'

      parser = Zendesk::Search::GuideQueryParser.new(query)
      assert parser.valid?

      expected_brands = ['gordian', 'banana', 'Potato Salad', 'search']
      assert_equal expected_brands, parser.brands
    end
  end
end
