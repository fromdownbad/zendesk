require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Search::CBPResults do
  fixtures :users, :accounts, :tickets

  before do
    @ticket1 = tickets(:minimum_1)
    @ticket2 = tickets(:minimum_2)
    records = [@ticket1, @ticket2]
    @meta = {after_cursor: "blah", has_more: true}
    @results = Zendesk::Search::CBPResults.new(records, @meta)
  end

  it "includes the records and only them" do
    assert @results.include? @ticket1
    assert @results.include? @ticket2
    assert_equal 2, @results.size
  end

  it "provides access to CBP-related information" do
    assert_equal @meta[:after_cursor], @results.after_cursor
    assert_nil @results.before_cursor
    assert_equal @meta[:has_more], @results.has_more?
    assert_equal 2, @results.cursor_pagination_version
  end
end
