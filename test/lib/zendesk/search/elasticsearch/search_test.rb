require_relative "../../../../support/test_helper"
require 'zendesk/search/elasticsearch/search'
require_relative "../../../../support/channels_test_helper"
require 'json'

SingleCov.covered! uncovered: 28

describe Zendesk::Search::Elasticsearch::Search do
  include ChannelsTestHelper

  fixtures :users, :organizations, :organization_memberships, :cf_fields, :cf_dropdown_choices, :forums, :tickets

  describe ".elasticsearch_params" do
    let(:agent_as_end_user_arturo_enabled) { false }

    before do
      @search_user = users(:minimum_search_user)
      @end_user = users(:minimum_end_user2)
      @agent = users(:minimum_agent)
      @anonymous_user = accounts(:minimum).anonymous_user
      Arturo.set_feature!(:agent_as_end_user, agent_as_end_user_arturo_enabled)
    end

    it "handles anonymous user forum searches correctly" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @anonymous_user, "foo", type: 'entry')
      inaccessible_forums = accounts(:minimum).forums - accounts(:minimum).forums.accessible_to(@anonymous_user)
      assert_same_elements [
        "_type:(entry OR article)",
        "-forum_id:(#{inaccessible_forums.map(&:id).join(" OR ")})"
      ], options[:fq]
    end

    it "handles end user forum searches correctly" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @end_user, "foo", type: 'entry')
      inaccessible_forums = accounts(:minimum).forums - accounts(:minimum).forums.accessible_to(@end_user)
      assert_same_elements [
        "requester_id:#{@end_user.id} OR cc_id:#{@end_user.id} OR organization_id:(#{@end_user.organization.id}) OR _type:(entry OR article)",
        "-forum_id:(#{inaccessible_forums.map(&:id).join(" OR ")})"
      ], options[:fq]
    end

    it "passes along agent entry bias correctly" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "foo", entry_bias: '.4')
      assert_equal ".4", options[:bv]
      assert_equal "+user_role:0 +_type:entry", options[:bq]
    end

    it "passes along problem bias correctly" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "foo", problem_bias: '.4')
      assert_equal ".4", options[:bv]
      assert_equal "+ticket_type_id:3 -status_id:4 +_type:ticket", options[:bq]
    end

    it "correctly limits access of end user with no organizations" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "", type: 'ticket')
      assert options[:fq].include? "requester_id:#{@search_user.id} OR cc_id:#{@search_user.id} OR _type:(entry OR article)"
    end

    it "correctly limits access of end user with an organization" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @end_user, "", type: 'ticket')
      assert options[:fq].include? "requester_id:#{@end_user.id} OR cc_id:#{@end_user.id} OR organization_id:(#{@end_user.organization_id}) OR _type:(entry OR article)"
    end

    it "does not limit access of minimum agent" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @agent, "", type: 'ticket')
      assert_equal [], options[:fq]
    end

    it "passes custom field type data for referenced custom fields" do
      query = "foo integer1:3 bar REGEXP1:xxx zap date1:2014-12-10 type:ticket text1:alfalfa bogus:yyy"
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, query, {})
      expected = {"integer1" => ["Integer"], "regexp1" => ["Regexp"], "date1" => ["Date"], "text1" => ["Text"]}
      assert_equal expected, JSON.parse(options[:custom_fields])
    end

    it "passes custom field type data for system custom field" do
      query = "system::integer2:12345 x::y"
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, query, {})
      expected = {"system::integer2" => ["Integer"]}
      assert_equal expected, JSON.parse(options[:custom_fields])
    end

    it "passes dropdown choice data for referenced dropdown custom field" do
      query = "dropdown1:whatever"
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, query, {})
      expected = {"dropdown1" => [{"100" => 6, "200" => 11}]}
      assert_equal expected, JSON.parse(options[:custom_fields])
    end

    it "passes through restrictions on organization" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "foo",
        with: {organization_id: [111, 222]}, without: {organization_id: 46})
      assert options[:fq].include? "organization_id:(111 OR 222)"
      assert options[:fq].include? "-organization_id:(46)"
    end

    it "passes AnyChannel via info along" do
      search_via_map = {'search_name' => [1, 2]}
      Zendesk::Search::Elasticsearch::Search.expects(:anychannel_via_map).with(@search_user).returns(search_via_map)
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "via:something", {})
      assert_equal search_via_map, JSON.parse(options[:via_map])
    end

    it "maps positive type filter" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "my query",
        with: {type: "entry"})
      assert options[:fq].include? "_type:(entry)"
    end

    it "maps negative type filter" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "my query",
        with: {type: "entry"}, without: {type: "ticket"})
      assert options[:fq].include? "_type:(entry)"
      assert options[:fq].include? "-_type:(ticket)"
    end

    it "maps tags containing colons" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "my query",
        with: {tags: "hello:world"})
      assert options[:fq].include? "tags:(hello\\:world)"
    end

    it "passed through the endpoint parameter if present" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "my query",
        endpoint: 'foo')

      assert options[:endpoint] == 'foo'
    end

    it "ignores missing options" do
      options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "my query", {})
      assert !options.key?(:endpoint)
    end

    describe ':agent_as_end_user Arturo enabled' do
      let(:agent_as_end_user_arturo_enabled) { true }

      it "limits access of minimum agent" do
        options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @agent, "", type: 'ticket')
        assert_equal ["NOT (_type:ticket AND (via_id:#{ViaType.WEB_SERVICE} OR via_id:#{ViaType.WEB_FORM}) AND via_reference_id:#{ViaType.HELPCENTER} AND submitter_id:#{@agent.id})"], options[:fq]
      end

      it "does not limit access of end user" do
        options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, @search_user, "", type: 'ticket')
        refute options[:fq].any? { |item| item.include?("via:web_service") }
      end

      it "does not limit access of administrator" do
        options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, users(:minimum_admin), "", type: 'ticket')
        assert_equal [], options[:fq]
      end

      it "does not limit access of a system user" do
        options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, users(:systemuser), "", type: 'ticket')
        assert_equal [], options[:fq]
      end
    end

    describe "searching with facets" do
      it "sets the facet to type" do
        options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, users(:minimum_admin), "", facets: true)
        assert_equal "type", options[:facet]
      end
    end

    describe "searching with JA extended mode" do
      it "sets the JA extended mode" do
        options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, users(:minimum_admin), "", use_ja_extended_mode: "something")
        assert options[:use_ja_extended_mode]
      end
    end

    describe "searching with nonstandard agent bias" do
      it "sets bq and bv" do
        options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, users(:minimum_admin), "", agent_bias: 0.4)
        assert_equal "-user_role:0 +_type:user", options[:bq]
        assert_equal 0.4, options[:bv]
      end
    end

    describe "searching with arturo flags" do
      it "passes them through" do
        options = Zendesk::Search::Elasticsearch::Search.send(:elasticsearch_params, users(:minimum_admin), "", arturo_flags: "something")
        assert_equal "something", options[:arturo_flags]
      end
    end
  end

  describe ".search" do
    let(:user) { users(:minimum_end_user) }
    let(:user2) { users(:minimum_end_user2) }
    let(:stubbed_users) { [user] }
    let(:options) do
      {
        use_guide_search_for_articles: true,
        type: "article",
        per_page: 25,
        page: 1
      }
    end
    let(:raw_stubbed_search_results) { stubbed_users.map { |user| {"id" => user.id, "type" => "user"} } }
    let(:stubbed_search_results) do
      {
        "results" => raw_stubbed_search_results,
        "facets" => { "type" => {} },
        "count" => raw_stubbed_search_results.size
      }
    end
    let(:stubbed_hc_search_results) do
      {
        "results" => [{
          "id" => 100,
          "title" => "foo",
          "created_at" => "2019-03-27 10:34:49 +0000",
          "updated_at" => "2019-03-27 10:34:49 +0000",
          "brand_name" => "yolo",
          "brand_id" => 3,
          "locale" => "en-us",
          "result_type" => "article",
          "html_url" => "https://example.com",
          "name" => "boo",
          "snippet" => "boo",
          "unknown_new_variable" => "this does not belong here",
          "this_is_not_here_either" => "this does not belong here"
        }],
        "count" => 1,
        "elasticsearch" => { "took" => 123 }
      }
    end

    # The search string is arbitrary, as the actual search is stubbed out.
    let(:search_string) { "*:*" }

    # General-purpose entry point to the Search Client gem (zendesk_search repo)
    let(:search_method) { :search }

    before do
      HelpCenter.
        stubs(:find_by_brand_id).
        returns(HelpCenter.new(id: 42, state: 'enabled'))
    end

    def stub_search
      ZendeskSearch::Client.any_instance.expects(search_method).returns(stubbed_search_results)
    end

    def stub_search_down
      exception = ZendeskSearch::ServerError.new("some connection error")
      ZendeskSearch::Client.any_instance.expects(search_method).raises(exception)
    end

    it "returns no results when search service is down" do
      stub_search_down
      results = Zendesk::Search::Elasticsearch::Search.search(user, search_string)
      assert_equal [], results
    end

    it "handles empty results" do
      ZendeskSearch::Client.any_instance.expects(search_method).returns([""])
      results = Zendesk::Search::Elasticsearch::Search.search(user, search_string)
      assert_equal [], results
    end

    it "finds models" do
      stub_search
      results = Zendesk::Search::Elasticsearch::Search.search(user, search_string)
      assert_equal [user], results
    end

    it "gets the articles from guide search when option enabled" do
      stub_search
      Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).returns(stubbed_hc_search_results)

      results = Zendesk::Search::Elasticsearch::Search.search(user, search_string, options)
      # Drops the unused fields
      assert_equal [{
        "id" => 100,
        "title" => "foo",
        "created_at" => "2019-03-27 10:34:49 +0000",
        "updated_at" => "2019-03-27 10:34:49 +0000",
        "brand_name" => "yolo",
        "brand_id" => 3,
        "locale" => "en-us",
        "result_type" => "article",
        "html_url" => "https://example.com",
        "name" => "boo",
        "snippet" => "boo",
     }], results
    end

    it "gets only the count from guide search when searching for some other entities" do
      stub_search

      options = {
        use_guide_search_for_articles: true,
        type: "ticket",
        per_page: 25,
        page: 1
      }

      Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).returns("18")
      results = Zendesk::Search::Elasticsearch::Search.search(user, "hello", options)
      assert_equal results.facets['type']['article'], 18
    end

    describe "checks the HC existence" do
      it "searches for articles if help center is enabled" do
        stub_search
        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).returns(stubbed_hc_search_results)

        Zendesk::Search::Elasticsearch::Search.search(user, search_string, options)
      end

      it "does not search for articles if help center is disabled" do
        stub_search

        # stub no help centers
        HelpCenter.
          stubs(:find_by_brand_id).
          returns(nil)

        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).never

        Zendesk::Search::Elasticsearch::Search.search(user, search_string, options)
      end
    end

    it "doesn't get the articles from guide search when option disabled" do
      stub_search
      Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).never

      results = Zendesk::Search::Elasticsearch::Search.search(user, search_string)
      assert_equal [user], results
    end

    describe "guide query params parsing" do
      it "successfuly passes down params to the HC api" do
        stub_search

        brand = user.account.brands.first
        query = "fresh sort:asc order_by:created brand:#{brand.name} potato"

        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).
          with('fresh potato', ['sort_order=asc', 'sort_by=created_at', 'page=1', 'per_page=25', "brand_id=#{brand.id}"]).
          returns(stubbed_hc_search_results)

        Zendesk::Search::Elasticsearch::Search.search(user, query, options)
      end

      it "successfuly passes down a brand id to the HC api" do
        stub_search

        brand = user.account.brands.first
        query = "potato brand:#{brand.name}"

        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).
          with('potato', ['page=1', 'per_page=25', "brand_id=#{brand.id}"]).
          returns(stubbed_hc_search_results)

        Zendesk::Search::Elasticsearch::Search.search(user, query, options)
      end

      it "filters down invalid brand names" do
        stub_search

        query = "potato brand:crazy"

        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).
          with('potato', ['page=1', 'per_page=25']).
          returns(stubbed_hc_search_results)

        Zendesk::Search::Elasticsearch::Search.search(user, query, options)
      end

      it "successfuly passes down a list of brand ids to the HC api" do
        stub_search

        brand = user.account.brands.first
        second_brand = user.account.brands.create! { |b| b.name = "World"; b.route = user.account.routes.create!(subdomain: "World") }
        inactive_one = user.account.brands.create! { |b| b.name = "Anplr"; b.route = user.account.routes.create!(subdomain: "inact"); b.active = false }

        query = "potato brand:#{brand.name} brand:#{second_brand.name} brand:#{inactive_one.name}"

        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).
          with('potato', ['page=1', 'per_page=25', "brand_id=#{brand.id},#{second_brand.id},#{inactive_one.id}"]).
          returns(stubbed_hc_search_results)

        Zendesk::Search::Elasticsearch::Search.search(user, query, options)
      end

      it "successfuly sorts ascending by created_at" do
        stub_search
        query = "potato sort:asc order_by:created"

        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).
          with('potato', ['sort_order=asc', 'sort_by=created_at', 'page=1', 'per_page=25']).
          returns(stubbed_hc_search_results)

        Zendesk::Search::Elasticsearch::Search.search(user, query, options)
      end

      it "successfuly formats date params to HC format" do
        stub_search
        query = "potato sort:asc order_by:created created>2012-10-21T12:00:00-08:00"

        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).
          with('potato', ['sort_order=asc', 'sort_by=created_at', 'created_after=2012-10-21', 'page=1', 'per_page=25']).
          returns(stubbed_hc_search_results)

        Zendesk::Search::Elasticsearch::Search.search(user, query, options)
      end

      it "ignores malformed date params" do
        stub_search
        query = "potato created>2012312-12310-21231T12:00:00-08:00"

        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).
          with('potato', ['page=1', 'per_page=25']).
          returns(stubbed_hc_search_results)

        Zendesk::Search::Elasticsearch::Search.search(user, query, options)
      end
    end

    def stub_for_destroy
      [Change, Comment, Notification].each do |klass|
        klass.any_instance.stubs(new_record?: true)
      end
    end

    it "ignores unfound models" do
      stub_search
      stub_for_destroy
      user.destroy
      results = Zendesk::Search::Elasticsearch::Search.search(user, search_string)
      assert_equal [], results
    end

    describe "loading models" do
      let(:stubbed_users) { [user, user2] }

      it "finds models in packs" do
        stub_search
        User.expects(:find_for_search_results).with(ids: [user.id, user2.id], stub_only: true, include_models: nil).returns [user, user2]
        results = Zendesk::Search::Elasticsearch::Search.search(user, search_string)
        assert_equal [user, user2].sort, results.sort
      end
    end

    # Sadly, this test is no good - it succeeds when it should fail, because the ordering
    # of results is never lost in the test environment as it is normally when a list of ids
    # is looked up in the database.
    describe "ordered results" do
      let(:orderable_users) { [user, users(:minimum_end_user2), users(:minimum_agent)] }
      [[1, 0, 2], [2, 1, 0]].each_with_index do |order, idx|
        describe "order #{idx}" do
          let(:stubbed_users) { order.map { |i| orderable_users[i] } }

          it "preserves order of docs" do
            stub_search
            results = Zendesk::Search::Elasticsearch::Search.search(user, search_string)
            assert_equal [0, 1, 2], results.map { |user| stubbed_users.index(user) }
            assert_equal stubbed_users, results
          end
        end
      end
    end

    describe "on archived tickets" do
      before do
        @ticket = accounts(:minimum).tickets.first
        archive_and_delete @ticket
      end

      let(:ticket) { @ticket }

      def stub_search_with_archived_tickets(tickets)
        results = tickets.map { |ticket| {"id" => ticket.id, "type" => "ticket"} }
        ZendeskSearch::Client.any_instance.expects(search_method).returns(
          "results" => results, "count" => results.size
        )
      end

      describe "incremental search" do
        it "should call all_with_archived with stub_only false" do
          stub_search_with_archived_tickets([ticket])
          Ticket.expects(:all_with_archived).with(stub_only: false).returns([ticket])
          Zendesk::Search::Elasticsearch::Search.search(user, search_string, incremental: true)
        end

        it "handles store not returning archived tickets" do
          stub_search_with_archived_tickets([ticket])
          ticket.ticket_archive_stub.remove_from_archive
          ZendeskArchive.expects(:report_missing_record_error) # mimic production's "not blowing up on missing records" quality
          results = Zendesk::Search::Elasticsearch::Search.search(user, search_string, incremental: true)
          assert_equal [TicketArchiveStub.find(ticket.id)], results
        end
      end

      it "should call all_with_archived without stub_only true if not predictive search" do
        stub_search_with_archived_tickets([ticket])
        Ticket.expects(:all_with_archived).with(stub_only: true).returns([ticket])
        Zendesk::Search::Elasticsearch::Search.search(user, search_string, {})
      end
    end

    describe "response is elasticsearch" do
      let(:stubbed_search_results) { { "results" => raw_stubbed_search_results, "count" => raw_stubbed_search_results.size, "elasticsearch" => { "took" => 123 } } }

      it "records elasticsearch time" do
        stub_search
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(regexp_matches(/Elasticsearch took: 123ms/))
        Zendesk::Search::Elasticsearch::Search.search(user, search_string)
      end
    end

    describe "elasticsearch response includes highlights" do
      let(:raw_stubbed_search_results) { stubbed_users.map { |user| {"id" => user.id, "type" => "user", "_highlights" => {}} } }

      it "annotates highlights" do
        stub_search
        results = Zendesk::Search::Elasticsearch::Search.search(user, search_string)
        highlight = results.highlights[:results].first

        assert_equal "user", highlight[:result_type]
        assert_equal user.id, highlight[:id]
      end

      describe "results include tickets" do
        let(:ticket) { tickets(:minimum_1) }
        let(:stubbed_tickets) { [ticket] }
        let(:raw_stubbed_search_results) { stubbed_tickets.map { |ticket| {"id" => ticket.id, "type" => "ticket", "nice_id" => ticket.nice_id, "_highlights" => {}} } }

        it "highlights based on nice_id" do
          stub_search
          results = Zendesk::Search::Elasticsearch::Search.search(user, search_string)
          highlight = results.highlights[:results].first

          assert_equal "ticket", highlight[:result_type]
          assert_equal ticket.nice_id, highlight[:id]
        end
      end
    end

    describe 'when monitoring performance' do
      let(:span) { Datadog::Span.new(Datadog.tracer, 'test-span') }
      let(:span_tags) { span.to_hash.fetch(:meta) }

      describe 'in a non-CBP endpoint' do
        let(:stubbed_search_results) { { "results" => raw_stubbed_search_results, "count" => raw_stubbed_search_results.size, "elasticsearch" => { "took" => 123 } } }

        before do
          stub_search

          span.expects(:set_tag).with('zendesk.account_id', user.account.id)
          span.expects(:set_tag).with('zendesk.account_subdomain', user.account.subdomain)
          span.expects(:set_tag).with('zendesk.account_is_sandbox', user.account.is_sandbox?)
          span.expects(:set_tag).with('zendesk.account_is_trial', user.account.is_trial?)
          span.expects(:set_tag).with('zendesk.user_id', user.id)

          span.expects(:set_tag).with('search.params.per_page', 15)
          span.expects(:set_tag).with('search.params.page', 1)

          span.expects(:set_tag).with('search.hits', 1)
          span.expects(:set_tag).with('search.search_time', any_parameters)
          span.expects(:set_tag).with('search.elasticsearch_time', any_parameters)

          ZendeskAPM.
            expects(:trace).
            with(
              Zendesk::Search::Elasticsearch::Search::HERE,
              service: Zendesk::Search::Elasticsearch::Search::APM_SERVICE_NAME
            ).
            yields(span)

          Rails.logger.expects(:append_attributes).with(search: { hits: 1, elasticsearch_time: 123 })
        end

        it 'sets APM facet tags when guide_search_for_articles is disabled' do
          span.expects(:set_tag).with('search.params.use_guide_search_for_articles', false)

          Zendesk::Search::Elasticsearch::Search.search(user, search_string)
        end

        it 'sets APM facet tags when guide_search_for_articles is enabled and searching for tickets' do
          Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).returns("50")

          span.expects(:set_tag).with('search.params.use_guide_search_for_articles', true)
          span.expects(:set_tag).with('search.params.type', 'ticket')

          Zendesk::Search::Elasticsearch::Search.search(user, search_string, use_guide_search_for_articles: true, type: 'ticket')
        end

        it 'sets APM facet tags when guide_search_for_articles is enabled and searching for articles' do
          Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:search_articles).returns("results" => [{ "id" => 1, "type" => "article"}], "count" => 1)

          span.expects(:set_tag).with('search.params.use_guide_search_for_articles', true)
          span.expects(:set_tag).with('search.params.type', 'article')

          Zendesk::Search::Elasticsearch::Search.search(user, search_string, use_guide_search_for_articles: true, type: 'article')
        end
      end

      describe 'in a CBP endpoint' do
        meta = { "has_more" => true, "after_cursor" => "bloh" }
        let(:stubbed_search_results) { { "results" => raw_stubbed_search_results, "meta" => meta, "elasticsearch" => { "took" => 123 } } }

        before do
          stub_search

          span.expects(:set_tag).with('zendesk.account_id', user.account.id)
          span.expects(:set_tag).with('zendesk.account_subdomain', user.account.subdomain)
          span.expects(:set_tag).with('zendesk.account_is_sandbox', user.account.is_sandbox?)
          span.expects(:set_tag).with('zendesk.account_is_trial', user.account.is_trial?)
          span.expects(:set_tag).with('zendesk.user_id', user.id)

          span.expects(:set_tag).with('search.hits', 0)
          span.expects(:set_tag).with('search.search_time', any_parameters)
          span.expects(:set_tag).with('search.elasticsearch_time', any_parameters)

          span.expects(:set_tag).with('search.params.cursor_based_pagination', true)

          ZendeskAPM.
            expects(:trace).
            with(
              Zendesk::Search::Elasticsearch::Search::HERE,
              service: Zendesk::Search::Elasticsearch::Search::APM_SERVICE_NAME
            ).
            yields(span)

          Rails.logger.expects(:append_attributes).with(search: { hits: 0, elasticsearch_time: 123 })
        end

        it 'sets APM facet tags for CBP parameters' do
          span.expects(:set_tag).with('search.params.cursor.size', 2)
          span.expects(:set_tag).with('search.params.cursor.after', 'blah')
          span.expects(:set_tag).with('search.params.filter.type', 'bleh')

          refute_includes span_tags, 'search.params.page'
          refute_includes span_tags, 'search.params.per_page'

          Zendesk::Search::Elasticsearch::Search.search(user, search_string, filter_type: 'bleh', page_size: '2', page_after: 'blah', cursor_based_pagination: true)
        end
      end
    end
  end

  describe ".expand_query_from_options" do
    it "maps order parameter" do
      expanded = Zendesk::Search::Elasticsearch::Search.send(:expand_query_from_options, "my query", order: "created_at")
      assert_equal "order_by:created_at my query", expanded
    end

    it "maps sort_mode parameter" do
      expanded = Zendesk::Search::Elasticsearch::Search.send(:expand_query_from_options, "my query", order: "created_at", sort_mode: "desc")
      assert_equal "order_by:created_at sort:desc my query", expanded
    end

    it "ignores sort_mode relevance" do
      expanded = Zendesk::Search::Elasticsearch::Search.send(:expand_query_from_options, "my query", sort_mode: :relevance)
      assert_equal "my query", expanded
    end
  end

  describe ".has_via_constraint?" do
    ['via:stuff', 'id:123 via:something', ' via:', '(via:stuff)', ' (via:blah)'].each do |success_string|
      describe "for '#{success_string}'" do
        it "returns true" do
          assert Zendesk::Search::Elasticsearch::Search.send(:has_via_constraint?, success_string)
        end
      end
    end

    ['notvia:stuff', 'id:123 vial:something'].each do |fail_string|
      describe "for '#{fail_string}'" do
        it "returns false" do
          refute Zendesk::Search::Elasticsearch::Search.send(:has_via_constraint?, fail_string)
        end
      end
    end
  end

  describe '.anychannel_via_map' do
    before(:all) do
      @user = users(:minimum_search_user)
      @expected = []
      @ris_list = []
      2.times do |ris_idx|
        ris = ::Channels::AnyChannel::RegisteredIntegrationService.new(
          name: "ris#{ris_idx}",
          search_name: "ris#{ris_idx}",
          external_id: ris_idx,
          manifest_url: "http://localhost/ris#{ris_idx}_manifest.json",
          admin_ui_url: "http://localhost/ris#{ris_idx}_admin"
        )
        ris.account = @user.account
        ris.save!

        instance_ids = []
        3.times do |isi_idx|
          isi = ::Channels::AnyChannel::IntegrationServiceInstance.new(
            registered_integration_service: ris,
            name: "isi_#{ris_idx}_#{isi_idx}",
            metadata: '{}',
            state: ::Channels::AnyChannel::IntegrationServiceInstance::State::ACTIVE
          )
          isi.account = @user.account
          isi.save!
          instance_ids.push(isi.id)
        end

        @expected.push(
          search_name: ris.search_name,
          via_id: ViaType.ANY_CHANNEL,
          instance_ids: instance_ids
        )

        @ris_list << ris
      end
    end
  end
end
