require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'Zendesk::Search::Error' do
  describe "A search error" do
    before do
      @error = Zendesk::Search::Error.new(Exception.new)
    end

    describe "timeout" do
      before do
        @error = Zendesk::Search::Error.new(Timeout::Error.new)
      end

      it "is a known error" do
        assert_equal false, @error.unknown?
      end

      it "provides a message" do
        assert_equal "Sorry, we could not complete your search query. Please try again in a moment.", @error.message
      end
    end

    describe "known" do
      before do
        @error = Zendesk::Search::Error.new(ZendeskSearch::QueryError.new("Invalid date format"))

        assert_equal false, @error.unknown?
      end

      it "provides a message" do
        assert_equal 'Invalid search: Invalid date format', @error.message
      end
    end

    describe "unknown" do
      before do
        @error = Zendesk::Search::Error.new(ArgumentError.new)
        assert @error.unknown?
      end

      it "provides a message" do
        assert_equal 'Internal Server Error', @error.message
      end
    end
  end

  describe "Error handling" do
    before do
      @query = Object.new
      @query.extend(Zendesk::Search::Error::Handling)
      def @query.result=(value) # rubocop:disable Style/TrivialAccessors
        @result = value
      end
    end

    describe "when encountering an exception in the results" do
      before do
        result_with_exception = Zendesk::Search::Results.empty(Exception.new)
        @query.result = result_with_exception
      end

      it "does not report success" do
        assert_equal false, @query.success?
      end

      it "provides an error message" do
        assert @query.error_message
      end
    end

    describe "when encountering an exception" do
      before do
        @result = @query.with_error_handling { raise Timeout::Error }
      end

      it "does not report success" do
        assert_equal false, @query.success?
      end

      it "provides an error message" do
        assert @query.error_message
      end

      it "provides empty search results" do
        assert @result.is_a?(Zendesk::Search::Results)
      end

      it "records unknown exceptions" do
        @query.stubs(:raise_unknown_exceptions?).returns(false)
        Zendesk::Search::Error.any_instance.expects(:record)

        @query.with_error_handling { raise 'Boom!' }
      end

      it "does not record known exceptions" do
        Zendesk::Search::Error.any_instance.expects(:record).never
        @query.with_error_handling { raise Timeout::Error }
      end
    end

    describe "when not encountering an exception" do
      before do
        @result = @query.with_error_handling { 'hi' }
      end

      it "provides the result" do
        assert_equal 'hi', @result
      end

      it "is successful" do
        assert(@query.success?)
      end

      it "does not have an error message" do
        assert_nil @query.error_message
      end
    end
  end
end
