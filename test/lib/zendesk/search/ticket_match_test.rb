require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 8

describe 'TicketMatch' do
  describe "A ticket match" do
    before do
      @account      = stub('Account', tickets: stub, sharing_agreements: stub(any?: false))
      @user         = stub(is_agent?: true)
      @ticket       = stub(nice_id: '1234')
      @ticket_match = Zendesk::Search::TicketMatch.new.tap do |tm|
        tm.account = @account
        tm.user = @user
        tm.stubs(allowed_tickets: @account.tickets)
        tm.stubs(:paginated_results)
      end
    end

    it "scopes ticket access by user" do
      ticket_match = Zendesk::Search::TicketMatch.new.tap do |tm|
        tm.account = @account
        tm.user    = @user
      end

      ticket_match.account.tickets.expects(:for_user).with(@user)

      ticket_match.allowed_tickets
    end

    describe "with a query for a specific ticket" do
      before do
        @ticket_match.query = '#1234'
      end

      it "does not search remote tickets if the user is not an agent" do
        @user = stub(is_agent?: false)
        @ticket_match.expects(:ticket)
        @ticket_match.expects(:remote_tickets).never

        @ticket_match.results
      end

      it "does not search remote tickets if no shared tickets are found" do
        @ticket_match.expects(:ticket_ids_from_shared_tickets).returns []
        @ticket_match.expects(:allowed_tickets).never

        @ticket_match.send(:remote_tickets)
      end

      it "returns [] if no shared tickets are found" do
        @ticket_match.expects(:ticket_ids_from_shared_tickets).returns []
        @ticket_match.expects(:allowed_tickets).never

        assert_equal([], @ticket_match.send(:remote_tickets))
      end

      it "does not search remote tickets if account has no sharing agreements" do
        @account.expects(:sharing_agreements).returns(stub(any?: false))
        @ticket_match.expects(:ticket)
        @ticket_match.expects(:remote_tickets).never

        @ticket_match.results
      end

      it "searchs remote tickets if account has sharing agreements" do
        @account.expects(:sharing_agreements).returns(stub(any?: true))
        @ticket_match.expects(:ticket)
        @ticket_match.expects(:remote_tickets).returns []

        @ticket_match.results
      end

      it "finds the ticket" do
        @ticket_match.allowed_tickets.expects(:find_by_nice_id).with('1234')
        @ticket_match.stubs(paginated_results: stub(present?: true))

        assert(@ticket_match.found?)
      end

      it "uses the supplied conditions when finding the ticket" do
        @ticket_match.conditions = { organization_id: 1 }
        @ticket_match.allowed_tickets.expects(:where).with(organization_id: 1).returns(@account.tickets)
        @ticket_match.allowed_tickets.expects(:find_by_nice_id).with('1234')
        @ticket_match.stubs(paginated_results: stub(present?: true))

        assert(@ticket_match.found?)
      end
    end

    describe "with a query that's not for a specific ticket" do
      before do
        @ticket_match.query = '$1234'
      end

      it "does not try to find a ticket" do
        @ticket_match.allowed_tickets.expects(:find_by_nice_id).never
        assert_equal false, @ticket_match.found?
      end
    end

    it "detects queries that are nice ids" do
      @ticket_match.query = '#1234'
      assert_equal '1234', @ticket_match.nice_id

      @ticket_match.query = '1234'
      assert_equal '1234', @ticket_match.nice_id

      @ticket_match.query = '$1234'
      assert_nil @ticket_match.nice_id

      @ticket_match.query = '#'
      assert_nil @ticket_match.nice_id
    end
  end
end
