require_relative '../../../support/test_helper'

SingleCov.covered!

class RoutingControllerTest < ActionController::TestCase
  class RoutingControllerTestController < ApplicationController
    include Zendesk::Routing::ControllerSupport
  end

  tests RoutingControllerTestController
  use_test_routes

  describe Zendesk::Routing::ControllerSupport do
    describe '#parse_error' do
      describe 'when there is a response' do
        before do
          @response = @controller.send(
            :parse_error,
            Kragle::BadRequest.new(
              '400 Bad Request',
              Faraday::Response.new(status: 400, body: {'errors' => errors})
            )
          )
        end

        describe 'when errors are absent in response' do
          let(:errors) { nil }

          it 'returns 0 as the code' do
            assert_equal ({code: 0}), @response
          end
        end

        describe 'when errors are present in response' do
          describe 'and first error is a referential integrity error' do
            let(:errors) { [{'title' => 'Ref Integrity Error'}] }

            it 'returns 1 as the code' do
              assert_equal ({code: 1}), @response
            end
          end

          describe 'and first error is a conflict error' do
            let(:errors) { [{'title' => 'Attribute Value Exists'}] }

            it 'returns 2 as the code' do
              assert_equal ({code: 2}), @response
            end
          end

          describe 'and first error is a limit exceeded error' do
            let(:errors) { [{'title' => 'Too Many Attributes'}] }

            it 'returns 3 as the code for attributes' do
              assert_equal 3, @response[:code]
            end

            it 'returns appropriate error title' do
              assert_equal 'Too Many Attributes', @response[:error]
            end

            it 'returns appropriate error description' do
              assert_equal 'Limit reached. Too many attributes have ' \
                   'already been created.', @response[:description]
            end
          end

          describe 'and first error is a limit exceeded error' do
            let(:errors) { [{'title' => 'Too Many Attribute Values'}] }

            it 'returns 3 as the code for attribute values' do
              assert_equal 3, @response[:code]
            end

            it 'returns appropriate error title' do
              assert_equal 'Too Many Attribute Values', @response[:error]
            end

            it 'returns appropriate error description' do
              assert_equal 'Limit reached. Too many attribute values have ' \
                   'already been created.', @response[:description]
            end
          end

          describe 'and first error is an unrecognized error' do
            let(:errors) { [{'title' => 'Unknown Error'}] }

            it 'returns 0 as the code' do
              assert_equal ({code: 0}), @response
            end
          end
        end
      end

      describe 'when there is no response' do
        before do
          @response = @controller.send(
            :parse_error,
            Kragle::BadRequest.new('400 Bad Request', nil)
          )
        end

        it 'returns 0 as the code' do
          assert_equal ({code: 0}), @response
        end
      end
    end

    describe '#with_error_handling' do
      describe 'no exception is raised' do
        it 'does not render' do
          @controller.expects(:render).never
          @controller.send(:with_error_handling) {}
        end
      end

      describe 'Kragle::BadRequest is raised' do
        it 'renders and returns :bad_request' do
          @controller.
            expects(:render).
            with(json: {code: 0}, status: :bad_request)
          @controller.send(:with_error_handling) { fail Kragle::BadRequest }
        end
      end

      describe 'Kragle::ResponseError subclass is raised' do
        describe 'response and status are available' do
          it 'renders and returns :status from exception' do
            @controller.
              expects(:render).
              with(json: {code: 0}, status: :unauthorized)
            @controller.send(:with_error_handling) do
              response = Faraday::Response.new(status: :unauthorized)
              fail Kragle::Unauthorized.new('Unauthorized', response)
            end
          end
        end

        describe 'status is not available' do
          it 'renders and returns :internal_server_error status' do
            @controller.
              expects(:render).
              with(json: {code: 0}, status: :internal_server_error)
            @controller.send(:with_error_handling) do
              fail Kragle::Unauthorized.new('Unauthorized',
                Faraday::Response.new)
            end
          end
        end

        describe 'response is not available' do
          it 'renders and returns :internal_server_error status' do
            @controller.
              expects(:render).
              with(json: {code: 0}, status: :internal_server_error)
            @controller.send(:with_error_handling) do
              fail Kragle::Unauthorized.new('Unauthorized', nil)
            end
          end
        end
      end
    end
  end
end
