require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Routing::Attribute::Definitions do
  fixtures :all

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:definitions) do
    Zendesk::Routing::Attribute::Definitions.new(account, user)
  end

  describe '#conditions_all' do
    let(:condition_context) { stub('condition_context') }

    let(:condition_definitions) do
      [
        {
          value: 'priority_id',
          title: 'Priority',
          values: {type: 'list', list: []},
          operators: [
            {value: :is,     title: 'Is'},
            {value: :is_not, title: 'Is not'}
          ]
        }
      ]
    end

    before do
      Zendesk::Rules::Context.
        stubs(:new).
        with(
          account,
          user,
          component_type: :condition,
          condition_type: :all,
          rule_type:      :attribute
        ).returns(condition_context)

      ZendeskRules::Definitions::Conditions.
        stubs(:definitions_as_json).
        with(condition_context).
        returns(condition_definitions)

      definitions.conditions_all
    end

    before_should 'construct a condition from each definition' do
      condition_definitions.each do |definition|
        Zendesk::Routing::Attribute::Condition.
          expects(:for_definition).
          with(definition, account: account)
      end
    end
  end

  describe '#conditions_any' do
    let(:condition_context) { stub('condition_context') }

    let(:condition_definitions) do
      [
        {
          value: 'priority_id',
          title: 'Priority',
          values: {type: 'list', list: []},
          operators: [
            {value: :is,     title: 'Is'},
            {value: :is_not, title: 'Is not'}
          ]
        }
      ]
    end

    before do
      Zendesk::Rules::Context.
        stubs(:new).
        with(
          account,
          user,
          component_type: :condition,
          condition_type: :any,
          rule_type:      :attribute
        ).returns(condition_context)

      ZendeskRules::Definitions::Conditions.
        stubs(:definitions_as_json).
        with(condition_context).
        returns(condition_definitions)

      definitions.conditions_any
    end

    before_should 'construct a condition from each definition' do
      condition_definitions.each do |definition|
        Zendesk::Routing::Attribute::Condition.
          expects(:for_definition).
          with(definition, account: account)
      end
    end
  end
end
