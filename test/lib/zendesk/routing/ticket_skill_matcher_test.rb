require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Routing::TicketSkillMatcher do
  fixtures :accounts, :users, :tickets

  let(:user)          { users(:minimum_admin) }
  let(:skill_matcher) { Zendesk::Routing::TicketSkillMatcher.new(user) }
  let(:deco)          { stub('deco') }

  let(:ticket_requirements) { stub('ticket_requirements') }

  let(:ticket_ids) do
    %i[minimum_6 minimum_5 minimum_4].map do |ticket|
      tickets(ticket).id
    end
  end

  before do
    Zendesk::Deco::Client.stubs(:new).returns(deco)

    Zendesk::Deco::Request::TicketRequirements.
      stubs(:new).
      with(deco).
      returns(ticket_requirements)
  end

  describe '#matching_tickets' do
    before do
      ticket_requirements.
        stubs(:fulfilled_ticket_ids).
        returns([ticket_ids.last])
    end

    it 'gets tickets from deco' do
      assert_equal(
        Array(ticket_ids.last),
        skill_matcher.matching_tickets(ticket_ids)
      )
    end

    describe 'when there is an error' do
      before do
        ticket_requirements.
          stubs(:fulfilled_ticket_ids).
          raises(Faraday::Error)
      end

      it 'returns the passed-in ticket ids' do
        assert_equal(
          ticket_ids,
          skill_matcher.matching_tickets(ticket_ids)
        )
      end
    end
  end
end
