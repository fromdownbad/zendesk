require_relative '../../../support/test_helper'

SingleCov.covered!

class TestViewController < Api::V2::Rules::ViewsController
  include Zendesk::Routing::ViewControllerSupport
end

describe Zendesk::Routing::ViewControllerSupport do
  fixtures :accounts, :users, :tickets

  let(:account)    { accounts(:minimum) }
  let(:controller) { TestViewController.new }
  let(:user)       { users(:minimum_agent) }
  let(:page_size)  { 1 }
  let(:tickets)    { accounts(:minimum).tickets }
  let(:matcher)    { stub('ticket_matcher') }

  before do
    controller.stubs(:current_user).returns(user)
    controller.stubs(:current_account).returns(account)

    controller.stubs(:params).returns(per_page: page_size)
  end

  describe '#match_tickets' do
    before do
      Zendesk::Routing::TicketSkillMatcher.stubs(:new).returns(matcher)
    end

    describe 'when first page of tickets contain required number of matches' do
      let(:first_page_tickets) { tickets.first(2) }
      let(:ticket_ids)         { first_page_tickets.map(&:id) }
      let(:matching_tickets)   { first_page_tickets.last(1) }
      let(:page_size)          { 1 }

      let(:expected_ticket_ids) { matching_tickets.map(&:id).to_set }

      before do
        matcher.
          stubs(:matching_tickets).
          with(ticket_ids).
          returns(ticket_ids.last(1))

        controller.stubs(:oversample_page_size).returns(2)
        controller.stubs(:view_tickets).returns(first_page_tickets)
        # controller.params[:per_page] = 1
      end

      it 'returns matching tickets' do
        assert_equal(
          expected_ticket_ids,
          controller.send(:match_tickets, 1).map(&:id).to_set
        )
      end

      it 'does not mark the result as `insufficient_results`' do
        controller.send(:match_tickets, 2)

        refute controller.send(:includes).include?(:insufficient_results)
      end
    end

    describe 'when there are more than requested matching tickets' do
      let(:total_tickets) { tickets.first(6) }
      let(:ticket_ids) { total_tickets.map(&:id) }
      let(:expected_ticket_ids) { total_tickets.first(4).map(&:id).to_set }

      before do
        matcher.
          stubs(:matching_tickets).
          with(ticket_ids).
          returns(ticket_ids)

        controller.stubs(:view_tickets).returns(total_tickets)
      end

      it 'returns only requested matching tickets' do
        assert_equal(
          expected_ticket_ids,
          controller.send(:match_tickets, 4).map(&:id).to_set
        )
      end
    end

    describe 'first page of tickets doesnt contain sufficent matches' do
      let(:tickets_page_1) { tickets.first(2) }
      let(:tickets_page_2) { tickets.last(2) }
      let(:page_size)      { 2 }

      let(:page_1_ticket_ids) { tickets_page_1.map(&:id) }
      let(:page_2_ticket_ids) { tickets_page_2.map(&:id) }

      let(:matching_ticket_ids) do
        [tickets_page_1.last, tickets_page_2.last].map(&:id).to_set
      end

      before do
        controller.
          stubs(:view_tickets).
          returns(tickets_page_1, tickets_page_2)

        matcher.
          stubs(:matching_tickets).
          returns(
            page_1_ticket_ids.last(1),
            page_2_ticket_ids.last(1)
          )

        controller.stubs(:oversample_page_size).returns(2)
      end

      it 'logs match ticket metrics' do
        Zendesk::StatsD::Client.
          any_instance.
          expects(:count).with('match_tickets_deco_loops', 2)
        Zendesk::StatsD::Client.
          any_instance.
          expects(:count).with('scrolled_tickets_count', 4)
        controller.send(:match_tickets, 2)
      end

      it 'returns matching tickets' do
        assert_equal(
          matching_ticket_ids,
          controller.send(:match_tickets, 2).map(&:id).to_set
        )
      end

      it 'does not mark the result as `insufficient_results`' do
        controller.send(:match_tickets, 2)

        refute controller.send(:includes).include?(:insufficient_results)
      end
    end

    describe 'when there are no sufficient tickets to match' do
      let(:first_page_tickets) { tickets.first(2) }
      let(:ticket_ids)         { first_page_tickets.map(&:id) }
      let(:matching_tickets)   { first_page_tickets.last(1) }
      let(:page_size)          { 2 }

      before do
        controller.stubs(:view_tickets).returns(first_page_tickets)

        matcher.
          stubs(:matching_tickets).
          with(ticket_ids).
          returns(ticket_ids.last(1))
      end

      it 'reports timing metrics' do
        Zendesk::StatsD::Client.
          any_instance.
          expects(:time).with('match_tickets_with_deco')
        controller.send(:match_tickets, 2)
      end

      it 'logs total number of loops taken to match tickets' do
        Zendesk::StatsD::Client.
          any_instance.expects(:count).
          with(anything, anything)
        Zendesk::StatsD::Client.
          any_instance.
          expects(:count).with('match_tickets_deco_loops', 1)

        controller.send(:match_tickets, 2)
      end

      it 'logs total number of tickets scrolled' do
        Zendesk::StatsD::Client.
          any_instance.expects(:count).
          with(anything, anything)
        Zendesk::StatsD::Client.
          any_instance.
          expects(:count).with('scrolled_tickets_count', 2)

        controller.send(:match_tickets, 2)
      end

      it 'returns results' do
        assert_equal(
          matching_tickets.map(&:id).to_set,
          controller.send(:match_tickets, 2).map(&:id).to_set
        )
      end

      it 'does not mark the result as `insufficient_results`' do
        controller.send(:match_tickets, 2)

        refute controller.send(:includes).include?(:insufficient_results)
      end
    end

    describe 'matching tickets takes too long' do
      let(:first_page_tickets) { tickets.first(2) }
      let(:ticket_ids)         { first_page_tickets.map(&:id) }
      let(:matching_tickets)   { first_page_tickets.last(1) }
      let(:page_size)          { 2 }

      before do
        controller.stubs(:view_tickets).returns(first_page_tickets)

        matcher.
          stubs(:matching_tickets).
          with(ticket_ids).
          returns(ticket_ids.last(1))

        controller.stubs(:oversample_page_size).returns(2)

        controller.
          stubs(:processing_end_time).
          returns(Time.current - 1.year)
      end

      it 'returns partial results' do
        assert_equal(
          matching_tickets.map(&:id).to_set,
          controller.send(:match_tickets, 2).map(&:id).to_set
        )
      end

      it 'logs it through statsd client' do
        Zendesk::StatsD::Client.
          any_instance.
          expects(:increment).
          with('insufficient_results')

        controller.send(:match_tickets, 2)
      end

      it 'marks the result as `insufficient_results`' do
        controller.send(:match_tickets, 2)

        assert controller.send(:includes).include?(:insufficient_results)
      end
    end

    describe 'when matching tickets takes too may loops' do
      let(:tickets) do
        Array.new(10) do
          FactoryBot.create(:ticket, account: account)
        end
      end

      let(:page_size) { 1 }

      before do
        controller.
          stubs(:view_tickets).
          returns(*(0..9).map { |i| Array(tickets[i]) })

        matcher.
          stubs(:matching_tickets).
          returns([])

        controller.stubs(:oversample_page_size).returns(1)
      end

      it 'returns partial results' do
        assert_equal(
          [],
          controller.send(:match_tickets, 2)
        )
      end

      it 'logs it through statsd client' do
        Zendesk::StatsD::Client.
          any_instance.
          expects(:increment).
          with('insufficient_results')

        controller.send(:match_tickets, 2)
      end

      it 'marks the result as `insufficient_results`' do
        controller.send(:match_tickets, 2)

        assert controller.send(:includes).include?(:insufficient_results)
      end
    end
  end

  describe '#oversample_page_size' do
    let(:oversample_page_size) do
      3 * Zendesk::Routing::ViewControllerSupport::DECO_OVERSAMPLING_FACTOR
    end

    it 'returns the value' do
      assert_equal(
        oversample_page_size,
        controller.send(:oversample_page_size, 3)
      )
    end
  end
end
