require_relative "../../support/test_helper"

SingleCov.covered!

class CommentOnQueriesTestClass < ActionController::Base
  include Zendesk::CommentOnQueries

  def index
    render json: 'something'
  end
end

class CommentOnQueries < ActionController::TestCase
  tests CommentOnQueriesTestClass
  use_test_routes

  fixtures :users, :accounts

  let(:uri) { 'https://test.host/test/route/comment_on_queries/comment_on_queries_test_class/index' }

  let(:comment) do
    {
      service: "classic-rails-controller",
      resource_name: 'CommentOnQueriesTestClass#index',
      code_owner: '@zendesk/squonk',
      account_id: users(:minimum_agent).account.id,
      user_id: users(:minimum_agent).id,
      from_app: 123,
      trace_id: nil,
      kill_flag: false,
      timeout: '30'
    }
  end

  before do
    CommentOnQueriesTestClass.any_instance.stubs(current_account: accounts(:minimum))
    CommentOnQueriesTestClass.any_instance.stubs(current_user: users(:minimum_agent))
    request.headers['X-Zendesk-App-Id'] = '123'
    Codeowner.stubs(codeowners: Codeowner.codeowners.merge("app/controllers/comment_on_queries_test_class.rb" => "@zendesk/squonk"))
  end

  describe "with arturo enabled" do
    before { Account.any_instance.stubs(:has_comment_on_queries?).returns(true) }

    it "adds comments to queries" do
      ActiveRecord::Comments.expects(:comment).with(comment.to_json).once
      get :index
    end

    describe "with unowned controller" do
      let(:expected_comment) { comment.merge(resource_name: 'UnownedCommentOnQueriesTestClass#index', code_owner: '@zendesk/support-review').to_json }

      before { CommentOnQueriesTestClass.stubs(:name).returns('UnownedCommentOnQueriesTestClass') }

      it "adds comment with default code owner" do
        ActiveRecord::Comments.expects(:comment).with(expected_comment).once
        get :index
      end
    end

    describe "with invalid app id header" do
      let(:expected_comment) { comment.merge(from_app: 'no').to_json }
      let(:unexpected_comment) { comment.merge(from_app: 'malicious input').to_json }

      before { request.headers['X-Zendesk-App-Id'] = 'malicious input' }

      it "does not include app id header value" do
        ActiveRecord::Comments.expects(:comment).with(expected_comment).once
        ActiveRecord::Comments.expects(:comment).with(unexpected_comment).never
        get :index
      end
    end

    describe "with datadog trace_id" do
      let(:expected_comment) { comment.merge(trace_id: 123).to_json }

      before do
        active_span_stub = stub(trace_id: 123)
        CommentOnQueriesTestClass.any_instance.stubs(:datadog_active_span).returns active_span_stub
      end

      it "includes trace_id in the comment" do
        ActiveRecord::Comments.expects(:comment).with(expected_comment).once
        get :index
      end
    end

    describe "without Datadog tracer" do
      let(:expected_comment) { comment.merge(trace_id: nil).to_json }

      before do
        Datadog.stubs(:tracer)
      end

      it "doesn't raise an error" do
        ActiveRecord::Comments.expects(:comment).with(expected_comment).once
        get :index
        assert_response 200
      end
    end
  end

  describe "with query params" do
    before { request.stubs(:original_url).returns('https://test.host/test/route/comment_on_queries/comment_on_queries_test_class/index?foo=bar') }

    it "excludes query parameters from the comment" do
      ActiveRecord::Comments.expects(:comment).with(comment.to_json).once
      get :index
      assert_response 200
    end
  end

  describe "with uri containing inline SQL comment terminators" do
    before { request.stubs(:original_url).returns('https://test.host/*/test/*/test') }

    it "substitutes out asterisk characters in the comment" do
      ActiveRecord::Comments.expects(:comment).with { |comment_body| comment_body.scan('*').count.zero? }.once
      get :index
    end
  end

  describe "with arturo disabled" do
    before { Account.any_instance.stubs(:has_comment_on_queries?).returns(false) }

    it "does not add comments to queries" do
      ActiveRecord::Comments.expects(:comment).with(comment.to_json).never
      get :index
    end
  end
end
