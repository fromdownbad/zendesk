require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 6

describe Zendesk::Auth::OauthDelegator do
  fixtures :accounts

  describe ".request_oauth!" do
    describe 'facebook' do
      before do
        @env = Rack::MockRequest.env_for('/')
        @env['zendesk.account'] = accounts(:minimum)
      end
      describe 'when Arturo for fb_graph_api_v9 is disabled' do
        it "returns url using 2.7 endpoint" do
          url = Zendesk::Auth::OauthDelegator.request_oauth!(
            'facebook',
            "https://support.zendesk-test.com/ping/redirect_to_account",
            true,
            @env
          )
          assert_match(/https:\/\/www.facebook.com\/v2.7/, url)
        end
      end

      describe 'when Arturo for fb_graph_api_v9 is enabled' do
        it "returns url using 9.0 endpoint" do
          Arturo.enable_feature! :fb_graph_api_v9
          url = Zendesk::Auth::OauthDelegator.request_oauth!(
            'facebook',
            "https://support.zendesk-test.com/ping/redirect_to_account",
            true,
            @env
          )
          assert_match(/https:\/\/www.facebook.com\/v9.0/, url)
        end
      end
    end
  end
end
