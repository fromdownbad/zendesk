require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Auth::ListUtils do
  fixtures :accounts

  describe ".address_in_list?" do
    def in_list?(address, list)
      Zendesk::Auth::ListUtils.address_in_list?(address, list)
    end

    it "detects elements in a list with various tokens of separation" do
      refute in_list?(nil, 'something')
      refute in_list?('morten@zendesk.com', '')
      refute in_list?('morten@zendesk.com', '    ')
      refute in_list?('morten@zendesk.com', nil)
      refute in_list?('morten@zendesk.com', 'suspend:morten@zendesk.com')
      refute in_list?('mikkel@zendesk.com', 'morten@zendesk.com')
      refute in_list?('morten', 'morten@zendesk.com')

      assert in_list?('morten@zendesk.com', 'zendesk.com')
      assert in_list?('morten@zendesk.com', 'morten@zendesk.com')
      assert in_list?('morten@zendesk.com', 'ib@skibskalle.com,morten@zendesk.com')
      assert in_list?('morten@zendesk.com', 'ib@skibskalle.com;morten@zendesk.com')
      assert in_list?('morten@zendesk.com', 'ib@skibskalle.com morten@zendesk.com')

      assert_equal 'reject:foo@bar.com zendesk.com hest.com', 'reject:foo@bar.com,zendesk.com;hest.com'.tokenize.join(' ')
    end

    it "is usable for basic boolean operations" do
      whitelist = 'zendesk.com subject:primdahl'
      blacklist = '*'

      allows = ['morten@zendesk.com', 'hans@zendesk.com']
      disall = ['primdahl@mac.com']

      allows.each do |a|
        assert in_list?(a, whitelist) || !in_list?(a, blacklist)
      end

      disall.each do |a|
        refute in_list?(a, whitelist) && in_list?(a, blacklist)
      end

      whitelist = ''
      blacklist = 'zendesk.com'

      refute in_list?('primdahl@mac.com', blacklist)
      refute in_list?('morten@zendesk.com', whitelist) && in_list?('morten@zendesk.com', blacklist)
    end
  end

  describe "Account blacklists and whitelists" do
    before do
      @account = accounts(:minimum)
    end

    it "supports blacklisting of email addresses and domains" do
      @account.domain_blacklist = 'filiong@gong.com google.com'
      @account.domain_whitelist = nil

      assert @account.blacklists?('primdahl@google.com')
      assert @account.blacklists?('filiong@gong.com')
      refute @account.whitelists?('filiong@gong.com')
    end

    it "supports blacklisting everything, yet give the whitelist precedence" do
      @account.domain_blacklist = '*'
      @account.domain_whitelist = 'gmail.com'

      assert @account.allows?('primdahl@gmail.com')
      refute @account.allows?('morten@zendesk.com')
      assert @account.blacklists?('morten@zendesk.com')
      refute @account.whitelists?('morten@zendesk.com')

      @account.domain_blacklist = ''
      assert @account.allows?('morten@zendesk.com')

      @account.domain_blacklist = 'zendesk.com'
      refute @account.allows?('morten@zendesk.com')
      assert @account.blacklists?('morten@zendesk.com')
      refute @account.whitelists?('morten@zendesk.com')

      @account.domain_whitelist = ''
      refute @account.whitelists?('morten@gmail.com')
    end

    it "does not whitelist subdomains or domain addresses" do
      @account.domain_blacklist = '*'
      @account.domain_whitelist = 'example.com dingeling@gmail.com'

      assert @account.whitelists?('morten@example.com')
      assert @account.whitelists?('morten@some.example.com')
      assert @account.whitelists?('morten@what.some.example.com')
      assert @account.whitelists?('dingeling@gmail.com')
      refute @account.whitelists?('hansen@gmail.com')
    end

    it "does not whitelist domains and emails that end in a whitelisted domains and emails" do
      @account.domain_whitelist = 'example.com example@zendesk.com'

      refute @account.whitelists?('hansen@otherexample.com')
      refute @account.whitelists?('otherexample@zendesk.com')
    end
  end
end
