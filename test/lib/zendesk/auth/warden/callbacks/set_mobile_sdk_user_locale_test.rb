require_relative "../../../../../support/test_helper"

SingleCov.covered!

module Zendesk::Auth::Warden::Callbacks
  describe SetMobileSdkUserLocale do
    fixtures :users, :translation_locales

    let(:user) { users(:minimum_sdk_end_user) }
    let(:agent) { users(:minimum_agent) }
    let(:account) { user.account }
    let(:japanese) { translation_locales(:japanese) }
    let(:norwegian) { translation_locales(:norwegian) }
    let(:spanish) { translation_locales(:spanish) }

    describe 'when the authentication is not through sdk anonymous strategy' do
      let(:warden) { stub(winning_strategy: nil) }

      it 'does nothing' do
        SetUserLocaleJob.expects(:enqueue).never
        User.any_instance.expects(:update_attribute).never

        callback = SetMobileSdkUserLocale.new(user, warden)
        callback.set_locale
      end
    end

    [Zendesk::Auth::Warden::SdkAnonymousStrategy, Zendesk::Auth::Warden::SdkJwtStrategy].each do |klass|
      describe "when the authentication is done through #{klass} strategy" do
        let(:warden) { stub(winning_strategy: klass.new(nil)) }
        let(:language) { 'GIVEN LANGUAGE' }

        describe_with_arturo_disabled :sdk_auth_synchronous_user_locale do
          it 'enqueues a job to set the user language to the one received in the `Accept-Language` header' do
            warden.stubs(request: stub(env: { 'HTTP_ACCEPT_LANGUAGE' => language }))

            SetUserLocaleJob.
              expects(:enqueue).
              with(account_id: account.id, user_id: user.id, language: language)

            callback = SetMobileSdkUserLocale.new(user, warden)
            callback.set_locale
          end

          it 'enqueues a job to set the user language to English if the `Accept-Language` header is empty' do
            warden.stubs(request: stub(env: { 'HTTP_ACCEPT_LANGUAGE' => nil }))

            SetUserLocaleJob.
              expects(:enqueue).
              with(account_id: account.id, user_id: user.id, language: ENGLISH_BY_ZENDESK)

            callback = SetMobileSdkUserLocale.new(user, warden)
            callback.set_locale
          end
        end

        describe_with_arturo_enabled :sdk_auth_synchronous_user_locale do
          describe 'without `Accept-Language` header' do
            before do
              warden.stubs(request: stub(env: { 'HTTP_ACCEPT_LANGUAGE' => nil }))
            end

            it 'does not update the user language if it is already ENGLISH_BY_ZENDESK' do
              agent.expects(:update_attribute).with(:translation_locale, ENGLISH_BY_ZENDESK).never
              SetMobileSdkUserLocale.new(agent, warden).set_locale
            end

            it 'updates the user language if it is not already ENGLISH_BY_ZENDESK' do
              agent.expects(:update_attribute).with(:translation_locale, ENGLISH_BY_ZENDESK).never
              SetMobileSdkUserLocale.new(agent, warden).set_locale
            end
          end

          describe 'with non-empty `Accept-Language` header' do
            describe 'when a compatible one is found in the settle' do
              before do
                warden.stubs(request: stub(env: { 'HTTP_ACCEPT_LANGUAGE' => 'ja' }))
                Account.any_instance.stubs(:available_languages).returns([japanese, norwegian])
              end

              it 'sets the user language to the compatible one if user has a different one' do
                agent.expects(:update_attribute).with(:translation_locale, japanese).once
                SetMobileSdkUserLocale.new(agent, warden).set_locale
              end

              it 'does not update the user if the user has that one already' do
                agent.update_attribute(:translation_locale, japanese)
                agent.expects(:update_attribute).with(:translation_locale, japanese).never
                SetMobileSdkUserLocale.new(agent, warden).set_locale
              end
            end

            describe 'when no compatible one is found in the settler' do
              before do
                warden.stubs(request: stub(env: { 'HTTP_ACCEPT_LANGUAGE' => 'ja' }))
                Account.any_instance.stubs(:available_languages).returns([spanish, norwegian])
                Account.any_instance.stubs(:translation_locale).returns(norwegian)
              end

              it 'sets the user language to the account default locale' do
                agent.update_attribute(:translation_locale, japanese)
                agent.expects(:update_attribute).with(:translation_locale, norwegian).once
                SetMobileSdkUserLocale.new(agent, warden).set_locale
              end
            end
          end

          describe 'when an exception happens in the sync method' do
            before do
              warden.stubs(request: stub(env: { 'HTTP_ACCEPT_LANGUAGE' => 'ja' }))
              Zendesk::I18n::LanguageSettlement.any_instance.stubs(:find_matching_zendesk_locale).raises(StandardError.new)
              Zendesk::StatsD::Client.any_instance.expects(:increment).with('auth.warden.callbacks.set_mobile_sdk_user_locale.exception', tags: ["exception:StandardError"])
            end

            it 'does not raise the exception to #set_locale' do
              SetMobileSdkUserLocale.new(agent, warden).set_locale
            end
          end
        end
      end
    end
  end
end
