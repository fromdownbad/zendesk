require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'AuthenticationEventPublisher' do
  describe_with_arturo_enabled :publish_user_authentication_events do
    describe "for an agent" do
      let(:user) { users(:minimum_agent) }

      before do
        Timecop.freeze
        message_id = SecureRandom.uuid
        ZendeskProtobufEncoder.any_instance.stubs(:message_id).returns(message_id)
      end

      it "publishes a login event" do
        expected_message = UserLoggedInEventProtobufEncoder.new(user)

        EscKafkaMessage.expects(:create!).with(
          value: expected_message.to_proto,
          account_id: user.account_id,
          topic: 'platform.standard.user_authentication_events',
          partition_key: user.id
        )

        Zendesk::Auth::AuthenticationEventPublisher.new(user: user).publish_login_event!
      end

      it "publishes a logout event" do
        expected_message = UserLoggedOutEventProtobufEncoder.new(user)

        EscKafkaMessage.expects(:create!).with(
          value: expected_message.to_proto,
          account_id: user.account_id,
          topic: 'platform.standard.user_authentication_events',
          partition_key: user.id
        )

        Zendesk::Auth::AuthenticationEventPublisher.new(user: user).publish_logout_event!
      end

      it "rescues StandardError" do
        expected_message = UserLoggedInEventProtobufEncoder.new(user)

        EscKafkaMessage.expects(:create!).with(
          value: expected_message.to_proto,
          account_id: user.account_id,
          topic: 'platform.standard.user_authentication_events',
          partition_key: user.id
        ).raises(StandardError)

        Zendesk::StatsD::Client.any_instance.expects(:increment).with('user_authentication_events.errors', tags: ["exception:StandardError"]).once

        Zendesk::Auth::AuthenticationEventPublisher.new(user: user).publish_login_event!
      end
    end

    describe "for an end user" do
      let(:user) { users(:minimum_end_user) }

      it "doesn't publish a login event" do
        EscKafkaMessage.expects(:create!).never
        Zendesk::Auth::AuthenticationEventPublisher.new(user: user).publish_login_event!
      end
    end

    describe "for the system user" do
      let(:systemuser) { users(:systemuser) }

      it "doesn't publish a login event" do
        EscKafkaMessage.expects(:create!).never
        Zendesk::Auth::AuthenticationEventPublisher.new(user: systemuser).publish_login_event!
      end
    end
  end

  describe_with_arturo_disabled :publish_user_authentication_events do
    describe "for an agent" do
      let(:user) { users(:minimum_agent) }

      it "doesn't publish a login event" do
        EscKafkaMessage.expects(:create!).never
        Zendesk::Auth::AuthenticationEventPublisher.new(user: user).publish_login_event!
      end
    end
  end
end
