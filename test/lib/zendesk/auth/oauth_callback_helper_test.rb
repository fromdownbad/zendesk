require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Auth::OauthCallbackHelper do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:zendesk_callback_url) { Zendesk::Auth::OauthCallbackHelper::OAUTH_CALLBACK_URL }
  let(:callback_path) { "some_oauth_callback_path" }
  let(:helper) do
    Zendesk::Auth::OauthCallbackHelper.new(
      subdomain: account.subdomain,
      oauth_callback_path: callback_path
    )
  end

  describe "#state" do
    it "returns the correct state" do
      assert_equal helper.state, "#{account.subdomain}:#{callback_path}"
    end
  end

  describe "#twitter_oauth_callback_url" do
    it "returns the correct callback url for twitter oauth" do
      assert_equal helper.twitter_oauth_callback_url, "#{zendesk_callback_url}?state=#{CGI.escape(helper.state)}"
    end
  end
end
