require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'AccountHome' do
  fixtures :all

  class AccountHomeTestController
    def self.helper_method(*); end
    include Zendesk::Auth::AccountHome
    attr_accessor :current_account, :current_user, :request, :current_brand, :current_url_provider

    def ssl_environment?;
      true;
    end

    def logged_in?
      current_user.present?
    end

    def main_app
      Rails.application.routes.url_helpers
    end
  end

  describe "#account_home" do
    let(:account) { accounts(:minimum) }

    before do
      @controller = AccountHomeTestController.new
      @controller.current_url_provider = account.route
      @controller.current_url_provider.account = account
      @controller.current_account = account
      @controller.current_user = users(:minimum_agent)
      @controller.current_brand = account.brands.first
      @controller.request = @request = ActionDispatch::Request.new(Rack::MockRequest.env_for('/'))
      refute account.certificates.active.any?
    end

    [ # help_center_state, web_portal_state, current_brand's help_center.state, has_public_forums, logged_in, is_mobile, expected_path
      [:disabled, :disabled, :archived, false, false, false, "minimum.zendesk-test.com/access"],
      [:disabled, :disabled, :archived, false, false, true, "minimum.zendesk-test.com/access"],
      [:disabled, :disabled, :archived, false, true, false, "https://minimum.zendesk-test.com/agent"],
      [:disabled, :disabled, :archived, false, true, true, "https://minimum.zendesk-test.com/agent"],
      [:disabled, :disabled, :archived, true, false, false, "minimum.zendesk-test.com/access"],
      [:disabled, :disabled, :archived, true, false, true, "minimum.zendesk-test.com/access"],
      [:disabled, :disabled, :archived, true, true, false, "https://minimum.zendesk-test.com/agent"],
      [:disabled, :disabled, :archived, true, true, true, "https://minimum.zendesk-test.com/agent"],

      ## Note that when a brand's hc is 'restricted', classic redirects to hc and hc is respnsible for redirecting to a login page.
      [:enabled, :disabled, :enabled, false, false, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :enabled, false, false, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :enabled, false, true, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :enabled, false, true, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :enabled, true, false, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :enabled, true, false, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :enabled, true, true, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :enabled, true, true, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :archived, false, false, false, "https://minimum.zendesk-test.com/access"],
      [:enabled, :disabled, :archived, false, false, true, "https://minimum.zendesk-test.com/access"],
      [:enabled, :disabled, :archived, false, true, false, "https://minimum.zendesk-test.com/agent"],
      [:enabled, :disabled, :archived, false, true, true, "https://minimum.zendesk-test.com/agent"],
      [:enabled, :disabled, :archived, true, false, false, "https://minimum.zendesk-test.com/access"],
      [:enabled, :disabled, :archived, true, false, true, "https://minimum.zendesk-test.com/access"],
      [:enabled, :disabled, :archived, true, true, false, "https://minimum.zendesk-test.com/agent"],
      [:enabled, :disabled, :archived, true, true, true, "https://minimum.zendesk-test.com/agent"],
      [:enabled, :disabled, :restricted, false, false, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :restricted, false, false, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :restricted, false, true, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :restricted, false, true, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :restricted, true, false, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :restricted, true, false, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :restricted, true, true, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :disabled, :restricted, true, true, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :enabled, false, false, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :enabled, false, false, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :enabled, false, true, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :enabled, false, true, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :enabled, true, false, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :enabled, true, false, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :enabled, true, true, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :enabled, true, true, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :archived, false, false, false, "https://minimum.zendesk-test.com/access"],
      [:enabled, :restricted, :archived, false, false, true, "https://minimum.zendesk-test.com/access"],
      [:enabled, :restricted, :archived, false, true, false, "https://minimum.zendesk-test.com/agent"],
      [:enabled, :restricted, :archived, false, true, true, "https://minimum.zendesk-test.com/agent"],
      [:enabled, :restricted, :archived, true, false, false, "https://minimum.zendesk-test.com/access"],
      [:enabled, :restricted, :archived, true, false, true, "https://minimum.zendesk-test.com/access"],
      [:enabled, :restricted, :archived, true, true, false, "https://minimum.zendesk-test.com/agent"],
      [:enabled, :restricted, :archived, true, true, true, "https://minimum.zendesk-test.com/agent"],
      [:enabled, :restricted, :restricted, false, false, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :restricted, false, false, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :restricted, false, true, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :restricted, false, true, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :restricted, true, false, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :restricted, true, false, true, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :restricted, true, true, false, "https://minimum.zendesk-test.com/hc"],
      [:enabled, :restricted, :restricted, true, true, true, "https://minimum.zendesk-test.com/hc"],
      [:restricted, :disabled, :archived, false, false, false, "minimum.zendesk-test.com/access"],
      [:restricted, :disabled, :archived, false, false, true, "minimum.zendesk-test.com/access"],
      [:restricted, :disabled, :archived, false, true, false, "https://minimum.zendesk-test.com/agent"],
      [:restricted, :disabled, :archived, false, true, true, "https://minimum.zendesk-test.com/agent"],
      [:restricted, :disabled, :archived, true, false, false, "minimum.zendesk-test.com/access"],
      [:restricted, :disabled, :archived, true, false, true, "minimum.zendesk-test.com/access"],
      [:restricted, :disabled, :archived, true, true, false, "https://minimum.zendesk-test.com/agent"],
      [:restricted, :disabled, :archived, true, true, true, "https://minimum.zendesk-test.com/agent"],
      [:restricted, :disabled, :restricted, false, false, false, "minimum.zendesk-test.com/access?theme=hc"],
      [:restricted, :disabled, :restricted, false, false, true, "minimum.zendesk-test.com/access?theme=hc"],
      [:restricted, :disabled, :restricted, false, true, false, "https://minimum.zendesk-test.com/agent"],
      [:restricted, :disabled, :restricted, false, true, true, "https://minimum.zendesk-test.com/agent"],
      [:restricted, :disabled, :restricted, true, false, false, "minimum.zendesk-test.com/access?theme=hc"],
      [:restricted, :disabled, :restricted, true, false, true, "minimum.zendesk-test.com/access?theme=hc"],
      [:restricted, :disabled, :restricted, true, true, false, "https://minimum.zendesk-test.com/agent"],
      [:restricted, :disabled, :restricted, true, true, true, "https://minimum.zendesk-test.com/agent"]
    ].each do |config|
      describe "with #{config.inspect}" do
        before do
          @controller.send(:current_account).settings.help_center_state = config[0]
          @controller.send(:current_account).settings.web_portal_state  = config[1]
          HelpCenter.
            stubs(:find_by_brand_id).
            with(@controller.current_brand.id).
            returns(HelpCenter.new(id: 42, state: config[2].to_s))
          @controller.send(:current_account).stubs(has_public_forums?: config[3])
          @controller.stubs(logged_in?: config[4])
          @request.stubs(is_mobile?: config[5])
          @request.format = config[5] ? "mobile_v2" : "html"
        end

        it "resolves to #{config[6]}" do
          if config[6].start_with?("https")
            @controller.send(:current_account).stubs(is_ssl_enabled?: false) # actually using secure_ssl_sessions overwrite
            assert_equal config[6], @controller.send(:account_home)
          else
            @controller.send(:current_account).expects(:is_ssl_enabled?).returns(false)
            assert_equal "https://#{config[6]}", @controller.send(:account_home)

            @controller.send(:current_account).expects(:is_ssl_enabled?).returns(true)
            assert_equal "https://#{config[6]}", @controller.send(:account_home)
          end
        end
      end
    end

    describe "as an agent" do
      before do
        @controller.current_user = account.owner
      end

      it "redirects to agent" do
        assert_equal "https://minimum.zendesk-test.com/agent", @controller.send(:account_home)
      end
    end

    describe "with active certificate and ssl" do
      let(:mapped) { "foo.com" }

      before do
        account.certificates.first.update_attribute(:state, "active")
        account.certificates.first.update_attribute(:valid_until, 10.days.from_now.to_date)

        assert account.certificates.active.any?
        account.stubs(is_ssl_enabled?: true)
        @controller.current_url_provider.host_mapping = mapped
        @controller.stubs(:logged_in?).returns(false)
      end

      it "is mapped" do
        assert_equal "https://#{mapped}/access", @controller.send(:account_home)
      end
    end

    describe "in a multi brand env with a branded url" do
      let(:brand) { FactoryBot.create(:brand) }

      before do
        account.stubs(:has_multibrand?).returns(true)
        @controller.current_url_provider = brand.route
      end

      it "redirects to the agent portal for an agent" do
        assert_equal account.url(mapped: false, ssl: true) + "/agent", @controller.send(:account_home)
      end
    end

    describe "when account's agent_route has no brand" do
      before do
        @controller.stubs(:current_brand).returns(nil)
        @controller.send(:current_account).settings.help_center_state = 'enabled'
        @controller.send(:current_account).settings.web_portal_state  = 'disabled'
      end

      describe "and user is an agent" do
        before do
          @controller.current_user = account.owner
        end

        it "redirects to /agent" do
          assert_equal "https://minimum.zendesk-test.com/agent", @controller.send(:account_home)
        end
      end

      describe "and a user is anonymous" do
        before do
          @controller.stubs(:logged_in?).returns(false)
        end

        it "redirects classic access page" do
          assert_equal "https://minimum.zendesk-test.com/access", @controller.send(:account_home)
        end
      end

      describe "and user is an end user" do
        before { @controller.current_user = users(:minimum_end_user) }
        it "redirects to help center" do
          assert_equal "https://minimum.zendesk-test.com/hc", @controller.send(:account_home)
        end
      end
    end

    describe 'non multiproduct account (SSO) with hc and sell enabled' do
      let(:current_account) { accounts(:minimum_jwt_saml) }

      let(:support_trial_product) { FactoryBot.build(:support_trial_product) }
      let(:sell_trial_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:sell_free_product, plan_settings: { billing_source: ''}).with_indifferent_access) }
      let(:brand) { FactoryBot.create(:brand) }

      def stub_account_service(response_body)
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
          to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      end

      def stub_staff_service_entitlements(response_body)
        stub_request(:get, "http://metropolis:8889/api/services/staff/#{@controller.current_user.id}/entitlements").
          to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      end

      before do
        @controller.current_user = users(:minimum_jwt_saml_admin)
        @controller.send(:current_account).settings.help_center_state = 'enabled'
        @controller.send(:current_account).settings.web_portal_state  = 'disabled'
        @controller.stubs(:current_brand).returns(brand)
        brand.stubs(:help_center_in_use?).returns(true)
      end

      describe "When Arturo agent_redirect_support is ON " do
        before do
          Arturo.enable_feature! :agent_redirect_support
        end

        it 'redirects to support for support + sell account' do
          response_body = { products: [sell_trial_product, support_trial_product] }.to_json
          stub_account_service(response_body)
          assert_equal 'https://minimum.zendesk-test.com/agent', @controller.send(:account_home)
        end
      end

      describe "When Arturo agent_redirect_support is OFF " do
        before do
          Arturo.disable_feature! :agent_redirect_support
        end

        it 'redirects to support for support + sell account' do
          response_body = { products: [sell_trial_product, support_trial_product] }.to_json
          stub_account_service(response_body)
          assert_equal 'https://minimum.zendesk-test.com/hc', @controller.send(:account_home)
        end
      end
    end

    describe 'with multiproduct account' do
      let(:account) { accounts(:multiproduct) }
      let(:support_trial_product) { FactoryBot.build(:support_trial_product) }
      let(:support_inactive_product) { FactoryBot.build(:support_inactive_product) }
      let(:acme_app_product) { FactoryBot.build(:acme_app_free_product) }
      let(:chat_trial_product) { FactoryBot.build(:chat_trial_product) }
      let(:sell_trial_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:sell_free_product, plan_settings: { billing_source: ''}).with_indifferent_access) }

      def stub_account_service(response_body)
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
          to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      end

      def stub_staff_service_entitlements(response_body)
        stub_request(:get, "http://metropolis:8888/api/services/staff/#{@controller.current_user.id}/entitlements").
          to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      end

      describe 'with hostmapping' do
        let(:mapped) { "foo.com" }
        let(:brand) { FactoryBot.create(:brand) }

        before do
          @controller.current_user = users(:multiproduct_support_agent)
          response_body = { products: [support_trial_product] }.to_json
          stub_account_service(response_body)

          account.stubs(host_mapping: true)
          account.update_attribute(:host_mapping, mapped)
          account.update_attribute(:is_ssl_enabled, true)
          c = account.certificates.create!
          c.update_attribute(:state, "active")
          c.update_attribute(:valid_until, 10.days.from_now.to_date)

          @controller.send(:current_account).settings.help_center_state = 'enabled'
          @controller.send(:current_account).settings.web_portal_state  = 'disabled'
          @controller.stubs(:current_brand).returns(brand)
          brand.stubs(:help_center_in_use?).returns(true)
        end

        it 'redirects agent to support' do
          assert_equal 'https://multiproduct.zendesk-test.com/agent', @controller.send(:account_home)
        end

        it 'redirects to support for support + chat account' do
          response_body = { products: [support_trial_product, chat_trial_product] }.to_json
          stub_account_service(response_body)
          assert_equal 'https://multiproduct.zendesk-test.com/agent', @controller.send(:account_home)
        end

        it 'redirects to support for support + sell account' do
          response_body = { products: [sell_trial_product, support_trial_product] }.to_json
          stub_account_service(response_body)
          assert_equal 'https://multiproduct.zendesk-test.com/agent', @controller.send(:account_home)
        end

        it 'does not redirect to support if it is not an active product - only chat ' do
          response_body = { products: [support_inactive_product, chat_trial_product] }.to_json
          stub_account_service(response_body)
          @controller.stubs(params: { after_verification: nil })
          assert_equal 'https://multiproduct.zendesk-test.com/chat', @controller.send(:account_home)
        end
      end

      it 'redirects to central admin if current_user is owner and has no entitlements' do
        Arturo.enable_feature! :owner_with_no_entitlements_redirect
        @controller.current_user = account.owner
        stub_staff_service_entitlements({entitlements: {'chat' => nil, 'support' => nil}}.to_json)
        assert_equal 'https://multiproduct.zendesk-test.com/admin', @controller.send(:account_home)
      end

      describe 'redirects to sell if current_user has only entitlements to sell and is a contributor' do
        it 'after_validation params nil and  sell_only_after_verification disabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          @controller.stubs(params: { after_verification: nil })
          Arturo.disable_feature! :sell_only_after_verification
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'support' => nil}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation=0 and  sell_only_after_verification disabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          @controller.stubs(params: { after_verification: '0' })
          Arturo.disable_feature! :sell_only_after_verification
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'support' => nil}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation params nil and  sell_only_after_verification enabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          @controller.stubs(params: { after_verification: nil })
          Arturo.enable_feature! :sell_only_after_verification
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'support' => nil}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation=0 and  sell_only_after_verification enabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          @controller.stubs(params: { after_verification: '0' })
          Arturo.enable_feature! :sell_only_after_verification
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'support' => nil}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation=1 with sell_only_after_verification disabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          @controller.stubs(params: { after_verification: '1' })
          Arturo.disable_feature! :sell_only_after_verification
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'support' => nil}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation=1 with sell_only_after_verification enabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          @controller.stubs(params: { after_verification: '1' })
          Arturo.enable_feature! :sell_only_after_verification
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'support' => nil}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell?after_verification=1', @controller.send(:account_home)
        end
      end

      describe "redirects to sell if current_user has entitlements to sell and to guide as a viewer" do
        it 'after_validation params nil and sell_only_after_verification disabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          Arturo.disable_feature! :sell_only_after_verification
          @controller.stubs(params: { after_verification: nil })
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'guide' => 'viewer'}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation=0 and sell_only_after_verification disabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          Arturo.disable_feature! :sell_only_after_verification
          @controller.stubs(params: { after_verification: '0' })
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'guide' => 'viewer'}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation params nil and sell_only_after_verification enabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          Arturo.enable_feature! :sell_only_after_verification
          @controller.stubs(params: { after_verification: nil })
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'guide' => 'viewer'}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation=0 with sell_only_after_verification disabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          Arturo.disable_feature! :sell_only_after_verification
          @controller.stubs(params: { after_verification: '0' })
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'guide' => 'viewer'}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation=0 with sell_only_after_verification enabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          Arturo.enable_feature! :sell_only_after_verification
          @controller.stubs(params: { after_verification: '0' })
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'guide' => 'viewer'}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation=1 with sell_only_after_verification disabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          Arturo.disable_feature! :sell_only_after_verification
          @controller.stubs(params: { after_verification: '1' })
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'guide' => 'viewer'}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell', @controller.send(:account_home)
        end

        it 'after_validation=1 with sell_only_after_verification enabled' do
          Arturo.enable_feature! :sell_only_user_redirect
          Arturo.enable_feature! :sell_only_after_verification
          @controller.stubs(params: { after_verification: '1' })
          @controller.current_user = users(:minimum_end_user)
          @controller.current_user.stubs(:is_contributor?).returns(true)
          stub_staff_service_entitlements({entitlements: {'sell' => 'agent', 'guide' => 'viewer'}}.to_json)
          assert_equal 'https://multiproduct.zendesk-test.com/sell?after_verification=1', @controller.send(:account_home)
        end
      end

      it 'skips over multiproduct redirection logic if support active' do
        response_body = { products: [support_trial_product] }.to_json
        stub_account_service(response_body)
        @controller.expects(:multiproduct_home).never
        @controller.send(:account_home)
      end

      it 'redirects to support if it is active' do
        response_body = { products: [support_trial_product] }.to_json
        stub_account_service(response_body)
        assert_equal 'https://multiproduct.zendesk-test.com/agent', @controller.send(:account_home)
      end

      it 'redirects to support if nothing is active' do
        response_body = { products: [] }.to_json
        stub_account_service(response_body)
        assert_equal 'https://multiproduct.zendesk-test.com/agent', @controller.send(:account_home)
      end

      it 'redirects to support if it is one of many active products' do
        response_body = { products: [support_trial_product, acme_app_product] }.to_json
        stub_account_service(response_body)
        assert_equal 'https://multiproduct.zendesk-test.com/agent', @controller.send(:account_home)
      end

      it 'redirects to support regardless of position in result body from account-service due to hierarchy' do
        response_body = { products: [acme_app_product, support_trial_product] }.to_json
        stub_account_service(response_body)
        assert_equal 'https://multiproduct.zendesk-test.com/agent', @controller.send(:account_home)
      end

      describe 'does not redirect to support if it is not active' do
        it 'after_validation nil And sell_only_after_verification disabled' do
          response_body = { products: [support_inactive_product, acme_app_product] }.to_json
          stub_account_service(response_body)
          @controller.stubs(params: { after_verification: nil })
          Arturo.disable_feature! :sell_only_after_verification
          assert_equal 'https://multiproduct.zendesk-test.com/acme_app', @controller.send(:account_home)
        end

        it 'after_validation=0 And sell_only_after_verification disabled' do
          response_body = { products: [support_inactive_product, acme_app_product] }.to_json
          stub_account_service(response_body)
          @controller.stubs(params: { after_verification: '0' })
          Arturo.disable_feature! :sell_only_after_verification
          assert_equal 'https://multiproduct.zendesk-test.com/acme_app', @controller.send(:account_home)
        end

        it 'after_validation nil And sell_only_after_verification enabled' do
          response_body = { products: [support_inactive_product, acme_app_product] }.to_json
          stub_account_service(response_body)
          @controller.stubs(params: { after_verification: nil })
          Arturo.enable_feature! :sell_only_after_verification
          assert_equal 'https://multiproduct.zendesk-test.com/acme_app', @controller.send(:account_home)
        end

        it 'after_validation=0 And sell_only_after_verification enabled' do
          response_body = { products: [support_inactive_product, acme_app_product] }.to_json
          stub_account_service(response_body)
          @controller.stubs(params: { after_verification: '0' })
          Arturo.enable_feature! :sell_only_after_verification
          assert_equal 'https://multiproduct.zendesk-test.com/acme_app', @controller.send(:account_home)
        end

        it 'after_validation=1 And sell_only_after_verification disabled' do
          response_body = { products: [support_inactive_product, acme_app_product] }.to_json
          stub_account_service(response_body)
          @controller.stubs(params: { after_verification: '1' })
          Arturo.disable_feature! :sell_only_after_verification
          assert_equal 'https://multiproduct.zendesk-test.com/acme_app', @controller.send(:account_home)
        end

        it 'after_validation=1 And sell_only_after_verification enabled' do
          response_body = { products: [support_inactive_product, acme_app_product] }.to_json
          stub_account_service(response_body)
          @controller.stubs(params: { after_verification: '1' })
          Arturo.enable_feature! :sell_only_after_verification
          assert_equal 'https://multiproduct.zendesk-test.com/acme_app?after_verification=1', @controller.send(:account_home)
        end
      end
    end
  end
end
