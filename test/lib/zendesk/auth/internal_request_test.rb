require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Auth::InternalRequest do
  class SomethingSpecial
    include Zendesk::Auth::InternalRequest

    def request
    end
  end

  subject { SomethingSpecial.new }

  describe "#internal_request?" do
    [true, false].each do |value|
      describe "when internal_client? returns #{value}" do
        before { subject.stubs(:request).returns(stub(internal_client?: value)) }
        it "mimics that value" do
          assert_equal value, subject.send(:internal_request?)
        end
      end
    end
  end
end
