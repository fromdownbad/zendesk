require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 5

describe 'AuthenticatedSessionMixin' do
  class FakeController
    include Zendesk::Auth::AuthenticatedSessionMixin

    attr_accessor :shared_session, :current_account, :current_user

    # Simplify testing
    public :auth_via, :not_auth_via?, :auth_via?
  end

  describe Zendesk::Auth::AuthenticatedSessionMixin do
    before do
      @controller = FakeController.new
      @controller.shared_session = { auth_via: 'password' }.with_indifferent_access
    end

    it "allows querying the authentication mechanism" do
      assert_equal 'password', @controller.auth_via
      assert @controller.not_auth_via?(:basic)
      assert @controller.auth_via?(:password)
    end
  end
end
