require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::SatisfactionRatings do
  fixtures :accounts, :users, :groups, :tickets, :events

  let(:account) { accounts(:minimum) }
  let(:ratings_counter) { Zendesk::RecordCounter::SatisfactionRatings.new(account: account) }

  before do
    @agent   = users(:minimum_agent)
    @user    = users(:minimum_end_user)
    @group   = groups(:minimum_group)

    @rating1 = account.satisfaction_ratings.create!(
      agent: @agent,
      group: @group,
      ticket: tickets(:minimum_2),
      enduser: @user,
      event: events(:create_comment_for_minimum_ticket_2),
      score: SatisfactionType.OFFERED,
      reason_code: Satisfaction::Reason::NONE,
      status_id: StatusType.CLOSED
    ) do |r|
      r.created_at = 1.day.ago
      r.ticket.update_column(:updated_at, r.created_at)
      r.ticket.update_column(:satisfaction_score, r.score)
    end

    @rating2 = account.satisfaction_ratings.create!(
      agent: @agent,
      group: @group,
      ticket: tickets(:minimum_1),
      enduser: @user,
      event: events(:create_comment_for_minimum_ticket_1),
      score: SatisfactionType.BAD,
      reason_code: Satisfaction::Reason::OTHER,
      status_id: StatusType.CLOSED
    ) do |r|
      r.created_at = 1.hour.ago
      r.ticket.update_column(:updated_at, r.created_at)
      r.ticket.update_column(:satisfaction_score, r.score)
    end
  end

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal ratings_counter.scope.count, 2
      assert_equal account.satisfaction_ratings, ratings_counter.scope
    end
  end
end
