require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::DeletedUsers do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:deleted_users) { Zendesk::RecordCounter::DeletedUsers.new(account: account) }
  let(:include_redacted) { Zendesk::RecordCounter::DeletedUsers.new(account: account, options: { include_redacted: true }) }

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal account.users.inactive_and_not_redacted, deleted_users.scope
    end

    describe "with include_redacted" do
      it "returns the correct scope" do
        assert_equal account.users.inactive, include_redacted.scope
      end
    end
  end
end
