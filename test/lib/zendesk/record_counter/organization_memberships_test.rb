require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::OrganizationMemberships do
  fixtures :accounts, :organization_memberships, :organizations

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:organization) { organizations(:minimum_organization1) }
  let(:organization_memberships_user) { Zendesk::RecordCounter::OrganizationMemberships.new(account: account, options: { user_id: user.id }) }
  let(:organization_memberships_organization) { Zendesk::RecordCounter::OrganizationMemberships.new(account: account, options: { organization_id: organization.id }) }

  describe "#scope" do
    it "returns the correct scope for users" do
      assert_equal account.users.find(user.id).organization_memberships.active, organization_memberships_user.scope
    end

    it "returns the correct scope for organizations" do
      assert_equal account.organizations.find(organization.id).organization_memberships.active, organization_memberships_organization.scope
    end
  end
end
