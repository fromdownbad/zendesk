require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::Organizations do
  fixtures :accounts, :organizations, :users

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:organizations) { Zendesk::RecordCounter::Organizations.new(account: account) }
  let(:with_user) { Zendesk::RecordCounter::Organizations.new(account: account, options: { user_id: user.id }) }

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal account.organizations, organizations.scope
    end

    describe "with user" do
      it "returns the correct scope" do
        assert_equal account.organizations.joins(:organization_memberships).where(organization_memberships: {user_id: user.id}), with_user.scope
      end
    end
  end
end
