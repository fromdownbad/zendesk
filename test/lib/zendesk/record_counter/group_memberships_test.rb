require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::GroupMemberships do
  fixtures :accounts, :memberships

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:group) { groups(:minimum_group) }
  let(:group_memberships_user) { Zendesk::RecordCounter::GroupMemberships.new(account: account, options: { user_id: user.id }) }
  let(:group_memberships_group) { Zendesk::RecordCounter::GroupMemberships.new(account: account, options: { group_id: group.id }) }

  describe "#scope" do
    it "returns the correct scope for users" do
      assert_equal account.users.find(user.id).memberships.active(account.id), group_memberships_user.scope
    end

    it "returns the correct scope for groups" do
      assert_equal account.groups.find(group.id).memberships.active(account.id), group_memberships_group.scope
    end
  end
end
