require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::Tickets do
  fixtures :accounts, :users, :organizations

  let(:account) { accounts(:minimum) }
  let(:external_id) { 123 }
  let(:organization) { organizations(:minimum_organization1) }
  let(:admin) { users(:minimum_admin) }
  let(:agent) { users(:minimum_agent) }
  let(:current_user) { admin }
  let(:tickets) do
    Zendesk::RecordCounter::Tickets.new(account: account, options: {
      current_user_id: current_user.id
    })
  end
  let(:with_organization_id) do
    Zendesk::RecordCounter::Tickets.new(account: account, options: {
      organization_id: organization.id,
      current_user_id: current_user.id
    })
  end
  let(:with_external_id) do
    Zendesk::RecordCounter::Tickets.new(account: account, options: {
      external_id: external_id,
      current_user_id: current_user.id
    })
  end

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal account.tickets, tickets.scope
    end

    describe "with organization_id" do
      it "returns the correct scope" do
        scope = organization.tickets

        assert_equal scope, with_organization_id.scope
      end
    end

    describe "with external_id" do
      it "returns the correct scope" do
        scope = account.tickets.where(external_id: external_id)

        assert_equal scope, with_external_id.scope
      end
    end

    describe "for agent" do
      let(:current_user) { agent }

      it "returns the correct scope" do
        scope = account.tickets.for_user(agent)

        assert_equal scope, tickets.scope
      end
    end

    describe "for system user" do
      let(:current_user) { users(:systemuser) }

      it "returns the correct scope" do
        scope = account.tickets

        assert_equal scope, tickets.scope
      end
    end
  end
end
