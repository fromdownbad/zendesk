require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::Base do
  class BaseImplementation < Zendesk::RecordCounter::Base
    def scope
      account.tickets
    end
  end

  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:options) { { option_a: 1 } }
  let(:base) { BaseImplementation.new(account: account, options: options) }
  let(:scope_sha) { Digest::SHA1.hexdigest(base.scope.to_sql) }
  let(:presentation) { base.present }
  let(:count) { presentation[:value] }
  let(:refreshed_at) { presentation[:refreshed_at] }

  describe "#present" do
    describe "when count is not cached" do
      it "presents the correct count" do
        assert_equal account.tickets.count, count
      end

      describe "and limited_count hits the maximum" do
        let(:limited_count) { MAX_COUNT_BEFORE_ASYNC }

        before do
          base.stubs(:limited_count).returns(limited_count)
          RecordCounterJob.expects(:enqueue).with(
            record_counter_class: 'BaseImplementation',
            account_id: account.id,
            options: options
          )
        end

        it "presents the correct values" do
          assert_equal limited_count, count
          assert_nil refreshed_at
        end
      end

      describe "and limited_count is smaller than maximum" do
        let(:limited_count) { MAX_COUNT_BEFORE_ASYNC - 1 }

        before do
          base.stubs(:limited_count).returns(limited_count)
          RecordCounterJob.expects(:enqueue).never
        end

        it "presents the correct values" do
          assert_equal limited_count, count
          assert refreshed_at.present?
        end
      end
    end

    describe "when count is cached" do
      let(:unlimited_count) { 10 }
      let(:iso_date) { '2020-07-23T03:21:55+00:00' }

      before do
        Rails.cache.write(
          base.cache_key,
          "#{unlimited_count}/#{iso_date}"
        )
      end

      it "presents the correct values" do
        RecordCounterJob.expects(:enqueue).never

        assert_equal unlimited_count, count
        assert_equal iso_date, refreshed_at
      end
    end
  end

  describe "#cache_key" do
    it "returns the correct string" do
      assert_equal "record_counter/base_implementation/#{account.id}/#{scope_sha}", base.cache_key
    end
  end

  describe "#cache_value" do
    let(:iso_date) { '2020-07-23T03:21:55+00:00' }

    before do
      base.stubs(:iso_date).returns(iso_date)
    end

    it "returns the correct string" do
      assert_equal "#{account.tickets.count}/#{iso_date}", base.cache_value
    end
  end

  describe "an empty subclass" do
    class EmptyBase < Zendesk::RecordCounter::Base
    end

    let(:base) { EmptyBase.new(account: account) }

    it "raises an exception during presentation because scope is not implemented" do
      err = assert_raises RuntimeError do
        base.present
      end
      assert_equal "implement me", err.message
    end
  end
end
