require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::Incidents do
  fixtures :accounts, :tickets, :users

  let(:account) { accounts(:minimum) }
  let(:problem) { tickets(:minimum_1) }
  let(:agent) { users(:minimum_agent) }
  let(:incidents_counter) { Zendesk::RecordCounter::Incidents.new(account: account, options: { current_user_id: agent.id, problem_nice_id: problem.nice_id }) }

  describe "#incidents" do
    before do
      problem.update_column(:ticket_type_id, TicketType.PROBLEM)
      @incident_1 = add_incident(problem, tickets(:minimum_2))
      @incident_2 = add_incident(problem, tickets(:minimum_3))
      problem.reload
    end

    it 'returns the correct scope' do
      assert_equal incidents_counter.scope.count, 2
      assert_equal problem.incidents, incidents_counter.scope
    end
  end

  def add_incident(problem, incident)
    incident.problem = problem
    incident.will_be_saved_by(accounts(:minimum).owner)
    incident.save
    incident
  end
end
