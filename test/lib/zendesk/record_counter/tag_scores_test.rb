require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::TagScores do
  fixtures :accounts, :tag_scores

  let(:account) { accounts(:minimum) }
  let(:tag_scores) { Zendesk::RecordCounter::TagScores.new(account: account) }

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal account.tag_scores, tag_scores.scope
    end
  end
end
