require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::UserFields do
  fixtures :accounts, :cf_fields

  let(:account) { accounts(:minimum) }
  let(:owner) { 'user' }
  let(:user_fields) { Zendesk::RecordCounter::UserFields.new(account: account, options: { owner: owner }) }

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal account.custom_fields.for_owner(owner), user_fields.scope
    end
  end
end
