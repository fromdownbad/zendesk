require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::OrganizationFields do
  fixtures :accounts, :cf_fields

  let(:account) { accounts(:minimum) }
  let(:owner) { 'organization' }
  let(:organization_fields) { Zendesk::RecordCounter::OrganizationFields.new(account: account, options: { owner: owner }) }

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal account.custom_fields.for_owner(owner), organization_fields.scope
    end
  end
end
