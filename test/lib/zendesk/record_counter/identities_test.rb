require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::Identities do
  fixtures :accounts, :user_identities

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:identities) { Zendesk::RecordCounter::Identities.new(account: account, options: { user_id: user.id }) }

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal account.user_identities.native.where(user_id: user.id), identities.scope
    end
  end
end
