require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::Groups do
  fixtures :accounts, :groups, :users

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:groups) { Zendesk::RecordCounter::Groups.new(account: account) }
  let(:with_user) { Zendesk::RecordCounter::Groups.new(account: account, options: { user_id: user.id }) }

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal account.groups, groups.scope
    end

    describe "with user" do
      it "returns the correct scope" do
        assert_equal account.groups.joins(:memberships).where(memberships: {user_id: user.id}), with_user.scope
      end
    end
  end
end
