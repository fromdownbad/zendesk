require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::OrganizationSubscriptions do
  fixtures :accounts, :organizations

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:end_user) { users(:minimum_end_user) }
  let(:organization) { organizations(:minimum_organization1) }
  let(:source_type) { Organization.name }

  let(:organization_subscriptions_for_end_user) do
    Zendesk::RecordCounter::OrganizationSubscriptions.new(account: account, options: { source_type: source_type, current_user_id: end_user.id })
  end

  let(:organization_subscriptions_for_agent) do
    Zendesk::RecordCounter::OrganizationSubscriptions.new(account: account, options: { source_type: source_type, current_user_id: agent.id })
  end

  let(:organization_subscriptions_for_agent_with_user_id) do
    Zendesk::RecordCounter::OrganizationSubscriptions.new(account: account, options: { source_type: source_type, current_user_id: agent.id, user_id: end_user.id })
  end

  let(:organization_subscriptions_for_agent_with_organization_id) do
    Zendesk::RecordCounter::OrganizationSubscriptions.new(account: account, options: { source_type: source_type, current_user_id: agent.id, organization_id: organization.id })
  end

  describe "#scope" do
    it "returns the correct scope for end user" do
      assert_equal end_user.watchings.where(source_type: source_type), organization_subscriptions_for_end_user.scope
    end

    it "returns the correct scope for agent" do
      assert_equal account.watchings.where(source_type: source_type), organization_subscriptions_for_agent.scope
    end

    it "returns the correct scope for agent with user_id param" do
      assert_equal account.users.find(end_user.id).watchings.where(source_type: source_type), organization_subscriptions_for_agent_with_user_id.scope
    end

    it "returns the correct scope for agent with organization_id param" do
      assert_equal account.organizations.find(organization.id).watchings.where(source_type: source_type), organization_subscriptions_for_agent_with_organization_id.scope
    end
  end
end
