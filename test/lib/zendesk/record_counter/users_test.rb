require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::RecordCounter::Users do
  fixtures :accounts, :groups, :organizations

  let(:account) { accounts(:minimum) }
  let(:group) { groups(:minimum_group) }
  let(:organization) { organizations(:minimum_organization1) }
  let(:users) { Zendesk::RecordCounter::Users.new(account: account) }
  let(:options) do
    {
      group_id: group.id,
      organization_id: organization.id,
      role: ['agent', 'admin', 'end-user']
    }
  end
  let(:with_params) { Zendesk::RecordCounter::Users.new(account: account, options: options) }

  describe "#scope" do
    it "returns the correct scope" do
      assert_equal account.users, users.scope
    end

    describe "with params" do
      it "returns the correct scope" do
        scope = account.users.
          joins(:memberships).where(memberships: {group_id: group.id}).
          joins(:organization_memberships).where(organization_memberships: {organization_id: organization.id}).
          where(roles: [Role::ADMIN.id, Role::AGENT.id, Role::END_USER.id])

        assert_equal scope, with_params.scope
      end
    end

    describe "with specific role" do
      let(:options) { { role: 'agent' } }

      it "returns the correct scope" do
        scope = account.users.where(roles: Role::AGENT.id)

        assert_equal scope, with_params.scope
      end
    end

    describe "with invalid roles" do
      let(:options) { { role: 'invalid' } }

      it "ignores them" do
        assert_equal account.users, with_params.scope
      end
    end
  end
end
