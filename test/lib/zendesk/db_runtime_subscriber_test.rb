require_relative '../../support/test_helper'

SingleCov.covered! file: 'lib/zendesk/db_runtime_subscriber.rb'

describe ApplicationController do
  class RuntimeSubscriberTestController < ApplicationController
    def index
      head 200
    end
  end

  tests RuntimeSubscriberTestController
  use_test_routes

  let(:datadog_rack_request_span) { Object.new }

  before do
    Datadog.stubs(tracer: stub(active_span: datadog_rack_request_span))
  end

  it 'sets a tag for db_runtime' do
    datadog_rack_request_span.expects(:set_tag).with('zendesk.db.sql_time', instance_of(Float))
    get :index
  end
end
