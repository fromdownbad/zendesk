require_relative "../../../support/test_helper"
require 'zendesk/salesforce/app_format_configuration'
require 'zendesk/salesforce/configuration'

SingleCov.covered!

describe Zendesk::Salesforce::AppFormatConfiguration do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    sample_config = '{"Contact":{"fields":["Name","Email","Account::Name"],"labels":{"Name":"Full Name","Email":"Email","Account::Name":"Account Name"},"relationships":{"Account":{"object":"Account","type":"child"}},"mapping":["Email","ticket.requester.email"],"sf_field_type":"email","object_label":"The Contact"}}'

    @app_format_configuration = Object.new
    @app_format_configuration.extend(Zendesk::Salesforce::AppFormatConfiguration)

    @configuration = Salesforce::Configuration.new(account: @account)
    @configuration.stubs(:configuration).returns(JSON.parse(sample_config))
    @app_format_configuration.stubs(:configuration).returns(@configuration)

    @app_format = {
      objects: [
        {
          api_name: "Contact",
          label: "The Contact",
          mapping: {
            zd: {
              key: "ticket.requester.email",
              label: nil
            },
            sf: {
              api_name: "Email",
              label: "Email"
            }
          },
          fields: [
            {
              api_name: "Name",
              label: "Full Name"
            },
            {
              api_name: "Email",
              label: "Email"
            }
          ],
          related_objects: [
            label: "Account",
            relationship: "Account",
            object: "Account",
            type: "child",
            limit: 5,
            filter_fields: [],
            fields: [
              {
                api_name: "Name",
                label: "Account Name"
              }
            ]
          ]
        }
      ]
    }
  end

  describe "#app_format" do
    it "returns objects in app format" do
      @app_format_configuration.send(:app_format).must_equal @app_format
    end
  end

  describe "#save_app_format_object" do
    before do
      @app_format_object = @app_format[:objects][0]

      @db_format = [
        ["Name", "Email", "Account::Name"],
        {
          "Name" => "Full Name",
          "Email" => "Email",
          "Account::Name" => "Account Name"
        },
        {
          "Account" => {
            "object" => "Account",
            "type" => "child"
          }
        }
      ]
      @configuration.stubs(:add).returns({})
    end

    it "converts app_format to db_format before saving" do
      @app_format_configuration.send(:save_app_format_object, @app_format_object).wont_be_nil
      @app_format_configuration.send(:db_format, @app_format_object).must_equal @db_format
    end
  end

  describe "#save_app_format_object_filter" do
    before do
      @app_format_filter = {
        parent: "Contact",
        relationship: "Account",
        limit: 3,
        filter_fields: [
          {
            api_name: "Name",
            data_type: "string",
            operator: "=",
            value: "Random Name"
          }
        ]
      }

      @db_format = [
        {
          field: "Name",
          type: "string",
          operator: "=",
          value: "Random Name"
        }.stringify_keys
      ]

      @configuration.stubs(:add_filters).returns({})
    end

    it "converts app format filter fields to db format filter fields" do
      @app_format_configuration.send(:save_app_format_object_filter, @app_format_filter).wont_be_nil
      @app_format_configuration.send(:db_format_filter_fields, @app_format_filter[:filter_fields]).must_equal @db_format
    end
  end
end
