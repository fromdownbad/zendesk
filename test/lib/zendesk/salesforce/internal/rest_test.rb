require_relative "../../../../support/test_helper"
require 'zendesk/salesforce/internal/tools'

SingleCov.covered! uncovered: 6

describe Salesforce::Tools::REST do
  before do
    @credentials = { }
  end

  describe '#query' do
    before do
      @token = stub(:[] => 'http://example.com', :token => 'access_token')
      Salesforce::Tools::REST.any_instance.stubs(:token).returns(@token)
      @rest = Salesforce::Tools::REST.new(@credentials)
    end

    it 'yields the records fetched and totalsize to the block' do
      params   = ["http://example.com/services/data/v20.0/query", { params: { q: "SOQL STATEMENT" }, headers: { "Authorization" => "Bearer access_token" } }]
      response = stub(parsed: JSON.parse('{ "done": true, "totalSize": 2, "records": [ { "attributes": { "type": "Account", "url": "/services/data/v20.0/sobjects/Account/001D000000IRFmaIAH" }, "Name": "Gabi" }, { "attributes": { "type": "Account", "url": "/services/data/v20.0/sobjects/Account/001D000000IomazIAB" }, "Name": "Reka" } ] }'))
      @token.expects(:get).once.with(*params).returns(response)
      @rest.query('SOQL STATEMENT') do |records, totalsize|
        assert_equal 2, records.count
        assert_equal 2, totalsize
      end
    end

    it 'is able to handle multi-page responses' do
      responses = [
        stub(parsed: JSON.parse('{ "done": false, "totalSize": 2, "records": [ { "attributes": { "type": "Account", "url": "/services/data/v20.0/sobjects/Account/001D000000IRFmaIAH" }, "Name": "Gabi" }, { "attributes": { "type": "Account", "url": "/services/data/v20.0/sobjects/Account/001D000000IomazIAB" }, "Name": "Reka" } ], "nextRecordsUrl": "/services/data/v20.0/query/01gD0000002HU6KIAW-2000" }')),
        stub(parsed: JSON.parse('{ "done": true,  "totalSize": 1, "records": [ { "attributes": { "type": "Account", "url": "/services/data/v20.0/sobjects/Account/001D000000IRFmaIAH" }, "Name": "Ellen" } ] }'))
      ]
      @token.expects(:get).twice.returns(*responses)

      data = []
      @rest.query('SOQL STATEMENT') { |records, _totalsize| data += records.collect { |entry| entry["Name"] } }
      assert_equal 3, data.count
      assert_equal %w[Gabi Reka Ellen], data
    end

    it 'is able to abort multi-page responses' do
      response = stub(parsed: JSON.parse('{ "done": false, "totalSize": 2, "records": [ { "attributes": { "type": "Account", "url": "/services/data/v20.0/sobjects/Account/001D000000IRFmaIAH" }, "Name": "Test 1" }, { "attributes": { "type": "Account", "url": "/services/data/v20.0/sobjects/Account/001D000000IomazIAB" }, "Name": "Test 2" } ], "nextRecordsUrl": "/services/data/v20.0/query/01gD0000002HU6KIAW-2000" }'))
      @token.expects(:get).once.returns(response)
      @rest.query('SOQL STATEMENT') { break }
    end
  end
end
