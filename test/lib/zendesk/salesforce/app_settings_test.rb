require_relative "../../../support/test_helper"
require 'zendesk/salesforce/app_settings'

SingleCov.covered!

describe Salesforce::AppSettings do
  fixtures :external_ticket_datas

  describe "#delay" do
    let(:configured_delay) { 123 }
    before do
      @salesforce_ticket_data = external_ticket_datas(:external_ticket_data_1)
    end

    it "uses the default development delay" do
      @salesforce_ticket_data.send(:delay).must_equal 20.seconds
    end

    it "uses default production delay" do
      Rails.env.expects(:production?).returns(true)
      @salesforce_ticket_data.send(:delay).must_equal 60.minutes
    end

    describe_with_arturo_enabled :configurable_salesforce_app_latency do
      it "uses default delay when app latency is nil" do
        salesforce_integration = SalesforceIntegration.new(app_latency: nil)
        Account.any_instance.stubs(:salesforce_integration).returns(salesforce_integration)

        @salesforce_ticket_data.send(:delay).must_equal 20.seconds
      end

      it "uses configured delay when app latency has value" do
        salesforce_integration = SalesforceIntegration.new(app_latency: configured_delay)
        Account.any_instance.stubs(:salesforce_integration).returns(salesforce_integration)

        @salesforce_ticket_data.send(:delay).must_equal configured_delay.minutes
      end
    end

    describe_with_arturo_enabled :has_central_admin_salesforce_integration do
      before do
        Account.any_instance.stubs(:has_central_admin_salesforce_integration?).returns(true)
        Salesforce::NextGeneration::Integration.any_instance.stubs(:configured_and_enabled?).returns(true)
        Salesforce::NextGeneration::Integration.any_instance.stubs(:app_latency).returns(15)
      end

      describe('when next_generation_salesforce data cache is disabled') do
        before do
          Salesforce::NextGeneration::Integration.any_instance.stubs(:data_cache_enabled?).returns(false)
        end

        it "uses default delay" do
          @salesforce_ticket_data.send(:delay).must_equal 20.seconds
        end
      end

      describe('when next_generation_salesforce data cache is enabled') do
        before do
          Salesforce::NextGeneration::Integration.any_instance.stubs(:data_cache_enabled?).returns(true)
        end

        it "uses the next_generation_salesforce latency app" do
          @salesforce_ticket_data.send(:delay).must_equal 15.minutes
        end
      end

      describe('when next_generation_salesforce is disabled but the data cache is enabled') do
        before do
          Salesforce::NextGeneration::Integration.any_instance.stubs(:configured_and_enabled?).returns(false)
          Salesforce::NextGeneration::Integration.any_instance.stubs(:data_cache_enabled?).returns(true)
        end

        it "uses default delay" do
          @salesforce_ticket_data.send(:delay).must_equal 20.seconds
        end
      end
    end
  end
end
