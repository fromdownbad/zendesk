require_relative "../../../support/test_helper"
require_dependency 'zendesk/salesforce/app_format_fields'
require_dependency 'zendesk/salesforce/controller_support'

SingleCov.covered!

describe Zendesk::Salesforce::AppFormatFields do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:field) do
    { key: "Name", title: "Name", type: "string", picklistValues: [], length: 255, updateable: true, scale: 0 }
  end
  let(:related_object) do
    { title: "Last Modified By", type: "child", object: "User", relationship: "LastModifiedBy", fields: [], length: 18, updateable: true, scale: 0 }
  end
  let(:related_object_field) do
    { key: "User::Name", title: "Name", type: "string", picklistValues: [], length: 255, updateable: true, scale: 0 }
  end
  let(:primary) { [field] }
  let(:related) { [related_object] }

  before do
    @app_format_fields = Object.new
    @app_format_fields.extend(Zendesk::Salesforce::AppFormatFields)

    @salesforce_support = Zendesk::Salesforce::ControllerSupport.new(account: account, params: {})
    @app_format_fields.stubs(:salesforce_support).returns(@salesforce_support)
  end

  describe "#app_format_fields" do
    before do
      @response = [{
       api_name: "Name",
       label: "Name",
       data_type: "string",
       picklist_values: [],
       length: 255,
       updateable: true,
       scale: 0
     }]

      @salesforce_support.stubs(:fields).returns(primary + related)
    end

    it "returns app format for all fields" do
      @app_format_fields.send(:app_format_fields).wont_be_empty
      @app_format_fields.send(:app_format_fields)[:fields].must_equal @response
    end
  end

  describe "#app_format_related_fields" do
    before do
      @salesforce_support.stubs(:relationship_fields).returns(primary)
    end

    it "is not empty" do
      @app_format_fields.send(:app_format_related_fields).wont_be_empty
    end
  end

  describe "#app_format_related" do
    before do
      @response = {
        object: "User",
        label: "Last Modified By",
        type: "child",
        relationship: "LastModifiedBy"
      }
    end
    it 'retuns app format of related object' do
      @app_format_fields.send(:app_format_related, related_object).must_equal @response
    end
  end

  describe "#app_format_field" do
    before do
      @response = {
        api_name: "Name",
        label: "Name",
        data_type: "string",
        picklist_values: [],
        length: 255,
        updateable: true,
        scale: 0
      }
    end

    it "retuns app format for primary fields" do
      @app_format_fields.send(:app_format_field, field).must_equal @response
    end

    it "retuns app format for related fields" do
      @app_format_fields.send(:app_format_field, related_object_field).must_equal @response
    end
  end
end
