require_relative '../../../../support/test_helper'
require 'zendesk/salesforce/next_generation/salesforce_connection'

SingleCov.covered!

describe Salesforce::NextGeneration::SalesforceConnection do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:zis_client) { Salesforce::NextGeneration::ZisClient.new(account) }
  let(:salesforce_connection) { Salesforce::NextGeneration::SalesforceConnection.new(zis_client) }

  before do
    zis_client.stubs(:access_token).returns('123456789')
    zis_client.stubs(:refresh_access_token).returns('987654321')
    zis_client.stubs(:salesforce_organization_uuid).returns('abcdefghi')
    zis_client.stubs(:salesforce_organization_url).returns('https://abc132.salesforce.com')
    zis_client.stubs(:is_sandbox?).returns(false)
  end

  describe "#get" do
    describe "when the request works as expected" do
      before do
        stub_request(:get, 'https://abc132.salesforce.com/some/path?q=SELECT%20*%20FROM%20Contacts').
          to_return(status: 200, body: 'response', headers: {})
      end

      it "should return the data" do
        response = salesforce_connection.get('/some/path?q=SELECT%20*%20FROM%20Contacts')
        assert_equal 200, response.status
        assert_equal 'response', response.body
      end
    end

    describe "when the token is invalid" do
      before do
        stub_request(:get, 'https://abc132.salesforce.com/some/path?q=SELECT%20*%20FROM%20Contacts').
          with(headers: {'Authorization' => 'Bearer 123456789'}).
          to_return(status: 401, body: 'error', headers: {})
        stub_request(:get, 'https://abc132.salesforce.com/some/path?q=SELECT%20*%20FROM%20Contacts').
          with(headers: {'Authorization' => 'Bearer 987654321'}).
          to_return(status: 200, body: 'response', headers: {})
      end

      it "should refresh the token" do
        response = salesforce_connection.get('/some/path?q=SELECT%20*%20FROM%20Contacts')
        assert_equal 200, response.status
        assert_equal 'response', response.body
      end
    end

    describe "when there is a error status code" do
      before do
        stub_request(:get, 'https://abc132.salesforce.com/some/path?q=SELECT%20*%20FROM%20Contacts').
          to_return(status: 403, body: 'error', headers: {})
      end

      it "should raise a Faraday::ClientError exception" do
        err = -> { salesforce_connection.get('/some/path?q=SELECT%20*%20FROM%20Contacts') }.must_raise Faraday::ClientError
        err.message.must_match 'the server responded with status 403'
      end
    end
  end

  describe "#post" do
    describe "when the request works as expected" do
      before do
        stub_request(:post, 'https://abc132.salesforce.com/some/path').
          with(headers: {'Authorization' => 'Bearer 123456789'}).
          to_return(status: 200, body: 'OK', headers: {})
      end

      it "should return OK" do
        parameters = {
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
          body: 'Body'
        }
        response = salesforce_connection.post('/some/path', parameters)
        assert_equal 200, response.status
        assert_equal 'OK', response.body
      end
    end

    describe "when the token is invalid" do
      before do
        stub_request(:post, 'https://abc132.salesforce.com/some/path').
          with(headers: {'Authorization' => 'Bearer 123456789'}).
          to_return(status: 401, body: 'Error', headers: {})
        stub_request(:post, 'https://abc132.salesforce.com/some/path').
          with(headers: {'Authorization' => 'Bearer 987654321'}).
          to_return(status: 200, body: 'OK', headers: {})
      end

      it "should refresh the token" do
        parameters = {
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
          body: 'Body'
        }
        response = salesforce_connection.post('/some/path', parameters)
        assert_equal 200, response.status
        assert_equal 'OK', response.body
      end
    end

    describe "when there is a error status code" do
      before do
        stub_request(:post, 'https://abc132.salesforce.com/some/path').
          to_return(status: 403, body: 'error', headers: {})
      end

      it "should raise a Faraday::ClientError exception" do
        parameters = {
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
          body: 'Body'
        }
        err = -> { salesforce_connection.post('/some/path', parameters) }.must_raise Faraday::ClientError
        err.message.must_match 'the server responded with status 403'
      end
    end
  end
end
