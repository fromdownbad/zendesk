require_relative '../../../../support/test_helper'
require 'zendesk/salesforce/next_generation/integration'

SingleCov.covered!

describe Salesforce::NextGeneration::Integration do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:integration) { Salesforce::NextGeneration::Integration.new(account) }

  describe "#token" do
    before do
      integration.stubs(:access_token).returns('123456789')
    end

    it "should return the access token" do
      assert_equal integration.token, '123456789'
    end
  end

  describe "#access_token" do
    before do
      Salesforce::NextGeneration::ZisClient.any_instance.stubs(:access_token).returns('123456789')
    end

    it "should return ZisClient.access_token value" do
      assert_equal integration.access_token, '123456789'
    end
  end

  describe "#instance_url" do
    before do
      Salesforce::NextGeneration::ZisClient.any_instance.stubs(:salesforce_organization_url).returns('https://abc132.salesforce.com')
    end

    it "should return ZisClient.url value" do
      assert_equal integration.instance_url, 'https://abc132.salesforce.com'
    end
  end

  describe "#app_latency" do
    before do
      Salesforce::NextGeneration::ZisClient.any_instance.stubs(:latency).returns(60)
    end

    it "should return ZisClient.latency value" do
      assert_equal integration.app_latency, 60
    end
  end

  it "#show_parent" do
    assert_equal integration.show_parent, 'none'
  end

  describe "#get_salesforce_session" do
    before do
      Salesforce::NextGeneration::ZisClient.any_instance.stubs(:access_token).returns('123456789')
      Salesforce::NextGeneration::ZisClient.any_instance.stubs(:salesforce_organization_url).returns('https://abc132.salesforce.com')
    end

    it "should return the Salesforce session from ZisClient" do
      result = { server_url: 'https://abc132.salesforce.com', session_id: '123456789' }
      assert_equal integration.get_salesforce_session, result
    end
  end

  describe "#configured?" do
    before do
      Salesforce::NextGeneration::ZisClient.any_instance.stubs(:configured?).returns(true)
    end

    it "should return ZisClient.configured? value" do
      assert_equal integration.configured?, true
    end
  end

  describe "#data_cache_enabled?" do
    before do
      Salesforce::NextGeneration::ZisClient.any_instance.stubs(:data_cache_enabled?).returns(true)
    end

    it "should return ZisClient.data_cache_enabled? value" do
      assert_equal integration.data_cache_enabled?, true
    end
  end

  describe "#configured_and_enabled?" do
    before do
      Salesforce::NextGeneration::ZisClient.any_instance.stubs(:enabled?).returns(true)
      Salesforce::NextGeneration::ZisClient.any_instance.stubs(:configured?).returns(true)
    end

    it "should return true if the integration is configured and enabled" do
      assert_equal integration.configured_and_enabled?, true
    end
  end

  describe "#http_get" do
    describe "when the request works as expected" do
      before do
        response = Object.new
        response.stubs(:status).returns(200)
        response.stubs(:body).returns('data')
        Salesforce::NextGeneration::SalesforceConnection.any_instance.stubs(:get).returns(response)
      end

      it "Should return the data" do
        response = integration.http_get('/some/path', q: 'SELECT * FROM Contacts')
        assert_equal 200, response.status
        assert_equal 'data', response.body
      end
    end
  end

  describe "#http_post" do
    describe "when the request works as expected" do
      before do
        response = Object.new
        response.stubs(:status).returns(200)
        response.stubs(:body).returns('OK')
        Salesforce::NextGeneration::SalesforceConnection.any_instance.stubs(:post).returns(response)
      end

      it "should return OK" do
        parameters = {
          headers: { 'Content-Type': 'application/json' },
          body: 'BODY'
        }
        response = integration.http_post('/some/path', parameters)
        assert_equal 200, response.status
        assert_equal 'OK', response.body
      end
    end
  end

  describe "#fetch_info_for" do
    describe "when fetching info for user" do
      let(:response_xml) { '<records><record><id>877a76d0</id><url>/id/877a76d0</url><label>Steven Yan (Lead)</label><record_type>Lead</record_type><fields><field><label>Title</label><value>Foo</label></field><field><label>Department</label><value>Bar</label></field></fields></record></records>' }

      before do
        integration.stubs(:get_salesforce_session).returns(session_id: 'foo', server_url: 'http://salesforce.com')
      end

      it "makes SOAP calls" do
        expected = {
          records: [
            {
              fields: [
                {
                  value: 'Foo',
                  label: 'Title'
                },
                {
                  value: 'Bar',
                  label: 'Department'
                }
              ],
              label: 'Steven Yan (Lead)',
              url: 'https://salesforce.com/id/877a76d0',
              id: '877a76d0',
              record_type: 'Lead'
            }
          ]
        }
        result = mock
        result.expects(:result).returns(response_xml)
        driver = mock
        driver.expects(:getUserFields).returns(result)
        driver.stubs(:options).returns({})
        Salesforce::Integration::Driver.expects(:get_driver).returns(driver)
        assert_equal expected, integration.fetch_info_for(users(:minimum_end_user))
      end

      describe "for an account which has the enhanced Salesforce user data lookup" do
        let(:user) { users(:minimum_end_user) }
        let(:empty_expected) { { records: [] } }

        before do
          account.stubs(:has_salesforce_integration_lookup?).returns(true)
        end

        it "returns empty data immediately if the user has no tags and lookup_type=UserTags" do
          integration.stubs(:lookup_type).returns("UserTags")
          user.stubs(:tags).returns([])
          assert_equal empty_expected, integration.fetch_info_for(users(:minimum_end_user))
        end

        it "returns empty data immediately if the user has no organization and lookup_type=OrganizationName" do
          integration.stubs(:lookup_type).returns("OrganizationName")
          user.stubs(:organization).returns(nil)
          assert_equal empty_expected, integration.fetch_info_for(users(:minimum_end_user))
        end

        it "returns empty data immediately if the user has no email address and lookup_type=EmailAddress" do
          integration.stubs(:lookup_type).returns("EmailAddress")
          user.stubs(:email).returns(nil)
          assert_equal empty_expected, integration.fetch_info_for(users(:minimum_end_user))
        end

        it "invokes getUserFieldsByList with an array of user tags if lookup_type=UserTags and user has tags" do
          integration.stubs(:lookup_type).returns("UserTags")
          user.stubs(:tags).returns(['tag1', 'tag2', 'tag3'])
          result = mock
          result.expects(:result).returns(response_xml)
          driver = mock
          driver.expects(:getUserFieldsByList).with(['tag1', 'tag2', 'tag3']).returns(result)
          driver.stubs(:options).returns({})
          Salesforce::Integration::Driver.expects(:get_driver).returns(driver)
          integration.fetch_info_for(users(:minimum_end_user))
        end

        it "invokes getUserFieldsByList with organization name if lookup_type=OrganizationName and user has an organization" do
          integration.stubs(:lookup_type).returns("OrganizationName")
          user.stubs(:organization).returns(stub(name: "LookupThisOrgName"))
          result = mock
          result.expects(:result).returns(response_xml)
          driver = mock
          driver.expects(:getUserFieldsByList).with(["LookupThisOrgName"]).returns(result)
          driver.stubs(:options).returns({})
          Salesforce::Integration::Driver.expects(:get_driver).returns(driver)
          integration.fetch_info_for(users(:minimum_end_user))
        end
      end
    end
  end
end
