require_relative '../../../../support/test_helper'
require 'zendesk/salesforce/next_generation/zis_client'

SingleCov.covered!

describe Salesforce::NextGeneration::ZisClient do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:zis_client) { Salesforce::NextGeneration::ZisClient.new(account) }

  describe "#access_token" do
    before do
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:fetch_salesforce_organization_url).returns('https://abc132.salesforce.com')
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:fetch_salesforce_organization_uuid).returns('abcdefghi')
      Salesforce::NextGeneration::ZisConnection.any_instance.stubs(:access_token).returns('123456789')
    end

    it "should return ZisConnection.access_token value" do
      assert_equal zis_client.access_token, '123456789'
    end
  end

  describe "#refresh_access_token" do
    before do
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:fetch_salesforce_organization_url).returns('https://abc132.salesforce.com')
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:fetch_salesforce_organization_uuid).returns('abcdefghi')
      Salesforce::NextGeneration::ZisConnection.any_instance.stubs(:access_token).returns('123456789')
      Salesforce::NextGeneration::ZisConnection.any_instance.stubs(:refresh_access_token).returns('987654321')
    end

    it "should return ZisConnection.refresh_access_token value" do
      assert_equal zis_client.refresh_access_token, '987654321'
    end
  end

  describe "#salesforce_organization_uuid" do
    before do
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:fetch_salesforce_organization_uuid).returns('abcdefghi')
    end

    it "should return ZisConfig.fetch_salesforce_organization_uuid value" do
      assert_equal zis_client.salesforce_organization_uuid, 'abcdefghi'
    end
  end

  describe "#salesforce_organization_url" do
    before do
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:fetch_salesforce_organization_url).returns('https://abc132.salesforce.com')
    end

    it "should return ZisConfig.fetch_salesforce_organization_url value" do
      assert_equal zis_client.salesforce_organization_url, 'https://abc132.salesforce.com'
    end
  end

  describe "#latency" do
    before do
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:fetch_latency).returns(60)
    end

    it "should return ZisConfig.fetch_latency value" do
      assert_equal zis_client.latency, 60
    end
  end

  describe "#enabled" do
    before do
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:has_enabled_config?).returns(true)
    end

    it "should return ZisConfig.has_enabled_config? value" do
      assert_equal zis_client.enabled?, true
    end
  end

  describe "#configured" do
    before do
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:has_connection_uuid?).returns(true)
    end

    it "should return ZisConfig.configured value" do
      assert_equal zis_client.configured?, true
    end
  end

  describe "#data_cache_enabled" do
    before do
      Salesforce::NextGeneration::ZisConfig.any_instance.stubs(:has_data_cache_enabled?).returns(true)
    end

    it "should return ZisConfig.has_data_cache_enabled? value" do
      assert_equal zis_client.data_cache_enabled?, true
    end
  end
end
