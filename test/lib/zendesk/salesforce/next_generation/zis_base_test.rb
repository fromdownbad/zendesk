require_relative '../../../../support/test_helper'
require 'zendesk/salesforce/next_generation/zis_base'
require 'zendesk/salesforce/next_generation/zis_connection'
require 'zendesk/salesforce/next_generation/zis_config'

SingleCov.covered!

describe Salesforce::NextGeneration::ZisBase do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:zis_connection) { Salesforce::NextGeneration::ZisConnection.new(account, 'uuid') }
  let(:zis_config) { Salesforce::NextGeneration::ZisConfig.new(account) }

  describe "zis_connetion" do
    it "#base_url" do
      assert_equal zis_connection.send(:base_url), 'https://minimum.zendesk-test.com/api/services/zis'
    end

    it "#service" do
      assert_equal zis_connection.send(:service).is_a?(Faraday::Connection), true
    end

    it "#account" do
      assert_equal zis_connection.send(:account).name, account.name
    end
  end

  describe "zis_config" do
    it "#base_url" do
      assert_equal zis_config.send(:base_url), 'https://minimum.zendesk-test.com/api/services/zis'
    end

    it "#service" do
      assert_equal zis_config.send(:service).is_a?(Faraday::Connection), true
    end

    it "#account" do
      assert_equal zis_config.send(:account).name, account.name
    end
  end
end
