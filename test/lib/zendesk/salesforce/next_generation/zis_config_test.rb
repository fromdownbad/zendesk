require_relative '../../../../support/test_helper'
require 'zendesk/salesforce/next_generation/zis_config'

SingleCov.covered!

describe Salesforce::NextGeneration::ZisConfig do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:zis_config) { Salesforce::NextGeneration::ZisConfig.new(account) }

  describe "#service_name" do
    it "should return zis_config" do
      assert_equal zis_config.service_name, 'zis_config'
    end
  end

  describe "#salesforce_organization_id" do
    describe 'happy path' do
      before do
        zis_config.stubs(:fetch_salesforce_organization_id).returns('00D4P000001')
      end

      it "should return the organization id" do
        assert_equal zis_config.salesforce_organization_id, '00D4P000001'
      end
    end

    describe 'when service returns not found' do
      let(:response) do
        {
          'errors' => {
            'status' => '404',
            'code' => '1022',
            'detail' => 'Scope not found'
          }
        }
      end

      before do
        stub_request(:get, "https://minimum.zendesk-test.com/api/services/zis/integrations/salesforce/configs?filter[scope]=salesforce-organization-id:*/").
          to_return(status: 404, body: response.to_json, headers: { 'Content-Type' => 'application/json' })
        Rails.logger.expects(:warn).with("No ZIS configurations found: 404 Not Found: #{response}")
      end

      it 'returns nil' do
        assert_nil zis_config.salesforce_organization_id
      end
    end
  end

  describe "#find_configs_by_scope" do
    describe "when the scope is general" do
      before do
        zis_config.stubs(:all_configs).returns(configs)
      end

      it "should return the list of general config" do
        assert_equal zis_config.find_configs_by_scope('/general/'), [configs[0], configs[1]]
      end
    end

    describe "when the scope is sidebar_app" do
      before do
        zis_config.stubs(:all_configs).returns(configs)
      end

      it "should return the list of sidebar_app config" do
        assert_equal zis_config.find_configs_by_scope('salesforce-organization-id:00D4P000001/sidebar_app/'), [configs[2]]
      end
    end

    describe "when there is not data" do
      before do
        zis_config.stubs(:all_configs).returns([])
      end

      it "should return an empty list" do
        assert_equal zis_config.find_configs_by_scope('general/'), []
      end
    end
  end

  describe "#find_enabled_config" do
    it "should return the enabled config" do
      general_configs = [configs[0], configs[1]]
      assert_equal zis_config.find_enabled_config(general_configs), configs[1]
    end
  end

  def configs
    [
      {
        'id' => 1,
        'scope' => 'salesforce-organization-id:00D4P000001/general/',
        'zendesk_account_id' => 1,
        'integration' => 'salesforce',
        'config' => {
            'enabled' => false,
            'salesforce' => {
              'organization' => {
                  'id' => '00D4P000001',
                  'url' => 'https://abc132.salesforce.com',
                  'connection_uuid' => 'e8d93ab1-76ab-4856-a866-222222223',
                  'oauth_client_uuid' => '3b098bdc-5a6f-4729-9e8a-1111111113'
              }
            },
            'zendesk' => {
              'account' => {
                  'id' => 1,
                  'subdomain' => 'support'
              },
              'integration_user_id' => 10001
            }
        },
        'created_at' => '2019-08-19T05:10:09Z',
        'updated_at' => '2019-10-04T05:30:10Z'
      },
      {
        'id' => 2,
        'scope' => 'salesforce-organization-id:00D4P000001/general/',
        'zendesk_account_id' => 1,
        'integration' => 'salesforce',
        'config' => {
            'enabled' => true,
            'salesforce' => {
              'organization' => {
                  'id' => '00D4P000001',
                  'url' => 'https://abc132.salesforce.com',
                  'connection_uuid' => 'e8d93ab1-76ab-4856-a866-222222222',
                  'oauth_client_uuid' => '3b098bdc-5a6f-4729-9e8a-1111111111'
              }
            },
            'zendesk' => {
              'account' => {
                  'id' => 1,
                  'subdomain' => 'support'
              },
              'integration_user_id' => 10001
            }
        },
        'created_at' => '2019-08-19T05:10:09Z',
        'updated_at' => '2019-10-04T05:30:10Z'
      },
      {
        'id' => 3,
        'scope' => 'salesforce-organization-id:00D4P000001/sidebar_app/',
        'zendesk_account_id' => 1,
        'integration' => 'salesforce',
        'config' => {
            'enabled' => true,
            'latency' => 60
        },
        'created_at' => '2019-10-02T03:41:57Z',
        'updated_at' => '2019-10-08T11:23:14Z'
      }
    ]
  end
end
