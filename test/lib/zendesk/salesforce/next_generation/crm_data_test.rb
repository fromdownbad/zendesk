require_relative '../../../../support/test_helper'
require 'zendesk/salesforce/next_generation/crm_data'
require 'zendesk/salesforce/next_generation/integration'

SingleCov.covered!

describe Salesforce::NextGeneration::CrmData do
  fixtures :accounts, :users, :tickets

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:ticket) { tickets(:minimum_1) }

  describe "new generation salesforce integration" do
    let(:integration) { Salesforce::NextGeneration::Integration.new(account) }

    before do
      account.stubs(:has_central_admin_salesforce_integration?).returns(true)
    end

    it "#integration_name" do
      assert_equal integration.integration_name, 'Salesforce'
    end

    it "#parse_salesforce_xml" do
      data = integration.parse_salesforce_xml(xml, 'ns1-api.salesforce.com')
      assert_equal expected, data
    end

    it "#crm_data" do
      assert_equal integration.crm_data(agent, true).is_a?(SalesforceData), true
    end

    it "#crm_ticket_data" do
      assert_equal integration.crm_ticket_data(ticket, true).is_a?(SalesforceTicketData), true
    end

    it "#configuration" do
      assert_equal integration.configuration.is_a?(Salesforce::Configuration), true
    end
  end

  describe "classic salesforce integration" do
    let(:integration) { SalesforceIntegration.new }

    before do
      account.stubs(:has_central_admin_salesforce_integration?).returns(false)
      integration.account = account
      integration.stubs(:is_sandbox?).returns(false)
    end

    it "#integration_name" do
      assert_equal integration.integration_name, 'Salesforce'
    end

    it "#parse_salesforce_xml" do
      data = integration.parse_salesforce_xml(xml, 'ns1-api.salesforce.com')
      assert_equal expected, data
    end

    it "#crm_data" do
      assert_equal integration.crm_data(agent, true).is_a?(SalesforceData), true
    end

    it "#crm_ticket_data" do
      assert_equal integration.crm_ticket_data(ticket, true).is_a?(SalesforceTicketData), true
    end

    it "#configuration" do
      assert_equal integration.configuration.is_a?(Salesforce::Configuration), true
    end
  end

  def xml
    <<-XML
      <records>
        <record>
          <id>877a76d0</id>
          <url>/id/877a76d0</url>
          <label>Steven Yan</label>
          <record_type>Lead</record_type>
          <fields>
            <field>
              <label>Title</label>
              <value>Foo</label>
            </field>
            <field>
              <label>Department</label>
              <value>Bar</label>
            </field>
          </fields>
        </record>
        <record>
          <id>8eb8ed08</id>
          <url>/id/8eb8ed08</url>
          <label>Steven Yan</label>
          <record_type>Contact</record_type>
          <fields>
            <field>
              <label>Title</label>
              <value>Foo</label>
            </field>
            <field>
              <label>Department</label>
              <value>Bar</label>
            </field>
          </fields>
        </record>
      </records>
    XML
  end

  def expected
    {
      records: [
        {
          fields: [
            {
              value: 'Foo',
              label: 'Title'
            },
            {
              value: 'Bar',
              label: 'Department'
            }
          ],
          label: 'Steven Yan',
          id: '877a76d0',
          url: 'https://ns1.salesforce.com/id/877a76d0',
          record_type: 'Lead'
        },
        {
          fields: [
            {
              value: 'Foo',
              label: 'Title'
            },
            {
              value: 'Bar',
              label: 'Department'
            }
          ],
          label: 'Steven Yan',
          id: '8eb8ed08',
          url: 'https://ns1.salesforce.com/id/8eb8ed08',
          record_type: 'Contact'
        }
      ]
    }
  end
end
