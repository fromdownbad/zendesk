require_relative '../../../../support/test_helper'
require 'zendesk/salesforce/next_generation/zis_connection'

SingleCov.covered!

describe Salesforce::NextGeneration::ZisConnection do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:zis_connection) { Salesforce::NextGeneration::ZisConnection.new(account, '123456') }

  let(:access_token_response) do
    {
      'UUID': 'e8d93ab1-76ab-4856-a866-1111111',
      'ZendeskAccountID': 1,
      'PermissionScope': 'refresh_token id api',
      'AccessToken': '00D4P000000x8bY!xxx',
      'TokenType': 'Bearer',
      'RefreshToken': '5Aep8618yVsldz6rZMOO',
      'TokenExpiry': '0001-01-01T00:00:00Z',
      'OAuthAccessTokenResponseBody': '',
      'CreatedBy': 'centraladminapp'
    }
  end

  let(:refresh_access_token_response) do
    {
      'UUID': 'e8d93ab1-76ab-4856-a866-1111111',
      'ZendeskAccountID': 1,
      'PermissionScope': 'refresh_token id api',
      'AccessToken': '00D4P000000x8bY!yyy',
      'TokenType': 'Bearer',
      'RefreshToken': '5Aep8618yVsldz6rZMOO',
      'TokenExpiry': '0001-01-01T00:00:00Z',
      'OAuthAccessTokenResponseBody': '',
      'CreatedBy': 'centraladminapp'
    }
  end

  describe "#access_token" do
    before do
      response = Faraday::Response.new(body: access_token_response.deep_stringify_keys)
      Salesforce::NextGeneration::ZisConnection.any_instance.stubs(:fetch_access_token).returns(response)
    end

    it "returns token" do
      assert_equal zis_connection.access_token, '00D4P000000x8bY!xxx'
    end
  end

  describe "#refresh_access_token" do
    before do
      response = Faraday::Response.new(body: refresh_access_token_response.deep_stringify_keys)
      Salesforce::NextGeneration::ZisConnection.any_instance.stubs(:fetch_refresh_token).returns(response)
    end

    it "returns token" do
      assert_equal zis_connection.refresh_access_token, '00D4P000000x8bY!yyy'
    end

    describe "when a Kragle::UnprocessableEntity exception is raised" do
      before do
        response = Faraday::Response.new(
            status: 422,
            body: 'oauth2: cannot fetch token: 400 Bad Request'
          )
        kragle_exception = Kragle::UnprocessableEntity.new('422 Unprocessable Entity', response)
        zis_connection.stubs(:fetch_refresh_token).raises(kragle_exception)
      end

      it "raise a Salesforce::NextGeneration::RefreshTokenError exception" do
        err = -> { zis_connection.refresh_access_token }.must_raise Salesforce::NextGeneration::RefreshTokenError
        err.message.must_match /RefreshTokenError Exception/
      end
    end
  end
end
