require_relative '../../../../../support/test_helper'
require 'zendesk/salesforce/next_generation/zis_configs/general'

SingleCov.covered!

describe Salesforce::NextGeneration::ZisConfigs::General do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:zis_config) { Salesforce::NextGeneration::ZisConfig.new(account) }

  describe "when there is a enabled config" do
    before do
      zis_config.stubs(:salesforce_organization).returns(enabled_config)
    end

    it "#fetch_salesforce_organization_id" do
      assert_equal zis_config.fetch_salesforce_organization_id, '00D4P000001'
    end

    it "#fetch_salesforce_organization_uuid" do
      assert_equal zis_config.fetch_salesforce_organization_uuid, 'e8d93ab1-76ab-4856-a866-222222222'
    end

    it "#fetch_salesforce_organization_url" do
      assert_equal zis_config.fetch_salesforce_organization_url, 'https://abc132.salesforce.com'
    end

    it "#has_enabled_config?" do
      assert_equal zis_config.has_enabled_config?, true
    end

    it "#has_connection_uuid?" do
      assert_equal zis_config.has_connection_uuid?, true
    end
  end

  describe "when there is a unexpected response" do
    before do
      zis_config.stubs(:salesforce_organization).returns({})
    end

    it "uuid, url and id methods of salesforce organization should return nil" do
      assert_nil zis_config.fetch_salesforce_organization_uuid
      assert_nil zis_config.fetch_salesforce_organization_url
      assert_nil zis_config.fetch_salesforce_organization_id
    end

    it "has_enabled_config? and has_connection_uuid? return false" do
      assert_equal zis_config.has_enabled_config?, false
      assert_equal zis_config.has_connection_uuid?, false
    end
  end

  let(:enabled_config) do
    {
      'id' => '00D4P000001',
      'url' => 'https://abc132.salesforce.com',
      'connection_uuid' => 'e8d93ab1-76ab-4856-a866-222222222',
      'oauth_client_uuid' => '3b098bdc-5a6f-4729-9e8a-1111111111'
    }
  end
end
