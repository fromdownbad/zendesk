require_relative '../../../../../support/test_helper'
require 'zendesk/salesforce/next_generation/zis_configs/sidebar_app'

SingleCov.covered!

describe Salesforce::NextGeneration::ZisConfigs::SidebarApp do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:zis_config) { Salesforce::NextGeneration::ZisConfig.new(account) }

  describe "when there is a enabled config" do
    before do
      zis_config.stubs(:siderbar_app).returns(enabled_config)
    end

    it "#fetch_latency" do
      assert_equal zis_config.fetch_latency, 60
    end

    it "#has_data_cache_enabled?" do
      assert_equal zis_config.has_data_cache_enabled?, true
    end
  end

  describe "when there is a unexpected response" do
    before do
      zis_config.stubs(:siderbar_app).returns({})
    end

    it "fetch_uuid and fetch_url return nil" do
      assert_nil zis_config.fetch_latency
    end

    it "has_data_cache_enabled? return false" do
      assert_equal zis_config.has_data_cache_enabled?, false
    end
  end

  let(:enabled_config) do
    {
      'enabled' => true,
      'latency' => 60
    }
  end
end
