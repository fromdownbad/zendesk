require_relative "../../../support/test_helper"

SingleCov.not_covered!

describe 'ConfigurationManagement' do
  fixtures :accounts, :users, :user_identities

  before do
    @account = accounts(:minimum)
    @salesforce_integration = SalesforceIntegration.new(account: @account)
  end

  describe "SalesforceConfiguration" do
    before do
      @sample_config = '{"Contact":{"fields":["Name","Email","Account::Name"],"labels":{"Name":"Full Name","Email":"Email","Account::Name":"Account Name"},"relationships":{"Account":{"object":"Account","type":"child"}},"mapping":["Email","ticket.requester.email"],"sf_field_type":"email","object_label":"The Contact"}}'
    end

    it "returns a default configuration" do
      @account.texts.salesforce_configuration = nil
      @account.save

      assert_equal Salesforce::Configuration.default,
        @salesforce_integration.configuration.send(:configuration)
    end

    it "returns a specific object configuration and the shortcut methods shoud work as expected" do
      @account.texts.salesforce_configuration = @sample_config
      @account.save

      fields = ["Name", "Email", "Account::Name"]
      labels = {
        "Name" => "Full Name",
        "Email" => "Email",
        "Account::Name" => "Account Name"
      }
      relationships = {
        "Account" => {
          "object" => "Account",
          "type" => "child"
        }
      }
      mapping = ["Email", "ticket.requester.email"]
      sf_field_type = "email"
      object_label = "The Contact"

      config = @salesforce_integration.configuration.object("Contact")
      assert_equal fields, config["fields"]
      assert_equal labels, config["labels"]
      assert_equal relationships, config["relationships"]
      assert_equal mapping, config["mapping"]
      assert_equal sf_field_type, config["sf_field_type"]
      assert_equal object_label, config["object_label"]

      assert_equal fields, @salesforce_integration.configuration.fields("Contact")
      assert_equal labels, @salesforce_integration.configuration.labels("Contact")
      assert_equal relationships, @salesforce_integration.configuration.relationships("Contact")
      assert_equal mapping, @salesforce_integration.configuration.mapping("Contact")
      assert_equal sf_field_type, @salesforce_integration.configuration.salesforce_mapped_field_type("Contact")
      assert_equal object_label, @salesforce_integration.configuration.object_label("Contact")

      assert_equal "Email", @salesforce_integration.configuration.salesforce_mapped_field("Contact")
    end

    it "saves a different object configuration" do
      @account.texts.salesforce_configuration = nil
      @account.save
      @salesforce_integration.save

      fields = ["Name", "Email", "Account::Name"]
      labels = {
        "Name" => "Full Name",
        "Email" => "Email",
        "Account::Name" => "Account Name"
      }
      relationships = {
        "Account" => {
          "object" => "Account",
          "type" => "child"
        }
      }
      mapping = ["Email", "ticket.requester.email"]
      sf_field_type = "email"
      object_label = "The Contact"

      @salesforce_integration.configuration.add("Contact", fields, labels, relationships, "Email", "ticket.requester.email", sf_field_type, object_label)

      config = @salesforce_integration.reload.configuration.object("Contact")
      assert_equal fields, config["fields"]
      assert_equal labels, config["labels"]
      assert_equal relationships, config["relationships"]
      assert_equal mapping, config["mapping"]
      assert_equal sf_field_type, config["sf_field_type"]
      assert_equal object_label, config["object_label"]
    end

    it "adds errors to an object configuration" do
      @account.texts.salesforce_configuration = @sample_config
      @account.save
      @salesforce_integration.save

      @salesforce_integration.configuration.add_errors(
        "Contact",
        Salesforce::Configuration::MISSING_OBJECT => true,
        Salesforce::Configuration::MISSING_SALESFORCE_MAPPED_FIELD => true,
        Salesforce::Configuration::MISSING_FIELDS => ["Email"]
      )

      errors = @salesforce_integration.reload.configuration.errors("Contact")
      assert errors[Salesforce::Configuration::MISSING_OBJECT]
      assert errors[Salesforce::Configuration::MISSING_SALESFORCE_MAPPED_FIELD]
      assert_equal ["Email"], errors[Salesforce::Configuration::MISSING_FIELDS]
    end

    it "adds filters to an object configuration" do
      @account.texts.salesforce_configuration = @sample_config
      @account.save
      @salesforce_integration.save

      filters = [
        {
          "field" => "Name",
          "type" => "string",
          "operator" => "=",
          "value" => "Some Name"
        },
        {
          "field" => "Date",
          "type" => "date",
          "operator" => "<",
          "value" => "April 12, 2013"
        }
      ]

      @salesforce_integration.configuration.add_filters("Contact", "Relationship", 3, filters)

      fields = @salesforce_integration.reload.configuration.relationship_filter_fields("Contact", "Relationship")
      assert_equal fields, filters

      limit = @salesforce_integration.reload.configuration.relationship_limit("Contact", "Relationship")
      assert_equal 3, limit
    end

    it "removes an object configuration" do
      @account.texts.salesforce_configuration = @sample_config
      @account.save
      @salesforce_integration.save

      @salesforce_integration.configuration.remove("Contact")

      config = @salesforce_integration.reload.configuration.object("Contact")
      assert_equal({}, config)
    end

    it "returns and organized structure of the configured fields" do
      @account.texts.salesforce_configuration = @sample_config
      @account.save
      @salesforce_integration.save

      @salesforce_integration.configuration.add_errors(
        "Contact",
        Salesforce::Configuration::MISSING_OBJECT => true,
        Salesforce::Configuration::MISSING_SALESFORCE_MAPPED_FIELD => true,
        Salesforce::Configuration::MISSING_FIELDS => ["Email"]
      )

      primary, related = @salesforce_integration.configuration.organized_fields("Contact", true)

      assert_equal ["Name", "Email"], primary
      assert_equal ["Name"], related["Account"]

      primary, related = @salesforce_integration.configuration.organized_fields("Contact", false)

      assert_equal ["Name"], primary
      assert_equal ["Name"], related["Account"]
    end

    it "returns an ordered list of configured objects and provide ability to sort them" do
      @account.texts.salesforce_configuration = @sample_config
      @account.save
      @salesforce_integration.save

      @salesforce_integration.configuration.add("Account",
        ["Name", "Type"],
        { "Name" => "Account Name", "Type" => "Account Type" },
        {},
        "Name",
        "ticket.organization.name",
        "email",
        "Account")

      assert_equal ["Contact", "Account"], @salesforce_integration.reload.configuration.objects
      @salesforce_integration.configuration.sort(["Account", "Contact"])

      assert_equal ["Account", "Contact"], @salesforce_integration.reload.configuration.objects
    end
  end
end
