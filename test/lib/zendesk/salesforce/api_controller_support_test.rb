require_relative "../../../support/test_helper"
require 'zendesk/salesforce/api_controller_support'

SingleCov.covered!

describe Zendesk::Salesforce::ApiControllerSupport do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  class RenderFailureTestClass
    def render_failure(args = {})
      args
    end
  end

  let(:account) { accounts(:minimum) }

  before do
    @api_controller_support = RenderFailureTestClass.new
    @api_controller_support.extend(Zendesk::Salesforce::ApiControllerSupport)
  end

  describe "#require_salesforce_configuration!" do
    describe "account has no salesforce configuration configured" do
      before do
        account.stubs(:salesforce_configuration_enabled?).returns(false)
        @api_controller_support.stubs(:current_account).returns(account)
      end

      it "returns unauthorized status" do
        assert_equal @api_controller_support.send(:require_salesforce_configuration!)[:status], :unauthorized
      end
    end

    describe "account has salesforce configuration configured" do
      before do
        account.stubs(:salesforce_configuration_enabled?).returns(true)
        @api_controller_support.stubs(:current_account).returns(account)
      end

      it "returns nil" do
        assert_nil @api_controller_support.send(:require_salesforce_configuration!)
      end
    end
  end

  describe "#handle_kragle_exception" do
    it "renders and return a RefreshTokenError Exception" do
      @api_controller_support.expects(:render).with(json: { errors: 'RefreshTokenError Exception' }, status: :unauthorized).once
      @api_controller_support.send(:handle_kragle_exception) { raise Salesforce::NextGeneration::RefreshTokenError }
    end

    it "renders and return a Faraday::ClientError Exception" do
      faraday_client_error = Faraday::ClientError.new('any client error')
      faraday_client_error.stubs(:response).returns(body: 'message', status: 403)

      @api_controller_support.expects(:render).with(json: { errors: 'message' }, status: 403).once
      @api_controller_support.send(:handle_kragle_exception) { raise faraday_client_error }
    end
  end
end
