require_relative "../../../support/test_helper"
require 'zendesk/salesforce/object_management'

SingleCov.covered! uncovered: 36

describe Salesforce::ObjectManagement do
  fixtures :accounts, :users, :user_identities, :tickets

  def generate_random_sfid
    [*('a'..'z'), *('0'..'9')].sample(18).join
  end

  contact = {
    "name" => "Contact",
    "fields" => [
      {
        "length" => 18,
        "updateable" => true,
        "scale" => 0,
        "name" => "Id",
        "type" => "id",
        "picklistValues" => [],
        "label" => "Contact ID",
        "referenceTo" => [],
        "relationshipName" => nil
      },
      {
        "length" => 80,
        "updateable" => true,
        "scale" => 0,
        "name" => "Email",
        "type" => "email",
        "picklistValues" => [],
        "label" => "Email",
        "referenceTo" => [],
        "relationshipName" => nil
      },
      {
        "length" => 121,
        "updateable" => true,
        "scale" => 0,
        "name" => "Name",
        "type" => "string",
        "picklistValues" => [],
        "label" => "Full Name",
        "referenceTo" => [],
        "relationshipName" => nil
      },
      {
        "length" => 80,
        "updateable" => true,
        "scale" => 0,
        "name" => "Notes",
        "type" => "textarea",
        "picklistValues" => [],
        "label" => "Notes",
        "referenceTo" => [],
        "relationshipName" => nil
      },
      {
        "length" => 18,
        "updateable" => true,
        "scale" => 0,
        "name" => "AccountId",
        "type" => "reference",
        "picklistValues" => [],
        "label" => "Account ID",
        "referenceTo" => ["Account"],
        "relationshipName" => "Account"
      },
      {
        "length" => 40,
        "updateable" => true,
        "scale" => 0,
        "name" => "Salutation",
        "type" => "picklist",
        "picklistValues" => [
          {
            "active": true,
            "defaultValue": false,
            "label": "Mr.",
            "validFor": nil,
            "value": "Mr."
          },
          {
            "active": true,
            "defaultValue": false,
            "label": "Ms.",
            "validFor": nil,
            "value": "Ms."
          }
        ],
        "label" => "Salutation",
        "referenceTo" => [],
        "relationshipName" => nil
      }
    ],
    "childRelationships" => [
      {
        "field" => "ContactId",
        "deprecatedAndHidden" => false,
        "relationshipName" => "Cases",
        "cascadeDelete" => false,
        "childSObject" => "Case"
      }
    ]
  }

  sf_case = {
    "name" => "Case",
    "fields" => [
      {
        "length" => 30,
        "updateable" => true,
        "scale" => 0,
        "name" => "CaseNumber",
        "type" => "string",
        "picklistValues" => [],
        "label" => "Case Number",
        "referenceTo" => [],
        "relationshipName" => nil
      }
    ]
  }

  account = {
    "name" => "Account",
    "fields" => [
      {
        "length" => 255,
        "updateable" => true,
        "scale" => 0,
        "name" => "Name",
        "type" => "string",
        "picklistValues" => [],
        "label" => "Account Name",
        "referenceTo" => [],
        "relationshipName" => nil
      }
    ]
  }

  before do
    @account = accounts(:minimum)
    @salesforce_integration = SalesforceIntegration.new(account: @account)
    @account_fields = ["Name", "Description"]
    account_labels = {
      "Name" => "Full Name",
      "Description" => "Description"
    }
    account_relationships = {}
    sf_field = "Name"
    zendesk_field = "ticket.requester.name"
    sf_field_type = "string"
    object_label = "Account"

    sample_config = '{"Contact":{"fields":["Name","Email","Notes","Salutation","Account::Name","Cases::CaseNumber"],"labels":{"Name":"Full Name","Email":"Email","Notes":"Notes","Salutation":"Salutation","Account::Name":"Account Name","Cases::CaseNumber":"Case Number"},"relationships":{"Account":{"object":"Account","type":"child"},"Cases":{"object":"Case","type":"parent"}},"mapping":["Email","ticket.requester.email"],"sf_field_type":"email"}}'
    @account.texts.salesforce_configuration = sample_config
    @salesforce_integration.configuration.add("Account", @account_fields, account_labels, account_relationships,
      sf_field, zendesk_field, sf_field_type, object_label)
    Account.any_instance.stubs(:salesforce_integration).returns(@salesforce_integration)
    @salesforce_integration.stubs(:get_salesforce_session).returns(session_id: "foo", server_url: "https://na2-api.salesforce.com")

    @salesforce_integration.stubs(:describe_sobject).with("Contact").returns(contact)
    @salesforce_integration.stubs(:describe_sobject).with("Case").returns(sf_case)
    @salesforce_integration.stubs(:describe_sobject).with("Account").returns(account)

    show_parent_default = 'show_parent_and_ultimate_parent'
    @account_ids = []
    @account_records = []
    @hierarchy = {}
    5.times do
      account_id = generate_random_sfid
      immediate_account_id = generate_random_sfid
      ultimate_account_id = generate_random_sfid
      @account_ids.push account_id
      @hierarchy[account_id] = [
        {"Id" => immediate_account_id, "attributes" => {"url" => "api/services/data/#{immediate_account_id}"}},
        {"Id" => ultimate_account_id, "attributes" => {"url" => "api/services/data/#{ultimate_account_id}"}}
      ]
      @account_records << {"Id" => account_id, "ParentId" => immediate_account_id}
    end

    response = stub('Response', status: 200, body: @hierarchy.to_json)
    @salesforce_integration.stubs(:http_get).with(@salesforce_integration.account_hierarchy_endpoint,
      ids: @account_ids.join(","),
      fields: @account_fields.join(","),
      type: show_parent_default).returns(response)
    @salesforce_integration.stubs(:show_parent).returns(show_parent_default)

    @sf_objects = ['Account', 'Contact', 'Case'].freeze
    @salesforce_integration.configuration.stubs(:objects).returns(@sf_objects)
    @sf_object_records = {}
    @sf_objects.each do |sf_object|
      @sf_object_records[sf_object] = []
      5.times do
        @sf_object_records[sf_object].push 'Id' => generate_random_sfid
      end
      @salesforce_integration.configuration.stubs(:errors).with(sf_object).returns({})
      @salesforce_integration.stubs(:salesforce_query).with(sf_object, {}).returns(@sf_object_records[sf_object])
    end
  end

  describe '#get_account_hierarchy' do
    it "fetches account hierarchy" do
      result = @salesforce_integration.get_account_hierarchy(@account_ids)
      assert_equal @hierarchy, result
    end

    it "returns empty account hierarchy when callout fails" do
      response = stub('Response', status: 500, body: "")
      @salesforce_integration.stubs(:http_get).with(@salesforce_integration.account_hierarchy_endpoint,
        ids: @account_ids.join(","),
        fields: @account_fields.join(","),
        type: @salesforce_integration.show_parent).returns(response)

      result = @salesforce_integration.get_account_hierarchy(@account_ids)
      assert_equal result, {}
    end
  end

  describe "#account_hierarchy_endpoint" do
    it "returns endpoint with namespace when in production" do
      production_endpoint = "/services/apexrest/#{Salesforce::ObjectManagement::APP_NAMESPACE}/app/account/hierarchy"
      Rails.stub(:env, ActiveSupport::StringInquirer.new('production')) do
        assert_equal production_endpoint, @salesforce_integration.account_hierarchy_endpoint
      end
    end

    it "returns endpoint with namespace when in staging" do
      staging_endpoint = "/services/apexrest/#{Salesforce::ObjectManagement::APP_NAMESPACE}/app/account/hierarchy"
      Rails.stub(:env, ActiveSupport::StringInquirer.new('staging')) do
        assert_equal staging_endpoint, @salesforce_integration.account_hierarchy_endpoint
      end
    end

    it "returns endpoint without namespace when in development" do
      dev_endpoint = "/services/apexrest/app/account/hierarchy"
      Rails.stub(:env, ActiveSupport::StringInquirer.new('development')) do
        assert_equal dev_endpoint, @salesforce_integration.account_hierarchy_endpoint
      end
    end
  end

  describe '#fetch_parent_accounts' do
    it "returns parent map correctly" do
      parent_map = @salesforce_integration.fetch_parent_accounts(@account_records)
      parent_map_keys = parent_map.keys
      @account_ids.each do |account_id|
        assert_includes parent_map_keys, account_id
        parents = @hierarchy[account_id].map { |parent| parent["Id"] }
        assert_includes parents, parent_map[account_id][0]["record_id"]
        assert_includes parents, parent_map[account_id][1]["record_id"]
      end
    end

    it "returns empty parent map when all accounts have no parent" do
      account_records = []
      # no parent ids
      5.times do
        account_records.push "Id" => generate_random_sfid
      end
      parent_map = @salesforce_integration.fetch_parent_accounts(account_records)
      assert_empty parent_map
    end
  end

  describe '#fetch_ticket_info' do
    it "returns ticket information" do
      test_params = {}
      ticket_information = @salesforce_integration.fetch_ticket_info(test_params)
      @sf_objects.each do |sf_object|
        @sf_object_records[sf_object].each do |record|
          assert_includes ticket_information[:records], record
        end
      end
    end

    it "returns ticket information even if one object has error" do
      test_params = {}
      error = {
        Salesforce::Configuration::MISSING_SALESFORCE_MAPPED_FIELD => true,
        Salesforce::Configuration::MISSING_OBJECT => 'Case',
        Salesforce::Configuration::MISSING_FIELDS => 'Name'
      }
      @salesforce_integration.configuration.stubs(:errors).with('Case').returns(error)
      ticket_information = @salesforce_integration.fetch_ticket_info(test_params)
      @sf_objects.each do |sf_object|
        next if sf_object == 'Case' # exclude Case
        @sf_object_records[sf_object].each do |record|
          assert_includes ticket_information[:records], record
        end
      end
    end
  end

  it "returns the list of related fields" do
    expected_result = [
      {
        relationship: "Account",
        object: "Account",
        type: SalesforceIntegration::CHILD_RELATIONSHIP
      },
      {
        relationship: "Cases",
        object: "Case",
        type: SalesforceIntegration::PARENT_RELATIONSHIP
      }
    ]

    assert_equal expected_result, @salesforce_integration.list_related_fields("Contact")
  end

  it "returns an ordered list of fields to build the tree" do
    @object_name = "Contact"
    selected_fields = @salesforce_integration.configuration.fields(@object_name)
    primary_fields = @salesforce_integration.fields(@object_name, selected_fields, nil)

    expected_result = [
      {
        title: "Account ID",
        key: "AccountId",
        select: false,
        type: "reference",
        picklistValues: [],
        length: 18,
        updateable: true,
        scale: 0
      },
      {
        title: "Contact ID",
        key: "Id",
        select: false,
        type: "id",
        picklistValues: [],
        length: 18,
        updateable: true,
        scale: 0
      },
      {
        title: "Email",
        key: "Email",
        select: true,
        type: "email",
        picklistValues: [],
        length: 80,
        updateable: true,
        scale: 0
      },
      {
        title: "Full Name",
        key: "Name",
        select: true,
        type: "string",
        picklistValues: [],
        length: 121,
        updateable: true,
        scale: 0
      },
      {
        title: "Notes",
        key: "Notes",
        select: true,
        type: "textarea",
        picklistValues: [],
        length: 80,
        updateable: true,
        scale: 0
      },
      {
        title: "Salutation",
        key: "Salutation",
        select: true,
        type: "picklist",
        picklistValues: [
          {
            "active": true,
            "defaultValue": false,
            "label": "Mr.",
            "validFor": nil,
            "value": "Mr."
          },
          {
            "active": true,
            "defaultValue": false,
            "label": "Ms.",
            "validFor": nil,
            "value": "Ms."
          }
        ],
        length: 40,
        updateable: true,
        scale: 0
      }
    ]

    assert_equal expected_result, primary_fields
  end

  it "returns the list of related fields to build the tree" do
    @object_name = "Contact"
    selected_fields = @salesforce_integration.configuration.fields(@object_name)
    related_fields = @salesforce_integration.related_fields(@object_name, selected_fields)

    expected_result = [
      {
        fields: [
          {
            key: "Account::Name",
            select: true,
            title: "Account Name",
            type: "string",
            picklistValues: [],
            length: 255,
            updateable: true,
            scale: 0
          }
        ],
        isFolder: true,
        object: "Account",
        relationship: "Account",
        title: "Account",
        type: "child"
      },
      {
        fields: [
          {
            key: "Cases::CaseNumber",
            select: true,
            title: "Case Number",
            type: "string",
            picklistValues: [],
            length: 30,
            updateable: true,
            scale: 0
          }
        ],
        isFolder: true,
        object: "Case",
        relationship: "Cases",
        title: "Cases",
        type: "parent"
      }
    ]

    assert_equal expected_result, related_fields
  end

  it "sanitizes condition to prevent soql injection" do
    assert_equal "Email = 'A problematic \\' string'", @salesforce_integration.soql_condition("Contact", "ticket.requester.email" => "A problematic ' string")
  end

  it "handles different types in soql condition" do
    @salesforce_integration.configuration.stubs(:salesforce_mapped_field_type).returns("string")
    assert_equal "Email = '1'", @salesforce_integration.soql_condition("Contact", "ticket.requester.email" => "1")

    @salesforce_integration.configuration.stubs(:salesforce_mapped_field_type).returns("double")
    assert_equal "Email = 1", @salesforce_integration.soql_condition("Contact", "ticket.requester.email" => "1")

    @salesforce_integration.configuration.stubs(:salesforce_mapped_field_type).returns("int")
    assert_equal "Email = 1", @salesforce_integration.soql_condition("Contact", "ticket.requester.email" => "1")

    @salesforce_integration.configuration.stubs(:salesforce_mapped_field_type).returns("percent")
    assert_equal "Email = 1", @salesforce_integration.soql_condition("Contact", "ticket.requester.email" => "1")

    @salesforce_integration.configuration.stubs(:salesforce_mapped_field_type).returns("boolean")
    assert_equal "Email = true", @salesforce_integration.soql_condition("Contact", "ticket.requester.email" => "true")

    @salesforce_integration.configuration.stubs(:salesforce_mapped_field_type).returns("boolean")
    assert_equal "Email = false", @salesforce_integration.soql_condition("Contact", "ticket.requester.email" => "0")

    # Handle case when type is boolean and value is not a valid boolean string representation
    @salesforce_integration.configuration.stubs(:salesforce_mapped_field_type).returns("boolean")
    assert_raises Exception, "The value for the condition is not a boolean" do
      assert_nil @salesforce_integration.soql_condition("Contact", "ticket.requester.email" => "333")
    end
  end

  describe "#salesforce_query" do
    [
      'invalid ID field',
      'API is disabled for this User',
      'implementation restriction on ActivityHistories',
      'TotalRequests Limit exceeded',
      "ERROR at Row:1:Column:146\\nvalue of filter criterion for field 'Amount' must be of type double and should not be enclosed in quotes",
      "ERROR at Row:1:Column:146\\nvalue of filter criterion for field 'AnnualRevenue' must be of type double and should not be enclosed in quotes",
      'reference your WSDL',
      'Server too busy',
      'There is an implementation restriction on OpenActivities. When you query this relationship, security evaluation is implemented for users who don\'t have administrator permissions, and only a single parent record may be evaluated.',
      'Your query request was running for too long'
    ].each do |error_message|
      it "handles '#{error_message}' errors and return an empty set" do
        soql = 'SELECT Name, Email FROM Contact'
        body = "[{\"message\":\"\\nsalesforce error preamble\\n#{error_message}\\nerror above should be matched\",\"errorCode\":\"SALESFORCE_ERROR_CODE\"}]"
        response = OAuth2::Response.new('')
        response.stubs(:body).returns(body)
        response.stubs(:status).returns(404)

        @salesforce_integration.stubs(:http_get).with("/services/data/v#{Salesforce::ObjectManagement::SALESFORCE_API_VERSION}/query", q: soql).returns(response)
        @salesforce_integration.stubs(:soql_expr).with("Contact", {}).returns(soql)
        @salesforce_integration.unstub(:salesforce_query)
        assert_equal [], @salesforce_integration.salesforce_query("Contact", {})
      end
    end

    it "reports unknown errors in query" do
      soql = 'SELECT Name, Email FROM Contact'
      body = "[{\"message\":\"Some Unknown Error\",\"errorCode\":\"SOME_UNKNOWN_ERROR_CODE\"}]"
      response = OAuth2::Response.new('')
      response.stubs(:body).returns(body)
      response.stubs(:status).returns(404)

      @salesforce_integration.stubs(:http_get).with("/services/data/v#{Salesforce::ObjectManagement::SALESFORCE_API_VERSION}/query", q: soql).returns(response)
      @salesforce_integration.stubs(:soql_expr).with("Contact", {}).returns(soql)
      @salesforce_integration.unstub(:salesforce_query)
      assert_raise RuntimeError do
        @salesforce_integration.salesforce_query("Contact", {})
      end
    end

    it "returns empty list when no soql is constructed" do
      soql = ''
      @salesforce_integration.stubs(:soql_expr).with("Contact", {}).returns(soql)
      @salesforce_integration.unstub(:salesforce_query)
      assert_equal [], @salesforce_integration.salesforce_query("Contact", {})
    end
  end

  it "adds the external id as filter in SOQL query when ticket has valid external id." do
    values = {"ticket.requester.name" => "Test Account", "ticket.organization.external_id" => "0013600001oPBwZ"}
    object = "Account"

    assert_equal "Name = 'Test Account' OR Id = '0013600001oPBwZ'", @salesforce_integration.soql_condition(object, values)
  end

  it "does not add the external id as filter in SOQL querywhen ticket has invalid external id." do
    values = {"ticket.requester.name" => "Test Account", "ticket.organization.external_id" => "some-invalid-external-id"}
    object = "Account"

    assert_equal "Name = 'Test Account'", @salesforce_integration.soql_condition(object, values)
  end

  it "does not add the external id as filter in SOQL query when ticket has no external id." do
    values = {"ticket.requester.name" => "Test Account", "ticket.organization.external_id" => ""}
    object = "Account"

    assert_equal "Name = 'Test Account'", @salesforce_integration.soql_condition(object, values)
  end

  describe "#describe_sobject" do
    before do
      @salesforce_integration.unstub(:describe_sobject)

      @salesforce_integration.
        stubs(:http_get).
        with("/services/data/v48.0/sobjects/Success/describe").
        returns(Faraday::Response.new(status: 200, body: contact.to_json))
      @salesforce_integration.
        stubs(:http_get).
        with("/services/data/v48.0/sobjects/Failure/describe").
        returns(Faraday::Response.new(status: 500, body: {}.to_json))
      @salesforce_integration.
        stubs(:http_get).
        with("/services/data/v48.0/sobjects/Nil/describe").
        returns(Faraday::Response.new(status: 200, body: nil))
    end

    after do
      @salesforce_integration.unstub(:http_get)

      @salesforce_integration.stubs(:describe_sobject).with("Contact").returns(contact)
      @salesforce_integration.stubs(:describe_sobject).with("Case").returns(sf_case)
      @salesforce_integration.stubs(:describe_sobject).with("Account").returns(account)
    end

    it "should return the body of the query result when the HTTP call is successful" do
      assert_equal contact.to_json, @salesforce_integration.describe_sobject("Success").to_json
    end

    it "should throw an exception when the HTTP call fails" do
      error_message = "Salesforce Request Failed (Account: test): SObject Describe request failed for object test"
      assert_raises Faraday::Error::ClientError, error_message do
        @salesforce_integration.describe_sobject("Failure")
      end
    end

    it "should throw an exception when the call return an nil body" do
      error_message = "Salesforce Request Failed (Account: test): SObject Describe request gave an empty response for object test"
      assert_raises Faraday::Error::ClientError, error_message do
        @salesforce_integration.describe_sobject("Nil")
      end
    end
  end
end
