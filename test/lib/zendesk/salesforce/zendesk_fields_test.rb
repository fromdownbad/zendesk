require_relative "../../../support/test_helper"
require 'zendesk/salesforce/zendesk_fields'

SingleCov.covered!

describe Zendesk::Salesforce::ZendeskFields do
  fixtures :accounts, :ticket_fields

  let(:account) { accounts(:minimum) }
  let(:supported_fields) { ticket_fields(:field_tagger_custom, :field_text_custom, :field_decimal_custom, :field_regexp_custom) }

  before do
    @zendesk_fields = Object.new
    @zendesk_fields.extend(Zendesk::Salesforce::ZendeskFields)
    @zendesk_fields.stubs(:current_account).returns(account)
  end

  describe "#mappable_zendesk_fields" do
    it "returns field options" do
      assert_not_nil @zendesk_fields.send(:mappable_zendesk_fields)
    end
  end

  describe "#field_options" do
    before do
      account.stubs(:has_multibrand?).returns(true)
      account.stubs(:where).with(ticket_fields).returns(supported_fields)

      @field_options = @zendesk_fields.send(:field_options)
    end

    it "returns custom field options" do
      supported_fields.map do |ticket_field|
        assert_includes @field_options, label: ticket_field.title, key: "ticket.ticket_field_#{ticket_field.id}"
      end
    end

    describe "current account has multibrand" do
      it "includes ticket.brand.name in return" do
        assert_includes @field_options, label: "Ticket brand", key: "ticket.brand.name"
      end
    end
  end

  describe "#mapped_zendesk_field" do
    describe "valid option" do
      it "returns mapped zendesk field" do
        assert_not_nil @zendesk_fields.send(:mapped_zendesk_field, Zendesk::Salesforce::ZendeskFields::OPTIONS[0])
      end
    end

    describe "invalid option" do
      it "returns nil" do
        assert_nil @zendesk_fields.send(:mapped_zendesk_field, SecureRandom.hex)
      end
    end
  end
end
