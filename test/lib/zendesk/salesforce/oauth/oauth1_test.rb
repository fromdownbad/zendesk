require_relative "../../../../support/test_helper"
require 'zendesk/salesforce/oauth/connection_handler'

SingleCov.covered! uncovered: 2

describe Salesforce::Oauth::Oauth1 do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @token = "token"
    @secret = "secret"
    @config = Zendesk::Configuration.fetch(:salesforce)

    @params = {}
    @session = {}
    @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
      account: @account,
      params: @params,
      session: @session
    )
    @oauth1 = Salesforce::Oauth::Oauth1.new(@connection_handler)
  end

  describe "#authorize_url" do
    before do
      @authorize_url = "http://www.authorize.com"
      @request_token = stub(token: @token, secret: @secret, authorize_url: @authorize_url)
      OAuth::Consumer.any_instance.expects(:get_request_token).returns(@request_token)
    end

    it "uses the request token to get the authorize_url and add token and secret to session" do
      assert_equal @authorize_url, @oauth1.authorize_url
      assert_equal @token, @session[:request_token]
      assert_equal @secret, @session[:request_token_secret]
    end
  end

  describe "#consumer" do
    it "uses the production options when the integration is not a sandbox" do
      SalesforceIntegration.any_instance.stubs(:is_sandbox?).returns(false)
      consumer = @oauth1.send(:consumer)
      assert_equal "https://login.salesforce.com", consumer.options[:site]
    end

    it "uses the sandbox options when the integration is a sandbox" do
      SalesforceIntegration.any_instance.stubs(:is_sandbox?).returns(true)
      consumer = @oauth1.send(:consumer)
      assert_equal "https://test.salesforce.com", consumer.options[:site]
    end
  end

  describe "#callback" do
    before do
      @verifier = "verifier"

      @params = { oauth_verifier: @verifier }
      @session = { request_token: @token, request_token_secret: @secret }
      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        params: @params,
        session: @session
      )
      @oauth1 = Salesforce::Oauth::Oauth1.new(@connection_handler)

      @authorize_url = "http://www.authorize.com"
      @access_token = stub(token: @token, secret: @secret)
      @request_token = stub
      OAuth::RequestToken.expects(:new).with(anything, @token, @secret).returns(@request_token)
    end

    it "uses the verifier to get the access token and save the values" do
      @request_token.expects(:get_access_token).with(oauth_verifier: @verifier).returns(@access_token)
      SalesforceIntegration.any_instance.expects(:update_attributes).with(
        token: @token,
        secret: @secret,
        encrypted_token: @token,
        encrypted_secret_value: @secret
      )

      @oauth1.callback
      assert_nil @session[:request_token]
      assert_nil @session[:request_token_secret]
    end

    it "handles errors" do
      @request_token.expects(:get_access_token).raises(OAuth::Unauthorized)

      assert_raise Salesforce::Integration::LoginFailed do
        @oauth1.callback
      end
    end
  end

  describe "#http_get" do
    before do
      @instance_url = "http://na2.salesforce.com"
      @session_id = "1234"
      @oauth1.stubs(:instance_url).returns(@instance_url)
      @oauth1.stubs(:session_info).returns(session_id: @session_id)
      stub_request(:get, "http://na2.salesforce.com/some/path?q=SELECT%20*%20FROM%20Contacts").
        with(headers: {'Authorization' => 'OAuth 1234'}).
        to_return(status: 200, body: "response", headers: {})
    end

    it "builds the correct url and send the request using the session_id in the authorization header" do
      response = @oauth1.http_get("/some/path", q: "SELECT * FROM Contacts")
      assert_equal 200, response.status
      assert_equal "response", response.body
    end
  end

  describe "#http_post" do
    before do
      @instance_url = "http://na2.salesforce.com"
      @session_id = "1234"
      @oauth1.stubs(:instance_url).returns(@instance_url)
      @oauth1.stubs(:session_info).returns(session_id: @session_id)
      stub_request(:post, "http://na2.salesforce.com/some/path").
        with(headers: { 'Authorization' => 'OAuth 1234', 'Content-Type' => 'application/json' }).
        with(body: "BODY").
        to_return(status: 200, body: "response", headers: {})
    end

    it "sends the correct request using the session_id in the authorization header" do
      options = {
        headers: { 'Content-Type' => 'application/json' },
        body: "BODY"
      }

      response = @oauth1.http_post("/some/path", options)
      assert_equal 200, response.status
      assert_equal "response", response.body
    end
  end

  describe "#session_info" do
    before do
      @salesforce_integration = SalesforceIntegration.new(
        account: @account,
        encrypted_token: @token,
        encrypted_secret_value: @secret
      )
      @salesforce_integration.save

      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        salesforce_integration: @salesforce_integration
      )
      @oauth1 = Salesforce::Oauth::Oauth1.new(@connection_handler)

      @access_token = stub
      OAuth::AccessToken.expects(:new).with(anything, @token, @secret).returns(@access_token)
      @response = stub(code: "200", body: "<response><sessionId>foo</sessionId><serverUrl>http://salesforce.com</serverUrl></response>")
      @session_info = { session_id: "foo", server_url: "http://salesforce.com" }
    end

    it "builds the access_token and get the session info using the production instance" do
      @salesforce_integration.stubs(is_sandbox?: false)
      @access_token.expects(:post).with(@config['login_url'], anything, anything).returns(@response)
      assert_equal @session_info, @oauth1.session_info
    end

    it "builds the access_token and get the session info using the sandbox instance" do
      @salesforce_integration.stubs(is_sandbox?: true)
      @access_token.expects(:post).with(@config['login_url_sandbox'], anything, anything).returns(@response)
      assert_equal @session_info, @oauth1.session_info
    end

    it "raises on non-200 responses" do
      @access_token.expects(:post).returns(stub(code: "401", body: "foo"))
      assert_raise Salesforce::Integration::LoginFailed do
        @oauth1.session_info
      end
    end

    it "raises if required XML isn't returned" do
      @access_token.expects(:post).returns(stub(code: "200", body: "<response><foo>bar</foo></response"))
      assert_raise Salesforce::Integration::LoginFailed do
        @oauth1.session_info
      end
    end
  end
end
