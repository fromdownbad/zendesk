require_relative "../../../../support/test_helper"
require 'zendesk/salesforce/oauth/connection_handler'

SingleCov.covered! uncovered: 12

describe 'ConnectionHandler' do
  fixtures :accounts

  before do
    @account = accounts(:minimum)

    @params = { salesforce_environment: "production" }
    @session = {}
    @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
      account: @account,
      params: @params,
      session: @session
    )
  end

  describe "#authorize_url" do
    before do
      @authorize_url = "http://www.authorize.com"
      @oauth = stub(authorize_url: @authorize_url)
      @connection_handler.stubs(:oauth).returns(@oauth)
    end

    it "sets the environment in the session and get the authorize_url" do
      assert_equal @authorize_url, @connection_handler.authorize_url
      assert_equal "production", @session[:salesforce_environment]
    end
  end

  describe "#callback" do
    before do
      @oauth = stub
      @connection_handler.stubs(:oauth).returns(@oauth)
    end

    it "delegates the callback and do the proper actions in the integration" do
      @oauth.expects(:callback)
      SalesforceIntegration.any_instance.expects(:destroy_other_integrations)
      SalesforceIntegration.any_instance.expects(:create_target)

      @connection_handler.callback
    end
  end

  describe "#oauth" do
    it "returns oauth1 if the integration is not configured with oauth2" do
      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        salesforce_integration: stub(configured_with_oauth2?: false)
      )

      assert_kind_of Salesforce::Oauth::Oauth1, @connection_handler.send(:oauth)
    end

    it "returns oauth2 if we're creating the handler using an integration with oauth2 tokens configured" do
      @access_token = "token"
      @refresh_token = "refresh_token"
      @instance_url = "https://na2.salesforce.com"
      @salesforce_integration = SalesforceIntegration.new(access_token: @access_token, refresh_token: @refresh_token, instance_url: @instance_url, is_sandbox: false)

      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        salesforce_integration: @salesforce_integration
      )

      assert_kind_of Salesforce::Oauth::Oauth2, @connection_handler.send(:oauth)
    end

    it "returns oauth2 if we're creating the handler without an integration" do
      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        params: {},
        session: {}
      )

      assert_kind_of Salesforce::Oauth::Oauth2, @connection_handler.send(:oauth)
    end
  end

  describe "#salesforce_integration" do
    it "returns the one specified in the constructor" do
      @salesforce_integration = SalesforceIntegration.new

      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        salesforce_integration: @salesforce_integration
      )

      assert_equal @salesforce_integration, @connection_handler.send(:salesforce_integration)
    end

    it "creates a new production integration if that's the env defined in the session" do
      @session = { salesforce_environment: "production" }
      @params = {}
      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        params: @params,
        session: @session
      )

      refute @connection_handler.send(:salesforce_integration).is_sandbox?
    end

    it "creates a new sandbox integration if that's the env defined in the session" do
      @session = { salesforce_environment: "sandbox" }
      @params = {}
      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        params: @params,
        session: @session
      )

      assert @connection_handler.send(:salesforce_integration).is_sandbox?
    end
  end
end
