require_relative "../../../../support/test_helper"
require 'zendesk/salesforce/oauth/connection_handler'

SingleCov.covered! uncovered: 3

describe Salesforce::Oauth::Oauth2 do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @access_token = "token"
    @refresh_token = "refresh_token"
    @instance_url = "https://na2.salesforce.com"

    @params = {}
    @session = {}
    @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
      account: @account,
      params: @params,
      session: @session
    )
    @oauth2 = Salesforce::Oauth::Oauth2.new(@connection_handler)
  end

  describe "#authorize_url" do
    before do
      @authorize_url = "http://www.authorize.com"
      @redirect_uri = "https://support.zendesk-test.com/ping/redirect_to_account"
      @account.settings.prefer_lotus = true
      state = "#{@account.subdomain}:agent?redirect=salesforce_auth"
      params = { redirect_uri: @redirect_uri, state: state }
      OAuth2::Strategy::AuthCode.any_instance.expects(:authorize_url).with(params).returns(@authorize_url)
    end

    it "uses the request token to get the authorize_url and add token and secret to session" do
      assert_equal @authorize_url, @oauth2.authorize_url
    end
  end

  describe "#client" do
    before do
      @config = Zendesk::Configuration.fetch(:salesforce)
    end

    it "uses the production options when the integration is not a sandbox" do
      SalesforceIntegration.any_instance.stubs(:is_sandbox?).returns(false)
      client = @oauth2.send(:client)
      assert_equal "https://login.salesforce.com", client.site
    end

    it "uses the sandbox options when the integration is a sandbox" do
      SalesforceIntegration.any_instance.stubs(:is_sandbox?).returns(true)
      client = @oauth2.send(:client)
      assert_equal "https://test.salesforce.com", client.site
    end
  end

  describe "#callback" do
    before do
      @code = "code"

      @params = { code: @code }
      @session = {}
      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        params: @params,
        session: @session
      )
      @oauth2 = Salesforce::Oauth::Oauth2.new(@connection_handler)
    end

    it "uses the code to get the access token and save the values" do
      @token = stub(token: @access_token, refresh_token: @refresh_token, params: { 'instance_url' => @instance_url })
      OAuth2::Strategy::AuthCode.any_instance.expects(:get_token).with(@code, anything).returns(@token)
      SalesforceIntegration.any_instance.expects(:update_attributes).with(
        access_token: @access_token,
        refresh_token: @refresh_token,
        encrypted_access_token: @access_token,
        encrypted_refresh_token: @refresh_token,
        instance_url: @instance_url
      )

      @oauth2.callback
    end

    it "handles errors" do
      @token = stub(token: "", refresh_token: "", params: { 'instance_url' => "" })
      OAuth2::Strategy::AuthCode.any_instance.expects(:get_token).with(@code, anything).returns(@token)

      assert_raise Salesforce::Integration::LoginFailed do
        @oauth2.callback
      end
    end
  end

  describe "#http_get" do
    before do
      @salesforce_integration = SalesforceIntegration.new(
        account: @account,
        instance_url: @instance_url,
        encrypted_access_token: @access_token,
        encrypted_refresh_token: @refresh_token,
        is_sandbox: false
      )
      @salesforce_integration.save

      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        salesforce_integration: @salesforce_integration
      )
      @oauth2 = Salesforce::Oauth::Oauth2.new(@connection_handler)

      @oauth2.stubs(:instance_url).returns(@instance_url)
    end

    describe "when request works as expected" do
      before do
        stub_request(:get, "#{@instance_url}/some/path?q=SELECT%20*%20FROM%20Contacts").
          with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization' => "OAuth #{@access_token}", 'User-Agent' => 'Faraday v0.11.0'}).
          to_return(status: 200, body: "response", headers: {})
      end

      it "builds the correct url and send the request using the token in the authorization header" do
        response = @oauth2.http_get("/some/path", q: "SELECT * FROM Contacts")
        assert_equal 200, response.status
        assert_equal "response", response.body
      end
    end

    describe "when authentication fails" do
      before do
        stub_request(:get, "#{@instance_url}/some/path?q=SELECT%20*%20FROM%20Contacts").
          with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization' => "OAuth #{@access_token}", 'User-Agent' => 'Faraday v0.11.0'}).
          to_return(status: 401, body: "response", headers: {})
        Salesforce::Oauth::Oauth2::ForceToken.any_instance.expects(:update_token).once
      end

      it "refreshs the token and raise an exception if that doesn't work" do
        assert_raise Salesforce::Integration::LoginFailed do
          @oauth2.http_get("/some/path", q: "SELECT * FROM Contacts")
        end
      end
    end

    describe "when there's an error not related to authentication" do
      before do
        @body = '[{"message":"The requested resource does not exist","errorCode":"NOT_FOUND"}]'
        stub_request(:get, "#{@instance_url}/some/path?q=SELECT%20*%20FROM%20Contacts").
          with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization' => "OAuth #{@access_token}", 'User-Agent' => 'Faraday v0.11.0'}).
          to_return(status: 403, body: @body, headers: {})
        Salesforce::Oauth::Oauth2::ForceToken.any_instance.expects(:update_token).never
      end

      it "does not refresh the token and not raise an exception" do
        response = @oauth2.http_get("/some/path", q: "SELECT * FROM Contacts")
        assert_equal 403, response.status
      end
    end
  end

  describe "#http_post" do
    before do
      @salesforce_integration = SalesforceIntegration.new(
        account: @account,
        instance_url: @instance_url,
        encrypted_access_token: @access_token,
        encrypted_refresh_token: @refresh_token,
        is_sandbox: false
      )
      @salesforce_integration.save

      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        salesforce_integration: @salesforce_integration
      )
      @oauth2 = Salesforce::Oauth::Oauth2.new(@connection_handler)

      @oauth2.stubs(:instance_url).returns(@instance_url)

      @opts = {
        headers: { 'Content-Type' => 'application/json' },
        body: "BODY"
      }
    end

    describe "when request works as expected" do
      before do
        stub_request(:post, "#{@instance_url}/some/path").
          with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization' => "OAuth #{@access_token}", 'User-Agent' => 'Faraday v0.11.0'}).
          with(body: "BODY").
          to_return(status: 200, body: "response", headers: {})
      end

      it "builds the correct url and send the request using the token in the authorization header" do
        response = @oauth2.http_post("/some/path", @opts)
        assert_equal 200, response.status
        assert_equal "response", response.body
      end
    end

    describe "when authentication fails" do
      before do
        stub_request(:post, "#{@instance_url}/some/path").
          with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization' => "OAuth #{@access_token}", 'User-Agent' => 'Faraday v0.11.0'}).
          with(body: "BODY").
          to_return(status: 401, body: "response", headers: {})
        Salesforce::Oauth::Oauth2::ForceToken.any_instance.expects(:update_token).once
      end

      it "refreshs the token and raise an exception if that doesn't work" do
        assert_raise Salesforce::Integration::LoginFailed do
          @oauth2.http_post("/some/path", @opts)
        end
      end
    end

    describe "when there's an error not related to authentication" do
      before do
        @body = '[{"message":"The requested resource does not exist","errorCode":"NOT_FOUND"}]'
        stub_request(:post, "#{@instance_url}/some/path").
          with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization' => "OAuth #{@access_token}", 'User-Agent' => 'Faraday v0.11.0'}).
          with(body: "BODY").
          to_return(status: 403, body: "response", headers: {})
        Salesforce::Oauth::Oauth2::ForceToken.any_instance.expects(:update_token).never
      end

      it "does not refresh the token and not raise an exception" do
        response = @oauth2.http_post("/some/path", @opts)
        assert_equal 403, response.status
      end
    end
  end

  describe "#session_info" do
    before do
      @salesforce_integration = SalesforceIntegration.new(
        account: @account,
        instance_url: @instance_url,
        encrypted_access_token: @access_token,
        encrypted_refresh_token: @refresh_token,
        is_sandbox: false
      )
      @salesforce_integration.save

      @connection_handler = Salesforce::Oauth::ConnectionHandler.new(
        account: @account,
        salesforce_integration: @salesforce_integration
      )
      @oauth2 = Salesforce::Oauth::Oauth2.new(@connection_handler)
    end

    it "returns the hash with the access_token and the instance_url" do
      session_info = { session_id: @access_token, server_url: @instance_url }
      assert_equal session_info, @oauth2.session_info
    end
  end
end
