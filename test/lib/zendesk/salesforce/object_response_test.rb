require_relative "../../../support/test_helper"
require 'zendesk/salesforce/object_response'
require 'zendesk/salesforce/configuration'
require 'salesforce_integration'

SingleCov.covered! uncovered: 3

describe Salesforce::ObjectResponse do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @configuration = Salesforce::Configuration.new(@account)
  end

  describe 'parse_record' do
    before do
      @contact_name = 'John Doe'
      @contact_email = 'john.doe@test.com'
      @account_name = 'Acme Inc'
      @account_parent_id = '0013600001KUgxxxA1'
      @test_contact_record = {
          'attributes' => {
              'type' => 'Contact',
              'url' => '/services/data/v23.0/sobjects/Contact/0033600001FyoDXAAZ'
          },
          'Email' => @contact_email,
          'Name' => @contact_name,
          'Account' => {
              'attributes' => {
                  'type' => 'Account',
                  'url' => '/services/data/v23.0/sobjects/Account/0013600001KUgABAA1'
              },
              'Name' => @account_name,
              'ParentId' => @account_parent_id,
          },
          'Id' => '0033600001FyoDXAAZ'
      }
    end
    it "should parse parent and related record fields for default mapping" do
      response = Salesforce::ObjectResponse.new('Contact', {"records" => [@test_contact_record]}, base_url: 'https://test.sfdc.com', configuration: @configuration)
      records = response.objects
      assert_equal(records.length, 2)
      contact = records[0]
      assert_equal(contact['record_type'], 'contact')
      assert_equal(contact['label'], @contact_name)
      assert_equal(contact['fields'][0]['label'], 'Email')
      assert_equal(contact['fields'][0]['value'], @contact_email)

      account = records[1]
      assert_equal(account['record_type'], 'account via contact')
      assert_equal(account['label'], @account_name)
      assert_equal(account['fields'].length, 0)
    end

    it "should parse parent and related record fields for user defined mapping" do
      @configuration.stubs(:organized_fields).with('Contact').returns([["Name", "Email"], {"Account" => ["Name", "ParentId"]}])

      response = Salesforce::ObjectResponse.new('Contact', {"records" => [@test_contact_record]}, base_url: 'https://test.sfdc.com', configuration: @configuration)
      records = response.objects

      assert_equal(records.length, 2)
      contact = records[0]
      assert_equal(contact['record_type'], 'contact')
      assert_equal(contact['label'], @contact_name)
      assert_equal(contact['fields'][0]['label'], 'Email')
      assert_equal(contact['fields'][0]['value'], @contact_email)

      account = records[1]
      assert_equal(account['record_type'], 'account via contact')
      assert_equal(account['label'], @account_name)
      assert_equal(account['fields'][0]['label'], 'ParentId')
      assert_equal(account['fields'][0]['value'], @account_parent_id)
    end
  end

  describe 'handle_errors' do
    let(:response) do
      Salesforce::ObjectResponse.new(
        'Contact',
        [{"errorCode" => true, "message" => error_message}],
        base_url: 'https://test.sfdc.com',
        configuration: @configuration
      )
    end

    describe "with RuntimeError as message" do
      let(:error_message) { 'RuntimeError' }

      it "raises a Salesforce::Integration::RuntimeError" do
        assert_raise Salesforce::Integration::RuntimeError do
          response
        end
      end
    end

    describe "with something else as error message" do
      let(:error_message) { 'SalesforceErrorMessage' }

      it "raises RuntimeError with whatever message is passed in" do
        err = assert_raise RuntimeError do
          response
        end
        assert_equal error_message, err.message
      end
    end

    describe 'given an invalid OAuth2 JSON response' do
      let(:response) do
        Salesforce::ObjectResponse.new(
          'Contact',
          OAuth2::Response.new(Faraday::Response.new(body: "This is the body")),
          base_url: 'https://test.sfdc.com',
          configuration: @configuration
        )
      end

      it "raises a Salesforce::Integration::RuntimeError" do
        assert_raise Salesforce::Integration::RuntimeError do
          response
        end
      end
    end
  end
end
