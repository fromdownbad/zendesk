require_relative '../../support/test_helper'
require_relative '../../support/entitlement_test_helper'
require_relative '../../support/suite_test_helper'

SingleCov.covered!

describe Zendesk::StaffClient do
  include EntitlementTestHelper
  include SuiteTestHelper
  include ArturoTestHelper
  fixtures :accounts

  let(:account) { accounts(:multiproduct) }
  let(:user_id) { 5 }
  let(:entitlements) { { chat: 'foo' } }
  let(:entitlements_seats_remaining) { { chat: 9, support: 9 } }
  let(:seats_remaining) { [{ seat: 'support', remaining: 9 }, { seat: 'chat', remaining: 9 }] }
  # Use high failure threshold to avoid tripping circuit breaker in tests
  let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }
  let(:staff_client) { Zendesk::StaffClient.new(account, client_options) }
  let(:account_url_base) { "http://metropolis:8888" }
  let(:roles_url) { "#{account_url_base}/api/services/products/#{product}/roles" }
  let(:product) { 'support' }
  let(:full_entitlements) do
    {
      chat: {
        'id'         => 123,
        'account_id' => 1,
        'user_id'    => user_id,
        'name'       => 'admin',
        'is_active'  => true,
        'created_at' => '2015-03-11T05:00Z',
        'updated_at' => '2015-03-11T05:00Z',
        'deleted_at' => '2015-03-11T05:00Z'
      }
    }
  end
  let(:role_patch_url) { "#{roles_url}/#{role_id}" }
  let(:role_fetch_url) { "#{roles_url}/name/#{role_name}" }

  before do
    ENV.stubs(:fetch).with("STAFF_SERVICE_URL").returns(account_url_base)
  end

  describe '#initialize' do
    it 'sets the correct default parameters for options' do
      staff_client = Zendesk::StaffClient.new(account)
      retryable_errors = [
        Faraday::ConnectionFailed,
        Faraday::TimeoutError,
        Faraday::ClientError,
        Kragle::ClientError,
        Kragle::ResponseError
      ]
      assert_equal staff_client.circuit_options, failure_threshold: 10, reset_timeout: 5
      assert_equal staff_client.connection_timeout, 2
      assert_equal staff_client.retry_options, max: 2, exceptions: retryable_errors, methods: Faraday::Request::Retry::IDEMPOTENT_METHODS + [:patch]
    end
  end

  describe '#seats_remaining_spp_light_agents!' do
    describe_with_arturo_enabled :staff_service_seats_remaining do
      let(:remaining_seats) { 5 }
      let(:seats_remaining_spp) { [{ seat: 'light_agents', remaining: remaining_seats }] }
      describe 'with spp accounts' do
        before do
          Account.any_instance.stubs(:spp?).returns(true)
        end
        it 'calls the staff service to get light-agent seats remaining' do
          request = stub_request(:get, staff_service_seats_remaining_url).
            to_return(status: 200, body: { seats_remaining: seats_remaining_spp }.to_json, headers: { "Content-Type" => "application/json" })
          response = staff_client.seats_remaining_spp_light_agents!
          assert_requested request
          assert_equal remaining_seats, response
        end
      end
    end

    describe_with_arturo_disabled :staff_service_seats_remaining do
      it 'calls the staff service to get entitlements seats remaining for support' do
        request = stub_request(:get, staff_service_entitlements_seats_remaining_url).
          to_return(status: 200, body: { seats_remaining: entitlements_seats_remaining }.to_json, headers: { "Content-Type" => "application/json" })
        response = staff_client.seats_remaining_spp_light_agents!
        assert_requested request
        assert_equal 9, response
      end
    end
  end

  describe '#seats_remaining_support_or_suite!' do
    describe_with_arturo_enabled :staff_service_seats_remaining do
      describe 'with suite active' do
        let(:seats_remaining_suite) { [{ seat: 'suite', remaining: 9 }] }

        before do
          Account.any_instance.stubs(:has_active_suite?).returns(true)
        end

        it 'calls the staff service to get seats remaining for support' do
          request = stub_request(:get, staff_service_seats_remaining_url).
            to_return(status: 200, body: { seats_remaining: seats_remaining_suite }.to_json, headers: { "Content-Type" => "application/json" })
          response = staff_client.seats_remaining_support_or_suite!
          assert_requested request
          assert_equal 9, response
        end
      end

      describe 'with suite inactive' do
        it 'calls the staff service to get seats remaining for support' do
          request = stub_request(:get, staff_service_seats_remaining_url).
            to_return(status: 200, body: { seats_remaining: seats_remaining }.to_json, headers: { "Content-Type" => "application/json" })
          response = staff_client.seats_remaining_support_or_suite!
          assert_requested request
          assert_equal 9, response
        end
      end

      describe 'with SPP suite account' do
        let(:remaining_seats) { 5 }
        let(:seats_remaining_spp) { [{ seat: 'zendesk_suite', remaining: remaining_seats }] }

        before do
          Account.any_instance.stubs(:spp_suite?).returns(true)
        end

        it 'calls the staff service to get seats remaining for support' do
          request = stub_request(:get, staff_service_seats_remaining_url).
            to_return(status: 200, body: { seats_remaining: seats_remaining_spp }.to_json, headers: { 'Content-Type' => 'application/json' })
          response = staff_client.seats_remaining_support_or_suite!
          assert_requested request
          assert_equal remaining_seats, response
        end
      end

      describe 'with SPP Support Plus account' do
        let(:remaining_seats) { 5 }
        let(:seats_remaining_spp) { [{ seat: 'support', remaining: remaining_seats }] }

        before do
          Account.any_instance.stubs(:spp_suite?).returns(false)
        end

        it 'returns seats remaining value from Support seats count' do
          request = stub_request(:get, staff_service_seats_remaining_url).
            to_return(status: 200, body: { seats_remaining: seats_remaining_spp }.to_json, headers: { 'Content-Type' => 'application/json' })
          response = staff_client.seats_remaining_support_or_suite!
          assert_requested request
          assert_equal remaining_seats, response
        end
      end

      describe 'when seat type not present in the response' do
        let(:seats_remaining) { [{}] }

        before do
          stub_request(:get, staff_service_seats_remaining_url).
            to_return(status: 200, body: { seats_remaining: seats_remaining }.to_json, headers: { 'Content-Type' => 'application/json' })
        end

        it 'returns nil' do
          response = staff_client.seats_remaining_support_or_suite!
          assert_nil response
        end

        it 'logs error' do
          Rails.logger.expects(:error).
            with(regexp_matches(/Failed to get seats remaining record for seat type 'support' on account #{account.idsub}/)).
            once
          staff_client.seats_remaining_support_or_suite!
        end
      end
    end

    describe_with_arturo_disabled :staff_service_seats_remaining do
      it 'calls the staff service to get entitlements seats remaining for support' do
        request = stub_request(:get, staff_service_entitlements_seats_remaining_url).
          to_return(status: 200, body: { seats_remaining: entitlements_seats_remaining }.to_json, headers: { "Content-Type" => "application/json" })
        response = staff_client.seats_remaining_support_or_suite!
        assert_requested request
        assert_equal 9, response
      end
    end
  end

  describe '#entitlements_seats_remaining' do
    it 'calls the staff service to get entitlements seats remaining' do
      request = stub_request(:get, staff_service_entitlements_seats_remaining_url).
        to_return(status: 200, body: { seats_remaining: entitlements_seats_remaining }.to_json, headers: { "Content-Type" => "application/json" })
      response = staff_client.entitlements_seats_remaining!
      assert_requested request
      assert_equal entitlements_seats_remaining, response.symbolize_keys
    end

    it 'raises ResponseError' do
      request = stub_request(:get, staff_service_entitlements_seats_remaining_url).to_raise(Kragle::ResponseError)
      assert_raises Kragle::ResponseError do
        staff_client.entitlements_seats_remaining!
      end
      assert_requested request, times: 3
    end

    it 'raises ClientError' do
      request = stub_request(:get, staff_service_entitlements_seats_remaining_url).to_raise(Faraday::ClientError)
      assert_raises Faraday::ClientError do
        staff_client.entitlements_seats_remaining!
      end
      assert_requested request, times: 3
    end
  end

  describe '#seats_remaining' do
    it 'calls the staff service to get seats remaining' do
      request = stub_request(:get, staff_service_seats_remaining_url).
        to_return(status: 200, body: { seats_remaining: seats_remaining }.to_json, headers: { "Content-Type" => "application/json" })
      response = staff_client.seats_remaining!
      assert_requested request
      assert_equal seats_remaining, response
    end

    it 'raises ResponseError' do
      request = stub_request(:get, staff_service_seats_remaining_url).to_raise(Kragle::ResponseError)
      assert_raises Kragle::ResponseError do
        staff_client.seats_remaining!
      end
      assert_requested request, times: 3
    end

    it 'raises ClientError' do
      request = stub_request(:get, staff_service_seats_remaining_url).to_raise(Faraday::ClientError)
      assert_raises Faraday::ClientError do
        staff_client.seats_remaining!
      end
      assert_requested request, times: 3
    end
  end

  describe '#seats_occupied' do
    let(:seats_occupied_data) do
      [
        { seat: "support", occupied: 7 },
        { seat: "sell", occupied: 2 }
      ]
    end

    it 'calls the staff service to get seats occupied' do
      request = stub_request(:get, staff_service_seats_occupied_url).to_return(
        status: 200,
        body: { seats_occupied: seats_occupied_data }.to_json,
        headers: { "Content-Type" => "application/json" }
      )
      response = staff_client.seats_occupied!

      assert_requested request
      assert_equal seats_occupied_data, response
    end

    it 'raises ResponseError' do
      request = stub_request(:get, staff_service_seats_occupied_url).to_raise(Kragle::ResponseError)

      assert_raises Kragle::ResponseError do
        staff_client.seats_occupied!
      end
      assert_requested request, times: 3
    end

    it 'raises ClientError' do
      request = stub_request(:get, staff_service_seats_occupied_url).to_raise(Faraday::ClientError)

      assert_raises Faraday::ClientError do
        staff_client.seats_occupied!
      end
      assert_requested request, times: 3
    end
  end

  describe '#seats_occupied_support_or_suite!' do
    let(:seats_occupied_data) { [] }
    let(:is_suite) { false }
    let(:is_spp_suite) { false }

    before do
      Account.any_instance.stubs(:has_active_suite?).returns(is_suite)
      Account.any_instance.stubs(:spp_suite?).returns(is_spp_suite)

      stub_request(:get, staff_service_seats_occupied_url).to_return(
        status: 200,
        body: { seats_occupied: seats_occupied_data }.to_json,
        headers: { "Content-Type" => "application/json" }
      )
    end

    describe 'not Suite and not SPP Suite account' do
      let(:is_suite) { false }
      let(:is_spp_suite) { false }
      let(:seats_occupied_data) do
        [
          { seat: 'support', occupied: 5 },
          { seat: 'sell', occupied: 2 }
        ]
      end

      it 'returns seats occupied value from Support seats count' do
        assert_equal 5, staff_client.seats_occupied_support_or_suite!
      end
    end

    describe 'is Suite account' do
      let(:is_suite) { true }
      let(:is_spp_suite) { false }
      let(:seats_occupied_data) do
        [
          { seat: 'suite', occupied: 7 },
          { seat: 'sell', occupied: 2 }
        ]
      end

      it 'returns seats occupied value from Suite seats count' do
        assert_equal 7, staff_client.seats_occupied_support_or_suite!
      end
    end

    describe 'is SPP Suite' do
      let(:is_suite) { false }
      let(:is_spp_suite) { true }
      let(:seats_occupied_data) do
        [
          { seat: 'zendesk_suite', occupied: 6 },
          { seat: 'sell', occupied: 2 }
        ]
      end

      it 'returns seats occupied value from Zendesk Suite seats count' do
        assert_equal 6, staff_client.seats_occupied_support_or_suite!
      end
    end

    describe 'is SPP Support Plus' do
      let(:seats_occupied_data) do
        [
          { seat: 'support', occupied: 3 },
          { seat: 'sell', occupied: 2 }
        ]
      end

      it 'returns seats occupied value from Support seats count' do
        assert_equal 3, staff_client.seats_occupied_support_or_suite!
      end
    end

    describe 'when seat type not present in the response' do
      let(:seats_occupied_data) { [{}] }

      it 'returns nil' do
        response = staff_client.seats_occupied_support_or_suite!
        assert_nil response
      end

      it 'logs error' do
        Rails.logger.expects(:error).
          with(regexp_matches(/Failed to get seats occupied record for seat type 'support' on account #{account.idsub}/)).
          once
        staff_client.seats_occupied_support_or_suite!
      end
    end
  end

  describe '#create_role!' do
    let(:expected_body) do
      {
        role: {
          name: role_name,
          title: title,
          description: description,
          max_agents_countable: max_agents_countable,
          is_system_role: is_system_role,
          is_active: is_active,
          assignable: true
        }
      }
    end

    let(:role_name) { 'custom_banana' }
    let(:title) { 'Banana Man' }
    let(:description) { 'I am a banana' }
    let(:max_agents_countable) { false }
    let(:is_system_role) { false }
    let(:is_active) { true }
    let(:create_role!) do
      staff_client.create_role!(
        name: role_name,
        title: title,
        description: description,
        max_agents_countable: max_agents_countable
      )
    end

    before do
      stub_request(:post, roles_url)
    end

    it 'makes the expected request to Staff Service' do
      create_role!
      assert_requested(:post, roles_url, body: expected_body)
    end

    describe 'inactive role' do
      let(:is_active) { false }
      let(:create_role!) do
        staff_client.create_role!(
          name: role_name,
          title: title,
          description: description,
          max_agents_countable: max_agents_countable,
          is_active: is_active
        )
      end

      it 'makes the expected request to Staff Service' do
        create_role!
        assert_requested(:post, roles_url, body: expected_body)
      end
    end

    describe 'system role' do
      let(:is_system_role) { true }
      let(:create_role!) do
        staff_client.create_role!(
          name: role_name,
          title: title,
          description: description,
          max_agents_countable: max_agents_countable,
          is_system_role: is_system_role
        )
      end

      it 'makes the expected request to Staff Service' do
        create_role!
        assert_requested(:post, roles_url, body: expected_body)
      end
    end

    describe 'Guide role' do
      let(:product) { 'guide' }
      let(:create_role!) do
        staff_client.create_role!(
          name: role_name,
          title: title,
          description: description,
          max_agents_countable: max_agents_countable,
          product: product
        )
      end

      it 'makes the expected request to Staff Service' do
        create_role!
        assert_requested(:post, roles_url, body: expected_body)
      end
    end
  end

  describe '#toggle_role!' do
    let(:expected_body) { { role: { assignable: assignable } } }

    let(:role_response) do
      {
        role: {
          id: role_id,
          account_id: 1,
          product_id: 1,
          name: role_name,
          title: title,
          description: description,
          max_agents_countable: max_agents_countable,
          is_active: true,
          assignable: true,
          deleted_at: "2015-03-13T05:00Z",
          created_at: "2015-03-11T05:00Z",
          updated_at: "2015-03-12T05:00Z"
        }
      }.to_json
    end

    let(:role_id) { '123' }
    let(:role_name) { 'custom_banana' }
    let(:title) { 'Banana Man' }
    let(:description) { 'I am a banana' }
    let(:max_agents_countable) { false }
    let(:assignable) { true }
    let(:toggle_role!) { staff_client.toggle_role!(name: role_name, assignable: assignable) }

    before do
      stub_request(:patch, role_patch_url)
      stub_request(:get, role_fetch_url).to_return(body: role_response, headers: { content_type: 'application/json; charset=utf-8' })
    end

    it 'fetches the role from pravda' do
      toggle_role!
      assert_requested(:get, role_fetch_url)
    end

    it 'makes the expected request to pravda' do
      toggle_role!
      assert_requested(:patch, role_patch_url, body: expected_body)
    end

    describe 'with another product' do
      let(:product) { 'guide' }
      let(:toggle_role!) { staff_client.toggle_role!(name: role_name, assignable: assignable, product: product) }

      it 'fetches the role from pravda' do
        toggle_role!
        assert_requested(:get, role_fetch_url)
      end

      it 'makes the expected request to pravda' do
        toggle_role!
        assert_requested(:patch, role_patch_url, body: expected_body)
      end
    end
  end

  describe '#update_role!' do
    let(:expected_body) do
      {
        role: {
          name: role_name,
          title: title,
          description: description,
          max_agents_countable: max_agents_countable,
          is_active: is_active,
          assignable: true
        }
      }
    end

    let(:role_response) do
      {
        role: {
          id: role_id,
          account_id: 1,
          product_id: 1,
          name: role_name,
          title: title,
          description: description,
          max_agents_countable: max_agents_countable,
          is_active: is_active,
          assignable: true,
          deleted_at: "2015-03-13T05:00Z",
          created_at: "2015-03-11T05:00Z",
          updated_at: "2015-03-12T05:00Z"
        }
      }.to_json
    end

    let(:role_id) { '123' }
    let(:role_name) { 'custom_banana' }
    let(:title) { 'Banana Man' }
    let(:description) { 'I am a banana' }
    let(:max_agents_countable) { false }
    let(:is_active) { true }
    let(:update_role!) do
      staff_client.update_role!(
        name: role_name,
        title: title,
        description: description,
        max_agents_countable: max_agents_countable,
        is_active: is_active
      )
    end

    before do
      stub_request(:patch, role_patch_url)
      stub_request(:get, role_fetch_url).to_return(body: role_response, headers: { content_type: 'application/json; charset=utf-8' })
    end

    it 'fetches the role from pravda' do
      update_role!
      assert_requested(:get, role_fetch_url)
    end

    it 'makes the expected request to pravda' do
      update_role!
      assert_requested(:patch, role_patch_url, body: expected_body)
    end

    describe 'deactivate role' do
      let(:is_active) { false }

      it 'makes the expected request to pravda' do
        update_role!
        assert_requested(:patch, role_patch_url, body: expected_body)
      end
    end

    describe 'nil values' do
      let(:title) { nil }
      let(:expected_body) do
        {
          role: {
            name: role_name,
            description: description,
            max_agents_countable: max_agents_countable,
            is_active: is_active,
            assignable: true
          }
        }
      end

      it 'does not update those values' do
        update_role!
        assert_requested(:patch, role_patch_url, body: expected_body)
      end
    end
  end

  describe '#delete_role!' do
    let(:role_response) do
      {
        role: {
          id: role_id,
          account_id: 1,
          product_id: 1,
          name: role_name,
          title: 'Banana Man',
          description: 'I am a banana',
          max_agents_countable: false,
          is_active: true,
          assignable: true,
          deleted_at: "2015-03-13T05:00Z",
          created_at: "2015-03-11T05:00Z",
          updated_at: "2015-03-12T05:00Z"
        }
      }.to_json
    end

    let(:role_id) { '123' }
    let(:role_name) { 'custom_banana' }
    let(:delete_role!) { staff_client.delete_role!(name: role_name) }

    before do
      stub_request(:delete, role_patch_url)
      stub_request(:get, role_fetch_url).to_return(body: role_response, headers: { content_type: 'application/json; charset=utf-8' })
    end

    it 'fetches the role from pravda' do
      delete_role!
      assert_requested(:get, role_fetch_url)
    end

    it 'makes the expected request to pravda' do
      delete_role!
      assert_requested(:delete, role_patch_url)
    end
  end

  describe '#update_or_create_role!' do
    let(:title) { 'role title' }
    let(:description) { 'role description' }
    let(:role_name) { 'role name' }
    let(:max_agents_countable) { false }
    let(:context) { 'updated in test' }
    let(:params) do
      {
        title: title,
        description: description,
        name: role_name,
        max_agents_countable: max_agents_countable,
        assignable: true,
      }
    end

    it 'updates the role' do
      staff_client.expects(:update_role!).with(params.merge(is_active: true))
      staff_client.expects(:create_role!).never
      staff_client.update_or_create_role!(params.merge(context: context))
    end

    it 'creates the role if it did not already exist' do
      staff_client.stubs(:update_role!).raises Kragle::ResourceNotFound
      staff_client.expects(:create_role!).with(params.merge(context: context, is_active: true))
      staff_client.update_or_create_role!(params.merge(context: context))
    end

    describe 'deactivate role' do
      let(:is_active) { false }
      let(:deactivate_params) { params.merge(is_active: is_active) }

      it 'updates the role' do
        staff_client.expects(:update_role!).with(deactivate_params)
        staff_client.expects(:create_role!).never
        staff_client.update_or_create_role!(deactivate_params.merge(context: context))
      end

      it 'creates the role if it did not already exist' do
        staff_client.stubs(:update_role!).raises Kragle::ResourceNotFound
        staff_client.expects(:create_role!).with(deactivate_params.merge(context: context))
        staff_client.update_or_create_role!(deactivate_params.merge(context: context))
      end
    end
  end

  describe '#get_entitlements!' do
    describe 'when is_end_user is true' do
      it 'calls the staff service to get entitlements with URL param is_end_user' do
        request = stub_request(:get, staff_service_entitlements_url(account.subdomain, true)).
          to_return(status: 200, body: { entitlements: entitlements }.to_json, headers: { "Content-Type" => "application/json" })
        response = staff_client.get_entitlements!(user_id, true)
        assert_requested request
        assert_equal entitlements, response.symbolize_keys
      end
    end

    it 'calls the staff service to get entitlements' do
      request = stub_request(:get, staff_service_entitlements_url(account.subdomain)).
        to_return(status: 200, body: { entitlements: entitlements }.to_json, headers: { "Content-Type" => "application/json" })
      response = staff_client.get_entitlements!(user_id)
      assert_requested request
      assert_equal entitlements, response.symbolize_keys
    end

    it 'raises ResponseError' do
      request = stub_request(:get, staff_service_entitlements_url(account.subdomain)).to_raise(Kragle::ResponseError)
      assert_raises Kragle::ResponseError do
        staff_client.get_entitlements!(user_id)
      end
      assert_requested request, times: 3
    end

    it 'raises ClientError' do
      request = stub_request(:get, staff_service_entitlements_url(account.subdomain)).to_raise(Faraday::ClientError)
      assert_raises Faraday::ClientError do
        staff_client.get_entitlements!(user_id)
      end
      assert_requested request, times: 3
    end
  end

  describe '#admin?' do
    it 'returns true if the staff member is is an admin for any product' do
      stub_request(:get, staff_service_entitlements_url(account.subdomain)).
        to_return(status: 200, body: { entitlements: { chat: nil, support: 'admin' } }.to_json, headers: { "Content-Type" => "application/json" })
      assert staff_client.admin?(user_id)
    end

    it 'returns false if the staff member is not an admin for any product' do
      stub_request(:get, staff_service_entitlements_url(account.subdomain)).
        to_return(status: 200, body: { entitlements: entitlements }.to_json, headers: { "Content-Type" => "application/json" })
      refute staff_client.admin?(user_id)
    end
  end

  describe '#get_full_entitlements!' do
    it 'calls the staff service to get full entitlements' do
      request = stub_request(:get, staff_service_get_full_entitlements_url(account.subdomain)).
        to_return(status: 200, body: { entitlements: full_entitlements }.to_json, headers: { "Content-Type" => "application/json" })
      response = staff_client.get_full_entitlements!(user_id)
      assert_requested request
      assert_equal full_entitlements, response.symbolize_keys
    end

    it 'raises ResponseError' do
      request = stub_request(:get, staff_service_get_full_entitlements_url(account.subdomain)).to_raise(Kragle::ResponseError)
      assert_raises Kragle::ResponseError do
        staff_client.get_full_entitlements!(user_id)
      end
      assert_requested request, times: 3
    end

    it 'raises ClientError' do
      request = stub_request(:get, staff_service_get_full_entitlements_url(account.subdomain)).to_raise(Faraday::ClientError)
      assert_raises Faraday::ClientError do
        staff_client.get_full_entitlements!(user_id)
      end
      assert_requested request, times: 3
    end
  end

  describe '#update_entitlements!' do
    before { Zendesk::StaffClient.any_instance.unstub(:update_entitlements!) }

    it 'calls the staff service to update entitlements and returns the entitlements' do
      request = stub_staff_service_patch_request(account.subdomain, 200)
      response = staff_client.update_entitlements!(user_id, entitlements)
      assert_requested request
      assert_equal entitlements, response.symbolize_keys
    end

    it 'raises ResponseError' do
      request = stub_request(:patch, staff_service_entitlements_url(account.subdomain)).to_raise(Kragle::ResponseError)
      assert_raises Kragle::ResponseError do
        staff_client.update_entitlements!(user_id, entitlements)
      end
      assert_requested request, times: 3
    end

    it 'raises ClientError' do
      request = stub_request(:patch, staff_service_entitlements_url(account.subdomain)).to_raise(Faraday::ClientError)
      assert_raises Faraday::ClientError do
        staff_client.update_entitlements!(user_id, entitlements)
      end
      assert_requested request, times: 3
    end

    describe 'when is_end_user is true' do
      it 'calls the staff service with is_end_user query param and returns the entitlements' do
        request = stub_staff_service_patch_request(account.subdomain, 200, true)
        response = staff_client.update_entitlements!(user_id, entitlements, true)
        assert_requested request
        assert_equal entitlements, response.symbolize_keys
      end
    end
  end

  describe '#update_full_entitlements!' do
    before { Zendesk::StaffClient.any_instance.unstub(:update_full_entitlements!) }

    it 'calls the staff service to update entitlements and returns the entitlements' do
      request = stub_staff_service_full_entitlements_patch_request(account.subdomain, 200)
      response = staff_client.update_full_entitlements!(user_id, entitlements)
      assert_requested request
      assert_equal entitlements, response.symbolize_keys
    end

    it 'skips validation if option is passed' do
      request = stub_staff_service_full_entitlements_patch_request(account.subdomain, 200, skip_validation: true)
      response = staff_client.update_full_entitlements!(user_id, entitlements, skip_validation: true)
      assert_requested request
      assert_equal entitlements, response.symbolize_keys
    end

    it 'update end user entitlement if option is passed' do
      request = stub_staff_service_full_entitlements_patch_request(account.subdomain, 200, skip_validation: false, is_end_user: true)
      response = staff_client.update_full_entitlements!(user_id, entitlements, is_end_user: true)
      assert_requested request
      assert_equal entitlements, response.symbolize_keys
    end

    it 'raises ResponseError' do
      request = stub_request(:patch, staff_service_patch_full_entitlements_url(account.subdomain)).to_raise(Kragle::ResponseError)
      assert_raises Kragle::ResponseError do
        staff_client.update_full_entitlements!(user_id, entitlements)
      end
      assert_requested request, times: 3
    end

    it 'raises ClientError' do
      request = stub_request(:patch, staff_service_patch_full_entitlements_url(account.subdomain)).to_raise(Faraday::ClientError)
      assert_raises Faraday::ClientError do
        staff_client.update_full_entitlements!(user_id, entitlements)
      end
      assert_requested request, times: 3
    end
  end

  describe '#update_entitlements' do
    before { Zendesk::StaffClient.any_instance.unstub(:update_entitlements!) }

    it 'calls the staff service to update entitlements and returns the entitlements' do
      request = stub_staff_service_patch_request(account.subdomain, 200)
      response = staff_client.update_entitlements(user_id, entitlements)
      assert_requested request
      assert_equal entitlements, response.symbolize_keys
    end

    it 'catches ResponseError and returns nil' do
      request = stub_request(:patch, staff_service_entitlements_url(account.subdomain)).to_raise(Kragle::ResponseError)
      response = staff_client.update_entitlements(user_id, entitlements)
      assert_requested request, times: 3
      assert_nil response
    end

    it 'catches ClientError and returns nil' do
      request = stub_request(:patch, staff_service_entitlements_url(account.subdomain)).to_raise(Faraday::ClientError)
      response = staff_client.update_entitlements(user_id, entitlements)
      assert_requested request, times: 3
      assert_nil response
    end

    describe 'retries' do
      describe 'when the request succeeds on the third retry' do
        before do
          stub_request(:patch, staff_service_entitlements_url(account.subdomain)).
            to_raise(Faraday::TimeoutError).then.
            to_return(status: 500).then.
            to_return(status: 200, body: { entitlements: entitlements }.to_json, headers: { "Content-Type" => "application/json" })
        end

        it 'returns the entitlements' do
          response = staff_client.update_entitlements(user_id, entitlements)
          assert_equal entitlements, response.symbolize_keys
        end
      end

      describe 'when the request does not succeed on the third retry' do
        before do
          stub_request(:patch, staff_service_entitlements_url(account.subdomain)).
            to_raise(Faraday::TimeoutError).then.
            to_return(status: 500).then.
            to_return(status: 500)
        end

        it 'records the update failure with datadog and fails silently' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).
            with(:failed_sync, tags: ['error:Kragle::InternalServerError', 'product:chat', 'product:support'])
          entitlements_payload = { chat: 'foo', support: 'bob' }
          response = staff_client.update_entitlements(user_id, entitlements_payload)
          assert_nil response
        end
      end
    end
  end

  describe '#staff_count' do
    let(:endpoint) { "api/services/entitlements/#{product}/staff_count" }
    let(:expected_url) { "#{account_url_base}/#{endpoint}?#{params}" }
    let(:params) { "include_inactive=false" }
    let(:product) { 'chat' }
    let(:staff_count) { staff_client.staff_count(product: product) }

    before do
      stub_request(:get, expected_url).to_return(status: 200, body: { staff_count: 2 }.to_json, headers: { "Content-Type" => "application/json"})
    end

    def self.it_behaves_like_an_staff_count_api_call
      it 'calls staff_count endpoint' do
        assert 2, staff_count
        assert_requested(:get, expected_url)
      end
    end

    it_behaves_like_an_staff_count_api_call

    describe 'when role is provided' do
      let(:role) { 'admin' }
      let(:params) { "include_inactive=false&role=admin" }
      let(:staff_count) { staff_client.staff_count(product: product, role: role) }

      it_behaves_like_an_staff_count_api_call
    end

    describe 'when include_inactive is provided' do
      let(:staff_count) { staff_client.staff_count(product: product, include_inactive: include_inactive) }

      describe 'with true' do
        let(:include_inactive) { true }
        let(:params) { "include_inactive=true" }

        it_behaves_like_an_staff_count_api_call
      end

      describe 'with false' do
        let(:include_inactive) { false }
        let(:params) { "include_inactive=false" }

        it_behaves_like_an_staff_count_api_call
      end
    end
  end

  describe '#get_staff!' do
    let(:product) { 'testproduct' }
    let(:include_inactive) { 'false' }
    let(:base_url) { "#{staff_service_url_prefix}/api/services/staff?product=#{product}&include_inactive=#{include_inactive}" }
    let(:requested_url) { base_url }

    it 'raises ResponseError' do
      request = stub_request(:get, requested_url).to_raise(Kragle::ResponseError)
      assert_raises Kragle::ResponseError do
        staff_client.get_staff!(product: product)
      end
      assert_requested request, times: 3
    end

    describe 'when the request succeeds' do
      let(:response) do
        {
          users: [
            {
              email: 'staff1@example.com',
              id: 10001
            },
            {
              email: 'staff2@example.com',
              id: 10002
            }
          ]
        }
      end

      before do
        stub_request(:get, requested_url).to_return(status: 200, body: response.to_json, headers: { "Content-Type" => "application/json" })
      end

      it 'calls staff service' do
        staff_client.get_staff!(product: product)
        assert_requested(:get, base_url)
      end

      it 'returns a list of users' do
        result = staff_client.get_staff!(product: product)
        assert_equal 'staff1@example.com', result.first['email']
        assert_equal 10002, result.second['id']
      end

      describe 'specify inactive flag' do
        let(:include_inactive) { 'true' }

        it 'calls staff service' do
          staff_client.get_staff!(product: product, include_inactive: include_inactive)
          assert_requested(:get, base_url)
        end
      end

      describe 'specify role' do
        let(:role) { 'testrole' }
        let(:requested_url) { "#{base_url}&role=#{role}" }

        it 'calls staff service' do
          staff_client.get_staff!(product: product, role: role)
          assert_requested(:get, requested_url)
        end
      end
    end
  end
end
