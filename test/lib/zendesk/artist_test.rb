require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::Artist do
  describe ".compare" do
    def compare
      Zendesk::Artist.compare(
        feature_name, account,
        control: -> { control_called << :control; :control },
        candidate: -> { candidate_called << :candidate; candidate_reply.first },
        eq: eq
      )
    end

    let(:account) { accounts(:minimum) }
    let(:feature_name) { :foo }
    let(:control_called) { [] }
    let(:candidate_called) { [] }
    let(:stats_reported) { [] }
    let(:candidate_reply) { [:candidate] }
    let(:eq) { nil }

    before do
      Zendesk::Artist.statsd.stubs(:timing).with { |*args| stats_reported << [:timing, args[0]] }
      Zendesk::Artist.statsd.stubs(:increment).with { |*args| stats_reported << [:increment, args[0]] }
    end

    it "calls only control when missing" do
      compare.must_equal :control
      control_called.must_equal [:control]
      candidate_called.must_equal []
      stats_reported.must_equal []
    end

    it "calls only control when off" do
      Arturo::Feature.create!(phase: 'off', symbol: feature_name)
      compare.must_equal :control
      control_called.must_equal [:control]
      candidate_called.must_equal []
      stats_reported.must_equal []
    end

    it "calls only candidate when on" do
      Arturo::Feature.create!(phase: 'on', symbol: feature_name)
      compare.must_equal :candidate
      control_called.must_equal []
      candidate_called.must_equal [:candidate]
      stats_reported.must_equal []
    end

    it "does not compare when disabled by rollout" do
      Arturo::Feature.create!(phase: 'rollout', deployment_percentage: 0, symbol: feature_name)
      compare.must_equal :control
      candidate_called.must_equal []
      control_called.must_equal [:control]
      stats_reported.must_equal []
    end

    describe "when enabled" do
      before { Arturo::Feature.create!(phase: 'rollout', deployment_percentage: 100, symbol: feature_name) }

      it "successfully compares" do
        candidate_reply[0] = :control
        compare.must_equal :control
        control_called.must_equal [:control]
        candidate_called.must_equal [:candidate]
        stats_reported.must_equal [
          [:timing, "artist.foo.control"],
          [:timing, "artist.foo.candidate"],
          [:timing, "artist.foo.delta"],
          [:increment, "artist.foo.matched"]
        ]
      end

      it "fails to compare" do
        candidate_called << :old
        compare.must_equal :control
        control_called.must_equal [:control]
        candidate_called.must_equal [:old, :candidate]
        stats_reported.must_equal [
          [:timing, "artist.foo.control"],
          [:timing, "artist.foo.candidate"],
          [:timing, "artist.foo.delta"],
          [:increment, "artist.foo.mismatched"]
        ]
      end

      it "silences candidate errors" do
        candidate_called.freeze
        compare.must_equal :control
        control_called.must_equal [:control]
        candidate_called.must_equal []
        stats_reported.must_equal [
          [:timing, "artist.foo.control"],
          [:increment, "artist.foo.mismatched"]
        ]
      end

      it "passes control errors" do
        control_called.freeze
        assert_raises(RuntimeError) { compare }
        control_called.must_equal []
        candidate_called.must_equal []
        stats_reported.must_equal []
      end

      describe "with a custom eq method" do
        let(:eq) { lambda { |a, b| a == :control && b == :almost_control } }

        it "reports a matched result" do
          candidate_reply[0] = :almost_control
          compare.must_equal :control
          stats_reported.must_equal [
            [:timing, "artist.foo.control"],
            [:timing, "artist.foo.candidate"],
            [:timing, "artist.foo.delta"],
            [:increment, "artist.foo.matched"]
          ]
        end
      end
    end
  end
end
