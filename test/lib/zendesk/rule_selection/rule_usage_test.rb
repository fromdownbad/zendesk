require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::RuleUsage do
  include TestSupport::Rule::UsageHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  let(:redis) { Zendesk::RedisStore.client }

  let(:today) { Date.new(2016, 1, 1) }

  let(:record_month_dates) do
    (0..30).step(3).map { |day| today + day }
  end

  let(:record_week_dates) { (0...7).map { |day| today + day } }

  let(:record_hours) do
    (0..24).step(4).
      map { |hour| Time.now + hour.hours }.
      map { |time| time - (time.hour % 4).hours }.
      map(&:beginning_of_hour)
  end

  let(:record_minutes) do
    (0..60).step(15).
      map { |minute| Time.now + minute.minutes }.
      map { |time| time - (time.min % 15).minutes }
  end

  let(:rule_usage) { Zendesk::RuleSelection::RuleUsage.new(account) }

  before do
    Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

    Timecop.freeze(today)

    rule_usage.record(rule_id: 1, type: :macro)

    Timecop.travel(13.minutes.ago) do
      record_usage(type: :trigger, id: 7, count: 5)
      record_usage(type: :macro,   id: 2, count: 4)
    end

    Timecop.travel(39.minutes.ago) do
      record_usage(type: :macro, id: 4, count: 3)
      record_usage(type: :macro, id: 3, count: 2)
    end

    Timecop.travel(6.hours.ago) do
      record_usage(type: :macro, id: 4, count: 3)
      record_usage(type: :macro, id: 3, count: 3)
    end

    Timecop.travel(3.days.ago) do
      record_usage(type: :macro, id: 3, count: 3)
      record_usage(type: :macro, id: 2, count: 3)
    end

    Timecop.travel(18.days.ago) do
      record_usage(type: :macro, id: 4, count: 6)
      record_usage(type: :macro, id: 2, count: 5)
    end
  end

  describe '#all_execution_counts' do
    describe 'when the timeframe is hourly' do
      it 'returns all execution counts over the last hour' do
        assert_equal(
          {2 => 4, 4 => 3, 3 => 2, 1 => 1},
          rule_usage.all_execution_counts(timeframe: :hourly, type: :macro)
        )
      end
    end

    describe 'when the timeframe is daily' do
      it 'returns all execution counts over the last 24 hours' do
        assert_equal(
          {4 => 6, 3 => 5, 2 => 4, 1 => 1},
          rule_usage.all_execution_counts(timeframe: :daily, type: :macro)
        )
      end
    end

    describe 'when the timeframe is weekly' do
      it 'returns all execution counts over the last 7 days' do
        assert_equal(
          {3 => 8, 2 => 7, 4 => 6, 1 => 1},
          rule_usage.all_execution_counts(timeframe: :weekly, type: :macro)
        )
      end
    end

    describe 'when the timeframe is monthly' do
      it 'returns all execution counts over the last 30 days' do
        assert_equal(
          {4 => 12, 2 => 12, 3 => 8, 1 => 1},
          rule_usage.all_execution_counts(timeframe: :monthly, type: :macro)
        )
      end
    end
  end

  describe '#execution_counts' do
    let(:execution_counts) do
      rule_usage.execution_counts(
        rule_ids:  rule_ids,
        timeframe: timeframe,
        type:      :macro
      ).map { |key, future| {id: key, count: future.value.to_i} }
    end

    describe 'when the timeframe is hourly' do
      let(:timeframe) { :hourly }
      let(:rule_ids)  { [3, 4, 2] }

      it 'returns the execution counts over the last hour' do
        assert_equal(
          [{id: 3, count: 2}, {id: 4, count: 3}, {id: 2, count: 4}],
          execution_counts
        )
      end
    end

    describe 'when the timeframe is daily' do
      let(:timeframe) { :daily }
      let(:rule_ids)  { [3, 2] }

      it 'returns the execution counts over the last 24 hours' do
        assert_equal [{id: 3, count: 5}, {id: 2, count: 4}], execution_counts
      end
    end

    describe 'when the timeframe is weekly' do
      let(:timeframe) { :weekly }
      let(:rule_ids)  { [4, 3] }

      it 'returns the execution counts over the last 7 days' do
        assert_equal [{id: 4, count: 6}, {id: 3, count: 8}], execution_counts
      end
    end

    describe 'when the timeframe is monthly' do
      let(:timeframe) { :monthly }
      let(:rule_ids)  { [2, 4] }

      it 'returns the execution counts over the last 30 days' do
        assert_equal [{id: 2, count: 12}, {id: 4, count: 12}], execution_counts
      end
    end
  end

  describe '#execution_count' do
    describe 'when the timeframe is hourly' do
      let(:timeframe) { :hourly }

      it 'returns the execution count for the rule over the last hour' do
        assert_equal(
          3,
          rule_usage.execution_count(
            rule_id:   4,
            timeframe: timeframe,
            type:      :macro
          )
        )
      end
    end

    describe 'when the timeframe is daily' do
      let(:timeframe) { :daily }

      it 'returns the execution count for the rule over the last 24 hours' do
        assert_equal(
          4,
          rule_usage.execution_count(
            rule_id:   2,
            timeframe: timeframe,
            type:      :macro
          )
        )
      end
    end

    describe 'when the timeframe is weekly' do
      let(:timeframe) { :weekly }

      it 'returns the execution count for the rule over the last 7 days' do
        assert_equal(
          8,
          rule_usage.execution_count(
            rule_id:   3,
            timeframe: timeframe,
            type:      :macro
          )
        )
      end
    end

    describe 'when the timeframe is monthly' do
      let(:timeframe) { :monthly }

      it 'returns the execution count for the rule over the last 30 days' do
        assert_equal(
          12,
          rule_usage.execution_count(
            rule_id:   2,
            timeframe: timeframe,
            type:      :macro
          )
        )
      end
    end
  end

  describe '#most_used' do
    describe 'when the timeframe is hourly' do
      let(:timeframe) { :hourly }

      it 'returns the most-used rules in the last hour' do
        assert_equal(
          [{id: 2, count: 4}, {id: 4, count: 3}, {id: 3, count: 2}],
          rule_usage.most_used(number: 3, timeframe: timeframe, type: :macro)
        )
      end

      describe 'and the specified number is not positive' do
        it 'returns the entire set' do
          assert_equal(
            [
              {id: 2, count: 4},
              {id: 4, count: 3},
              {id: 3, count: 2},
              {id: 1, count: 1}
            ],
            rule_usage.most_used(number: 0, timeframe: timeframe, type: :macro)
          )
        end
      end

      describe 'and specified number is greater than the number of macros' do
        it 'returns the entire set' do
          assert_equal(
            [
              {id: 2, count: 4},
              {id: 4, count: 3},
              {id: 3, count: 2},
              {id: 1, count: 1}
            ],
            rule_usage.most_used(
              number:    100,
              timeframe: timeframe,
              type:      :macro
            )
          )
        end
      end

      describe 'and a rule was used more than an hour in the past' do
        before do
          Timecop.freeze(Time.now - 61.minutes) do
            record_usage(type: :macro, id: 6, count: 10)
          end
        end

        it 'does not factor into the results' do
          assert_equal(
            [{id: 2, count: 4}],
            rule_usage.most_used(number: 1, timeframe: timeframe, type: :macro)
          )
        end
      end
    end

    describe 'when the timeframe is daily' do
      let(:timeframe) { :daily }

      it 'returns the most-used rules in the last 24 hours' do
        assert_equal(
          [{id: 4, count: 6}, {id: 3, count: 5}, {id: 2, count: 4}],
          rule_usage.most_used(number: 3, timeframe: timeframe, type: :macro)
        )
      end

      describe 'and the specified number is not positive' do
        it 'returns the entire set' do
          assert_equal(
            [
              {id: 4, count: 6},
              {id: 3, count: 5},
              {id: 2, count: 4},
              {id: 1, count: 1}
            ],
            rule_usage.most_used(number: 0, timeframe: timeframe, type: :macro)
          )
        end
      end

      describe 'and specified number is greater than the number of macros' do
        it 'returns the entire set' do
          assert_equal(
            [
              {id: 4, count: 6},
              {id: 3, count: 5},
              {id: 2, count: 4},
              {id: 1, count: 1}
            ],
            rule_usage.most_used(
              number:    100,
              timeframe: timeframe,
              type:      :macro
            )
          )
        end
      end

      describe 'and a rule was used more than a day in the past' do
        before do
          Timecop.freeze(today.yesterday - 1.minute) do
            record_usage(type: :macro, id: 6, count: 10)
          end
        end

        it 'does not factor into the results' do
          assert_equal(
            [{id: 4, count: 6}],
            rule_usage.most_used(number: 1, timeframe: timeframe, type: :macro)
          )
        end
      end
    end

    describe 'when the timeframe is weekly' do
      let(:timeframe) { :weekly }

      it 'returns the most-used rules in the last 7 days' do
        assert_equal(
          [{id: 3, count: 8}, {id: 2, count: 7}, {id: 4, count: 6}],
          rule_usage.most_used(number: 3, timeframe: timeframe, type: :macro)
        )
      end

      describe 'and the specified number is not positive' do
        it 'returns the entire set' do
          assert_equal(
            [
              {id: 3, count: 8},
              {id: 2, count: 7},
              {id: 4, count: 6},
              {id: 1, count: 1}
            ],
            rule_usage.most_used(number: 0, timeframe: timeframe, type: :macro)
          )
        end
      end

      describe 'and specified number is greater than the number of macros' do
        it 'returns the entire set' do
          assert_equal(
            [
              {id: 3, count: 8},
              {id: 2, count: 7},
              {id: 4, count: 6},
              {id: 1, count: 1}
            ],
            rule_usage.most_used(
              number:    100,
              timeframe: timeframe,
              type:      :macro
            )
          )
        end
      end

      describe 'and a rule was used more than 7 days in the past' do
        before do
          Timecop.freeze(today - 8) do
            record_usage(type: :macro, id: 6, count: 10)
          end
        end

        it 'does not factor into the results' do
          assert_equal(
            [{id: 3, count: 8}],
            rule_usage.most_used(number: 1, timeframe: timeframe, type: :macro)
          )
        end
      end
    end

    describe 'when the timeframe is monthly' do
      let(:timeframe) { :monthly }

      it 'returns the most-used rules in the last 30 days' do
        assert_equal(
          [{id: 2, count: 12}, {id: 4, count: 12}, {id: 3, count: 8}],
          rule_usage.most_used(number: 3, timeframe: timeframe, type: :macro)
        )
      end

      describe 'and the specified number is not positive' do
        it 'returns the entire set' do
          assert_equal(
            [
              {id: 2, count: 12},
              {id: 4, count: 12},
              {id: 3, count: 8},
              {id: 1, count: 1}
            ],
            rule_usage.most_used(number: 0, timeframe: timeframe, type: :macro)
          )
        end
      end

      describe 'and specified number is greater than the number of macros' do
        it 'returns the entire set' do
          assert_equal(
            [
              {id: 2, count: 12},
              {id: 4, count: 12},
              {id: 3, count: 8},
              {id: 1, count: 1}
            ],
            rule_usage.most_used(
              number:    100,
              timeframe: timeframe,
              type:      :macro
            )
          )
        end
      end

      describe 'and a rule was used more than 30 days in the past' do
        before do
          Timecop.freeze(today - 31) do
            record_usage(type: :macro, id: 6, count: 10)
          end
        end

        it 'does not factor into the results' do
          assert_equal(
            [{id: 2, count: 12}],
            rule_usage.most_used(number: 1, timeframe: timeframe, type: :macro)
          )
        end
      end
    end
  end

  describe '#record' do
    let(:type)    { :trigger }
    let(:rule_id) { 1 }

    before { rule_usage.record(rule_id: rule_id, type: type) }

    it 'counts the hourly usage for the next hour' do
      assert(
        record_minutes.all? do |time|
          redis.zscore(hourly_key(account, type, time), rule_id).present?
        end
      )
    end

    it 'does not count the hourly usage after an hour' do
      refute(
        redis.zscore(
          hourly_key(account, type, 61.minutes.from_now),
          rule_id
        ).present?
      )
    end

    it 'counts the daily usage for the next 24 hours' do
      assert(
        record_hours.all? do |time|
          redis.zscore(daily_key(account, type, time), rule_id).present?
        end
      )
    end

    it 'does not count the daily usage after 24 hours' do
      refute(
        redis.zscore(
          daily_key(account, type, 25.hours.from_now),
          rule_id
        ).present?
      )
    end

    it 'counts the weekly usage for the next 7 days' do
      assert(
        record_week_dates.all? do |date|
          redis.zscore(weekly_key(account, type, date), rule_id).present?
        end
      )
    end

    it 'does not count the weekly usage after 7 days' do
      refute(
        redis.zscore(
          weekly_key(account, type, 8.days.from_now.to_date),
          rule_id
        ).present?
      )
    end

    it 'counts the monthly usage for the next 30 days' do
      assert(
        record_month_dates.all? do |date|
          redis.zscore(monthly_key(account, type, date), rule_id).present?
        end
      )
    end

    it 'does not count the monthly usage after 30 days' do
      refute(
        redis.zscore(
          monthly_key(account, type, 31.days.from_now.to_date),
          rule_id
        ).present?
      )
    end

    before_should 'expire each key at the end of the respective usage period' do
      record_month_dates.each do |date|
        redis.expects(:expireat).
          with(monthly_key(account, type, date), (date + 3).to_time.utc.to_i)
      end

      record_week_dates.each do |date|
        redis.expects(:expireat).
          with(weekly_key(account, type, date), date.next_day.to_time.utc.to_i)
      end

      record_hours.each do |time|
        redis.expects(:expireat).
          with(daily_key(account, type, time), (time + 4.hour).to_i)
      end

      record_minutes.each do |time|
        redis.expects(:expireat).
          with(hourly_key(account, type, time), (time + 15.minutes).to_i)
      end
    end
  end

  describe '#record_many' do
    let(:type)     { :trigger }
    let(:rule_ids) { [1, 2, 3] }

    before { rule_usage.record_many(rule_ids: rule_ids, type: type) }

    it 'counts the hourly usage for the next hour' do
      assert(
        record_minutes.all? do |time|
          rule_ids.all? do |rule_id|
            redis.zscore(hourly_key(account, type, time), rule_id).present?
          end
        end
      )
    end

    it 'does not count the hourly usage after an hour' do
      refute(
        rule_ids.any? do |rule_id|
          redis.zscore(
            hourly_key(account, type, 61.minutes.from_now),
            rule_id
          ).present?
        end
      )
    end

    it 'counts the daily usage for the next 24 hours' do
      assert(
        record_hours.all? do |time|
          rule_ids.all? do |rule_id|
            redis.zscore(daily_key(account, type, time), rule_id).present?
          end
        end
      )
    end

    it 'does not count the daily usage after 24 hours' do
      refute(
        rule_ids.any? do |rule_id|
          redis.zscore(
            daily_key(account, type, 25.hours.from_now),
            rule_id
          ).present?
        end
      )
    end

    it 'counts the weekly usage for the next 7 days' do
      assert(
        record_week_dates.all? do |date|
          rule_ids.all? do |rule_id|
            redis.zscore(weekly_key(account, type, date), rule_id).present?
          end
        end
      )
    end

    it 'does not count the weekly usage after 7 days' do
      refute(
        rule_ids.any? do |rule_id|
          redis.zscore(
            weekly_key(account, type, 8.days.from_now.to_date),
            rule_id
          ).present?
        end
      )
    end

    it 'counts the monthly usage for the next 30 days' do
      assert(
        record_month_dates.all? do |date|
          rule_ids.all? do |rule_id|
            redis.zscore(monthly_key(account, type, date), rule_id).present?
          end
        end
      )
    end

    it 'does not count the monthly usage after 30 days' do
      refute(
        rule_ids.any? do |rule_id|
          redis.zscore(
            monthly_key(account, type, 31.days.from_now.to_date),
            rule_id
          ).present?
        end
      )
    end

    before_should 'expire each key at the end of the respective usage period' do
      record_month_dates.each do |date|
        redis.expects(:expireat).
          with(monthly_key(account, type, date), (date + 3).to_time.utc.to_i).
          once
      end

      record_week_dates.each do |date|
        redis.expects(:expireat).
          with(weekly_key(account, type, date), date.next_day.to_time.utc.to_i).
          once
      end

      record_hours.each do |time|
        redis.expects(:expireat).
          with(daily_key(account, type, time), (time + 4.hours).to_i).
          once
      end

      record_minutes.each do |time|
        redis.expects(:expireat).
          with(hourly_key(account, type, time), (time + 15.minutes).to_i).
          once
      end
    end
  end
end
