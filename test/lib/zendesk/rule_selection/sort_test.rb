require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Sort do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :tickets,
    :users

  let(:described_class) { Zendesk::RuleSelection::Sort }

  let(:user)    { users(:minimum_agent) }
  let(:account) { user.account }
  let(:ticket)  { tickets(:minimum_1) }

  describe '.order' do
    it 'returns the allowed orders' do
      assert_equal %w[asc desc], described_class.order.to_a
    end
  end

  describe '.for_context' do
    describe 'when the rule type is `macro`' do
      let(:context) do
        Zendesk::RuleSelection::Context::Macro.new(user, options)
      end

      describe 'and a valid sort exists' do
        describe 'and is alphabetical' do
          let(:options) { {sort_by: 'alphabetical'} }

          it 'creates an alphabetical sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Alphabetical,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is usage' do
          let(:options) { {sort_by: 'usage_1h'} }

          it 'creates a usage sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Usage,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is timestamp' do
          let(:options) { {sort_by: 'created_at'} }

          it 'creates a timestamp sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Timestamp,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is position' do
          let(:options) { {sort_by: 'position'} }

          it 'creates an owner position sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::OwnerPosition,
              described_class.for_context(context).class
            )
          end
        end
      end

      describe 'and no sorts are valid' do
        let(:options) { {sort_by: 'invalid'} }

        it 'creates a default sort' do
          assert_equal(
            Zendesk::RuleSelection::Sort::Default,
            described_class.for_context(context).class
          )
        end
      end
    end

    describe 'when the rule type is `view`' do
      let(:context) { Zendesk::RuleSelection::Context::View.new(user, options) }

      describe 'and a valid sort exists' do
        describe 'and is alphabetical' do
          let(:options) { {sort_by: 'alphabetical'} }

          it 'creates an alphabetical sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Alphabetical,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is timestamp' do
          let(:options) { {sort_by: 'created_at'} }

          it 'creates a timestamp sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Timestamp,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is position' do
          let(:options) { {sort_by: 'position'} }

          it 'creates an owner position sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::OwnerPosition,
              described_class.for_context(context).class
            )
          end
        end
      end

      describe 'and no sorts are valid' do
        let(:options) { {sort_by: 'invalid'} }

        it 'creates a default sort' do
          assert_equal(
            Zendesk::RuleSelection::Sort::Default,
            described_class.for_context(context).class
          )
        end
      end
    end

    describe 'when the rule type is `automation`' do
      let(:context) do
        Zendesk::RuleSelection::Context::Automation.new(user, options)
      end

      describe 'and a valid sort exists' do
        describe 'and is alphabetical' do
          let(:options) { {sort_by: 'alphabetical'} }

          it 'creates an alphabetical sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Alphabetical,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is usage' do
          let(:options) { {sort_by: 'usage_1h'} }

          it 'creates a usage sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Usage,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is timestamp' do
          let(:options) { {sort_by: 'created_at'} }

          it 'creates a timestamp sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Timestamp,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is position' do
          let(:options) { {sort_by: 'position'} }

          it 'creates a position sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Position,
              described_class.for_context(context).class
            )
          end
        end
      end

      describe 'and no sorts are valid' do
        let(:options) { {sort_by: 'invalid'} }

        it 'creates a default sort' do
          assert_equal(
            Zendesk::RuleSelection::Sort::Default,
            described_class.for_context(context).class
          )
        end
      end
    end

    describe 'when the rule type is `trigger`' do
      let(:context) do
        Zendesk::RuleSelection::Context::Trigger.new(user, options)
      end

      describe 'and a valid sort exists' do
        describe 'and is alphabetical' do
          let(:options) { {sort_by: 'alphabetical'} }

          it 'creates an alphabetical sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Alphabetical,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is usage' do
          let(:options) { {sort_by: 'usage_1h'} }

          it 'creates a usage sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Usage,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is timestamp' do
          let(:options) { {sort_by: 'created_at'} }

          it 'creates a timestamp sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Timestamp,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and is position' do
          let(:options) { {sort_by: 'position'} }

          it 'creates a position sort' do
            assert_equal(
              Zendesk::RuleSelection::Sort::Position,
              described_class.for_context(context).class
            )
          end
        end
      end

      describe 'and no sorts are valid' do
        let(:options) { {sort_by: 'invalid'} }

        it 'creates a default sort' do
          assert_equal(
            Zendesk::RuleSelection::Sort::Default,
            described_class.for_context(context).class
          )
        end
      end
    end
  end

  describe '.for_search' do
    let(:context) do
      Zendesk::RuleSelection::Context::Macro.new(user, options)
    end

    describe 'when a valid sort exists' do
      let(:options) { {sort_by: 'created_at'} }

      it 'creates the correct sort' do
        assert_equal(
          Zendesk::RuleSelection::Sort::Timestamp,
          described_class.for_context(context).class
        )
      end
    end

    describe 'when no sorts are valid' do
      let(:options) { {sort_by: 'invalid'} }

      it 'creates a relevance sort' do
        assert_equal(
          Zendesk::RuleSelection::Sort::Relevance,
          described_class.for_search(context).class
        )
      end
    end
  end
end
