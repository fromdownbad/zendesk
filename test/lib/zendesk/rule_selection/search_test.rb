require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Search do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :tickets,
    :users

  let(:described_class) { Zendesk::RuleSelection::Search }

  let(:user)    { users(:minimum_agent) }
  let(:account) { user.account }

  let(:query) { 'test query' }

  let(:context) do
    Zendesk::RuleSelection::Context::Macro.new(
      user,
      page:       1,
      per_page:   5,
      query:      query,
      sort_by:    sort_by,
      sort_order: sort_order
    )
  end

  let(:rule_1) { Rule.where(position: 1).first }
  let(:rule_2) { Rule.where(position: 2).first }
  let(:rule_3) { Rule.where(position: 3).first }
  let(:rule_4) { Rule.where(position: 4).first }

  let(:filter_query) do
    '((owner_type:Account AND is_active:true) OR ' \
      '(owner_type:Group AND group_ids:(7 OR 8 OR 9) AND is_active:true) OR ' \
      "(owner_type:User AND owner_id:#{user.id}))"
  end

  let(:search_client) { stub('search client') }

  before do
    Rule.destroy_all

    account.subscription.stubs(has_group_rules?: true)
    account.subscription.stubs(has_personal_rules?: true)

    ZendeskSearch::Client.
      stubs(:new).
      with(
        user.account_id,
        user.account.has_search_green?,
        user.account.has_ss_enable_search_service_hub?
      ).
      returns(search_client)

    create_macro(title: 'AA', owner: account, active: true,  position: 1)
    create_macro(title: 'AB', owner: account, active: false, position: 2)

    create_macro(title: 'AG', owner: user, active: true,  position: 3)
    create_macro(title: 'AH', owner: user, active: false, position: 4)

    user.stubs(group_ids: [7, 8, 9])
  end

  describe '.for_context' do
    let(:sort_by)    { nil }
    let(:sort_order) { nil }

    before do
      search_client.
        stubs(:search).
        with(
          query,
          account_id:  account.id,
          endpoint:    'rule',
          from:        0,
          fq:          filter_query,
          hl:          true,
          incremental: true,
          size:        5,
          type:        'macro'
        ).
        returns('results' => results, 'count' => results.size)
    end

    describe 'when the results are in scope' do
      let(:results) do
        [
          {'id' => rule_3.id, 'type' => 'Macro', '_highlights' => '<em>3</em>'},
          {'id' => rule_1.id, 'type' => 'Macro', '_highlights' => '<em>1</em>'},
          {'id' => rule_4.id, 'type' => 'Macro', '_highlights' => '<em>4</em>'}
        ]
      end

      it 'returns the results in order' do
        assert_rules_equal(
          [
            {owner_type: 'User',    active: true,  title: 'AG', position: 3},
            {owner_type: 'Account', active: true,  title: 'AA', position: 1},
            {owner_type: 'User',    active: false, title: 'AH', position: 4}
          ],
          described_class.for_context(context).rules
        )
      end

      it 'returns the highlights' do
        assert_equal(
          {
            rule_3.id => '<em>3</em>',
            rule_1.id => '<em>1</em>',
            rule_4.id => '<em>4</em>'
          },
          described_class.for_context(context).highlights
        )
      end
    end

    describe 'when the results are not in scope' do
      let(:results) { [{'id' => rule_2.id, 'type' => 'Macro'}] }

      it 'omits the results' do
        assert_empty described_class.for_context(context).rules
      end
    end
  end

  describe 'when sorting' do
    let(:sort_by)    { nil }
    let(:sort_order) { nil }

    let(:results) do
      [
        {'id' => rule_3.id, 'type' => 'Macro'},
        {'id' => rule_1.id, 'type' => 'Macro'},
        {'id' => rule_4.id, 'type' => 'Macro'}
      ]
    end

    describe 'and no sorting parameters are provided' do
      before { described_class.for_context(context) }

      before_should 'construct the appropriate query' do
        search_client.
          expects(:search).
          with(
            query,
            account_id:  account.id,
            endpoint:    'rule',
            from:        0,
            fq:          filter_query,
            hl:          true,
            incremental: true,
            size:        5,
            type:        'macro'
          ).
          returns('results' => results, 'count' => results.size)
      end
    end

    describe 'and `sort_by` is provided' do
      let(:sort_by) { 'alphabetical' }

      before { described_class.for_context(context) }

      before_should 'construct the appropriate query' do
        search_client.
          expects(:search).
          with(
            "#{query} order_by:title sort:asc",
            account_id:  account.id,
            endpoint:    'rule',
            from:        0,
            fq:          filter_query,
            hl:          true,
            incremental: true,
            size:        5,
            type:        'macro'
          ).
          returns('results' => results, 'count' => results.size)
      end
    end

    describe 'and `sort_order` is provided' do
      let(:sort_order) { 'desc' }

      before { described_class.for_context(context) }

      before_should 'construct the appropriate query' do
        search_client.
          expects(:search).
          with(
            query,
            account_id:  account.id,
            endpoint:    'rule',
            from:        0,
            fq:          filter_query,
            hl:          true,
            incremental: true,
            size:        5,
            type:        'macro'
          ).
          returns('results' => results, 'count' => results.size)
      end
    end

    describe 'and both parameters are provided' do
      let(:sort_by)    { 'alphabetical' }
      let(:sort_order) { 'desc' }

      before { described_class.for_context(context) }

      before_should 'construct the appropriate query' do
        search_client.
          expects(:search).
          with(
            "#{query} order_by:title sort:desc",
            account_id:  account.id,
            endpoint:    'rule',
            from:        0,
            fq:          filter_query,
            hl:          true,
            incremental: true,
            size:        5,
            type:        'macro'
          ).
          returns('results' => results, 'count' => results.size)
      end
    end
  end
end
