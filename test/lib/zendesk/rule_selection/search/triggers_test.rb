require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Search::Triggers do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :tickets,
    :users

  let(:described_class) { Zendesk::RuleSelection::Search::Triggers }

  let(:user)    { users(:minimum_admin) }
  let(:account) { user.account }

  let(:query) { nil }

  let(:filter) do
    CGI.escape(JSON.generate({
      "conditions" => [
        {
          "field" => "tags",
          "operator" => "is",
          "value" => "tag123*"
        },
        {
          "field" => "status",
          "operator" => "is",
          "value" => "open"
        }
      ],
      "title" => "example"
    }))
  end

  let(:parsed_filter) do
    h = {
      filter: {
        "$and" => [
          { "conditions" => { "$eq" => "tags is tag123*" } },
          { "conditions" => { "$eq" => "status_id is 1" } },
          { "title" => { "$eq" => "example" } }
        ]
      }
    }

    h[:filter]["$and"] << { "is_active" => { "$eq" => active.to_s } } unless active.nil?
    { json: JSON.generate(h) }
  end

  let(:context) do
    Zendesk::RuleSelection::Context::Trigger.new(
      user,
      filter:     filter,
      page:       1,
      per_page:   5,
      query:      query,
      sort_by:    sort_by,
      sort_order: sort_order,
      active:     active
    )
  end

  let(:context_by_agent) do
    Zendesk::RuleSelection::Context::Trigger.new(
      users(:minimum_agent),
      filter:     filter,
      page:       1,
      per_page:   5,
      query:      query,
      sort_by:    sort_by,
      sort_order: sort_order,
      active:     active
    )
  end

  let(:rule_1) { Trigger.where(position: 1).first }
  let(:rule_2) { Trigger.where(position: 2).first }
  let(:rule_3) { Trigger.where(position: 3).first }
  let(:rule_4) { Trigger.where(position: 4).first }

  let(:search_client) { stub('search client') }

  before do
    Rule.destroy_all

    ZendeskSearch::Client.
      stubs(:new).
      with(
        user.account_id,
        user.account.has_search_green?,
        user.account.has_ss_enable_search_service_hub?
      ).
      returns(search_client)

    create_trigger(title: 'AA', owner: account, active: true,  position: 1)
    create_trigger(title: 'AB', owner: account, active: false, position: 2)

    create_trigger(title: 'AG', owner: account, active: true,  position: 3)
    create_trigger(title: 'AH', owner: account, active: false, position: 4)
  end

  describe '.for_context' do
    let(:sort_by)    { nil }
    let(:sort_order) { nil }

    before do
      search_client.
        stubs(:search).
        with(
          '',
          account_id:  account.id,
          endpoint:    'trigger',
          filter:      parsed_filter,
          from:        0,
          hl:          true,
          incremental: true,
          size:        5
        ).
        returns('results' => results, 'count' => results.size)
    end

    describe 'when filtering for active triggers' do
      let(:active) { true }

      describe 'when some of the results are in scope' do
        let(:results) do
          [
            {'id' => rule_3.id, '_highlights' => '<em>3</em>'},
            {'id' => rule_1.id, '_highlights' => '<em>1</em>'},
            {'id' => rule_4.id, '_highlights' => '<em>4</em>'}
          ]
        end

        it 'returns the results in order, discarding the inactive triggers' do
          assert_rules_equal(
            [
              {owner_type: "Account", active: true, title: "AG", position: 3},
              {owner_type: "Account", active: true, title: "AA", position: 1}
            ],
            described_class.for_context(context).rules
          )
        end

        it 'returns the highlights' do
          assert_equal(
            {
              rule_3.id => '<em>3</em>',
              rule_1.id => '<em>1</em>',
              rule_4.id => '<em>4</em>'
            },
            described_class.for_context(context).highlights
          )
        end
      end

      describe 'when the results are not in scope' do
        let(:results) { [{'id' => rule_2.id, 'type' => 'Trigger'}] }

        it 'omits the results' do
          assert_empty described_class.for_context(context).rules
        end
      end
    end

    describe 'when filtering for inactive triggers' do
      let(:active) { false }

      describe 'when some of the results are in the scope' do
        let(:results) do
          [
            {'id' => rule_3.id, '_highlights' => '<em>3</em>'},
            {'id' => rule_1.id, '_highlights' => '<em>1</em>'},
            {'id' => rule_4.id, '_highlights' => '<em>4</em>'}
          ]
        end

        it 'returns the results in order, discarding the active triggers' do
          assert_rules_equal(
            [
              {owner_type: "Account", active: false, title: "AH", position: 4},
            ],
            described_class.for_context(context).rules
          )
        end

        it 'returns the highlights' do
          assert_equal(
            {
              rule_3.id => '<em>3</em>',
              rule_1.id => '<em>1</em>',
              rule_4.id => '<em>4</em>'
            },
            described_class.for_context(context).highlights
          )
        end

        it 'omits the results if the user is an agent and not an admin' do
          assert_empty described_class.for_context(context_by_agent).rules
        end
      end

      describe 'when the results are not in scope' do
        let(:results) { [{'id' => rule_1.id, 'type' => 'Trigger'}] }

        it 'omits the results' do
          assert_empty described_class.for_context(context).rules
        end
      end
    end
  end

  describe 'when sorting' do
    let(:sort_by)    { nil }
    let(:sort_order) { nil }
    let(:active)     { true }

    let(:results) do
      [
        {'id' => rule_3.id, 'type' => 'Trigger'},
        {'id' => rule_1.id, 'type' => 'Trigger'},
        {'id' => rule_4.id, 'type' => 'Trigger'}
      ]
    end

    describe 'and no sorting parameters are provided' do
      before { described_class.for_context(context) }

      before_should 'construct the appropriate query' do
        search_client.
          expects(:search).
          with(
            '',
            account_id:  account.id,
            endpoint:    'trigger',
            filter:      parsed_filter,
            from:        0,
            hl:          true,
            incremental: true,
            size:        5
          ).
          returns('results' => results, 'count' => results.size)
      end
    end

    describe 'and `sort_by` is provided' do
      let(:sort_by) { 'alphabetical' }
      let(:active)  { true }

      before { described_class.for_context(context) }

      before_should 'construct the appropriate query' do
        search_client.
          expects(:search).
          with(
            "",
            account_id:  account.id,
            endpoint:    'trigger',
            filter:      parsed_filter,
            from:        0,
            hl:          true,
            incremental: true,
            size:        5,
            sort:        'title'
          ).
          returns('results' => results, 'count' => results.size)
      end
    end

    describe 'and `sort_order` is provided' do
      let(:sort_order) { 'desc' }
      let(:active)     { true }

      before { described_class.for_context(context) }

      before_should 'construct the appropriate query, with no sorting option' do
        search_client.
          expects(:search).
          with(
            '',
            account_id:  account.id,
            endpoint:    'trigger',
            filter:      parsed_filter,
            from:        0,
            hl:          true,
            incremental: true,
            size:        5
          ).
          returns('results' => results, 'count' => results.size)
      end
    end

    describe 'and both parameters are provided' do
      let(:sort_by)    { 'alphabetical' }
      let(:sort_order) { 'desc' }
      let(:active)     { true }

      before { described_class.for_context(context) }

      before_should 'construct the appropriate query' do
        search_client.
          expects(:search).
          with(
            "",
            account_id:  account.id,
            endpoint:    'trigger',
            filter:      parsed_filter,
            from:        0,
            hl:          true,
            incremental: true,
            size:        5,
            sort:        '-title'
          ).
          returns('results' => results, 'count' => results.size)
      end
    end
  end
end
