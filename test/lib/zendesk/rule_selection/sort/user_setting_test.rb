require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Sort::UserSetting do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :users,
    :groups

  let(:described_class) { Zendesk::RuleSelection::Sort::UserSetting }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }
  let(:group)   { groups(:minimum_group) }

  let(:scope) { Rule.where(account_id: account.id) }

  let(:context) { Zendesk::RuleSelection::Context::View.new(user, options) }

  let(:sort) { described_class.new(context) }

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when `sort_by` is `user_setting`' do
      let(:options) { {sort_by: 'user_setting'} }

      describe 'and user setting sort is supported' do
        before { context.stubs(user_setting_sort?: true) }

        describe 'and the user setting order is specified' do
          before { context.stubs(user_setting_order: [123]) }

          it 'returns true' do
            assert described_class.valid?(context)
          end
        end

        describe 'and no user setting order is specified' do
          before { context.stubs(user_setting_order: []) }

          it 'returns false' do
            refute described_class.valid?(context)
          end
        end
      end

      describe 'and user setting sort is not supported' do
        before { context.stubs(user_setting_sort?: false) }

        it 'returns false' do
          refute described_class.valid?(context)
        end
      end
    end

    describe 'when `sort_by` is not `user_setting`' do
      let(:options) { {sort_by: 'alphabetical'} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#order' do
    let(:options) { {sort_by: 'user_setting'} }

    let!(:reordered_rule_1) do
      create_view(title: 'AC', owner: account, active: true, position: 6)
    end

    let!(:reordered_rule_2) do
      create_view(title: 'ZC', owner: account, active: true, position: 5)
    end

    before do
      create_view(title: 'AA', owner: account, active: true,  position: 3)
      create_view(title: 'AB', owner: account, active: false, position: 2)
      create_view(title: 'ZB', owner: account, active: true,  position: 2)
      create_view(title: 'ZA', owner: account, active: true,  position: 1)

      create_view(title: 'BC', owner: group, active: false, position: 2)
      create_view(title: 'CA', owner: group, active: true,  position: 4)
      create_view(title: 'AB', owner: group, active: false, position: 3)

      context.stubs(
        user_setting_order: [reordered_rule_1.id, reordered_rule_2.id]
      )
    end

    it 'sorts them appropriately' do
      assert_rules_equal(
        [
          {owner_type: 'Account', active: true,  title: 'AC', position: 6},
          {owner_type: 'Account', active: true,  title: 'ZC', position: 5},
          {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
          {owner_type: 'Account', active: false, title: 'AB', position: 2},
          {owner_type: 'Group',   active: false, title: 'BC', position: 2},
          {owner_type: 'Account', active: true,  title: 'ZB', position: 2},
          {owner_type: 'Account', active: true,  title: 'AA', position: 3},
          {owner_type: 'Group',   active: false, title: 'AB', position: 3},
          {owner_type: 'Group',   active: true,  title: 'CA', position: 4}
        ],
        sort.order(scope)
      )
    end
  end

  describe '#to_elasticsearch' do
    let(:options) { {sort_by: 'user_setting'} }

    it 'raises an error' do
      assert_raises(RuntimeError) do
        sort.to_elasticsearch('query')
      end
    end
  end
end
