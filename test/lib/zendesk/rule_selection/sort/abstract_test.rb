require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 3

describe Zendesk::RuleSelection::Sort::Abstract do
  let(:described_class) { Zendesk::RuleSelection::Sort::Abstract }

  let(:extending_class) do
    Class.new(described_class) do
      def default_order
        'default_order'
      end

      def elasticsearch_ordering
        'elasticsearch_ordering'
      end

      def ordering
        'ordering'
      end
    end
  end

  let(:context_stub) { stub('context') }

  let(:subject) { extending_class.new(context_stub) }

  describe '#order' do
    let(:scope)         { stub('scope') }
    let(:ordered_scope) { stub('ordered scope') }

    before do
      scope.stubs(:order).with('ordering').returns(ordered_scope)
    end

    it 'returns the ordered scope' do
      assert_equal ordered_scope, subject.order(scope)
    end

    describe 'when `ordering` is not defined' do
      let(:subject) { described_class.new(context_stub) }

      it 'raises an error' do
        assert_raises(RuntimeError) { subject.order(scope) }
      end
    end
  end

  describe '#to_elasticsearch' do
    let(:query) { 'test' }

    it 'appends the elasticsearch ordering to the query' do
      assert_equal(
        "#{query} elasticsearch_ordering",
        subject.to_elasticsearch(query)
      )
    end

    describe 'when `elasticsearch_ordering` is not defined' do
      let(:subject) { described_class.new(context_stub) }

      it 'raises an error' do
        assert_raises(RuntimeError) { subject.to_elasticsearch(query) }
      end
    end
  end
end
