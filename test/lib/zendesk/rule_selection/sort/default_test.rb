require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Sort::Default do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :users

  let(:described_class) { Zendesk::RuleSelection::Sort::Default }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:scope) { Rule.where(account_id: account.id) }

  let(:options) { {} }
  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, options) }

  let(:sort) { described_class.new(context) }

  before { Rule.destroy_all }

  describe '.valid?' do
    it 'returns true' do
      assert described_class.valid?(context)
    end
  end

  describe '#order' do
    let(:options) { {sort_by: :invalid} }

    before do
      create_macro(title: 'AB', owner: account, active: false, position: 4)
      create_macro(title: 'AA', owner: account, active: true,  position: 3)
      create_macro(title: 'AC', owner: account, active: true,  position: 2)
      create_macro(title: 'ZA', owner: account, active: true,  position: 1)
      create_macro(title: 'ZB', owner: account, active: false, position: 2)
    end

    describe 'when default sort is `position`' do
      before { context.stubs(default_sort: :position) }

      it 'sorts them appropriately' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
            {owner_type: 'Account', active: true,  title: 'AC', position: 2},
            {owner_type: 'Account', active: false, title: 'ZB', position: 2},
            {owner_type: 'Account', active: true,  title: 'AA', position: 3},
            {owner_type: 'Account', active: false, title: 'AB', position: 4}
          ],
          sort.order(scope)
        )
      end
    end

    describe 'when default sort is `alphabetical`' do
      before { context.stubs(default_sort: :alphabetical) }

      it 'sorts them appropriately' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: true,  title: 'AA', position: 3},
            {owner_type: 'Account', active: false, title: 'AB', position: 4},
            {owner_type: 'Account', active: true,  title: 'AC', position: 2},
            {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
            {owner_type: 'Account', active: false, title: 'ZB', position: 2}
          ],
          sort.order(scope)
        )
      end
    end
  end
end
