require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Sort::Timestamp do
  include TestSupport::Rule::Helper

  fixtures :accounts, :users

  let(:described_class) { Zendesk::RuleSelection::Sort::Timestamp }
  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }
  let(:scope) { Rule.where(account_id: account.id) }
  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, options) }
  let(:sort) { described_class.new(context) }

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when `sort_by` is `created at`' do
      let(:options) { {sort_by: 'created_at'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `sort_by` is `updated at`' do
      let(:options) { {sort_by: 'updated_at'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `sort_by` is neither value' do
      let(:options) { {sort_by: 'position'} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#order' do
    before do
      create_macro(title: 'ZB', owner: account, active: false, position: 2)

      Timecop.travel(1.hour) do
        create_macro(title: 'ZA', owner: account, active: true,  position: 1)
      end

      Timecop.travel(2.hours) do
        create_macro(title: 'AB', owner: account, active: false, position: 4)
      end

      Timecop.travel(3.hours) do
        create_macro(title: 'AA', owner: account, active: true,  position: 3)
      end

      Timecop.travel(4.hours) do
        Rule.where(position: 4).first.touch
      end

      Timecop.travel(5.hours) do
        Rule.where(position: 2).first.touch
      end
    end

    describe 'when `sort_by` is `created at`' do
      describe 'and `sort_order` is ascending' do
        let(:options) { {sort_by: 'created_at', sort_order: 'asc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is descending' do
        let(:options) { {sort_by: 'created_at', sort_order: 'desc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is not specified' do
        let(:options) { {sort_by: 'created_at'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and a join includes the `created_at` column' do
        let(:options) { {sort_by: 'created_at'} }

        let(:scope) do
          Macro.includes(:groups_macros).references(:groups_macros).where(account_id: account.id)
        end

        before { GroupMacro.destroy_all }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2}
            ],
            sort.order(scope)
          )
        end
      end
    end

    describe 'when `sort_by` is `updated at`' do
      describe 'and `sort_order` is ascending' do
        let(:options) { {sort_by: 'updated_at', sort_order: 'asc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is descending' do
        let(:options) { {sort_by: 'updated_at', sort_order: 'desc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is not provided' do
        let(:options) { {sort_by: 'updated_at'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and a join includes the `updated_at` column' do
        let(:options) { {sort_by: 'updated_at'} }

        let(:scope) do
          Macro.includes(:groups_macros).references(:groups_macros).where(account_id: account.id)
        end

        before { GroupMacro.destroy_all }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end
    end
  end

  describe '#to_elasticsearch' do
    describe 'when `sort_by` is `created_at`' do
      let(:options) { {sort_by: 'created_at', sort_order: 'desc'} }

      it 'contructs the correct query' do
        assert_equal(
          'close order_by:created_at sort:desc',
          sort.to_elasticsearch('close')
        )
      end
    end

    describe 'when `sort_by` is `updated_at`' do
      let(:options) { {sort_by: 'updated_at', sort_order: 'desc'} }

      it 'constructs the correct query' do
        assert_equal(
          'close order_by:updated_at sort:desc',
          sort.to_elasticsearch('close')
        )
      end
    end
  end
end
