require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Sort::Alphabetical do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :users

  let(:described_class) { Zendesk::RuleSelection::Sort::Alphabetical }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:scope) { Rule.where(account_id: account.id) }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, options) }

  let(:sort) { described_class.new(context) }

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when `sort_by` is `alphabetical`' do
      let(:options) { {sort_by: 'alphabetical'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `sort_by` is not `alphabetical`' do
      let(:options) { {sort_by: 'created_at'} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#order' do
    before do
      create_macro(title: 'ZA', owner: account, active: true,  position: 1)
      create_macro(title: 'ZB', owner: account, active: false, position: 2)
      create_macro(title: 'AA', owner: account, active: true,  position: 3)
      create_macro(title: 'AB', owner: account, active: false, position: 4)
    end

    describe 'when `sort_order` is ascending' do
      let(:options) { {sort_by: 'alphabetical', sort_order: 'asc'} }

      it 'sorts them appropriately' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: true,  title: 'AA', position: 3},
            {owner_type: 'Account', active: false, title: 'AB', position: 4},
            {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
            {owner_type: 'Account', active: false, title: 'ZB', position: 2}
          ],
          sort.order(scope)
        )
      end
    end

    describe 'when `sort_order` is descending' do
      let(:options) { {sort_by: 'alphabetical', sort_order: 'desc'} }

      it 'sorts them appropriately' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: false, title: 'ZB', position: 2},
            {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
            {owner_type: 'Account', active: false, title: 'AB', position: 4},
            {owner_type: 'Account', active: true,  title: 'AA', position: 3}
          ],
          sort.order(scope)
        )
      end
    end

    describe 'when `sort_order` is not specified' do
      let(:options) { {sort_by: 'alphabetical'} }

      it 'sorts them appropriately' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: true,  title: 'AA', position: 3},
            {owner_type: 'Account', active: false, title: 'AB', position: 4},
            {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
            {owner_type: 'Account', active: false, title: 'ZB', position: 2}
          ],
          sort.order(scope)
        )
      end
    end
  end

  describe '#to_elasticsearch' do
    let(:options) { {sort_by: 'alphabetical', sort_order: 'desc'} }

    it 'constructs the correct query' do
      assert_equal(
        'close order_by:title sort:desc',
        sort.to_elasticsearch('close')
      )
    end
  end
end
