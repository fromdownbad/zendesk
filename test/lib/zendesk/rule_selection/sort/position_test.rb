require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Sort::Position do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :users

  let(:described_class) { Zendesk::RuleSelection::Sort::Position }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:scope) { Rule.where(account_id: account.id) }

  let(:context) { Zendesk::RuleSelection::Context::Trigger.new(user, options) }

  let(:sort) { described_class.new(context) }

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when `sort_by` is `position`' do
      let(:options) { {sort_by: 'position'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `sort_by` is not `position`' do
      let(:options) { {sort_by: 'alphabetical'} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#order' do
    describe_with_arturo_setting_enabled :trigger_categories_api do
      before do
        RuleCategory.destroy_all

        category_1 = create_category(position: 1)
        category_2 = create_category(position: 2)

        create_trigger(title: 'A', position: 5, rules_category_id: category_1.id)
        create_trigger(title: 'B', position: 6, rules_category_id: category_1.id)
        create_trigger(title: 'C', position: 7, rules_category_id: category_1.id)

        create_trigger(title: 'D', position: 1, rules_category_id: category_2.id)
        create_trigger(title: 'E', position: 2, rules_category_id: category_2.id)
        create_trigger(title: 'F', position: 3, rules_category_id: category_2.id)
        create_trigger(title: 'FA', position: 3, rules_category_id: category_2.id)
      end

      describe 'when `sort_order` is ascending' do
        let(:options) { {sort_by: :position, sort_order: 'asc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true, title: 'A', position: 5},
              {owner_type: 'Account', active: true, title: 'B', position: 6},
              {owner_type: 'Account', active: true, title: 'C', position: 7},
              {owner_type: 'Account', active: true, title: 'D', position: 1},
              {owner_type: 'Account', active: true, title: 'E', position: 2},
              {owner_type: 'Account', active: true, title: 'F', position: 3},
              {owner_type: 'Account', active: true, title: 'FA', position: 3}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'when `sort_order` is descending' do
        let(:options) { {sort_by: :position, sort_order: 'desc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true, title: 'FA', position: 3},
              {owner_type: 'Account', active: true, title: 'F', position: 3},
              {owner_type: 'Account', active: true, title: 'E', position: 2},
              {owner_type: 'Account', active: true, title: 'D', position: 1},
              {owner_type: 'Account', active: true, title: 'C', position: 7},
              {owner_type: 'Account', active: true, title: 'B', position: 6},
              {owner_type: 'Account', active: true, title: 'A', position: 5}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'when `sort_order` is not specified' do
        let(:options) { {sort_by: :position} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true, title: 'A', position: 5},
              {owner_type: 'Account', active: true, title: 'B', position: 6},
              {owner_type: 'Account', active: true, title: 'C', position: 7},
              {owner_type: 'Account', active: true, title: 'D', position: 1},
              {owner_type: 'Account', active: true, title: 'E', position: 2},
              {owner_type: 'Account', active: true, title: 'F', position: 3},
              {owner_type: 'Account', active: true, title: 'FA', position: 3}
            ],
            sort.order(scope)
          )
        end
      end
    end

    describe_with_arturo_setting_disabled :trigger_categories_api do
      before do
        create_trigger(title: 'AA', active: true,  position: 3)
        create_trigger(title: 'AB', active: false, position: 2)
        create_trigger(title: 'ZB', active: true,  position: 2)
        create_trigger(title: 'ZA', active: true,  position: 1)

        create_trigger(title: 'BC', active: false, position: 2)
        create_trigger(title: 'AB', active: true,  position: 1)
        create_trigger(title: 'AD', active: false, position: 3)
      end

      describe 'when `sort_order` is ascending' do
        let(:options) { {sort_by: :position, sort_order: 'asc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AB', position: 1},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'AB', position: 2},
              {owner_type: 'Account', active: false, title: 'BC', position: 2},
              {owner_type: 'Account', active: true,  title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AD', position: 3}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'when `sort_order` is descending' do
        let(:options) { {sort_by: :position, sort_order: 'desc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'AD', position: 3},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: true,  title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'BC', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 2},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: true,  title: 'AB', position: 1}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'when `sort_order` is not specified' do
        let(:options) { {sort_by: :position} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AB', position: 1},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'AB', position: 2},
              {owner_type: 'Account', active: false, title: 'BC', position: 2},
              {owner_type: 'Account', active: true,  title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AD', position: 3}
            ],
            sort.order(scope)
          )
        end
      end
    end
  end

  describe '#new_endpoint_order_param' do
    before { create_category }
    let(:options) { {sort_by: :position, sort_order: 'desc'} }

    describe_with_arturo_setting_enabled :trigger_categories_api do
      it 'returns the correct query' do
        assert_equal(
          {sort: "-category_position,-position,-title"},
          sort.new_endpoint_order_param
        )
      end
    end

    describe_with_arturo_setting_disabled :trigger_categories_api do
      it 'returns the correct query' do
        assert_equal(
          {sort: "-position,-title"},
          sort.new_endpoint_order_param
        )
      end
    end
  end

  describe '#to_elasticsearch' do
    before { create_category }
    let(:options) { {sort_by: :position, sort_order: 'desc'} }

    describe_with_arturo_disabled :new_trigger_search do
      before { Arturo.disable_feature!(:new_trigger_search) }

      describe_with_arturo_setting_enabled :trigger_categories_api do
        it 'constructs the correct query' do
          assert_equal(
            'close order_by:category_position,position,title sort:desc',
            sort.to_elasticsearch('close')
          )
        end
      end

      describe_with_arturo_setting_disabled :trigger_categories_api do
        it 'constructs the correct query' do
          assert_equal(
            'close order_by:position,title sort:desc',
            sort.to_elasticsearch('close')
          )
        end
      end
    end

    describe_with_arturo_enabled :new_trigger_search do
      let(:filter_in_query) do
        {
          conditions: [
            {
              field: 'status',
              operator: 'is',
              value: 'open'
            },
            {
              field: 'group_id'
            }
          ],
          actions: [
            {
              field: 'notification_user',
              value: 557372
            }
          ],
          title: 'foo'
        }
      end

      let(:json_filter) do
        CGI.escape(JSON.generate(filter_in_query))
      end

      let(:options) { {sort_by: :position, sort_order: 'desc', filter: json_filter} }

      before { Arturo.enable_feature!(:new_trigger_search) }

      describe 'and rule is a Trigger' do
        describe_with_arturo_setting_enabled :trigger_categories_api do
          it 'constructs the correct query' do
            assert_equal(
              'close sort:-category_position,-position,-title',
              sort.to_elasticsearch('close')
            )
          end
        end

        describe_with_arturo_setting_disabled :trigger_categories_api do
          it 'constructs the correct query' do
            assert_equal(
              'close sort:-position,-title',
              sort.to_elasticsearch('close')
            )
          end
        end
      end

      describe 'and rule is not a Trigger' do
        let(:context) { Zendesk::RuleSelection::Context::View.new(user, options) }

        it 'constructs the correct query' do
          assert_equal(
            'close order_by:position,title sort:desc',
            sort.to_elasticsearch('close')
          )
        end
      end
    end
  end
end
