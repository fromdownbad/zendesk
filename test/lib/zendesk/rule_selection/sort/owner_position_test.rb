require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Sort::OwnerPosition do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :users,
    :groups

  let(:described_class) { Zendesk::RuleSelection::Sort::OwnerPosition }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }
  let(:group)   { groups(:minimum_group) }

  let(:scope) { Rule.where(account_id: account.id) }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, options) }

  let(:sort) { described_class.new(context) }

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when `sort_by` is `position`' do
      let(:options) { {sort_by: 'position'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `sort_by` is not `position`' do
      let(:options) { {sort_by: 'alphabetical'} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#order' do
    before do
      create_macro(title: 'AA', owner: account, active: true,  position: 3)
      create_macro(title: 'AB', owner: account, active: false, position: 2)
      create_macro(title: 'ZB', owner: account, active: true,  position: 2)
      create_macro(title: 'ZA', owner: account, active: true,  position: 1)

      create_macro(title: 'BC', owner: group, active: false, position: 2)
      create_macro(title: 'CA', owner: group, active: true,  position: 4)
      create_macro(title: 'AB', owner: group, active: false, position: 3)

      create_macro(title: 'BC', owner: user, active: false, position: 2)
      create_macro(title: 'AB', owner: user, active: true,  position: 1)
      create_macro(title: 'AD', owner: user, active: false, position: 3)
    end

    describe 'when `sort_order` is ascending' do
      let(:options) { {sort_by: 'position', sort_order: 'asc'} }

      it 'sorts them appropriately' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
            {owner_type: 'Account', active: false, title: 'AB', position: 2},
            {owner_type: 'Group',   active: false, title: 'BC', position: 2},
            {owner_type: 'Account', active: true,  title: 'ZB', position: 2},
            {owner_type: 'Account', active: true,  title: 'AA', position: 3},
            {owner_type: 'Group',   active: false, title: 'AB', position: 3},
            {owner_type: 'Group',   active: true,  title: 'CA', position: 4},
            {owner_type: 'User',    active: true,  title: 'AB', position: 1},
            {owner_type: 'User',    active: false, title: 'BC', position: 2},
            {owner_type: 'User',    active: false, title: 'AD', position: 3}
          ],
          sort.order(scope)
        )
      end
    end

    describe 'when `sort_order` is descending' do
      let(:options) { {sort_by: 'position', sort_order: 'desc'} }

      it 'sorts them appropriately' do
        assert_rules_equal(
          [
            {owner_type: 'User',    active: false, title: 'AD', position: 3},
            {owner_type: 'User',    active: false, title: 'BC', position: 2},
            {owner_type: 'User',    active: true,  title: 'AB', position: 1},
            {owner_type: 'Group',   active: true,  title: 'CA', position: 4},
            {owner_type: 'Group',   active: false, title: 'AB', position: 3},
            {owner_type: 'Account', active: true,  title: 'AA', position: 3},
            {owner_type: 'Account', active: true,  title: 'ZB', position: 2},
            {owner_type: 'Group',   active: false, title: 'BC', position: 2},
            {owner_type: 'Account', active: false, title: 'AB', position: 2},
            {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
          ],
          sort.order(scope)
        )
      end
    end

    describe 'when `sort_order` is not specified' do
      let(:options) { {sort_by: 'position'} }

      it 'sorts them appropriately' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
            {owner_type: 'Account', active: false, title: 'AB', position: 2},
            {owner_type: 'Group',   active: false, title: 'BC', position: 2},
            {owner_type: 'Account', active: true,  title: 'ZB', position: 2},
            {owner_type: 'Account', active: true,  title: 'AA', position: 3},
            {owner_type: 'Group',   active: false, title: 'AB', position: 3},
            {owner_type: 'Group',   active: true,  title: 'CA', position: 4},
            {owner_type: 'User',    active: true,  title: 'AB', position: 1},
            {owner_type: 'User',    active: false, title: 'BC', position: 2},
            {owner_type: 'User',    active: false, title: 'AD', position: 3}

          ],
          sort.order(scope)
        )
      end
    end
  end

  describe '#to_elasticsearch' do
    let(:options) { {sort_by: 'position', sort_order: 'desc'} }

    it 'constructs the correct query' do
      assert_equal(
        'close order_by:owner_filter sort:desc order_by:position sort:desc',
        sort.to_elasticsearch('close')
      )
    end
  end
end
