require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Sort::Usage do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper

  fixtures :all

  let(:described_class) { Zendesk::RuleSelection::Sort::Usage }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }

  let(:scope) { Rule.where(account_id: account.id) }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, options) }

  let(:sort) { described_class.new(context) }

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when `sort_by` is `usage_1h`' do
      let(:options) { {sort_by: 'usage_1h'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `sort_by` is `usage_24h`' do
      let(:options) { {sort_by: 'usage_24h'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `sort_by` is `usage_7d`' do
      let(:options) { {sort_by: 'usage_7d'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `sort_by` is `usage_30d`' do
      let(:options) { {sort_by: 'usage_30d'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `sort_by` is not a usage parameter' do
      let(:options) { {sort_by: 'position'} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#order' do
    let(:rule_1) { Rule.where(position: 1).first }
    let(:rule_2) { Rule.where(position: 2).first }
    let(:rule_3) { Rule.where(position: 3).first }
    let(:rule_4) { Rule.where(position: 4).first }

    before do
      Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

      Macro.destroy_all

      Timecop.freeze

      create_macro(title: 'ZB', owner: account, active: false, position: 2)
      create_macro(title: 'ZA', owner: account, active: true,  position: 1)
      create_macro(title: 'AB', owner: account, active: false, position: 4)
      create_macro(title: 'AA', owner: account, active: true,  position: 3)

      Timecop.travel(30.minutes.ago) do
        record_usage(type: rule_2.rule_type, id: rule_2.id, count: 3)
        record_usage(type: rule_4.rule_type, id: rule_4.id, count: 2)
        record_usage(type: rule_3.rule_type, id: rule_3.id, count: 1)
      end

      Timecop.travel(6.hours.ago) do
        record_usage(type: rule_4.rule_type, id: rule_4.id, count: 3)
        record_usage(type: rule_3.rule_type, id: rule_3.id, count: 3)
      end

      Timecop.travel(3.days.ago) do
        record_usage(type: rule_3.rule_type, id: rule_3.id, count: 3)
        record_usage(type: rule_2.rule_type, id: rule_2.id, count: 3)
      end

      Timecop.travel(15.days.ago) do
        record_usage(type: rule_1.rule_type, id: rule_1.id, count: 9)
      end
    end

    describe 'when `sort_by` is `usage_1h`' do
      describe 'and `sort_order` is descending' do
        let(:options) { {sort_by: 'usage_1h', sort_order: 'desc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is ascending' do
        let(:options) { {sort_by: 'usage_1h', sort_order: 'asc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is not specified' do
        let(:options) { {sort_by: 'usage_1h'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end
    end

    describe 'when `sort_by` is `usage_24h`' do
      describe 'and `sort_order` is descending' do
        let(:options) { {sort_by: 'usage_24h', sort_order: 'desc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is ascending' do
        let(:options) { {sort_by: 'usage_24h', sort_order: 'asc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AB', position: 4}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is not specified' do
        let(:options) { {sort_by: 'usage_24h'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end
    end

    describe 'when `sort_by` is `usage_7d`' do
      describe 'and `sort_order` is descending' do
        let(:options) { {sort_by: 'usage_7d', sort_order: 'desc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is ascending' do
        let(:options) { {sort_by: 'usage_7d', sort_order: 'asc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is not specified' do
        let(:options) { {sort_by: 'usage_7d'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end
    end

    describe 'when `sort_by` is `usage_30d`' do
      describe 'and `sort_order` is descending' do
        let(:options) { {sort_by: 'usage_30d', sort_order: 'desc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 4}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is ascending' do
        let(:options) { {sort_by: 'usage_30d', sort_order: 'asc'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1}
            ],
            sort.order(scope)
          )
        end
      end

      describe 'and `sort_order` is not specified' do
        let(:options) { {sort_by: 'usage_30d'} }

        it 'sorts them appropriately' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: false, title: 'AB', position: 4}
            ],
            sort.order(scope)
          )
        end
      end
    end
  end
end
