require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::RuleSelection::Sort::Relevance do
  let(:described_class) { Zendesk::RuleSelection::Sort::Relevance }

  let(:user) { users(:minimum_admin) }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user) }

  let(:sort) { described_class.new(context) }

  describe '.valid?' do
    it 'returns true' do
      assert described_class.valid?(context)
    end
  end

  describe '#to_elasticsearch' do
    it 'constructs the correct query' do
      assert_equal 'close', sort.to_elasticsearch('close')
    end
  end
end
