require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::RuleSelection::MacroUsage do
  fixtures :users

  let(:user)    { users(:minimum_agent) }
  let(:account) { user.account }

  let(:redis) { Zendesk::RedisStore.client }

  let(:today)        { Time.utc(2016, 1, 1).to_date }
  let(:record_dates) { (0..7).map { |day| today + day } }

  let(:legacy_macro_usage_key) do
    'rule:macro:usage:for-user:%{user_id}:last-7-days:%{date}'
  end

  let(:macro_usage_key) do
    'rule:macro:usage:for-account:%{account_id}:for-user:%{user_id}:' \
      'last-7-days:%{date}'
  end

  let(:macro_usage) { Zendesk::RuleSelection::MacroUsage.new(user) }

  before do
    Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

    Timecop.freeze(today)
  end

  arturo = :rule_usage_read_from_rules_cluster

  describe_with_and_without_arturo_enabled arturo do
    describe '#most_used' do
      before do
        5.times { macro_usage.record(1) }
        1.times { macro_usage.record(2) }
        2.times { macro_usage.record(3) }
        4.times { macro_usage.record(4) }
        3.times { macro_usage.record(5) }
      end

      it 'returns the specified number of most-used macros in last 7 days' do
        assert_equal [1, 4, 5, 3], macro_usage.most_used(4)
      end

      describe 'when the specified number is not positive' do
        it 'returns the entire set' do
          assert_equal [1, 4, 5, 3, 2], macro_usage.most_used(0)
        end
      end

      describe 'when the specified number is greater than number of macros' do
        it 'returns the entire set' do
          assert_equal [1, 4, 5, 3, 2], macro_usage.most_used(100)
        end
      end

      describe 'when a macro was used more than 7 days in the past' do
        before do
          Timecop.freeze(today - 8) do
            10.times do
              Zendesk::RuleSelection::MacroUsage.new(user).record(6)
            end
          end
        end

        it 'does not factor into the results' do
          assert_equal [1], macro_usage.most_used(1)
        end
      end
    end
  end

  describe '#record' do
    let(:macro_id) { 1 }

    before { macro_usage.record(macro_id) }

    before_should 'expire each date key at the end of the respective date' do
      record_dates.each do |date|
        redis.
          expects(:expireat).
          with(
            format(
              macro_usage_key,
              user_id:    user.id,
              account_id: account.id,
              date:       date
            ),
            date.next_day.to_time.utc.to_i
          ).once

        redis.
          expects(:expireat).
          with(
            format(legacy_macro_usage_key, user_id: user.id, date: date),
            date.next_day.to_time.utc.to_i
          ).once
      end
    end

    it 'counts the usage for the next 7 days' do
      assert(
        record_dates.all? do |date|
          redis.
            zscore(
              format(
                macro_usage_key,
                user_id:    user.id,
                account_id: account.id,
                date:       date
              ),
              macro_id
            ).present?
        end
      )
    end

    it 'does not count the usage after 7 days' do
      refute(
        redis.
          zscore(
            format(
              macro_usage_key,
              user_id:    user.id,
              account_id: account.id,
              date:       record_dates.last.next_day
            ),
            macro_id
          ).present?
      )
    end

    it 'counts the usage for the next 7 days using the legacy key' do
      assert(
        record_dates.all? do |date|
          redis.
            zscore(
              format(
                legacy_macro_usage_key,
                user_id: user.id,
                date:    date
              ),
              macro_id
            ).present?
        end
      )
    end

    it 'does not count the usage after 7 days using the legacy key' do
      refute(
        redis.
          zscore(
            format(
              legacy_macro_usage_key,
              user_id: user.id,
              date:    record_dates.last.next_day
            ),
            macro_id
          ).present?
      )
    end
  end

  describe '#record_many' do
    let(:macro_ids) { [1, 2] }

    before { macro_usage.record_many(macro_ids) }

    before_should 'expire each date key at the end of the respective date' do
      macro_ids.each do
        record_dates.each do |date|
          redis.
            expects(:expireat).
            with(
              format(
                macro_usage_key,
                user_id:    user.id,
                account_id: account.id,
                date:       date
              ),
              date.next_day.to_time.utc.to_i
            ).once

          redis.
            expects(:expireat).
            with(
              format(
                legacy_macro_usage_key,
                user_id: user.id,
                date:    date
              ),
              date.next_day.to_time.utc.to_i
            ).once
        end
      end
    end

    it 'counts the usage for the next 7 days' do
      macro_ids.each do |macro_id|
        assert(
          record_dates.all? do |date|
            redis.
              zscore(
                format(
                  macro_usage_key,
                  user_id:    user.id,
                  account_id: account.id,
                  date:       date
                ),
                macro_id
              ).present?
          end
        )
      end
    end

    it 'does not count the usage after 7 days' do
      macro_ids.each do |macro_id|
        refute(
          redis.
          zscore(
            format(
              macro_usage_key,
              user_id:    user.id,
              account_id: account.id,
              date:       record_dates.last.next_day
            ),
            macro_id
          ).present?
        )
      end
    end

    it 'counts the usage for the next 7 days using the legacy key' do
      macro_ids.each do |macro_id|
        assert(
          record_dates.all? do |date|
            redis.
              zscore(
                format(
                  legacy_macro_usage_key,
                  user_id: user.id,
                  date:    date
                ),
                macro_id
              ).present?
          end
        )
      end
    end

    it 'does not count the usage after 7 days using the legacy key' do
      macro_ids.each do |macro_id|
        refute(
          redis.
          zscore(
            format(
              legacy_macro_usage_key,
              user_id: user.id,
              date:    record_dates.last.next_day
            ),
            macro_id
          ).present?
        )
      end
    end
  end
end
