require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Context::View do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:user)    { users(:minimum_admin) }
  let(:account) { user.account }

  let(:context) { Zendesk::RuleSelection::Context::View.new(user, options) }

  let(:options) do
    {
      active:     false,
      access:     'shared',
      group_id:   123,
      sort_order: 'desc'
    }
  end

  describe '#options' do
    it 'returns an options object' do
      assert_equal(
        Zendesk::RuleSelection::Context::Options,
        context.options.class
      )
    end
  end

  describe '#user' do
    it 'returns the user' do
      assert_equal user, context.user
    end
  end

  describe '#default_sort' do
    it 'returns `owner_position`' do
      assert_equal :owner_position, context.default_sort
    end
  end

  describe '#foreign_key' do
    it 'returns `view_id`' do
      assert_equal :view_id, context.foreign_key
    end
  end

  describe '#group_table' do
    it 'returns the group-view arel table' do
      assert_equal GroupView.arel_table, context.group_table
    end
  end

  describe '#modern_groups?' do
    it 'returns true' do
      assert context.modern_groups?
    end

    describe_with_arturo_disabled :multiple_group_views_reads do
      it 'returns false' do
        refute context.modern_groups?
      end
    end
  end

  describe '#user_setting_sort?' do
    describe_with_arturo_enabled :user_level_shared_view_ordering do
      it 'returns true' do
        assert context.user_setting_sort?
      end
    end

    describe_with_arturo_disabled :user_level_shared_view_ordering do
      it 'returns false' do
        refute context.user_setting_sort?
      end
    end
  end

  describe '#option_filters' do
    it 'returns the allowed option filters' do
      assert_equal %i[access active group], context.option_filters.to_a
    end
  end

  describe '#reorder_filters' do
    it 'returns the allowed reorder user filters' do
      assert_equal %i[personal shared], context.reorder_filters.to_a
    end
  end

  describe '#rules' do
    let(:group) { groups(:minimum_group) }
    let(:view)  { create_view(owner: group) }

    before { GroupView.destroy_all }

    it 'returns all of the views associated with the account' do
      assert_equal View.where(account_id: account.id).to_a, context.rules
    end

    it 'includes group-view associations' do
      assert_equal(
        view,
        context.rules.where('rules.id = groups_views.view_id').first
      )
    end

    describe_with_arturo_disabled :multiple_group_views_reads do
      let(:error) { ActiveRecord::StatementInvalid }

      it 'does not include group-view associations' do
        assert_raises error do
          context.rules.where('rules.id = groups_views.view_id').first
        end
      end
    end
  end

  describe '#sorts' do
    it 'returns the allowed sorts' do
      assert_equal(
        %i[user_setting alphabetical owner_position timestamp default],
        context.sorts.to_a
      )
    end
  end

  describe '#search_sorts' do
    it 'returns the allowed sorts' do
      assert_equal(
        %i[alphabetical owner_position timestamp relevance],
        context.search_sorts.to_a
      )
    end
  end

  describe '#table' do
    it 'returns the view arel table' do
      assert_equal ::View.arel_table, context.table
    end
  end

  describe '#type' do
    it 'returns the view class' do
      assert_equal ::View, context.type
    end
  end

  describe '#user_filters' do
    it 'returns the allowed user filters' do
      assert_equal(
        [
          Zendesk::RuleSelection::UserFilter::Global,
          Zendesk::RuleSelection::UserFilter::Group,
          Zendesk::RuleSelection::UserFilter::Limited
        ],
        context.user_filters.to_a
      )
    end
  end
end
