require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::RuleSelection::Context::Options do
  let(:options) { Zendesk::RuleSelection::Context::Options.new(params) }

  describe '#access' do
    describe 'when `access` is passed' do
      describe 'and the value is `account`' do
        let(:params) { {access: 'account'} }

        it 'returns the `account` access mapping' do
          assert_equal 'Account', options.access
        end
      end

      describe 'and the value is `personal`' do
        let(:params) { {access: 'personal'} }

        it 'returns the `personal` access mapping' do
          assert_equal 'User', options.access
        end
      end

      describe 'and the value is `shared`' do
        let(:params) { {access: 'shared'} }

        it 'returns the `shared` access mapping' do
          assert_equal %w[Account Group], options.access
        end
      end

      describe 'and the value is not valid' do
        let(:params) { {access: 'group'} }

        it 'returns nil' do
          assert_nil options.access
        end
      end
    end

    describe 'when `access` is not passed' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.access
      end
    end
  end

  describe '#access?' do
    describe 'when `access` is valid' do
      let(:params) { {access: 'account'} }

      it 'returns true' do
        assert options.access?
      end
    end

    describe 'when `access` is not valid' do
      let(:params) { {} }

      it 'returns false' do
        refute options.access?
      end
    end
  end

  describe '#active' do
    describe 'when `active` is passed' do
      let(:params) { {active: true} }

      it 'returns the active value' do
        assert options.active
      end
    end

    describe 'when `active` is not passed' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.active
      end
    end
  end

  describe '#active?' do
    describe 'when `active` is valid' do
      let(:params) { {active: false} }

      it 'returns true' do
        assert options.active?
      end
    end

    describe 'when `active` is not valid' do
      let(:params) { {} }

      it 'returns false' do
        refute options.active?
      end
    end
  end

  describe '#category' do
    describe 'when `category` is passed' do
      let(:params) { {category: 'category'} }

      it 'returns the category value' do
        assert_equal 'category', options.category
      end
    end

    describe 'when `category` is not passed' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.category
      end
    end
  end

  describe '#category?' do
    describe 'when `category` is valid' do
      let(:params) { {category: 'category'} }

      it 'returns true' do
        assert options.category?
      end

      describe 'and `category` is blank' do
        let(:params) { {category: ''} }

        it 'returns true' do
          assert options.category?
        end
      end
    end

    describe 'when `category` is not valid' do
      let(:params) { {} }

      it 'returns false' do
        refute options.category?
      end
    end
  end

  describe '#group_id' do
    describe 'when `group_id` is specified' do
      let(:params) { {group_id: 7} }

      it 'returns the group ID value' do
        assert_equal 7, options.group_id
      end
    end

    describe 'when `group_id` is not specified' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.group_id
      end
    end
  end

  describe '#group?' do
    describe 'when `group_id` is valid' do
      let(:params) { {group_id: 7} }

      it 'returns true' do
        assert options.group?
      end
    end

    describe 'when `group_id` is not valid' do
      let(:params) { {} }

      it 'returns false' do
        refute options.group?
      end
    end
  end

  describe '#offset' do
    describe 'when pagination parameters are specified' do
      let(:params) { {per_page: 5, page: 3} }

      it 'returns the offset value' do
        assert_equal 10, options.offset
      end
    end

    describe 'when pagination parameters are not specified' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.per_page
      end
    end
  end

  describe '#page' do
    describe 'when `page` is specified' do
      let(:params) { {page: 1} }

      it 'returns the page value' do
        assert_equal 1, options.page
      end
    end

    describe 'when `page` is not specified' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.page
      end
    end
  end

  describe '#per_page' do
    describe 'when `per_page` is specified' do
      let(:params) { {per_page: 5} }

      it 'returns the per-page value' do
        assert_equal 5, options.per_page
      end
    end

    describe 'when `per_page` is not specified' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.per_page
      end
    end
  end

  describe '#query' do
    describe 'when `query` is specified' do
      let(:params) { {query: 'test query'} }

      it 'returns the query value' do
        assert_equal 'test query', options.query
      end
    end

    describe 'when `query` is not specified' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.query
      end
    end
  end

  describe '#sort_by' do
    describe 'when `sort_by` is specified' do
      let(:params) { {sort_by: 'alphabetical'} }

      it 'returns the sort-by value' do
        assert_equal 'alphabetical', options.sort_by
      end
    end

    describe 'when `sort_by` is not specified' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.sort_by
      end
    end
  end

  describe '#sort_by?' do
    let(:params) { {sort_by: 'usage'} }

    describe 'when `sort_by` is the specified sort' do
      it 'returns true' do
        assert options.sort_by?(:usage)
      end
    end

    describe 'when `sort_by` is not the specified sort' do
      it 'returns false' do
        refute options.sort_by?(:alphabetical)
      end
    end
  end

  describe 'sort_order' do
    describe 'when `sort_order` is ascending' do
      describe 'and is lowercase' do
        let(:params) { {sort_order: 'asc'} }

        it 'returns `asc`' do
          assert_equal 'asc', options.sort_order
        end
      end

      describe 'and is uppercase' do
        let(:params) { {sort_order: 'ASC'} }

        it 'returns `asc`' do
          assert_equal 'asc', options.sort_order
        end
      end
    end

    describe 'when `sort_order` is descending' do
      describe 'and is lowercase' do
        let(:params) { {sort_order: 'desc'} }

        it 'returns `desc`' do
          assert_equal 'desc', options.sort_order
        end
      end

      describe 'and is uppercase' do
        let(:params) { {sort_order: 'DESC'} }

        it 'returns `desc`' do
          assert_equal 'desc', options.sort_order
        end
      end
    end

    describe 'when `sort_order` is not valid' do
      let(:params) { {sort_order: 'up'} }

      it 'returns nil' do
        assert_nil options.sort_order
      end
    end

    describe 'when `sort_order` is not specified' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.sort_order
      end
    end
  end

  describe '#rules_category_id' do
    describe 'when `category_id` is passed' do
      let(:params) { {category_id: 123} }

      it 'returns the category ID value' do
        assert options.rules_category_id
      end
    end

    describe 'when `category_id` is not passed' do
      let(:params) { {} }

      it 'returns nil' do
        assert_nil options.rules_category_id
      end
    end
  end

  describe '#rules_category_id?' do
    describe 'when `category_id` is specified' do
      let(:params) { {category_id: 123} }

      it 'returns true' do
        assert options.rules_category_id?
      end
    end

    describe 'when `category_id` is not specified' do
      let(:params) { {} }

      it 'returns false' do
        refute options.rules_category_id?
      end
    end
  end

  describe '#categories_enabled?' do
    describe 'when `trigger_categories_api` is true' do
      let(:params) { {trigger_categories_api: true} }

      it 'returns true' do
        assert options.categories_enabled?
      end
    end

    describe 'when `trigger_categories_api` is false' do
      let(:params) { {trigger_categories_api: false} }

      it 'returns false' do
        refute options.categories_enabled?
      end
    end

    describe 'when `trigger_categories_api` is not passed' do
      let(:params) { {} }

      it 'returns false' do
        refute options.categories_enabled?
      end
    end
  end
end
