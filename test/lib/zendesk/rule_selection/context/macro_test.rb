require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Context::Macro do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:user)    { users(:minimum_admin) }
  let(:account) { user.account }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, options) }

  let(:options) do
    {
      access:     'personal',
      active:     false,
      category:   'test',
      group_id:   123,
      sort_order: 'asc'
    }
  end

  describe '#options' do
    it 'returns an options object' do
      assert_equal(
        Zendesk::RuleSelection::Context::Options,
        context.options.class
      )
    end
  end

  describe '#user' do
    it 'returns the user' do
      assert_equal user, context.user
    end
  end

  describe '#default_sort' do
    describe 'when the `macro_order` setting is `alphabetical`' do
      before do
        account.settings.set(macro_order: 'alphabetical')

        account.settings.save!
      end

      it 'returns `alphabetical`' do
        assert_equal :alphabetical, context.default_sort
      end
    end

    describe 'when the `macro_order` setting is `position`' do
      before do
        account.settings.set(macro_order: 'position')

        account.settings.save!
      end

      it 'returns `owner_position`' do
        assert_equal :owner_position, context.default_sort
      end
    end
  end

  describe '#foreign_key' do
    it 'returns `macro_id`' do
      assert_equal :macro_id, context.foreign_key
    end
  end

  describe '#group_table' do
    it 'returns the group-macro arel table' do
      assert_equal GroupMacro.arel_table, context.group_table
    end
  end

  describe '#modern_groups?' do
    it 'returns true' do
      assert context.modern_groups?
    end
  end

  describe '#option_filters' do
    it 'returns the allowed option filters' do
      assert_equal %i[access active category group], context.option_filters.to_a
    end
  end

  describe '#reorder_filters' do
    it 'returns the allowed reorder user filters' do
      assert_equal %i[personal shared], context.reorder_filters.to_a
    end
  end

  describe '#sorts' do
    it 'returns the allowed sorts' do
      assert_equal(
        %i[alphabetical owner_position timestamp usage default],
        context.sorts.to_a
      )
    end
  end

  describe '#search_sorts' do
    it 'returns the allowed sorts' do
      assert_equal(
        %i[alphabetical owner_position timestamp relevance],
        context.search_sorts.to_a
      )
    end
  end

  describe '#rules' do
    let(:group) { groups(:minimum_group) }
    let(:macro) { create_macro(title: 'AA', owner: group) }

    before { GroupMacro.destroy_all }

    it 'returns all of the macros associated with the account' do
      assert_equal Macro.where(account_id: account.id).to_a, context.rules
    end

    it 'includes group-macro associations' do
      assert_equal(
        macro,
        context.rules.where('rules.id = groups_macros.macro_id').first
      )
    end
  end

  describe '#table' do
    it 'returns the macro arel table' do
      assert_equal Macro.arel_table, context.table
    end
  end

  describe '#type' do
    it 'returns the macro class' do
      assert_equal Macro, context.type
    end
  end

  describe '#user_filters' do
    it 'returns the allowed user filters' do
      assert_equal(
        [
          Zendesk::RuleSelection::UserFilter::Global,
          Zendesk::RuleSelection::UserFilter::Group,
          Zendesk::RuleSelection::UserFilter::Limited
        ],
        context.user_filters.to_a
      )
    end
  end

  describe '#user_setting_sort?' do
    it 'returns false' do
      refute context.user_setting_sort?
    end
  end
end
