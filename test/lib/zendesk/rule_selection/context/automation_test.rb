require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 3

describe Zendesk::RuleSelection::Context::Automation do
  fixtures :accounts,
    :groups,
    :users

  let(:user)    { users(:minimum_admin) }
  let(:account) { user.account }

  let(:context) do
    Zendesk::RuleSelection::Context::Automation.new(user, options)
  end

  let(:options) do
    {
      access:     'personal',
      active:     false,
      group_id:   123,
      sort_order: 'asc'
    }
  end

  describe '#options' do
    it 'returns an options object' do
      assert_equal(
        Zendesk::RuleSelection::Context::Options,
        context.options.class
      )
    end
  end

  describe '#user' do
    it 'returns the user' do
      assert_equal user, context.user
    end
  end

  describe '#default_sort' do
    it 'returns `position`' do
      assert_equal :position, context.default_sort
    end
  end

  describe '#option_filters' do
    it 'returns the allowed option filters' do
      assert_equal %i[active], context.option_filters.to_a
    end
  end

  describe '#reorder_filters' do
    it 'returns the allowed reorder user filters' do
      assert_equal %i[account], context.reorder_filters.to_a
    end
  end

  describe '#sorts' do
    it 'returns the allowed sorts' do
      assert_equal(
        %i[alphabetical position timestamp usage default],
        context.sorts.to_a
      )
    end
  end

  describe '#search_sorts' do
    it 'returns the allowed sorts' do
      assert_equal(
        %i[alphabetical position timestamp relevance],
        context.search_sorts.to_a
      )
    end
  end

  describe '#user_filters' do
    it 'returns the allowed user filters' do
      assert_equal(
        [
          Zendesk::RuleSelection::UserFilter::Account,
          Zendesk::RuleSelection::UserFilter::Active
        ],
        context.user_filters.to_a
      )
    end
  end

  describe '#user_setting_sort?' do
    it 'returns false' do
      refute context.user_setting_sort?
    end
  end
end
