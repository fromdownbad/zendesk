require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 3

describe Zendesk::RuleSelection::Context::Trigger do
  fixtures :accounts,
    :groups,
    :users

  let(:user)    { users(:minimum_admin) }
  let(:account) { user.account }

  let(:context) { Zendesk::RuleSelection::Context::Trigger.new(user, options) }

  let(:options) do
    {
      active:     false,
      access:     'shared',
      group_id:   123,
      sort_order: 'desc'
    }
  end

  describe '#options' do
    it 'returns an options object' do
      assert_equal(
        Zendesk::RuleSelection::Context::Options,
        context.options.class
      )
    end
  end

  describe '#user' do
    it 'returns the user' do
      assert_equal user, context.user
    end
  end

  describe '#default_sort' do
    it 'defaults the sort to `position`' do
      assert_equal :position, context.default_sort
    end
  end

  describe '#option_filters' do
    describe 'when categories are enabled' do
      describe 'and `trigger_categories_api` is enabled' do
        let(:options) { { trigger_categories_api: true } }

        it 'returns the allowed option filters' do
          assert_equal %i[active rules_category_id], context.option_filters.to_a
        end
      end

      describe 'and `trigger_categories_api` is not enabled' do
        let(:options) { { trigger_categories_api: false } }

        it 'returns the allowed option filters' do
          assert_equal %i[active], context.option_filters.to_a
        end
      end
    end

    describe 'when categories are not enabled' do
      it 'returns the allowed option filters' do
        assert_equal %i[active], context.option_filters.to_a
      end
    end
  end

  describe '#reorder_filters' do
    it 'returns the allowed reorder user filters' do
      assert_equal %i[account], context.reorder_filters.to_a
    end
  end

  describe '#sorts' do
    it 'returns the allowed sorts' do
      assert_equal(
        %i[alphabetical position timestamp usage default],
        context.sorts.to_a
      )
    end
  end

  describe '#search_sorts' do
    it 'returns the allowed sorts' do
      assert_equal(
        %i[alphabetical position timestamp relevance],
        context.search_sorts.to_a
      )
    end
  end

  describe '#user_filters' do
    it 'returns the allowed user filters' do
      assert_equal(
        [
          Zendesk::RuleSelection::UserFilter::Account,
          Zendesk::RuleSelection::UserFilter::Active
        ],
        context.user_filters.to_a
      )
    end
  end

  describe '#user_setting_sort?' do
    it 'returns false' do
      refute context.user_setting_sort?
    end
  end
end
