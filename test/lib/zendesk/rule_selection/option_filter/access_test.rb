require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::OptionFilter::Access do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::OptionFilter::Access }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }
  let(:group)   { groups(:minimum_group) }

  let(:context) do
    Zendesk::RuleSelection::Context::Macro.new(user, options)
  end

  let(:access_filter) { described_class.new(context) }

  let(:rule) do
    Macro.create!(
      account:    account,
      title:      "#{owner.class} Rule",
      owner:      owner,
      definition: build_macro_definition,
      is_active:  true
    )
  end

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when context has `access`' do
      let(:options) { {access: access_key} }

      describe 'and `access` is `account`' do
        let(:access_key) { 'account' }

        it 'returns true' do
          assert described_class.valid?(context)
        end
      end

      describe 'and `access` is `personal`' do
        let(:access_key) { 'personal' }

        it 'returns true' do
          assert described_class.valid?(context)
        end
      end

      describe 'and `access` is `shared`' do
        let(:access_key) { 'shared' }

        it 'returns true' do
          assert described_class.valid?(context)
        end
      end

      describe 'and `access` is other' do
        let(:access_key) { 'full' }

        it 'returns false' do
          refute described_class.valid?(context)
        end
      end
    end

    describe 'when context does not have `access`' do
      let(:options) { {} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#to_arel' do
    describe 'when `access` is `account`' do
      let(:options) { {access: 'account'} }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].in(%w[Account]).to_sql,
          access_filter.to_arel.to_sql
        )
      end
    end

    describe 'when `access` is `personal`' do
      let(:options) { {access: 'personal'} }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].in(%w[User]).to_sql,
          access_filter.to_arel.to_sql
        )
      end
    end

    describe 'when `access` is `shared`' do
      let(:options) { {access: 'shared'} }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].in(%w[Account Group]).to_sql,
          access_filter.to_arel.to_sql
        )
      end
    end
  end

  describe '#to_elasticsearch' do
    describe 'when `access` is `account`' do
      let(:options) { {access: 'account'} }

      it 'returns the correct Elasticsearch query string' do
        assert_equal '(owner_type:(Account))', access_filter.to_elasticsearch
      end
    end

    describe 'when `access` is `personal`' do
      let(:options) { {access: 'personal'} }

      it 'returns the correct Elasticsearch query string' do
        assert_equal '(owner_type:(User))', access_filter.to_elasticsearch
      end
    end

    describe 'when `access` is `shared`' do
      let(:options) { {access: 'shared'} }

      it 'returns the correct Elasticsearch query string' do
        assert_equal(
          '(owner_type:(Account OR Group))',
          access_filter.to_elasticsearch
        )
      end
    end
  end
end
