require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::OptionFilter::Category do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::OptionFilter::Category }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }
  let(:group)   { groups(:minimum_group) }

  let(:context) do
    Zendesk::RuleSelection::Context::Macro.new(user, options)
  end

  let(:category_filter) { described_class.new(context) }

  let(:rule) do
    Macro.create!(
      account:    account,
      title:      title,
      owner:      account,
      definition: build_macro_definition,
      is_active:  true
    )
  end

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when context has `category`' do
      let(:options) { {category: 'category'} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when `category` is empty' do
      let(:options) { {category: ''} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when context does not have `category`' do
      let(:options) { {} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#to_arel' do
    let(:options) { {category: 'category'} }

    it 'returns the correct arel' do
      assert_equal(
        context.table[:title].matches('category::%').
          or(context.table[:title].eq('category')).
          to_sql,
        category_filter.to_arel.to_sql
      )
    end
  end

  describe '#to_elasticsearch' do
    let(:options) { {category: 'CAT'} }

    it 'returns the correct Elasticsearch filter query' do
      assert_equal 'category:"CAT"', category_filter.to_elasticsearch
    end

    describe 'when the category is the empty string' do
      let(:options) { {category: ''} }

      it 'returns the correct Elasticsearch filter query' do
        assert_equal 'category:""', category_filter.to_elasticsearch
      end
    end

    describe 'when the category contains multiple words' do
      let(:options) { {category: 'Pacific Standard Time'} }

      it 'returns the correct Elasticsearch filter query' do
        assert_equal(
          'category:"Pacific Standard Time"',
          category_filter.to_elasticsearch
        )
      end
    end
  end
end
