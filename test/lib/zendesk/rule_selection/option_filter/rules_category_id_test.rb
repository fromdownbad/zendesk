require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered! uncovered: 1

describe Zendesk::RuleSelection::OptionFilter::RulesCategoryId do
  include TestSupport::Rule::Helper

  fixtures :accounts, :users

  let(:described_class) { Zendesk::RuleSelection::OptionFilter::RulesCategoryId }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:context) do
    Zendesk::RuleSelection::Context::Trigger.new(user, options)
  end

  let(:rules_category_id_filter) { described_class.new(context) }

  let(:rule_category_id) { create_category.id }
  let(:rule) { create_trigger(rules_category_id: rules_category_id) }

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when context has `category_id`' do
      let(:options) { {category_id: rule_category_id} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when context does not have `category_id`' do
      let(:options) { {} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#to_arel' do
    let(:options) { {category_id: rule_category_id} }

    it 'returns the correct arel' do
      assert_equal(
        context.table[:rules_category_id].eq(rule_category_id).to_sql,
        rules_category_id_filter.to_arel.to_sql
      )
    end
  end
end
