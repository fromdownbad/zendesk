require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::RuleSelection::OptionFilter::Group do
  fixtures :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::OptionFilter::Group }

  let(:user) { users(:minimum_admin) }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, options) }

  let(:group_filter) { described_class.new(context) }

  describe '.valid?' do
    describe 'when context has `group_id`' do
      let(:options) { {group_id: 1} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when context does not have `group_id`' do
      let(:options) { {} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#to_arel' do
    let(:options) { {group_id: 7} }

    describe_with_arturo_enabled :multiple_group_views_reads do
      it 'returns the correct arel' do
        assert_equal(
          context.table[:id].eq(context.group_table[context.foreign_key]).
            and(context.group_table[:group_id].eq(7)).
            and(context.table[:owner_type].eq('Group')).
            to_sql,
          group_filter.to_arel.to_sql
        )
      end
    end

    describe_with_arturo_disabled :multiple_group_views_reads do
      it 'returns the correct arel' do
        assert_equal(
          context.table[:id].eq(context.group_table[context.foreign_key]).
            and(context.group_table[:group_id].eq(7)).
            and(context.table[:owner_type].eq('Group')).
            and(context.table[:owner_id].in([7, Rule.group_owner_id])).
            to_sql,
          group_filter.to_arel.to_sql
        )
      end
    end

    describe 'when using legacy groups' do
      before { context.stubs(modern_groups?: false) }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Group').
            and(context.table[:owner_id].eq(7)).
            to_sql,
          group_filter.to_arel.to_sql
        )
      end
    end
  end

  describe '#to_elasticsearch' do
    let(:options) { {group_id: 7} }

    it 'returns the correct Elasticsearch filter query' do
      assert_equal(
        '(owner_type:Group AND group_ids:7)',
        group_filter.to_elasticsearch
      )
    end

    describe 'when using legacy groups' do
      before { context.stubs(modern_groups?: false) }

      it 'returns the correct Elasticsearch filter query' do
        assert_equal(
          '(owner_type:Group AND owner_id:7)',
          group_filter.to_elasticsearch
        )
      end
    end
  end
end
