require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::OptionFilter::Active do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::OptionFilter::Active }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }
  let(:group)   { groups(:minimum_group) }

  let(:context) do
    Zendesk::RuleSelection::Context::Macro.new(user, options)
  end

  let(:active_filter) { described_class.new(context) }

  let(:rule) do
    Macro.create!(
      account:    account,
      title:      'Rule',
      owner:      account,
      definition: build_macro_definition,
      is_active:  is_active
    )
  end

  before { Rule.destroy_all }

  describe '.valid?' do
    describe 'when context has `active`' do
      let(:options) { {active: true} }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when context does not have `active`' do
      let(:options) { {} }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#to_arel' do
    describe 'when `active` is true' do
      let(:options) { {active: true} }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:is_active].eq(true).to_sql,
          active_filter.to_arel.to_sql
        )
      end
    end

    describe 'when `active` is false' do
      let(:options) { {active: false} }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:is_active].eq(false).to_sql,
          active_filter.to_arel.to_sql
        )
      end
    end
  end

  describe '#to_elasticsearch' do
    describe 'when `active` is true' do
      let(:options) { {active: true} }

      it 'returns the correct Elasticsearch filter query' do
        assert_equal 'is_active:true', active_filter.to_elasticsearch
      end
    end

    describe 'when `active` is false' do
      let(:options) { {active: false} }

      it 'returns the correct Elasticsearch filter query' do
        assert_equal 'is_active:false', active_filter.to_elasticsearch
      end
    end
  end
end
