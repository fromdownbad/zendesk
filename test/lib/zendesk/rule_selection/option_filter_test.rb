require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::RuleSelection::OptionFilter do
  fixtures :users

  let(:described_class) { Zendesk::RuleSelection::OptionFilter }

  let(:user)    { users(:minimum_agent) }
  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, options) }

  describe '.for_context' do
    describe 'when the context is valid for `access`' do
      let(:options) { {access: 'shared'} }

      it 'includes an `access` option filter' do
        assert(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::Access)
        )
      end
    end

    describe 'when the context is not valid for `access`' do
      let(:options) { {access: 'invalid'} }

      it 'does not include an `access` option filter' do
        refute(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::Access)
        )
      end
    end

    describe 'when the context is valid for `active`' do
      let(:options) { {active: true} }

      it 'includes an `active` option filter' do
        assert(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::Active)
        )
      end
    end

    describe 'when the context is not valid for `active`' do
      let(:options) { {} }

      it 'does not include an `active` option filter' do
        refute(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::Active)
        )
      end
    end

    describe 'when the context is valid for `category`' do
      let(:options) { {category: 'email'} }

      it 'includes a `category` option filter' do
        assert(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::Category)
        )
      end
    end

    describe 'when the context is not valid for `category`' do
      let(:options) { {} }

      it 'does not include a `category` option filter' do
        refute(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::Category)
        )
      end
    end

    describe 'when the context is valid for `group`' do
      let(:options) { {group_id: 123} }

      it 'includes a `group` option filter' do
        assert(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::Group)
        )
      end
    end

    describe 'when the context is not valid for `group`' do
      let(:options) { {} }

      it 'does not include a `group` option filter' do
        refute(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::Group)
        )
      end
    end

    describe 'when the context is valid for `rules_category_id`' do
      let(:context) { Zendesk::RuleSelection::Context::Trigger.new(user, options) }
      let(:options) { {category_id: 123, trigger_categories_api: true} }

      it 'includes a `rules_category_id` option filter' do
        assert(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::RulesCategoryId)
        )
      end
    end

    describe 'when the context is not valid for `rules_category_id`' do
      let(:options) { {} }

      it 'does not include a `rules_category_id` option filter' do
        refute(
          described_class.for_context(context).
            map(&:class).
            include?(Zendesk::RuleSelection::OptionFilter::RulesCategoryId)
        )
      end
    end
  end
end
