require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::RuleSelection::Context do
  describe '.access_mapping' do
    it 'returns the access mapping' do
      assert_equal(
        {
          'account'  => 'Account',
          'personal' => 'User',
          'shared'   => %w[Account Group]
        },
        Zendesk::RuleSelection::Context.access_mapping
      )
    end
  end
end
