require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 2

describe Zendesk::RuleSelection::UserFilter do
  fixtures :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::UserFilter }

  describe '.edit' do
    it 'returns the edit permission-based user filters' do
      assert_equal(
        [
          Zendesk::RuleSelection::UserFilter::Account,
          Zendesk::RuleSelection::UserFilter::Active
        ],
        described_class.edit.to_a
      )
    end
  end

  describe '.role' do
    it 'returns the role permission-based user filters' do
      assert_equal(
        [
          Zendesk::RuleSelection::UserFilter::Global,
          Zendesk::RuleSelection::UserFilter::Group,
          Zendesk::RuleSelection::UserFilter::Limited
        ],
        described_class.role.to_a
      )
    end
  end

  describe '.for_context' do
    let(:user)           { users(:minimum_agent) }
    let(:permission_set) { build_valid_permission_set }

    before { user.stubs(has_permission_set?: true) }

    describe 'when the rule has role-based permissions' do
      let(:context) { Zendesk::RuleSelection::Context::Macro.new(user) }

      before do
        user.account.subscription.stubs(has_group_rules?: true)
        user.account.subscription.stubs(has_personal_rules?: true)

        permission_set.permissions.macro_access = macro_access

        user.stubs(permission_set: permission_set)
      end

      describe 'when the `global` user filter is valid' do
        let(:macro_access) { 'full' }

        it 'creates a `global` user filter' do
          assert_equal(
            Zendesk::RuleSelection::UserFilter::Global,
            described_class.for_context(context).class
          )
        end
      end

      describe 'when the `global` user filter is not valid' do
        describe 'and the `group` user filter is valid' do
          let(:macro_access) { 'manage-group' }

          it 'creates a `group` user filter' do
            assert_equal(
              Zendesk::RuleSelection::UserFilter::Group,
              described_class.for_context(context).class
            )
          end
        end

        describe 'and the `group` user filter is not valid' do
          let(:macro_access) { 'readonly' }

          it 'creates a `limited` user filter' do
            assert_equal(
              Zendesk::RuleSelection::UserFilter::Limited,
              described_class.for_context(context).class
            )
          end
        end
      end
    end

    describe 'when the rule does not have role-based permissions' do
      let(:context) { Zendesk::RuleSelection::Context::Automation.new(user) }

      before do
        user.account.subscription.stubs(has_unlimited_automations?: true)

        permission_set.permissions.business_rule_management = rule_management

        user.stubs(permission_set: permission_set)
      end

      describe 'and the `account` user filter is valid' do
        let(:rule_management) { true }

        it 'creates an `account` user filter' do
          assert_equal(
            Zendesk::RuleSelection::UserFilter::Account,
            described_class.for_context(context).class
          )
        end
      end

      describe 'and the `account` user filter is not valid' do
        let(:rule_management) { false }

        it 'creates an `active` user filter' do
          assert_equal(
            Zendesk::RuleSelection::UserFilter::Active,
            described_class.for_context(context).class
          )
        end
      end
    end
  end
end
