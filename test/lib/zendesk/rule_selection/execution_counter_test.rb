require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::ExecutionCounter do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }

  let(:rule_1) { Trigger.where(title: 'rule_1').first }
  let(:rule_2) { Trigger.where(title: 'rule_2').first }
  let(:rule_3) { Trigger.where(title: 'rule_3').first }

  let(:collection) { [rule_1, rule_2, rule_3] }

  let(:execution_counter) do
    Zendesk::RuleSelection::ExecutionCounter.new(
      account: account,
      type:    :trigger
    )
  end

  before do
    Timecop.freeze

    Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

    Trigger.destroy_all

    create_trigger(title: 'rule_1')
    create_trigger(title: 'rule_2')
    create_trigger(title: 'rule_3')

    [30.minutes.ago, 10.hours.ago, 4.days.ago].each do |time|
      Timecop.travel(time) do
        record_usage(type: :trigger, id: rule_1.id)
      end
    end

    6.times do |i|
      Timecop.travel(i.days.ago - 12.hours) do
        record_usage(type: :trigger, id: rule_2.id)
      end
    end
  end

  describe '#rules_with_execution_counts' do
    let(:rules_with_counts) do
      execution_counter.rules_with_execution_counts(
        timeframe,
        collection
      )
    end

    let(:rule_1_with_counts) do
      rules_with_counts.find { |rule| rule.id == rule_1.id }
    end
    let(:rule_2_with_counts) do
      rules_with_counts.find { |rule| rule.id == rule_2.id }
    end
    let(:rule_3_with_counts) do
      rules_with_counts.find { |rule| rule.id == rule_3.id }
    end

    describe 'when the timeframe is hourly' do
      let(:timeframe) { :hourly }

      it 'counts over the last hour for the first rule' do
        assert_equal 1, rule_1_with_counts.usage.hourly
      end

      it 'counts over the last hour for the second rule' do
        assert_equal 0, rule_2_with_counts.usage.hourly
      end

      it 'returns zero for unexecuted rules' do
        assert_equal 0, rule_3_with_counts.usage.hourly
      end
    end

    describe 'when the timeframe is daily' do
      let(:timeframe) { :daily }

      it 'counts over the last 24 hours for the first rule' do
        assert_equal 2, rule_1_with_counts.usage.daily
      end

      it 'counts over the last 24 hours for the second rule' do
        assert_equal 1, rule_2_with_counts.usage.daily
      end

      it 'returns zero for unexecuted rules' do
        assert_equal 0, rule_3_with_counts.usage.daily
      end
    end

    describe 'when the timeframe is weekly' do
      let(:timeframe) { :weekly }

      it 'counts over the last week for the first rule' do
        assert_equal 3, rule_1_with_counts.usage.weekly
      end

      it 'counts over the last week for the second rule' do
        assert_equal 6, rule_2_with_counts.usage.weekly
      end

      it 'returns zero for unexecuted rules' do
        assert_equal 0, rule_3_with_counts.usage.weekly
      end
    end
  end

  describe '#all_execution_counts' do
    it 'returns all execution counts over the last hour' do
      assert_equal(
        {rule_1.id => 1},
        execution_counter.all_execution_counts(:hourly)
      )
    end

    it 'returns all execution counts over the last 24 hours' do
      assert_equal(
        {rule_1.id => 2, rule_2.id => 1},
        execution_counter.all_execution_counts(:daily)
      )
    end

    it 'returns all execution counts over the last 7 days' do
      assert_equal(
        {rule_2.id => 6, rule_1.id => 3},
        execution_counter.all_execution_counts(:weekly)
      )
    end
  end

  describe '#execution_count' do
    it 'returns the execution count of a rule over the last hour' do
      assert_equal(
        1,
        execution_counter.execution_count(rule_1, :hourly)
      )
    end

    it 'returns the execution count of a rule over the last 24 hours' do
      assert_equal(
        2,
        execution_counter.execution_count(rule_1, :daily)
      )
    end

    it 'returns the execution count of a rule over the last 7 days' do
      assert_equal(
        3,
        execution_counter.execution_count(rule_1, :weekly)
      )
    end
  end
end
