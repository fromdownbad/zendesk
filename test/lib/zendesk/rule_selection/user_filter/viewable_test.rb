require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::UserFilter::Viewable do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::UserFilter::Viewable }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:rule) { create_macro(title: 'Macro', owner: owner, active: is_active) }

  let(:is_active) { true }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user) }

  let(:user_filter) { described_class.new(context) }

  let(:filtered_scope) do
    context.rules.where(user_filter.to_arel)
  end

  before do
    GroupMacro.destroy_all
    Rule.destroy_all
  end

  describe '#to_arel' do
    before do
      Subscription.any_instance.stubs(has_group_rules?: true)
      Subscription.any_instance.stubs(has_personal_rules?: true)
    end

    describe 'when the rule is account owned' do
      let(:owner) { account }

      describe 'and it is active' do
        let(:is_active) { true }

        it 'is included' do
          filtered_scope.must_include rule
        end
      end

      describe 'and it is not active' do
        let(:is_active) { false }

        it 'is not included' do
          filtered_scope.wont_include rule
        end
      end
    end

    describe 'when the rule is owned by a group' do
      before { GroupMacro.destroy_all }

      describe 'and the user is a member of the group' do
        let(:owner) { groups(:minimum_group) }

        describe_with_arturo_enabled :multiple_group_views_reads do
          describe 'and it is active' do
            let(:is_active) { true }

            it 'is included' do
              filtered_scope.must_include rule
            end
          end

          describe 'and it is not active' do
            let(:is_active) { false }

            it 'is not included' do
              filtered_scope.wont_include rule
            end
          end
        end

        describe_with_arturo_disabled :multiple_group_views_reads do
          describe 'and it is active' do
            let(:is_active) { true }

            it 'is included' do
              filtered_scope.must_include rule
            end
          end

          describe 'and it is not active' do
            let(:is_active) { false }

            it 'is not included' do
              filtered_scope.wont_include rule
            end
          end
        end
      end

      describe 'and the user is not a member of the group' do
        let(:owner) { groups(:with_groups_group1) }

        it 'is not included' do
          filtered_scope.wont_include rule
        end
      end
    end

    describe 'when using legacy groups' do
      before { context.stubs(modern_groups?: false) }

      describe 'and the rule is owned by a group' do
        describe 'and the user is a member of the group' do
          let(:owner) { groups(:minimum_group) }

          describe 'and it is active' do
            let(:is_active) { true }

            it 'is included' do
              assert filtered_scope.where(user_filter.to_arel).include?(rule)
            end
          end

          describe 'and it is not active' do
            let(:is_active) { false }

            it 'is not included' do
              refute filtered_scope.where(user_filter.to_arel).include?(rule)
            end
          end
        end

        describe 'and the user is not a member of the group' do
          let(:owner) { groups(:with_groups_group1) }

          it 'is not included' do
            refute filtered_scope.where(user_filter.to_arel).include?(rule)
          end
        end
      end
    end

    describe 'when the rule is owned by a user' do
      describe 'and the user owns the rule' do
        let(:owner) { user }

        describe 'and it is active' do
          let(:is_active) { true }

          it 'is included' do
            filtered_scope.must_include rule
          end
        end

        describe 'and it is not active' do
          let(:is_active) { false }

          it 'is not included' do
            filtered_scope.wont_include rule
          end
        end
      end

      describe 'and the user is not the owner of the rule' do
        let(:owner) { users(:minimum_agent) }

        it 'is not included' do
          filtered_scope.wont_include rule
        end
      end
    end
  end
end
