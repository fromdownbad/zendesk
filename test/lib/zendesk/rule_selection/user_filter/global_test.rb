require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::UserFilter::Global do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::UserFilter::Global }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:permission_set) { build_valid_permission_set }

  let(:rule) do
    create_macro(title: 'Macro', owner: owner, active: is_active)
  end
  let(:is_active) { true }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user) }

  let(:user_filter) { described_class.new(context) }

  before do
    Rule.destroy_all

    Subscription.any_instance.stubs(has_group_rules?: true)
    Subscription.any_instance.stubs(has_personal_rules?: true)
  end

  describe '.valid?' do
    let(:permission_set) { build_valid_permission_set }

    before do
      account.stubs(has_permission_sets?: true)
      user.stubs(has_permission_set?: true)

      permission_set.permissions.macro_access = macro_access

      user.stubs(permission_set: permission_set)
    end

    describe 'when the user can manage shared rules' do
      let(:macro_access) { 'full' }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when the user cannot manage shared rules' do
      let(:macro_access) { 'manage-group' }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#to_arel' do
    it 'returns the correct arel' do
      assert_equal(
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(account.id)).
          or(context.table[:owner_type].eq('Group')).
          or(
            context.table[:owner_type].eq('User').
              and(context.table[:owner_id].eq(user.id))
          ).to_sql,
        user_filter.to_arel.to_sql
      )
    end
  end

  describe '#to_elasticsearch' do
    it 'returns the correct Elasticsearch filter query' do
      assert_equal(
        '(owner_type:Account OR owner_type:Group OR ' \
          "(owner_type:User AND owner_id:#{user.id}))",
        user_filter.to_elasticsearch
      )
    end
  end
end
