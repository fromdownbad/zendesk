require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::UserFilter::Account do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::UserFilter::Account }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:permission_set) { build_valid_permission_set }

  let(:rule) do
    create_automation(
      title:  "Automation Active: #{is_active}",
      active: is_active
    )
  end

  let(:is_active) { true }

  let(:context) { Zendesk::RuleSelection::Context::Automation.new(user) }

  let(:user_filter) { described_class.new(context) }

  describe '.valid?' do
    let(:permission_set) { build_valid_permission_set }

    before do
      account.stubs(has_permission_sets?: true)
      user.stubs(has_permission_set?: true)

      permission_set.permissions.business_rule_management = rule_management

      user.stubs(permission_set: permission_set)
    end

    describe 'when the user can manage business rules' do
      let(:rule_management) { true }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when the user cannot manage business rules' do
      let(:rule_management) { false }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#to_arel' do
    it 'returns the correct arel' do
      assert_equal(
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(account.id)).
          to_sql,
        user_filter.to_arel.to_sql
      )
    end
  end
end
