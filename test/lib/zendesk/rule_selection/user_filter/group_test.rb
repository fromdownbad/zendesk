require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::RuleSelection::UserFilter::Group do
  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::UserFilter::Group }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user) }

  let(:user_filter) { described_class.new(context) }

  before do
    Subscription.any_instance.stubs(has_group_rules?: true)
    Subscription.any_instance.stubs(has_personal_rules?: true)
  end

  describe '.valid?' do
    let(:permission_set) { build_valid_permission_set }

    before do
      account.stubs(has_permission_sets?: true)
      user.stubs(has_permission_set?: true)

      user.account.subscription.stubs(has_group_rules?: true)

      permission_set.permissions.macro_access = macro_access

      user.stubs(permission_set: permission_set)
    end

    describe 'when the user can manage group rules' do
      let(:macro_access) { 'manage-group' }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when the user cannot manage group rules' do
      let(:macro_access) { 'readonly' }

      it 'returns false' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#to_arel' do
    describe_with_arturo_enabled :multiple_group_views_reads do
      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            and(context.table[:owner_id].eq(account.id)).
            and(context.table[:is_active].eq(true)).
            or(
              context.table[:id].eq(context.group_table[context.foreign_key]).
              and(context.group_table[:group_id].in(user.group_ids)).
              and(context.table[:owner_type].eq('Group'))
            ).
            or(
              context.table[:owner_type].eq('User').
                and(context.table[:owner_id].eq(user.id))
            ).to_sql,
          user_filter.to_arel.to_sql
        )
      end
    end

    describe_with_arturo_disabled :multiple_group_views_reads do
      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            and(context.table[:owner_id].eq(account.id)).
            and(context.table[:is_active].eq(true)).
            or(
              context.table[:id].eq(context.group_table[context.foreign_key]).
                and(context.group_table[:group_id].in(user.group_ids)).
                and(context.table[:owner_type].eq('Group')).
                and(
                  context.table[:owner_id].in(
                    [*user.group_ids, Rule.group_owner_id]
                  )
                )
            ).
            or(
              context.table[:owner_type].eq('User').
                and(context.table[:owner_id].eq(user.id))
            ).to_sql,
          user_filter.to_arel.to_sql
        )
      end
    end

    describe 'when using legacy groups' do
      before { context.stubs(modern_groups?: false) }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            and(context.table[:owner_id].eq(account.id)).
            and(context.table[:is_active].eq(true)).
            or(
              context.table[:owner_type].eq('Group').
                and(context.table[:owner_id].in(user.group_ids))
            ).
            or(
              context.table[:owner_type].eq('User').
                and(context.table[:owner_id].eq(user.id))
            ).to_sql,
          user_filter.to_arel.to_sql
        )
      end
    end

    describe 'when the user has no groups' do
      before { user.stubs(group_ids: []) }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            and(context.table[:owner_id].eq(account.id)).
            and(context.table[:is_active].eq(true)).
            or(
              context.table[:owner_type].eq('User').
                and(context.table[:owner_id].eq(user.id))
            ).to_sql,
          user_filter.to_arel.to_sql
        )
      end
    end
  end

  describe '#to_elasticsearch' do
    before { user.stubs(group_ids: [5, 7]) }

    it 'returns the correct Elasticsearch filter query' do
      assert_equal(
        '((owner_type:Account AND is_active:true) OR ' \
          '(owner_type:Group AND group_ids:(5 OR 7)) OR ' \
          "(owner_type:User AND owner_id:#{user.id}))",
        user_filter.to_elasticsearch
      )
    end

    describe 'when using legacy groups' do
      before { context.stubs(modern_groups?: false) }

      it 'returns the correct Elasticsearch filter query' do
        assert_equal(
          '((owner_type:Account AND is_active:true) OR ' \
            '(owner_type:Group AND owner_id:(5 OR 7)) OR ' \
            "(owner_type:User AND owner_id:#{user.id}))",
          user_filter.to_elasticsearch
        )
      end
    end

    describe 'when the user has no groups' do
      before { user.stubs(group_ids: []) }

      it 'returns the correct Elasticsearch filter query' do
        assert_equal(
          '((owner_type:Account AND is_active:true) OR ' \
            "(owner_type:User AND owner_id:#{user.id}))",
          user_filter.to_elasticsearch
        )
      end
    end
  end
end
