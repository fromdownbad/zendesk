require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::UserFilter::Abstract do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::UserFilter::Abstract }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:rule) { create_macro(title: 'Macro title', owner: owner) }

  let(:context) { Zendesk::RuleSelection::Context::Macro.new(user) }

  let(:user_filter_class) do
    Class.new(described_class) do
      def account_arel
        context.table[:owner_type].eq('Account')
      end

      def account_es
        'owner_type:Account'
      end

      def group_arel
        context.table[:owner_type].eq('Group')
      end

      def group_es
        'owner_type:Group'
      end

      def personal_arel
        context.table[:owner_type].eq('User')
      end

      def personal_es
        'owner_type:User'
      end
    end
  end

  let(:other_user_filter_class) do
    Class.new(described_class) do
      def account_arel
        context.table[:owner_type].eq('Account')
      end

      def account_es
        'owner_type:Account'
      end

      def personal_arel
        context.table[:owner_type].eq('User')
      end

      def personal_es
        'owner_type:User'
      end
    end
  end

  let(:user_filter) { user_filter_class.new(context) }

  describe '#to_arel' do
    before do
      Subscription.any_instance.stubs(has_group_rules?: true)
      Subscription.any_instance.stubs(has_personal_rules?: true)
    end

    describe 'when the user filter supports account, group, and personal' do
      let(:user_filter) { user_filter_class.new(context) }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            or(context.table[:owner_type].eq('Group')).
            or(context.table[:owner_type].eq('User')).
            to_sql,
          user_filter.to_arel.to_sql
        )
      end

      describe 'and the account does not have the `group_rules` feature' do
        before { Subscription.any_instance.stubs(has_group_rules?: false) }

        it 'returns the correct arel' do
          assert_equal(
            context.table[:owner_type].eq('Account').
              or(context.table[:owner_type].eq('User')).
              to_sql,
            user_filter.to_arel.to_sql
          )
        end
      end

      describe 'and the account does not have the `personal_rules` feature' do
        before do
          Subscription.any_instance.stubs(has_personal_rules?: false)
        end

        it 'returns the correct arel' do
          assert_equal(
            context.table[:owner_type].eq('Account').
              or(context.table[:owner_type].eq('Group')).
              to_sql,
            user_filter.to_arel.to_sql
          )
        end
      end

      describe 'and the account does not have either feature' do
        before do
          Subscription.any_instance.stubs(has_group_rules?: false)
          Subscription.any_instance.stubs(has_personal_rules?: false)
        end

        it 'returns the correct arel' do
          assert_equal(
            context.table[:owner_type].eq('Account').to_sql,
            user_filter.to_arel.to_sql
          )
        end
      end
    end

    describe 'when the user filter does not support all of the values' do
      let(:user_filter) { other_user_filter_class.new(context) }

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            or(context.table[:owner_type].eq('User')).
            to_sql,
          user_filter.to_arel.to_sql
        )
      end
    end
  end

  describe '#to_elasticsearch' do
    describe 'when the user filter supports account, group, and personal' do
      let(:user_filter) { user_filter_class.new(context) }

      describe 'and the account has both features' do
        before do
          Subscription.any_instance.stubs(has_personal_rules?: true)
          Subscription.any_instance.stubs(has_group_rules?: true)
        end

        it 'returns the correct Elasticsearch filter query' do
          assert_equal(
            '(owner_type:Account OR owner_type:Group OR owner_type:User)',
            user_filter.to_elasticsearch
          )
        end
      end

      describe 'and the account has the `personal_rules` feature' do
        before do
          Subscription.any_instance.stubs(has_personal_rules?: true)
          Subscription.any_instance.stubs(has_group_rules?: false)
        end

        it 'returns the correct Elasticsearch filter query' do
          assert_equal(
            '(owner_type:Account OR owner_type:User)',
            user_filter.to_elasticsearch
          )
        end
      end

      describe 'and the account has the `group_rules` feature' do
        before do
          Subscription.any_instance.stubs(has_group_rules?: true)
          Subscription.any_instance.stubs(has_personal_rules?: false)
        end

        it 'returns the correct Elasticsearch filter query' do
          assert_equal(
            '(owner_type:Account OR owner_type:Group)',
            user_filter.to_elasticsearch
          )
        end
      end

      describe 'and the account has neither feature' do
        before do
          Subscription.any_instance.stubs(has_personal_rules?: false)
          Subscription.any_instance.stubs(has_group_rules?: false)
        end

        it 'returns the correct Elasticsearch filter query' do
          assert_equal '(owner_type:Account)', user_filter.to_elasticsearch
        end
      end
    end

    describe 'when the user filter does not support all of the values' do
      let(:user_filter) { other_user_filter_class.new(context) }

      it 'returns the correct Elasticsearch filter query' do
        assert_equal(
          '(owner_type:Account OR owner_type:User)',
          user_filter.to_elasticsearch
        )
      end
    end
  end
end
