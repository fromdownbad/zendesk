require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::UserFilter::Shared do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::UserFilter::Shared }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:permission_set) { build_valid_permission_set }

  let(:rule) do
    create_view(title: 'ZB', owner: owner, active: is_active, position: 2)
  end
  let(:is_active) { true }

  let(:context) { Zendesk::RuleSelection::Context::View.new(user) }

  let(:user_filter) { described_class.new(context) }

  before { Rule.destroy_all }

  describe '.valid?' do
    let(:permission_set) { build_valid_permission_set }

    before do
      account.stubs(has_permission_sets?: true)
      user.stubs(has_permission_set?: true)

      permission_set.permissions.view_access = view_access

      user.stubs(permission_set: permission_set)
    end

    describe 'when the user can manage shared rules' do
      let(:view_access) { 'full' }

      it 'returns true' do
        assert described_class.valid?(context)
      end
    end

    describe 'when the user cannot manage shared rules' do
      let(:view_access) { 'readonly' }

      it 'returns true' do
        refute described_class.valid?(context)
      end
    end
  end

  describe '#to_arel' do
    before { Subscription.any_instance.stubs(has_group_rules?: true) }

    it 'returns the correct arel' do
      assert_equal(
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(account.id)).
          or(context.table[:owner_type].eq('Group')).
          to_sql,
        user_filter.to_arel.to_sql
      )
    end
  end
end
