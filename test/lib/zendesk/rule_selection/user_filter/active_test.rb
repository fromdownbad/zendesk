require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::UserFilter::Active do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::UserFilter::Active }

  let(:user)    { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }

  let(:permission_set) { build_valid_permission_set }

  let(:rule) do
    create_automation(
      title: "Automation Active: #{is_active}",
      active: is_active
    )
  end

  let(:is_active) { true }

  let(:context) { Zendesk::RuleSelection::Context::Automation.new(user) }

  let(:user_filter) { described_class.new(context) }

  describe '.valid?' do
    let(:permission_set) { build_valid_permission_set }

    it 'returns true' do
      assert described_class.valid?(context)
    end
  end

  describe '#to_arel' do
    it 'returns the correct arel' do
      assert_equal(
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(account.id)).
          and(context.table[:is_active].eq(true)).
          to_sql,
        user_filter.to_arel.to_sql
      )
    end
  end

  describe '#to_elasticsearch' do
    it 'returns the correct Elasticsearch filter query' do
      assert_equal('(is_active:true)', user_filter.to_elasticsearch)
    end
  end
end
