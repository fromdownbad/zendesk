require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::RuleSelection::Filter do
  fixtures :accounts,
    :users

  let(:described_class) { Zendesk::RuleSelection::Filter }

  let(:account) { user.account }
  let(:user)    { users(:minimum_admin) }

  describe '.for_context' do
    let(:filter) { described_class.for_context(context) }

    describe 'when type is `macro`' do
      let(:context) do
        Zendesk::RuleSelection::Context::Macro.new(user, active: true)
      end

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            and(context.table[:owner_id].eq(account.id)).
            or(
              context.table[:owner_type].eq('User').
                and(context.table[:owner_id].eq(user.id))
            ).
            and(context.table[:is_active].eq(true)).
            to_sql,
          filter.to_arel.to_sql
        )
      end

      it 'returns the correct Elasticsearch filter query' do
        assert_equal(
          '(owner_type:Account OR ' \
            "(owner_type:User AND owner_id:#{user.id})) AND " \
            'is_active:true',
          filter.to_elasticsearch
        )
      end
    end

    describe 'when type is `view`' do
      let(:context) do
        Zendesk::RuleSelection::Context::View.new(user, active: true)
      end

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            and(context.table[:owner_id].eq(account.id)).
            or(
              context.table[:owner_type].eq('User').
                and(context.table[:owner_id].eq(user.id))
            ).
            and(context.table[:is_active].eq(true)).
            to_sql,
          filter.to_arel.to_sql
        )
      end

      it 'returns the correct Elasticsearch filter query' do
        assert_equal(
          '(owner_type:Account OR ' \
            "(owner_type:User AND owner_id:#{user.id})) AND " \
            'is_active:true',
          filter.to_elasticsearch
        )
      end
    end

    describe 'when type is `trigger`' do
      let(:context) do
        Zendesk::RuleSelection::Context::Trigger.new(user, active: true)
      end

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            and(context.table[:owner_id].eq(account.id)).
            and(context.table[:is_active].eq(true)).
            to_sql,
          filter.to_arel.to_sql
        )
      end

      it 'returns the correct Elasticsearch filter query' do
        assert_equal 'is_active:true', filter.to_elasticsearch
      end
    end

    describe 'when type is `automation`' do
      let(:context) do
        Zendesk::RuleSelection::Context::Automation.new(user, active: true)
      end

      it 'returns the correct arel' do
        assert_equal(
          context.table[:owner_type].eq('Account').
            and(context.table[:owner_id].eq(account.id)).
            and(context.table[:is_active].eq(true)).
            to_sql,
          filter.to_arel.to_sql
        )
      end

      it 'returns the correct Elasticsearch filter query' do
        assert_equal 'is_active:true', filter.to_elasticsearch
      end
    end
  end

  describe '#to_arel' do
    let(:context) do
      Zendesk::RuleSelection::Context::Automation.new(user)
    end

    let(:user_filter) do
      Class.new do
        def initialize(context)
          @context = context
        end

        def to_arel
          @context.table[:is_active].eq(true)
        end
      end
    end

    let(:option_filters) { [] }

    let(:filter) do
      described_class.new(
        context,
        user_filter:    user_filter.new(context),
        option_filters: option_filters
      )
    end

    describe 'when there is a single filter' do
      it 'returns the correct arel' do
        assert_equal(
          context.table[:is_active].eq(true).to_sql,
          filter.to_arel.to_sql
        )
      end
    end

    describe 'when there is an empty filter' do
      let(:option_filters) do
        Class.new do
          def initialize(context)
            @context = context
          end

          def to_arel
            nil
          end
        end.new(context)
      end

      it 'returns the correct arel' do
        assert_equal(
          context.table[:is_active].eq(true).to_sql,
          filter.to_arel.to_sql
        )
      end
    end

    describe 'when there are multiple filters' do
      let(:option_filters) do
        Class.new do
          def initialize(context)
            @context = context
          end

          def to_arel
            @context.table[:owner_type].in('User')
          end
        end.new(context)
      end

      it 'returns the correct arel' do
        assert_equal(
          context.table[:is_active].eq(true).
            and(context.table[:owner_type].in('User')).
            to_sql,
          filter.to_arel.to_sql
        )
      end
    end
  end

  describe '#to_elasticsearch' do
    let(:context) do
      Zendesk::RuleSelection::Context::Automation.new(user)
    end

    let(:user_filter) do
      Class.new do
        def initialize(context)
          @context = context
        end

        def to_elasticsearch
          'is_active:true'
        end
      end
    end

    let(:option_filters) { [] }

    let(:filter) do
      described_class.new(
        context,
        user_filter:    user_filter.new(context),
        option_filters: option_filters
      )
    end

    describe 'when there is a single filter' do
      it 'returns the correct Elasticsearch filter query' do
        assert_equal 'is_active:true', filter.to_elasticsearch
      end
    end

    describe 'when there is an empty filter' do
      let(:option_filters) do
        Class.new do
          def initialize(context)
            @context = context
          end

          def to_elasticsearch
            nil
          end
        end.new(context)
      end

      it 'returns the correct Elasticsearch filter query' do
        assert_equal 'is_active:true', filter.to_elasticsearch
      end
    end

    describe 'when there are multiple filters' do
      let(:option_filters) do
        Class.new do
          def initialize(context)
            @context = context
          end

          def to_elasticsearch
            '(owner_type:(User))'
          end
        end.new(context)
      end

      it 'returns the correct Elasticsearch filter query' do
        assert_equal(
          'is_active:true AND (owner_type:(User))',
          filter.to_elasticsearch
        )
      end
    end
  end
end
