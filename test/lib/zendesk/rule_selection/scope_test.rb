require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Zendesk::RuleSelection::Scope do
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :groups,
    :users

  let(:described_class) { Zendesk::RuleSelection::Scope }

  let(:user)    { users(:minimum_admin) }
  let(:account) { user.account }

  let(:other_user) { users(:minimum_agent) }

  let(:permission_set) { build_valid_permission_set }

  let(:associated_group)   { groups(:minimum_group) }
  let(:unassociated_group) { groups(:with_groups_group1) }

  before do
    account.stubs(has_permission_sets?: true)
    user.stubs(has_permission_set?: true)

    account.subscription.stubs(has_group_rules?: true)
    account.subscription.stubs(has_personal_rules?: true)

    user.stubs(permission_set: permission_set)

    View.destroy_all

    create_view(title: 'AA', owner: account, active: true,  position: 3)
    create_view(title: 'AB', owner: account, active: false, position: 4)
    create_view(title: 'ZA', owner: account, active: true,  position: 1)
    create_view(title: 'ZB', owner: account, active: false, position: 2)

    create_view(title: 'AC', owner: associated_group, active: true,  position: 7)
    create_view(title: 'AD', owner: associated_group, active: false, position: 8)
    create_view(title: 'ZC', owner: associated_group, active: true,  position: 5)
    create_view(title: 'ZD', owner: associated_group, active: false, position: 6)

    create_view(
      title:    'AE',
      owner:    unassociated_group,
      active:   true,
      position: 11
    )
    create_view(
      title:    'AF',
      owner:    unassociated_group,
      active:   false,
      position: 12
    )
    create_view(
      title:    'ZE',
      owner:    unassociated_group,
      active:   true,
      position: 9
    )
    create_view(
      title:    'ZF',
      owner:    unassociated_group,
      active:   false,
      position: 10
    )

    create_view(title: 'AG', owner: user, active: true,  position: 15)
    create_view(title: 'AH', owner: user, active: false, position: 16)
    create_view(title: 'ZG', owner: user, active: true,  position: 13)
    create_view(title: 'ZH', owner: user, active: false, position: 14)

    create_view(title: 'AI', owner: other_user, active: true,  position: 19)
    create_view(title: 'AJ', owner: other_user, active: false, position: 20)
    create_view(title: 'ZI', owner: other_user, active: true,  position: 17)
    create_view(title: 'ZJ', owner: other_user, active: false, position: 18)

    GroupMacro.destroy_all
    Macro.destroy_all

    create_macro(title: 'AA', owner: account, active: true,  position: 3)
    create_macro(title: 'AB', owner: account, active: false, position: 4)
    create_macro(title: 'ZA', owner: account, active: true,  position: 1)
    create_macro(title: 'ZB', owner: account, active: false, position: 2)

    create_macro(title: 'AC', owner: associated_group, active: true,  position: 7)
    create_macro(title: 'AD', owner: associated_group, active: false, position: 8)
    create_macro(title: 'ZC', owner: associated_group, active: true,  position: 5)
    create_macro(title: 'ZD', owner: associated_group, active: false, position: 6)

    create_macro(
      title:    'AE',
      owner:    unassociated_group,
      active:   true,
      position: 11
    )
    create_macro(
      title:    'AF',
      owner:    unassociated_group,
      active:   false,
      position: 12
    )
    create_macro(
      title:    'ZE',
      owner:    unassociated_group,
      active:   true,
      position: 9
    )
    create_macro(
      title:    'ZF',
      owner:    unassociated_group,
      active:   false,
      position: 10
    )

    create_macro(title: 'AG', owner: user, active: true,  position: 15)
    create_macro(title: 'AH', owner: user, active: false, position: 16)
    create_macro(title: 'ZG', owner: user, active: true,  position: 13)
    create_macro(title: 'ZH', owner: user, active: false, position: 14)

    create_macro(title: 'AI', owner: other_user, active: true,  position: 19)
    create_macro(title: 'AJ', owner: other_user, active: false, position: 20)
    create_macro(title: 'ZI', owner: other_user, active: true,  position: 17)
    create_macro(title: 'ZJ', owner: other_user, active: false, position: 18)
  end

  describe '.for_context' do
    describe 'when the rule type is `macro`' do
      let(:filters)      { {} }
      let(:macro_access) { 'full' }

      let(:context) do
        Zendesk::RuleSelection::Context::Macro.new(user, filters)
      end

      before { permission_set.permissions.macro_access = macro_access }

      describe 'and the context contains `active`' do
        describe 'and `active` is true' do
          let(:filters) { {active: true} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: true,  title: 'AE', position: 11},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and `active` is false' do
          let(:filters) { {active: false} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: false, title: 'AB', position: 4},
                {owner_type: 'Group',   active: false, title: 'AD', position: 8},
                {owner_type: 'Group',   active: false, title: 'AF', position: 12},
                {owner_type: 'User',    active: false, title: 'AH', position: 16},
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
                {owner_type: 'Group',   active: false, title: 'ZF', position: 10},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14}
              ],
              described_class.for_context(context)
            )
          end
        end
      end

      describe 'and the context contains `access`' do
        describe 'and `access` is `account`' do
          let(:filters) { {access: 'account'} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Account', active: false, title: 'AB', position: 4},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: false, title: 'ZB', position: 2}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and `access` is `personal`' do
          let(:filters) { {access: 'personal'} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'User', active: true,  title: 'AG', position: 15},
                {owner_type: 'User', active: false, title: 'AH', position: 16},
                {owner_type: 'User', active: true,  title: 'ZG', position: 13},
                {owner_type: 'User', active: false, title: 'ZH', position: 14}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and `access` is `shared`' do
          let(:filters) { {access: 'shared'} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Account', active: false, title: 'AB', position: 4},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: false, title: 'AD', position: 8},
                {owner_type: 'Group',   active: true,  title: 'AE', position: 11},
                {owner_type: 'Group',   active: false, title: 'AF', position: 12},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
                {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
                {owner_type: 'Group',   active: false, title: 'ZF', position: 10}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and `access` is invalid' do
          let(:filters) { {access: 'invalid'} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Account', active: false, title: 'AB', position: 4},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: false, title: 'AD', position: 8},
                {owner_type: 'Group',   active: true,  title: 'AE', position: 11},
                {owner_type: 'Group',   active: false, title: 'AF', position: 12},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
                {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
                {owner_type: 'Group',   active: false, title: 'ZF', position: 10},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14}
              ],
              described_class.for_context(context)
            )
          end
        end
      end

      describe 'and the context contains `group_id`' do
        before do
          macro = Macro.where(title: 'ZE').first
          GroupMacro.create! do |group_macro|
            group_macro.account_id = macro.account_id
            group_macro.group_id   = associated_group.id
            group_macro.macro_id   = macro.id
          end
          macro.groups_macros.reload
        end

        describe_with_arturo_enabled :multiple_group_views_reads do
          describe 'and `group_id` corresponds to a group with rules' do
            let(:filters) { {group_id: associated_group.id} }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Group', active: true,  title: 'AC', position: 7},
                  {owner_type: 'Group', active: false, title: 'AD', position: 8},
                  {owner_type: 'Group', active: true,  title: 'ZC', position: 5},
                  {owner_type: 'Group', active: false, title: 'ZD', position: 6},
                  {owner_type: 'Group', active: true,  title: 'ZE', position: 9}
                ],
                described_class.for_context(context)
              )
            end
          end

          describe 'and `group_id` does not corresponds to a group with rules' do
            let(:filters) { {group_id: associated_group.id.succ} }

            it 'returns the correct scope' do
              assert_rules_equal [], described_class.for_context(context)
            end
          end
        end

        describe_with_arturo_disabled :multiple_group_views_reads do
          describe 'and `group_id` corresponds to a group with rules' do
            let(:filters) { {group_id: associated_group.id} }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Group', active: true,  title: 'AC', position: 7},
                  {owner_type: 'Group', active: false, title: 'AD', position: 8},
                  {owner_type: 'Group', active: true,  title: 'ZC', position: 5},
                  {owner_type: 'Group', active: false, title: 'ZD', position: 6}
                ],
                described_class.for_context(context)
              )
            end
          end

          describe 'and `group_id` does not corresponds to a group with rules' do
            let(:filters) { {group_id: associated_group.id.succ} }

            it 'returns the correct scope' do
              assert_rules_equal [], described_class.for_context(context)
            end
          end
        end
      end

      describe 'and the context contains `category`' do
        let(:filters) { {category: 'A'} }

        before do
          create_macro(title: 'Alpha',    owner: account, active: true,  position: 1)
          create_macro(title: 'A',        owner: account, active: false, position: 2)
          create_macro(title: 'A::One',   owner: account, active: true,  position: 3)
          create_macro(title: 'A B::Two', owner: account, active: false, position: 4)
          create_macro(title: 'A::Three', owner: account, active: true,  position: 5)
          create_macro(title: 'B::A',     owner: account, active: true,  position: 6)
          create_macro(title: 'A::',      owner: account, active: true,  position: 7)
          create_macro(title: '::A',      owner: account, active: true,  position: 8)
        end

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {title: 'A',        owner_type: 'Account', active: false, position: 2},
              {title: 'A::',      owner_type: 'Account', active: true,  position: 7},
              {title: 'A::One',   owner_type: 'Account', active: true,  position: 3},
              {title: 'A::Three', owner_type: 'Account', active: true,  position: 5}
            ],
            described_class.for_context(context)
          )
        end
      end

      describe 'and the context contains multiple filters' do
        let(:filters) do
          {
            active:   true,
            access:   'shared',
            group_id: associated_group.id
          }
        end

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Group', active: true, title: 'AC', position: 7},
              {owner_type: 'Group', active: true, title: 'ZC', position: 5}
            ],
            described_class.for_context(context)
          )
        end
      end

      describe 'and the user can manage shared macros' do
        let(:macro_access) { 'full' }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
              {owner_type: 'Group',   active: false, title: 'AD', position: 8},
              {owner_type: 'Group',   active: true,  title: 'AE', position: 11},
              {owner_type: 'Group',   active: false, title: 'AF', position: 12},
              {owner_type: 'User',    active: true,  title: 'AG', position: 15},
              {owner_type: 'User',    active: false, title: 'AH', position: 16},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
              {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
              {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
              {owner_type: 'Group',   active: false, title: 'ZF', position: 10},
              {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
              {owner_type: 'User',    active: false, title: 'ZH', position: 14}
            ],
            described_class.for_context(context)
          )
        end

        describe 'and the account does not have `group rules`' do
          before { account.subscription.stubs(has_group_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Account', active: false, title: 'AB', position: 4},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and the account does not have `personal rules`' do
          before { account.subscription.stubs(has_personal_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Account', active: false, title: 'AB', position: 4},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: false, title: 'AD', position: 8},
                {owner_type: 'Group',   active: true,  title: 'AE', position: 11},
                {owner_type: 'Group',   active: false, title: 'AF', position: 12},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
                {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
                {owner_type: 'Group',   active: false, title: 'ZF', position: 10}
              ],
              described_class.for_context(context)
            )
          end
        end
      end

      describe 'and the user can manage group macros' do
        let(:macro_access) { 'manage-group' }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
              {owner_type: 'Group',   active: false, title: 'AD', position: 8},
              {owner_type: 'User',    active: true,  title: 'AG', position: 15},
              {owner_type: 'User',    active: false, title: 'AH', position: 16},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
              {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
              {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
              {owner_type: 'User',    active: false, title: 'ZH', position: 14}
            ],
            described_class.for_context(context)
          )
        end

        describe 'and the account does not have `group rules`' do
          before { account.subscription.stubs(has_group_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and the account does not have `personal rules`' do
          before { account.subscription.stubs(has_personal_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: false, title: 'AD', position: 8},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: false, title: 'ZD', position: 6}
              ],
              described_class.for_context(context)
            )
          end
        end
      end

      describe 'and the user can manage personal macros' do
        let(:macro_access) { 'manage-personal' }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
              {owner_type: 'User',    active: true,  title: 'AG', position: 15},
              {owner_type: 'User',    active: false, title: 'AH', position: 16},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
              {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
              {owner_type: 'User',    active: false, title: 'ZH', position: 14}
            ],
            described_class.for_context(context)
          )
        end

        describe 'and the account does not have `group rules`' do
          before { account.subscription.stubs(has_group_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and the account does not have `personal rules`' do
          before { account.subscription.stubs(has_personal_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5}
              ],
              described_class.for_context(context)
            )
          end
        end
      end

      describe 'and the user cannot manage macros' do
        let(:macro_access) { 'readonly' }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
              {owner_type: 'User',    active: true,  title: 'AG', position: 15},
              {owner_type: 'User',    active: false, title: 'AH', position: 16},
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
              {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
              {owner_type: 'User',    active: false, title: 'ZH', position: 14}
            ],
            described_class.for_context(context)
          )
        end

        describe 'and the account does not have `group rules`' do
          before { account.subscription.stubs(has_group_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and the account does not have `personal rules`' do
          before { account.subscription.stubs(has_personal_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5}
              ],
              described_class.for_context(context)
            )
          end
        end
      end
    end

    describe 'when the rule type is `view`' do
      let(:context) { Zendesk::RuleSelection::Context::View.new(user) }

      before { permission_set.permissions.view_access = view_access }

      before do
        view = View.where(title: 'ZE').first
        GroupView.create! do |group_view|
          group_view.account_id = view.account_id
          group_view.group_id   = associated_group.id
          group_view.view_id    = view.id
        end
        view.groups_views.reload
      end

      describe 'and the user can manage shared views' do
        let(:view_access) { 'full' }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AB', position: 4},
              {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
              {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
              {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
              {owner_type: 'Group',   active: false, title: 'AD', position: 8},
              {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
              {owner_type: 'Group',   active: false, title: 'ZF', position: 10},
              {owner_type: 'Group',   active: true,  title: 'AE', position: 11},
              {owner_type: 'Group',   active: false, title: 'AF', position: 12},
              {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
              {owner_type: 'User',    active: false, title: 'ZH', position: 14},
              {owner_type: 'User',    active: true,  title: 'AG', position: 15},
              {owner_type: 'User',    active: false, title: 'AH', position: 16}
            ],
            described_class.for_context(context)
          )
        end

        describe 'and the account does not have `group rules`' do
          before { account.subscription.stubs(has_group_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Account', active: false, title: 'AB', position: 4},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and the account does not have `personal rules`' do
          before { account.subscription.stubs(has_personal_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Account', active: false, title: 'AB', position: 4},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: false, title: 'AD', position: 8},
                {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
                {owner_type: 'Group',   active: false, title: 'ZF', position: 10},
                {owner_type: 'Group',   active: true,  title: 'AE', position: 11},
                {owner_type: 'Group',   active: false, title: 'AF', position: 12}
              ],
              described_class.for_context(context)
            )
          end
        end
      end

      describe 'when the user can manage group views' do
        let(:view_access) { 'manage-group' }

        describe_with_arturo_enabled :multiple_group_views_reads do
          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: false, title: 'AD', position: 8},
                {owner_type: 'Group',   active: true, title: 'ZE', position: 9},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe_with_arturo_disabled :multiple_group_views_reads do
          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: false, title: 'AD', position: 8},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and the account does not have `group rules`' do
          before { account.subscription.stubs(has_group_rules?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and the account does not have `personal rules`' do
          before { account.subscription.stubs(has_personal_rules?: false) }

          describe_with_arturo_enabled :multiple_group_views_reads do
            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                  {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                  {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
                  {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                  {owner_type: 'Group',   active: false, title: 'AD', position: 8},
                  {owner_type: 'Group',   active: true, title: 'ZE', position: 9}
                ],
                described_class.for_context(context)
              )
            end
          end

          describe_with_arturo_disabled :multiple_group_views_reads do
            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                  {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                  {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
                  {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                  {owner_type: 'Group',   active: false, title: 'AD', position: 8}
                ],
                described_class.for_context(context)
              )
            end
          end
        end
      end

      describe 'when the user can manage personal views' do
        let(:view_access) { 'manage-personal' }

        describe_with_arturo_enabled :multiple_group_views_reads do
          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16}
              ],
              described_class.for_context(context)
            )
          end

          describe 'and the account does not have `group rules`' do
            before { account.subscription.stubs(has_group_rules?: false) }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                  {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                  {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                  {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                  {owner_type: 'User',    active: false, title: 'AH', position: 16}
                ],
                described_class.for_context(context)
              )
            end
          end

          describe 'and the account does not have `personal rules`' do
            before { account.subscription.stubs(has_personal_rules?: false) }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true, title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true, title: 'AA', position: 3},
                  {owner_type: 'Group',   active: true, title: 'ZC', position: 5},
                  {owner_type: 'Group',   active: true, title: 'AC', position: 7},
                  {owner_type: 'Group',   active: true, title: 'ZE', position: 9}
                ],
                described_class.for_context(context)
              )
            end
          end
        end

        describe_with_arturo_disabled :multiple_group_views_reads do
          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16}
              ],
              described_class.for_context(context)
            )
          end

          describe 'and the account does not have `group rules`' do
            before { account.subscription.stubs(has_group_rules?: false) }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                  {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                  {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                  {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                  {owner_type: 'User',    active: false, title: 'AH', position: 16}
                ],
                described_class.for_context(context)
              )
            end
          end

          describe 'and the account does not have `personal rules`' do
            before { account.subscription.stubs(has_personal_rules?: false) }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true, title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true, title: 'AA', position: 3},
                  {owner_type: 'Group',   active: true, title: 'ZC', position: 5},
                  {owner_type: 'Group',   active: true, title: 'AC', position: 7}
                ],
                described_class.for_context(context)
              )
            end
          end
        end
      end

      describe 'when the user cannot manage views' do
        let(:view_access) { 'playonly' }

        describe_with_arturo_enabled :multiple_group_views_reads do
          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'Group',   active: true, title: 'ZE', position: 9},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16}
              ],
              described_class.for_context(context)
            )
          end

          describe 'and the account does not have `group rules`' do
            before { account.subscription.stubs(has_group_rules?: false) }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                  {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                  {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                  {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                  {owner_type: 'User',    active: false, title: 'AH', position: 16}
                ],
                described_class.for_context(context)
              )
            end
          end

          describe 'and the account does not have `personal rules`' do
            before { account.subscription.stubs(has_personal_rules?: false) }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true, title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true, title: 'AA', position: 3},
                  {owner_type: 'Group',   active: true, title: 'ZC', position: 5},
                  {owner_type: 'Group',   active: true, title: 'AC', position: 7},
                  {owner_type: 'Group',   active: true, title: 'ZE', position: 9}
                ],
                described_class.for_context(context)
              )
            end
          end
        end

        describe_with_arturo_disabled :multiple_group_views_reads do
          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
                {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                {owner_type: 'User',    active: false, title: 'AH', position: 16}
              ],
              described_class.for_context(context)
            )
          end

          describe 'and the account does not have `group rules`' do
            before { account.subscription.stubs(has_group_rules?: false) }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                  {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
                  {owner_type: 'User',    active: false, title: 'ZH', position: 14},
                  {owner_type: 'User',    active: true,  title: 'AG', position: 15},
                  {owner_type: 'User',    active: false, title: 'AH', position: 16}
                ],
                described_class.for_context(context)
              )
            end
          end

          describe 'and the account does not have `personal rules`' do
            before { account.subscription.stubs(has_personal_rules?: false) }

            it 'returns the correct scope' do
              assert_rules_equal(
                [
                  {owner_type: 'Account', active: true, title: 'ZA', position: 1},
                  {owner_type: 'Account', active: true, title: 'AA', position: 3},
                  {owner_type: 'Group',   active: true, title: 'ZC', position: 5},
                  {owner_type: 'Group',   active: true, title: 'AC', position: 7}
                ],
                described_class.for_context(context)
              )
            end
          end
        end
      end
    end

    describe 'when the rule type is `automation`' do
      let(:context) { Zendesk::RuleSelection::Context::Automation.new(user, filters) }
      let(:filters) { {} }

      let(:rule_management) { true }

      before do
        Automation.destroy_all

        create_automation(title: 'ZB', active: false, position: 2)
        create_automation(title: 'ZA', active: true,  position: 1)
        create_automation(title: 'AB', active: false, position: 4)
        create_automation(title: 'AA', active: true,  position: 3)

        permission_set.permissions.business_rule_management = rule_management
      end

      describe 'and the context contains `active`' do
        describe 'and `active` is true' do
          let(:filters) { {active: true} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and `active` is false' do
          let(:filters) { {active: false} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'Account', active: false, title: 'AB', position: 4}
              ],
              described_class.for_context(context)
            )
          end
        end
      end

      describe 'and the user can manage business rules' do
        let(:rule_management) { true }

        describe 'and the account has the `unlimited_automations` feature' do
          before { account.subscription.stubs(has_unlimited_automations?: true) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'Account', active: true,  title: 'AA', position: 3},
                {owner_type: 'Account', active: false, title: 'AB', position: 4}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and the account does not have the `unlimited_automations` feature' do
          before { account.subscription.stubs(has_unlimited_automations?: false) }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true, title: 'ZA', position: 1},
                {owner_type: 'Account', active: true, title: 'AA', position: 3}
              ],
              described_class.for_context(context)
            )
          end
        end
      end

      describe 'and the user cannot manage business rules' do
        let(:rule_management) { false }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true, title: 'ZA', position: 1},
              {owner_type: 'Account', active: true, title: 'AA', position: 3}
            ],
            described_class.for_context(context)
          )
        end
      end
    end

    describe 'when the rule type is `trigger`' do
      let(:context) do
        Zendesk::RuleSelection::Context::Trigger.new(user, filters)
      end
      let(:filters) { {} }

      let(:rule_management) { true }

      before do
        Trigger.destroy_all

        create_trigger(title: 'ZB', active: false, position: 2)
        create_trigger(title: 'ZA', active: true,  position: 1)
        create_trigger(title: 'AB', active: false, position: 4)
        create_trigger(title: 'AA', active: true,  position: 3)

        permission_set.permissions.business_rule_management = rule_management
      end

      describe 'and the context contains `active`' do
        describe 'and `active` is true' do
          let(:filters) { {active: true} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true, title: 'ZA', position: 1},
                {owner_type: 'Account', active: true, title: 'AA', position: 3}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and `active` is false' do
          let(:filters) { {active: false} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: false, title: 'ZB', position: 2},
                {owner_type: 'Account', active: false, title: 'AB', position: 4}
              ],
              described_class.for_context(context)
            )
          end
        end
      end

      describe 'and the user can manage business rules' do
        let(:rule_management) { true }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
              {owner_type: 'Account', active: false, title: 'ZB', position: 2},
              {owner_type: 'Account', active: true,  title: 'AA', position: 3},
              {owner_type: 'Account', active: false, title: 'AB', position: 4}
            ],
            described_class.for_context(context)
          )
        end
      end

      describe 'and the user cannot manage business rules' do
        let(:rule_management) { false }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true, title: 'ZA', position: 1},
              {owner_type: 'Account', active: true, title: 'AA', position: 3}
            ],
            described_class.for_context(context)
          )
        end
      end
    end
  end

  describe '.for_reorder' do
    let(:context) { Zendesk::RuleSelection::Context::View.new(user) }

    before { permission_set.permissions.view_access = view_access }

    describe 'when the user can manage shared views' do
      let(:view_access) { 'full' }

      it 'returns the correct personal scope' do
        assert_rules_equal(
          [
            {owner_type: 'User', active: true,  title: 'ZG', position: 13},
            {owner_type: 'User', active: false, title: 'ZH', position: 14},
            {owner_type: 'User', active: true,  title: 'AG', position: 15},
            {owner_type: 'User', active: false, title: 'AH', position: 16}
          ],
          described_class.for_reorder(context).first
        )
      end

      it 'returns the correct shared scope' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
            {owner_type: 'Account', active: false, title: 'ZB', position: 2},
            {owner_type: 'Account', active: true,  title: 'AA', position: 3},
            {owner_type: 'Account', active: false, title: 'AB', position: 4},
            {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
            {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
            {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
            {owner_type: 'Group',   active: false, title: 'AD', position: 8},
            {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
            {owner_type: 'Group',   active: false, title: 'ZF', position: 10},
            {owner_type: 'Group',   active: true,  title: 'AE', position: 11},
            {owner_type: 'Group',   active: false, title: 'AF', position: 12}
          ],
          described_class.for_reorder(context).second
        )
      end
    end

    describe 'when the user can manage personal views' do
      let(:view_access) { 'manage-personal' }

      it 'returns the correct personal scope' do
        assert_rules_equal(
          [
            {owner_type: 'User', active: true,  title: 'ZG', position: 13},
            {owner_type: 'User', active: false, title: 'ZH', position: 14},
            {owner_type: 'User', active: true,  title: 'AG', position: 15},
            {owner_type: 'User', active: false, title: 'AH', position: 16}
          ],
          described_class.for_reorder(context).first
        )
      end

      describe 'and the account does not have `personal rules`' do
        before { account.subscription.stubs(has_personal_rules?: false) }

        it 'does not return a scope' do
          assert_empty described_class.for_reorder(context)
        end
      end
    end

    describe 'when the user cannot manage views' do
      let(:view_access) { 'playonly' }

      it 'does not return a scope' do
        assert_empty described_class.for_reorder(context)
      end
    end

    describe 'when an other ordering options are provided' do
      let(:view_access) { 'full' }

      it 'returns in sorted order with `sorted` ordering' do
        assert_rules_equal(
          [
            {owner_type: 'User', active: true,  title: 'ZG', position: 13},
            {owner_type: 'User', active: false, title: 'ZH', position: 14},
            {owner_type: 'User', active: true,  title: 'AG', position: 15},
            {owner_type: 'User', active: false, title: 'AH', position: 16}
          ],
          described_class.for_reorder(context, ordering: :sorted).first
        )
      end

      it 'returns in sorted order without specified ordering' do
        assert_rules_equal(
          [
            {owner_type: 'User', active: true,  title: 'ZG', position: 13},
            {owner_type: 'User', active: false, title: 'ZH', position: 14},
            {owner_type: 'User', active: true,  title: 'AG', position: 15},
            {owner_type: 'User', active: false, title: 'AH', position: 16}
          ],
          described_class.for_reorder(context).first
        )
      end

      it 'raises an error on invalid ordering arguments' do
        assert_raises RuntimeError do
          described_class.for_reorder(context, ordering: :invalid_sort_order)
        end
      end
    end
  end

  describe '.viewable' do
    describe 'when the rule type is `macro`' do
      let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, filters) }
      let(:filters) { {} }

      it 'returns the correct scope' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: true, title: 'AA', position: 3},
            {owner_type: 'Group',   active: true, title: 'AC', position: 7},
            {owner_type: 'User',    active: true, title: 'AG', position: 15},
            {owner_type: 'Account', active: true, title: 'ZA', position: 1},
            {owner_type: 'Group',   active: true, title: 'ZC', position: 5},
            {owner_type: 'User',    active: true, title: 'ZG', position: 13}
          ],
          described_class.viewable(context)
        )
      end

      describe 'and the context contains `active`' do
        describe 'and `active` is true' do
          let(:filters) { {active: true} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true, title: 'AA', position: 3},
                {owner_type: 'Group',   active: true, title: 'AC', position: 7},
                {owner_type: 'User',    active: true, title: 'AG', position: 15},
                {owner_type: 'Account', active: true, title: 'ZA', position: 1},
                {owner_type: 'Group',   active: true, title: 'ZC', position: 5},
                {owner_type: 'User',    active: true, title: 'ZG', position: 13}
              ],
              described_class.viewable(context)
            )
          end
        end

        describe 'and `active` is false' do
          let(:filters) { {active: false} }

          it 'returns the correct scope' do
            assert_rules_equal [], described_class.viewable(context)
          end
        end
      end

      describe 'and the context contains `access`' do
        describe 'and `access` is `account`' do
          let(:filters) { {access: 'account'} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true, title: 'AA', position: 3},
                {owner_type: 'Account', active: true, title: 'ZA', position: 1}
              ],
              described_class.viewable(context)
            )
          end
        end

        describe 'and `access` is `personal`' do
          let(:filters) { {access: 'personal'} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'User', active: true, title: 'AG', position: 15},
                {owner_type: 'User', active: true, title: 'ZG', position: 13}
              ],
              described_class.viewable(context)
            )
          end
        end

        describe 'and `access` is `shared`' do
          let(:filters) { {access: 'shared'} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true, title: 'AA', position: 3},
                {owner_type: 'Group',   active: true, title: 'AC', position: 7},
                {owner_type: 'Account', active: true, title: 'ZA', position: 1},
                {owner_type: 'Group',   active: true, title: 'ZC', position: 5}
              ],
              described_class.viewable(context)
            )
          end
        end

        describe 'and `access` is invalid' do
          let(:filters) { {access: 'invalid'} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Account', active: true, title: 'AA', position: 3},
                {owner_type: 'Group',   active: true, title: 'AC', position: 7},
                {owner_type: 'User',    active: true, title: 'AG', position: 15},
                {owner_type: 'Account', active: true, title: 'ZA', position: 1},
                {owner_type: 'Group',   active: true, title: 'ZC', position: 5},
                {owner_type: 'User',    active: true, title: 'ZG', position: 13}
              ],
              described_class.viewable(context)
            )
          end
        end
      end

      describe 'and the context contains `group_id`' do
        describe 'and `group_id` corresponds to a group with rules' do
          let(:filters) { {group_id: associated_group.id} }

          it 'returns the correct scope' do
            assert_rules_equal(
              [
                {owner_type: 'Group', active: true,  title: 'AC', position: 7},
                {owner_type: 'Group', active: false, title: 'AD', position: 8},
                {owner_type: 'Group', active: true,  title: 'ZC', position: 5},
                {owner_type: 'Group', active: false, title: 'ZD', position: 6}
              ],
              described_class.for_context(context)
            )
          end
        end

        describe 'and `group_id` does not corresponds to a group with rules' do
          let(:filters) { {group_id: associated_group.id.succ} }

          it 'returns the correct scope' do
            assert_rules_equal [], described_class.for_context(context)
          end
        end
      end

      describe 'and the context contains `category`' do
        let(:filters) { {category: 'A'} }

        before do
          create_macro(title: 'Alpha',    owner: account, active: true,  position: 1)
          create_macro(title: 'A',        owner: account, active: false, position: 2)
          create_macro(title: 'A::One',   owner: account, active: true,  position: 3)
          create_macro(title: 'A B::Two', owner: account, active: false, position: 4)
          create_macro(title: 'A::Three', owner: account, active: true,  position: 5)
          create_macro(title: 'B::A',     owner: account, active: true,  position: 6)
          create_macro(title: 'A::',      owner: account, active: true,  position: 7)
          create_macro(title: '::A',      owner: account, active: true,  position: 8)
        end

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {title: 'A',        owner_type: 'Account', active: false, position: 2},
              {title: 'A::',      owner_type: 'Account', active: true,  position: 7},
              {title: 'A::One',   owner_type: 'Account', active: true,  position: 3},
              {title: 'A::Three', owner_type: 'Account', active: true,  position: 5}
            ],
            described_class.for_context(context)
          )
        end
      end

      describe 'and the context contains multiple filters' do
        let(:filters) do
          {
            active:   true,
            access:   'shared',
            group_id: associated_group.id
          }
        end

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Group', active: true, title: 'AC', position: 7},
              {owner_type: 'Group', active: true, title: 'ZC', position: 5}
            ],
            described_class.for_context(context)
          )
        end
      end

      describe 'and the account does not have `group rules`' do
        before { account.subscription.stubs(has_group_rules?: false) }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true, title: 'AA', position: 3},
              {owner_type: 'User',    active: true, title: 'AG', position: 15},
              {owner_type: 'Account', active: true, title: 'ZA', position: 1},
              {owner_type: 'User',    active: true, title: 'ZG', position: 13}
            ],
            described_class.viewable(context)
          )
        end
      end

      describe 'and the account does not have `personal rules`' do
        before { account.subscription.stubs(has_personal_rules?: false) }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true, title: 'AA', position: 3},
              {owner_type: 'Group',   active: true, title: 'AC', position: 7},
              {owner_type: 'Account', active: true, title: 'ZA', position: 1},
              {owner_type: 'Group',   active: true, title: 'ZC', position: 5}
            ],
            described_class.viewable(context)
          )
        end
      end
    end

    describe 'when the rule type is `view`' do
      let(:context) { Zendesk::RuleSelection::Context::View.new(user) }

      it 'returns the correct scope' do
        assert_rules_equal(
          [
            {owner_type: 'Account', active: true, title: 'ZA', position: 1},
            {owner_type: 'Account', active: true, title: 'AA', position: 3},
            {owner_type: 'Group',   active: true, title: 'ZC', position: 5},
            {owner_type: 'Group',   active: true, title: 'AC', position: 7},
            {owner_type: 'User',    active: true, title: 'ZG', position: 13},
            {owner_type: 'User',    active: true, title: 'AG', position: 15}
          ],
          described_class.viewable(context)
        )
      end

      describe 'and the account does not have `group rules`' do
        before { account.subscription.stubs(has_group_rules?: false) }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true, title: 'ZA', position: 1},
              {owner_type: 'Account', active: true, title: 'AA', position: 3},
              {owner_type: 'User',    active: true, title: 'ZG', position: 13},
              {owner_type: 'User',    active: true, title: 'AG', position: 15}
            ],
            described_class.viewable(context)
          )
        end
      end

      describe 'and the account does not have `personal rules`' do
        before { account.subscription.stubs(has_personal_rules?: false) }

        it 'returns the correct scope' do
          assert_rules_equal(
            [
              {owner_type: 'Account', active: true, title: 'ZA', position: 1},
              {owner_type: 'Account', active: true, title: 'AA', position: 3},
              {owner_type: 'Group',   active: true, title: 'ZC', position: 5},
              {owner_type: 'Group',   active: true, title: 'AC', position: 7}
            ],
            described_class.viewable(context)
          )
        end
      end
    end
  end

  describe '.for_search' do
    let(:context) { Zendesk::RuleSelection::Context::Macro.new(user, {}) }

    it 'returns the correct scope' do
      assert_rules_equal(
        [
          {owner_type: 'Account', active: true,  title: 'ZA', position: 1},
          {owner_type: 'Account', active: false, title: 'ZB', position: 2},
          {owner_type: 'Account', active: true,  title: 'AA', position: 3},
          {owner_type: 'Account', active: false, title: 'AB', position: 4},
          {owner_type: 'Group',   active: true,  title: 'ZC', position: 5},
          {owner_type: 'Group',   active: false, title: 'ZD', position: 6},
          {owner_type: 'Group',   active: true,  title: 'AC', position: 7},
          {owner_type: 'Group',   active: false, title: 'AD', position: 8},
          {owner_type: 'Group',   active: true,  title: 'ZE', position: 9},
          {owner_type: 'Group',   active: false, title: 'ZF', position: 10},
          {owner_type: 'Group',   active: true,  title: 'AE', position: 11},
          {owner_type: 'Group',   active: false, title: 'AF', position: 12},
          {owner_type: 'User',    active: true,  title: 'ZG', position: 13},
          {owner_type: 'User',    active: false, title: 'ZH', position: 14},
          {owner_type: 'User',    active: true,  title: 'AG', position: 15},
          {owner_type: 'User',    active: false, title: 'AH', position: 16}
        ],
        described_class.for_search(context).sort_by(&:position)
      )
    end
  end
end
