require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'ForumSanitizer' do
  fixtures :entries, :posts, :users

  describe Zendesk::ForumSanitizer do
    before do
      @agent = users(:minimum_agent)
      @script_text = "Scripted<script>alert('baddie');</script>Text"
    end

    describe "Post" do
      before { @post = posts(:fish_post_1) }

      it "does not sanitize when the body or title have not changed" do
        assert(@post.update_columns(body: @script_text))
        @post.update_attribute :is_informative, true
        assert_equal @script_text, @post.reload.body
      end
    end

    describe "Entry" do
      before { @entry = entries(:fish) }
      describe 'with current_user' do
        before do
          @entry.current_user = @agent
        end

        it "does not sanitize when the body or title have not changed" do
          assert(@entry.update_columns(body: @script_text))
          @entry.update_attribute :pinned_index, true
          assert_equal @script_text, @entry.reload.body
        end
      end
    end
  end
end
