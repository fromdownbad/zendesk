require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2 # uber case is untested

class ReplicaReadsController < ActionController::Base
  include Zendesk::Auth::Warden::ControllerMixin
  include Zendesk::Auth::AuthenticatedSessionMixin

  include Zendesk::ReplicaReads

  treat_as_read_request :reader_on_post

  def action
    render plain: !!User.on_slave_by_default?
  end

  def reader_on_post
    render plain: !!User.on_slave_by_default?
  end
end

class ReplicaReadsTest < ActionController::TestCase
  tests ReplicaReadsController
  use_test_routes

  describe "#get_on_reader" do
    before do
      @current_account = accounts(:minimum)
      @request.account = @current_account
      login(@current_account.owner)
      ReplicaReadsController.any_instance.stubs(replica_reads_disabled?: false)
    end

    before do
      Account.any_instance.stubs(created_at: 30.minutes.ago)
    end

    it "uses reader for get requests" do
      get :action
      assert_equal "true", response.body.strip
    end

    it "uses reader for get requests without account" do
      @controller.stubs(current_account: nil)
      get :action
      assert_equal "true", response.body.strip
    end

    it "does not use reader when disabled" do
      ReplicaReadsController.any_instance.stubs(replica_reads_disabled?: true)
      get :action
      assert_equal "false", response.body.strip
    end

    it "does not use reader for other requests" do
      post :action
      assert_equal "false", response.body.strip
    end

    it "uses reader for post actions marked as read requests" do
      post :reader_on_post
      assert_equal "true", response.body.strip
    end

    it "marks only single create as read request" do
      Api::V2::Rules::PreviewsController.treat_as_read_request.must_include :create
      Api::V2::TicketsController.treat_as_read_request.wont_include :create
    end

    it "does not use reader on new accounts since they were created by a different user" do
      Account.any_instance.stubs(created_at: 1.minute.ago)
      get :action
      assert_equal "false", response.body.strip
    end
  end
end
