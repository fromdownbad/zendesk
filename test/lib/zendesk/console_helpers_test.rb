require_relative "../../support/test_helper"
require 'json'

SingleCov.covered! uncovered: 101

describe Zendesk::ConsoleHelpers do
  include Zendesk::ConsoleHelpers
  include MailTestHelper

  let(:account) { accounts(:minimum) }

  describe '#email_json' do
    let(:raw_eml_path) { '9948/57049b8b-b446-436b-8bee-619c3feb6cbb.eml' }

    it 'retrieves the json for the processed email' do
      Zendesk::Mailer::RawEmailAccess.expects(:new).with(raw_eml_path).once.returns(stub(json: stub))
      email_json(raw_eml_path)
    end
  end

  describe "#clear_all_cache" do
    describe "in production" do
      before { Rails.env.stubs(production?: true) }

      describe "when confirmation is absent" do
        it "does not clear caches" do
          Prop.cache.expects(:clear).never
          clear_all_cache
        end
      end

      describe "when confirmation is present" do
        it "clears caches" do
          Prop.cache.expects(:clear).once
          clear_all_cache(true)
        end
      end
    end

    describe "in non-production environments" do
      it "clears caches" do
        Prop.cache.expects(:clear).once
        clear_all_cache
      end
    end
  end

  describe 'process_email' do
    fixtures :accounts, :events

    let(:headers) do
      {
        "from" => ["john.doe@example.com"],
        "reurn-path" => ["john.doe@example.com"]
      }
    end

    let(:mail_params) do
      {
        "body" => "blah blah blah",
        "errors" => { },
        "headers" => headers,
        "id" => "a",
        "parts" => []
      }
    end

    let(:audit) { events(:create_audit_for_minimum_ticket_1) }
    let(:msg) { Comment.new(account: account, audit: audit) }

    before do
      audit.raw_email.stubs(:json).returns(mail_params.to_json)
      Zendesk::InboundMail::TicketProcessingJob.stubs(:work)
    end

    it 'starts a ticket processing job' do
      Zendesk::InboundMail::TicketProcessingJob.expects(:work).once
      process_email(msg, account)
    end

    it 'generates a standard message id' do
      Zendesk::Mail::InboundMessage.expects(:generate_message_id)
      process_email(msg, account)
    end

    describe 'assigns a message id' do
      let(:mail) { Zendesk::Mail::InboundMessage.new }
      let(:zendesk_mid_pattern) { /\A<?(\h+_\h+){1}_sprut@zendesk-test.com>?\z/ }

      before do
        JSON.expects(:load).with(mail_params.to_json).returns(mail_params)
        Zendesk::Mail::Serializers::InboundMessageSerializer.expects(:load).with(mail_params).returns(mail)
      end

      it 'in zendesk format' do
        process_email(msg, account)
        assert mail.message_id =~ zendesk_mid_pattern
      end
    end
  end
end
