require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::I18n::LocalizeUser do
  class TestClass < ActionController::Base
    include Zendesk::I18n::LocalizeUser
  end

  fixtures :users, :accounts, :translation_locales
  describe "Localize User" do
    let(:japanese) { translation_locales(:japanese) }
    let(:portuguese) { translation_locales(:brazilian_portuguese) }
    let(:spanish) { translation_locales(:spanish) }

    before do
      TestClass.any_instance.stubs(current_account: accounts(:minimum))

      TestClass.stubs(:use_new_user_localization?).returns(true)
      @preserve_locale_test_2 = TestClass.new

      TestClass.stubs(:use_new_user_localization?).returns(false)
      @preserve_locale_test = TestClass.new
    end

    describe "#get_locale_from_params" do
      let(:account) { accounts(:minimum) }

      before { TestClass.any_instance.stubs(:current_account).returns(account) }

      it "returns nil when the parameter locale is not present" do
        TestClass.any_instance.stubs(:params).returns({})
        assert_nil @preserve_locale_test.send(:get_locale_from_params)
      end

      it "returns nil when the parameter locale isn't a valid id" do
        TestClass.any_instance.stubs(:params).returns(locale: -10)
        assert_nil @preserve_locale_test.send(:get_locale_from_params)
      end

      it "returns nil when the parameter locale isn't a valid locale code" do
        TestClass.any_instance.stubs(:params).returns(locale: "not-here")
        assert_nil @preserve_locale_test.send(:get_locale_from_params)
      end

      describe 'with indivivual language selection' do
        describe 'set to false' do
          before { Subscription.any_instance.stubs(has_individual_language_selection?: false) }

          it "returns nil when the account has no individual selection with the locale param" do
            TestClass.any_instance.stubs(:params).returns(locale: portuguese.id)
            assert_nil @preserve_locale_test.send(:get_locale_from_params)
          end

          it "returns the locale if the param force_locale is set to true" do
            TestClass.any_instance.stubs(:params).returns(locale: portuguese.id, force_locale: 'true')
            assert_equal portuguese, @preserve_locale_test.send(:get_locale_from_params)
          end
        end

        describe 'set to true' do
          before do
            Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
            Account.any_instance.stubs(:available_languages).returns([ENGLISH_BY_ZENDESK, portuguese])
          end

          it "returns the locale if the locale exists and it is in the account with locale a param containing the id" do
            TestClass.any_instance.stubs(:params).returns(locale: portuguese.id)
            assert_equal portuguese, @preserve_locale_test.send(:get_locale_from_params)
          end

          it "returns the locale if the locale exists and it is in the account with locale a param containing the locale code" do
            TestClass.any_instance.stubs(:params).returns(locale: portuguese.locale)
            assert_equal portuguese, @preserve_locale_test.send(:get_locale_from_params)
          end

          it "returns nil if the language is not valid" do
            TestClass.any_instance.stubs(:params).returns(locale: 12345678)
            assert_nil @preserve_locale_test.send(:get_locale_from_params)
          end

          describe 'with a language that exists but it is not available for the account' do
            let(:support) { accounts(:support) }

            before do
              japanese.account = support
              japanese.localized_agent = false
              japanese.official_translation = false
              japanese.public = false
              japanese.save!
            end

            it "returns nil when requesting the locale" do
              TestClass.any_instance.stubs(:params).returns(locale: japanese.id)
              assert_nil @preserve_locale_test.send(:get_locale_from_params)
            end
          end

          it "returns the locale if the locale is available in the account using locale param" do
            Account.any_instance.stubs(:available_languages).returns([ENGLISH_BY_ZENDESK, portuguese, japanese])
            TestClass.any_instance.stubs(:params).returns(locale: japanese.id)
            assert_equal japanese, @preserve_locale_test.send(:get_locale_from_params)
          end
        end
      end
    end

    describe "#localize_user" do
      let(:agent) { users(:minimum_agent) }

      before do
        TestClass.any_instance.stubs(:current_user).returns(agent)
        TestClass.any_instance.stubs(:params).returns({})
      end

      describe "when there is no current_account" do
        it "defaults to ENGLISH_BY_ZENDESK" do
          TestClass.any_instance.stubs(current_account: nil)
          TestClass.any_instance.expects(:get_locale_from_params).never

          request_locale = nil
          @preserve_locale_test_2.send(:localize_user) { request_locale = I18n.locale }

          assert_equal ENGLISH_BY_ZENDESK.to_sym, agent.translation_locale.to_sym
          assert_equal ENGLISH_BY_ZENDESK.to_sym, request_locale
        end
      end

      describe "with a locale in the request" do
        it "sets the locale to the requested locale" do
          TestClass.any_instance.stubs(:params).returns(locale: portuguese.id)
          TestClass.any_instance.stubs(:shared_session).returns({})
          Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)

          request_locale = nil
          @preserve_locale_test_2.send(:localize_user) { request_locale = I18n.locale }

          assert_equal portuguese.to_sym, agent.translation_locale.to_sym
          assert_equal portuguese.to_sym, request_locale
        end
      end

      describe "with no locale in the request" do
        before do
          TestClass.any_instance.stubs(:shared_session).returns(locale_id: spanish.id)
        end

        describe "with no session locale id set" do
          before do
            TestClass.any_instance.stubs(:shared_session).returns({})
            agent.translation_locale = japanese
            agent.save!
          end

          it "use the current users locale" do
            request_locale = nil
            @preserve_locale_test_2.send(:localize_user) { request_locale = I18n.locale }

            assert_equal japanese.to_sym, agent.translation_locale.to_sym
            assert_equal japanese.to_sym, request_locale
          end
        end

        describe "with no current_user" do
          before do
            TestClass.any_instance.stubs(:current_user).returns(nil)
          end

          it "defaults to ENGLISH_BY_ZENDESK" do
            request_locale = nil
            @preserve_locale_test_2.send(:localize_user) { request_locale = I18n.locale }

            assert_equal ENGLISH_BY_ZENDESK.to_sym, agent.translation_locale.to_sym
            assert_equal ENGLISH_BY_ZENDESK.to_sym, request_locale
          end
        end
      end
    end
  end
end
