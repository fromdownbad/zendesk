require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'Zendesk::I18n::LanguageSettlement' do
  fixtures :translation_locales

  def settle_language(user_locale, available_languages)
    settle = Zendesk::I18n::LanguageSettlement.new(HttpAcceptLanguage::Parser.new(user_locale), available_languages)
    settle.find_matching_zendesk_locale
  end

  before do
    @english = translation_locales(:english_by_zendesk)
    @japanese = translation_locales(:japanese)
    @japanese_x = translation_locales(:japanese_x)
    @pt_br = translation_locales(:brazilian_portuguese)
    @spanish = translation_locales(:spanish)
    @french = translation_locales(:french)
    @zh_trad = translation_locales(:chinese_traditional)
    @zh_simple = translation_locales(:chinese_simplified)
    @spanish_la = translation_locales(:latin_american_spanish)
  end

  describe 'LanguageSettlement' do
    it 'matchs simple locales' do
      assert_equal @english, settle_language('en-us', [@english, @spanish])
      assert_equal @english, settle_language('en-GB', [@english, @spanish])
    end

    it 'matchs complex Accept-Language strings' do
      assert_equal @english, settle_language('ja,en-US;q=0.8,en;q=0.6,fr-FR;q=0.4,fr;q=0.2', [@english, @spanish])
      assert_equal @japanese, settle_language('ja,en-US;q=0.8,en;q=0.6,fr-FR;q=0.4,fr;q=0.2', [@english, @japanese])
    end

    it 'matchs secondary locales' do
      assert_equal @english, settle_language('fr,en-US;q=0.8', [@english])
      assert_equal @japanese, settle_language('fr,ja;q=0.8', [@english, @japanese])
    end

    it 'matchs fallback locales' do
      assert_equal @spanish_la, settle_language('fr,es-MX;q=0.8', [@english, @spanish_la])
      assert_equal @spanish_la, settle_language('es-PR', [@english, @spanish_la])
      assert_equal @spanish_la, settle_language('es-PR', [@spanish, @spanish_la])

      assert_equal @zh_trad, settle_language('zh-Hant', [@zh_simple, @zh_trad])

      assert_equal @zh_trad, settle_language('zh-HK', [@zh_simple, @zh_trad])

      assert_equal @zh_simple, settle_language('zh-Hans', [@zh_simple, @zh_trad])
    end

    it 'returns nil on no match found' do
      assert_nil settle_language('en-us', [@french])
      assert_nil settle_language('ja,en-US;q=0.8,en;q=0.6,fr-FR;q=0.4,fr;q=0.2', [@spanish_la])
    end
  end
end
