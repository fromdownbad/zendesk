require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'Zendesk::I18n::TranslationFiles' do
  let(:path) { ['test/files/test_locales'] }
  let(:translation_files) { Zendesk::I18n::TranslationFiles::Proxy.new(path) }

  it 'has JS translations' do
    assert_equal ['txt.default.forums.announcements.name', 'txt.not.in.api.best_support'], translation_files.instance.js_keys
  end

  it 'has translations' do
    assert_equal 'One', translation_files.instance.translations['txt.not.in.api.nested.one']
  end
end
