require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

class I18nLanguageDetectionTestController < ActionController::Base
  include Zendesk::I18n::LanguageDetection
  include Zendesk::Auth::AuthenticatedSessionMixin
  include Zendesk::Auth::Warden::ControllerMixin

  def show
    head 200
  end
end

class I18n::LanguageDetectionControllerTest < ActionController::TestCase
  fixtures :accounts, :translation_locales, :users, :user_identities, :subscriptions, :addresses, :account_property_sets

  tests I18nLanguageDetectionTestController
  use_test_routes

  before do
    @minimum_account = accounts(:minimum)
    @request.account = @account = @minimum_account
    @controller.stubs(:current_account).returns(@account)

    @request.stubs(:language_region_compatible_from).returns("en-US")
    @english = translation_locales(:english_by_zendesk)
    @japanese = translation_locales(:japanese)
    @spanish = translation_locales(:spanish)
    @la_spanish = translation_locales(:latin_american_spanish)
    @japanese_x = translation_locales(:japanese_x)
    Account.any_instance.stubs(:available_languages).returns([@english, @japanese])
    set_header('HTTP_ACCEPT_LANGUAGE', "ja,en-US;q=0.8,en;q=0.6,fr-FR;q=0.4,fr;q=0.2")
  end

  describe "#normalize_locale" do
    it "normalize ja-x-test to ja" do
      assert_equal "ja", @controller.send(:normalize_locale, @japanese_x.locale)
    end
  end

  describe "#account_available_locales" do
    before do
      Account.any_instance.stubs(:available_languages).returns([@english, @japanese_x])
    end

    it "normalize only the custom locale ja-x-test" do
      locales = @controller.send(:account_available_locales)

      assert_equal @english.locale, locales.first
      assert_equal @controller.send(:normalize_locale, @japanese_x.locale), locales.last
    end
  end

  describe "#available_locales" do
    before do
      Account.any_instance.stubs(:available_languages).returns([@english, @japanese_x])
    end

    it "normalize the japanese locale so #best_language_match will work" do
      assert_equal "ja", @controller.send(:available_locales).last
    end
  end

  describe "perform_language_detection" do
    it "set locale for end-user without locale" do
      login(users(:minimum_end_user))
      @controller.expects(:set_user_locale)
      get :show
    end

    it "not set local for agents" do
      login('minimum_agent')
      @controller.expects(:set_user_locale).never
      get :show
    end

    it "not set local for users with locale" do
      user = users(:minimum_end_user)
      login(user)
      user.locale_id = 4
      @controller.expects(:set_user_locale).never
      get :show
    end

    describe "spanish language fallback" do
      before do
        Account.any_instance.stubs(:available_languages).returns([@english, @la_spanish, @spanish])
      end

      describe "with Spanish - Spain (es-es) in the request header" do
        before do
          user = users(:minimum_end_user)
          login(user)
          set_header("HTTP_ACCEPT_LANGUAGE", "es-es,es-mx;q=0.7,en-us;q=0.3")
        end

        it "match es-es with spanish" do
          @controller.expects(:set_user_locale).with(@spanish)

          get :show
        end
      end

      describe "with Latin American Spanish (es-419) in the request header" do
        before do
          set_header("HTTP_ACCEPT_LANGUAGE", "es-mx,es-es;q=0.7,en-us;q=0.3")
        end

        it "match es-mx with latin american spanish" do
          @controller.expects(:set_user_locale).with(@la_spanish)

          get :show
        end
      end
    end

    describe "user update" do
      let(:user) { users(:minimum_end_user) }

      it "update user on post" do
        user.update_attribute(:locale_id, nil)
        login(user)
        user.expects(:save).returns true
        Rails.logger.expects(:error).never
        post :show
      end

      it "not update user on get" do
        user.update_attribute(:locale_id, nil)
        login(user)
        user.expects(:save).never
        get :show
      end

      it "not fail when user is invalid" do
        user.update_attribute(:name, "")
        login(user)
        user.expects(:save).returns false
        Rails.logger.expects(:error)
        post :show
      end
    end

    describe "detect user's language preference" do
      before do
        @user = users(:minimum_end_user)
        login(@user)
        @user.locale_id = nil
        I18n.locale = translation_locales(:english_by_zendesk)
        @controller.stubs(:current_account).returns(@account)
      end

      it "set users translation locale and I18n locale to user's session locale" do
        shared_session[:locale_id] = @japanese.id
        @controller.expects(:best_language_match).never

        get :show
        assert_equal shared_session[:locale_id], @user.translation_locale.id
        I18n.locale.to_s.must_equal @japanese.to_s
      end

      describe "with Zendesk japanese as an available option" do
        it "set users translation locale and I18n locale to accepted languages from browser header if locale_id and shared_session[:locale_id] are nil" do
          shared_session[:locale_id] = nil
          @controller.expects(:best_language_match).returns("ja")

          get :show
          assert_equal @japanese, @user.translation_locale
          assert_equal @japanese.id, shared_session[:locale_id]
          I18n.locale.to_s.must_equal @japanese.to_s
        end
      end

      describe "with custom Japanese as an option" do
        before do
          Account.any_instance.stubs(:available_languages).returns([@english, @japanese_x])
        end

        it "set users translation locale and I18n locale to accepted languages from browser header if locale_id and shared_session[:locale_id] are nil" do
          shared_session[:locale_id] = nil
          @controller.expects(:best_language_match).returns("ja")

          get :show
          assert_equal @japanese_x, @user.translation_locale
          assert_equal @japanese_x.id, shared_session[:locale_id]
          I18n.locale.to_s.must_equal @japanese_x.to_s
        end
      end

      it "set users locale to I18n.translation_locale if matched_locale is nil" do
        shared_session[:locale_id] = nil
        Account.any_instance.stubs(:available_languages).returns([@english, @english])
        @controller.expects(:best_language_match).returns(nil)

        get :show
        assert_equal @english, @user.translation_locale
        assert_equal @english.id, shared_session[:locale_id]
        I18n.locale.to_s.must_equal @english.to_s
      end
    end
  end
end
