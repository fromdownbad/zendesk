require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::DB::Util do
  describe ".quote_for_like_clause" do
    it "escapes %" do
      assert_equal '\%foo\%', Zendesk::DB::Util.quote_for_like_clause("%foo%")
    end

    it "escapes _" do
      assert_equal '\_foo\_', Zendesk::DB::Util.quote_for_like_clause("_foo_")
    end

    it "performs standard connection escaping" do
      assert_equal "\\'foo.com", Zendesk::DB::Util.quote_for_like_clause("'foo.com")
    end
  end

  describe ".truthy?" do
    it "returns true for expected true values" do
      assert Zendesk::DB::Util.truthy?("true")
    end

    it "returns true for an interger" do
      assert Zendesk::DB::Util.truthy?(1)
    end

    it "returns false for false values" do
      refute Zendesk::DB::Util.truthy?("false")
    end
    it "returns false for unexcepected values" do
      refute Zendesk::DB::Util.truthy?(nil)
    end

    it "returns false for empty strings" do
      refute Zendesk::DB::Util.truthy?("")
    end
  end
end
