require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 9

describe Zendesk::DB::RaceGuard do
  describe "#cache_guarded" do
    it "runs when unlocked" do
      x = ""
      Zendesk::DB::RaceGuard.cache_guarded("blah") { x << "A" }
      assert_equal "A", x
    end

    it "blocks subsequent blocks on a guarded key" do
      x = ""
      Zendesk::DB::RaceGuard.cache_guarded("blah") { x << "A" }
      Zendesk::DB::RaceGuard.cache_guarded("blah") { x << "B" }
      assert_equal "A", x
    end

    it "timeouts appropriately" do
      x = ""
      Zendesk::DB::RaceGuard.cache_guarded("blah", 0.25) { x << "A" }
      Zendesk::DB::RaceGuard.cache_guarded("blah") { x << "B" }
      Timecop.travel(Time.now + 0.5.seconds)
      Zendesk::DB::RaceGuard.cache_guarded("blah") { x << "C" }
      assert_equal "AC", x
    end
  end

  describe "#cache_locked" do
    it "runs when unlocked" do
      x = 0
      Zendesk::DB::RaceGuard.cache_locked("blah") { x += 1 }
      assert_equal 1, x
    end

    it "blows up when cache is not unlocked in time" do
      x = 0
      Rails.cache.write("blah", true)
      assert_raise Timeout::Error do
        Zendesk::DB::RaceGuard.cache_locked("blah") { x += 1 }
      end
      assert_equal 0, x
    end

    it "executes on timeout when instructed to" do
      x = 0
      Rails.cache.write("blah", true)
      Zendesk::DB::RaceGuard.cache_locked("blah", execute_on_timeout: true) { x += 1 }
      assert_equal 1, x
    end

    it "waits while locked" do
      x = ""
      main_thread = Thread.current
      Thread.new { Zendesk::DB::RaceGuard.cache_locked("blah") { main_thread.wakeup; x << "A"; sleep 0.2; x << "B" } } # rubocop:disable Lint/Sleep
      Thread.stop # Ensure spawned thread gets the lock first

      Zendesk::DB::RaceGuard.cache_locked("blah") { x << "C" }
      assert_equal "ABC", x
    end
  end
end
