require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::DB::BulkUidAllocation do
  let(:account) { accounts(:minimum) }

  describe "#reserve_global_uids" do
    let (:count) { 10 }

    it "should assign reserved uids to new records" do
      uids = CustomFieldOption.reserve_global_uids(count)
      assert_equal count, uids.length
      uids.each do |uid|
        o = CustomFieldOption.new(account: account, name: "Option #{uid}", value: "option_#{uid}")
        o.save
        assert_equal uid, o.id
      end
    end
  end
end
