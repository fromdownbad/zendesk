require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 9

describe 'AuthenticatedSession' do
  describe Zendesk::AuthenticatedSession do
    fixtures :accounts, :account_settings, :users, :user_settings, :role_settings

    before do
      @data    = Zendesk::SharedSession::Session.load({})
      @account = accounts(:minimum)
      @user    = users(:minimum_agent)
      @admin   = users(:minimum_admin)

      @account.settings.stubs(:session_timeout).returns(5)
    end

    def create_auth(options = {})
      options = {
        user: @user,
        account: @account,
        session: @data,
        store: true
      }.merge(options)

      Zendesk::AuthenticatedSession.create(options)
    end

    def create_session_auth(options = {})
      options = {
        via: 'password',
        session: Zendesk::SharedSession::Session.load({})
      }.merge(options)
      @session = options[:session]
      @session['auth_password_expired'] = true
      auth = create_auth(options)
      @session[Zendesk::SharedSession::Session::USER_KEY] = auth.current_user.id
      @session.save
      auth
    end

    describe "#create" do
      it "creates simple" do
        create_auth(via: 'password')
        @authentication = Zendesk::AuthenticatedSession.new(@data)
        assert @authentication.created_at
        assert_equal 'password', @authentication.via
      end

      it "creates a session authentication" do
        auth = create_session_auth
        assert_equal @account.id, auth.send(:session).session['account']
        assert_equal @user.id, auth.send(:session).session['warden.user.default.key']
      end

      it "does not invalidate session_id when assuming user" do
        old_id = @data.id
        @data[:auth_original_user_id] = @admin.id
        create_auth(via: 'manual')
        assert_equal old_id, @data.id
      end

      it "does not check password expiration when assuming user from monitor" do
        User.any_instance.expects(:password_expired?).never
        assert_equal false, create_auth(via: 'master_token').password_expired?
      end

      it "does not check password expiration when authenticating with google" do
        User.any_instance.expects(:password_expired?).never
        assert_equal false, create_auth(via: 'google').password_expired?
      end

      it "does not check password expiration when authenticating with office 365" do
        User.any_instance.expects(:password_expired?).never
        assert_equal false, create_auth(via: 'office_365').password_expired?
      end

      it "does not check password expiration when authenticating with remote auth" do
        User.any_instance.expects(:password_expired?).never
        assert_equal false, create_auth(via: 'remote').password_expired?
      end

      it "checks password expiration when authenticating with a password" do
        User.any_instance.expects(:password_expired?)
        assert_equal false, create_auth(via: 'password').password_expired?
      end

      it "invalidates session_id when switching user" do
        old_id = @data.id
        create_auth(via: 'manual')
        refute_equal old_id, @data.id
      end

      describe "invalidate_id?" do
        it "does not invalidate session_id when warden strategy says not to" do
          old_id = @data.id
          create_auth(via: 'challenge_token', invalidate_id: false)
          assert_equal old_id, @data.id
        end
      end
    end

    describe "authentication duration" do
      it "be set to the security policy of the user" do
        create_auth(via: 'manual')
        assert_equal @account.settings.session_timeout.try(:minutes).to_i, @data["auth_duration"]
      end

      it "be set to the security policy of the user, if remember me is activated and remove_stay_signed_in Arturo is enabled" do
        Arturo.enable_feature! :remove_stay_signed_in
        create_auth(via: 'manual', remember_me_requested: true)
        assert_equal @account.settings.session_timeout.try(:minutes).to_i, @data["auth_duration"]
      end

      it "be set to the max session duration if the remember me is activated" do
        create_auth(via: 'manual', remember_me_requested: true)
        assert_equal 24 * 60 * 60 * 14, @data["auth_duration"]
      end
    end

    describe "#destroy" do
      before do
        create_auth(via: 'password')
        @authentication = Zendesk::AuthenticatedSession.new(@data)
      end

      it "clears the session" do
        @authentication.destroy
        assert_equal({}, @data)
        assert_nil @data.session_key
      end
    end

    describe "#keepalive" do
      describe "when logging in" do
        before do
          @session_auth = create_session_auth(via: 'password')
          @authentication = Zendesk::AuthenticatedSession.new(@data)
          @authentication.touch
        end

        it "does not touch the session when recently updated" do
          updated_at = @session_auth.updated_at
          Timecop.travel(30.seconds.from_now) do
            @session_auth.keepalive
          end

          assert_equal updated_at, @session_auth.updated_at
        end

        it "touches the session when the period passed is long enough" do
          updated_at = @session_auth.updated_at
          Timecop.travel(2.minutes.from_now) do
            @session_auth.keepalive
          end

          assert (updated_at < @session_auth.updated_at), "New: #{@session_auth.updated_at} was not greater than original: #{updated_at}"
        end

        it "does not touch the user's and account's last_login when recently updated" do
          @session_auth.current_user.last_login = Time.now
          @session_auth.current_user.save!
          user_last_login = @session_auth.current_user.last_login
          account_last_login = @session_auth.current_user.account.last_login
          Timecop.travel(4.hours.from_now) do
            @session_auth.keepalive
          end

          new_user_last_login = @session_auth.current_user.reload.last_login
          new_account_last_login = @session_auth.current_user.reload.account.last_login
          assert_equal user_last_login.to_i, new_user_last_login.to_i
          assert_equal account_last_login.to_i, new_account_last_login.to_i
        end

        it "touches the user's and account's last_login when not recently updated" do
          @session_auth.current_user.last_login = Time.now
          @session_auth.current_user.save!
          user_last_login = @session_auth.current_user.last_login
          account_last_login = @session_auth.current_user.account.last_login
          Timecop.travel(1.days.from_now) do
            @session_auth.keepalive
          end

          new_user_last_login = @session_auth.current_user.reload.last_login
          new_account_last_login = @session_auth.current_user.reload.account.last_login
          assert (user_last_login < new_user_last_login), "New: #{new_user_last_login} was not greater than original: #{user_last_login}"
          assert (account_last_login < new_account_last_login), "New: #{new_account_last_login} was not greater than original: #{account_last_login}"
        end
      end

      describe "when assuming a user" do
        before do
          @data[:auth_original_user_id] = @admin.id
          @session_auth = create_auth(via: 'master_token')
          @authentication = Zendesk::AuthenticatedSession.new(@data)
          @authentication.touch
        end

        it "does not touch the user's and account's last_login" do
          @session_auth.current_user.last_login = Time.now
          @session_auth.current_user.save!
          user_last_login = @session_auth.current_user.last_login
          account_last_login = @session_auth.current_user.account.last_login
          Timecop.travel(1.days.from_now) do
            @session_auth.keepalive
          end

          new_user_last_login = @session_auth.current_user.reload.last_login
          new_account_last_login = @session_auth.current_user.reload.account.last_login
          assert_equal user_last_login.to_i, new_user_last_login.to_i
          assert_equal account_last_login.to_i, new_account_last_login.to_i
        end
      end
    end

    describe "#password_changed!" do
      before do
        @auth1 = create_session_auth(user: @user)
        @auth2 = create_session_auth(user: @user)
        assert_equal 2, @user.shared_sessions.count(:all)
      end

      describe "with a session" do
        describe_with_and_without_arturo_enabled :regenerate_session_id_after_self_password_change do
          it "deletes all sessions expect the current" do
            @auth1.password_changed_for_user!(@user)
            assert_equal 1, @user.shared_sessions.count(:all)
            assert_equal @auth1.send(:session).session.id, @user.shared_sessions.first.session_id
          end

          it "clears password expired from the session" do
            @auth1.password_changed_for_user!(@user)
            assert_nil @auth1.send(:session)['password_expired']
          end
        end

        describe_with_arturo_enabled :regenerate_session_id_after_self_password_change do
          it "marks current session to roll its session id" do
            current_session = @user.shared_sessions.first

            @auth1.password_changed_for_user!(@user)

            assert_equal current_session.reload.session_rollable?, true
          end
        end
      end

      describe "changing a password for another user" do
        before do
          @admin_session = create_session_auth(user: @admin)
          @admin_session.password_changed_for_user!(@user)
        end
        it "deletes all sessions for that user" do
          assert_equal 0, @user.shared_sessions.count(:all)
        end

        it "does not delete sessions for the acting user" do
          assert_equal @admin_session.send(:session).session.id, @admin.shared_sessions.first.session_id
        end
      end

      describe "with an explict shared_session_record_id" do
        it "deletes all sessions expect the specified session" do
          second_session = @user.shared_sessions.where(session_id: @auth2.send(:session).session.id).first
          assert second_session
          @auth1.password_changed_for_user!(@user, shared_session_record_id: second_session.id)

          assert_equal 1, @user.shared_sessions.count(:all)
          assert_equal second_session.session_id, @user.shared_sessions.first.session_id
        end
      end

      describe "when assuming" do
        before do
          @assumption = create_session_auth(user: @admin)
          @assumption.original_user = @admin
          @assumption.current_user = @user
          @assumption.password_changed_for_user!(@user)
        end

        it "deletes all sessions for the current user" do
          assert_equal 0, @user.shared_sessions.count(:all)
        end

        it "does not delete the sessions for the original user" do
          assert_equal 1, @admin.shared_sessions.count(:all)
          assert_equal @assumption.send(:session).session.id, @admin.shared_sessions.first.session_id
        end
      end

      describe "without a session" do
        it "deletes all sesssions for current user" do
          @auth1.send(:session).session[:id] = nil
          @auth1.password_changed_for_user!(@user)
          assert_equal 0, @auth1.current_user.shared_sessions.count(:all)
        end
      end
    end
  end
end
