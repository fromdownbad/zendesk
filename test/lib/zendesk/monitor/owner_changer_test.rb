require_relative '../../../support/test_helper'
require_relative '../../../support/suite_test_helper'

SingleCov.covered!

describe Zendesk::Monitor::OwnerChanger do
  include SuiteTestHelper

  fixtures :all

  # cannot use `around` since that uses Fiber which resets Thread.current
  def change_owner
    CIA.audit(actor: account.users.first) { changer.change_owner }
  end

  let(:account) { accounts(:minimum) }
  let!(:old_owner) { account.owner }
  let(:demote_role_id) { 0 }
  let(:new_owner) { users(:minimum_end_user) }
  let(:changer) { Zendesk::Monitor::OwnerChanger.new(account, new_owner, demote_role_id) }

  it 'promotes the new owner to admin' do
    change_owner

    assert(changer.success)
    account.reload
    assert_equal new_owner, account.owner
  end

  it 'demotes the old owner to an end user' do
    change_owner

    assert(changer.success, changer.message)
    refute old_owner.reload.is_agent?
  end

  it 'suspends the old owner' do
    change_owner

    assert(changer.success, changer.message)
    assert old_owner.reload.suspended?
  end

  describe 'when error is raised during owner change' do
    before do
      old_owner.stubs(:save).raises(StandardError, 'something is wrong')
    end

    it 'catches errors and return false' do
      change_owner

      assert_equal false, changer.success
      assert_equal 'An error occured during ownership change', changer.message
    end
  end

  describe 'when new owner is a end user, and demote_role_id is for agent' do
    let(:new_owner) { users(:minimum_end_user) }
    let(:demote_role_id) { 4 }

    it 'promotes the new owner to admin and demotes the old owner to an agent' do
      change_owner

      assert(changer.success)
      assert old_owner.reload.is_agent?
      assert new_owner.reload.is_admin?
    end

    it 'does not suspend old owner' do
      change_owner

      assert(changer.success)
      refute old_owner.reload.suspended?
    end
  end

  it 'catches an error when trying to demote the owner' do
    User.any_instance.stubs(:save).returns(false, true) # Second one is for restoration

    change_owner

    assert_equal false, changer.success, changer.message
    assert_equal "Can't downgrade current owner", changer.message
  end

  describe 'when account update failed and owner cannot be promoted back to admin' do
    let(:new_owner) { users(:minimum_admin_not_verified) }

    before do
      User.any_instance.stubs(:save).returns(true, false) # First one is for downgrade
      Account.any_instance.stubs(:save).returns(false, true)
    end

    it 'warns when the owner can not be promoted back to admin' do
      change_owner

      assert_equal false, changer.success, changer.message
      assert_equal "Update of the account failed; Owner is not an admin anymore", changer.message
    end
  end

  it 'warns when the owner can not be set back to the previous value' do
    User.any_instance.stubs(:save).returns(true, true)
    Account.any_instance.stubs(:save).returns(false, false)

    change_owner

    assert_equal false, changer.success, changer.message
    assert_equal "Update of the account failed; Couldn't restore the owner to its previous status", changer.message
  end

  describe 'when new owner cannot be suspended' do
    let(:new_owner) { users(:minimum_admin_not_verified) }
    before do
      User.any_instance.stubs(:save).returns(true, false)
      Account.any_instance.stubs(:save).returns(true)
    end

    it 'warns when the owner was changed but not suspended' do
      change_owner

      assert_equal false, changer.success, changer.message
      assert_equal 'Owner was changed but previous owner was not suspended', changer.message
    end
  end

  describe 'when the new owner is the current owner' do
    let(:new_owner) { old_owner }

    it 'does not update anything if the new owner is the current owner' do
      change_owner

      assert_equal false, changer.success, changer.message
      assert_equal 'Update of the account failed; New Owner is already the Current Owner', changer.message
      account.reload
      old_owner.reload
      assert_equal old_owner, account.owner
      refute old_owner.suspended?
    end
  end

  describe 'when the owner is the only admin' do
    let(:account) { accounts(:multiproduct) }
    let(:new_owner) { users(:multiproduct_support_agent) }

    before do
      Zendesk::StaffClient.any_instance.stubs(:update_entitlements!)
      # New Owner needs a mail
      new_owner.email = 'jdoe@zendesk.com'
      new_owner.save!
    end

    it 'updates new owner Support entitlement in Staff Service' do
      Zendesk::StaffClient.any_instance.expects(:update_entitlements!).once

      change_owner
    end

    it 'bypasses the validation enforcing to have at least one admin' do
      change_owner

      assert(changer.success, changer.message)
      refute old_owner.reload.is_agent?
      assert new_owner.reload.is_admin?
    end
  end
end
