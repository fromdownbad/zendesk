require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Monitor::Jobs::ExceptionTrackerHeartbeatJob do
  let(:job) { Zendesk::Monitor::Jobs::ExceptionTrackerHeartbeatJob }

  describe '.work' do
    before { job.work }

    before_should 'record an exception' do
      ZendeskExceptions::Logger.expects(:record)
    end
  end

  describe '.args_to_log' do
    it 'has no arguments to log' do
      assert_equal({}, job.args_to_log)
    end
  end
end
