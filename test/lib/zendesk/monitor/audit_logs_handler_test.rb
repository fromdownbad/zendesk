require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Monitor::AuditLogsHandler do
  fixtures :accounts, :users, :role_settings

  let(:handler) { Zendesk::Monitor::AuditLogsHandler.new(cia_event) }
  let(:cia_event) do
    CIA::Event.create!(
      account: account,
      actor:   account.owner,
      source:  account.owner,
      action:  'create',
      visible: true
    )
  end
  let(:account) { accounts(:minimum) }

  describe '#add_attribute_changes' do
    let(:event_params) do
      {
        account_id:          account_id,
        actor_id:            actor_id,
        actor_type:          actor_type,
        ip_address:          ip_address,
        source_id:           source_id,
        source_type:         source_type,
        source_display_name: source_display_name,
        message:             event_message,
        visible:             visible,
        action:              action,
        changes:             changes
      }
    end
    let(:account_id) { account.id }
    let(:actor_id) { account.owner.id }
    let(:actor_type) { 'User' }
    let(:ip_address) { '172.58.4.6' }
    let(:source_id) { cia_event.source.id }
    let(:source_type) { 'Voice::VoiceAccount' }
    let(:source_display_name) { 'Voice::VoiceAccount' }
    let(:event_message) { 'customer message' }
    let(:visible) { true }
    let(:action) { 'create' }
    let(:changes) { {attribute_name: [2, 3], another_attribute_name: [false, true]} }

    it 'adds attribute changes to the given CIA::Event' do
      handler.add_attribute_changes(event_params)

      check_common_attribute_changes
    end

    describe 'when action is not create' do
      let(:action) { 'update' }

      it 'adds attribute changes to the given CIA::Event' do
        handler.add_attribute_changes(event_params)

        check_common_attribute_changes
      end

      describe 'and app_setting_changes exists' do
        before { event_params.merge!(app_setting_changes: app_setting_changes) }

        let(:app_setting_changes) { {a: [1, 2], b: [true, false]} }

        it 'adds attribute changes as JSON dump to the given CIA::Event and ignores other changes' do
          handler.add_attribute_changes(event_params)

          assert_equal 1, cia_event.attribute_changes.size

          change = cia_event.attribute_changes.first
          assert_equal false, change.persisted?
          assert_equal account_id, change.account_id
          assert_equal cia_event.id, change.cia_event_id
          assert_equal source_id, change.source_id
          assert_equal source_type, change.source_type
          assert_equal 'settings', change.attribute_name
          assert_equal({a: 1, b: true}.to_json, change.old_value)
          assert_equal({a: 2, b: false}.to_json, change.new_value)
        end

        describe 'when the JSON dump is 255 bytes or more' do
          # Longer key/value for testing the block for `CIA::AttributeChange.serialize_for_storage`.
          # The block only gets run if the JSON dump has bytesize >= 255
          let(:app_setting_changes) do
            {
              some_longer_key_name: %w[some_longer_value_1 some_longer_value_2],
              another_longer_key_name1: %w[some_longer_value_3 some_longer_value_4],
              another_longer_key_name2: %w[some_longer_value_5 some_longer_value_6],
              another_longer_key_name3: %w[some_longer_value_7 some_longer_value_8],
              another_longer_key_name4: %w[some_longer_value_9 some_longer_value_10],
              another_longer_key_name5: %w[some_longer_value_11 some_longer_value_12]
            }
          end

          it 'adds attribute changes as JSON dump to the given CIA::Event and ignores other changes' do
            handler.add_attribute_changes(event_params)

            assert_equal 1, cia_event.attribute_changes.size

            change = cia_event.attribute_changes.first
            assert_equal false, change.persisted?
            assert_equal account_id, change.account_id
            assert_equal cia_event.id, change.cia_event_id
            assert_equal source_id, change.source_id
            assert_equal source_type, change.source_type
            assert_equal 'settings', change.attribute_name

            expected_old_value = {
              some_longer_key_name:     'some_longer_value_1',
              another_longer_key_name1: 'some_longer_value_3',
              another_longer_key_name2: 'some_longer_value_5',
              another_longer_key_name3: 'some_longer_value_7',
              another_longer_key_name4: 'some_longer_value_9'
            }.to_json

            expected_new_value = {
              some_longer_key_name:     'some_longer_value_2',
              another_longer_key_name1: 'some_longer_value_4',
              another_longer_key_name2: 'some_longer_value_6',
              another_longer_key_name3: 'some_longer_value_8',
              another_longer_key_name4: 'some_longer_value_10'
            }.to_json

            assert_equal expected_old_value, change.old_value
            assert_equal expected_new_value, change.new_value
          end
        end
      end
    end

    def check_common_attribute_changes
      assert_equal(changes.keys.count, cia_event.attribute_changes.size)

      change = cia_event.attribute_changes.detect { |c| c.attribute_name.to_s == 'attribute_name' }
      assert_equal false, change.persisted?
      assert_equal account_id, change.account_id
      assert_equal cia_event.id, change.cia_event_id
      assert_equal source_id, change.source_id
      assert_equal source_type, change.source_type
      assert_equal '2', change.old_value.to_s
      assert_equal '3', change.new_value.to_s

      change = cia_event.attribute_changes.detect { |c| c.attribute_name.to_s == 'another_attribute_name' }
      assert_equal false, change.persisted?
      assert_equal account_id, change.account_id
      assert_equal cia_event.id, change.cia_event_id
      assert_equal source_id, change.source_id
      assert_equal source_type, change.source_type
      assert_equal '0', change.old_value
      assert_equal '1', change.new_value
    end
  end
end
