require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::Retrier do
  describe ".retry_on_error" do
    it "works when nothing happens" do
      Zendesk::Retrier.retry_on_error([TypeError], 5) { 1 }.must_equal 1
    end

    it "retries once on error" do
      c = 0
      Zendesk::Retrier.retry_on_error([TypeError], 5) do
        c += 1
        raise TypeError if c == 1
        :returned
      end.must_equal :returned
      c.must_equal 2
    end

    it "does not retry when over limit" do
      c = 0
      assert_raises TypeError do
        Zendesk::Retrier.retry_on_error([TypeError], 5) do
          c += 1
          raise TypeError
        end
      end
      c.must_equal 5
    end

    it "does not retry on unknown error" do
      c = 0
      assert_raises TypeError do
        Zendesk::Retrier.retry_on_error([ArgumentError], 5) do
          c += 1
          raise TypeError
        end
      end
      c.must_equal 1
    end

    it "does not retry when condition is not met" do
      c = 0
      retry_call = nil
      assert_raises TypeError do
        Zendesk::Retrier.retry_on_error([TypeError], 5, retry_if: -> (e) { retry_call = e; false }) do
          c += 1
          raise TypeError
        end
      end
      c.must_equal 1
      retry_call.class.must_equal TypeError
    end

    it "retries when condition is met" do
      c = 0
      retry_call = nil
      assert_raises TypeError do
        Zendesk::Retrier.retry_on_error([TypeError], 5, retry_if: -> (e) { retry_call = e; true }) do
          c += 1
          raise TypeError
        end
      end
      c.must_equal 5
      retry_call.class.must_equal TypeError
    end

    it "yield the exception" do
      c = 0
      yielded = []
      Zendesk::Retrier.retry_on_error([TypeError], 5) do |e|
        c += 1
        yielded << e
        raise TypeError if c == 1
      end
      c.must_equal 2
      yielded.map(&:class).must_equal [NilClass, TypeError]
    end
  end
end
