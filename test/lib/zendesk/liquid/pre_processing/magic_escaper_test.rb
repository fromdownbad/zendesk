require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Liquid::PreProcessing::MagicEscaper do
  before do
    @context = stub
    Zendesk::Liquid::PreProcessing::MagicEscaper.any_instance.stubs(:account_has_markdown_comments?).returns(markdown_enabled?)
    Zendesk::Liquid::PreProcessing::MagicEscaper.any_instance.stubs(:account_has_rich_text_comments?).returns(rich_text_enabled?)
    @context.stubs(:content_type?).with(Mime[:text]).returns(true)
  end

  describe "with markdown and rich text enabled" do
    let(:markdown_enabled?) { true }
    let(:rich_text_enabled?) { true }

    [
      # when using conditionals in liquid passed into the rich text
      # editor, replace the html character codes
      ["{% assign my_var = ticket.id %}{% if my_var &gt;= 0.4 %}ZenTrue{% else %}ZenFalse{% endif %}<br>", "{% assign my_var = ticket.id %}{% if my_var >= 0.4 %}ZenTrue{% else %}ZenFalse{% endif %}<br>"],
      ["{% if ticket.brand.name == 'Sample&amp;Sample' %} YES {% else %} NO {% endif %}<br>", "{% if ticket.brand.name == 'Sample&Sample' %} YES {% else %} NO {% endif %}<br>"],

      # when evaluating content with html character codes and backspaces, do not eat them!
      ["\"title\":\"{{ticket.title | replace:'&gt;','!' | replace:'\"','!' | replace:'\\','\\\\'}}", "\"title\":\"{{ticket.title | replace:'>','!' | replace:'\"','!' | replace:'\\','\\\\'}}"],

    # when using liquid within a markdown codeblock it
      # does not evaluate liquid placeholders
      ["Hello \n\n```\nMr. {{ticket.id}}\n```\nYou marvelous bastard!", "Hello \n\n```\nMr. {% raw %}{{ticket.id}}{% endraw %}\n```\nYou marvelous bastard!"],
        # evaluates incomplete/invalid code blocks
      ["Hello \n\n``````\nMr. {{ticket.id}}\n````\nYou marvelous bastard!", "Hello \n\n``````\nMr. {% raw %}{{ticket.id}}{% endraw %}\n````\nYou marvelous bastard!"],
        # works inline too
      ["Hello ```Mr. {{ticket.id}}``` You marvelous bastard!", "Hello ```Mr. {% raw %}{{ticket.id}}{% endraw %}``` You marvelous bastard!"],
      ["Hello `Mr. {{ticket.id}}` You marvelous bastard!", "Hello `Mr. {% raw %}{{ticket.id}}{% endraw %}` You marvelous bastard!"],
        # leaves stuff alone"
      ["Testing:\n\n```\n{% if ticket.cc_names != empty %}\nCC names: {{ticket.cc_names}}\n{% endif %}\n```\n\nstuff", "Testing:\n\n```\n{% raw %}{% if ticket.cc_names != empty %}{% endraw %}\nCC names: {% raw %}{{ticket.cc_names}}{% endraw %}\n{% raw %}{% endif %}{% endraw %}\n```\n\nstuff"],
        # escapes in wysiwyg code segments
      ["<code>INLINE {{ticket.requester}}</code> normal text <pre>CODE BLOCK {{ticket.requester}}</pre>", "<code>INLINE {% raw %}{{ticket.requester}}{% endraw %}</code> normal text <pre>CODE BLOCK {% raw %}{{ticket.requester}}{% endraw %}</pre>"],
      # when there is no liquid it
        # should not change the comment
      ["```\nvar root = 'C:'\nvar dir1 = 'windows'\nvar dir2 = 'system32'\nvar path = root + '\\' + dir1 + '\\' + dir2;\n```", "```\nvar root = 'C:'\nvar dir1 = 'windows'\nvar dir2 = 'system32'\nvar path = root + '\\' + dir1 + '\\' + dir2;\n```"],
        # it only escapes the correct liquid if the same tag appears more than once
      ["`{{ticket.id}}`\n\n{{ticket.id}}", "`{% raw %}{{ticket.id}}{% endraw %}`\n\n{{ticket.id}}"],
        # it escapes the second block if the first doesn't contain liquid
      ["<code>one<br></code>What if there's text here?<br><pre>two {{ticket.requester}}&nbsp;</pre>", "<code>one<br></code>What if there's text here?<br><pre>two {% raw %}{{ticket.requester}}{% endraw %}&nbsp;</pre>"]
    ].each do |template, expected|
      it "escapes markdown and rich text" do
        assert_equal expected, Zendesk::Liquid::PreProcessing::MagicEscaper.new(@context, template).apply
      end
    end
  end

  describe "with markdown enabled and rich text disabled" do
    let(:markdown_enabled?) { true }
    let(:rich_text_enabled?) { false }

    [
    # when using liquid within a markdown codeblock it
      # does not evaluate liquid placeholders
      ["Hello \n\n```\nMr. {{ticket.id}}\n```\nYou marvelous bastard!", "Hello \n\n```\nMr. {% raw %}{{ticket.id}}{% endraw %}\n```\nYou marvelous bastard!"],
      # doesn't escape in wysiwyg code segments
      ["<code>INLINE {{ticket.requester}}</code> normal text <pre>CODE BLOCK {{ticket.requester}}</pre>", "<code>INLINE {{ticket.requester}}</code> normal text <pre>CODE BLOCK {{ticket.requester}}</pre>"]
    ].each do |template, expected|
      it "escapes markdown but not rich text" do
        assert_equal expected, Zendesk::Liquid::PreProcessing::MagicEscaper.new(@context, template).apply
      end
    end
  end

  describe "with markdown disabled and rich text enabled" do
    let(:markdown_enabled?) { false }
    let(:rich_text_enabled?) { true }

    [
    # when using liquid within a markdown codeblock it
      # does not escape liquid placeholders
      ["Hello \n\n```\nMr. {{ticket.id}}\n```\nYou marvelous bastard!", "Hello \n\n```\nMr. {{ticket.id}}\n```\nYou marvelous bastard!"],
        # escapes in wysiwyg code segments
      ["<code>INLINE {{ticket.requester}}</code> normal text <pre>CODE BLOCK {{ticket.requester}}</pre>", "<code>INLINE {% raw %}{{ticket.requester}}{% endraw %}</code> normal text <pre>CODE BLOCK {% raw %}{{ticket.requester}}{% endraw %}</pre>"]
    ].each do |template, expected|
      it "escapes markdown but not rich text" do
        assert_equal expected, Zendesk::Liquid::PreProcessing::MagicEscaper.new(@context, template).apply
      end
    end
  end

  describe "with markdown and rich text disabled" do
    let(:markdown_enabled?) { false }
    let(:rich_text_enabled?) { false }

    [
    # when using liquid within a markdown codeblock it
      # does not escape liquid placeholders
      ["Hello \n\n```\nMr. {{ticket.id}}\n```\nYou marvelous bastard!", "Hello \n\n```\nMr. {{ticket.id}}\n```\nYou marvelous bastard!"],
      # doesn't escape in wysiwyg code segments
      ["<code>INLINE {{ticket.requester}}</code> normal text <pre>CODE BLOCK {{ticket.requester}}</pre>", "<code>INLINE {{ticket.requester}}</code> normal text <pre>CODE BLOCK {{ticket.requester}}</pre>"]
    ].each do |template, expected|
      it "doesn't escape markdown or rich text" do
        assert_equal expected, Zendesk::Liquid::PreProcessing::MagicEscaper.new(@context, template).apply
      end
    end
  end
end
