require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Liquid::AnswerBot::Renderability do
  fixtures :accounts, :tickets, :ticket_deflections

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }
  let(:renderability) { Zendesk::Liquid::AnswerBot::Renderability.new(ticket: ticket, account: account) }

  before do
    Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
    AnswerBot::Deflectability.any_instance.stubs(:deflected_by_another_channel?).returns(false)
  end

  describe "#can_render_deflection_information" do
    describe 'when the answer bot account setting is enabled' do
      describe 'and a deflection exists' do
        describe 'and deflection articles have not been presented by another channel' do
          it 'returns true' do
            assert renderability.can_render_deflection_information?
          end
        end

        describe 'and deflection articles have been presented by another channel' do
          before do
            AnswerBot::Deflectability.any_instance.stubs(:deflected_by_another_channel?).returns(true)
          end

          it 'returns false' do
            refute renderability.can_render_deflection_information?
          end
        end
      end

      describe 'and a deflection does not exist' do
        before do
          ticket.ticket_deflection = nil
          ticket.will_be_saved_by User.system
          ticket.save!
        end

        it 'returns false' do
          refute renderability.can_render_deflection_information?
        end
      end
    end

    describe 'when the answer bot account setting is disabled' do
      before do
        Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(false)
      end

      it 'returns false' do
        refute renderability.can_render_deflection_information?
      end
    end
  end
end
