require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Liquid::AnswerBot::UrlHelper do
  fixtures :tickets, :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:auth_token) { stub('auth_token', value: '1234.abcd.wxyz') }
  let(:article) do
    {
      title: 'Title',
      body: 'Body',
      html_url: 'http://whatever/123',
      id: '3453249857'
    }
  end
  let(:articles) { [article] }
  let(:article_ids) { articles.map { |x| x[:id] }.map(&:to_i) }
  let(:config) do
    Zendesk::Liquid::AnswerBot::UrlHelper.new(
      account: account,
      ticket: ticket,
      article_ids: article_ids
    )
  end

  before do
    AutomaticAnswersJwtToken.stubs(:encrypted_token).returns(auth_token.value)
  end

  describe '#help_center_article_url' do
    let(:expected) { "https://minimum.zendesk-test.com/requests/automatic-answers/ticket/article/#{article[:id]}?auth_token=#{auth_token.value}" }

    it 'returns the help center article redirection url' do
      assert_equal expected, config.help_center_article_url(article[:id])
    end

    it 'encrypts an auth token with the correct data' do
      AutomaticAnswersJwtToken.expects(:encrypted_token).with(
        account.id,
        ticket.requester_id,
        ticket.nice_id,
        ticket.ticket_deflection.id,
        article_ids
      )

      config.help_center_article_url(article[:id])
    end
  end

  describe '#close_request_url' do
    let(:expected) { "https://minimum.zendesk-test.com/requests/automatic-answers/ticket/solve/article/#{article[:id]}?auth_token=#{auth_token.value}" }

    it 'returns the close request redirection url' do
      assert_equal expected, config.close_request_url(article[:id])
    end

    it 'encrypts an auth token with the correct data' do
      AutomaticAnswersJwtToken.expects(:encrypted_token).with(
        account.id,
        ticket.requester_id,
        ticket.nice_id,
        ticket.ticket_deflection.id,
        article_ids
      )

      config.close_request_url(article[:id])
    end
  end

  describe '#reject_article_url' do
    let(:expected) { "https://minimum.zendesk-test.com/requests/automatic-answers/ticket/feedback/article/#{article[:id]}?auth_token=#{auth_token.value}" }

    it 'returns the reject article redirection url' do
      assert_equal expected, config.reject_article_url(article[:id])
    end

    it 'encrypts an auth token with the correct data' do
      AutomaticAnswersJwtToken.expects(:encrypted_token).with(
        account.id,
        ticket.requester_id,
        ticket.nice_id,
        ticket.ticket_deflection.id,
        article_ids
      )

      config.reject_article_url(article[:id])
    end
  end

  describe 'host' do
    let(:expected) { 'minimum.zendesk-test.com' }

    it 'returns the host name of the account' do
      assert_equal expected, config.host
    end
  end

  describe 'static_assets_host' do
    describe 'when not defined' do
      ENV['ZENDESK_STATIC_ASSETS_DOMAIN'] = ''

      let(:expected) { 'static.zdassets.com' }

      it 'returns the default static assets host' do
        assert_equal expected, config.static_assets_host
      end
    end
  end
end
