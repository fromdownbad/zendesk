require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Liquid::AnswerBot::Drop do
  fixtures :tickets, :users, :ticket_deflections, :ticket_deflection_articles

  let(:end_user) { users(:minimum_end_user) }
  let(:ticket) { tickets(:minimum_1) }
  let(:hc_article) do
    {
       title: 'zomg',
       body: 'lorem    ipsum <a href="/hc/en-us/lorem-ipsum">here</a>  <p></p>',
       html_url: 'http://whatever/123',
       id: '3453249857'
    }
  end
  let(:format) { Mime::HTML.to_s }
  let(:drop) { Zendesk::Liquid::AnswerBot::Drop.new(ticket, format) }
  let(:fetched_articles) { [hc_article] }
  let(:url_helper) do
    stub(
      'url_helper',
      help_center_article_url: 'https://article',
      close_request_url: 'https://close',
      reject_article_url: 'https://reject',
      host: 'minimum.zendesk-test.com',
      static_assets_host: 'static.zdassets-test.com'
    )
  end
  let(:can_render_deflection_information?) { true }
  let(:renderability) do
    stub(
      'renderability',
      can_render_deflection_information?: can_render_deflection_information?
    )
  end

  before do
    Zendesk::Liquid::AnswerBot::Renderability.
      stubs(:new).
      returns(renderability)
    Zendesk::HelpCenter::InternalApiClient.any_instance.
      stubs(:fetch_articles_for_ids_and_locales).
      returns(fetched_articles)
    Zendesk::Liquid::AnswerBot::UrlHelper.
      stubs(:new).
      returns(url_helper)
  end

  describe '#article_count' do
    describe 'when deflection information can be rendered' do
      it 'returns the number of fetched articles' do
        assert_equal 1, drop.article_count
      end
    end

    describe 'when deflection information cannot be rendered' do
      let(:can_render_deflection_information?) { false }

      it 'returns 0' do
        assert_equal 0, drop.article_count
      end
    end
  end

  describe '#article_list' do
    describe 'when deflection information can be rendered' do
      describe 'and one or more articles can be fetched from help center' do
        describe 'and the Mime format is HTML' do
          it 'renders a html article list' do
            Zendesk::Liquid::AnswerBot::ArticleListDrop.
              expects(:new).
              with(
                articles: fetched_articles,
                url_helper: url_helper,
                font_family: Zendesk::Liquid::AnswerBot::Drop::SYSTEM_FONT_STACK
              )

            drop.article_list
          end

          it 'instruments the render' do
            Zendesk::Liquid::AnswerBot::ArticleListDrop.
              any_instance.
              expects(:timed_render)

            drop.article_list
          end
        end

        describe 'and the Mime format is TEXT' do
          let(:format) { Mime::TEXT.to_s }

          it 'renders a plain text article list' do
            Zendesk::Liquid::AnswerBot::ArticleListPlainTextDrop.
              expects(:new).
              with(
                articles: fetched_articles,
                url_helper: url_helper
              )

            drop.article_list
          end

          it 'instruments the render' do
            Zendesk::Liquid::AnswerBot::ArticleListPlainTextDrop.
              any_instance.
              expects(:timed_render)

            drop.article_list
          end
        end
      end

      describe 'and no articles can be fetched from the help center' do
        before do
          Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).returns([])
        end

        it 'returns an empty string' do
          assert_equal "", drop.article_list
        end
      end

      describe 'and an exception is thrown' do
        let(:log_message) { "Unable to generate automatic_answers email for account id: #{ticket.account.id} ticket id: #{ticket.id} ticket nice id: #{ticket.nice_id}" }
        let(:exception) { StandardError.new }

        before do
          Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).raises(exception)
        end

        it 'records a log entry' do
          ZendeskExceptions::Logger.expects(:record).with do |e, params|
            assert_equal(e, exception)
            assert_equal(log_message, params[:message])
            assert_equal(drop, params[:location])
          end
          drop.article_list
        end

        it 'returns an empty string' do
          assert_equal "", drop.article_list
        end
      end
    end

    describe 'when deflection information cannot be rendered' do
      let(:can_render_deflection_information?) { false }

      it 'returns an empty string' do
        assert_equal "", drop.article_list
      end
    end
  end

  describe '#first_article_body' do
    describe 'when deflection information can be rendered' do
      describe 'and one or more articles can be fetched from help center' do
        it 'instruments the render' do
          Zendesk::Liquid::AnswerBot::FirstArticleBodyDrop.any_instance.expects(:timed_render)

          drop.first_article_body
        end

        it 'renders the first article HTML title' do
          title = Nokogiri::HTML(drop.first_article_body).css('.first-article-title').text

          assert_equal hc_article[:title], title
        end

        it 'renders the first article HTML body' do
          body = 'lorem ipsum <a href="https://minimum.zendesk-test.com/hc/en-us/lorem-ipsum">here</a><p></p>'

          assert_includes drop.first_article_body, body
        end
      end

      describe 'and no articles can be fetched from the help center' do
        before do
          Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).returns([])
        end

        it 'returns an empty string' do
          assert_equal "", drop.first_article_body
        end
      end

      describe 'and an exception is thrown' do
        let(:log_message) { "Unable to generate automatic_answers email for account id: #{ticket.account.id} ticket id: #{ticket.id} ticket nice id: #{ticket.nice_id}" }
        let(:exception) { StandardError.new }

        before do
          Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).raises(exception)
        end

        it 'records a log entry' do
          ZendeskExceptions::Logger.expects(:record).with do |e, params|
            assert_equal(e, exception)
            assert_equal(log_message, params[:message])
            assert_equal(drop, params[:location])
          end

          drop.first_article_body
        end

        it 'returns an empty string' do
          assert_equal "", drop.first_article_body
        end
      end
    end

    describe 'when deflection information cannot be rendered' do
      let(:can_render_deflection_information?) { false }

      it 'returns an empty string' do
        assert_equal "", drop.first_article_body
      end
    end

    describe 'when the format is plain text' do
      let(:format) { Mime::TEXT.to_s }

      it 'returns an empty string' do
        assert_equal "", drop.first_article_body
      end
    end
  end

  describe "#fetch_articles memoization" do
    let(:drop) { Zendesk::Liquid::AnswerBot::Drop.new(ticket, format) }
    let(:fetched_articles) { [hc_article] }

    before do
      Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
      Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).returns(fetched_articles)
      Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:answer_bot_setting_enabled?).returns(false)
    end

    describe "when fetched_articles has not been memoized" do
      it "requests articles" do
        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:fetch_articles_for_ids_and_locales).once

        drop.article_list
      end
    end

    describe "when fetched_articles has been memoized" do
      it "returns the fetched_articles instance variable" do
        Zendesk::HelpCenter::InternalApiClient.any_instance.expects(:fetch_articles_for_ids_and_locales).once

        drop.article_list
        drop.first_article_body
      end
    end
  end
end
