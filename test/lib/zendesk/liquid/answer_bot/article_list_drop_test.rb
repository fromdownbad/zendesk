require_relative "../../../../support/test_helper"
require "zendesk/liquid/answer_bot/article_list_drop"

SingleCov.covered!

describe Zendesk::Liquid::AnswerBot::ArticleListDrop do
  let(:articles) do
    [
      { title: "\f Winter is coming. \t ",
        body: 'lorem ipsum <a href="/hc/en-us/lorem-ipsum">here</a>  <p></p>',
        html_url: 'http://whatever/123',
        id: '3453249857' },
      { title: "\t The Lannisters \r send their regards.",
        body: 'Amet lorem',
        html_url: 'http://whatever/123',
        id: '4643534535' },
      { title: "I'm not going to stop the wheel.\n\r     I'm going to break the wheel. \r\n ",
        body: 'Dolar amet',
        html_url: 'http://whatever/456',
        id: '1340598736' }
    ]
  end
  let(:url_helper) do
    stub(
      'url_helper',
      help_center_article_url: 'https://minimum.zendesk-test.com/requests/automatic-answers/ticket/article/123/?auth_token=blah',
      close_request_url: 'https://minimum.zendesk-test.com/requests/automatic-answers/ticket/solve/123/?auth_token=blah',
      host: 'minimum.zendesk-test.com',
      static_assets_host: 'static.zdassets-test.com'
    )
  end
  let(:drop) do
    Zendesk::Liquid::AnswerBot::ArticleListDrop.new(
      articles: articles,
      url_helper: url_helper,
      font_family: 'comic sans!'
    )
  end

  describe "#render" do
    describe "given one or more articles" do
      describe "when rendering help center article titles and redirection links" do
        let(:icon) { Nokogiri::HTML(drop.render).css('.article-icon').first }
        let(:links) { Nokogiri::HTML(drop.render).css('.article-link') }

        it 'renders an article icon with a fully qualified url' do
          assert_equal 'https://static.zdassets-test.com/classic/images/icons/article.png', icon[:src]
        end

        it "renders the correct number of links" do
          assert_equal 3, links.length
        end

        it "includes a help center url for each article" do
          article_ids = articles.map { |article| article[:id] }

          article_ids.map do |article_id|
            url_helper.expects(:help_center_article_url).with(article_id)
          end

          drop.render
        end

        it "renders a sanitized title inside each link" do
          titles = links.map(&:text)

          assert titles.include? "Winter is coming."
          assert titles.include? "The Lannisters send their regards."
          assert titles.include? "I'm not going to stop the wheel. I'm going to break the wheel."
        end
      end

      describe "when rendering buttons to close the request" do
        let(:buttons) { Nokogiri::HTML(drop.render).css('.close-request-button a') }

        it "renders the correct number of buttons" do
          assert_equal 3, buttons.length
        end

        it "includes a close request url for each button" do
          article_ids = articles.map { |article| article[:id] }

          article_ids.map do |article_id|
            url_helper.expects(:close_request_url).with(article_id)
          end

          drop.render
        end

        it "renders a call to action text string" do
          assert_match Regexp.new(I18n.t('txt.admin.answer_bot.email.first_article.close_request')), buttons.first.css('span').text
        end
      end

      it 'renders the snippet for each article' do
        assert_equal 3, Nokogiri::HTML(drop.render).css('.article-snippet').length
      end
    end

    describe "when articles is empty" do
      let(:articles) { [] }

      it "returns an empty string" do
        assert_empty drop.render
      end
    end
  end
end
