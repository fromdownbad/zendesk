require_relative '../../../../support/test_helper'
require 'zendesk/liquid/answer_bot/first_article_body_drop'

SingleCov.covered!

describe Zendesk::Liquid::AnswerBot::FirstArticleBodyDrop do
  let(:articles) do
    [
      { title: "\f Winter is coming. \t ",
        body: 'lorem ipsum <a href="/hc/en-us/lorem-ipsum">here</a>  <p></p>',
        html_url: 'http://whatever/123',
        id: '3453249857' },
      { title: "\t The Lannisters \r send their regards.",
        body: 'Amet lorem',
        html_url: 'http://whatever/123',
        id: '4643534535' },
      { title: "I'm not going to stop the wheel.\n\r     I'm going to break the wheel. \r\n ",
        body: 'Dolar amet',
        html_url: 'http://whatever/456',
        id: '1340598736' }
    ]
  end
  let(:close_request_url) { 'https://minimum.zendesk-test.com/requests/automatic-answers/ticket/solve/123/?auth_token=blah' }
  let(:reject_article_url) { 'https://minimum.zendesk-test.com/requests/automatic-answers/ticket/feedback/123/?auth_token=blah' }
  let(:url_helper) do
    stub(
      'url_helper',
      close_request_url: close_request_url,
      reject_article_url: reject_article_url,
      host: 'minimum.zendesk-test.com',
      static_assets_host: 'static.zdassets-test.com'
    )
  end

  let(:drop) do
    Zendesk::Liquid::AnswerBot::FirstArticleBodyDrop.new(
      articles: articles,
      url_helper: url_helper,
      font_family: 'comic sans'
    )
  end

  describe '#render' do
    describe 'given one or more articles' do
      describe 'renders the first article title' do
        let(:icon) { Nokogiri::HTML(drop.render).css('.article-icon img').first }
        let(:title) { Nokogiri::HTML(drop.render).css('.first-article-title') }

        it 'with an article icon with a fully qualified url' do
          assert_equal 'https://static.zdassets-test.com/classic/images/icons/article.png', icon[:src]
        end

        it 'with sanitized whitespace' do
          assert_equal 'Winter is coming.', title.text
        end
      end

      describe 'renders the article body section including' do
        describe 'the first article body' do
          let(:expected) { 'lorem ipsum <a href="https://minimum.zendesk-test.com/hc/en-us/lorem-ipsum">here</a><p></p>' }
          let(:actual) { Nokogiri::HTML(drop.render).css('.first-article-body').children.to_html }

          it 'with sanitized whitespace and converted urls' do
            assert_equal expected, actual
          end
        end

        describe 'a button for users to close their request' do
          let(:link) { Nokogiri::HTML(drop.render).css('.first-article-actions a').first }

          it 'with a url to solve the ticket and redirect to the article' do
            assert_equal close_request_url, link[:href]
          end

          it 'with a `Yes, close my request` translation' do
            assert_match Regexp.new(I18n.t('txt.admin.answer_bot.email.first_article.close_request')), link.css('span').text
          end
        end

        describe 'a button for users to say the article did not answer their question' do
          let(:link) { Nokogiri::HTML(drop.render).css('.first-article-actions a')[1] }

          it 'with a url to record feedback and redirect to the article' do
            assert_equal reject_article_url, link[:href]
          end

          it 'with a `No` translation' do
            assert_match Regexp.new(I18n.t('txt.admin.answer_bot.email.first_article.not_solved_request')), link.css('span').text
          end
        end
      end
    end

    describe 'when articles is empty' do
      let(:articles) { [] }

      it 'returns an empty string' do
        assert_empty drop.render
      end
    end
  end
end
