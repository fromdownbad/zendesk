require_relative '../../../../support/test_helper'
require 'zendesk/liquid/answer_bot/article_list_plain_text_drop'

SingleCov.covered!

describe Zendesk::Liquid::AnswerBot::ArticleListPlainTextDrop do
  let(:articles) do
    [
      { title: "\f Winter is coming. \t ",
        body: "lorem ipsum <a href='/hc/en-us/lorem-ipsum'>here</a>  <p></p>",
        html_url: "http://whatever/123",
        id: "3453249857" },
      { title: "\t The Lannisters \r send their regards.",
        body: "Amet lorem",
        html_url: "http://whatever/123",
        id: "4643534535" },
      { title: "I'm not going to stop the wheel.\n\r     I'm going to break the wheel. \r\n ",
        body: "Dolar amet",
        html_url: "http://whatever/456",
        id: "1340598736" }
    ]
  end
  let(:url_helper) do
    stub(
      'url_helper',
      help_center_article_url: 'https://minimum.zendesk-test.com/requests/automatic-answers/ticket/article/123/?auth_token=blah',
      close_request_url: 'https://minimum.zendesk-test.com/requests/automatic-answers/ticket/solve/123/?auth_token=blah',
      host: 'minimum.zendesk-test.com'
    )
  end
  let(:drop) do
    Zendesk::Liquid::AnswerBot::ArticleListPlainTextDrop.new(
      articles: articles,
      url_helper: url_helper
    )
  end

  describe '#render' do
    describe 'when articles exist' do
      let(:article_ids) { articles.map { |article| article[:id] } }

      it 'renders a help center article url for each article' do
        article_ids.map do |id|
          url_helper.
            expects(:help_center_article_url).
            with(id)
        end

        drop.render
      end

      it 'renders a close request url for each article' do
        article_ids.map do |id|
          url_helper.
            expects(:close_request_url).
            with(id)
        end

        drop.render
      end
    end

    describe 'when articles do not exist' do
      let(:articles) { [] }

      it 'returns an empty string' do
        assert_empty drop.render
      end
    end
  end
end
