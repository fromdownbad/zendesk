require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Liquid::AnswerBot::ArticleRenderer do
  describe '#present?' do
    describe 'basic html input' do
      let(:article_body) do
        "<p>Lorem Ipsum is simply dummy &nbsp;text of   the printing   and <b>typesetting</b> industry. Lorem Ipsum \n has been the industry standard dummy text ever since the 1500s</p>"
      end
      let(:expected) do
        "Lorem Ipsum is simply dummy &nbsp;text of the printing and typesetting industry. Lorem Ipsum has been the industry..."
      end

      it 'returns sanitized and truncated text' do
        assert_equal expected, Zendesk::Liquid::AnswerBot::ArticleRenderer.present(article_body)
      end
    end

    describe 'complex html input' do
      let(:article_body) do
        "<div data-type='custom-html'> <script>console.log(10);</script><style> #toc { background-color:rgba(187,222,251,.6);</style><p>Lorem Ipsum is dummy text and <b>very cool</b>.</p></div>"
      end
      let(:expected) do
        "Lorem Ipsum is dummy text and very cool."
      end

      it 'returns sanitized text when article body contains html' do
        assert_equal expected, Zendesk::Liquid::AnswerBot::ArticleRenderer.present(article_body)
      end

      describe "when html exceeds Nokogumbo::DEFAULT_MAX_TREE_DEPTH" do
        it "returns the expected html fragment" do
          nested_html_content = nest_html_content(article_body, Nokogumbo::DEFAULT_MAX_TREE_DEPTH + 1)
          assert_equal expected, Zendesk::Liquid::AnswerBot::ArticleRenderer.present(nested_html_content)
        end
      end

      describe "when a SystemStackError exception is raised" do
        before { Nokogiri::HTML5.stubs(:fragment).raises(SystemStackError).once }

        it "returns an empty string" do
          Rails.logger.expects(:warn).once.with(regexp_matches(/Answer Bot article body could not be parsed/))
          assert_equal '', Zendesk::Liquid::AnswerBot::ArticleRenderer.present(article_body)
        end
      end
    end
  end

  private

  def nest_html_content(html_content, nesting)
    "#{'<div>' * nesting}#{html_content}#{'</div>' * nesting}"
  end
end
