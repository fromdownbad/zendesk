require_relative "../../../support/test_helper"
require_relative "../../../support/mailer_test_helper"

SingleCov.not_covered! # kitchen sink of random liquid tests

describe 'LiquidScenarios' do
  include MailerTestHelper
  fixtures :all

  before do
    @ticket = tickets(:minimum_1)
    @current_user = users(:minimum_agent)

    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    Timecop.travel(Time.parse("2012-08-17 13:45:37 -0700"))
    Account.any_instance.stubs(:uses_12_hour_clock).returns(false)
  end

  describe "a template with double newlines separating blocks of text" do
    before do
      template = "Uw aanvraag is afgerond. \n\nOm dit ticket te heropenen kunt u deze e-mail beantwoorden.\n\nIndien u telefonisch contact met ons opneemt verzoeken wij u het onderstaande Ticket ID te vermelden:\n\nTicket ID\#{{ticket.id}}\n\n{{ticket.public_comments_formatted}}"

      definition = Definition.new
      definition.conditions_all.push(DefinitionItem.build(
        operator: "less_than",
        source: "status_id",
        value: ["4"],
        first_value: "4"
      ))

      definition.actions.push(DefinitionItem.build(
        operator: "is",
        source: "notification_user",
        value: ["current_user", "SUBJECT", template]
      ))

      trigger = Trigger.new(
        title: "DC",
        definition: definition,
        account: accounts(:minimum),
        owner: accounts(:minimum)
      )
      trigger.save!

      @ticket.will_be_saved_by(@current_user)
      @ticket.add_comment(body: "foo\n\nbar")
      @ticket.save!

      @email = ActionMailer::Base.deliveries.last
    end

    it "generates paragaph elements for each text block" do
      expected = "<p dir=\"auto\">Uw aanvraag is afgerond. </p><p dir=\"auto\">Om dit ticket te heropenen kunt u deze e-mail beantwoorden.</p><p dir=\"auto\">Indien u telefonisch contact met ons opneemt verzoeken wij u het onderstaande Ticket ID te vermelden:</p>"
      assert_includes body_by_content_type(@email, Mime[:html]), expected
    end
  end

  describe "for templates containing the newline_to_br filter" do
    before do
      @ticket.will_be_saved_by(@current_user)
      @ticket.add_comment(body: "foo\n\nbar")
      @ticket.save!
      @context = Zendesk::Liquid::TicketContext.new(template_1, @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
    end

    it "correctly renders" do
      assert_equal "<p>foo</p><p>bar</p><p>minimum ticket one duplicate</p><p>     LINE 1</p><p>    LINE 2</p><p>minimum ticket 1</p><p>minimum ticket 1</p>",
        @context.render
    end
  end

  describe "for templates containing the replace_first filter" do
    before do
      @ticket.will_be_saved_by(@current_user)
      @ticket.add_comment(body: "foo\n\nbar\n\nbaz")
      @ticket.save!
      @context = Zendesk::Liquid::TicketContext.new(template_2, @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
    end

    it "correctly renders" do
      assert_equal "<p> </p><hr style=\"border: none 0; border-top: 1px dashed #c2c2c2; height: 1px; margin: 15px 0 0 0;\" /><br /><strong>Agent Minimum, Aug 17, 2012, 22:45 (private):</strong><div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\"><p dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\">foo</p><br /><p dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\"></p></div>",
        @context.render
    end
  end

  describe "for templates containing rich text" do
    before do
      @ticket.will_be_saved_by(@current_user)
      @ticket.add_comment(html_body: "<ul>\n<li>\nHello\n</li>\n</ul>")
      @ticket.save!
      @context = Zendesk::Liquid::TicketContext.new("{{ticket.comments_formatted}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
      @rendered = @context.render
    end

    it "correctly renders" do
      assert_match %r{<ul style="[^"]*" dir="auto">(\s*|<br />)<li style="[^"]*">(<br />)?Hello(<br />)?</li>(\s*|<br />)</ul>}, @rendered
    end
  end

  describe "for templates containing rich text, on accounts with rtl arturo enabled" do
    before do
      Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(true)
      @ticket.will_be_saved_by(@current_user)
      @ticket.add_comment(html_body: "<ul>\n<li>\nHello\n</li>\n</ul>")
      @ticket.save!
      @context = Zendesk::Liquid::TicketContext.new("{{ticket.comments_formatted}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
      @rendered = @context.render
    end

    it "correctly renders with auto directionality" do
      assert_match %r{<ul style="[^"]*" dir=\"auto\">(\s*|<br />)<li style="[^"]*">(<br />)?Hello(<br />)?</li>(\s*|<br />)</ul>}, @rendered
    end
  end

  describe "for templates containing markdown" do
    describe "standard case" do
      before do
        Comment.any_instance.expects(:process_as_markdown?).at_least_once.returns(true)

        @ticket.will_be_saved_by(@current_user)
        @ticket.add_comment(body: "# foo\n* bar\n* baz\n\n## qux")
        @ticket.save!
        @context = Zendesk::Liquid::TicketContext.new("{{ticket.comments_formatted}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
        @rendered = @context.render
      end

      it "correctly renders" do
        @rendered.must_include "foo</h1>"
        @rendered.must_include "bar</li>"
        @rendered.must_include "baz</li>"
        @rendered.must_include "qux</h2>"
      end
    end

    describe "just a header" do
      before do
        Comment.any_instance.expects(:process_as_markdown?).at_least_once.returns(true)

        @ticket.will_be_saved_by(@current_user)
        @ticket.add_comment(body: "# foo")
        @ticket.save!
        @context = Zendesk::Liquid::TicketContext.new("{{ticket.comments_formatted}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
        @rendered = @context.render
      end

      it "correctly renders" do
        refute_includes @rendered, "Z_START_MARKDOWN"
        assert_includes @rendered, "foo</h1>"
      end
    end
  end

  describe "for templates containing code sections" do
    before do
      @ticket.account.settings.markdown_ticket_comments = true
      @ticket.account.save!

      @ticket.will_be_saved_by(@current_user)
      @ticket.add_comment(body: body)
      @ticket.save!
    end

    describe "inline code segment" do
      let(:body) { "`{{ticket.requester.first_name}}`" }

      it "does not expand placeholders" do
        context = Zendesk::Liquid::TicketContext.new("{{ticket.latest_comment}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:text])
        context.render.must_include "`{{ticket.requester.first_name}}`"
      end
    end

    describe "valid code block" do
      let(:body) { "# HEADER\n\n```\nparagraph 1\nparagraph 2\nparagraph 3\n```" }

      it "correctly renders and escapes liquid" do
        Comment.any_instance.expects(:process_as_markdown?).at_least_once.returns(true)
        context = Zendesk::Liquid::TicketContext.new("{{ticket.comments_formatted}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])

        render_result = context.render
        render_result.must_include "paragraph 1\nparagraph 2\nparagraph 3"
      end
    end

    describe "invalid liquid code block" do
      let(:body) { template_4 }

      it "does not print raw escapes for malformed liquid" do
        context = Zendesk::Liquid::TicketContext.new("{{ticket.latest_comment}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:text])

        assert_match template_4, context.render
      end
    end
  end

  describe "for templates containing split on space" do
    before { @context = Zendesk::Liquid::TicketContext.new(template_3, @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html]) }

    it "correctly renders" do
      @context.render.must_equal <<~LIQUID.delete("\n")
        <p>First word: Uses<br />
        First word: Uses<br />
        Second word: cheat<br />
        Last word: boring.<br />
        All words: Uses, cheat, codes,, calls, the, game, boring.<br /></p>
        <ul><p></p><li>Uses</li><p></p><li>cheat</li><p></p><li>codes,</li><p></p><li>calls</li><p></p><li>the</li><p></p><li>game</li><p></p><li>boring.</li><p></p></ul>
      LIQUID
    end
  end

  private

  def template_1
    "{% for comment in ticket.comments %}" \
      "{{newline_to_br}}\n" \
      "{{comment.value | newline_to_br}}\n" \
    "{% endfor %}"
  end

  # Don't ask...
  def template_2
    "{% assign startdiv = 'xxxxx<br />' %}" \
    "{% capture ebody %} {{ ticket.comments_formatted }} {% endcapture %}" \
    "{{ ebody | replace_first:'bar',startdiv | split:'xxxxx' | first | html_safe }}"
  end

  def template_3
    "{% assign words = 'Uses cheat codes, calls the game boring.' | split: ' ' %}\n" \
    "First word: {{ words.first }}\n" \
    "First word: {{ words[0] }}\n" \
    "Second word: {{ words[1] }}\n" \
    "Last word: {{ words.last }}\n" \
    "All words: {{ words | join: ', ' }}\n" \
    "<ul>\n" \
      "{% for word in words %}\n" \
      "<li>{{ word }}</li>\n" \
      "{% endfor %}\n" \
    "</ul>"
  end

  def template_4
    "```\n" \
    "{% case application.name %}\n" \
    "{% when 'My application (1)' %}\n" \
    "<!-- Email template for application 1 -->\n" \
    "{% when 'My application (2)' %]\n" \
    "<!-- Email template for application 2 -->\n" \
    "{% endcase %}\n" \
    "```"
  end
end
