require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Liquid::DcContext do
  fixtures :tickets, :accounts, :users, :translation_locales, :groups, :addresses, :subscriptions, :user_identities

  before do
    @japanese = translation_locales(:japanese)
    @spanish = translation_locales(:spanish)

    @ticket = tickets(:minimum_5)
    @user = @ticket.requester
    @user.account.locale_id = @spanish.id

    fallback_locale = translation_locales(:english_by_zendesk)
    @fbattrs = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: fallback_locale.id }
    @cms_text = Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: @fbattrs, account: @ticket.account)
    @cms_japanese_variant = @cms_text.variants.create!(active: true, translation_locale_id: @japanese.id, value: "こんにちわ！ワムバとはスノボ−ドがすきです！")
    @cms_spanish_variant = @cms_text.variants.create!(active: true, translation_locale_id: @spanish.id, value: "ello! wotcher! wombats are awesome")
  end

  describe "When users locale is set" do
    before do
      @user.locale_id = @japanese.id
    end

    it "renders variant in users locale" do
      @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @user.account, @user, "text/plain")
      assert_equal @cms_japanese_variant.value, @context.render
    end

    describe "when the active variant is inactive" do
      before do
        @cms_japanese_variant.active = false
        @cms_japanese_variant.save!
        @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @user.account, @user, "text/plain")
      end

      it "renders fallback variant if users locale variant is not active" do
        cms_fallback_variant = @cms_text.fallback
        assert_equal cms_fallback_variant.value, @context.render
      end
    end
  end

  describe "When the dynamic content identifier is dc.ticket" do
    it "does not return a Ticket object and return the right value" do
      fallback_locale = translation_locales(:english_by_zendesk)
      @fbattrs = {is_fallback: true, nested: true, value: "The Ticket", translation_locale_id: fallback_locale.id }
      @cms_text = Cms::Text.create!(name: "Ticket", fallback_attributes: @fbattrs, account: @ticket.account)

      @context = Zendesk::Liquid::DcContext.new("{{dc.ticket}}", @user.account, @user, "text/plain")
      assert_equal "The Ticket", @context.render
    end
  end

  describe "When locale param is passed" do
    it "renders variant of locale" do
      @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @user.account, @user, "text/plain", locale: @japanese)
      @user.expects(:translation_locale).never
      assert_equal @cms_japanese_variant.value, @context.render
    end

    describe "when the active variant is inactive" do
      before do
        @cms_japanese_variant.active = false
        @cms_japanese_variant.save!
        @user.expects(:translation_locale).never
        @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @user.account, @user, "text/plain", locale: @japanese)
      end

      it "renders fallback variant if locale variant is not active" do
        cms_fallback_variant = @cms_text.fallback
        assert_equal cms_fallback_variant.value, @context.render
      end
    end
  end

  describe "#render" do
    before { @user.locale_id = @japanese.id }

    it "renders variant of locale" do
      @context = Zendesk::Liquid::DcContext.render("{{dc.welcome_wombat}}", @user.account, @user, "text/plain", @japanese)
      @user.expects(:translation_locale).never
      assert_equal @cms_japanese_variant.value, @context
    end

    it "renders user if no locale is pass" do
      @context = Zendesk::Liquid::DcContext.render("{{dc.welcome_wombat}}", @user.account, @user, "text/plain")
      assert_equal @cms_japanese_variant.value, @context
    end
  end

  describe "When users locale is nil" do
    before do
      @user.locale_id = nil
    end

    it "renders variant in the account's locale" do
      @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @user.account, @user, "text/plain")
      assert_equal @cms_spanish_variant.value, @context.render
    end

    describe "and the account's locale is nil" do # KB Huh?
      it "renders fallback variant" do
        @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @user.account, @user, "text/plain")
        @context.stubs(:locale).returns(nil)

        cms_fallback_variant = @cms_text.fallback
        assert_equal cms_fallback_variant.value, @context.render
      end
    end

    describe "When requester is nil" do # KB Huh?
      before { @ticket.requester = nil }

      it "renders fallback variant" do
        @context = Zendesk::Liquid::TicketContext.new("{{dc.welcome_wombat}}", @ticket, nil, true, @ticket.account.translation_locale, "text/plain")
        assert_equal @cms_text.fallback.value, @context.render
      end
    end
  end

  describe "Cms::Generic Context" do
    it "ifs user set, use current_user's locale from the context to render cms content" do
      @user.locale_id = @japanese.id
      @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @ticket.account, @user, Mime[:html])
      assert_equal @cms_japanese_variant.value, @context.render
    end
  end

  describe "When format needed is html" do
    before do
      @cms_text = Cms::Text.create!(name: "Hello Honey Badger", fallback_attributes: @fbattrs, account: @ticket.account)
      @context = Zendesk::Liquid::DcContext.new("{{dc.hello_honey_badger}}", @ticket.account, @user, Mime[:html].to_s)
    end
    it "escapes content before rendering" do
      unescaped_text = " <hr /><div>HI!</div>"
      @cms_text.variants.create!(
        active: true,
        translation_locale_id: @spanish.id,
        value: unescaped_text
      )
      assert_equal " &lt;hr /&gt;&lt;div&gt;HI!&lt;/div&gt;", @context.render
    end
  end

  describe "When user is nil" do
    it "renders fallback variant" do
      @user = nil
      @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @ticket.account, @user, "text/plain")
      assert_equal @cms_text.fallback.value, @context.render
    end
  end

  describe "render_dc_markdown parameter" do
    describe "when it is passed as true" do
      before { @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @ticket.account, @user, Mime[:text], render_dc_markdown: true) }

      it "attempts to process markdown" do
        Zendesk::Liquid::CmsDrop.any_instance.expects(:process_markdown).at_least(1)
        @context.render
      end
    end

    describe "when it is not passed" do
      before { @context = Zendesk::Liquid::DcContext.new("{{dc.welcome_wombat}}", @ticket.account, @user, Mime[:text]) }

      it "does not attempt to process markdown" do
        Zendesk::Liquid::CmsDrop.any_instance.expects(:process_markdown).never
        @context.render
      end
    end
  end
end
