require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 9

describe Zendesk::Liquid::Context do
  fixtures :all
  let(:account) { accounts(:minimum) }

  class DummyContext < Zendesk::Liquid::Context
    attr_writer :account

    def fields
      { 'a' => 'b' }
    end
  end

  before do
    DummyContext.any_instance.stubs(:account).returns(account)
  end

  describe "#template_contains_liquid?" do
    describe "when the template contains liquid markers" do
      before { @context = DummyContext.new("{{a.b}}", stub) }
      it "returns true" do
        assert @context.template_contains_liquid?
      end
    end

    describe "when the template contains inline liquid" do
      before { @context = DummyContext.new("{% asdf %}", stub) }
      it "returns true" do
        assert @context.template_contains_liquid?
      end
    end

    describe "when the template doesn't contain liquid" do
      before { @context = DummyContext.new("wut foo bar { ba } } } { ", stub) }
      it "returns false" do
        assert_equal false, @context.template_contains_liquid?
      end
    end
  end

  describe "#render" do
    let(:japanese) { translation_locales(:japanese) }

    describe "when the template does not contain liquid" do
      before do
        @template = "template"
        @context = DummyContext.new(@template, stub)
      end
      it "returns the original template" do
        Zendesk::Liquid::Context.any_instance.expects(:render_template).never
        assert_equal @template, @context.render
      end
    end

    describe "when the context has been nested > 5 times" do
      before do
        @context = DummyContext.new("{{template}}", stub)
        @context.nesting_level = 6
      end

      it "returns nil" do
        assert_nil @context.render
      end
    end

    describe "when the template contains a URI encoded href containing placeholder" do
      before do
        input_template = %(
          # a href with a placeholder
          <a href=\"http://mydomain-1.com/?email=%7B%7Bticket.requester_1.email%7D%7Daa\" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>

          # href with two placeholders
          <a href=\"http://mydomain-2.com/?email=%7B%7Bticket.requester_2.email%7D%7D&email2=%7B%7Bticket.requester_22.email%7D%7Dbb\" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>

          # another href with a placeholder
          <a href="http://mydomain-3.com/?email=%7B%7Bticket.requester_3.email%7D%7Dcc" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>

          # href with with missing closing %7D%7D
          <a href=\"http://mydomain-4.com/?email=%7B%7Bticket.requester_4.emaildd\" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>

          # no href, but with URI enclode placeholders
          <a some=\"http://mydomain-5.com/?email=%7B%7Bticket.requester_5.email%7D%7Dee\" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>
        )
        @converted_template = %(
          # a href with a placeholder
          <a href=\"http://mydomain-1.com/?email={{ticket.requester_1.email}}aa\" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>

          # href with two placeholders
          <a href=\"http://mydomain-2.com/?email={{ticket.requester_2.email}}&email2={{ticket.requester_22.email}}bb\" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>

          # another href with a placeholder
          <a href="http://mydomain-3.com/?email={{ticket.requester_3.email}}cc" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>

          # href with with missing closing %7D%7D
          <a href=\"http://mydomain-4.com/?email=%7B%7Bticket.requester_4.emaildd\" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>

          # no href, but with URI enclode placeholders
          <a some=\"http://mydomain-5.com/?email=%7B%7Bticket.requester_5.email%7D%7Dee\" \
          target=\"_blank\"rel=\"noreferrer\">http://mydomain.com/?email={{ticket.requester.email}}</a>
        )
        @context = DummyContext.new(input_template, japanese)
      end
      it "converts placeholders within href" do
        @context.render
        assert_equal @context.template, @converted_template
      end
    end

    describe "valid templates" do
      before do
        @context = DummyContext.new("{{a}}", japanese)
      end

      it "sets the locale" do
        I18n.expects(:with_locale).with(japanese).returns("what")
        @context.render.must_equal "what"
      end

      describe "when #format is nil" do
        before { @context.format = nil }

        it "disables HTML escaping of Liquid variables" do
          Liquid::Extensions::HtmlSafety.expects(:with_enabled).with(false).returns("what")
          assert_equal "what", @context.render
        end
      end

      describe "when #format is Mime[:text]" do
        before { @context.format = Mime[:text] }

        it "disables HTML escaping of Liquid variables" do
          Liquid::Extensions::HtmlSafety.expects(:with_enabled).with(false).returns("what")
          @context.render.must_equal "what"
        end
      end

      describe "when #format is Mime[:html]" do
        before { @context.format = Mime[:html] }

        it "enables HTML escaping of Liquid variables" do
          Liquid::Extensions::HtmlSafety.expects(:with_enabled).with(true).returns("what")
          @context.render.must_equal "what"
        end
      end

      it "renders something reasonable" do
        @context.render.must_equal "b"
      end
    end

    describe "invalid templates" do
      before do
        @context = DummyContext.new("- {{a}} | {{b -", japanese, options)
      end

      describe "when raw template option is passed in" do
        let(:options) { {raw_template: "foo bar"} }

        it "returns raw template" do
          @context.render.must_equal "foo bar"
        end
      end

      describe "when no raw template is passed in" do
        let(:options) { {} }

        it "returns the template" do
          @context.render.must_equal "- {{a}} | {{b -"
        end
      end
    end

    describe "dynamic content" do
      before do
        @context = DummyContext.new(template, ENGLISH_BY_ZENDESK)
      end

      describe "escaped placeholders" do
        let(:template) { '\{{dc.foobar}}' }

        before do
          @context.dc_cache = { 'foobar' => 'Hello, world!' }
        end

        it "leaves the placeholder alone" do
          @context.expects(:template_contains_liquid?).returns(false)
          @context.render.must_equal '\{{dc.foobar}}'
        end
      end

      describe "when dc_cache contains the placeholder" do
        let(:template) { '{{dc.foobar}}' }

        before do
          @context.dc_cache = {
            'foobar' => 'Hello, world!',
            'barfoo' => 'Goodbye, all.',
            'placeholder' => 'placeholder',
            'html' => '<div class="foo">Div</div>'
          }
        end

        it "replaces the placeholder" do
          @context.expects(:template_contains_liquid?).returns(false)
          @context.render.must_equal 'Hello, world!'
        end

        it "does not call #render_template" do
          @context.expects(:render_template).never
          @context.render
        end

        describe "extra whitespace" do
          let(:template) { '{{ dc.foobar }}' }

          it "replaces the placeholder" do
            @context.expects(:template_contains_liquid?).returns(false)
            @context.render.must_equal 'Hello, world!'
          end
        end

        describe "newlines" do
          let(:template) { "{{dc.foobar}}\n{{dc.barfoo}}\n\nSome text then a {{dc.placeholder}}" }

          it "replaces all placeholders" do
            @context.expects(:template_contains_liquid?).returns(false)
            @context.render.must_equal "Hello, world!\nGoodbye, all.\n\nSome text then a placeholder"
          end
        end

        describe "html" do
          let(:template) { "{{dc.html}}\n<p>This is a paragraph</p>" }

          it "doesn't escape the HTML" do
            @context.expects(:template_contains_liquid?).returns(false)
            @context.render.must_equal "<div class=\"foo\">Div</div>\n<p>This is a paragraph</p>"
          end
        end
      end

      describe "when dc_cache is not provided" do
        let(:template) { '{{dc.foobar}}' }

        before do
          Cms::Text.create! do |t|
            t.name = "foobar"
            t.fallback_attributes = {
              is_fallback: true,
              nested: true,
              translation_locale_id: ENGLISH_BY_ZENDESK.id,
              value: "foosball"
            }
            t.account = account
          end
        end

        describe_with_arturo_disabled :dc_account_content_cache_default do
          before do
            @context = DummyContext.new(template, ENGLISH_BY_ZENDESK)
          end

          it "leaves the placeholder in the string during the shortcut" do
            @context.expects(:template_contains_liquid?).returns(false)
            @context.render.must_equal '{{dc.foobar}}'
          end

          it "calls #render_template" do
            @context.expects(:render_template).returns('').once
            @context.render
          end
        end

        describe_with_arturo_enabled :dc_account_content_cache_default do
          before do
            @context = DummyContext.new(template, ENGLISH_BY_ZENDESK)
          end

          it "inserts the placeholder in the string during the shortcut" do
            @context.expects(:template_contains_liquid?).returns(false)
            @context.render.must_equal 'foosball'
          end

          it "calls #render_template" do
            @context.expects(:render_template).never
            @context.render
          end
        end
      end

      describe "multiple pieces of dynamic content" do
        let(:template) { '{{dc.foo}} - {{dc.bar}}' }

        before do
          @context.dc_cache = { 'foo' => 'One', 'bar' => 'Two' }
          @context.expects(:template_contains_liquid?).returns(false)
        end

        it "replace all of the placeholders" do
          @context.render.must_equal 'One - Two'
        end

        describe "mixed escaped pieces" do
          describe "string starts with escaped dc" do
            let(:template) { '\{{dc.foo}} - {{dc.bar}}' }

            it "only replace the unescaped dc" do
              @context.render.must_equal '\{{dc.foo}} - Two'
            end
          end

          describe "string contains dc" do
            let(:template) { '{{dc.foo}} - \{{dc.bar}}' }

            it "only replace the unescaped dc" do
              @context.render.must_equal 'One - \{{dc.bar}}'
            end
          end
        end
      end

      describe "nested dynamic content" do
        let(:template) { '{{dc.one}}' }

        before do
          @context.expects(:template_contains_liquid?).returns(false)
        end

        describe "when dc_cache contains all of the placeholders" do
          before do
            @context.dc_cache = {
              'one'   => '{{dc.two}}',
              'two'   => '{{dc.three}}',
              'three' => '{{dc.four}}',
              'four'  => '{{dc.five}}',
              'five'  => '{{dc.six}}',
              'six'   => '{{dc.seven}}',
              'seven' => 'OH NO!'
            }
          end

          it "renders 5 levels deep" do
            @context.render.must_equal '{{dc.six}}'
          end
        end

        describe "when dc_cache only has some of the placeholders" do
          before do
            @context.dc_cache = {
              'one' => '{{dc.two}}',
              'two' => '{{dc.three}}'
            }
          end

          it "replaces the placeholders it has, and leaves the others alone" do
            @context.render.must_equal '{{dc.three}}'
          end
        end
      end

      describe "renderer memoization" do
        let(:template) { '{{dc.one}} ' }

        before do
          @context.stubs(:template_contains_liquid?).returns(false)
        end

        it 'queries the database at most once' do
          assert_sql_queries(0..1) do
            10.times { @context.render }
          end
        end
      end
    end

    describe "timing" do
      before do
        @context = DummyContext.new("{{template}}", ENGLISH_BY_ZENDESK)
        @context.account = accounts(:minimum)
        @context.format = Mime[:text]
      end

      it "notifies DataDog including content_type tag" do
        Zendesk::Liquid::Context.expects(:time_and_record).with('render_and_post_process', ["content_type:text/plain", "context:#{@context.class}"]).yields
        @context.render
      end
    end

    # addressing a weird bug in liquid-c gem, c_parse method does not handle templates that contain a solo hyphen
    # we can refactor this test once the library is patched, for now let's just use this for coverage
    # https://support.zendesk.com/agent/tickets/3721549
    describe "template contains solo hyphen" do
      let(:template) { "No {{-}} spaces!" }
      let(:context) { DummyContext.new(template, ENGLISH_BY_ZENDESK) }

      describe "when liquid_c enabled" do
        before { Arturo.enable_feature!(:liquid_c) }

        it "raise an error" do
          assert Liquid::C.enabled
          assert_raise ArgumentError do
            context.render
          end
        end
      end

      describe "when liquid_c disabled" do
        before { Arturo.disable_feature!(:liquid_c) }

        it "should render" do
          refute Liquid::C.enabled
          assert_equal "Nospaces!", context.render
        end
      end
    end

    describe "when suppress_placeholder option is true" do
      let(:template) { "{{ticket.comments}} {{dc.bar}} thanks!" }
      let(:statsd_client) { stub_for_statsd }

      before do
        DummyContext.stubs(:statsd_client).returns(statsd_client)
        Rails.logger.stubs(:info)
        @context = DummyContext.new(template, ENGLISH_BY_ZENDESK, suppress_placeholder: true)
      end

      it "removes the placeholder from the template" do
        @context.render
        assert_equal " {{dc.bar}} thanks!", @context.template
      end

      it "logs the placeholder removed" do
        Rails.logger.expects(:info).with('Public comment placeholder suppressed: ["{{ticket.comments}}"]')
        @context.render
      end

      it "increments a statsd_client" do
        statsd_client.expects(:increment).with("liquid_context.placeholder_suppressed")
        @context.render
      end

      describe "when there are no placeholders in the suppressed list to be removed" do
        let(:template) { "No placeholders" }

        it "does not log" do
          Rails.logger.expects(:info).with('Public comment placeholder suppressed: ["{{ticket.comments}}"]').never
          @context.render
        end

        it "does not send any stats" do
          statsd_client.expects(:increment).with("liquid_context.placeholder_suppressed").never
          @context.render
        end
      end

      describe "when a target placeholder is in the dynamic content" do
        let(:template) { "{{dc.foobar}} {{dc.placeholder}}" }

        before do
          @context = DummyContext.new(template, ENGLISH_BY_ZENDESK, suppress_placeholder: true)
          @context.dc_cache = {
            'foobar' => 'Hello, world!',
            'placeholder' => '{{ticket.comments}}'
          }
          @context.render
        end

        it "removes the placeholder from the lower dynamic content level" do
          assert_equal "Hello, world! ", @context.template
        end

        it "logs the placeholder removed" do
          Rails.logger.expects(:info).with('Public comment placeholder suppressed: ["{{ticket.comments}}"]')
          @context.render
        end

        it "increments a statsd_client" do
          statsd_client.expects(:increment).with("liquid_context.placeholder_suppressed")
          @context.render
        end

        describe "when the target placeholder is in a lower level of dynamic content" do
          let(:template) { "{{dc.lower}}" }

          before do
            @context = DummyContext.new(template, ENGLISH_BY_ZENDESK, suppress_placeholder: true)
            @context.dc_cache = {
              'foobar' => 'Hello, world!',
              'placeholder' => '{{ticket.comments}}',
              'lower' => '{{dc.foobar}} {{dc.placeholder}}'
            }
            @context.render
          end

          it "removes the placeholder from the second dynamic content level" do
            assert_equal "Hello, world! ", @context.template
          end

          it "logs the placeholder removed" do
            Rails.logger.expects(:info).with('Public comment placeholder suppressed: ["{{ticket.comments}}"]')
            @context.render
          end

          it "increments a statsd_client" do
            statsd_client.expects(:increment).with("liquid_context.placeholder_suppressed")
            @context.render
          end
        end
      end
    end

    describe "when suppress_placeholder option is false" do
      let(:template) { "{{ticket.comments}} thanks!" }
      before do
        @context = DummyContext.new(template, ENGLISH_BY_ZENDESK, suppress_placeholder: false)
        @context.render
      end

      it "does not remove the placeholder from the template" do
        assert_equal template, @context.template
      end
    end
  end

  describe 'detect public comment placeholders' do
    describe 'when ticket placeholder is present' do
      let(:template) { "{{ticket.comments}} thanks!" }

      before do
        @context = DummyContext.new(template, ENGLISH_BY_ZENDESK, detect_public_comment_placeholder: true)
        @context.render
      end

      it 'should set public_comment_placeholder to true' do
        assert @context.public_comment_placeholder
      end
    end

    describe 'when ticket placeholder is not present' do
      let(:template) { "thanks!" }

      before do
        @context = DummyContext.new(template, ENGLISH_BY_ZENDESK, detect_public_comment_placeholder: true)
        @context.render
      end

      it 'should set public_comment_placeholder to false' do
        refute @context.public_comment_placeholder
      end
    end

    describe 'when the target placeholder is in a lower level of dynamic content' do
      let(:template) { '{{dc.lower}}' }

      before do
        @context = DummyContext.new(template, ENGLISH_BY_ZENDESK, detect_public_comment_placeholder: true)
        @context.dc_cache = {
          'foobar' => 'Hello, world!',
          'placeholder' => '{{ticket.comments}}',
          'lower' => '{{dc.foobar}} {{dc.placeholder}}'
        }
        @context.render
      end

      it 'should set public_comment_placeholder to true' do
        assert @context.public_comment_placeholder
      end
    end

    describe 'when the target placeholder is not in a lower level of dynamic content' do
      let(:template) { '{{dc.lower}}' }

      before do
        @context = DummyContext.new(template, ENGLISH_BY_ZENDESK, detect_public_comment_placeholder: true)
        @context.dc_cache = {
          'foobar' => 'Hello, world!',
          'placeholder' => 'Placeholder',
          'lower' => '{{dc.foobar}} {{dc.placeholder}}'
        }
        @context.render
      end

      it 'should set public_comment_placeholder to false' do
        refute @context.public_comment_placeholder
      end
    end

    describe 'when detect_public_comment_placeholder option is false' do
      describe 'when ticket placeholder is present' do
        let(:template) { "{{ticket.comments}} thanks!" }

        before do
          @context = DummyContext.new(template, ENGLISH_BY_ZENDESK, detect_public_comment_placeholder: false)
          @context.render
        end

        it 'should set public_comment_placeholder to the default value of false' do
          refute @context.public_comment_placeholder
        end
      end
    end

    describe 'when detect_public_comment_placeholder option is nil' do
      describe 'when ticket placeholder is present' do
        let(:template) { "{{ticket.comments}} thanks!" }

        before do
          @context = DummyContext.new(template, ENGLISH_BY_ZENDESK)
          @context.render
        end

        it 'should set public_comment_placeholder to the default value of false' do
          refute @context.public_comment_placeholder
        end
      end
    end
  end

  describe "#rich_comment?" do
    it "defaults to false" do
      refute Zendesk::Liquid::Context.new("foo", ENGLISH_BY_ZENDESK).rich_comment?
    end

    it "respects the option passed in" do
      assert Zendesk::Liquid::Context.new("foo", ENGLISH_BY_ZENDESK, rich_comment: true).rich_comment?
    end
  end

  describe "#content_type?" do
    before do
      @context = DummyContext.new("{{template}}", stub)
      @context.format = Mime[:html]
    end

    it "returns true when the content type matches the input" do
      assert @context.content_type?(Mime[:html])
    end

    it "returns false when the content type does not match the input" do
      assert_equal false, @context.content_type?(Mime[:text])
    end
  end

  describe "#parse" do
    it "returns nil for invalid templates" do
      assert_nil DummyContext.new(stub, stub).send(:parse, "{{}")
    end
  end

  describe "#post_process" do
    it "call apply on each post processor" do
      Zendesk::Liquid::PostProcessing::SimpleFormatter.any_instance.expects(:apply)
      Zendesk::Liquid::PostProcessing::Autolinker.any_instance.expects(:apply)
      Zendesk::Liquid::PostProcessing::DocumentCleanup.any_instance.expects(:apply)
      DummyContext.new("foo {{a.b}}", translation_locales(:japanese)).render
    end
  end
end
