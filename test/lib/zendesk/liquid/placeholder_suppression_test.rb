require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'PlaceholderSuppression' do
  describe '#redact_placeholders' do
    Zendesk::Liquid::PlaceholderSuppression::PUBLIC_COMMENT_PLACEHOLDER_STRINGS.each do |placeholder|
      let(:body) { "Here is your comment {{ #{placeholder} }}." }

      it "returns the body without the placeholder #{placeholder}" do
        assert_equal 'Here is your comment .', Zendesk::Liquid::PlaceholderSuppression.redact_placeholders(body)
      end
    end
  end
end
