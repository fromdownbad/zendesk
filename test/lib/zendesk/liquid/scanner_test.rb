require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Zendesk::Liquid::Scanner do
  it "handles empty placeholders gracefully" do
    template = Liquid::Template.parse("{{}}")
    assert_equal [], Zendesk::Liquid::Scanner.scan(template, "foo")
  end

  describe "given a liquid case block" do
    let(:text) do
      "{% case ticket.quest_objective %}"\
      "{% when 'seek the wizard' %}"\
      "  Begin your search at {{dc.wizard_location}}."\
      "{% when dc.event_quest_name %}"\
      "  {{dc.event_quest_text}}"\
      "{% else %}"\
      "  That is a mystery to me, {{ticket.requester}}."\
      "{% endcase %}"
    end
    let(:template) { Liquid::Template.parse(text) }

    it "scans case and when statements" do
      assert_includes Zendesk::Liquid::Scanner.scan(template, "dc"), "event_quest_name"
      assert_includes Zendesk::Liquid::Scanner.scan(template, "ticket"), "quest_objective"
    end

    it "scans inside body blocks" do
      ["wizard_location", "event_quest_text"].each do |lookup|
        assert_includes Zendesk::Liquid::Scanner.scan(template, "dc"), lookup
      end
      assert_includes Zendesk::Liquid::Scanner.scan(template, "ticket"), "requester"
    end
  end
end
