require_relative "../../../support/test_helper"
require_relative "../../../support/satisfaction_test_helper"

SingleCov.covered! uncovered: 18

describe 'SatisfactionRatingDrop' do
  include SatisfactionTestHelper

  fixtures :accounts, :subscriptions, :tickets, :users, :organizations, :tokens, :translation_locales, :rules, :events

  describe 'a Satisfaction Rating Drop' do
    before do
      @ticket = tickets(:minimum_4)
      @ticket.will_be_saved_by(@ticket.requester)
      @drop = Zendesk::Liquid::SatisfactionRatingDrop.new(@ticket, 'text/html', @ticket.requester, ENGLISH_BY_ZENDESK)
    end

    subject { @drop }

    describe '#rated_main_content' do
      describe 'for a rated ticket' do
        before do
          @ticket.satisfaction_score = SatisfactionType.GOOD
          @ticket.save!
        end

        it 'translates the {{score}} place holder' do
          subject.expects(:html?).returns(true)
          assert_match /This ticket has been rated as Good, I'm satisfied/, subject.send(:rated_main_content)
        end
      end
    end

    describe "all _urls when invoked on a new ticket" do
      before do
        @ticket = @ticket.account.tickets.new
      end

      it "is blank" do
        @drop = Zendesk::Liquid::SatisfactionRatingDrop.new(@ticket, 'text/html', @ticket.requester, ENGLISH_BY_ZENDESK)
        assert_nil @drop.rating_url
        assert_nil @drop.positive_rating_url
        assert_nil @drop.negative_rating_url
      end
    end

    describe '#rating_section' do
      let(:notification) do
        rule = rules(:automation_request_csr)
        event = events(:untrusted_audit_for_minimum_ticket_1)
        event.notifications.create(
          via_id: ViaType.RULE,
          via_reference_id: rule.id, recipients: [User.first.id],
          account: accounts(:minimum), subject: "New Comment", body: "There was a new comment...{{satisfaction.rating_section}}"
        )
      end

      it "only create one token when sending a multipart email" do
        minimum_ticket_1 = tickets(:minimum_1)
        Ticket.any_instance.expects(:title_for_role).with(false, 250).at_least(1).returns("foo")

        2.times { EventsMailer.deliver_rule(notification, minimum_ticket_1, "foo@bar.com", false) }
        assert_equal 1, Tokens::SatisfactionRatingToken.count(:all)
      end

      describe "with stubbing" do
        before do
          @token = Tokens::SatisfactionRatingToken.create!(ticket: @ticket)
          Tokens::SatisfactionRatingToken.expects(:find_token).with(@ticket).returns(nil)
          Tokens::SatisfactionRatingToken.stubs(:create_token).returns(@token).once
          @ticket.stubs(:ever_solved?).returns(true)
        end

        it 'generates a single SatisfactionRatingToken' do
          subject.rating_section

          assert_received(Tokens::SatisfactionRatingToken, :create_token, &:once)
        end

        it 'includes URLs to rate the ticket' do
          base = "#{@ticket.account.url}/requests/#{@ticket.nice_id}/satisfaction/new/#{@token.value}?locale=1&intention="
          rating_section = subject.rating_section
          assert_includes rating_section, base + SatisfactionType.GOOD.to_s
          assert_includes rating_section, base + SatisfactionType.BAD.to_s
        end

        it "includes branded URLs using the ticket's brand" do
          brand = FactoryBot.create(:brand, account_id: @ticket.account_id)
          @ticket.stubs(brand: brand)

          base = "#{brand.url}/requests/#{@ticket.nice_id}/satisfaction/new/#{@token.value}?locale=1&intention="
          rating_section = subject.rating_section
          assert_includes rating_section, base + SatisfactionType.GOOD.to_s
          assert_includes rating_section, base + SatisfactionType.BAD.to_s
        end

        # Ratings w/ or w/o comments render the same generic string for ease of i18n
        describe 'when she has already rated it' do
          before do
            @ticket.satisfaction_score = SatisfactionType.GOOD
            @ticket.satisfaction_comment = nil
            @ticket.save!
          end

          it 'offers the option to change the rating or comment' do
            assert_match(%r{You may still change your rating or comment}i, subject.rating_section)
          end
        end

        describe 'when she has already rated it and added a comment' do
          before do
            @ticket.satisfaction_score = SatisfactionType.GOOD
            @ticket.satisfaction_comment = "woah"
            @ticket.save!
          end

          it 'offers the option to change the rating' do
            assert_match(%r{You may still change your rating or comment}i, subject.rating_section)
          end
        end
      end
    end

    describe "#rating_text" do
      before do
        @ticket.satisfaction_score = SatisfactionType.GOODWITHCOMMENT
      end

      it "returns the translation correctly " do
        assert_equal @drop.send(:translate_csr, 'txt.satisfaction.score.good'), @drop.send(:rating_text)
      end

      describe "when the passed in locale is Japanese" do
        before { @drop = Zendesk::Liquid::SatisfactionRatingDrop.new(@ticket, 'text/html', @ticket.requester, translation_locales(:japanese)) }

        it "returns the japanese translation correctly" do
          assert_equal "Japanese good score", @drop.send(:rating_text)
        end
      end

      describe "when there the passed in locale is nil and there is a current_user" do
        before do
          @ticket.assignee.stubs(translation_locale: translation_locales(:spanish))
          I18n.expects(:t).with('txt.satisfaction.score.good', locale: translation_locales(:spanish))
        end

        it "uses the current_user's locale" do
          @drop = Zendesk::Liquid::SatisfactionRatingDrop.new(@ticket, 'text/html', @ticket.assignee, nil)
          @drop.send(:rating_text)
        end
      end

      describe "when there the passed in locale is nil and there is no current_user" do
        before do
          @ticket.requester.stubs(translation_locale: translation_locales(:spanish))
          I18n.expects(:t).with('txt.satisfaction.score.good', locale: translation_locales(:spanish))
        end

        it "uses the requester's locale" do
          @drop = Zendesk::Liquid::SatisfactionRatingDrop.new(@ticket, 'text/html', nil, nil)
          @drop.send(:rating_text)
        end
      end
    end
  end
end
