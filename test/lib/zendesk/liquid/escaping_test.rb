require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'Escaping' do
  fixtures :tickets, :users

  def create_comment!(comment)
    @ticket.add_comment(body: comment, is_public: true)
    @ticket.will_be_saved_by(@user)
    @ticket.save!
    @ticket.reload
  end

  describe "with liquid in the body" do
    before do
      @ticket = tickets(:minimum_1)
      @user = users(:minimum_agent)

      @ticket.account.settings.markdown_ticket_comments = true
      @ticket.account.settings.rich_text_comments = true
      @ticket.account.save!
    end

    it "renders properly when saving a comment" do
      [
        # does not evaluate any liquid placeholders that are escaped
        ["Hello, a \\{{ticket.requester}} {{ticket.assignee}}", "Hello, a {{ticket.requester}} #{@user.name}"],
        ["Hello, b \\{% ticket.requester %} {{ticket.assignee}}", "Hello, b {% ticket.requester %} #{@user.name}"],
        ["Hello, \\{% ticket.requester %} {{ticket.assignee}} \\{{ticket.assignee}}", "Hello, {% ticket.requester %} #{@user.name} {{ticket.assignee}}"],

        # when using liquid within a markdown codeblock it
          # does not evaluate liquid placeholders
        ["Hello \n\n```\nMr. {{ticket.id}}\n```\nYou marvelous bastard!", "Hello \n\n```\nMr. {{ticket.id}}\n```\nYou marvelous bastard!"],
          # evaluates incomplete/invalid code blocks
        ["Hello \n\n````\nMr. {{ticket.id}}\n\nYou marvelous bastard!", "Hello \n\n````\nMr. #{@ticket.nice_id}\n\nYou marvelous bastard!"],
        ["Hello \n\n``````\nMr. {{ticket.id}}\n````\nYou marvelous bastard!", "Hello \n\n``````\nMr. {{ticket.id}}\n````\nYou marvelous bastard!"],
          # works inline too
        ["Hello ```Mr. {{ticket.id}}``` You marvelous bastard!", "Hello ```Mr. {{ticket.id}}``` You marvelous bastard!"],
        ["Hello `Mr. {{ticket.id}}` You marvelous bastard!", "Hello `Mr. {{ticket.id}}` You marvelous bastard!"],
          # leaves stuff alone" do
        ["Testing:\n\n```\n{% if ticket.cc_names != empty %}\nCC names: {{ticket.cc_names}}\n{% endif %}\n```\n\nstuff", "Testing:\n\n```\n{% if ticket.cc_names != empty %}\nCC names: {{ticket.cc_names}}\n{% endif %}\n```\n\nstuff"],
          # escapes in wysiwyg code segments
        ["<code>INLINE {{ticket.requester}}</code> normal text <pre>CODE BLOCK {{ticket.requester}}</pre>", "<code>INLINE {{ticket.requester}}</code> normal text <pre>CODE BLOCK {{ticket.requester}}</pre>"],
        # when there is no liquid it
          # should not change the comment
        ["```\nvar root = 'C:'\nvar dir1 = 'windows'\nvar dir2 = 'system32'\nvar path = root + '\\' + dir1 + '\\' + dir2;\n```", "```\nvar root = 'C:'\nvar dir1 = 'windows'\nvar dir2 = 'system32'\nvar path = root + '\\' + dir1 + '\\' + dir2;\n```"]
      ].each do |template, expected|
        create_comment!(template)
        assert_equal @ticket.reload.comments.last.body, expected
      end
    end

    describe Zendesk::Liquid::Escaping::Codeblock do
      let(:string_value) { "\n```\nI pitty the guy named {{ticket.requester.name}}.\n```\n" }
      subject { Zendesk::Liquid::Escaping::Codeblock.new string_value.scan(/(`+)(.*?)(`+)/im).first }
      it "sets the codeblock from the match" do
        assert_equal "\nI pitty the guy named {{ticket.requester.name}}.\n", subject.codeblock
      end

      it "sets the valid attribute" do
        assert subject.valid?
      end

      describe "when passed an invalid codeblock" do
        let(:string_value) { "\n```\nI pitty the guy named {{ticket.requester.name}}.\n" }
        it "returns false for the attribute 'valid'" do
          refute subject.valid?
        end

        it "does not blow up" do
          assert_equal "", subject.codeblock
          assert_equal "", subject.escaped_codeblock
        end
      end
    end
  end
end
