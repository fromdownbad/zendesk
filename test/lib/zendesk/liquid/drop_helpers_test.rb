require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Liquid::DropHelpers do
  fixtures :accounts, :brands, :tickets

  let(:ticket) { tickets(:minimum_5) }
  let(:account) { accounts(:minimum) }
  let(:brand) { account.brands.first }
  let(:author) { account.users.first }
  let(:comment) do
    Comment.new do |c|
      c.author     = author
      c.account    = account
      c.body       = "Hello World #1\n<3"
      c.created_at = Time.at(1304619457)
    end
  end

  # TicketDrop includes DropHelpers but doesn't implement template_id or tags.
  let(:ticket_drop) { Zendesk::Liquid::TicketDrop.new(ticket, true, "text/plain") }

  let(:drop) do
    # PlainCommentBodyDrop includes DropHelpers and implements tags and template_id.
    Zendesk::Liquid::Comments::PlainCommentBodyDrop.new(
      account, comment, Mime[:html].to_s, brand, false
    )
  end

  describe ".time_and_record" do
    let(:key) { :test_metric }
    let(:statsd_client) { stub_for_statsd }

    it "returns the result from the yield" do
      result = drop.time_and_record(key) { 2 * 5 }
      assert_equal 10, result
    end

    it "benchmarks a code block" do
      Zendesk::StatsD::Client.expects(:new).
        with(namespace: ['liquid', 'drop']).
        returns(statsd_client)

      statsd_client.expects(:time).
        with(key, tags: [], sample_rate: 0.1).
        yields(11)

      result = drop.time_and_record(key) { 10 + 1 }
      assert_equal 11, result
    end
  end

  describe "#render" do
    it "renders" do
      drop.render
    end
  end

  describe "#timed_render" do
    let(:tags) do
      [
        "drop:Zendesk::Liquid::Comments::PlainCommentBodyDrop",
        "template:na"
      ]
    end

    it "times and renders" do
      drop.expects(:time_and_record).with('render', tags).yields
      drop.expects(:render).returns("hello").once
      assert_equal "hello", drop.timed_render
    end
  end

  describe "#statsd_tags" do
    it "returns the drop and template tags" do
      assert_equal ["drop:Zendesk::Liquid::TicketDrop", "template:na"],
        ticket_drop.send(:statsd_tags)

      assert_equal ["drop:Zendesk::Liquid::Comments::PlainCommentBodyDrop", "template:na"],
        drop.send(:statsd_tags)
    end
  end

  describe "#template_id" do
    it "returns the template identifier" do
      assert_equal :na, ticket_drop.send(:template_id)
      assert_equal :na, drop.send(:template_id)
    end
  end
end
