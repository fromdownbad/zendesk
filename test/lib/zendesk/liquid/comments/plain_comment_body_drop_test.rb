require_relative "../../../../support/test_helper"
require "zendesk/liquid/comments/comment_drop"
require "zendesk/liquid/comments/plain_comment_body_drop"

SingleCov.covered!

describe Zendesk::Liquid::Comments::PlainCommentBodyDrop do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:author) { User.new(name: "Argle < Bargle", account: account) }
  let(:brand) { FactoryBot.create(:brand, subdomain: 'wombats', account_id: account.id) }
  let(:comment) do
    Comment.new do |c|
      c.author     = author
      c.account    = account
      c.body       = "Hello World #1\n<3"
      c.created_at = Time.at(1304619457)
    end
  end
  let(:rich_comment_only) { false }

  let(:drop) do
    Zendesk::Liquid::Comments::PlainCommentBodyDrop.new(
      account,
      comment,
      Mime[:html].to_s,
      brand,
      false,
      false,
      rich_comment_only
    )
  end

  before { Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false) }

  describe "#render" do
    describe "when format is Mime[:html]" do
      before { drop.stubs(:format).returns(Mime[:html]) }

      describe "and the comment is not to be rendered as Markdown" do
        before { comment.stubs(:process_as_markdown?).returns(false) }

        describe "and the account is on the modern template" do
          before { account.settings.stubs(:modern_email_template?).returns(true) }

          it "responds with modern formatted content" do
            assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\"><p style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\">Hello World #1<br>&lt;3</p></div>", drop.render.strip
          end

          describe_with_arturo_enabled :email_simplified_threading do
            it "responds with modern formatted content with markdown_v2 CSS" do
              assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\"><p style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\">Hello World #1<br>&lt;3</p></div>", drop.render.strip
            end
          end

          it "does not contain liquid errors" do
            drop.render.wont_include 'Liquid'
          end
        end

        describe "and the account is on the classic template" do
          before { account.settings.stubs(:modern_email_template?).returns(false) }

          it "responds with classic formatted content" do
            assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p>Hello World #1<br>&lt;3</p></div>", drop.render.strip
          end

          it "does not contain liquid errors" do
            drop.render.wont_include 'Liquid'
          end
        end
      end

      describe "and the comment is to be rendered as Markdown" do
        before do
          comment.stubs(:process_as_markdown?).returns(true)
          comment.body = "Hello #123 World <3"
        end

        describe_with_arturo_disabled :email_simplified_threading do
          it "renders using default markdown CSS instead of templates" do
            drop.expects(:template).never
            assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\"><p dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\">Hello #123 World &lt;3</p></div>", drop.render
          end
        end

        describe_with_arturo_enabled :email_simplified_threading do
          it "renders using markdown_v2 CSS instead of templates" do
            drop.expects(:template).never
            assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\"><p dir=\"auto\" style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\">Hello #123 World &lt;3</p></div>", drop.render
          end
        end
      end

      describe "and the comment is pre styled" do
        before do
          comment.stubs(pre_styled?: true)
          comment.body = "Hola\n\nHello\n\nHej"
        end

        describe "when account has inbound-HTML processing enabled" do
          before { account.settings.stubs(rich_content_in_emails?: true) }

          describe_with_arturo_disabled :email_simplified_threading do
            it "renders using the pre-styled CSS" do
              assert_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><p style=\"font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif\">Hola</p><p style=\"font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif\">Hello</p><p style=\"font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif\">Hej</p></div>", drop.render
            end
          end
        end

        describe "when account has inbound-HTML processing disabled" do
          before { account.settings.stubs(rich_content_in_emails?: false) }

          it "does not render the pre-styled CSS" do
            assert_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><p>Hola</p><p>Hello</p><p>Hej</p></div>", drop.render
          end
        end

        describe "when account has simplified email threading enabled" do
          describe_with_arturo_enabled :email_simplified_threading do
            it "does not render the pre-styled CSS" do
              assert_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><p>Hola</p><p>Hello</p><p>Hej</p></div>", drop.render
            end
          end
        end
      end

      describe "when the comment would normally be processed as markdown but comment_html_only is requested" do
        let(:rich_comment_only) { true }

        before do
          comment.html_body = "<p>Hola<br>Hello<br>Hej</p>"
          comment.stubs(rich?: true)
        end

        describe_with_arturo_enabled :email_simplified_threading do
          it "renders using markdown_v2 CSS" do
            assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\"><p style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\">Hola<br>Hello<br>Hej</p></div>", drop.render
          end
        end

        describe "when the comment was created via WEB_SERVICE" do
          before { comment.stubs(via_id: ViaType.WEB_SERVICE) }

          it "renders using pre-styled CSS" do
            assert_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><p>Hola<br>Hello<br>Hej</p></div>", drop.render
          end

          describe_with_arturo_disabled :email_pre_styled_rich_api_comments do
            it "renders using default markdown CSS" do
              assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\"><p style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\">Hola<br>Hello<br>Hej</p></div>", drop.render
            end
          end
        end

        describe "when the comment was not created via WEB_SERVICE" do
          before { comment.stubs(via_id: ViaType.LOTUS) }

          describe_with_and_without_arturo_enabled :email_pre_styled_rich_api_comments do
            it "renders using default markdown CSS" do
              assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\"><p style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\">Hola<br>Hello<br>Hej</p></div>", drop.render
            end
          end
        end
      end

      it "does not emojify if not enabled in account" do
        drop.account.stubs(:has_emoji_autocompletion_enabled?).returns(false)
        drop.expects(:emojify!).never
        drop.render
      end

      it "emojifys if enabled in account" do
        drop.account.stubs(:has_emoji_autocompletion_enabled?).returns(true)
        comment.stubs(:process_as_markdown?).returns(true)
        comment.expects(:emojify!).once
        drop.render
      end
    end

    describe "when format is Mime[:text]" do
      before { drop.stubs(:format).returns(Mime[:text]) }

      it "responds with text formatted content" do
        assert_equal comment.body, drop.render
      end

      it "does not contain liquid errors" do
        drop.render.wont_include 'Liquid'
      end
    end
  end

  describe "#host" do
    describe "without host mapping" do
      it "returns a branded host for an end user" do
        drop.expects(:for_agent).returns(false)
        assert_equal "https://wombats.zendesk-test.com", drop.host
      end

      it "returns a non-branded host for an agent" do
        drop.expects(:for_agent).returns(true)
        assert_equal "https://minimum.zendesk-test.com", drop.host
      end
    end

    describe "with host mapping" do
      before { brand.host_mapping = "wombats.example.com" }

      it "returns a host-mapped host for an end user" do
        drop.expects(:for_agent).returns(false)
        assert_equal "http://wombats.example.com", drop.host
      end
      it "returns a non-branded and non-host-mapped host for an agent" do
        drop.expects(:for_agent).returns(true)
        assert_equal "https://minimum.zendesk-test.com", drop.host
      end
    end
  end
end
