require_relative "../../../../support/test_helper"
require "zendesk/liquid/comments/comment_drop"
require "zendesk/liquid/comments/voice_comment_body_drop"

SingleCov.covered!

describe Zendesk::Liquid::Comments::VoiceCommentBodyDrop do
  fixtures :tickets, :accounts, :users

  before do
    author = users(:minimum_end_user)
    data = {
      recording_url: "http://example.com/2010-04-01/Accounts/AC987414b864/Recordings/mjallo",
      transcription_text: "foo: hi\nbar: bye\n",
      transcription_status: "completed",
      answered_by_id: author.id,
      call_id: 789,
      from: '+14155556666',
      to: '+14155556666',
      started_at: '2013-06-24 15:31:44 +0000',
      call_duration: 50
    }

    @ticket = tickets(:minimum_1)
    @comment = @ticket.add_voice_api_comment(data)
    @ticket.will_be_saved_by(users(:minimum_agent))
    @ticket.save!

    @drop = Zendesk::Liquid::Comments::VoiceCommentBodyDrop.new(@account, @comment, Mime::HTML.to_s)
  end

  describe "#render" do
    describe "when format is Mime::HTML" do
      before { @drop.stubs(:format).returns(Mime::HTML) }

      it "responds with HTML formatted content" do
        assert @drop.render.index("<label>Call to:</label> +1 (415) 555-6666<br/>")
      end

      it "does not contain liquid errors" do
        assert @drop.render !~ /Liquid/
      end
    end

    describe "when format is Mime::TEXT" do
      before { @drop.stubs(:format).returns(Mime::TEXT) }

      it "responds with text formatted content" do
        assert_equal @drop.render, @comment.body
      end

      it "does not contain liquid errors" do
        assert @drop.render !~ /Liquid/
      end
    end
  end
end
