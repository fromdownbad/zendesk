require_relative "../../../../support/test_helper"
require_relative "../../../../support/attachment_test_helper"
require "zendesk/liquid/comments/comment_drop"

SingleCov.covered! uncovered: 2

describe Zendesk::Liquid::Comments::CommentDrop do
  let(:time_zone) { 'America/Halifax' }
  let(:author) { User.new(name: "Argle < Bargle", account: account) }
  let(:brand) { FactoryBot.create(:brand, subdomain: 'wombat', name: "Brando") }
  let(:path_prefix) { "#{Rails.root}/test/files" }
  let(:account) do
    account = Account.new(name: "Support")
    account.subdomain = 'test'
    account.route = brand.route
    account
  end

  let(:ticket) do
    ticket = Ticket.new(brand: brand, account: account)
    ticket.audit = Audit.new(account: account)
    ticket
  end

  let(:comment) do
    Comment.new do |c|
      c.author     = author
      c.account    = account
      c.created_at = Time.at(1304619457)
      c.ticket     = ticket
    end
  end

  let(:drop_options) { {} }

  let(:drop) do
    Zendesk::Liquid::Comments::CommentDrop.new(
      account, comment, Mime[:html].to_s, true, author, brand, drop_options
    )
  end

  before { account.stubs(uses_12_hour_clock?: true) }

  describe "#template" do
    it "returns an ERB instance" do
      assert drop.send(:template).is_a?(ERB)
    end
  end

  describe "#render" do
    describe "when there's a comment to render" do
      it "renders from the ERB template" do
        drop.send(:template).expects(:result).returns("result")
        assert_equal "result", drop.render
      end
    end

    describe "when the comment isn't trusted" do
      before do
        @key_prefix = "ticket.comment.flagged_reason_"
        @default_reasons = ["default"]
        @default_learn_more = I18n.t("ticket.comment.flagged_link_#{@default_reasons[0]}")
        @default_warning_message = I18n.t(@key_prefix + @default_reasons[0], learn_more: nil)
      end

      describe "when the flag reason is the default" do
        before do
          comment.stubs(:flagged_reasons).returns(@default_reasons)
          comment.ticket.audit.untrusted!
        end

        it "includes a warning and no link for text" do
          drop.stubs(:format).returns(Mime[:text].to_s)
          result = drop.render

          result.must_include @default_warning_message
          refute_match %r{#{@default_learn_more}}, result
        end

        it "includes a warning and link for html v1" do
          drop.stubs(:format).returns(Mime[:html].to_s)
          result = drop.render

          result.must_include @default_warning_message
          result.must_include @default_learn_more
        end

        it "includes a warning and link for html v2" do
          drop.stubs(:format).returns(Mime[:html].to_s)
          result = drop.render

          result.must_include @default_warning_message
          result.must_include @default_learn_more
        end
      end

      describe "when the flag reason is not the default" do
        before do
          @reasons = ["other_user_update"]
          comment.stubs(:flagged_reasons).returns(@reasons)
          comment.ticket.audit.untrusted!
        end

        it "includes a different warning and link for html" do
          drop.stubs(:format).returns(Mime[:html].to_s)
          result = drop.render

          learn_more = I18n.t("ticket.comment.flagged_link_#{@reasons[0]}")
          warning_message = I18n.t(@key_prefix + @reasons[0], learn_more: nil, comment_author: nil)

          result.must_include warning_message
          result.must_include learn_more

          refute_match /#{@default_warning_message}/, result
          refute_match %r{#{@default_learn_more}}, result

          refute_equal warning_message, @default_warning_message
          refute_equal learn_more, @default_learn_more
        end
      end

      describe "when there is more than one flag" do
        before do
          @reasons = ["other_user_update", "machine_generated"]
          comment.stubs(:flagged_reasons).returns(@reasons)
          comment.ticket.audit.untrusted!
        end

        it "includes a warning and link in HTML for each flag" do
          drop.stubs(:format).returns(Mime[:html].to_s)
          result = drop.render

          @reasons.each do |reason|
            learn_more = I18n.t("ticket.comment.flagged_link_#{reason}")
            warning_message = I18n.t(@key_prefix + reason, learn_more: nil, comment_author: nil)

            result.must_include warning_message
            result.must_include learn_more
          end
        end

        it "includes a warning and no link in text for each flag" do
          drop.stubs(:format).returns(Mime[:text].to_s)
          result = drop.render

          @reasons.each do |reason|
            learn_more = I18n.t("ticket.comment.flagged_link_#{reason}")
            warning_message = I18n.t(@key_prefix + reason, learn_more: nil, comment_author: nil)

            result.must_include warning_message
            refute_match %r{#{learn_more}}, result
          end
        end
      end
    end

    describe "when the comment is trusted" do
      before do
        assert comment.trusted?
      end

      it "does not include a warning" do
        drop.stubs(:format).returns(Mime[:text].to_s)
        result = drop.render

        assert_no_match(/#{I18n.t('ticket.comment.flagged_learn_more')}/, result)
      end
    end

    describe "output" do
      before do
        attachments = [
          stub(display_filename: "Attachment A", url: "http://foo.com/a.jpg"),
          stub(display_filename: "Attachment B", url: "http://foo.com/b.jpg")
        ]

        screencasts = [
          stub(display_name: "Screencast1", url: "http://foo.com/abd123"),
          stub(display_name: "Screencast2", url: "http://foo.com/abd456")
        ]

        drop.stubs(:header).returns("Header")
        drop.stubs(:rendered_body).returns("Body & Head > Bottle\n\nA second paragraph\nwith more lines")
        drop.stubs(:attachments).returns(attachments)
        drop.stubs(:screencasts).returns(screencasts)
      end

      describe "for text" do
        before do
          drop.stubs(:format).returns(Mime[:text].to_s)
          @result = drop.render
        end

        it "contains the expected data" do
          @result.must_include "Attachment A"
          @result.must_include "Attachment B"
          @result.must_include "Header"
          assert_match /Body & Head > Bottle\n\nA second/, @result
          assert_match /with more lines\n\nAttachment/, @result
          @result.must_include "Screencast1"
          @result.must_include "Screencast2"
          @result.must_include "http://foo.com/abd123"
          @result.must_include "http://foo.com/abd456"
        end
      end

      describe "for HTML" do
        before do
          drop.stubs(:format).returns(Mime[:html].to_s)
          @result = drop.render
        end

        it "contains the expected data" do
          @result.must_include "Screencast1"
          @result.must_include "Screencast2"
          @result.must_include "<a href='http://foo.com/abd123'>Screencast1</a>"
          @result.must_include "<a href='http://foo.com/abd456'>Screencast2</a>"
        end

        describe "with minimal rich comment text-only template" do
          let(:drop_options) { { comment_html_only: true } }

          it "contains only the expected data" do
            @result.wont_include "Attachment A"
            @result.wont_include "Attachment B"
            @result.wont_include "Screencast1"
            @result.wont_include "Screencast2"
            @result.wont_include "<a href='http://foo.com/abd123'>Screencast1</a>"
            @result.wont_include "<a href='http://foo.com/abd456'>Screencast2</a>"

            @result.must_include "Body & Head"
          end
        end
      end
    end
  end

  describe "#date" do
    it "handles a bad time" do
      comment.expects(:created_at).returns(nil)
      assert_equal "", drop.date
    end

    describe "when time is valid" do
      before do
        drop.instance_variable_set(:@time_zone, time_zone)
      end

      it "formats comment created_at with appropriate time settings" do
        assert_equal "May 5, 2011, 3:17 PM", drop.date
      end

      describe "when include_tz is true" do
        it "formats the comment created_at with a timezone" do
          drop.instance_variable_set(:@include_tz, true)
          assert_equal "May 5, 2011, 3:17 PM ADT", drop.date
        end
      end

      describe "when include_tz is false" do
        it "formats the comment created_at without a timezone" do
          drop.instance_variable_set(:@include_tz, false)
          assert_equal "May 5, 2011, 3:17 PM", drop.date
        end
      end

      describe "when account uses 24-hour clock" do
        before { account.stubs(uses_12_hour_clock?: false) }

        it "formats the timestamp correctly" do
          assert_equal "May 5, 2011, 15:17", drop.date
        end
      end
    end
  end

  describe "#brand" do
    it "has the brand of the ticket the comment belongs to" do
      assert_equal brand, drop.brand
    end
  end

  describe "#host" do
    it "returns the url of the account" do
      assert_equal account.url, drop.host
    end
  end

  describe "time settings" do
    describe "for no current_user" do
      let (:author) { nil }
      it "defaults to account settings" do
        account.expects(:time_zone).returns("Bogota")
        assert_equal "May 5, 2011, 1:17 PM", drop.date
      end
    end

    describe "for current_user that is a system_user" do
      let (:author) { User.find(-1) }
      it "defaults to account settings" do
        account.expects(:time_zone).returns("Bogota")
        assert_equal "May 5, 2011, 1:17 PM", drop.date
      end
    end

    describe "for current_user with time_zone" do
      let (:author) { User.new(name: "with timzone", account: account, time_zone: "Arizona") }
      before do
        account.expects(:time_zone).never
      end

      it "uses user's settings" do
        assert_equal "May 5, 2011, 11:17 AM", drop.date
      end

      describe "when timezone indicator is requested" do
        let (:drop_options) { { include_tz: true } }

        it "includes the timezone" do
          assert_equal "May 5, 2011, 11:17 AM MST", drop.date
        end
      end
    end
  end

  describe "#attachments_header" do
    it "renders the attachments header in the account locale" do
      I18n.expects(:t).with('txt.uploads.header').returns("attachments")
      assert_equal "attachments", drop.attachments_header
    end
  end

  describe "#attachments" do
    it "defers to the ticket's brand the comment belongs to when rendering attachment urls" do
      wombat_attachment = create_attachment("#{Rails.root}/test/files/normal_1.jpg")
      attachments = [wombat_attachment]
      comment.expects(:attachments).returns(attachments)
      assert_equal "https://wombat.zendesk-test.com/attachments/token/#{wombat_attachment.token}/?name=normal_1.jpg", drop.attachments.first.url(brand: brand)
    end

    it "hides inline attachments" do
      wombat_attachment = create_attachment("#{Rails.root}/test/files/normal_1.jpg")
      wombat_attachment.inline = true
      attachments = [wombat_attachment]
      comment.expects(:attachments).returns(attachments)
      assert_equal 0, drop.attachments.length
    end
  end

  describe "#screencasts" do
    it "defers to Comment#screencasts" do
      comment.expects(:screencasts).returns("screencasts")
      assert_equal "screencasts", drop.screencasts
    end
  end

  describe "#header" do
    before { drop.instance_variable_set(:@time_zone, time_zone) }

    describe "when the comment is public" do
      before { comment.is_public = true }
      it "generates the unescaped translated public comment header" do
        assert_equal "Argle < Bargle, May 5, 2011, 3:17 PM", drop.header
      end
    end

    describe "when the comment is private" do
      before { comment.is_public = false }
      it "generates the unescaped translated private comment header" do
        assert_equal "Argle < Bargle, May 5, 2011, 3:17 PM (private)", drop.header
      end
    end

    describe "when the comment is rich, but currently rendering plain text part" do
      let(:drop_options) { { skip_header: true } }
      before { drop.stubs(:format).returns(Mime[:text].to_s) }

      it "does not return a header" do
        assert_equal "", drop.header
      end
    end
  end

  let(:facebook_comment) { FacebookComment.new(author: author, value_reference: {}) }
  let(:facebook_comment_with_user) { FacebookComment.new(value_reference: { requester: { name: "abc", id: "123" }}) }

  describe "#author_name" do
    it "exposes the real name of the author when for_agent" do
      author.expects(:safe_name).with(true).returns("real name")
      drop.expects(:for_agent).returns(true)
      assert_equal "real name", drop.author_name
    end

    it "exposes the safe name of the author when not for_agent" do
      author.expects(:safe_name).with(false).returns("safe name")
      drop.expects(:for_agent).returns(false)
      assert_equal "safe name", drop.author_name
    end

    it "does not escape HTML" do
      assert_equal "Argle < Bargle", drop.author_name
    end

    describe "for facebook comments that don't contain external users" do
      let (:comment) { facebook_comment }
      it "uses the author's name" do
        assert_equal "Argle < Bargle", drop.author_name
      end
    end

    describe "for facebook comments containing external users" do
      let (:comment) { facebook_comment_with_user }
      it "uses the requester's name from the FB comment data" do
        assert_equal "abc", drop.author_name
      end
    end
  end

  describe "#author_photo" do
    it "returns the photo path for the author" do
      assert_equal author.large_photo_url, drop.author_photo
    end

    describe "for facebook comments that don't contain external users" do
      let (:comment) { facebook_comment }
      it "uses the author's photo" do
        assert_equal author.large_photo_url, drop.author_photo
      end
    end

    describe "for facebook comments containing external users" do
      let (:comment) { facebook_comment_with_user }
      it "uses the requester's id from the FB comment data" do
        assert_equal "http://graph.facebook.com/v2.9/123/picture?type=large", drop.author_photo
      end
    end
  end

  describe "#show_thumbnail?" do
    before do
      drop
      @settings = stub
      @settings.stubs(:modern_email_template?).returns(true)
      account.stubs(:settings).returns(@settings)
    end

    describe "when modern template and photos are enabled" do
      before { @settings.stubs(:email_template_photos?).returns(true) }
      it "returns true" do
        assert drop.show_thumbnail?
      end
    end

    describe "when email_template_photos is not enabled" do
      before { @settings.stubs(:email_template_photos?).returns(false) }
      it "returns false" do
        refute drop.show_thumbnail?
      end
    end
  end

  describe "#last_attachment?" do
    let(:attachments) { [MiniTest::Mock.new, MiniTest::Mock.new] }

    describe "when attachment is the last attachment" do
      it "returns true" do
        assert drop.last_attachment?(1, attachments)
      end
    end

    describe "when attachment is not the last attachment" do
      it "returns false" do
        refute drop.last_attachment?(0, attachments)
      end
    end
  end

  describe "#file_icon" do
    let(:attachment) { create_attachment("#{path_prefix}/normal_1.jpg") }

    describe "when the attachment file type is document" do
      before { Attachment.any_instance.stubs(:file_type).returns("document") }

      it "returns the correct icon markup" do
        assert drop.file_icon(attachment).include?("file-document-stroke.png")
      end
    end

    describe "when the attachment file type is image" do
      before { Attachment.any_instance.stubs(:file_type).returns("image") }

      it "returns the correct icon markup" do
        assert drop.file_icon(attachment).include?("file-image-stroke.png")
      end
    end

    describe "when the attachment file type is pdf" do
      before { Attachment.any_instance.stubs(:file_type).returns("pdf") }

      it "returns the correct icon markup" do
        assert drop.file_icon(attachment).include?("file-pdf-stroke.png")
      end
    end

    describe "when the attachment file type is presentation" do
      before { Attachment.any_instance.stubs(:file_type).returns("presentation") }

      it "returns the correct icon markup" do
        assert drop.file_icon(attachment).include?("file-presentation-stroke.png")
      end
    end

    describe "when the attachment file type is spreadsheet" do
      before { Attachment.any_instance.stubs(:file_type).returns("spreadsheet") }

      it "returns the correct icon markup" do
        assert drop.file_icon(attachment).include?("file-spreadsheet-stroke.png")
      end
    end

    describe "when the attachment file type is zip" do
      before { Attachment.any_instance.stubs(:file_type).returns("zip") }

      it "returns the correct icon markup" do
        assert drop.file_icon(attachment).include?("file-zip-stroke.png")
      end
    end

    describe "when the attachment file type is unknown" do
      before { Attachment.any_instance.stubs(:file_type).returns("unknown") }

      it "returns the correct icon markup" do
        assert drop.file_icon(attachment).include?("file-generic-stroke.png")
      end
    end
  end

  describe "#agent_label" do
    it "shows brand name" do
      assert_equal "(Brando)", drop.agent_label
    end
  end

  describe '#rendered_body' do
    let(:format) { Mime[:html].to_s }
    let(:for_agent) { true }
    let(:final_drop) { mock }
    let(:rtl) { false }
    let(:comment_html_only) { false }

    it "records the time it takes to render a plain comment drop" do
      Zendesk::Liquid::Comments::PlainCommentBodyDrop.expects(:new).
        with(account, comment, format, brand, for_agent, rtl, comment_html_only).
        returns(final_drop).
        once

      final_drop.expects(:timed_render).once
      drop.rendered_body
    end

    describe 'when the comment is a VoiceComment' do
      let(:comment) do
        VoiceComment.new(
          author:     author,
          account:    account,
          created_at: Time.at(1304619457),
          ticket:     ticket
        )
      end

      it "records the time it takes to render a voice comment drop" do
        Zendesk::Liquid::Comments::VoiceCommentBodyDrop.expects(:new).
          with(account, comment, format).
          returns(final_drop).
          once

        final_drop.expects(:timed_render).once
        drop.rendered_body
      end
    end
  end
end
