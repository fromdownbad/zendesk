require_relative "../../../../support/test_helper"
require "zendesk/liquid/comments/plain_comment_drop"

SingleCov.covered!

describe Zendesk::Liquid::Comments::PlainCommentDrop do
  before do
    @account = accounts(:minimum)
    @account.time_zone = nil
    @author = User.new(name: "Argle < Bargle", account: @account)
    @brand = @account.default_brand

    ticket = Ticket.new(brand: @brand)
    ticket.audit = Audit.new

    @comment = Comment.new do |c|
      c.author     = @author
      c.account    = @account
      c.created_at = Time.at(1304619457)
      c.ticket     = ticket
    end

    @drop = Zendesk::Liquid::Comments::PlainCommentDrop.new(@account, @comment, Mime::HTML.to_s, true, @author, @brand)
  end

  describe "#template" do
    describe "when the format is HTML" do
      before { @drop.stubs(:format).returns(Mime::HTML.to_s) }

      it "returns the HTML ERB instance" do
        assert @drop.send(:template).is_a?(ERB)
        assert_equal @drop.send(:template), Zendesk::Liquid::Comments::PlainCommentDrop::BODY_TEMPLATES[:html]
      end
    end

    describe "when the format is text/plain" do
      before { @drop.stubs(:format).returns(Mime::TEXT.to_s) }

      it "returns the text ERB instance" do
        assert @drop.send(:template).is_a?(ERB)
        assert_equal @drop.send(:template), Zendesk::Liquid::Comments::PlainCommentDrop::BODY_TEMPLATES[:text]
      end
    end
  end

  describe "#render" do
    describe "when there's a comment to render" do
      it "renders from the ERB template" do
        @drop.send(:template).expects(:result).returns("result")
        assert_equal "result\n\n", @drop.render
      end
    end

    describe "output" do
      before do
        attachments = [
          stub(display_filename: "Attachment A", url: "http://foo.com/a.jpg"),
          stub(display_filename: "Attachment B", url: "http://foo.com/b.jpg")
        ]

        screencasts = [
          stub(display_name: "Screencast1", url: "http://foo.com/abd123"),
          stub(display_name: "Screencast2", url: "http://foo.com/abd456")
        ]

        @drop.stubs(:header).returns("Header")
        @drop.stubs(:rendered_body).returns("Body & Head > Bottle\n\nA second paragraph\nwith more lines")
        @drop.stubs(:attachments).returns(attachments)
        @drop.stubs(:screencasts).returns(screencasts)
      end

      describe "for text" do
        before do
          @drop.stubs(:format).returns(Mime::TEXT.to_s)
          @result = @drop.render
        end

        it "contains the expected data" do
          @result.must_include "Attachment A"
          @result.must_include "Attachment B"
          @result.must_include "Header"
          assert_match /Body & Head > Bottle\n\nA second/, @result
          assert_match /with more lines\n\nAttachment/, @result
          @result.must_include "Screencast1"
          @result.must_include "Screencast2"
          @result.must_include "http://foo.com/abd123"
          @result.must_include "http://foo.com/abd456"
        end
      end

      describe "for HTML" do
        before do
          @drop.stubs(:format).returns(Mime::HTML.to_s)
          @result = @drop.render
        end

        it "contains the expected data" do
          @result.must_include "Screencast1"
          @result.must_include "Screencast2"
          @result.must_include "<a href='http://foo.com/abd123'>Screencast1</a>"
          @result.must_include "<a href='http://foo.com/abd456'>Screencast2</a>"
        end
      end
    end
  end

  describe "header" do
    let(:time_zone) { 'America/Halifax' }

    before { @drop.instance_variable_set(:@time_zone, time_zone) }

    describe "when the comment is public" do
      before { @comment.is_public = true }
      it "generates the unescaped translated public comment header" do
        assert_equal "Argle < Bargle, May 5, 2011, 15:17", @drop.header
      end
    end

    describe "when the comment is private" do
      before { @comment.is_public = false }
      it "generates the unescaped translated private comment header" do
        assert_equal "Argle < Bargle, May 5, 2011, 15:17 (private)", @drop.header
      end
    end
  end
end
