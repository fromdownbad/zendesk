require_relative "../../../support/test_helper"
require_relative "../../../support/collaboration_settings_test_helper"

SingleCov.covered! uncovered: 13

describe Zendesk::Liquid::TicketDrop do
  include CustomFieldsTestHelper

  fixtures :tickets, :events, :groups, :users, :brands, :accounts,
    :organizations, :ticket_forms, :ticket_fields, :ticket_field_entries,
    :translation_locales, :custom_field_options,
    :cf_fields, :cf_values, :cf_dropdown_choices

  before do
    @account = accounts(:minimum)
    @ticket  = tickets(:minimum_5)
    @locale  = translation_locales(:english_by_zendesk)
    @drop    = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
    @account.uses_12_hour_clock = true
    @account.save!
  end

  describe "rendering" do
    describe "render placeholders in the language requested" do
      before do
        @spanish = translation_locales(:spanish)
        @drop    = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
      end

      it "renders {{ticket.status}}" do
        I18n.with_locale(@spanish) do
          text = Liquid::Template.parse("{{ticket.status}}").render("ticket" => @drop)
          assert_equal "Cerrado", text
        end
      end

      it "renders {{ticket.priority}}" do
        I18n.with_locale(@spanish) do
          text = Liquid::Template.parse("{{ticket.priority}}").render("ticket" => @drop)
          assert_equal "Normal", text
        end
      end

      it "renders {{ticket.via}}" do
        I18n.with_locale(@spanish) do
          text = Liquid::Template.parse("{{ticket.via}}").render("ticket" => @drop)
          assert_equal "Formato web", text
        end
      end

      it "renders {{ticket.ticket_type}}" do
        I18n.with_locale(@spanish) do
          text = Liquid::Template.parse("{{ticket.ticket_type}}").render("ticket" => @drop)
          assert_equal "Incidente", text
        end
      end

      describe "render ticket_field_option_title with DC in the language requested" do
        before do
          @ticket_drop_down = ticket_fields(:field_tagger_custom)
          fallback_locale = translation_locales(:english_by_zendesk)
          @fbattrs = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: fallback_locale.id }
          @cms_text = Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: @fbattrs, account: @ticket.account)
          @cms_spanish_variant = @cms_text.variants.create!(active: true, translation_locale_id: @spanish.id, value: "Esto es contenido dinamico")
          @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain", current_user: @ticket.requester, locale: @spanish)
        end

        it "renders in Spanish language" do
          text = Liquid::Template.parse("{{ticket.ticket_field_option_title_#{@ticket_drop_down.id}}}").render("ticket" => @drop)
          assert_equal "Esto es contenido dinamico", text
        end

        it "renders in ticket requester fallback" do
          @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain", current_user: @ticket.requester, locale: @ticket.requester.translation_locale)
          text = Liquid::Template.parse("{{ticket.ticket_field_option_title_#{@ticket_drop_down.id}}}").render("ticket" => @drop)
          assert_equal "The fallback value", text
        end
      end
    end

    it "renders {{ticket.id}} as the nice_id" do
      text = Liquid::Template.parse("{{ticket.id}}").render("ticket" => @drop)
      assert_equal @ticket.nice_id.to_s, text
    end

    it "does not render {{ticket.current_user}}" do
      text = Liquid::Template.parse("{{ticket.current_user}}").render("ticket" => @drop)
      assert_equal "", text
    end

    it "renders {{ticket.due_date}} as a date stamp" do
      @ticket.due_date = Time.at(1306670611)
      text = Liquid::Template.parse("{{ticket.due_date}}").render("ticket" => @drop)
      assert_equal "May 29, 2011", text
    end

    it "renders a missing due date as blank" do
      text = Liquid::Template.parse("{{ticket.due_date}}").render("ticket" => @drop)
      assert_equal "", text
    end

    it "renders {{ticket.title}} as the title for the role" do
      text = Liquid::Template.parse("{{ticket.title}}").render("ticket" => @drop)
      assert_equal @ticket.title_for_role(true, 150), text
    end

    it "renders {{ticket.ticket_form}} as ''" do
      text = Liquid::Template.parse("{{ticket.ticket_form}}").render("ticket" => @drop)
      assert_equal '', text
    end

    describe 'with ticket forms' do
      it "renders {{ticket.ticket_form}} as the ticket_form.display_name" do
        @ticket_form = ticket_forms(:default_ticket_form)
        @ticket.ticket_form = @ticket_form
        text = Liquid::Template.parse("{{ticket.ticket_form}}").render("ticket" => @drop)
        assert_equal @ticket.ticket_form.display_name, text
      end

      it "renders {{ticket.ticket_form}} as the ticket_form" do
        @ticket_form = ticket_forms(:default_ticket_form)
        @ticket_form.stubs(:display_name).returns(nil)
        @ticket.ticket_form = @ticket_form
        text = Liquid::Template.parse("{{ticket.ticket_form}}").render("ticket" => @drop)
        assert_equal @ticket.ticket_form.name, text
      end
    end

    it "renders {{ticket.brand}} as the brand name" do
      text = Liquid::Template.parse("{{ticket.brand}}").render("ticket" => @drop)
      assert_equal @ticket.brand.name, text
    end

    it "renders {{ticket.brand.name}} as the brand name" do
      text = Liquid::Template.parse("{{ticket.brand.name}}").render("ticket" => @drop)
      assert_equal @ticket.brand.name, text
    end

    it "renders {{ticket.description}} as the value of the first comment" do
      text = Liquid::Template.parse("{{ticket.description}}").render("ticket" => @drop)
      assert_match @ticket.comments.first.body, text
    end

    it "renders {{ticket.tags}} as the ticket tag list" do
      text = Liquid::Template.parse("{{ticket.tags}}").render("ticket" => @drop)
      assert_equal "five", @ticket.current_tags
      assert_equal @ticket.current_tags, text
    end

    it "renders {{ticket.requester}} as the requester name" do
      text = Liquid::Template.parse("{{ticket.requester}}").render("ticket" => @drop)
      assert_equal @ticket.requester.name, text
    end

    it "renders {{ticket.requester.name}} as the requester name" do
      text = Liquid::Template.parse("{{ticket.requester.name}}").render("ticket" => @drop)
      assert_equal @ticket.requester.name, text
    end

    it "renders {{ticket.organization}} as the organization name" do
      text = Liquid::Template.parse("{{ticket.organization}}").render("ticket" => @drop)
      assert_equal @ticket.organization.name, text
    end

    it "renders {{ticket.organization.name}} as the organization name" do
      text = Liquid::Template.parse("{{ticket.organization.name}}").render("ticket" => @drop)
      assert_equal @ticket.organization.name, text
    end

    describe_with_arturo_enabled :no_org_tags_for_ticket_placeholder do
      it "renders {{ticket.organization}} as the organization name" do
        text = Liquid::Template.parse("{{ticket.organization}}").render("ticket" => @drop)
        assert_equal @ticket.organization.name, text
      end

      it "renders {{ticket.organization.name}} as the organization name" do
        text = Liquid::Template.parse("{{ticket.organization.name}}").render("ticket" => @drop)
        assert_equal @ticket.organization.name, text
      end

      it "renders {{ticket.organization.tags}} as the empty string" do
        Organization.any_instance.stubs(current_tags: "tags")
        text = Liquid::Template.parse("{{ticket.organization.tags}}").render("ticket" => @drop)
        assert_equal "", text
      end
    end

    it "renders {{ticket.organization.xyz}} as the blank" do
      text = Liquid::Template.parse("{{ticket.organization.xyz}}").render("ticket" => @drop)
      assert_equal "", text
    end

    it "renders {{ticket.organization.tags}} as the organization_tags" do
      Organization.any_instance.stubs(current_tags: "tags")
      text = Liquid::Template.parse("{{ticket.organization.tags}}").render("ticket" => @drop)
      assert_equal "tags", text
    end

    it "renders {{ticket.assigne.extended_role}} as role/permission of the assignee" do
      text = Liquid::Template.parse("{{ticket.assignee.extended_role}}").render("ticket" => @drop)
      assert_equal 'Agent', text
    end

    describe "custom ticket fields" do
      before do
        @ticket = tickets(:minimum_2)
        @drop   = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
        @field  = ticket_fields(:field_text_custom)
      end

      it "supports rendering the field value" do
        text = Liquid::Template.parse("{{ticket.ticket_field_#{@field.id}}}").render("ticket" => @drop)
        text.must_include "here is some text"
        # accepts plural now too
        text = Liquid::Template.parse("{{ticket.ticket_fields_#{@field.id}}}").render("ticket" => @drop)
        text.must_include "here is some text"
      end
    end

    describe "custom user/org fields" do
      before do
        @ticket = tickets(:minimum_2)
        create_user_custom_field!('dropdown2', 'something else', 'Dropdown')
        create_organization_custom_field!('org_dropdown1', 'something else', 'Dropdown', dropdown_choices: [{ name: 'Something', value: '100' }])
        create_organization_custom_field!('org_dropdown2', 'something else', 'Dropdown')
        dropdown = @account.custom_fields.find_by_key('org_dropdown1')
        choice   = dropdown.dropdown_choices.find_by_name('Something')
        @ticket.organization.custom_field_values.create!(field: dropdown, value: choice.id)

        @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
      end

      # User

      it "supports rendering a user dropdown option tag" do
        text = Liquid::Template.parse("{{ticket.requester.custom_fields.dropdown1}}").render("ticket" => @drop)
        assert_match(/^100$/, text)
      end

      it "supports rendering a user dropdown option title" do
        text = Liquid::Template.parse("{{ticket.requester.custom_fields.dropdown1.title}}").render("ticket" => @drop)
        assert_match(/^Something$/, text)
      end

      it "supports rendering a user dropdown with empty value" do
        text = Liquid::Template.parse("{{ticket.requester.custom_fields.dropdown2}}").render("ticket" => @drop)
        assert_match "", text
        text = Liquid::Template.parse("{{ticket.requester.custom_fields.dropdown2.title}}").render("ticket" => @drop)
        assert_match "", text
      end

      it "supports rendering a user checkbox value" do
        text = Liquid::Template.parse("{{ticket.requester.custom_fields.checkbox1}}").render("ticket" => @drop)
        assert_match(/^true$/, text)
      end

      it "supports rendering a user decimal value" do
        text = Liquid::Template.parse("{{ticket.requester.custom_fields.decimal1}}").render("ticket" => @drop)
        assert_match(/^123.0$/, text)
      end

      it "supports rendering a user date value with a filter" do
        text = Liquid::Template.parse("{{ticket.requester.custom_fields.date1 | date: '%m/%d/%Y'}}").render("ticket" => @drop)
        assert_match(/^01\/01\/2013$/, text)
      end

      # Organization

      it "supports rendering a organization dropdown option tag" do
        text = Liquid::Template.parse("{{ticket.organization.custom_fields.org_dropdown1}}").render("ticket" => @drop)
        assert_match(/^100$/, text)
      end

      it "supports rendering a organization dropdown option title" do
        text = Liquid::Template.parse("{{ticket.organization.custom_fields.org_dropdown1.title}}").render("ticket" => @drop)
        assert_match(/^Something$/, text)
      end

      it "supports rendering a organization dropdown with empty value" do
        text = Liquid::Template.parse("{{ticket.organization.custom_fields.org_dropdown2}}").render("ticket" => @drop)
        assert_match "", text
        text = Liquid::Template.parse("{{ticket.organization.custom_fields.org_dropdown2.title}}").render("ticket" => @drop)
        assert_match "", text
      end

      it "supports rendering a organization checkbox value" do
        # no such field in organization (just in requester user)
        text = Liquid::Template.parse("{{ticket.organization.custom_fields.checkbox1}}").render("ticket" => @drop)
        assert_equal "", text
      end

      it "supports rendering a organization decimal value" do
        text = Liquid::Template.parse("{{ticket.organization.custom_fields.decimal1}}").render("ticket" => @drop)
        assert_match(/^10.1$/, text)
      end
    end

    describe "{{ticket.description}}" do
      before do
        @template = Liquid::Template.parse("{{ticket.description}}")
        @private_comment, @public_comment = @ticket.comments[0..1]
        @public_comment.update_columns is_public: 1, value: 'public comment', created_at: Time.now
        @private_comment.update_columns is_public: 0, value: 'private comment', created_at: 1.hour.ago
      end

      describe "for agent" do
        before do
          @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
        end

        it "renders the ticket's first comment" do
          @template.render("ticket" => @drop).must_include "private comment"
        end
      end

      describe "for end-user" do
        before do
          @drop = Zendesk::Liquid::TicketDrop.new(@ticket, false, "text/plain")
        end

        it "renders the ticket's first public comment" do
          @template.render("ticket" => @drop).must_include "public comment"
        end
      end
    end

    describe "{% for comment in ticket.comments %} loop" do
      before do
        @ticket   = tickets(:minimum_1)
        @drop     = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
        @template = Liquid::Template.parse("{% for comment in ticket.comments %}{{ comment }} - {% endfor %}")
      end

      it "renders the the ticket's comments" do
        assert_equal "minimum ticket one duplicate\n\n\n     LINE 1\n\n\n    LINE 2 - minimum ticket 1 - minimum ticket 1 - ",
          @template.render("ticket" => @drop)
      end
    end
  end

  describe "#id" do
    it "defers to #nice_id" do
      @drop.expects(:nice_id).returns(89)
      assert_equal 89, @drop.id
    end
  end

  describe "#nice_id" do
    it "defers to ticket" do
      @ticket.expects(:nice_id).returns(124)
      assert_equal 124, @drop.nice_id
    end
  end

  describe "#encoded_id" do
    it "defers to ticket encoded_id v1" do
      @ticket.expects(:encoded_id).returns("JBZC-5YZS")
      assert_equal "JBZC-5YZS", @drop.encoded_id
    end

    it "can also defer to ticket encoded_id with v2 format" do
      @ticket.expects(:encoded_id).returns("L62NMG-ENYO")
      assert_equal "L62NMG-ENYO", @drop.encoded_id
    end
  end

  describe "#title" do
    it "defers to ticket.title_for_role" do
      @ticket.expects(:title_for_role).returns("hello")
      assert_equal "hello", @drop.title
    end
  end

  describe "#description" do
    it "calls render on the first comment on the ticket" do
      @drop.description.must_include "minimum ticket 5"
    end
  end

  describe "#url" do
    describe "when no ticket_url was passed in options" do
      it "defers to ticket.url" do
        @ticket.expects(:url).with(for_agent: true).returns("wibble")
        assert_equal "wibble", @drop.url
      end

      describe "and no help center or web portal is available" do
        before do
          @account.settings.web_portal_state = 'disabled'
          @account.save!
          @drop = Zendesk::Liquid::TicketDrop.new(@ticket.reload, false, "text/plain")
        end

        it "a restricted HC returns no url for end users" do
          @account.settings.help_center_state = 'restricted'
          assert_nil @drop.url
        end

        it "a disabled HC returns no url for end users" do
          @account.settings.help_center_state = 'disabled'
          assert_nil @drop.url
        end

        it "still returns an url for agents" do
          @account.settings.help_center_state = 'disabled'
          @drop = Zendesk::Liquid::TicketDrop.new(@ticket.reload, true, "text/plain")
          assert_equal "minimum.zendesk-test.com/agent/tickets/5", @drop.url
        end
      end
    end

    describe "when a ticket_url was passed in options" do
      before { @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain", ticket_url: "the url") }
      it "returns that URL" do
        assert_equal "the url", @drop.url
      end
    end
  end

  describe "#link" do
    it "renders {{ticket.url_with_protocol}} as the url_with_protocol" do
      text = Liquid::Template.parse("{{ticket.url_with_protocol}}").render("ticket" => @drop)
      assert_equal @ticket.url(for_agent: true), text
    end
  end

  describe "#url_with_protocol" do
    it "is same as ticket.link" do
      assert_equal @drop.link, @drop.url_with_protocol
    end
  end

  describe "#tags" do
    it "defers to ticket.current_tags" do
      @ticket.expects(:current_tags).returns("kettle black")
      assert_equal "kettle black", @drop.tags
    end
  end

  describe "#in_business_hours" do
    it "defers to ticket.in_business_hours?" do
      @ticket.expects(:in_business_hours?).returns(true)
      assert_equal "true", @drop.in_business_hours
    end
  end

  describe "#current_holiday_name" do
    before do
      @account.schedules.create(name: 'Test', time_zone: 'UTC').tap do |schedule|
        schedule.workweek.configure_as_default

        schedule.holiday_calendar.add_holiday(
          name:       'Holiday',
          start_date: Date.today,
          end_date:   Date.today.next_day
        )
      end
    end

    describe "when the current time is on a holiday" do
      it "returns the name of the holiday" do
        assert_equal 'Holiday', @drop.current_holiday_name
      end
    end

    describe "when the current time is not on a holiday" do
      before { Timecop.travel(2.days.ago) }

      it "returns nil" do
        assert_nil @drop.current_holiday_name
      end
    end
  end

  it "renders {{ticket.created_at_with_time}} as a date with time" do
    date             = Liquid::Template.parse("{{ticket.created_at}}").render("ticket" => @drop)
    date_with_time   = Liquid::Template.parse("{{ticket.created_at_with_time}}").render("ticket" => @drop)
    assert_equal date_with_time.length, (date.length + 7)
  end

  describe "date and time methods" do
    [:created_at, :updated_at, :due_date].each do |stamp|
      describe "##{stamp}" do
        it "calls format_date with ticket.#{stamp}" do
          @drop.expects(:format_date).with(@ticket.send(stamp)).returns("date")
          assert_equal "date", @drop.send(stamp)
        end
      end

      describe "##{stamp}_with_time" do
        it "calls format_time with ticket.#{stamp}" do
          @drop.expects(:format_time).with(@ticket.send(stamp)).returns("time")
          assert_equal "time", @drop.send("#{stamp}_with_time")
        end
      end

      describe "##{stamp}_with_timestamp" do
        it "calls format_timestamp with ticket.#{stamp}" do
          @drop.expects(:format_timestamp).with(@ticket.send(stamp)).returns("timestamp")
          assert_equal "timestamp", @drop.send("#{stamp}_with_timestamp")
        end
      end
    end

    describe "formatting" do
      describe "#format_date" do
        it "handles a bad time" do
          assert_equal '', @drop.send(:format_date, nil)
        end

        it "defers to Time#to_format(false, format, zone)" do
          @drop.instance_variable_set(:@time_format, :foo)
          @drop.instance_variable_set(:@time_zone, :bar)
          time = Time.now
          time.expects(:to_format).with(time_zone: :bar).returns("date")
          assert_equal "date", @drop.send(:format_date, time)
        end
      end

      describe "#format_time" do
        it "handles a bad time" do
          assert_equal '', @drop.send(:format_time, nil)
        end

        it "defers to Time#to_format(true, format, zone)" do
          @drop.instance_variable_set(:@time_format, :foo)
          @drop.instance_variable_set(:@time_zone, :bar)
          time = Time.now
          time.expects(:to_format).with(with_time: true, time_format: :foo, time_zone: :bar).returns("time")
          assert_equal "time", @drop.send(:format_time, time)
        end
      end

      describe "#format_timestamp" do
        it "handles a bad time" do
          assert_equal '', @drop.send(:format_timestamp, nil)
        end

        it "invokes iso8601 formatting" do
          time = Time.now
          time.expects(:iso8601).returns("timestamp")
          assert_equal "timestamp", @drop.send(:format_timestamp, time)
        end
      end
    end
  end

  describe "time settings" do
    describe "with no current_user" do
      it "defaults to account settings" do
        @ticket.account.expects(:time_zone).returns("Bogota")
        @ticket.account.expects(:time_format_string).returns("%H:%M")
        drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
        assert_equal "February 13, 2009, 18:31", drop.send(:format_time, Time.at(1234567890))
      end
    end

    describe "with current_user that is system user" do
      it "defaults to account settings" do
        @ticket.account.expects(:time_zone).returns("Bogota")
        @ticket.account.expects(:time_format_string).returns("%H:%M")
        user = User.find(-1)
        drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain", current_user: user)
        assert_equal "February 13, 2009, 18:31", drop.send(:format_time, Time.at(1234567890))
      end
    end

    describe "with regular current_user" do
      let(:user) { User.new(name: "timeless user", account: @account, time_zone: "Arizona") }
      let(:drop_options) { { current_user: user } }
      let(:drop) { Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain", drop_options) }
      before do
        @ticket.account.expects(:time_zone).never
        @ticket.account.expects(:time_format_string).never
      end

      it "uses user's settings" do
        assert_equal "February 13, 2009, 4:31 PM", drop.send(:format_time, Time.at(1234567890))
      end

      describe "when timezone indicator is requested" do
        before { drop_options[:include_tz] = true }

        it "includes the timezone" do
          assert_equal "February 13, 2009, 4:31 PM MST", drop.send(:format_time, Time.at(1234567890))
        end
      end
    end
  end

  describe "#liquid_method_missing" do
    Zendesk::Liquid::TicketDrop::ALLOWED_METHODS.each do |method|
      describe "method #{method}" do
        before { @drop.expects(:escape).with(@ticket.send(method)).returns(@ticket.send(method)) }

        it "proxys straight to ticket" do
          assert_equal_with_nil @ticket.send(method), @drop.liquid_method_missing(method)
        end
      end
    end

    describe "with description system field" do
      def render_description_field
        field = @ticket.account.ticket_fields.find_by_type("FieldDescription")
        @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
        Liquid::Template.parse("asdf {{ticket.ticket_field_option_title_#{field.id}}}").render("ticket" => @drop)
      end

      before { Ticket.any_instance.stubs(description: "qwerty") }

      it "renders an empty string" do
        render_description_field.must_equal "asdf "
      end
    end
  end

  describe "#escape" do
    describe "when given a string" do
      describe "and format is HTML" do
        before { @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/html", @locale) }
        it "returns the escaped value" do
          assert_equal "1 &lt; 3", @drop.send(:escape, "1 < 3")
        end
      end

      describe "and format is not HTML" do
        before { @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain", @locale) }
        it "returns the unescaped value" do
          assert_equal "1 < 3", @drop.send(:escape, "1 < 3")
        end
      end
    end

    describe "when given a non-string" do
      it "returns that instance" do
        assert_equal 123, @drop.send(:escape, 123)
      end
    end
  end

  describe "associations" do
    it "is read from ticket" do
      [:account, :brand, :ticket_form, :group, :organization, :assignee, :submitter, :requester].each do |assoc|
        assert_equal_with_nil @drop.send(assoc), @ticket.send(assoc)
      end
    end
  end

  describe "#account" do
    it "returns the account instance from the ticket" do
      assert_equal @ticket.account, @drop.account
    end
  end

  describe "#latest_comment" do
    describe "when the target is an end user" do
      before do
        @drop.expects(:for_agent).returns(false)
        @ticket.expects(:latest_public_comment).returns("hello")
      end

      it "renders the latest public comment" do
        assert_equal "hello", Liquid::Template.parse("{{ticket.latest_comment}}").render("ticket" => @drop)
      end
    end

    describe "when the target is an agent" do
      before do
        @drop.expects(:for_agent).returns(true)
        @ticket.expects(:latest_comment).returns("hello")
      end

      it "renders the latest comment" do
        assert_equal "hello", Liquid::Template.parse("{{ticket.latest_comment}}").render("ticket" => @drop)
      end
    end
  end

  describe "#follower_reply_type_message" do
    before { @drop.stubs(format: Mime[:html]) }

    let(:comment) { events(:create_comment_2_for_minimum_ticket_5) }
    let(:template) { Liquid::Template.parse("{{ticket.follower_reply_type_message}}") }

    subject { template.render("ticket" => @drop) }

    describe 'when account has follower_and_email_cc_collaborations disabled' do
      before { Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: false) }
      it { subject.must_include "Reply to this email to add a comment to the request." }
    end

    describe 'when account has follower_and_email_cc_collaborations enabled' do
      before { Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }

      describe 'and the last comment was an Internal note' do
        before { Comment.any_instance.stubs(is_public: false, is_private?: true) }
        it { subject.must_include "Reply to this email to add an internal note to the request." }
      end

      describe 'and the last comment was a Public comment' do
        before { Comment.any_instance.stubs(is_public: true, is_private?: false) }
        it { subject.must_include "Reply to this email to add a comment to the request." }
      end
    end
  end

  describe "#last_three_comments_formatted" do
    before do
      @ticket = tickets(:minimum_6)
      @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
      @drop.stubs(format: Mime[:html])
    end

    it "renders the last three public comments" do
      html = Liquid::Template.parse("{{ticket.last_three_comments_formatted}}").render("ticket" => @drop)
      @ticket.last_few_comments.map(&:value).each { |comment| assert_includes html, comment }
    end
  end

  describe "#latest_comment_rich" do
    let(:comment) { events(:create_comment_2_for_minimum_ticket_5) }

    before { @drop.stubs(format: Mime[:html]) }

    it "renders the latest comment" do
      Liquid::Template.parse("{{ticket.latest_comment_rich}}").render("ticket" => @drop).must_include "minimum <span>ticket</span> 5"
    end
  end

  describe "collaboration" do
    def render_template_drop
      Liquid::Template.parse(template).render('ticket' => @drop)
    end

    let(:agent_with_alias) do
      agent = users(:minimum_admin)
      agent.name = 'Agent'
      agent.alias = 'Alias'
      agent.save
      agent
    end

    let(:agent_with_name) do
      agent = users(:minimum_admin_not_owner)
      agent.name = 'Agent'
      agent.save
      agent
    end

    let(:legacy_agent) do
      agent = users(:minimum_admin_not_verified)
      agent.name = 'Legacy'
      agent.save
      agent
    end

    let(:end_user) do
      end_user = users(:minimum_end_user)
      end_user.name = 'EndUser'
      end_user.save
      end_user
    end

    before do
      # NOTE: Agents can have aliases only if agent_display_names is enabled.
      Account.any_instance.stubs(has_agent_display_names?: true)

      @ticket.collaborations << Collaboration.new(collaborator_type: CollaboratorType.LEGACY_CC, user: legacy_agent)
      @ticket.collaborations << Collaboration.new(collaborator_type: CollaboratorType.FOLLOWER,  user: agent_with_alias)
      @ticket.collaborations << Collaboration.new(collaborator_type: CollaboratorType.EMAIL_CC,  user: agent_with_name)
      @ticket.collaborations << Collaboration.new(collaborator_type: CollaboratorType.EMAIL_CC,  user: end_user)
      @ticket.save
    end

    describe "when the new collaboration is disabled" do
      before { CollaborationSettingsTestHelper.disable_all_new_collaboration_settings }

      describe '#cc_names' do
        let(:template) { '{{ticket.cc_names}}' }

        it { assert_equal 'Legacy, Alias, Agent, EndUser', render_template_drop }
      end

      describe '#email_cc_names' do
        let(:template) { '{{ticket.email_cc_names}}' }

        # Like `Ticket#email_ccs` (and the API), email_cc_names is not populated when the CCs feature is off
        it { assert_equal '', render_template_drop }
      end

      describe '#follower_names' do
        let(:template) { '{{ticket.follower_names}}' }

        # follower_names is empty when the CCs feature is off
        it { assert_equal '', render_template_drop }
      end

      describe '#ccs' do
        let(:template) { '{% for user in ticket.ccs %} {{user.email}} {% endfor %}' }

        it { assert_includes render_template_drop, 'minimum_admin@aghassipour.com' }
        it { assert_includes render_template_drop, 'minimum_admin_not_owner@aghassipour.com' }
        it { assert_includes render_template_drop, 'minimum_end_user@aghassipour.com' }
      end

      describe '#email_ccs' do
        let(:template) { '{% for user in ticket.email_ccs %} {{user.email}} {% endfor %}' }
        it { assert_equal render_template_drop, '' }
      end

      describe '#followers' do
        let(:template) { '{% for user in ticket.followers %} {{user.email}} {% endfor %}' }

        it { refute_includes render_template_drop, 'minimum_admin@aghassipour.com' }
        it { refute_includes render_template_drop, 'minimum_admin_not_owner@aghassipour.com' }
        it { refute_includes render_template_drop, 'minimum_end_user@aghassipour.com' }

        it "does not include followers" do
          @ticket.expects(:followers).never
          render_template_drop
        end
      end
    end

    describe "when the new collaboration is enabled" do
      before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

      describe "#cc_names" do
        let(:template) { '{{ticket.cc_names}}' }

        it { assert_equal 'Agent, EndUser', render_template_drop }
      end

      describe "#email_cc_names" do
        let(:template) { '{{ticket.email_cc_names}}' }
        it { assert_equal 'Agent, EndUser', render_template_drop }
      end

      describe "#follower_names" do
        # LEGACY_CC is transformed to FOLLOWER if it is an Agent
        let(:template) { '{{ticket.follower_names}}' }

        it { assert_equal 'Alias, Legacy', render_template_drop }
      end

      describe '#ccs' do
        let(:template) { '{% for user in ticket.ccs %} {{user.email}} {% endfor %}' }

        it { assert_includes render_template_drop, 'minimum_admin_not_owner@aghassipour.com' }
        it { assert_includes render_template_drop, 'minimum_end_user@aghassipour.com' }

        # Agent with legacy CC type would be a follower and this Arturo fixes "ccs"
        # behavior to be consistent with "email_ccs"
        it { refute_includes render_template_drop, 'minimum_admin@aghassipour.com' }
      end

      describe '#email_ccs' do
        let(:template) { '{% for user in ticket.email_ccs %} {{user.email}} {% endfor %}' }
        it { assert_includes render_template_drop, 'minimum_admin_not_owner@aghassipour.com' }
        it { assert_includes render_template_drop, 'minimum_end_user@aghassipour.com' }
      end

      describe '#followers' do
        let(:template) { '{% for user in ticket.followers %} {{user.email}} {% endfor %}' }

        it { assert_includes render_template_drop, 'minimum_admin@aghassipour.com' }

        it "includes both persisted and unpersisted followers" do
          @ticket.expects(:followers).with(include_uncommitted: true)
          render_template_drop
        end
      end
    end
  end

  describe "#latest_comment_html" do
    describe_with_arturo_enabled :email_simplified_threading_placeholders do
      describe "when the latest comment is private" do
        let(:ticket) { tickets(:minimum_5) }
        let(:drop) { Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain") }

        before do
          public_comment, private_comment = ticket.comments[0..1]
          private_comment.update_columns is_public: 0, value: 'private comment', created_at: Time.now
          public_comment.update_columns is_public: 1, value: 'public comment', created_at: 1.hour.ago
        end

        describe "for an agent" do
          before { drop.stubs(for_agent: true) }

          it "shows the private comment" do
            Liquid::Template.parse("{{ticket.latest_comment_html}}").render("ticket" => drop).must_include "private comment"
          end
        end

        describe "for an end user" do
          before { drop.stubs(for_agent: false) }

          it "shows the latest public comment" do
            Liquid::Template.parse("{{ticket.latest_comment_html}}").render("ticket" => drop).must_include "public comment"
          end
        end
      end
    end

    describe_with_arturo_disabled :email_simplified_threading_placeholders do
      it "must be empty" do
        assert Liquid::Template.parse("{{ticket.latest_comment_html}}").render("ticket" => @drop).must_be_empty
      end
    end
  end

  describe "#latest_public_comment_html" do
    describe_with_arturo_enabled :email_simplified_threading_placeholders do
      describe "when the latest comment is private" do
        let(:ticket) { tickets(:minimum_5) }
        let(:drop) { Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain") }

        before do
          public_comment, private_comment = ticket.comments[0..1]
          private_comment.update_columns is_public: 0, value: 'private comment', created_at: Time.now
          public_comment.update_columns is_public: 1, value: 'public comment', created_at: 1.hour.ago
        end

        describe "for an agent" do
          before { drop.stubs(for_agent: true) }

          it "shows the latest public comment" do
            Liquid::Template.parse("{{ticket.latest_public_comment_html}}").render("ticket" => drop).must_include "public comment"
          end
        end

        describe "for an end user" do
          before { drop.stubs(for_agent: false) }

          it "shows the latest public comment" do
            Liquid::Template.parse("{{ticket.latest_public_comment_html}}").render("ticket" => drop).must_include "public comment"
          end
        end
      end
    end

    describe_with_arturo_disabled :email_simplified_threading_placeholders do
      it "must be empty" do
        assert Liquid::Template.parse("{{ticket.latest_public_comment_html}}").render("ticket" => @drop).must_be_empty
      end
    end
  end

  describe "#latest_public_comment" do
    it "defers to ticket" do
      @ticket.expects(:latest_public_comment).returns("boo")
      assert_equal "boo", @drop.latest_public_comment
    end
  end

  describe "#comments_formatted" do
    let(:ticket) { tickets(:minimum_1) }
    let(:drop) { Zendesk::Liquid::TicketDrop.new(ticket, true, "text/html") }
    let(:legacy_font_family) { "'Lucida Grande', 'Tahoma', Verdana, sans-serif" }
    let(:new_font_family) { "'system-ui', '-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Roboto', 'Oxygen-Sans', 'Ubuntu', 'Cantarell', 'Helvetica Neue', monospace, 'Arial', 'sans-serif'" }

    describe_with_arturo_enabled :email_simplified_threading do
      it "uses the new font family" do
        Liquid::Template.parse("{{ticket.comments_formatted}}").render("ticket" => drop).must_include new_font_family
      end

      it "does not use the legacy font family" do
        Liquid::Template.parse("{{ticket.comments_formatted}}").render("ticket" => @drop).wont_include legacy_font_family
      end
    end

    describe_with_arturo_disabled :email_simplified_threading do
      it "uses the legacy font family" do
        Liquid::Template.parse("{{ticket.comments_formatted}}").render("ticket" => drop).must_include legacy_font_family
      end

      it "does not use the new font family" do
        Liquid::Template.parse("{{ticket.comments_formatted}}").render("ticket" => drop).wont_include new_font_family
      end
    end
  end

  describe "#latest_comment_formatted" do
    let(:ticket) { tickets(:minimum_1) }
    let(:drop) { Zendesk::Liquid::TicketDrop.new(ticket, true, "text/html") }
    let(:legacy_font_family) { "'Lucida Grande', 'Tahoma', Verdana, sans-serif" }
    let(:new_font_family) { "'system-ui', '-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Roboto', 'Oxygen-Sans', 'Ubuntu', 'Cantarell', 'Helvetica Neue', monospace, 'Arial', 'sans-serif'" }

    describe_with_arturo_enabled :email_simplified_threading do
      it "uses the new font family" do
        Liquid::Template.parse("{{ticket.latest_comment_formatted}}").render("ticket" => drop).must_include new_font_family
      end

      it "does not use the legacy font family" do
        Liquid::Template.parse("{{ticket.latest_comment_formatted}}").render("ticket" => @drop).wont_include legacy_font_family
      end
    end

    describe_with_arturo_disabled :email_simplified_threading do
      it "uses the legacy font family" do
        Liquid::Template.parse("{{ticket.latest_comment_formatted}}").render("ticket" => drop).must_include legacy_font_family
      end

      it "does not use the new font family" do
        Liquid::Template.parse("{{ticket.latest_comment_formatted}}").render("ticket" => drop).wont_include new_font_family
      end
    end
  end

  describe "#latest_public_comment_rich" do
    let(:comment) { events(:create_comment_2_for_minimum_ticket_5) }

    before { @drop.stubs(format: Mime[:html]) }

    it "renders the latest public comment" do
      Liquid::Template.parse("{{ticket.latest_public_comment_rich}}").render("ticket" => @drop).must_include "minimum <span>ticket</span> 5"
    end
  end

  describe "#comments" do
    describe "when the target is an end user" do
      before do
        @drop.expects(:for_agent).returns(false)
        @drop.expects(:public_comments).returns("hello")
      end

      it "renders the public comments" do
        assert_equal "hello", Liquid::Template.parse("{{ticket.comments}}").render("ticket" => @drop)
      end
    end

    describe "when the target is an agent" do
      before do
        @drop.expects(:for_agent).returns(true)
        @drop.expects(:finder_options).returns("hello")
      end

      it "renders the comments" do
        assert_equal "hello", Liquid::Template.parse("{{ticket.comments}}").render("ticket" => @drop)
      end
    end
  end

  describe "#format_comments" do
    before do
      @context = Zendesk::Liquid::Comments::CollectionDrop.new(
        @account, @ticket.recent_comments.all, _format = "text/plain", _for_agent = true, nil, @ticket.brand
      )
    end

    it "renders the formatted comments" do
      assert_equal "----------------------------------------------\n\nminimum_end_user, Jan 1, 2007, 5:32 AM\n\nminimum ticket 5\n\n----------------------------------------------\n\nminimum_search_user, Jan 1, 2007, 5:32 AM\n\nminimum ticket 5", @context.render
    end

    it "renders the formatted comments with proper brand context" do
      assert_equal @context.brand, @ticket.brand
    end
  end

  describe "accessing ticket.audits" do
    before do
      @ticket = tickets(:minimum_1)
      @admin  = users(:minimum_admin)
      @ticket.will_be_saved_by(@admin)
      @ticket.save!

      @drop = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
    end

    it "renders {{ticket.audits.size}} as the audit count" do
      text = Liquid::Template.parse("{{ticket.audits.size}}").render("ticket" => @drop)
      assert_equal @ticket.audits.count(:all).to_s, text
    end

    it "renders {{ticket.audits.last.timestamp}} as seconds since epoch" do
      text = Liquid::Template.parse("{{ticket.audits.last.timestamp}}").render("ticket" => @drop)
      assert_equal @ticket.audits.last.created_at.to_i.to_s, text
    end

    it "renders {{ticket.audits.first.comment.value}} as body of the first comment" do
      text = Liquid::Template.parse("{{ticket.audits.first.comment.value}}").render("ticket" => @drop)
      assert_equal @ticket.audits.first.comment.body, text
    end

    it "renders {{ticket.audits.first.comment.is_public}} as a boolean" do
      text = Liquid::Template.parse("{{ticket.audits.first.comment.is_public}}").render("ticket" => @drop)
      assert_equal @ticket.audits.first.comment.is_public.to_s, text
    end

    it "exposes {{ticket.audits.last.changes}}" do
      text = Liquid::Template.parse("{{ticket.audits.last.changes}}").render("ticket" => @drop)
      assert_equal "Status changed from New to Open", text
      text = Liquid::Template.parse("{{ticket.audits.last.changes.first.field_name}}").render("ticket" => @drop)
      assert_equal "status_id", text
      text = Liquid::Template.parse("{{ticket.audits.last.changes.first.value}}").render("ticket" => @drop)
      assert_equal "1", text
      text = Liquid::Template.parse("{{ticket.audits.last.changes.first.value_previous}}").render("ticket" => @drop)
      assert_equal "0", text
    end

    it "works with complex templates" do
      template = <<-LIQUID
      {% for audit in ticket.audits %}
        {{ audit.timestamp }}
        {{ audit.is_public }}
        {{ audit.author.name }}
        {{ audit.is_public }}
        {% if audit.comment != nil %}
          {{ audit.comment.body }}
        {% endif %}
        {% if audit.changes.size != 0 %}
          {% for change in audit.changes %}
            {{ change }}
            {{ change.field_name }}
            {{ change.value }}
            {{ change.value_previous }}
          {% endfor %}
        {% endif %}
      {% endfor %}"
      LIQUID
      assert Liquid::Template.parse(template).render("ticket" => @drop)
    end
  end

  describe "#render_custom_field" do
    before do
      @ticket = tickets(:minimum_2)
      @drop   = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
      @field  = ticket_fields(:field_text_custom)
      TicketFieldEntry.any_instance.stubs(value: "<script>alert(1)</script>")
    end

    it "escapes custom field values" do
      text = @drop.send(:render_custom_field, @field)
      assert_equal "<script>alert(1)</script>", text
      assert text.html_safe?
    end
  end

  describe "#quoted_comments" do
    before do
      @ticket = tickets(:minimum_1)
      @drop   = Zendesk::Liquid::TicketDrop.new(@ticket, true, "text/plain")
    end

    describe "for agent" do
      describe "for html" do
        before { @drop.stubs(:format).returns('text/html') }

        it "renders previous replies one level quoted in block quotes, zendesk formatted" do
          assert_equal "<hr style=\"border: none 0; border-top: 1px dashed #c2c2c2; height: 1px; margin: 15px 0 0 0;\"/><br /><strong>Agent Minimum, Jan 1, 2007, 5:32 AM:</strong><div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\"><div><p style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\" dir=\"auto\">minimum <span>ticket</span> one duplicate</p>\n<pre style=\"background-color: #F8F8F8; border: 1px solid #ccc; border-radius: 3px; font-size: 13px; line-height: 19px; margin: 15px 0; max-width: 100%; padding: 6px 10px\"><code style=\"background: #F8F8F8 none; border: 0 none transparent; display: block; font-family: Consolas, Menlo, Monaco, 'Lucida Console', 'Liberation Mono', 'DejaVu Sans Mono', 'Bitstream Vera Sans Mono', 'Courier New', monospace, serif; font-size: 13px; margin: 0; max-width: 100%; padding: 0; white-space: pre-wrap\">\n LINE 1\n\n\nLINE 2\n</code>\n</pre>\n</div></div>\n\n<div class=\"gmail_quote\">  <blockquote class=\"gmail_quote\"><hr style=\"border: none 0; border-top: 1px dashed #c2c2c2; height: 1px; margin: 15px 0 0 0;\"/><br /><strong>Agent Minimum, Jan 1, 2007, 5:32 AM:</strong>      <p style=\"color:#B94A48;margin-bottom:10px;\">This comment has been flagged. <a href=\"https://support.zendesk.com/hc/en-us/articles/203663756#topic_nr4_4s5_cq\" target=\"_blank\">Learn more</a></p>  <div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\">minimum <span>ticket 1</span></div>\n\n<hr style=\"border: none 0; border-top: 1px dashed #c2c2c2; height: 1px; margin: 15px 0 0 0;\"/><br /><strong>Agent Minimum, Jan 1, 2007, 5:32 AM:</strong><div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\">minimum <span>ticket 1</span></div>\n\n  </blockquote></div>",
            Liquid::Template.parse("{{ticket.quoted_comments}}").render("ticket" => @drop)
        end
      end

      describe "for text" do
        it "renders previous replies one level quoted in block quotes, zendesk formatted" do
          assert_equal "----------------------------------------------\n\nAgent Minimum, Jan 1, 2007, 5:32 AM\n\nminimum ticket one duplicate\n\n\n     LINE 1\n\n\n    LINE 2\n\n----------------------------------------------\n>\n>!*** This comment has been flagged. Learn more ***!\n>\n>Agent Minimum, Jan 1, 2007, 5:32 AM\n>\n>minimum ticket 1\n>\n>----------------------------------------------\n>\n>Agent Minimum, Jan 1, 2007, 5:32 AM\n>\n>minimum ticket 1\n>\n>",
            Liquid::Template.parse("{{ticket.quoted_comments}}").render("ticket" => @drop)
        end
      end
    end

    describe "for end_user" do
      before { @drop.stubs(:for_agent).returns(false) }

      describe "for html" do
        before { @drop.stubs(:format).returns('text/html') }

        it "renders previous replies deep quoted in block quotes, plain email formatted" do
          assert_equal "<div><div><p>minimum <span>ticket</span> one duplicate</p>\n<pre><code>\n LINE 1\n\n\nLINE 2\n</code>\n</pre>\n</div></div>\n\n<div class=\"gmail_quote\">On Jan 1, 2007, 5:32 AM, Agent Minimum <minimum_agent@aghassipour.com> wrote<br><blockquote class=\"gmail_quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><div>minimum <span>ticket 1</span></div>\n\n<div class=\"gmail_quote\">On Jan 1, 2007, 5:32 AM, Agent Minimum <minimum_agent@aghassipour.com> wrote<br><blockquote class=\"gmail_quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><div>minimum <span>ticket 1</span></div>\n\n</blockquote></div></blockquote></div>",
            Liquid::Template.parse("{{ticket.quoted_comments}}").render("ticket" => @drop)
        end
      end

      describe "for text" do
        it "renders previous replies deep quoted in block quotes, plain email formatted" do
          assert_equal "----------------------------------------------\n\nAgent Minimum, Jan 1, 2007, 5:32 AM\n\nminimum ticket one duplicate\n\n\n     LINE 1\n\n\n    LINE 2\n\n----------------------------------------------\n>\n>Agent Minimum, Jan 1, 2007, 5:32 AM\n>\n>minimum ticket 1\n>\n>----------------------------------------------\n>>\n>>Agent Minimum, Jan 1, 2007, 5:32 AM\n>>\n>>minimum ticket 1\n>>\n>>",
            Liquid::Template.parse("{{ticket.quoted_comments}}").render("ticket" => @drop)
        end
      end
    end
  end
end
