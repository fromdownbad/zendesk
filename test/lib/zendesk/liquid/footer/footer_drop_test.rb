require_relative "../../../../support/test_helper"
require "zendesk/liquid/footer/footer_drop"

SingleCov.covered!

describe Zendesk::Liquid::Footer::FooterDrop do
  let(:drop) do
    @drop = Zendesk::Liquid::Footer::FooterDrop.new({
      ticket_status: 'new',
      ticket_id: '44089228',
      ticket_url: 'ticket_url/44089228',
      requester_image_url: '',
      requester_name: 'Michelle Chen',
      assignee_image_url: '',
      assignee_name: 'Francis Reynolds',
      assignee_status_changed: true,
      display_ticket_ccs_for_agent: true,
      ticket_ccs_for_agent: '-',
      display_ticket_followers_for_agent: true,
      ticket_followers_for_agent: '-',
      group_title: 'Default Group',
      ticket_type: 'Ticket',
      channel: 'Zendesk Chat',
      priority: 'Normal',
      locale: "en-US-x-1".to_sym,
    })
  end

  let(:drop_v2) do
    @drop = Zendesk::Liquid::Footer::FooterDrop.new({
      ticket_status: 'new',
      ticket_id: '44089228',
      ticket_url: 'ticket_url/44089228',
      requester_image_url: '',
      requester_name: 'Michelle Chen',
      assignee_image_url: '',
      assignee_name: '',
      assignee_status_changed: false,
      display_ticket_ccs_for_agent: true,
      ticket_ccs_for_agent: '-',
      display_ticket_followers_for_agent: false,
      ticket_followers_for_agent: '-',
      group_title: 'Default Group',
      ticket_type: 'Ticket',
      channel: 'Zendesk Chat',
      priority: 'Normal',
      locale: "en-US-x-1".to_sym,
    })
  end

  describe "#template" do
    it "returns an ERB instance" do
      assert drop.send(:template).is_a?(ERB)
    end
  end

  describe "rendering" do
    describe "when the ticket is assigned to the agent" do
      before do
        drop.stubs(:format).returns(Mime[:html].to_s)
        @result = drop.render
      end

      it "renders {{ticket.status}}" do
        @result.must_include "New"
      end

      it "renders {{ticket_status_bg_color}}" do
        @result.must_include "#FFB648"
        @result.wont_include "#E34F32"
      end

      it "renders {{ticket_status_color}}" do
        @result.must_include "#2f3941"
        @result.wont_include "#49545C"
      end

      it "renders {{ticket.url}}" do
        @result.must_include "ticket_url/44089228"
      end

      it "renders {{requester_name}}" do
        @result.must_include "Michelle Chen"
      end

      it "does not render {{assignee_name}}" do
        @result.must_include "Francis Reynolds"
        @result.wont_include "Unassigned"
      end

      it "renders {{assignee_status_changed}}" do
        @result.must_include "background-color: rgb(209, 232, 223)"
        @result.wont_include "background-color:rgb(255, 240, 241)"
      end

      it "renders {{ticket_ccs_for_agent}}" do
        @result.must_include "-"
        @result.must_include "CCs"
      end

      it "does not render {{ticket_followers_for_agent}}" do
        @result.must_include "Followers"
      end

      it "renders {{group_title}}" do
        @result.must_include "Default Group"
      end

      it "renders {{ticket_type}}" do
        @result.must_include "Ticket"
      end

      it "renders {{channel}}" do
        @result.must_include "Zendesk Chat"
      end

      it "renders {{priority}}" do
        @result.must_include "Normal"
      end
    end

    describe "when the ticket is not assigned to the agent" do
      before do
        drop.stubs(:format).returns(Mime[:html].to_s)
        @result = drop_v2.render
      end

      it "renders {{ticket.status}}" do
        @result.must_include "New"
      end

      it "renders {{ticket_status_bg_color}}" do
        @result.must_include "#FFB648"
        @result.wont_include "#E34F32"
      end

      it "renders {{ticket_status_color}}" do
        @result.must_include "#2f3941"
        @result.wont_include "#49545C"
      end

      it "renders {{ticket.url}}" do
        @result.must_include "ticket_url/44089228"
      end

      it "renders {{requester_name}}" do
        @result.must_include "Michelle Chen"
      end

      it "does not render {{assignee_name}}" do
        @result.wont_include "Francis Reynolds"
        @result.must_include "Unassigned"
      end

      it "renders {{assignee_status_changed}}" do
        @result.wont_include "background-color: rgb(209, 232, 223)"
        @result.must_include "background-color:rgb(255, 240, 241)"
      end

      it "renders {{ticket_ccs_for_agent}}" do
        @result.must_include "-"
        @result.must_include "CCs"
      end

      it "does not render {{ticket_followers_for_agent}}" do
        @result.wont_include "Followers"
      end

      it "renders {{group_title}}" do
        @result.must_include "Default Group"
      end

      it "renders {{ticket_type}}" do
        @result.must_include "Ticket"
      end

      it "renders {{channel}}" do
        @result.must_include "Zendesk Chat"
      end

      it "renders {{priority}}" do
        @result.must_include "Normal"
      end
    end
  end
end
