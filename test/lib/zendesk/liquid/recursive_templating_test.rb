require_relative "../../../support/test_helper"

SingleCov.not_covered! # strange 1-off tests

describe 'Liquid::RecursiveTemplating' do
  fixtures :accounts, :users, :tickets

  describe "Liquid templates" do
    before do
      @text = accounts(:minimum).cms_texts.create!(
        name: "top level",
        fallback_attributes: {
          is_fallback: true,
          nested: true,
          account: accounts(:minimum),
          value: "3{{dc.top_level}}4",
          translation_locale_id: 1
        }
      )

      @ticket = tickets(:minimum_1)
      @current_user = users(:minimum_agent)
      @template_context = Zendesk::Liquid::TicketContext.new("", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = 'text/plain')
    end

    describe "(example 1)" do
      before { @template_context.template = "1{{dc.top_level}}2" }
      it "renders recursively five times" do
        assert_equal "133333444442", @template_context.render
      end
    end

    describe "(example 2)" do
      before { @template_context.template = "1{{dc.top_level}}-{{dc.top_level}}2" }

      it "renders recursively five times" do
        assert_equal "13333344444-33333444442", @template_context.render
      end
    end

    describe "simple formatting" do
      before do
        @text = accounts(:minimum).cms_texts.create!(
          name: "line breaks",
          fallback_attributes: {
            account: accounts(:minimum),
            is_fallback: true,
            nested: true,
            value: "Hi,\n\nYour ticket is {{ticket.id}}.\n\nThanks",
            translation_locale_id: 1
          }
        )
      end

      describe "with nested DC inside of ticket comments" do
        before do
          @ticket.add_comment(body: "{{dc.line_breaks}}",
                              is_public: true)
          @ticket.will_be_saved_by(@current_user)
          @ticket.save!

          @template = "EMAIL TEMPLATE: {{dc.line_breaks}}"
          @template_context = Zendesk::Liquid::TicketContext.new(@template, @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = 'text/html')
        end

        it "simples format DC if not surrounded by HTML" do
          assert_equal "<p>EMAIL TEMPLATE: Hi,</p><p>Your ticket is 1.</p><p>Thanks</p>",
            @template_context.render
        end

        it "simples format DC when the parent template contains html tags" do
          @template_context.template = "EMAIL TEMPLATE: <p>{{dc.line_breaks}}"
          assert_equal "<p>EMAIL TEMPLATE: </p><p>Hi,</p><p>Your ticket is 1.</p><p>Thanks</p>",
            @template_context.render

          @template_context.template = "EMAIL TEMPLATE: {{dc.line_breaks}}</p>"
          assert_equal "<p>EMAIL TEMPLATE: Hi,</p><p>Your ticket is 1.</p><p>Thanks</p>",
            @template_context.render
        end

        it "formats ticket comments correctly" do
          @template_context.template = "sdf <div>Hi</div>{{ticket.comments_formatted}}"
          rendered = @template_context.render

          assert_match /<p[^>]*>Hi,<\/p><p[^>]*>Your ticket is 1.<\/p><p[^>]*>Thanks<\/p>/, rendered
          assert_no_match /<\/p><\/p>/, rendered
        end
      end
    end
  end
end
