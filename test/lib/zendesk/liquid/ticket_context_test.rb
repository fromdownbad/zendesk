require_relative "../../../support/test_helper"
require_relative "../../../support/mailer_test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Liquid::TicketContext do
  def add_comment_to_ticket(comment)
    @ticket.will_be_saved_by(@current_user)
    @ticket.add_comment(body: comment)
    @ticket.save!
  end

  include MailerTestHelper
  fixtures :all

  before do
    @text = accounts(:minimum).cms_texts.create!(
      name: "html",
      fallback_attributes: {
        is_fallback: true,
        nested: true,
        account: accounts(:minimum),
        value: "<div>yay</div>",
        translation_locale_id: 1
      }
    )

    @text_with_url = accounts(:minimum).cms_texts.create!(
      name: "html_with_url",
      fallback_attributes: {
        is_fallback: true,
        nested: true,
        account: accounts(:minimum),
        value: "Here is a link: http://www.zendesk.com ",
        translation_locale_id: 1
      }
    )

    @ticket = tickets(:minimum_1)
    @current_user = users(:minimum_agent)

    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
  end

  describe "truncate" do
    before { add_comment_to_ticket("Testable set of strings fooby dooby") }

    it "truncates correctly when more than one variable" do
      template = "{{ticket.latest_comment | upcase | truncate:9}}"
      @context = Zendesk::Liquid::TicketContext.new(template, @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
      assert_equal "Testable set of strings fooby dooby".upcase.truncate(9), @context.render
    end

    it "truncates correctly when only one variable is used" do
      template = "{{ticket.latest_comment | truncate:9}}"
      @context = Zendesk::Liquid::TicketContext.new(template, @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
      assert_equal "Testable set of strings fooby dooby".truncate(9), @context.render
    end
  end

  describe "#render" do
    before { add_comment_to_ticket("{{dc.html}}") }

    describe "for templates containing dynamic content" do
      before do
        @context = Zendesk::Liquid::TicketContext.new("<div>foo</div>\n\n{{ticket.comments_formatted}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
      end

      it "escapes dynamic content, once" do
        @context.render.must_include "&lt;div&gt;yay&lt;/div&gt;"
      end
    end

    describe "for templates containing current_user placeholder" do
      describe "when author is not passed in" do
        before do
          @context = Zendesk::Liquid::TicketContext.new("{{current_user}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
        end

        it "uses current_user as the current_user placeholder" do
          assert_equal "Agent Minimum", @context.render
        end
      end

      describe "when author is passed in as an option" do
        before do
          @context = Zendesk::Liquid::TicketContext.new("{{current_user}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html], 0, updater: users(:minimum_end_user))
        end

        it "uses the author as the current_user placeholder" do
          assert_equal "minimum_end_user", @context.render
        end
      end

      describe "when using liquid date format filters" do
        before do
          @translation_locales = TranslationLocale.where(public: true)
          assert @translation_locales.any?, "There must be at least one translation locale to test the date filter with"
        end

        it "show dates correctly" do
          @translation_locales.each do |l|
            assert_equal "Locale #{l}: #{@ticket.created_at.strftime("%d/%m/%Y")}", "Locale #{l}: #{Zendesk::Liquid::TicketContext.new("{{ticket.created_at | date: \"%d/%m/%Y\"}}", @ticket, @current_user, _for_agent = true, l, _format = Mime[:html]).render}"
            assert_equal "Locale #{l}: #{Time.zone.now.strftime("%d/%m/%Y")}", "Locale #{l}: #{Zendesk::Liquid::TicketContext.new("{{'now' | date: \"%d/%m/%Y\"}}", @ticket, @current_user, _for_agent = true, l, _format = Mime[:html]).render}"
          end
        end

        it "handles invalid UTF8" do
          @translation_locales.each do |l|
            assert_equal "Locale #{l}: #{Time.zone.now.strftime("%d/%m/%Y")}  B������si", "Locale #{l}: #{Zendesk::Liquid::TicketContext.new("{{'now' | date: \"%d/%m/%Y\"}}  B\xED\xB9\xB2\xED\xB7\xA5si", @ticket, @current_user, _for_agent = true, l, _format = Mime[:html]).render}"
          end
        end
      end
    end

    describe "for templates containing satisfaction rating templates" do
      before do
        @context = Zendesk::Liquid::TicketContext.new("{{satisfaction.rating_section}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
      end

      it "does not escape HTML" do
        @context.render.must_include "<h3 style=\"font-size: 1.1em;"
        @context.render.must_include "Bad, I'm unsatisfied</a>"
      end

      it "does not produce extra line-breaks" do
        assert_no_match %r{<br/>}, @context.render
      end
    end

    describe "account option" do
      let(:ticket) { tickets(:minimum_3) }
      let(:account) { accounts(:support) }

      it "uses the passed in account" do
        context = Zendesk::Liquid::TicketContext.new(stub, ticket, stub(time_zone: nil), stub, stub, stub, stub, account: account)
        assert_equal context.fields['account'], account
      end

      it "uses the account from ticket without the account option" do
        context = Zendesk::Liquid::TicketContext.new(stub, ticket, stub(time_zone: nil), stub, stub, stub, stub)
        assert_equal context.fields['account'], ticket.account
      end
    end

    describe "when passing in a dc_cache as an option" do
      let(:ticket) { tickets(:minimum_3) }

      describe "when an account has dynamic content" do
        before do
          fallback_attributes = { is_fallback: true, nested: true, value: "Welcome Wombat", translation_locale_id: 1 }
          @text = Cms::Text.create!(name: "welcome_wombat", fallback_attributes: fallback_attributes, account: ticket.account)
        end

        it "does not query the db for cms_texts or cms_variants" do
          dc_cache = {@text.identifier => @text.fallback.value}

          queries = sql_queries do
            rendered = Zendesk::Liquid::TicketContext.new("{{dc.#{@text.identifier}}}", ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html], 0, dc_cache: dc_cache).render
            assert_equal @text.fallback.value, rendered
          end

          queries.join("\n").wont_include "cms_texts"
          queries.join("\n").wont_include "cms_variants"
        end

        describe "when not passing in a dc_cache" do
          it "queries the db for cms_texts or cms_variants" do
            queries = sql_queries do
              rendered = Zendesk::Liquid::TicketContext.new("{{dc.#{@text.identifier}}}", ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html]).render
              assert_equal @text.fallback.value, rendered
            end

            queries.join("\n").must_include "cms_texts"
            queries.join("\n").must_include "cms_variants"
          end
        end
      end
    end

    describe "for templates containing just ticket context" do
      before do
        @context = Zendesk::Liquid::TicketContext.new("{{ticket}}", @ticket, @current_user, _for_agent = true, _locale = ENGLISH_BY_ZENDESK, _format = Mime[:html])
      end

      it "renders nothing" do
        assert_equal @context.render, ""
      end
    end
  end

  describe 'Triggers' do
    let(:account)    { accounts(:minimum) }
    let(:definition) { Definition.new }
    let(:email)      { ActionMailer::Base.deliveries.last }

    let(:trigger) do
      Trigger.new(
        title:      'DC',
        definition: definition,
        account:    account,
        owner:      account
      )
    end

    before do
      definition.conditions_all.push(
        DefinitionItem.build(
          operator:    'less_than',
          source:      'status_id',
          value:       ['4'],
          first_value: '4'
        )
      )
    end

    describe 'when containing dynamic content' do
      let(:ticket)       { tickets(:minimum_1) }
      let(:current_user) { users(:minimum_agent) }

      let(:ticket_url) { ticket.url(user: current_user, for_agent: true) }
      let(:delimiter)  { 'delimiter-delimiter-delimiter' }

      let(:header_text) do
        I18n.t(
          'txt.email.notification.message',
          ticket_id:    ticket.nice_id,
          locale:       ENGLISH_BY_ZENDESK,
          ticket_title: CGI.escapeHTML(ticket.title_for_role(true, 250))
        )
      end

      let(:footer_text) do
        I18n.t(
          'txt.email.footer',
          account_name: ticket.brand.name,
          locale:       ENGLISH_BY_ZENDESK
        )
      end

      before do
        definition.actions.push(DefinitionItem.build(
          operator: 'is',
          source:   'notification_user',
          value:    ['current_user', 'SUBJECT', '{{ticket.comments_formatted}}']
        ))

        trigger.save!

        add_comment_to_ticket('<div>bar</div>{{dc.html}}')
      end

      describe 'and rendering HTML' do
        let(:header) do
          "<a href=\"#{ticket_url}\" class=\"zd_link\">#{header_text}</a>".
            html_safe
        end

        let(:content) do
          Zendesk::Liquid::TicketContext.render(
            "{{ticket.comments_formatted}}",
            ticket,
            current_user,
            true,
            ENGLISH_BY_ZENDESK,
            Mime[:html].to_s,
            ticket_url:       ticket_url,
            current_user:     current_user,
            updater:          current_user,
            include_tz:       true,
            skip_attachments: []
          )
        end

        let(:message_slug) do
          "<span style='color:#FFFFFF' aria-hidden='true'>[#{ticket.encoded_id}]</span>" \
            "<span style='color:#FFFFFF' aria-hidden='true'>Ticket-Id:#{ticket.nice_id}</span>" \
            "<span style='color:#FFFFFF' aria-hidden='true'>Account-Subdomain:#{account.subdomain}</span>"
        end

        let(:gmail_markup) do
          <<~HTML.delete("\n")
            <div itemscope itemtype="http://schema.org/EmailMessage" style="display:none">
              <div itemprop="action" itemscope itemtype="http://schema.org/ViewAction">
                <link itemprop="url" href="#{ticket.url(for_agent: true)}" />
                <meta itemprop="name" content="#{I18n.t(
                  'txt.events_mailer.gmail_markup.view_ticket.button_v2'
                )}"/>
              </div>
            </div>
          HTML
        end

        let(:expected_body) do
          Zendesk::Liquid::MailContext.render(
            template:           account.html_mail_template,
            header:             header,
            content:            content,
            footer:             footer_text,
            account:            account,
            delimiter:          delimiter,
            translation_locale: ENGLISH_BY_ZENDESK
          ) << message_slug << gmail_markup
        end

        it 'renders the HTML content correctly' do
          assert_equal EventsMailer.add_direction_tag(expected_body, current_user.account), body_by_content_type(email, Mime[:html])
        end
      end

      describe 'and rendering plain text' do
        let(:footer) do
          "--------------------------------\n#{footer_text}"
        end

        let(:content) do
          Zendesk::Liquid::TicketContext.render(
            '{{ticket.comments_formatted}}',
            ticket,
            current_user,
            true,
            ENGLISH_BY_ZENDESK,
            Mime[:text].to_s,
            ticket_url:       ticket_url,
            current_user:     current_user,
            updater:          current_user,
            include_tz:       true,
            skip_attachments: []
          )
        end

        let(:expected_body) do
          Zendesk::Liquid::MailContext.render(
            template:           account.text_mail_template,
            header:             header_text,
            content:            "#{delimiter}\n\n#{content}",
            footer:             footer,
            account:            account,
            delimiter:          delimiter,
            translation_locale: ENGLISH_BY_ZENDESK
          ) << "\n\n\n\n\n\n\n\n\n\n[#{ticket.encoded_id}]"
        end

        it 'renders the text content correctly' do
          assert_equal expected_body, body_by_content_type(email, Mime[:text])
        end
      end
    end

    describe "when containing dynamic content with URLs" do
      before do
        definition.actions.push(DefinitionItem.build(
          operator: "is",
          source: "notification_user",
          value: ["current_user", "SUBJECT", "{{dc.html_with_url}} {{ticket.comments_formatted}}"]
        ))

        trigger.save!

        add_comment_to_ticket("<div>bar</div>")
      end

      it "renders HTML content correctly" do
        body_by_content_type(email, Mime[:html]).must_include 'Here is a link: <a href="http://www.zendesk.com" rel="noreferrer">http://www.zendesk.com</a>'
      end
    end

    describe "that do not contain any liquid" do
      before do
        definition.actions.push(DefinitionItem.build(
          operator: "is",
          source: "notification_user",
          value: ["current_user", "SUBJECT", "i\nam\na\n\ngolden\ngod"]
        ))

        trigger.save!

        add_comment_to_ticket("oh yeah")
      end

      describe_with_arturo_enabled(:email_notification_body_with_dir_auto_tags) do
        let(:html_content) { "<p dir=\"ltr\">i<br />am<br />a</p><p dir=\"ltr\">golden<br />god</p>" }

        before { add_comment_to_ticket("oh yeah") }

        it "renders HTML content correctly" do
          body_by_content_type(email, Mime[:html]).sub(/<script.*script>/, "").must_include html_content
        end
      end

      describe_with_arturo_disabled(:email_notification_body_with_dir_auto_tags) do
        let(:html_content) { "<p>i<br />am<br />a</p><p>golden<br />god</p>" }

        before { add_comment_to_ticket("oh yeah") }

        it "renders HTML content correctly" do
          body_by_content_type(email, Mime[:html]).sub(/<script.*script>/, "").must_include html_content
        end
      end

      it "renders text content correctly" do
        body_by_content_type(email, Mime[:text]).must_include "i\nam\na\n\ngolden\ngod"
      end
    end
  end

  describe "Targets" do
    before do
      @target = EmailTarget.create!(
        account: accounts(:minimum),
        title: "whoa there, son",
        is_active: true,
        email: "foo@example.com",
        subject: "{{ticket.title}}"
      )

      @definition = Definition.new
      @definition.conditions_all.push(DefinitionItem.build(
        operator: "less_than",
        source: "status_id",
        value: ["4"],
        first_value: "4"
      ))
    end

    describe "containing dynamic content" do
      before do
        @definition.actions.push(DefinitionItem.build(
          operator: "is",
          source: "notification_target",
          value: [@target.id.to_s, "<div>\n\n{{dc.html}}"]
        ))

        @trigger = Trigger.new(
          title: "DC",
          definition: @definition,
          account: accounts(:minimum),
          owner: accounts(:minimum)
        )
        @trigger.save!

        add_comment_to_ticket("{{dc.html}}")

        assert @target.valid? && @target.is_active

        @email = ActionMailer::Base.deliveries.last
      end

      it "renders HTML content correctly" do
        body_by_content_type(@email, Mime[:html]).must_include "<p></p><div><p>&lt;div&gt;yay&lt;/div&gt;</p></div>"
      end

      it "renders text content correctly" do
        body_by_content_type(@email, Mime[:text]).must_include "yay"
      end
    end

    describe "containing newlines" do
      before do
        @definition.actions.push(DefinitionItem.build(
          operator: "is",
          source: "notification_target",
          value: [@target.id.to_s, "Ticket ID: {{ticket.id}}\nRequester: {{ticket.requester.name}}\nStatus: {{ticket.status}}\n\n{{ticket.comments_formatted}}"]
        ))

        @trigger = Trigger.new(
          title: "Trigger",
          definition: @definition,
          account: accounts(:minimum),
          owner: accounts(:minimum)
        )
        @trigger.save!

        add_comment_to_ticket("comment")

        assert @target.valid? && @target.is_active

        @email = ActionMailer::Base.deliveries.last
      end

      it "renders HTML content correctly" do
        body_by_content_type(@email, Mime[:html]).must_include "<p>Ticket ID: 1<br />Requester: Author Minimum<br />Status: Open</p>"
      end

      it "renders text content correctly" do
        assert_match %r{^Ticket ID: 1\nRequester: Author Minimum\nStatus: Open\n\n}, body_by_content_type(@email, Mime[:text])
      end
    end
  end

  it "recognizes all allowed fields" do
    ticket = tickets('minimum_2')
    ticket.due_date = Time.now

    ticket.account.settings.has_user_tags = '1'
    ticket.account.settings.save!

    template = "{{ticket.title}} {{ticket.description}} {{ticket.url}} {{ticket.status}} {{ticket.priority}} "\
    "{{ticket.ticket_type}} {{ticket.ticket_form}} {{ticket.via}} {{ticket.cc_names}} {{ticket.ccs}} {{ticket.id}} "\
    "{{ticket.lastest_comment.attachments}} {{ticket.score}} {{ticket.group.name}} {{ticket.brand.name}} "\
    "{{ticket.latest_comment_formatted}} {{ticket.latest_public_comment}} {{ticket.comments_formatted}} "\
    "{{ticket.comments}} {{ticket.public_comments}} {{ticket.account}} {{ticket.assignee.name}} {{ticket.assignee}} " \
    "{{ticket.requester.name}} {{ticket.requester.time_zone}} {{ticket.requester.language}} {{ticket.requester.tags}} " \
    "{{ticket.requester.organization.name}} {{ticket.requester.organization.tags}} {{ticket.due_date}} {{ticket.tags}} " \
    "{{ticket.external_id}} {{ticket.in_business_hours}}" \
    "{{current_user.name}} {{current_user.organization.is_shared}} {{current_user.group.name}} "\
    "{{current_user.notes}} {{current_user.details}} {{current_user.signature}}"

    result = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true)

    if result.index('}') || result.index('Liquid error') || result.index("<")
      fail("Bad template result:\n#{result}")
    end

    result += Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true, ENGLISH_BY_ZENDESK, 'text/html')

    refute_includes result, '}'
    refute_includes result, 'Liquid error'
  end

  it "provides a verbatim_description method" do
    ticket = tickets('minimum_5')
    ticket.stubs(:description).returns("hello <b>there</b> html")
    template = "{{ticket.verbatim_description}}"

    result = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true)
    assert_equal ticket.description, result

    result = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true, ENGLISH_BY_ZENDESK, 'text/html')
    assert_equal "hello &lt;b&gt;there&lt;/b&gt; html", result
  end

  it "ensures proper comment visibility" do
    value    = 'This is a private comment'
    ticket   = tickets('minimum_1')
    template = "{{ticket.comments}} xx1 {{ticket.comments_formatted}} xx2 {{ticket.latest_comment}} xx3 {{ticket.latest_comment_formatted}}"

    ticket.add_comment(body: value, is_public: false)
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    ticket.reload

    # Agents can see private comments
    result = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true)
    parts  = result.strip.split(value)

    assert_equal 4, parts.length

    # End users can't see private comments
    result = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, false)
    assert_equal 1, result.split(value).length
  end

  it "shows comments from last to first using created_at instead of id" do
    value    = "last_comment"
    ticket   = tickets('minimum_1')
    template = "{{ticket.comments}}"

    ticket.add_comment(body: value, is_public: false)
    # manipulate created_at to check it sorts on that rather than id
    ticket.comment.created_at = ticket.comments.pluck(:created_at).min - 1.day
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    ticket.reload

    result = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true)
    assert result.ends_with?(value)
  end

  it "is able to render to text/html" do
    ticket   = tickets('minimum_2')
    template = '{{ticket.group.name}} {{ticket.requester.name}}'
    result   = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true, ENGLISH_BY_ZENDESK, "text/html")

    assert result.strip.length > 5

    if result.index('}') || result.index('Liquid error')
      fail("Bad template result:\n#{result}")
    end

    refute_includes result, '}'
    refute_includes result, 'Liquid error'
  end

  it "has a working satisfaction drop" do
    ticket = tickets(:minimum_4)
    User.any_instance.stubs(:can?).with(:rate_satisfaction, ticket).returns(true)

    template = [
      :rating_section, :rating_url,
      :positive_rating_url, :negative_rating_url
    ].map { |method| "{{ satisfaction.#{method} }}" }.join(' ')

    result = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, false)

    assert_match(%r{/requests/#{ticket.nice_id}/satisfaction/new}, result)
    assert_includes result, 'How would you rate'
  end

  it "renders {{account.incoming_phone_number_ID}} as incoming phone number of the account" do
    ticket = tickets('minimum_1')

    FactoryBot.create(:voice_sub_account, account: accounts(:minimum))
    number = FactoryBot.create(:incoming_phone_number, account: accounts(:minimum), number: '+18474476588')
    ticket.reload
    template = "{{account.incoming_phone_number_#{number.id + 1}}}"
    result   = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true, ENGLISH_BY_ZENDESK, "text/html")
    assert_equal '', result
    template = "{{account.incoming_phone_number_#{number.id}}}"
    result   = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true, ENGLISH_BY_ZENDESK, "text/html")
    assert_equal '+1 (847) 447-6588', result
  end

  it "shows latest voice comment" do
    ticket   = tickets('minimum_1')
    template = "{{ticket.latest_comment}}"

    call = mock('Call')
    call.expects(:account_id).returns(accounts(:minimum).id)
    call.expects(:voice_comment_data).returns(
      recording_url: 'http://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/RE5d4706b2a7ce9c6085119352bc3f96c',
      transcription_text: "foo: hi\nbar: bye\n",
      transcription_status: "completed",
      answered_by_id: users(:minimum_agent).id,
      call_id: nil,
      from: '+14155556666',
      to: '+14155556666',
      started_at: 'June 04, 2014 05:10:27 AM'
    )
    ticket.add_voice_comment(
      call,
      author_id: users(:minimum_agent).id,
      body: "Call recording",
      via_id: ViaType.VOICEMAIL,
      is_public: true
    )
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    ticket.reload

    # Agents can see latest voice comment
    result = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, true)
    result.must_include "Call from: +1 (415) 555-6666"

    # End users can see latest voice comment
    result = Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, false)
    result.must_include "Call from: +1 (415) 555-6666"
  end

  describe 'Answer Bot' do
    let(:fetched_articles) do
      [
        { title: "\f Winter is coming. \t ",
          body: 'lorem ipsum <a href="/hc/en-us/lorem-ipsum">here</a>  <p></p>',
          html_url: 'http://whatever/123',
          id: '3453249857' },
        { title: "\t The Lannisters \r send their regards.",
          body: 'Amet lorem',
          html_url: 'http://whatever/123',
          id: '4643534535' }
      ]
    end

    before do
      ticket_deflection = ticket_deflections(:minimum_ticket_deflection)
      ticket_deflection.ticket_deflection_articles.destroy_all
      articles = [{'article_id' => 1, 'score' => 0.1, 'locale' => 'en-us'}, {'article_id' => 2, 'score' => 0.2, 'locale' => 'en-us'}]
      deflection_articles = articles.map do |article|
        TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article)
      end
      ticket_deflection.ticket_deflection_articles = deflection_articles
      ticket_deflection.save!
      Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).returns(fetched_articles)
      Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:answer_bot_setting_enabled?).returns(false)
    end

    it "has a working answer_bot drop" do
      Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
      template = "article count: {{ answer_bot.article_count }}"

      result = Zendesk::Liquid::TicketContext.render(template, @ticket, @ticket.requester, false)

      assert_includes result, 'article count: 2'
    end

    it "has a working automatic_answers drop" do
      Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
      template = "article count: {{ automatic_answers.article_count }}"

      result = Zendesk::Liquid::TicketContext.render(template, @ticket, @ticket.requester, false)

      assert_includes result, 'article count: 2'
    end
  end
end
