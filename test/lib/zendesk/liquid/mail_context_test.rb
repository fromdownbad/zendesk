require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'MailContext' do
  fixtures :accounts, :translation_locales

  let(:account) { accounts(:minimum) }
  let(:html_mail_template) do
    Zendesk::OutgoingMail::Variables::SUPPORT_HTML_MAIL_TEMPLATE_V2
  end

  describe ".render" do
    describe "with valid fields" do
      let(:rendered_content) do
        Zendesk::Liquid::MailContext.render(
            template: html_mail_template,
            header: "a header",
            content: "some content",
            footer: "a footer",
            account: account,
            delimiter: "a delimiter",
            footer_link: "a footer_link",
            attributes: "an attribute",
            styles: "a style"
          )
      end

      it "returns rendered template with correct fields" do
        rendered_content.must_include "a header"
        rendered_content.must_include "some content"
        rendered_content.must_include "a footer"
        rendered_content.must_include "a delimiter"
        rendered_content.must_include "a footer_link"
        rendered_content.must_include "an attribute"
        rendered_content.must_include "a style"
        rendered_content.wont_include "simplified-email-footer"
      end

      describe_with_arturo_setting_enabled :no_mail_delimiter do
        it "returns rendered template with no delimiter" do
          rendered_content.must_include "a header"
          rendered_content.must_include "some content"
          rendered_content.must_include "a footer"
          rendered_content.wont_include "a delimiter"
          rendered_content.must_include "a footer_link"
          rendered_content.must_include "an attribute"
          rendered_content.must_include "a style"
          rendered_content.wont_include "simplified-email-footer"
        end
      end

      describe_with_arturo_enabled :email_simplified_threading do
        it "returns rendered template with no delimiter" do
          rendered_content.must_include "a header"
          rendered_content.must_include "some content"
          rendered_content.must_include "a footer"
          rendered_content.must_include "a delimiter"
          rendered_content.must_include "a footer_link"
          rendered_content.must_include "an attribute"
          rendered_content.must_include "a style"
          rendered_content.must_include "simplified-email-footer"
        end
      end

      describe "with no footer_link passed" do
        it "returns rendered templated with correct fields and default localized footer_link" do
          footer_tracking = "?utm_medium=poweredbyzendesk&utm_source=email-notification&utm_campaign=text&company=Company"
          footer_link = I18n.t("txt.email.email_landing_page_url") + footer_tracking

          rendered_content = Zendesk::Liquid::MailContext.render(
            template: html_mail_template,
            header: "a header",
            content: "some content",
            footer: "a footer",
            account: account,
            delimiter: "a delimiter"
          )
          rendered_content.must_include "a header"
          rendered_content.must_include "some content"
          rendered_content.must_include "a footer"
          # Split the account name in case it has spaces
          account.name.split.each { |word| rendered_content.must_include word }
          rendered_content.must_include "a delimiter"
          rendered_content.include?(footer_link)
        end
      end

      describe "with a rtl locale" do
        before do
          I18n.translation_locale.stubs(:rtl?).returns(true)
        end

        it "returns rendered templated with rtl dir attribute" do
          footer_tracking = "?utm_medium=poweredbyzendesk&utm_source=email-notification&utm_campaign=text&company=Company"
          footer_link = I18n.t("txt.email.email_landing_page_url") + footer_tracking

          rendered_content = Zendesk::Liquid::MailContext.render(
            template: html_mail_template,
            header: "a header",
            content: "some content",
            footer: "a footer",
            account: account,
            delimiter: "a delimiter"
          )
          rendered_content.must_include "a header"
          rendered_content.must_include "some content"
          rendered_content.must_include "a footer"
          account.name.split.each { |word| rendered_content.must_include word }
          rendered_content.must_include "a delimiter"
          rendered_content.include?(footer_link)
          rendered_content.must_include "rtl"
          rendered_content.must_include "padding-right"
          rendered_content.must_include "text-align: right"
        end
      end

      describe "when a locale is explicitly passed as an argument" do
        describe "when the locale is RTL" do
          before do
            @locale = TranslationLocale.new
            @locale.stubs(:rtl?).returns(true)
          end

          it "returns rendered template with correct fields" do
            rendered_content =
              Zendesk::Liquid::MailContext.render(
                template: html_mail_template,
                header: "a header",
                content: "some content",
                footer: "a footer",
                account: account,
                delimiter: "a delimiter",
                footer_link: "a footer_link",
                attributes: "an attribute",
                styles: "a style",
                translation_locale: @locale
              )

            assert_equal false, I18n.translation_locale.rtl?
            rendered_content.must_include "rtl"
            rendered_content.must_include "padding-right"
            rendered_content.must_include "text-align: right"
          end
        end

        describe "when the locale is English by Zendesk" do
          it "returns rendered template with correct fields" do
            rendered_content =
              Zendesk::Liquid::MailContext.render(
                template: html_mail_template,
                header: "a header",
                content: "some content",
                footer: "a footer",
                account: account,
                delimiter: "a delimiter",
                footer_link: "a footer_link",
                attributes: "an attribute",
                styles: "a style",
                translation_locale: "en-us-x-1"
              )

            rendered_content.wont_include "padding-right"
            rendered_content.wont_include "text-align: right"
          end
        end
      end
    end
  end

  describe ".translation_locale" do
    before do
      @non_default_locale = translation_locales(:spanish)
      I18n.translation_locale = translation_locales(:english_by_zendesk)
      @context = Zendesk::Liquid::MailContext.new(
        template: html_mail_template,
        header: "a header",
        content: "some content",
        footer: "a footer",
        account: account,
        delimiter: "a delimiter",
        footer_link: "a footer_link",
        attributes: "an attribute",
        styles: "a style",
        translation_locale: locale
      )
    end

    describe "default locale" do
      let(:locale) { nil }

      it "uses default locale if no translation_locale given" do
        context_locale = @context.send(:translation_locale)
        assert_equal context_locale.id, I18n.translation_locale.id
        refute_equal context_locale.id, @non_default_locale.id
      end
    end

    describe "uses non-default locale if one is given" do
      let(:locale) { translation_locales(:spanish) }

      it "uses non-default locale if one is given" do
        context_locale = @context.send(:translation_locale)
        refute_equal context_locale.id, I18n.translation_locale.id
        assert_equal context_locale.id, @non_default_locale.id
      end
    end

    describe_with_arturo_enabled :dc_account_content_cache_default do
      describe "when initializing a dc cache" do
        describe "when no translation_locale is given" do
          let(:locale) { nil }

          it "creates a new default cache" do
            Zendesk::DynamicContent::AccountContent.expects(:cache).with(account, I18n.translation_locale)
            @context.default_dc_cache
          end
        end

        describe "when a translation_locale IS given" do
          let(:locale) { translation_locales(:spanish) }

          it "creates a new cache with the locale" do
            Zendesk::DynamicContent::AccountContent.expects(:cache).with(account, locale)
            @context.default_dc_cache
          end
        end
      end
    end
  end
end
