require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Liquid::CmsDrop do
  fixtures :accounts,
    :translation_locales

  let(:account) { accounts(:minimum) }
  let(:locale)  { translation_locales(:english_by_zendesk) }

  let(:identifier) { 'content' }
  let(:template)   { "{{dc.#{identifier}}}" }
  let(:cms_value)  { "Test\n**New Line**" }

  let(:content_type)       { Mime[:text] }
  let(:dc_cache)           { {} }
  let(:render_dc_markdown) { false }

  let(:context_class) do
    Class.new(Zendesk::Liquid::Context) do
      def fields
        {'dc' => Zendesk::Liquid::CmsDrop.new(self)}
      end
    end
  end

  let(:context) do
    context_class.new(
      template,
      locale,
      render_dc_markdown: render_dc_markdown
    ).tap do |liquid_context|
      liquid_context.account  = account
      liquid_context.format   = content_type
      liquid_context.dc_cache = dc_cache
    end
  end

  let(:cms_drop) { Zendesk::Liquid::CmsDrop.new(context) }

  before do
    Cms::Text.create!(
      name:                identifier,
      account:             account,
      fallback_attributes: {
        is_fallback:           true,
        nested:                true,
        value:                 cms_value,
        translation_locale_id: locale.id
      }
    )
  end

  describe 'when rendering a template' do
    it 'renders the dynamic content' do
      assert_equal cms_value, context.render
    end

    describe 'and the content type is HTML' do
      let(:content_type) { Mime[:html] }

      it 'renders the dynamic content as HTML' do
        assert_equal '<p>Test<br />**New Line**</p>', context.render
      end
    end

    describe 'and the specified dynamic content identifier is cached' do
      let(:dc_cache) { {identifier => 'Cached dynamic content'} }

      it 'renders the cached dynamic content' do
        assert_equal(
          'Cached dynamic content',
          Liquid::Template.parse(template).render('dc' => cms_drop)
        )
      end

      describe 'multi-level rendering' do
        let(:template) { "{{dc.#{identifier}_3}}" }
        let(:dc_cache) do
          {
            identifier => 'real content value',
            "#{identifier}_2" => "{{dc.#{identifier}}}",
            "#{identifier}_3" => "{{dc.#{identifier}_2}}"
          }
        end

        it 'renders all levels of placeholders' do
          assert_equal(
            'real content value',
            Liquid::Template.parse(template).render('dc' => cms_drop)
          )
        end
      end

      describe 'and the content type is HTML' do
        let(:content_type) { Mime[:html] }

        it 'renders the cached dynamic content' do
          assert_equal(
            'Cached dynamic content',
            Liquid::Template.parse(template).render('dc' => cms_drop)
          )
        end
      end
    end

    describe 'when rendering markdown' do
      let(:render_dc_markdown) { true }

      let(:cms_value) { "# Header\nline 1\n\nline 2\nline 3\n**Strong**" }

      it 'does not include any newlines' do
        refute_includes context.render, "\n"
      end

      it 'renders the markdown as HTML' do
        assert_equal(
          '<h1 dir="auto">Header</h1><br />line 1<br /><br />line 2<br />line 3<br />' \
            '<strong>Strong</strong>',
          context.render
        )
      end

      describe 'when the content starts with normal text' do
        let(:cms_value) { "test\n**Strong**" }

        it 'does not include a leading linebreak' do
          assert_equal 'test<br /><strong>Strong</strong>', context.render
        end
      end

      describe 'when the content ends with normal text' do
        let(:cms_value) { "**Strong**\ntest" }

        it 'does not include a trailing linebreak' do
          assert_equal '<strong>Strong</strong><br />test', context.render
        end
      end

      describe 'when the content contains nested references' do
        let(:cms_value) { "**Testing**\n{{dc.content}}" }

        it 'returns the nested markdown to a particular level' do
          assert_equal(
            '<strong>Testing</strong><br /><strong>Testing</strong><br />' \
              '<strong>Testing</strong><br /><strong>Testing</strong><br />' \
              '<strong>Testing</strong>',
            context.render
          )
        end
      end
    end

    describe 'and the template does not match any identifiers' do
      let(:template) { '{{dc.lack_of_content}}' }

      it 'returns an empty string' do
        assert_equal '', context.render
      end
    end

    describe 'and the template does not include an identifier' do
      let(:template) { '{{dc}}' }

      it 'returns an empty string' do
        assert_equal '', context.render
      end
    end
  end
end
