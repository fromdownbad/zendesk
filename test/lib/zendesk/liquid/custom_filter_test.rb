require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::Liquid::CustomFilter do
  fixtures :tickets, :users, :accounts

  describe "#url_encode" do
    it "uses Rack::Utils.escape to do url encoding" do
      Rack::Utils.expects(:escape).with("blåbærgrød").returns("blueberrything")
      assert_equal("blueberrything", Liquid::Template.parse("{{'blåbærgrød' | url_encode}}").render)
    end
  end

  describe '#split' do
    it 'splits the input' do
      assert_equal 'foo,bar', Liquid::Template.parse("{{'foo bar' | split:' ' | join:',' }}").render
    end
  end

  describe '#json' do
    describe "when the input is a string" do
      it "properly escape quotes" do
        assert_equal "\\\"GOOD\\\"", Liquid::Template.parse(%({{'"GOOD"' | json}})).render
      end
    end

    describe "when the input is a comment" do
      before do
        @ticket = tickets('minimum_1')
        @user = users(:minimum_agent)
        @ticket.add_comment(body: "\" \n weird json {{ticket.description}} stuff \n \"", author: @user)
        @ticket.will_be_saved_by(@user)
        @ticket.save!

        @ticket.reload
      end

      it "returns the properly escaped the comment" do
        rendered_comment = Zendesk::Liquid::TicketContext.render(%({{ticket.latest_comment | json}}), @ticket, @user, true)
        assert_equal "\\\" \\n weird json ----------------------------------------------\\n\\nAgent Minimum, Jan 1, 2007, 5:32\\n\\nminimum ticket 1 stuff \\n \\\"",
          rendered_comment
      end
    end
  end
end
