require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Liquid::PostProcessing::DocumentCleanup do
  describe "#applies?" do
    def applies?(*args)
      Zendesk::Liquid::PostProcessing::DocumentCleanup.new(*args).send(:applies?)
    end

    it "is false for nested contexts" do
      context = stub(nested?: true)
      context.stubs(:content_type?).returns(true)
      refute applies?(context, "")
    end

    it "is false for text/plain content types" do
      context = stub
      context.expects(:content_type?).with(Mime[:html]).returns(false)
      refute applies?(context, "")
    end

    it "is true for non-nested, text/html contexts" do
      context = stub(nested?: false)
      context.expects(:content_type?).with(Mime[:html]).returns(true)
      assert applies?(context, "")
    end
  end

  describe "#apply" do
    it "emits valid XHTML fragments" do
      processor = Zendesk::Liquid::PostProcessing::DocumentCleanup.new(stub, "<div><p>foo<div>bar")
      processor.expects(:applies?).returns(true)
      assert_equal "<div><p>foo</p><div>bar</div></div>", processor.apply
    end
  end
end
