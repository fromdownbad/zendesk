require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Liquid::PostProcessing::SimpleFormatter do
  def apply_to(text)
    context = stub
    @processor = Zendesk::Liquid::PostProcessing::SimpleFormatter.new(context, text)
    @processor.stubs(:applies?).returns(true)
    @processor.apply
  end

  describe "#applies?" do
    def applies?(*args)
      Zendesk::Liquid::PostProcessing::SimpleFormatter.new(*args).send(:applies?)
    end

    describe "rich comments" do
      let(:context) { stub(rich_comment?: true) }

      it "returns true if there are new lines" do
        assert applies?(context, "foo\nbar")
      end

      it "returns false if there are no new lines" do
        refute applies?(context, "foo")
      end

      it "returns true even when not Mime[:html] if there are newlines" do
        context.stubs(content_type?: false)
        assert applies?(context, "foo\nbar")
      end

      it "returns true even when nested if there are newlines" do
        context.stubs(content_type?: true, nested?: true)
        assert applies?(context, "foo\nbar")
      end
    end

    describe "non-rich comments" do
      let(:context) { stub(rich_comment?: false) }

      it "returns false when not Mime[:html]" do
        context.expects(:content_type?).with(Mime[:html]).returns(false)
        refute applies?(context, "foo")
      end

      it "returns false with nested contexts" do
        context.stubs(content_type?: true, nested?: true)
        refute applies?(context, "foo")
      end

      it "returns false when the output does not contain newlines" do
        context.stubs(content_type?: true, nested?: false)
        refute applies?(context, "foo")
      end

      it "returns true for Mime[:html], unnested, and newline-containing contexts" do
        context.stubs(content_type?: true, nested?: false)
        assert applies?(context, "foo\nbar")
      end
    end
  end

  describe "#apply" do
    describe "when #applies? is false" do
      before do
        context = stub
        @processor = Zendesk::Liquid::PostProcessing::SimpleFormatter.new(context, "foo\n\nbar")
        @processor.stubs(:applies?).returns(false)
      end

      it "returns the raw output" do
        assert_equal "foo\n\nbar", @processor.apply
      end
    end

    describe "when #applies? is true" do
      it "keeps html" do
        assert_equal "<p>foo</p><p>bar<div>baz</div></p>", apply_to("foo\n\nbar<div>baz</div>")
      end

      it "keeps html entities" do
        assert_equal "<p>&nbsp;foo</p><p>bar<br />baz</p>", apply_to("&nbsp;foo\n\nbar\nbaz")
      end

      it "does not sanitize by default" do
        html = '<td valign="top"></td>'
        assert_equal "<p>#{html}</p>", apply_to(html)
      end

      it "formats non-html" do
        assert_equal "<p>foo</p><p>bar</p>", apply_to("foo\n\nbar")
      end

      it "does not make non html output html_safe" do
        result = apply_to("foo")
        assert_equal "<p>foo</p>", result
        refute result.html_safe?
      end

      it "does not make html output html_safe" do
        result = apply_to("<span>foo</span>")
        assert_equal "<p><span>foo</span></p>", result
        refute result.html_safe?
      end

      it "does not keep html html_safe" do
        result = apply_to("<span>foo</span>".html_safe)
        assert_equal "<p><span>foo</span></p>", result
        refute result.html_safe?
      end

      it "does not preserve newlines in non-markdown" do
        output = apply_to("<code class=\"bar\">baz\n123\n456\n789</code>")
        assert_includes output, "<code class=\"bar\">baz<br />123<br />456<br />789</code>"
      end

      it "does not break on headers" do
        content = "<!-- Z_START_MARKDOWN --><h1 foo=\"bar\">baz\n123</h1><!-- Z_TERM_MARKDOWN -->"
        output = apply_to(content)
        assert_equal "<p></p>#{content}", output
      end

      it "does not simpleformat markdown blocks" do
        content = "<!-- Z_START_MARKDOWN -->Hello<!-- Z_TERM_MARKDOWN -->"
        output = apply_to(content)
        assert_equal "<p></p>#{content}", output
      end

      it "preserves newlines in markdown" do
        output = apply_to("<!-- Z_START_MARKDOWN --><code foo=\"bar\">baz\n123\n456\n789</code><!-- Z_TERM_MARKDOWN -->")
        assert_includes output, "<code foo=\"bar\">baz\n123\n456\n789</code>"
      end

      it "preserves newlines in multiline markdown" do
        template =  "<!-- Z_START_MARKDOWN --><code foo=\"bar\">123\n456\n789</code><!-- Z_TERM_MARKDOWN -->"
        template += "some other junk\nyeah"
        template += "<!-- Z_START_MARKDOWN --><code>foo\nbar\nbaz</code><!-- Z_TERM_MARKDOWN -->"
        output = apply_to(template)
        assert_includes output, "<code foo=\"bar\">123\n456\n789</code>"
        assert_includes output, "<code>foo\nbar\nbaz</code>"
        assert_includes output, "<p>some other junk<br />yeah</p>"
      end
    end
  end
end
