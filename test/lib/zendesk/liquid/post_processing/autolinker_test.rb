require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Liquid::PostProcessing::Autolinker do
  describe "#applies?" do
    def applies?(*args)
      Zendesk::Liquid::PostProcessing::Autolinker.new(*args).send(:applies?)
    end

    it "returns true when Mime[:html]" do
      context = stub(skip_auto_link: false)
      context.expects(:content_type?).with(Mime[:html]).returns(true)
      assert applies?(context, "foo")
    end

    it "returns false when Mime[:html]" do
      context = stub(skip_auto_link: false)
      context.expects(:content_type?).with(Mime[:html]).returns(false)
      refute applies?(context, "foo")
    end
  end

  describe "apply" do
    it "calls auto_link_urls on the output" do
      @processor = Zendesk::Liquid::PostProcessing::Autolinker.new(stub, "http://foo.com")
      @processor.expects(:applies?).returns(true)
      assert_equal '<a href="http://foo.com" rel="noreferrer">http://foo.com</a>', @processor.apply
    end
  end
end
