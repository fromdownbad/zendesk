require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Incidents::Finder do
  fixtures :accounts, :users, :tickets

  describe "#incidents" do
    let(:account) { accounts(:minimum) }
    let(:agent) { users(:minimum_agent) }
    let(:problem) { tickets(:minimum_1) }
    let(:finder) { Zendesk::Incidents::Finder.new(account, agent, problem_nice_id: problem.nice_id) }

    it 'returns the correct scope' do
      expected = problem.incidents
      assert_equal expected.to_sql, finder.incidents.to_sql
    end

    describe "when agent has 'assigned-only' ticket access" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
        agent.permission_set = account.permission_sets.create!(name: "Test")
        agent.permission_set.permissions.ticket_access = 'assigned-only'
        agent.permission_set.save!
        agent.save!
      end

      it "queries for incidents the agent is permitted to see" do
        agents_incidents = problem.incidents.for_user(agent)
        everyones_incidents = problem.incidents
        refute_equal agents_incidents.to_sql, everyones_incidents.to_sql
        assert_equal agents_incidents.to_sql, finder.incidents.to_sql
      end
    end
  end
end
