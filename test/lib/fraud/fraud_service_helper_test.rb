require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'FraudServiceHelper' do
  include Fraud::FraudServiceHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:shell_account) { accounts(:shell_account_without_support) }
  let(:z1_ticket_payload) do
    {
      data: {
        account_id: account.id,
        subdomain: account.subdomain,
        ticket_action: 'tag',
        tag_name: 'spam-cleanup1202',
        action_count: 1,
        z1_ticket_type: 'spam_cleanup'
      },
      timestamp: Time.now
    }
  end

  before do
    Timecop.freeze(Date.today)
    Rails.env.stubs(:development?).returns(true)
  end

  describe "#send_to_fraud_service" do
    before do
      @current_account = account
      @account_id = account.id
      @subdomain = account.subdomain
      @owner_email = 'test@example.com'
      @time_zone = account.time_zone
      @account_name = account.name
      @raw_response = nil
      @source_event = Fraud::SourceEvent::TRIAL_SIGNUP
    end

    describe 'when it is a ok to send' do
      before do
        FraudScoreJob.any_instance.stubs(:send_to_fraud_service?).returns(true)
      end

      it 'posts payload activity' do
        Fraud::FraudServiceClient.expects(:post_activity).
          with(account_payload, '/api/services/private/activity', false)
        send_to_fraud_service
      end

      describe 'when it is a shell account' do
        before do
          @current_account = shell_account
          @account_id = shell_account.id
          @subdomain = shell_account.subdomain
          @account_name = shell_account.name
          @current_account.trial_extras << TrialExtra.from_hash("client_ip" => "10.21.19.192", "product_trial_selected" => "acme_app")
          @source_event = Fraud::SourceEvent::SHELL_CREATED
        end

        it 'can post payload activity' do
          Fraud::FraudServiceClient.expects(:post_activity).
            with(account_payload, '/api/services/private/activity', false)
          send_to_fraud_service
        end

        it 'uses the client IP for the payload' do
          assert_equal(account_payload[:data][:created_from_ip_address], "10.21.19.192")
        end
      end
    end

    describe 'when it is above fraud service request limit' do
      let(:rate_limit) { Prop.configurations[:fraud_service_requests][:threshold] }

      before do
        Prop.reset(:fraud_service_requests, account.account.id)
        rate_limit.times { Prop.throttle(:fraud_service_requests, account.account.id) }
      end

      it 'does not post payload activity' do
        Fraud::FraudServiceClient.expects(:post_activity).
          with(account_payload, '/api/services/private/activity').never
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('fraud_service.request.throttled')
        send_to_fraud_service
      end
    end

    describe "for a trial account payload" do
      let(:fraud_service_activity_response) do
        {
            success: true,
            actions: {
                auto_suspend: false
            }
        }
      end

      before do
        @current_account = account
        @current_account.stubs(:created_at).returns(Time.now.to_s)
        @current_account.trial_extras << TrialExtra.from_hash("foo" => "bar")
        @current_account.settings.stubs(:created_from_ip_address).returns("234.128.78.21")
        @source_event = Fraud::SourceEvent::TRIAL_SIGNUP.to_s
        @account_id = account.id
        @subdomain = account.subdomain
        @owner_email = 'test@example.com'
        @time_zone = account.time_zone
        @account_name = account.name
        @raw_response = nil
        FraudScoreJob.any_instance.stubs(:send_to_fraud_service?).returns(true)
      end

      it 'returns the afs payload for new accounts with success' do
        time_now_json = JSON(Time.now.to_json)
        stub_request(:post, "http://orca.zd-dev.com/api/services/private/activity").
          with(
            body: "{\"source_event\":\"trial_signup\",\"data\":{\"account_id\":90538,\"subdomain\":\"minimum\",\"owner_email\":\"test@example.com\",\"time_zone\":\"Copenhagen\",\"account_name\":\"Minimum account\",\"owner_name\":null,\"raw_response\":null,\"source_event\":\"trial_signup\",\"created_at\":\"#{@current_account.created_at}\",\"zd_pod\":null,\"is_trial\":false,\"created_from_ip_address\":\"234.128.78.21\",\"account_update\":false,\"support_risky_status\":false,\"trial_extras\":{\"foo\":\"bar\"},\"minfraud_id\":\"\"},\"timestamp\":\"#{time_now_json}\",\"vendor_review_prohibited\":false}",
            headers: {
                'Accept' => '*/*',
                'Content-Type' => 'application/json',
            }
          ).
          to_return(status: 200, body: fraud_service_activity_response.to_json, headers: {})

        outcome = JSON(send_to_fraud_service)
        assert_equal outcome['actions']['auto_suspend'], false
      end

      it 'uses the created_from_ip_address setting for the payload' do
        assert_equal(account_payload[:data][:created_from_ip_address], "234.128.78.21")
      end
    end

    describe 'when a call to spam_cleanup_job should create an email' do
      let(:fraud_service_create_z1_ticket_with_fraud_service_response) do
        {
            success: true,
            z1_ticket_type: 'spam_cleanup'
        }
      end

      it 'returns the afs payload for #create_z1_ticket_with_fraud_service' do
        stub_request(:post, "http://orca.zd-dev.com/api/services/private/create_z1_ticket").
          with(
            body: z1_ticket_payload,
            headers: {
                'Accept' => '*/*',
                'Content-Type' => 'application/json',
            }
          ).
          to_return(status: 200, body: fraud_service_create_z1_ticket_with_fraud_service_response.to_json, headers: {})

        outcome = JSON(create_z1_ticket_with_fraud_service(z1_ticket_payload).body)
        assert outcome['success']
        assert_equal outcome['z1_ticket_type'], 'spam_cleanup'
      end
    end
  end

  describe "#create_z1_ticket_with_fraud_service" do
    it 'posts email payload activity' do
      Fraud::FraudServiceClient.expects(:post_activity).
        with(z1_ticket_payload, Fraud::FraudServiceHelper::ACCOUNT_FRAUD_SERVICE_CREATE_Z1_TICKET_URL)
      create_z1_ticket_with_fraud_service(z1_ticket_payload)
    end
  end
end
