require_relative '../../support/test_helper'

SingleCov.covered!

describe Fraud::FraudAction do
  describe "when accessed using a constant" do
    it "returns the matching action" do
      assert_equal :suspend, Fraud::FraudAction::SUSPEND
      assert_equal :support_risky, Fraud::FraudAction::SUPPORT_RISKY
    end
  end

  describe "when accessed using an index" do
    it "creates a FraudAction object with the matching action" do
      suspend = Fraud::FraudAction[:auto_suspend]
      assert_equal suspend.name, :auto_suspend

      takeover = Fraud::FraudAction[:takeover]
      assert_equal takeover.name, :takeover
    end

    it "returns nil if no match" do
      assert_nil  Fraud::FraudAction[:fake_action]
    end
  end

  describe "trial only actions" do
    it "whitelist does not return trial only" do
      whitelist = Fraud::FraudAction[:whitelist]
      refute whitelist.trial_only
    end

    it "unsuspend does not return trial only" do
      unsuspend = Fraud::FraudAction[:unsuspend]
      refute unsuspend.trial_only
    end

    it "takeover does not return trial only" do
      takeover = Fraud::FraudAction[:takeover]
      refute takeover.trial_only
    end

    it "suspend returns trial only" do
      suspend = Fraud::FraudAction[:auto_suspend]
      assert suspend.trial_only
    end
  end
end
