require_relative '../../support/test_helper'

SingleCov.covered!

describe Fraud::SourceEvent do
  describe "when accessed using a constant" do
    it "returns the matching event" do
      assert_equal :trial_signup, Fraud::SourceEvent::TRIAL_SIGNUP
      assert_equal :purchase_with_new_cc, Fraud::SourceEvent::PURCHASE_WITH_NEW_CC
    end
  end

  describe "when accessed using an index" do
    it "returns the matching event" do
      assert_equal :trial_signup, Fraud::SourceEvent[:trial_signup].name
      assert_equal :purchase_with_new_cc, Fraud::SourceEvent[:purchase_with_new_cc].name
    end

    it "raises KeyError if no matching event" do
      fake_event = 'fake_event'
      expected_error = "key not found: \"#{fake_event}\""
      assert_raises KeyError, expected_error do
        Fraud::SourceEvent[fake_event]
      end
    end
  end

  describe "should include fraud score job source events" do
    it 'and not raise any KeyErrors' do
      Fraud::SourceEvent::ACCOUNT_UPDATE_EVENTS.each do |source_event|
        refute_nil Fraud::SourceEvent[source_event]
        refute_nil Fraud::SourceEvent["sc_account_#{source_event}"]
      end
    end
  end

  describe "is_manual_action events" do
    it "trial signup does not return is_manual_action" do
      trial_signup = Fraud::SourceEvent[:trial_signup]
      refute trial_signup.is_manual_action
    end

    it "sc_account_suspended_account returns manual action" do
      sc_account_suspended_account = Fraud::SourceEvent[:sc_account_suspended_account]
      assert sc_account_suspended_account.is_manual_action
    end

    it "sc_button returns manual action" do
      sc_button = Fraud::SourceEvent[:sc_button]
      assert sc_button.is_manual_action
    end
  end
end
