require_relative '../../../support/test_helper'

SingleCov.covered!

describe Fraud::Normalizer::FraudEmailNormalizer do
  include Fraud::Normalizer::FraudEmailNormalizer

  let(:period_email) { 'elliot......@gmail.com' }
  let(:spammy_email) { 'elliot......+123@gmail.com' }
  let(:reduced_email) { 'elliot@gmail.com' }
  let(:no_at_email) { 'ellio........tgmail' }
  let(:no_at_email_result) { 'elliotgmail@' }
  let(:multiple_at_email) { 'elliotgmail@@@@gmail.com' }
  let(:multiple_at_email_result) { 'elliotgmail@' }

  it 'removes periods' do
    assert_equal normalize_email_address(period_email), reduced_email
  end

  it 'removes periods and everything after the + sign' do
    assert_equal normalize_email_address(spammy_email), reduced_email
  end

  it 'does not crash on missing @ sign' do
    assert_equal normalize_email_address(no_at_email), no_at_email_result
  end

  it 'does not crash on multiple @ sign' do
    assert_equal normalize_email_address(multiple_at_email), multiple_at_email_result
  end
end
