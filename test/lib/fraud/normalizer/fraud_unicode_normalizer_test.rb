require_relative '../../../support/test_helper'

SingleCov.covered!

describe Fraud::Normalizer::FraudUnicodeNormalizer do
  include Fraud::Normalizer::FraudUnicodeNormalizer

  let(:circle_number_string) { '⑪⑦⑦⑦④②⑦⑦⑦⑪' }
  let(:parenthesized_number_string) { '⑻⒇⒇⒇⒇⑻' }
  let(:full_stop_number_string) { '⒏⒐⒑' }
  let(:kanji_number_string) { '可　以来➢.单结算 abc= 210④④' }
  let(:kanji_number_string_2) { '零一二三四五六七八九' }
  let(:thai_accents) { '询ึี17ั3ื3905035' }
  let(:full_width_number_string) { '８６７５３０９' }

  let(:reduced_circle_number_string) { '117774277711' }
  let(:reduced_parenthesized_number_string) { '8202020208' }
  let(:reduced_full_stop_number_string) { '8910' }
  let(:reduced_kanji_number_string) { '可以来单结算21044' }
  let(:reduced_kanji_number_string_2) { '0123456789' }
  let(:reduced_thai_accents) { '询1733905035' }
  let(:reduced_full_width_number_string) { '8675309' }

  it 'normalizes circle numbers' do
    assert_equal normalize_unicode(circle_number_string), reduced_circle_number_string
  end

  it 'normalizes parenthesized numbers' do
    assert_equal normalize_unicode(parenthesized_number_string), reduced_parenthesized_number_string
  end

  it 'normalizes full stop numbers' do
    assert_equal normalize_unicode(full_stop_number_string), reduced_full_stop_number_string
  end

  it 'normalizes circle numbers' do
    assert_equal normalize_unicode(circle_number_string), reduced_circle_number_string
  end

  it 'normalizes kanji' do
    assert_equal normalize_unicode(kanji_number_string_2), reduced_kanji_number_string_2
  end

  it 'keeps all Kanji, Numbers, and spaces and removes seperators' do
    assert_equal normalize_unicode(kanji_number_string), reduced_kanji_number_string
  end

  it 'normalizes thai accents' do
    assert_equal normalize_unicode(thai_accents), reduced_thai_accents
  end

  it 'normalizes full width digits' do
    assert_equal normalize_unicode(full_width_number_string), reduced_full_width_number_string
  end
end
