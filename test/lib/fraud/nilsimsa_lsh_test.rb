require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Fraud::NilsimsaLSH do
  let (:text) { 'I would like a nilsimsa digest, please' }
  let (:text_field) { 'description' }
  let (:nilsimsa_digest) { Nilsimsa.new(text).digest }

  let (:alt_text) { 'This is another nilsimsa digest' }
  let (:alt_text_field) { 'subject' }
  let (:alt_nilsimsa_digest) { Nilsimsa.new(alt_text).digest }

  describe '#initialize' do
    describe 'when Nilsimsa processes successfully' do
      it 'logs the relevant nilsimsa information' do
        Rails.logger.expects(:info).with("nilsimsa_lsh: field #{text_field} with #{text.first(100)} has hash: #{nilsimsa_digest}").at_most_once
        Rails.logger.expects(:append_attributes).with(
          nilsimsa: {
            description: {
              digest: nilsimsa_digest,
              source_text: text.first(100)
            }
          }
        )
        Fraud::NilsimsaLSH.new(text, text_field)
      end

      describe 'when Nilsimsa is run twice on different fields' do
        it 'will append the new text field to the Nilsimsa attribute' do
          Rails.logger.expects(:info).once
          Rails.logger.expects(:append_attributes).with(
            nilsimsa: {
              description: {
                digest: nilsimsa_digest,
                source_text: text.first(100)
              }
            }
          )
          Fraud::NilsimsaLSH.new(text, text_field)

          Rails.logger.expects(:info).once
          # attributes will return a nested hash with string keys
          Rails.logger.expects(:append_attributes).with(
            nilsimsa: {
              'description' => {
                'digest' => nilsimsa_digest,
                'source_text' => text.first(100)
              },
              'subject' => {
                'digest' => alt_nilsimsa_digest,
                'source_text' => alt_text.first(100)
              }
            }
          )
          Fraud::NilsimsaLSH.new(alt_text, alt_text_field)
        end
      end

      describe 'when log_attributes is set to false' do
        it 'does not append_attributes' do
          Rails.logger.expects(:info).with("nilsimsa_lsh: field #{text_field} with #{text.first(100)} has hash: #{nilsimsa_digest}").at_most_once
          Rails.logger.expects(:append_attributes).never
          Fraud::NilsimsaLSH.new(text, text_field, false)
        end
      end
    end

    describe 'when there is an error with Nilsimsa' do
      it 'rescues the error and logs it' do
        Rails.logger.expects(:info).with(regexp_matches(/nilsimsa_lsh_error/)).at_least_once
        # pass in the wrong type of object
        Fraud::NilsimsaLSH.new([1])
      end
    end
  end

  describe '#neighbor_match?' do
    before do
      @nilsimsa_lsh = Fraud::NilsimsaLSH.new(text, text_field, true, true)
    end

    describe 'when there is a match' do
      before do
        Nilsimsa.any_instance.stubs(:bit_difference).returns(42)
      end

      describe 'when log_score is true' do
        before do
          @nilsimsa_lsh.instance_variable_set(:@log_score, true)
        end
        it 'returns true' do
          Rails.logger.expects(:append_attributes).with(
              nilsimsa: {
                  'description' => {
                      'digest' => 'ICAQQJOBKACUYAAREAEEACDQAIAQEKYYQSCNAJQBAZIJ6ACARRGA====', 'source_text' => 'I would like a nilsimsa digest, please'
                  },
                  'neighbor_match' => '76JVX363PKGH5MPREZFPVK57S4J2V7KJW2XPU63HKZZJ3WSOZ3VA====',
                  'neighbor_match_score' => 42
              }
            )
          Rails.logger.expects(:info).with(regexp_matches(/nilsimsa_neighbor_match/)).once
          assert @nilsimsa_lsh.neighbor_match?(alt_nilsimsa_digest)
        end
      end

      describe 'when log_score is false' do
        before do
          @nilsimsa_lsh.instance_variable_set(:@log_score, false)
        end
        it 'returns true' do
          Rails.logger.expects(:append_attributes).with(
              nilsimsa: {
                  'description' => {
                      'digest' => 'ICAQQJOBKACUYAAREAEEACDQAIAQEKYYQSCNAJQBAZIJ6ACARRGA====', 'source_text' => 'I would like a nilsimsa digest, please'
                  },
                  'neighbor_match' => '76JVX363PKGH5MPREZFPVK57S4J2V7KJW2XPU63HKZZJ3WSOZ3VA===='
              }
            )
          Rails.logger.expects(:info).with(regexp_matches(/nilsimsa_neighbor_match/)).once
          assert @nilsimsa_lsh.neighbor_match?(alt_nilsimsa_digest)
        end
      end
    end

    describe 'when there is not a match' do
      before do
        Nilsimsa.any_instance.stubs(:bit_difference).returns(900)
      end

      describe 'when log_score is true' do
        before do
          @nilsimsa_lsh.instance_variable_set(:@log_score, true)
        end

        it 'returns false' do
          Rails.logger.expects(:append_attributes).never
          Rails.logger.expects(:info).with(regexp_matches(/nilsimsa_neighbor_match/)).never
          refute @nilsimsa_lsh.neighbor_match?(alt_nilsimsa_digest)
        end
      end

      describe 'when log_score is false' do
        before do
          @nilsimsa_lsh.instance_variable_set(:@log_score, false)
        end

        it 'returns false' do
          Rails.logger.expects(:append_attributes).never
          Rails.logger.expects(:info).with(regexp_matches(/nilsimsa_neighbor_match/)).never
          refute @nilsimsa_lsh.neighbor_match?(alt_nilsimsa_digest)
        end
      end
    end

    describe 'when there is some exception' do
      before do
        Nilsimsa.any_instance.stubs(:bit_difference).raises(StandardError)
      end

      it 'returns false' do
        Rails.logger.expects(:info).with(regexp_matches(/nilsimsa_neighbor_error/)).once
        refute @nilsimsa_lsh.neighbor_match?(alt_nilsimsa_digest)
      end
    end
  end
end
