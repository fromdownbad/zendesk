require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 2

describe Fraud::SpamDetector do
  include ArturoTestHelper
  include Fraud::Normalizer::FraudUnicodeNormalizer

  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_end_user) }
  let(:user_email_domain) { Zendesk::Mail::Address.new(address: user.email).domain }
  let(:requester) { users(:minimum_end_user) }
  let(:spammy_string) { '비트 ┗┗홈피:【VS-11.COM】추천인코드 : one7◐' }
  let(:spammy_url_string) { '◆注冊送菜金28园,无套路！转发再宋43园，渞洊百分百，洊10宋18，\r\n\r\n洊100宋100，最高宋您1288园，AG真人洊宋10％，10年品牌，大额无忧.\r\n\r\n提款5秒到账.VIP专属網址：www.qifa608.com 美女微信：WKYEYV\r\n\r\n【八大佬虎雞、真人視訊、捕魚、体育、棋牌】多种玩法.期待您的到来~◇\n\nPhone number: 3018433256\nItem Number: #13.31.1' }
  let(:spammy_regex) { '12345点com is the best website ever' }
  let(:spammy_targeted_domain_string) { '123456789 is the best website ever' }
  let(:spammy_kanji_string) { '◆注冊金28最 高宋您 园园⑦⑦⑦园' }
  let(:spammy_normalized_string_regex) { '注注注注注注◆◆◆最111111◆' }
  let(:spammy_targeted_domain_regex) { '66612a.com is the best website ever' }
  let(:statsd_client) { mock('statsd_client') }
  let(:suspended_action_tag) { ['action:suspended_ticket'] }
  let(:rejected_action_tag) { ['action:rejected_ticket'] }
  let(:comment) { { public: false, value: 'harmless comment' } }

  let(:system_with_bad_ip) do
    {
      'message_id' => 'a',
      'client' => 'b',
      'ip_address' => '151.248.122.191',
      'location' => 'd'
    }
  end

  let (:system_with_safe_ip) { {'ip_address' => '10.210.80.219'} }

  before do
    # Stub the account `domain_whitelist` because `minimum_user`'s email's
    # domain is on this whitelist in the fixtures
    account.stubs(:domain_whitelist).returns('some.com domains.net here.edu')
    # Stub the account `organization_whitelist` because `minimum_user`'s
    # email's domain is on this whitelist in the fixtures
    account.stubs(:organization_whitelist).returns('other.com ones.net there.edu')
    account.stubs(:allows?).returns(true)

    # Prevents unrelated failures with this field being nil
    Zendesk::Tickets::Initializer.any_instance.stubs(:editable_field?).returns(true)

    Timecop.freeze(Date.today)
  end

  before do
    Zendesk::StatsD::Client.stubs(:new).
      with(namespace: 'spam_detector').
      returns(statsd_client)
    statsd_client.stubs(:histogram)
    Rails.logger.stubs(:info)
    Rails.logger.stubs(:append_attributes)
  end

  describe 'benchmarking' do
    before do
      @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
          via_id: ViaType.WEB_FORM,
      }).ticket
    end
    it 'still sends benchmarking data to statsd' do
      statsd_client.expects(:histogram).with('full_run.duration', kind_of(Float), tags: ["source:Web form"]).once
      Fraud::SpamDetector.new(@ticket)
    end
  end

  describe 'whether a ticket should be evaluated for spam indicators' do
    describe 'when a ticket is an online chat ticket' do
      before do
        @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
            submitter: user,
            subject: 'zendesk ticket',
            via_id: ViaType.CHAT,
            description: 'zendesk description',
            tags: Fraud::SpamDetector::ONLINE_CHAT_TAG,
            comment: { public: false, value: spammy_string }
        }).ticket
        @ticket.stubs(:current_tags).returns Fraud::SpamDetector::ONLINE_CHAT_TAG
        @ticket.stubs(:via_id).returns ViaType.CHAT
      end

      it 'should not process spam indicators' do
        Fraud::SpamDetector.any_instance.expects(:process_spam_indicators).never
        Fraud::SpamDetector.new(@ticket)
      end
    end

    describe 'when a ticket is an offline chat ticket' do
      before do
        @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
            submitter: user,
            subject: 'zendesk ticket',
            via_id: ViaType.CHAT,
            description: 'zendesk description',
            tags: 'zopim_offline_message',
            comment: { public: false, value: spammy_string }
        }).ticket
      end

      it 'should process spam indicators' do
        Fraud::SpamDetector.any_instance.expects(:process_spam_indicators).once
        Fraud::SpamDetector.new(@ticket)
      end
    end

    describe 'when a ticket is an email ticket' do
      before do
        @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
          submitter: user,
          subject: 'zendesk ticket',
          via_id: ViaType.MAIL,
          description: 'zendesk description',
          tags: 'zopim_offline_message',
          comment: { public: false, value: spammy_string }
        }).ticket
        @ticket.stubs(:current_tags).returns nil
      end

      it 'should process spam indicators' do
        Fraud::SpamDetector.any_instance.expects(:process_spam_indicators).once
        Fraud::SpamDetector.new(@ticket)
      end
    end
  end

  describe 'when a ticket may be created in a suspended state' do
    describe 'initialize spammy arturos' do
      before do
        ssb_list = ['longhu007.com', 'something else', 'www.qifa608.com', '【vs-11.com']
        if ssb = Arturo::Feature.find_feature(:spammy_string_blacklist)
          ssb.external_beta_subdomains = ssb_list
          ssb.save!
        else
          Arturo::Feature.create!(
            symbol: :spammy_string_blacklist,
            external_beta_subdomains: ssb_list
          )
        end

        ssb_td_list = ['123456789', 'something else', 'www.qifa608.com', '【vs-11.com']
        if ssb_td = Arturo::Feature.find_feature(:spammy_string_for_targeted_domains_blacklist)
          ssb_td.external_beta_subdomains = ssb_td_list
          ssb_td.save!
        else
          Arturo::Feature.create!(
            symbol: :spammy_string_for_targeted_domains_blacklist,
            external_beta_subdomains: ssb_td_list
          )
        end

        srb_list = ['\\d{5}点C[0O]M', '\\d{6}点C[0O]M', 'q.?\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*']
        if srb = Arturo::Feature.find_feature(:spammy_regex_blacklist)
          srb.external_beta_subdomains = srb_list
          srb.save!
        else
          Arturo::Feature.create!(
            symbol: :spammy_regex_blacklist,
            external_beta_subdomains: srb_list
          )
        end

        srb_td_list = ['66612[a-z]\.com\b', 'q.?\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*\\d\\s*']
        if srb_td = Arturo::Feature.find_feature(:spammy_regex_for_targeted_domains_blacklist)
          srb_td.external_beta_subdomains = srb_td_list
          srb_td.save!
        else
          Arturo::Feature.create!(
            symbol: :spammy_regex_for_targeted_domains_blacklist,
            external_beta_subdomains: srb_td_list
          )
        end

        Arturo.enable_feature!(:spam_detector_blacklist_pattern_check)
      end

      describe 'when a ticket has a string in the #spammy_string_blacklist' do
        let(:spam_pattern_tags) { ['type:blacklisted_string', 'source:Web form', 'matching_pattern:【vs-11.com', 'log_only:false'] }

        before do
          @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
            submitter: user,
            subject: spammy_string,
            description: 'allllo is best dot comа',
            tags: 'hello',
            comment: comment
          }).ticket
          @ticket.audit.metadata[:system].merge!(system_with_bad_ip)
        end

        it 'blocks the ticket' do
          statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
          statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          assert ticket_spam_detector.suspend_ticket?
          assert_equal ['blacklisted_string'], ticket_spam_detector.spam_indicators
        end

        describe 'if blacklisted token is too short' do
          before do
            ssb_list = ['longhu007.com', 'something else', 'www.qifa608.com', '【vs-11.com', 'xyz']
            if ssb = Arturo::Feature.find_feature(:spammy_string_blacklist)
              ssb.external_beta_subdomains = ssb_list
              ssb.save!
            else
              Arturo::Feature.create!(symbol: :spammy_string_blacklist, external_beta_subdomains: ssb_list)
            end
            @ticket.stubs(:subject).returns "hey xyz"
          end

          it 'does not block the ticket' do
            statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags).never
            statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
            statsd_client.expects(:increment).with('ticket.atsd.invalid_pattern', tags: ["pattern:xyz"]).times 3
            ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
            assert_empty ticket_spam_detector.spam_indicators
          end
        end

        describe_with_arturo_disabled :spam_detector_blacklist_pattern_check do
          it 'does not block the ticket' do
            statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
            ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
            assert_empty(ticket_spam_detector.spam_indicators)
          end
        end
      end

      describe 'when a ticket has a requester email in the #spammy_string_blacklist' do
        let(:spam_pattern_tags) { ['type:blacklisted_string', 'source:Web form', 'matching_pattern:elliot@gmail.com', 'log_only:false'] }
        before do
          @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
              submitter: user,
              subject: 'hey whats up its me mario!',
              description: 'allllo is best dot comа',
              tags: 'hello',
              comment: comment
          }).ticket
          @ticket.requester.stubs(:email).returns('e...lliot+123@gmail.com')
          ssb_list = ['elliot@gmail.com']
          if ssb = Arturo::Feature.find_feature(:spammy_string_blacklist)
            ssb.external_beta_subdomains = ssb_list
            ssb.save!
          else
            Arturo::Feature.create!(symbol: :spammy_string_blacklist, external_beta_subdomains: ssb_list)
          end
        end

        describe_with_arturo_enabled :orca_classic_spam_detector_blacklisted_requester_email do
          it 'does block the ticket' do
            statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
            statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
            Rails.logger.expects(:info).with("ATSD_requester_block: {account_id: 90538, original_email: e...lliot+123@gmail.com, sanitized_email: elliot@gmail.com}")
            Rails.logger.expects(:info).with("ATSD_SPAM_DETECTED: {account:90538, type:blacklisted_string, matching_pattern:elliot@gmail.com, matching_field:requester_email_address, log_only:false}")
            ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
            assert ticket_spam_detector.suspend_ticket?
            assert_equal ['blacklisted_string'], ticket_spam_detector.spam_indicators
          end

          describe 'when email does not match blacklist' do
            before do
              @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
                  submitter: user,
                  subject: 'hey whats up its me mario!',
                  description: 'allllo is best dot comа',
                  tags: 'hello',
                  comment: comment
              }).ticket
              @ticket.requester.stubs(:email).returns('elliot@gmail.com')
              ssb_list = ['elliot@123.com']
              if ssb = Arturo::Feature.find_feature(:spammy_string_blacklist)
                ssb.external_beta_subdomains = ssb_list
                ssb.save!
              else
                Arturo::Feature.create!(symbol: :spammy_string_blacklist, external_beta_subdomains: ssb_list)
              end
            end

            it 'does not block the ticket' do
              statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags).never
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert_equal ticket_spam_detector.suspend_ticket?, false
              assert_empty ticket_spam_detector.spam_indicators
            end
          end
        end
      end

      describe 'it has a spammy url string' do
        let(:spam_pattern_tags) { ['type:blacklisted_string', 'source:Web form', 'matching_pattern:www.qifa608.com', 'log_only:false'] }

        before do
          @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
            submitter: user,
            subject: 'test ticket',
            description: spammy_url_string,
            tags: 'hello',
            comment: comment
          }).ticket
        end

        it 'blocks the ticket' do
          statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
          statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          assert ticket_spam_detector.suspend_ticket?
          assert_equal ['blacklisted_string'], ticket_spam_detector.spam_indicators
        end

        describe_with_arturo_disabled :spam_detector_blacklist_pattern_check do
          it 'does not block the ticket' do
            statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
            ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
            assert_empty(ticket_spam_detector.spam_indicators)
          end
        end
      end

      describe 'it has a spammy requester name string' do
        let(:spam_pattern_tags) { ['type:blacklisted_string', 'source:Web form', 'matching_pattern:【vs-11.com', 'log_only:false'] }

        before do
          User.any_instance.stubs(:name).returns(spammy_string)
          @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
            submitter: user,
            subject: 'zendesk ticket',
            description: 'zendesk description',
            tags: 'hello',
          }).ticket
        end

        it 'blocks the ticket' do
          statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
          statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          assert ticket_spam_detector.suspend_ticket?
          assert_equal ['blacklisted_string'], ticket_spam_detector.spam_indicators
        end

        describe_with_arturo_disabled :spam_detector_blacklist_pattern_check do
          it 'does not block the ticket' do
            statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
            ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
            assert_empty(ticket_spam_detector.spam_indicators)
          end
        end
      end

      describe 'when looking for spammy strings in the comments of a ticket' do
        before do
          Arturo.enable_feature! :spam_detector_comments
          @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
            submitter: user,
            subject: 'zendesk ticket',
            via_id: ViaType.WEB_FORM,
            description: 'zendesk description',
            tags: 'hello',
            comment: { public: false, value: spammy_string }
          }).ticket
        end

        describe 'when the comment of a ticket' do
          let(:spam_pattern_tags) { ['type:blacklisted_string', 'source:Chat', 'matching_pattern:【vs-11.com', 'log_only:false'] }

          before { @ticket.stubs(:via_id).returns(ViaType.CHAT) }

          it 'blocks the ticket' do
            statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
            statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
            ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
            assert ticket_spam_detector.suspend_ticket?
            assert_equal ['blacklisted_string'], ticket_spam_detector.spam_indicators
          end

          describe_with_arturo_disabled :spam_detector_comments do
            it 'does not block the ticket' do
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert_empty(ticket_spam_detector.spam_indicators)
            end
          end

          describe_with_arturo_disabled :spam_detector_blacklist_pattern_check do
            it 'does not block the ticket' do
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert_empty(ticket_spam_detector.spam_indicators)
            end
          end
        end
      end

      describe 'it has a spammy regex' do
        let(:spam_pattern_tags) { ['type:blacklisted_regex', 'source:Web form', 'matching_pattern:(?i-mx:\\d{5}点C[0O]M)', 'log_only:false'] }

        before do
          @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
            submitter: user,
            subject: spammy_regex,
            description: 'allllo is best dot comа',
            tags: 'hello',
            comment: comment
          }).ticket
        end

        it 'blocks the ticket' do
          statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
          statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          assert ticket_spam_detector.suspend_ticket?
          assert_equal ['blacklisted_regex'], ticket_spam_detector.spam_indicators
        end

        describe_with_arturo_disabled :spam_detector_blacklist_pattern_check do
          it 'does not block the ticket' do
            statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
            ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
            assert_equal false, ticket_spam_detector.suspend_ticket?
          end
        end
      end

      describe 'tickets requester_email_address has a targeted domain' do
        let(:targeted_email_address) { '12345@qq.com' }

        describe 'the ticket has a blacklisted_string_for_targeted_domains' do
          let(:spam_pattern_tags) { ['type:blacklisted_string_for_targeted_domains', 'source:Web form', 'matching_pattern:123456789', 'log_only:false'] }

          before do
            @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
              submitter: user,
              subject: spammy_targeted_domain_string,
              description: 'allllo is best dot comа',
              tags: 'hello',
              comment: comment
            }).ticket
            @ticket.requester.stubs(:email).returns(targeted_email_address)
          end

          describe_with_arturo_enabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
            it 'blocks the ticket' do
              statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
              Rails.logger.expects(:append_attributes).with(
                  targeted_domains: {
                      ticket_subject: {subdomain: account.subdomain, account_id: account.id, result: true, atsd_reduced_text: "123456789", atsd_original_text: spammy_targeted_domain_string}
                  }
                )
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert ticket_spam_detector.suspend_ticket?
              assert_equal ['blacklisted_string_for_targeted_domains'], ticket_spam_detector.spam_indicators
            end

            describe_with_arturo_disabled :spam_detector_blacklist_pattern_check do
              it 'does not block the ticket' do
                statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
                ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
                assert_equal false, ticket_spam_detector.suspend_ticket?
              end
            end
          end

          describe 'the normalized only ticket has a blacklisted_string_for_targeted_domains' do
            let(:spam_pattern_tags) { ['type:blacklisted_string_for_targeted_domains_reduced', 'source:Web form', 'matching_pattern:园园777园', 'log_only:false'] }

            before do
              ssb_td_list = ['园园777园']
              if ssb_td = Arturo::Feature.find_feature(:spammy_string_for_targeted_domains_blacklist)
                ssb_td.external_beta_subdomains = ssb_td_list
                ssb_td.save!
              else
                Arturo::Feature.create!(
                    symbol: :spammy_string_for_targeted_domains_blacklist,
                    external_beta_subdomains: ssb_td_list
                  )
              end
              @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
                  submitter: user,
                  subject: spammy_kanji_string,
                  description: 'allllo is best dot comа',
                  tags: 'hello',
                  comment: comment
              }).ticket
              @ticket.requester.stubs(:email).returns(targeted_email_address)
            end

            describe_with_arturo_enabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
              describe_with_arturo_enabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
                it 'blocks the ticket' do
                  statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
                  statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
                  ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
                  assert ticket_spam_detector.suspend_ticket?
                  assert_equal ["blacklisted_string_for_targeted_domains_reduced"], ticket_spam_detector.spam_indicators
                end
              end

              describe_with_arturo_disabled :spam_detector_blacklist_pattern_check do
                it 'does not block the ticket' do
                  statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
                  ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
                  assert_equal false, ticket_spam_detector.suspend_ticket?
                end
              end
            end
            describe_with_arturo_disabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
              it 'does not block the ticket' do
                statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
                ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
                assert_equal false, ticket_spam_detector.suspend_ticket?
              end
            end
          end
        end

        describe 'the ticket has a blacklisted_regex_for_targeted_domains' do
          let(:spam_pattern_tags) { ['type:blacklisted_regex_for_targeted_domains', 'source:Web form', 'matching_pattern:(?i-mx:66612[a-z]\\.com\\b)', 'log_only:false'] }

          before do
            @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
              submitter: user,
              subject: spammy_targeted_domain_regex,
              description: 'allllo is best dot comа',
              tags: 'hello',
              comment: comment
            }).ticket
            @ticket.requester.stubs(:email).returns(targeted_email_address)
          end

          describe_with_arturo_enabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
            it 'blocks the ticket' do
              statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert ticket_spam_detector.suspend_ticket?
              assert_equal ['blacklisted_regex_for_targeted_domains'], ticket_spam_detector.spam_indicators
            end

            describe_with_arturo_disabled :spam_detector_blacklist_pattern_check do
              it 'does not block the ticket' do
                statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
                ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
                assert_equal false, ticket_spam_detector.suspend_ticket?
              end
            end
          end

          describe_with_arturo_disabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
            it 'does not block the ticket' do
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert_equal false, ticket_spam_detector.suspend_ticket?
            end
          end

          describe 'the normalized ticket has a blacklisted_regex_for_targeted_domains' do
            let(:spam_pattern_tags) { ['type:blacklisted_regex_for_targeted_domains_reduced', 'source:Web form', 'matching_pattern:(?i-mx:注+最111111)', 'log_only:false'] }

            before do
              ssb_td_list = ['注+最111111']
              if ssb_td = Arturo::Feature.find_feature(:spammy_regex_for_targeted_domains_blacklist)
                ssb_td.external_beta_subdomains = ssb_td_list
                ssb_td.save!
              else
                Arturo::Feature.create!(
                    symbol: :spammy_regex_for_targeted_domains_blacklist,
                    external_beta_subdomains: ssb_td_list
                  )
              end

              @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
                  submitter: user,
                  subject: spammy_normalized_string_regex,
                  description: 'allllo is best dot comа',
                  tags: 'hello',
                  comment: comment
              }).ticket
              @ticket.requester.stubs(:email).returns(targeted_email_address)
            end

            describe_with_arturo_enabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
              it 'blocks on the ticket' do
                statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
                statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
                Rails.logger.expects(:append_attributes).with(spam_detector: { account_id: account.id, type: 'blacklisted_regex_for_targeted_domains_reduced', matching_pattern: '(?i-mx:注+最111111)', matching_field: 'ticket_subject', log_only: false}).at_most_once
                Rails.logger.expects(:info).with("ATSD_SPAM_DETECTED: {account:90538, type:blacklisted_regex_for_targeted_domains_reduced, matching_pattern:(?i-mx:注+最111111), matching_field:ticket_subject, log_only:false}")
                Rails.logger.expects(:append_attributes).with(targeted_domains: {ticket_subject: {subdomain: account.subdomain, account_id: account.id, result: true, atsd_reduced_text: normalize_unicode(spammy_normalized_string_regex), atsd_original_text: spammy_normalized_string_regex}})
                ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
                assert ticket_spam_detector.suspend_ticket?
                assert_equal ["blacklisted_regex_for_targeted_domains_reduced"], ticket_spam_detector.spam_indicators
              end

              it 'does not log attributes if log_attributes set to false' do
                statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
                statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
                Rails.logger.expects(:append_attributes).with(spam_detector: { account_id: account.id, type: 'blacklisted_regex_for_targeted_domains_reduced', matching_pattern: '(?i-mx:注+最111111)', matching_field: 'ticket_subject', log_only: false}).never
                Rails.logger.expects(:info).with("ATSD_SPAM_DETECTED: {account:90538, type:blacklisted_regex_for_targeted_domains_reduced, matching_pattern:(?i-mx:注+最111111), matching_field:ticket_subject, log_only:false}")
                Rails.logger.expects(:append_attributes).with(targeted_domains: {subdomain: account.subdomain, account_id: 90538, result: true, ATSD_reduced_text: normalize_unicode(spammy_normalized_string_regex), ATSD_original_text: spammy_normalized_string_regex}).never
                Rails.logger.expects(:info).with("SPAM_DETECTOR - subdomain: #{account.subdomain}, account_id: 90538, result: true, ATSD_reduced_text: #{normalize_unicode(spammy_normalized_string_regex)}, ATSD_original_text: #{spammy_normalized_string_regex}")
                ticket_spam_detector = Fraud::SpamDetector.new(@ticket, false)
                assert ticket_spam_detector.suspend_ticket?
                assert_equal ["blacklisted_regex_for_targeted_domains_reduced"], ticket_spam_detector.spam_indicators
              end

              describe_with_arturo_disabled :spam_detector_blacklist_pattern_check do
                it 'does not block the ticket' do
                  statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags).never
                  statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
                  Rails.logger.expects(:append_attributes).never
                  ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
                  assert_equal false, ticket_spam_detector.suspend_ticket?
                end
              end
            end

            describe_with_arturo_disabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
              it 'does not block the ticket' do
                statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags).never
                statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
                Rails.logger.expects(:append_attributes).never
                ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
                assert_equal false, ticket_spam_detector.suspend_ticket?
              end
            end
          end
        end
      end
    end

    describe 'has a rejected_domain' do
      before do
        @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
          submitter: user,
          subject: 'not-so private subject',
          description: 'safe',
          tags: 'hello',
          comment: comment
        }).ticket
      end

      let(:domain_blacklist_suspend) { "suspend:#{user_email_domain}" }
      let(:domain_blacklist_reject) { "reject:#{user_email_domain}" }

      describe 'there is an email on the suspend list' do
        let(:spam_pattern_tags) { ['type:suspended_domain', 'source:Web form', 'matching_pattern:', 'log_only:false'] }
        before { @ticket.account.stubs(:domain_blacklist).returns(domain_blacklist_suspend) }

        it 'suspends the ticket based on the requester' do
          statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
          statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          assert_equal ['suspended_domain'], ticket_spam_detector.spam_indicators
          assert ticket_spam_detector.suspend_ticket?
        end
      end

      describe 'there is an email on the reject list' do
        let(:spam_pattern_tags) { ['type:rejected_domain', 'source:Web form', 'matching_pattern:', 'log_only:false'] }
        before { @ticket.account.stubs(:domain_blacklist).returns(domain_blacklist_reject) }

        it 'rejects the ticket based on the requester' do
          statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
          statsd_client.expects(:increment).with('ticket.spam', tags: rejected_action_tag)
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          assert_equal ['rejected_domain'], ticket_spam_detector.spam_indicators
          assert ticket_spam_detector.reject_ticket?
        end
      end

      describe 'email is not on the list' do
        before { Account.any_instance.stubs(:domain_blacklist).returns('') }
        it 'does not reject or suspend the ticket based on the requester' do
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          assert_empty(ticket_spam_detector.spam_indicators)
          refute ticket_spam_detector.reject_ticket?
          refute ticket_spam_detector.suspend_ticket?
        end
      end

      describe 'requester email and blacklist aren\'t valid e-mails' do
        before { Account.any_instance.stubs(:domain_blacklist).returns('bob lablaw') }
        it 'does not reject the ticket based on the requester' do
          @ticket.requester.stubs(:email).returns('bob lablaw')
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          assert_empty(ticket_spam_detector.spam_indicators)
          refute ticket_spam_detector.reject_ticket?
        end
      end

      describe 'has a blocklisted domain' do
        before do
          Account.any_instance.stubs(:domain_blacklist).returns('')
          @ticket.account.stubs(:allows?).returns(false)
        end
        describe_with_arturo_disabled :orca_classic_blocklist_all_channel do
          let(:spam_pattern_tags) { ['type:all_channels_suspended_domain', 'source:Web form', 'matching_pattern:', 'log_only:true'] }
          describe_with_arturo_disabled :orca_classic_blocklist_all_channel_log_only do
            it "does not log the ticket, does not suspend" do
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert_empty(ticket_spam_detector.spam_indicators)
              refute ticket_spam_detector.suspend_ticket?
            end
          end

          describe_with_arturo_enabled :orca_classic_blocklist_all_channel_log_only do
            it "only logs the ticket, does not suspend" do
              statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert_empty(ticket_spam_detector.spam_indicators)
              refute ticket_spam_detector.suspend_ticket?
            end
          end
        end

        describe_with_arturo_enabled :orca_classic_blocklist_all_channel do
          let(:spam_pattern_tags) { ['type:all_channels_suspended_domain', 'source:Web form', 'matching_pattern:', 'log_only:false'] }
          describe_with_and_without_arturo_enabled :orca_classic_blocklist_all_channel_log_only do
            it "suspends the ticket" do
              statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert_equal ['all_channels_suspended_domain'], ticket_spam_detector.spam_indicators
              assert ticket_spam_detector.suspend_ticket?
            end
          end
        end
      end
    end

    describe 'has a blacklisted requester ip' do
      before do
        unless Arturo::Feature.find_feature(:spammy_ip_address_blacklist)
          Arturo::Feature.create!(
            symbol: :spammy_ip_address_blacklist,
            external_beta_subdomains: '151.248.122.191'
          )
        end

        @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
          submitter: user,
          subject: 'testing out bad IP',
          description: 'bad IP territory here',
          tags: 'hello',
          comment: comment
        }).ticket

        @ticket.will_be_saved_by(user)
      end

      describe_with_arturo_enabled :spam_detector_bad_ip do
        let(:spam_pattern_tags) { ['type:blacklisted_ip_address', 'source:Web form', 'matching_pattern:151.248.122.191', 'log_only:false'] }

        it 'suspends the ticket based on the requester' do
          @ticket.audit.metadata[:system].merge!(system_with_bad_ip)
          statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
          statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          assert ticket_spam_detector.suspend_ticket?
          assert_equal ['blacklisted_ip_address'], ticket_spam_detector.spam_indicators
        end

        it 'does not suspend ticket if missing IP' do
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          refute ticket_spam_detector.suspend_ticket?
          refute_equal ['blacklisted_ip_address'], ticket_spam_detector.spam_indicators
        end
      end

      describe_with_arturo_disabled :spam_detector_bad_ip do
        it 'does not suspend the ticket based on the requester' do
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          refute ticket_spam_detector.suspend_ticket?
        end
      end
    end

    describe 'has a blocklisted requester name' do
      before do
        unless Arturo::Feature.find_feature(:spammy_requester_name_blacklist)
          Arturo::Feature.create!(
            symbol: :spammy_requester_name_blacklist,
            external_beta_subdomains: 'minimum_end_user'
          )
        end

        @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
          submitter: user,
          subject: 'testing out bad requester name',
          description: 'bad users',
          tags: 'hello',
          comment: comment
        }).ticket

        @ticket.will_be_saved_by(user)
      end

      describe_with_arturo_enabled :orca_classic_spam_detector_blacklisted_requester_name do
        describe "when log_only is disabled" do
          describe_with_arturo_enabled :orca_classic_req_name_blacklist_take_action do
            let(:spam_pattern_tags) { ['type:blacklisted_requester_name', 'source:Web form', 'matching_pattern:minimum_end_user', 'log_only:false'] }

            it 'suspends the ticket based on the requester name' do
              statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag)
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              assert ticket_spam_detector.suspend_ticket?
              assert_equal ['blacklisted_requester_name'], ticket_spam_detector.spam_indicators
            end
          end
        end

        describe "when log_only is enabled" do
          describe_with_arturo_disabled :orca_classic_req_name_blacklist_take_action do
            let(:spam_pattern_tags) { ['type:blacklisted_requester_name', 'source:Web form', 'matching_pattern:minimum_end_user', 'log_only:true'] }

            it 'does not suspend the ticket based on the requester name' do
              statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
              statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).never
              ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
              refute ticket_spam_detector.suspend_ticket?
            end
          end
        end
      end

      describe_with_arturo_disabled :orca_classic_spam_detector_blacklisted_requester_name do
        it 'does not suspend the ticket based on the requester name' do
          ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
          refute ticket_spam_detector.suspend_ticket?
        end
      end
    end
  end

  describe 'spammy_ip_address_blacklist and requester contain a safelisted ip' do
    before do
      unless Arturo::Feature.find_feature(:spammy_ip_address_blacklist)
        Arturo::Feature.create!(
          symbol: :spammy_ip_address_blacklist,
          external_beta_subdomains: '10.210.80.219'
        )
      end

      @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
        submitter: user,
        subject: 'testing out bad IP',
        description: 'bad IP territory here',
        tags: 'hello',
        comment: comment
      }).ticket

      @ticket.will_be_saved_by(user)
      @ticket.audit.metadata[:system].merge!(system_with_safe_ip)
    end

    describe_with_arturo_enabled :spam_detector_bad_ip do
      it 'does not suspend the ticket based on the requester' do
        ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
        refute ticket_spam_detector.suspend_ticket?
      end

      it 'logs the offending IP address on the safe list' do
        Rails.logger.expects(:info).with('10.210.80.219 was found to be a safelisted IP address, skipping action')
        ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
        refute ticket_spam_detector.suspend_ticket?
      end
    end
  end

  describe 'simulating blacklist string patterns' do
    let(:spam_pattern_tags) { ['type:simulate_blacklisted_string', 'source:Web form', 'matching_pattern:spammystring', 'log_only:true'] }

    before do
      unless Arturo::Feature.find_feature(:simulate_string_blacklist)
        Arturo::Feature.create!(
          symbol: :simulate_string_blacklist,
          external_beta_subdomains: 'spammystring'
        )
      end

      @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
        submitter: user,
        subject: 'testing out spammystring',
        description: 'just some text',
        tags: 'hello',
        comment: comment
      }).ticket

      @ticket.will_be_saved_by(user)
    end

    it 'logs the pattern and does not suspend the ticket' do
      Rails.logger.expects(:info).with("ATSD_SPAM_DETECTED: {account:90538, type:simulate_blacklisted_string, matching_pattern:spammystring, matching_field:ticket_subject, log_only:true}").once
      statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags).once
      ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
      refute ticket_spam_detector.suspend_ticket?
    end

    describe_with_arturo_disabled :orca_classic_spam_detector_simulate_patterns do
      it 'does not log any matching patterns' do
        Rails.logger.expects(:info).with("ATSD_SPAM_DETECTED: {account:90538, type:simulate_blacklisted_string, matching_pattern:spammystring, log_only:true}").never
        statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags).never
        ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
        refute ticket_spam_detector.suspend_ticket?
      end
    end
  end

  describe 'comparing nilsimsa neighbor digests' do
    let(:spam_pattern_tags) { ['type:spam_digest_neighbor', 'source:Web form', 'matching_pattern:IDVWWUMNUL3B7XBDJI5D2FTVGXZ6HFEE76KBMW3HR4WVOF3FQAQA====', 'log_only:false'] }
    let(:spam_pattern_tags_log_only) { ['type:spam_digest_neighbor', 'source:Web form', 'matching_pattern:IDVWWUMNUL3B7XBDJI5D2FTVGXZ6HFEE76KBMW3HR4WVOF3FQAQA====', 'log_only:true'] }
    let(:spam_text) { 'hello to orca from spammer' }
    let(:spam_digest) { Fraud::NilsimsaLSH.new(spam_text, 'description').digest }

    before do
      unless Arturo::Feature.find_feature(:orca_nilsimsa_spam_digests)
        Arturo::Feature.create!(
          symbol: :orca_nilsimsa_spam_digests,
          external_beta_subdomains: spam_digest.downcase # the arturo UI will downcase any new entries, so matching that behavior
        )
      end

      @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
        submitter: user,
        subject: 'testing out spammystring',
        description: spam_text
      }).ticket
      @ticket.will_be_saved_by(user)

      Arturo.enable_feature! :orca_classic_nilsimsa_atsd_compare_spam_digests
    end

    describe 'when a match returns true' do
      before { Fraud::NilsimsaLSH.any_instance.stubs(:neighbor_match?).returns(true) }
      it 'suspends the ticket' do
        statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags).at_least_once
        statsd_client.expects(:increment).with('ticket.spam', tags: suspended_action_tag).at_least_once
        spam_detector = Fraud::SpamDetector.new(@ticket)
        assert spam_detector.suspend_ticket?
      end

      describe_with_arturo_enabled :orca_classic_nilsimsa_atsd_block_spam_digest_log_only do
        it 'does not suspend the ticket, but logs the decision' do
          statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags_log_only).at_least_once
          spam_detector = Fraud::SpamDetector.new(@ticket)
          refute spam_detector.suspend_ticket?
        end
      end
    end

    describe 'when matches return false' do
      before { Fraud::NilsimsaLSH.any_instance.stubs(:neighbor_match?).returns(false) }
      it 'does not suspend the ticket' do
        spam_detector = Fraud::SpamDetector.new(@ticket)
        refute spam_detector.suspend_ticket?
      end
    end

    describe 'when spam digest is malformed and not the correct length' do
      before do
        @malformed_digest = spam_digest[1..-5]
        ssb_list = [@malformed_digest]
        if ssb = Arturo::Feature.find_feature(:orca_nilsimsa_spam_digests)
          ssb.external_beta_subdomains = ssb_list
          ssb.save!
        else
          Arturo::Feature.create!(symbol: :spammy_string_blacklist, external_beta_subdomains: ssb_list)
        end
      end

      it 'does not block the ticket' do
        statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags).never
        statsd_client.expects(:increment).with('ticket.atsd.invalid_pattern', tags: ["pattern:#{@malformed_digest}"]).at_least_once
        spam_detector = Fraud::SpamDetector.new(@ticket)
        refute spam_detector.suspend_ticket?
      end
    end

    describe_with_arturo_disabled :orca_classic_nilsimsa_atsd_compare_spam_digests do
      it 'does not compare neighbor digests' do
        Fraud::NilsimsaLSH.any_instance.expects(:neighbor_match?).never
        Fraud::SpamDetector.new(@ticket)
      end
    end
  end

  describe 'handles ticket without a requester' do
    before do
      @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
        subject: spammy_string * 500,
        description: 'Привет мир его Дэвид! Это описание русского спама',
        tags: 'hello',
        comment: comment
      }).ticket
      @ticket.stubs(:requester).returns(nil)
      @ticket_spam_detector = Fraud::SpamDetector.new(@ticket)
    end

    it 'returns empty string for requester name' do
      assert_equal @ticket_spam_detector.send(:ticket_requester_name, @ticket), ''
      assert_empty @ticket_spam_detector.spam_indicators
    end

    it 'returns nil for mail gem sanitized address' do
      assert_nil @ticket_spam_detector.send(:sanitized_email_address)
      assert_empty @ticket_spam_detector.spam_indicators
    end
  end

  describe 'when logging reduced text' do
    let(:targeted_email_address) { '12345@qq.com' }
    let(:excluded_text) { 'EXCLUDED from orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains' }
    let(:reduced_text) { '123456789' }
    let(:spam_pattern_tags) { ['type:blacklisted_string_for_targeted_domains', 'source:Web form', 'matching_pattern:123456789', 'log_only:false'] }
    before do
      @ticket = Zendesk::Tickets::Initializer.new(account, user, ticket: {
          submitter: user,
          subject: spammy_targeted_domain_string,
          description: 'allllo is best dot comа',
          tags: 'hello',
          comment: comment
      }).ticket
      @ticket.requester.stubs(:email).returns(targeted_email_address)
      ssb_td_list = ['123456789', 'something else', 'www.qifa608.com', '【vs-11.com']
      if ssb_td = Arturo::Feature.find_feature(:spammy_string_for_targeted_domains_blacklist)
        ssb_td.external_beta_subdomains = ssb_td_list
        ssb_td.save!
      else
        Arturo::Feature.create!(
            symbol: :spammy_string_for_targeted_domains_blacklist,
            external_beta_subdomains: ssb_td_list
          )
      end
    end

    describe_with_arturo_enabled :spam_detector_blacklist_pattern_check do
      describe_with_arturo_enabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
        describe 'with log_attributes set to default' do
          it 'logs the result via append_attributes' do
            statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
            Rails.logger.expects(:info).with("SPAM_DETECTOR - subdomain: #{account.subdomain}, account_id: #{account.id}, result: true, ATSD_reduced_text: #{reduced_text}, ATSD_original_text: #{spammy_targeted_domain_string}").never
            Rails.logger.expects(:append_attributes).with(targeted_domains: {
                ticket_subject: {
                    subdomain: account.subdomain,
                    account_id: account.id,
                    result: true,
                    atsd_reduced_text: reduced_text,
                    atsd_original_text: spammy_targeted_domain_string
                }
            })
            Fraud::SpamDetector.new(@ticket)
          end
        end
        describe 'with log_attributes set to false' do
          it 'logs the result via info' do
            statsd_client.expects(:increment).with('ticket.spam', tags: spam_pattern_tags)
            Rails.logger.expects(:info).with("SPAM_DETECTOR - subdomain: #{account.subdomain}, account_id: #{account.id}, result: true, ATSD_reduced_text: #{reduced_text}, ATSD_original_text: #{spammy_targeted_domain_string}")
            Rails.logger.expects(:append_attributes).never
            Fraud::SpamDetector.new(@ticket, false)
          end
        end
      end
      describe_with_arturo_disabled :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains do
        describe 'with log_attributes set to default' do
          it 'logs the excluded message via append_attributes' do
            Rails.logger.expects(:info).with("SPAM_DETECTOR - subdomain: #{account.subdomain}, account_id: #{account.id}, result: #{excluded_text}, ATSD_reduced_text: #{reduced_text}, ATSD_original_text: #{spammy_targeted_domain_string}").never
            Rails.logger.expects(:append_attributes).with(targeted_domains: {
                'ticket_subject' => {'subdomain' => account.subdomain, 'account_id' => account.id, 'result' => excluded_text, 'atsd_reduced_text' => reduced_text, 'atsd_original_text' => spammy_targeted_domain_string},
                'ticket_description' => {'subdomain' => account.subdomain, 'account_id' => account.id, 'result' => excluded_text, 'atsd_reduced_text' => '', 'atsd_original_text' => 'allllo is best dot comа'},
                'ticket_requester_name' => {'subdomain' => account.subdomain, 'account_id' => account.id, 'result' => excluded_text, 'atsd_reduced_text' => '', 'atsd_original_text' => 'minimum_end_user'},
                'ticket_comment' => {'subdomain' => account.subdomain, 'account_id' => account.id, 'result' => excluded_text, 'atsd_reduced_text' => '', 'atsd_original_text' => ''}
            })
            Fraud::SpamDetector.new(@ticket)
          end
        end
        describe 'with log_attributes set to false' do
          it 'logs the excluded message via info' do
            Rails.logger.expects(:info).with("SPAM_DETECTOR - subdomain: #{account.subdomain}, account_id: #{account.id}, result: #{excluded_text}, ATSD_reduced_text: #{reduced_text}, ATSD_original_text: #{spammy_targeted_domain_string}")
            Rails.logger.expects(:append_attributes).never
            Fraud::SpamDetector.new(@ticket, false)
          end
        end
      end
    end
  end
end
