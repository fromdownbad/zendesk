require_relative "../../support/test_helper"

SingleCov.covered!

describe Fraud::FraudServiceClient do
  describe "fraud_service_client" do
    circuit_options = {
        failure_threshold: 3,
        reset_timeout: 5,
        dry_run: false
    }
    before do
      Fraud::FraudServiceClient.send(:reset)
      Rails.env.stubs(:development?).returns(true)
    end

    it 'creates faraday client without retry parameters' do
      Kragle.expects(:new).with('account_fraud_service', retry_options: { max: 3 }, circuit_options: circuit_options).returns(stub('kragle', tap: "something", url_prefix: ""))
      Fraud::FraudServiceClient.send(:fraud_service_client, false)
    end

    it 'creates faraday client with retry parameters' do
      Kragle.expects(:new).with('account_fraud_service', retry_options: { max: 0 }, circuit_options: circuit_options).returns(stub('kragle', tap: "something", url_prefix: ""))
      Fraud::FraudServiceClient.send(:fraud_service_client, true)
    end

    it 'uses the correct url prefix' do
      faraday_connecton = Fraud::FraudServiceClient.send(:fraud_service_client, true)
      assert_equal faraday_connecton.url_prefix.host, 'orca.zd-dev.com'
    end
  end

  describe "post client exception handlers" do
    before do
      Fraud::FraudServiceClient.send(:reset)
    end

    describe "when there is a circuit error" do
      before do
        stub_request(:post, 'http://orca.zd-dev.com/api/services/private/activity').with(body: {}).to_raise(JohnnyFive::CircuitTrippedError)
      end

      it 'increments datadog' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('fraud_service.client.circuit.error')
        Fraud::FraudServiceClient.post_activity({}, "http://orca.zd-dev.com/api/services/private/activity")
      end
    end

    describe "when there is a timeout error" do
      before do
        stub_request(:post, 'http://orca.zd-dev.com/api/services/private/activity').to_timeout
      end

      it 'increments datadog ' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('fraud_service.client.connection.error')
        Fraud::FraudServiceClient.post_activity({}, "http://orca.zd-dev.com/api/services/private/activity")
      end
    end

    describe "when there is a an internal error" do
      before do
        stub_request(:post, 'http://orca.zd-dev.com/api/services/private/activity').with(body: {}).to_raise(Kragle::InternalServerError)
      end
      it 'does not increment datadog' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).never
        Fraud::FraudServiceClient.post_activity({}, "http://orca.zd-dev.com/api/services/private/activity")
      end
    end
  end

  describe "post_config" do
    it "returns correct values with retry" do
      config = Fraud::FraudServiceClient.post_config(false)
      assert_equal({timeout: 7, open_timeout: 4, max_retries: 3, circuit_breaker_failure_threshold: 3, circuit_breaker_reset_timeout_secs: 5}, config)
    end

    it "returns correct values with no_retry" do
      config = Fraud::FraudServiceClient.post_config(true)
      assert_equal({timeout: 1, open_timeout: 0.5, max_retries: 0, circuit_breaker_failure_threshold: 3, circuit_breaker_reset_timeout_secs: 5}, config)
    end
  end
end
