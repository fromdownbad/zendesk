require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 6

describe Fraud::FraudSafelist do
  fixtures :accounts

  include Fraud::FraudSafelist

  let(:account) { accounts(:minimum) }

  describe '#safe_ip_address?' do
    before do
      @spammy_ip = '151.248.122.191'
      @safe_ip = '10.210.80.219'
      @malformed_ip = '1.5.0'
    end

    it 'should identify a safe ip' do
      Rails.logger.expects(:info).with("#{@safe_ip} was found to be a safelisted IP address, skipping action")
      assert safe_ip_address?(@safe_ip)
    end

    it 'should not identify a spammy ip' do
      refute safe_ip_address?(@spammy_ip)
    end

    it 'should not identify a malformed address' do
      Rails.logger.expects(:info).with("#{@malformed_ip} is not an ip address, FraudSafelist check has been skipped")
      refute safe_ip_address?(@malformed_ip)
    end

    describe 'when the env variable is set with IPs' do
      before { ENV['FRAUD_SAFELIST_IP_RANGES'] = "10.0.0.0/8" }

      it 'should identify a safe ip' do
        assert safe_ip_address?(@safe_ip)
      end

      it 'should not identify a spammy ip' do
        refute safe_ip_address?(@spammy_ip)
      end

      describe 'when list has malformed IP' do
        before { ENV['FRAUD_SAFELIST_IP_RANGES'] = @malformed_ip }

        it 'should not identify a spammy IP' do
          refute safe_ip_address?(@spammy_ip)
        end
      end
    end

    describe 'when unexpected error occurs' do
      before { Fraud::FraudSafelist.stubs(:safe_ip_address?).returns(StandardError) }

      it 'should not identify a spammy ip' do
        refute safe_ip_address?(@spammy_ip)
      end
    end

    describe_with_arturo_disabled :orca_classic_ip_safelist do
      it 'should not identify a safe ip' do
        refute safe_ip_address?(@safe_ip)
      end
    end
  end
end
