require_relative "../../support/test_helper"
require_relative "../../support/arturo_test_helper"

SingleCov.covered! uncovered: 2

describe Fraud::FraudScoreActions do
  include ArturoTestHelper
  include Fraud::FraudScoreActions

  fixtures :accounts, :account_settings

  class TestFraudSecurityPolicy
    include Zendesk::SecurityPolicy::Enforcement

    class_attribute :id
    self.id = 99

    attr_accessor :password

    validations[:unencrypted_password] << :short_password_length

    def policy_level
      id
    end

    def short_password_length
      Zendesk::SecurityPolicy::Validation::Length.new(minimum: 4)
    end

    def unencrypted_password
      password.unencrypted
    end

    def random_password
      Token.generate(15, false)
    end
  end

  let(:actions) do
    {
      auto_suspend: false,
      enable_captcha: false,
      mark_risk_assessment_as_safe_age: false,
      mark_risk_assessment_as_safe_paid: false,
      outbound_email_risky: false,
      outbound_email_safe: false,
      support_risky: false,
      takeover: false,
      talk_risky: false,
      unwhitelist: false,
      whitelist: false
    }
  end

  let(:current_account) { accounts(:minimum) }
  let(:fraud_score) { FactoryBot.create(:fraud_score, account: current_account) }
  let(:statsd_client_fraud_actions) { Zendesk::StatsD::Client.new(namespace: ['fraud_actions']) }

  describe "prepare #performs_fraud_actions" do
    before do
      current_account.stubs(:latest_fraud_score).returns(fraud_score)
      current_account.stubs(:is_trial?).returns(true)
      fraud_score
    end

    describe '#whitelist' do
      before do
        actions[:whitelist] = true
        current_account.is_serviceable = false
        current_account.settings.whitelisted_from_fraud_restrictions = false
        current_account.settings.user_identity_limit_for_trialers = true
        current_account.settings.bulk_user_upload = false
        current_account.settings.is_abusive = true
        current_account.save!
      end

      describe_with_arturo_enabled :whitelist_via_afs do
        let(:pravda_response_body) { { account: { is_abusive: false } } }

        [true, false].each do |trial_status|
          before do
            stub_abusive_response(body: pravda_response_body)
            Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns(false)
            current_account.stubs(:is_trial?).returns(trial_status)
            perform_fraud_actions(actions)
          end

          it "whitelists the account from fraud restrictions" do
            assert current_account.settings.whitelisted_from_fraud_restrictions
          end

          it "sets bulk user upload to true" do
            assert current_account.settings.bulk_user_upload
          end

          it "account abusive helper returns false" do
            refute current_account.abusive?
          end

          it "sets user_identity_limit_for_trialers to false" do
            refute current_account.settings.user_identity_limit_for_trialers
          end

          it "sets whitelist risk assessment" do
            assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::MONITOR_WHITELIST_RISK_ASSESSMENT
          end

          it 'sets is_serviceable to true' do
            assert current_account.is_serviceable
          end
        end
      end

      describe_with_arturo_disabled :whitelist_via_afs do
        before { perform_fraud_actions(actions) }
        it "does not whitelist the account from fraud restrictions" do
          refute current_account.settings.whitelisted_from_fraud_restrictions
        end

        it "does not set bulk user upload to true" do
          refute current_account.settings.bulk_user_upload
        end

        it "does not set the is_abusive setting to false" do
          assert current_account.settings.is_abusive
        end

        it "does not set user_identity_limit_for_trialers to false" do
          assert current_account.settings.user_identity_limit_for_trialers
        end

        it "does not set whitelist risk assessment" do
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::MONITOR_WHITELIST_RISK_ASSESSMENT
        end
      end
    end

    describe '#unwhitelist' do
      before do
        actions[:unwhitelist] = true
        current_account.settings.whitelisted_from_fraud_restrictions = true
        current_account.settings.user_identity_limit_for_trialers = false
        current_account.settings.prior_risk_assessment = ::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT
        current_account.settings.save!
      end

      describe_with_arturo_enabled :unwhitelist_via_afs do
        it "unwhitelists the account" do
          perform_fraud_actions(actions)
          refute current_account.settings.whitelisted_from_fraud_restrictions
          assert current_account.settings.user_identity_limit_for_trialers
          assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT
        end

        it "unwhitelists paid accounts" do
          current_account.stubs(:is_trial?).returns(false)
          perform_fraud_actions(actions)
          refute current_account.settings.whitelisted_from_fraud_restrictions
          refute current_account.settings.user_identity_limit_for_trialers
        end
      end

      describe_with_arturo_disabled :unwhitelist_via_afs do
        it "does not whitelist the account" do
          perform_fraud_actions(actions)
          assert current_account.settings.whitelisted_from_fraud_restrictions
        end
      end
    end

    describe '#takeover' do
      let(:pravda_response_body) { { account: { is_abusive: true } } }

      before do
        actions[:takeover] = true
        @policy = TestFraudSecurityPolicy.new
        current_account.owner.stubs(:security_policy).returns(@policy)
        current_account.settings.mobile_app_access = true
        current_account.settings.email_agent_when_sensitive_fields_changed = true
        current_account.settings.api_token_access = true
        current_account.settings.api_password_access = true
        current_account.settings.save!
        stub_abusive_response(body: pravda_response_body)
      end

      it "changes current account owner and logouts all user and devices" do
        pre_take_over_email = current_account.owner.email
        TerminateAllSessionsJob.expects(:enqueue).with(current_account.id, true).once
        perform_fraud_actions(actions)
        MailRenderingJob.expects(:enqueue).never
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::MONITOR_TAKEOVER_RISK_ASSESSMENT
        assert current_account.owner.reload.email.start_with?("#{Fraud::FraudScoreActions::TAKEN_OVER_EMAIL_SUBSTRING}+")
        assert_equal pre_take_over_email, current_account.settings.prior_owner_email
      end

      it "restricts token and email access to API" do
        assert current_account.settings.mobile_app_access?
        assert current_account.settings.email_agent_when_sensitive_fields_changed?

        perform_fraud_actions(actions)

        refute current_account.settings.mobile_app_access?
        refute current_account.settings.email_agent_when_sensitive_fields_changed?
      end

      it "disables mobile app access and email notifcations" do
        assert current_account.settings.api_token_access?
        assert current_account.settings.api_password_access?

        perform_fraud_actions(actions)

        refute current_account.settings.api_token_access?
        refute current_account.settings.api_password_access?
      end

      it "changes password and saves old prior owner password" do
        pre_take_over_password = current_account.owner[:crypted_password]
        perform_fraud_actions(actions)
        assert_equal pre_take_over_password.to_s, current_account.settings.prior_owner_password.to_s
        refute_equal pre_take_over_password.to_s, current_account.owner.crypted_password.to_s
        refute_equal pre_take_over_password.to_s, current_account.owner[:crypted_password]
      end

      it "does takeover paid accounts" do
        current_account.stubs(:is_trial?).returns(false)
        pre_taken_over_email = current_account.owner.email
        perform_fraud_actions(actions)
        assert current_account.owner.reload.email.start_with?("#{Fraud::FraudScoreActions::TAKEN_OVER_EMAIL_SUBSTRING}+")
        assert_equal pre_taken_over_email, current_account.settings.prior_owner_email
      end

      it "marks the account abusive" do
        perform_fraud_actions(actions)
        assert current_account.abusive?
      end

      describe "when an account is whitelisted_from_fraud_restrictions" do
        before do
          current_account.settings.whitelisted_from_fraud_restrictions = true
        end

        it "does not takeover the account" do
          perform_fraud_actions(actions)
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::MONITOR_TAKEOVER_RISK_ASSESSMENT
        end
      end

      describe "when a paid account is whitelisted? but not whitelisted_from_fraud_restrictions" do
        before do
          current_account.settings.whitelisted_from_fraud_restrictions = false
          current_account.stubs(:whitelisted?).returns(true)
          current_account.stubs(:is_trial?).returns(false)
        end

        it "does takeover account" do
          pre_taken_over_email = current_account.owner.email
          perform_fraud_actions(actions)
          assert current_account.owner.reload.email.start_with?("#{Fraud::FraudScoreActions::TAKEN_OVER_EMAIL_SUBSTRING}+")
          assert_equal pre_taken_over_email, current_account.settings.prior_owner_email
        end
      end

      describe "when an account has already been taken over" do
        before do
          current_account.owner.primary_email_identity.update_attribute(:value, "#{Fraud::FraudScoreActions::TAKEN_OVER_EMAIL_SUBSTRING}+1@gmail.com")
          current_account.settings.risk_assessment = ::Account::FraudSupport::MONITOR_TAKEOVER_RISK_ASSESSMENT
          current_account.settings.save! && current_account.owner.save!
        end

        it "does not change prior_owner_email" do
          current_account.settings.prior_owner_email = 'original.prior.owner@email.com'
          current_account.settings.save!

          perform_fraud_actions(actions)

          assert_equal current_account.settings.prior_owner_email, 'original.prior.owner@email.com'
        end

        it "does not change password again" do
          pre_take_over_password = current_account.owner.crypted_password
          perform_fraud_actions(actions)
          assert_equal pre_take_over_password.to_s, current_account.owner.crypted_password.to_s
        end
      end

      describe_with_arturo_disabled :takeover_via_afs do
        it "does not takeover the account" do
          pre_taken_over_email = current_account.owner.email
          perform_fraud_actions(actions)
          assert_equal current_account.owner.reload.email, pre_taken_over_email
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::MONITOR_TAKEOVER_RISK_ASSESSMENT
        end
      end
    end

    describe '#mark_abusive' do
      before do
        actions[:mark_abusive] = true
      end

      it 'marks the account as abusive' do
        Zendesk::Accounts::Client.any_instance.expects(:update_account).with(is_abusive: true)
        perform_fraud_actions(actions)
      end
    end

    describe '#mark_not_abusive' do
      before do
        actions[:mark_not_abusive] = true
      end

      it 'marks the account as not abusive' do
        Zendesk::Accounts::Client.any_instance.expects(:update_account).with(is_abusive: false)
        perform_fraud_actions(actions)
      end
    end

    describe '#auto_suspend' do
      let(:pravda_response_body) { { account: { is_abusive: true } } }

      before do
        actions[:auto_suspend] = true
        current_account.is_serviceable = true
        current_account.text_mail_template = "{{content}}\n\ntest\n{{footer}}"
        current_account.settings.api_token_access = true
        current_account.settings.api_password_access = true
        stub_abusive_response(body: pravda_response_body)
      end

      it "suspends the account" do
        perform_fraud_actions(actions)
        assert_equal current_account.settings.help_center_state, 'disabled'
        refute current_account.is_serviceable
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT
      end

      it "reverts the templates back to defaults" do
        perform_fraud_actions(actions)
        assert_equal current_account.text_mail_template, Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE
      end

      it "restricts api access" do
        perform_fraud_actions(actions)
        refute current_account.settings.api_token_access?
        refute current_account.settings.api_password_access?
      end

      it "marks the account abusive" do
        perform_fraud_actions(actions)
        assert current_account.abusive?
      end

      it "does not suspend paid accounts" do
        current_account.stubs(:is_trial?).returns(false)
        perform_fraud_actions(actions)
        assert current_account.is_serviceable
      end

      describe "when an account is whitelisted" do
        before do
          current_account.settings.whitelisted_from_fraud_restrictions = true
        end

        it "does not suspend the account" do
          perform_fraud_actions(actions)
          assert current_account.is_serviceable
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT
        end
      end

      describe_with_arturo_disabled :auto_suspend_via_afs do
        it "does not suspend the account" do
          perform_fraud_actions(actions)
          assert current_account.is_serviceable
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT
        end

        it "does not revert templates back to defaults" do
          perform_fraud_actions(actions)
          refute_equal current_account.text_mail_template, Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE
        end

        it "does not restrict api access" do
          perform_fraud_actions(actions)
          assert current_account.settings.api_token_access?
          assert current_account.settings.api_password_access?
        end
      end
    end

    describe '#outbound_email_risky' do
      before do
        actions[:outbound_email_risky] = true
        current_account.settings.risk_assessment = Account::FraudSupport::EMAIL_SAFE_RISK_ASSESSMENT
      end

      it "marks risk assessment at risky level" do
        perform_fraud_actions(actions)
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::EMAIL_UNSAFE_RISK_ASSESSMENT
      end

      it "does not mark paid account risk assessment at risky level" do
        current_account.stubs(:is_trial?).returns(false)
        perform_fraud_actions(actions)
        refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::EMAIL_UNSAFE_RISK_ASSESSMENT
      end

      describe_with_arturo_disabled :outbound_email_risky_via_afs do
        it "does not mark risk assessment at risky level" do
          perform_fraud_actions(actions)
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::EMAIL_UNSAFE_RISK_ASSESSMENT
        end
      end
    end

    describe '#outbound_email_safe' do
      before do
        actions[:outbound_email_safe] = true
        current_account.settings.risk_assessment = Account::FraudSupport::DEFAULT_RISK_ASSESSMENT
      end

      it "marks risk assessment at safe level" do
        perform_fraud_actions(actions)
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::EMAIL_SAFE_RISK_ASSESSMENT
      end

      it "does not mark paid account risk assessment at safe level" do
        current_account.stubs(:is_trial?).returns(false)
        perform_fraud_actions(actions)
        refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::EMAIL_SAFE_RISK_ASSESSMENT
      end

      describe_with_arturo_disabled :outbound_email_safe_via_afs do
        it "does not mark risk assessment at safe level" do
          perform_fraud_actions(actions)
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::EMAIL_SAFE_RISK_ASSESSMENT
        end
      end
    end

    describe '#unsuspend' do
      let(:pravda_response_body) { { account: { is_abusive: false } } }

      before do
        current_account.settings.is_abusive = true
        current_account.is_serviceable = false
        FactoryBot.create(:voice_sub_account, :suspended, account: current_account)
        Voice::Core::SubAccount.any_instance.stubs(:suspended?).returns(true)
        current_account.save!
        actions[:unsuspend] = true
        ::Voice::ActivateVoiceJob.stubs(:enqueue)
        Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns(false)
        stub_abusive_response(body: pravda_response_body)
      end

      it "unsuspends the account" do
        ::Voice::ActivateVoiceJob.expects(:enqueue).once
        perform_fraud_actions(actions, Fraud::SourceEvent::MONITOR)
        refute current_account.abusive?
        assert current_account.is_serviceable
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::DEFAULT_RISK_ASSESSMENT
      end

      it "does not activate talk when account does not have talk suspended before" do
        ::Voice::ActivateVoiceJob.expects(:enqueue).never
        Voice::Core::SubAccount.any_instance.stubs(:suspended?).returns(false)
        perform_fraud_actions(actions, Fraud::SourceEvent::MONITOR)
      end

      it "unsuspends paid accounts" do
        ::Voice::ActivateVoiceJob.expects(:enqueue).once
        current_account.stubs(:is_trial?).returns(false)
        perform_fraud_actions(actions, Fraud::SourceEvent::MONITOR)
        refute current_account.abusive?
        assert current_account.is_serviceable
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::DEFAULT_RISK_ASSESSMENT
      end

      describe_with_arturo_disabled :unsuspend_via_afs do
        it "does not unsuspend the account" do
          perform_fraud_actions(actions, Fraud::SourceEvent::MONITOR)
          assert current_account.abusive?
          refute current_account.is_serviceable
        end
      end
    end

    describe '#support_risky' do
      before do
        actions[:support_risky] = true
      end

      it "makes the account support_risky" do
        perform_fraud_actions(actions, Fraud::SourceEvent::TRIAL_SIGNUP)
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
      end

      it "does not make paid account support_risky" do
        current_account.stubs(:is_trial?).returns(false)
        perform_fraud_actions(actions, Fraud::SourceEvent::TRIAL_SIGNUP)
        refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
      end

      describe_with_arturo_disabled :support_risky_via_afs do
        it "does not make paid account support_risky" do
          current_account.stubs(:is_trial?).returns(false)
          perform_fraud_actions(actions, Fraud::SourceEvent::TRIAL_SIGNUP)
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
        end
      end
    end

    describe '#talk_risky' do
      before do
        actions[:talk_risky] = true
      end

      it "makes the account talk_risky" do
        perform_fraud_actions(actions, Fraud::SourceEvent::TRIAL_SIGNUP)
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::TALK_RISKY_RISK_ASSESSMENT
      end

      it "does not make paid account talk_risky" do
        current_account.stubs(:is_trial?).returns(false)
        perform_fraud_actions(actions, Fraud::SourceEvent::TRIAL_SIGNUP)
        refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::TALK_RISKY_RISK_ASSESSMENT
      end

      describe_with_arturo_disabled :talk_risky_via_afs do
        it "does not make paid account talk_risky" do
          current_account.stubs(:is_trial?).returns(false)
          perform_fraud_actions(actions, Fraud::SourceEvent::TRIAL_SIGNUP)
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::TALK_RISKY_RISK_ASSESSMENT
        end
      end
    end

    describe 'mark_risk_assessment_as_safe_age' do
      before do
        actions[:mark_risk_assessment_as_safe_age] = true
      end

      it "updates the risk assessment as safe age" do
        perform_fraud_actions(actions, Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB)
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SAFE_AGE_RISK_ASSESSMENT
      end

      describe_with_arturo_disabled :mark_risk_assessment_as_safe_age_via_afs do
        it "does not update the risk assessment as safe age" do
          perform_fraud_actions(actions, Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB)
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SAFE_AGE_RISK_ASSESSMENT
        end
      end
    end

    describe '#mark_risk_assessment_as_safe_paid' do
      before do
        actions[:mark_risk_assessment_as_safe_paid] = true
      end

      it "updates the risk assessment as safe paid" do
        perform_fraud_actions(actions, Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB)
        assert_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SAFE_PAID_RISK_ASSESSMENT
      end

      describe_with_arturo_disabled :mark_risk_assessment_as_safe_paid_via_afs do
        it "does not update the risk assessment as safe paid" do
          perform_fraud_actions(actions, Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB)
          refute_equal current_account.settings.risk_assessment, ::Account::FraudSupport::SAFE_PAID_RISK_ASSESSMENT
        end
      end
    end

    describe '#enable_captcha' do
      before { actions[:enable_captcha] = true }

      describe_with_arturo_enabled :enable_captcha_via_afs do
        it "enables captcha" do
          perform_fraud_actions(actions, Fraud::SourceEvent::SPAMMY_INBOUND_TICKET_ACTIVITY)
          assert current_account.settings.captcha_required?
        end
      end

      describe_with_arturo_disabled :enable_captcha_via_afs do
        it "does not enable captcha" do
          perform_fraud_actions(actions, Fraud::SourceEvent::SPAMMY_INBOUND_TICKET_ACTIVITY)
          refute current_account.settings.captcha_required?
        end
      end
    end

    describe '#statsd_client_fraud_actions' do
      let(:pravda_response_body) { { account: { is_abusive: true } } }
      before { stub_abusive_response(body: pravda_response_body) }

      it "logs an action with the source event" do
        tags = ["action:#{Fraud::FraudAction::AUTO_SUSPEND}", "source_event:#{Fraud::SourceEvent::SC_ACCOUNT_TRIAL_SIGNUP}", "whitelisted:#{current_account.settings.whitelisted_from_fraud_restrictions}"]
        statsd_client_fraud_actions.expects(:increment).with('success', tags: tags)
        perform_fraud_actions({Fraud::FraudAction::AUTO_SUSPEND => true}, Fraud::SourceEvent::SC_ACCOUNT_TRIAL_SIGNUP)
      end
    end

    describe 'when many actions are triggered' do
      it "only calls save once" do
        current_account.expects(:save!).once
        perform_fraud_actions(actions)
      end
    end
  end

  def stub_abusive_response(body: {}, status: 200)
    stub_request(:patch, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{current_account.id}").
      to_return(
        status:  status,
        body:    body.to_json,
        headers: { content_type: 'application/json; charset=utf-8' }
      )
  end
end
