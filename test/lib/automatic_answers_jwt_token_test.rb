require_relative "../support/test_helper"

SingleCov.covered!

describe AutomaticAnswersJwtToken do
  let(:account_id) { 1 }
  let(:user_id) { 2 }
  let(:ticket_id) { 20 }
  let(:deflection_id) { 5 }
  let(:articles) { [1, 2, 3, 4] }
  let(:token) { 'CRC32VALUE' }
  let(:decrypted_token) do
    {
      "account_id" => account_id,
      "user_id" => user_id,
      "ticket_id" => ticket_id,
      "deflection_id" => deflection_id,
      "articles" => articles,
      "token" => token,
      "exp" => Time.parse("(2014-11-04").to_i
    }
  end

  it 'should encrypt the token using the shared secret' do
    Timecop.travel(2014, 11, 04, 0, 00) do
      encrypted_token = AutomaticAnswersJwtToken.encrypted_token(account_id, user_id, ticket_id, deflection_id, articles, token)
      decrypted_token['exp'] = 30.days.from_now.to_i

      assert_equal decrypted_token, JWT.decode(encrypted_token, AUTOMATIC_ANSWERS_JWT_SECRET, true)[0]
    end
  end

  it 'should fail after the token has expired' do
    encrypted_token = AutomaticAnswersJwtToken.encrypted_token(account_id, user_id, ticket_id, deflection_id, articles, token)

    Timecop.travel 33.days.from_now do
      assert_raises JWT::ExpiredSignature do
        JWT.decode(encrypted_token, AUTOMATIC_ANSWERS_JWT_SECRET, true)
      end
    end
  end

  it 'should return nil if token signature fails verification and throws JWT::VerificationError' do
    assert_nil AutomaticAnswersJwtToken.decode("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhY2NvdW50X2lkIjoxLCJ1c2VyX2lkIjoxMDAwMiwidGlja2V0X2lkIjoxMSwiYXJ0aWNsZXMiOlsxMDAwMSwxMDAwMywxMDAwNF0sInRva2VuIjpudWxsLCJleHAiOjE0ODQ3MDczNjl9.NlBsuU3CDAO93_unSDqW5g3ZRJkK0uvqQlK-zOOLc2s")
  end

  it 'should return nil if token is incorrect format and throws JWT::DecodeError' do
    assert_nil AutomaticAnswersJwtToken.decode("Th1s1sDefinitelyNotA-JWT")
  end
end
