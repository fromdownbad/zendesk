require_relative '../support/test_helper'
require 'hc_uploader_configuration'

SingleCov.covered!

describe HCUploaderConfiguration do
  describe '.storage' do
    it 'symbolizes what it loads from ENV' do
      assert_equal(
        :fog,
        HCUploaderConfiguration.storage
      )
    end
  end

  describe '.bucket_name' do
    it 'loads from ENV' do
      assert_equal(
        'zendesk-help-center-test',
        HCUploaderConfiguration.bucket_name
      )
    end
  end

  describe '.region' do
    it 'loads from ENV' do
      assert_equal(
        'us-east-3',
        HCUploaderConfiguration.region
      )
    end
  end

  describe '.aws_access_key_id' do
    it 'loads from ENV' do
      assert_equal(
        'FAKE_access_key_id',
        HCUploaderConfiguration.aws_access_key_id
      )
    end
  end

  describe '.aws_secret_access_key' do
    it 'loads from ENV' do
      assert_equal(
        'FAKE_secret_access_key',
        HCUploaderConfiguration.aws_secret_access_key
      )
    end
  end

  describe '.replicated_bucket_name' do
    it 'loads from ENV' do
      assert_equal(
        'zendesk-help-center-test-replicated',
        HCUploaderConfiguration.replicated_bucket_name
      )
    end
  end

  describe '.replicated_region' do
    it 'loads from ENV' do
      assert_equal(
        'us-east-3',
        HCUploaderConfiguration.replicated_region
      )
    end
  end

  describe '.replicated_aws_access_key_id' do
    it 'loads from ENV' do
      assert_equal(
        'FAKE_replicated_access_key_id',
        HCUploaderConfiguration.replicated_aws_access_key_id
      )
    end
  end

  describe '.replicated_aws_secret_access_key' do
    it 'loads from ENV' do
      assert_equal(
        'FAKE_replicated_secret_access_key',
        HCUploaderConfiguration.replicated_aws_secret_access_key
      )
    end
  end
end
