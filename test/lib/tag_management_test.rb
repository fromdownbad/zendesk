require_relative "../support/test_helper"

SingleCov.covered! uncovered: 42

describe TagManagement do
  describe ".normalize_tags" do
    it "handles hashes" do
      assert_equal [], TagManagement.normalize_tags({})
    end

    it "normalizes tag strings inside array" do
      assert_equal ["abc", "def"], TagManagement.normalize_tags(["abc def"])
    end

    it "normalizes array" do
      assert_equal ["abc"], TagManagement.normalize_tags(["abc"])
      assert_equal [], TagManagement.normalize_tags([])
      assert_equal [], TagManagement.normalize_tags(nil) # empty array in params is converted to nil
    end

    it "does not include duplicates" do
      assert_equal ["abc", "def"], TagManagement.normalize_tags("abc def abc")
    end

    it "normalizes string" do
      assert_equal ["abc"], TagManagement.normalize_tags("abc")
    end

    it "does not remove empty strings" do
      assert_equal ["abc", ""], TagManagement.normalize_tags("abc %")
    end

    describe_with_arturo_disabled :downcase_tags_as_ascii do
      let(:account) { accounts(:minimum) }

      it 'correctly downcases non-ascii characters' do
        assert_equal ['hello', 'привет'], TagManagement.normalize_tags('HELLO ПРИВЕТ', account)
      end
    end

    describe_with_arturo_enabled :downcase_tags_as_ascii do
      let(:account) { accounts(:minimum) }

      it 'only downcases ascii characters' do
        assert_equal ['hello', 'ПРИВЕТ'], TagManagement.normalize_tags('HELLO ПРИВЕТ', account)
      end
    end
  end

  describe ".normalize_tag_string" do
    it "tokenizes string" do
      assert_equal ["abc", "def"], TagManagement.normalize_tag_string("abc def")
    end

    it "removes special characters" do
      assert_equal ["abc", "def", ""], TagManagement.normalize_tag_string("abc@ d%e>f #")
    end

    describe "funky space characters" do
      spaces = [
        "\u00A0", "\u1680", "\u180E", "\u2000", "\u2001", "\u2002",
        "\u2003", "\u2004", "\u2005", "\u2006", "\u2007", "\u2008",
        "\u2009", "\u200A", "\u200B", "\u202F", "\u205F", "\u3000",
        "\uFEFF"
      ]

      spaces.each do |s|
        it "removes the #{'%4.4x' % s.ord} character" do
          assert_equal ["foo", "normaltag"], TagManagement.normalize_tag_string("foo normal#{s}tag")
        end
      end
    end

    it "returns nil for anything else" do
      assert_nil TagManagement.normalize_tag_string([])
    end
  end

  describe ".equals?" do
    it "normalizes" do
      assert TagManagement.equals?("abc def", "abc def")
    end

    it "handles mixed modes" do
      assert TagManagement.equals?("abc def", ["abc", "def"])
    end

    it "does not be equal" do
      refute TagManagement.equals?("def abc", ["abc", "def"])
    end
  end

  describe ".tags_changes" do
    before do
      @user = users(:with_groups_end_user)
    end

    it "returns change set from the set_tags with the persisted tags" do
      @user.set_tags = 'tag1 tag2'
      @user.tags_changes[:removed].must_equal []
      @user.tags_changes[:added].must_equal ['tag1', 'tag2']
    end

    it "returns a hash with empty attribute" do
      @user.tags_changes[:removed].must_equal []
      @user.tags_changes[:added].must_equal []
    end
  end

  describe ".tags_changed?" do
    before do
      @user = users(:with_groups_end_user)
    end

    it "returns true if there are changes between current_tags and persisted tags" do
      @user.set_tags = 'tag1 tag2'
      assert @user.tags_changed?
    end

    it "returns false if there are no changes between current_tags and persisted tags" do
      refute @user.tags_changed?
    end
  end
end
