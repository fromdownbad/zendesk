require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zopim::InternalApiClient do
  let(:described_class) { Zopim::InternalApiClient }

  describe "#new" do
    it "configures and provisions an instance of a Faraday connection via Kragle" do
      conn = mock('Faraday::Connection')

      Kragle.expects(:new).with('zopim').returns(conn)
      conn.expects('url_prefix=').with(Zopim::Configuration['scribe_api']['base_url'])
      conn.expects('port=').with(Zopim::Configuration['scribe_api']['port'])

      assert Zopim::InternalApiClient.new(1)
    end
  end

  describe "#notify_account_settings_change" do
    it "runs a post call to /account if notification_type is 'ip_settings_changed'" do
      Faraday::Connection.any_instance.expects('post').with(
        '/system/notification/account',
        account_id: 1,
        notification_type: 'ip_settings_changed'
      ).returns(true)

      assert Zopim::InternalApiClient.new(1).notify_account_settings_change('ip_settings_changed')
    end

    it "runs a post call to /classic if notification_type is 'polaris_settings_changed'" do
      Faraday::Connection.any_instance.expects('post').with(
        '/system/notification/classic',
        account_id: 1,
        notification_type: 'polaris_settings_changed'
      ).returns(true)

      assert Zopim::InternalApiClient.new(1).notify_account_settings_change('polaris_settings_changed')
    end

    it 'handles error' do
      Faraday::Connection.any_instance.expects('post').raises(Faraday::Error)
      Rails.logger.
        expects(:error).
        with(regexp_matches(/Failed to update Chat for account settings changes something_changed for account 1/))
      refute Zopim::InternalApiClient.new(1).notify_account_settings_change('something_changed')
    end
  end

  describe "#update_agent_setting_avatar" do
    it "runs a put call to /system/admin/accounts/:account_id/agents/:agent_id/settings" do
      Faraday::Connection.any_instance.expects('put').with(
        '/system/admin/accounts/1/agents/33/settings',
        avatar_path: 'http://img.test.com/img.jpg'
      ).returns(true)

      assert Zopim::InternalApiClient.new(1).update_agent_setting_avatar(33, 'http://img.test.com/img.jpg')
    end

    it 'handles error' do
      Faraday::Connection.any_instance.expects('put').raises(Faraday::Error)
      Rails.logger.
        expects(:error).
        with(regexp_matches(/Failed update Chat avatar setting for agent 33 account 1/))
      refute Zopim::InternalApiClient.new(1).update_agent_setting_avatar(33, 'test url')
    end
  end

  describe '#get_migration_options' do
    let(:client) { Zopim::InternalApiClient.new(1) }

    it 'proxies to scribe' do
      client_response_body = { options: ['rename'] }.to_json
      stub_request(:get, 'https://scribe.chat.zd-dev.com/system/migrations/departments/options?account_id=1').
        to_return(status: 200, body: client_response_body, headers: {})
      assert_equal client_response_body, client.get_migration_options(:departments).body
    end

    it 'wraps errors' do
      stub_request(:get, 'https://scribe.chat.zd-dev.com/system/migrations/departments/options?account_id=1').
        to_return(status: 404, body: 'rah', headers: {})
      client_response = client.get_migration_options(:departments)
      assert_equal 'rah', client_response.body
      assert_equal 404, client_response.status
    end
  end

  describe '#start_migration' do
    let(:client) { Zopim::InternalApiClient.new(1) }

    it 'proxies to scribe' do
      client_response_body = { 'message' => 'migration enqueued' }.to_json
      stub_request(:post, 'https://scribe.chat.zd-dev.com/system/migrations/departments').
        with(body: { 'account_id' => 1, 'option' => 'rename' }.to_json).
        to_return(status: 202, body: client_response_body, headers: {})
      client_response = client.start_migration(:departments, option: 'rename')
      assert_equal client_response_body, client_response.body
      assert_equal 202, client_response.status
    end

    it 'wraps errors' do
      stub_request(:post, 'https://scribe.chat.zd-dev.com/system/migrations/departments').
        with(body: { 'account_id' => 1, 'option' => 'rename' }.to_json).
        to_return(status: 404, body: 'rah', headers: {})
      client_response = client.start_migration(:departments, option: 'rename')
      assert_equal 'rah', client_response.body
      assert_equal 404, client_response.status
    end
  end

  describe '#send_messaging_csat' do
    let(:client) { described_class.new(1) }
    let(:requester) { stub(translation_locale: ENGLISH_BY_ZENDESK) }
    let(:ticket) { stub(nice_id: 123, requester: requester) }
    let(:event) { stub(ticket: ticket, visitor_id: mock) }

    it 'sends csat request to scribe' do
      Faraday::Connection.any_instance.expects('post').with(
        '/system/notification/messaging_csat_requests',
        {
          account_id: 1,
          ticket_id: event.ticket.nice_id,
          visitor_id: event.visitor_id,
          rating_request_text: I18n.t('txt.chat.csat.rating_request_text'),
          comment_request_text: I18n.t('txt.chat.csat.comment_request_text'),
          feedback_text: I18n.t('txt.chat.csat.feedback_text'),
          rating_labels: {
            good: I18n.t('txt.chat.csat.rating_labels.good', described_class::EMOTICONS_MAP),
            bad: I18n.t('txt.chat.csat.rating_labels.bad', described_class::EMOTICONS_MAP),
          },
        }
      )
      client.send_messaging_csat(event)
    end
  end
end
