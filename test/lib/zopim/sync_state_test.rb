require_relative "../../support/test_helper"

SingleCov.not_covered!

describe ZendeskBillingCore::Zopim::SyncState do
  describe ".types" do
    it "returns all supported types" do
      expected = %w[ready pending error]
      actual   = ZendeskBillingCore::Zopim::SyncState::TYPES
      assert_equal expected, actual
    end

    it "does not define constants for an unsupported type" do
      refute ZendeskBillingCore::Zopim::SyncState.const_defined? :Unsupported
    end
  end

  describe "when referenced as a constant" do
    it "returns the matching state" do
      assert_equal 'ready',   ZendeskBillingCore::Zopim::SyncState::Ready
      assert_equal 'pending', ZendeskBillingCore::Zopim::SyncState::Pending
      assert_equal 'error',   ZendeskBillingCore::Zopim::SyncState::Error
    end
  end
end
