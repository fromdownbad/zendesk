require_relative "../../support/test_helper"
require 'zopim_reseller_api'

SingleCov.covered!

describe Zopim::Reseller do
  it "configures and provisions an instance of the reseller API client" do
    Zopim::Reseller.instance_variable_set(:@singleton__instance__, nil) # can already be set from prior tests

    settings = Hash(Zopim.config[:reseller_api_client_settings])
    credentials = Hash(Zopim.config[:reseller_api_credentials])
    config_and_provision = sequence('config')

    ZopimResellerApi::Client.expects(:base_url).
      with(settings['base_url']).
      in_sequence(config_and_provision)
    ZopimResellerApi::Client.expects(:base_path).
      with(settings['base_path']).
      in_sequence(config_and_provision)
    ZopimResellerApi::Client.expects(:rest_auth_prefix).
      with(settings['rest_auth_prefix']).
      in_sequence(config_and_provision)
    ZopimResellerApi::Client.expects(:new).
      with(credentials['token'], credentials['secret']).
      in_sequence(config_and_provision).
      returns(stub)

    assert Zopim::Reseller.client
  end
end
