require_relative '../../../../support/test_helper'
require_relative '../../../../support/zopim_test_helper'
require_relative '../../../../support/suite_test_helper'

SingleCov.not_covered!

describe ZendeskBillingCore::Zopim::Subscription::Creation do
  include SuiteTestHelper
  include ZopimTestHelper

  fixtures :all

  before do
    Timecop.freeze('2014-12-23')
    ZopimIntegration.any_instance.stubs(
      app_installed?: false,
      install_app: true,
      ensure_not_phase_four: nil
    )
    Zopim::Reseller.client.stubs(
      create_account!: Hashie::Mash.new(id: 1, account_key: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      find_accounts!:  []
    )
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)
  end

  describe "creating a trial zopim subscription" do
    let(:account) { accounts(:minimum) }

    let(:zopim_subscription) { account.zopim_subscription }

    describe "when the zendesk account is eligible" do
      before do
        ::ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.expects(:enqueue).with(account.id, 1).once
        stub_request(:post, %r{embeddable/api/internal/config_sets.json})
        account.create_zopim_subscription!
      end

      it "should have default values assigned" do
        expected = {
          account_id: account.id,
          subscription_id: account.subscription.id,
          billing_cycle_type: account.subscription.billing_cycle_type,
          owner_email: account.owner.zopim_email,
          max_agents: account.subscription.max_agents,
          plan_type: 'trial',
          status: 'active',
          trial_days: 30,
          zopim_reseller_id: 1055,
          zopim_account_id: 1
        }
        actual = zopim_subscription.attributes.symbolize_keys.
          slice(
            :account_id,
            :subscription_id,
            :billing_cycle_type,
            :owner_email,
            :max_agents,
            :plan_type,
            :status,
            :trial_days,
            :zopim_reseller_id,
            :zopim_account_id
          )
        assert_equal expected, actual
      end

      it "should have created an owner-agent record" do
        assert zopim_subscription.owner_agent.present?
      end

      it "should have created an integration record" do
        assert zopim_subscription.reload.zopim_integration.present?
      end

      it "should set the chat permission set for the account" do
        assert account.permission_sets.
          find_by_role_type(::PermissionSet::Type::CHAT_AGENT)
      end
    end

    describe "when the account is not active or serviceable" do
      before do
        ::ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.expects(:enqueue).never
        Account.any_instance.stubs(
          is_active?: false,
          is_serviceable?: false
        )
      end

      it "should raise an error" do
        message = 'Validation failed: AccountNotTrialEligible'
        stub_request(:post, %r{embeddable/api/internal/config_sets.json})
        error = assert_raises ActiveRecord::RecordInvalid do
          account.create_zopim_subscription!
        end
        assert_equal message, error.message
      end

      it "should not have created an owner-agent record" do
        assert account.zopim_agents.blank?
      end

      it "should not have created an integration record" do
        refute account.zopim_integration.present?
      end

      it "should not set the chat permission set for the account" do
        refute account.permission_sets.
          find_by_role_type(::PermissionSet::Type::CHAT_AGENT)
      end
    end

    describe "when there was a previous trial or subscription" do
      before do
        ZendeskBillingCore::Zopim::SubscriptionDeactivator.stubs(cancelled?: true)
      end

      it "should raise an error" do
        message = 'Validation failed: TooManyTrialAttempts'
        stub_request(:post, %r{embeddable/api/internal/config_sets.json})
        error = assert_raises ActiveRecord::RecordInvalid do
          account.create_zopim_subscription!
        end
        assert_equal message, error.message
      end
    end
  end
end
