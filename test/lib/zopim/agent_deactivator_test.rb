require_relative '../../support/test_helper'
require_relative '../../support/zopim_test_helper'
require_relative '../../support/suite_test_helper'

SingleCov.covered!

describe Zopim::AgentDeactivator do
  include SuiteTestHelper
  include ZopimTestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:owner) { account.owner }
  let(:zopim_identity) { owner.zopim_identity }

  let(:reseller_client) do
    stub(update_account_agent!: stub, find_accounts!: [])
  end

  subject { Zopim::AgentDeactivator.new(zopim_identity) }

  before do
    Timecop.freeze('2015-01-13')
    Zopim::AgentSyncJob.stubs(:work)
    Zopim::Reseller.stubs(client: reseller_client)
    Account.any_instance.stubs(has_chat_permission_set?: true)
    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )

    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)
    ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
    ZopimIntegration.stubs(where: stub(:first_or_create!))
    Zopim::Reseller.client.stubs(
      create_account!: Hashie::Mash.new(id: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      find_accounts!: []
    )

    setup_zopim_subscription(account)
  end

  describe "#deactivate!" do
    describe "when the user belongs to a chat phase 3 account" do
      let(:zopim_subscription) { account.zopim_subscription }
      let(:zopim_account_id) { zopim_subscription.zopim_account_id }
      let(:zopim_agent_id) { zopim_identity.zopim_agent_id }

      before do
        Zopim::Reseller.stubs(:client).raises(
          Exception, 'Should not invoke Reseller API'
        )
      end

      describe "and is an owner account" do
        before do
          zopim_identity.stubs(
            phase_three?: true,
            is_owner?: true
          )
        end

        it "does not call the Reseller API" do
          subject.deactivate!
        end
      end

      describe "and is not an owner account" do
        before do
          zopim_identity.stubs(
            phase_three?: true,
            is_owner?: false
          )
        end

        it "does not call the Reseller API" do
          subject.deactivate!
        end
      end
    end

    describe "when the chat-identity is not linked" do
      before { zopim_identity.stubs(is_linked?: false) }

      it "does not disconnect from zopim" do
        reseller_client.expects(:update_account_agent!).never
        subject.deactivate!
      end
    end

    describe "when the chat-identity is linked" do
      let(:zopim_subscription) { account.zopim_subscription }
      let(:zopim_account_id) { zopim_subscription.zopim_account_id }
      let(:zopim_agent_id) { zopim_identity.zopim_agent_id }

      before { zopim_identity.stubs(is_linked?: true) }

      describe "and is an owner account" do
        before { zopim_identity.stubs(is_owner?: true) }

        it "disables the chat-identity record" do
          timestamp  = Time.now.to_i
          account_id = account.id
          user_id    = owner.id
          email      = zopim_identity.email
          Digest::MD5.expects(:hexdigest).
            with("####{account_id}-#{user_id}###-#{timestamp}-#{email}").
            returns('digest')

          payload = {
            email: "####{account_id}-#{user_id}###-digest@aghassipour.com"
          }
          reseller_client.expects(:update_account_agent!).once.
            with(id: zopim_account_id, agent_id: zopim_agent_id, data: payload)

          subject.deactivate!
        end
      end

      describe "and is not an owner account" do
        before { zopim_identity.stubs(is_owner?: false) }

        it "drops the chat-identity record" do
          reseller_client.expects(:delete_account_agent!).once.
            with(id: zopim_account_id, agent_id: zopim_agent_id)
          subject.deactivate!
        end

        describe "when the chat-identity no longer exists" do
          before do
            reseller_client.expects(:delete_account_agent!).
              raises(ZopimResellerApi::Middleware::Exception::NotFound, 'not found')
          end

          it "does not raise an error" do
            subject.deactivate!
          end
        end

        describe "when owner chat-identity no longer exists" do
          before do
            zopim_identity.stubs(:is_owner?).returns(true)
            reseller_client.expects(:update_account_agent!).
              raises(ZopimResellerApi::Middleware::Exception::NotFound, 'not found')
          end

          it "does not raise an error" do
            subject.deactivate!
          end
        end

        describe "when the reseller API fails" do
          before do
            reseller_client.expects(:delete_account_agent!).
              raises('some error')
          end

          it "raises an error" do
            assert_raise(RuntimeError) { subject.deactivate! }
          end
        end
      end
    end
  end
end
