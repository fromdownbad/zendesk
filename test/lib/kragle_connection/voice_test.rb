require_relative "../../support/test_helper"

SingleCov.covered!

describe KragleConnection::Voice do
  fixtures :accounts
  let(:account) { accounts(:minimum) }

  describe '.build_for_voice' do
    it 'returns a voice connection' do
      connection = KragleConnection.build_for_voice(account)
      assert_equal 'http://voice-test:3000/', connection.url_prefix.to_s
    end
  end
end
