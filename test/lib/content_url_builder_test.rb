require_relative "../support/test_helper"

SingleCov.covered!

describe 'ContentUrlBuilder' do
  let(:url_builder) { ContentUrlBuilder.new }

  fixtures :all

  describe 'url_for' do
    describe_with_arturo_disabled :no_partitioning_of_id_in_attachment_url do
      it 'returns the url for photo' do
        photo = FactoryBot.create(:photo, filename: "large_1.png")
        photo.stubs(:public_filename).returns("/somewhere/large_1.png")
        assert url_builder.url_for(photo), "https://minimum.zendesk-test.com/somewhere/large_1.png"
      end

      it 'returns the url for brand logo' do
        logo = FactoryBot.create(:brand_logo, filename: "small.png")
        logo.stubs(:public_filename).returns("/somewhere/small.png")
        assert url_builder.url_for(logo), "https://minimum.zendesk-test.com/somewhere/small.png"
      end

      it 'returns the url for attachment' do
        attachment = attachments(:foreign_file_attachment)
        attachment.stubs(:public_filename).returns("/somewhere/large_1.png")
        assert url_builder.url_for(attachment), "https://minimum.zendesk-test.com/somewhere/large_1.png"
      end
    end

    describe_with_arturo_enabled :no_partitioning_of_id_in_attachment_url do
      it 'returns the url for photo' do
        photo = FactoryBot.create(:photo, filename: "large_1.png")
        assert url_builder.url_for(photo), "https://minimum.zendesk-test.com/somewhere/large_1.png"
      end

      it 'returns the url for brand logo' do
        logo = FactoryBot.create(:brand_logo, filename: "small.png")
        assert url_builder.url_for(logo), "https://minimum.zendesk-test.com/somewhere/small.png"
      end

      it 'returns the url for attachment' do
        attachment = attachments(:foreign_file_attachment)
        assert url_builder.url_for(attachment), "https://minimum.zendesk-test.com/somewhere/large_1.png"
      end
    end
  end
end
