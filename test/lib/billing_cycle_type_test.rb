require_relative "../support/test_helper"

SingleCov.not_covered!

describe 'BillingCycleType' do
  describe "Billing cycles" do
    it "is defined" do
      assert BillingCycleType.Monthly
      assert BillingCycleType.Quarterly
      assert BillingCycleType.Biannually
      assert BillingCycleType.Annually
      assert BillingCycleType.Semiannually
      assert BillingCycleType.Biennially
      assert BillingCycleType.Pentannually
      assert_equal BillingCycleType.all,
        [
          BillingCycleType.Monthly, BillingCycleType.Quarterly,
          BillingCycleType.Annually, BillingCycleType.Triennially, BillingCycleType.Semiannually,
          BillingCycleType.Biennially, BillingCycleType.Pentannually
        ]
    end
  end
end
