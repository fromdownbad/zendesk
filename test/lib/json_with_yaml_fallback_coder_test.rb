require_relative "../support/test_helper"
require 'json_with_yaml_fallback_coder'

SingleCov.covered!

describe 'JsonWithYamlFallbackCoder' do
  setup do
    @coder = JsonWithYamlFallbackCoder.new
    @hash = {
      "string"  => "bar",
      "integer" => 42,
      "hash"    => { "foo" => "bar" },
      "array"   => [1, "baz", 3]
    }
    @json = '{"string":"bar","integer":42,"hash":{"foo":"bar"},"array":[1,"baz",3]}'
    @yaml = "---\nstring: bar\ninteger: 42\nhash:\n  foo: bar\narray:\n- 1\n- baz\n- 3\n"
  end

  it "#dump returns JSON" do
    assert_equal @coder.dump(@hash), @json
  end

  it "#load from JSON returns the hash" do
    assert_equal @coder.load(@json), @hash
  end

  it "#load from YAML returns the hash" do
    assert_equal @coder.load(@yaml), @hash
  end
end
