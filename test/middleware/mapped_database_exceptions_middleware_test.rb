require_relative '../support/test_helper'

SingleCov.covered!

describe MappedDatabaseExceptionsMiddleware do
  let(:app) { stub(call: [200, {}, []]) }
  let(:middleware) { MappedDatabaseExceptionsMiddleware.new(app) }

  describe '#call' do
    it 'calls the middleware chain' do
      app.expects(:call).once

      middleware.call({})
    end

    describe 'when a mapped database exception is raised' do
      before do
        app.expects(:call).raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound, 'required_parameter')
      end

      it 'rescues, logs and reraises them' do
        Rails.logger.expects(:error).once
        Rails.application.config.statsd.client.expects(:increment).with('db_exception.deadlock_found').once

        assert_raises ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound do
          middleware.call({})
        end
      end
    end
  end
end
