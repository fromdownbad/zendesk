require_relative "../../support/test_helper"
require 'rack/mock'

SingleCov.covered!

describe Billing::ScheduledMaintenance do
  # rubocop:disable Naming/VariableName
  # rubocop:disable Lint/UnderscorePrefixedVariableName
  _MIDDLEWARE = Billing::ScheduledMaintenance
  # rubocop:enable Lint/UnderscorePrefixedVariableName

  let(:service_available_response) do
    [200, { 'content-type' => 'text/plain' }, ['OK']]
  end

  let(:scheduled_maintenance_response) do
    [503, { 'content-type' => 'text/plain' }, ['Scheduled Maintenance']]
  end

  let(:app) { ->(_env) { service_available_response } }

  let(:middleware) { _MIDDLEWARE.new(app) }

  let(:content_type) { 'text/plain' }

  let(:request) do
    Rack::MockRequest.env_for(
      request_path,
      :method        => request_method,
      'CONTENT_TYPE' => content_type
    )
  end

  describe '#call' do
    subject { middleware.call(request) }

    describe 'when not in maintenance' do
      before { Arturo.disable_feature! :billing_maintenance }

      _MIDDLEWARE::BLACKLISTED_ENDPOINTS.each do |name, (method, path)|
        let(:request_path) { path }
        let(:request_method) { method }

        describe "when request is for #{name}" do
          it { subject.must_equal service_available_response }
        end
      end
    end

    describe 'when in maintenance' do
      before { Arturo.enable_feature! :billing_maintenance }

      _MIDDLEWARE::BLACKLISTED_ENDPOINTS.each do |name, (method, path)|
        let(:request_path) { path }
        let(:request_method) { method }

        describe "when request is for #{name}" do
          it { subject.must_equal scheduled_maintenance_response }
        end
      end
    end

    describe 'whitelisting request-endpoints in maintenance' do
      before { Arturo.enable_feature! :billing_maintenance }

      _MIDDLEWARE::BLACKLISTED_ENDPOINTS.each do |name, (method, path)|
        let(:request_path) { path }
        let(:request_method) { method }
        let(:allowed_endpoint) { :"billing_maintenance_allow_#{name}" }

        describe "when request for #{name} is allowed" do
          before { Arturo.enable_feature! allowed_endpoint }
          it { subject.must_equal service_available_response }
        end

        describe "when request for #{name} is not allowed" do
          before { Arturo.disable_feature! allowed_endpoint }
          it { subject.must_equal scheduled_maintenance_response }
        end
      end
    end
  end

  describe 'blacklisted endpoints' do
    subject { _MIDDLEWARE::BLACKLISTED_ENDPOINTS }

    let(:blacklisted_endpoints) do
      {
        purchases:                    [:post,   '/api/v2/account/subscription'],
        amendments:                   [:put,    '/api/v2/account/subscription'],
        payment_method_updates:       [:post,   '/api/v2/account/subscription/update_payment_method'],
        currency_updates:             [:post,   '/api/v2/account/subscription/update_currency'],
        add_agents:                   [:post,   '/api/v2/account/subscription/add_agents'],
        cancellations:                [:delete, '/api/v2/account/subscription'],
        account_address_updates:      [:put,    '/settings/account/update_address'],
        account_invoice_updates:      [:post,   '/settings/account/update_invoices'],
        internal_api_amendments:      [:put,    '/api/v2/internal/billing/account'],
        internal_api_cancellations:   [:delete, '/api/v2/internal/billing/subscription'],
        internal_api_update_addons:   [:put,    '/api/v2/internal/billing/subscription/addons'],
        internal_api_update_features: [:put,    '/api/v2/internal/billing/subscription/features'],
        monitor_cancellations:        [:post,   '/api/internal/monitor/destroy_account']
      }
    end

    it { subject.must_equal blacklisted_endpoints }
  end

  describe 'response format' do
    before { Arturo.enable_feature! :billing_maintenance }

    let(:request_path) { '/api/v2/account/subscription' }
    let(:request_method) { :post }

    describe 'content-type' do
      subject { middleware.call(request)[1] }

      let(:json_content_type) { { 'content-type' => 'application/json' } }
      let(:html_content_type) { { 'content-type' => 'text/html' } }
      let(:plain_content_type) { { 'content-type' => 'text/plain' } }

      describe 'when the request content-type is "application/json"' do
        let(:content_type) { 'application/json' }
        it { subject.must_equal json_content_type }
      end

      describe 'when the request content-type is "text/html"' do
        let(:content_type) { 'text/html' }
        it { subject.must_equal html_content_type }
      end

      describe 'when the request content-type is "text/plain"' do
        let(:content_type) { 'text/plain' }
        it { subject.must_equal plain_content_type }
      end
    end

    describe 'response-body' do
      subject { middleware.call(request)[2] }

      let(:json_response_body) do
        [{ message: 'Scheduled Maintenance' }.to_json]
      end
      let(:html_response_body) { ['Scheduled Maintenance'] }
      let(:plain_response_body) { ['Scheduled Maintenance'] }

      describe 'when the request content-type is "application/json"' do
        let(:content_type) { 'application/json' }
        it { subject.must_equal json_response_body }
      end

      describe 'when the request content-type is "text/html"' do
        let(:content_type) { 'text/html' }
        it { subject.must_equal html_response_body }
      end

      describe 'when the request content-type is "text/plain"' do
        let(:content_type) { 'text/plain' }
        it { subject.must_equal plain_response_body }
      end
    end
  end
  # rubocop:enable Naming/VariableName
end
