require_relative "../support/test_helper"

SingleCov.covered!

describe 'IpWhitelistMiddleware' do
  fixtures :accounts, :account_settings, :subscriptions, :users

  let(:path_env) { "PATH_INFO" }

  def assert_request_blocked_with_html_response(status, headers, _body)
    assert_equal 403, status
    assert_equal({'Content-Type' => 'text/html' }, headers)
  end

  before do
    @app = stub(call: [])
    @account = accounts(:minimum)
    @ip_middleware = IpWhitelistMiddleware.new(@app)
    @env = {'HTTP_HOST' => accounts(:minimum).host_name, 'zendesk.account' => @account }
    Subscription.any_instance.stubs(:has_ip_restrictions?).returns(true)
  end

  describe "a request with ip restriction enabled" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "1.1.1.1"
    end

    it "allows access if from whitelisted ip" do
      @env["REMOTE_ADDR"] = "1.1.1.1"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
    end

    it "denys access if from a non whitelisted ip" do
      @env["REMOTE_ADDR"] = "2.2.2.2"
      res = @ip_middleware.call(@env)
      assert_request_blocked_with_html_response(*res)
    end
  end

  describe "a request on account with ip restriction not enabled" do
    before do
      @account.settings.ip_restriction_enabled = '0'
    end

    it "allows all requests regardless of ip" do
      @env["REMOTE_ADDR"] = "1.1.1.1"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
    end
  end

  describe "a request on an account with a very limited ip restriction range" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "1.1.1.1/32"
    end

    it "allows access if from whitelisted ip" do
      @env["REMOTE_ADDR"] = "1.1.1.1"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
      refute @env["zendesk.internal.trusted_request"]
    end

    it "allows access if from whitelisted ip and the x_forwarded_for header is set" do
      @env["HTTP_X_FORWARDED_FOR"] = "3.3.3.3,1.1.1.1"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
      # Check to see if it copes with spaces in the HTTP_X_FORWARDED_FOR
      @env["HTTP_X_FORWARDED_FOR"] = "3.3.3.3, 1.1.1.1"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
    end

    it "ignores internal IP addresses" do
      @env["HTTP_X_FORWARDED_FOR"] = "1.1.1.1, 192.168.0.2"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
    end

    it "denys access if from a non whitelisted ip" do
      @env["REMOTE_ADDR"] = "2.2.2.2"
      res = @ip_middleware.call(@env)
      assert_request_blocked_with_html_response(*res)
    end

    it "allows access if from a internal zendesk ip" do
      @env["REMOTE_ADDR"] = "127.0.0.1"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
      assert @env["zendesk.internal.trusted_request"]
    end
  end

  describe "a request on an account with a less limited ip restriction range" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "199.16.156.0/22"
    end

    it "allows access if from whitelisted ip" do
      @env["REMOTE_ADDR"] = "199.16.157.88"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
    end

    it "denys access if from a non whitelisted ip" do
      @env["REMOTE_ADDR"] = "1.1.1.1"
      res = @ip_middleware.call(@env)
      assert_request_blocked_with_html_response(*res)
    end
  end

  describe "a request on an account with multiple ip restriction ranges" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "2.2.2.2/32 3.3.3.3"
    end

    it "allows access if from whitelisted ip which is first in the list" do
      @env["REMOTE_ADDR"] = "2.2.2.2"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
    end

    it "allows access if from whitelisted ip which is not first in the list" do
      @env["REMOTE_ADDR"] = "3.3.3.3"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
    end

    it "does not allow access if the ip is not whitelisted" do
      @env["REMOTE_ADDR"] = "1.1.1.1"
      res = @ip_middleware.call(@env)
      assert_request_blocked_with_html_response(*res)
    end
  end

  describe "csv_exports" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "192.168.0.5"
      @env[path_env] = '/csv_exports/135536.zip'
      @env["QUERY_STRING"] = 'token=crp7eektrlxyi93'
    end

    describe "from whitelisted IP addresses" do
      before { @env["REMOTE_ADDR"] = "192.168.0.5" }
      it "is allowed" do
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end
    end

    describe "from non-whitelisted IP addresses" do
      before { @env["REMOTE_ADDR"] = "3.3.3.3" }
      it "is allowed" do
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end
    end
  end

  describe 'channels endpoints' do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "192.168.0.5"
    end

    describe 'from whitelisted IP address' do
      before do
        @env["REMOTE_ADDR"] = "192.168.0.5"
      end

      it "is allowed for any_channel" do
        @env[path_env] = '/api/v2/any_channel/push.json'
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end
    end

    describe 'from non-whitelisted IP address' do
      before do
        @env["REMOTE_ADDR"] = "3.3.3.3"
      end

      it "is allowed for any_channel" do
        @env[path_env] = '/api/v2/any_channel/push.json'
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end
    end
  end

  describe "voice callbacks" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "192.168.0.5"
    end

    describe "from whitelisted IP addresses" do
      before do
        @env["REMOTE_ADDR"] = "192.168.0.5"
      end
      it "is allowed for calls" do
        @env[path_env] = '/voice/calls'
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end
      it "is allowed for media" do
        @env[path_env] = '/media/voice'
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end
    end

    describe "from non-whitelisted IP addresses" do
      before do
        @env["REMOTE_ADDR"] = "3.3.3.3"
      end
      it "is allowed for calls" do
        @env[path_env] = '/voice/calls'
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end
      it "is allowed for media" do
        @env[path_env] = '/media/voice'
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end
    end
  end

  describe "ticket sharing" do
    before do
      @account.settings.ip_restriction_enabled = '1'
    end

    describe "agreements" do
      describe "/sharing/agreements" do
        before { @env[path_env] = '/sharing/agreements' }

        describe "from whitelisted IP addresses" do
          it "is allowed" do
            @env["REMOTE_ADDR"] = "192.168.0.5"
            @app.expects(:call).with(@env)
            @ip_middleware.call(@env)
          end
        end

        describe "from non-whitelisted IP addresses" do
          it "is allowed" do
            @env["REMOTE_ADDR"] = "4.4.4.4"
            @app.expects(:call).with(@env)
            @ip_middleware.call(@env)
          end
        end
      end

      describe "/sharing/agreements/1234" do
        before { @env[path_env] = '/sharing/agreements/1234' }

        describe "from whitelisted IP addresses" do
          it "is allowed" do
            @env["REMOTE_ADDR"] = "192.168.0.5"
            @app.expects(:call).with(@env)
            @ip_middleware.call(@env)
          end
        end

        describe "from non-whitelisted IP addresses" do
          it "is allowed" do
            @env["REMOTE_ADDR"] = "4.4.4.4"
            @app.expects(:call).with(@env)
            @ip_middleware.call(@env)
          end
        end
      end
    end

    describe "tickets" do
      before { @env[path_env] = '/sharing/tickets/1234' }

      describe "from whitelisted IP addresses" do
        it "is allowed" do
          @env["REMOTE_ADDR"] = "192.168.0.5"
          @app.expects(:call).with(@env)
          @ip_middleware.call(@env)
        end
      end

      describe "from non-whitelisted IP addresses" do
        it "is allowed" do
          @env["REMOTE_ADDR"] = "4.4.4.4"
          @app.expects(:call).with(@env)
          @ip_middleware.call(@env)
        end
      end
    end
  end

  describe "using mobile app with ip restrictions enabled" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "1.1.1.1"
      @env["HTTP_USER_AGENT"] = 'Zendesk for iPad'
    end

    it "allows access ip if mobile access is enabled " do
      @account.settings.enable_ip_mobile_access = '1'
      @env["REMOTE_ADDR"] = "1.1.3.3"
      @app.expects(:call).with(@env)
      res = @ip_middleware.call(@env)
      assert_nil res
    end

    it "denys access if mobile access is not enabled" do
      @account.settings.enable_ip_mobile_access = '0'
      @env["REMOTE_ADDR"] = "2.2.2.2"
      res = @ip_middleware.call(@env)
      assert_request_blocked_with_html_response(*res)
    end

    it "denys access if mobile access is not enabled, agent ip restrictions are enabled, and user is agent" do
      @account.settings.enable_ip_mobile_access = '0'
      @account.settings.enable_agent_ip_restrictions = '1'
      @env['zendesk.user'] = users(:minimum_agent)
      @env["REMOTE_ADDR"] = "2.2.2.2"
      res = @ip_middleware.call(@env)
      assert_request_blocked_with_html_response(*res)
    end
  end

  describe "agent ip restrictions" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "1.1.1.1"
      @account.settings.enable_ip_mobile_access = '0'
    end

    describe "enabled" do
      before do
        @account.settings.enable_agent_ip_restrictions = '1'
      end
      it "allows access from non-whitelisted ip if user is not an agent " do
        @env['zendesk.user'] = users(:minimum_end_user)
        @env["REMOTE_ADDR"] = "1.1.1.3"
        @app.expects(:call).with(@env)
        res = @ip_middleware.call(@env)
        assert_nil res
      end

      it "denys access from non-whitelisted ip if user is an agent " do
        @env['zendesk.user'] = users(:minimum_agent)
        @env["REMOTE_ADDR"] = "1.1.1.3"
        res = @ip_middleware.call(@env)
        assert_request_blocked_with_html_response(*res)
      end

      it "allows access from whitelisted ip if user is an agent " do
        @env['zendesk.user'] = users(:minimum_agent)
        @env["REMOTE_ADDR"] = "1.1.1.1"
        @app.expects(:call).with(@env)
        res = @ip_middleware.call(@env)
        assert_nil res
      end
    end

    describe "not enabled" do
      before do
        @account.settings.enable_agent_ip_restrictions = '0'
      end
      it "denys access from non-whitelisted ip if user is not an agent " do
        @env['zendesk.user'] = users(:minimum_end_user)
        @env["REMOTE_ADDR"] = "1.1.1.3"
        res = @ip_middleware.call(@env)
        assert_request_blocked_with_html_response(*res)
      end

      it "denys access from non-whitelisted ip if user is an agent " do
        @env['zendesk.user'] = users(:minimum_agent)
        @env["REMOTE_ADDR"] = "1.1.1.3"
        res = @ip_middleware.call(@env)
        assert_request_blocked_with_html_response(*res)
      end

      it "allows access from whitelisted ip for any user type" do
        @env['zendesk.user'] = users(:minimum_end_user)
        @env["REMOTE_ADDR"] = "1.1.1.1"
        @app.expects(:call).with(@env)
        res = @ip_middleware.call(@env)
        assert_nil res
      end
    end
  end

  describe "#requesting_voice_assets?" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "1.1.1.1"
      @env["REMOTE_ADDR"] = "192.168.0.5"
      @method = "GET"
      @env["REQUEST_METHOD"] = @method
      @signature = 12343234234
      @env["X-Twilio-Signature"] = @signature
    end

    describe "when @uri begins with /cassets" do
      before do
        @uri = "/cassets/systEm/voice/uploads/0001/4478/Please_hold.mp3"
        @env["REQUEST_URI"] = @uri
      end

      describe "when the twilio header, the path, and the method are correct" do
        it("returns true") { @ip_middleware.send(:requesting_voice_assets?, @signature, @uri, @method).must_equal true }
        it("allows the request") { @ip_middleware.call(@env).must_equal [] }
      end

      describe "when the method is not a get" do
        before { @env["REQUEST_METHOD"] = "POST" }

        it("returns false") { @ip_middleware.send(:requesting_voice_assets?, @signature, @uri, "POST").must_equal false }
        it("disallows the request") { assert_request_blocked_with_html_response(*@ip_middleware.call(@env)) }
      end

      describe "when the REQUEST_URI is not a match" do
        before { @env["REQUEST_URI"] = "/some/other/uri" }

        it("returns false") { @ip_middleware.send(:requesting_voice_assets?, @signature, "/some/other/uri", @method).must_equal false }
        it("disallows the request") { assert_request_blocked_with_html_response(*@ip_middleware.call(@env)) }
      end

      describe "when the twilio signature is missing" do
        before { @env.delete('X-Twilio-Signature') }

        it("returns false") { @ip_middleware.send(:requesting_voice_assets?, false, @uri, @method).must_equal false }
        it("disallows the request") { assert_request_blocked_with_html_response(*@ip_middleware.call(@env)) }
      end
    end

    describe "when @uri does not begin with /cassets" do
      before do
        @uri = "/system/voice/uploads/0001/4436/Busy___Out_of_hours_message.mp3"
        @env["REQUEST_URI"] = @uri
      end

      describe "when the twilio header, the path, and the method are correct" do
        it("returns true") { @ip_middleware.send(:requesting_voice_assets?, @signature, @uri, @method).must_equal true }
        it("allows the request") { @ip_middleware.call(@env).must_equal [] }
      end

      describe "when the method is not a get" do
        before { @env["REQUEST_METHOD"] = "POST" }

        it("returns false") { @ip_middleware.send(:requesting_voice_assets?, @signature, @uri, "POST").must_equal false }
        it("disallows the request") { assert_request_blocked_with_html_response(*@ip_middleware.call(@env)) }
      end

      describe "when the REQUEST_URI is not a match" do
        before { @env["REQUEST_URI"] = "/some/other/uri" }

        it("returns false") { @ip_middleware.send(:requesting_voice_assets?, @signature, "/some/other/uri", @method).must_equal false }
        it("disallows the request") { assert_request_blocked_with_html_response(*@ip_middleware.call(@env)) }
      end

      describe "when the twilio signature is missing" do
        before { @env.delete('X-Twilio-Signature') }

        it("returns false") { @ip_middleware.send(:requesting_voice_assets?, false, @uri, @method).must_equal false }
        it("disallows the request") { assert_request_blocked_with_html_response(*@ip_middleware.call(@env)) }
      end
    end
  end

  describe "BIME export" do
    describe 'BIME standalone NAP IP' do
      before do
        @env["REMOTE_ADDR"] = "107.23.115.201"
        @account.settings.ip_restriction_enabled = '1'
        @account.texts.ip_restriction = "3.3.3.3"
      end

      describe 'calls from BIME IP addresses' do
        %w[/api/v2/incremental/tickets.json
           /api/v2/incremental/ticket_events.json
           /api/v2/incremental/users.json
           /api/v2/incremental/organizations.json].each do |path|
          it "is allowed for #{path}" do
            @env[path_env] = path
            @env["QUERY_STRING"] = "start=0"
            @app.expects(:call).with(@env)
            @ip_middleware.call(@env)
          end
        end
      end
    end

    describe 'Explore master' do
      before do
        @env["REMOTE_ADDR"] = "35.165.243.93"
        @account.settings.ip_restriction_enabled = '1'
        @account.texts.ip_restriction = "3.3.3.3"
      end

      describe 'calls from BIME IP addresses' do
        %w[/api/v2/incremental/tickets.json
           /api/v2/incremental/ticket_events.json
           /api/v2/incremental/users.json
           /api/v2/incremental/organizations.json].each do |path|
          it "is allowed for #{path}" do
            @env[path_env] = path
            @env["QUERY_STRING"] = "start=0"
            @app.expects(:call).with(@env)
            @ip_middleware.call(@env)
          end
        end
      end
    end

    describe 'Explore staging' do
      before do
        @env["REMOTE_ADDR"] = "34.193.176.172"
        @account.settings.ip_restriction_enabled = '1'
        @account.texts.ip_restriction = "3.3.3.3"
      end

      describe 'calls from BIME IP addresses' do
        %w[/api/v2/incremental/tickets.json
           /api/v2/incremental/ticket_events.json
           /api/v2/incremental/users.json
           /api/v2/incremental/organizations.json].each do |path|
          it "is allowed for #{path}" do
            @env[path_env] = path
            @env["QUERY_STRING"] = "start=0"
            @app.expects(:call).with(@env)
            @ip_middleware.call(@env)
          end
        end
      end
    end

    describe 'Explore production US' do
      before do
        @env["REMOTE_ADDR"] = "34.193.122.226"
        @account.settings.ip_restriction_enabled = '1'
        @account.texts.ip_restriction = "3.3.3.3"
      end

      describe 'calls from BIME IP addresses' do
        %w[/api/v2/incremental/tickets.json
           /api/v2/incremental/ticket_events.json
           /api/v2/incremental/users.json
           /api/v2/incremental/organizations.json].each do |path|
          it "is allowed for #{path}" do
            @env[path_env] = path
            @env["QUERY_STRING"] = "start=0"
            @app.expects(:call).with(@env)
            @ip_middleware.call(@env)
          end
        end
      end
    end

    describe 'Explore production EU' do
      before do
        @env["REMOTE_ADDR"] = "52.214.149.124"
        @account.settings.ip_restriction_enabled = '1'
        @account.texts.ip_restriction = "3.3.3.3"
      end

      describe 'calls from BIME IP addresses' do
        %w[/api/v2/incremental/tickets.json
           /api/v2/incremental/ticket_events.json
           /api/v2/incremental/users.json
           /api/v2/incremental/organizations.json].each do |path|
          it "is allowed for #{path}" do
            @env[path_env] = path
            @env["QUERY_STRING"] = "start=0"
            @app.expects(:call).with(@env)
            @ip_middleware.call(@env)
          end
        end
      end
    end
  end

  describe "gooddata export" do
    describe "from gooddata IP addresses" do
      before do
        @env["REMOTE_ADDR"] = "161.47.43.128"
        @account.settings.ip_restriction_enabled = '1'
        @account.texts.ip_restriction = "3.3.3.3"
      end

      %w[/api/v2/incremental/tickets.json
         /api/v2/incremental/ticket_events.json
         /api/v2/incremental/users.json
         /api/v2/incremental/organizations.json
         /api/v2/goodddata/tickets_export.json
         /api/v2/goodddata/account_options.json].each do |path|
        it "is allowed for #{path}" do
          @env[path_env] = path
          @env["QUERY_STRING"] = "start=0"
          @app.expects(:call).with(@env)
          @ip_middleware.call(@env)
        end
      end
    end

    describe "from non gooddata IP addresses" do
      before do
        @env["REMOTE_ADDR"] = "1.1.1.1"
        @account.settings.ip_restriction_enabled = '1'
        @account.texts.ip_restriction = "3.3.3.3"
      end
      it "denys for other apis" do
        @env[path_env] = "/api/v2/gooddata/tickets_export.json"
        @env["QUERY_STRING"] = "start=0"
        res = @ip_middleware.call(@env)
        assert_request_blocked_with_html_response(*res)
      end
    end
  end

  describe "LetsEncrypt callback" do
    before do
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "192.168.0.5"
    end

    describe "from whitelisted IP addresses" do
      before do
        @env["REMOTE_ADDR"] = "192.168.0.5"
      end

      it "is allowed" do
        @env[path_env] = '/.well-known/acme-challenge/evaGxfADs6pSRb2LAv9IZf17Dt3juxGJ-PCt92wr-oA'
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end
    end

    describe "from non-whitelisted IP addresses" do
      before do
        @env["REMOTE_ADDR"] = "3.3.3.3"
      end

      it "is allowed" do
        @env[path_env] = '/.well-known/acme-challenge/evaGxfADs6pSRb2LAv9IZf17Dt3juxGJ-PCt92wr-oA'
        @app.expects(:call).with(@env)
        @ip_middleware.call(@env)
      end

      it "requires a token" do
        @env[path_env] = '/.well-known/acme-challenge/'
        @app.expects(:call).never
        @ip_middleware.call(@env)
      end

      it "requires a valid token" do
        @env[path_env] = '/.well-known/acme-challenge/not+~base64~+url+safe'
        @app.expects(:call).never
        @ip_middleware.call(@env)
      end
    end
  end

  describe 'Connect user backfill' do
    before do
      @env["REMOTE_ADDR"] = Rails.configuration.connect_ips.first
      @account.settings.ip_restriction_enabled = '1'
      @account.texts.ip_restriction = "3.3.3.3"
    end

    describe 'calls from a Connect IP address' do
      %w[/api/v2/incremental/users.json].each do |path|
        it "is allowed for #{path}" do
          @env[path_env] = path
          @env["QUERY_STRING"] = "start=0"
          @app.expects(:call).with(@env)
          @ip_middleware.call(@env)
        end
      end
    end
  end
end
