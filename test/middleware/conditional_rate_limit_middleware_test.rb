require_relative '../support/test_helper'
require_relative '../../app/middleware/conditional_rate_limit_middleware'

SingleCov.covered!

class ConditionalRateLimitMiddleware::App
  def call(env)
    response = ActionDispatch::Response.new(422, {}, "Hello")
    response.request = ActionDispatch::Request.new(env)
    [422, {}, response]
  end
end

describe ConditionalRateLimitMiddleware do
  fixtures :accounts, :account_texts, :users

  def self.it_limits_matching_requests
    it 'limits matching requests that exceed the given threshold' do
      middleware.call(matching_env)
      statsd_client.expects(:increment).with('ratelimited', anything)

      assert_raises(Prop::RateLimited) do
        middleware.call(matching_env)
      end
    end
  end

  def self.it_allows_unmatched_requests
    it 'allows requests that dont match any limits' do
      statsd_client.expects(:increment).never
      3.times do
        middleware.call(unmatched_env)
      end
    end
  end

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_admin) }
  let(:other_user) { users(:minimum_agent) }
  let(:app) { ConditionalRateLimitMiddleware::App.new }
  let(:middleware) { ConditionalRateLimitMiddleware.new(app) }
  let(:statsd_client) { stub_for_statsd }

  let(:base_env) do
    {
      'REQUEST_METHOD'  => 'GET',
      'PATH_INFO'       => '/',
      'HTTP_USER_AGENT' => 'Ruby',
      'zendesk.account' => account,
      'zendesk.user'    => nil,
      'QUERY_STRING'    => '',
    }
  end

  before do
    account.clear_conditional_rate_limits!
    ::Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
    Timecop.freeze
  end

  describe 'no rules set for the account' do
    it 'just continues the request' do
      Prop.expects(:configurations).never

      middleware.call(base_env)
    end
  end

  describe 'a single rule for the account' do
    describe 'requests from lotus' do
      let(:matching_env) { base_env.merge('REQUEST_METHOD' => 'POST', 'HTTP_X_ZENDESK_LOTUS_VERSION' => 'v123') }
      let(:unmatched_env) { base_env }

      before do
        account.add_conditional_rate_limit(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          request_method: 'POST'
        )
      end

      it 'does not limit requests from lotus by default' do
        3.times do
          middleware.call(matching_env)
        end
      end

      it_allows_unmatched_requests

      describe 'when the rule limits a request from lotus' do
        let(:matching_env) { base_env.merge('REQUEST_METHOD' => 'POST', 'HTTP_X_ZENDESK_LOTUS_VERSION' => 'v123') }
        let(:unmatched_env) { base_env }

        before do
          account.add_conditional_rate_limit(
            key:      'limit_1',
            limit:    1,
            interval: 1,
            request_method: 'POST',
            enforce_lotus: true
          )
        end

        it_limits_matching_requests
        it_allows_unmatched_requests
      end
    end

    describe 'a request method rule' do
      let(:matching_env) { base_env.merge('REQUEST_METHOD' => 'POST') }
      let(:unmatched_env) { base_env }

      before do
        account.add_conditional_rate_limit(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          request_method: 'POST'
        )
      end

      it_limits_matching_requests
      it_allows_unmatched_requests
    end

    describe 'a specific endpoint rule' do
      let(:matching_env) { base_env.merge('PATH_INFO' => '/api/v2/users/create_many.json') }
      let(:unmatched_env) { matching_env.merge('PATH_INFO' => '/api/v2/users/create.json') }

      before do
        account.add_conditional_rate_limit(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          endpoint: 'users/create_many'
        )
      end

      it_limits_matching_requests
      it_allows_unmatched_requests
    end

    describe 'a user agent rule' do
      let(:matching_env) { base_env.merge('HTTP_USER_AGENT' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2\'') }
      let(:unmatched_env) { base_env.merge('HTTP_USER_AGENT' => 'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1') }

      before do
        account.add_conditional_rate_limit(
          key:        'limit_1',
          limit:      1,
          interval:   1,
          user_agent: 'Chrome'
        )
      end

      it_limits_matching_requests
      it_allows_unmatched_requests
    end

    describe 'a specific user rule' do
      let(:matching_env) { base_env.merge('zendesk.user' => user) }
      let(:unmatched_env) { base_env.merge('zendesk.user' => other_user) }

      before do
        account.add_conditional_rate_limit(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          user_id:  user.id.to_s
        )
      end

      it_limits_matching_requests
      it_allows_unmatched_requests
    end

    describe 'a specific query parameter rule' do
      let(:matching_env) { base_env.merge('QUERY_STRING' => 'include=translations&packages=lotus') }
      let(:unmatched_env) { base_env.merge('QUERY_STRING' => 'include=translations&packages=classic') }

      before do
        account.add_conditional_rate_limit(
          key:          'limit_1',
          limit:        1,
          interval:     1,
          query_string: 'packages=lotus'
        )
      end

      it_limits_matching_requests
      it_allows_unmatched_requests
    end
  end

  describe 'a specific ip address rule with CIDR support' do
    describe 'checks for HTTP_X_FORWARDED_FOR' do
      let(:unmatched_env) { base_env.merge('HTTP_X_FORWARDED_FOR' => '124.0.0.2') }

      before do
        account.add_conditional_rate_limit(
          key:        'limit_1',
          limit:      1,
          interval:   1,
          ip_address: '123.0.0.1/31'
        )
      end

      let(:matching_env) { base_env.merge('HTTP_X_FORWARDED_FOR' => '123.0.0.0') }
      it_limits_matching_requests
      let(:matching_env) { base_env.merge('HTTP_X_FORWARDED_FOR' => '123.0.0.1') }
      it_limits_matching_requests

      it_allows_unmatched_requests
    end

    describe 'checks for REMOTE_ADDR' do
      let(:matching_env) { base_env.merge('REMOTE_ADDR' => '123.0.0.1') }
      let(:unmatched_env) { base_env.merge('REMOTE_ADDR' => '124.0.0.1') }

      before do
        account.add_conditional_rate_limit(
          key:        'limit_1',
          limit:      1,
          interval:   1,
          ip_address: '123.0.0.1'
        )
      end

      it_limits_matching_requests
      it_allows_unmatched_requests
    end
  end

  describe 'a rule with multiple conditions' do
    let(:matching_env) { base_env.merge('REQUEST_METHOD' => 'POST', 'PATH_INFO' => '/api/v2/users/create_many.json') }
    let(:unmatched_env) { base_env.merge('REQUEST_METHOD' => 'POST', 'PATH_INFO' => '/api/v2/users/create.json') }

    before do
      account.add_conditional_rate_limit(
        key:            'limit_1',
        limit:          1,
        interval:       1,
        request_method: 'POST',
        endpoint:       '_many'
      )
    end

    it_limits_matching_requests
    it_allows_unmatched_requests
  end

  describe 'multiple rules for the account' do
    let(:env) { base_env.merge('PATH_INFO' => "/api/v2/incremental/tickets.json?start_time=#{Time.now.to_i}") }

    before do
      account.add_conditional_rate_limit(
        key:      'limit_1',
        limit:    1,
        interval: 1,
        user_id:  user.id.to_s
      )
      account.add_conditional_rate_limit(
        key:      'limit_2',
        limit:    1,
        interval: 1,
        endpoint: 'incremental/tickets'
      )
    end

    it 'it matches the correct rule and limits with its threshold' do
      middleware.call(env)

      exception = assert_raises(Prop::RateLimited) do
        middleware.call(env)
      end

      exception.message.wont_include 'limit_1'
      exception.message.must_include 'limit_2'
    end
  end

  describe 'strict_match' do
    describe 'a / endpoint rule' do
      let(:matching_env) { base_env.merge('PATH_INFO' => '/') }
      let(:unmatched_env) { matching_env.merge('PATH_INFO' => '/api/v2/users/create.json') }

      before do
        account.add_conditional_rate_limit(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          strict_match: true,
          endpoint: '/'
        )
      end

      it_limits_matching_requests
      it_allows_unmatched_requests
    end

    describe 'rule with endpoint' do
      let(:matching_env) { base_env.merge('PATH_INFO' => '/api/v2/users.json') }
      let(:unmatched_env) { matching_env.merge('PATH_INFO' => '/api/v2/users/123456.json') }

      before do
        account.add_conditional_rate_limit(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          strict_match: true,
          endpoint: '/api/v2/users.json'
        )
      end

      it_limits_matching_requests
      it_allows_unmatched_requests
    end

    describe 'a rule with multiple conditions' do
      let(:matching_env) { base_env.merge('REQUEST_METHOD' => 'GET', 'PATH_INFO' => '/api/v2/users/users.json') }
      let(:unmatched_env) { base_env.merge('REQUEST_METHOD' => 'GET', 'PATH_INFO' => '/api/v2/users/123456.json') }

      before do
        account.add_conditional_rate_limit(
          key:            'limit_1',
          limit:          1,
          interval:       1,
          strict_match:   true,
          request_method: 'GET',
          endpoint:       '/api/v2/users/users.json'
        )
      end

      it_limits_matching_requests
      it_allows_unmatched_requests
    end
  end
end
