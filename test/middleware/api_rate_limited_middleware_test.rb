require_relative "../support/test_helper"
require_relative "../../app/middleware/api_rate_limited_middleware"

SingleCov.covered!

describe "ApiRateLimitedMiddleware Integration" do
  class ApiRateLimitedMiddlewareTestController < ActionController::Base
    def blow_up
      Prop.throttle!(:rule_pagination, 'x', increment: 99999)
    end
  end
  prepend ControllerDefaultParams
  integrate_test_routes ApiRateLimitedMiddlewareTestController

  # not sure how to test this without testing it's side-effects see
  # https://github.com/zendesk/zendesk_shared_session/blob/master/lib/zendesk_shared_session/middleware/session_persistence.rb
  def assert_session_persistence_skipped(yes)
    stub = Account.any_instance.expects(:help_center_enabled?)
    yes ? stub.with { raise }.never : stub.returns(false)
  end

  fixtures :accounts, :users, :role_settings

  let(:rate_headers) do
    interesting = response.headers.select { |k, _v| k =~ /Rate-Limit|Cache-Control/ }
    interesting["Cache-Control"] = interesting["Cache-Control"].sub(/\d+/, "111")
    interesting
  end

  it "renders 429 response when rate limit was triggered in regular controller" do
    get("https://support.zendesk-test.com/test/route/api_rate_limited_middleware_test/blow_up")
    assert_response 429
    rate_headers.must_equal("Cache-Control" => "no-cache")
  end

  describe "when hitting account rate limit and not going into the controller" do
    before do
      login users(:minimum_end_user)
      assert Prop.throttle(:api_requests, 1, increment: 99999)
      Account.any_instance.expects(:api_rate_limit).returns(10)
    end

    it "adds rate-limit + cache headers" do
      get("https://support.zendesk-test.com/api/v2/users/me.json") # need an api endpoint for ZendeskApi::Limiter check
      rate_headers.must_equal(
        "X-Rate-Limit-Remaining" => 0,
        "X-Rate-Limit" => "10",
        "Cache-Control" => "max-age=111,public"
      )
    end

    it "skips session persistence" do
      assert_session_persistence_skipped true
      get("https://support.zendesk-test.com/api/v2/users/me.json")
    end
  end
end
