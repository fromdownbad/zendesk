# frozen_string_literal: true

require_relative "../support/test_helper"

SingleCov.covered!

describe MobileSdkApiRedirector do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:metadata) do
    {
      'PATH_INFO' => '/api/mobile/sdk/settings/12345.json',
      'zendesk.account' => account
    }
  end
  let(:env) { ActiveSupport::HashWithIndifferentAccess.new(metadata) }
  let(:app) { proc { |_env| [200, {}, []] } }
  let(:middleware) { MobileSdkApiRedirector.new(app) }
  let(:request) { {} }

  describe '#call' do
    describe_with_arturo_enabled :mobile_sdk_api_redirector do
      it 'returns 200 with X-Accel-Redirect header' do
        assert_equal middleware.send(:call, env).second['X-Accel-Redirect'], '/mobile_sdk_api/v1/settings/12345.json'
      end

      it 'returns 200 with Cache-Control headers' do
        assert_equal middleware.send(:call, env).second['Cache-Control'], 'max-age=600, public'
      end

      it 'returns 200 without X-Accel-Redirect header if not settings call' do
        env = ActiveSupport::HashWithIndifferentAccess.new(metadata.merge('PATH_INFO' => '/api/does/not/exist/abcd.json'))
        assert_nil middleware.send(:call, env).second['X-Accel-Redirect']
      end

      it 'will skip session persistance' do
        middleware.send(:call, env)
        refute env['zendesk.session.persistence']
      end
    end
  end

  describe '#mobile_sdk_api_request?' do
    describe 'when the request path is in PATHS' do
      it 'returns true' do
        path = '/api/mobile/sdk/settings/1234.json'
        assert_equal middleware.send(:mobile_sdk_api_request?, path), true
      end

      it "sets @new_path by replacing the original path's prefix with Mobile SDK APi's" do
        path = '/api/mobile/sdk/settings/1234.json'
        middleware.send(:mobile_sdk_api_request?, path)
        assert_equal middleware.instance_variable_get(:@new_path), '/mobile_sdk_api/v1/settings/1234.json'
      end
    end

    describe 'when the request path is not in PATHS' do
      it 'returns false' do
        path = '/api/does/not/exist/abc.json'
        assert_equal middleware.send(:mobile_sdk_api_request?, path), false
      end

      it "sets @new_path by replacing the original path's prefix with Mobile SDK APi's" do
        path = '/api/does/not/exist/1234.json'
        middleware.send(:mobile_sdk_api_request?, path)
        assert_nil middleware.instance_variable_get(:@new_path)
      end
    end
  end
end
