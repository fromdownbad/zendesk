require_relative "../support/test_helper"

SingleCov.covered!

describe 'RuleRoutingMiddleware' do
  before do
    @app    = proc { |_env| [200, { }, ["Hi"]] }
    @router = RuleRoutingMiddleware.new(@app)
    @env    = Rack::MockRequest.env_for('/')
  end

  %w[automations triggers views].each do |rule|
    it "routes to the correct rule path" do
      @env['PATH_INFO'] = '/rules'
      @env['QUERY_STRING'] = "filter=#{rule}"
      assert @router.rule_path?(@env)
      @router.call(@env)

      assert_equal "/rules/#{rule}", @env['PATH_INFO']
      assert_equal "filter=#{rule}", @env['QUERY_STRING']
    end
  end

  it "does not route to a rule path" do
    @env['PATH_INFO'] = '/'
    refute @router.rule_path?(@env)
    @router.call(@env)

    assert_equal '/', @env['PATH_INFO']
  end

  describe "rule_path?" do
    it "handles questionable paths gracefully" do
      @env['PATH_INFO'] = '/rules'
      assert(@router.rule_path?(@env))
      assert_equal 200, @router.call(@env)[0]

      @env['PATH_INFO'] = '/rules.json'
      assert(@router.rule_path?(@env))
      assert_equal 404, @router.call(@env)[0]
    end

    it "trues for routes that lead to the rules controller" do
      @env['PATH_INFO'] = '/rules/3'
      assert(@router.rule_path?(@env))

      @env['PATH_INFO'] = '/rules'
      @env['QUERY_STRING'] = 'filter=views'
      assert(@router.rule_path?(@env))

      @env['PATH_INFO'] = '/rules/search'
      @env['QUERY_STRING'] = ''
      assert(@router.rule_path?(@env))

      @env['PATH_INFO'] = '/rules/sort'
      assert(@router.rule_path?(@env))
    end

    it "is false for routes that don't lead to the rules controller" do
      @env['PATH_INFO'] = '/rules/views'
      assert_equal false, @router.rule_path?(@env)

      @env['PATH_INFO'] = '/rules/views/1'
      assert_equal false, @router.rule_path?(@env)

      @env['PATH_INFO'] = '/rules/automations'
      assert_equal false, @router.rule_path?(@env)

      @env['PATH_INFO'] = '/rules/triggers'
      assert_equal false, @router.rule_path?(@env)

      @env['PATH_INFO'] = '/rules/1/tickets'
      assert_equal false, @router.rule_path?(@env)

      @env['PATH_INFO'] = '/rules/count'
      assert_equal false, @router.rule_path?(@env)

      @env['PATH_INFO'] = '/rules/analysis'
      assert_equal false, @router.rule_path?(@env)

      @env['PATH_INFO'] = 'api/v2/rules'
      assert_equal false, @router.rule_path?(@env)
    end
  end
end
