require_relative '../support/test_helper'
require_relative '../../app/middleware/datadog_middleware'

SingleCov.covered!

class DatadogMiddleware::App
  def call(env)
    response = ActionDispatch::Response.new(422, {}, "Hey")
    response.request = ActionDispatch::Request.new(env)
    [422, {}, response]
  end
end

describe DatadogMiddleware do
  # This method exists because of the way base `before` blocks work. Since they are called in the
  # order they are added, it makes it hard to put `middleware.call` in our top-level before block,
  # but allow tests to manipulate things before it's called. To get around this, we use `let`, so a
  # block can lazily be evaluated right before the middleware call (see line 43). The method is just
  # for a nice name and nicer syntax.
  def self.before_middleware_call(&block)
    let(:before_middleware_call_block, &block)
  end

  let(:middleware) { DatadogMiddleware.new(DatadogMiddleware::App.new) }
  let(:span) { Datadog::Span.new(Datadog.tracer, 'test-span') }
  let(:span_tags) { span.to_hash.fetch(:meta) }
  let(:env) { { 'rack.input' => '' }.merge(extra_env) }
  let(:extra_env) { {} }
  let(:before_middleware_call_block) {}

  describe 'when no span is available' do
    it 'does nothing' do
      span.expects(:set_tag).never

      middleware.call(env)
    end
  end

  describe 'when a span is available' do
    before do
      Datadog.tracer.stubs(active_span: span)
      before_middleware_call_block
      middleware.call(env)
    end

    it 'sets expected tags' do
      missing = %w[
        application.version
        application.revision
        application.github_tree
        rails.request_id
        rails.version
        ruby.version
        http.trace_id
        zendesk.account_id
        zendesk.account_subdomain
        zendesk.brand_id
        zendesk.brand_subdomain
        zendesk.user_id
        zendesk.user_role
        zendesk.lotus_version
        zendesk.lotus_feature
        zendesk.app_id
        zendesk.app_name
        zendesk.request_source
        zendesk.pod_relay
        zendesk.mobile.client
        zendesk.mobile.client_version
        zendesk.mobile.integration
        zendesk.mobile.integration_version
        zendesk.doorman.time_spent
        zendesk.doorman.bypassed
        request_ip
        http.request.headers.referer
        http.request.params.sort_by
        http.request.params.sort_order
        warden.winning_strategy
      ] - span.to_hash.fetch(:meta).keys

      assert_empty(missing, "Span tags missing. Expected tags to include: #{missing.join(',')}")
    end

    describe 'user tagging' do
      let(:user) { users(:minimum_agent) }
      let(:extra_env) { { 'zendesk.user' => user } }

      it 'sets user tags' do
        assert_equal user.id.to_s,   span.get_tag('zendesk.user_id')
        assert_equal user.role.to_s, span.get_tag('zendesk.user_role')
      end
    end

    describe 'with a request time header added by NGINX' do
      let(:extra_env) do
        time = Time.now
        span.start_time = time + 10.seconds

        { 'HTTP_X_REQUEST_START' => "t=#{time.to_f}" }
      end

      it 'adds a queueing time metric' do
        assert_equal '10.0', span.get_tag('http.request.queueing_delay')
      end
    end

    describe 'with a lotus tab id header added by lotus' do
      let(:user) { users(:minimum_agent) }
      let(:extra_env) { { 'HTTP_X_ZENDESK_LOTUS_TAB_ID' => 'RANDOM_UUID_8888' } }

      it 'sets the tag for the tab_id reported by lotus' do
        assert_equal 'RANDOM_UUID_8888', span.get_tag('zendesk.lotus_tab_id')
      end
    end

    describe 'with a lotus uuid header added by lotus' do
      let(:user) { users(:minimum_agent) }

      describe 'reporting the lotus uuid' do
        let(:extra_env) { { 'HTTP_X_ZENDESK_LOTUS_UUID' => 'RANDOM_UUID_1234' } }

        it 'sets the tag for the uuid reported by lotus' do
          assert_equal 'RANDOM_UUID_1234', span.get_tag('zendesk.lotus_uuid')
        end
      end

      describe 'reporting the lotus expiry_refetch' do
        let(:extra_env) { { 'HTTP_X_ZENDESK_LOTUS_EXPIRY_REFETCH' => 'RANDOM_EXPIRE_REFETCH_1234' } }

        it 'sets the tag for the uuid reported by lotus' do
          assert_equal 'RANDOM_EXPIRE_REFETCH_1234', span.get_tag('zendesk.lotus_expiry_refetch')
        end
      end

      describe 'reporting the force_exception_locale header' do
        let(:extra_env) { { 'HTTP_X_FORCE_EXCEPTION_LOCALE' => 'true' } }

        it 'sets the tag' do
          assert_equal 'true', span.get_tag('zendesk.force_exception_locale')
        end
      end

      describe 'reporting a mismatch' do
        before_middleware_call do
          user.expects(:id).returns(1234).at_least_once
          Rails.cache.write("zendesk.lotus_uuid-1", 'RANDOM_UUID_1235')
        end

        let(:extra_env) do
          {
            'HTTP_X_ZENDESK_LOTUS_INITIAL_USER_ID' => '1234',
            'zendesk.user' => user,
            'zendesk.shared_session' => { 'id' => 1 }
          }
        end

        it 'sets zendesk.lotus_uuid_id_mismatch to true' do
          assert_equal 'true', span.get_tag('zendesk.lotus_uuid_mismatch')
          assert_equal 'mismatch', span.get_tag('zendesk.lotus_uuid_mismatch_details')
        end
      end

      describe 'reporting radar socket and status' do
        let(:extra_env) do
          {
            'HTTP_X_ZENDESK_RADAR_SOCKET' => 'abc123',
            'HTTP_X_ZENDESK_RADAR_STATUS' => 'activated'
          }
        end

        it 'sets radar tags' do
          assert_equal 'abc123', span.get_tag('zendesk.lotus_radar_client_socket')
          assert_equal 'activated', span.get_tag('zendesk.lotus_radar_client_status')
        end
      end
    end

    describe 'with an initial user role header added by lotus' do
      let(:user) { users(:minimum_agent) }
      let(:extra_env) do
        {
          'HTTP_X_ZENDESK_LOTUS_INITIAL_USER_ROLE' => 'admin',
          'zendesk.user' => user
        }
      end

      it 'sets the tag to the user id reported by lotus' do
        assert_equal 'admin', span.get_tag('zendesk.lotus_initial_user_role')
      end
    end

    describe 'report whether request is from assumed user' do
      let(:user) { users(:minimum_agent) }

      describe 'when assuming another user' do
        let(:extra_env) { { 'zendesk.shared_session' => { 'auth_original_user_id' => 123456 } } }

        it 'sets the tag for whether user is assumed' do
          assert_equal 'true', span.get_tag('zendesk.is_assuming_user')
        end

        it 'sets the tag for the original user id' do
          assert_equal '123456', span.get_tag('zendesk.original_user_id')
        end
      end

      describe 'when not assuming another user' do
        it 'sets the tag for whether user is assumed to false' do
          assert_equal 'false', span.get_tag('zendesk.is_assuming_user')
        end

        it 'sets the tag for the original user id to N/A' do
          assert_equal 'N/A', span.get_tag('zendesk.original_user_id')
        end
      end
    end

    describe 'with an initial user header added by lotus' do
      let(:user) { users(:minimum_agent) }
      let(:extra_env) do
        {
          'HTTP_X_ZENDESK_LOTUS_INITIAL_USER_ID' => '1234',
          'zendesk.user' => user
        }
      end

      it 'sets the tag to the user id reported by lotus' do
        assert_equal '1234', span.get_tag('zendesk.lotus_initial_user_id')
      end

      describe 'reporting mismatches' do
        describe 'if the initial user is the same' do
          before_middleware_call do
            user.expects(:id).returns(1234).at_least_once
          end

          it 'sets zendesk.lotus_user_id_mismatch to false' do
            assert_equal 'false', span.get_tag('zendesk.lotus_user_id_mismatch')
            assert_equal 'no_mismatch', span.get_tag('zendesk.lotus_user_id_mismatch_details')
          end
        end

        describe 'if the initial user is not the same' do
          it 'sets zendesk.lotus_user_id_mismatch to true' do
            assert_equal 'true', span.get_tag('zendesk.lotus_user_id_mismatch')
            assert_equal 'mismatch', span.get_tag('zendesk.lotus_user_id_mismatch_details')
          end
        end

        describe 'if the initial user is nil' do
          let(:extra_env) { { 'zendesk.user' => user } }

          it 'sets zendesk.lotus_user_id_mismatch to true' do
            assert_equal 'true', span.get_tag('zendesk.lotus_user_id_mismatch')
            assert_equal 'no_lotus_initial_user_id', span.get_tag('zendesk.lotus_user_id_mismatch_details')
          end
        end

        describe 'if both are nil' do
          let(:extra_env) { {} }

          it 'sets zendesk.lotus_user_id_mismatch to true' do
            assert_equal 'false', span.get_tag('zendesk.lotus_user_id_mismatch')
            assert_equal 'both_blank', span.get_tag('zendesk.lotus_user_id_mismatch_details')
          end
        end

        describe 'if the zendesk user is nil' do
          let(:extra_env) { { 'HTTP_X_ZENDESK_LOTUS_INITIAL_USER_ID' => '1234' } }

          it 'sets zendesk.lotus_user_id_mismatch to true' do
            assert_equal 'true', span.get_tag('zendesk.lotus_user_id_mismatch')
            assert_equal 'no_user_id', span.get_tag('zendesk.lotus_user_id_mismatch_details')
          end
        end
      end
    end

    describe 'tags that should not have a default value' do
      describe 'and an account is present' do
        let(:account) { accounts(:minimum) }
        let(:extra_env) { { 'zendesk.account' => account } }

        before_middleware_call do
          @old_db_shards_to_clusters = DB_SHARDS_TO_CLUSTERS
          Object.send(:remove_const, :DB_SHARDS_TO_CLUSTERS)
          Object.const_set(:DB_SHARDS_TO_CLUSTERS, account.shard_id => '1.1')
        end

        after do
          Object.send(:remove_const, :DB_SHARDS_TO_CLUSTERS)
          Object.const_set(:DB_SHARDS_TO_CLUSTERS, @old_db_shards_to_clusters)
        end

        it 'sets `zendesk.db.shard`' do
          assert_equal account.shard_id.to_s, span.get_tag('zendesk.db.shard')
        end

        it 'sets `zendesk.db.cluster`' do
          assert_equal '1.1', span.get_tag('zendesk.db.cluster')
        end

        it 'sets `zendesk.account_is_sandbox`' do
          assert_equal "false", span.get_tag('zendesk.account_is_sandbox')
        end

        it 'sets `zendesk.account_is_trial`' do
          assert_equal "false", span.get_tag('zendesk.account_is_trial')
        end

        it 'sets `zendesk.account_premier_support`' do
          assert_equal "false", span.get_tag('zendesk.account_premier_support')
        end

        it 'sets `zendesk.account_features.agent_workspace`' do
          assert_equal "false", span.get_tag('zendesk.account_features.agent_workspace')
        end
      end

      describe 'and an account is not present' do
        it 'does not set `zendesk.db.shard`' do
          refute_includes span_tags, 'zendesk.db.shard'
        end

        it 'does not set `zendesk.db.cluster`' do
          refute_includes span_tags, 'zendesk.db.cluster'
        end
      end

      describe 'and the `page` param is present' do
        let(:extra_env) { { 'QUERY_STRING' => 'page=5' } }

        it 'sets `http.request.params.page`' do
          assert_equal '5', span.get_tag('http.request.params.page')
        end

        describe 'unexpected value types' do
          describe 'an array' do
            let(:extra_env) { { 'QUERY_STRING' => 'page[]=1' } }

            it 'ignores it' do
              assert_nil span.get_tag('http.request.params.page')
            end
          end

          describe 'a hash' do
            let(:extra_env) { { 'QUERY_STRING' => 'page[foo]=1' } }

            it 'ignores it' do
              assert_nil span.get_tag('http.request.params.page')
            end
          end

          describe 'nil' do
            let(:extra_env) { { 'QUERY_STRING' => 'page=' } }

            it 'ignores it' do
              assert_nil span.get_tag('http.request.params.page')
            end
          end
        end
      end

      # CBP v2:
      #   * page[size]=5
      #   * page[size]=5&page[after]=xxx
      #   * page[size]=5&page[before]=yyy
      #   * page[size]=5&sort=created_at
      #
      # To separate things from offset pagination, we add the tags
      # using cursor[size], cursor[before] and cursor[after].
      describe 'and the `page` param is present as a Hash' do
        let(:params) { 'page[size]=5&page[after]=abc&page[before]=def' }
        let(:extra_env) { { 'QUERY_STRING' => params } }

        it 'sets `http.request.params.cursor.size`' do
          assert_equal '5', span.get_tag('http.request.params.cursor.size')
        end

        it 'sets `http.request.params.cursor.after`' do
          assert_equal 'abc', span.get_tag('http.request.params.cursor.after')
        end

        it 'sets `http.request.params.cursor.before`' do
          assert_equal 'def', span.get_tag('http.request.params.cursor.before')
        end

        it 'does not set `http.request.params.page`' do
          refute_includes span_tags, 'http.request.params.page'
        end

        describe 'that only contains size' do
          let(:params) { 'page[size]=5' }

          it 'sets `http.request.params.cursor.size`' do
            assert_equal '5', span.get_tag('http.request.params.cursor.size')
          end

          it 'does not set `http.request.params.cursor.after`' do
            refute_includes span_tags, 'http.request.params.cursor.after'
          end

          it 'does not set `http.request.params.cursor.before`' do
            refute_includes span_tags, 'http.request.params.cursor.before'
          end

          it 'does not set `http.request.params.sort`' do
            refute_includes span_tags, 'http.request.params.sort'
          end
        end

        describe 'that includes custom sort' do
          let(:params) { 'page[size]=5&sort=created_at' }

          it 'sets `http.request.params.cursor.size`' do
            assert_equal '5', span.get_tag('http.request.params.cursor.size')
          end

          it 'sets `http.request.params.sort`' do
            assert_equal 'created_at', span.get_tag('http.request.params.sort')
          end
        end

        describe 'with an unexpected key' do
          let(:params) { 'page[foo]=1' }

          it 'is ignored' do
            refute_includes span_tags, 'http.request.params.page.foo'
            refute_includes span_tags, 'http.request.params.cursor.foo'
          end
        end
      end

      describe 'and the `page` param is not present' do
        it 'does not set `http.request.params.page`' do
          refute_includes span_tags, 'http.request.params.page'
        end
      end

      describe 'and the `per_page` param is present' do
        let(:extra_env) { { 'QUERY_STRING' => 'per_page=25' } }

        it 'sets `http.request.params.per_page`' do
          assert_equal '25', span.get_tag('http.request.params.per_page')
        end

        describe 'unexpected value types' do
          describe 'an array' do
            let(:extra_env) { { 'QUERY_STRING' => 'per_page[]=1' } }

            it 'ignores it' do
              assert_nil span.get_tag('http.request.params.per_page')
            end
          end

          describe 'a hash' do
            let(:extra_env) { { 'QUERY_STRING' => 'per_page[foo]=1' } }

            it 'ignores it' do
              assert_nil span.get_tag('http.request.params.per_page')
            end
          end

          describe 'nil' do
            let(:extra_env) { { 'QUERY_STRING' => 'per_page=' } }

            it 'ignores it' do
              assert_nil span.get_tag('http.request.params.per_page')
            end
          end
        end
      end

      describe 'and the `per_page` param is not present' do
        it 'does not set `http.request.params.per_page`' do
          refute_includes span_tags, 'http.request.params.per_page'
        end
      end

      describe 'and the `include` param is present' do
        let(:extra_env) { { 'QUERY_STRING' => 'include=users,brands' } }

        it 'sets `http.request.params.include`' do
          assert_includes span_tags, 'http.request.params.include'
        end

        it 'sorts multiple includes' do
          assert_equal 'brands,users', span.get_tag('http.request.params.include')
        end
      end

      describe 'and the `include` param is not present' do
        it 'does not set `http.request.params.include`' do
          refute_includes span_tags, 'http.request.params.include'
        end
      end

      describe 'and `idempotency_hit` is true' do
        let(:extra_env) { { 'zendesk.idempotency_hit' => true } }

        it 'sets `zendesk.idempotency_hit`' do
          assert_equal 'true', span.get_tag('zendesk.idempotency_hit')
        end
      end

      describe 'and `idempotency_hit` is false' do
        let(:extra_env) { { 'zendesk.idempotency_hit' => false } }

        it 'sets `zendesk.idempotency_hit`' do
          assert_equal 'false', span.get_tag('zendesk.idempotency_hit')
        end
      end

      describe 'and `idempotency_hit` is not present' do
        it 'does not set `idempotency_hit`' do
          refute_includes span_tags, 'zendesk.idempotency_hit'
        end
      end
    end

    describe 'rate limit headers' do
      describe 'and an account is present' do
        let(:account) { accounts(:minimum) }
        let(:extra_env) { { 'zendesk.account' => account } }

        describe 'for the very first request' do
          it 'set count based facet tags to default values' do
            assert_equal "700", span.get_tag('http.rate_limit.api_requests.limit')
            assert_equal "700", span.get_tag('http.rate_limit.api_requests.remaining')
            assert_equal "0", span.get_tag('http.rate_limit.api_requests.used')
            assert_equal "0", span.get_tag('http.rate_limit.api_requests.used_percentage')
          end

          it 'set time based facet tags to default values' do
            assert_equal "600", span.get_tag('http.rate_limit.api_time.limit')
            assert_equal "600", span.get_tag('http.rate_limit.api_time.remaining')
            assert_equal "0", span.get_tag('http.rate_limit.api_time.used')
            assert_equal "0", span.get_tag('http.rate_limit.api_time.used_percentage')
          end
        end

        describe 'for the 350th request' do
          before_middleware_call do
            Prop.throttle!(:api_requests, account.id, increment: 350)
            Prop.throttle!(:api_time, account.id, increment: 200)
          end

          it 'set count based facet tags to specific values' do
            assert_equal "700", span.get_tag('http.rate_limit.api_requests.limit')
            assert_equal "350", span.get_tag('http.rate_limit.api_requests.remaining')
            assert_equal "350", span.get_tag('http.rate_limit.api_requests.used')
            assert_equal "50", span.get_tag('http.rate_limit.api_requests.used_percentage')
          end

          it 'set time based facet tags to default values' do
            assert_equal "600", span.get_tag('http.rate_limit.api_time.limit')
            assert_equal "400", span.get_tag('http.rate_limit.api_time.remaining')
            assert_equal "200", span.get_tag('http.rate_limit.api_time.used')
            assert_equal "33", span.get_tag('http.rate_limit.api_time.used_percentage')
          end
        end

        describe 'when account settings return nil' do
          let(:account) do
            Account.any_instance.stubs(:api_rate_limit).returns(nil)
            Account.any_instance.stubs(:api_time_rate_limit).returns(nil)
            accounts(:minimum)
          end

          it 'does not set count based facet tags' do
            refute_includes span_tags, 'http.rate_limit.api_requests.limit'
            refute_includes span_tags, 'http.rate_limit.api_requests.remaining'
            refute_includes span_tags, 'http.rate_limit.api_requests.used'
            refute_includes span_tags, 'http.rate_limit.api_requests.used_percentage'
          end

          it 'does not set time based facet tags' do
            refute_includes span_tags, 'http.rate_limit.api_time.limit'
            refute_includes span_tags, 'http.rate_limit.api_time.remaining'
            refute_includes span_tags, 'http.rate_limit.api_time.used'
            refute_includes span_tags, 'http.rate_limit.api_time.used_percentage'
          end
        end
      end

      describe 'and an account is not present' do
        it 'does not set count based facet tags' do
          refute_includes span_tags, 'http.rate_limit.api_requests.limit'
          refute_includes span_tags, 'http.rate_limit.api_requests.remaining'
          refute_includes span_tags, 'http.rate_limit.api_requests.used'
          refute_includes span_tags, 'http.rate_limit.api_requests.used_percentage'
        end

        it 'does not set time based facet tags' do
          refute_includes span_tags, 'http.rate_limit.api_time.limit'
          refute_includes span_tags, 'http.rate_limit.api_time.remaining'
          refute_includes span_tags, 'http.rate_limit.api_time.used'
          refute_includes span_tags, 'http.rate_limit.api_time.used_percentage'
        end
      end
    end

    describe 'with a doorman time header added by NGINX' do
      let(:doorman_time_spent_value) { "3" }
      let(:extra_env) do
        { 'HTTP_X_ZENDESK_DOORMAN_TIME' => doorman_time_spent_value }
      end

      it 'adds a doorman time spent metric' do
        assert_equal doorman_time_spent_value, span.get_tag('zendesk.doorman.time_spent')
      end

      it 'adds a doorman bypassed metric' do
        assert_equal "false", span.get_tag('zendesk.doorman.bypassed')
      end
    end

    describe 'without a doorman time header added by NGINX' do
      it 'adds a doorman time spent metric' do
        assert_equal '0', span.get_tag('zendesk.doorman.time_spent')
      end

      it 'adds a doorman bypassed metric' do
        assert_equal "true", span.get_tag('zendesk.doorman.bypassed')
      end
    end

    describe 'with doorman auth response code added by NGINX' do
      let (:doorman_response_code) { '200' }
      let(:extra_env) do
        { 'HTTP_X_ZENDESK_DOORMAN_AUTH_RESPONSE' => doorman_response_code }
      end

      it 'adds a doorman time spent metric' do
        assert_equal doorman_response_code, span.get_tag('zendesk.doorman.auth_response_code')
      end
    end

    describe 'without a doorman time header added by NGINX' do
      it 'adds a doorman time spent metric' do
        assert_equal 'N/A', span.get_tag('zendesk.doorman.auth_response_code')
      end
    end

    describe 'with a blank doorman time header added by NGINX' do
      let(:extra_env) do
        { 'HTTP_X_ZENDESK_DOORMAN_TIME' => '' }
      end
      it 'adds a doorman time spent metric' do
        assert_equal '0', span.get_tag('zendesk.doorman.time_spent')
      end

      it 'adds a doorman bypassed metric' do
        assert_equal "true", span.get_tag('zendesk.doorman.bypassed')
      end
    end
  end

  describe '#correlate_logs' do
    before { Datadog.tracer.stubs(active_span: span) }

    describe 'lotus data' do
      describe 'when `lotus_version` is set' do
        let(:extra_env) { { 'HTTP_X_ZENDESK_LOTUS_VERSION' => 'testversion' } }

        it 'logs with the correct tags' do
          Rails.logger.expects(:append_attributes).with(
            dd: {
              trace_id: span.trace_id,
              span_id: span.span_id
            },
            zendesk: {
              lotus: {
                version: 'testversion',
                app_id: nil
              }
            }
          ).at_least_once

          middleware.call(env)
        end
      end

      describe 'when `app_id` is set' do
        let(:extra_env) { { 'HTTP_X_ZENDESK_APP_ID' => 'test_app_id' } }

        it 'logs with the correct tags' do
          Rails.logger.expects(:append_attributes).with(
            dd: {
              trace_id: span.trace_id,
              span_id: span.span_id
            },
            zendesk: {
              lotus: {
                version: nil,
                app_id: 'test_app_id'
              }
            }
          ).at_least_once

          middleware.call(env)
        end
      end

      describe 'when `lotus_version` and `app_id` are set' do
        let(:extra_env) do
          {
            'HTTP_X_ZENDESK_APP_ID' => 'test_app_id',
            'HTTP_X_ZENDESK_LOTUS_VERSION' => 'testversion'
          }
        end

        it 'logs with the correct tags' do
          Rails.logger.expects(:append_attributes).with(
            dd: {
              trace_id: span.trace_id,
              span_id: span.span_id
            },
            zendesk: {
              lotus: {
                version: 'testversion',
                app_id: 'test_app_id'
              }
            }
          ).at_least_once

          middleware.call(env)
        end
      end

      describe 'when `lotus_version` and `app_id` are not set' do
        it 'logs with the correct tags' do
          Rails.logger.expects(:append_attributes).with(
            dd: {
              trace_id: span.trace_id,
              span_id: span.span_id
            }
          ).at_least_once

          middleware.call(env)
        end
      end
    end

    describe 'when `account` is set' do
      let(:account) { accounts(:minimum) }
      let(:extra_env) { { 'zendesk.account' => account } }

      it 'logs with the correct tags' do
        Rails.logger.expects(:append_attributes).with(
          dd: {
            trace_id: span.trace_id,
            span_id: span.span_id
          },
          zendesk: {
            account_id: account.id.to_i,
            account_subdomain: account.subdomain,
            account_created_at: account.created_at.to_date.to_s(:db),
            account_premier_support: false,
            account_age: (Date.today - account.created_at.to_date).to_i,
            whitelisted_from_fraud_restrictions: account.settings.whitelisted_from_fraud_restrictions
          }
        ).at_least_once

        middleware.call(env)
      end
    end

    describe 'when `account` is not set' do
      it 'logs with the correct tags' do
        Rails.logger.expects(:append_attributes).with(
          dd: {
            trace_id: span.trace_id,
            span_id: span.span_id
          }
        ).at_least_once

        middleware.call(env)
      end
    end

    describe 'mobile data' do
      describe 'when the request is from a mobile source' do
        describe 'and the client version is set' do
          let(:extra_env) do
            {
              'HTTP_X_ZENDESK_CLIENT' => 'mobile/android/apps/support',
              'HTTP_X_ZENDESK_CLIENT_VERSION' => 'testversion'
            }
          end

          it 'logs with the correct tags' do
            Rails.logger.expects(:append_attributes).with(
              dd: {
                trace_id: span.trace_id,
                span_id: span.span_id
              },
              zendesk: {
                mobile: {
                  client: 'mobile/android/apps/support',
                  client_version: 'testversion',
                  integration: nil,
                  integration_version: nil
                }
              }
            ).at_least_once

            middleware.call(env)
          end
        end

        describe 'and the client integration is set' do
          let(:extra_env) do
            {
              'HTTP_X_ZENDESK_CLIENT' => 'mobile/android/apps/support',
              'HTTP_X_ZENDESK_INTEGRATION' => 'testintegration'
            }
          end

          it 'logs with the correct tags' do
            Rails.logger.expects(:append_attributes).with(
              dd: {
                trace_id: span.trace_id,
                span_id: span.span_id
              },
              zendesk: {
                mobile: {
                  client: 'mobile/android/apps/support',
                  client_version: nil,
                  integration: 'testintegration',
                  integration_version: nil
                }
              }
            ).at_least_once

            middleware.call(env)
          end
        end

        describe 'and the client integration version is set' do
          let(:extra_env) do
            {
              'HTTP_X_ZENDESK_CLIENT' => 'mobile/android/apps/support',
              'HTTP_X_ZENDESK_INTEGRATION_VERSION' => 'testversion'
            }
          end

          it 'logs with the correct tags' do
            Rails.logger.expects(:append_attributes).with(
              dd: {
                trace_id: span.trace_id,
                span_id: span.span_id
              },
              zendesk: {
                mobile: {
                  client: 'mobile/android/apps/support',
                  client_version: nil,
                  integration: nil,
                  integration_version: 'testversion'
                }
              }
            ).at_least_once

            middleware.call(env)
          end
        end

        describe 'and no additional details are set' do
          let(:extra_env) do
            {
              'HTTP_X_ZENDESK_CLIENT' => 'mobile/android/apps/support',
            }
          end

          it 'logs with the correct tags' do
            Rails.logger.expects(:append_attributes).with(
              dd: {
                trace_id: span.trace_id,
                span_id: span.span_id
              },
              zendesk: {
                mobile: {
                  client: 'mobile/android/apps/support',
                  client_version: nil,
                  integration: nil,
                  integration_version: nil
                }
              }
            ).at_least_once

            middleware.call(env)
          end
        end
      end

      describe 'when the request is not from a mobile source' do
        it 'logs with the correct tags' do
          Rails.logger.expects(:append_attributes).with(
            dd: {
              trace_id: span.trace_id,
              span_id: span.span_id
            }
          ).at_least_once

          middleware.call(env)
        end
      end
    end
  end
end
