require_relative "../support/test_helper"
require_relative "../../app/middleware/account_rate_limit_middleware"

SingleCov.covered!

class AccountRateLimitMiddleware::App
  def call(_env)
    response = ActionDispatch::Response.new(422, {}, "Hey")
    [422, {}, response]
  end
end

describe "AccountRateLimitMiddleware Integration" do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:metadata) do
    {
      'request_id' => 'asdf1234',
      'rack.session.options' => { skip: nil },
      'zendesk.account' => account
    }
  end
  let(:env) { ActiveSupport::HashWithIndifferentAccess.new(metadata) }
  let(:app) { AccountRateLimitMiddleware::App.new }

  describe "When account has classic_short_circuit_limiter" do
    before do
      Arturo.enable_feature!(:classic_short_circuit_limiter)
    end

    it "returns 429 when account has Arturo feature" do
      response = AccountRateLimitMiddleware.new(app).call(env)

      response[0].must_equal 429
    end

    it "returns header with cache control" do
      response = AccountRateLimitMiddleware.new(app).call(env)
      headers = { 'Cache-Control' => 'public, max-age=120' }

      response[1].must_equal headers
    end

    it 'strips session set-cookie' do
      AccountRateLimitMiddleware.new(app).call(env)

      assert env['rack.session.options'][:skip]
    end
  end

  it "is idempotent when account has not Arturo flag" do
    Arturo.disable_feature!(:classic_short_circuit_limiter)
    response = AccountRateLimitMiddleware.new(app).call(env)

    response[0].must_equal 422
  end

  describe 'post body size limit arturo' do
    let(:request_method) { 'GET' }
    let(:body) { '{}' }
    let(:content_type) { 'application/json' }
    let(:metadata) do
      {
        'zendesk.account' => account,
        'REQUEST_METHOD' => request_method,
        'CONTENT_TYPE' => content_type,
        'rack.input' => Rack::UTF8Sanitizer::SanitizedRackInput.new(
          StringIO.new(body), StringIO.new(body)
        )
      }
    end
    let(:response) { AccountRateLimitMiddleware.new(app).call(env) }

    describe 'when the arturo is off' do
      before do
        Arturo.disable_feature!(:block_large_post_requests)
      end

      it 'proceeds as normal' do
        response[0].wont_equal 413
      end
    end

    describe 'when the arturo is on' do
      before do
        Arturo.enable_feature!(:block_large_post_requests)
      end

      describe 'and it is a GET request' do
        it 'proceeds as normal' do
          response[0].wont_equal 413
        end
      end

      describe 'and it is a POST request' do
        let(:request_method) { 'POST' }

        describe 'with a small enough body' do
          let(:body) { 'a' * 100 }

          it 'proceeds as normal' do
            response[0].wont_equal 413
          end
        end

        describe 'with a large body' do
          let(:body) { 'a' * AccountRateLimitMiddleware::MAX_POST_BODY_SIZE + 'a' }

          it 'blocks the request' do
            response[0].must_equal 413
          end

          describe 'when not json content type' do
            let(:content_type) { 'multipart/form-data' }

            it 'proceeds as normal' do
              response[0].wont_equal 413
            end
          end
        end
      end
    end
  end
end
