require_relative "../support/test_helper"
require_relative "../../app/middleware/invalid_api_request_handler"

SingleCov.covered! uncovered: 5

class InvalidApiRequestHandler::App
  def call(_env)
    [200, {}, "Hello"]
  end
end

describe InvalidApiRequestHandler do
  fixtures :accounts

  before do
    @app = InvalidApiRequestHandler::App.new
    @env = {}
    @middleware = InvalidApiRequestHandler.new(@app)
  end

  it 'passes valid requests through' do
    response = @middleware.call(@env)
    assert_equal 200, response.first
  end

  it 'handles invalid payloads' do
    @app.expects(:call).raises(Yajl::ParseError)
    response = @middleware.call(@env)
    assert_equal 422, response.first
  end

  it 'handles invalid api request' do
    @app.expects(:call).raises(ZendeskApi::ApiAccessDenied)
    response = @middleware.call(@env)
    assert_equal 403, response.first
  end

# RAILS5UPGRADE: Remove in rails 5.
  describe 'ActionDispatch::ParamsParser::ParseError' do
    before { @app.expects(:call).raises(ActionDispatch::ParamsParser::ParseError.new(1, 2)) }

    it 'handles json' do
      response = @middleware.call(@env)
      response[0].must_equal 422
      response[1].must_equal("Content-Type" => "application/json")
    end

    it 'handles xml' do
      @env['HTTP_ACCEPT'] = 'application/xml'
      response = @middleware.call(@env)
      response[0].must_equal 422
      response[1].must_equal("Content-Type" => "application/xml")
    end
  end if RAILS4

  describe 'with non-json content type' do
    before do
      @env['CONTENT_TYPE'] = 'application/xml'
      @env['PATH_INFO'] = 'api/v2/tickets.json'
      @env['REQUEST_METHOD'] = 'POST'
      # arturo needs an account to work properly
      @env['zendesk.account'] = accounts(:minimum)
    end

    it "does not change content type" do
      @middleware.call(@env)
      assert_equal 'application/xml', @env['CONTENT_TYPE']
    end

    describe "with arturo enabled default_json_content_type" do
      before do
        @env['PATH_INFO'] = 'api/v2/tickets'
        @env['CONTENT_TYPE'] = "application/x-www-form-urlencoded"
        Arturo.enable_feature!(:default_json_content_type)
      end

      it "changes content type to json" do
        @middleware.call(@env)
        assert_equal 'application/json', @env['CONTENT_TYPE']
      end
    end

    describe "with arturo disabled default_json_content_type" do
      before do
        @env['PATH_INFO'] = 'api/v2/tickets'
        @env['CONTENT_TYPE'] = "application/x-www-form-urlencoded"
        Arturo.disable_feature!(:default_json_content_type)
      end

      it "does not change content type" do
        @middleware.call(@env)
        assert_equal 'application/x-www-form-urlencoded', @env['CONTENT_TYPE']
      end
    end
  end
end
