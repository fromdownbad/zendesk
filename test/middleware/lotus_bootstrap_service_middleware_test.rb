require_relative "../support/test_helper"

SingleCov.covered!

describe 'LotusBootstrapServiceMiddleware' do
  fixtures :accounts

  def call_app(path)
    app = stub(call: [200, {}, []])
    lotus_bootstrap = LotusBootstrapServiceMiddleware.new(app)
    env = Rack::MockRequest.env_for(path)
    env['zendesk.account'] = accounts(:minimum)

    lotus_bootstrap.call(env)
  end

  def redirect_response(redirect_path)
    [200, { "X-Accel-Redirect" => redirect_path }, ['Lotus Bootstrap Service Redirect']]
  end

  describe 'with Lotus Bootstrap Service feature disabled' do
    it 'does not redirect to the Lotus Bootstrap Service' do
      assert_equal call_app('/agent/dashboard'), [200, {}, []]
    end
  end

  describe 'with Lotus Bootstrap Service feature enabled' do
    before { Arturo.enable_feature!(:lotus_bootstrap_service) }

    it 'redirects with a long Lotus url' do
      assert_equal call_app('/agent/dashboard'), redirect_response('/agent/boot/dashboard')
    end

    it 'redirects with a short Lotus url and trailing slash' do
      assert_equal call_app('/agent/'), redirect_response('/agent/boot/')
    end

    it 'redirects with a short Lotus url and no trailing slash' do
      assert_equal call_app('/agent'), redirect_response('/agent/boot/')
    end

    it 'does not redirect with a non-Lotus url' do
      assert_equal call_app('/not/lotus/path'), [200, {}, []]
    end
  end
end
