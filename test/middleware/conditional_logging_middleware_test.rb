require_relative "../support/test_helper"
require_relative "../../app/middleware/conditional_logging_middleware"

SingleCov.covered! uncovered: 1

class ConditionalLoggingMiddleware::App
  def call(_env)
    Rails.logger.debug 'debug message'
    [422, {}, ["a", "b"]]
  end
end

describe ConditionalLoggingMiddleware do
  fixtures :accounts, :account_settings, :users

  before do
    @app = ConditionalLoggingMiddleware::App.new
    @account = accounts(:minimum)
    @user = users(:minimum_agent)
    @expected = {
        body: ["a", "b"],
        status: 422
    }
    @env = {
      'HTTP_HOST' => accounts(:minimum).host_name,
      'zendesk.account' => @account,
      'zendesk.user' => @user,
      'REQUEST_METHOD' => 'PUT',
      'PATH_INFO' => '/',
      'HTTP_USER_AGENT' => 'Zendesk Target',
      'QUERY_STRING' => 'zendesk_test',
      'rack.input' => "test",
      'action_dispatch.request.request_parameters' => {"test" => {}}
}
    @middleware = ConditionalLoggingMiddleware.new(@app)
  end

  it 'passes requests through' do
    status, headers, response = @middleware.call(@env)
    assert_equal [@expected[:status], {}, @expected[:body]], [status, headers, response]
  end

  it 'does not log if account is nil' do
    @env['zendesk.account'] = nil
    @middleware.expects(:should_log).never
    @middleware.call(@env)
  end

  it 'does not log if pattern is nil' do
    @account.set_conditional_logging
    @middleware.expects(:should_log).never
    @middleware.call(@env)
  end

  it 'logs debug information and response' do
    @account.set_conditional_logging(user_id: /#{@user.id}/)
    CONDITIONAL_LOG.expects(:info).at_least(3)
    regular_logging_expectation = "Initiating conditional_logging per user_id: /(?-mix:#{@user.id})/"
    Rails.logger.expects(:info).with(regular_logging_expectation)
    @middleware.call(@env)
  end

  it 'still works with missing request_parameters in env' do
    @account.set_conditional_logging(user_id: /#{@user.id}/)
    new_env = @env.except('action_dispatch.request.request_parameters')
    status, headers, response = @middleware.call(new_env)
    assert_equal [@expected[:status], {}, @expected[:body]], [status, headers, response]
  end

  describe 'handles invalid conditional_logging_pattern' do
    it 'still returns a response if unable to parse the JSON' do
      @account.settings.conditional_logging_pattern = "not JSON"
      status, headers, response = @middleware.call(@env)
      assert_equal [@expected[:status], {}, @expected[:body]], [status, headers, response]
    end

    it 'still returns a response with JSON that does not result in the expected array' do
      @account.settings.conditional_logging_pattern = JSON.dump(something: ['asdf', 'hjkl'])
      status, headers, response = @middleware.call(@env)
      assert_equal [@expected[:status], {}, @expected[:body]], [status, headers, response]
    end

    it 'still returns a response when the array does not contain regular expressions' do
      @account.settings.conditional_logging_pattern = JSON.dump(['string', @account, @user, 1234, {test: 1}, 1234])
      status, headers, response = @middleware.call(@env)
      assert_equal [@expected[:status], {}, @expected[:body]], [status, headers, response]
    end
  end

  describe 'Logging based on response' do
    before do
      @app = stub(call: [200, {}, ["a"]])
      @middleware = ConditionalLoggingMiddleware.new(@app)
    end

    it 'does not log when setting is true and request is success' do
      @account.set_conditional_logging(status: /[4-5][0-9][0-9]/, user_id: /#{@user.id}/)
      CONDITIONAL_LOG.expects(:info).never
      @middleware.call(@env)
    end

    it 'does log when setting is false and request is success' do
      @account.set_conditional_logging(status: /[1-3][0-9][0-9]/, user_id: /#{@user.id}/)
      CONDITIONAL_LOG.expects(:info).at_least(3)
      @middleware.call(@env)
    end
  end

  describe '#should_log' do
    it 'matches based on user ID' do
      @account.set_conditional_logging(user_id: /#{@user.id}/)
      CONDITIONAL_LOG.expects(:info).at_least(3)
      @middleware.call(@env)
    end

    it 'matches based on REQUEST_METHOD' do
      @account.set_conditional_logging(request_method: /PUT/)
      CONDITIONAL_LOG.expects(:info).at_least(3)
      @middleware.call(@env)
    end

    it 'matches based on PATH_INFO' do
      @account.set_conditional_logging(path_info: /\//)
      CONDITIONAL_LOG.expects(:info).at_least(3)
      @middleware.call(@env)
    end

    it 'matches based on HTTP_USER_AGENT' do
      @account.set_conditional_logging(http_user_agent: /Zendesk Target/)
      CONDITIONAL_LOG.expects(:info).at_least(3)
      @middleware.call(@env)
    end

    it 'matches based on QUERY_STRING' do
      @account.set_conditional_logging(query_string: /zendesk_test/)
      CONDITIONAL_LOG.expects(:info).at_least(3)
      @middleware.call(@env)
    end

    it 'matches if all conditions are true' do
      @account.set_conditional_logging(
        user_id: /#{@user.id}/,
        request_method: /PUT/,
        path_info: /\//,
        http_user_agent: /Zendesk Target/,
        query_string: /zendesk_test/
      )
      CONDITIONAL_LOG.expects(:info).at_least(3)
      @middleware.call(@env)
    end

    it 'does not match if some conditions are false' do
      @account.set_conditional_logging(
        user_id: /#{@user.id}/,
        request_method: /GET/,
        path_info: /\//,
        http_user_agent: /Zendesk Target/,
        query_string: /zendesk_test/
      )
      CONDITIONAL_LOG.expects(:info).never
      @middleware.call(@env)
    end
  end
end
