require_relative "../support/test_helper"
require_relative "../support/fake_esc_kafka_message"

SingleCov.covered!

describe UserEventsProtobufEncoder do
  fixtures :users, :events, :cf_fields

  let(:user) { users(:minimum_end_user) }

  describe 'with events to encode' do
    describe 'when encoding context' do
      let(:actor) { users(:minimum_end_user2) }

      before do
        user.id = 1234
        user.will_be_saved_by(actor)
      end

      it 'provides a thin user context' do
        UserV2ProtobufEncoder.any_instance.expects(:thin).returns(UserV2ProtobufEncoder.new(user)).times(proto_user_events.count + 1)
        proto_user_events.each do |event|
          assert_kind_of Zendesk::Protobuf::Support::Users::V2::User, event.user
        end
      end

      it 'provides actor' do
        proto_user_events.each do |event|
          assert_kind_of Zendesk::Protobuf::Support::Users::V2::User, event.deprecated_actor
          assert_equal actor.id, event.deprecated_actor.id&.value
          assert_kind_of Zendesk::Protobuf::AccountAudits::Actor, event.actor
          assert_equal actor.id, event.actor.id&.value
          assert_equal "", event.actor.ip_address&.value
        end
      end

      it 'provides actor with ip-address supplied' do
        CIA.audit(actor: actor, ip_address: "127.0.0.1") do
          proto_user_events.each do |event|
            assert_kind_of Zendesk::Protobuf::AccountAudits::Actor, event.actor
            assert_equal actor.id, event.actor.id&.value
            assert_equal "127.0.0.1", event.actor.ip_address&.value
          end
        end
      end

      it 'provides a header' do
        proto_user_events.each do |event|
          assert_equal 'support/users/v2/user_events', event.header.schema_path
          assert_kind_of Zendesk::Protobuf::Common::ProtobufHeader, event.header
          assert_equal 36, event.header.message_id&.value&.length
          assert_equal 90538, event.header.account_id&.value
          assert_equal user.updated_at.to_i, event.header.occurred_at&.seconds
          assert_kind_of Zendesk::Protobuf::Common::MessageSequence, event.header.sequence
        end
      end

      it 'increments header.sequence.position as 1-indexed' do
        assert_equal [1], proto_user_events.map { |e| e.header.sequence.position }
      end
    end
  end

  describe 'when encoding events' do
    describe 'when creating a new user' do
      before do
        user.id = 1234
      end

      it 'contains UserCreated event' do
        assert_event! :user_created
      end
    end

    describe 'when updating an existing user' do
      before do
        user.name = 'Argonauts'
        user.external_id = '12345'
        user.notes = 'changed_notes'
        user.time_zone = Time.now
        user.is_private_comments_only = true
        user.is_active = false
        user.details = 'user details changed'
      end

      it 'contains UserNameChanged event' do
        assert_event! :user_name_changed
      end

      it 'contains ExternalIdChanged event' do
        assert_event! :external_id_changed
      end

      it 'contains UserNotesChanged event' do
        assert_event! :user_notes_changed
      end

      it 'contains TimeZoneChanged event' do
        assert_event! :time_zone_changed
      end

      it 'contains OnlyPrivateCommentsChanged event' do
        assert_event! :only_private_comments_changed
      end

      it 'contains UserIsActiveChanged event' do
        assert_event! :user_is_active_changed
      end

      it 'contains UserDetailsChanged event' do
        assert_event! :user_details_changed
      end
    end

    describe 'When a user is soft deleted' do
      let(:deleted_user) { users(:minimum_end_user2) }
      let(:current_account) { deleted_user.account }
      let(:zendesk_agent) do
        FactoryBot.create(:agent,
          account: current_account,
          name: "Double Agent")
      end

      it 'contains UserSoftDeleted event' do
        deleted_user.current_user = zendesk_agent
        deleted_user.delete do
          assert_event! :user_soft_deleted, deleted_user
        end
      end
    end

    describe 'when updating an existing user alias' do
      let(:domain_event_publisher) { FakeEscKafkaMessage.new }

      before do
        User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
        user.alias = 'Argonauts'
        user.save!
      end

      it 'contains UserAliasChanged event' do
        events = get_domain_events(domain_event_publisher, :user_alias_changed)
        assert_equal events.size, 1
      end
    end
  end

  describe 'when updating the user photo' do
    let(:user) { users(:minimum_end_user) }

    before do
      user.set_photo(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png"))
      user.save!
    end

    it 'contains UserPhotoChanged event' do
      assert_event! :user_photo_changed
    end
  end

  describe 'when updating the default group' do
    let(:agent) { users(:with_groups_agent2) }
    let(:new_group_id) { agent.memberships.reject(&:default?).first.group_id }
    let(:domain_event_publisher) { FakeEscKafkaMessage.new }

    before do
      User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
    end

    it "contains UserDefaultGroupChanged event" do
      agent.set_default_group(new_group_id)
      events = get_domain_events(domain_event_publisher, :user_default_group_changed)
      assert_equal events.size, 1
    end
  end

  describe 'with no events to encode' do
    it 'any? returns false' do
      assert_equal false, UserEventsProtobufEncoder.new(user).any?
    end
  end

  def proto_user_events(current_user = user)
    UserEventsProtobufEncoder.new(current_user).to_a.map(&:to_proto).
      map { |proto| ::Zendesk::Protobuf::Support::Users::V2::UserEvent.decode(proto) }
  end

  def assert_event!(event_type, current_user = user)
    events = proto_user_events(current_user).map(&:event)
    events.must_include(event_type)
  end

  def get_domain_events(domain_event_publisher, event_name)
    events = domain_event_publisher.events.map do |event|
      proto = event.fetch(:value)
      ::Zendesk::Protobuf::Support::Users::V2::UserEvent.decode(proto)
    end

    events.select { |event| event.event == event_name }
  end
end
