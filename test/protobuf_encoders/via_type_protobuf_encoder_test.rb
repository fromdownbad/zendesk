require_relative "../support/test_helper"

SingleCov.covered!

describe ViaTypeProtobufEncoder do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:proto_via_type) do
    ViaTypeProtobufEncoder.new(ticket).to_object
  end

  describe "for each valid via type" do
    ViaTypeProtobufEncoder::VIA_MAP.keys.each do |via|
      it "correctly encodes #{via}" do
        ticket.update_column(:via_id, via)

        assert_equal ViaType[via].name.gsub(/\s/, '_').downcase.to_sym, proto_via_type.type
      end
    end
  end

  describe 'with no matching via_type' do
    it "returns unknown" do
      ticket.update_column(:via_id, -1)

      assert_equal :unknown, proto_via_type.type
    end
  end
end
