require_relative "../support/test_helper"

SingleCov.covered!

describe GroupProtobufEncoder do
  fixtures :groups

  let(:group) { groups(:minimum_group) }
  let(:proto_group) do
    ::Zendesk::Protobuf::Support::Groups::Group.decode(
      GroupProtobufEncoder.new(group).to_proto
    )
  end

  describe "for primitive fields" do
    # For cases where the model and proto field name are different, use a subarray
    [:id, :is_active, :name, :default, :chat_enabled].each do |model_field, proto_field|
      proto_field ||= model_field

      it "correctly encodes #{proto_field}" do
        assert_equal group.send(model_field), proto_group.send(proto_field).value
      end
    end
  end

  describe "for nested fields" do
    # Each hash key is the protobuf object field, and the value it's expected type
    {account: Zendesk::Protobuf::Support::Accounts::Account}.each do |field, encoder|
      it "encodes #{field}" do
        assert_instance_of encoder, proto_group.send(field)
      end
    end
  end

  describe "for timestamps" do
    [:created_at, :updated_at].each do |field|
      it "correctly encodes #{field}" do
        assert_equal group.send(field).to_i, proto_group.timestamps.send(field).seconds
        assert_equal group.send(field).nsec, proto_group.timestamps.send(field).nanos
      end
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        GroupProtobufEncoder.new(group, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      GroupProtobufEncoder.any_instance.expects(:full).once

      GroupProtobufEncoder.new(group)
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = GroupProtobufEncoder.new(group, [:id, :is_active]).to_object

      proto_object.to_h.each do |field, value|
        if [:id, :is_active].include?(field)
          refute_nil value
        else
          assert_nil value
        end
      end
    end

    it "passes sub the map to the next encoder" do
      AccountProtobufEncoder.expects(:new).with(group.account, [:id]).returns(mock(to_object: nil))

      GroupProtobufEncoder.new(group, [account: [:id]]).to_object
    end
  end
end
