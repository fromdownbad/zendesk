require_relative "../support/test_helper"

SingleCov.covered!

describe TicketFormProtobufEncoder do
  fixtures :ticket_forms

  let(:ticket_form) { ticket_forms(:default_ticket_form) }
  let(:proto_ticket_form) do
    ::Zendesk::Protobuf::Support::Tickets::V2::TicketForm.decode(
      TicketFormProtobufEncoder.new(ticket_form).to_proto
    )
  end

  describe "for primitive fields" do
    # For cases where the model and proto field name are different, use a subarray
    [:id, :name, :display_name, [:end_user_visible, :is_end_user_visible], :position, [:active, :is_active], [:default, :is_default], [:in_all_organizations, :is_in_all_organizations], [:in_all_brands, :is_in_all_brands]].each do |model_field, proto_field|
      it "correctly encodes #{proto_field}" do
        proto_field ||= model_field

        assert_equal ticket_form.send(model_field), proto_ticket_form.send(proto_field).value
      end
    end
  end

  describe "for nested fields" do
    # Each hash key is the protobuf object field, and the value it's expected type
    {account: Zendesk::Protobuf::Support::Accounts::Account}.each do |field, encoder|
      it "encodes #{field}" do
        assert_instance_of encoder, proto_ticket_form.send(field)
      end
    end
  end

  describe "for timestamps" do
    [:created_at, :updated_at].each do |field|
      it "correctly encodes #{field}" do
        assert_equal ticket_form.send(field).to_i, proto_ticket_form.timestamps.send(field).seconds
        assert_equal ticket_form.send(field).nsec, proto_ticket_form.timestamps.send(field).nanos
      end
    end

    describe "for deleted_at" do
      it "encodes deleted_at if present" do
        ticket_form.update_column(:deleted_at, Time.now)
        assert_equal ticket_form.deleted_at.to_i, proto_ticket_form.deleted_at.seconds
        assert_equal ticket_form.deleted_at.nsec, proto_ticket_form.deleted_at.nanos
      end

      it "can encode without deleted_at" do
        ticket_form.update_column(:deleted_at, nil)
        assert_nil proto_ticket_form.deleted_at
      end
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        TicketFormProtobufEncoder.new(ticket_form, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      TicketFormProtobufEncoder.any_instance.expects(:full).once

      TicketFormProtobufEncoder.new(ticket_form)
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = TicketFormProtobufEncoder.new(ticket_form, [:id, :is_active]).to_object

      proto_object.to_h.each do |field, value|
        if [:id, :is_active].include?(field)
          refute_nil value
        else
          assert_nil value
        end
      end
    end

    it "passes sub the map to the next encoder" do
      AccountProtobufEncoder.expects(:new).with(ticket_form.account, [:id]).returns(mock(to_object: nil))

      TicketFormProtobufEncoder.new(ticket_form, [account: [:id]]).to_object
    end
  end
end
