require_relative "../support/test_helper"

SingleCov.covered!

describe UserRoleProtobufEncoder do
  fixtures :users

  let(:user) { users(:minimum_end_user) }
  let(:user_encoder) { UserRoleProtobufEncoder.new(user) }
  let(:proto) { user_encoder.to_object }
  let(:fields) do
    %i[role_id permission_set_id]
  end

  it 'returns object with #to_proto' do
    assert_kind_of String, user_encoder.to_proto
  end

  it 'contains the correct fields' do
    encoded_fields = user_encoder.to_object.to_h.keys
    assert_empty (encoded_fields - fields) | (fields - encoded_fields)
  end
end
