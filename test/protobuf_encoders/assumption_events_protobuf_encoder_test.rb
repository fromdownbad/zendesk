require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe AssumptionEventsProtobufEncoder do
  describe 'encodes correctly' do
    let(:event_time) { Time.now }
    let(:event_type) { "assume" }
    let(:account_id) { 1234 }
    let(:original_user_id) { 11111 }
    let(:current_user_id) { 22222 }

    let(:proto_assumption_event) do
      AssumptionEventsProtobufEncoder.new(event_type: event_type, event_time: event_time, account_id: account_id, original_user_id: original_user_id, current_user_id: current_user_id).full.to_proto
    end

    let(:actual_assumption_event) do
      ::Zendesk::Protobuf::Auth::AssumptionEvents::AssumptionEvent.decode(proto_assumption_event)
    end

    it 'all values except event_time' do
      {
        event_type: event_type,
        account_id: account_id,
        original_user_id: original_user_id,
        current_user_id: current_user_id
      }.each do |proto_key, actual_value|
        assert_equal actual_value, actual_assumption_event.send(proto_key).value
      end
    end

    it 'event_time' do
      assert_equal event_time.to_i, actual_assumption_event.event_time.seconds
      assert_equal event_time.nsec, actual_assumption_event.event_time.nanos
    end
  end
end
