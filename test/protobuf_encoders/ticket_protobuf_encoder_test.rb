require_relative "../support/test_helper"

SingleCov.covered!

describe TicketProtobufEncoder do
  fixtures :tickets, :users, :groups, :organizations, :ticket_forms

  let(:ticket) { tickets(:minimum_1) }

  describe "with a full field map" do
    let(:proto_ticket) do
      ::Zendesk::Protobuf::Support::Tickets::V2::Ticket.decode(
        TicketProtobufEncoder.new(ticket).full.to_proto
      )
    end

  # For cases where the model and proto field name are different, use a subarray
    describe "for primitive fields" do
      [:id, :description, :nice_id, :is_public].each do |model_field, proto_field|
        it "correctly encodes #{model_field}" do
          proto_field ||= model_field

          assert_equal ticket.send(model_field), proto_ticket.send(proto_field).value
        end
      end

      describe "for nullable primitive fields" do
        [:linked_id, :recipient, [:current_collaborators, :collaborators], [:subject, :raw_subject], :external_id].each do |model_field, proto_field|
          it "encodes #{model_field} if present" do
            proto_field ||= model_field
            set_field_value(ticket, model_field)

            assert_equal ticket.send(model_field), proto_ticket.send(proto_field).value
          end

          it "can encode without #{model_field}" do
            proto_field ||= model_field
            ticket.update_column(model_field, nil)

            assert_nil proto_ticket.send(proto_field)
          end
        end
      end
    end

    describe "for enum fields" do
      {
        status:      Zendesk::Protobuf::Support::Tickets::V2::TicketStatus,
        priority:    Zendesk::Protobuf::Support::Tickets::V2::TicketPriority,
        ticket_type: Zendesk::Protobuf::Support::Tickets::V2::TicketType,
      }.each do |field, enum|
        it "encodes #{field}" do
          refute_equal 0, enum.resolve(proto_ticket.send(field))
        end
      end
    end

    describe "for nested fields" do
      # Each hash key is the protobuf object field, and the value it's expected type
      {
        account:            Zendesk::Protobuf::Support::Accounts::Account,
        requester:          Zendesk::Protobuf::Support::Users::User,
        submitter:          Zendesk::Protobuf::Support::Users::User,
        satisfaction_score: Zendesk::Protobuf::Support::Tickets::V2::SatisfactionScore,
        via:                Zendesk::Protobuf::Support::Tickets::V2::ViaType
      }.each do |field, encoder|
        it "encodes #{field}" do
          assert_instance_of encoder, proto_ticket.send(field)
        end
      end

      describe "for nullable nested fields" do
        describe "when encoding assignee" do
          it "encodes assignee if present" do
            ticket.assignee = users(:minimum_agent)
            assert_instance_of Zendesk::Protobuf::Support::Users::User, proto_ticket.assignee
          end

          it "can encode without assignee" do
            ticket.assignee = nil
            assert_nil proto_ticket.assignee
          end
        end

        describe "when encoding submitter" do
          it "can encode without submitter" do
            ticket.submitter = nil
            assert_nil proto_ticket.submitter
          end
        end

        describe "when encoding group" do
          it "encodes group if present" do
            ticket.group = groups(:minimum_group)
            assert_instance_of Zendesk::Protobuf::Support::Groups::Group, proto_ticket.group
          end

          it "can encode without group" do
            ticket.group = nil
            assert_nil proto_ticket.group
          end
        end

        describe "when encoding organization" do
          it "encodes organization if present" do
            ticket.organization = organizations(:minimum_organization1)
            assert_instance_of Zendesk::Protobuf::Support::Users::V2::Organization, proto_ticket.organization
          end

          it "can encode without organization" do
            assert_nil proto_ticket.organization
          end
        end

        describe "when encoding tags" do
          it "encodes tags if present" do
            ticket.current_tags = "a b c"
            assert_equal "a b c".split, proto_ticket.tags.tags
          end

          it "can encode without tags" do
            ticket.current_tags = nil
            assert_nil proto_ticket.tags
          end
        end

        describe "when encoding ticket_form" do
          it "encodes ticket_form if present" do
            ticket.ticket_form = ticket_forms(:default_ticket_form)
            assert_instance_of Zendesk::Protobuf::Support::Tickets::V2::TicketForm, proto_ticket.ticket_form
          end

          it "can encode without ticket_form" do
            assert_nil proto_ticket.ticket_form
          end
        end

        describe "when encoding brand" do
          it "encodes brand if present" do
            ticket.brand = brands(:minimum)
            assert_instance_of Zendesk::Protobuf::Support::Accounts::Brand, proto_ticket.brand
          end

          it "can encode without brand" do
            ticket.brand = nil
            assert_nil proto_ticket.brand
          end
        end
      end
    end

    describe "for timestamps" do
      [:created_at, :updated_at].each do |field|
        it "correctly encodes #{field}" do
          assert_equal ticket.send(field).to_i, proto_ticket.timestamps.send(field).seconds
          assert_equal ticket.send(field).nsec, proto_ticket.timestamps.send(field).nanos
        end
      end

      describe "when encoding due_at" do
        let(:ticket) { tickets(:minimum_1) }

        it "encodes due_at using the ticket due_date if present" do
          Timecop.freeze do
            ticket.update_column(:due_date, Time.current)

            assert_equal ticket.due_date.to_i, proto_ticket.due_at.seconds
          end
        end

        it "can encode without a ticket due_date" do
          ticket.update_column(:due_date, nil)

          assert_nil proto_ticket.due_at
        end
      end

      describe "when encoding generated_timestamp" do
        it "encodes generated_timestamp if present" do
          ticket.update_column(:generated_timestamp, Time.now)

          assert_equal ticket.generated_timestamp.to_i, proto_ticket.generated_timestamp.seconds
          assert_equal ticket.generated_timestamp.nsec, proto_ticket.generated_timestamp.nanos
        end

        it "can encode without generated_timestamp" do
          ticket.update_column(:generated_timestamp, nil)

          assert_equal 0, proto_ticket.generated_timestamp.seconds
          assert_equal 0, proto_ticket.generated_timestamp.nanos
        end
      end

      describe "when encoding external_id" do
        it "encodes empty strings as null" do
          ticket.update_column(:external_id, '')
          assert_nil proto_ticket.external_id
        end
      end
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        TicketProtobufEncoder.new(ticket, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      TicketProtobufEncoder.any_instance.expects(:full).once

      TicketProtobufEncoder.new(ticket)
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = TicketProtobufEncoder.new(ticket, [:id, :is_public]).to_object

      proto_object.to_h.each do |field, value|
        if [:id, :is_public].include?(field)
          refute_nil value
        elsif [:status, :priority, :ticket_type].include?(field)
          assert_includes value.to_s, "UNKNOWN"
        else
          assert_nil value
        end
      end
    end

    it "passes sub the map to the next encoder" do
      UserProtobufEncoder.expects(:new).with(ticket.requester, [:id]).returns(mock(to_object: nil))

      TicketProtobufEncoder.new(ticket, [requester: [:id]]).to_object
    end
  end
end
