require_relative "../support/test_helper"

SingleCov.covered!

describe TicketFieldProtobufEncoder do
  fixtures :ticket_fields

  let(:ticket_field) { ticket_fields(:field_tagger_custom) }

  describe "with a thin field map" do
    let(:proto_ticket_field) do
      ::Zendesk::Protobuf::Support::Tickets::V2::TicketField.decode(
        TicketFieldProtobufEncoder.new(ticket_field).thin.to_proto
      )
    end

    it "encodes the id" do
      proto_ticket_field.id.value.must_equal ticket_field.id
    end

    it "encodes the raw_title without interpolation" do
      proto_ticket_field.raw_title.value.must_equal "Fun factor {{dc.welcome_wombat}}"
    end

    it "encodes the type using the protobuf schema enum" do
      proto_ticket_field.type.must_equal :TAGGER
    end

    describe "with a system field type" do
      before do
        ticket_field.update_column(:type, "FieldStatus")
      end

      it "encodes the type as UNKNOWN" do
        proto_ticket_field.type.must_equal :UNKNOWN
      end
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        TicketFieldProtobufEncoder.new(ticket_field, [:foo]).to_object
      end
    end
  end
end
