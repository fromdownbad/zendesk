require_relative "../support/test_helper"

SingleCov.covered!

describe CustomFieldProtobufEncoder do
  fixtures :cf_fields

  let(:user_field) { cf_fields(:dropdown1) }

  describe "with a thin field map" do
    let(:proto_user_field) do
      ::Zendesk::Protobuf::Support::Users::V2::CustomField.decode(
        CustomFieldProtobufEncoder.new(user_field).thin.to_proto
      )
    end

    it "encodes the id" do
      proto_user_field.id.value.must_equal user_field.id
    end

    it "encodes the title without interpolation" do
      proto_user_field.title.value.must_equal "something else"
    end

    it "encodes the type using the protobuf schema enum" do
      proto_user_field.type.must_equal :TAGGER
    end

    describe "with a system field type" do
      before do
        user_field.update_column(:type, "FieldStatus")
      end

      it "encodes the type as UNKNOWN_CUSTOM_FIELD_TYPE" do
        proto_user_field.type.must_equal :UNKNOWN_CUSTOM_FIELD_TYPE
      end
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        CustomFieldProtobufEncoder.new(user_field, [:foo]).to_object
      end
    end
  end
end
