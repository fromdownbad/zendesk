require_relative "../support/test_helper"

SingleCov.covered!

describe OrganizationProtobufEncoder do
  fixtures :organizations, :groups

  let(:organization) { organizations(:minimum_organization1) }
  let(:proto_organization) do
    ::Zendesk::Protobuf::Support::Users::V2::Organization.decode(
      OrganizationProtobufEncoder.new(organization).to_proto
    )
  end

  describe "for primitive fields" do
    # For cases where the model and proto field name are different, use a subarray
    [:id, :name, :is_shared, :is_shared_comments].each do |model_field, proto_field|
      it "correctly encodes #{proto_field}" do
        proto_field ||= model_field

        assert_equal organization.send(model_field), proto_organization.send(proto_field).value
      end
    end

    describe "for nullable primitive fields" do
      [:details, :notes, :external_id].each do |field|
        it "encodes #{field} if present" do
          set_field_value(organization, field)
          assert_equal organization.send(field), proto_organization.send(field).value
        end

        it "can encode without #{field}" do
          organization.update_column(field, nil)
          assert_nil proto_organization.send(field)
        end
      end
    end
  end

  describe "for nested fields" do
    # Each hash key is the protobuf object field, and the value it's expected type
    {account: Zendesk::Protobuf::Support::Accounts::Account}.each do |field, encoder|
      it "encodes #{field}" do
        assert_instance_of encoder, proto_organization.send(field)
      end
    end

    describe "for nullable nested fields" do
      describe "when encoding group" do
        it "encodes group if present" do
          organization.group = groups(:minimum_group)
          assert_instance_of Zendesk::Protobuf::Support::Groups::Group, proto_organization.group
        end

        it "can encode without group" do
          assert_nil proto_organization.group
        end
      end
    end
  end

  describe "for timestamps" do
    [:created_at, :updated_at].each do |field|
      it "correctly encodes #{field}" do
        assert_equal organization.send(field).to_i, proto_organization.timestamps.send(field).seconds
        assert_equal organization.send(field).nsec, proto_organization.timestamps.send(field).nanos
      end
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        OrganizationProtobufEncoder.new(organization, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      OrganizationProtobufEncoder.any_instance.expects(:full).once

      OrganizationProtobufEncoder.new(organization)
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = OrganizationProtobufEncoder.new(organization, [:id]).to_object

      proto_object.to_h.each do |field, value|
        if [:id].include?(field)
          refute_nil value
        else
          assert_nil value
        end
      end
    end

    it "passes sub the map to the next encoder" do
      AccountProtobufEncoder.expects(:new).with(organization.account, [:id]).returns(mock(to_object: nil))

      OrganizationProtobufEncoder.new(organization, [account: [:id]]).to_object
    end
  end
end
