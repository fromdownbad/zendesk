require_relative "../support/test_helper"

SingleCov.covered!

describe TicketTypeProtobufEncoder do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:proto_ticket_type) do
    TicketTypeProtobufEncoder.new(ticket).to_object
  end

  describe "for each valid ticket type" do
    TicketTypeProtobufEncoder::TYPE_MAP.keys.each do |type|
      it "correctly encodes #{type}" do
        ticket.update_column(:ticket_type_id, type)

        # Handle the "-" null type
        name = if type == 0
          :UNKNOWN_TICKET_TYPE
        else
          TicketType[type].name.upcase.to_sym
        end

        assert_equal name, ::Zendesk::Protobuf::Support::Tickets::V2::TicketType.lookup(proto_ticket_type)
      end
    end
  end

  it "returns unknown for unknown type" do
    ticket.update_column(:ticket_type_id, -1)

    assert_equal ::Zendesk::Protobuf::Support::Tickets::V2::TicketType::UNKNOWN_TICKET_TYPE, proto_ticket_type
  end
end
