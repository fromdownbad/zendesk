require_relative "../support/test_helper"

SingleCov.covered!

describe TicketStatusProtobufEncoder do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:proto_ticket_status) do
    TicketStatusProtobufEncoder.new(ticket).to_object
  end

  describe "for each valid ticket status" do
    TicketStatusProtobufEncoder::STATUS_MAP.keys.each do |status|
      it "correctly encodes #{status}" do
        ticket.update_column(:status_id, status)

        assert_equal StatusType[status].name.upcase.to_sym, ::Zendesk::Protobuf::Support::Tickets::V2::TicketStatus.lookup(proto_ticket_status)
      end
    end
  end

  it "returns unknown for unknown statuses" do
    ticket.update_column(:status_id, -1)

    assert_equal ::Zendesk::Protobuf::Support::Tickets::V2::TicketStatus::UNKNOWN_TICKET_STATUS, proto_ticket_status
  end
end
