require_relative "../support/test_helper"

SingleCov.covered!

describe TimestampsProtobufEncoder do
  fixtures :tickets

  let(:record) { tickets(:minimum_1) }

  let(:proto_timestamps) do
    ::Zendesk::Protobuf::Common::Timestamps.decode(
      TimestampsProtobufEncoder.new(record).to_proto
    )
  end

  [:created_at, :updated_at].each do |time|
    it "correctly encodes a record's #{time}" do
      assert_equal record.send(time).to_i, proto_timestamps.send(time).seconds
      assert_equal record.send(time).nsec, proto_timestamps.send(time).nanos
    end
  end
end
