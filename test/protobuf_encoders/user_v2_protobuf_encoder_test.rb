require_relative "../support/test_helper"

SingleCov.covered!

describe UserV2ProtobufEncoder do
  include CustomFieldsTestHelper

  fixtures :accounts, :users, :organization_memberships, :organizations

  let(:user) do
    fake_user = users(:minimum_end_user)
    fake_user.locale_id = 1
    fake_user
  end
  let(:tags) { ["hey", "ho"] }
  let(:proto_user) do
    ::Zendesk::Protobuf::Support::Users::V2::User.decode(
      UserV2ProtobufEncoder.new(user).to_proto
    )
  end

  it "correctly encodes user record data" do
    assert_equal user.account_id, proto_user.account.id.value
    assert_equal user.account.name, proto_user.account.name.value
    assert_equal user.account.subdomain, proto_user.account.subdomain.value

    assert_equal user.created_at.to_i, proto_user.timestamps.created_at.seconds
    assert_equal user.created_at.nsec, proto_user.timestamps.created_at.nanos

    assert_equal user.id, proto_user.id.value
    assert_equal user.name, proto_user.name.value
    assert_equal user.email, proto_user.email.value
    assert_equal user.organization_from_default_membership.id.to_i, proto_user.organization_id.value
    assert_equal user.default_group_id.to_i, proto_user.group_id.value
    assert_equal user.roles.to_i, proto_user.role.role_id.value
    assert_equal user.permission_set_id.to_i, proto_user.role.permission_set_id.value
    assert_equal user.is_active?, proto_user.active.value
    assert_equal user.time_zone, proto_user.time_zone.value

    assert_equal user.locale_id, proto_user.locale.id.value
    assert_equal user.agent_display_name.to_s, proto_user.alias.value
    assert_equal user.details.to_s, proto_user.details.value
    assert_equal user.notes.to_s, proto_user.notes.value
  end

  describe "with user photo" do
    it "correctly encodes and decodes" do
      assert_equal "https://minimum.zendesk-test.com/images/2016/default-avatar-80.png", proto_user.photo.thumbnail_url.value
      assert_equal "https://minimum.zendesk-test.com/images/2016/default-avatar-80.png", proto_user.photo.original_url.value
    end
  end

  describe "with user fields" do
    before do
      @account = accounts(:minimum) # used by helper below

      user.custom_field_values.build(
        field: create_user_custom_field!("field_1", "Field 1", "Date"),
        value: Time.new(2002, 10, 31)
      )
      user.save!
    end

    it "correctly encodes and decodes" do
      assert_equal "2002-10-31 00:00:00 +0000", proto_user.user_field_values["field_1"]
    end
  end

  describe "with user tags" do
    before do
      Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)

      user.current_tags = tags
      user.save!
    end

    it "correctly encodes tags" do
      assert_equal tags, proto_user.tag_list.tags
    end
  end

  it "correctly encodes identities" do
    assert_equal 8, proto_user.identities.identities.count
    assert_equal "minimum_end_user@aghassipour.com", proto_user.identities.identities.first.value.value
    assert_equal "EMAIL", proto_user.identities.identities.first.type.to_s
  end

  it "correctly encodes organizations" do
    assert_equal "minimum organization", proto_user.organizations.organizations.first.name.value
  end

  describe "with a deleted organization" do
    before do
      organization = organizations(:minimum_organization1)
      organization.update_attribute(:deleted_at, Time.now)
    end

    it "skips deleted organization" do
      organization2 = organizations(:minimum_organization2)
      user.organization_memberships.create!(organization: organization2, account: user.account, default: true)

      assert_equal "organization 2", proto_user.organizations.organizations.first.name.value
    end

    it "returns nil if only deleted organization" do
      assert_blank proto_user.organizations.organizations
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        UserV2ProtobufEncoder.new(user, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      UserV2ProtobufEncoder.any_instance.expects(:full).once

      UserV2ProtobufEncoder.new(user)
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = UserV2ProtobufEncoder.new(user, [:id, :active]).to_object

      proto_object.to_h.each do |field, value|
        if [:id, :active].include?(field)
          refute_nil value
        else
          assert_nil value
        end
      end
    end
  end
end
