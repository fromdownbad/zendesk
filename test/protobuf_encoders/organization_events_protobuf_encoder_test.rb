require_relative "../support/test_helper"

SingleCov.covered!

describe OrganizationEventsProtobufEncoder do
  fixtures :organizations, :cf_fields

  let(:organization) { organizations(:minimum_organization1) }

  let(:protobuf_encoder) { OrganizationEventsProtobufEncoder.new(organization) }
  let(:proto_organization_events) do
    protobuf_encoder.to_a.map(&:to_proto).
      map { |proto| ::Zendesk::Protobuf::Support::Users::V2::OrganizationEvent.decode(proto) }
  end

  describe 'with events to encode' do
    describe 'when encoding context' do
      before do
        organization.id = 1234
      end

      it 'provides a thin organization context' do
        OrganizationProtobufEncoder.any_instance.expects(:thin).returns(OrganizationProtobufEncoder.new(organization)).times(proto_organization_events.count)
        proto_organization_events.each do |event|
          assert_kind_of Zendesk::Protobuf::Support::Users::V2::Organization, event.organization
        end
      end

      it 'provides a header' do
        proto_organization_events.each do |event|
          assert_equal 'support/users/v2/organization_events', event.header.schema_path
          assert_kind_of Zendesk::Protobuf::Common::ProtobufHeader, event.header
          assert_equal 36, event.header.message_id&.value&.length
          assert_equal organization.id, event.organization.id&.value
          assert_equal organization.account_id, event.header.account_id&.value
          assert_equal organization.updated_at.to_i, event.header.occurred_at&.seconds
          assert_kind_of Zendesk::Protobuf::Common::MessageSequence, event.header.sequence
          assert_kind_of Zendesk::Protobuf::AccountAudits::Actor, event.actor
          assert_equal -1, event.actor.id.value
          assert_equal organization.account_id, event.actor.account_id.value
          assert_equal '', event.actor.ip_address.value
        end
      end

      it 'increments header.sequence.position as 1-indexed' do
        assert_equal [1], proto_organization_events.map { |e| e.header.sequence.position }
      end

      it 'captures the current actor' do
        user = users(:minimum_admin)
        CIA.audit(actor: user) do
          proto_organization_events.each do |event|
            assert_kind_of Zendesk::Protobuf::AccountAudits::Actor, event.actor
            assert_equal user.id, event.actor.id.value
            assert_equal '', event.actor.ip_address.value
          end
        end
      end

      it 'captures the current actor ip-address' do
        user = users(:minimum_admin)
        CIA.audit(actor: user, ip_address: '127.0.0.1') do
          proto_organization_events.each do |event|
            assert_kind_of Zendesk::Protobuf::AccountAudits::Actor, event.actor
            assert_equal user.id, event.actor.id.value
            assert_equal '127.0.0.1', event.actor.ip_address.value
          end
        end
      end
    end

    describe 'when encoding events' do
      describe 'when creating a new organization' do
        before do
          organization.id = 3583495
        end

        it 'contains OrganizationCreated event' do
          assert_event! :organization_created
        end
      end

      describe 'when the external id is changed for an organization' do
        before do
          organization.external_id = 'ejhgrnseg'
        end

        it 'contains OrganizationExternalIdChanged event' do
          assert_event! :external_id_changed
        end
      end
    end
  end

  describe 'with no events to encode' do
    it 'any? returns false' do
      assert_equal false, OrganizationEventsProtobufEncoder.new(organization).any?
    end
  end

  describe '#clear' do
    before do
      organization.id = 3583495
      organization.domain_events << OrganizationCustomFieldChangedProtobufEncoder.new(cf_fields(:org_integer1), nil, "1").to_object
    end

    it 'clears inferred and explicit domain events' do
      refute_empty protobuf_encoder.inferred_events
      refute_empty organization.domain_events
      protobuf_encoder.clear
      assert_empty protobuf_encoder.inferred_events
      assert_empty organization.domain_events
    end
  end

  def assert_event!(event_type)
    ids = proto_organization_events.map { |e| e.organization.id&.value }
    ids.must_include(organization.id)
    events = proto_organization_events.map(&:event)
    events.must_include(event_type)
  end
end
