require_relative "../support/test_helper"

SingleCov.covered! uncovered: 0

describe TicketFieldEntryValueProtobufEncoder do
  fixtures :ticket_fields

  let(:ticket_field)       { ticket_fields(:field_integer_custom) }
  let(:ticket_field_value) { 1 }

  let(:subject) do
    TicketFieldEntryValueProtobufEncoder.new
  end

  describe '#encode_value' do
    describe 'when ticket field type is date' do
      let(:ticket_field) { ticket_fields(:field_date_custom) }
      let(:ticket_field_value) { '2019-06-21' }

      it 'contains correct ticket field entry' do
        assert subject.encode_value(
          ticket_field,
          ticket_field_value
        )[:date_value]
      end

      describe 'and there is an error' do
        let(:ticket_field_value) { '2737907006988507635340185-11-15' }

        it 'returns nil' do
          assert_equal(
            {date_value: nil},
            subject.encode_value(ticket_field, ticket_field_value)
          )
        end
      end
    end

    describe 'when ticket field type is checkbox' do
      let(:ticket_field) { ticket_fields(:field_checkbox_custom) }
      let(:ticket_field_value) { '1' }

      it 'contains correct ticket field entry' do
        assert_equal(
          {checkbox_value: true},
          subject.encode_value(ticket_field, ticket_field_value)
        )
      end
    end

    describe 'when ticket field type is decimal' do
      let(:ticket_field) { ticket_fields(:field_decimal_custom) }
      let(:ticket_field_value) { '1.0' }

      it 'contains correct ticket field entry' do
        assert_equal(
          {decimal_string: ticket_field_value},
          subject.encode_value(ticket_field, ticket_field_value)
        )
      end
    end

    describe 'when ticket field type is regexp' do
      let(:ticket_field) { ticket_fields(:field_regexp_custom) }
      let(:ticket_field_value) { '1' }

      it 'contains correct ticket field entry' do
        assert_equal(
          {regexp_value: ticket_field_value},
          subject.encode_value(ticket_field, ticket_field_value)
        )
      end
    end

    describe 'when ticket field type is multi-select' do
      let(:ticket_field) { FieldMultiselect.new }
      let(:ticket_field_value) { '1' }

      it 'contains correct ticket field entry' do
        assert subject.encode_value(
          ticket_field,
          ticket_field_value
        )[:multiselect_value]
      end
    end

    describe 'when ticket field type is tag' do
      let(:ticket_field) { ticket_fields(:field_tagger_custom) }
      let(:ticket_field_value) { '1, 2' }

      it 'contains correct ticket field entry' do
        assert_equal(
          {tagger_value: ticket_field_value},
          subject.encode_value(ticket_field, ticket_field_value)
        )
      end
    end

    describe 'when ticket field type is text' do
      let(:ticket_field) { ticket_fields(:field_text_custom) }
      let(:ticket_field_value) { 'one' }

      it 'contains correct ticket field entry' do
        assert_equal(
          {text_value: ticket_field_value},
          subject.encode_value(ticket_field, ticket_field_value)
        )
      end
    end

    describe 'when ticket field type is text-area' do
      let(:ticket_field) { ticket_fields(:field_textarea_custom) }
      let(:ticket_field_value) { 'one two' }

      it 'contains correct ticket field entry' do
        assert_equal(
          {text_area_value: ticket_field_value},
          subject.encode_value(ticket_field, ticket_field_value)
        )
      end
    end

    describe 'when ticket field type is credit-card' do
      let(:ticket_field) { FieldPartialCreditCard.new }
      let(:ticket_field_value) { '1' }

      it 'contains correct ticket field entry' do
        assert_equal(
          {credit_card_value: ticket_field_value},
          subject.encode_value(ticket_field, ticket_field_value)
        )
      end
    end

    describe 'when ticket field type is unknown' do
      let(:ticket_field) { TicketField.new }
      let(:ticket_field_value) { '1' }

      it 'contains correct ticket field entry' do
        assert_raises(ArgumentError) do
          subject.encode_value(ticket_field, ticket_field_value)
        end
      end
    end
  end
end
