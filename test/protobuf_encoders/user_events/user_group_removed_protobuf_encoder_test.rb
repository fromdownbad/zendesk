require_relative "../../support/test_helper"

SingleCov.covered!

describe UserGroupRemovedProtobufEncoder do
  fixtures :users, :groups
  let(:user) { users(:minimum_end_user) }
  let(:group) { groups(:minimum_group) }
  let(:subject) { UserGroupRemovedProtobufEncoder.new(user, group) }

  it 'builds an UserGroupRemoved event' do
    object = subject.to_object
    object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
    object.user_group_removed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserGroupRemoved)
  end
end
