require_relative "../../support/test_helper"

SingleCov.covered!

describe UserTimeZoneChangedProtobufEncoder do
  fixtures :users
  let(:subject) { UserTimeZoneChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has change their timezone' do
    before do
      user.time_zone = 'Kyev'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TimeZoneChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.time_zone_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::TimeZoneChanged)
      object.time_zone_changed.previous.value.must_equal(user.time_zone_was)
      object.time_zone_changed.current.value.must_equal(user.time_zone)
    end
  end
end
