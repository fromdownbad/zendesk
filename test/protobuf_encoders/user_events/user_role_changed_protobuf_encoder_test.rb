require_relative "../../support/test_helper"

SingleCov.covered!

describe UserRoleChangedProtobufEncoder do
  fixtures :users, :events
  let(:subject) { UserRoleChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has changed their role' do
    before do
      user.roles = Role::AGENT.id
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserRoleChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_role_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::SupportUserRoleChanged)
      object.user_role_changed.previous.role_id.value.must_equal(user.roles_was)
      object.user_role_changed.current.role_id.value.must_equal(user.roles)
    end

    it 'when user is created, not matches' do
      user.save
      subject.matches?.must_equal false
    end
  end
end
