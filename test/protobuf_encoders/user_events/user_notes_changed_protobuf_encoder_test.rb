require_relative "../../support/test_helper"

SingleCov.covered!

describe UserNotesChangedProtobufEncoder do
  fixtures :users
  let(:subject) { UserNotesChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has change their notes' do
    before do
      user.notes = 'test john doe'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserNotesChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_notes_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserNotesChanged)
      object.user_notes_changed.previous.must_be_nil
      object.user_notes_changed.current.value.must_equal(user.notes)
    end

    it 'when user is created, not matches' do
      user.save
      subject.matches?.must_equal false
    end
  end
end
