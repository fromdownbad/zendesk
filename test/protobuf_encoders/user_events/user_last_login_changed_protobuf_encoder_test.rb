require_relative "../../support/test_helper"

SingleCov.covered!

describe UserLastLoginChangedProtobufEncoder do
  describe "when previous and current params are the same" do
    it "raises an DoesNotMatch error" do
      assert_raises BaseUserEventProtobufEncoder::DoesNotMatch do
        timestamp = 10.minutes.ago
        UserLastLoginChangedProtobufEncoder.new(timestamp, timestamp).to_object
      end
    end
  end

  describe "when previous and current params are different" do
    let(:previous) { 30.minutes.ago }
    let(:current) { 10.minutes.ago }
    let(:subject) { UserLastLoginChangedProtobufEncoder.new(previous, current) }
    it "builds a UserLastLoginChanged event" do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.last_login_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::LastLoginChanged)
      object.last_login_changed.previous.seconds.must_equal(previous.to_i)
      object.last_login_changed.current.seconds.must_equal(current.to_i)
      object.last_login_changed.previous.nanos.must_equal(previous.nsec)
      object.last_login_changed.current.nanos.must_equal(current.nsec)
    end
  end
end
