require_relative "../../support/test_helper"

SingleCov.covered!

describe UserAliasChangedProtobufEncoder do
  fixtures :users, :events
  let(:user) { users(:minimum_end_user) }
  let(:new_alias) { 'x men' }
  let(:subject) { UserAliasChangedProtobufEncoder.new(user, user.alias, new_alias) }

  describe 'existed user' do
    it 'should not have UserAliasChanged event' do
      assert user.domain_events.length, 0
    end
  end

  describe 'user has change their alias' do
    before do
      user.alias = new_alias
      user.save!
    end

    it 'builds a UserAliasChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_alias_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserAliasChanged)
    end
  end
end
