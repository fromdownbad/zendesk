require_relative "../../support/test_helper"

SingleCov.covered!

describe UserIdentityRemovedProtobufEncoder do
  fixtures :user_identities
  let(:subject) { UserIdentityRemovedProtobufEncoder.new(user_identity) }
  let(:user_identity) { user_identities(:minimum_end_user) }

  describe 'a valid user identity event' do
    it 'builds a UserIdentityRemoved event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_identity_removed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserIdentityRemoved)
      object.user_identity_removed.identity.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserIdentity)
      object.user_identity_removed.identity.id.value.must_equal user_identity.id
      object.user_identity_removed.identity.type.must_equal :EMAIL
      object.user_identity_removed.identity.value.value.must_equal user_identity.value
      object.user_identity_removed.identity.primary.value.must_equal user_identity.primary?
    end
  end
end
