require_relative "../../support/test_helper"

SingleCov.covered!

describe UserIdentityChangedProtobufEncoder do
  fixtures :user_identities
  let(:subject) { UserIdentityChangedProtobufEncoder.new(user_identity, "minimum_end_user_1@aghassipour.com") }
  let(:user_identity) { user_identities(:minimum_end_user) }

  describe 'a valid user identity event' do
    it 'builds a UserIdentityChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_identity_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserIdentityChanged)
      object.user_identity_changed.current.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserIdentity)
      object.user_identity_changed.current.id.value.must_equal user_identity.id
      object.user_identity_changed.current.type.must_equal :EMAIL
      object.user_identity_changed.current.value.value.must_equal user_identity.value
      object.user_identity_changed.current.primary.value.must_equal user_identity.primary?

      object.user_identity_changed.previous.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserIdentity)
      object.user_identity_changed.previous.id.value.must_equal user_identity.id
      object.user_identity_changed.previous.type.must_equal :EMAIL
      object.user_identity_changed.previous.value.value.must_equal "minimum_end_user_1@aghassipour.com"
      object.user_identity_changed.previous.primary.value.must_equal user_identity.primary?
    end
  end
end
