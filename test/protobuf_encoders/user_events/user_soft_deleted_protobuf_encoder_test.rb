require_relative "../../support/test_helper"

SingleCov.covered!

describe UserSoftDeletedProtobufEncoder do
  fixtures :users
  let(:subject) { UserSoftDeletedProtobufEncoder.new(soft_deleted_user) }
  let(:soft_deleted_user) { users(:minimum_end_user2) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'an existing user' do
    before do
      soft_deleted_user.is_active = false
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserSoftDeleted event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_soft_deleted.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserSoftDeleted)
    end
  end
end
