require_relative "../../support/test_helper"

SingleCov.covered!

describe UserCustomRoleChangedProtobufEncoder do
  fixtures :users, :events
  let(:subject) { UserCustomRoleChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_agent) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'when user is created' do
    it 'does not match' do
      user.save
      subject.matches?.must_equal false
    end
  end

  describe 'user has changed their custom role' do
    before do
      user.permission_set_id = 123456
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserCustomRoleChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.custom_role_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::SupportCustomRoleChanged)
      object.custom_role_changed.previous.permission_set_id.value.must_equal(0)
      object.custom_role_changed.current.permission_set_id.value.must_equal(user.permission_set_id)
    end
  end
end
