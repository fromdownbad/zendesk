require_relative "../../support/test_helper"

SingleCov.covered!

describe UserTagsChangedProtobufEncoder do
  fixtures :users
  let(:subject) { UserTagsChangedProtobufEncoder.new(user) }
  let(:user) { users(:with_groups_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has change their tags' do
    before do
      user.tags = 'test1 test2'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TagsChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.tags_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::TagsChanged)
      object.tags_changed.tags_added.tags.must_equal %w[test1 test2]
      object.tags_changed.tags_removed.tags.must_equal []
    end

    it 'when user is created, not matches' do
      user.save
      subject.matches?.must_equal false
    end
  end
end
