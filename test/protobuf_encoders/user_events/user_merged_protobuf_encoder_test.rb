require_relative "../../support/test_helper"

SingleCov.covered!

describe UserMergedProtobufEncoder do
  fixtures :users
  let(:winner) { users(:minimum_end_user2) }
  let(:subject) { UserMergedProtobufEncoder.new(winner.id) }

  describe 'loser is merged into winner' do
    it 'builds a UserMerged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_merged.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserMerged)
    end
  end
end
