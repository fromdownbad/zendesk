require_relative "../../support/test_helper"

SingleCov.covered!

describe UserCreatedProtobufEncoder do
  fixtures :users, :events
  let(:subject) { UserCreatedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a newly created user' do
    before do
      user.id = 1234
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserCreated event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_created.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserCreated)
    end
  end
end
