require_relative "../../support/test_helper"

SingleCov.covered!

describe UserPhotoChangedProtobufEncoder do
  fixtures :users, :photos
  let(:subject) { UserPhotoChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe "user doesn't update the photo" do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe "user updates the photo" do
    before do
      user.set_photo(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png"))
      user.save!
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserPhotoChangedProtobufEncoder event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_photo_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserPhotoChanged)
    end
  end

  describe "user deletes the photo" do
    let(:photo) { photos(:minimum_photo) }
    let(:user) { photo.user }
    let(:subject) { UserPhotoChangedProtobufEncoder.new(user) }
    let(:url) { 'https://zendesk-test.s3.us-west-2.amazonaws.com/public/system/photos/20090590/minimum_photo.jpg' }

    before do
      stub_request(:delete, url).to_return(status: 200, body: "", headers: {})
      photo.destroy
      photo.user.dispatch_event_after_photo_deletion
    end

    it 'matches' do
      subject.matches?.must_equal true
    end
  end
end
