require_relative "../../support/test_helper"

SingleCov.covered!

describe UserDetailsChangedProtobufEncoder do
  fixtures :users, :events
  let(:subject) { UserDetailsChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has change their details' do
    before do
      user.details = 'test details'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserDetailsChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_details_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserDetailsChanged)
      object.user_details_changed.previous.must_be_nil
      object.user_details_changed.current.value.must_equal(user.details)
    end

    it 'when user is created, not matches' do
      user.save
      subject.matches?.must_equal false
    end
  end
end
