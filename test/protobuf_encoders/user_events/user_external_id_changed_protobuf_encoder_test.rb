require_relative "../../support/test_helper"

SingleCov.covered!

describe UserExternalIdChangedProtobufEncoder do
  fixtures :users
  let(:subject) { UserExternalIdChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has change their external_id' do
    before do
      user.external_id = 'omega'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a ExternalIdChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.external_id_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::ExternalIdChanged)
      object.external_id_changed.previous.must_be_nil
      object.external_id_changed.current.value.must_equal(user.external_id)
    end

    it 'when user is created, not matches' do
      user.save
      subject.matches?.must_equal false
    end
  end
end
