require_relative "../../support/test_helper"

SingleCov.covered!

describe UserIsActiveChangedProtobufEncoder do
  fixtures :users
  let(:subject) { UserIsActiveChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has change their activity' do
    before do
      user.is_active = false
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserNameChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_is_active_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserIsActiveChanged)
      object.user_is_active_changed.previous.value.must_equal(true)
      object.user_is_active_changed.current.value.must_equal(false)
    end
  end
end
