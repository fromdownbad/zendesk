require_relative "../../support/test_helper"

SingleCov.covered!

describe UserOnlyPrivateCommentsChangedProtobufEncoder do
  fixtures :users
  let(:subject) { UserOnlyPrivateCommentsChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has change their only_private_comments' do
    before do
      user.is_private_comments_only = true
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a OnlyPrivateCommentsChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.only_private_comments_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OnlyPrivateCommentsChanged)
      object.only_private_comments_changed.previous.value.must_equal(user.is_private_comments_only_was)
      object.only_private_comments_changed.current.value.must_equal(user.is_private_comments_only)
    end

    it 'when user is created, not matches' do
      user.save
      subject.matches?.must_equal false
    end
  end
end
