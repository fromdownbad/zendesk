require_relative "../../support/test_helper"

SingleCov.covered!

describe UserIdentityCreatedProtobufEncoder do
  fixtures :user_identities
  let(:subject) { UserIdentityCreatedProtobufEncoder.new(user_identity) }
  let(:user_identity) { user_identities(:minimum_end_user) }

  describe 'a valid user identity event' do
    it 'builds a UserIdentityCreated event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_identity_created.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserIdentityCreated)
      object.user_identity_created.identity.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserIdentity)
      object.user_identity_created.identity.id.value.must_equal user_identity.id
      object.user_identity_created.identity.type.must_equal :EMAIL
      object.user_identity_created.identity.value.value.must_equal user_identity.value
      object.user_identity_created.identity.primary.value.must_equal user_identity.primary?
    end
  end
end
