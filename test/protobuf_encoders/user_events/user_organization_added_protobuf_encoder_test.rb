require_relative "../../support/test_helper"

SingleCov.covered!

describe UserOrganizationAddedProtobufEncoder do
  fixtures :users, :organizations
  let(:user) { users(:minimum_end_user) }
  let(:organization) { organizations(:minimum_organization1) }
  let(:subject) { UserOrganizationAddedProtobufEncoder.new(user, organization) }

  it 'builds an UserOrganizationAdded event' do
    object = subject.to_object
    object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
    object.user_organization_added.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserOrganizationAdded)
  end
end
