require_relative "../../support/test_helper"

SingleCov.covered!

describe UserGroupAddedProtobufEncoder do
  fixtures :users, :groups
  let(:user) { users(:minimum_end_user) }
  let(:group) { groups(:minimum_group) }
  let(:subject) { UserGroupAddedProtobufEncoder.new(user, group) }

  it 'builds an UserGroupAdded event' do
    object = subject.to_object
    object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
    object.user_group_added.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserGroupAdded)
  end
end
