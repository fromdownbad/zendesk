require_relative "../../support/test_helper"

SingleCov.covered!

describe UserDefaultGroupChangedProtobufEncoder do
  fixtures :users
  let(:agent) { users(:with_groups_agent2) }

  describe "default group updated" do
    let(:subject) do
      default_group = agent.memberships.detect(&:default?).group
      new_default_group = agent.memberships.reject(&:default?).first.group
      UserDefaultGroupChangedProtobufEncoder.new(agent, previous: default_group, current: new_default_group)
    end

    it "builds a UserDefaultGroupChangedProtobufEncoder event" do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_default_group_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserDefaultGroupChanged)
    end
  end

  describe "membership with default group deleted" do
    let(:subject) do
      default_group = agent.memberships.detect(&:default?).group
      UserDefaultGroupChangedProtobufEncoder.new(agent, previous: default_group, current: nil)
    end

    it "builds a UserDefaultGroupChangedProtobufEncoder event" do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_default_group_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserDefaultGroupChanged)
    end
  end
end
