require_relative "../../support/test_helper"

SingleCov.covered!

describe UserNameChangedProtobufEncoder do
  fixtures :users, :events
  let(:subject) { UserNameChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has change their name' do
    before do
      user.name = 'test john doe'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserNameChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_name_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserNameChanged)
      object.user_name_changed.previous.value.must_equal(user.name_was)
      object.user_name_changed.current.value.must_equal(user.name)
    end

    it 'when user is created, not matches' do
      user.save
      subject.matches?.must_equal false
    end
  end
end
