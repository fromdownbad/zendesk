require_relative "../../support/test_helper"

SingleCov.covered!

describe UserPasswordChangedProtobufEncoder do
  fixtures :users
  let(:subject) { UserPasswordChangedProtobufEncoder.new(user) }
  let(:user) { users(:minimum_end_user) }

  describe 'an existing user' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'user has change their password' do
    before do
      user.crypted_password = 'YwxvIDH3B4T063NVHAsmFh1AAAAA'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a UserPasswordChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
      object.user_password_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserPasswordChanged)
    end
  end
end
