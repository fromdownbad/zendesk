require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1 # on the `raise ArgumentError` catch all

describe CustomFieldChangedProtobufEncoder do
  fixtures :tickets, :ticket_fields, :custom_field_options
  let(:subject) { CustomFieldChangedProtobufEncoder.new(field, previous_field_value, field_value) }
  let(:object) { subject.to_object }
  let(:ticket) { tickets(:minimum_1) }
  let(:field) { ticket_fields(:field_integer_custom) }
  let(:previous_field_value) { nil }
  let(:field_value) { '1337' }

  it 'provides TicketField context' do
    object.custom_field_changed.ticket_field.id.value.must_equal field.id
    object.custom_field_changed.ticket_field.raw_title.value.must_equal field.title
  end

  describe 'setting new an integer field' do
    it 'builds a CustomFieldChanged event' do
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.custom_field_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::CustomFieldChanged)
      object.custom_field_changed.previous.must_be_nil
      object.custom_field_changed.current.integer_string.must_equal '1337'
    end
  end

  describe 'clearing an existing field' do
    let(:previous_field_value) { '1337' }
    let(:field_value) { ' ' }

    it 'builds a null current' do
      object.custom_field_changed.previous.integer_string.must_equal '1337'
      object.custom_field_changed.current.must_be_nil
    end
  end

  describe 'other fields' do
    describe 'checkbox' do
      let(:field) { ticket_fields(:field_checkbox_custom) }
      let(:field_value) { '1' }
      it 'builds a CustomFieldChanged event' do
        object.custom_field_changed.current.checkbox_value.must_equal true
      end
    end

    describe 'date' do
      let(:field) { ticket_fields(:field_date_custom) }
      let(:field_value) { '2019-06-21' }
      it 'builds a CustomFieldChanged event' do
        object.custom_field_changed.current.date_value.year.must_equal 2019
        object.custom_field_changed.current.date_value.month.must_equal 6
        object.custom_field_changed.current.date_value.day.must_equal 21
      end

      describe 'clearing an existing date' do
        let(:previous_field_value) { '2019-06-21' }
        let(:field_value) { ' ' }
        it 'builds a null current' do
          object.custom_field_changed.previous.date_value.year.must_equal 2019
          object.custom_field_changed.previous.date_value.month.must_equal 6
          object.custom_field_changed.previous.date_value.day.must_equal 21
          object.custom_field_changed.current.must_be_nil
        end
      end

      describe 'invalid date' do
        let(:field_value) { '2020-02-31' }
        it 'builds a null date_value' do
          object.custom_field_changed.current.date_value.must_be_nil
        end
      end

      describe 'out of range date' do
        # setting 999999999999999999999999999 'days from now' in a trigger action will will overflow when we convert it in the action executer
        let(:field_value) { '2737907006988507635340185-11-15' }
        it 'builds a null date_value' do
          object.custom_field_changed.current.date_value.must_be_nil
        end
      end
    end

    describe 'decimal' do
      let(:field) { ticket_fields(:field_decimal_custom) }
      let(:field_value) { '1337.0' }
      it 'builds a CustomFieldChanged event' do
        object.custom_field_changed.current.decimal_string.must_equal '1337.0'
      end
    end

    describe 'regexp' do
      let(:field) { ticket_fields(:field_regexp_ssn_custom) }
      let(:field_value) { '123-45-6789' }
      it 'builds a CustomFieldChanged event' do
        object.custom_field_changed.current.regexp_value.must_equal '123-45-6789'
      end
    end

    describe 'tagger' do
      let(:field) { ticket_fields(:field_tagger_custom) }
      let(:field_value) { 'hilarious' }
      it 'builds a CustomFieldChanged event' do
        object.custom_field_changed.current.tagger_value.must_equal 'hilarious'
      end
    end

    describe 'text' do
      let(:field) { ticket_fields(:field_text_custom) }
      let(:field_value) { 'bees' }
      it 'builds a CustomFieldChanged event' do
        object.custom_field_changed.current.text_value.must_equal 'bees'
      end
    end

    describe 'textarea' do
      let(:field) { ticket_fields(:field_textarea_custom) }
      let(:field_value) { 'more bees' }
      it 'builds a CustomFieldChanged event' do
        object.custom_field_changed.current.text_area_value.must_equal 'more bees'
      end
    end

    describe 'multiselect' do
      let(:field) do
        FieldMultiselect.create!(
          account: accounts(:minimum),
          title: "Multiselect Wombat",
          custom_field_options: [
            CustomFieldOption.new(name: "Wombats", value: "wombats"),
            CustomFieldOption.new(name: "Giraffes", value: "giraffes"),
            CustomFieldOption.new(name: "Tazzmanian Devils", value: "tazzies"),
            CustomFieldOption.new(name: "Lemurs", value: "lemurs")
          ]
        )
      end
      let(:field_value) { 'wombats tazzies' }
      it 'builds a CustomFieldChanged event' do
        object.custom_field_changed.current.multiselect_value.tags.sort.must_equal %w[wombats tazzies].sort
      end
    end

    describe 'credit card' do
      let(:field) { FieldPartialCreditCard.create!(account: ticket.account, title: 'credit card field') }
      let(:field_value) { '4242424242424242' }
      it 'builds a CustomFieldChanged event' do
        object.custom_field_changed.current.credit_card_value.must_equal '4242424242424242'
      end
    end
  end
end
