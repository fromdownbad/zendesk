require_relative "../../support/test_helper"

SingleCov.covered!

describe TaskDueAtChangedProtobufEncoder do
  fixtures :tickets
  let(:due_at_encoder) { TaskDueAtChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing the due date' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!due_at_encoder.matches?).must_equal false
    end
  end

  describe 'a new ticket with a due_date' do
    let(:due_date) { 3.days.from_now }
    let(:ticket) { accounts(:minimum).tickets.build(due_date: due_date) }

    it 'matches' do
      due_at_encoder.matches?.must_equal true
    end

    it 'builds a TaskDueAtChanged event' do
      Timecop.freeze do
        object = due_at_encoder.to_object
        object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
        object.task_due_at_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TaskDueAtChanged)
        object.task_due_at_changed.previous.must_be_nil
        object.task_due_at_changed.current.seconds.must_equal(due_date.to_i)
      end
    end
  end

  describe 'when changing the due date' do
    let(:previous_due_date) { 1.day.from_now }
    let(:current_due_date) { 2.days.from_now }
    before do
      ticket.update_columns(due_date: previous_due_date)
      ticket.reload
      ticket.due_date = current_due_date
    end

    it 'matches' do
      due_at_encoder.matches?.must_equal true
    end

    it 'builds a TaskDueAtChanged event' do
      Timecop.freeze do
        object = due_at_encoder.to_object
        object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
        object.task_due_at_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TaskDueAtChanged)
        object.task_due_at_changed.previous.seconds.must_equal(previous_due_date.to_i)
        object.task_due_at_changed.current.seconds.must_equal(current_due_date.to_i)
      end
    end
  end
end
