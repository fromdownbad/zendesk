require_relative "../../support/test_helper"

SingleCov.covered!

describe AttachmentLinkedToCommentProtobufEncoder do
  fixtures :events, :attachments

  let(:encoder) { AttachmentLinkedToCommentProtobufEncoder.new(attachment, comment) }

  let(:comment) { events(:create_comment_for_minimum_ticket_1) }
  let(:attachment) { attachments(:serialization_comment_attachment) }

  describe '#to_object' do
    it 'builds the attachment linked event' do
      object = encoder.to_object

      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.attachment_linked_to_comment.attachment.id.value.must_equal attachment.id
      object.attachment_linked_to_comment.attachment.content_type.value.must_equal attachment.content_type

      object.attachment_linked_to_comment.comment.id.value.must_equal comment.id
    end
  end
end
