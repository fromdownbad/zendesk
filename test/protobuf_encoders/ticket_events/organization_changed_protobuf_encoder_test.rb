require_relative "../../support/test_helper"

SingleCov.covered!

describe OrganizationChangedProtobufEncoder do
  fixtures :tickets, :organizations
  let(:subject) { OrganizationChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_2) } # minimum_1 does not have an organization

  describe 'existing ticket not changing changing the status' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with a organization' do
    let(:ticket) { accounts(:minimum).tickets.build(organization: organizations(:minimum_organization2)) }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a OrganizationChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.organization_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::OrganizationChanged)
      object.organization_changed.previous.must_be_nil
      object.organization_changed.current.id.value.must_equal organizations(:minimum_organization2).id
    end
  end

  describe 'when assigning to a different agent' do
    before do
      ticket.organization = organizations(:minimum_organization2)
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a OrganizationChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.organization_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::OrganizationChanged)
      object.organization_changed.previous.id.value.must_equal organizations(:minimum_organization1).id
      object.organization_changed.current.id.value.must_equal organizations(:minimum_organization2).id
    end
  end

  describe 'when unassigning a ticket' do
    before do
      ticket.organization = nil
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a OrganizationChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.organization_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::OrganizationChanged)
      object.organization_changed.previous.id.value.must_equal organizations(:minimum_organization1).id
      object.organization_changed.current.must_be_nil
    end
  end
end
