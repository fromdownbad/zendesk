require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketUndeletedProtobufEncoder do
  fixtures :tickets, :users, :events
  let(:subject) { TicketUndeletedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'restoring a soft deleted ticket' do
    before do
      ticket.will_be_saved_by(User.system)
      ticket.soft_delete!
      # No `mark_as_undeleted` method as of now.
      ticket.status_id = ticket.previous_status
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TicketUndeleted event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_undeleted.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketUndeleted)
    end
  end
end
