require_relative "../../support/test_helper"

SingleCov.covered!

describe GroupAssignmentChangedProtobufEncoder do
  fixtures :tickets, :groups
  let(:subject) { GroupAssignmentChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing changing the group' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with a group' do
    let(:ticket) { accounts(:minimum).tickets.build(group: groups(:minimum_group)) }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a GroupAssignmentChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.group_assignment_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::GroupAssignmentChanged)
      object.group_assignment_changed.previous.must_be_nil
      object.group_assignment_changed.current.id.value.must_equal groups(:minimum_group).id
    end
  end

  describe 'when assigning to a different group' do
    before do
      ticket.group = groups(:support_group)
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a GroupAssignmentChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.group_assignment_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::GroupAssignmentChanged)
      object.group_assignment_changed.previous.id.value.must_equal groups(:minimum_group).id
      object.group_assignment_changed.current.id.value.must_equal groups(:support_group).id
    end
  end

  describe 'when unassigning the group on a ticket' do
    before do
      ticket.group = nil
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a GroupAssignmentChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.group_assignment_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::GroupAssignmentChanged)
      object.group_assignment_changed.previous.id.value.must_equal groups(:minimum_group).id
      object.group_assignment_changed.current.must_be_nil
    end
  end
end
