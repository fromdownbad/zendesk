require_relative "../../support/test_helper"

SingleCov.covered!

describe CommentRedactedProtobufEncoder do
  fixtures :users, :events
  let(:subject) { CommentRedactedProtobufEncoder.new(comment) }
  let(:comment) { events(:create_comment_for_minimum_ticket_1) }

  describe 'a redacted comment' do
    it 'builds a CommentRedacted event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.comment_redacted.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::CommentRedacted)
      object.comment_redacted.comment.body.value.must_equal 'minimum ticket 1'
      object.comment_redacted.comment.is_public.value.must_equal true
    end
  end
end
