require_relative "../../support/test_helper"

SingleCov.covered!

describe DescriptionChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { DescriptionChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing changing the status' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  # Note: Description can only be set at creation, and can not be updated.
  describe 'a new ticket with a description' do
    let(:ticket) { accounts(:minimum).tickets.build(description: 'This is a new ticket') }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a DescriptionChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.description_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::DescriptionChanged)
      object.description_changed.previous.must_be_nil
      object.description_changed.current.value.must_equal 'This is a new ticket'
    end
  end
end
