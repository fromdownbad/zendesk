require_relative "../../support/test_helper"

SingleCov.covered!

describe AttachmentRedactedFromCommentProtobufEncoder do
  fixtures :events, :attachments

  let(:encoder) { AttachmentRedactedFromCommentProtobufEncoder.new(attachment, comment) }

  let(:comment) { events(:create_comment_for_minimum_ticket_1) }
  let(:attachment) { attachments(:serialization_comment_attachment) }

  describe '#to_object' do
    it 'builds the attachment redacted event' do
      object = encoder.to_object

      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.attachment_redacted_from_comment.attachment.id.value.must_equal attachment.id
      object.attachment_redacted_from_comment.comment.id.value.must_equal comment.id
    end
  end
end
