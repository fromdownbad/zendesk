require_relative "../../support/test_helper"

SingleCov.covered!

describe ProblemLinkChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { ProblemLinkChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing the problem link' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with a problem' do
    let(:ticket) do
      accounts(:minimum).tickets.build.tap do |t|
        t.problem = tickets(:minimum_2)
      end
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a ProblemLinkChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.problem_link_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::ProblemLinkChanged)
      object.problem_link_changed.previous.must_be_nil
      object.problem_link_changed.current.id.value.must_equal tickets(:minimum_2).id
      object.problem_link_changed.current.nice_id.value.must_equal tickets(:minimum_2).nice_id
    end
  end

  describe 'when linking to a problem' do
    before do
      ticket.problem = tickets(:minimum_2)
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a ProblemLinkChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.problem_link_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::ProblemLinkChanged)
      object.problem_link_changed.previous.must_be_nil
      object.problem_link_changed.current.id.value.must_equal tickets(:minimum_2).id
      object.problem_link_changed.current.nice_id.value.must_equal tickets(:minimum_2).nice_id
    end
  end

  describe 'with a linked problem' do
    before do
      ticket.update_columns(linked_id: tickets(:minimum_2).id)
    end

    describe 'when unlinking from a problem' do
      before do
        ticket.problem = nil
      end

      it 'matches' do
        subject.matches?.must_equal true
      end

      it 'builds a ProblemLinkChanged event with no current problem' do
        object = subject.to_object
        object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
        object.problem_link_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::ProblemLinkChanged)
        object.problem_link_changed.previous.id.value.must_equal tickets(:minimum_2).id
        object.problem_link_changed.previous.nice_id.value.must_equal tickets(:minimum_2).nice_id
        object.problem_link_changed.current.must_be_nil
      end
    end

    describe 'when the linked problem is soft-deleted' do
      before do
        linked_ticket = ticket.problem
        linked_ticket.will_be_saved_by(accounts(:minimum).owner)
        linked_ticket.soft_delete!

        ticket.problem = nil
      end

      it 'builds a ProblemLinkChanged event with the old deleted problem in the event' do
        object = subject.to_object
        object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
        object.problem_link_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::ProblemLinkChanged)
        object.problem_link_changed.previous.id.value.must_equal tickets(:minimum_2).id
        object.problem_link_changed.previous.nice_id.value.must_equal tickets(:minimum_2).nice_id
        object.problem_link_changed.current.must_be_nil
      end
    end
  end
end
