require_relative "../../support/test_helper"

SingleCov.covered!

describe CommentMadePrivateProtobufEncoder do
  fixtures :events
  let(:subject) { CommentMadePrivateProtobufEncoder.new(comment) }
  let(:comment) { events(:create_comment_for_minimum_ticket_1) }

  describe 'a private comment' do
    before do
      comment.is_public = false
    end

    it 'builds a CommentMadePrivate event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.comment_made_private.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::CommentMadePrivate)
      object.comment_made_private.comment.id.value.must_equal comment.id
      object.comment_made_private.comment.is_public.value.must_equal false
    end
  end
end
