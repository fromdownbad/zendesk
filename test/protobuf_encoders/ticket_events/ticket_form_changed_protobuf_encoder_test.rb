require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketFormChangedProtobufEncoder do
  fixtures :accounts, :tickets, :ticket_forms
  let(:account) { accounts(:minimum) }
  let(:ticket_form) { FactoryBot.create(:ticket_form, account: account) }
  let(:ticket) do
    tickets(:minimum_1).tap do |t|
      t.ticket_form = ticket_form
      t.will_be_saved_by(account.owner)
      t.save
    end
  end
  let(:subject) { TicketFormChangedProtobufEncoder.new(ticket) }

  describe 'existing ticket not changing changing the status' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with a ticket form' do
    let(:ticket) { account.tickets.build(ticket_form_id: ticket_form.id) }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TicketFormChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_form_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketFormChanged)
      object.ticket_form_changed.previous.must_be_nil
      object.ticket_form_changed.current.id.value.must_equal ticket_form.id
    end
  end

  describe 'when assigning to a different ticket form' do
    let(:diff_ticket_form) { FactoryBot.create(:ticket_form, account: account, name: "Wombat Form") }

    before do
      ticket.ticket_form = diff_ticket_form
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TicketFormChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_form_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketFormChanged)
      object.ticket_form_changed.previous.id.value.must_equal ticket_form.id
      object.ticket_form_changed.current.id.value.must_equal diff_ticket_form.id
    end
  end

  describe 'when removing a ticket form' do
    before do
      ticket.ticket_form = nil
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TicketFormChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_form_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketFormChanged)
      object.ticket_form_changed.previous.id.value.must_equal ticket_form.id
      object.ticket_form_changed.current.must_be_nil
    end
  end
end
