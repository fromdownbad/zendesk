require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketSoftDeletedProtobufEncoder do
  fixtures :tickets, :users, :events
  let(:subject) { TicketSoftDeletedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'a soft deleted ticket' do
    before do
      # This is the method the soft_deletion gem calls that the Ticket overrides
      # https://github.com/grosser/soft_deletion/blob/v1.3.1/lib/soft_deletion/core.rb#L67
      ticket.mark_as_deleted
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TicketSoftDeleted event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_soft_deleted.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketSoftDeleted)
    end
  end
end
