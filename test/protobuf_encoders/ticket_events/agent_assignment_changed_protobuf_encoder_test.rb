require_relative "../../support/test_helper"

SingleCov.covered!

describe AgentAssignmentChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { AgentAssignmentChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing changing the status' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with the default NEW status' do
    let(:ticket) { accounts(:minimum).tickets.build(assignee: users(:minimum_agent)) }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds an AgentAssignmentChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.agent_assignment_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::AgentAssignmentChanged)
      object.agent_assignment_changed.previous.must_be_nil
      object.agent_assignment_changed.current.id.value.must_equal users(:minimum_agent).id
    end
  end

  describe 'when assigning to a different agent' do
    before do
      ticket.assignee = users(:support_agent)
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds an AgentAssignmentChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.agent_assignment_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::AgentAssignmentChanged)
      object.agent_assignment_changed.previous.id.value.must_equal users(:minimum_agent).id
      object.agent_assignment_changed.current.id.value.must_equal users(:support_agent).id
    end
  end

  describe 'when unassigning a ticket' do
    before do
      ticket.assignee = nil
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds an AgentAssignmentChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.agent_assignment_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::AgentAssignmentChanged)
      object.agent_assignment_changed.previous.id.value.must_equal users(:minimum_agent).id
      object.agent_assignment_changed.current.must_be_nil
    end
  end
end
