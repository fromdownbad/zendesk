require_relative "../../support/test_helper"

SingleCov.covered!

describe EmailCcsChangedProtobufEncoder do
  fixtures :users

  let(:end_user_wombat) { users(:minimum_end_user) }
  let(:end_user_lemur) { users(:minimum_author) }

  describe 'adding a new cc' do
    let(:subject) { EmailCcsChangedProtobufEncoder.new([end_user_wombat.id], []) }

    it 'builds a CcsChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ccs_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::CcsChanged)
      object.ccs_changed.users_removed.user_ids.first.must_be_nil
      object.ccs_changed.users_added.user_ids.first.must_equal end_user_wombat.id
    end
  end

  describe 'adding and removing ccs' do
    let(:subject) { EmailCcsChangedProtobufEncoder.new([end_user_lemur.id], [end_user_wombat.id]) }

    it 'builds a CcsChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ccs_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::CcsChanged)
      object.ccs_changed.users_removed.user_ids.first.must_equal end_user_wombat.id
      object.ccs_changed.users_added.user_ids.first.must_equal end_user_lemur.id
    end
  end

  describe 'with no users added/removed' do
    let(:subject) { EmailCcsChangedProtobufEncoder.new([], []) }

    it 'raises ArgumentError' do
      assert_raises ArgumentError do
        subject.to_object
      end
    end
  end
end
