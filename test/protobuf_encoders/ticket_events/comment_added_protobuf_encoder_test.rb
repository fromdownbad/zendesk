require_relative "../../support/test_helper"

SingleCov.covered!

describe CommentAddedProtobufEncoder do
  fixtures :tickets, :users, :events
  let(:subject) { CommentAddedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'an existing ticket without a comment' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a newly created ticket' do
    before do
      ticket.comment = events(:create_comment_for_minimum_ticket_1)
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a CommentAdded event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.comment_added.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::CommentAdded)
      object.comment_added.comment.body.value.must_equal 'minimum ticket 1'
      object.comment_added.comment.is_public.value.must_equal true
      object.comment_added.comment.author.id.value.wont_equal nil
      object.comment_added.comment.author.name.value.must_equal 'Agent Minimum'
      object.comment_added.comment.author.is_staff.value.must_equal true
    end
  end
end
