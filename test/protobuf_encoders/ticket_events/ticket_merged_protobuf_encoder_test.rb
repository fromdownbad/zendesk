require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketMergedProtobufEncoder do
  fixtures :tickets
  let(:subject) { TicketMergedProtobufEncoder.new(source_ticket, target_ticket) }
  let(:source_ticket) { mock }
  let(:target_ticket) { tickets(:minimum_1) }

  describe 'a merged ticket' do
    it 'builds a TicketMerged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_merged.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketMerged)
      object.ticket_merged.target_ticket.id.value.must_equal target_ticket.id
      object.ticket_merged.target_ticket.nice_id.value.must_equal target_ticket.nice_id
    end
  end
end
