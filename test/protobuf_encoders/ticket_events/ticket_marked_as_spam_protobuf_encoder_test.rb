require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketMarkedAsSpamProtobufEncoder do
  let(:subject) { TicketMarkedAsSpamProtobufEncoder.new(Ticket.new) }

  it 'builds a MarkedAsSpam event' do
    object = subject.to_object
    object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
    object.marked_as_spam.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::MarkedAsSpam)
  end
end
