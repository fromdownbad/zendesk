require_relative "../../support/test_helper"

SingleCov.covered!

describe ExternalIdChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { ExternalIdChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing changing the status' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with a external_id' do
    let(:ticket) { accounts(:minimum).tickets.build(external_id: 'a6910582-92ae-11e9-bc42-526af7764f64') }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a ExternalIdChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.external_id_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::ExternalIdChanged)
      object.external_id_changed.previous.must_be_nil
      object.external_id_changed.current.value.must_equal 'a6910582-92ae-11e9-bc42-526af7764f64'
    end
  end

  describe 'when changing external_id' do
    before do
      ticket.update_columns(external_id: 'a6910582-92ae-11e9-bc42-526af7764f64')
      ticket.external_id = 'lJ8ydIuPFeU'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a ExternalIdChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.external_id_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::ExternalIdChanged)
      object.external_id_changed.previous.value.must_equal 'a6910582-92ae-11e9-bc42-526af7764f64'
      object.external_id_changed.current.value.must_equal 'lJ8ydIuPFeU'
    end
  end
end
