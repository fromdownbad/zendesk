require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketCreatedProtobufEncoder do
  fixtures :tickets, :users, :events
  let(:subject) { TicketCreatedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'an existing ticket' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a newly created ticket' do
    before do
      # This is an integration lie, but easier than building a new ticket from scratch.
      ticket.id = 1234
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TicketCreated event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_created.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketCreated)
    end
  end
end
