require_relative "../../support/test_helper"

SingleCov.covered!

describe SubjectChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { SubjectChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing the subject' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with a subject' do
    let(:ticket) { accounts(:minimum).tickets.build(subject: 'This is a new ticket') }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a SubjectChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.subject_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::SubjectChanged)
      object.subject_changed.previous.must_be_nil
      object.subject_changed.current.value.must_equal 'This is a new ticket'
    end
  end

  describe 'when changing the existing subject' do
    before do
      ticket.subject = 'This is a different subject'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a SubjectChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.subject_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::SubjectChanged)
      object.subject_changed.previous.value.must_equal 'minimum 1 ticket'
      object.subject_changed.current.value.must_equal 'This is a different subject'
    end
  end
end
