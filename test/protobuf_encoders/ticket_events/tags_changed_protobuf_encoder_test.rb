require_relative "../../support/test_helper"

SingleCov.covered!

describe TagsChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { TagsChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_2) } # minimum_2 has 'big hello' as tags

  describe 'existing ticket not changing the tags' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket without tags' do
    let(:ticket) { accounts(:minimum).tickets.build }

    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with tags' do
    let(:ticket) do
      accounts(:minimum).tickets.build.tap do |t|
        t.set_tags = 'alpha bravo charlie'
      end
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TagsChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.tags_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TagsChanged)
      object.tags_changed.tags_added.tags.must_equal %w[alpha bravo charlie]
      object.tags_changed.tags_removed.tags.must_equal []
    end
  end

  describe 'when changing tags' do
    before do
      ticket.set_tags = 'small hello farewell'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TagsChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.tags_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TagsChanged)
      object.tags_changed.tags_added.tags.must_equal %w[small farewell]
      object.tags_changed.tags_removed.tags.must_equal %w[big]
    end
  end
end
