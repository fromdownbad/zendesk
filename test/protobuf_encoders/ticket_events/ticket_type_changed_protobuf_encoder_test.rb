require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketTypeChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { TicketTypeChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing the ticket_type' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket without an explicit type' do
    let(:ticket) { accounts(:minimum).tickets.build }

    it 'does not match' do
      # This behavior is open for discussion
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with an explicit type' do
    let(:ticket) { accounts(:minimum).tickets.build(ticket_type_id: TicketType.QUESTION) }

    it 'matches' do
      # This behavior is open for discussion
      (!!subject.matches?).must_equal true
    end

    it 'builds a TicketTypeChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_type_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketTypeChanged)
      object.ticket_type_changed.previous.must_equal :UNKNOWN_TICKET_TYPE
      object.ticket_type_changed.current.must_equal :QUESTION
    end
  end

  describe 'when changing the type' do
    before do
      ticket.ticket_type_id = TicketType.problem
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TicketTypeChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_type_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketTypeChanged)
      object.ticket_type_changed.previous.must_equal :INCIDENT
      object.ticket_type_changed.current.must_equal :PROBLEM
    end
  end
end
