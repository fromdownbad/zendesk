require_relative "../../support/test_helper"

SingleCov.covered!

describe BrandChangedProtobufEncoder do
  fixtures :accounts, :tickets, :brands
  let(:subject) { BrandChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing changing the status' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with a brand' do
    let(:ticket) { accounts(:minimum).tickets.build(brand: brands(:minimum)) }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a BrandChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.brand_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::BrandChanged)
      object.brand_changed.previous.must_be_nil
      object.brand_changed.current.id.value.must_equal brands(:minimum).id
    end
  end

  describe 'when assigning to a different brand' do
    let(:new_brand) do
      ticket.account.brands.create!(name: "Foo") do |b|
        b.build_route(subdomain: "foobars") { |r| r.account = ticket.account }
      end
    end

    before do
      ticket.brand = new_brand
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a BrandChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.brand_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::BrandChanged)
      object.brand_changed.previous.id.value.must_equal brands(:minimum).id
      object.brand_changed.current.id.value.must_equal new_brand.id
    end
  end

  describe 'when removing a brand' do
    before do
      ticket.brand = nil
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a BrandChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.brand_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::BrandChanged)
      object.brand_changed.previous.id.value.must_equal brands(:minimum).id
      object.brand_changed.current.must_be_nil
    end
  end
end
