require_relative "../../support/test_helper"

SingleCov.covered!

describe RequesterChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { RequesterChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing changing the status' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with a requester' do
    let(:ticket) { accounts(:minimum).tickets.build(requester: users(:minimum_author)) }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a RequesterChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.requester_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::RequesterChanged)
      object.requester_changed.previous.must_be_nil
      object.requester_changed.current.id.value.must_equal users(:minimum_author).id
    end
  end

  describe 'when changing the requester' do
    before do
      ticket.requester = users(:minimum_end_user)
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a RequesterChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.requester_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::RequesterChanged)
      object.requester_changed.previous.id.value.must_equal users(:minimum_author).id
      object.requester_changed.current.id.value.must_equal users(:minimum_end_user).id
    end
  end

  # This is a mandatory field and shouldn't happen, but lets gracefully handle it.
  describe 'when removing the requester' do
    before do
      ticket.requester = nil
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a RequesterChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.requester_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::RequesterChanged)
      object.requester_changed.previous.id.value.must_equal users(:minimum_author).id
      object.requester_changed.current.must_be_nil
    end
  end
end
