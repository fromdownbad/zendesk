require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketPermanentlyDeletedProtobufEncoder do
  let(:subject) { TicketPermanentlyDeletedProtobufEncoder.new(ticket) }
  let(:ticket) { mock }

  describe 'deleted ticket' do
    it 'builds a TicketPermanentlyDeleted event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.ticket_permanently_deleted.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketPermanentlyDeleted)
    end
  end
end
