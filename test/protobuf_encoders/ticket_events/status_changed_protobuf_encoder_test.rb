require_relative "../../support/test_helper"

SingleCov.covered!

describe StatusChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { StatusChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing the status' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with the default NEW status' do
    let(:ticket) { accounts(:minimum).tickets.build }

    it 'does not match' do
      # This behavior is open for discussion
      (!!subject.matches?).must_equal false
    end
  end

  describe 'when changing the status' do
    before do
      ticket.status_id = StatusType.closed
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a StatusChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.status_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::StatusChanged)
      object.status_changed.previous.must_equal :NEW
      object.status_changed.current.must_equal :CLOSED
    end
  end
end
