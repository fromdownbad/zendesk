require_relative "../../support/test_helper"

SingleCov.covered!

describe FollowersChangedProtobufEncoder do
  fixtures :users

  let(:agent_wombat) { users(:minimum_agent) }
  let(:admin_lemur) { users(:minimum_admin) }

  describe 'adding a new follower' do
    let(:subject) { FollowersChangedProtobufEncoder.new([agent_wombat.id], []) }

    it 'builds a FollowersChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.followers_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::FollowersChanged)
      object.followers_changed.users_removed.user_ids.first.must_be_nil
      object.followers_changed.users_added.user_ids.first.must_equal agent_wombat.id
    end
  end

  describe 'adding and removing followers' do
    let(:subject) { FollowersChangedProtobufEncoder.new([admin_lemur.id], [agent_wombat.id]) }

    it 'builds a FollowersChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.followers_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::FollowersChanged)
      object.followers_changed.users_removed.user_ids.first.must_equal agent_wombat.id
      object.followers_changed.users_added.user_ids.first.must_equal admin_lemur.id
    end
  end

  describe 'with no users added/removed' do
    let(:subject) { FollowersChangedProtobufEncoder.new([], []) }

    it 'raises ArgumentError' do
      assert_raises ArgumentError do
        subject.to_object
      end
    end
  end
end
