require_relative "../../support/test_helper"

SingleCov.covered!

describe PriorityChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { PriorityChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing the priority' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with the default unset priority' do
    let(:ticket) { accounts(:minimum).tickets.build }

    it 'does not match' do
      # This behavior is open for discussion
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with an explicit priority' do
    let(:ticket) { accounts(:minimum).tickets.build(priority_id: PriorityType.URGENT) }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a PriorityChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.priority_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::PriorityChanged)
      object.priority_changed.previous.must_equal :UNKNOWN_TICKET_PRIORITY
      object.priority_changed.current.must_equal :URGENT
    end
  end

  describe 'when changing the priority' do
    before do
      ticket.priority_id = PriorityType.high
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a PriorityChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.priority_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::PriorityChanged)
      object.priority_changed.previous.must_equal :NORMAL
      object.priority_changed.current.must_equal :HIGH
    end
  end
end
