require_relative "../../support/test_helper"

SingleCov.covered!

describe SubmitterChangedProtobufEncoder do
  fixtures :tickets
  let(:subject) { SubmitterChangedProtobufEncoder.new(ticket) }
  let(:ticket) { tickets(:minimum_1) }

  describe 'existing ticket not changing the submitter' do
    it 'does not match' do
      # `!!` => https://github.com/rails/rails/issues/24220
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new ticket with a submitter' do
    let(:ticket) { accounts(:minimum).tickets.build(submitter: users(:minimum_agent)) }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a SubmitterChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.submitter_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::SubmitterChanged)
      object.submitter_changed.previous.must_be_nil
      object.submitter_changed.current.id.value.must_equal users(:minimum_agent).id
    end
  end

  describe 'when assigning to a different agent' do
    before do
      ticket.submitter = users(:minimum_end_user)
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a SubmitterChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.submitter_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::SubmitterChanged)
      object.submitter_changed.previous.id.value.must_equal users(:minimum_agent).id
      object.submitter_changed.current.id.value.must_equal users(:minimum_end_user).id
    end
  end

  describe 'when removing the submitter' do
    before do
      ticket.submitter = nil
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a SubmitterChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
      object.submitter_changed.must_be_kind_of(Zendesk::Protobuf::Support::Tickets::V2::SubmitterChanged)
      object.submitter_changed.previous.id.value.must_equal users(:minimum_agent).id
      object.submitter_changed.current.must_be_nil
    end
  end
end
