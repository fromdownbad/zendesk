require_relative "../support/test_helper"

SingleCov.covered!

describe GroupEventsProtobufEncoder do
  fixtures :groups

  let(:group) { groups(:minimum_group) }

  let(:protobuf_encoder) { GroupEventsProtobufEncoder.new(group) }
  let(:proto_group_events) do
    protobuf_encoder.to_a.map(&:to_proto).
      map { |proto| ::Zendesk::Protobuf::Support::Users::V2::GroupEvent.decode(proto) }
  end

  describe 'with events to encode' do
    describe 'when encoding context' do
      before do
        group.id = 1234
      end

      it 'provides a group context' do
        proto_group_events.each do |event|
          assert_kind_of Zendesk::Protobuf::Support::Users::V2::Group, event.group
          assert group.id, event.group.id&.value
          assert group.account_id, event.group.account_id&.value
          assert group.name, event.group.name&.value
        end
      end

      it 'provides a header' do
        proto_group_events.each do |event|
          assert_equal 'support/users/v2/group_events', event.header.schema_path
          assert_kind_of Zendesk::Protobuf::Common::ProtobufHeader, event.header
          assert_equal 36, event.header.message_id&.value&.length
          assert_equal group.id, event.group.id&.value
          assert_equal group.account_id, event.header.account_id&.value
          assert_equal group.updated_at.to_i, event.header.occurred_at&.seconds
          assert_kind_of Zendesk::Protobuf::Common::MessageSequence, event.header.sequence
          assert_kind_of Zendesk::Protobuf::AccountAudits::Actor, event.actor
          assert_equal -1, event.actor.id.value
          assert_equal group.account_id, event.actor.account_id.value
          assert_equal '', event.actor.ip_address.value
        end
      end

      it 'increments header.sequence.position as 1-indexed' do
        assert_equal [1], proto_group_events.map { |e| e.header.sequence.position }
      end

      it 'captures the current actor' do
        user = users(:minimum_admin)
        CIA.audit(actor: user) do
          proto_group_events.each do |event|
            assert_kind_of Zendesk::Protobuf::AccountAudits::Actor, event.actor
            assert_equal user.id, event.actor.id.value
            assert_equal '', event.actor.ip_address.value
          end
        end
      end

      it 'captures the current actor ip-address' do
        user = users(:minimum_admin)
        CIA.audit(actor: user, ip_address: '127.0.0.1') do
          proto_group_events.each do |event|
            assert_kind_of Zendesk::Protobuf::AccountAudits::Actor, event.actor
            assert_equal user.id, event.actor.id.value
            assert_equal '127.0.0.1', event.actor.ip_address.value
          end
        end
      end
    end

    describe 'when encoding events' do
      describe 'when creating a new group' do
        before do
          group.id = 3583495
        end

        it 'contains GroupCreated event' do
          assert_event! :group_created
        end
      end
    end
  end

  describe 'with no events to encode' do
    it 'any? returns false' do
      assert_equal false, GroupEventsProtobufEncoder.new(group).any?
    end
  end

  describe '#clear' do
    before do
      group.id = 3583495
    end

    it 'clears inferred and explicit domain events' do
      refute_empty protobuf_encoder.inferred_events
      protobuf_encoder.clear
      assert_empty protobuf_encoder.inferred_events
    end
  end

  def assert_event!(event_type)
    ids = proto_group_events.map { |e| e.group.id&.value }
    ids.must_include(group.id)
    events = proto_group_events.map(&:event)
    events.must_include(event_type)
  end
end
