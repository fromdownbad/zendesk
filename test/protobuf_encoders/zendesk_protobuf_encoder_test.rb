require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe ZendeskProtobufEncoder do
  let(:zpe) { ZendeskProtobufEncoder.new(nil, field_map) }

  describe "error handling" do
    let(:field_map) { nil }

    it "raises if to_object is called" do
      assert_raises('Must be implemented by subclass') { zpe.to_proto }
    end

    it "raises if full is called" do
      assert_raises('Must be implemented by subclass') { zpe.full }
    end

    it "raises if encode_field is called" do
      assert_raises('Must be implemented by subclass') { zpe.send(:encode_field, nil, nil) }
    end
  end

  describe "#apply_field_map" do
    describe "with an array field map" do
      let(:field_map) { [:id, :name, :foobar] }

      it "encodes each field" do
        zpe.expects(:encode_field).with(instance_of(Symbol), instance_of(Array)).times(field_map.size)

        mapped_fields = zpe.send(:apply_field_map)

        assert_equal field_map, mapped_fields.keys
      end
    end

    describe "with hashes in the field map" do
      let(:field_map) { [:id, :name, foobar: [:bazz]] }

      it "encodes each field" do
        zpe.expects(:encode_field).with(instance_of(Symbol), instance_of(Array)).times(field_map.size)

        mapped_fields = zpe.send(:apply_field_map)

        expected_fields = field_map.map { |field| field.is_a?(Symbol) ? field : field.keys.first }

        assert_equal expected_fields, mapped_fields.keys
      end
    end
  end
end
