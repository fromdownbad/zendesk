require_relative "../../support/test_helper"

SingleCov.covered!

describe GroupCreatedProtobufEncoder do
  fixtures :groups

  let(:subject) { GroupCreatedProtobufEncoder.new(group) }
  let(:group) { groups(:minimum_group) }

  describe 'an existing group' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a newly created group' do
    before do
      group.id = 1234
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds an GroupCreated event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::GroupEvent)
      object.group_created.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::GroupCreated)
    end
  end
end
