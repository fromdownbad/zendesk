require_relative "../../support/test_helper"

SingleCov.covered!

describe GroupDeletedProtobufEncoder do
  fixtures :groups

  let(:subject) { GroupDeletedProtobufEncoder.new(group) }
  let(:group) { groups(:minimum_group) }

  describe 'an existing group' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a newly created group' do
    before do
      group.is_active = false
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds an GroupCreated event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::GroupEvent)
      object.group_deleted.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::GroupDeleted)
    end
  end
end
