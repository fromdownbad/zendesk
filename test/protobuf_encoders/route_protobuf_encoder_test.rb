require_relative "../support/test_helper"

SingleCov.covered!

describe RouteProtobufEncoder do
  fixtures :routes

  let(:account_route) { routes(:minimum) }
  let(:proto_route) do
    ::Zendesk::Protobuf::Support::Accounts::Route.decode(
      RouteProtobufEncoder.new(account_route).to_proto
    )
  end

  describe "for primitive fields" do
    # For cases where the model and proto field name are different, use a subarray
    [:id, :subdomain, :ssl_required].each do |model_field, proto_field|
      it "correctly encodes #{proto_field}" do
        proto_field ||= model_field

        assert_equal account_route.send(model_field), proto_route.send(proto_field).value
      end
    end

    describe "for nullable primitive fields" do
      [:host_mapping, :gam_domain].each do |field|
        it "encodes #{field} if present" do
          set_field_value(account_route, field)
          assert_equal account_route.send(field), proto_route.send(field).value
        end

        it "can encode without #{field}" do
          account_route.update_column(field, nil)
          assert_nil proto_route.send(field)
        end
      end
    end
  end

  describe "for nested fields" do
    # Each hash key is the protobuf object field, and the value it's expected type
    {account: Zendesk::Protobuf::Support::Accounts::Account}.each do |field, encoder|
      it "encodes #{field}" do
        assert_instance_of encoder, proto_route.send(field)
      end
    end
  end

  describe "for timestamps" do
    [:created_at, :updated_at].each do |field|
      it "correctly encodes #{field}" do
        assert_equal account_route.send(field).to_i, proto_route.timestamps.send(field).seconds
        assert_equal account_route.send(field).nsec, proto_route.timestamps.send(field).nanos
      end
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        RouteProtobufEncoder.new(account_route, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      RouteProtobufEncoder.any_instance.expects(:full).once

      RouteProtobufEncoder.new(account_route)
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = RouteProtobufEncoder.new(account_route, [:id, :subdomain]).to_object

      proto_object.to_h.each do |field, value|
        if [:id, :subdomain].include?(field)
          refute_nil value
        else
          assert_nil value
        end
      end
    end

    it "passes sub the map to the next encoder" do
      AccountProtobufEncoder.expects(:new).with(account_route.account, [:id]).returns(mock(to_object: nil))

      RouteProtobufEncoder.new(account_route, [account: [:id]]).to_object
    end
  end
end
