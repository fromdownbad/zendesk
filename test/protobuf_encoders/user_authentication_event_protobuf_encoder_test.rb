require_relative "../support/test_helper"

SingleCov.covered!

describe UserAuthenticationEventProtobufEncoder do
  fixtures :users

  let(:user) { users(:minimum_agent) }

  it "raises NotImplemented error" do
    assert_raises RuntimeError do
      UserAuthenticationEventProtobufEncoder.new(user).to_proto
    end
  end
end
