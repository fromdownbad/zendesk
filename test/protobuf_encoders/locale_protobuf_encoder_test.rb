require_relative "../support/test_helper"

SingleCov.covered!

describe LocaleProtobufEncoder do
  fixtures :translation_locales

  let(:locale) { translation_locales(:english_by_zendesk) }
  let(:proto_locale) do
    ::Zendesk::Protobuf::Support::Locales::Locale.decode(
      LocaleProtobufEncoder.new(locale).to_proto
    )
  end

  describe "for primitive fields" do
    # For cases where the model and proto field name are different, use a subarray
    [:id].each do |model_field, proto_field|
      it "correctly encodes #{proto_field}" do
        proto_field ||= model_field

        assert_equal locale.send(model_field), proto_locale.send(proto_field).value
      end
    end
  end
end
