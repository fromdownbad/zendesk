require_relative "../support/test_helper"
require_relative "../support/test_helper"

SingleCov.covered!

describe TicketEventsProtobufEncoder do
  fixtures :tickets, :users, :events, :ticket_fields

  let(:ticket) { tickets(:minimum_1) }

  let(:proto_ticket_events) do
    TicketEventsProtobufEncoder.new(ticket).to_a.map(&:to_proto).
      map { |proto| ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(proto) }
  end

  describe 'with events to encode' do
    describe 'when encoding context' do
      # TODO: Reincorporate the ticket once the schema is updated to Ticket V2
      # before do
      #   TicketEventsProtobufEncoder.any_instance.stubs(:event_list).returns(
      #     [
      #       ::Zendesk::Protobuf::Support::Tickets::TicketEvent.new
      #     ]
      #   )
      # end

      # it 'correctly encodes metadata' do
      #   assert_equal ticket.id, proto_ticket_events.ticket_id.value
      #   assert_equal ticket.account_id, proto_ticket_events.account_id.value
      #   assert_equal ::Zendesk::Protobuf::Support::Tickets::Ticket, proto_ticket_events.ticket.class

      #   assert_equal ::Zendesk::Protobuf::Support::Tickets::TicketEventList, proto_ticket_events.ticket_event_list.class
      # end

      let(:actor) { users(:minimum_end_user) }

      before do
        ticket.id = 1234
        ticket.will_be_saved_by(actor)
      end

      it 'provides a thin ticket context' do
        TicketProtobufEncoder.any_instance.expects(:thin).returns(TicketProtobufEncoder.new(ticket)).times(proto_ticket_events.count)
        proto_ticket_events.each do |event|
          assert_kind_of Zendesk::Protobuf::Support::Tickets::V2::Ticket, event.ticket
        end
      end

      it 'provides via' do
        proto_ticket_events.each do |event|
          assert_kind_of Zendesk::Protobuf::Support::Tickets::V2::ViaType, event.via
          assert_equal :web_form, event.via.type
        end
      end

      it 'provides actor' do
        proto_ticket_events.each do |event|
          assert_kind_of Zendesk::Protobuf::Support::Users::User, event.actor
          assert_equal actor.id, event.actor.id&.value
        end
      end

      it 'provides a header' do
        proto_ticket_events.each do |event|
          assert_equal 'support/tickets/v2/ticket_events', event.header.schema_path
          assert_kind_of Zendesk::Protobuf::Common::ProtobufHeader, event.header
          assert_equal 36, event.header.message_id&.value&.length
          assert_equal 90538, event.header.account_id&.value
          assert_equal ticket.updated_at.to_i, event.header.occurred_at&.seconds
          assert_kind_of Zendesk::Protobuf::Common::MessageSequence, event.header.sequence
        end
      end

      it 'increments header.sequence.position as 1-indexed' do
        assert_equal [1], proto_ticket_events.map { |e| e.header.sequence.position }
      end
    end

    describe 'when encoding events' do
      describe 'when creating a new ticket' do
        before do
          # This is an integration lie, but easier than building a new ticket from scratch.
          ticket.id = 1234
          ticket.comment = events(:create_comment_for_minimum_ticket_1)
        end

        it 'contains TicketCreated event' do
          assert_event! :ticket_created
        end
        it 'contains CommentCreated event' do
          assert_event! :comment_added
        end
      end

      describe 'when soft deleted a ticket' do
        before do
          ticket.mark_as_deleted
        end

        it 'contains TicketSoftDeleted event' do
          assert_event! :ticket_soft_deleted
        end
      end

      describe 'when undeletining deleted a ticket' do
        before do
          ticket.will_be_saved_by(User.system)
          ticket.soft_delete!
          # No `mark_as_undeleted` method as of now.
          ticket.status_id = ticket.previous_status
        end

        it 'contains TicketUndeleted event' do
          assert_event! :ticket_undeleted
        end
      end

      describe 'when updating an existing ticket' do
        before do
          ticket.status_id      = StatusType.closed
          ticket.ticket_type_id = TicketType.problem
          ticket.priority_id    = PriorityType.high
          ticket.set_tags       = 'foo bar'
          ticket.subject        = 'New subject'
          ticket.submitter      = users(:minimum_end_user)
          ticket.requester      = users(:minimum_end_user)
          ticket.assignee       = users(:support_agent)
          ticket.group          = groups(:support_group)
          ticket.brand_id       = nil
          ticket.external_id    = 'oHg5SJYRHA0'
          ticket.due_date       = Date.today
          ticket.problem        = tickets(:minimum_2)
          ticket.ticket_form    = ticket_forms(:default_ticket_form)
        end

        it 'contains StatusChanged event' do
          assert_event! :status_changed
        end

        it 'contains PriorityChanged event' do
          assert_event! :priority_changed
        end

        it 'contains TagsChanged event' do
          assert_event! :tags_changed
        end

        it 'contains SubjectChanged event' do
          assert_event! :subject_changed
        end

        it 'contains SubmitterChanged event' do
          assert_event! :submitter_changed
        end

        it 'contains RequesterChanged event' do
          assert_event! :requester_changed
        end

        it 'contains AgentAssignmentChanged event' do
          assert_event! :agent_assignment_changed
        end

        it 'contains GroupAssignmentChanged event' do
          assert_event! :group_assignment_changed
        end

        it 'contains BrandChanged event' do
          assert_event! :brand_changed
        end

        it 'contains ExternalIdChanged event' do
          assert_event! :external_id_changed
        end

        it 'contains TicketTypeChanged event' do
          assert_event! :ticket_type_changed
        end

        it 'contains TaskDueAtChanged event' do
          assert_event! :task_due_at_changed
        end

        it 'contains ProblemLinkChanged event' do
          assert_event! :problem_link_changed
        end

        it 'contains TicketFormChanged event' do
          assert_event! :ticket_form_changed
        end
      end
    end

    describe 'ticket field entries' do
      before do
        Arturo.enable_feature!(:publish_ticket_events_to_bus)
        Arturo.enable_feature!(:publish_tfe_domain_events)
        ticket.domain_event_publisher = domain_event_publisher
        ticket.fields = [[field.id, field_value]]
        ticket.will_be_saved_by(User.system)
        ticket.save! # Full integration!
      end

      let(:domain_event_publisher) { FakeEscKafkaMessage.new }
      let(:field) { ticket_fields(:field_text_custom) }
      let(:field_value) { 'bees' }
      let(:proto_ticket_events) do
        domain_event_publisher.events.map do |event|
          ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value])
        end
      end

      it 'contains CustomFieldChanged event' do
        assert_event! :custom_field_changed
        event = proto_ticket_events.find { |e| e.event == :custom_field_changed }
        event.custom_field_changed.ticket_field.id.value.must_equal field.id
        event.custom_field_changed.current.text_value.must_equal 'bees'
        event.custom_field_changed.previous.must_be_nil
      end

      describe 'with nil values on a new record' do
        let(:field_value) { '' }
        it 'does not publish events' do
          proto_ticket_events.map(&:event).wont_include :custom_field_changed
        end
      end
    end

    describe 'with explicit events' do
      before do
        ticket.domain_events << Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.new(
          ticket_permanently_deleted: Zendesk::Protobuf::Support::Tickets::V2::TicketPermanentlyDeleted.new
        )
      end

      it 'includes explicit events in the output' do
        assert_event! :ticket_permanently_deleted
      end
    end
  end

  describe 'with no events to encode' do
    it 'any? returns false' do
      assert_equal false, TicketEventsProtobufEncoder.new(ticket).any?
    end
  end

  def assert_event!(event_type)
    events = proto_ticket_events.map(&:event)
    events.must_include(event_type)
  end
end
