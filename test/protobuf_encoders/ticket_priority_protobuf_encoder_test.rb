require_relative "../support/test_helper"

SingleCov.covered!

describe TicketPriorityProtobufEncoder do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:proto_ticket_priority) do
    TicketPriorityProtobufEncoder.new(ticket).to_object
  end

  describe "for each valid ticket priority" do
    TicketPriorityProtobufEncoder::PRIORITY_MAP.keys.each do |priority|
      it "correctly encodes #{priority}" do
        ticket.update_column(:priority_id, priority)

        # Handle the "-" null type
        name = if priority == 0
          :UNKNOWN_TICKET_PRIORITY
        else
          PriorityType[priority].name.upcase.to_sym
        end

        assert_equal name, ::Zendesk::Protobuf::Support::Tickets::V2::TicketPriority.lookup(proto_ticket_priority)
      end
    end
  end

  it "returns unknown for unknown priorities" do
    ticket.update_column(:priority_id, -1)

    assert_equal ::Zendesk::Protobuf::Support::Tickets::V2::TicketPriority::UNKNOWN_TICKET_PRIORITY, proto_ticket_priority
  end
end
