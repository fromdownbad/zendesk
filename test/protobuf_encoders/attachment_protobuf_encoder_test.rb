require_relative "../support/test_helper"

SingleCov.covered!

describe AttachmentProtobufEncoder do
  fixtures :attachments

  let(:attachment) { attachments(:serialization_comment_attachment) }
  let(:proto_attachment) do
    ::Zendesk::Protobuf::Support::Attachments::Attachment.decode(
      AttachmentProtobufEncoder.new(attachment).full.to_proto
    )
  end

  describe 'primitive values' do
    {
      id: :id,
      content_type: :content_type,
      display_filename: :filename,
      size: :size,
      inline: :is_inline,
      is_public: :is_public
    }.each do |column, proto_key|
      it "correctly encodes #{column}" do
        assert_equal attachment.send(column), proto_attachment.send(proto_key).value
      end
    end
  end

  it 'correctly encodes content_url' do
    expected = 'https://serialization.zendesk-test.com/attachments/token/123456adsa7890/?name=x.png'
    assert_equal proto_attachment.content_url.value, expected
  end

  describe 'id_only' do
    let(:proto_attachment) do
      ::Zendesk::Protobuf::Support::Attachments::Attachment.decode(
        AttachmentProtobufEncoder.new(attachment).id_only.to_proto
      )
    end

    it "encodes id only" do
      assert_equal attachment.id, proto_attachment.id.value
      assert_nil proto_attachment.content_type
      assert_nil proto_attachment.size
    end
  end
end
