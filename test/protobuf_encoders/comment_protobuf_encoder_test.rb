require_relative "../support/test_helper"

SingleCov.covered!

describe CommentProtobufEncoder do
  fixtures :events

  let(:subject) { CommentProtobufEncoder.new(comment) }
  let(:comment) { events(:create_comment_for_minimum_ticket_1) }
  let(:proto_comment) do
    ::Zendesk::Protobuf::Support::Tickets::V2::Comment.decode(
      subject.to_proto
    )
  end

  describe "for primitive fields" do
    # For cases where the model and proto field name are different, use a subarray
    [:id, :body, :is_public].each do |model_field, proto_field|
      it "correctly encodes #{proto_field}" do
        proto_field ||= model_field

        assert_equal comment.send(model_field), proto_comment.send(proto_field).value
      end
    end
  end

  describe "for nested fields" do
    # Each hash key is the protobuf object field, and the value it's expected type
    { author: Zendesk::Protobuf::Support::Users::User }.each do |field, encoder|
      it "encodes #{field}" do
        assert_instance_of encoder, proto_comment.send(field)
      end
    end
  end

  describe "for timestamps" do
    [:created_at, :updated_at].each do |field|
      it "correctly encodes #{field}" do
        assert_equal comment.send(field).to_i, proto_comment.timestamps.send(field).seconds
        assert_equal comment.send(field).nsec, proto_comment.timestamps.send(field).nanos
      end
    end
  end

  describe 'with the id_only project' do
    before { subject.id_only }

    it 'only encodes the id field' do
      proto_comment_data = proto_comment.to_h
      assert_equal comment.id, proto_comment_data[:id][:value]
      assert_nil proto_comment_data[:name]
    end
  end
end
