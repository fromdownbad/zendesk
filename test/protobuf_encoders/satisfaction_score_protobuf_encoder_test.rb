require_relative "../support/test_helper"
require_relative "../support/satisfaction_test_helper"

SingleCov.covered!

describe SatisfactionScoreProtobufEncoder do
  include SatisfactionTestHelper
  fixtures :tickets, :users

  let(:ticket) { tickets(:minimum_2) }
  let(:proto_ticket_satisfaction) do
    SatisfactionScoreProtobufEncoder.new(ticket).to_object
  end

  describe "for each satisfaction score" do
    SatisfactionScoreProtobufEncoder::SATISFACTION_MAP.keys.each do |satisfaction|
      it "correctly encodes #{satisfaction}" do
        ticket.update_column(:satisfaction_score, satisfaction)

        assert_match SatisfactionType[satisfaction].name.upcase.to_sym, /#{proto_ticket_satisfaction.score}/
      end
    end
  end

  it "returns unknown for unknown scores" do
    ticket.update_column(:satisfaction_score, -1)

    assert_equal ::Zendesk::Protobuf::Support::Tickets::V2::Score::UNKNOWN_SCORE, ::Zendesk::Protobuf::Support::Tickets::V2::Score.resolve(proto_ticket_satisfaction.score)
  end

  describe "satisfaction comments" do
    it "encodes comment if present" do
      build_satisfaction_rating(ticket)

      assert_equal ticket.satisfaction_comment, proto_ticket_satisfaction.comment.value
    end

    it "can encode without comment" do
      assert_nil proto_ticket_satisfaction.comment
    end
  end
end
