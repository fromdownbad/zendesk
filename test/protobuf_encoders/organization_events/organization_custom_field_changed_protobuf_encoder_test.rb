require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe OrganizationCustomFieldChangedProtobufEncoder do
  fixtures :organizations, :cf_fields, :cf_dropdown_choices
  let(:subject) { OrganizationCustomFieldChangedProtobufEncoder.new(field, previous_field_value, field_value) }
  let(:object) { subject.to_object }
  let(:organization) { organizations(:minimum_organization1) }
  let(:field) { cf_fields(:org_integer1) }
  let(:previous_field_value) { nil }
  let(:field_value) { "123" }

  it 'provides OrganizationField context' do
    object.custom_field_changed.field.id.value.must_equal field.id
    object.custom_field_changed.field.title.value.must_equal field.title
  end

  describe "clearing an existing field" do
    let(:previous_field_value) { "12345" }
    let(:field_value) { " " }

    it "builds a null current" do
      object.custom_field_changed.previous.integer_string.must_equal "12345"
      object.custom_field_changed.current.must_be_nil
    end
  end

  describe "other fields" do
    describe "checkbox" do
      let(:field) { cf_fields(:checkbox1) }
      let(:field_value) { "1" }

      it "builds a CustomFieldChanged event" do
        object.custom_field_changed.current.checkbox_value.must_equal true
      end
    end

    describe "date" do
      let(:field) { cf_fields(:date1) }
      let(:field_value) { "2019-11-04" }

      it "builds a CustomFieldChanged event" do
        object.custom_field_changed.current.date_value.year.must_equal 2019
        object.custom_field_changed.current.date_value.month.must_equal 11
        object.custom_field_changed.current.date_value.day.must_equal 04
      end

      describe "clearing an existing date" do
        let(:previous_field_value) { "2019-11-04" }
        let(:field_value) { " " }

        it "builds a null current" do
          object.custom_field_changed.previous.date_value.year.must_equal 2019
          object.custom_field_changed.previous.date_value.month.must_equal 11
          object.custom_field_changed.previous.date_value.day.must_equal 04
          object.custom_field_changed.current.must_be_nil
        end
      end

      describe "invalid date" do
        let(:field_value) { "2020-22-26" }

        it "builds a null date_value" do
          object.custom_field_changed.current.date_value.must_be_nil
        end
      end
    end

    describe "decimal" do
      let(:field) { cf_fields(:decimal1) }
      let(:field_value) { "1337.0" }

      it "builds a CustomFieldChanged event" do
        object.custom_field_changed.current.decimal_string.must_equal "1337.0"
      end
    end

    describe "decimal field with leading zeros" do
      let(:field) { cf_fields(:decimal1) }
      let(:field_value) { "0000001337.0" }

      it "removes the leading zeros from CustomFieldChanged event" do
        object.custom_field_changed.current.decimal_string.must_equal "1337.0"
      end
    end

    describe "decimal field with nil values" do
      let(:previous_field_value) { "0000012345" }
      let(:field_value) { " " }

      it "removes the leading zeros from CustomFieldChanged event" do
        object.custom_field_changed.previous.integer_string.must_equal "12345"
        object.custom_field_changed.current.must_be_nil
      end
    end

    describe "negative integer field with leading zeros" do
      let(:field) { cf_fields(:integer1) }
      let(:field_value) { "-00000012345" }

      it "removes the leading zeros from CustomFieldChanged event" do
        object.custom_field_changed.current.integer_string.must_equal "-12345"
      end
    end

    describe "regexp" do
      let(:field) { cf_fields(:regexp1) }
      let(:field_value) { "123-45-6789" }

      it "builds a CustomFieldChanged event" do
        object.custom_field_changed.current.regexp_value.must_equal "123-45-6789"
      end
    end

    describe "text" do
      let(:field) { cf_fields(:text1) }
      let(:field_value) { "bees" }

      it "builds a CustomFieldChanged event" do
        object.custom_field_changed.current.text_value.must_equal "bees"
      end
    end

    describe "tagger" do
      let(:field) { cf_fields(:dropdown1) }
      let(:field_value) { cf_dropdown_choices(:v200) }

      it "builds a CustomFieldChanged event" do
        object.custom_field_changed.current.tagger_value.must_equal "200"
      end
    end
  end
end
