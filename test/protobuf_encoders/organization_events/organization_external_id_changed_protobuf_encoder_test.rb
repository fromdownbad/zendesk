require_relative "../../support/test_helper"

SingleCov.covered!

describe OrganizationExternalIdChangedProtobufEncoder do
  fixtures :organizations, :accounts

  let(:subject) { OrganizationExternalIdChangedProtobufEncoder.new(organization) }
  let(:organization) { organizations(:minimum_organization1) }

  describe 'existing organization not changed' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a new organization with an external_id' do
    let(:organization) { accounts(:minimum).organizations.build(external_id: 'external_id') }

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds an OrganizationExternalIdChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationEvent)
      object.external_id_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationExternalIdChanged)
      object.external_id_changed.previous.must_be_nil
      object.external_id_changed.current.value.must_equal('external_id')
    end
  end

  describe 'an existing organization with an updated external_id' do
    before do
      organization.update_column(:external_id, 'old')
      organization.external_id = 'new'
    end

    it 'builds an OrganizationExternalIdChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationEvent)
      object.external_id_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationExternalIdChanged)
      object.external_id_changed.previous.value.must_equal('old')
      object.external_id_changed.current.value.must_equal('new')
    end
  end
end
