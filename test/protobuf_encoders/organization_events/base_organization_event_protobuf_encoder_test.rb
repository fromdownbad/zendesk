require_relative "../../support/test_helper"

SingleCov.covered!

describe BaseOrganizationEventProtobufEncoder do
  let(:subject) { BaseOrganizationEventProtobufEncoder.new(mock('Organization')) }

  describe 'matches?' do
    it 'raises NotImplemented' do
      -> { subject.matches? }.must_raise(BaseOrganizationEventProtobufEncoder::NotImplemented)
    end
  end

  describe 'to_object' do
    it 'raises NotImplemented' do
      -> { subject.to_object }.must_raise(BaseOrganizationEventProtobufEncoder::NotImplemented)
    end
  end

  describe 'to_safe_object' do
    it 'rescues errors, reports them, and returns nil' do
      subject.encoder_statsd_client.expects(:increment).with('errors', tags: ["exception:BaseOrganizationEventProtobufEncoder::NotImplemented"])
      subject.to_safe_object.must_be_nil
    end
  end
end
