require_relative "../../support/test_helper"

SingleCov.covered!

describe OrganizationDeletedProtobufEncoder do
  fixtures :organizations

  let(:subject) { OrganizationDeletedProtobufEncoder.new(organization) }
  let(:organization) { organizations(:minimum_organization1) }

  describe 'existing organization not changed' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'organization deleted' do
    before do
      organization.deleted_at = Time.now
    end

    it 'matches' do
      (!!subject.matches?).must_equal true
    end

    it 'builds an OrganizationDeleted event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationEvent)
      object.organization_deleted.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationDeleted)
    end
  end
end
