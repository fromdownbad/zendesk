require_relative "../../support/test_helper"

SingleCov.covered!

describe OrganizationCreatedProtobufEncoder do
  fixtures :organizations

  let(:subject) { OrganizationCreatedProtobufEncoder.new(organization) }
  let(:organization) { organizations(:minimum_organization1) }

  describe 'an existing organization' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'a newly created user' do
    before do
      organization.id = 1234
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds an OrganizationCreated event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationEvent)
      object.organization_created.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationCreated)
    end
  end
end
