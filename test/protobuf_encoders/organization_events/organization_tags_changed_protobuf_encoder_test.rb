require_relative "../../support/test_helper"

SingleCov.covered!

describe OrganizationTagsChangedProtobufEncoder do
  fixtures :organizations, :taggings

  let(:subject) { OrganizationTagsChangedProtobufEncoder.new(organization) }
  let(:organization) { organizations(:minimum_organization2) }

  before do
    Account.any_instance.stubs(:has_user_and_organization_tags?).returns true
  end

  describe 'organization has changed tags' do
    before do
      organization.set_tags = 'one two'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a TagsChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationEvent)
      object.tags_changed.
        must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationTagsChanged)
      object.tags_changed.tags_added.tags.must_equal %w[one two]
      object.tags_changed.tags_removed.tags.must_equal []
    end
  end
end
