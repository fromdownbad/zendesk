require_relative "../../support/test_helper"

SingleCov.covered!

describe OrganizationNameChangedProtobufEncoder do
  fixtures :organizations, :accounts

  let(:subject) { OrganizationNameChangedProtobufEncoder.new(organization) }
  let(:organization) { organizations(:minimum_organization1) }

  describe 'existing organization not changed' do
    it 'does not match' do
      (!!subject.matches?).must_equal false
    end
  end

  describe 'organization deleted' do
    it 'does not match even though name changes' do
      organization.name = 'zen_deleted_zen'
      organization.deleted_at = Time.now
      subject.matches?.must_equal false
    end
  end

  describe 'organization has changed their name' do
    before do
      organization.name = 'zen'
    end

    it 'matches' do
      subject.matches?.must_equal true
    end

    it 'builds a OrganizationNameChanged event' do
      object = subject.to_object
      object.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationEvent)
      object.organization_name_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::OrganizationNameChanged)
      object.organization_name_changed.previous.value.must_equal(organization.name_was)
      object.organization_name_changed.current.value.must_equal(organization.name)
    end

    it 'when user is created, not matches' do
      organization.save
      subject.matches?.must_equal false
    end
  end
end
