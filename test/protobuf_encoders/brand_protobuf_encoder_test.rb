require_relative "../support/test_helper"

SingleCov.covered!

describe BrandProtobufEncoder do
  fixtures :brands

  let(:brand) { brands(:minimum) }
  let(:proto_brand) do
    ::Zendesk::Protobuf::Support::Accounts::Brand.decode(
      BrandProtobufEncoder.new(brand).to_proto
    )
  end

  describe "for primitive fields" do
    # For cases where the model and proto field name are different, use a subarray
    [:id, :name, [:active, :is_active]].each do |model_field, proto_field|
      it "correctly encodes #{proto_field}" do
        proto_field ||= model_field

        assert_equal brand.send(model_field), proto_brand.send(proto_field).value
      end
    end
  end

  describe "for nullable fields" do
    [:signature_template].each do |field|
      it "encodes #{field} if present" do
        set_field_value(brand, field)
        assert_equal brand.send(field), proto_brand.send(field).value
      end

      it "can encode without #{field}" do
        brand.update_column(field, nil)
        assert_nil proto_brand.send(field)
      end
    end
  end

  describe "for nested fields" do
    # Each hash key is the protobuf object field, and the value it's expected type
    {
      account: Zendesk::Protobuf::Support::Accounts::Account,
      route:   Zendesk::Protobuf::Support::Accounts::Route
    }.each do |field, encoder|
      it "encodes #{field}" do
        assert_instance_of encoder, proto_brand.send(field)
      end
    end
  end

  describe "for timestamps" do
    [:created_at, :updated_at].each do |field|
      it "correctly encodes #{field}" do
        assert_equal brand.send(field).to_i, proto_brand.timestamps.send(field).seconds
        assert_equal brand.send(field).nsec, proto_brand.timestamps.send(field).nanos
      end
    end
  end

  describe "for logo" do
    it "correctly encodes logo when a logo exists" do
      thumbnail = stub('thumb', thumbnail: 'thumb')
      thumbnails = stub('thumbnails', thumbnails: [thumbnail])

      brand.stubs(:logo).returns(thumbnails)

      url_builder = stub('url_builder', url_for: 'logo')
      ContentUrlBuilder.stubs(:new).returns(url_builder)

      assert_equal proto_brand.logo.thumbnail_url.value, "logo"
    end

    it "encodes empty string as logo when a logo is not defined" do
      assert_equal proto_brand.logo.thumbnail_url.value, ""
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        BrandProtobufEncoder.new(brand, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      BrandProtobufEncoder.any_instance.expects(:full).once

      BrandProtobufEncoder.new(brand)
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = BrandProtobufEncoder.new(brand, [:id, :is_active]).to_object

      proto_object.to_h.each do |field, value|
        if [:id, :is_active].include?(field)
          refute_nil value
        else
          assert_nil value
        end
      end
    end

    it "passes sub the map to the next encoder" do
      AccountProtobufEncoder.expects(:new).with(brand.account, [:id]).returns(mock(to_object: nil))

      BrandProtobufEncoder.new(brand, [account: [:id]]).to_object
    end
  end
end
