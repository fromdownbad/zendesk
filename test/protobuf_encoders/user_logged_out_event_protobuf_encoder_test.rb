require_relative "../support/test_helper"

SingleCov.covered!

describe UserLoggedOutEventProtobufEncoder do
  fixtures :users

  let(:user) { users(:minimum_agent) }
  let(:message_id) { SecureRandom.uuid }
  let(:user_proto) { UserLoggedOutEventProtobufEncoder.new(user).to_object }

  before do
    Timecop.freeze
    ZendeskProtobufEncoder.any_instance.stubs(:message_id).returns(message_id)
  end

  it "matches user fields" do
    assert_equal user_proto.user_id.value, user.id
    assert_equal user_proto.header.account_id.value.to_i, user.account_id
    assert_equal user_proto.header.occurred_at.seconds, Time.now.to_i
    assert_equal user_proto.header.message_id.value, message_id
    assert_instance_of(
      Zendesk::Protobuf::Platform::Standard::Users::UserLoggedOut,
      user_proto.user_logged_out
    )
    assert_nil user_proto.user_logged_in
  end
end
