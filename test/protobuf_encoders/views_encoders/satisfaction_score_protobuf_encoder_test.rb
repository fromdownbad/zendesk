require_relative "../../support/test_helper"

SingleCov.covered!

describe ViewsEncoders::SatisfactionScoreProtobufEncoder do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:subject) { ViewsEncoders::SatisfactionScoreProtobufEncoder.new(ticket) }
  let(:proto) { subject.to_object }

  before do
    ticket.satisfaction_score = SatisfactionType.GOOD
    ticket.satisfaction_reason_code = Satisfaction::Reason::OTHER
    TicketPrediction.update_satisfaction_probability(ticket, 0.5)
  end

  it 'uses the id if set' do
    assert_equal 16, proto.score_id.value
  end

  it 'correctly encodes the score if set' do
    assert_equal :GOOD, proto.score
  end

  describe 'when ticket prediction changes' do
    it 'uses the probability if set' do
      assert_equal 0.5, proto.probability.value
    end
  end

  it 'uses the reason code if set' do
    assert_equal 100, proto.reason_code.value
  end

  describe 'when sastisfaction data is not present' do
    before do
      ticket.satisfaction_score = nil
      ticket.satisfaction_reason_code = nil
      ticket.ticket_prediction = nil
    end

    it 'does not set values if not present in ticket' do
      assert_nil proto.score_id
      assert_nil proto.probability
      assert_equal :UNKNOWN_SCORE, proto.score
      assert_nil proto.reason_code
    end
  end
end
