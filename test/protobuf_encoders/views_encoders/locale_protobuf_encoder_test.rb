require_relative "../../support/test_helper"

SingleCov.covered!

describe ViewsEncoders::LocaleProtobufEncoder do
  fixtures :tickets, :users

  let(:user) { users(:minimum_author) }

  before do
    user.update_column(:locale_id, ENGLISH_BY_ZENDESK.id)
    @ticket = accounts(:minimum).tickets.new(requester: user, description: 'Hello')
    @ticket.will_be_saved_by(users(:minimum_agent))
    @ticket.save!
    @subject = ViewsEncoders::LocaleProtobufEncoder.new(@ticket)
  end

  it 'uses the requester locale id' do
    assert_equal user.translation_locale.id, @subject.to_object.id.value
  end

  it 'correctly encodes the field value from requester locale' do
    assert_equal user.translation_locale.locale, @subject.to_object.code.value
  end
end
