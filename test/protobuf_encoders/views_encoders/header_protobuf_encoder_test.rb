require_relative "../../support/test_helper"

SingleCov.covered!

describe ViewsEncoders::HeaderProtobufEncoder do
  fixtures :tickets

  let(:entity) { tickets(:minimum_1) }
  let(:klass) { Class.new(Struct.new(:message_id, :entity)) { include ViewsEncoders::HeaderProtobufEncoder } }
  let(:subject) { klass.new('1', entity) }
  let(:encoded_entity) { Struct.new(:header).new }

  describe '#headers' do
    it 'builds the header' do
      header = subject.headers('schema_path')
      assert_equal 'schema_path', header.schema_path
      assert_equal 90538, header.account_id.value
      assert_kind_of Google::Protobuf::StringValue, header.message_id
      assert_kind_of Google::Protobuf::Timestamp, header.occurred_at
      refute header.sequence
    end
  end
end
