require_relative "../../support/test_helper"
require_relative "../../support/sharing_test_helper"

SingleCov.covered!

describe ViewsEncoders::ViewsTicketProtobufEncoder do
  fixtures :tickets, :users, :organizations, :ticket_forms

  let(:ticket) { tickets(:minimum_1) }
  let(:ticket_encoder) { ViewsEncoders::ViewsTicketProtobufEncoder.new(ticket) }
  let(:proto_ticket) { ticket_encoder.to_object }
  let(:fields) do
    %i[account assigned_at assignee assignee_updated_at brand description subject due_at
       group header id initially_assigned_at is_public locale nice_id organization
       priority recipient requester requester_updated_at sharing_agreements satisfaction
       sla_breach_status solved_at status submitter tags ticket_form ticket_type
       timestamps updated_by_type via via_reference_id schedule]
  end

  it 'returns object with #to_proto' do
    assert_kind_of String, ticket_encoder.to_proto
  end

  it 'sets the header' do
    assert ticket_encoder.to_object.header
  end

  it 'sets the correct schema' do
    assert_equal "support/views/ticket", ticket_encoder.to_object.header.schema_path
  end

  it 'contains the correct fields' do
    encoded_fields = ticket_encoder.to_object.to_h.keys
    assert_empty (encoded_fields - fields) | (fields - encoded_fields)
  end

  describe ':account' do
    it 'correctly encodes if present' do
      assert_equal ticket.account_id.to_i, proto_ticket.account.id.value
    end
  end

  describe ':assigned_at' do
    it 'correctly encodes if present' do
      assert_equal ticket.assigned_at.to_i, proto_ticket.assigned_at.seconds
    end

    it 'can encode without' do
      ticket.assigned_at = nil

      assert_nil proto_ticket.assigned_at
    end
  end

  describe ':assignee' do
    it 'correctly encodes if present' do
      assert_equal ticket.assignee.id, proto_ticket.assignee.id.value
      assert_equal ticket.assignee.name, proto_ticket.assignee.name.value
    end

    it 'can encode without' do
      ticket.assignee = nil

      assert_nil proto_ticket.assignee
    end
  end

  describe ':assignee_updated_at' do
    it 'correctly encodes if present' do
      assert_equal ticket.assignee_updated_at.to_i, proto_ticket.assignee_updated_at.seconds
    end

    it 'can encode without' do
      ticket.assignee_updated_at = nil

      assert_nil proto_ticket.assignee_updated_at
    end
  end

  describe ':brand' do
    it 'correctly encodes if present' do
      assert_equal ticket.brand_id.to_i, proto_ticket.brand.id.value
      assert_equal ticket.brand.name, proto_ticket.brand.name.value
    end

    it 'can encode without' do
      ticket.brand = nil

      assert_nil proto_ticket.brand
    end
  end

  describe ':description' do
    it 'correctly encodes if present' do
      assert_equal ticket.description, proto_ticket.description.value
    end
  end

  describe ':due_at' do
    it 'correctly encodes if present' do
      ticket.update_column(:due_date, Time.current)
      assert_equal ticket.due_date.to_i, proto_ticket.due_at.seconds
    end

    it 'can encode without' do
      ticket.due_date = nil

      assert_nil proto_ticket.due_at
    end
  end

  describe ':group' do
    it 'correctly encodes if present' do
      assert_equal ticket.group_id.to_i, proto_ticket.group.id.value
      assert_equal ticket.group.name, proto_ticket.group.name.value
    end

    it 'can encode without' do
      ticket.group = nil

      assert_nil proto_ticket.group
    end
  end

  describe ':id' do
    it 'correctly encodes if present' do
      assert_equal ticket.id.to_i, proto_ticket.id.value
    end
  end

  describe ':initially_assigned_at' do
    it 'correctly encodes if present' do
      assert_equal ticket.initially_assigned_at.to_i, proto_ticket.initially_assigned_at.seconds
    end

    it 'can encode without' do
      ticket.initially_assigned_at = nil

      assert_nil proto_ticket.initially_assigned_at
    end
  end

  describe ':is_public' do
    it 'correctly encodes if present' do
      assert_equal ticket.is_public, proto_ticket.is_public.value
    end
  end

  describe ':locale' do
    it 'correctly encodes if present' do
      ticket.requester.locale_id = 1

      assert_equal ticket.locale_id, proto_ticket.locale.id.value
      assert_equal ticket.translation_locale.locale, proto_ticket.locale.code.value
    end

    it 'can encode without' do
      assert_nil proto_ticket.locale
    end
  end

  describe ':nice_id' do
    it 'correctly encodes if present' do
      assert_equal ticket.nice_id.to_i, proto_ticket.nice_id.value
    end
  end

  describe ':organization' do
    it 'correctly encodes if present' do
      ticket.organization = organizations(:minimum_organization1)
      assert_equal ticket.organization_id.to_i, proto_ticket.organization.id.value
      assert_equal ticket.organization.name, proto_ticket.organization.name.value
    end

    it 'can encode without' do
      ticket.organization = nil

      assert_nil proto_ticket.organization
    end
  end

  describe ':priority' do
    it 'correctly encodes if present' do
      assert_equal ticket.priority_id, proto_ticket.priority.id.value
      assert ticket.priority.casecmp(proto_ticket.priority.type.to_s)
    end

    it 'can encode without' do
      ticket.priority_id = nil

      assert_nil proto_ticket.priority
    end
  end

  describe ':recipient' do
    it 'correctly encodes if present' do
      assert_equal ticket.recipient, proto_ticket.recipient.value
    end

    it 'can encode without' do
      ticket.recipient = nil

      assert_nil proto_ticket.recipient
    end
  end

  describe ':requester' do
    it 'correctly encodes if present' do
      assert_equal ticket.requester.id, proto_ticket.requester.id.value
      assert_equal ticket.requester.name, proto_ticket.requester.name.value
      assert_equal ticket.requester.roles, proto_ticket.requester.role.role_id.value
    end

    it 'can encode without' do
      ticket.requester = nil

      assert_nil proto_ticket.requester
    end
  end

  describe ':requester_updated_at' do
    it 'correctly encodes if present' do
      assert_equal ticket.requester_updated_at.to_i, proto_ticket.requester_updated_at.seconds
    end

    it 'can encode without' do
      ticket.requester_updated_at = nil

      assert_nil proto_ticket.requester_updated_at
    end
  end

  describe ':satisfaction' do
    it 'correctly encodes if all present' do
      ticket.satisfaction_probability = 0.5

      assert SatisfactionType.to_s(ticket.satisfaction_score).casecmp(proto_ticket.satisfaction.score.to_s)
      assert_equal ticket.satisfaction_score, proto_ticket.satisfaction.score_id.value
      assert_equal ticket.satisfaction_probability, proto_ticket.satisfaction.probability.value
      assert_equal ticket.satisfaction_reason_code, proto_ticket.satisfaction.reason_code.value
    end

    it 'can encode without satisfaction score' do
      ticket.satisfaction_score = nil

      enum = Zendesk::Protobuf::Support::Tickets::V2::Score
      assert_equal enum::UNKNOWN_SCORE, enum.resolve(proto_ticket.satisfaction.score)
      assert_nil proto_ticket.satisfaction.score_id
    end

    it 'can encode without satisfaction probability' do
      assert_nil proto_ticket.satisfaction.probability
    end

    it 'can encode without satisfaction reason code' do
      ticket.satisfaction_reason_code = nil

      assert_nil proto_ticket.satisfaction.reason_code
    end
  end

  describe ':sharing_agreements' do
    it 'correctly encodes if present' do
      ticket.stubs(shared_tickets: [stub(agreement_id: 1)])

      assert_equal [1], proto_ticket.sharing_agreements.ids.map(&:value)
    end

    it 'can encode without' do
      assert_nil proto_ticket.sharing_agreements
    end
  end

  describe ':sla_breach_status' do
    it 'correctly encodes if present' do
      assert_equal ticket.sla_breach_status, proto_ticket.sla_breach_status.value
    end

    it 'can encode without' do
      ticket.sla_breach_status = nil

      assert_nil proto_ticket.sla_breach_status
    end
  end

  describe ':solved_at' do
    it 'correctly encodes if present' do
      ticket.status_id = StatusType.SOLVED
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!

      assert_equal ticket.solved_at.to_i, proto_ticket.solved_at.seconds
    end

    it 'can encode without' do
      assert_nil proto_ticket.solved_at
    end
  end

  describe ':status' do
    it 'correctly encodes if present' do
      ticket.status_id = 6 # Test On Hold as it's a special case

      assert_equal ticket.status_id, proto_ticket.status.id.value
      assert ticket.status.casecmp(proto_ticket.status.type.to_s)
      assert_equal StatusType.order.index(ticket.status_id), proto_ticket.status.position.value
      assert_equal ticket.status_updated_at.to_i, proto_ticket.status.updated_at.seconds
    end

    it 'can encode without' do
      ticket.status_id = nil
      ticket.status_updated_at = nil

      assert_nil proto_ticket.status.id
      enum = Zendesk::Protobuf::Support::Tickets::V2::TicketStatus
      assert_equal enum::UNKNOWN_TICKET_STATUS, enum.resolve(proto_ticket.status.type)
      assert_nil proto_ticket.status.position
      assert_nil proto_ticket.status.updated_at
    end
  end

  describe ':submitter' do
    it 'correctly encodes if present' do
      assert_equal ticket.submitter.id, proto_ticket.submitter.id.value
      assert_equal ticket.submitter.name, proto_ticket.submitter.name.value
    end

    it 'can encode without' do
      ticket.submitter = nil

      assert_nil proto_ticket.submitter
    end
  end

  describe ':tags' do
    it "encodes tags if present" do
      ticket.current_tags = "a b c"
      assert_equal "a b c".split, proto_ticket.tags.tags
    end

    it "can encode without" do
      ticket.current_tags = nil
      assert_nil proto_ticket.tags
    end
  end

  describe ':ticket_form' do
    it 'correctly encodes if present' do
      ticket.ticket_form = ticket_forms(:default_ticket_form)
      assert_equal ticket.ticket_form_id, proto_ticket.ticket_form.id.value
    end

    it 'can encode without' do
      ticket.ticket_form = nil

      assert_nil proto_ticket.ticket_form
    end
  end

  describe ':ticket_type' do
    it 'correctly encodes if present' do
      assert_equal ticket.ticket_type_id, proto_ticket.ticket_type.id.value
      assert ticket.ticket_type.casecmp(proto_ticket.ticket_type.type.to_s)
    end

    it 'can encode without' do
      ticket.ticket_type_id = nil

      assert_nil proto_ticket.ticket_type
    end
  end

  describe ':timestamps' do
    it 'correctly encodes if present' do
      assert_equal ticket.created_at.to_i, proto_ticket.timestamps.created_at.seconds
      assert_equal ticket.updated_at.to_i, proto_ticket.timestamps.updated_at.seconds
    end

    it 'can encode without updated_at' do
      ticket.updated_at = nil

      assert_nil proto_ticket.timestamps.updated_at
    end

    it 'can encode without created_at' do
      ticket.created_at = nil

      assert_nil proto_ticket.timestamps.created_at
    end
  end

  describe ':updated_by_type' do
    it 'correctly encodes if present' do
      assert_equal ticket.updated_by_type_id, proto_ticket.updated_by_type.id.value
      assert ticket.updated_by_type.casecmp(proto_ticket.updated_by_type.type.to_s)
    end

    it 'can encode without' do
      ticket.updated_by_type_id = nil

      assert_nil proto_ticket.updated_by_type
    end
  end

  describe 'via' do
    it 'correctly encodes if present' do
      assert_equal ticket.via_id, proto_ticket.via.id.value
      assert ticket.via.casecmp(proto_ticket.via.type.to_s)
    end

    it 'can encode without' do
      ticket.via_id = nil

      assert_nil proto_ticket.via
    end
  end

  describe ':via_reference_id' do
    it 'correctly encodes if present' do
      ticket.via_reference_id = 1

      assert_equal ticket.via_reference_id, proto_ticket.via_reference_id.value
    end

    it 'can encode without' do
      assert_nil proto_ticket.via_reference_id
    end
  end

  describe ':subject' do
    it 'correctly encodes if present' do
      ticket.subject = "minimum 1 ticket"

      assert_equal ticket.subject, proto_ticket.subject.value
    end

    it 'can encode without' do
      ticket.subject = nil

      assert_nil proto_ticket.subject
    end
  end

  describe ':schedule' do
    it 'correctly encodes if present' do
      schedule = ticket.account.schedules.create(
        name: 'schedule_1',
        time_zone: ticket.account.time_zone
      )

      ticket.create_ticket_schedule(
        account:  ticket.account,
        schedule: schedule
      )

      assert_equal ticket.ticket_schedule.id, proto_ticket.schedule.id.value
    end

    it 'can encode without' do
      ticket.ticket_schedule = nil

      assert_nil proto_ticket.schedule
    end
  end
end
