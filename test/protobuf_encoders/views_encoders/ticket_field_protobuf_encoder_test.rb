require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 0

describe ViewsEncoders::TicketFieldProtobufEncoder do
  fixtures :tickets, :ticket_fields, :accounts

  let(:ticket)       { tickets(:minimum_1) }
  let(:account)      { ticket.account }
  let(:ticket_field) { ticket_fields(:field_integer_custom) }

  let(:ticket_field_value) { 1 }

  let(:ticket_field_entry) do
    TicketFieldEntry.new(
      ticket_field: ticket_field,
      ticket: ticket,
      account: account
    )
  end

  before { ticket_field_entry.value = ticket_field_value }

  let(:ticket_field_encoder) do
    ViewsEncoders::TicketFieldProtobufEncoder.new(ticket_field_entry)
  end

  it 'returns object with #to_proto' do
    assert_kind_of String, ticket_field_encoder.to_proto
  end

  it 'sets the header' do
    assert ticket_field_encoder.to_object.header
  end

  it 'contains a ticket field' do
    assert ticket_field_encoder.to_object.ticket_field
  end

  it 'contains a ticket field entry' do
    assert ticket_field_encoder.to_object.ticket_field_entry
  end

  it 'contains a ticket id' do
    assert ticket_field_encoder.to_object.ticket_id
  end

  it 'sets the correct schema' do
    assert_equal(
      "support/views/ticket_field",
      ticket_field_encoder.to_object.header.schema_path
    )
  end
end
