require_relative "../../support/test_helper"

SingleCov.covered!

describe ViewsEncoders::UpdatedByTypeProtobufEncoder do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:subject) { ViewsEncoders::UpdatedByTypeProtobufEncoder.new(ticket) }
  let(:proto) { subject.to_object }

  it 'correctly encodes agent type' do
    ticket.updated_by_type_id = 0

    assert_equal 0, proto.id.value
    assert_equal :AGENT, proto.type
  end

  it 'correctly encodes agent type' do
    ticket.updated_by_type_id = 1

    assert_equal 1, proto.id.value
    assert_equal :END_USER, proto.type
  end
end
