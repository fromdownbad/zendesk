require_relative "../../support/test_helper"

SingleCov.covered!

describe ViewsEncoders::StatusProtobufEncoder do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:subject) { ViewsEncoders::StatusProtobufEncoder.new(ticket) }
  let(:proto) { subject.to_object }

  before do
    ticket.status_id = 6
  end

  it 'uses the id' do
    assert_equal 6, proto.id.value
  end

  it 'correctly encodes the type' do
    assert_equal :HOLD, proto.type
  end

  it 'correctly encodes the position' do
    assert_equal 3, proto.position.value
  end

  it 'encodes status_updated_at' do
    assert_kind_of Google::Protobuf::Timestamp, proto.updated_at
  end
end
