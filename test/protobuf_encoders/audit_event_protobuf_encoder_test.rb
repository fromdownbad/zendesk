require_relative "../support/test_helper"

SingleCov.covered!

describe AuditEventProtobufEncoder do
  include CustomFieldsTestHelper

  fixtures :accounts, :users, :subscriptions

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_end_user) }
  let(:subscription) { subscriptions(:minimum) }
  let(:audit) do
    CIA::Event.new(
      account: account,
      actor: user,
      created_at: Time.parse('2019-01-01T00:00:00Z'),
      ip_address: '127.0.0.1',
      source: subscription,
      action: "update",
      visible: true,
      message: "bleep bloop",
      attribute_changes: [CIA::AttributeChange.new(attribute_name: "foo", old_value: "old", new_value: "newv")]
    )
  end
  let(:proto_audit) do
    ::Zendesk::Protobuf::AccountAudits::AuditEvent.decode(
      AuditEventProtobufEncoder.new(audit).to_proto
    )
  end

  it "correctly encodes audit event data" do
    assert_equal audit.account_id, proto_audit.account_id
    assert_equal audit.actor_id, proto_audit.actor_id
    assert_equal audit.action, proto_audit.action.to_s.downcase
    assert_equal audit.ip_address, proto_audit.ip_address
    assert_equal audit.message, proto_audit.raw_message.text
    assert_equal subscription.id, proto_audit.source_id.to_i
    assert_equal "Subscription", proto_audit.source_type
    assert_equal audit.visible, proto_audit.visible
    assert_equal audit.attribute_changes.first.attribute_name, proto_audit.changes.first.attribute_name
    assert_equal audit.attribute_changes.first.old_value, proto_audit.changes.first.old_value
    assert_equal audit.attribute_changes.first.new_value, proto_audit.changes.first.new_value
  end

  describe "with an invalid action type" do
    it "raises an error" do
      audit.action = "foo bar"
      assert_raises 'invalid action type in an audit' do
        AuditEventProtobufEncoder.new(audit).to_object
      end
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        AuditEventProtobufEncoder.new(audit, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      AuditEventProtobufEncoder.any_instance.expects(:full).once

      AuditEventProtobufEncoder.new(audit)
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = AuditEventProtobufEncoder.new(audit, [:account_id, :action]).to_object

      proto_object.to_h.each do |field, value|
        if [:account_id, :action].include?(field)
          refute_nil value
        else
          assert 0, value
        end
      end
    end
  end
end
