require_relative "../support/test_helper"

SingleCov.covered!

describe AccountSettingProtobufEncoder do
  include CustomFieldsTestHelper

  let(:proto_account_setting) do
    ::Zendesk::Protobuf::Support::Accounts::AccountSettingEvent.decode(
      AccountSettingProtobufEncoder.new(setting).to_proto
    )
  end

  describe "end_user_attachments" do
    let(:setting) do
      AccountSetting.new(account_id: '123', name: 'end_user_attachments', value: '1')
    end

    it "is supported" do
      assert AccountSettingProtobufEncoder.supported? 'end_user_attachments'
    end

    it "correctly encodes changes" do
      assert_equal setting.account_id, proto_account_setting.header.account_id.value
      assert_nil proto_account_setting.end_user_attachments_changed.previous
      assert proto_account_setting.end_user_attachments_changed.current.value
    end
  end

  describe "focus_mode" do
    let(:setting) do
      AccountSetting.new(account_id: '123', name: 'focus_mode', value: '1')
    end

    it "is supported" do
      assert AccountSettingProtobufEncoder.supported? 'focus_mode'
    end

    it "correctly encodes changes" do
      assert_equal setting.account_id, proto_account_setting.header.account_id.value
      assert_nil proto_account_setting.focus_mode_changed.previous
      assert proto_account_setting.focus_mode_changed.current.value
    end
  end

  describe 'agent workspace' do
    let(:setting) { AccountSetting.new(account_id: '123', name: 'polaris', value: '1', updated_at: Time.zone.now) }

    it "is supported" do
      assert AccountSettingProtobufEncoder.supported? setting.name
    end

    it "correctly encodes changes" do
      assert_equal setting.account_id, proto_account_setting.header.account_id.value
      assert_nil proto_account_setting.polaris_changed.previous
      assert proto_account_setting.polaris_changed.current.value
    end

    it "correctly encodes headers" do
      assert_equal 'support/accounts/settings', proto_account_setting.header.schema_path
      assert_equal setting.updated_at.to_i, proto_account_setting.header.occurred_at&.seconds
    end
  end

  describe 'social messaging in agent workspace' do
    let(:setting) { AccountSetting.new(account_id: '123', name: 'social_messaging_agent_workspace', value: '1') }

    it "is supported" do
      assert AccountSettingProtobufEncoder.supported? setting.name
    end

    it "correctly encodes changes" do
      assert_equal setting.account_id, proto_account_setting.header.account_id.value
      assert_nil proto_account_setting.social_messaging_agent_workspace_changed.previous
      assert proto_account_setting.social_messaging_agent_workspace_changed.current.value
    end
  end

  describe 'native messaging' do
    let(:setting) { AccountSetting.new(account_id: '123', name: 'native_messaging', value: '1') }

    it "is supported" do
      assert AccountSettingProtobufEncoder.supported? setting.name
    end

    it "correctly encodes changes" do
      assert_equal setting.account_id, proto_account_setting.header.account_id.value
      assert_nil proto_account_setting.native_messaging_changed.previous
      assert proto_account_setting.native_messaging_changed.current.value
    end
  end

  describe "others" do
    let(:setting) do
      AccountSetting.new(account_id: '123', name: 'others_does_not_exist', value: 'anything')
    end

    describe "#supported?" do
      it "returns false" do
        refute AccountSettingProtobufEncoder.supported? 'others_does_not_exist'
      end
    end

    it "does not encode event data" do
      assert_equal setting.account_id, proto_account_setting.header.account_id.value
      assert_nil proto_account_setting.end_user_attachments_changed
    end
  end
end
