require_relative "../support/test_helper"

SingleCov.covered!

describe AccountProtobufEncoder do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  describe "with a full field map" do
    let(:proto_account) do
      ::Zendesk::Protobuf::Support::Accounts::Account.decode(
        AccountProtobufEncoder.new(account).to_proto
      )
    end

    describe "for primitive fields" do
      # For cases where the model and proto field name are different, use a subarray
      [:id, :is_active, :name, :subdomain, :time_zone, :is_serviceable, :shard_id, :lock_state, :multiproduct].each do |model_field, proto_field|
        it "correctly encodes #{proto_field}" do
          proto_field ||= model_field

          assert_equal account.send(model_field), proto_account.send(proto_field).value
        end
      end

      describe "for nullable primitive fields" do
        [:host_mapping, :dnsttl, :help_desk_size].each do |field|
          it "encodes #{field} if present" do
            set_field_value(account, field)
            assert_equal account.send(field), proto_account.send(field).value
          end

          it "can encode without #{field}" do
            account.update_column(field, nil)
            assert_nil proto_account.send(field)
          end
        end
      end

      describe "for default_brand_id" do
        it "encodes default_brand_id if present" do
          account.stubs(:default_brand_id).returns(0)
          assert_equal account.default_brand_id, proto_account.default_brand_id.value
        end

        it "can encode without default_brand_id" do
          account.stubs(:default_brand_id).returns(nil)
          assert_nil proto_account.default_brand_id
        end
      end
    end

    describe "for nested fields" do
      # Each hash key is the protobuf object field, and the value it's expected type
      {
        locale: Zendesk::Protobuf::Support::Locales::Locale
      }.each do |field, encoder|
        it "encodes #{field}" do
          assert_instance_of encoder, proto_account.send(field)
        end
      end

      describe "for nullable nested fields" do
        describe "when encoding owner" do
          it "encodes owner if present" do
            account.owner = users(:minimum_admin)
            assert_instance_of Zendesk::Protobuf::Support::Users::User, proto_account.owner
          end

          it "can encode without owner" do
            account.owner = nil
            assert_nil proto_account.owner
          end
        end

        describe "when encoding sandbox_master" do
          it "encodes sandbox_master id if present" do
            account.sandbox_master = accounts(:multiproduct)
            assert_equal account.sandbox_master.id, proto_account.sandbox_master.id.value
            assert_nil proto_account.sandbox_master.owner
          end

          it "can encode without sandbox_master" do
            assert_nil proto_account.sandbox_master
          end
        end
      end
    end

    describe "for timestamps" do
      [:created_at, :updated_at].each do |field|
        it "correctly encodes #{field}" do
          assert_equal account.send(field).to_i, proto_account.timestamps.send(field).seconds
          assert_equal account.send(field).nsec, proto_account.timestamps.send(field).nanos
        end
      end

      describe "for deleted_at" do
        it "encodes deleted_at if present" do
          account.update_column(:deleted_at, Time.now)
          assert_equal account.deleted_at.to_i, proto_account.deleted_at.seconds
          assert_equal account.deleted_at.nsec, proto_account.deleted_at.nanos
        end

        it "can encode without deleted_at" do
          account.update_column(:deleted_at, nil)
          assert_nil proto_account.deleted_at
        end
      end
    end
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        AccountProtobufEncoder.new(account, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      AccountProtobufEncoder.any_instance.expects(:full).once

      AccountProtobufEncoder.new(account)
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = AccountProtobufEncoder.new(account, [:id, :is_active]).to_object

      proto_object.to_h.each do |field, value|
        if [:id, :is_active].include?(field)
          refute_nil value
        else
          assert_nil value
        end
      end
    end

    it "passes sub the map to the next encoder" do
      UserProtobufEncoder.expects(:new).with(account.owner, [:id]).returns(mock(to_object: nil))

      AccountProtobufEncoder.new(account, [owner: [:id]]).to_object
    end
  end
end
