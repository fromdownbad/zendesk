require_relative "../support/test_helper"

SingleCov.covered!

describe UserProtobufEncoder do
  include CustomFieldsTestHelper

  fixtures :accounts, :users, :organization_memberships, :organizations

  let(:user) { users(:minimum_end_user) }
  let(:tags) { ["hey", "ho"] }
  let(:proto_user) do
    ::Zendesk::Protobuf::Support::Users::User.decode(
      UserProtobufEncoder.new(user).to_proto
    )
  end

  it "correctly encodes user record data" do
    assert_equal user.account_id, proto_user.account.id.value
    assert_equal user.account.name, proto_user.account.name.value
    assert_equal user.account.subdomain, proto_user.account.subdomain.value

    assert_equal user.created_at.to_i, proto_user.timestamps.created_at.seconds
    assert_equal user.created_at.nsec, proto_user.timestamps.created_at.nanos

    assert_equal user.id, proto_user.id.value
    assert_equal user.name, proto_user.name.value
    assert_equal !user.is_end_user?, proto_user.is_staff.value
    assert_equal user.is_active?, proto_user.is_active.value
    assert_equal user.suspended?, proto_user.is_suspended.value
    assert_equal user.time_zone, proto_user.time_zone.value

    assert_equal user.locale.to_s, proto_user.locale.value
    assert_equal user.agent_display_name.to_s, proto_user.agent_display_name.value
    assert_equal user.details.to_s, proto_user.details.value
    assert_equal user.notes.to_s, proto_user.notes.value
  end

  describe "with user photo" do
    it "correctly encodes and decodes" do
      user_with_photo = users(:minimum_end_user)
      user_with_photo.set_photo(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png"))
      user_with_photo.save!

      proto_user_with_photo = ::Zendesk::Protobuf::Support::Users::User.decode(
        UserProtobufEncoder.new(user).to_proto
      )

      assert_equal user_with_photo.photo_path(:thumb), proto_user_with_photo.photo.thumbnail_url.value
      assert_equal user_with_photo.photo_path, proto_user_with_photo.photo.original_url.value
    end

    it "correctly encodes and decodes non-ascii characters" do
      user_with_photo = users(:minimum_end_user)
      user_with_photo.set_photo(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png"))
      user_with_photo.photo.update(filename: "iOS_の画像__5__thumb.jpg")
      user_with_photo.save!

      proto_user_with_photo = ::Zendesk::Protobuf::Support::Users::User.decode(
        UserProtobufEncoder.new(user).to_proto
      )

      assert_equal user_with_photo.photo_path(:thumb), proto_user_with_photo.photo.thumbnail_url.value
      assert_equal user_with_photo.photo_path, proto_user_with_photo.photo.original_url.value
    end

    it "does not encode photo if user has no photo" do
      assert_nil user.photo
      assert_nil proto_user.photo
    end
  end

  describe "with user fields" do
    before do
      @account = accounts(:minimum) # used by helper below

      user.custom_field_values.build(
        field: create_user_custom_field!("field_1", "Field 1", "Date"),
        value: Time.new(2002, 10, 31)
      )
      user.save!
    end

    it "correctly encodes and decodes" do
      assert_equal "2002-10-31 00:00:00 +0000", proto_user.user_field_values["field_1"]
    end
  end

  describe "with user tags" do
    before do
      Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)

      user.current_tags = tags
      user.save!
    end

    it "correctly encodes tags" do
      assert_equal tags, proto_user.tag_list.tags
    end
  end

  it "correctly encodes deliverable email identity" do
    verified_user = users(:minimum_end_user)
    identity = UserEmailIdentity.create(user: verified_user, value: "john@example.com", is_verified: true, deliverable_state: "deliverable")

    proto_verified_user = ::Zendesk::Protobuf::Support::Users::User.decode(
      UserProtobufEncoder.new(verified_user.reload).to_proto
    )
    proto_identity = proto_verified_user.identities.identities.find { |x| x.id.value == identity.id }

    assert proto_identity.deliverable.value
  end

  it "correctly encodes identities" do
    assert_equal 8, proto_user.identities.identities.count
    assert_equal "minimum_end_user@aghassipour.com", proto_user.identities.identities.first.value.value
    assert_equal "EMAIL", proto_user.identities.identities.first.type.to_s
    assert proto_user.identities.identities.first.deliverable.value
  end

  it "correctly encodes email" do
    assert_equal "minimum_end_user@aghassipour.com", proto_user.email.value
  end

  it "correctly encodes nil locale_id" do
    assert_nil proto_user.locale_id
  end

  it "correctly encodes locale_id" do
    user.locale_id = 1
    user.save
    proto_user = ::Zendesk::Protobuf::Support::Users::User.decode(
      UserProtobufEncoder.new(user.reload).to_proto
    )

    assert 1, proto_user.locale_id
  end

  it "correctly encodes organizations" do
    assert_equal "minimum organization", proto_user.orgs.orgs.first.name
  end

  describe "with an invalid field map" do
    it "raises an error" do
      assert_raises ZendeskProtobufEncoder::InvalidFieldMap do
        UserProtobufEncoder.new(user, [:foo]).to_object
      end
    end
  end

  describe "with no field map" do
    it "defaults to full" do
      UserProtobufEncoder.any_instance.expects(:full).once

      UserProtobufEncoder.new(user)
    end
  end

  describe "with a thin field map" do
    let(:proto_user) do
      ::Zendesk::Protobuf::Support::Users::User.decode(
        UserProtobufEncoder.new(user).thin.to_proto
      )
    end

    it 'encodes the thin fields' do
      refute_nil proto_user.id
      refute_nil proto_user.name
    end
  end

  describe "with a specified field map" do
    it "only encodes the specified fields" do
      proto_object = UserProtobufEncoder.new(user, [:id, :is_active]).to_object

      proto_object.to_h.each do |field, value|
        if [:id, :is_active].include?(field)
          refute_nil value
        else
          assert_nil value
        end
      end
    end
  end
end
