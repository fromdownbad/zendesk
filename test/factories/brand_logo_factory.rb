# BrandLogo Factories

FactoryBot.define do
  factory :brand_logo do
    account { Account.find_by_subdomain("minimum") }
    association :brand
    filename { "#{Rails.root}/test/files/normal_1.jpg" }
    uploaded_data do |u|
      filename = "#{Rails.root}/test/files/" + u.filename unless u.filename.index('/')
      Zendesk::Attachments::CgiFileWrapper.new(filename.to_s)
    end
  end
end
