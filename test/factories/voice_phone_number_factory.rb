FactoryBot.define do
  factory :incoming_phone_number, class: Voice::PhoneNumber do
    association :account
    sequence(:number) { |n| "+123456789#{n}" }
    sid { '123sid' }
    recorded { true }
    country_id { 156 } # United States
  end
end
