Account.class_eval do
  # fake out :set_owner so factories can set the owner
  attr_writer :owner_params
end

FactoryBot.define do
  factory :account, class: 'Accounts::Classic' do
    name { 'NewTestAccount' }
    sequence(:subdomain) { |n| "subdomain#{n}" }
    time_zone        { 'Copenhagen' }
    help_desk_size   { "Small team" }
    owner_params     { FactoryBot.attributes_for(:verified_admin, account: nil) }
    after(:build)      do |account|
      account.set_address(name: 'My Company name', phone: '123', country: 'Denmark')
      account.build_pre_account_creation(
        locale_id:     account.locale_id,
        region:        'us',
        source:        'classic',
        pod_id:        Zendesk::Configuration.fetch(:pod_id),
        status:        PreAccountCreation::ProvisionStatus::ONGOING
      )
    end

    factory :account_with_unverified_owner do
      owner_params { FactoryBot.attributes_for(:unverified_admin, account: nil) }
      after(:create)     do |account|
        account.owner.send_verify_email = true if account.owner.present?
      end
    end
  end
end
