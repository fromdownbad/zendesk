FactoryBot.define do
  factory :facebook_page, class: Facebook::Page do
    account { Account.find_by_subdomain("minimum") }
    sequence(:graph_object_id) { |n| "graph_object_id#{n}" }
    sequence(:access_token) { |n| "access_token#{n}" }
    sequence(:link) { |n| "link#{n}" }
    sequence(:name) { |n| "name#{n}" }
    sequence(:category) { |n| "category#{n}" }
    state { Facebook::Page::State::FRESH }
    page_settings { { include_posts_by_page: false, include_wall_posts: false, include_messages: false } }

    trait :active do
      state { Facebook::Page::State::ACTIVE }
    end

    trait :include_posts_by_page do
      page_settings { { include_posts_by_page: true } }
    end

    trait :include_wall_posts do
      page_settings { { include_wall_posts: true } }
    end

    trait :include_messages do
      page_settings { { include_messages: true } }
    end
  end
end
