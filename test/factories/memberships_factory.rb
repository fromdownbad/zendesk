FactoryBot.define do
  sequence(:membership_name) { |n| "Membership #{n}" }

  factory :membership do
    association :user
    association :group
    default { false }
  end
end
