FactoryBot.define do
  factory :suspended_ticket do
    association :account

    subject     { "A subject" }
    content     { "Some content" }
    from_name   { "Gretta Garbo" }
    from_mail   { "gretta@zendesk.com" }
    cause       { SuspensionType.SIGNUP_REQUIRED }
  end
end
