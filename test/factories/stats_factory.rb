# Stats Factories

FactoryBot.define do
  factory :hc_search_string_stat, class: StatRollup::HCSearchStringStat do
    origin { 'all' }
    ts { 30.days.ago.utc.beginning_of_day }
    duration { 2592000 }
    string { 'a search query' }
    searches { 1 }
    ctr { 1 }
    avg_results { 1 }
    tickets { 0 }
  end
end
