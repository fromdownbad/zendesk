FactoryBot.define do
  factory :voice_insights_field, class: Voice::InsightsField do
    association :account

    after(:build)  { |requirement| requirement.stubs(:readonly?).returns(false) }
    after(:create) { |requirement| requirement.unstub(:readonly?) }
  end
end
