FactoryBot.define do
  factory :voice_partner_edition_account, class: Voice::PartnerEditionAccount do
    association :account
    plan_type { Voice::PartnerEditionAccount::PLAN_TYPE[:regular] }
    active { true }
    trial_expires_at { 30.days.from_now }
  end
end
