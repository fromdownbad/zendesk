FactoryBot.define do
  factory :voice_account, class: Voice::VoiceAccount do
    plan_type { 2 }

    after(:build)  { |requirement| requirement.stubs(:readonly?).returns(false) }
    after(:create) { |requirement| requirement.unstub(:readonly?) }
  end
end
