FactoryBot.define do
  factory :voice_greeting, class: Voice::Greeting do
    after(:build)  { |requirement| requirement.stubs(:readonly?).returns(false) }
    after(:create) { |requirement| requirement.unstub(:readonly?) }
  end
end
