FactoryBot.define do
  factory :voice_subscription, class: Voice::Subscription do
    association :account
    plan_type { 1 }
    max_agents { 5 }

    after(:build) do |voice_subscription|
      voice_subscription.stubs(:notify_voice_of_creation)
      voice_subscription.stubs(:recent_active_agent_ids).returns([])
    end
  end
end
