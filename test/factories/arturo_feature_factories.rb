FactoryBot.define do
  factory :arturo_feature, class: Arturo::Feature do
    sequence(:symbol) { |n| "feature_#{n}".to_sym }
    deployment_percentage { rand(101) }
    phase { 'on' }
  end
end
