FactoryBot.define do
  sequence(:entry_title) { |n| "Entry #{n}" }

  factory :entry do
    association :account
    association :forum

    title   { FactoryBot.generate(:entry_title) }
    body    { "An entry in a forum" }

    submitter
    current_user

    is_locked { false }
    is_pinned { false }
    is_public { 1 }
  end
end
