# Group Factories
FactoryBot.define do
  sequence(:group_name) { |n| "Group #{n}" }

  factory :group do |_g|
    name      { FactoryBot.generate(:group_name) }
    is_active { true }
  end
end
