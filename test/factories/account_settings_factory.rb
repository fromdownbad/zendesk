FactoryBot.define do
  factory :account_setting do
    account { Account.find_by_subdomain("minimum") }

    factory :max_identities_account_setting do
      name { 'max_identities' }
      value { 1 }
    end
  end
end
