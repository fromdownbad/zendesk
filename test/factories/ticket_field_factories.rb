FactoryBot.define do
  factory :integer_ticket_field, class: FieldInteger do
    account               { Account.first }
    title                 { 'my_integer_field' }
    title_in_portal       { 'My integer field' }
    is_required           { false }
    is_required_in_portal { false }
    is_visible_in_portal  { true }
    is_editable_in_portal { true }
    description           { 'Run of the mill integer field' }
    is_active             { true }
  end

  factory :checkbox_ticket_field, class: FieldCheckbox do
    account               { Account.first }
    title                 { 'my_checkbox_field' }
    title_in_portal       { 'My checkbox field' }
    is_required           { false }
    is_required_in_portal { false }
    is_visible_in_portal  { true }
    is_editable_in_portal { true }
    description           { 'Run of the mill checkbox field' }
    is_active             { true }
  end

  factory :priority_ticket_field, class: FieldPriority do
    account               { Account.first }
    title                 { 'my_priority_field' }
    title_in_portal       { 'My priority field' }
    is_required           { false }
    is_required_in_portal { false }
    is_visible_in_portal  { true }
    is_editable_in_portal { true }
    description           { 'Run of the mill priority field' }
    is_active             { true }
  end

  factory :ticket_type_ticket_field, class: FieldTicketType do
    account               { Account.first }
    title                 { 'my_ticket_type_field' }
    title_in_portal       { 'My ticket type field' }
    is_required           { false }
    is_required_in_portal { false }
    is_visible_in_portal  { true }
    is_editable_in_portal { true }
    description           { 'Run of the mill ticket type field' }
    is_active             { true }
  end

  factory :decimal_ticket_field, class: FieldDecimal do
    account               { Account.first }
    title                 { 'my_decimal_field' }
    title_in_portal       { 'My decimal field' }
    is_required           { false }
    is_required_in_portal { false }
    is_visible_in_portal  { true }
    is_editable_in_portal { true }
    description           { 'Run of the mill decimal field' }
    is_active             { true }
  end

  factory :text_ticket_field, class: FieldText do
    account               { Account.first }
    title                 { 'my_text_field' }
    title_in_portal       { 'My text field' }
    is_required           { false }
    is_required_in_portal { false }
    is_visible_in_portal  { true }
    is_editable_in_portal { true }
    description           { 'Run of the mill text field' }
    is_active             { true }
  end

  factory :field_tagger, class: FieldTagger do
    transient do
      sequence :seq
    end

    account               { Account.first }
    title                 { "Tagger Field #{seq}" }
    title_in_portal       { title }
    is_required           { false }
    is_required_in_portal { false }
    is_visible_in_portal  { true }
    is_editable_in_portal { true }
    description           { "Description for #{title}" }
    is_active             { true }

    transient do
      custom_field_options_count { 2 }
    end

    after(:build) do |field, evaluator|
      seq = evaluator.seq
      evaluator.custom_field_options_count.times do |i|
        field.custom_field_options.build(name: "Custom field option #{seq} #{i}", value: "cfo_#{seq}_#{i}")
      end
    end
  end

  factory :field_multiselect, class: FieldMultiselect do
    account               { Account.first }
    title                 { 'Favorite Colors' }
    title_in_portal       { 'Favorite Colors' }
    is_required           { false }
    is_required_in_portal { false }
    is_visible_in_portal  { true }
    is_editable_in_portal { true }
    description           { 'Your favorite Colors' }
    is_active             { true }

    after(:build) do |field|
      field.custom_field_options.build(name: 'Red',    value: 'red')
      field.custom_field_options.build(name: 'Purple', value: 'purple')
      field.custom_field_options.build(name: 'Green',  value: 'green')
    end
  end
end
