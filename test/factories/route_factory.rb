FactoryBot.define do
  factory :route do
    account { Account.find_by_subdomain("minimum") }
    sequence(:subdomain) { |n| "subdomain#{n}" }
  end
end
