FactoryBot.define do
  factory :payment do
    factory :paid_current_quarterly_payment do
      max_agents             { 5 }
      billing_cycle_type     { BillingCycleType.Quarterly }
      plan_type              { SubscriptionPlanType.Medium }
      status                 { PaymentType.PAID }
      pricing_model_revision { 2 }
      period_begin_at        { 15.days.ago.utc.at_midnight }
      period_end_at          { 15.days.ago.utc.at_midnight + 90.days }
    end

    factory :voice_payment do
      account                { Account.find_by_subdomain("minimum") }
      subscription           { account.subscription }
      feature_type           { Payment::FeatureType::VOICE }
      period_begin_at        { 15.days.ago.utc.at_midnight }
      period_end_at          { 15.days.ago.utc.at_midnight + 90.days }
      amount                 { 19.99 }
      net                    { 19.99 }
      pricing_model_revision { 2 }
    end
  end
end
