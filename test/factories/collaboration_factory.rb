FactoryBot.define do
  factory :collaboration do
    account
    ticket

    factory :end_user_legacy_cc do
      association :user, factory: :end_user
      collaborator_type { CollaboratorType.LEGACY_CC }
    end

    factory :end_user_email_cc do
      association :user, factory: :end_user
      collaborator_type { CollaboratorType.EMAIL_CC }
    end

    factory :agent_legacy_cc do
      association :user, factory: :agent
      collaborator_type { CollaboratorType.LEGACY_CC }
    end

    factory :agent_email_cc do
      association :user, factory: :agent
      collaborator_type { CollaboratorType.EMAIL_CC }
    end

    factory :agent_follower do
      association :user, factory: :agent
      collaborator_type { CollaboratorType.FOLLOWER }
    end
  end
end
