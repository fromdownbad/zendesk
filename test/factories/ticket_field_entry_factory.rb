FactoryBot.define do
  factory :integer_ticket_field_entry, class: TicketFieldEntry do
    account
    ticket
    ticket_field  { FactoryBot.build :integer_ticket_field }
    value         { '99' }
  end

  factory :decimal_ticket_field_entry, class: TicketFieldEntry do
    account
    ticket
    ticket_field  { FactoryBot.build :decimal_ticket_field }
    value         { '3.14' }
  end
end
