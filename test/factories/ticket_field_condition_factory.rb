FactoryBot.define do
  factory :ticket_field_condition do
    account          { Account.first }
    ticket_form      { FactoryBot.create(:ticket_form, account: account) }
    parent_field     { FactoryBot.create(:text_ticket_field, account: account) }
    child_field      { FactoryBot.create(:decimal_ticket_field, account: account) }
    value            { rand(9999999).to_s }
    is_required      { false }
    user_type        { :agent }

    trait :agent do
      user_type      { :agent }
    end

    trait :end_user do
      user_type      { :end_user }
    end

    trait :checkbox_parent do
      parent_field     { FactoryBot.create(:checkbox_ticket_field, account: account) }
    end

    trait :priority_parent do
      parent_field     { FactoryBot.create(:priority_ticket_field, account: account) }
    end

    trait :ticket_type_parent do
      parent_field     { FactoryBot.create(:ticket_type_ticket_field, account: account) }
    end

    # Warning: This does not scale into > 1 uses i.e. `create_list` will not work.
    trait :tagger_parent do
      parent_field     { FactoryBot.create(:field_tagger, account: account) }
      value            { parent_field.custom_field_options.first.value }
    end

    after(:build) do |condition|
      TicketFormField.create(
        ticket_form: condition.ticket_form,
        account: condition.account,
        ticket_field: condition.parent_field,
        position: 1
      )
      TicketFormField.create(
        ticket_form: condition.ticket_form,
        account: condition.account,
        ticket_field: condition.child_field,
        position: 2
      )
    end
  end
end
