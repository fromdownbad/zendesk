FactoryBot.define do
  factory :guide_subscription, class: Guide::Subscription do
    association           :account
    active                { true }
    legacy                { false }
    plan_type             { 1 } # 'Lite'
    max_agents            { 5 }
    trial                 { false }
    trial_expires_at      { nil }
  end
end
