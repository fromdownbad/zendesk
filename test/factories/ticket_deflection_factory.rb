FactoryBot.define do
  factory :ticket_deflection do
    account

    state { TicketDeflection::STATE_SOLVED }

    after(:build) do |ticket_deflection|
      user = FactoryBot.create(:user, account: ticket_deflection.account)
      ticket_deflection.user = user

      ticket = FactoryBot.create(:ticket, account: ticket_deflection.account)
      ticket_deflection.ticket = ticket
    end
  end
end
