# CreditCard Factories

FactoryBot.define do
  factory :credit_card do
    card_type           { 'Visa' }
    name                { 'Will Waffle' }
    number              { '4571059999999982' }
    verification_value  { '123' }
    year                { 3.years.from_now.year.to_s }
    month               { '4' }
    payment_gateway_type { 'quickpay' }
  end

  factory :credit_card_blank, class: CreditCard do
    card_type           { '' }
    name                { '' }
    number              { '' }
    verification_value  { '' }
    year                { 3.years.from_now.year.to_s }
    month               { '4' }
  end

  factory :credit_card_invalid, class: CreditCard do
    card_type           { '' }
    name                { '' }
    number              { '123' }
    verification_value  { '' }
    year                { 3.years.from_now.year.to_s }
    month               { '4' }
  end
end
