FactoryBot.define do
  factory :account_product_base, class: Hash do
    created_at { Time.now.to_s :db }
    updated_at { Time.now.to_s :db }
    state_updated_at { Time.now.to_s :db }
    account_id { Account.find_by_subdomain('multiproduct').id }
    initialize_with { attributes }

    factory :support_trial_product do
      name { 'support' }
      state { 'trial' }
      active { true }
      trial_expires_at { 30.days.from_now.to_s :db }
      plan_settings { { max_agents: 5, plan_type: 1 } }
    end

    factory :support_expired_trial_product do
      name { 'support' }
      state { 'expired' }
      active { false }
      trial_expires_at { 30.days.ago.to_s :db }
    end

    factory :support_not_started_product do
      name { 'support' }
      state { 'not_started' }
      active { false }
    end

    factory :support_free_product do
      name { 'support' }
      state { 'free' }
      active { true }
      plan_settings { { max_agents: 5, plan_type: 1 } }
    end

    factory :support_subscribed_product do
      name { 'support' }
      state { 'subscribed' }
      active { true }
      plan_settings { { max_agents: 5, plan_type: 1 } }
    end

    factory :support_inactive_product do
      name { 'support' }
      state { 'inactive' }
      active { false }
    end

    factory :support_cancelled_product do
      name { 'support' }
      state { 'cancelled' }
      active { false }
    end

    factory :support_trial_spp_suite do
      name { 'support' }
      state { 'trial' }
      active { true }
      skus { ['zendesk_suite'] }
      trial_expires_at { 30.days.from_now.to_s :db }
      plan_settings { { max_agents: 5, suite: false } }
    end

    factory :support_trial_spp_non_suite do
      name { 'support' }
      state { 'trial' }
      active { true }
      skus { ['support'] }
      trial_expires_at { 30.days.from_now.to_s :db }
      plan_settings { { max_agents: 5, suite: false } }
    end

    factory :explore_cancelled_product do
      name { 'explore' }
      state { 'cancelled' }
      active { false }
    end

    factory :acme_app_free_product do
      name { 'acme_app' }
      state { 'free' }
      active { true }
    end

    factory :explore_free_product do
      name { 'explore' }
      state { 'free' }
      active { true }
    end

    factory :chat_trial_product do
      name { 'chat' }
      state { 'trial' }
      active { true }
      plan_settings { { max_agents: 100, plan_type: 0 } }
    end

    factory :chat_subscribed_product do
      name { 'chat' }
      state { 'subscribed' }
      active { true }
      plan_settings { { max_agents: 100, plan_type: 5 } }
    end

    factory :chat_inactive_product do
      name { 'chat' }
      state { 'inactive' }
      active { false }
    end

    factory :chat_free_product do
      name { 'chat' }
      state { 'free' }
      active { false }
    end

    factory :answer_bot_trial_product do
      name { 'answer_bot' }
      state { 'trial' }
      active { true }
      plan_settings { { max_resolutions: 100 } }
    end

    factory :outbound_trial_product do
      name { 'outbound' }
      state { 'trial' }
      active { true }
      plan_settings { {} }
    end

    factory :talk_trial_product do
      name { 'talk' }
      state { 'trial' }
      active { true }
      plan_settings { {} }
    end

    factory :sell_free_product do
      name { 'sell' }
      state { 'free' }
      active { true }
    end

    factory :sell_trial_product do
      name { 'sell' }
      state { 'trial' }
      active { true }
      plan_settings { {} }
    end

    factory :sell_trial_inactive_product do
      name { 'sell' }
      state { 'trial' }
      active { false }
      plan_settings { {} }
    end

    factory :chat_trial_phase3_product do
      name { 'chat' }
      state { 'trial' }
      active { true }
      plan_settings { { max_agents: 100, plan_type: 3, phase: 3 } }
    end

    factory :chat_trial_phase4_product do
      name { 'chat' }
      state { 'trial' }
      active { true }
      plan_settings { { max_agents: 100, plan_type: 3, phase: 4 } }
    end
  end
end
