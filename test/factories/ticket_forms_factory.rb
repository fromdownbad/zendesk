FactoryBot.define do
  factory :ticket_form do
    account                 { Account.first }
    sequence(:name)         { |n| "My ticket form#{n}" }
    sequence(:display_name) { |n| "My ticket form#{n}" }
    position           { 1 }
    default            { true }
    active             { true }
    end_user_visible   { true }
  end
end
