FactoryBot.define do
  factory :mobile_sdk_auth do
    name { "My SDK Auth" }
    account { Account.find_by_subdomain("minimum") }
    endpoint { "https://example.com/zendesk/auth" }
    shared_secret { "iruw467boq78wb6ro8q75b6o23857boq623865bq23o764" }
    client { account.oauth_clients.first }
  end
end
