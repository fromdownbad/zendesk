FactoryBot.define do
  factory :plan do
    # empty factory
  end

  factory :plan_free, class: Plan do
    name            { "Free" }
    price           { 0 }
    has_ssl         { 0 }
    max_agents      { 1 }
    max_automations { 4 }
    max_triggers    { 7 }
    max_views       { 11 }
    max_macros      { 400 }
    is_public       { 1 }
    max_end_users   { 20 }
  end

  factory :plan_small, class: Plan do
    name            { "Small" }
    price           { 49 }
    has_ssl         { 0 }
    max_agents      { 2 }
    max_automations { 4 }
    max_triggers    { 7 }
    max_views       { 11 }
    max_macros      { 400 }
    is_public       { 1 }
    max_end_users   { 5000 }
  end

  factory :plan_medium, class: Plan do
    name            { "Medium" }
    price           { 99 }
    has_ssl         { 0 }
    max_agents      { 5 }
    max_automations { 4 }
    max_triggers    { 7 }
    max_views       { 11 }
    max_macros      { 400 }
    is_public       { 1 }
    max_end_users   { 5000 }
  end

  factory :plan_large, class: Plan do
    name            { "Large" }
    price           { 249 }
    has_ssl         { 1 }
    max_agents      { 15 }
    max_automations { 4 }
    max_triggers    { 7 }
    max_views       { 11 }
    max_macros      { 400 }
    is_public       { 1 }
    max_end_users   { 5000 }
  end

  factory :plan_x_large, class: Plan do
    name            { "X-Large" }
    price           { 499 }
    has_ssl         { 1 }
    max_agents      { 35 }
    max_automations { 20 }
    max_triggers    { 20 }
    max_views       { 200 }
    max_macros      { 400 }
    is_public       { 1 }
    max_end_users   { 5000 }
  end
end
