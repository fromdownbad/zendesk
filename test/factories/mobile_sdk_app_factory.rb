FactoryBot.define do
  factory :mobile_sdk_app do
    sequence(:title) { |n| "My SDK App #{n}" }
    sequence(:identifier) { |n| "identifier#{n}abcdef" }
    account { Account.find_by_subdomain("minimum") }
  end
end
