FactoryBot.define do
  factory :fraud_score do
    account_id                    { nil }
    verified_fraud                { false }
    score                         { 0 }
    account_fraud_service_release { "1" }
    is_whitelisted                { 0 }

    other_params do
      {
        owner_email:        'test@test.com',
        subdomain:          'test',
        score_params:       {"free_mail_domain" => false},
        source_event:       Fraud::SourceEvent::TRIAL_SIGNUP
      }
    end
  end
end
