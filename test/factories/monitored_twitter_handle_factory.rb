FactoryBot.define do
  factory :monitored_twitter_handle do
    account { Account.find_by_subdomain("minimum") }
    sequence(:twitter_user_id) { |n| "twitter_user_id#{n}" }
    sequence(:twitter_screen_name) { |n| "twitter_screen_name#{n}" }
    sequence(:access_token) { |n| "access_token#{n}" }
    sequence(:secret) { |n| "secret#{n}" }

    trait :active do
      state { MonitoredTwitterHandle::State::ACTIVE }
    end

    trait :favoriting_enabled do
      favoriting_enabled_at { 1.day.ago }
    end

    trait :dm_enabled do
      direct_messages_enabled_at { 1.day.ago }
    end

    trait :mention_enabled do
      mention_autoconversion_enabled_at { 1.day.ago }
    end
  end
end
