FactoryBot.define do
  factory :zopim_subscription, class: ZBC::Zopim::Subscription do
    association :account
    status      { 'active' }
    plan_type   { 'basic' }
    max_agents  { 5 }
  end
end
