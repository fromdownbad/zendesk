FactoryBot.define do
  factory :zopim_agent, class: ::Zopim::Agent do
    account
    user

    sequence(:email)        { user.email }
    sequence(:display_name) { user.name }

    # NOTE: Aliasing this to `:zopim_identity`, as that's what
    #       `Users::ZopimSupport` names the association
    factory :zopim_identity
  end
end
