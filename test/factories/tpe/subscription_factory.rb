FactoryBot.define do
  factory :tpe_subscription, class: Tpe::Subscription do
    association :account
    active      { true }
    plan_type   { 1 } # 'Partner edition'
    max_agents  { 2 }
  end
end
