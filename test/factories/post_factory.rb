FactoryBot.define do
  sequence(:comment_body) { |n| "Comment #{n}" }

  factory :post do
    account { entry.account }
    forum   { entry.forum }
    entry   { FactoryBot.create(:entry) }

    body    { FactoryBot.generate(:comment_body) }

    user
    created_at { 8.days.ago.to_s(:db) }
    updated_at { 3.days.ago.to_s(:db) }
  end
end
