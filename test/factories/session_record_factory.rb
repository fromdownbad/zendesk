require 'zendesk_shared_session/database_store'

FactoryBot.define do
  factory :session_record, class: 'Zendesk::SharedSession::SessionRecord' do
    account { Account.find_by_subdomain("minimum") }
    user { Account.find_by_subdomain("minimum").owner }

    session_id { SecureRandom.hex(16) }
    expire_at { (Time.now + 1.hour).to_i }
    after(:build) do |s|
      s.data['account'] = s.account_id
      s.data['warden.user.default.key'] = s.user_id
      s.data['auth_via'] = 'manual'
      s.data['auth_authenticated_at'] = Time.now.to_i
      s.data['auth_duration'] = 8.hours.to_i
      s.data['auth_updated_at'] = s.data['auth_authenticated_at'] + 10.minutes
      s.data['auth_password_expired_check'] = s.data['auth_authenticated_at']
      s.data['auth_stored'] = true
      s.data['user_role'] = s.user.roles
      s.data['csrf_token'] = SecureRandom.hex(16)
    end
  end
end
