FactoryBot.define do
  factory :compliance_deletion_status do
    association :account
    association :user

    executer { account.owner }

    trait :skip_validate do
      to_create { |instance| instance.save(validate: false) }
    end
  end
end
