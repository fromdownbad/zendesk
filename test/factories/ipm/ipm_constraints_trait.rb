# Generic IPM constraints to be mixed into factories

FactoryBot.define do
  trait :ipm_constraints do
    internal_name       { 'My Test' }
    pods                { '1, 2' }
    shards              { '100, 200' }
    roles               { ['Owner'] }
    languages           { [1] }
    account_types       { ['trial'] }
    interfaces          { ['lotus'] }
    plans               { [4] }
    account_ids         { [] }
    exclude_account_ids { [] }
    subdomains          { 'browser' }
    exclude_subdomains  { 'support' }
  end
end
