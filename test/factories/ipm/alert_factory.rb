# Alert Factories

FactoryBot.define do
  factory :ipm_alert, class: "Ipm::Alert" do
    text      { "New alert" }
    link_url  { "http://www.zendesk.com/alert" }
    test_mode { false }

    factory :ipm_alert_with_constraints, traits: [:ipm_constraints] do
      custom_factors { 'remote_auth_retired' }
    end
  end
end
