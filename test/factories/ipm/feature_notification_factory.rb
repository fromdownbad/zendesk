# Feature Notification Factories

FactoryBot.define do
  factory :ipm_feature_notification, class: "Ipm::FeatureNotification" do
    title         { "New feature!" }
    body          { "Here's a new feature!" }
    category_code { '1' }
    image_name    { "zendesk_logo-b876e42defe77cccbb36dbd7005f8339.png" }
    test_mode     { false }

    factory :ipm_feature_notification_with_constraints, traits: [:ipm_constraints]
  end
end
