# AddonBoost Factories
FactoryBot.define do
  factory :addon_boost, class: SubscriptionFeatureAddon do
    account { FactoryBot.build :account }
    name               { :data_center }
    boost_expires_at   { Time.zone.now.to_date + 10 }
  end

  factory :addon_boost_nps, class: SubscriptionFeatureAddon do
    account { FactoryBot.build :account }
    name               { :nps }
    boost_expires_at   { Time.zone.now.to_date + 10 }
  end

  factory :addon_boost_high_volume_api, class: SubscriptionFeatureAddon do
    account { FactoryBot.build :account }
    name               { :high_volume_api }
    boost_expires_at   { Time.zone.now.to_date + 10 }
  end
end
