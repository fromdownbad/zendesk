FactoryBot.define do
  factory :satisfaction_reason, class: Satisfaction::Reason do
    association :account
    reason_code { 1000 }
    value { 'Poor performance' }
    system { false }
  end
end
