FactoryBot.define do
  sequence(:email) { |n| "test_#{n}@test.com" }
  sequence(:user_name) { |n| "Test Person #{n}" }

  factory :user do
    name                     { FactoryBot.generate(:user_name) }
    email                    { FactoryBot.generate(:email) }
    time_zone                { 'Copenhagen' }
    is_active                { true }
    skip_verification        { true }
    password                 { "Password123!" }
    association              :account
    skip_password_validation { true }

    factory :agent, parent: :user do
      sequence(:name) { |n| "Support-only Agent #{n}" }
      roles           { Role::AGENT.id }

      factory :contributor do
        sequence(:name) { |n| "Contributor #{n}" }

        permission_set do
          ::PermissionSet.where(
            account_id: account.id,
            name:       I18n.t('txt.default.roles.contributor.name'),
            role_type:  ::PermissionSet::Type::CONTRIBUTOR
          ).first_or_create
        end
      end

      factory :light_agent do
        sequence(:name) { |n| "Light Agent #{n}" }

        permission_set do
          ::PermissionSet.where(
            account_id: account.id,
            name:       'Light Agent',
            role_type:  ::PermissionSet::Type::LIGHT_AGENT
          ).first_or_create
        end
      end

      factory :chat_agent do
        permission_set do
          chat_agent_permission_sets = account.permission_sets.
            where(role_type: ::PermissionSet::Type::CHAT_AGENT)

          chat_agent_permission_sets.first_or_create! do |permission_set|
            permission_set.assign_attributes(
              name:        I18n.t('txt.default.roles.chat_agent.name'),
              description: I18n.t('txt.default.roles.chat_agent.description')
            )

            permission_set.permissions.
              set(PermissionSet::ChatAgent.configuration)
          end
        end

        factory :chat_only_agent do
          sequence(:name) { |n| "Chat-only Agent #{n}" }
        end
      end

      factory :chat_enabled_agent do
        sequence(:name) { |n| "Chat-enabled Agent #{n}" }

        after(:build) do |agent|
          agent.zopim_identity ||= FactoryBot.build(
            :zopim_identity,
            account:            agent.account,
            display_name:       agent.name,
            email:              agent.email,
            user:               agent,
            zopim_subscription: agent.account.zopim_subscription
          )
        end
      end
    end

    factory :verified_admin, parent: :user do
      roles { Role::ADMIN.id }

      factory :unverified_admin, parent: :verified_admin do
        skip_verification { false }
        send_verify_email { true }
      end
    end

    factory :end_user, parent: :user do
      roles { Role::END_USER.id }

      factory :unverified_end_user, parent: :end_user do
        skip_verification { false }
        send_verify_email { true }
      end
    end
  end
end
