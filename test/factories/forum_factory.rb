FactoryBot.define do
  sequence(:forum_name) { |n| "Forum #{n}" }

  factory :forum do
    association :account

    description               { "This is an active, public forum" }
    is_locked                 { 0 }
    visibility_restriction_id { 1 }
    name                      { FactoryBot.generate(:forum_name) }
    display_type_id           { 1 }

    # sorting       "id ASC"
    # entries_count 0
    # organization
    # position      9999
  end
end
