FactoryBot.define do
  factory :subscription do
    factory :trial_subscription do
      max_agents             { 5 }
      billing_cycle_type     { 4 }
      plan_type              { 3 }
      payment_method_type    { PaymentMethodType.CREDIT_CARD }
      pricing_model_revision { ZBC::Zendesk::PricingModelRevision::PATAGONIA }
      trial_expires_on       { 30.days.from_now.to_date }
    end
  end
end
