[Macro, Automation, Trigger, View].each do |klass|
  klass_name = klass.name.underscore
  sequence_name = :"#{klass_name}_name"
  FactoryBot.define do
    sequence(sequence_name) { |n| "#{klass_name} #{n}" }

    factory klass_name do
      # type  klass.name
      association :owner, factory: :account
      title      { FactoryBot.generate(sequence_name) }
      output     { Output.new }
      definition do
        Definition.new.tap do |d|
          d.conditions_all << DefinitionItem.new('status_id', 'is_not', [StatusType.CLOSED]) unless klass == Macro
          d.actions << DefinitionItem.new('status_id', 'is', [StatusType.CLOSED]) unless klass == View
        end
      end
    end
  end
end
