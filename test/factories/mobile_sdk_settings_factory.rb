FactoryBot.define do
  factory :mobile_sdk_settings do
    account { Account.find_by_subdomain("minimum") }
    auth_endpoint { "https://example.com/zendesk/auth" }
    auth_token { "iruw467boq78wb6ro8q75b6o23857boq623865bq23o764" }
  end
end
