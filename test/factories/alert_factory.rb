FactoryBot.define do
  factory :alert do
    value     { "Alert!" }
    is_active { true }
  end
end
