FactoryBot.define do
  factory :agreement, class: Sharing::Agreement do
    uuid { Digest::SHA1.hexdigest(rand.to_s + Time.now.to_s) }
    access_key { |ag| ag.uuid.reverse }
    direction { :in }
    remote_url { 'http://example.com/sharing' }
    created_type { :remote }
    account_id { 1 } # This points to the support account fixture which hard codes id 1
  end
end
