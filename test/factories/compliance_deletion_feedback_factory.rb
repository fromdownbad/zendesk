FactoryBot.define do
  factory :compliance_deletion_feedback do
    association :compliance_deletion_status

    account { compliance_deletion_status.account }
    user    { compliance_deletion_status.user }
    pod_id  { compliance_deletion_status.account.pod_id }

    application { ComplianceDeletionStatus::TALK }
    required    { true }
    state       { ComplianceDeletionFeedback::PENDING }

    trait :talk_complete do
      state { ComplianceDeletionFeedback::COMPLETE }
    end
  end
end
