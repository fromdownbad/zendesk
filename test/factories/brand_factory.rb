FactoryBot.define do
  factory :brand do
    sequence(:name) { |n| "Brand #{n}" }
    account_id { Account.find_by_subdomain("minimum").id }
    route { |brand| FactoryBot.create(:route, account: brand.account) }
    active { true }
  end
end
