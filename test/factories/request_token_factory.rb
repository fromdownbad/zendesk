FactoryBot.define do
  factory :request_token do
    association :account
    association :ticket

    value { 'oneothertoken' }
  end
end
