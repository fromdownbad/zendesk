FactoryBot.define do
  sequence(:category_name) { |n| "category #{n}" }

  factory :category do
    association :account
    name { FactoryBot.generate(:category_name) }
  end
end
