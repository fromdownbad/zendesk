# Coupons Factory
FactoryBot.define do
  sequence(:coupon_name) { |n| "Get your CoupOn (take #{n})" }
  sequence(:trial_code) { |n| "trial_coupon_#{n}" }
  sequence(:discount_code) { |n| "discount_coupon_#{n}" }

  factory :trial_coupon do
    name              { FactoryBot.generate(:coupon_name) }
    coupon_code       { FactoryBot.generate(:trial_code) }
    expiry            { 1.year.from_now }
    effective         { 1.month.from_now }
    forced_inactive   { false }
    length_days       { 365 }
    trial_plan_type   { SubscriptionPlanType.Medium }
    trial_max_agents  { 5 }

    factory :active_trial_coupon do
      effective { 9.days.ago }
    end
  end

  factory :discount_coupon do
    name                        { FactoryBot.generate(:coupon_name) }
    coupon_code                 { FactoryBot.generate(:discount_code) }
    expiry                      { 1.year.from_now }
    effective                   { 1.month.from_now }
    length_days                 { 365 }
    discount_percentage         { 20 }
    minimum_incremental_revenue { 100 }

    factory :active_discount_coupon do
      effective { 4.days.ago }
    end
  end
end
