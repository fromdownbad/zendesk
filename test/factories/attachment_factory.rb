FactoryBot.define do
  factory :attachment do
    uploaded_data { Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/normal_1.jpg") }
  end
end
