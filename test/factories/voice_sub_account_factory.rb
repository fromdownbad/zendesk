FactoryBot.define do
  factory :voice_sub_account, class: Voice::Core::SubAccount do
    sequence(:sid) { |n| "sid#{n}" }
    association :account
    sequence(:auth_token) { |n| "auth_token#{n}" }
    status { "inactive" }
    trait :active do
      status { "active" }
    end
    trait :suspended do
      status { "suspended" }
    end
  end
end
