FactoryBot.define do
  factory :resource_collection_resource do
    association :account

    identifier              { 'a_resource' }
    resource                { nil }
    resource_collection_id  { 999 }
  end
end
