FactoryBot.define do
  factory :event, class: Audit do
    association :account
    association :ticket
    author { account.owner }

    factory :audit, class: Audit do
      via_id { 0 }
      is_public { false }
    end

    factory :voice_comment, class: VoiceComment do
      after(:build) do |vc|
        vc.audit = FactoryBot.create(:audit, account: vc.account, ticket: vc.ticket)
      end
    end
  end
end
