FactoryBot.define do
  factory :ticket do
    association :account

    subject      { "A subject" }
    requester    { account.owner }
    submitter    { account.owner }
    description  { "A description" }

    after(:build) do |ticket|
      ticket.will_be_saved_by ticket.submitter
      ticket.audit.disable_triggers = true
    end
  end
end
