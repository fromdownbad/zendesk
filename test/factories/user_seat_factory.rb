FactoryBot.define do
  factory :user_seat do
    association :account
    association :user, factory: :agent

    factory :voice_user_seat do
      seat_type { 'voice' }
      role { 0 }
    end

    factory :connect_agent_user_seat do
      seat_type { 'connect' }
      role { 1 }
    end
  end
end
