require_relative '../support/test_helper'
require_relative '../support/agent_test_helper'
require_relative '../support/collaboration_settings_test_helper'

SingleCov.covered! uncovered: 20

describe SuspendedTicket do
  include ArturoTestHelper
  include AgentTestHelper

  fixtures :all
  should_serialize :properties

  before do
    stub_request(:put, %r{\.amazonaws.com/data/attachments/.*})
  end

  describe "presence validations" do
    before { SuspendedTicket.any_instance.stubs(account: accounts(:minimum)) }

    should_validate_presence_of :account_id, :content, :from_name, :from_mail, :cause

    it 'ticket with empty html content is valid' do
      suspended_ticket = suspended_tickets(:minimum_unknown_author)
      suspended_ticket.properties = {html: true}
      suspended_ticket.content = '<div></div>'

      suspended_ticket.content.must_equal ''
      assert(suspended_ticket.valid?)
    end
  end

  describe "pre validation truncation" do
    let(:account) { accounts(:minimum) }
    let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
    let(:from_name) { 'John Doe' }
    let(:from_mail) { 'john@example.com' }
    let(:message_id) { 'a_message_id@example.com' }

    let(:suspended_ticket) do
      account.suspended_tickets.new.tap do |suspended_ticket|
        suspended_ticket.from_name = from_name
        suspended_ticket.from_mail = from_mail
        suspended_ticket.message_id = message_id
        suspended_ticket.cause = SuspensionType.SPAM
        suspended_ticket.content = "Content"
      end
    end

    subject { suspended_ticket.valid? }

    it { assert subject }
    it { statsd_client.expects(:increment).never; subject }
    it { statsd_client.expects(:histogram).never; subject }

    describe "#from_name" do
      let(:from_name_limit) { Zendesk::Extensions::TruncateAttributesToLimit.limit(SuspendedTicket.columns_hash.fetch('from_name')) }
      let(:from_name) { 'a' * (from_name_limit + 1) }

      it { subject; assert from_name_limit, suspended_ticket.from_name.length }

      it "logs metrics" do
        statsd_client.expects(:increment).with(:truncated_attribute, tags: ["model:SuspendedTicket", "attr:from_name"]).once
        statsd_client.expects(:histogram).with(:truncated_attribute_length, from_name.size, tags: ["model:SuspendedTicket", "attr:from_name"]).once
        subject
      end
    end

    describe "#from_mail" do
      let(:from_mail_limit) { Zendesk::Extensions::TruncateAttributesToLimit.limit(SuspendedTicket.columns_hash.fetch('from_mail')) }
      let(:from_mail) { 'a' * from_mail_limit + '@example.com' }

      it { subject; assert from_mail_limit, suspended_ticket.from_mail.length }

      it "logs metrics" do
        statsd_client.expects(:increment).with(:truncated_attribute, tags: ["model:SuspendedTicket", "attr:from_mail"]).once
        statsd_client.expects(:histogram).with(:truncated_attribute_length, from_mail.size, tags: ["model:SuspendedTicket", "attr:from_mail"]).once
        subject
      end
    end

    describe "#message_id" do
      let(:message_id_limit) { Zendesk::Extensions::TruncateAttributesToLimit.limit(SuspendedTicket.columns_hash.fetch('message_id')) }
      let(:message_id) { 'a' * message_id_limit + '@example.com' }

      it { subject; assert message_id_limit, suspended_ticket.message_id.length }

      it "logs metrics" do
        statsd_client.expects(:increment).with(:truncated_attribute, tags: ["model:SuspendedTicket", "attr:message_id"]).once
        statsd_client.expects(:histogram).with(:truncated_attribute_length, message_id.size, tags: ["model:SuspendedTicket", "attr:message_id"]).once
        subject
      end
    end
  end

  def recover(suspended_tickets, options = {})
    recoverer = Zendesk::Tickets::Recoverer.new(account: @account, user: @agent)
    recoverer.recover(Array.wrap(suspended_tickets), options)
  end

  it "is not susceptible to mysql coercion exploits" do
    suspended_ticket = suspended_tickets(:minimum_unknown_author)
    assert suspended_ticket.token

    assert_nil SuspendedTicket.find_by_token(0)
  end

  describe "#content" do
    let(:suspended_ticket) { SuspendedTicket.new(content: "CONTENT<h1>not html</h1>", subject: 'SUBJECT') }

    it "returns plain content" do
      suspended_ticket.content.must_equal "CONTENT<h1>not html</h1>"
    end

    describe "with html" do
      before { suspended_ticket.properties = {html: true} }

      it "returns plain" do
        suspended_ticket.content.must_equal "CONTENT\n\nnot html"
      end

      it "caches plain" do
        Zendesk::MtcMpqMigration::Content::HTMLPlainText.expects(:convert).with("CONTENT<h1>not html</h1>").returns("PLAIN")
        suspended_ticket.content.must_equal "PLAIN"
        suspended_ticket.content.must_equal "PLAIN"
      end

      it "expires plain when content is changed" do
        Zendesk::MtcMpqMigration::Content::HTMLPlainText.expects(:convert).with("CONTENT<h1>not html</h1>").returns("PLAIN")
        suspended_ticket.content.must_equal "PLAIN"
        suspended_ticket.content = "CHANGED"
        Zendesk::MtcMpqMigration::Content::HTMLPlainText.unstub(:convert)
        suspended_ticket.content.must_equal "CHANGED"
      end
    end
  end

  describe "#title" do
    let(:suspended_ticket) do
      SuspendedTicket.new(
        account: accounts(:minimum),
        cause: SuspensionType.SPAM,
        from_name: "Lando Rando",
        from_mail: "rando@example.com",
        content: 'CONTENT',
        subject: 'SUBJECT'
      )
    end

    it "truncates subject length" do
      suspended_ticket.subject = "x" * 9999
      assert suspended_ticket.valid?
      assert_equal "x" * 255, suspended_ticket.subject
    end

    it "returns subject" do
      suspended_ticket.title.must_equal "SUBJECT"
    end

    it "removes duplicate whitespace" do
      suspended_ticket.subject = "a\t\n  #{' ' * 300}a"
      suspended_ticket.title.must_equal "a a"
    end

    describe "without subject" do
      before { suspended_ticket.subject = nil }

      it "returns content" do
        suspended_ticket.title.must_equal "CONTENT"
      end

      it "returns truncated content" do
        suspended_ticket.content = 'a' * 300
        suspended_ticket.title.size.must_equal 200
      end

      it "removes duplicate whitespace" do
        suspended_ticket.content = "a\t\n  #{' ' * 300}a"
        suspended_ticket.title.must_equal "a a"
      end
    end
  end

  describe "with a credit card number" do
    before do
      @suspended_ticket = SuspendedTicket.new(
        account: accounts(:minimum),
        cause: SuspensionType.SPAM,
        from_name: "Wombat Taz",
        from_mail: "wombat@taz.com",
        author: users(:minimum_end_user)
      )
      @suspended_ticket.content = 'Hello, my credit card number is 4111 1111 1111 1111'
    end

    describe "while sanitization is enabled" do
      before do
        Account.any_instance.stubs(:has_credit_card_sanitization_enabled?).returns(true)
      end

      describe "when only the subject has a credit card number" do
        before do
          @suspended_ticket.content = "Test"
          @suspended_ticket.subject = 'Hello, my credit card number is 4111 1111 1111 1111'
        end

        it "sanitizes the subject" do
          @suspended_ticket.save!
          @suspended_ticket.reload

          assert_equal "Hello, my credit card number is 4111 11▇▇ ▇▇▇▇ 1111", @suspended_ticket.subject
        end

        it "remembers that it was redacted" do
          @suspended_ticket.save!
          @suspended_ticket.reload

          assert(@suspended_ticket.redacted?)
        end

        it "adds a redaction tag to the ticket when recovered" do
          @suspended_ticket.save!
          ticket = @suspended_ticket.recover(@suspended_ticket.account.owner)

          assert_includes ticket.tag_array, "system_credit_card_redaction"
        end
      end

      describe "when only the content has a credit card number" do
        before do
          @suspended_ticket.content = 'Hello, my credit card number is 4111 1111 1111 1111'
          @suspended_ticket.subject = 'Test'
        end

        it "sanitizes the content" do
          @suspended_ticket.save!
          @suspended_ticket.reload

          assert_equal "Hello, my credit card number is 4111 11▇▇ ▇▇▇▇ 1111", @suspended_ticket.content
        end

        it "remembers that it was redacted" do
          @suspended_ticket.save!
          @suspended_ticket.reload

          assert(@suspended_ticket.redacted?)
        end

        it "adds a redaction tag to the ticket when recovered" do
          @suspended_ticket.save!
          ticket = @suspended_ticket.recover(@suspended_ticket.account.owner)

          assert_includes ticket.tag_array, "system_credit_card_redaction"
        end
      end

      describe "when both the subject and content has a credit card number" do
        before do
          @suspended_ticket.content = 'Hello, my credit card number is 4111 1111 1111 1111'
          @suspended_ticket.subject = 'Hello, my credit card number is 4111 1111 1111 1111'
        end

        it "sanitizes the content" do
          @suspended_ticket.save!
          @suspended_ticket.reload

          assert_equal "Hello, my credit card number is 4111 11▇▇ ▇▇▇▇ 1111", @suspended_ticket.content
        end

        it "sanitizes the subject" do
          @suspended_ticket.save!
          @suspended_ticket.reload

          assert_equal "Hello, my credit card number is 4111 11▇▇ ▇▇▇▇ 1111", @suspended_ticket.subject
        end

        it "remembers that it was redacted" do
          @suspended_ticket.save!
          @suspended_ticket.reload

          assert(@suspended_ticket.redacted?)
        end

        it "adds a redaction tag to the ticket when recovered" do
          @suspended_ticket.save!
          ticket = @suspended_ticket.recover(@suspended_ticket.account.owner)

          assert_includes ticket.tag_array, "system_credit_card_redaction"
        end
      end

      describe "when neither subject nor content have credit card numbers" do
        before do
          @suspended_ticket.content = 'Test'
          @suspended_ticket.subject = 'Test'
        end

        it "does not remember that it was redacted when nothing is sanitized" do
          @suspended_ticket.save!
          @suspended_ticket.reload

          assert_equal false, @suspended_ticket.redacted?
        end
      end
    end

    describe "while sanitization is disabled" do
      before do
        @suspended_ticket.content = "Hello, my credit card number is 4111 1111 1111 1111"
        @suspended_ticket.subject = "Hello, my credit card number is 4111 1111 1111 1111"

        assert_equal false, @suspended_ticket.account.has_credit_card_sanitization?
        @suspended_ticket.save!
        @suspended_ticket.reload
      end

      it "does not sanitize the content" do
        assert_equal "Hello, my credit card number is 4111 1111 1111 1111", @suspended_ticket.content
      end

      it "does not sanitize the subject" do
        assert_equal "Hello, my credit card number is 4111 1111 1111 1111", @suspended_ticket.subject
      end

      it "does not remember that it was redacted" do
        assert_equal false, @suspended_ticket.redacted?
      end
    end
  end

  describe "#deliver_verification_when_signup_required" do
    before do
      @account = accounts(:minimum)
      @suspended_ticket = SuspendedTicket.new(
        account: @account,
        content: "Suspended stuff",
        from_name: "Wombat Taz",
        from_mail: "wombat@taz.com"
      )
    end

    describe "when cause is type SIGNUP_REQUIRED" do
      before do
        @suspended_ticket.cause = SuspensionType.SIGNUP_REQUIRED
      end

      it "delivers verification if user portal state allows it" do
        UsersMailer.expects(:deliver_suspend_verify)
        Zendesk::UserPortalState.any_instance.expects(:send_suspend_verify_email?).returns(true)
        @suspended_ticket.save!
      end

      it "does not deliver verification when triggers are disabled" do
        UsersMailer.expects(:deliver_suspend_verify).never
        @suspended_ticket.properties = {disable_triggers: true}
        @suspended_ticket.save!
      end

      it "does not deliver if verification portal doesn't allow it" do
        UsersMailer.expects(:deliver_suspend_verify).never
        Zendesk::UserPortalState.any_instance.expects(:send_suspend_verify_email?).returns(false)
        @suspended_ticket.save!
      end
    end

    it "does not deliver verification when cause is not type SIGNUP_REQUIRED" do
      UsersMailer.expects(:deliver_suspend_verify).never
      @suspended_ticket.cause = SuspensionType.SPAM
      @suspended_ticket.save!
    end
  end

  describe "#translation_locale" do
    before do
      @account = accounts(:minimum)
      @jp = translation_locales(:japanese)
    end
    it "returns suspended ticket's locale if set" do
      suspended_ticket = SuspendedTicket.new(account: @account, properties: {locale_id: @jp.id })
      assert_equal @jp, suspended_ticket.translation_locale
    end

    it "returns account's locale if suspended ticket locale is not set" do
      suspended_ticket = SuspendedTicket.new(account: @account)
      assert_equal @account.translation_locale, suspended_ticket.translation_locale
    end
  end

  describe "#to_ticket" do
    before do
      account = accounts(:minimum)
      account.stubs(
        field_subject: FieldSubject.new,
        field_ticket_type: FieldTicketType.new
      )
      @date_time_now = Time.zone.now
      @suspended_ticket = SuspendedTicket.new(
        account: account,
        content: "Suspended ticket content",
        subject: "Suspended subject",
        properties: {
          set_tags: "hello there",
          locale_id: 4,
          ticket_form_id: 2,
          brand_id: 1,
          due_date: @date_time_now,
          via_reference_id: ViaType.HELPCENTER
        }
      )
    end

    describe "with a new author" do
      before do
        @suspended_ticket.from_name = "Doctor John"
        @suspended_ticket.from_mail = "dr@john.com"
      end

      it "turns a suspended ticket into a ticket" do
        ticket = @suspended_ticket.to_ticket
        assert_equal ticket.class, Ticket
        assert_equal ticket.requester.class, User
        assert_equal ticket.comment.body, "Suspended ticket content"
        assert_equal ticket.current_tags, "hello there"
        assert_equal ticket.read_attribute(:locale_id), 4
        assert_equal ticket.due_date.to_i, @date_time_now.to_i
        assert_equal ticket.via_reference_id, ViaType.HELPCENTER
        assert_equal 2, ticket.ticket_form_id
        assert_equal 1, ticket.brand_id
        refute ticket.requester.machine?
      end

      it "does not use requester when suspended for being sent to a support address" do
        @suspended_ticket.from_mail = @suspended_ticket.account.reply_address
        assert_nil @suspended_ticket.to_ticket.requester
      end

      describe 'with tags only recorded as custom fields' do
        before do
          field = FactoryBot.create(:field_tagger, account: @suspended_ticket.account)
          @cfo = field.custom_field_options.first
          @suspended_ticket.properties[:fields] ||= {}
          @suspended_ticket.properties[:fields].merge! field.id => @cfo.value
        end

        it 'copies those tags over to the ticket' do
          ticket = @suspended_ticket.to_ticket
          assert_match %r{#{@cfo.value}}i, ticket.current_tags
        end
      end

      describe 'with screencast' do
        before do
          @suspended_ticket.screencasts =
            [
              {
                id: 'test1',
                position: "5",
                url: "http://supporttest.sfssdev.com/embed/test5",
                thumbnail: 'http://pic.com/test1.jpg'
              },
              {
                id: 'test2',
                position: "6",
                url: "http://supporttest.sfssdev.com/embed/test6",
                thumbnail: 'http://pic.com/test2.jpg'
              }
            ]
        end

        it 'has assigned screencasts' do
          assert_equal(@suspended_ticket.screencasts.count, 2)
          assert_equal(@suspended_ticket.screencasts.last[:id], "test2")
        end
      end

      describe "when machine property is true" do
        before do
          @suspended_ticket.properties[:from_zendesk] = true
        end

        it "marks user email identity deliverable state as a machine" do
          ticket = @suspended_ticket.to_ticket
          assert ticket.requester.identities.first.machine?
        end

        it "marks user as a machine" do
          ticket = @suspended_ticket.to_ticket
          assert ticket.requester.machine?
        end
      end
    end

    describe "with an existing author" do
      before do
        @suspended_ticket.author = users(:minimum_end_user)
        @suspended_ticket.from_name = @suspended_ticket.author.name
        @suspended_ticket.from_mail = @suspended_ticket.author.email
      end

      describe "when machine property is true" do
        before do
          @suspended_ticket.properties[:from_zendesk] = true
        end

        it "marks user email identity deliverable state as a machine" do
          ticket = @suspended_ticket.to_ticket
          assert ticket.requester.identities.email.find_by_value(@suspended_ticket.from_mail).machine?
        end
      end
    end
  end

  describe "#full_content" do
    before do
      @suspended_ticket = FactoryBot.build(:suspended_ticket, content: "Test Content", account: accounts(:minimum))
      @field_subject    = FieldSubject.new
      @suspended_ticket.account.stubs(:field_subject).returns(@field_subject)
    end

    it "does not append the subject to the content if the field subject is active and the subject is not blank" do
      @suspended_ticket.subject = "Test Subject"
      @field_subject.is_active  = true
      assert_equal "Test Content", @suspended_ticket.send(:full_content)
    end

    it "does not append the subject to the content if the field subject is not active and the subject is blank" do
      @suspended_ticket.subject = ""
      @field_subject.is_active = false
      assert_equal "Test Content", @suspended_ticket.send(:full_content)
    end

    it "appends the subject to the content if the field subject is not active and the content is not blank" do
      @suspended_ticket.subject = "Test Subject"
      @field_subject.is_active = false
      assert_equal "Test Subject: Test Content", @suspended_ticket.send(:full_content)
    end
  end

  describe "#undelete" do
    before do
      @account = accounts(:minimum)
      4.times { FactoryBot.create(:suspended_ticket, account: @account) }
      @account.reload
    end

    it "undeletes suspended tickets within the given range" do
      count = @account.suspended_tickets.count(:all)
      assert count != 0

      assert(@account.suspended_tickets.first.update_columns(deleted: 1, updated_at: 1.year.ago))
      assert_equal @account.suspended_tickets.count(:all), count - 1

      SuspendedTicket.undelete(@account, 6.months.ago.to_date)
      assert_equal @account.suspended_tickets.count(:all), count - 1

      SuspendedTicket.undelete(@account, 2.years.ago.to_date)
      assert_equal @account.suspended_tickets.count(:all), count
    end
  end

  describe '#set_error_messages' do
    before do
      @account = accounts(:minimum)
      @author = users(:minimum_end_user)
      @ticket_field = ticket_fields(:field_tagger_custom)
      @ticket_field_error = Zendesk::TicketFieldErrors::Error.new(@ticket_field, "Error message")
      @suspended_ticket = FactoryBot.create(:suspended_ticket,
        account: @account,
        author: @author,
        client: "IP address: 255.255.255.255")
    end

    it "adds the error message as a String" do
      @suspended_ticket.set_error_messages([@ticket_field_error])

      assert @suspended_ticket.get_error_messages.last.is_a? String
      assert_equal @ticket_field_error, @suspended_ticket.get_error_messages.last
    end
  end

  describe "#recover" do
    before do
      @account = accounts(:minimum)
      @agent = users(:minimum_agent)
      @author = users(:minimum_end_user)
      @attachment = FactoryBot.create(:attachment, account: @account)
      @screencast = {
        id: 'test1',
        position: "5",
        url: "http://supporttest.sfssdev.com/embed/test5",
        thumbnail: 'http://pic.com/test1.jpg'
      }

      @suspended_ticket = FactoryBot.create(:suspended_ticket,
        account: @account,
        author: @author,
        attachments: [@attachment],
        client: "IP address: 255.255.255.255")

      @suspended_ticket.screencasts = [@screencast]

      @account.field_subject.update_attribute(:is_active, true)
    end

    it "marks the corresponding email as ham" do
      message_id = '12345@example.com'
      @inbound = InboundEmail.create!(account: @account, from: "whatever@example.com", message_id: message_id)
      refute @inbound.ham?
      @suspended_ticket.message_id = message_id
      @suspended_ticket.recover(@agent, @author)
      assert @inbound.reload.ham?
    end

    describe "with unrecoverable ticket, SuspensionType.SPAM" do
      before do
        @suspended_ticket = suspended_tickets(:minimum_spam)
        @user = users(:minimum_end_user)
        @account = accounts(:minimum)
        @ticket = tickets(:minimum_1)

        SuspendedTicket.any_instance.stubs(:account).returns(@account)
        SuspendedTicket.any_instance.stubs(:cause).returns(SuspensionType.SPAM)
        SuspendedTicket.any_instance.stubs(:ticket).returns(@ticket)

        ticket_field = ticket_fields(:support_field_description)
        @account.ticket_fields << ticket_field
        @account.save!
        @account.reload

        @suspended_ticket.recover(@user)
        assert_equal @account, @suspended_ticket.account
      end

      it "does not recover ticket, populate error_messages with useful information" do
        assert_nil @suspended_ticket.recover(@user)
        assert_equal "Suspended ticket was already recovered.", @suspended_ticket.get_error_messages[0]
      end
    end

    describe "with known author and new ticket" do
      before { @recovered = @suspended_ticket.recover(@agent, @author) }
      should_create :ticket
      should_not_change("The number of Users") { User.count(:all) }
      it "creates a ticket with correct values" do
        assert_equal @suspended_ticket.subject, @recovered.subject
        assert_equal @suspended_ticket.send(:full_content), @recovered.description
        assert_equal @agent, @recovered.submitter
        assert_equal @suspended_ticket.author, @recovered.requester
        assert_equal_with_nil @suspended_ticket.recipient, @recovered.recipient
        assert_equal @suspended_ticket.client, @recovered.client
        assert_equal @attachment.id, @recovered.comments.first.attachments.first.id
        assert_equal "test1", @recovered.comments.first.screencasts.first.id
      end

      it "maintains original via_id" do
        assert_equal @suspended_ticket.via_id, @recovered.via_id
      end
    end

    describe "when setting the audit author and comment author when recovering as a specific agent (recover_as_this_user)" do
      let(:account) { accounts(:minimum) }
      let(:agent_with_comment_permission) { users(:minimum_admin) }
      let(:suspended_ticket) do
        account.suspended_tickets.create!(
          subject: "I am a subject!",
          recipient: account.recipient_addresses.first,
          cause: SuspensionType.SPAM,
          via_id: ViaType.MAIL,
          from_mail: "new.user@example.com",
          from_name: "New User",
          content: "I am a content!"
        )
      end

      describe "when recovery results in a new ticket being created" do
        describe_with_and_without_arturo_setting_enabled :comment_email_ccs_allowed do
          it "sets the comment author and the audit author to be the same user" do
            ticket = suspended_ticket.recover(agent_with_comment_permission, agent_with_comment_permission)

            assert_equal ticket.comments.last.author, ticket.audits.last.author
          end
        end
      end

      describe "when recovery results in a new comment on an existing ticket" do
        let(:ticket) { tickets(:minimum_1) }

        before do
          suspended_ticket.ticket = ticket
          suspended_ticket.save!
        end

        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it "sets the comment author and the audit author to be the same user" do
            ticket = suspended_ticket.recover(agent_with_comment_permission, agent_with_comment_permission)

            assert_equal ticket.comments.last.author, ticket.audits.last.author
          end
        end

        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          it "sets the comment author and the audit author to be the same user" do
            ticket = suspended_ticket.recover(agent_with_comment_permission, agent_with_comment_permission)

            assert_equal ticket.comments.last.author, ticket.audits.last.author
          end
        end
      end
    end

    describe "with custom ticket fields and editable ticket system properties" do
      before do
        @suspended_ticket.properties = { fields: { ticket_fields(:field_textarea_custom).id => "Mjallo" },
                                         priority_id: PriorityType.HIGH }
        @suspended_ticket.recover(@agent, @author)
      end

      it "recovers fields also" do
        assert_equal Ticket.last.ticket_field_entries.first.value, "Mjallo"
        assert_equal Ticket.last.priority_id, PriorityType.HIGH
      end
    end

    describe "with via_reference_id properties" do
      before do
        @suspended_ticket.properties = { via_reference_id: ViaType.HELPCENTER }

        @suspended_ticket.recover(@agent, @author)
      end

      it "recovers via_reference_id also" do
        assert_equal ViaType.HELPCENTER, Ticket.last.via_reference_id
      end
    end

    describe "with flags" do
      before do
        @suspended_ticket.properties = HashWithIndifferentAccess.new(
          flags: AuditFlags.new([EventFlagType.OTHER_USER_UPDATE, EventFlagType.ATTACHMENT_TOO_BIG]),
          flags_options: {
            EventFlagType.ATTACHMENT_TOO_BIG => {
              message: {
                file: "big_file.txt",
                account_limit: 4096
              }
            }
          }
        )
        @suspended_ticket.recover(@agent, @author)
      end

      it "transfers flags to Ticket audit" do
        assert_equal Ticket.last.audits.first.flags, @suspended_ticket.properties[:flags]
      end

      it "transfers flags options to Ticket audit" do
        # FIXME: these are both nil
        assert_equal_with_nil Ticket.last.audits.first.flags_options[EventFlagType.ATTACHMENT_TOO_BIG], @suspended_ticket.properties[EventFlagType.ATTACHMENT_TOO_BIG]
      end

      it "Ticket audit is not trusted" do
        refute Ticket.last.audits.first.trusted?
      end
    end

    describe "with mail metadata" do
      before do
        @metadata = {
          message_id: "12345@example.com",
          ip_address: "127.0.0.1",
          raw_email_identifier: '1/not_spam.eml'
        }
        @suspended_ticket.properties = { metadata: @metadata }
      end

      it "successfully recovers the ticket" do
        ticket = @suspended_ticket.recover(@agent, @author)
        assert_equal false, ticket.new_record?
      end

      it "transfers the metadata to the audit" do
        ticket = @suspended_ticket.recover(@agent, @author)
        audit  = ticket.audits.first

        assert_equal @metadata.with_indifferent_access, audit.metadata[:system]
        assert_equal "Message ID: 12345@example.com\nIP address: 127.0.0.1", audit.client
      end

      describe "with redactions" do
        before do
          @suspended_ticket.properties[:redacted] = true
        end

        it "marks as redacted" do
          ticket = @suspended_ticket.recover(@agent, @author)
          assert ticket.redactions?
          assert ticket.comments.last.redaction_event
        end
      end

      describe "with CCs in metadata" do
        describe "when recovering the suspended ticket creates a new ticket" do
          before do
            # This is necessary, because the minimum account in the fixtures
            # inexplicably has a domain_blacklist value of '*'
            Account.any_instance.stubs(allows?: true)

            @suspended_ticket.metadata[:ccs] = [
              { name: "Roger De Vlaeminck", address: "roger.de.vlaeminck@brooklyn.it" },
              { name: "Sean Kelly", address: "sean.kelly@eurosport.eu" },
              { name: "Eddy Merckx", address: "eddy.merckx@merckxcycles.be" },
              { name: "Bernard Hinault", address: "bernard.hinault@blaireau.fr" },
              { name: "Johan Museeuw", address: "johan.museeuw@mapei.be" }
            ]
          end

          describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
            it "adds CCs from metadata as collaborators to the recovered ticket" do
              ticket = @suspended_ticket.recover(@agent)
              assert_equal 5, ticket.collaborators.count
            end
          end

          describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
            before do
              Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true)
              Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true)
            end

            describe "with end user CCs in metadata" do
              it "adds CCs from metadata as email CCs to the recovered ticket" do
                suspended_ticket_ccs_addresses = @suspended_ticket.metadata[:ccs].map { |cc| cc[:address] }
                recovered_email_ccs_addresses = @suspended_ticket.recover(@agent).email_ccs.map(&:email)

                assert_equal suspended_ticket_ccs_addresses.sort, recovered_email_ccs_addresses.sort
              end
            end

            describe "with agent CCs in metadata" do
              before do
                @suspended_ticket.metadata[:ccs] = [
                  { name: users(:minimum_agent).name, address: users(:minimum_agent).email },
                  { name: users(:minimum_admin).name, address: users(:minimum_admin).email }
                ]
              end

              describe_with_arturo_setting_enabled :agent_email_ccs_become_followers do
                it "adds the agent CCs from metadata as email CCs and followers to the recovered ticket" do
                  suspended_ticket_ccs_addresses = @suspended_ticket.metadata[:ccs].map { |cc| cc[:address] }
                  ticket = @suspended_ticket.recover(@agent)
                  recovered_email_ccs_addresses = ticket.email_ccs.map(&:email)
                  recovered_followers_addresses = ticket.followers.map(&:email)

                  assert_equal suspended_ticket_ccs_addresses.sort, recovered_email_ccs_addresses.sort
                  assert_equal suspended_ticket_ccs_addresses.sort, recovered_followers_addresses.sort
                end
              end

              describe_with_arturo_setting_disabled :agent_email_ccs_become_followers do
                it "adds the agent CCs from metadata as email CCs to the recovered ticket but does not add them as followers" do
                  suspended_ticket_ccs_addresses = @suspended_ticket.metadata[:ccs].map { |cc| cc[:address] }
                  ticket = @suspended_ticket.recover(@agent)
                  recovered_email_ccs_addresses = ticket.email_ccs.map(&:email)
                  recovered_followers = ticket.followers

                  assert_equal suspended_ticket_ccs_addresses.sort, recovered_email_ccs_addresses.sort
                  assert_empty recovered_followers
                end
              end
            end
          end

          describe_with_and_without_arturo_setting_enabled :follower_and_email_cc_collaborations do
            it "does not add the suspended ticket metadata's CCs to the recovered ticket's audit metadata" do
              ticket = @suspended_ticket.recover(@agent)

              assert ticket.audits.none? { |audit| audit.metadata[:system].key?("ccs") }
            end
          end
        end
      end

      describe "without redactions" do
        it "does not mark as redacted" do
          ticket = @suspended_ticket.recover(@agent, @author)
          refute ticket.redactions?
          refute ticket.comments.last.redaction_event
        end
      end

      ["SPAM", "MALICIOUS_PATTERN_DETECTED"].each do |cause|
        describe "mail misclassified as spam due to #{cause}" do
          before do
            @suspended_ticket.cause  = SuspensionType.send(cause)
            @suspended_ticket.via_id = ViaType.MAIL
            @suspended_ticket.recipient = 'test@support.zendesk.com'
          end

          it "reports feedback to Cloudmark via mail and Rspamd" do
            CloudmarkFeedbackJob.expects(:report_ham)
            RspamdFeedbackJob.expects(:report_ham)
            @suspended_ticket.recover(@agent, @author)
          end

          describe "when in the Rails development environment" do
            before { Rails.env.stubs(:development?).returns(true) }

            it "does not report feedback to Cloudmark or Rspamd" do
              CloudmarkFeedbackJob.expects(:report_ham).never
              RspamdFeedbackJob.expects(:report_ham).never
              @suspended_ticket.recover(@agent, @author)
            end
          end

          describe_with_arturo_disabled :email_rspamd_training_enabled do
            it "does not report feedback to Rspamd via http if arturo disabled" do
              RspamdFeedbackJob.expects(:report_ham).never
              @suspended_ticket.recover(@agent, @author)
            end
          end
        end
      end

      describe "mail not misclassified as spam" do
        before do
          @suspended_ticket.cause  = SuspensionType.LOOP
          @suspended_ticket.via_id = ViaType.MAIL
        end

        it "does not attempt to report to Cloudmark and Rspamd" do
          CloudmarkFeedbackJob.expects(:report_ham).never
          RspamdFeedbackJob.expects(:report_ham).never
          @suspended_ticket.recover(@agent, @author)
        end
      end

      describe "misclassified as spam, but not an email" do
        before do
          @suspended_ticket.cause    = SuspensionType.SPAM
          @suspended_ticket.via_id   = ViaType.WEB_FORM
          @suspended_ticket.metadata = nil
        end

        it "does not attempt to report to Cloudmark and Rspamd" do
          CloudmarkFeedbackJob.expects(:report_ham).never
          RspamdFeedbackJob.expects(:report_ham).never
          @suspended_ticket.recover(@agent, @author)
        end
      end
    end

    describe "with locale set on suspended ticket" do
      before do
        @suspended_ticket.properties = {locale_id: 4}
        User.any_instance.stubs(:organization_id).returns(organizations(:minimum_organization1).id)
      end

      it "sets author's locale to suspended ticket's locale if author has a nil locale" do
        assert @author.locale_id.nil?
        @suspended_ticket.recover(@agent, @author)
        @author.reload
        # assert_equal 4, @author.locale_id
      end

      it "does not set author's locale if they already have one" do
        @author.locale_id = 2
        @author.save
        @author.reload
        # assert_equal 2, @author.locale_id

        @suspended_ticket.recover(@agent, @author)
        # assert_equal 2, @author.locale_id
      end

      it "does not break if author is nil" do
        @author = nil
        @suspended_ticket.recover(@agent)
      end
    end

    describe "with unknown author and a suspension type not of SIGNUP_REQUIRED" do
      before do
        @suspended_ticket.cause = SuspensionType.SPAM
        @suspended_ticket.author = nil
        @suspended_ticket.from_name = 'Unknown'
        @suspended_ticket.from_mail = 'someone@example.com'
        @suspended_ticket.save!
        recover(@suspended_ticket)
      end
      should_create :ticket
      should_create :user
      it "creates a user with correct values" do
        user = User.last
        assert_equal @suspended_ticket.from_name, user.name
        assert_equal @suspended_ticket.from_mail, user.email
      end
    end

    describe "with unknown author and existing ticket, recovering as a specific user" do
      before do
        @suspended_ticket.update_attribute(:author_id, nil)
        @suspended_ticket.update_attribute(:ticket, tickets(:minimum_1))
        @suspended_ticket.update_attribute(:from_mail, 'recover@example.com')
      end

      it "adds the ticket requester's email to the specified user's identities" do
        assert_difference '@author.identities.count(:all)' do
          recover(@suspended_ticket, recover_as_user: @author)
        end
        assert @author.identities.find_by_value(@suspended_ticket.from_mail)
      end

      it "verifys the specified user's identity" do
        recover(@suspended_ticket, recover_as_user: @author)
        assert(@author.identities.find_by_value(@suspended_ticket.from_mail).is_verified)
      end

      it "does not create a new author" do
        refute_difference 'User.count(:all)' do
          @suspended_ticket.recover(@agent, @author)
        end
      end

      it "has the specified user as the comment author" do
        @suspended_ticket.recover(@agent, @author)
        assert_equal @author, @suspended_ticket.ticket.comments.last.author
      end

      it "provides the recovered parent ticket" do
        assert_equal @suspended_ticket.ticket, @suspended_ticket.recover(@agent, @author)
      end

      it "recovers the ticket as a comment" do
        @ticket = @suspended_ticket.ticket
        assert_difference '@ticket.comments.count(:all)' do
          @suspended_ticket.recover(@agent, @author)
        end
      end

      it "sets the newly added ticket comment and audit via_id as mail" do
        @suspended_ticket.recover(@agent, @author)
        assert_equal ViaType.MAIL, @suspended_ticket.ticket.comments.last.via_id
        assert_equal ViaType.MAIL, @suspended_ticket.ticket.audits.last.via_id
      end
    end

    describe "querying on update_with_unknown_author?" do
      before do
        @suspended_ticket = SuspendedTicket.new(author: @author, ticket: @account.tickets.first)
      end

      it "returns false if suspended ticket has an author and a ticket" do
        assert_equal false, @suspended_ticket.update_with_unknown_author?
      end

      it "returns false if suspended ticket has an author and no ticket" do
        @suspended_ticket.ticket = nil
        assert_equal false, @suspended_ticket.update_with_unknown_author?
      end

      it "returns false if suspended ticket has neither an author nor a ticket" do
        @suspended_ticket.author = nil
        @suspended_ticket.ticket = nil
        assert_equal false, @suspended_ticket.update_with_unknown_author?
      end

      it "returns true if suspended ticket has a ticket but no author" do
        @suspended_ticket.author = nil
        assert(@suspended_ticket.update_with_unknown_author?)
      end
    end

    describe "recover as users" do
      before do
        @suspended_ticket = FactoryBot.create(:suspended_ticket, account: @account, author: @author, ticket: @account.tickets.first, cause: SuspensionType.UNKNOWN_AUTHOR)
      end

      it "includes the original ticket's requester" do
        @suspended_ticket.ticket.requester = @author

        assert @suspended_ticket.potential_recover_as_users.include?(@author),
          @suspended_ticket.potential_recover_as_users.inspect
      end

      it "includes the original ticket's collaborators" do
        non_agent_collaborator = users(:minimum_end_user)
        @suspended_ticket.ticket.collaborators = [non_agent_collaborator]

        assert @suspended_ticket.potential_recover_as_users.include?(non_agent_collaborator),
          @suspended_ticket.potential_recover_as_users.inspect
      end

      it "does not include agent requesters" do
        @suspended_ticket.ticket.requester = @agent

        refute @suspended_ticket.potential_recover_as_users.include?(@agent),
          @suspended_ticket.potential_recover_as_users.inspect
      end

      it "does not include agent collaborators" do
        @suspended_ticket.ticket.collaborations.build(user: @agent)
        assert_equal false, @suspended_ticket.potential_recover_as_users.include?(@agent)
      end

      it "does not choke on suspended tickets without tickets" do
        @suspended_ticket.ticket = nil

        @suspended_ticket.potential_recover_as_users
      end

      describe "with a deleted suspended_ticket (simulating a race condition)" do
        before do
          SuspendedTicket.delete(@suspended_ticket.id)
        end

        should_not_change("The number of tickets") { Ticket.all.size }
        it "Nots attempt to delete again" do
          SuspendedTicket.expects(:delete).never
        end
      end
    end

    describe "with unknown author and suspension cause is SIGNUP_REQUIRED" do
      before do
        Zendesk::UserPortalState.any_instance.stubs(:send_suspend_verify_email?).returns(true)
        @suspended_ticket.author = nil
        @suspended_ticket.from_name = 'Unknown'
        @suspended_ticket.from_mail = 'someone@example.com'
        @suspended_ticket.cause = SuspensionType.SIGNUP_REQUIRED
        @account.update_attribute(:is_welcome_email_when_agent_register_enabled, true)
        UsersMailer.expects(:deliver_verify).never
        recover(@suspended_ticket)
      end
      should_create :user
    end

    describe "with unknown author and welcome mail is disabled" do
      before do
        Zendesk::UserPortalState.any_instance.stubs(:send_suspend_verify_email?).returns(true)
        @suspended_ticket.author = nil
        @suspended_ticket.from_name = 'Unknown'
        @suspended_ticket.from_mail = 'someone@example.com'
        @suspended_ticket.cause     = SuspensionType.SPAM
        @account.update_attribute(:is_welcome_email_when_agent_register_enabled, false)
        UsersMailer.expects(:deliver_verify).never
        recover(@suspended_ticket)
      end
      should_create :user
    end

    describe "with field_subject disabled" do
      before do
        @account.field_subject.update_attribute(:is_active, false)
        @suspended_ticket.recover(@agent, @author)
      end
      should_create :ticket
      it "creates a ticket with blank subject" do
        ticket = Ticket.last
        assert ticket.subject.blank?
        assert_equal @suspended_ticket.send(:full_content), ticket.description
      end
    end

    # This is necessary for now, until suspension recovery handles new CCs & Followers scenarios. Using a light agent to
    # recover a ticket that has ccs _should_ not be permitted, unless we decide a flag of some type is necessary to override
    describe_with_arturo_disabled :email_restricted_agent_fix do
      describe 'with existing ticket' do
        let(:recover_ticket) { recover(@suspended_ticket, recover_as_user: user) }
        let(:ticket)         { tickets(:minimum_1) }
        let(:last_comment)   { ticket.comments.last }
        let(:user)           { create_light_agent }
        let(:ccs)            { [{address: ticket.requester.email}] }

        before do
          @suspended_ticket.update_attribute(:cause, 20)
          @suspended_ticket.stubs(:ticket).returns(ticket)
          @suspended_ticket.properties = {metadata: {ccs: ccs}}
        end

        should_not_change('The number of Tickets') { Ticket.count(:all) }

        it 'recovers the correct ticket' do
          assert_equal ticket.id, recover_ticket.first.id
        end

        it 'makes a new comment' do
          recover_ticket

          assert_equal @suspended_ticket.send(:full_content), last_comment.body
        end

        it 'adds the correct author' do
          recover_ticket

          assert_equal @suspended_ticket.author, last_comment.author
        end

        it 'sets the correct client' do
          recover_ticket

          assert_equal @suspended_ticket.client, last_comment.client
        end

        describe 'and author does not have permission to post public comments' do
          before { recover_ticket }

          it 'makes a private comment' do
            assert ticket.comments.last.is_private?
          end
        end

        describe 'and the requester is not present of suspended email' do
          let(:user) { users(:minimum_end_user) }
          let(:ccs)  { [{address: user.email}] }

          before { Ticket.any_instance.stubs(is_participant?: true) }

          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            before { recover_ticket }

            it 'makes a private comment' do
              assert ticket.comments.last.is_private?
            end
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            before { recover_ticket }

            it 'makes a public comment' do
              assert ticket.comments.last.is_public?
            end
          end
        end

        describe 'when the requester is the end user who sends the email' do
          let(:user) { users(:minimum_end_user) }
          let(:ccs) { [{address: users(:minimum_agent).email}] }
          let(:from_mail) { ticket.requester.email }

          before do
            ticket.requester = user

            ticket.will_be_saved_by(user)
            ticket.save!

            Ticket.any_instance.stubs(is_participant?: true)
            @suspended_ticket.update_attribute(:from_mail, from_mail)
            @suspended_ticket.stubs(:ticket).returns(ticket)
            @suspended_ticket.properties = {metadata: {ccs: ccs}}
          end

          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            before { recover_ticket }

            it 'makes a public comment' do
              assert ticket.comments.last.is_public?
            end
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            before { recover_ticket }

            it 'makes a public comment' do
              assert ticket.comments.last.is_public?
            end
          end
        end

        describe 'when the requester is an agent who sends the email' do
          let(:user) { users(:minimum_agent) }
          let(:ccs) { [{address: users(:minimum_admin).email}] }
          let(:from_mail) { ticket.requester.email }

          before do
            ticket.requester = user

            ticket.will_be_saved_by(user)
            ticket.save!

            Ticket.any_instance.stubs(is_participant?: true)
            @suspended_ticket.update_attribute(:from_mail, from_mail)
            @suspended_ticket.stubs(:ticket).returns(ticket)
          end

          describe_with_and_without_arturo_setting_enabled :comment_email_ccs_allowed do
            describe 'with is_email_comment_public_by_default set to false' do
              before do
                Account.any_instance.stubs(is_email_comment_public_by_default: false)
                recover_ticket
              end

              it 'makes a private comment' do
                assert ticket.comments.last.is_private?
              end
            end

            describe 'with is_email_comment_public_by_default set to true' do
              before do
                Account.any_instance.stubs(is_email_comment_public_by_default: true)
                recover_ticket
              end
            end
          end
        end

        describe 'and the author is an end user' do
          let(:user) { users(:minimum_end_user) }

          describe 'and is not a participant on the ticket' do
            before do
              Ticket.any_instance.stubs(is_follower?: false)
            end

            it 'makes a private comment' do
              recover_ticket

              assert ticket.comments.last.is_private?
            end

            it 'flags the comment with OTHER_USER_UPDATE' do
              recover_ticket

              ticket.comments.last.audit.flags.must_equal(
                AuditFlags.new([EventFlagType.OTHER_USER_UPDATE])
              )
            end

            describe_with_arturo_disabled :email_make_other_user_comments_private do
              before { recover_ticket }

              it 'makes a public comment' do
                assert ticket.comments.last.is_public?
              end
            end
          end

          describe 'and is a participant on the ticket' do
            let(:ccs) { [{address: user.email}] }

            describe 'as a requester' do
              before do
                ticket.requester = user

                ticket.will_be_saved_by(user)
                ticket.save!

                recover_ticket
              end

              it 'makes a public comment' do
                assert ticket.comments.last.is_public?
              end
            end

            describe 'as an email CC' do
              let(:ccs) do
                [
                  {address: ticket.requester.email},
                  {address: user.email}
                ]
              end

              before do
                Ticket.any_instance.stubs(is_follower?: false)
                Ticket.any_instance.stubs(is_email_cc?: true)

                recover_ticket
              end

              it 'does not flag the comment' do
                assert_empty(ticket.comments.last.audit.flags)
              end
            end

            # See app/models/ticket/collaborating.rb for what it means to be a participant
            describe 'as an email participant' do
              let(:ccs) do
                [
                  {address: ticket.requester.email},
                  {address: user.email}
                ]
              end

              before do
                Ticket.any_instance.stubs(is_participant?: true)

                recover_ticket
              end

              it 'makes a public comment' do
                assert ticket.comments.last.is_public?
              end
            end

            describe_with_arturo_disabled :email_make_other_user_comments_private do
              let(:ccs) { [{address: ticket.requester.email}] }

              before { recover_ticket }

              it 'makes a public comment' do
                assert ticket.comments.last.is_public?
              end
            end
          end
        end

        describe 'and the metadata contains CCs' do
          let(:ccs)      { [] }
          let(:user)     { users(:minimum_search_user) }
          let(:end_user) { users(:minimum_end_user) }

          before do
            SuspendedTicket.any_instance.stubs(metadata: {ccs: ccs})
          end

          describe 'and the CCs/Followers settings are enabled' do
            before do
              CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
            end

            describe 'and the requester of existing ticket is present on suspended email' do
              describe 'and the author of suspended email is a CC on existing ticket' do
                let(:ccs) do
                  [
                    {address: ticket.requester.email},
                    {address: user.email},
                    {address: end_user.email}
                  ]
                end

                before do
                  ticket.collaborations.create!(
                    collaborator_type: CollaboratorType.EMAIL_CC,
                    user: user
                  )

                  recover_ticket
                end

                it 'adds all the CCs from suspended ticket to existing ticket' do
                  [@suspended_ticket.author, end_user].each do |user|
                    assert ticket.collaborators.include?(user)
                  end
                end
              end

              describe 'and the author of suspended email is not a CC on existing ticket' do
                let(:ccs) do
                  [
                    {address: ticket.requester.email},
                    {address: end_user.email}
                  ]
                end

                describe_with_arturo_disabled :email_make_other_user_comments_private do
                  before { recover_ticket }

                  it 'adds only the author as CC to existing ticket' do
                    assert ticket.collaborators.include?(@suspended_ticket.author)
                  end

                  it 'does not add CCs on suspended ticket to existing ticket' do
                    refute ticket.collaborators.include?(end_user)
                  end
                end

                describe_with_arturo_enabled :email_make_other_user_comments_private do
                  before { recover_ticket }

                  it 'does not add any new CCs to existing ticket' do
                    [@suspended_ticket.author, end_user].each do |user|
                      refute ticket.collaborators.include?(user)
                    end
                  end
                end
              end
            end

            describe 'and the requester of existing ticket is not present on suspended email' do
              let(:ccs) do
                [
                  {address: user.email},
                  {address: end_user.email}
                ]
              end

              describe_with_arturo_disabled :email_make_other_user_comments_private do
                before { recover_ticket }

                it 'adds only the author as CC to existing ticket' do
                  assert ticket.collaborators.include?(@suspended_ticket.author)
                end

                it 'does not add CCs on suspended ticket to existing ticket' do
                  refute ticket.collaborators.include?(end_user)
                end
              end

              describe_with_arturo_enabled :email_make_other_user_comments_private do
                before { recover_ticket }

                it 'does not add any new CCs to existing ticket' do
                  [@suspended_ticket.author, end_user].each do |user|
                    refute ticket.collaborators.include?(user)
                  end
                end
              end
            end
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            let(:ccs) do
              [
                {address: ticket.requester.email},
                {address: user.email},
                {address: end_user.email}
              ]
            end

            describe_with_arturo_setting_enabled :collaboration_enabled do
              before { recover_ticket }

              it 'adds only the author as CC to existing ticket' do
                assert ticket.collaborators.include?(@suspended_ticket.author)
              end

              it 'does not add CCs on suspended ticket to existing ticket' do
                refute ticket.collaborators.include?(end_user)
              end
            end

            describe_with_arturo_setting_disabled :collaboration_enabled do
              before { recover_ticket }

              it 'does not add any new CCs to existing ticket' do
                [@suspended_ticket.author, end_user].each do |user|
                  refute ticket.collaborators.include?(user)
                end
              end
            end
          end
        end
      end
    end

    describe "when author is nil and ticket exists" do
      before do
        @suspended_ticket.ticket = Ticket.working.last
        @suspended_ticket.author = nil
        @suspended_ticket.recover(@agent, nil)
      end

      it "set original author as suspended_ticket author" do
        assert_equal @suspended_ticket.author.email, @suspended_ticket.from_mail
      end
    end

    describe "with existing branded ticket" do
      before do
        @brand1 = FactoryBot.create(:brand)
        @brand2 = FactoryBot.create(:brand)
        @ticket = tickets(:minimum_1)
        @ticket.brand = @brand1
        @ticket.will_be_saved_by(@agent)
        @ticket.save
        @suspended_ticket.properties[:brand_id] = @brand2.id
        @suspended_ticket.ticket_id = @ticket.nice_id
        @suspended_ticket.recover(@agent, @author)
      end
      should_create :comment
      it "does not alter the existing ticket's brand" do
        assert_equal @brand1, @ticket.reload.brand
      end
    end

    describe "with private comment from agent" do
      before do
        @suspended_ticket.author = @agent
        @suspended_ticket.ticket_id = tickets(:minimum_1).nice_id
        @account.update_attribute(:is_email_comment_public_by_default, false)

        @suspended_ticket.recover(@agent, @agent)
      end
      should_create :comment
      it "creates as a private comment" do
        comment = Comment.last
        assert_equal false, comment.is_public
      end
    end

    describe "with from_mail equals to account reply address" do
      before do
        @suspended_ticket.author = nil
        @suspended_ticket.from_mail = @account.reply_address
        @suspended_ticket.recover(@agent, @author)
      end
      should_not_change("The number of Tickets") { Ticket.count(:all) }
      should_not_change("The number of Users") { User.count(:all) }
      should_not_change("The number of Comments") { Comment.count(:all) }
    end

    describe "with closed ticket" do
      before do
        @suspended_ticket.ticket_id = tickets(:minimum_5).nice_id
        @suspended_ticket.recover(@agent, @author)
      end
      should_create :ticket
    end

    describe "with agent_comment" do
      let(:admin) { users(:minimum_admin) }
      let(:comments) { Comment.last(2).map { |c| [c.author.name, c.is_public?, c.body] } }

      before do
        @suspended_ticket.properties[:agent_comment] = { value: "Hello from agent", submitter_id: admin.id, is_public: true }
      end

      def assert_recover
        assert_difference "Comment.count(:all)", 2 do
          assert @suspended_ticket.recover(@agent, @author)
        end
      end

      it "recovers agent_comment as separate comment" do
        assert_recover
        comments.must_equal(
          [
            [@author.name, true, @suspended_ticket.send(:full_content)],
            [admin.name, true, "Hello from agent"]
          ]
        )
      end

      it "recovers agent_comment as private" do
        @suspended_ticket.properties[:agent_comment][:is_public] = false
        assert_recover
        comments.must_equal(
          [
            [@author.name, true, @suspended_ticket.send(:full_content)],
            [admin.name, false, "Hello from agent"]
          ]
        )
      end
    end

    describe "with html" do
      before do
        @suspended_ticket.properties[:html] = true
        @suspended_ticket.content = "<div>Hello</div>\n<h1>Head</h1>"
      end

      it "recovers html for new ticket" do
        ticket = @suspended_ticket.recover(users(:minimum_admin))
        ticket.description.must_equal "Hello\n\nHead"
        comment = ticket.comments.last
        comment.account.stubs(:has_rtl_lang_html_attr?).returns(false)
        comment.html_body.must_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><div>Hello</div>\n<h1>Head</h1></div>"
      end

      it "recovers html for new ticket, with auto directionality when arturo enabled" do
        ticket = @suspended_ticket.recover(users(:minimum_admin))
        ticket.description.must_equal "Hello\n\nHead"
        comment = ticket.comments.last
        comment.account.stubs(:has_rtl_lang_html_attr?).returns(true)
        comment.html_body.must_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><div>Hello</div>\n<h1 dir=\"auto\">Head</h1></div>"
      end
    end

    describe "when run_spam_detector is true" do
      describe "when the SpamDetector detects the ticket as spammy" do
        it "runs SpamDetector and does not create a ticket" do
          Fraud::SpamDetector.any_instance.expects(suspend_ticket?: true)
          ticket = @suspended_ticket.recover(users(:minimum_admin), nil, true)
          assert_nil ticket
        end
      end

      describe "when the SpamDetector detects the ticket as not spammy" do
        it "runs SpamDetector and does create a ticket" do
          Fraud::SpamDetector.any_instance.expects(suspend_ticket?: false)
          ticket = @suspended_ticket.recover(users(:minimum_admin), nil, true)
          assert ticket.is_a?(Ticket)
        end
      end
    end

    describe "when run_spam_detector is false" do
      it "does not run SpamDetector and does create a ticket" do
        Fraud::SpamDetector.any_instance.expects(:suspend_ticket?).never
        ticket = @suspended_ticket.recover(users(:minimum_admin))
        assert ticket.is_a?(Ticket)
      end
    end

    describe "when recovered as a comment on an existing ticket by and as a light agent or other agent who cannot publicly comment" do
      let(:ticket) { tickets(:minimum_1) }
      let(:private_comment_agent) { users(:minimum_agent) }
      let(:light_agent) { create_light_agent }

      before do
        private_comment_agent.is_private_comments_only = true
        private_comment_agent.save
        @suspended_ticket.ticket = ticket
        @suspended_ticket.save
      end

      describe_with_and_without_arturo_enabled :email_restricted_agent_fix do
        describe "when the modern email CCs setting is enabled" do
          before do
            @suspended_ticket.account.stubs(has_comment_email_ccs_allowed_enabled?: true)
          end

          describe "with agent that can comment privately" do
            it "recovers the suspended ticket successfully, returning the ticket on which it has been recovered as a new comment" do
              assert_equal ticket, @suspended_ticket.recover(private_comment_agent, private_comment_agent)
            end
          end

          describe "with light agent" do
            it "does not recover the ticket" do
              assert_nil @suspended_ticket.recover(light_agent, light_agent)
            end
          end
        end

        describe "when the modern email CCs setting is disabled" do
          before do
            @suspended_ticket.account.stubs(has_comment_email_ccs_allowed_enabled?: false)
          end

          describe "when the legacy CCs setting is enabled" do
            before do
              @suspended_ticket.account.stubs(is_collaboration_enabled?: true)
            end

            describe "with agent that can comment privately" do
              it "recovers the suspended ticket successfully, returning the ticket on which it has been recovered as a new comment" do
                assert_equal ticket, @suspended_ticket.recover(private_comment_agent, private_comment_agent)
              end
            end

            describe "with light agent" do
              it "recovers the suspended ticket successfully, returning the ticket on which it has been recovered as a new comment" do
                assert_equal ticket, @suspended_ticket.recover(light_agent, light_agent)
              end
            end
          end

          describe "when the legacy CCs setting is disabled" do
            before do
              @suspended_ticket.account.stubs(is_collaboration_enabled?: false)
            end

            describe "with agent that can comment privately" do
              it "recovers the suspended ticket successfully, returning the ticket on which it has been recovered as a new comment" do
                assert_equal ticket, @suspended_ticket.recover(private_comment_agent, private_comment_agent)
              end
            end

            describe "with light agent" do
              it "does not recover the ticket" do
                assert_nil @suspended_ticket.recover(light_agent, light_agent)
              end
            end
          end
        end
      end
    end

    describe "when recording metrics about recovering tickets suspended due to malware" do
      before { Rails.logger.stubs(:info) }

      let(:account) { accounts(:minimum) }
      let(:suspended_ticket) do
        account.suspended_tickets.create(
          from_name: 'Malware Sender',
          from_mail: 'malware.sender@example.com',
          message_id: 'abc123@localhost',
          cause: SuspensionType.SPAM,
          content: "Ruh roh!"
        )
      end
      let(:suspended_ticket_statsd_client) { suspended_ticket.send(:suspended_ticket_statsd_client) }
      let(:agent) { users(:minimum_agent) }

      describe "when the cause is SuspensionType.MALWARE" do
        before { suspended_ticket.update_column(:cause, SuspensionType.MALWARE_DETECTED) }

        describe_with_arturo_enabled :email_malware_recovery_metric do
          it "records a metric" do
            suspended_ticket_statsd_client.expects(:increment).with(:malware_recovery, instance_of(Hash)).once
            suspended_ticket.recover(agent)
          end

          it "logs a message" do
            Rails.logger.expects(:info).with(regexp_matches(/\ASuspensionType\.MALWARE_DETECTED recovery/)).once
            suspended_ticket.recover(agent)
          end
        end

        describe_with_arturo_disabled :email_malware_recovery_metric do
          it "does not record a metric" do
            suspended_ticket_statsd_client.expects(:increment).with(:malware_recovery, tags: instance_of(Array)).never
            suspended_ticket.recover(agent)
          end

          it "does not log anything" do
            Rails.logger.expects(:info).with(regexp_matches(/\ASuspensionType\.MALWARE_DETECTED recovery/)).never
            suspended_ticket.recover(agent)
          end
        end
      end

      describe "when the cause is not SuspensionType.MALWARE" do
        describe_with_and_without_arturo_enabled :email_malware_recovery_metric do
          it "does not record a metric" do
            suspended_ticket_statsd_client.expects(:increment).with(:malware_recovery, tags: instance_of(Array)).never
            suspended_ticket.recover(agent)
          end

          it "does not log anything" do
            Rails.logger.expects(:info).with(regexp_matches(/\ASuspensionType\.MALWARE_DETECTED recovery/)).never
            suspended_ticket.recover(agent)
          end
        end
      end
    end
  end

  describe ".bulk_delete" do
    before do
      @other_account_suspended_ticket = FactoryBot.create(:suspended_ticket, account: accounts(:support))

      account = accounts(:minimum)
      @suspended_tickets = Array.new(2) { FactoryBot.create(:suspended_ticket, account: account) }
      @suspended_ticket = FactoryBot.create(:suspended_ticket, account: account)

      SuspendedTicket.bulk_delete(account, @suspended_tickets.map(&:id))
    end

    it "only delete suspended tickets in account" do
      refute @other_account_suspended_ticket.reload.deleted?
    end

    it "softs delete specified tickets" do
      @suspended_tickets.each do |ticket|
        assert ticket.reload.deleted?, "#{ticket.inspect} not soft deleted"
      end
    end

    it "does not soft delete other tickets" do
      refute @suspended_ticket.reload.deleted?
    end

    before_should "expire cache key" do
      Account.any_instance.expects(:expire_scoped_cache_key).at_least_once.with(:views)
    end
  end

  describe ".cleanup" do
    let(:account) { accounts(:minimum) }

    before do
      @attachments = (0..1).map { FactoryBot.create(:attachment, account: account) }
      @old_ticket = FactoryBot.create(:suspended_ticket, account: account, attachments: [@attachments[0]], created_at: Time.now - 6.weeks)
      @new_ticket = FactoryBot.create(:suspended_ticket, account: account, attachments: [@attachments[1]])
      @deleted_ticket = FactoryBot.create(:suspended_ticket, account: account, created_at: Time.now - 6.weeks)
      SuspendedTicket.delete(@deleted_ticket.id)
      stub_request(:delete, %r{\.amazonaws.com/data/attachments/.*.jpg})
    end

    it "destroys old tickets" do
      SuspendedTicket.cleanup
      assert_nil SuspendedTicket.find_by_id(@old_ticket.id)
    end

    it "does not destroy tickets for accounts that are locked" do
      account.lock!
      SuspendedTicket.cleanup
      assert SuspendedTicket.find_by_id(@old_ticket.id)
    end

    it "destroys associated attachments" do
      SuspendedTicket.cleanup
      assert_nil Attachment.find_by_id(@attachments[0].id)
    end

    it "leaves new tickets and attachments in place" do
      SuspendedTicket.cleanup
      assert @new_ticket.reload
      assert @new_ticket.attachments.first
    end

    it "destroys soft-deleted tickets" do
      SuspendedTicket.cleanup
      SuspendedTicket.with_deleted do
        assert_nil SuspendedTicket.find_by_id(@deleted_ticket.id)
      end
    end

    it "destroys tickets that have no account" do
      @old_ticket.update_attribute(:account_id, 123456)
      SuspendedTicket.cleanup
      SuspendedTicket.with_deleted do
        assert_nil SuspendedTicket.find_by_id(@old_ticket.id)
      end
    end
  end

  describe "#initialize_defaults" do
    let(:account) { accounts(:minimum) }

    before do
      @suspended_ticket = account.suspended_tickets.new(
        cause: SuspensionType.SIGNUP_REQUIRED,
        from_name: users(:minimum_agent).name,
        from_mail: users(:minimum_agent).email,
        author: users(:minimum_agent),
        content: "test"
      )
    end

    describe "when uploads is an array of tokens" do
      describe "when one upload token is invalid" do
        before do
          @suspended_ticket.uploads = [account.upload_tokens.first.value, "invalidtoken"]
          @suspended_ticket.save!
        end

        it "associates attachments from valid tokens" do
          assert_equal 2, @suspended_ticket.attachments.size
        end
      end

      describe "when the tokens are valid" do
        before do
          @suspended_ticket.uploads = [account.upload_tokens.first.value, account.upload_tokens.last.value]
          @suspended_ticket.save!
        end

        it "associates attachments from all tokens" do
          assert_equal 3, @suspended_ticket.attachments.size
        end
      end
    end

    describe "when uploads is a string token" do
      describe "when the token is valid" do
        before do
          @suspended_ticket.uploads = account.upload_tokens.first.value
          @suspended_ticket.save!
        end

        it "associates attachments from the token" do
          assert_equal 2, @suspended_ticket.attachments.size
        end
      end

      describe "when the token is invalid" do
        before do
          @suspended_ticket.uploads = "invalidtoken"
          @suspended_ticket.save!
        end

        it "does not create any attachemnts" do
          assert_equal [], @suspended_ticket.attachments
        end
      end
    end
  end

  describe "#flags_options" do
    let(:suspended_ticket) { SuspendedTicket.new(content: 'CONTENT', subject: 'SUBJECT') }

    it "returns an empty hash when no flags_options are present" do
      assert_equal({}, suspended_ticket.flags_options)
    end
  end

  describe "#add_flags" do
    let(:suspended_ticket) { SuspendedTicket.new(content: 'CONTENT', subject: 'SUBJECT') }
    before do
      flags_options = {
        EventFlagType.ATTACHMENT_TOO_BIG => {
          message: {
            file: "big_file.txt",
            account_limit: 4096
          }
        }
      }
      suspended_ticket.add_flags(AuditFlags.new([EventFlagType.ATTACHMENT_TOO_BIG]), flags_options)
    end

    it "captures flags" do
      suspended_ticket.flags.must_equal AuditFlags.new([EventFlagType.ATTACHMENT_TOO_BIG])
    end

    it "captures flags_options" do
      suspended_ticket.flags_options[EventFlagType.ATTACHMENT_TOO_BIG][:message][:file].must_equal "big_file.txt"
    end

    it "overwrites captured flags" do
      flags = AuditFlags.new([EventFlagType.OTHER_USER_UPDATE])
      suspended_ticket.add_flags(flags, {})
      suspended_ticket.flags.must_equal flags
    end
  end

  describe "#ticket" do
    before do
      suspended_tickets(:minimum_spam).update_column(:ticket_id, tickets(:minimum_1).nice_id)
      suspended_tickets(:minimum_unverified).update_column(:ticket_id, tickets(:minimum_2).nice_id)
      @account = accounts(:minimum)
    end

    it "does not crash when loading via include" do
      assert_sql_queries 2 do
        tickets = @account.suspended_tickets.includes(:ticket).map(&:ticket).compact
        tickets.size.must_equal 2
      end
    end
  end
end
