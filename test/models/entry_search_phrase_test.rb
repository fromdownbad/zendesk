require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe EntrySearchPhrase do
  fixtures :entries, :accounts

  should_validate_presence_of :account, :entry, :phrase

  describe Entry do
    subject { Entry.new }
  end
end
