require_relative "../support/test_helper"

SingleCov.covered!

describe TicketDeflectionArticle do
  fixtures :tickets, :users, :accounts, :brands, :ticket_deflections

  let(:account) { accounts(:minimum) }
  let(:brand) { brands(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:user) { users(:minimum_agent) }
  let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }
  let(:articles) do
    [
      { id: 82, title: 'title', url: 'http:whatever/82', html_url: 'http://whatever/82', body: "I definitely used way more glue in the first five years of my life than I have in all the time since then." },
      { id: 91, title: 'title', url: 'http:whatever/91', html_url: 'http://whatever/91', body: "Vinyl records don't have grooves. They have one groove that spirals inward. They have groove." },
      { id: 32, title: 'title', url: 'http:whatever/32', html_url: 'http://whatever/32', body: "I'd update my software much more often if prompted to when exiting the program rather than opening it." }
    ]
  end

  let(:automatic_answer_viewed) do
    event = AutomaticAnswerViewed.new
    event.viewer_id = user.id
    event.article = articles.first.slice(:id, :title, :html_url, :url)
    event
  end

  let(:automatic_answer_send) do
    send_event = AutomaticAnswerSend.new
    send_event.suggested_articles = articles
    send_event
  end

  let(:automatic_answer_reject) do
    event = AutomaticAnswerReject.new
    event.reviewer_id = user.id
    event.irrelevant = true
    event.article = articles.first.slice(:id, :title, :html_url, :url)
    event
  end

  let(:irrelevant_article_id) { articles.first[:id] }

  should belong_to(:ticket_deflection).touch(true)
  should belong_to(:account)
  should belong_to(:brand)

  should validate_presence_of(:ticket_deflection)
  should validate_presence_of(:account)
  should validate_presence_of(:brand)
  should validate_numericality_of(:score)

  describe 'validations' do
    let(:article_info) do
      { 'article_id' => 1, 'score' => 0.192819, 'locale' => 'en-us', 'clicked_at' => 20.minutes.ago }
    end

    before do
      TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info).save!
    end

    describe 'when a record with the same deflection id and article id exists' do
      let(:ticket_deflection_article) do
        TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
      end

      it 'is invalid' do
        refute ticket_deflection_article.valid?
      end

      it 'gives an error' do
        ticket_deflection_article.valid?

        assert_includes(
          ticket_deflection_article.errors.full_messages,
          "Article article_id must be unique per ticket_deflection_id."
        )
      end
    end
  end

  describe ".build_from_deflection_and_article" do
    let(:article_info) do
      { 'article_id' => 1, 'score' => 0.192819, 'locale' => 'en-us', 'clicked_at' => 20.minutes.ago }
    end
    let(:ticket_deflection_article) { TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info) }

    it 'sets the deflection' do
      assert_equal ticket_deflection, ticket_deflection_article.ticket_deflection
    end

    it 'sets the brand_id from deflection' do
      assert_equal brand, ticket_deflection_article.brand
    end
  end

  describe '#update_clicked_at' do
    let(:article_id) { 12345 }
    let(:clicked_time) { Time.now + 5.minutes }
    let!(:deflection_article) do
      TicketDeflectionArticle.create!(
        ticket_deflection: ticket_deflection,
        article_id: article_id,
        account: account,
        score: 0.89,
        brand: brand
      )
    end

    it 'updates clicked_at time' do
      deflection_article.update_clicked_at(clicked_time)
      assert_equal clicked_time.to_i, deflection_article.reload.clicked_at.to_i
    end

    it 'publishes article clicked event' do
      deflection_article.update_clicked_at(clicked_time)
    end
  end

  describe '#update_solved_article' do
    let(:solved_article_id) { 123 }
    let(:deflection_article) do
      article_info = { 'article_id' => solved_article_id, 'score' => 0.192819, 'locale' => 'en-us' }
      deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
      deflection_article.save!

      deflection_article
    end

    it 'update solved at' do
      Timecop.freeze(2016, 5, 1, 13, 0) do
        deflection_article.update_solved_article(Time.now)
        assert_equal Time.now, deflection_article.reload.solved_at
      end
    end
  end

  describe '#reject_by_end_user' do
    let(:statsd_client_stub) { stub_for_statsd }

    describe 'with multiple articles' do
      let(:some_reason) { TicketDeflectionArticle::REASON_IRRELEVANT }
      let(:some_reason_string) { TicketDeflectionArticle::REASON_STRINGS[some_reason] }

      before do
        TicketDeflectionArticle.stubs(:statsd_client).returns(statsd_client_stub)
        statsd_client_stub.expects(:increment).with('mark_suggested_article_irrelevant', tags: ['subdomain:minimum', 'by:end_user', "reason:#{some_reason_string}"]).at_least_once

        2.times do |i|
          article_info = { 'article_id' => i + 1, 'score' => 0.192819, 'locale' => 'en-us' }
          deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
          deflection_article.save!
        end

        article_info = { 'article_id' => articles.first[:id], 'score' => 0.271284, 'locale' => 'en-us' }
        @deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
        @deflection_article.save!
      end

      it 'updates the irrelevant reason' do
        Timecop.freeze(2017, 1, 16, 10, 0) do
          TicketDeflectionArticle.reject_by_end_user(ticket_deflection, irrelevant_article_id, some_reason)
          @deflection_article.reload

          assert @deflection_article.irrelevant_by_end_user
          assert_equal some_reason, @deflection_article.irrelevant_reason_by_end_user
          assert_equal Time.now, @deflection_article.irrelevant_by_end_user_updated_at
        end
      end

      it 'leaves other deflection articles alone' do
        TicketDeflectionArticle.reject_by_end_user(ticket_deflection, irrelevant_article_id, some_reason)

        deflection_articles_without_marked_irrelevant = TicketDeflectionArticle.where(account_id: account.id).where("id != #{@deflection_article.id}")
        deflection_article_irrelevant_updated_at = deflection_articles_without_marked_irrelevant.map(&:irrelevant_by_end_user_updated_at)

        assert_empty deflection_article_irrelevant_updated_at.compact
      end
    end

    describe '#reject_by_end_user should not change the by_agent attributes' do
      before do
        article_info = { 'article_id' => irrelevant_article_id, 'score' => 0.192819, 'locale' => 'en-us' }
        @deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
        @deflection_article.save!

        @original_irrelevant_by_agent = @deflection_article.irrelevant_by_agent
        @original_irrelevant_reason_by_agent = @deflection_article.irrelevant_reason_by_agent
        assert_nil @deflection_article.irrelevant_by_agent_updated_at

        TicketDeflectionArticle.reject_by_end_user(ticket_deflection, irrelevant_article_id, 1)
        @deflection_article.reload
      end

      it 'does not touch irrelevant_by_agent' do
        assert_equal @original_irrelevant_by_agent, @deflection_article.irrelevant_by_agent
      end

      it 'does not touch irrelevant_reason_by_agent' do
        assert_equal @original_irrelevant_reason_by_agent, @deflection_article.irrelevant_reason_by_agent
      end

      it 'does not touch irrelevant_by_agent_updated_by' do
        assert_nil @deflection_article.irrelevant_by_agent_updated_at
      end
    end

    describe 'logging when non suggested article is updated' do
      let(:reason) { TicketDeflectionArticle::REASON_IRRELEVANT }
      let(:reason_string) { TicketDeflectionArticle::REASON_STRINGS[reason] }

      before do
        TicketDeflectionArticle.stubs(:statsd_client).returns(statsd_client_stub)
      end

      it 'logs an event' do
        statsd_client_stub.expects(:increment).with('mark_unsuggested_article_irrelevant', tags: ['subdomain:minimum', 'by:end_user', "reason:#{reason_string}"])

        TicketDeflectionArticle.reject_by_end_user(ticket_deflection, 9999, reason)
      end
    end

    describe 'with non-exist or unknown reason' do
      let(:non_existent_reason) { 16 } # the reason fields are tinyint(4)
      let(:irrelevant_article_id) { '123123' }
      let(:article_info) do
        {
          'article_id' => irrelevant_article_id,
          'score' => 0.192819,
          'locale' => 'en-us',
          'clicked_at' => 20.minutes.ago
        }
      end
      let(:deflection_article) { TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info) }

      before do
        TicketDeflectionArticle.stubs(:statsd_client).returns(statsd_client_stub)
        deflection_article.save!
      end

      it 'set default reason to UNKNOWN when nil' do
        TicketDeflectionArticle.reject_by_end_user(ticket_deflection, irrelevant_article_id, nil)
        deflection_article.reload

        assert_equal TicketDeflectionArticle::REASON_UNKNOWN, deflection_article.irrelevant_reason_by_end_user
      end

      it 'raises ArgumentError exception when unknown' do
        assert_raises(ArgumentError) do
          TicketDeflectionArticle.reject_by_end_user(ticket_deflection, irrelevant_article_id, non_existent_reason)
        end
      end
    end

    describe 'with deflection without ticket and user' do
      let(:ticket_deflection) { ticket_deflections(:without_ticket) }
      let(:non_existent_reason) { 16 }
      let(:irrelevant_article_id) { '123123' }
      let(:article_info) do
        {
          'article_id' => irrelevant_article_id,
          'score' => 0.192819,
          'locale' => 'en-us',
          'clicked_at' => 20.minutes.ago
        }
      end
      let(:deflection_article) { TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info) }

      before do
        TicketDeflectionArticle.stubs(:statsd_client).returns(statsd_client_stub)
        deflection_article.save!
      end

      it 'does not invoke EventCreator to create reject event' do
        TicketDeflectionArticle.reject_by_end_user(ticket_deflection, irrelevant_article_id, nil)

        ::AnswerBot::EventCreator.expects(:reject_event).never
      end
    end
  end

  describe '#reject_by_agent' do
    let(:irrelevant_article_id) { 123 }
    let(:statsd_client_stub) { stub_for_statsd }
    let(:reason_unknown_string) { TicketDeflectionArticle::REASON_STRINGS[TicketDeflectionArticle::REASON_UNKNOWN] }
    let(:reason_irrelevant_string) { TicketDeflectionArticle::REASON_STRINGS[TicketDeflectionArticle::REASON_IRRELEVANT] }

    describe 'given existing deflection articles' do
      let(:some_reason) { 11 }

      before do
        TicketDeflectionArticle.stubs(:statsd_client).returns(statsd_client_stub)

        2.times do |i|
          article_info = { 'article_id' => i + 1, 'score' => 0.192819, 'locale' => 'en-us' }
          deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
          deflection_article.save!
        end

        article_info = { 'article_id' => irrelevant_article_id, 'score' => 0.271284, 'locale' => 'en-us' }
        @deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
        @deflection_article.save!
      end

      it 'marks as irrelevant' do
        Timecop.freeze(2017, 1, 16, 10, 0) do
          statsd_client_stub.expects(:increment).with(
            'mark_suggested_article_irrelevant',
            tags: ['subdomain:minimum', 'by:agent', "reason:#{reason_irrelevant_string}"]
          ).once

          TicketDeflectionArticle.reject_by_agent(ticket_deflection, irrelevant_article_id, true, user.id, user.name)
          @deflection_article.reload

          assert @deflection_article.irrelevant_by_agent
          assert_equal TicketDeflectionArticle::REASON_IRRELEVANT, @deflection_article.irrelevant_reason_by_agent
          assert_equal Time.now, @deflection_article.irrelevant_by_agent_updated_at
        end
      end

      it 'unmarks as irrelevant' do
        Timecop.freeze(2017, 1, 16, 10, 0) do
          statsd_client_stub.expects(:increment).with(
            'unmark_suggested_article_irrelevant',
            tags: ['subdomain:minimum', 'by:agent', "reason:#{reason_unknown_string}"]
          ).once

          TicketDeflectionArticle.reject_by_agent(ticket_deflection, irrelevant_article_id, false, user.id, user.name)
          @deflection_article.reload

          assert !@deflection_article.irrelevant_by_agent
          assert_equal TicketDeflectionArticle::REASON_UNKNOWN, @deflection_article.irrelevant_reason_by_agent
          assert_equal Time.now, @deflection_article.irrelevant_by_agent_updated_at
        end
      end

      it 'leaves other deflection articles alone' do
        statsd_client_stub.expects(:increment).with(
          'mark_suggested_article_irrelevant',
          tags: ['subdomain:minimum', 'by:agent', "reason:#{reason_irrelevant_string}"]
        ).once

        TicketDeflectionArticle.reject_by_agent(ticket_deflection, irrelevant_article_id, true, user.id, user.name)

        deflection_articles_without_marked_irrelevant = TicketDeflectionArticle.where(account_id: account.id).where("id != #{@deflection_article.id}")
        deflection_article_irrelevant_updated_at = deflection_articles_without_marked_irrelevant.map(&:irrelevant_by_agent_updated_at)

        assert_empty deflection_article_irrelevant_updated_at.compact
      end
    end

    describe '#reject_by_agent should not change the by_end_user attributes' do
      before do
        article_info = { 'article_id' => irrelevant_article_id, 'score' => 0.192819, 'locale' => 'en-us' }
        @deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
        @deflection_article.save!

        @original_irrelevant_by_end_user = @deflection_article.irrelevant_by_end_user
        @original_irrelevant_reason_by_end_user = @deflection_article.irrelevant_reason_by_end_user
        assert_nil @deflection_article.irrelevant_by_end_user_updated_at

        TicketDeflectionArticle.reject_by_agent(ticket_deflection, irrelevant_article_id, true, user.id, user.name)
        @deflection_article.reload
      end

      it 'does not touch irrelevant_by_end_user' do
        assert_equal @original_irrelevant_by_end_user, @deflection_article.irrelevant_by_end_user
      end

      it 'does not touch irrelevant_reason_by_end_user' do
        assert_equal @original_irrelevant_reason_by_end_user, @deflection_article.irrelevant_reason_by_end_user
      end

      it 'does not touch irrelevant_by_end_user_updated_by' do
        assert_nil @deflection_article.irrelevant_by_end_user_updated_at
      end
    end

    describe 'logging when non suggested article is updated' do
      before do
        TicketDeflectionArticle.stubs(:statsd_client).returns(statsd_client_stub)
      end

      it 'logs an event' do
        statsd_client_stub.expects(:increment).with(
          'mark_unsuggested_article_irrelevant',
          tags: ['subdomain:minimum', 'by:agent', "reason:#{reason_irrelevant_string}"]
        )

        TicketDeflectionArticle.reject_by_agent(ticket_deflection, 9999, true, user.id, user.name)
      end
    end
  end

  describe "#add_auto_answer_reject_event" do
    let(:article) { articles.first }
    let(:deflection_article) do
      article_info = { 'article_id' => article[:id], 'score' => 0.192819, 'locale' => 'en-us' }

      deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
      deflection_article.save!
      deflection_article
    end

    let(:irrelevant) { true }
    let(:reviewer_id) { 1 }
    let(:reviewer_name) { 'John Doe' }

    before do
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << automatic_answer_send
      ticket.save!
    end

    it "adds an AutomaticAnswerReject event to the ticket" do
      deflection_article.add_auto_answer_reject_event(irrelevant, reviewer_id, reviewer_name)
      assert(ticket.events.where(type: 'AutomaticAnswerReject').count > 0)
    end

    it "does not execute triggers and stops radar notification" do
      Zendesk::Rules::Trigger::Execution.expects(:execute).never

      deflection_article.add_auto_answer_reject_event(irrelevant, reviewer_id, reviewer_name)
    end

    it "adds an AutomaticAnswerReject event to the ticket" do
      deflection_article.add_auto_answer_reject_event(false, reviewer_id, reviewer_name)
      aa_reject = AutomaticAnswerReject.last
      assert(ticket.events.where(type: 'AutomaticAnswerReject').count > 0)
      refute(aa_reject.irrelevant)
    end

    it "should not add an AutomaticAnswerReject event to the ticket, if the user_id is not passed" do
      deflection_article.add_auto_answer_reject_event(true, nil, reviewer_name)
      refute(ticket.events.where(type: 'AutomaticAnswerReject').count > 0)
    end

    it "should not add an AutomaticAnswerReject event to the ticket, if the ticket is already closed" do
      Ticket.any_instance.stubs(:closed?).returns(true)
      deflection_article.add_auto_answer_reject_event(true, 1, reviewer_name)
      refute(ticket.events.where(type: 'AutomaticAnswerReject').count > 0)
    end

    it "should not add an AutomaticAnswerReject event, if the ticket does not exist" do
      TicketDeflection.any_instance.stubs(:ticket).returns(nil)
      deflection_article.add_auto_answer_reject_event(true, 1, reviewer_name)
      refute(ticket.events.where(type: 'AutomaticAnswerReject').count > 0)
    end

    describe 'answer_bot_for_agents' do
      let(:ticket_deflection) do
        deflection = ticket_deflections(:minimum_ticket_deflection)
        deflection.update_attributes!(
          deflection_channel_id: ViaType.ANSWER_BOT_FOR_AGENTS,
          ticket: ticket
        )
        deflection
      end

      it 'adds AutomaticAnswerReject event (ie. answer bot behaviour)' do
        deflection_article.add_auto_answer_reject_event(true, 1, reviewer_name)
        assert_equal(1, ticket.events.where(type: 'AutomaticAnswerReject').count)
      end
    end
  end

  describe "#already_viewed_by?" do
    let(:deflection) { ticket_deflections(:minimum_ticket_deflection) }

    before do
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << automatic_answer_send
      ticket.save!

      article_info = { 'article_id' => automatic_answer_viewed.article[:id], 'score' => 0.192819, 'locale' => 'en-us' }
      @deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(deflection, article_info)
      @deflection_article.clicked_at = Time.now

      @deflection_article.save!
    end

    it "returns true if an article is viewed by the user" do
      @deflection_article.clicked_at = Time.now
      @deflection_article.save!

      assert @deflection_article.already_viewed_by?(ticket.requester_id)
    end

    it "returns false if an article is not viewed by the user" do
      refute @deflection_article.already_viewed_by?(automatic_answer_viewed.viewer_id)
    end
  end

  describe "#add_auto_answer_viewed_event" do
    let(:deflection) { ticket_deflections(:minimum_ticket_deflection) }
    let(:deflection_article) do
      article_info = { 'article_id' => articles.first[:id], 'score' => 0.192819, 'locale' => 'en-us' }
      deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(deflection, article_info)
      deflection_article.save!

      deflection_article
    end

    before do
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << automatic_answer_send
      ticket.save!
    end

    it "adds an AutomaticAnswerViewed event to the ticket when the article's clicked_at is updated" do
      deflection_article.clicked_at = Time.now
      deflection_article.save!
      assert_equal(1, ticket.events.where(type: 'AutomaticAnswerViewed').count)
    end

    it "does not add an AutomaticAnswerViewed event to the ticket when the article's clicked_at is updated and the article is already viewed" do
      deflection_article.clicked_at = Time.now
      deflection_article.save!

      deflection_article.clicked_at = Time.now
      deflection_article.save!
      assert_equal(1, ticket.events.where(type: 'AutomaticAnswerViewed').count)
    end

    it "does not add an AutomaticAnswerViewed event to the ticket if the ticket is already closed" do
      Ticket.any_instance.stubs(:closed?).returns(true)
      deflection_article.clicked_at = Time.now
      deflection_article.save!

      refute(ticket.events.where(type: 'AutomaticAnswerViewed').count > 0)
    end

    it "does not add an AutomaticAnswerViewed event if the ticket does not exist" do
      TicketDeflection.any_instance.stubs(:ticket).returns(nil)
      deflection_article.clicked_at = Time.now
      deflection_article.save!

      refute(ticket.events.where(type: 'AutomaticAnswerViewed').count > 0)
    end

    describe 'deflection without ticket' do
      let(:deflection) { ticket_deflections(:without_ticket) }

      it 'does not throw exception' do
        deflection_article.clicked_at = Time.now

        assert deflection_article.save!
      end
    end

    describe 'answer_bot_for_agents' do
      let(:ticket) { tickets(:minimum_2) }
      let(:deflection) do
        deflection = ticket_deflections(:minimum_ticket_deflection)
        deflection.update_attributes!(
          deflection_channel_id: ViaType.ANSWER_BOT_FOR_AGENTS,
          ticket: ticket
        )
        deflection
      end

      it 'adds AutomaticAnswerViewed event (ie. answer bot behaviour)' do
        deflection_article.clicked_at = Time.now
        deflection_article.save!

        assert_equal(1, ticket.events.where(type: 'AutomaticAnswerViewed').count)
      end
    end
  end

  describe '#creation_to_clicked_time_delta' do
    let!(:deflection_article) do
      TicketDeflectionArticle.create!(
        ticket_deflection: ticket_deflection,
        article_id: 1111,
        account: account,
        score: 0.89,
        brand: brand,
        clicked_at: clicked_time
      )
    end

    describe 'deflection article has been clicked' do
      let(:clicked_time) { ticket_deflection.created_at + 5.seconds }

      it 'returns time difference of clicked time and ticket deflection created time, in seconds' do
        assert_equal 5, deflection_article.creation_to_clicked_time_delta
      end
    end

    describe 'deflection article has not been clicked' do
      let(:clicked_time) { nil }

      it 'returns zero' do
        assert_equal 0, deflection_article.creation_to_clicked_time_delta
      end
    end
  end

  describe 'locale' do
    let(:deflection) { mock('TicketDeflection') }
    let!(:deflection_article) do
      TicketDeflectionArticle.create!(
        ticket_deflection: ticket_deflection,
        article_id: 1111,
        account: account,
        score: 0.89,
        brand: brand,
        locale: 'en-us'
      )
    end
    let!(:deflection_article_two) do
      TicketDeflectionArticle.create!(
        ticket_deflection: ticket_deflection,
        article_id: 1112,
        account: account,
        score: 0.89,
        brand: brand,
        locale: 'es'
      )
    end
    let(:deflection_articles) { [deflection_article] }
    let(:deflection_articles_empty) { [] }

    it 'returns the locale of the article if present' do
      deflection.stubs(:ticket_deflection_articles).returns(deflection_articles)
      deflection_articles.stubs(:where).returns(deflection_articles)

      assert_equal 'en-us', TicketDeflectionArticle.locale(deflection, deflection_article.id)
    end

    it 'returns the default locale from one of the deflection articles' do
      deflection.stubs(:ticket_deflection_articles).returns(deflection_articles_empty)
      deflection_articles_empty.stubs(:where).returns([])

      refute TicketDeflectionArticle.locale(deflection, deflection_article.id)
    end
  end

  describe '#suppressed?' do
    let!(:deflection_article) do
      TicketDeflectionArticle.create!(
        ticket_deflection: ticket_deflection,
        article_id: 1111,
        account: account,
        score: 0.89,
        brand: brand
      )
    end

    it 'returns true if article was viewed before deflection' do
      deflection_article.clicked_at = 2.minutes.ago
      assert deflection_article.suppressed?
    end

    it 'returns false if article was never viewed' do
      deflection_article.clicked_at = nil
      refute deflection_article.suppressed?
    end

    it 'returns false if article was viewed after deflection' do
      deflection_article.clicked_at = 20.minutes.from_now
      refute deflection_article.suppressed?
    end
  end

  describe 'ticket associations' do
    let!(:deflection_article) do
      TicketDeflectionArticle.create!(
        ticket_deflection: ticket_deflection,
        article_id: 1111,
        account: account,
        score: 0.89,
        brand: brand
      )
    end

    describe '#ticket' do
      it 'returns the ticket associated with the ticket deflection' do
        assert_equal ticket_deflection.ticket, deflection_article.ticket
      end
    end

    describe '#ticket_id' do
      it 'returns the id of the ticket associated with the ticket deflection' do
        assert_equal ticket_deflection.ticket_id, deflection_article.ticket_id
      end
    end
  end
end
