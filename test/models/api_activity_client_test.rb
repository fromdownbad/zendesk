require_relative "../support/test_helper"
require_relative "../support/api_activity_test_helper"

SingleCov.covered! uncovered: 1

describe ApiActivityClient do
  include ApiActivityTestHelper

  fixtures :accounts, :api_activity_clients

  let(:account) { accounts(:minimum) }
  let(:api_activity_client) { api_activity_clients(:client_one) }

  should_validate_presence_of :account_id

  describe "add entry from memcached" do
    before do
      # this is so tests will cross the top of the hour
      @current_time = Time.new(2016, 11, 16, 2, 0, 0)
      populate_client_data(account, @current_time, @current_time)
      # add an old entry. This should be ignored
      @twenty_five_hours_ago = @current_time - 26.hours
      Timecop.freeze(@twenty_five_hours_ago)
      write_hour_data_to_memcached(account, api_activity_client.client_key, @twenty_five_hours_ago)
      api_activity_client.update_client(@twenty_five_hours_ago.to_i / 1.hour, @twenty_five_hours_ago, 7 * 24)

      # go forward to current time
      Timecop.freeze(@current_time)

      write_hour_data_to_memcached(account, api_activity_client.client_key, @current_time)
      # use one hour ago so we ensure that we are reading the correct memcached entry
      @one_hour_ago = (@current_time - 1.hour).to_i / 1.hour
      api_activity_client.update_client(@one_hour_ago, @current_time, 7 * 24)
      @hour_data = JSON.parse(api_activity_client.data).with_indifferent_access
    end

    it "writes hour data properly" do
      assert_equal(@hour_data.keys.count, 168) # 169 is seven days => 7*24
      assert_equal(@hour_data.keys.first, @one_hour_ago.to_s)
      hour_counts = @hour_data[@one_hour_ago.to_s]
      assert_equal(hour_counts['count'], 200)
      assert_equal(hour_counts['peak_count'], 22)
      assert_equal(api_activity_client.last_request_time, @current_time)
      # the day before is the 15th of the month. The counts for the 15th are 1015 except for
      # one day where it is 100 (from memcached) => 23*1015 + 100 = 23445
      assert_equal(api_activity_client.total_count_last_24_hours, 23445)
    end
  end

  private

  def write_hour_data_to_memcached(account, client_key, current_time)
    current_hour = current_time.to_i / 1.hour
    # write peak count key for current and previous two hours to ensure we only write previous hour only
    Rails.cache.write(peak_count_key(account.id, current_hour), 33, expires_in: 24.hours)
    Rails.cache.write(peak_count_key(account.id, current_hour - 1), 22, expires_in: 24.hours)
    Rails.cache.write(peak_count_key(account.id, current_hour - 2), 11, expires_in: 24.hours)

    # write hour keys
    count = 100
    3.times do |i|
      hour_key = hour_cache_key(account.id, client_key, current_hour - i)
      Rails.cache.write(hour_key, count, expires_in: 24.hours)
      count += 100
    end
  end
end
