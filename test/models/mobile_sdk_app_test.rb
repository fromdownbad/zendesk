require_relative "../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"

SingleCov.covered! uncovered: 6

describe MobileSdkApp do
  fixtures :accounts, :account_settings, :brands, :translation_locales, :users, :mobile_sdk_auths, :oauth_clients

  let(:user) { users(:minimum_sdk_agent) }
  let(:mobile_sdk_app) { FactoryBot.create(:mobile_sdk_app, account: @account, title: 'My App', identifier: new_app_identifier, mobile_sdk_auth: @mobile_sdk_auth) }

  def new_app_identifier
    (0...50).map { ('a'..'z').to_a[rand(26)] }.join
  end

  before do
    brands(:minimum)
    @account = accounts(:minimum_sdk)
    @mobile_sdk_auth = mobile_sdk_auths(:minimum_sdk)
  end

  describe "defaults" do
    before do
      @mobile_sdk_app = mobile_sdk_app
    end

    subject { mobile_sdk_app }

    should_validate_presence_of :identifier, :account, :mobile_sdk_auth

    it "has an id" do
      assert_respond_to @mobile_sdk_app, :id
    end

    it "has a name" do
      assert_respond_to @mobile_sdk_app, :title
    end

    it "has a description" do
      assert_respond_to @mobile_sdk_app, :identifier
    end
  end

  it "creates minimum MobileSdkApp" do
    assert @account.mobile_sdk_apps.create!(title: "My App", identifier: new_app_identifier, mobile_sdk_auth: @mobile_sdk_auth)
  end

  it "creates more than one app for an account" do
    assert @account.mobile_sdk_apps.create!(title: "My App", identifier: new_app_identifier, mobile_sdk_auth: @mobile_sdk_auth)
    assert @account.mobile_sdk_apps.create!(title: "My Second App", identifier: new_app_identifier, mobile_sdk_auth: @mobile_sdk_auth)
  end

  it "sets default brand" do
    assert msa = @account.mobile_sdk_apps.create!(title: "My App", identifier: new_app_identifier, mobile_sdk_auth: @mobile_sdk_auth)
    assert_equal @account.default_brand, msa.brand
  end

  it "has an unique identifier" do
    app_identifier = new_app_identifier
    assert @account.mobile_sdk_apps.create!(title: "My App", identifier: app_identifier, mobile_sdk_auth: @mobile_sdk_auth)
    assert_raise(ActiveRecord::RecordInvalid) do
      @account.mobile_sdk_apps.create!(title: "My App 2", identifier: app_identifier, mobile_sdk_auth: @mobile_sdk_auth)
    end
  end

  describe "search" do
    before do
      @msa = mobile_sdk_app
      @app_identifier = @msa.identifier
      @account.reload
    end

    it "finds by identifier" do
      assert found_msa = @account.mobile_sdk_apps.find_by_identifier(@app_identifier)
      assert_equal @msa, found_msa
    end
  end

  describe "settings" do
    before do
      @mobile_sdk_app = mobile_sdk_app
    end

    it "does not need auth if conversations was enabled" do
      @mobile_sdk_app.settings.conversations_enabled = true
      @mobile_sdk_app.save!
    end

    it "corectly serialize tags" do
      tags = ["one", "two"]
      @mobile_sdk_app.settings.rma_tags = tags
      @mobile_sdk_app.save
      found_msa = @account.mobile_sdk_apps.find_by_id(@mobile_sdk_app.id)
      assert_equal tags, found_msa.settings.rma_tags
    end

    # Save visits, duration, delay within limits
    it "enforces rma_visits limits" do
      @mobile_sdk_app.settings.rma_visits = 2
      assert @mobile_sdk_app.save!

      @mobile_sdk_app.settings.rma_visits = 0
      assert_raise(ActiveRecord::RecordInvalid) do
        @mobile_sdk_app.save!
      end

      @mobile_sdk_app.settings.rma_visits = 100
      assert_raise(ActiveRecord::RecordInvalid) do
        @mobile_sdk_app.save!
      end
    end

    it "enforces rma_duration limits" do
      @mobile_sdk_app.settings.rma_duration = 2
      assert @mobile_sdk_app.save!

      @mobile_sdk_app.settings.rma_duration = -1
      assert_raise(ActiveRecord::RecordInvalid) do
        @mobile_sdk_app.save!
      end

      @mobile_sdk_app.settings.rma_duration = 100
      assert_raise(ActiveRecord::RecordInvalid) do
        @mobile_sdk_app.save!
      end
    end

    it "enforces rma_delay limits" do
      @mobile_sdk_app.settings.rma_delay = 1600
      assert @mobile_sdk_app.save!

      @mobile_sdk_app.settings.rma_delay = 0
      assert_raise(ActiveRecord::RecordInvalid) do
        @mobile_sdk_app.save!
      end

      @mobile_sdk_app.settings.rma_delay = 3601
      assert_raise(ActiveRecord::RecordInvalid) do
        @mobile_sdk_app.save!
      end
    end
  end

  describe "destroy" do
    before do
      @msa = mobile_sdk_app
      @account.reload
      @msa.destroy
    end

    it "should destroy auth" do
      assert @mobile_sdk_auth.destroyed?
    end

    it "should destroy client" do
      assert @mobile_sdk_auth.client.destroyed?
    end
  end

  describe "#conversations_enabled?" do
    before do
      @mobile_sdk_app = mobile_sdk_app
    end

    describe "when the account's 'has_sdk_manage_requests?' is false" do
      before do
        @account.stubs(:has_sdk_manage_requests?).returns(false)
      end

      it "is false when the 'conversations_enabled' setting is false" do
        @mobile_sdk_app.settings.conversations_enabled = false
        @mobile_sdk_app.save!

        refute @mobile_sdk_app.conversations_enabled?
      end

      it "is false and the 'conversations_enabled' setting is true" do
        @mobile_sdk_app.settings.conversations_enabled = true
        @mobile_sdk_app.save!

        refute @mobile_sdk_app.conversations_enabled?
      end
    end

    describe "when the account's 'has_sdk_manage_requests?' is true" do
      before do
        @account.stubs(:has_sdk_manage_requests?).returns(true)
      end

      it "is false when the 'conversations_enabled' setting is false" do
        @mobile_sdk_app.settings.conversations_enabled = false
        @mobile_sdk_app.save!

        refute @mobile_sdk_app.conversations_enabled?
      end

      it "is true and the 'conversations_enabled' setting is true" do
        @mobile_sdk_app.settings.conversations_enabled = true
        @mobile_sdk_app.save!

        assert @mobile_sdk_app.conversations_enabled?
      end
    end
  end

  describe "#help_center_article_voting_enabled?" do
    before do
      @mobile_sdk_app = mobile_sdk_app
    end

    describe_with_arturo_disabled :mobile_sdk_help_center_article_voting_enabled do
      it "is false when the the arturo is off & the help_center_article_voting_enabled setting is false" do
        @mobile_sdk_app.settings.help_center_article_voting_enabled = false
        @mobile_sdk_app.save!

        refute @mobile_sdk_app.help_center_article_voting_enabled?, "Value should be false when arturo is disabled and setting is false."
      end

      it "is false when the arturo is off & the help_center_article_voting_enabled setting is true" do
        @mobile_sdk_app.settings.help_center_article_voting_enabled = true
        @mobile_sdk_app.save!

        refute @mobile_sdk_app.help_center_article_voting_enabled?, "Value should be false when setting is true but arturo is disbabled"
      end
    end

    describe_with_arturo_enabled :mobile_sdk_help_center_article_voting_enabled do
      it "returns false when arturo is enabled & `help_center_article_voting_enabled` setting is false" do
        @mobile_sdk_app.settings.help_center_article_voting_enabled = false
        @mobile_sdk_app.save!

        refute @mobile_sdk_app.help_center_article_voting_enabled?, "Value should be false when arturo is enabled but setting is false"
      end

      it "returns true when arturo is enabled & `help_center_article_voting_enabled` setting is true" do
        @mobile_sdk_app.settings.help_center_article_voting_enabled = true
        @mobile_sdk_app.save!

        assert @mobile_sdk_app.help_center_article_voting_enabled?, "Value should be true when arturo is enabled and setting is true"
      end
    end
  end

  describe "blips settings helpers" do
    before do
      @mobile_sdk_app = mobile_sdk_app
    end

    describe "default values" do
      describe "when the 'mobile_sdk_blips' Arturo feature is OFF" do
        it "all of them are false" do
          Arturo.disable_feature!(:mobile_sdk_blips)
          assert_equal false, @mobile_sdk_app.required_blips_enabled?
          assert_equal false, @mobile_sdk_app.behavioural_blips_enabled?
          assert_equal false, @mobile_sdk_app.pathfinder_blips_enabled?
        end
      end

      describe "when the 'mobile_sdk_blips' Arturo feature is ON and the 'mobile_sdk_<type>_blips' Arturo flag is ON" do
        it "all of them are true" do
          Arturo.enable_feature!(:mobile_sdk_blips)
          Arturo.enable_feature!(:mobile_sdk_required_blips)
          Arturo.enable_feature!(:mobile_sdk_behavioural_blips)
          Arturo.enable_feature!(:mobile_sdk_pathfinder_blips)
          assert(@mobile_sdk_app.required_blips_enabled?)
          assert(@mobile_sdk_app.behavioural_blips_enabled?)
          assert(@mobile_sdk_app.pathfinder_blips_enabled?)
        end
      end
    end

    describe "#required_blips_enabled?" do
      # truth table indexes mapping
      indexes = {
        blips_arturo: 0,
        req_blips_arturo: 1,
        blips_acc_setting: 2,
        req_blips_acc_setting: 3,
        blips_app_setting: 4,
        req_blips_app_setting: 5,
        expected_result: 6
      }

      [ # blips_arturo, req_blips_arturo,  blips_acc_setting, req_blips_acc_setting,  blips_app_setting, req_blips_app_setting,  expected_result
        [false,         false,             false,             false,                  false,             false,                  false],
        [true,          false,             false,             false,                  false,             false,                  false],
        [true,          true,              false,             false,                  false,             false,                  false],
        [true,          true,              true,              false,                  false,             false,                  false],
        [true,          true,              true,              true,                   false,             false,                  false],
        [true,          true,              true,              true,                   true,              false,                  false],
        [true,          true,              true,              true,                   true,              true,                   true],
        [false,         true,              true,              true,                   true,              true,                   false],
        [true,          false,             true,              true,                   true,              true,                   false],
        [true,          true,              false,             true,                   true,              true,                   false],
        [true,          true,              true,              false,                  true,              true,                   false],
        [true,          true,              true,              true,                   false,             true,                   false],
        [true,          true,              true,              true,                   true,              false,                  false]
      ].each do |truth_table|
        msg = "\nWhen the 'Arturo top level blips feature' is '#{truth_table[indexes[:blips_arturo]]}' and\n" \
              "the 'Arturo for required blips feature' is '#{truth_table[indexes[:req_blips_arturo]]}' and\n" \
              "the 'Account level blips setting' is '#{truth_table[indexes[:blips_acc_setting]]}' and\n" \
              "the 'Account level required blips setting' is '#{truth_table[indexes[:req_blips_acc_setting]]}' and\n" \
              "the 'Mobile SDK level blips' setting is '#{truth_table[indexes[:blips_app_setting]]}' then\n" \
              "the 'Mobile SDK level required blips' setting is '#{truth_table[indexes[:req_blips_app_setting]]}' then\n" \
              "'required_blips_enabled?' must return '#{truth_table[indexes[:expected_result]]}'"
        describe msg do
          it "should be '#{truth_table[indexes[:expected_result]]}'" do
            @account.stubs(:has_mobile_sdk_blips?).returns(truth_table[indexes[:blips_arturo]])
            @account.stubs(:has_mobile_sdk_required_blips?).returns(truth_table[indexes[:req_blips_arturo]])
            @account.settings.stubs(:mobile_sdk_blips?).returns(truth_table[indexes[:blips_acc_setting]])
            @account.settings.stubs(:mobile_sdk_required_blips?).returns(truth_table[indexes[:req_blips_acc_setting]])
            @mobile_sdk_app.settings.stubs(:blips?).returns(truth_table[indexes[:blips_app_setting]])
            @mobile_sdk_app.settings.stubs(:required_blips?).returns(truth_table[indexes[:req_blips_app_setting]])

            assert_equal truth_table[indexes[:expected_result]], @mobile_sdk_app.required_blips_enabled?
          end
        end
      end
    end

    describe "#behavioural_blips_enabled?" do
      # truth table indexes mapping
      indexes = {
        blips_arturo: 0,
        behav_blips_arturo: 1,
        blips_acc_setting: 2,
        behav_blips_acc_setting: 3,
        blips_app_setting: 4,
        behav_blips_app_setting: 5,
        expected_result: 6
      }

      [ # blips_arturo, behav_blips_arturo, blips_acc_setting, behav_blips_acc_setting, blips_app_setting, behav_blips_app_setting, expected_result
        [false,         false,              false,             false,                   false,             false,                   false],
        [true,          false,              false,             false,                   false,             false,                   false],
        [true,          true,               false,             false,                   false,             false,                   false],
        [true,          true,               true,              false,                   false,             false,                   false],
        [true,          true,               true,              true,                    false,             false,                   false],
        [true,          true,               true,              true,                    true,              false,                   false],
        [true,          true,               true,              true,                    true,              true,                    true],
        [false,         true,               true,              true,                    true,              true,                    false],
        [true,          false,              true,              true,                    true,              true,                    false],
        [true,          true,               false,             true,                    true,              true,                    false],
        [true,          true,               true,              false,                   true,              true,                    false],
        [true,          true,               true,              true,                    false,             true,                    false],
        [true,          true,               true,              true,                    true,              false,                   false]
      ].each do |truth_table|
        msg = "\nWhen the 'Arturo top level blips feature' is '#{truth_table[indexes[:blips_arturo]]}' and\n" \
              "the 'Arturo for behavioural blips feature' is '#{truth_table[indexes[:behav_blips_arturo]]}' and\n" \
              "the 'Account level blips setting' is '#{truth_table[indexes[:blips_acc_setting]]}' and\n" \
              "the 'Account level behavioural blips setting' is '#{truth_table[indexes[:behav_blips_acc_setting]]}' and\n" \
              "the 'Mobile SDK level blips' setting is '#{truth_table[indexes[:blips_app_setting]]}' then\n" \
              "the 'Mobile SDK level behavioural blips' setting is '#{truth_table[indexes[:behav_blips_app_setting]]}' then\n" \
              "'behavioural_blips_enabled?' must return '#{truth_table[indexes[:expected_result]]}'"
        describe msg do
          it "should be '#{truth_table[indexes[:expected_result]]}'" do
            @account.stubs(:has_mobile_sdk_blips?).returns(truth_table[indexes[:blips_arturo]])
            @account.stubs(:has_mobile_sdk_behavioural_blips?).returns(truth_table[indexes[:behav_blips_arturo]])
            @account.settings.stubs(:mobile_sdk_blips?).returns(truth_table[indexes[:blips_acc_setting]])
            @account.settings.stubs(:mobile_sdk_behavioural_blips?).returns(truth_table[indexes[:behav_blips_acc_setting]])
            @mobile_sdk_app.settings.stubs(:blips?).returns(truth_table[indexes[:blips_app_setting]])
            @mobile_sdk_app.settings.stubs(:behavioural_blips?).returns(truth_table[indexes[:behav_blips_app_setting]])

            assert_equal truth_table[indexes[:expected_result]], @mobile_sdk_app.behavioural_blips_enabled?
          end
        end
      end
    end

    describe "#pathfinder_blips_enabled?" do
      # truth table indenxes mapping
      indexes = {
        blips_arturo: 0,
        path_blips_arturo: 1,
        blips_acc_setting: 2,
        path_blips_acc_setting: 3,
        blips_app_setting: 4,
        path_blips_app_setting: 5,
        expected_result: 6
      }

      [ # blips_arturo, path_blips_arturo, blips_acc_setting, path_blips_acc_setting, blips_app_setting, path_app_setting, expected_result
        [false,         false,             false,             false,                  false,             false,            false],
        [true,          false,             false,             false,                  false,             false,            false],
        [true,          true,              false,             false,                  false,             false,            false],
        [true,          true,              true,              false,                  false,             false,            false],
        [true,          true,              true,              true,                   false,             false,            false],
        [true,          true,              true,              true,                   true,              false,            false],
        [true,          true,              true,              true,                   true,              true,             true],
        [false,         true,              true,              true,                   true,              true,             false],
        [true,          false,             true,              true,                   true,              true,             false],
        [true,          true,              false,             true,                   true,              true,             false],
        [true,          true,              true,              false,                  true,              true,             false],
        [true,          true,              true,              true,                   false,             true,             false],
        [true,          true,              true,              true,                   true,              false,            false]
      ].each do |truth_table|
        msg = "\nWhen the 'Arturo top level blips feature' is '#{truth_table[indexes[:blips_arturo]]}' and\n" \
              "the 'Arturo for pathfinder blips feature' is '#{truth_table[indexes[:path_blips_arturo]]}' and\n" \
              "the 'Account level blips setting' is '#{truth_table[indexes[:blips_acc_setting]]}' and\n" \
              "the 'Account level pathfinder blips setting' is '#{truth_table[indexes[:path_blips_acc_setting]]}' and\n" \
              "the 'Mobile SDK level blips' setting is '#{truth_table[indexes[:blips_app_setting]]}' then\n" \
              "the 'Mobile SDK level pathfinder blips' setting is '#{truth_table[indexes[:path_blips_app_setting]]}' then\n" \
              "'pathfinder_blips_enabled?' must return '#{truth_table[indexes[:expected_result]]}'"
        describe msg do
          it "should be '#{truth_table[indexes[:expected_result]]}'" do
            @account.stubs(:has_mobile_sdk_blips?).returns(truth_table[indexes[:blips_arturo]])
            @account.stubs(:has_mobile_sdk_pathfinder_blips?).returns(truth_table[indexes[:path_blips_arturo]])
            @account.settings.stubs(:mobile_sdk_blips?).returns(truth_table[indexes[:blips_acc_setting]])
            @account.settings.stubs(:mobile_sdk_pathfinder_blips?).returns(truth_table[indexes[:path_blips_acc_setting]])
            @mobile_sdk_app.settings.stubs(:blips?).returns(truth_table[indexes[:blips_app_setting]])
            @mobile_sdk_app.settings.stubs(:pathfinder_blips?).returns(truth_table[indexes[:path_blips_app_setting]])

            assert_equal truth_table[indexes[:expected_result]], @mobile_sdk_app.pathfinder_blips_enabled?
          end
        end
      end
    end
  end

  describe "#support_never_request_email?" do
    before do
      @mobile_sdk_app = mobile_sdk_app
    end

    describe_with_arturo_enabled :mobile_sdk_support_never_request_email_setting do
      describe "and the 'support_never_request_email' setting is true" do
        it do
          @mobile_sdk_app.settings.stubs(:support_never_request_email?).returns(true)
          assert(@mobile_sdk_app.support_never_request_email?)
        end
      end

      describe "and the 'support_never_request_email' setting is: false" do
        it do
          @mobile_sdk_app.settings.stubs(:support_never_request_email?).returns(false)
          assert_equal false, @mobile_sdk_app.support_never_request_email?
        end
      end
    end

    describe_with_arturo_disabled :mobile_sdk_support_never_request_email_setting do
      describe "and the 'support_never_request_email' setting is: true" do
        it do
          @mobile_sdk_app.settings.stubs(:support_never_request_email?).returns(true)
          assert_equal false, @mobile_sdk_app.support_never_request_email?
        end
      end

      describe "and the 'support_never_request_email' setting is: false" do
        it do
          @mobile_sdk_app.settings.stubs(:support_never_request_email?).returns(false)
          assert_equal false, @mobile_sdk_app.support_never_request_email?
        end
      end
    end
  end

  describe "#support_show_closed_requests?" do
    before do
      @mobile_sdk_app = mobile_sdk_app
      @essential_plan = ZBC::Zendesk::PlanType::Essential
      @valid_plans = ZBC::Zendesk::PlanType::TYPES - [@essential_plan]
    end

    describe_with_arturo_enabled :mobile_sdk_support_show_closed_requests_setting do
      [
        ZBC::Zendesk::PlanType::Team,
        ZBC::Zendesk::PlanType::Professional,
        ZBC::Zendesk::PlanType::Enterprise
      ].each do |plan|
        describe "and the account's plan is '#{plan.name}'" do
          before do
            @account.subscription.stubs(:plan_type).returns(plan.plan_type)
          end

          describe "and the 'support_show_closed_requests' setting is true" do
            it do
              @mobile_sdk_app.settings.stubs(:support_show_closed_requests?).returns(true)
              assert(@mobile_sdk_app.support_show_closed_requests?)
            end
          end

          describe "and the 'support_show_closed_requests' setting is: false" do
            it do
              @mobile_sdk_app.settings.stubs(:support_show_closed_requests?).returns(false)
              assert_equal false, @mobile_sdk_app.support_show_closed_requests?
            end
          end
        end
      end

      describe "and the account's plan is 'Essential'" do
        before do
          @account.subscription.stubs(:plan_type).returns(ZBC::Zendesk::PlanType::Essential.plan_type)
        end

        describe "and the 'support_show_closed_requests' setting is true" do
          it do
            @mobile_sdk_app.settings.stubs(:support_show_closed_requests?).returns(true)
            assert(@mobile_sdk_app.support_show_closed_requests?)
          end
        end

        describe "and the 'support_show_closed_requests' setting is: false" do
          it do
            @mobile_sdk_app.settings.stubs(:support_show_closed_requests?).returns(false)
            assert(@mobile_sdk_app.support_show_closed_requests?)
          end
        end
      end
    end

    describe_with_arturo_disabled :mobile_sdk_support_show_closed_requests_setting do
      describe "and the 'support_show_closed_requests' setting is: true" do
        it do
          @mobile_sdk_app.settings.stubs(:support_show_closed_requests?).returns(true)
          assert(@mobile_sdk_app.support_show_closed_requests?)
        end
      end

      describe "and the 'support_show_closed_requests' setting is: false" do
        it do
          @mobile_sdk_app.settings.stubs(:support_show_closed_requests?).returns(false)
          assert(@mobile_sdk_app.support_show_closed_requests?)
        end
      end
    end
  end

  describe "#support_show_referrer_logo?" do
    before do
      @account = accounts(:extra_large)
      @mobile_sdk_app = mobile_sdk_app
    end

    describe_with_arturo_enabled :mobile_sdk_support_show_referrer_logo_setting do
      describe "and the 'support_show_referrer_logo' setting is true" do
        it do
          @mobile_sdk_app.settings.stubs(:support_show_referrer_logo?).returns(true)
          assert(@mobile_sdk_app.support_show_referrer_logo?)
        end
      end

      describe "and the 'support_show_referrer_logo' setting is: false" do
        it do
          @mobile_sdk_app.settings.stubs(:support_show_referrer_logo?).returns(false)
          assert_equal false, @mobile_sdk_app.support_show_referrer_logo?
        end
      end
    end

    describe_with_arturo_disabled :mobile_sdk_support_show_referrer_logo_setting do
      describe "and the 'support_show_referrer_logo' setting is: true" do
        it do
          @mobile_sdk_app.settings.stubs(:support_show_referrer_logo?).returns(true)
          assert(@mobile_sdk_app.support_show_referrer_logo?)
        end
      end

      describe "and the 'support_show_referrer_logo' setting is: false" do
        it do
          @mobile_sdk_app.settings.stubs(:support_show_referrer_logo?).returns(false)
          assert(@mobile_sdk_app.support_show_referrer_logo?)
        end
      end
    end
  end

  describe "#support_system_message" do
    let(:translation_locale) { translation_locales(:english_by_zendesk) }

    before do
      @mobile_sdk_app = mobile_sdk_app
    end

    describe_with_arturo_disabled :mobile_sdk_support_system_message do
      describe "default" do
        it "renders en empty string" do
          @mobile_sdk_app.settings.support_system_message_type = MobileSdkApp::SYSTEM_MESSAGE_TYPE_DEFAULT
          @mobile_sdk_app.save!
          assert_equal "", @mobile_sdk_app.support_system_message(translation_locale, user)
        end
      end

      describe "custom" do
        it "renders en empty string" do
          @mobile_sdk_app.settings.support_system_message_type = MobileSdkApp::SYSTEM_MESSAGE_TYPE_CUSTOM
          @mobile_sdk_app.settings.support_system_message = "custom message"
          @mobile_sdk_app.save!
          assert_equal "", @mobile_sdk_app.support_system_message(translation_locale, user)
        end
      end
    end

    describe_with_arturo_enabled :mobile_sdk_support_system_message do
      describe "default" do
        before do
          @mobile_sdk_app.settings.support_system_message_type = MobileSdkApp::SYSTEM_MESSAGE_TYPE_DEFAULT
          @mobile_sdk_app.save!
        end

        describe "with valid locale" do
          let(:spanish_translation_locale) { translation_locales(:spanish) }

          it "renders the default message localized" do
            # Tested like this because:
            #
            # `I18n.with_locale(locale.locale) { I18n.t('txt.embeddables.support.sdk.default_system_message') }`
            #
            # where `locale` is different than english in the test environment will always return the english
            # string as the other language translations are not available.
            spanish_default_sys_msg = "Su mensaje ha sido enviado. Si se necesita una respuesta, nos comunicaremos en breve."
            I18n.expects(:with_locale).with(spanish_translation_locale.locale).returns(spanish_default_sys_msg)
            assert_equal spanish_default_sys_msg, @mobile_sdk_app.support_system_message(spanish_translation_locale, user)
          end
        end

        # If for some reason the `TranslationLocale` object returns `nil` on it's `locale` method
        describe "with nil 'locale'" do
          it "renders the default message in english" do
            translation_locale.stubs(:locale).returns(nil)
            expected_translation = I18n.with_locale("en") { I18n.t('txt.embeddables.support.sdk.default_system_message') }
            assert_equal expected_translation, @mobile_sdk_app.support_system_message(translation_locale, user)
          end
        end
      end

      describe "custom" do
        before do
          @mobile_sdk_app.settings.support_system_message_type = MobileSdkApp::SYSTEM_MESSAGE_TYPE_CUSTOM
          @mobile_sdk_app.settings.support_system_message = custom_system_message_string
          @mobile_sdk_app.save!
        end

        describe "no dynamic content" do
          let(:custom_system_message_string) { "custom system message" }

          it "renders the custom message" do
            assert_equal custom_system_message_string, @mobile_sdk_app.support_system_message(translation_locale, user)
          end
        end

        describe "dynamic content" do
          let(:custom_system_message_string) { "{{dc.sdk_system_message}}" }
          let(:spanish_translation_locale) { translation_locales(:spanish) }

          before do
            Zendesk::Liquid::DcContext.expects(:render).with(
              "{{dc.sdk_system_message}}",
              @account,
              user,
              'text/plain',
              spanish_translation_locale,
              {}
            ).returns("mensaje de sistema personalizado")
          end

          it "renders the custom message using dc variantions on the provided translation locale" do
            assert_equal "mensaje de sistema personalizado", @mobile_sdk_app.support_system_message(spanish_translation_locale, user)
          end
        end
      end
    end
  end

  describe "#answer_bot_enabled?" do
    before do
      @account = accounts(:extra_large)
      stub_request(:get, "https://extralarge.zendesk-test.com/api/v2/answer_bot/availability?deflection_channel_id=#{Zendesk::Types::ViaType.ANSWER_BOT_FOR_SDK}").
        to_return(status: 200, body: { available: true }.to_json, headers: { 'Content-Type' => 'application/json' })

      @mobile_sdk_app = mobile_sdk_app
    end

    describe_with_arturo_enabled :mobile_sdk_answer_bot do
      describe "and the 'answer_bot' setting is true" do
        it do
          @mobile_sdk_app.settings.stubs(:answer_bot?).returns(true)
          assert(@mobile_sdk_app.answer_bot_enabled?)
        end
      end

      describe "and the 'answer_bot' setting is: false" do
        it do
          @mobile_sdk_app.settings.stubs(:answer_bot?).returns(false)
          assert_equal false, @mobile_sdk_app.answer_bot_enabled?
        end
      end
    end

    describe_with_arturo_disabled :mobile_sdk_answer_bot do
      describe "and the 'answer_bot' setting is: true" do
        it do
          @mobile_sdk_app.settings.stubs(:answer_bot?).returns(true)
          assert_equal false, @mobile_sdk_app.answer_bot_enabled?
        end
      end

      describe "and the 'answer_bot' setting is: false" do
        it do
          @mobile_sdk_app.settings.stubs(:answer_bot?).returns(false)
          assert_equal false, @mobile_sdk_app.answer_bot_enabled?
        end
      end
    end
  end

  describe "#csat" do
    before do
      @account = accounts(:extra_large)
      @mobile_sdk_app = mobile_sdk_app
    end

    describe_with_arturo_setting_enabled :customer_satisfaction do
      describe_with_arturo_enabled :mobile_sdk_csat do
        it "and the 'csat' setting is true it expects true" do # This is the only case where 'csat' can be true in settings
          @mobile_sdk_app.settings.stubs(:support_show_csat?).returns(true)
          assert @mobile_sdk_app.support_show_csat?
        end

        it "and the 'csat' setting is false" do
          @mobile_sdk_app.settings.stubs(:support_show_csat?).returns(false)
          refute @mobile_sdk_app.support_show_csat?
        end
      end

      describe_with_arturo_disabled :mobile_sdk_csat do
        it "and the 'csat' setting is true it expects false" do
          @mobile_sdk_app.settings.stubs(:support_show_csat?).returns(true)
          assert_equal false, @mobile_sdk_app.support_show_csat?
        end

        it "and the 'csat' setting is false it expects false" do
          @mobile_sdk_app.settings.stubs(:support_show_csat?).returns(false)
          refute @mobile_sdk_app.support_show_csat?
        end
      end
    end

    describe_with_arturo_setting_disabled :customer_satisfaction do
      describe_with_arturo_enabled :mobile_sdk_csat do
        it "and the 'csat' setting is true it expects false" do
          @mobile_sdk_app.settings.stubs(:support_show_csat?).returns(true)
          refute @mobile_sdk_app.support_show_csat?
        end

        it "and the 'csat' setting is false it expects false" do
          @mobile_sdk_app.settings.stubs(:support_show_csat?).returns(false)
          refute @mobile_sdk_app.support_show_csat?
        end
      end

      describe_with_arturo_disabled :mobile_sdk_csat do
        it "and the 'csat' setting is true it expects false" do
          @mobile_sdk_app.settings.stubs(:support_show_csat?).returns(true)
          refute @mobile_sdk_app.support_show_csat?
        end

        it "and the 'csat' setting is false it expects false" do
          @mobile_sdk_app.settings.stubs(:support_show_csat?).returns(false)
          refute @mobile_sdk_app.support_show_csat?
        end
      end
    end
  end
end
