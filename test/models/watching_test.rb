require_relative "../support/test_helper"

SingleCov.covered! uncovered: 14

describe Watching do
  fixtures :users, :forums, :accounts
  should_validate_presence_of :source, :user

  let(:watching) { Watching.new source: users(:minimum_agent), user: users(:minimum_agent), account: accounts(:minimum) }

  describe "validations" do
    it "is valid" do
      assert watching.valid?
    end

    it "is invalid with non-unique user_id" do
      existing = watching.dup
      existing.save!
      refute watching.valid?
      assert_equal ["You are already watching this."], watching.errors[:user_id]
    end
  end

  describe "#initialize" do
    it "always generate a token" do
      Token.expects(:generate).returns("testtoken")
      before = watching.token
      watching.save!
      assert_not_equal before, watching.token
    end

    it "includes post (comments)" do
      assert watching.include_posts?
    end

    # TODO: Check why this is triggering 'Can't find first Watching'
    # should validate_uniqueness_of(:user_id).scoped_to([:source_id, :source_type])
  end

  describe "#unsubscribe_posts!" do
    before do
      @watching = Watching.create!(
        source: forums(:solutions),
        account: accounts(:minimum),
        user: users(:minimum_agent)
      )
      @watching.unsubscribe_posts!
    end

    it "does not include post (comments) in watching" do
      assert_equal false, @watching.include_posts?
    end
  end

  describe "#can_view_source?" do
    before do
      @user = users(:minimum_agent)
    end

    it "falses for nil" do
      refute Watching.new(source: nil).can_view_source?(@user)
    end

    it "is true for viewable forums" do
      User.any_instance.stubs(:can?).returns true
      assert Watching.new(source: Forum.new).can_view_source?(@user)
    end

    it "is false for non-viewable forums" do
      User.any_instance.stubs(:can?).returns false
      refute Watching.new(source: Forum.new).can_view_source?(@user)
    end

    it "is true for viewable entries" do
      User.any_instance.stubs(:can?).returns true
      assert Watching.new(source: Entry.new(forum: Forum.new)).can_view_source?(@user)
    end

    it "is false for non-viewable entries" do
      User.any_instance.stubs(:can?).returns false
      refute Watching.new(source: Entry.new(forum: Forum.new)).can_view_source?(@user)
    end
  end
end
