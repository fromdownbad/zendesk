require_relative "../support/test_helper"

SingleCov.covered! uncovered: 17
SingleCov.covered! file: 'lib/zendesk/extensions/soap_http_stream_handler.rb', uncovered: 13

describe MsDynamicsIntegration do
  fixtures :accounts, :users, :user_identities, :tickets
  should_validate_presence_of :user_id, :password, :web_service_address

  before do
    @account                 = accounts(:minimum)
    @user                    = users(:minimum_agent)
    @ticket                  = tickets(:minimum_1)
    @server_address          = "https://crmservername.com:8080/orgname"
    @domain_name             = "domainname"
    @user_id                 = "admin@server.com"
    @password                = "password"
    @web_service_address     = "https://zendeskdynamics.cloudapp.net"
    @discovery_uri           = "https://dev.crm.dynamics.com/XRMServices/2011/Discovery.svc"
    @organization_uri        = "https://dev.crm.dynamics.com/XRMServices/2011/Organization.svc"

    @ms_dynamics_integration = MsDynamicsIntegration.new(
      user_id: @user_id,
      password: @password,
      account: @account,
      web_service_address: @web_service_address,
      server_address: @server_address,
      discovery_uri: @discovery_uri,
      organization_uri: @organization_uri,
      organization_name: "orgname",
      domain_name: @domain_name,
      type_code: 0
    )
  end

  subject { @ms_dynamics_integration }

  describe "#user_info_xml" do
    before do
      @email = @user.email
      @expected_xml = <<-XML
        <InputXml>
          <EmailId>#{@user.email}</EmailId>
          <ZendeskOrganization/>
          <CrmInstallTypeCode>0</CrmInstallTypeCode>
          <IsCRMsecuredsite>1</IsCRMsecuredsite>
          <UserID>#{@user_id}</UserID>
          <Password>#{@password}</Password>
          <DiscoveryUri>#{@discovery_uri}</DiscoveryUri>
          <OrganizationUri>#{@organization_uri}</OrganizationUri>
          <ServerAddress/>
          <CRMservername>crmservername.com</CRMservername>
          <PortNumber>8080</PortNumber>
          <Domainname>domainname</Domainname>
          <OrganizationName>orgname</OrganizationName>
        </InputXml>
      XML
    end

    it "generates the correct input xml" do
      xml = @ms_dynamics_integration.send(:user_info_xml, @user.email)
      xml.gsub(/\s/, "").must_equal @expected_xml.gsub(/\s/, "")
    end
  end

  describe "#fetch_info_for" do
    describe "when we receive results" do
      before do
        @user_info_json = <<-JSON
          {
            "records": {
              "record_type":"contact",
              "fields": {"value":"Some place","label":"Address 1: Country/Region"},
              "url": "/main.aspx?etc=2&extraqs=formid%3d894cc46a-b0cb-4ab0-8bf6-200544e46a2d&id=%7b7849ab2d-c367-e111-b524-1cc1de7983eb%7d&pagetype=entityrecord",
              "id": "7849ab2d-c367-e111-b524-1cc1de7983eb",
              "label": "User"
            }
          }
        JSON
        @hash = {
          records: [{
            label: "User",
            record_type: "contact",
            fields: [{value: "Some place", label: "Address 1: Country/Region"}],
            url: "#{@server_address}/main.aspx?etc=2&extraqs=formid%3d894cc46a-b0cb-4ab0-8bf6-200544e46a2d&id=%7b7849ab2d-c367-e111-b524-1cc1de7983eb%7d&pagetype=entityrecord",
            id: "7849ab2d-c367-e111-b524-1cc1de7983eb"
          }]
        }
      end

      it "converts incoming json to label/value hash" do
        @ms_dynamics_integration.stubs(:get_user_info).returns(@user_info_json)
        data = @ms_dynamics_integration.fetch_info_for(@user)
        assert_equal(@hash, data)
      end

      describe "when a record has no fields" do
        before do
          @user_info_json = <<-JSON
            {
              "records": {
                "record_type": "contact",
                "url": "/main.aspx?etc=2&extraqs=formid%3d894cc46a-b0cb-4ab0-8bf6-200544e46a2d&id=%7b7849ab2d-c367-e111-b524-1cc1de7983eb%7d&pagetype=entityrecord",
                "id": "7849ab2d-c367-e111-b524-1cc1de7983eb",
                "label": "User"
              }
            }
          JSON
          @ms_dynamics_integration.stubs(:get_user_info).returns(@user_info_json)
        end

        it "does not throw an exception" do
          @ms_dynamics_integration.fetch_info_for(@user)
        end
      end
    end

    describe "when we receive NO results" do
      before do
        @user_info_json = '{"code":"0","message":"No user profile was found in the search"}'
        @hash = { records: [] }
      end

      it "converts incoming json to label/value hash" do
        @ms_dynamics_integration.stubs(:get_user_info).returns(@user_info_json)
        data = @ms_dynamics_integration.fetch_info_for(@user)
        assert_equal(@hash, data)
      end
    end
  end

  describe "#test_connection" do
    describe "when we receive results" do
      before do
        @json = '{"records":[{"record_type":"systemuser","fields":{"value":"c3034d5b-c368-4977-ab09-6f5adc54a8a2","label":"User"}}]}'
      end

      it "does not raise an exception because there are records" do
        @ms_dynamics_integration.stubs(:validate_connection).returns(@json)
        @ms_dynamics_integration.test_connection
      end
    end

    describe "when we receive NO results" do
      before do
        @json = '{"code":"0","message":"Unable to connect to MS CRM Instance. Error: An unsecured or incorrectly secured fault was received from the other party. See the inner FaultException for the fault code and detail.\n"}'
      end

      it "raises an exception" do
        @ms_dynamics_integration.stubs(:validate_connection).returns(@json)
        assert_raise(RuntimeError, 'Unable to connect to MS CRM Instance') { @ms_dynamics_integration.test_connection }
      end
    end
  end

  describe "following redirections" do
    let(:url_for_redirection) { "http://example.com/other/path/1" }
    let(:redirection_response) { stub(status: 302, header: { "location" => [url_for_redirection] }) }

    before do
      SOAP::HTTPStreamHandler.any_instance.stubs(:do_post_call).returns(redirection_response)
    end

    it "passes when it does not redirect" do
      Zendesk::Net::AddressUtil.expects(:safe_url?).with(url_for_redirection).returns(true).at_least_once
      SOAP::HTTPStreamHandler.any_instance.expects(:do_post_call).with(url_for_redirection, anything, anything).at_least_once.returns(
        stub(status: 200, content: "", contenttype: "application/json")
      )

      assert_raises SOAP::EmptyResponseError do
        @ms_dynamics_integration.test_connection
      end
    end

    it "does not raise a 'redirection url not safe' exception when the url we're following is safe, it redirects until we exceed the maximum calls" do
      Zendesk::Net::AddressUtil.expects(:safe_url?).with(url_for_redirection).returns(true).at_least_once
      SOAP::HTTPStreamHandler.any_instance.expects(:do_post_call).with(url_for_redirection, anything, anything).at_least_once.returns(redirection_response)

      e = assert_raise(SOAP::HTTPStreamError) { @ms_dynamics_integration.test_connection }
      assert_equal 'redirect count exceeded', e.message
    end

    it "raises a 'redirection url not safe' exception when the url we're following is not safe" do
      Zendesk::Net::AddressUtil.expects(:safe_url?).with(url_for_redirection).returns(false)
      SOAP::HTTPStreamHandler.any_instance.expects(:do_post_call).with(url_for_redirection, anything, anything).never

      e = assert_raise(SOAP::HTTPStreamError) { @ms_dynamics_integration.test_connection }
      assert_equal 'The redirection url is not safe', e.message
    end
  end

  describe "#ticket_xml" do
    before do
      @ticket.stubs(:ticket_type_id).returns(0)
      @ticket.stubs(:priority).returns("-")
      @expected_xml = <<~XML
        <InputXml>
          <CrmRecordType>C</CrmRecordType>
          <CrmRecordId>1234</CrmRecordId>
          <ContactLastName>#{@ticket.requester.last_name}</ContactLastName>
          <ContactEmailId>#{@ticket.requester.email}</ContactEmailId>
          <ZD_CaseTypeCode>No Value</ZD_CaseTypeCode>
          <ZD_Description>#{@ticket.description}</ZD_Description>
          <ZD_StatusCode>#{@ticket.status}</ZD_StatusCode>
          <ZD_PriorityCode>No Value</ZD_PriorityCode>
          <ZD_Title>#{@ticket.subject}</ZD_Title>
          <CrmInstallTypeCode>0</CrmInstallTypeCode>
          <IsCRMsecuredsite>1</IsCRMsecuredsite>
          <UserID>#{@user_id}</UserID>
          <Password>#{@password}</Password>
          <DiscoveryUri>#{@discovery_uri}</DiscoveryUri>
          <OrganizationUri>#{@organization_uri}</OrganizationUri>
          <ServerAddress/>
          <CRMservername>crmservername.com</CRMservername>
          <PortNumber>8080</PortNumber>
          <Domainname>domainname</Domainname>
          <OrganizationName>orgname</OrganizationName>
        </InputXml>
      XML
    end

    it "generates the correct input xml" do
      xml = @ms_dynamics_integration.send(:ticket_xml, @ticket, "1234")
      xml.gsub(/\s/, "").must_equal @expected_xml.gsub(/\s/, "")
    end
  end

  describe "#parse_ticket_response" do
    describe "when the ticket was created" do
      before do
        @response = '{"code":"1","message":"CAS-01036-G1G0M7"}'
      end

      it "returns the case id" do
        id = @ms_dynamics_integration.send(:parse_ticket_response, @response)
        assert_equal("CAS-01036-G1G0M7", id)
      end
    end

    describe "when the ticket creation failed" do
      before do
        @response = '{"code":"0","message":"Some Error\n"}'
      end

      it "raises an exception" do
        assert_raise(RuntimeError) { @ms_dynamics_integration.send(:parse_ticket_response, @response) }
      end
    end
  end

  describe "#driver" do
    it "creates a soap adapter with the correct web service address" do
      web_service = "https://zendeskdynamics.cloudapp.net/MSCRMAdapter.asmx"
      MsDynamics::Integration::MSCRMAdapterSoap.expects(:new).with(web_service)
      @ms_dynamics_integration.send(:driver)
    end
  end

  describe "#additional_domains_for_policy" do
    it "parses additional_domains string and return an array of domains" do
      @ms_dynamics_integration.stubs(:additional_domains).returns("http://domain1.com, http://domain2.com")
      assert_equal ["http://domain1.com", "http://domain2.com"], @ms_dynamics_integration.additional_domains_for_policy

      @ms_dynamics_integration.stubs(:additional_domains).returns("http://domain1.com,http://domain2.com")
      assert_equal ["http://domain1.com", "http://domain2.com"], @ms_dynamics_integration.additional_domains_for_policy

      @ms_dynamics_integration.stubs(:additional_domains).returns("http://domain1.com")
      assert_equal ["http://domain1.com"], @ms_dynamics_integration.additional_domains_for_policy

      @ms_dynamics_integration.stubs(:additional_domains).returns(nil)
      assert_equal [], @ms_dynamics_integration.additional_domains_for_policy
    end
  end

  describe "#destroy" do
    it "creates and destroy the target and trigger" do
      MsDynamicsIntegration.any_instance.stubs(:configured?).returns(true)
      MsDynamicsTarget.destroy_all

      assert_equal 0, MsDynamicsTarget.count(:all)
      assert @ms_dynamics_integration.save
      assert_equal 1, MsDynamicsTarget.count(:all)
      target = MsDynamicsTarget.last
      assert target.is_active?
      trigger = Trigger.find_by_title(target.send(:title_for_trigger))
      refute trigger.is_active?

      assert @ms_dynamics_integration.destroy
      assert_equal 0, MsDynamicsTarget.count(:all)
      refute Trigger.find_by_title(target.send(:title_for_trigger))
    end
  end
end
