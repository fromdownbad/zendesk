require_relative "../support/test_helper"
require_relative "../support/arturo_test_helper"

SingleCov.covered!

describe WorkspaceElement do
  include ArturoTestHelper
  fixtures :accounts, :workspaces, :rules

  let(:account) { accounts(:minimum) }
  let(:workspace) { workspaces(:refund_workspace) }
  let(:macro) { rules(:macro_for_minimum_agent) }

  it "must have account_id, workspace_id, element_id, and element_type" do
    workspace_element = WorkspaceElement.new
    refute workspace_element.save
    assert workspace_element.errors.full_messages.join.must_include "Account cannot be blank"
    assert workspace_element.errors.full_messages.join.must_include "Workspace cannot be blank"
    assert workspace_element.errors.full_messages.join.must_include "Element cannot be blank"
    assert workspace_element.errors.full_messages.join.must_include "Element type cannot be blank"
  end

  it "should retrieve the associated workspace and element" do
    workspace_element = account.workspace_elements.new
    workspace_element.workspace_id = workspace.id
    workspace_element.element_id = macro.id
    workspace_element.element_type = "Macro"
    workspace_element.save!

    assert_equal workspace_element.workspace.id, workspace.id
    assert_equal workspace_element.element.id, macro.id
  end

  describe "#build_app_element_with" do
    it "builds an app using the attributes" do
      app_attributes = { account_id: account.id, workspace_id: workspace.id, id: 1, expand: true, position: 4 }

      ws_element = account.workspace_elements.new
      ws_element.build_app_element_with(app_attributes)

      assert ws_element
      assert_equal app_attributes[:account_id], ws_element.account_id
      assert_equal app_attributes[:workspace_id], ws_element.workspace_id
      assert_equal app_attributes[:id], ws_element.element_id
      assert_equal app_attributes[:expand], ws_element.expand
      assert_equal app_attributes[:position], ws_element.position
      assert_equal "App", ws_element.element_type
    end

    it "returns nil when attributes is not of type 'Hash'" do
      ws_element = account.workspace_elements.new
      ws_element.build_app_element_with([])

      refute ws_element.valid?
    end
  end

  describe "#app?" do
    it "returns true for element type 'App'" do
      ws_element = account.workspace_elements.new
      ws_element.element_type = 'App'

      assert ws_element.app?
    end

    it "returns false for element type 'App'" do
      ws_element = account.workspace_elements.new
      ws_element.element_type = 'Macro'

      refute ws_element.app?
    end
  end

  describe "#macro?" do
    it "returns true for element type 'Macro'" do
      ws_element = account.workspace_elements.new
      ws_element.element_type = 'Macro'

      assert ws_element.macro?
    end

    it "returns false for element type 'Macro'" do
      ws_element = account.workspace_elements.new
      ws_element.element_type = 'App'

      refute ws_element.macro?
    end
  end

  describe "#app_info_for_admin_page" do
    it "returns app's id, expand and position attributes" do
      app_attributes = { account_id: account.id, workspace_id: workspace.id, id: 1, expand: true, position: 4 }

      ws_element = account.workspace_elements.new
      ws_element.build_app_element_with(app_attributes)

      assert_equal ws_element.app_info_for_admin_page, id: 1, expand: true, position: 4
    end
  end
end
