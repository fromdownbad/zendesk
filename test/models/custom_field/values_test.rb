require_relative "../../support/test_helper"

SingleCov.not_covered! # test random bits on various custom fields

describe CustomField do
  include CustomFieldsTestHelper
  fixtures :accounts, :subscriptions, :users, :organizations, :credit_cards

  before do
    # don't use these fixtures
    CustomField::Field.without_arsi.delete_all
    CustomField::Value.without_arsi.delete_all
    Account.any_instance.stubs(:has_user_and_organization_fields?).returns(true)
    @account = accounts(:minimum)
    @custom_field = create_user_custom_field!("field_1", "Field 1", "Text")
    @custom_field_org = create_organization_custom_field!("field_1", "Field 1", "Text")
    @custom_field_org2 = create_organization_custom_field!("field_22", "Field 1", "Text")
    @user = users(:minimum_end_user)
  end

  describe "validations" do
    describe "decimal field" do
      before do
        create_user_custom_field!("field_decimal", "decimal field", "Decimal")
      end

      it "rejects some stuff as invalid" do
        @user.custom_field_values.update_from_hash("field_decimal" => "hello, no")
        refute @user.save
        assert @user.errors["custom_field_values.base"].any?
        @user.errors.full_messages.join.must_include "expected a floating point number"
        @user.errors.full_messages.join.must_include "decimal field"
      end
    end

    describe "checkbox fields" do
      before do
        create_user_custom_field!("field_checkbox", "checkbox field", "Checkbox")
      end

      ["true", "false", true, false, "1", "0"].each do |valid|
        it "accepts #{valid.inspect} as valid" do
          @user.custom_field_values.update_from_hash("field_checkbox" => valid)
          assert @user.save
        end
      end

      it "does not accept random garbage" do
        @user.custom_field_values.update_from_hash("field_checkbox" => true)
        assert @user.save
        @user.custom_field_values.update_from_hash("field_checkbox" => "no, not this")
        refute @user.save
        assert @user.errors["custom_field_values.base"].any?
        @user.errors.full_messages.join.must_include "expected true"
        @user.errors.full_messages.join.must_include "checkbox field"
      end
    end

    describe "integer fields" do
      before do
        create_user_custom_field!("field_integer", "integer field", "Integer")
      end

      ["123", "-123", 123, 900, "0123", "0888"].each do |valid|
        it "accepts #{valid.inspect} as valid" do
          @user.custom_field_values.update_from_hash("field_integer" => valid)
          assert @user.save!
        end
      end

      ["0.009123", "hello", true, 0.123].each do |invalid|
        it "does not accept #{invalid}" do
          @user.custom_field_values.update_from_hash("field_integer" => invalid)
          refute @user.save
          assert @user.errors["custom_field_values.base"].any?
          @user.errors.full_messages.join.must_include "expected an integer"
          @user.errors.full_messages.join.must_include "integer field is invalid"
        end
      end
    end

    describe "date fields" do
      before do
        create_user_custom_field!("field_date", "date field", "Date")
      end

      ["2012-01-01", Time.now, "2009-07-20T22:55:29Z", "0000-01-01", "0000-01-01T00:00:00Z"].each do |valid|
        it "accepts #{valid.inspect} as valid" do
          @user.custom_field_values.update_from_hash("field_date" => valid)
          assert @user.save!
        end
      end

      ["0.009123", "hello", "2012", "20109-07-20T22:55:29Z", "2009-00-00T00:00:00Z"].each do |invalid|
        it "does not accept #{invalid}" do
          @user.custom_field_values.update_from_hash("field_date" => invalid)
          refute @user.save
          assert @user.errors["custom_field_values.base"].any?
          @user.errors.full_messages.join.must_include "expected a date"
          @user.errors.full_messages.join.must_include "date field is invalid"
        end
      end
    end

    describe "regexp fields" do
      before do
        cf = create_user_custom_field!("field_regexp", "regexp field", "Regexp")
        cf.regexp_for_validation = '^[a-z]\d+$'
        cf.save!
      end

      ["a123", "b4421", "c1123"].each do |valid|
        it "accepts #{valid.inspect} as valid" do
          @user.custom_field_values.update_from_hash("field_regexp" => valid)
          assert @user.save!
        end
      end

      ["0.009123", "hello", 0.123, "2012", true].each do |invalid|
        it "does not accept #{invalid}" do
          @user.custom_field_values.update_from_hash("field_regexp" => invalid)
          refute @user.save
          assert @user.errors["custom_field_values.base"].any?
          @user.errors.full_messages.join.must_include "regexp field is invalid"
        end
      end

      describe "with SafeRegexp" do
        before do
          @field = create_user_custom_field!("slow_field_regexp", "regexp field", "Regexp")
          @field.regexp_for_validation = 'aa?' * 84
          @field.save!
          value = 'a' * 40
          @user.custom_field_values.update_from_hash(slow_field_regexp: value)
        end

        describe_with_arturo_disabled :use_safe_regexp do
          it "refutes the match successfully" do
            time = Benchmark.realtime { refute @user.save }
            assert_operator time, :>, 2
          end
        end

        describe_with_arturo_enabled :use_safe_regexp do
          it "refutes the match with reporting, logging, and a nice error message" do
            Rails.logger.expects(:warn).with(regexp_matches(/SafeRegexp::RegexpTimeout: CustomField::Regexp: account_id: #{@account.id}, field id: #{@field.id}/))
            Zendesk::StatsD::Client.any_instance.expects(:increment).with("safe_regexp_regexp_timeout", tags: ["type:CustomField::Regexp", "account_id:#{@field.account_id}"])

            time = Benchmark.realtime { refute @user.save }
            assert_operator time, :<, 2
            @user.errors.full_messages.join.must_include "Custom field values base The regex field should be modified to prevent slow regex matching. Field id: #{@field.id}"
          end

          it "falls back to traditional regex.match if safe_regexp raises Errno::ENOMEM" do
            Rails.logger.expects(:warn).with(regexp_matches(/Errno::ENOMEM: CustomField::Regexp: account_id: #{@account.id}, field id: #{@field.id}/))
            Zendesk::StatsD::Client.any_instance.expects(:increment).with("safe_regexp_enomem", tags: ["type:CustomField::Regexp", "account_id:#{@field.account_id}"])
            SafeRegexp.stubs(:execute).raises(Errno::ENOMEM)
            value = 'a'
            @user.custom_field_values.update_from_hash(slow_field_regexp: value)

            refute @user.save
            assert @user.errors["custom_field_values.base"].any?
            @user.errors.full_messages.join.must_include "regexp field is invalid"
          end

          it "reports a successful safe_regexp match" do
            @user.custom_field_values.field_changes.each do |cf_change|
              @user.add_domain_event(UserCustomFieldChangedProtobufEncoder.new(cf_change[0], cf_change[1], cf_change[2]).to_safe_object)
            end
            Zendesk::StatsD::Client.any_instance.expects(:increment).with("domain_event", tags: ["event:time_zone_changed"])
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('domain_event', tags: ['event:custom_field_changed'])
            Zendesk::StatsD::Client.any_instance.expects(:increment).with("safe_regexp_success", tags: ["type:CustomField::Regexp"])
            @user.custom_field_values.update_from_hash(field_regexp: 'a123', slow_field_regexp: nil)

            assert @user.save
          end
        end
      end
    end

    describe "dropdown fields" do
      before do
        @cf = create_user_custom_field!("field_dropdown", "dropdown field", "Dropdown")
        @choice_1 = @cf.dropdown_choices.build(name: "Choice 1", value: "choice_1")
        @choice_2 = @cf.dropdown_choices.build(name: "Choice 2", value: "choice_2")
        @cf.dropdown_choices.build(name: "Choice 3", value: "choice_3")
        @cf.dropdown_choices.build(name: "Choice 4", value: "choice_4")
        @cf.save!
      end

      it "accepts a key as valid" do
        @user.custom_field_values.update_from_hash("field_dropdown" => "choice_1")
        assert @user.save
      end

      it "accepts a string representation of a dropdown_choice_id as valid" do
        @user.custom_field_values.update_from_hash("field_dropdown" => @choice_1.id.to_s)
        assert @user.save!
      end

      ["0.009123", "hello", 0.123, true].each do |invalid|
        it "does not accept #{invalid}" do
          @user.custom_field_values.update_from_hash("field_dropdown" => invalid)
          refute @user.save
          assert @user.errors["custom_field_values.base"].any?
          assert_match /not a valid drop-down option/, @user.errors.full_messages.join
          @user.errors.full_messages.join.must_include "dropdown field is invalid"
        end
      end

      # technically I'm actually testing standard rails behavior here, but what.the.hell.
      describe "when a dropdown choice is deleted" do
        before do
          @user.custom_field_values.update_from_hash("field_dropdown" => "choice_1")
          @user.save!
          @choice_1.destroy
          @user.reload
        end

        it "is valid when the value is not changed" do
          assert @user.valid?
          assert @user.save!
        end
      end

      describe "which is system" do
        before do
          @cf = create_user_custom_field!("system::field_dropdown", "txt.dropdown", "Dropdown", is_system: true)
        end

        it "does not be created if it's dropdownchoice name isn't I18n string" do
          @cf.dropdown_choices.build(name: "Choice 10", value: "choice_10")
          assert_equal false, @cf.save
          assert_includes @cf.errors.messages[:"dropdown_choices.title"], "Names of dropdown choices of system fields must be i18n keys as they will be translated in UI wherever they are shown."
        end

        it "is created successfully  if it's dropdownchoice name is a I18n string" do
          @cf.dropdown_choices.build(name: "txt.choice_10", value: "choice_10")
          assert(@cf.save)
        end
      end
    end

    describe "bytesize limits" do
      let (:user) { users(:minimum_end_user) }

      before do
        create_user_custom_field!("field_text", "text field", "Text")
        text = 'a' * (ENV.fetch("CUSTOM_FIELD_VALUE_SIZE_LIMIT_IN_BYTES", 125000) + 1)
        user.custom_field_values.update_from_hash("field_text" => text)
      end

      describe_with_arturo_enabled :enforce_user_and_org_field_size_limit do
        it "raises a validation error" do
          refute user.save
          assert user.errors["custom_field_values.base"].any?
          user.errors.full_messages.join.must_include "is too large. Limit 125 kilobytes."
        end

        it "increments datadog" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(
            "over_bytesize_limit", tags: ["account_id:#{user.account_id}", "limit:#{ENV.fetch("CUSTOM_FIELD_VALUE_SIZE_LIMIT_IN_BYTES", 125000) / 1000}kb"]
          )

          user.save
        end
      end

      describe_with_arturo_disabled :enforce_user_and_org_field_size_limit do
        it "doesn't raise a validation error" do
          assert user.save!
        end
      end
    end
  end

  describe "comparisons" do
    describe "decimal field" do
      before do
        create_user_custom_field!("field_decimal", "decimal field", "Decimal")
        @user.custom_field_values.update_from_hash("field_decimal" => 123.45)
        @user.save!
        @user.reload
        @field = @user.custom_field_values.value_for_key("field_decimal").field
      end

      it "'is's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is", 123.45)
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is", "123.45")
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is", "0000000000123.4500")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is", 0.0)
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is", nil)

        # if no value set
        assert @field.compare(nil, "is", nil)
        assert @field.compare(nil, "is", "")
        refute @field.compare(nil, "is", 0.0)
      end

      it "'is_not's should work as expected" do
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is_not", 123.45)
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is_not", "123.45")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is_not", "0000000000123.4500")
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is_not", 0.0)
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "is_not", nil)

        # if no value set
        refute @field.compare(nil, "is_not", nil)
        refute @field.compare(nil, "is_not", "")
        assert @field.compare(nil, "is_not", 0.0)
      end

      it "'less_than's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "less_than", 123.46)
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "less_than", "123.46")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "less_than", "-000000000123")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "less_than", nil)

        # if no value set
        refute @field.compare(nil, "less_than", nil)
        refute @field.compare(nil, "less_than", 0.0)
      end

      it "'greater_than's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "greater_than", -123.46)
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "greater_than", "-123.46")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "greater_than", "0000000000124")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "greater_than", nil)

        # if no value set
        refute @field.compare(nil, "greater_than", nil)
        refute @field.compare(nil, "greater_than", 0.0)
      end

      it "'less_than_equal's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "less_than_equal", 123.45)
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "less_than_equal", "123.45")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "less_than_equal", "0000000000123")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "less_than_equal", nil)

        # if no value set
        refute @field.compare(nil, "less_than_equal", nil)
        refute @field.compare(nil, "less_than_equal", 0.0)
      end

      it "'greater_than_equal's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "greater_than_equal", 123.45)
        assert @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "greater_than_equal", "123.45")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "greater_than_equal", "0000000000124")
        refute @field.compare(@user.custom_field_values.value_for_key("field_decimal").value, "greater_than_equal", nil)

        # if no value set
        refute @field.compare(nil, "greater_than_equal", nil)
        refute @field.compare(nil, "greater_than_equal", 0.0)
      end
    end

    describe "integer field" do
      before do
        create_user_custom_field!("field_integer", "integer field", "Integer")
        @user.custom_field_values.update_from_hash("field_integer" => 123)
        @user.save!
        @user.reload
        @field = @user.custom_field_values.value_for_key("field_integer").field
      end

      it "'is's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is", 123)
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is", "123")
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is", "0000000000123")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is", 0.0)
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is", nil)

        # if no value set
        assert @field.compare(nil, "is", nil)
        assert @field.compare(nil, "is", "")
        refute @field.compare(nil, "is", 0)
      end

      it "'is_not's should work as expected" do
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is_not", 123)
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is_not", "123")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is_not", "0000000000123")
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is_not", 0.0)
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "is_not", nil)

        # if no value set
        refute @field.compare(nil, "is_not", nil)
        refute @field.compare(nil, "is_not", "")
        assert @field.compare(nil, "is_not", 0)
      end

      it "'less_than's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "less_than", 124)
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "less_than", "124")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "less_than", "-000000000124")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "less_than", nil)

        # if no value set
        refute @field.compare(nil, "less_than", nil)
        refute @field.compare(nil, "less_than", 0)
      end

      it "'greater_than's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "greater_than", -1)
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "greater_than", "-1")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "greater_than", "0000000000124")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "greater_than", nil)

        # if no value set
        refute @field.compare(nil, "greater_than", nil)
        refute @field.compare(nil, "greater_than", 0)
      end

      it "'less_than_equal's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "less_than_equal", 123)
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "less_than_equal", "123")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "less_than_equal", "0000000000122")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "less_than_equal", nil)

        # if no value set
        refute @field.compare(nil, "less_than_equal", nil)
        refute @field.compare(nil, "less_than_equal", 0)
      end

      it "'greater_than_equal's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "greater_than_equal", 123)
        assert @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "greater_than_equal", "123")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "greater_than_equal", "0000000000124")
        refute @field.compare(@user.custom_field_values.value_for_key("field_integer").value, "greater_than_equal", nil)

        # if no value set
        refute @field.compare(nil, "greater_than_equal", nil)
        refute @field.compare(nil, "greater_than_equal", 0)
      end
    end

    describe "text field" do
      before do
        @thetext = "Something different"
        @text2 = "Rotten eggs"
        create_user_custom_field!("field_text", "text field", "Text")
        @user.custom_field_values.update_from_hash("field_text" => @thetext)
        @user.save!
        @user.reload
        @field = @user.custom_field_values.value_for_key("field_text").field
      end

      it "'is's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_text").value, "is", "something Different  ")
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "is", @text2)
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "is", nil)

        # if no value set
        assert @field.compare(nil, "is", nil)
        assert @field.compare(nil, "is", "")
        refute @field.compare(nil, "is", @text2)
      end

      it "'is_not's should work as expected" do
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "is_not", "something Different  ")
        assert @field.compare(@user.custom_field_values.value_for_key("field_text").value, "is_not", @text2)
        assert @field.compare(@user.custom_field_values.value_for_key("field_text").value, "is_not", nil)

        # if no value set
        refute @field.compare(nil, "is_not", nil)
        refute @field.compare(nil, "is_not", "")
        assert @field.compare(nil, "is_not", @text2)
      end

      it "'less_than's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_text").value, "less_than", "toaster")
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "less_than", @thetext)
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "less_than", @text2)
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "less_than", nil)

        # if no value set
        refute @field.compare(nil, "less_than", nil)
        refute @field.compare(nil, "less_than", "abc")
      end

      it "'greater_than's should work as expected" do
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "greater_than", "toaster")
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "greater_than", @thetext)
        assert @field.compare(@user.custom_field_values.value_for_key("field_text").value, "greater_than", @text2)
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "greater_than", nil)

        # if no value set
        refute @field.compare(nil, "greater_than", nil)
        refute @field.compare(nil, "greater_than", "abc")
      end

      it "'less_than_equal's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_text").value, "less_than_equal", "toaster")
        assert @field.compare(@user.custom_field_values.value_for_key("field_text").value, "less_than_equal", @thetext)
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "less_than_equal", @text2)
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "less_than_equal", nil)

        # if no value set
        refute @field.compare(nil, "less_than_equal", nil)
        refute @field.compare(nil, "less_than_equal", "abc")
      end

      it "'greater_than_equal's should work as expected" do
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "greater_than_equal", "toaster")
        assert @field.compare(@user.custom_field_values.value_for_key("field_text").value, "greater_than_equal", @thetext)
        assert @field.compare(@user.custom_field_values.value_for_key("field_text").value, "greater_than_equal", @text2)
        refute @field.compare(@user.custom_field_values.value_for_key("field_text").value, "greater_than_equal", nil)

        # if no value set
        refute @field.compare(nil, "greater_than_equal", nil)
        refute @field.compare(nil, "greater_than_equal", "abc")
      end
    end

    describe "checkbox field" do
      before do
        @field = create_user_custom_field!("field_checkbox", "checkbox field", "Checkbox")
        @user.custom_field_values.update_from_hash("field_checkbox" => false)
        @user.save!
        @user.reload
      end

      it "'is's should work as expected" do
        assert_nil @user.custom_field_values.value_for_key("field_checkbox")
        refute @field.compare(nil, "is", true)
        assert @field.compare(nil, "is", "false")
        assert @field.compare(nil, "is", false)
        assert @field.compare(nil, "is", nil)
      end

      it "'is_not's should work as expected" do
        assert_nil @user.custom_field_values.value_for_key("field_checkbox")
        assert @field.compare(nil, "is_not", true)
        refute @field.compare(nil, "is_not", "false")
        refute @field.compare(nil, "is_not", false)
        refute @field.compare(nil, "is_not", nil)
      end

      # NOTE: <, <=, >, >= will blow up since true and false can not be compared with these operators
    end

    describe "dropdown field" do
      before do
        @cf = create_user_custom_field!("field_dropdown", "dropdown field", "Dropdown")
        @choice_1 = @cf.dropdown_choices.build(name: "Choice 1", value: "choice_1")
        @choice_2 = @cf.dropdown_choices.build(name: "Choice 2", value: "choice_2")
        @cf.dropdown_choices.build(name: "Choice 3", value: "choice_3")
        @cf.dropdown_choices.build(name: "Choice 4", value: "choice_4")
        @cf.save!

        @user.custom_field_values.update_from_hash("field_dropdown" => "choice_2")
        @user.save!
        @user.reload
        @field = @user.custom_field_values.value_for_key("field_dropdown").field
      end

      it "'is's should work as expected" do
        refute @field.compare(@user.custom_field_values.value_for_key("field_dropdown").value, "is", "choice_1")
        assert @field.compare(@user.custom_field_values.value_for_key("field_dropdown").value, "is", "choice_2")
        assert @field.compare(@user.custom_field_values.value_for_key("field_dropdown").value, "is", @choice_2.id.to_s)
        refute @field.compare(@user.custom_field_values.value_for_key("field_dropdown").value, "is", nil)

        # if no value set
        assert @field.compare(nil, "is", nil)
        assert @field.compare(nil, "is", "")
        refute @field.compare(nil, "is", "choice_2")
      end

      it "'is_not's should work as expected" do
        assert @field.compare(@user.custom_field_values.value_for_key("field_dropdown").value, "is_not", "choice_1")
        refute @field.compare(@user.custom_field_values.value_for_key("field_dropdown").value, "is_not", "choice_2")
        refute @field.compare(@user.custom_field_values.value_for_key("field_dropdown").value, "is_not", @choice_2.id.to_s)
        assert @field.compare(@user.custom_field_values.value_for_key("field_dropdown").value, "is_not", nil)

        # if no value set
        refute @field.compare(nil, "is_not", nil)
        refute @field.compare(nil, "is_not", "")
        assert @field.compare(nil, "is_not", "choice_2")
      end

      # NOTE: <, <=, >, >= will returned garbage since they are comparing unordered id's
    end
  end
end
