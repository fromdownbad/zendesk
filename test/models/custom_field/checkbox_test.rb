require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 10

describe 'CustomField::Checkbox' do
  truthy_values = CustomField::Checkbox::TRUTHY_VALUES
  falsy_values = CustomField::Checkbox::FALSY_VALUES

  describe "#value_as_json" do
    it "returns true for truthy values" do
      truthy_values.each do |value|
        assert CustomField::Checkbox.new.value_as_json(value)
      end
    end

    it "returns false for falsy values" do
      falsy_values.each do |value|
        refute CustomField::Checkbox.new.value_as_json(value)
      end
    end
  end

  describe "#cast_value_for_db" do
    it "returns true for truthy values" do
      truthy_values.each do |value|
        assert_equal "1", CustomField::Checkbox.new.cast_value_for_db(value)
      end
    end

    it "returns false for falsy values" do
      falsy_values.each do |value|
        assert_nil CustomField::Checkbox.new.cast_value_for_db(value)
      end
    end
  end

  describe "#normalize_value" do
    it "returns 1 for truthy values" do
      truthy_values.each do |value|
        assert_equal "1", CustomField::Checkbox.new.normalize_value(value)
      end
    end

    it "returns 0 for falsy values" do
      falsy_values.each do |value|
        assert_equal "0", CustomField::Checkbox.new.normalize_value(value)
      end
    end

    it "returns 0 for falsy strings" do
      assert_equal "0", CustomField::Checkbox.new.normalize_value("off")
      assert_equal "0", CustomField::Checkbox.new.normalize_value("OFF")
      assert_equal "0", CustomField::Checkbox.new.normalize_value("only_true_is_true")
    end

    it "returns 1 for true strings" do
      assert_equal "1", CustomField::Checkbox.new.normalize_value("on")
      assert_equal "1", CustomField::Checkbox.new.normalize_value("ON")
    end
  end

  describe "#is_blank?" do
    it "returns false for truthy values" do
      truthy_values.each do |value|
        refute CustomField::Checkbox.new.is_blank?(value)
      end
    end

    it "returns true for falsy values" do
      falsy_values.each do |value|
        assert CustomField::Checkbox.new.is_blank?(value)
      end
    end

    it "returns true for nil values" do
      falsy_values.each do |_value|
        assert CustomField::Checkbox.new.is_blank?(nil)
      end
    end
  end
end
