require_relative "../../support/test_helper"

SingleCov.not_covered! # covers a few random parts of custom fields

describe 'CustomFieldTags' do
  include CustomFieldsTestHelper
  fixtures :all

  before do
    # don't use these fixtures
    CustomField::Field.without_arsi.delete_all
    CustomField::Value.without_arsi.delete_all
    CustomField::Dropdown.without_arsi.delete_all

    @account = accounts(:minimum)
    @account.settings.has_user_tags = true
    @account.save!

    @user = users(:minimum_end_user)

    @dropdown = create_user_custom_field!("field_dropdown", "dropdown field", "Dropdown")
    @choice_1 = @dropdown.dropdown_choices.build(name: "Choice 1", value: "choice_1")
    @choice_2 = @dropdown.dropdown_choices.build(name: "Choice 2", value: "choice_2")
    @dropdown.save!

    @checkbox = create_user_custom_field!("field_checkbox", "Nothing Here", "Checkbox")
    @checkbox.tag = "moose"
    @checkbox.save!
  end

  describe "Tags testing" do
    describe "Dropdown and checkbox" do
      before do
        @save_tags = @user.tags
        @user.custom_field_values.update_from_hash("field_dropdown" => "choice_2", "field_checkbox" => true)
        @user.save!
      end

      it "has correct tags" do
        assert_equal((@save_tags + ["choice_2", "moose"]).sort, @user.tags.sort)
      end
    end

    describe "tags are only for checkboxes" do
      before do
        @decimal = create_user_custom_field!("field_decimal", "A number", "Decimal")
      end

      it "does not allow tag to be set" do
        @decimal.tag = "t1 t2"
        exception = assert_raise(ActiveRecord::RecordInvalid) { @decimal.save! }
        assert_match /is invalid$/, exception.message
      end
    end

    describe "well formed tag for checkbox" do
      before do
        @cb2 = create_user_custom_field!("field_checkbox2", "Lots Here", "Checkbox")
      end

      it "does not be blank" do
        @cb2.tag = " "
        exception = assert_raise(ActiveRecord::RecordInvalid) { @cb2.save! }
        exception.message.must_include I18n.t('txt.admin.models.rules.rule.tag_cannot_be_blank')
      end

      it "does not contain special characters" do
        @cb2.tag = "100 200"
        exception = assert_raise(ActiveRecord::RecordInvalid) { @cb2.save! }
        exception.message.must_include I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_special_characters')
      end
    end

    describe "well formed tag for dropdown" do
      before do
        @dd2 = create_user_custom_field!("field_dropdown2", "Lots Here", "Dropdown")
      end

      it "does not be blank" do
        @dd2.dropdown_choices.build(name: "Choice 1", value: " ")
        exception = assert_raise(ActiveRecord::RecordInvalid) { @dd2.save! }
        exception.message.must_include I18n.t('txt.admin.models.ticket_fields.field_tagger.must_have_tag')
      end

      it "does not contain special characters" do
        @dd2.dropdown_choices.build(name: "Choice 1", value: "100 200")
        exception = assert_raise(ActiveRecord::RecordInvalid) { @dd2.save! }
        exception.message.must_include I18n.t('txt.admin.models.ticket_fields.field_tagger.cannot_contain_special_characters')
      end
    end

    describe "tag collision" do
      it "does not allow checkbox tag collision" do
        cb = create_user_custom_field!("field_checkbox2", "Lots Here", "Checkbox")
        cb.tag = "moose"
        exception = assert_raise(ActiveRecord::RecordInvalid) { cb.save! }
        emsg = I18n.t('txt.admin.models.ticket_fields.tagger_fields.tag_already_used_singular').gsub("{{tags}}", "<strong>#{cb.tag}<\/strong>")
        assert_match emsg, exception.message
      end

      it "does not allow dropdown tag collision" do
        dd2 = create_user_custom_field!("field_dropdown2", "dropdown field", "Dropdown")
        c1 = dd2.dropdown_choices.build(name: "Choice 1", value: "choice_1")
        exception = assert_raise(ActiveRecord::RecordInvalid) { dd2.save! }
        emsg = I18n.t('txt.admin.models.ticket_fields.tagger_fields.tag_already_used_singular').gsub("{{tags}}", "<strong>#{c1.value}<\/strong>")
        assert_match emsg, exception.message
      end

      it "allows dropdown tag collision of if the colliding field is inactive" do
        dd2 = create_user_custom_field!("field_dropdown2", "dropdown field", "Dropdown")
        dd2.dropdown_choices.build(name: "Choice 1", value: @choice_1.value)
        dd2.is_active = false
        assert dd2.save
      end

      it "allows dropdown tag collision if the collision is with soft deleted options" do
        @choice_1.soft_delete!
        dd2 = create_user_custom_field!("field_dropdown2", "dropdown field", "Dropdown")
        dd2.dropdown_choices.build(name: "Choice 1", value: "choice_1")
        assert dd2.save
      end

      it "allows checkbox and dropdown non-collision" do
        cb = create_user_custom_field!("field_checkbox2", "Lots Here", "Checkbox")
        cb.tag = "choice_X"
        cb.save!
        cb.reload
        assert_equal cb.tag, "choice_X"
      end

      it "does not allow checkbox collision with dropdown" do
        cb = create_user_custom_field!("field_checkbox2", "Lots Here", "Checkbox")
        cb.tag = "choice_2"
        exception = assert_raise(ActiveRecord::RecordInvalid) { cb.save! }
        emsg = I18n.t('txt.admin.models.ticket_fields.tagger_fields.tag_already_used_singular').gsub("{{tags}}", "<strong>#{cb.tag}<\/strong>")
        assert_match emsg, exception.message
      end

      it "allows organization checkbox collision with user dropdown" do
        cb = create_organization_custom_field!("field_checkbox2", "Lots Here", "Checkbox")
        cb.tag = "choice_2"
        assert cb.save
      end

      it "does not allow dropdown collision with checkbox" do
        dd = create_user_custom_field!("field_dropdown2", "dropdown field", "Dropdown")
        dd.dropdown_choices.build(name: "Choice 1", value: "choice_1")
        dd.dropdown_choices.build(name: "Choice 2", value: "choice_2")
        dd.dropdown_choices.build(name: "Choice 3", value: "moose")
        exception = assert_raise(ActiveRecord::RecordInvalid) { dd.save! }
        tags = "choice_1, choice_2, moose"
        emsg = I18n.t('txt.admin.models.ticket_fields.tagger_fields.tag_already_used_plural').gsub("{{tags}}", "<strong>#{tags}<\/strong>")
        assert_match emsg, exception.message
      end

      it "allows organization dropdown collision with user checkbox" do
        dd = create_organization_custom_field!("field_dropdown2", "dropdown field", "Dropdown")
        dd.dropdown_choices.build(name: "Choice 1", value: "choice_1")
        dd.dropdown_choices.build(name: "Choice 2", value: "choice_2")
        dd.dropdown_choices.build(name: "Choice 3", value: "moose")
        assert dd.save
      end
    end
  end
end
