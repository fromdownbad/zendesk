require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 8

describe 'CustomFields' do
  include CustomFieldsTestHelper
  fixtures :accounts, :users, :organizations, :cf_fields, :rules

  before do
    @account = accounts(:minimum)
    @custom_field = create_custom_field!("field_1", "Field 1", "User", "Text")
  end

  describe "fixtures" do
    fixtures :cf_fields

    it "inserts 12 records for the minimum account as of 2015-09-15" do
      assert_equal 12, accounts(:minimum).custom_fields.count(:all)
    end

    it "inserts valid records" do
      accounts(:minimum).custom_fields.each do |custom_field|
        # The following line may be usefully substituted for the assertion if you need to debug an an assertion failure.
        # puts "custom field: #{custom_field.key}; valid? #{custom_field.valid?}; errors=#{custom_field.errors.full_messages}"
        assert custom_field.valid?
      end
    end
  end

  describe "CustomField::Field" do
    subject { CustomField::Field.new }
    should_validate_presence_of :account, :title
    should validate_uniqueness_of(:key).scoped_to(:account_id, :owner)
    it "includes Zendesk::PreventCollectionModificationMixin" do
      assert(CustomField::Field.include?(Zendesk::PreventCollectionModificationMixin))
    end
  end

  describe "creating custom fields" do
    it "requires either 'User' or 'Organization' as an owner" do
      cf = @account.custom_fields.build(key: "field_X", title: "something", owner: nil)
      cf.type = "Text"
      refute cf.save
      assert cf.errors[:owner].any?

      cf.owner = "Foobar"
      refute cf.save
      assert cf.errors[:owner].any?
    end

    it "asserts that the type is valid" do
      cf = @account.custom_fields.build(key: "field_X", title: "something", owner: "user")

      cf.type = "NoExist"
      refute cf.save
      assert cf.errors[:type].any?
    end

    it "asserts that the key is valid" do
      cf = @account.custom_fields.build(key: "field_X", title: "something", owner: "user")

      cf.key = "@bad"
      refute cf.save
      assert cf.errors[:key].any?
    end

    it "asserts that keys starting with a number are valid" do
      cf = @account.custom_fields.for_user.build(key: "field_X", title: "something")
      cf.type = "Checkbox"

      cf.key = "1good"
      assert cf.save!
    end

    it "does not assert that the key is valid if blank" do
      cf = @account.custom_fields.build(key: "field_X", title: "something", owner: "user")

      cf.key = ""
      refute cf.save
      assert_equal cf.errors[:key].size, 1
      assert cf.errors[:key].first.include?("blank")
      refute cf.errors[:key].first.include?("invalid")
    end

    ["Checkbox", "Text", "Textarea"].each do |type|
      it "is able to create a #{type}" do
        cf = @account.custom_fields.for_user.build(key: "field_X", title: "something")
        cf.type = type.to_s
        assert cf.save!
      end
    end

    ["Date"].each do |type|
      describe type do
        it "is created with a regexp" do
          cf = @account.custom_fields.for_user.build(key: "field_X", title: "something")
          cf.type = type.to_s
          assert cf.save!
          assert cf.regexp_for_validation
        end
      end
    end

    describe "with key prefix 'system::'" do
      describe "for a system field" do
        before do
          @cf      = @account.custom_fields.build(key: "system::sf1", title: "txt.something", owner: "User", is_system: true)
          @cf.type = 'Text'
        end

        it "is successful" do
          assert @cf.save
        end
      end

      describe "for a non system field" do
        before do
          @cf      = @account.custom_fields.build(key: "system::sf1", title: "something", owner: "User")
          @cf.type = 'Text'
        end

        it "does not be successful" do
          refute @cf.save
          assert_includes @cf.errors[:key], "is invalid"
        end
      end
    end

    describe "without key prefix 'system::'" do
      describe "for a system field" do
        before do
          @cf      = @account.custom_fields.build(key: "yo_sf1", title: "something", owner: "User", is_system: true)
          @cf.type = 'Text'
        end

        it "does not be successful" do
          refute @cf.save
          assert_includes @cf.errors[:key], "System fields must be prefixed with 'system::' and can contain only underscore or alphanumerics"
        end
      end

      describe "for a non system field" do
        before do
          @cf      = @account.custom_fields.build(key: "yo_sf1", title: "something", owner: "User")
          @cf.type = 'Text'
        end

        it "is successful" do
          assert @cf.save
        end
      end
    end

    describe "without title prefix 'txt.'" do
      describe "for a system field" do
        before do
          @cf      = @account.custom_fields.build(key: "system::sf1", title: "something", owner: "User", is_system: true)
          @cf.type = 'Text'
        end

        it "does not be successful" do
          assert_equal false, @cf.save
          @cf.errors[:title].first.must_include "title must be i18n keys"
        end
      end

      describe "for a non system field" do
        before do
          @cf      = @account.custom_fields.build(key: "sf1", title: "something", owner: "User")
          @cf.type = 'Text'
        end

        it "is successful" do
          assert @cf.save
        end
      end
    end
  end

  describe "get_all_tags" do
    before do
      cf = create_user_custom_field!("field_dropdown", "dropdown field", "Dropdown")
      cf.dropdown_choices.build(name: "Choice 1", value: "choice_1")
      cf.dropdown_choices.build(name: "Choice 2", value: "choice_2")
      cf.save!
    end

    it "gets all tags excluding the current field" do
      cf2 = create_user_custom_field!("field_dropdown_2", "dropdown field", "Dropdown")
      cf2.dropdown_choices.build(name: "Choice 3", value: "choice_3")
      cf2.dropdown_choices.build(name: "Choice 4", value: "choice_4")
      cf2.save!
      all_tags = CustomField::Field.get_all_tags(cf2)
      assert all_tags.include?("choice_1")
      assert all_tags.include?("choice_2")
      refute all_tags.include?("choice_3")
      refute all_tags.include?("choice_4")
    end

    it "gets all tags excluding the current field for new records" do
      cf2 = @account.custom_fields.dropdowns.build(key: "field_dropdown_2", title: "dropdown field", owner: "User")
      cf2 = cf2.becomes_new(CustomField::Dropdown)
      cf2.dropdown_choices.build(name: "Choice 3", value: "choice_3")
      cf2.dropdown_choices.build(name: "Choice 4", value: "choice_4")
      all_tags = CustomField::Field.get_all_tags(cf2)
      assert all_tags.include?("choice_1")
      assert all_tags.include?("choice_2")
      refute all_tags.include?("choice_3")
      refute all_tags.include?("choice_4")
    end
  end

  describe "updating custom fields" do
    before do
      @custom_field = create_user_custom_field!("field_2", "Field 2", "Text")
    end

    it "does not allow :key to change on update" do
      @custom_field.key = "field_change"
      refute @custom_field.save
      assert @custom_field.errors[:key].any?
    end

    it "does not allow type to change on update" do
      @custom_field.type = "Checkbox"
      refute @custom_field.save
      assert @custom_field.errors[:type].any?
    end
  end

  describe "#cast_value_for_db" do
    describe "dates" do
      before do
        @custom_field = create_user_custom_field!("date", "Date", "Date")
      end

      it "casts to dates for date values" do
        t = Time.now
        assert_equal @custom_field.cast_value_for_db(t), t.strftime("%Y-%m-%d")
      end

      it "casts to dates for string values" do
        t = "9 July 2013"
        assert_equal @custom_field.cast_value_for_db(t), Date.parse(t).strftime("%Y-%m-%d")
      end
    end
  end

  describe "#value_as_json" do
    describe "dates" do
      before do
        @custom_field = create_user_custom_field!("date", "Date", "Date")
      end

      it "returns dates for date values" do
        assert_equal @custom_field.value_as_json("9 july 2013"), Time.parse("9 july 2013")
      end
    end
  end

  describe "#cast_for_compare" do
    describe "dates" do
      before do
        @custom_field = create_user_custom_field!("date", "Date", "Date")
      end

      it "returns dates for date values" do
        t = Time.now
        assert_equal @custom_field.cast_for_compare(t), t
      end
    end
  end

  describe "#rule_value" do
    describe "dates" do
      before do
        @custom_field = create_user_custom_field!("date", "Date", "Date")
      end

      it "returns date strings for string values" do
        t = Time.now
        assert_equal t.strftime("%Y-%m-%d"), @custom_field.rule_value(t.strftime("%Y-%m-%d"))
      end

      it "returns value for integer-castable values" do
        assert_equal @custom_field.rule_value("1"), "1"
      end
    end
  end

  describe "#validate_field_not_used" do
    def add(field)
      item = rule.definition.actions.last.dup
      rule.definition.actions << item
      item.source = field
      rule.save!
    end

    let(:rule) { rules(:automation_close_ticket) }
    let(:error) { ["Field cannot be deleted because it is still in use by: #{rule.lotus_permalink}"] }

    it "allows to delete unused field" do
      assert @custom_field.destroy
    end

    it "does not allow to delete used user field" do
      add "requester.custom_fields.field_1"
      refute @custom_field.destroy
      @custom_field.errors.full_messages.must_equal error
    end

    describe "organization" do
      before { @custom_field.update_column :owner, "Organization" }

      it "does not allow to delete used organization field" do
        add "organization.custom_fields.field_1"
        refute @custom_field.destroy
        @custom_field.errors.full_messages.must_equal error
      end

      it "does not allow to delete used requester organization field" do
        add "requester.organization.custom_fields.field_1"
        refute @custom_field.destroy
        @custom_field.errors.full_messages.must_equal error
      end
    end
  end
end
