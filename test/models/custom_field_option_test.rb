require_relative "../support/test_helper"
SingleCov.covered!

describe CustomFieldOption do
  fixtures :accounts, :ticket_fields, :ticket_forms, :custom_field_options, :users

  describe_with_arturo_disabled :special_chars_in_custom_field_options do
    let(:account) { accounts(:minimum) }
    let(:custom_field) { ticket_fields(:field_tagger_custom) }
    let(:custom_field_option) { custom_field_options(:field_tagger_custom_option_1) }

    should_validate_presence_of :account, :value

    it "should mark values with spaces as invalid" do
      custom_field_option.value = "value with space"
      refute custom_field_option.valid?
      assert_equal custom_field_option.errors[:value].length, 1
    end

    it "should not mark values with spaces as invalid if the option is not active" do
      custom_field_option.value = "value with space"
      custom_field_option.soft_delete!
      assert custom_field_option.valid?
    end

    it "calls normalize_value before validations are run" do
      custom_field_option.expects(:normalize_value).once
      custom_field_option.valid?
    end

    it "responds to position" do
      assert custom_field_option.respond_to?(:position)
    end

    it "returns an account even when NULL in the database" do
      custom_field_option.send(:write_attribute, :account_id, nil)
      assert_equal account, custom_field_option.account
    end

    it "inherits account from ticket_field" do
      new_custom_field_option = custom_field.custom_field_options.create!(value: 'test')
      assert_equal custom_field.account, new_custom_field_option.account
    end

    describe ".normalize_value" do
      it "removes leading and trailing whitespace" do
        actual = "  test  "
        expected = "test"

        assert_equal expected, CustomFieldOption.normalize_value(actual)
      end

      it "downcases everything" do
        actual = "ThisIsTitleCase"
        expected = "thisistitlecase"

        assert_equal expected, CustomFieldOption.normalize_value(actual)
      end
    end

    describe "#fetch_custom_field_option_value" do
      it "returns the value of the ticket_field_option" do
        assert_equal custom_field_option.value, account.fetch_custom_field_option_value(custom_field_option.id)
      end

      it "returns the value of the ticket_field_option even if argument is a string" do
        assert_equal custom_field_option.value, account.fetch_custom_field_option_value(custom_field_option.id.to_s)
      end

      it "returns nil when passed a non-int compatible value" do
        assert_nil account.fetch_custom_field_option_value("not integer")
      end

      it "returns nil when passed a boolean value" do
        assert_nil account.fetch_custom_field_option_value(true)
      end

      it "returns nil when passed an id of a non-existent custom field option" do
        assert_nil account.fetch_custom_field_option_value(-1)
      end

      describe "when an option is soft_deleted" do
        before { custom_field_option.soft_delete! }

        it "does not return the option" do
          assert_nil account.fetch_custom_field_option_value(custom_field_option.id)
        end
      end

      describe "when option is gone" do
        it "does not hit the DB" do
          queries = sql_queries do
            10.times do
              assert_nil account.fetch_custom_field_option_value("123123")
            end
          end
          assert_equal 1, queries.select { |q| q.match('custom_field_options') }.count
        end

        it "returns nil when zero is passed" do
          assert_nil account.fetch_custom_field_option_value(0)
        end
      end

      it "invalidates cache when new custom field option is created and field saved" do
        account.fetch_custom_field_option_value(custom_field_option.id) # populate cache
        new_custom_field_option = custom_field.custom_field_options.create!(name: 'name', value: 'test')
        assert custom_field.save
        assert_equal new_custom_field_option.value,
          Account.find(account.id).fetch_custom_field_option_value(new_custom_field_option.id)
      end
    end

    it "shows parity between the value and enhanced value field" do
      %w[ábc Abc niño nino çar car よう быть].each do |word|
        new_custom_field_option = custom_field.custom_field_options.build(value: word.to_s)
        assert_equal(new_custom_field_option[:value], new_custom_field_option[:enhanced_value])
      end
    end

    it "can't distinguish between variants" do
      custom_field.custom_field_options.create!(value: "abc")
      # Due to collation method, the db returns the previous option when trying to create the variant
      assert_raises ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation do
        custom_field.custom_field_options.create!(value: "ábc")
      end
    end

    describe 'when name length is invalid' do
      it "returns with an error message" do
        name = "z" * 256
        new_custom_field_option = custom_field.custom_field_options.build(value: "abc", name: name)
        refute new_custom_field_option.valid?
        assert_equal(
          new_custom_field_option.errors.messages[:name],
          ["is too long (maximum is 255 characters after encoding)"]
        )
      end
    end
  end

  describe "#validate_used_in_conditions" do
    let(:account) { accounts(:minimum) }
    let(:ticket_form) { ticket_forms(:minimum_ticket_form) }
    let(:parent_field) { ticket_fields(:field_tagger_custom) }
    let(:child_field) { ticket_fields(:field_textarea_custom) }
    let(:cfo1) { custom_field_options(:field_tagger_custom_option_1) }
    let(:cfo2) { custom_field_options(:field_tagger_custom_option_2) }
    let(:user) { users(:minimum_admin) }
    let(:condition) do
      condition = TicketFieldCondition.new.tap do |c|
        c.account = account
        c.ticket_form = ticket_form
        c.parent_field = parent_field
        c.child_field = child_field
        c.value = cfo2.value
        c.user_type = :agent
      end
      condition.save

      condition
    end

    before do
      ticket_form.account_id = account.id
      Account.any_instance.stubs(:has_ticket_forms?).returns(true)
      Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(true)

      TicketFormField.create(
        ticket_form: ticket_form,
        account: account,
        ticket_field: parent_field,
        position: 1
      )
      TicketFormField.create(
        ticket_form: ticket_form,
        account: account,
        ticket_field: child_field,
        position: 2
      )

      condition
    end

    it "doesn't allow updates on option values used in conditions" do
      cfo2.value = "changed"
      refute cfo2.valid?
    end

    it "allows updates on option name, even if the option is used in in conditions" do
      cfo2.name = "changed"
      assert cfo2.valid?
    end

    it "allows updates on option values not used in conditions" do
      cfo1.value = "changed"
      assert cfo1.valid?
    end
  end

  describe_with_arturo_enabled :special_chars_in_custom_field_options do
    let(:account) { accounts(:minimum) }
    let(:custom_field) { ticket_fields(:field_tagger_custom) }
    let(:custom_field_option) { custom_field_options(:field_tagger_custom_option_1) }

    should_validate_presence_of :account, :value, :enhanced_value

    it "should mark values with spaces as invalid" do
      custom_field_option.value = "value with space"
      refute custom_field_option.valid?
      assert_equal custom_field_option.errors[:value].length, 1
    end

    it "should mark values with pipe as invalid" do
      custom_field_option.value = "value|space"
      refute custom_field_option.valid?
      assert_equal custom_field_option.errors[:value].length, 1
      assert custom_field_option.errors.full_messages.first.match?(/\|/), 'doesnt contain a pipe character'
    end

    it "calls normalize_value before validations are run" do
      custom_field_option.expects(:normalize_value).once
      custom_field_option.valid?
    end

    it "responds to position" do
      assert custom_field_option.respond_to?(:position)
    end

    it "returns an account even when NULL in the database" do
      custom_field_option.send(:write_attribute, :account_id, nil)
      assert_equal account, custom_field_option.account
    end

    it "inherits account from ticket_field" do
      new_custom_field_option = custom_field.custom_field_options.create!(value: 'test')
      assert_equal custom_field.account, new_custom_field_option.account
    end

    describe ".normalize_value" do
      it "removes leading and trailing whitespace" do
        actual = "  test  "
        expected = "test"

        assert_equal expected, CustomFieldOption.normalize_value(actual)
      end

      it "downcases everything" do
        actual = "ThisIsTitleCase"
        expected = "thisistitlecase"

        assert_equal expected, CustomFieldOption.normalize_value(actual)
      end
    end

    describe "#fetch_custom_field_option_value" do
      it "returns the value of the ticket_field_option" do
        assert_equal custom_field_option.value, account.fetch_custom_field_option_value(custom_field_option.id)
      end

      it "returns the value of the ticket_field_option even if argument is a string" do
        assert_equal custom_field_option.value, account.fetch_custom_field_option_value(custom_field_option.id.to_s)
      end

      it "returns nil when passed a non-int compatible value" do
        assert_nil account.fetch_custom_field_option_value("not integer")
      end

      it "returns nil when passed a boolean value" do
        assert_nil account.fetch_custom_field_option_value(true)
      end

      it "returns nil when passed an id of a non-existent custom field option" do
        assert_nil account.fetch_custom_field_option_value(-1)
      end

      describe "when an option is soft_deleted" do
        before { custom_field_option.soft_delete! }

        it "does not return the option" do
          assert_nil account.fetch_custom_field_option_value(custom_field_option.id)
        end
      end

      describe "when option is gone" do
        it "does not hit the DB" do
          queries = sql_queries do
            10.times do
              assert_nil account.fetch_custom_field_option_value("123123")
            end
          end
          assert_equal 1, queries.select { |q| q.match('custom_field_options') }.count
        end

        it "returns nil when zero is passed" do
          assert_nil account.fetch_custom_field_option_value(0)
        end
      end

      it "invalidates cache when new custom field option is created and field saved" do
        account.fetch_custom_field_option_value(custom_field_option.id) # populate cache
        new_custom_field_option = custom_field.custom_field_options.create!(name: 'name', value: 'test')
        assert custom_field.save
        assert_equal new_custom_field_option.value,
          Account.find(account.id).fetch_custom_field_option_value(new_custom_field_option.id)
      end
    end

    it "allows enhanced character variants and does not write to value" do
      %w[abc ábc niño nino çar car čar].each do |word|
        new_custom_field_option = custom_field.custom_field_options.create!(value: word.to_s)
        assert new_custom_field_option.value
        refute new_custom_field_option[:value]
      end
    end

    it "can distinguish between variants in the db" do
      # Column collation allows db to distinguish accents as distinct options
      custom_field.custom_field_options.create!(value: "abc")
      assert custom_field.custom_field_options.create!(value: "ábc")
    end

    it "falls back to value if enhanced_value is nil" do
      new_option = custom_field.custom_field_options.create!(value: 'fubar')
      # This mimics the case where a field was deleted when the special_chars_in_custom_field_options arturo was enabeld
      new_option.update_columns(deleted_at: 1.day.ago, value: 'fubar', enhanced_value: nil)

      assert_equal 'fubar', new_option.reload.value
    end

    describe "when name length is invalid" do
      it "returns with an error message" do
        name = "a" * 256
        new_custom_field_option = custom_field.custom_field_options.build(value: "abc", name: name)
        refute new_custom_field_option.valid?
        assert_equal(
          new_custom_field_option.errors.messages[:name],
          ["is too long (maximum is 255 characters after encoding)"]
        )
      end
    end
  end
end
