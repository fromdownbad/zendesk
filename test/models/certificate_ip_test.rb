require_relative "../support/test_helper"

SingleCov.covered! uncovered: 17

describe CertificateIp do
  fixtures :accounts, :certificates, :certificate_ips

  let(:aws_certificate_ip) do
    certificates(:pending_no_chain).certificate_ips.create! do |cert_ip|
      cert_ip.alias_record = 'xxx'
      cert_ip.pod_id = 12
    end
  end
  let(:sni_certificate_ip) { certificates(:active_sni).certificate_ips.first }

  it "has good test fixtures" do
    assert_equal 2, CertificateIp.assigned.length

    assert_equal 2, CertificateIp.unassigned.length
    assert_equal 1, CertificateIp.assigned.by_pod(1).length
    assert_equal 1, CertificateIp.unassigned.by_pod(1).length
    assert_equal 1, CertificateIp.assigned.by_pod(2).length
    assert_equal 1, CertificateIp.assigned.by_pod(2).length
    assert_equal 0, CertificateIp.unassigned.by_pod(3).length
    assert_equal 0, CertificateIp.unassigned.by_pod(3).length
  end

  describe ".release_scheduled_ips!" do
    it "resets the certificate association and release_at timestamp" do
      @scheduled_future = certificate_ips(:three)
      @scheduled_future.schedule_release!(1.day.from_now)
      @scheduled_past = certificate_ips(:four)
      @scheduled_past.schedule_release!(1.day.ago)

      CertificateIp.release_scheduled_ips!(@scheduled_past.pod_id)
      CertificateIp.release_scheduled_ips!(@scheduled_future.pod_id)

      @scheduled_past.reload
      @scheduled_future.reload

      assert_nil @scheduled_past.certificate_id
      assert_nil @scheduled_past.release_at

      refute_nil @scheduled_future.certificate_id
      refute_nil @scheduled_future.release_at
    end

    it "does not affect unscheduled certificates" do
      @unscheduled = certificate_ips(:three)
      assert_nil @unscheduled.release_at
      refute_nil @unscheduled.certificate_id

      CertificateIp.release_scheduled_ips!(@unscheduled.pod_id)

      @unscheduled.reload
      refute_nil @unscheduled.certificate_id
    end

    it "deletes SNI ip records" do
      sni_certificate_ip.schedule_release!(1.day.ago)
      CertificateIp.release_scheduled_ips!(sni_certificate_ip.pod_id)
      assert_raises(ActiveRecord::RecordNotFound) { sni_certificate_ip.reload }
    end
  end

  describe "#schedule_release!" do
    before { travel_to Time.now }

    after { travel_back }

    it "sets the release_at timestamp" do
      release_at = 1.week.from_now
      certificate_ip = certificate_ips(:three)
      certificate_ip.schedule_release!(release_at)
      assert_equal certificate_ip.release_at, release_at

      certificate_ip.cancel_release!
      assert_nil certificate_ip.release_at
    end
  end

  describe "#detach_from_certificate!" do
    it "destroys aws" do
      aws_certificate_ip.expects(:destroy)
      aws_certificate_ip.detach_from_certificate!
    end

    it "destroys SNI" do
      sni_certificate_ip.schedule_release!(1.day.ago)
      sni_certificate_ip.detach_from_certificate!
      assert_raises(ActiveRecord::RecordNotFound) { sni_certificate_ip.reload }
    end

    it "frees ip based" do
      ip = CertificateIp.assigned.first
      ip.release_at = Time.now
      ip.certificate_id = 1

      ip.detach_from_certificate!

      ip.reload
      ip.release_at.must_be_nil
      ip.certificate_id.must_be_nil
    end
  end

  describe "#pod_vip_address" do
    it "returns nil without a pod" do
      ip = certificate_ips(:one)
      ip.pod_id = nil
      assert_nil ip.pod_vip_address
    end

    it "returns the pod A record" do
      ip = certificate_ips(:one)
      assert_equal '127.0.0.1', ip.pod_vip_address
    end

    it "resolves and returns the pod alias_record" do
      ip = certificate_ips(:one)
      ip.pod_id = 3
      Resolv.expects(:getaddress).with('alias_record').returns('ip address').once
      assert_equal 'ip address', ip.pod_vip_address
    end
  end

  describe "#destroy" do
    it "destroys SNI" do
      sni_certificate_ip.destroy
      assert_raises(ActiveRecord::RecordNotFound) { sni_certificate_ip.reload }
    end

    it "does not clean up for normal ips" do
      assert certificate_ips(:three).destroy
    end
  end
end
