require_relative "../../support/test_helper"

SingleCov.covered!

describe PasswordResetToken do
  fixtures :accounts, :users

  before do
    travel_to Time.now
  end

  after { travel_back }

  describe "callbacks" do
    it "generates a value on create" do
      token = PasswordResetToken.create!(account: accounts(:minimum), source: users(:minimum_agent))
      assert_equal 32, token.value.size
    end

    it "sets expires on create" do
      token = PasswordResetToken.create!(account: accounts(:minimum), source: users(:minimum_agent))
      assert_equal 24.hours.from_now, token.expires_at
    end

    it "deletes a user's existing tokens if they exist" do
      user = users(:minimum_agent)
      token1 = PasswordResetToken.create!(account: accounts(:minimum), source: user)
      assert_equal [token1], user.reload.password_reset_tokens
      token2 = PasswordResetToken.create!(account: accounts(:minimum), source: user)
      assert_equal [token2], user.reload.password_reset_tokens
    end
  end
end
