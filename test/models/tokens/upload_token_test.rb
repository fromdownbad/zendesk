require_relative "../../support/test_helper"

SingleCov.covered!

describe 'UploadToken' do
  fixtures :users, :accounts

  before do
    @user    = users(:minimum_admin)
    @account = @user.account
    Timecop.freeze
    @token = @account.upload_tokens.create!(source: @user)

    @file       = Pathname.new(__FILE__)
    @upload     = Rack::Test::UploadedFile.new(@file.realpath.to_s)
  end

  describe '#add_raw_attachment' do
    before do
      @attachment = @token.add_raw_attachment(@upload, @file.basename.to_s)
    end

    it "is destroyed when token is destroyed" do
      @attachment.save!
      @token.destroy
      assert_raise(ActiveRecord::RecordNotFound) { @attachment.reload }
    end

    describe "expires_at" do
      it "expires 3 days after creation by default" do
        assert_equal 3.days.from_now.to_s, @token.expires_at.to_s
      end

      describe "with account_setting upload_token_expiry set" do
        before do
          @account.settings.upload_token_expiry = 10.days.to_i
          @account.save!
          @token = @account.upload_tokens.create!(source: @user)
        end

        it "expires based on the account setting" do
          assert_equal 10.days.from_now.to_s, @token.expires_at.to_s
        end
      end
    end

    describe "being added" do
      it "does not be public" do
        assert_equal false, @attachment.is_public
      end

      it "belongs to the token account" do
        assert_equal @account, @attachment.account
      end

      it "has the token's source as the author" do
        assert_equal @token.source, @attachment.author
      end

      it "has a filename the same as the uploaded file" do
        assert_equal @file.basename.to_s, @attachment.filename
      end

      it "has the uploaded file's size" do
        assert_equal @file.size, @attachment.size
      end

      it "has the file's mimetype when available" do
        assert_equal 'application/x-ruby', @attachment.content_type
      end
    end
  end

  describe '#add_attachment' do
    before do
      @attachment = @token.add_attachment(@upload, 'wombat', true)
    end

    it "sets the display filename" do
      assert_equal 'wombat', @attachment.display_filename
    end

    it "sets the inline setting on the attachment" do
      assert(@attachment.inline)
    end

    it "does not be public" do
      assert_equal false, @attachment.is_public
    end

    it "belongs to the token account" do
      assert_equal @account, @attachment.account
    end

    it "has the token's source as the author" do
      assert_equal @token.source, @attachment.author
    end

    it "has a filename the same as the uploaded file" do
      assert_equal @file.basename.to_s, @attachment.filename
    end

    it "has the uploaded file's size" do
      assert_equal @file.size, @attachment.size
    end

    it "has the file's mimetype when available" do
      assert_equal 'text/plain', @attachment.content_type
    end
  end
end
