require_relative "../../support/test_helper"

SingleCov.covered!

describe VerificationToken do
  fixtures :accounts, :users

  before do
    travel_to Time.now
  end

  after { travel_back }

  describe "callbacks" do
    it "generates a value on create" do
      token = VerificationToken.create!(account: accounts(:minimum), source: users(:minimum_agent))
      assert_equal 32, token.value.size
    end

    it "sets expires on create" do
      token = VerificationToken.create!(account: accounts(:minimum), source: users(:minimum_agent))
      assert_equal 24.hours.from_now, token.expires_at
    end

    it "deletes a user's existing tokens if they exist" do
      user = users(:minimum_agent)
      VerificationToken.create!(account: accounts(:minimum), source: user)
      user.reload
      token2 = VerificationToken.create!(account: accounts(:minimum), source: user)
      assert_equal [token2], user.reload.verification_tokens
    end

    it "deletes tokens associated with a user's UserEmailIdentity" do
      user = users(:minimum_agent)
      identity = user.identities.first
      VerificationToken.create!(account: accounts(:minimum), source: identity)
      user.reload
      token2 = VerificationToken.create!(account: accounts(:minimum), source: user)
      user.reload
      assert_equal [], identity.verification_tokens
      assert_equal [token2], user.verification_tokens
    end
  end
end
