require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe RequestToken do
  fixtures :tickets, :accounts

  before do
    Timecop.travel(2014, 11, 04, 17, 00)
    @ticket = tickets(:minimum_2)
    @token = RequestToken.new(ticket: @ticket)
  end

  subject { @token }

  should_validate_presence_of :ticket

  it 'is valid with a Ticket' do
    subject.ticket = @ticket
    assert subject.valid?
  end

  it 'does not be valid with a non-Ticket' do
    subject.ticket = @ticket.account
    refute subject.valid?
    assert subject.errors[:ticket].any?
  end

  it 'uses its class as its type' do
    assert_equal 'RequestToken', subject.type.to_s
  end

  it 'expires in 90 days' do
    subject.save!
    assert(Time.now + 90.days - 1.hours < subject.expires_at)
    assert(Time.now + 90.days + 1.hours > subject.expires_at)
  end

  it 'finds via a simple method' do
    subject.save
    assert RequestToken.find_by_value!(subject.value)
    assert_equal @ticket, subject.ticket
  end

  it "finds many via optimized query" do
    subject.save
    tokens = [subject.value]
    tokens_crc = tokens.map { |t| Zlib.crc32(t) }
    tickets = @ticket.account.tickets.joins(:request_token).where(
      "tokens.value IN (?) AND tokens.token_crc IN (?) AND tokens.account_id = ?",
      tokens, tokens_crc, @ticket.account.id
    )
    assert_same_elements tickets, [subject.ticket]
  end
end
