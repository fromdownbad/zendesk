require_relative "../../support/test_helper"

SingleCov.covered!

describe 'SatisfactionRatingToken' do
  fixtures :tickets, :accounts

  describe Tokens::SatisfactionRatingToken do
    before do
      Timecop.travel(2010, 06, 12, 17, 00)
      @ticket = tickets(:minimum_2)
      @token = Tokens::SatisfactionRatingToken.new(ticket: @ticket)
    end

    subject { @token }

    should_validate_presence_of :ticket

    it 'is valid with a Ticket' do
      subject.ticket = @ticket
      assert subject.valid?
    end

    it 'does not be valid with a non-Ticket' do
      subject.ticket = @ticket.account
      refute subject.valid?
      assert subject.errors[:ticket].any?
    end

    it 'uses its fully-qualified class as its type' do
      assert_equal 'Tokens::SatisfactionRatingToken', subject.type.to_s
    end

    it 'expires in 150 days' do
      subject.save!
      assert(Time.now + 150.days - 1.hours < subject.expires_at)
      assert(Time.now + 150.days + 1.hours > subject.expires_at)
    end
  end

  describe "find_or_create_by_ticket" do
    before do
      @ticket = tickets(:minimum_1)
    end

    it "creates a token for a ticket" do
      Tokens::SatisfactionRatingToken.find_or_create_by_ticket(@ticket)
      assert_equal Tokens::SatisfactionRatingToken.where(source_id: @ticket.id).size, 1
    end

    it "returns the same token when called a second time" do
      token = Tokens::SatisfactionRatingToken.find_or_create_by_ticket(@ticket)
      assert_equal Tokens::SatisfactionRatingToken.where(source_id: @ticket.id).size, 1
      token2 = Tokens::SatisfactionRatingToken.find_or_create_by_ticket(@ticket)
      assert_equal Tokens::SatisfactionRatingToken.where(source_id: @ticket.id).size, 1
      assert_equal token, token2
    end
  end

  describe 'tickets created with a SatisfactionRatingToken' do
    before do
      @ticket = tickets(:minimum_1)
      @token = Tokens::SatisfactionRatingToken.create!(ticket: @ticket)
    end

    describe "good or bad ratings" do
      before do
        @ticket.satisfaction_score = SatisfactionType.GOOD
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end
      it 'destroys the token after being saved' do
        @ticket.save!
        assert_raise(ActiveRecord::RecordNotFound) { @token.reload }
      end
    end
  end
end
