require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Token do
  fixtures :accounts, :users, :tokens, :tickets, :attachments
  should_validate_presence_of :source, :account

  before do
    @account1_user = accounts(:minimum).users.first
    @account2_user = accounts(:with_groups).users.first
  end

  it "does not be susceptible to mysql coercion exploits" do
    ChallengeToken.create!(source: @account1_user, value: 'blah')

    assert_nil ChallengeToken.find_by_value(0)
    assert_equal [], ChallengeToken.lookup(0)
    assert_raise(ActiveRecord::RecordNotFound) { ChallengeToken.find_by_value!(0) }
  end

  describe "creation" do
    it "validates uniqueness of value on account" do
      token1 = ChallengeToken.create!(source: @account1_user)
      token2 = ChallengeToken.new(source: @account1_user)
      token2.value = token1.value

      refute token2.valid?
      assert token2.errors[:value].any?

      token3 = ChallengeToken.new(source: @account2_user)
      token3.value = token1.value
      assert token3.valid?
    end

    describe "UploadToken" do
      it "is able to set the source to an account" do
        token = UploadToken.create!(source: @account1_user.account)
        assert_equal @account1_user.account, token.account
        assert_equal @account1_user.account, token.source
      end
    end
  end

  describe "#cleanup for upload tokens" do
    it "doesn't delete inline attachments or corresponding upload tokens" do
      expired_upload_token = UploadToken.create(source: accounts(:minimum))
      expired_upload_token.expires_at = 1.hour.ago
      expired_upload_token.save(validate: false) # uniq validation prevents updates
      inline_attachment = FactoryBot.create(:attachment,
        account: accounts(:minimum),
        inline: true,
        source: expired_upload_token)

      Token.cleanup

      assert_equal inline_attachment, Attachment.find_by_id(inline_attachment.id)
      assert_equal expired_upload_token, Token.find_by_id(expired_upload_token.id)
    end

    it "doesn't delete tokens or corresponding attachments for accounts with skip_deleting_expired_tokens arturo" do
      Arturo.enable_feature!(:skip_deleting_expired_tokens)
      expired_upload_token = UploadToken.create(source: accounts(:minimum))
      expired_upload_token.expires_at = 1.hour.ago
      expired_upload_token.save(validate: false) # uniq validation prevents updates
      attachment = FactoryBot.create(:attachment, account: accounts(:minimum), source: expired_upload_token)

      Token.cleanup

      assert_equal attachment, Attachment.find_by_id(attachment.id)
      assert_equal expired_upload_token, Token.find_by_id(expired_upload_token.id)
    end
  end

  describe "#cleanup" do
    before do
      @satisfaction_rating_token = Tokens::SatisfactionRatingToken.create!(ticket: tickets(:minimum_1), account: accounts(:minimum))
      Token.without_arsi.update_all(expires_at: 5.minutes.ago)
      @upload_token_ids = UploadToken.all.map(&:id)
    end

    describe "with csr token retention" do
      before do
        Account.any_instance.stubs(:has_csr_token_retention?).returns(true)
      end

      describe "when ticket is missing" do
        before do
          @satisfaction_rating_token.update_column(:source_id, 1234567890)
        end

        it "removes token" do
          Token.cleanup
          assert_empty(Tokens::SatisfactionRatingToken.all)
        end
      end

      describe "when account is missing" do
        before do
          @satisfaction_rating_token.update_column(:account_id, 1234567890)
        end

        it "removes token" do
          Token.cleanup
          assert_empty(Tokens::SatisfactionRatingToken.all)
        end
      end

      describe "when ticket status is not closed" do
        before do
          Token.expects(:delete_all).with(id: Token.all.map(&:id).sort - @upload_token_ids - [@satisfaction_rating_token.id])
          # no expects() for UploadToken.destroy so they get deleted before Token.delete_all
          Token.cleanup
        end

        it "retains the satisfaction rating token" do
          assert @satisfaction_rating_token.reload
          assert_empty(UploadToken.all)
        end
      end

      describe "when ticket status is closed" do
        before do
          Ticket.any_instance.stubs(:status_id).returns(StatusType.CLOSED)
          Token.expects(:delete_all).with(id: Token.all.map(&:id).sort - @upload_token_ids)
          # no expects() for UploadToken.destroy so they get deleted before Token.delete_all
          Token.cleanup
        end

        it "batch delete tokens" do
          assert_empty(UploadToken.all)
        end
      end
    end

    describe "without csr token retention" do
      before do
        Account.any_instance.stubs(:has_csr_token_retention?).returns(false)
        Token.expects(:delete_all).with(id: Token.all.map(&:id).sort - @upload_token_ids)
        # no expects() for UploadToken.destroy so they get deleted before Token.delete_all
        Token.cleanup
      end

      it "batch delete tokens" do
        assert_empty(UploadToken.all)
      end
    end
  end

  describe "find_by_value" do
    it "ignores bogus data" do
      assert_nil Token.find_by_value(File.new(__FILE__))
    end
  end

  describe "find_all_by_value" do
    subject { [tokens(:other_minimum_upload_token), tokens(:minimum_upload_token)] }

    before do
      @finder = accounts(:minimum).upload_tokens.find_all_by_value(subject.map(&:value))
    end

    it "returns same number" do
      assert_equal subject.size, @finder.size
    end

    it "finds all tokens" do
      subject.each do |token|
        assert @finder.include?(token)
      end
    end
  end
end
