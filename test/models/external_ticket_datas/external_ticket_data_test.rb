require_relative "../../support/test_helper"

SingleCov.covered!

describe ExternalTicketData do
  fixtures :external_ticket_datas

  describe "An ExternalTicketData object" do
    before do
      @external_ticket_data = external_ticket_datas(:external_ticket_data_1)
    end

    it "reports state" do
      @external_ticket_data.data = nil
      refute @external_ticket_data.data?
      @external_ticket_data.data = {foo: :bar}
      assert @external_ticket_data.data?

      @external_ticket_data.stubs(:stale?).returns(false)
      refute @external_ticket_data.needs_sync?

      @external_ticket_data.stubs(:stale?).returns(true)
      assert @external_ticket_data.needs_sync?

      @external_ticket_data.stubs(:stale?).returns(false)
      @external_ticket_data.data = nil
      assert @external_ticket_data.needs_sync?

      @external_ticket_data.stubs(:stale?).returns(false)
      @external_ticket_data.data = {foo: :bar}
      @external_ticket_data.sync_errored!
      assert @external_ticket_data.needs_sync?
    end

    it "syncs data" do
      @external_ticket_data.data = nil
      @external_ticket_data.sync!(foo: :bar)
      assert @external_ticket_data.data?
      assert_equal ExternalTicketData::SyncStatus::SYNC_OK, @external_ticket_data.sync_status
    end

    it "records error state" do
      refute @external_ticket_data.sync_errored?
      @external_ticket_data.sync_errored!
      assert @external_ticket_data.sync_errored?
    end

    describe "for an archived ticket" do
      before do
        archive_and_delete(@external_ticket_data.ticket)
        @external_ticket_data.reload
      end

      it "is still able to validate and save" do
        assert @external_ticket_data.save
      end
    end
  end
end
