require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe SalesforceTicketData do
  fixtures :tickets, :external_ticket_datas

  describe "#start_sync" do
    before do
      @salesforce_ticket_data = external_ticket_datas(:external_ticket_data_1)
    end

    it "reports properties" do
      assert_nil @salesforce_ticket_data.status
      assert_nil @salesforce_ticket_data.records

      @salesforce_ticket_data.sync!(records: ["a", "b"])

      assert_equal "ok", @salesforce_ticket_data.status
      assert_equal ["a", "b"], @salesforce_ticket_data.records

      @salesforce_ticket_data.sync_errored!
      assert_equal "errored", @salesforce_ticket_data.status

      @salesforce_ticket_data.sync_errored!(records: ["c", "d"])
      assert_equal "errored", @salesforce_ticket_data.status
      assert_equal ["c", "d"], @salesforce_ticket_data.records
    end

    it "schedules a data sync" do
      SalesforceTicketSyncJob.expects(:enqueue)
      @salesforce_ticket_data.start_sync(tickets(:minimum_1))
      assert_equal ExternalTicketData::SyncStatus::SYNC_PENDING, @salesforce_ticket_data.sync_status
    end
  end

  describe "#delete_cache" do
    let(:salesforce_data) { external_ticket_datas(:external_ticket_data_1) }
    it "deletes data" do
      assert_equal salesforce_data, salesforce_data.delete_cache
    end
  end
end
