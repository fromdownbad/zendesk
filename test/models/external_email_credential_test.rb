require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe ExternalEmailCredential do
  fixtures :accounts, :account_settings, :users, :recipient_addresses, :brands

  let(:username) { "USERNAME@gmail.net" }
  let(:unencrypted_token) { 'adadadadddadadada' }
  let(:valid_attributes) do
    {
      account: accounts(:minimum),
      username: username,
      current_user: users(:minimum_agent),
    }
  end

  let(:credential) do
    ExternalEmailCredential.new(valid_attributes).tap do |external_email_credential|
      external_email_credential.encrypted_value = unencrypted_token
      external_email_credential.save!
    end
  end

  let(:account) { accounts(:minimum) }

  describe "validations" do
    let(:credential)  do
      ExternalEmailCredential.new(valid_attributes).tap do |external_email_credential|
        external_email_credential.encrypted_value = unencrypted_token
      end
    end

    should_validate_presence_of :account_id

    it "is valid" do
      assert credential.valid?
    end

    it "is invalid without username" do
      credential.username = ""
      refute credential.valid?
      assert_equal 1, Array.wrap(credential.errors[:username]).size
    end

    it "is invalid without encrypted_value" do
      credential.encrypted_value = ""
      refute credential.valid?
      assert_equal 1, Array.wrap(credential.errors[:encrypted_value]).size
    end

    describe "#validate_username" do
      before do
        credential.save!
      end

      it "is valid when username matches existing username" do
        credential.username = username
        assert credential.valid?
      end

      it "is valid when username matches recipient address" do
        RecipientAddress.any_instance.stubs(:email).returns("otherusername@gmail.net")
        credential.username = "otherusername@gmail.net"
        assert credential.valid?
      end

      it "is invalid when username does not match existing username or recipient address" do
        credential.username = "otherusername@gmail.net"
        refute credential.valid?
      end
    end
  end

  describe "callbacks" do
    describe "creating the recipient address" do
      it "creates a recipient address" do
        address = credential.recipient_address
        assert_equal credential.email, address.email
        assert_equal account.name, address.name
        assert address.forwarding_verified?
        assert_equal account.default_brand_id, address.send(:brand_id)
      end

      it "links the recipient address if one already exists for the email address" do
        recipient_address = recipient_addresses(:not_default)
        previous_name     = recipient_address.name
        credential        = ExternalEmailCredential.new(valid_attributes.merge(username: recipient_address.email)).tap do |external_email_credential|
          external_email_credential.encrypted_value = unencrypted_token
          external_email_credential.save!
        end

        assert_equal recipient_address, credential.recipient_address
        assert_equal previous_name, credential.recipient_address.name
      end
    end

    describe "with preexisting errors" do
      before do
        valid_attributes[:error_count] = 5
        assert_equal 5, credential.error_count
      end

      it "resets error count when user renews token" do
        credential.encrypted_value = 'afafafafafa'
        credential.save!
        assert_equal 0, credential.error_count
      end

      it "does not reset error count when updating unrelated values" do
        credential.update_attributes!(last_error_message: "xxx", last_error_at: Time.now)
        assert_equal 5, credential.error_count
      end
    end

    describe 'when reconnecting the credential' do
      before do
        Timecop.freeze(Time.now)
        valid_attributes[:credential_updated_at] = 1.day.ago
      end

      after { Timecop.return }

      it 'updates #credential_updated_at when saved with new #encrypted_value' do
        credential.encrypted_value = 'afafafafafa'
        credential.save!
        assert_equal Time.now, credential.credential_updated_at
      end
    end

    describe 'when creating the credential' do
      before { Timecop.freeze(Time.now) }
      after { Timecop.return }

      it 'sets #credential_updated_at when saved' do
        assert_equal Time.now, credential.credential_updated_at
      end
    end

    describe "test email" do
      before do
        ActionMailer::Base.perform_deliveries = true
        ActionMailer::Base.deliveries.clear
      end

      it "sends test email when not importing" do
        credential
        assert_equal 1, ActionMailer::Base.deliveries.size # test-email
      end

      it "does not send a test email when importing" do
        valid_attributes[:initial_import] = true
        credential
        assert_equal 0, ActionMailer::Base.deliveries.size
      end
    end

    describe 'encryption' do
      describe '#encrypt_refresh_token' do
        it 'encrypts the encrypted_value' do
          refute_equal credential.encrypted_value, unencrypted_token
        end

        it 'sets the encryption_cipher_name' do
          assert_equal credential.encryption_cipher_name, 'aes-256-gcm'
        end

        it 'sets the encryption_key_name' do
          assert_equal credential.encryption_key_name, 'EXTERNAL_EMAIL_CREDENTIAL_TEST_KEY_A'
        end
      end

      describe '#decrypted_refresh_token' do
        it 'correctly decrypts the encrypted refresh token' do
          assert_equal credential.decrypted_refresh_token, unencrypted_token
        end
      end
    end
  end

  describe "#destroy" do
    it "works" do
      assert credential.destroy
    end

    it "works a job to revoke the credential" do
      RevokeExternalEmailCredentialJob.expects(:work).with(credential.encrypted_value, credential.encryption_key_name, credential.encryption_cipher_name).once
      credential.destroy
    end

    describe 'when it fails to revoke' do
      before do
        RevokeExternalEmailCredentialJob.expects(:work).raises(StandardError, 'Could not revoke token')
      end

      it { refute credential.destroy }
    end

    describe 'when there is no encrypted_value' do
      before { credential.stubs(encrypted_value: nil) }

      it "does not enqueue a job to revoke the credential" do
        RevokeExternalEmailCredentialJob.expects(:enqueue).with(credential.encrypted_value, credential.encryption_key_name, credential.encryption_cipher_name).never
        credential.destroy
      end
    end

    describe "when default" do
      before do
        credential.recipient_address.update_attributes(default: true)
      end

      it "does not delete to avoid sending emails from an email that we no longer receive" do
        refute credential.destroy
        assert credential.reload.recipient_address
      end

      it "does not enqueue a job to revoke the credential" do
        RevokeExternalEmailCredentialJob.expects(:enqueue).with(credential.encrypted_value, credential.encryption_key_name, credential.encryption_cipher_name).never
        credential.destroy
      end
    end
  end

  describe ".redirect_uri" do
    it "builds on test" do
      assert_equal "https://support.zendesk-test.com/ping/redirect_to_account", ExternalEmailCredential.redirect_uri
    end
  end

  describe "#active?" do
    it "returns true when last_fetched is within a day" do
      credential.last_fetched_at = Time.now
      assert credential.active?
    end

    it "returns false when last_fetched is older than a day" do
      credential.last_fetched_at = Time.now - 3.days
      refute credential.active?
    end

    it "returns false when last_fetched is nil" do
      refute credential.active?
    end
  end

  describe "#ensure recipient address" do
    it 'sets brand_id if one is set on the external credential object' do
      credential = ExternalEmailCredential.new
      credential.brand_id = '123'
      credential.update_attributes(valid_attributes)
      assert_equal credential.recipient_address.brand_id, 123
    end

    it 'sets brand_id to default brand if no brand_id is set' do
      credential = ExternalEmailCredential.new
      credential.update_attributes(valid_attributes)
      account = valid_attributes[:account]
      assert_equal credential.recipient_address.brand_id, account.default_brand_id
    end
  end
end
