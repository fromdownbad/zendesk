require_relative "../support/test_helper"

SingleCov.covered! uncovered: 18

describe 'Role' do
  describe "A Role" do
    before { @role = Role.new }

    it "has an id" do
      assert_respond_to @role, :id
    end

    it "has a name" do
      assert_respond_to @role, :name
    end

    it "has a description" do
      assert_respond_to @role, :description
    end

    it "is accessible as a constant" do
      Role.all.each do |role|
        constant_name = role.name.upcase.gsub(/\W/, "_")

        assert_equal Role.const_get(constant_name), role
      end
    end
  end
end
