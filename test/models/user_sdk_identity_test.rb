require_relative "../support/test_helper"

SingleCov.covered!

describe UserSdkIdentity do
  fixtures :users

  before do
    @user = users(:minimum_end_user)
  end

  describe "Creation" do
    it "increments the correct StatsD metric with the correct tags" do
      metric_name = UserSdkIdentity::STATSD_METRIC_IDENTITY_CREATED
      expected_tags = [
        "user_id:#{@user.id}",
        "account_id:#{@user.account.id}"
      ]
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('domain_event', tags: ['event:user_identity_created'])
      Zendesk::StatsD::Client.any_instance.expects(:increment).with(metric_name, tags: expected_tags)
      UserSdkIdentity.create(user: @user, value: "111a1111-b11c-11d1-e111-999999999999")
    end
  end

  describe "identities validation" do
    before do
      @user = users(:minimum_end_user)
      identity_limit = 2
      Account.any_instance.stubs(:settings).returns(stub(
        max_sdk_identities: identity_limit,
        max_identities: 100,
        ssl_required?: false,
        ssl_enabled?: false,
        has_user_tags?: false
      ))

      identity_limit.times do |i|
        identity = UserSdkIdentity.new(user: @user, value: "111a1111-b11c-11d1-e111-1111111111#{i + 10}", account: @user.account, is_verified: true)
        identity.save!
      end
    end

    describe "User identities limits" do
      before do
        @identity = UserSdkIdentity.create(user: @user, value: "111a1111-b11c-11d1-e111-999999999999")
      end

      it "prevents the saving of the user adding an error to the user" do
        refute @identity.valid?, "Identity should not be valid because the sdk limit has been reached."
        refute @identity.save, "Identity should be saved because the sdk limit has been reached."

        expected_error_msg = "sdk identities limit for user_id: #{@user.id}, account_id: #{@user.account.id} exceeded"
        assert @identity.errors.full_messages.join.must_include(expected_error_msg)
      end

      it "prevents the creation of the SDK identity" do
        assert_no_difference -> { UserSdkIdentity.count } do
          @identity.save
        end
      end
    end
  end

  describe "scopes queries to account ID" do
    it "for device_identifier association" do
      identity = UserSdkIdentity.create!(user: @user, value: "111a1111-b11c-11d1-e111-999999999999")
      identity.reload

      assert_sql_queries(1, /`device_identifiers`.`account_id` = #{identity.account_id}/) do
        identity.device_identifier
      end
    end
  end
end
