require_relative "../support/test_helper"
require_relative "../support/billing_test_helper"

SingleCov.covered!

describe 'ShardedSubscription' do
  fixtures :accounts, :subscriptions, :payments,
    :account_property_sets, :users, :user_identities, :groups,
    :sequences, :remote_authentications, :credit_cards

  include BillingTestHelper

  describe "a sharded_subscription for a trial account" do
    before do
      @account = accounts(:trial)
      @subscription = @account.subscription
      @sharded_sub = @account.build_sharded_subscription
    end

    it "matchs the values of the subscription" do
      @sharded_sub.sync_from_account_database

      assert_equal @account.subdomain, @sharded_sub.subdomain
      assert_equal @subscription.plan_type, @sharded_sub.plan_type
      assert_equal @subscription.max_agents, @sharded_sub.max_agents
      assert_equal @subscription.account_type, @sharded_sub.account_type
      assert_equal @subscription.account_status, @sharded_sub.account_status
      assert_equal @account.owner_id, @sharded_sub.owner_id

      assert_equal @account.is_serviceable?, @sharded_sub.is_serviceable?
      assert_equal @account.is_active?, @sharded_sub.is_active?
    end
  end

  describe "a sharded_subscription for a paying account" do
    before do
      @account = paying_account
      @subscription = @account.subscription
      @sharded_sub = @account.build_sharded_subscription
    end

    it "matchs the values of the subscription" do
      @sharded_sub.sync_from_account_database

      assert_equal @account.subdomain, @sharded_sub.subdomain
      assert_equal @subscription.plan_type, @sharded_sub.plan_type
      assert_equal @subscription.max_agents, @sharded_sub.max_agents
      assert_equal @subscription.account_type, @sharded_sub.account_type
      assert_equal @subscription.account_status, @sharded_sub.account_status
      assert_equal @account.owner_id, @sharded_sub.owner_id

      assert_equal @account.is_serviceable?, @sharded_sub.is_serviceable?
      assert_equal @account.is_active?, @sharded_sub.is_active?
    end

    it "changes when the details of the plan change" do
      old_agents = @account.subscription.max_agents
      @account.sharded_subscription.sync_from_account_database
      @account.sharded_subscription.save!
      assert_equal old_agents, @account.sharded_subscription.max_agents

      new_agents = old_agents + 10
      @account.subscription.base_agents = new_agents
      @account.subscription.save!
      @account.sharded_subscription.reload
      @account.sharded_subscription.sync_from_account_database
      assert_equal new_agents, @account.sharded_subscription.max_agents
    end
  end

  describe 'with a soft-deleted account' do
    before do
      @account = accounts(:trial)
      @subscription = @account.subscription
      @sharded_sub = @account.build_sharded_subscription
      @account.cancel!
      @account.soft_delete!
    end

    it "syncs from account database without errors" do
      @sharded_sub.sync_from_account_database
    end
  end
end
