require_relative '../support/test_helper'

SingleCov.covered!

describe Skip do
  fixtures :accounts, :tickets, :users

  let (:account) { accounts(:minimum) }
  describe "association queries are scoped to the account" do
    before do
      account.skips.create(
        ticket_id: tickets(:minimum_1).id,
        user_id: users(:minimum_agent).id,
        reason: "lazyness"
      )
    end

    it "should show the account_id in the query" do
      assert_sql_queries 2, %r{account_id} do
        Skip.last.ticket.account
        Skip.last.user.account
      end
    end
  end
end
