require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe QueueAudit do
  fixtures :accounts

  class QueueAuditTestJob
    extend Resque::Durable
  end

  let(:job_klass) { QueueAuditTestJob }

  before do
    @account = accounts(:minimum)
    @audit   = QueueAudit.initialize_by_klass_and_args(job_klass, ['hello'])
    @audit.account = @account
    @audit.enqueued!
    @fails_at = @audit.timeout_at + 1.second
  end

  it "includes the account when finding failed jobs" do
    Timecop.freeze(@fails_at) { @failed_audit = QueueAudit.failed.first }

    Account.expects(:find).never
    @failed_audit.account
  end

  it "skips locked accounts" do
    @account.lock!

    Timecop.freeze(@fails_at) do
      assert_equal [], QueueAudit.failed
      assert_equal [], QueueAudit.cleanup(@fails_at)
    end
  end

  it "does not throw an exception when the account does not exist" do
    @audit.account_id = 99999
    @audit.save!

    Timecop.freeze(@fails_at) { @failed_audit = QueueAudit.failed.first }
  end

  describe "for legitimate unknown job_klass" do
    before do
      @audit.job_klass = 'LegitimateJob'
      @old_ignored = QueueAudit::IGNORED_CLASSES
      QueueAudit.send(:remove_const, :IGNORED_CLASSES)
      QueueAudit.const_set('IGNORED_CLASSES', ["LegitimateJob"])
    end

    after do
      QueueAudit.send(:remove_const, :IGNORED_CLASSES)
      QueueAudit.const_set('IGNORED_CLASSES', @old_ignored)
    end

    it "ignores it" do
      ZendeskExceptions::Logger.expects(:record).never
      @audit.enqueue
    end
  end

  describe "encountering an exception during enqueue" do
    before do
      @audit.job_klass = 'UnknownConstant'
    end

    it "raises" do
      @audit.enqueued_id = 'app20.sys.zendesk.com/6155/1331836690/13'
      assert_raise(NameError) { @audit.enqueue }
    end

    it "notifys" do
      @audit.job_klass = job_klass
      @audit.enqueued_id = 'app20.sys.zendesk.com/6155/1331836690/13'
      @audit.expects(:job_klass).raises(Account::LockedException.new(@account))
      ZendeskExceptions::Logger.expects(:record)
      assert_raise(Zendesk::Accounts::Locking::LockedException) { @audit.enqueue }
    end
  end

  describe "delay" do
    before do
      Timecop.freeze(@fails_at + 1.minute)
    end

    describe "with class not defining a custom delay" do
      it "uses default delay" do
        assert(@audit.retryable?)
      end
    end

    describe "with class defining a custom delay" do
      class CustomDelayTestJob
        extend Resque::Durable

        DELAY = 10.minutes

        def self.delay(_enqueue_count)
          DELAY
        end
      end

      let(:job_klass) { CustomDelayTestJob }

      it "uses delay from job class" do
        assert_equal false, @audit.retryable?

        Timecop.freeze(@fails_at + CustomDelayTestJob::DELAY) { assert(@audit.retryable?) }
      end
    end
  end

  describe "when an account is moved to another shard" do
    it "does not retry the audit on the old shard" do
      Timecop.freeze(@fails_at + 1.hour) do
        failed_audit = QueueAudit.failed.first

        assert(failed_audit.retryable?)
        Account.any_instance.stubs(:shard_local?).returns(false)
        assert_equal false, failed_audit.retryable?
      end
    end
  end

  describe 'enqueued!' do
    it 'increments a statsd counter when re-enqueuing' do
      @audit.statsd_client.expects(:increment).with(
        'reenqueued',
        tags: [
          'enqueue_count:2', 'job:QueueAuditTestJob'
        ]
      )
      @audit.enqueued!
    end
  end
end
