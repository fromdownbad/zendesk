require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe AcmeCertificateJobStatus do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:job_status) { account.acme_certificate_job_statuses.build }

  before { Timecop.freeze }

  describe "#working!" do
    before { job_status.working! }

    it "sets the status to working" do
      assert_equal 'working', job_status.status
      assert_equal Time.now, job_status.started_at
    end

    it "is working?" do
      assert(job_status.working?)
    end
  end

  describe "#failed!" do
    before { job_status.failed!("nope") }

    it "sets the status to failed" do
      assert_equal 'failed', job_status.status
      assert_equal Time.now, job_status.completed_at
      assert_equal 'nope', job_status.message
    end

    it "is failed?" do
      assert(job_status.failed?)
    end
  end

  describe "#completed!" do
    before { job_status.completed! }

    it "sets the status to completed" do
      assert_equal 'completed', job_status.status
      assert_equal Time.now, job_status.completed_at
    end

    it "is completed?" do
      assert(job_status.completed?)
    end
  end
end
