require_relative '../support/test_helper'
require_relative '../support/voice_test_helper'
require_relative '../support/business_hours_test_helper'
require_relative '../support/collaboration_settings_test_helper'
require_relative "../support/collaboration_test_helper"
require_relative '../support/arturo_test_helper'
require_relative '../support/ticket_test_helper'
require_relative '../support/routing'
require_relative '../support/rule'
require 'preload'
require 'zendesk/radar_factory'
require 'set'

SingleCov.covered! uncovered: 64

describe Ticket do
  include BusinessHoursTestHelper
  include ArturoTestHelper
  include CollaborationTestHelper
  include TestSupport::Routing::Helper
  include TestSupport::Rule::Helper
  include TicketTestHelper

  fixtures :all

  resque_inline false

  let(:statsd_client) { Zendesk::StatsD::Client.new(namespace: 'ticket') }

  before do
    accounts(:minimum).expire_scoped_cache_key(:ticket_fields)
    Ticket.any_instance.stubs(:statsd_client).returns(statsd_client)
  end

  describe Ticket do
    before do
      @ticket = Ticket.new
      @ticket.account = accounts(:minimum)
      @ticket.account.stubs(:is_serviceable?).returns(true)
    end

    subject { @ticket }

    [
      :account_id,
      :assignee_updated_at,
      :base_score,
      :current_tags,
      :delta,
      :nice_id,
      :requester_updated_at,
      :resolution_time,
      :solved_at,
      :status_updated_at,
      :updated_at,
      :updated_by_type_id,
      :attribute_value_ids
    ].each do |attribute|
      should_not allow_mass_assignment_of(attribute)
    end if RAILS4

    [
      :account,
      :additional_tags,
      :agreement_id,
      :assignee,
      :assignee_id,
      :client,
      :collaborator_ids,
      :comment,
      :created_at,
      :current_collaborators,
      :description,
      :due_date,
      :"due_date(1i)",
      :"due_date(2i)",
      :"due_date(3i)",
      :entry_id,
      :external_id,
      :external_links,
      :fields,
      :force_status_change,
      :group_id,
      :group,
      :linked_id,
      :locale_id,
      :metadata,
      :monitored_twitter_handle_id,
      :priority_id,
      :problem_id,
      :recipient,
      :remove_tags,
      :requester,
      :requester_data,
      :requester_email,
      :requester_id,
      :requester_name,
      :satisfaction_comment,
      :satisfaction_score,
      :screencasts,
      :set_collaborators,
      :set_followers,
      :set_tags,
      :sharing_agreement_custom_fields,
      :skip_dm,
      :solved,
      :status_id,
      :subject,
      :submitter_id,
      :submitter,
      :tags,
      :ticket_field_entries,
      :ticket_form_id,
      :ticket_type_id,
      :twitter_status_message_id,
      :unshare_ticket,
      :via_followup_source_id,
      :via_id,
      :custom_id_override,
      :original_recipient_address,
      :organization,
      :organization_id,
      :brand,
      :brand_id
    ].each do |attribute|
      should allow_mass_assignment_of(attribute)
    end if RAILS4

    describe "when executing triggers on save" do
      let(:account) { accounts(:minimum) }
      let(:ticket)  { tickets(:minimum_1) }
      let(:user)    { users(:minimum_agent) }

      before do
        ticket.account = account
        ticket.current_user = user
        ticket.will_be_saved_by(user)
      end

      describe_with_arturo_enabled :pause_trigger_execution do
        before { ticket.save! }
        let (:error) { ticket.audits.last.events.detect { |event| event.class.name == "TranslatableError" } }

        before_should "not run triggers" do
          Zendesk::Rules::Trigger::Execution.expects(:execute).never
        end

        it "creates a TranslatableError event and adds it to the audit" do
          assert error.present?
        end

        it "has the correct error message" do
          assert error.value[:key] == "txt.error_message.models.ticket.audit_event.paused_execution"
        end
      end

      describe_with_arturo_disabled :pause_trigger_execution do
        it 'executes triggers' do
          Zendesk::Rules::Trigger::Execution.expects(:execute).once.returns(Zendesk::Rules::Trigger::RunState.new)
          ticket.save!
        end

        describe 'with instrumentation' do
          let(:tag_args) { ['zendesk.ticket.trigger_execution.time', 123.4] }
          let(:active_span)      { stub(:span) }
          let(:parent_span)      { stub(:parent_span) }
          let(:grandparent_span) { stub(name: 'rack.request') }

          before do
            Datadog.tracer.stubs(:active_span).returns(active_span)
            Benchmark.stubs(:ms).returns(tag_args.last).yields
          end

          describe_with_arturo_enabled :instrument_ticket_save do
            before { Benchmark.stubs(:ms).returns(tag_args.last).yields }

            describe 'when the Datadog `active_span` is defined' do
              before { active_span.stubs(parent: parent_span) }

              describe 'and the parent span is defined' do
                before { parent_span.stubs(parent: grandparent_span) }

                describe 'and the grandparent span is defined' do
                  describe 'and the grandparent span name is `rack.reqeust`' do
                    before { ticket.save! }

                    before_should 'set appropriate tags' do
                      grandparent_span.stubs(:set_tag).with { |args| args != tag_args }
                      grandparent_span.expects(:set_tag).with(*tag_args)
                    end
                  end

                  describe 'and the grandparent span name is not `rack.request`' do
                    let(:grandparent_span) { stub(name: 'something') }
                    before { ticket.save! }

                    before_should 'not try to set tags' do
                      grandparent_span.expects(:set_tag).with(*tag_args).never
                    end
                  end
                end

                describe 'and the grandparent span is not defined' do
                  let(:grandparent_span) { nil }

                  it 'does not try to determine the span name' do
                    ticket.save!
                  end
                end
              end

              describe 'and the parent span is not defined' do
                let(:parent_span) { nil }

                it 'does not try to access grandparent span' do
                  ticket.save!
                end
              end
            end

            describe 'when the Datadog `active_span` is not defined' do
              let(:active_span) { nil }

              it 'does not try to access the parent span' do
                ticket.save!
              end
            end
          end

          describe_with_arturo_disabled :instrument_ticket_save do
            before { ticket.save! }

            before_should 'not try to instrument trigger run' do
              active_span.expects(:parent).never
            end
          end
        end
      end
    end

    it "ignores latest_recipients column" do
      assert_includes Ticket.ignored_columns, "latest_recipients"
    end

    it "has current via id when assigning via id" do
      @ticket.via_id = ViaType.Mail
      assert_equal ViaType.Mail, @ticket.via_id
      assert_equal @ticket.via_id, @ticket.current_via_id
    end

    it "has current via reference id when assigning via reference id" do
      @ticket.via_reference_id = 12345
      assert_equal 12345, @ticket.via_reference_id
      assert_equal @ticket.via_reference_id, @ticket.current_via_reference_id
    end

    it "returns the correct description for tickets created via rule" do
      @ticket.via_id = ViaType.RULE
      assert_equal "By Rule", @ticket.via_description
    end

    describe "original_recipient_address=" do
      before do
        @ticket.original_recipient_address = "hello@test.com"
        @ticket.original_recipient_address = email
      end

      describe "invalid email" do
        let(:email) { "asdasd" }

        it "does not be set" do
          assert_nil @ticket.original_recipient_address
        end
      end

      describe "invalid-ish email" do
        let(:email) { "<testT!@test.com" }

        it "is sanitized" do
          assert_equal "testt!@test.com", @ticket.original_recipient_address
        end
      end

      describe "valid email" do
        let(:email) { "test@test.com" }

        it "is set" do
          assert_equal email, @ticket.original_recipient_address
        end
      end
    end

    it "allows setting ticket_field_entries to nil" do
      @ticket.ticket_field_entries = nil
    end

    it "bumps the nice_id sequence by 1" do
      @ticket.account = accounts(:minimum)
      before_sequence = @ticket.account.nice_id_sequence.peek
      @ticket.build_requester(name: 'Sir Skisalot', email: 'ski@example.com', account: @ticket.account)
      @ticket.requester_name  = 'Mr Snowboarder'
      @ticket.requester_email = 'board@example.com'
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.description = "foo"
      @ticket.save!

      after_sequence = @ticket.account.nice_id_sequence.peek
      assert_equal before_sequence + 1, after_sequence
    end

    it "does not create a new requester on validation" do
      @ticket.account = accounts(:minimum)
      @ticket.build_requester(name: 'Sir Skisalot', email: 'ski@example.com', account: @ticket.account)
      @ticket.requester_name  = 'Mr Snowboarder'
      @ticket.requester_email = 'board@example.com'
      refute_difference 'User.count(:all)' do
        @ticket.valid?
      end
    end

    describe "#satisfaction_probability" do
      before do
        @ticket.description = 'hai'
        @ticket.requester = users(:minimum_agent)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
      end

      it "returns the latest probability when it is updated" do
        @ticket.create_ticket_prediction(satisfaction_probability: 0.1)
        Timecop.travel(5)
        @ticket.satisfaction_probability = 0.5

        assert_equal 0.5, @ticket.satisfaction_probability
      end

      it 'saves the prediction update properly' do
        @ticket.create_ticket_prediction(satisfaction_probability: 0.1)
        Timecop.travel(5)
        @ticket.satisfaction_probability = 0.5

        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        t = Ticket.find @ticket.id
        assert_equal 0.5, t.satisfaction_probability
      end

      it "doesn't explode if there's no satisfaction_probability recorded" do
        assert_nil @ticket.satisfaction_probability
      end
    end

    describe "#last_few_comments" do
      before { @ticket = tickets(:minimum_6) }

      it "returns the last three public comments offset by 1" do
        last_few_comments = @ticket.last_few_comments
        assert_equal 3, last_few_comments.map(&:is_public).compact.length
        assert_not_includes last_few_comments.map(&:id), @ticket.comments.where(is_public: true).last.id
      end
    end

    describe "#satisfaction_probability=" do
      before do
        @ticket.description = 'hai'
        @ticket.requester = users(:minimum_agent)
        @ticket.satisfaction_probability = 0.1
        @ticket.will_be_saved_by(users(:minimum_agent))
      end

      it "tracks changes to probability" do
        @ticket.satisfaction_probability = 0.5
        @ticket.save!

        assert @ticket.events.where(value_previous: 0.1, value: 0.5, value_reference: 'satisfaction_probability').present?
      end
    end

    describe "#set_requester with a requester" do
      let(:spanish) { translation_locales(:spanish) }
      let(:japanese) { translation_locales(:japanese) }
      let(:enabled) { false }

      before do
        Arturo.enable_feature!(:disable_updating_requester_via_ticket) if enabled

        @requester = users(:minimum_end_user)
        @requester.external_id = 1234
        @requester.save!
        @requester.update_column(:locale_id, spanish.id)

        @ticket = tickets(:minimum_1)
        @ticket.update_column(:locale_id, japanese.id)
        @ticket.requester = @requester
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!

        @ticket.requester_data = subject
      end

      describe "with an email" do
        describe "that exists" do
          subject { { email: @requester.email, name: "Name" } }

          it "sets the requester" do
            assert_equal @requester, @ticket.requester
          end
        end

        describe "that exists but is padded with whitespace" do
          subject { { email: padded_email, name: "Name" } }
          let(:padded_email) { " #{@requester.email} " }

          it "sets the requester despite whitespace padding" do
            assert_equal @requester, @ticket.requester
          end
        end

        describe "that doesn't exist" do
          subject { { email: "different.email@gmail.com" } }

          it "sets the email" do
            assert_equal "different.email@gmail.com", @ticket.requester.email
          end

          it "creates a new user" do
            assert @ticket.requester.new_record?
          end
        end

        describe "when the email is garbage" do
          subject { { email: ["doob"] } }

          it "does not blow up" do
            @ticket.requester
          end
        end
      end

      describe "with an external_id" do
        describe 'an external_id that is too long' do
          let(:external_id) { "a" * 256 }

          it 'should not be valid' do
            @ticket.external_id = external_id
            @ticket.will_be_saved_by(users(:minimum_agent))

            refute @ticket.save
            assert @ticket.errors.full_messages.first.include?('external_id is too long')
          end
        end

        describe "with a different external_id" do
          subject { { external_id: "4567", email: @requester.email } }

          describe "with a valid ticket" do
            before do
              @ticket.description = "Hello"
              @ticket.will_be_saved_by(users(:minimum_agent))
              @ticket.save!
            end

            it "updates user's external_id" do
              assert_equal "4567", @requester.reload.external_id
            end
          end

          describe "with an invalid ticket" do
            before do
              @ticket.status_id = nil
              @ticket.will_be_saved_by(users(:minimum_agent))
            end

            it "does not update user's external_id" do
              refute @ticket.save
              assert_equal "1234", @requester.reload.external_id
            end
          end
        end

        describe "with a locale_id" do
          subject { { email: "different.email@gmail.com", name: "John Doe", locale_id: 6 } }
          before do
            @ticket.via_id = ViaType.WEB_FORM
          end

          it "sets the locale_id" do
            assert_equal 6, @ticket.requester.locale_id
          end

          it "creates a new user" do
            assert @ticket.requester.new_record?
          end
        end

        describe "with a different email" do
          subject { { external_id: 1234, email: "different.email@gmail.com" } }

          it "errors" do
            assert @ticket.invalid?
            assert @ticket.errors[:requester].present?
          end
        end

        describe "with the same email" do
          subject { { external_id: 1234, email: @requester.email } }

          it "sets the requester" do
            assert_equal @requester, @ticket.requester
          end
        end

        describe "with data" do
          subject { { external_id: 1234, email: @requester.email, name: "test user" } }

          before do
            @ticket.description = "Hello"
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!
          end

          it "updates the requester" do
            assert_equal "test user", @requester.reload.name
          end
        end

        describe "with random data" do
          subject { { external_id: 1234, email: @requester.email, name: "test user", phone: "1234567891" } }

          before do
            @ticket.description = "Hello"
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!
          end

          it "updates the requester" do
            assert_equal "test user", @requester.reload.name
          end

          it "does not update the phone number" do
            assert_nil @requester.reload.phone
          end
        end

        describe "with arturo feature disable_updating_requester_via_ticket enbled" do
          let(:enabled) { true }

          describe "with a different external_id" do
            subject { { external_id: "4567", email: @requester.email } }

            before do
              @ticket.description = "Hello"
              @ticket.will_be_saved_by(users(:minimum_agent))
              @ticket.save!
            end

            it "does not update user's external_id" do
              assert_equal "1234", @requester.reload.external_id
            end
          end

          describe "with data" do
            subject { { external_id: 1234, email: @requester.email, name: "test user" } }

            before do
              @ticket.description = "Hello"
              @ticket.will_be_saved_by(users(:minimum_agent))
              @ticket.save!
            end

            it "does not update the requester" do
              refute_equal "test user", @requester.reload.name
            end
          end
        end
      end

      describe "with nothing" do
        subject { { name: "test user" } }

        it "creates a new user" do
          assert @ticket.requester.new_record?
        end
      end

      it "updates the locale_id to the new requesters locale_id" do
        assert_equal @ticket.locale_id, @requester.locale_id
        assert_equal spanish.id, @ticket.locale_id
      end
    end

    describe "saved with {{ticket.title}} placeholder in the subject field" do
      let(:ticket)      { tickets(:minimum_1) }
      let(:placeholder) { "{{ticket.id}} {{ticket.title}}" }

      before do
        ticket.will_be_saved_by(@minimum_admin)
      end

      it "interpolates the placeholder" do
        ticket.subject = placeholder
        ticket.save
        assert_equal "{{ticket.id}} minimum 1 ticket", ticket.subject
      end

      describe "when placeholder already existed in the ticket's subject field" do
        before do
          Ticket.where(id: ticket.id).update_all(subject: "something {{ticket.title}} something")
          ticket.will_be_saved_by(@minimum_admin)
        end

        it "does not interpolate the placeholder when the subject has changed" do
          ticket.reload.subject = placeholder
          ticket.save!
          assert_equal "{{ticket.id}} {{ticket.title}}", ticket.reload.subject
        end

        it "does not interpolate the placeholder when the subject has not changed" do
          ticket.save!
          assert_equal "something {{ticket.title}} something", ticket.reload.subject
        end

        it "does not blow up when the ticket is a new record" do
          ticket.stubs(:subject_was).returns(nil)
          ticket.subject = "Brand new ticket with {{ticket.title}}"
          ticket.save!
          assert_equal "Brand new ticket with {{ticket.title}}", ticket.reload.subject
        end
      end
    end

    describe "#current_ticket_form" do
      let(:ticket)  { tickets(:minimum_1) }
      let(:account) { ticket.account }
      let(:default_form) do
        form = TicketForm.init_default_form(account)
        form.save!

        form
      end
      let(:another_form) do
        form_params = {
          ticket_form: {
            name: "Non default",
            display_name: "Non default",
            position: 2,
            default: false,
            active: true,
            end_user_visible: true
          }
        }

        ticket_form = Zendesk::TicketForms::Initializer.new(account, form_params).ticket_form
        ticket_form.save!

        ticket_form
      end

      describe "when the account has no access to ticket forms" do
        before { account.stubs(:has_ticket_forms?).returns(false) }

        it "returns nothing" do
          refute ticket.current_ticket_form
        end
      end

      describe "when the account has multiple ticket forms" do
        before do
          account.stubs(:has_ticket_forms?).returns(true)
          default_form
          another_form
        end

        describe "and the ticket has an assigned ticket_form" do
          before { ticket.ticket_form = another_form }

          it "returns the assigned ticket form" do
            assert_equal another_form, ticket.current_ticket_form
          end
        end

        describe "and the ticket has no assigned ticket_form" do
          before { ticket.ticket_form = nil }
        end

        it "returns the default ticket form" do
          assert_equal default_form, ticket.current_ticket_form
        end
      end
    end

    describe "#schedule" do
      let(:ticket)  { tickets(:minimum_1) }
      let(:account) { ticket.account }

      before do
        Account.any_instance.stubs(:has_multiple_schedules?).returns(true)
        2.times do |index|
          account.schedules.create(
            name:      "schedule_#{index}",
            time_zone: account.time_zone
          )
        end
      end

      describe 'when explicitly assigned a schedule' do
        before do
          ticket.create_ticket_schedule(
            account: account,
            schedule: account.schedules.last
          )
        end

        it "returns that schedule" do
          assert_equal ticket.ticket_schedule.schedule, ticket.schedule
        end
      end

      describe 'when not explicitly assigned a schedule' do
        it "returns the account schedule" do
          assert_equal account.schedule, ticket.schedule
        end
      end
    end

    describe "#set_requester with an email that isn't primary" do
      subject { { email: "test@test.com" } }

      before do
        @requester = users(:minimum_end_user)

        @requester.identities << UserEmailIdentity.new do |id|
          id.user = @requester
          id.account = @requester.account
          id.value = "test@test.com"
          id.is_verified = true
        end

        @ticket.requester_data = subject
      end

      it "sets the requester" do
        assert_equal @requester, @ticket.requester
      end
    end

    describe "#set_requester with an external_id and an email that isn't primary" do
      subject { { external_id: "1234", email: "deliverable@example.com" } }

      before do
        @requester = users(:minimum_end_user)
        @requester.external_id = 1234
        @requester.save!
      end

      it "sets the requester" do
        @ticket.requester_data = subject
        assert_equal @requester, @ticket.requester
      end
    end

    describe "#set_requester with a previous requester that does not exist" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.update_column(:requester_id, 1234567890)
      end

      it "sets the requester" do
        @ticket.requester = users(:minimum_end_user)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
      end
    end

    describe "#set_requester without a requester" do
      before do
        @requester = users(:minimum_end_user)
        @requester.external_id = 1234
        @requester.save!
        @ticket.requester_data = subject
      end

      describe "with an email" do
        describe "that exists" do
          subject { { email: @requester.email, name: "Name" } }

          it "sets the requester" do
            assert_equal @requester, @ticket.requester
          end
        end

        describe "that doesn't exist" do
          subject { { email: "different.email@gmail.com" } }

          it "sets the email" do
            assert_equal "different.email@gmail.com", @ticket.requester.email
          end

          it "creates a new user" do
            assert @ticket.requester.new_record?
          end
        end
      end

      describe "with an external_id" do
        describe "with a different external_id" do
          subject { { external_id: "4567", email: @requester.email } }

          describe "with a valid ticket" do
            before do
              @ticket.description = "Hello"
              @ticket.will_be_saved_by(users(:minimum_agent))
              @ticket.save!
            end

            it "updates user's external_id" do
              assert_equal "4567", @requester.reload.external_id
            end
          end

          describe "with an invalid ticket" do
            before do
              @ticket.will_be_saved_by(users(:minimum_agent))
            end

            it "does not update user's external_id" do
              refute @ticket.save
              assert_equal "1234", @requester.reload.external_id
            end
          end
        end

        describe "with a different email" do
          subject { { external_id: 1234, email: "different.email@gmail.com" } }

          it "errors" do
            assert @ticket.invalid?
            assert @ticket.errors[:requester].present?
          end
        end

        describe "with the same email" do
          subject { { external_id: 1234, email: @requester.email } }

          it "sets the requester" do
            assert_equal @requester, @ticket.requester
          end
        end

        describe "with data" do
          subject { { external_id: 1234, email: @requester.email, name: "test user" } }

          before do
            @ticket.description = "Hello"
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!
          end

          it "updates the requester" do
            assert_equal "test user", @requester.reload.name
          end
        end

        describe "with random data" do
          subject { { external_id: 1234, email: @requester.email, name: "test user", phone: "1234567891" } }

          before do
            @ticket.description = "Hello"
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!
          end

          it "updates the requester" do
            assert_equal "test user", @requester.reload.name
          end

          it "does not update the phone number" do
            assert_nil @requester.reload.phone
          end
        end
      end

      describe "with nothing" do
        subject { { name: "test user" } }

        it "creates a new user" do
          assert @ticket.requester.new_record?
        end
      end

      describe "with invalid data" do
        describe "integer" do
          subject { 7 }

          it "dos nothing" do
            assert_nil @ticket.requester
          end
        end

        describe "array" do
          subject { [1] }

          it "dos nothing" do
            assert_nil @ticket.requester
          end
        end

        describe "string" do
          subject { "hello" }

          it "dos nothing" do
            assert_nil @ticket.requester
          end
        end
      end
    end

    describe "with a comment that is redacted" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(@ticket.assignee)
        @comment = @ticket.add_comment(body: "hello")
        @ticket.comment.build_redaction_event
        @ticket.save!
        @ticket.reload
      end

      it "has redactions" do
        assert @comment.reload.redaction_event
        assert_equal [@comment.redaction_event], @ticket.redaction_events
        assert @ticket.redactions?
      end
    end

    describe "without redaction events" do
      it "does not be considered redacted" do
        assert @ticket.redaction_events.blank?
        assert_equal false, @ticket.redactions?
      end
    end

    describe "omniredact_comment!" do
      describe_with_arturo_enabled :agent_workspace_omni_redaction do
        describe "with a comment that is redacted" do
          before do
            @ticket = tickets(:minimum_1)
            @ticket.will_be_saved_by(@ticket.assignee)
            @comment = @ticket.add_comment(body: "hello")
            @ticket.save!
            @ticket.reload
          end

          it "has redactions applied by omniredaction" do
            @ticket.omniredact_comment!(@ticket.assignee, @comment, '<redact>he</redact>llo')
            assert @comment.reload.redaction_event
            assert_equal [@comment.redaction_event], @ticket.redaction_events
            assert @ticket.redactions?
          end
        end
      end

      describe_with_arturo_disabled :agent_workspace_omni_redaction do
        describe "with a comment that is redacted" do
          before do
            @ticket = tickets(:minimum_1)
            @ticket.will_be_saved_by(@ticket.assignee)
            @comment = @ticket.add_comment(body: "hello")
            @ticket.save!
            @ticket.reload
          end

          it "does not have redactions applied by omniredaction" do
            @ticket.omniredact_comment!(@ticket.assignee, @comment, '<redact>he</redact>llo')
            refute @comment.reload.redaction_event
            refute @ticket.redactions?
          end
        end
      end
    end

    describe "#validate_requester_is_not_suspended" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by @ticket.requester
        @ticket.requester.suspended = true
      end

      it "does not allow to create with suspended requester" do
        @ticket = @ticket.dup
        @ticket.requester.suspended = true
        refute_valid @ticket
        assert_includes @ticket.errors[:requester].join, "suspended"
      end

      it "allows to update with suspended requester" do
        assert_valid @ticket
      end
    end

    describe "with a nonexistant requester" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.requester_id = 1929493459
        @ticket.add_comment(body: "Hello!", is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
      end

      it "does not be valid" do
        refute @ticket.valid?
      end
    end

    describe "requester_id and requester_data setting" do
      before do
        @account = accounts(:minimum)
        @agent = users(:minimum_agent)
        @user = users(:minimum_end_user)

        @ticket = Ticket.new do |t|
          t.account = @account
          t.subject = "Test"
          t.description = "Hello"
          t.submitter_id = @agent.id
          t.requester_id = requester_id
          t.requester_data = requester_data
        end

        @ticket.will_be_saved_by(@agent)
      end

      describe "with a invalid requester_id and existing requester_data" do
        let(:requester_data) { { email: @user.email } }
        let(:requester_id) { "0" }

        before { @ticket.save! }

        it "has the new user as the requester" do
          assert_equal @user.email, @ticket.reload.requester.email
        end
      end

      describe "with a invalid requester_id and valid requester_data" do
        let(:requester_data) { { name: "Test User", email: "test@user.com" } }
        let(:requester_id) { "0" }

        before { @ticket.save! }

        it "has the new user as the requester" do
          assert_equal "test@user.com", @ticket.reload.requester.email
        end
      end

      describe "with an invalid requester_id and invalid requester_data" do
        let(:requester_data) { { email: "non.existant@gmail.com" } }
        let(:requester_id) { 123145 }

        it "has the user as the requester" do
          assert_raise ActiveRecord::RecordInvalid do
            @ticket.save!
          end
        end
      end

      describe "with a valid requester_id and invalid requester_data" do
        let(:requester_data) { { email: "non.existant@gmail.com" } }
        let(:requester_id) { @agent.id }

        it "has the user as the requester" do
          assert_raise ActiveRecord::RecordInvalid do
            @ticket.save!
          end
        end
      end

      describe "with a valid requester_id and valid requester_data" do
        let(:requester_data) { { name: "Test User", email: "test@user.com" } }
        let(:requester_id) { @agent.id }

        before { @ticket.save! }

        it "has the user as the requester" do
          assert_equal "test@user.com", @ticket.reload.requester.email
        end
      end

      describe "with a valid requester_id and existing requester_data" do
        let(:requester_data) { { email: @user.email } }
        let(:requester_id) { @agent.id }

        before { @ticket.save! }

        it "has the user as the requester" do
          assert_equal @user.email, @ticket.reload.requester.email
        end
      end
    end

    describe "with an otherwise valid ticket" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(users(:minimum_agent))
        assert @ticket.valid?
      end

      it "is invalid if we set an invalid priority_id" do
        @ticket.priority_id = 5
        refute @ticket.valid?
      end

      it "is invalid if we set group_id to a non existent group" do
        @ticket.assignee = nil
        @ticket.group_id = 0
        refute @ticket.valid?
      end

      it "is invalid if we set organization_id to a non existent organization" do
        @ticket.organization_id = 0
        refute @ticket.valid?
      end
    end

    describe "flagged comments" do
      describe "flagging existing ticket" do
        before do
          @agent = users(:minimum_agent)
        end

        describe "comment is untrusted" do
          before do
            @ticket = tickets(:minimum_1)
            @ticket.will_be_saved_by(@agent)
            @ticket.add_comment(body: 'existing ticket comment')

            @ticket.add_flags!([EventFlagType.DEFAULT_UNTRUSTED])
            @comment = @ticket.comment
            @ticket.save!
          end

          it "still trusts other comments" do
            initial_comment = @ticket.comments.first
            refute_equal @comment, initial_comment
            assert initial_comment.trusted?
          end

          it "adds a comment to the ticket" do
            assert @ticket.comments.include?(@comment)
          end

          it "marks the audit and comment as flagged" do
            assert @comment.flagged?
            @ticket.valid? # establishes audit/comment relationship
            assert @comment.audit.flagged?
          end

          it "marks the ticket, audit and comment as untrusted" do
            refute @comment.trusted?
            @ticket.valid? # establishes audit/comment relationship
            refute @comment.audit.trusted?
            refute @ticket.trusted?
          end

          it "remembers the comment is untrusted and flagged" do
            @comment.reload
            refute @comment.trusted?
            assert @comment.flagged?
          end
        end

        describe "flagging audit of existing ticket after adding new trusted comment" do
          before do
            # minimum_1 ticket has an untrusted comment in it by default, so use minimum_2
            @ticket_trusted = tickets(:minimum_2)
            @ticket_trusted.will_be_saved_by(@agent)
            @ticket_trusted.add_comment(body: 'existing ticket comment')

            @ticket_trusted.add_flags!([EventFlagType.DEFAULT_TRUSTED])
            @comment = @ticket_trusted.comment
            @ticket_trusted.save!
          end

          it "still trusts other comments" do
            initial_comment = @ticket_trusted.comments.first
            assert initial_comment.trusted?
          end

          it "adds a comment to the ticket" do
            assert @ticket_trusted.comments.include?(@comment)
          end

          it "marks the audit and comment as flagged" do
            assert @comment.flagged?
            @ticket_trusted.valid? # establishes audit/comment relationship
            assert @comment.audit.flagged?
          end

          it "does not mark the ticket, audit and comment as untrusted" do
            assert @comment.trusted?
            @ticket_trusted.valid? # establishes audit/comment relationship
            assert @comment.audit.trusted?
            assert @ticket_trusted.trusted?
          end

          it "remembers the comment is trusted and flagged" do
            @comment.reload
            assert @comment.trusted?
            assert @comment.flagged?
          end
        end
      end

      describe "flagging new ticket before will_be_saved_by" do
        before do
          @agent = users(:minimum_agent)
        end

        describe "comment is untrusted" do
          before do
            @ticket.requester = @agent
            @ticket.description = 'wow'

            @ticket.add_flags!([EventFlagType.DEFAULT_UNTRUSTED])
            @ticket.will_be_saved_by(@agent)
            @ticket.add_comment(body: 'untrusted')
            @comment = @ticket.comment
            @ticket.save!
          end

          it "adds a comment to the ticket" do
            assert @ticket.comments.include?(@comment)
          end

          it "marks the audit and comment as flagged" do
            assert @comment.flagged?
            @ticket.valid? # establishes audit/comment relationship
            assert @comment.audit.flagged?
          end

          it "marks the ticket, audit and comment as untrusted" do
            refute @comment.trusted?
            @ticket.valid? # establishes audit/comment relationship
            refute @comment.audit.trusted?
            refute @ticket.trusted?
          end

          it "remembers the comment is untrusted and flagged" do
            @comment.reload

            refute @comment.trusted?
            assert @comment.flagged?
          end
        end

        describe "comment is flagged but trusted" do
          before do
            # minimum_1 ticket has an untrusted comment in it by default, so use minimum_2
            @ticket_trusted = tickets(:minimum_2)
            @ticket_trusted.requester = @agent
            @ticket_trusted.description = 'wow'

            @ticket_trusted.add_flags!([EventFlagType.DEFAULT_TRUSTED])
            @ticket_trusted.will_be_saved_by(@agent)
            @ticket_trusted.add_comment(body: 'trusted')
            @comment = @ticket_trusted.comment
            @ticket_trusted.save!
          end

          it "adds a comment to the ticket" do
            assert @ticket_trusted.comments.include?(@comment)
          end

          it "marks the audit and comment as flagged" do
            assert @comment.flagged?
            @ticket_trusted.valid? # establishes audit/comment relationship
            assert @comment.audit.flagged?
          end

          it "does not mark the ticket, audit and comment as untrusted" do
            assert @comment.trusted?
            @ticket.valid? # establishes audit/comment relationship
            assert @comment.audit.trusted?
            assert @ticket_trusted.trusted?
          end

          it "remembers the comment is trusted and flagged" do
            @comment.reload
            assert @comment.trusted?
            assert @comment.flagged?
          end
        end
      end

      describe "#new_audit_flags_options" do
        it "uses options stored in AuditFlags" do
          ticket = tickets(:minimum_1)
          AuditFlags.any_instance.expects(:options).returns({})
          ticket.send(:new_audit_flags_options)
        end
      end

      describe "#add_audit_flags!" do
        it "uses AuditFlags#merge_flags" do
          flag_types = [EventFlagType.DEFAULT_UNTRUSTED]
          ticket = tickets(:minimum_1)
          AuditFlags.any_instance.expects(:merge_flags).with(flag_types, {})
          ticket.send(:add_audit_flags!, flag_types)
        end
      end

      describe "#flags_allow_comment?" do
        let(:ticket) { tickets(:minimum_1) }
        let(:flags_allowing_comment) { [EventFlagType.OTHER_USER_UPDATE] }
        let(:flags_not_allowing_comment) { [EventFlagType.ATTACHMENT_TOO_BIG] }
        let(:flags) { [] }
        let(:submitter) { users(:minimum_agent) }

        describe "when ticket is using new_audit_flags" do
          before do
            ticket.add_flags!(flags)
            ticket.will_be_saved_by(submitter)
            ticket.add_comment(body: 'no audit yet')
          end

          describe "and flags that allow comment are added" do
            let(:flags) { flags_allowing_comment }

            it "uses new_audit_flags instead of comment" do
              Comment.any_instance.expects(:current_audit).never
              ticket.flags_allow_comment?
            end

            it { assert ticket.flags_allow_comment? }
          end

          describe "and flags that do not allow comment are added" do
            let(:flags) { flags_not_allowing_comment }

            it "tries both new_audit_flags and comment for flag info" do
              Comment.any_instance.expects(:current_audit)
              ticket.flags_allow_comment?
            end

            it { refute ticket.flags_allow_comment? }
          end
        end

        describe "when flags are on ticket's audit" do
          before do
            ticket.will_be_saved_by(submitter)
            ticket.add_comment(body: 'has an audit')
            ticket.add_flags!(flags)
          end

          describe "and flags that allow comment are added" do
            let(:flags) { flags_allowing_comment }

            it "uses comment's flags instead of new_audit_flags" do
              Comment.any_instance.expects(:current_audit)
              ticket.flags_allow_comment?
            end

            it { assert ticket.flags_allow_comment? }
          end

          describe "and flags that do not allow comment are added" do
            let(:flags) { flags_not_allowing_comment }

            it "tries both new_audit_flags and comment for flag info" do
              Comment.any_instance.expects(:current_audit)
              ticket.flags_allow_comment?
            end

            it { refute ticket.flags_allow_comment? }
          end
        end
      end
    end

    describe "with a credit card number" do
      before do
        agent = users(:minimum_agent)
        @ticket.requester = agent
        @ticket.will_be_saved_by(agent)
      end

      describe "while sanitization is enabled" do
        before { Account.any_instance.stubs(has_credit_card_sanitization_enabled?: true) }

        describe "in the description" do
          let(:credit_card_string) { 'Hello, my credit card number is 4111 1111 1111 1111' }
          before do
            @ticket.description = credit_card_string
            @ticket.save!
          end

          it "sanitizes the description" do
            assert_equal "Hello, my credit card number is 4111 11▇▇ ▇▇▇▇ 1111", @ticket.description
          end

          it "sanitizes the comment" do
            assert_equal "Hello, my credit card number is 4111 11▇▇ ▇▇▇▇ 1111", @ticket.comments.first.to_s
          end

          it "adds a credit_card_sanitized tag" do
            assert_includes @ticket.tag_array, "system_credit_card_redaction"
          end

          it "adds a CommentRedactionEvent" do
            assert_equal CommentRedactionEvent, @ticket.redaction_events.first.class
          end

          describe "when detected credit card number is actually a part of a longer string" do
            let(:credit_card_string) { "<div class='4111111111111111abc'></div>" }

            it "doesn't sanitize" do
              assert_equal credit_card_string, @ticket.description
            end
          end
        end

        describe "in the subject" do
          before do
            @ticket.subject = 'Hello, my credit card number is 4111 1111 1111 1111'
            @ticket.description = "Hah"
            @ticket.save!
          end

          it "sanitizes the subject" do
            assert_equal "Hello, my credit card number is 4111 11▇▇ ▇▇▇▇ 1111", @ticket.subject
          end

          it "adds a credit_card_sanitized tag" do
            assert_includes @ticket.tag_array, "system_credit_card_redaction"
          end
        end

        describe "in custom fields" do
          before do
            @ticket = tickets(:minimum_2) # has entries
            @ticket.will_be_saved_by(@ticket.account.owner)
            @ticket_field_entry = @ticket.ticket_field_entries.first
            @ticket.fields = { @ticket_field_entry.ticket_field_id => "credit card number is 4111 1111 1111 1111" }
            @ticket.save!
          end

          it "sanitizes custom fields" do
            assert_equal "credit card number is 4111 11▇▇ ▇▇▇▇ 1111", @ticket_field_entry.reload.value
          end

          it "adds a credit_card_sanitized tag" do
            assert_includes @ticket.tag_array, "system_credit_card_redaction"
          end

          it "adds a TicketFieldRedactEvent" do
            assert_equal TicketFieldRedactEvent, @ticket.redaction_events.first.class
          end
        end
      end

      describe "while sanitization is disabled" do
        before { Account.any_instance.stubs(has_credit_card_sanitization_enabled?: false) }

        describe "in the description" do
          before do
            @ticket.description = 'Hello, my credit card number is 4111 1111 1111 1111'
            @ticket.save!
          end

          it "does not sanitize the ticket" do
            assert_equal 'Hello, my credit card number is 4111 1111 1111 1111', @ticket.description
            assert_equal 'Hello, my credit card number is 4111 1111 1111 1111', @ticket.comments.first.to_s
          end
        end

        describe "in the subject" do
          before do
            @ticket.subject = 'Hello, my credit card number is 4111 1111 1111 1111'
            @ticket.description = 'lol'
            @ticket.save!
          end

          it "does not sanitize the subject" do
            assert_equal 'Hello, my credit card number is 4111 1111 1111 1111', @ticket.subject
          end
        end

        describe "in custom fields" do
          before do
            @ticket = tickets(:minimum_2) # has entries
            @ticket.will_be_saved_by(@ticket.account.owner)
            @ticket_field_entry = @ticket.ticket_field_entries.first
            @ticket.fields = { @ticket_field_entry.ticket_field_id => "credit card number is 4111 1111 1111 1111" }
            @ticket.save!
          end

          it "does not sanitize custom fields" do
            assert_equal "credit card number is 4111 1111 1111 1111", @ticket_field_entry.reload.value
          end
        end
      end
    end

    describe "validate!" do
      let(:ticket) { tickets(:minimum_2).dup }

      before { ticket.will_be_saved_by(users(:minimum_agent)) }

      it "does not set the nice id" do
        ticket.nice_id = nil
        ticket.expects(:set_nice_id).never
        assert ticket.valid?
        ticket.validate!
      end

      it "raises an error when invalid" do
        ticket.priority_id = 7000
        refute ticket.valid?
        assert_raise(ActiveRecord::RecordInvalid) { ticket.validate! }
      end

      it "does not raise an error when valid" do
        assert ticket.valid?, ticket.errors.inspect
        assert ticket.validate!
      end

      describe "overflowing tags" do
        let(:ticket) { tickets(:minimum_2) }

        it "truncates extraneous tags" do
          tags = ('a'..'y').map { |s| s * 202 }
          pruned_tags = ["z1" * 100, "z2" * 10] # both of these are too large to fit at the end.
          ticket.set_tags = tags + pruned_tags
          ticket.will_be_saved_by(ticket.account.owner)
          assert ticket.save
          refute ticket.errors[:current_tags].any?
          error = ticket.audits.last.events.find { |e| e.is_a? TranslatableError }
          assert_equal ticket.current_tags, tags.join(' ')
          assert_equal error.value,
            key: "ticket_fields.current_tags.too_long_truncated", pruned_tags: pruned_tags.join(' ')
        end
      end
    end

    it "does not change nice_id once set" do
      @ticket.account.nice_id_sequence.stubs(:next).returns(77)
      @ticket.set_nice_id
      assert_equal @ticket.nice_id, 77
      @ticket.account.nice_id_sequence.stubs(:next).returns(88)
      @ticket.set_nice_id
      assert_equal @ticket.nice_id, 77
    end

    describe "url" do
      before do
        @ticket = tickets(:minimum_1)
      end

      it "is the agent url" do
        assert_equal "https://minimum.zendesk-test.com/agent/tickets/1", @ticket.url(for_agent: true)
      end

      describe "when help_center is enabled" do
        before do
          Account.any_instance.stubs(:help_center_enabled?).returns(true)
        end

        it "points to the hc url" do
          assert_equal "https://minimum.zendesk-test.com/hc/requests/1", @ticket.url
        end

        it "stills point to the classic if for_arent is true" do
          assert_equal "https://minimum.zendesk-test.com/agent/tickets/1", @ticket.url(for_agent: true)
        end
      end

      describe "when prefer_lotus -> true" do
        before do
          @ticket.account.settings.prefer_lotus = true
          @ticket.account.stubs(:help_center_enabled?).returns(true)
          assert @ticket.account.settings.prefer_lotus
        end

        describe "when account ssl enabled" do
          before do
            Account.any_instance.stubs(:is_ssl_enabled?).returns(true)
          end

          describe "url_with_protocol with for_agent -> true" do
            it "returns an ssl, lotus url" do
              assert_equal "https://minimum.zendesk-test.com/agent/tickets/1", @ticket.url(for_agent: true)
            end
          end
          describe "url_with_protocol with for_agent -> false" do
            it "returns an ssl, non-lotus url" do
              assert_equal "https://minimum.zendesk-test.com/hc/requests/1", @ticket.url(for_agent: false)
            end
          end
        end

        describe "when account ssl disabled" do
          before do
            @ticket.account.is_ssl_enabled = false
          end

          describe "url_with_protocol with for_agent -> true" do
            it "returns an ssl, lotus url" do
              assert_equal "https://minimum.zendesk-test.com/agent/tickets/1", @ticket.url(for_agent: true)
            end
          end
          describe "url_with_protocol with for_agent -> false" do
            it "returns a non-ssl, non-lotus url" do
              assert_equal "https://minimum.zendesk-test.com/hc/requests/1", @ticket.url(for_agent: false)
            end
          end
        end

        describe "with host_mapping" do
          before do
            Account.any_instance.stubs(host_mapping: "test.com")
            Route.any_instance.stubs(host_mapping: "test.com")
          end

          it "returns the hostmapped domain" do
            assert_equal "http://test.com/hc/requests/1", @ticket.url
          end

          it "alsos return the hostmapped domain" do
            assert_equal "http://test.com/hc/requests/1", @ticket.url(for_agent: false)
          end

          it "returns the un hostmapped domain" do
            assert_equal "https://minimum.zendesk-test.com/agent/tickets/1", @ticket.url(for_agent: true)
          end
        end

        describe "without host_mapping" do
          it "returns the un hostmapped domain 1" do
            assert_equal "https://minimum.zendesk-test.com/hc/requests/1", @ticket.url
          end

          it "returns the un hostmapped domain 2" do
            assert_equal "https://minimum.zendesk-test.com/hc/requests/1", @ticket.url(for_agent: false)
          end

          it "returns the un hostmapped domain 3" do
            assert_equal "https://minimum.zendesk-test.com/agent/tickets/1", @ticket.url(for_agent: true)
          end
        end
      end

      describe "when prefer_lotus -> false" do
        before do
          @ticket.account.settings.prefer_lotus = false
          @ticket.account.stubs(:help_center_enabled?).returns(true)
        end

        describe "when account ssl enabled" do
          before do
            Account.any_instance.stubs(:is_ssl_enabled?).returns(true)
          end

          describe "url_with_protocol with for_agent -> true" do
            it "returns an ssl, non-lotus url" do
              assert_equal "https://minimum.zendesk-test.com/agent/tickets/1", @ticket.url(for_agent: true)
            end
          end

          describe "url_with_protocol with for_agent -> false" do
            it "returns an ssl, non-lotus url" do
              assert_equal "https://minimum.zendesk-test.com/hc/requests/1", @ticket.url(for_agent: false)
            end
          end
        end

        describe "when account ssl disabled" do
          before do
            @ticket.account.is_ssl_enabled = false
          end

          describe "url_with_protocol with for_agent -> true" do
            it "returns the ssl agent url" do
              assert_equal "https://minimum.zendesk-test.com/agent/tickets/1", @ticket.url(for_agent: true)
            end
          end

          describe "url_with_protocol with for_agent -> false" do
            it "returns a non-ssl, non-lotus url" do
              assert_equal "https://minimum.zendesk-test.com/hc/requests/1", @ticket.url(for_agent: false)
            end
          end
        end
      end

      describe "when account has multiple brands" do
        before do
          @brand = FactoryBot.create(:brand, subdomain: 'wombat', account_id: @ticket.account_id)
          @ticket.account.stubs(:help_center_enabled?).returns(true)
        end
        it "returns the branded url for the brand of the ticket for end users" do
          @ticket.stubs(:brand).returns(@brand)
          assert_equal "https://wombat.zendesk-test.com/hc/requests/1", @ticket.url(for_agent: false)
        end

        it "returns the non branded url for agents" do
          assert_equal "https://minimum.zendesk-test.com/hc/requests/1", @ticket.url(for_agent: false)
        end
      end

      describe "#url_path" do
        it "returns the path with a leading slash" do
          path = @ticket.url_path(user: users(:minimum_agent))
          assert_equal '/', path.first
          refute path.start_with?(@ticket.account.url)
        end
      end
    end

    describe "solved_since" do
      before do
        @agent  = users(:minimum_agent)
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(@agent)
      end

      it "provides tickets solved since the given date" do
        @ticket.status_id = StatusType.SOLVED
        @ticket.save!
        @ticket.reload

        assert_equal [@ticket], @agent.assigned.solved_since(1.minute.ago)
      end

      it "does not provide tickets not solved since the given date" do
        assert_equal [], @agent.assigned.solved_since(1.minute.ago)

        Timecop.freeze(4.minutes.ago) do
          @ticket.status_id = StatusType.SOLVED
          @ticket.save!
          @ticket.reload
        end
        assert_equal [@ticket], @agent.assigned.solved_since(5.minutes.ago)

        assert_equal [], @agent.assigned.solved_since(1.minute.ago)
      end
    end

    describe "solved?" do
      it "is true when it's solved, closed or archived" do
        @ticket.status_id = StatusType.SOLVED
        assert(@ticket.solved?)

        @ticket.status_id = StatusType.CLOSED
        assert(@ticket.solved?)
      end

      it "is false when it's new or open" do
        @ticket.status_id = StatusType.NEW
        assert_equal false, @ticket.solved?

        @ticket.status_id = StatusType.OPEN
        assert_equal false, @ticket.solved?
      end

      it "is false when no status is set" do
        @ticket.status_id = nil
        assert_equal false, @ticket.solved?
      end
    end

    describe "#pending?" do
      describe "when the status is in the 'pending' state" do
        before { @ticket.status_id = StatusType.PENDING }

        it "is true" do
          assert @ticket.pending?
        end
      end

      describe "when the status is not in the 'pending' state" do
        before { @ticket.status_id = StatusType.OPEN }

        it "is false" do
          refute @ticket.pending?
        end
      end
    end

    describe "#on_hold?" do
      describe "when the status is in the 'on hold' state" do
        before { @ticket.status_id = StatusType.HOLD }

        it "is true" do
          assert @ticket.on_hold?
        end
      end

      describe "when the status is not in the 'on hold' state" do
        before { @ticket.status_id = StatusType.OPEN }

        it "is false" do
          refute @ticket.on_hold?
        end
      end
    end

    describe "#working?" do
      [
        StatusType.NEW,
        StatusType.OPEN,
        StatusType.HOLD,
        StatusType.PENDING
      ].each do |unfinished_status|
        describe "when the status is #{unfinished_status}" do
          before { @ticket.status_id = unfinished_status }

          it "is true" do
            assert @ticket.working?
          end
        end
      end

      [
        StatusType.CLOSED,
        StatusType.SOLVED,
        StatusType.DELETED,
        StatusType.ARCHIVED
      ].each do |finished_status|
        describe "when the status is #{finished_status}" do
          before { @ticket.status_id = finished_status }

          it "is false" do
            refute @ticket.working?
          end
        end
      end
    end

    describe "#finished?" do
      [
        StatusType.CLOSED,
        StatusType.SOLVED,
        StatusType.DELETED,
        StatusType.ARCHIVED
      ].each do |finished_status|
        describe "when the status is #{finished_status}" do
          before { @ticket.status_id = finished_status }

          it "is true" do
            assert @ticket.finished?
          end
        end
      end

      [
        StatusType.NEW,
        StatusType.OPEN,
        StatusType.HOLD,
        StatusType.PENDING
      ].each do |unfinished_status|
        describe "when the status is #{unfinished_status}" do
          before { @ticket.status_id = unfinished_status }

          it "is false" do
            refute @ticket.finished?
          end
        end
      end
    end

    describe '#ever_solved?' do
      before do
        @ticket = tickets(:minimum_1)
      end

      it "is true when it's solved, closed, or archived" do
        @ticket.status_id = StatusType.SOLVED
        assert @ticket.ever_solved?

        @ticket.status_id = StatusType.CLOSED
        assert @ticket.ever_solved?
      end

      it "is true when it was solved and re-opened" do
        @ticket.status_id = StatusType.SOLVED
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!

        @ticket.status_id = StatusType.OPEN
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!

        assert @ticket.ever_solved?
      end

      it "is false when it's never been solved" do
        @ticket.status_id = StatusType.NEW
        refute @ticket.ever_solved?

        @ticket.status_id = StatusType.OPEN
        refute @ticket.ever_solved?
      end
    end

    describe "audit log" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(users(:minimum_agent))
      end

      it "logs soft delete" do
        CIA.audit actor: users(:minimum_agent) do
          @ticket.soft_delete!
        end

        audit = CIA::Event.last
        assert audit
        assert_equal "destroy", audit.action
        assert_equal users(:minimum_agent), audit.actor
      end

      it "does not log update" do
        refute_difference "CIA::Event.count(:all)" do
          CIA.audit actor: users(:minimum_agent) do
            @ticket.save!
          end
        end
      end

      it "does not log create" do
        author = users(:minimum_agent)
        ticket = @ticket.account.tickets.new(requester: author,
                                             subject: "woot",
                                             description: "also woot")
        ticket.will_be_saved_by(author)
        refute_difference "CIA::Event.count(:all)" do
          CIA.audit actor: users(:minimum_agent) do
            ticket.save!
          end
        end
      end
    end

    describe "without a solved timestamp" do
      before do
        Timecop.freeze(DateTime.parse('04-01-2010 00:00:00'))
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(@ticket.assignee)
        @ticket.solved_at = nil
      end

      it "has its solved at set when it's solved" do
        @ticket.status_id = StatusType.CLOSED
        assert @ticket.save!

        assert_equal Time.current, @ticket.solved_at
      end

      it "does not have its solved at set when it isn't solved" do
        @ticket.status_id = StatusType.OPEN
        @ticket.save!

        assert_nil @ticket.solved_at
      end
    end

    describe "with a solved timestamp" do
      before do
        travel_to Time.now
        @ticket = tickets(:minimum_1)
        @ticket.status_id = StatusType.CLOSED
        @original_solved_at = @ticket.solved_at = 5.days.ago
      end

      after { travel_back }

      it "does not change its solved at" do
        @ticket.will_be_saved_by(@ticket.assignee)
        @ticket.save!
        assert_equal @original_solved_at, @ticket.solved_at
      end
    end

    describe "with a set group_id" do
      before { @ticket.group_id = groups(:minimum_group).id }

      it "does not call infer_group_from_assignee when validating" do
        @ticket.expects(:infer_group_from_assignee).never

        @ticket.valid?
      end

      it "is invalid when its assignee_id points to a non-existent agent" do
        @ticket.assignee_id = 0
        refute @ticket.valid?
      end

      describe "and a set assignee_id" do
        before { @ticket.assignee_id = users(:minimum_agent).id }

        it "calls check_assignee_belongs_to_group when validating" do
          @ticket.expects(:check_assignee_belongs_to_group).once

          @ticket.valid?
        end
      end

      describe "and a set assignee_email" do
        before { @ticket.assignee_email = users(:minimum_agent).email }

        it "calls check_assignee_belongs_to_group when validating" do
          @ticket.expects(:check_assignee_belongs_to_group).once

          @ticket.valid?
        end
      end

      describe "and a nil assignee_id" do
        it "does not call validate_assignee_belongs_to_group when validating" do
          @ticket.expects(:validate_assignee_belongs_to_group).never

          @ticket.valid?
        end
      end
    end

    describe "with an assignee that is not an agent" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(users(:minimum_agent))
      end

      it "assigns to no agent" do
        @ticket.assignee_id = users(:minimum_end_user).id
        assert @ticket.valid?
        assert_nil @ticket.assignee
        assert @ticket.save
      end
    end

    describe "that is being assigned to a light agent" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @old_assignee_id = @ticket.assignee_id

        create_light_agent

        @ticket.group.memberships.create!(user: @light_agent)
        @ticket.group.save!
      end

      describe "and has a nil assignee_id" do
        before { @ticket.assignee_id = nil }

        it "has nil assignee_id" do
          @ticket.send(:ensure_new_assignee_can_be_assigned)
          assert_nil @ticket.assignee_id
        end
      end

      describe "and assigned to another agent in the group" do
        it "does not change the assignee_id" do
          assert_not_equal @ticket.assignee_id, @light_agent.id

          @ticket.assignee_id = @light_agent.id
          assert_equal @light_agent, @ticket.reload_assignee

          @ticket.send(:ensure_new_assignee_can_be_assigned)

          assert_equal @old_assignee_id, @ticket.assignee_id
          assert_not_equal @light_agent, @ticket.reload_assignee
        end

        it "removes the assignee" do
          @ticket.assignee_id = users(:minimum_end_user).id
          assert @ticket.valid?
          assert_nil @ticket.assignee
          assert @ticket.save
        end
      end
    end

    describe "that is not a twicket" do
      it "shoulds not have a monitored twitter handle" do
        refute @ticket.twitter_ticket_source
      end
    end

    describe "ticket fields" do
      before do
        @ticket = tickets(:minimum_2)
        @ticket_field_entry = @ticket.ticket_field_entries[0]
      end

      it "is autosaved when set" do
        @ticket_field_entry.value = "autosaved value 1"
        @ticket.will_be_saved_by(@ticket.account.owner)
        @ticket.save!

        assert_equal "autosaved value 1", @ticket_field_entry.reload.value
      end

      it "is autosaved when assigned via id" do
        @ticket.fields = {@ticket_field_entry.ticket_field_id => "autosaved value 2"}
        @ticket.will_be_saved_by(@ticket.account.owner)
        @ticket.save!

        assert_equal "autosaved value 2", @ticket_field_entry.reload.value
      end

      it "makes a save fail if they cannot be saved" do
        old_value = @ticket_field_entry.value
        @ticket_field_entry.value = 'new value'
        @ticket_field_entry.ticket_field.regexp_for_validation = /xxxxx/ # validated -> does not save
        @ticket.will_be_saved_by(@ticket.account.owner)
        assert_equal false, @ticket.save
        assert_equal old_value, @ticket_field_entry.reload.value
        assert_includes @ticket.errors.full_messages.join(" ").delete(":"), "Product name (portal) is invalid"
      end

      describe "#fields=" do
        it "does not save the ticket fields" do
          ticket = tickets(:minimum_2)
          ticket_field = ticket_fields(:field_text_custom)

          TicketFieldEntry.any_instance.expects(:save).never
          TicketFieldEntry.any_instance.expects(:save!).never

          ticket.fields = { ticket_field.id => "brand new value" }
        end
      end
    end

    describe "being updated" do
      let(:ticket)        { tickets(:minimum_2) }
      let(:requester)     { ticket.requester }
      let(:organization1) { ticket.organization }
      let(:organization2) { organizations(:minimum_organization2) }

      before do
        Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
        requester.update_attribute(:organizations, [organization1, organization2])
        ticket.will_be_saved_by(accounts(:minimum).owner)
      end

      it "audits organization changes" do
        ticket.update_attribute(:organization, organization2)
        audit_event = ticket.audits.last.events.detect { |e| e.changes?(:organization_id) }

        assert_equal organization2.id, audit_event.value.to_i
        assert_equal organization1.id, audit_event.value_previous.to_i
      end

      it "audits external_id changes" do
        ticket.update_attribute(:external_id, 5)
        audit_event = ticket.audits.last.events.detect { |e| e.changes?(:external_id) }

        assert_equal 5, audit_event.value.to_i
        assert_nil audit_event.value_previous
      end

      describe "changing ticket type to a problem" do
        it "expires ticket_fields cache" do
          old_cache_key = ticket.account.scoped_cache_key(:ticket_fields)
          ticket.account.stubs(:has_extended_ticket_types?).returns(true)
          ticket.ticket_type_id = TicketType.PROBLEM
          ticket.save!
          Timecop.travel(Time.now + 1.seconds)
          assert_not_equal old_cache_key, ticket.reload.account.scoped_cache_key(:ticket_fields)
        end

        it "does not record linked_id change on non-problems" do
          Account.any_instance.stubs(has_extended_ticket_types?: true)
          ticket.ticket_type_id = TicketType.QUESTION
          ticket.linked_id = 12345
          ticket.save!
          Timecop.travel(Time.now + 1.seconds)
          changed_attributes = ticket.events.select { |x| x.is_a?(Change) }.map(&:value_reference)
          changed_attributes.must_equal ["ticket_type_id"]
        end
      end

      describe "with only a ticket field entry change" do
        before do
          ticket.save!
          ticket.reload
          Timecop.travel(1.minute.from_now)
          ticket.will_be_saved_by(accounts(:minimum).owner)
          ticket.fields = {ticket_fields(:field_text_custom).id => Time.now.to_s}
        end

        it "updates updated_at on the ticket" do
          updated_at = ticket.updated_at
          ticket.save!
          assert ticket.reload.updated_at != updated_at
        end
      end
    end

    describe "#requester_is_agent?" do
      let(:requester) { @ticket.requester }
      let(:submitter) { @ticket.submitter }

      before do
        @ticket.account = accounts(:minimum)
        @ticket.build_requester(name: 'Sir Skisalot', email: 'ski@example.com', account: @ticket.account)
        @ticket.requester_name  = 'Mr Snowboarder'
        @ticket.requester_email = 'board@example.com'
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.description = "foo"
        @ticket.save!
      end

      describe "with fallback to submitter" do
        describe "with a requester" do
          it "returns the expected value for the requester" do
            assert_equal requester.is_agent?, @ticket.requester_is_agent?
          end
        end

        describe "without a requester, but with a submitter" do
          it "doesn't throw any exceptions and returns whether or not the submitter is an agent" do
            @ticket.requester = nil

            assert_equal submitter.is_agent?, @ticket.requester_is_agent?
          end
        end

        describe "without a requester or submitter" do
          it "doesn't fail and returns false" do
            @ticket.requester = nil
            @ticket.submitter = nil

            refute @ticket.requester_is_agent?, "Return false, don't throw exception with no requester or submitter"
          end
        end
      end
    end

    describe "#locale_id" do
      let(:spanish) { translation_locales(:spanish) }

      before do
        @ticket.account = accounts(:minimum)
        @ticket.build_requester(name: 'Sir Skisalot', email: 'ski@example.com', account: @ticket.account)
        @ticket.requester_name  = 'Mr Snowboarder'
        @ticket.requester_email = 'board@example.com'
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.description = "foo"
        @ticket.save!

        @ticket.update_column(:locale_id, ENGLISH_BY_ZENDESK.id)
        @ticket.requester.update_column(:locale_id, spanish.id)
      end

      it "calls requester.locale_id" do
        @ticket.requester.expects(:locale_id).once
        @ticket.locale_id
      end

      it "uses the requesters locale_id" do
        assert_equal spanish.id, @ticket.locale_id
      end
    end

    describe "#translation_locale" do
      let(:spanish) { translation_locales(:spanish) }

      before do
        @ticket.account = accounts(:minimum)
        @ticket.build_requester(name: 'Sir Skisalot', email: 'ski@example.com', account: @ticket.account)
        @ticket.requester_name  = 'Mr Snowboarder'
        @ticket.requester_email = 'board@example.com'
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.description = "foo"
        @ticket.save!

        @ticket.update_column(:locale_id, ENGLISH_BY_ZENDESK.id)
        @ticket.requester.update_column(:locale_id, spanish.id)
      end

      it "calls requester.locale_id" do
        @ticket.requester.expects(:translation_locale).once
        @ticket.translation_locale
      end

      it "uses the requesters locale_id" do
        assert_equal spanish, @ticket.translation_locale
      end
    end

    describe "#group_includes?" do
      subject { @ticket.group_includes?(member) }

      let(:member) { FactoryBot.create(:agent, account: @ticket.account) }

      describe "for a ticket without a group assigned" do
        it { refute subject }
      end

      describe "for a ticket with a group assigned" do
        before { @ticket.group = groups(:minimum_group) }

        describe "and the user is not a member of that group" do
          it { refute subject }
        end

        describe "and the user is a member of that group" do
          before { FactoryBot.create(:membership, user: member, group: @ticket.group) }

          it { assert subject }
        end
      end
    end

    describe "#is_public" do
      it "will default to true" do
        assert(@ticket.is_public)
      end
    end

    describe "#solve" do
      it "sets status to SOLVED" do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(@ticket.account.owner)
        refute_equal StatusType.SOLVED, @ticket.status_id
        @ticket.solve(@ticket.account.owner)
        assert_equal StatusType.SOLVED, @ticket.status_id
      end
    end

    describe "#inoperable?" do
      it "is true when status is DELETED" do
        @ticket.status_id = StatusType.DELETED
        assert @ticket.inoperable?
      end

      it "is false when status is NEW" do
        @ticket.status_id = StatusType.NEW
        refute @ticket.inoperable?
      end
    end

    describe "#operable?" do
      it "is false when status is DELETED" do
        @ticket.status_id = StatusType.DELETED
        refute @ticket.operable?
      end

      it "is true when status is NEW" do
        @ticket.status_id = StatusType.NEW
        assert @ticket.operable?
      end
    end
  end

  describe "Suspending a Ticket" do
    before do
      @ticket = tickets(:with_groups_1)
      @ticket.client = "foobar"
      @cause = SuspensionType.SPAM
      @suspended = @ticket.suspend(@cause)
    end

    it "provides a suspended ticket" do
      assert @suspended.is_a?(SuspendedTicket)
      assert @suspended.valid?
    end

    it "includes the ticket's details" do
      assert_equal @ticket.account,     @suspended.account
      assert_equal @ticket.subject,     @suspended.subject
      assert_equal @ticket.description, @suspended.content
      assert_equal @cause,              @suspended.cause
      assert_equal @ticket.via_id,      @suspended.via_id
      assert_equal @ticket.requester,   @suspended.author
      assert_equal @ticket.client,      @suspended.client
      assert_equal @ticket.requester.name, @suspended.from_name
      assert_equal @ticket.requester.email, @suspended.from_mail
    end

    it "does not include unknown authors" do
      @ticket.requester = User.new
      assert_nil @ticket.suspend(@cause).author
    end

    describe "that is a task and has a due_date" do
      before do
        @ticket.due_date = Time.zone.now
        @suspended = @ticket.suspend(@cause)
      end

      it "includes the ticket's due date in the properties" do
        assert_equal @ticket.due_date, @suspended.properties[:due_date]
      end
    end
  end

  describe "A Ticket from a new user" do
    before do
      @account = accounts(:minimum)
      @account.is_open = true
      @account.is_signup_required = false
      @account.settings.ticket_tagging = true
      @account.settings.tickets_inherit_requester_tags = true
      @account.settings.has_user_tags = true
      @account.save!
      @group = @account.groups.create!(name: 'Tequila', is_active: true)
      @organization_one = @account.organizations.create!(domain_names: 'organization-one.com', name: 'Acme Corp.', group: @group)
      @organization_one.attributes = {set_tags: ["organization_one"]}
      @organization_one.save!
      @user = @account.users.new(name: 'Bud Dha', email: 'hola@organization.com')
      @user.update_attribute(:organizations, [@organization_one])
      @user.save!
    end

    describe 'who has not yet been saved' do
      before do
        @ticket = @account.tickets.new(recipient: 'support@example.com', requester: @user, description: 'I should have a group')
        User.any_instance.stubs(:can?).returns(true)
        @ticket.will_be_saved_by(@user)
      end

      it "inherits the user's default organization along with the organizations tags" do
        @ticket.save!
        assert_equal @organization_one, @ticket.organization
        assert_equal "organization_one", @ticket.current_tags
      end

      describe "without ticket tagging" do
        before do
          @account.settings.ticket_tagging = false
          @account.save!
        end

        it "inherits the user's default organization along with the organizations tags" do
          @ticket.save!
          assert_equal @organization_one, @ticket.organization
          assert_equal "organization_one", @ticket.current_tags
        end
      end

      describe "when the user has multiple organizations" do
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
          @organization_two = @account.organizations.create!(domain_names: 'example-two.com', name: 'Anvils Ltd.', group: @group)
          @organization_two.attributes = {set_tags: ["organization_two"]}
          @organization_two.save!
          @user.update_attribute(:organizations, [@organization_one, @organization_two])
          @user.save!
        end

        describe "and no organization_id is explicitly supplied" do
          it "inherits the user's default organization along with the organizations tags" do
            @ticket.save!
            assert_equal @organization_one, @ticket.organization
            assert_equal "organization_one", @ticket.current_tags
          end
        end

        describe "and the organization_id of the user's second id explicitly supplied" do
          before do
            @ticket = @account.tickets.new(recipient: 'support@example.com', requester: @user, description: 'I should have a group', organization_id: @organization_two.id)
            @ticket.will_be_saved_by(@user)
          end

          it "inherits the organization along with the tags of that organization" do
            @ticket.save!
            assert_equal @organization_two, @ticket.organization
            assert_equal "organization_two", @ticket.current_tags
          end
        end
      end
    end

    describe 'who has just been saved' do
      before do
        @user.save!
      end

      it "has the user organization's group as the ticket's group" do
        ticket = @account.tickets.new(recipient: 'support@example.com', requester: @user, description: 'I should have a group')
        ticket.will_be_saved_by(@user)
        ticket.save!

        assert_equal @group, ticket.group, ticket.inspect
      end
    end
  end

  describe "A ticket created in an account with one group and one agent only" do
    before do
      ActiveRecord::Base.connection.execute("update users set roles = 0 where id <> #{users(:minimum_admin).id}")
      ActiveRecord::Base.connection.execute("delete from memberships where user_id <> #{users(:minimum_admin).id}")
      assert_equal 1, accounts(:minimum).groups.count(:all)
      assert_equal 1, accounts(:minimum).agents.count(:all)

      @ticket = accounts(:minimum).tickets.new(description: 'bubba')
      @ticket.will_be_saved_by(users(:minimum_end_user))
    end

    it "is auto-assigned to a group and agent" do
      @ticket.save
      assert @ticket.group
      assert @ticket.assignee
    end

    it "only be auto-assigned to a group, not the agent, if the agent does not belong to the group" do
      memberships(:minimum_admin).destroy
      assert_equal 0, users(:minimum_admin).groups.count(:all)
      @ticket.save
      assert @ticket.group
      refute @ticket.assignee
    end

    it "only be auto-assigned to a group, not the agent, if the agent is a light agent" do
      memberships(:minimum_admin).destroy
      @ticket.save

      create_light_agent

      @ticket.group.memberships.create!(user: @light_agent)
      @ticket.group.save!

      @ticket.send(:set_assignee_if_group_has_only_one_agent)

      assert @ticket.group
      refute @ticket.assignee
    end

    it "also works for has_chat_permission_set?" do
      memberships(:minimum_admin).destroy
      @ticket.save

      create_light_agent
      @light_agent.permission_set = PermissionSet.create_chat_agent!(accounts(:minimum))
      @light_agent.save!

      @ticket.group.memberships.create!(user: @light_agent)
      @ticket.group.save!

      @ticket.account.expects(:has_permission_sets?).returns(false)
      @ticket.account.expects(:has_chat_permission_set?).returns(true)

      @ticket.send(:set_assignee_if_group_has_only_one_agent)

      assert @ticket.group
      refute @ticket.assignee
    end

    it "assigns to the first non light agent" do
      @ticket.save

      create_light_agent

      @ticket.group.memberships.create!(user: @light_agent)
      @ticket.group.save!

      assert_equal 2, accounts(:minimum).agents.count(:all)
      assert_equal false, @ticket.assignee.is_light_agent?
    end
  end

  describe "A ticket created in an account with one group with no members, one deleted group with member, and one agent only" do
    before do
      ActiveRecord::Base.connection.execute("update users set roles = 0 where id <> #{users(:minimum_admin).id}")
      ActiveRecord::Base.connection.execute("delete from memberships")
      refute groups(:support_group).is_active?
      groups(:support_group).users << users(:minimum_admin)
      assert_equal 1, accounts(:minimum).groups.count(:all)
      assert_equal 1, accounts(:minimum).agents.count(:all)
      assert_equal 0, groups(:minimum_group).memberships.count(:all)

      @ticket = accounts(:minimum).tickets.new(description: 'bubba')
      @ticket.will_be_saved_by(users(:minimum_end_user))
    end

    it "only be auto-assigned to a group, not the agent" do
      @ticket.save!
      assert @ticket.group
      refute @ticket.assignee
    end
  end

  describe "A ticket created in an account with many groups and one agent only" do
    before do
      @account = accounts(:minimum)
      @group1 = @account.groups.create!(name: 'Zgroup1', is_active: true)
      @group2 = @account.groups.create!(name: 'Zgroup2', is_active: true)

      ActiveRecord::Base.connection.execute("update users set roles = 0 where id <> #{users(:minimum_admin).id}")
      @group1.memberships.create!(user: users(:minimum_admin))
      assert_not_equal 1, @account.groups.count(:all)
      assert_equal 1, @account.agents.count(:all)

      @ticket = accounts(:minimum).tickets.new(description: 'bubba', requester: users(:minimum_end_user))
      @ticket.group = @group1
      @ticket.will_be_saved_by(users(:minimum_end_user))
    end

    it "does not update assignee when light agent adds private comment" do
      create_light_agent
      @group2.memberships.create!(user: @light_agent)

      @ticket.add_comment(body: 'light agent comment', is_public: false)
      @ticket.will_be_saved_by(@light_agent.reload)
      @ticket.save!

      assert_equal false, @ticket.reload.assignee_id_delta_changed?
    end
  end

  describe "A ticket with a private first comment" do
    before do
      @ticket = tickets(:minimum_1)
    end
    it "returns description when filtering description and first comment is public" do
      @ticket.filter_description
      assert_not_nil @ticket.description
      assert_equal @ticket.attributes['description'], @ticket.description
    end
    it "returns nil when filtering description and first comment is private" do
      @ticket.stubs(:first_comment).returns(stub(is_public?: false))
      @ticket.filter_description
      assert_nil @ticket.description
      assert_not_equal @ticket.attributes['description'], @ticket.description
    end
  end

  describe "ticket description" do
    let(:description) { "We\r\nbreak lines\r\n\r\nand <br /> Hi</div>stuff!" }

    def create_follow_up_ticket
      source = tickets(:minimum_5)
      assert(source.update_columns(status_id: StatusType.CLOSED))
      @ticket = Ticket.new(account: accounts(:minimum))
      @ticket.via_followup_source_id = source.nice_id
      @ticket.set_followup_source(source.requester)
      @ticket.description = description
      @ticket.add_comment({ author: source.requester, body: description, is_public: true, format: 'rich' })
      @ticket.will_be_saved_by(source.requester)
      @ticket.via_id = ViaType.CLOSED_TICKET
      @ticket.save!
    end

    describe "when ticket is a follow-up ticket" do
      it "returns the plain text body of the first comment" do
        create_follow_up_ticket
        assert_not_includes @ticket.reload.description, "<br />"
      end

      describe_with_arturo_disabled :views_ticket_description_fix do
        it "returns the rich text body of the first comment" do
          create_follow_up_ticket
          assert_includes @ticket.reload.description, "<br />"
        end
      end
    end
  end

  describe "A Ticket's title" do
    before do
      @ticket = Ticket.new(account: accounts(:minimum))
      @ticket.description = "We\r\nbreak lines\r\n\r\nand     stuff!"
      @ticket.subject     = "Hello\r\n  mister"
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.save!
    end

    it "does not contain newlines or excessive white space" do
      assert_equal "Hello mister", @ticket.title(60)
    end

    it "supports getting the title from the description" do
      assert_equal "Hello mister", @ticket.title(60, false)
      assert_equal "We break lines and stuff!", @ticket.title(60, true)
    end

    it "uses title_for_role to differentiate between the title shown" do
      @ticket.account.expects(:field_subject).returns(mock(is_visible_in_portal?: false))
      assert_equal "Hello mister", @ticket.title_for_role(true, 60)
      assert_equal "We break lines and stuff!", @ticket.title_for_role(false, 60)
    end

    it "always show the subject if visible in portal" do
      @ticket.account.expects(:field_subject).returns(mock(is_visible_in_portal?: true))
      assert_equal "Hello mister", @ticket.title_for_role(true, 60)
      assert_equal "Hello mister", @ticket.title_for_role(false, 60)
    end

    it "defaults to the public_description if there's no subject" do
      @ticket.account.expects(:field_subject).returns(mock(is_visible_in_portal?: true))
      @ticket.subject = nil
      assert_equal "We break lines and stuff!", @ticket.title(60, true)
      assert_equal "We break lines and stuff!", @ticket.title(60)
      assert_equal "We break lines and stuff!", @ticket.title_for_role(true, 60)
      assert_equal "We break lines and stuff!", @ticket.title_for_role(false, 60)
    end
  end

  describe "A Ticket's title when the first comment is private" do
    before do
      @ticket = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_agent), requester_id: users(:minimum_author).id, ticket: {
        submitter: users(:minimum_agent),
        subject: "not-so private subject",
        tags: "hello",
        comment: { public: false, value: "I'm mad as hell!" }
      }).ticket
    end

    describe 'without a public comment' do
      it "defaults to the subject if force_description is true" do
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "I'm mad as hell!", @ticket.title(60, true)
      end

      it "defaults to the subject if the is_visible_in_portal is false for non-agents" do
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!

        # We should never show tickets in any UI for private tickets when `is_agent = false`
        assert_equal "Untitled ticket #{@ticket.nice_id}", @ticket.title_for_role(false, 60) # is_agent = false
      end

      it "defaults to the subject if force_description is false for agents" do
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!

        assert_equal "not-so private subject", @ticket.title(60) # force_description = false
      end

      it "defaults to the subject for agents" do
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        @ticket.account.stubs(:field_subject).returns(mock)

        assert_equal "not-so private subject", @ticket.title_for_role(true, 60) # is_agent = true
        assert_equal "not-so private subject", @ticket.title_for_role(true, 60) # is_agent = true
      end

      it "defaults to the private description if the subject is blank" do
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        @ticket.subject = ''
        # Private Tickets should not ever be displayed in Help Center Thus title returns the private description
        assert_equal "I'm mad as hell!", @ticket.title(60, true)
        assert_equal "Untitled ticket #{@ticket.nice_id}", @ticket.title_for_role(false, 60) # non-agent & private
        assert_equal "I'm mad as hell!", @ticket.title(60) # force_description = false
        assert_equal "I'm mad as hell!", @ticket.title_for_role(true, 60) # is_agent = true
      end
    end

    describe 'with a public comment' do
      before do
        @ticket.add_comment(is_public: true, body: 'Have a Yabba Dabba Doo Day!')
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
      end

      it "defaults to the public_description for non-agent or force_description" do
        @ticket.account.expects(:field_subject).returns(mock(is_visible_in_portal?: false))
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title(60, true) # force_description = true
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title_for_role(false, 60)
      end

      it "defaults to the subject for agent or force_description = false" do
        assert_equal "not-so private subject", @ticket.title(60)
        assert_equal "not-so private subject", @ticket.title_for_role(true, 60) # is_agent = true
      end

      it "defaults to the public_description for non-agent or force_description" do
        @ticket.account.expects(:field_subject).returns(mock(is_visible_in_portal?: true))
        @ticket.subject = nil
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title(60, true) # force_description = true
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title(60)
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title_for_role(true, 60) # is_agent = true
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title_for_role(false, 60)
      end

      it "defaults to the public_description if there's no subject" do
        @ticket.account.expects(:field_subject).returns(mock(is_visible_in_portal?: true))
        @ticket.subject = nil
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title(60, true) # force_description = true
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title(60)
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title_for_role(true, 60) # is_agent = true
        assert_equal "Have a Yabba Dabba Doo Day!", @ticket.title_for_role(false, 60)
      end
    end
  end

  describe "a closed ticket" do
    before do
      @account = accounts(:minimum)
      @ticket  = @account.tickets.where(status_id: StatusType.CLOSED).first
    end

    it "does not allow to be saved" do
      @ticket.current_tags = "hello"
      @ticket.will_be_saved_by(@ticket.assignee)
      refute @ticket.save
      @ticket.errors.full_messages.join.must_include "closed prevents"
    end

    it "does not allow comments to be added" do
      @ticket.add_comment(body: "hello", is_public: false)
      @ticket.will_be_saved_by(@ticket.assignee)
      refute @ticket.save
      @ticket.errors.full_messages.join.must_include "closed prevents"
    end

    it "does not allow comments to be added if setting a system via_id" do
      @ticket.add_comment(body: "hello", is_public: false)
      @ticket.via_id = ViaType.GITHUB
      @ticket.will_be_saved_by(@ticket.assignee)
      refute @ticket.save
      @ticket.errors.full_messages.join.must_include "closed prevents"
    end
  end

  describe "#throttle_updates" do
    let(:handle) { Prop::Limiter.handles.fetch(:ticket_updates) }

    before do
      @account = accounts(:minimum)
      @user    = users(:minimum_end_user)
      @agent   = users(:minimum_agent)
      @ticket  = ticket_create(@user)

      @ticket_update_rate_limit = @account.settings.ticket_update_rate_limit
      @account.settings.ticket_update_rate_limit = 5
      @account.save!

      Timecop.freeze
    end

    after do
      @account.settings.ticket_update_rate_limit = @ticket_update_rate_limit
      @account.save!
    end

    it "registers updates in the rate limiter, raise Prop::RateLimited when limit exceeded, allow other users to save when one user is throttled, and create warnings as needed" do
      assert_equal 0, Prop.query(:ticket_updates, [@ticket.id, @user.id])

      Rails.logger.stubs(:warn)
      Rails.logger.expects(:warn).with { |msg| msg =~ /^High ticket update ratio.*by user #{@user.id} for account #{@user.account_id}/ }
      5.times do |i|
        @ticket.will_be_saved_by(@user)
        @ticket.save!
        assert_equal((i + 1), Prop.query(:ticket_updates, [@ticket.id, @user.id]))
      end
      assert_raise(Prop::RateLimited) do
        @ticket.will_be_saved_by(@user)
        @ticket.save
      end

      @ticket.will_be_saved_by(@agent)
      @ticket.save!
    end

    describe "facebook tickets" do
      before do
        @ticket.via_id = ViaType.FACEBOOK_POST
      end

      it "does not throttle ticket updates from FB" do
        assert_equal 0, Prop.query(:ticket_updates, [@ticket.id, @user.id])

        @page = Facebook::Page.create(graph_object_id: "2423423", access_token: "abc", account: @ticket.account)
        @user.identities.facebook.create(value: "2423423")

        5.times do |_i|
          @ticket.will_be_saved_by(@user)
          @ticket.save!
          assert_equal 0, Prop.query(:ticket_updates, [@ticket.id, @user.id])
        end

        @ticket.will_be_saved_by(@user)
        @ticket.save
      end

      it "throttles ticket updates not from FB" do
        assert_equal 0, Prop.query(:ticket_updates, [@ticket.id, @user.id])

        5.times do |i|
          @ticket.will_be_saved_by(@user, via_id: ViaType.WEB_FORM)
          @ticket.save!
          assert_equal((i + 1), Prop.query(:ticket_updates, [@ticket.id, @user.id]))
        end

        assert_raise(Prop::RateLimited) do
          @ticket.will_be_saved_by(@user)
          @ticket.save
        end
      end
    end

    describe "twitter tickets" do
      before do
        @ticket.via_id = ViaType.TWITTER
      end

      it "does not throttle ticket updates when the audit is via twitter" do
        assert_equal 0, Prop.query(:ticket_updates, [@ticket.id, @user.id])

        Audit.any_instance.stubs(:via_twitter?).returns(true)

        5.times do |_i|
          @ticket.will_be_saved_by(@user)
          @ticket.save!
          assert_equal 0, Prop.query(:ticket_updates, [@ticket.id, @user.id])
        end

        @ticket.will_be_saved_by(@user)
        @ticket.save
      end

      it "throttles ticket updates when the audit is not via twitter" do
        assert_equal 0, Prop.query(:ticket_updates, [@ticket.id, @user.id])
        Audit.any_instance.stubs(:via_twitter?).returns(false)

        5.times do |i|
          @ticket.will_be_saved_by(@user)
          @ticket.save!
          assert_equal((i + 1), Prop.query(:ticket_updates, [@ticket.id, @user.id]))
        end

        assert_raise(Prop::RateLimited) do
          @ticket.will_be_saved_by(@user)
          @ticket.save
        end
      end
    end
  end

  describe "#to_xml" do
    describe "v1" do
      it "only hit the DB a limited number of times when finding tickets using Ticket::SERIALIZATION_FINDER_INCLUDES" do
        assert_sql_queries 15 do
          # if this fails, make sure it's not because you're introducing an N+1 query
          Ticket.includes(Ticket::SERIALIZATION_FINDER_INCLUDES).to_a.to_xml
        end
      end
    end

    describe "v2" do
      it "only hit the DB a limited number of times when finding tickets using Ticket::SERIALIZATION_FINDER_INCLUDES" do
        Account.any_instance.stubs(:url).returns('https://foo.zendesk.com')
        # make sure you're not introducing an N+1 query
        assert_sql_queries 32 do
          Ticket.
            where(account_id: accounts(:minimum).id).
            includes(Ticket::SERIALIZATION_FINDER_INCLUDES_V2).
            to_a.
            to_xml(version: 2)
        end
      end
    end
  end

  describe "base_score" do
    before do
      @ticket = tickets(:minimum_2) # Ticket.where(status_id: StatusType.OPEN).last
      @ticket.will_be_saved_by(User.find(-1))
      Ticket.connection.update("UPDATE tickets SET base_score = 0, updated_at = '#{1.day.ago.to_s(:db)}' WHERE id = #{@ticket.id}")
      @ticket.reload
    end

    it "recalculates on ticket save" do
      assert_equal(0, Ticket.connection.select_value("SELECT base_score FROM tickets WHERE id = #{@ticket.id}").to_i)
      @ticket.update_attribute(:status_id, StatusType.SOLVED)
      assert(Ticket.connection.select_value("SELECT base_score FROM tickets WHERE id = #{@ticket.id}").to_i > 0)
    end

    it 'does not touch the ticket on "empty" saves' do
      ticket_attributes = Ticket.connection.select_rows("SELECT * FROM tickets WHERE id = #{@ticket.id}").first
      @ticket.save!
      assert_equal(ticket_attributes, Ticket.connection.select_rows("SELECT * FROM tickets WHERE id = #{@ticket.id}").first)
    end
  end

  describe "User tags" do
    before do
      accounts(:minimum).settings.has_user_tags = '1'
      accounts(:minimum).settings.save!
    end

    describe "when account has user_and_organization_fields feature" do
      let(:account) { accounts(:minimum) }

      before do
        account.stubs(:has_user_and_organization_fields?).returns(true)
      end

      it "handles user and organization tags on ticket create and requester change" do
        user = users(:minimum_end_user)
        ticket = account.tickets.new(description: "hejsa")
        ticket.will_be_saved_by(user)
        ticket.save!

        assert_equal 'beta.test premium superuser', ticket.current_tags

        ticket.requester = users(:minimum_author)
        ticket.will_be_saved_by(users(:minimum_author))
        ticket.save!
        assert_equal 'beta.test vip', ticket.current_tags
      end
    end

    describe "when account does not have user_and_organization_fields feature" do
      let(:account) { accounts(:minimum) }
      let(:ticket) { account.tickets.new(description: 'hejsa', set_tags: 'hi') }

      before do
        Account.any_instance.stubs(:has_user_and_organization_fields?).returns(false)
      end

      it "does not set user or organization tags on ticket create" do
        ticket.will_be_saved_by(users(:minimum_end_user))
        ticket.save!
        assert_equal 'hi', ticket.current_tags
      end

      it "does not set user tags or organization on requester change" do
        ticket.requester = users(:minimum_author)
        ticket.will_be_saved_by(users(:minimum_author))
        ticket.save!
        assert_equal 'hi', ticket.current_tags
      end
    end
  end

  describe "Status create events" do
    it "has value_previous=nil when creating the ticket with a status that is not NEW" do
      user = users(:minimum_end_user)
      ticket = user.account.tickets.new(description: "hejsa", status_id: StatusType.PENDING)
      ticket.will_be_saved_by(user)
      ticket.save!
      status_create_event = ticket.audits.first.events.detect { |e| e.value_reference == 'status_id' }
      assert_equal StatusType.PENDING, status_create_event.value.to_i
      assert_nil status_create_event.value_previous
    end
  end

  describe "Ticket Forms" do
    before do
      @account = accounts(:minimum)
      @ticket_form = TicketForm.init_default_form(@account)
      @ticket_form.save!
    end

    describe "#check_ticket_form_id" do
      it "does not run if ticket_form_id is null" do
        ticket = @account.tickets.new(description: "Wombats", ticket_form_id: nil)
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.expects(:validate_ticket_form_id).never
        ticket.save!
      end
      it "does not run if ticket_form_id has not changed" do
        ticket = @account.tickets.new(description: "Wombats")
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.expects(:validate_ticket_form_id).never
        ticket.save!
      end
      it "sets ticket form to the default ticket form if ticket_form_id is invalid or deleted" do
        ticket = @account.tickets.new(description: "Wombats", ticket_form_id: 5000)
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!
        assert_equal @account.ticket_forms.default.first.id, ticket.ticket_form_id
      end
      it "sets ticket_form_id given if ticket_form_id is valid" do
        ticket = @account.tickets.new(description: "Wombats", ticket_form_id: @ticket_form.id)
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!
        assert_equal @ticket_form.id, ticket.ticket_form_id
      end
    end

    describe "#set_ticket_form" do
      before do
        @admin = users(:minimum_admin)
        @new_ticket = @account.tickets.new(description: "Wombats", ticket_form_id: nil)
        Arturo.enable_feature!(:ticket_form_default)
      end

      it "does not run if ticket_form_default arturo is not enabled" do
        Arturo.disable_feature!(:ticket_form_default)
        ticket = tickets(:minimum_1)
        ticket.will_be_saved_by(@admin)
        ticket.expects(:set_ticket_form).never
        ticket.save!
      end

      it "does not run on existing tickets" do
        ticket = tickets(:minimum_1)
        ticket.will_be_saved_by(@admin)
        ticket.expects(:set_ticket_form).never
        ticket.save!
      end

      it "does not run if ticket_form is already set" do
        ticket = @account.tickets.new(description: "Wombats", ticket_form_id: @ticket_form.id)
        ticket.will_be_saved_by(@admin)
        ticket.expects(:set_ticket_form).never
        ticket.save!
      end

      it "sets a ticket form when the feature is enabled on a new ticket that has no ticket form" do
        ticket = @account.tickets.new(description: "Wombats")
        ticket.will_be_saved_by(@admin)
        ticket.save!
        assert_equal @ticket_form.id, ticket.ticket_form.id
      end
    end

    describe "events" do
      it "creates event for setting a ticket_form on a ticket" do
        ticket = @account.tickets.new(description: "hejsa", ticket_form_id: @ticket_form.id)
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!
        tf_event = ticket.audits.first.events.detect { |e| e.value_reference == 'ticket_form_id' }
        assert_equal @ticket_form.id, tf_event.value.to_i
        assert_nil tf_event.value_previous
      end

      it "creates event for changing a ticket_form on a ticket" do
        params = { ticket_form: { name: "Boo", display_name: "Boo stuff", default: false, end_user_visible: true, position: 3 } }
        new_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        new_form.save!

        ticket = @account.tickets.new(description: "hejsa", ticket_form_id: @ticket_form.id)
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!
        ticket.ticket_form_id = new_form.id
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!

        tf_event = ticket.audits.last.events.detect { |e| e.value_reference == 'ticket_form_id' }
        assert_equal new_form.id, tf_event.value.to_i
        assert_equal @ticket_form.id, tf_event.value_previous.to_i
      end
    end
  end

  describe "Brands" do
    before do
      @account = accounts(:minimum)
      @brand = FactoryBot.create(:brand, account_id: @account.id)
      @account.update_attributes default_brand: @brand
    end

    describe "integrity check" do
      it "does not run if brand_id is null" do
        ticket = @account.tickets.new(account_id: @account.id, description: "Wombats", brand_id: nil)
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.expects(:check_brand_id).never
        ticket.save!
      end

      it "does not run if brand_id has not changed" do
        ticket = @account.tickets.new(account_id: @account.id, description: "Wombats")
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.expects(:check_brand_id).never
        ticket.save!
      end

      it "returns an error if brand_id is invalid" do
        ticket = @account.tickets.new(account_id: @account.id, description: "Wombats", brand_id: 5000)
        ticket.will_be_saved_by(users(:minimum_admin))
        refute ticket.save
        ticket.errors.messages[:brand].include?("is invalid")
      end

      it "returns an error if brand_id is a deleted brand" do
        deleted_brand = FactoryBot.create(:brand, account_id: @account.id, active: false)
        deleted_brand.soft_delete!
        ticket = @account.tickets.new(account_id: @account.id, description: "Wombats", brand_id: deleted_brand.id)
        ticket.will_be_saved_by(users(:minimum_admin))
        refute ticket.save
        ticket.errors.messages[:brand].include?("is invalid")
      end

      it "returns an error if brand_id is a deactivated brand" do
        inactive_brand = FactoryBot.create(:brand, active: false, account_id: @account.id)
        ticket = @account.tickets.new(account_id: @account.id, description: "Wombats", brand_id: inactive_brand.id)
        ticket.will_be_saved_by(users(:minimum_admin))
        refute ticket.save
        ticket.errors.messages[:brand].include?("is invalid")
      end

      it "sets brand_id given if brand_id is valid" do
        ticket = @account.tickets.new(account_id: @account.id, description: "Wombats", brand_id: @brand.id)
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!
        assert_equal @brand.id, ticket.brand_id
      end
    end

    describe "validation check, #brand_invalid?" do
      it "returns the invalid brand attribute if the brand does not exist" do
        ticket = tickets(:minimum_1)
        ticket.brand_id = 1234
        assert_equal [:brand, :invalid], ticket.has_invalid_attribute_change?
      end

      it "returns the invalid brand attribute if the brand is not active" do
        ticket = tickets(:minimum_1)
        brand = FactoryBot.create(:brand, account_id: ticket.account_id, active: false)
        ticket.brand_id = brand.id
        assert_equal [:brand, :invalid], ticket.has_invalid_attribute_change?
      end

      it "returns false if the brand attribute has not changed" do
        ticket = tickets(:minimum_1)
        assert_equal false, ticket.has_invalid_attribute_change?
      end

      it "returns false if the brand has been changed to a valid brand" do
        ticket = tickets(:minimum_1)
        brand = FactoryBot.create(:brand, account_id: ticket.account_id, active: true)
        ticket.brand_id = brand.id
        assert_equal false, ticket.has_invalid_attribute_change?
      end
    end

    describe "set_default_brand" do
      it "does not run if brand_id is present" do
        ticket = @account.tickets.new(description: "Wombats", brand_id: @brand.id)
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.expects(:set_default_brand).never
        ticket.save!
      end

      it "does not run when updating existing tickets" do
        ticket = tickets(:minimum_1)
        ticket.brand_id = nil
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.expects(:set_default_brand).never
        ticket.save!
      end

      it "sets brand to the default brand if brand_id is nil" do
        ticket = @account.tickets.new(account_id: @account.id, description: "Wombats", brand_id: nil)
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!
        assert_equal @account.default_brand, ticket.brand
      end
    end

    describe "events" do
      let(:minimum_admin) { users(:minimum_admin) }

      describe "when multibrand feature is enabled" do
        before { Account.any_instance.stubs(:has_multibrand?).returns(true) }

        it "creates event for setting a brand on a ticket" do
          ticket = @account.tickets.new(description: "hejsa", brand_id: @brand.id)
          ticket.will_be_saved_by(minimum_admin)
          ticket.save!
          brand_event = ticket.audits.first.events.detect { |e| e.value_reference == 'brand_id' }
          assert_equal @brand.id, brand_event.value.to_i
          assert_nil brand_event.value_previous
        end

        it "creates event for changing a brand on a ticket" do
          new_brand = FactoryBot.create(:brand, name: 'Top Ramen')

          ticket = @account.tickets.new(description: "hejsa", brand_id: @brand.id)
          ticket.will_be_saved_by(minimum_admin)
          ticket.save!
          ticket.brand_id = new_brand.id
          ticket.will_be_saved_by(minimum_admin)
          ticket.save!

          brand_event = ticket.audits.last.events.detect { |e| e.value_reference == 'brand_id' }
          assert_equal new_brand.id, brand_event.value.to_i
          assert_equal @brand.id, brand_event.value_previous.to_i
        end
      end

      describe "when multibrand feature is disabled" do
        before { Account.any_instance.stubs(:has_multibrand?).returns(false) }

        it "does not create an event for setting a brand on ticket" do
          ticket = @account.tickets.new(description: "hejsa", brand_id: @brand.id)
          ticket.will_be_saved_by(minimum_admin)
          ticket.save!
          assert_nil ticket.audits.first.events.detect { |e| e.value_reference == 'brand_id' }
        end

        it "does not create an event for changing a brand on a ticket" do
          new_brand = FactoryBot.create(:brand, name: 'Top Ramen')

          ticket = @account.tickets.new(description: "hejsa", brand_id: @brand.id)
          ticket.will_be_saved_by(minimum_admin)
          ticket.save!
          ticket.brand_id = new_brand.id
          ticket.will_be_saved_by(minimum_admin)
          ticket.save!

          assert_nil ticket.audits.last.events.detect { |e| e.value_reference == 'brand_id' }
        end
      end
    end
  end

  describe ".capped_count" do
    it "returns a count" do
      assert_equal 1, Ticket.capped_count(1)
    end

    it "inherits conditions from its scope" do
      # this used to be .solved.working but this does not work in Rails 3+
      # https://github.com/rails/rails/issues/7365
      assert_equal 0, accounts(:minimum).tickets.where("1=2").capped_count(1)
    end
  end

  it "tests validation defaults" do
    Timecop.freeze(Time.now) do
      ticket = accounts(:minimum).tickets.new
      refute ticket.valid? # missing account and current_user

      user = users(:minimum_end_user)
      ticket.account = user.account
      refute ticket.valid? # missing current_user

      ticket.will_be_saved_by(user)
      ticket.description = "hello"
      assert ticket.valid?
      ticket.save!

      assert_equal ticket.account, user.account
      assert_equal user, ticket.requester
      assert_equal ticket.requester, ticket.submitter
      assert_equal groups(:minimum_group), ticket.group
      refute ticket.assignee
      assert_equal organizations(:minimum_organization1), ticket.organization
      assert_equal StatusType.NEW, ticket.status_id
      assert_equal TicketType.INCIDENT, ticket.ticket_type_id
      assert_equal 0, ticket.priority_id
      assert_equal ViaType.WEB_SERVICE, ticket.via_id
      assert ticket.via?(:web_service)
      assert ticket.via_kind?(:full)

      assert ticket.nice_id
      assert ticket.status_updated_at
      assert_equal ticket.updated_at, ticket.latest_comment_added_at
      refute ticket.solved_at
      refute ticket.resolution_time
      refute ticket.initially_assigned_at
      assert ticket.requester_updated_at
      refute ticket.assignee_updated_at
      refute ticket.linked_id
      refute ticket.due_date
    end
  end

  it "tests create ticket" do
    # Disable admin as agent, so we only have one agent in the system...
    ActiveRecord::Base.connection.execute("update users set roles = 0 where id <> #{users(:minimum_agent).id}")
    ActiveRecord::Base.connection.execute("delete from memberships where user_id <> #{users(:minimum_agent).id}")

    user = users(:minimum_end_user)
    Account.any_instance.stubs(:has_multibrand?).returns(true)
    Timecop.freeze do
      ticket = ticket_create(user, ticket_options: { is_public: true })

      assert_equal 0, ticket.priority_id
      assert_equal TicketType.INCIDENT, ticket.ticket_type_id
      assert ticket.group
      assert_equal(users(:minimum_agent), ticket.assignee)
      assert ticket.assigned_at
      assert_equal ticket.assigned_at, ticket.initially_assigned_at
      assert_equal ticket.updated_at, ticket.latest_comment_added_at
      refute ticket.assignee_updated_at
      assert_nil ticket.solved_at
      assert_equal StatusType.OPEN, ticket.status_id

      # Events
      assert_equal [
        "Comment : first comment : web form by minimum_end_user",
        "Create :  : web form by minimum_end_user",
        "Create : Assigned to Agent Minimum : web form by minimum_end_user",
        "Create : Group set to minimum_group : web form by minimum_end_user",
        "Create : Organization set to minimum organization : web form by minimum_end_user",
        "Create : Product Name set to \"Some text here\" : web form by minimum_end_user",
        "Create : Requester set to minimum_end_user : web form by minimum_end_user",
        "Create : Status set to Open : web form by minimum_end_user",
        "Create : Ticket brand set to minimum : web form by minimum_end_user",
        "Create : Type set to Incident : web form by minimum_end_user",
        "Notification : Agent Minimum : rule Notify assignee of assignment by minimum_end_user",
        "Notification : minimum_end_user : rule Notify requester of received request by minimum_end_user"
      ], ticket.audits[0].inspect_events.sort
      assert_equal 13, ticket.events.count(:all)
    end
  end

  it "tests submitter creates ticket on behalf of requester" do
    Timecop.freeze do
      requester = users(:minimum_end_user)
      ticket = requester.account.tickets.new(requester: requester, description: 'this is a problem')
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!
      ticket.reload

      assert_equal users(:minimum_end_user), ticket.requester
      assert_equal users(:minimum_agent), ticket.submitter
      assert_equal ticket.requester, ticket.comments.first.author
      assert_equal ticket.requester_updated_at, ticket.updated_at
    end
  end

  it "tests submitter creates ticket on behalf of requester force comment being from submitter" do
    requester = users(:minimum_end_user)
    ticket = requester.account.tickets.new(requester: requester, description: 'this is a problem')
    ticket.comment.author = users(:minimum_agent)
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    ticket.reload

    assert_equal users(:minimum_end_user), ticket.requester
    assert_equal users(:minimum_agent), ticket.submitter
    assert_equal users(:minimum_agent), ticket.comments.first.author
  end

  it "tests assignee replies with tags" do
    users(:minimum_admin).update_attribute(:roles, 0)

    ticket = tickets(:minimum_1)
    ticket.is_public = true

    # Assignee reply
    assert_difference "Comment.count(:all)", 1 do
      Timecop.freeze(Time.now) do
        ticket.add_comment(body: 'agent comment', is_public: true)
        ticket.additional_tags = 'a2 MEANWHILE'
        ticket.status_id = StatusType.PENDING
        ticket.priority_id = PriorityType.HIGH
        ticket.will_be_saved_by(users(:minimum_agent))
        ticket.save!
      end
    end

    ticket.reload

    assert_equal 'agent comment', ticket.comments.order("created_at").last.to_s
    assert ticket.comments.last.is_public?
    assert ticket.status?(:pending)
    assert ticket.priority?(:high)
    assert ticket.assignee_updated_at
    assert_equal ticket.updated_at, ticket.latest_comment_added_at
    assert_equal 'a2 hello meanwhile', ticket.current_tags # tag_list
    assert_includes ticket.audits.last.inspect_events, "Change : Priority changed from Normal to High : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Change : Status changed from New to Pending : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Change : Tags changed from \"hello\" to \"a2 hello meanwhile\" : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Comment : agent comment : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Notification : Author Minimum : rule Notify requester of comment update by Agent Minimum"
  end

  it "tests closed ticket cannot be altered" do
    ticket = tickets(:minimum_1)
    ticket.status_id = StatusType.CLOSED
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    ticket.reload
    ticket.will_be_saved_by(users(:minimum_agent))
    refute ticket.save # Ticket is closed and can't be handled anymore
  end

  it "saves checkbox field changes in a ticket audit as 0/1" do
    ticket = tickets(:minimum_1)
    checkbox = FieldCheckbox.create!(title: "Wombat Checkbox", account: ticket.account)
    checkbox.ticket_field_entries.create!(value: true, account: ticket.account, ticket_field: checkbox, ticket: ticket)
    ticket.will_be_saved_by(ticket.account.owner)
    ticket.save!

    # Change checkbox value
    ticket.fields = { checkbox.id.to_s => 'false' }
    ticket.will_be_saved_by(ticket.account.owner)
    ticket.save!
    change_event = ticket.audits.last.events.first
    assert_equal "0", change_event.value
    assert_equal "1", change_event.value_previous
  end

  it "tests assignee replies with tags for private first message" do
    # Temporarily disable admin as agent, so we only have one agent in the system...
    users(:minimum_admin).update_attribute(:roles, 0)

    ticket = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_agent), requester_id: users(:minimum_author).id, ticket: {
      submitter: users(:minimum_agent),
      subject: "Private Wombats",
      tags: "hello",
      comment: { public: false, value: "private wombat ticket about end user" }
    }).ticket
    ticket.priority_id = 2
    ticket.assignee_id = users(:minimum_agent).id
    ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.WEB_FORM)
    ticket.save!

    assert_equal false, ticket.is_public?
    assert_equal ViaType.WEB_FORM, ticket.via_id

    # Assignee reply
    assert_difference "Comment.count(:all)", 1 do
      Timecop.freeze(Time.now) do
        ticket.add_comment(body: 'agent comment', is_public: true)
        ticket.additional_tags = 'a2 MEANWHILE'
        ticket.status_id = StatusType.PENDING
        ticket.priority_id = PriorityType.HIGH
        ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.WEB_FORM)
        ticket.save!
      end
    end

    ticket.reload

    assert_equal 'agent comment', ticket.comments.order("created_at").last.to_s
    assert ticket.comments.last.is_public?

    # Since agent made a public comment, this ticket should change to a public ticket
    assert ticket.is_public?

    assert_includes ticket.audits.last.inspect_events, "Change : Customer visible changed from No to Yes : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Change : Priority changed from Normal to High : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Change : Status changed from Open to Pending : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Change : Tags changed from \"hello\" to \"a2 hello meanwhile\" : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Comment : agent comment : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Notification : Author Minimum : rule Notify requester of comment update by Agent Minimum"
  end

  it "tests user reply from new ticket" do
    ticket = tickets(:minimum_1)
    user = ticket.requester

    old_updated_at = ticket.updated_at
    assert_difference "ticket.comments.count(:all)", 1 do
      ticket.add_comment(body: 'user comment', is_public: true)
      ticket.will_be_saved_by(user)
      ticket.save!
    end

    ticket.reload
    assert_equal 'user comment', ticket.comments.last.to_s
    assert ticket.comments.last.is_public?
    assert_not_equal old_updated_at, ticket.updated_at

    assert ticket.status?(:open)
    assert_equal [
      "Change : Status changed from New to Open : web form by Author Minimum",
      "Comment : user comment : web form by Author Minimum",
      "Notification : Agent Minimum : rule Notify assignee of comment update by Author Minimum"
    ], ticket.audits.last.inspect_events.sort
  end

  it "tests user reply to open ticket" do
    ticket = tickets(:minimum_1)
    ticket.status_id = StatusType.OPEN
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    ticket.reload

    assert_difference "ticket.comments.count(:all)", 1 do
      ticket.add_comment(body: 'user comment', is_public: true)
      ticket.will_be_saved_by(ticket.requester)
      ticket.save!
    end

    ticket.reload
    assert_equal 'user comment', ticket.comments.last.to_s
    assert ticket.comments.last.is_public?

    assert ticket.status?(:open)
    assert_equal [
      "Comment : user comment : web form by Author Minimum",
      "Notification : Agent Minimum : rule Notify assignee of comment update by Author Minimum"
    ], ticket.audits.last.inspect_events.sort
  end

  it "tests solve and close" do
    # Assignee solve
    ticket = tickets(:minimum_1)
    ticket.current_tags = "shoop woop"
    ticket.status_id = StatusType.SOLVED
    ticket.add_comment(body: 'Setting this to solved...', is_public: true)
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    ticket.reload
    assert_includes ticket.audits.last.inspect_events, "Change : Status changed from New to Solved : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Change : Tags changed from \"hello\" to \"shoop woop\" : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Comment : Setting this to solved... : web form by Agent Minimum"
    assert_includes ticket.audits.last.inspect_events, "Notification : Author Minimum : rule Notify requester of solved ticket by Agent Minimum"
    assert ticket.status?(:solved)
    assert ticket.solved_at

    # the ticket needs to be older that one minute in order for automations to pick it up
    Ticket.record_timestamps = false
    ticket.update_attribute(:updated_at, 10.minutes.ago)
    Ticket.record_timestamps = true

    Zendesk::Rules::RuleExecuter.
      any_instance.
      stubs(:find_tickets_with_occam_client).
      returns([ticket])

    # Test automation
    rules(:automation_close_ticket).execute

    assert_includes ticket.reload.audits.last.inspect_events, "Change : Status changed from Solved to Closed : rule Close ticket 4 days after status is set to solved by Agent Minimum"
    assert_includes ticket.reload.audits.last.inspect_events, "Notification : Author Minimum : rule Close ticket 4 days after status is set to solved by Agent Minimum"

    assert ticket.status?(:closed)
    assert_equal 0, rules(:automation_close_ticket).execute.changed

    # Attempt to reopen closed ticket
    ticket.status_id = StatusType.OPEN
    ticket.will_be_saved_by(users(:minimum_admin))
    refute ticket.save
  end

  it "tests close job" do
    # Set a ticket to solved
    ticket = tickets(:minimum_1)
    ticket.status_id = StatusType.SOLVED
    ticket.solved_at = 5.weeks.ago

    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    ActiveRecord::Base.connection.execute("update tickets set solved_at = '#{5.weeks.ago.to_s(:db)}' where id = #{ticket.id}")

    # Run Ticket Close batch job
    assert_difference "Audit.count(:all)", 1 do
      Zendesk::Maintenance::Jobs::DailyTicketCloseOnShardJob.work_on_shard(ticket.account.shard_id)
    end

    # Check that the ticket was closed by the Ticket Close Batch job
    assert_equal ["Change : Status changed from Solved to Closed : batch by SYSTEM"],
      ticket.reload.audits.last.inspect_events.sort
  end

  it "tests reassign" do
    ticket = tickets(:minimum_1)
    assert_equal users(:minimum_agent), ticket.assignee
    ticket.assignee = users(:minimum_admin)
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    ticket.reload
    assert_equal users(:minimum_admin), ticket.assignee
    assert_not_equal ticket.assigned_at, ticket.initially_assigned_at
    assert_not_equal ticket.updated_at, ticket.latest_comment_added_at
  end

  describe "test cascading triggers" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.additional_tags = 'findme'
      @ticket.will_be_saved_by(users(:minimum_agent))
    end

    it "cascades properly" do
      @ticket.save!
      @ticket.reload

      # Check cascading triggers: trigger_cascade1 and trigger_cascade2
      assert_equal 'findme gotcha hello hereiam', @ticket.current_tags # tag_list
      assert_includes @ticket.audits.last.inspect_events, "Change : Status changed from New to Open : web form by Agent Minimum"
      assert_includes @ticket.audits.last.inspect_events, "Change : Tags changed from \"findme hello hereiam\" to \"findme gotcha hello hereiam\" : rule cascade2 by Agent Minimum"
      assert_includes @ticket.audits.last.inspect_events, "Change : Tags changed from \"findme hello\" to \"findme hello hereiam\" : rule cascade1 by Agent Minimum"
      assert_includes @ticket.audits.last.inspect_events, "Change : Tags changed from \"hello\" to \"findme hello\" : web form by Agent Minimum"
    end

    it "truncates tags" do
      # Append a stupid amount of tags with the second trigger
      tag_sequence = ('a'..'y').map { |s| s * 202 }.join(' ')
      cascade2 = rules(:trigger_cascade2)
      cascade2.definition.actions.first.value = ["zthistagistoolongtofitontheendofthetaglist #{tag_sequence}"]
      cascade2.save!

      @ticket.save!
      @ticket.reload

      assert_includes @ticket.current_tags.split(' '), "findme"
      refute_includes @ticket.current_tags.split(' '), "zthistagistoolongtofitontheendofthetaglist"
    end
  end

  it "tests group assignee trigger" do
    user = users(:minimum_end_user)
    Account.any_instance.stubs(:has_multibrand?).returns(true)
    ticket = ticket_create(user, ticket_options: { is_public: true })

    # assert_equal 0, ticket.priority_id
    assert_equal TicketType.INCIDENT, ticket.ticket_type_id
    assert ticket.group
    refute ticket.assignee
    refute ticket.assigned_at
    assert_equal [
      "Comment : first comment : web form by minimum_end_user",
      "Create :  : web form by minimum_end_user",
      "Create : Group set to minimum_group : web form by minimum_end_user",
      "Create : Organization set to minimum organization : web form by minimum_end_user",
      "Create : Product Name set to \"Some text here\" : web form by minimum_end_user",
      "Create : Requester set to minimum_end_user : web form by minimum_end_user",
      "Create : Status set to New : web form by minimum_end_user",
      "Create : Ticket brand set to minimum : web form by minimum_end_user",
      "Create : Type set to Incident : web form by minimum_end_user",
      "Notification : Admin Man, Agent Minimum : rule Notify group of assignment by minimum_end_user",
      "Notification : minimum_end_user : rule Notify requester of received request by minimum_end_user"
    ], ticket.audits[0].inspect_events.sort
  end

  it "tests triggers autocorrect ticket group when the assignee is not in the ticket group" do
    ticket = tickets(:with_groups_2)

    definition = Definition.new
    [DefinitionItem.new('assignee_id', nil, users(:with_groups_agent1).id)].each { |a| definition.actions.push a }
    trigger = Trigger.new(definition: definition)

    assert_equal users(:with_groups_agent2), ticket.assignee
    assert_equal groups(:with_groups_group2), ticket.group

    trigger.apply_actions(ticket)

    # Trigger updated assignee - at the same time, the group was auto-corrected,
    # as assignee (with_groups_agent1) does not belong to the ticket's original group (with_groups_group2)
    assert ticket.assignee.reload.in_group?(ticket.group)
    assert_equal users(:with_groups_agent1), ticket.assignee.reload
    assert_equal groups(:with_groups_group1), ticket.group.reload
  end

  it "tests triggers cannot set assignee that does not belong in any group" do
    ticket = tickets(:with_groups_2)

    definition = Definition.new
    [DefinitionItem.new('assignee_id', nil, users(:with_groups_agent1).id)].each { |a| definition.actions.push a }
    trigger = Trigger.new(definition: definition)

    assert_equal users(:with_groups_agent2), ticket.assignee

    users(:with_groups_agent1).memberships.delete_all
    trigger.apply_actions(ticket)

    # Not updated according to trigger, as assignee (with_groups_agent1) does not belong to any groups, and can't be assigned
    assert_equal users(:with_groups_agent2), ticket.assignee.reload
  end

  describe "resetting assignee within the group" do
    before do
      @ticket  = tickets(:minimum_1)
      @agent   = users(:minimum_agent)
    end

    it "is allowed when the setting is enabled" do
      @ticket.account.settings.change_assignee_to_general_group = true
      @ticket.assignee = nil
      @ticket.will_be_saved_by(@agent)
      assert @ticket.save
    end

    it "does not be allowed when the setting is disabled" do
      @ticket.account.settings.change_assignee_to_general_group = false
      @ticket.assignee = nil
      @ticket.will_be_saved_by(@agent)
      refute @ticket.save
    end

    it "does not be allowed for accounts created before this setting was added" do
      @ticket.assignee = nil
      @ticket.will_be_saved_by(@agent)
      refute @ticket.save
    end
  end

  it "tests invalid priority setting" do
    ticket = tickets(:minimum_1)
    ticket.priority_id = PriorityType.Urgent
    ticket.will_be_saved_by(users(:minimum_agent))
    refute ticket.save
  end

  it "tests ticket operations for account with multiple groups" do
    user = users(:with_groups_end_user)
    ticket = ticket_create user

    assert_equal 0, ticket.priority_id
    assert_equal 0, ticket.ticket_type_id
    assert_equal StatusType.NEW, ticket.status_id
    assert_equal [
      "Comment : first comment : web form by with_groups_end_user",
      "Create :  : web form by with_groups_end_user",
      "Create :  : web form by with_groups_end_user",
      "Create : Requester set to with_groups_end_user : web form by with_groups_end_user",
      "Create : Status set to New : web form by with_groups_end_user",
      "Notification : John Doe, with_groups_admin, with_groups_agent_two : rule Notify all agents of received request by with_groups_end_user"
    ], ticket.audits[0].inspect_events.sort

    # Assignment and assignee reply
    ticket.assignee = users(:with_groups_agent1)
    ticket.add_comment(body: 'agent comment', is_public: true)

    ticket.group = groups(:with_groups_group1)
    ticket.will_be_saved_by(users(:with_groups_agent1))
    ticket.save!

    ticket.priority_id = PriorityType.URGENT

    ticket.ticket_type_id = TicketType.INCIDENT
    ticket.status_id = StatusType.PENDING
    ticket.will_be_saved_by(users(:with_groups_agent1))
    ticket.save!

    ticket.reload
    assert_equal 'agent comment', ticket.comments.last.to_s
    assert_equal StatusType.PENDING, ticket.status_id
    assert_equal TicketType.INCIDENT, ticket.ticket_type_id
    assert_equal PriorityType.URGENT, ticket.priority_id
    assert ticket.assignee_updated_at
  end

  describe "voice via types" do
    it "returns true for appropriate via ids" do
      vias = ViaType.list.map(&:last)
      vias.each do |via|
        ticket = Ticket.new(via_id: via)
        case via
        when ViaType.PHONE_CALL_INBOUND, ViaType.PHONE_CALL_OUTBOUND
          assert ticket.via_voice?
          assert ticket.via_phone?
        when ViaType.VOICEMAIL
          assert ticket.via_voice?
          refute ticket.via_phone?
        else
          refute ticket.via_voice?
          refute ticket.via_phone?
        end
      end
    end
  end

  describe "when changing the ticket type from TASK to something else" do
    let(:ticket) { tickets(:minimum_1) }
    before { ticket.update_attributes("ticket_type_id" => TicketType.TASK, "due_date" => Time.zone.now) }

    it "nullifys the current due_date on save" do
      ticket.account.stubs(:has_extended_ticket_types?).returns(true) # needed to avoid 'cleanup_attributes' before_save from changing the type
      ticket.ticket_type_id = TicketType.QUESTION
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!
      assert_equal TicketType.QUESTION, ticket.reload.ticket_type_id
      assert_nil ticket.due_date
    end
  end

  describe "tweet back" do
    [true, false].each do |reply_options_enabled|
      describe "reply_options Arturo flag is #{reply_options_enabled ? 'en' : 'dis'}abled" do
        before do
          Arturo.set_feature!(:reply_options, reply_options_enabled)
        end

        describe "existing tickets" do
          before do
            @ticket = tickets(:minimum_1)
            @ticket.via_id = ViaType.TWITTER
            @ticket.requester_id = users(:minimum_end_user).id
            @ticket.add_comment(body: 'public comment with tweet',
                                is_public: true,
                                channel_back: "1")
            @ticket.will_be_saved_by(users(:minimum_agent))
          end

          it "has a monitored twitter handle" do
            assert @ticket.twitter_ticket_source
          end

          it "does not tweet with old events if the monitored twitter handle is still monitored" do
            @ticket.expects(:create_channel_back_event)
            refute_difference('ChannelBackEvent.count(:all)') do
              @ticket.save
              @ticket.reload
            end
          end

          it "does not tweet the requester if the monitored twitter handle is no longer monitored" do
            @ticket.stubs(:twitter_source).returns(nil)
            refute_difference('ChannelBackEvent.count(:all)') do
              @ticket.save
              @ticket.reload
            end
          end

          describe "when the user is trying to update a twitter ticket with an email only with a requester without an email" do
            before do
              @ticket.requester.identities.email.destroy_all
              @ticket.requester.identities.reload
              @ticket.collaborations = []
              @ticket.comment.channel_back = "false"
              @ticket.comment.stubs(:is_email_only?).returns(true)
            end

            it "does not be valid if there are no ccs" do
              refute @ticket.save
              assert_includes @ticket.errors[:base], I18n.t("txt.admin.models.ticket.validation.email_only_without_email")
            end

            it "is valid if there are ccs" do
              @ticket.collaborations.create(user: users(:minimum_end_user2))
              @ticket.collaborators.reload
              @ticket.save
              assert_not_includes @ticket.errors[:base], I18n.t("txt.admin.models.ticket.validation.email_only_without_email")
            end

            it "is valid if the comment is not email only" do
              @ticket.comment.stubs(:is_email_only?).returns(false)
              @ticket.save
              assert_not_includes @ticket.errors[:base], I18n.t("txt.admin.models.ticket.validation.email_only_without_email")
            end
          end
        end
      end
    end
  end

  describe '#allow_channelback?' do
    before do
      @ticket = tickets(:minimum_1)
      @event_decoration = event_decorations(:minimum)
    end

    it 'is true when there is no event_decoration' do
      assert_nil @ticket.latest_public_comment.event_decoration
      assert @ticket.allow_channelback?
    end

    it 'is true when event_decoration has no data' do
      @event_decoration.data = nil
      @ticket.latest_public_comment.event_decoration = @event_decoration
      assert @ticket.allow_channelback?
    end

    it 'is true when event_decoration has no any_channel data' do
      assert_nil @event_decoration.data.any_channel
      @ticket.latest_public_comment.event_decoration = @event_decoration
      assert @ticket.allow_channelback?
    end

    it 'is false when event_decoration has any_channel data and allow_channelback flag is false' do
      data = @event_decoration.data
      data.any_channel = { allow_channelback: false }
      @event_decoration.data = data
      @ticket.latest_public_comment.event_decoration = @event_decoration
      refute @ticket.allow_channelback?
    end
  end

  it "wired up destroy for audits and activities" do
    assert_equal :destroy, Ticket.reflect_on_association(:audits).options[:dependent]
    assert_equal :destroy, Ticket.reflect_on_association(:activities).options[:dependent]
  end

  it "tests agent restrictions for views" do
    # Group restricted
    tickets = accounts(:with_groups).tickets.for_user(users(:with_groups_agent_groups_restricted))
    assert_equal 1, tickets.size
    assert_equal tickets(:with_groups_2), tickets.first

    # Assigned restricted
    tickets = accounts(:with_groups).tickets.for_user(users(:with_groups_agent_assigned_restricted))
    assert_equal 1, tickets.size
    assert_equal tickets(:with_groups_4), tickets.first

    # Organization restricted
    assert_equal 0, accounts(:with_groups).tickets.for_user(users(:with_groups_agent_organization_restricted)).count(:all)

    # Not restricted - finds all tickets
    assert_equal 4, accounts(:with_groups).tickets.for_user(users(:with_groups_agent2)).count(:all)
  end

  it "tests agent can add private comments" do
    current_user = users(:minimum_agent)
    ticket = tickets(:minimum_1)

    current_user.update_attribute(:is_private_comments_only, true)

    ticket.add_comment(body: 'private agent comment', is_public: false)
    ticket.will_be_saved_by(current_user.reload)
    ticket.save!
    refute ticket.comments.last.reload.is_public? # Comment is not public
  end

  it "tests agent can only add private comments" do
    current_user = users(:minimum_agent)
    ticket = tickets(:minimum_1)

    current_user.update_attribute(:is_private_comments_only, true)

    ticket.add_comment(body: 'private agent comment', is_public: true)
    ticket.will_be_saved_by(current_user.reload)
    refute ticket.save
  end

  it "tests agent with private permissions can add private comments" do
    current_user = users(:minimum_agent)
    ticket = tickets(:minimum_1)
    refute current_user.is_private_comments_only?

    current_user.permission_set = current_user.account.permission_sets.create!(name: "Test")
    current_user.permission_set.name = "AgentPermissions"
    current_user.permission_set.account = current_user.account
    assert current_user.permission_set.save!
    assert current_user.account.stubs(:has_permission_sets?).returns(true)

    current_user.permission_set.permissions.comment_access = 'private'
    assert current_user.permission_set.save!
    ticket.add_comment(body: 'agent comment', is_public: false)
    ticket.will_be_saved_by(current_user)
    ticket.save!
    refute ticket.comments.last.reload.is_public? # Comment is not public
  end

  it "tests agent with public permissions can add public comments" do
    current_user = users(:minimum_agent)
    ticket = tickets(:minimum_1)

    current_user.is_private_comments_only = true
    assert current_user.is_private_comments_only?

    current_user.permission_set = current_user.account.permission_sets.create!(name: "Test")

    current_user.permission_set.name = "AgentPermissions"
    current_user.permission_set.account = current_user.account
    current_user.permission_set.permissions.comment_access = 'public'

    assert current_user.permission_set.save!
    assert current_user.account.stubs(:has_permission_sets?).returns(true)
    current_user.save!

    ticket.add_comment(body: 'agent comment', is_public: true)
    ticket.will_be_saved_by(current_user)
    ticket.save!
    assert ticket.comments.last.reload.is_public? # Comment is public
  end

  it "tests set existing requester" do
    ticket = tickets(:minimum_1)
    assert_equal users(:minimum_author), ticket.requester
    ticket.requester = users(:minimum_end_user)
    ticket.will_be_saved_by(ticket.requester)
    ticket.save!
    assert_equal users(:minimum_end_user), ticket.reload.requester
  end

  it "tests set existing requester via set requester data" do
    ticket = tickets(:minimum_1)
    assert_equal users(:minimum_author), ticket.requester
    ticket.requester_data = 'minimum_end_user@aghassipour.com'
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    assert_equal users(:minimum_end_user), ticket.reload.requester
  end

  it "tests set new requester via set requester data" do
    ticket = tickets(:minimum_1)
    assert_equal users(:minimum_author), ticket.requester
    ticket.requester_data = 'Hans Hansen <hans@gmail.com>'
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    assert_equal 'Hans Hansen', ticket.reload.requester.name
    assert_equal 'hans@gmail.com', ticket.requester.email
  end

  # Tests also code in FieldTagger#normalize_value_and_sync_tags
  it "tests taggers" do
    ticket = tickets(:minimum_1)
    assert_equal 1, ticket.taggings.size

    ticket_field = ticket_fields(:field_tagger_custom)

    ticket.additional_tags = 'hilarious'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 2, ticket.reload.taggings.size
    assert_equal 'hilarious', ticket_field.value(ticket)
    events = ticket.audits.last.events
    assert_equal "Change : Tags changed from \"hello\" to \"hello hilarious\" : web form by Admin Man",
      events.find { |e| e.value_reference == "current_tags" }.inspect_me
    assert_equal "hilarious",
      events.find { |e| e.value_reference == ticket_field.id.to_s }.value

    # Add new tagger tag, replace the preceding
    ticket.additional_tags = 'booring'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 2, ticket.reload.taggings.size
    assert_equal 'booring', ticket_field.value(ticket)
    assert_equal 'booring hello', ticket.current_tags
    assert_equal ticket.current_tags, ticket.tag_array.sort.join(' ')
    events = ticket.audits.last.events
    assert_equal "Change : Tags changed from \"hello hilarious\" to \"booring hello\" : web form by Admin Man",
      events.find { |e| e.value_reference == "current_tags" }.inspect_me
    assert_equal "booring",
      events.find { |e| e.value_reference == ticket_field.id.to_s }.value

    # Remove tagger tag
    ticket.additional_tags = 'boing boing'
    ticket.fields = {ticket_field.id.to_s => ''} # Add tagger field though params
    ticket.additional_tags = 'banko'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 'banko boing hello', ticket.reload.current_tags
    assert_equal '', ticket_field.value(ticket)
    assert_equal ticket.current_tags, ticket.tag_array.sort.join(' ')

    # Set tagger directly, allow upcased values
    ticket.fields = {ticket_field.id.to_s => 'Hilarious'}
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 'banko boing hello hilarious', ticket.reload.current_tags
    events = ticket.audits.last.events
    assert_equal "Change : Tags changed from \"banko boing hello\" to \"banko boing hello hilarious\" : web form by Admin Man",
      events.find { |e| e.value_reference == "current_tags" }.inspect_me
    assert_equal "hilarious",
      events.find { |e| e.value_reference == ticket_field.id.to_s }.value

    # Do not allow tags to be set from different taggers
    new_field = FieldTagger.new(account: accounts(:minimum), title: "new tagger")
    new_field.custom_field_options.new(name: "ABC", value: "abc")
    new_field.save!
    ticket.fields = {ticket_field.id => "abc" }
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal '', new_field.value(ticket)
  end

  # Tests also code in FieldMultiselect#normalize_value_and_sync_tags
  it "tests multiselects" do
    account = accounts(:minimum)
    ticket = tickets(:minimum_1)
    assert_equal 1, ticket.taggings.size

    ticket_field = FieldMultiselect.create!(
      title: 'Test multiselect',
      account: account,
      is_active: true,
      custom_field_options: [
        CustomFieldOption.new(name: "Option1", value: "option1"),
        CustomFieldOption.new(name: "Option2", value: "option2"),
        CustomFieldOption.new(name: "Option3", value: "option3"),
        CustomFieldOption.new(name: "Option4", value: "option4")
      ]
    )

    # Set field value through tags
    ticket.additional_tags = 'option1'
    Timecop.travel(Time.now + 1.seconds)
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 2, ticket.reload.taggings.size
    assert_equal 'option1', ticket_field.value(ticket)

    # Add new tag, keep the preceding valid option
    ticket.additional_tags = 'option2'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 3, ticket.reload.taggings.size
    assert_equal 'option1 option2', ticket_field.value(ticket)
    assert_equal 'hello option1 option2', ticket.current_tags
    assert_equal ticket.current_tags, ticket.tag_array.sort.join(' ')
    events = ticket.audits.last.events
    assert_equal "Change : Tags changed from \"hello option1\" to \"hello option1 option2\" : web form by Admin Man",
      events.find { |e| e.value_reference == "current_tags" }.inspect_me
    assert_equal "option1 option2",
      events.find { |e| e.value_reference == ticket_field.id.to_s }.value

    # Remove multiselect tag
    ticket.additional_tags = 'boing boing'
    ticket.fields = { ticket_field.id.to_s => '' } # Add multiselect field though params
    ticket.additional_tags = 'banko'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 'banko boing hello', ticket.reload.current_tags
    assert_equal '', ticket_field.value(ticket)
    assert_equal ticket.current_tags, ticket.tag_array.sort.join(' ')
    events = ticket.audits.last.events
    assert_equal "Change : Tags changed from \"hello option1 option2\" to \"banko boing hello\" : web form by Admin Man",
      events.find { |e| e.value_reference == "current_tags" }.inspect_me
    assert_equal "",
      events.find { |e| e.value_reference == ticket_field.id.to_s }.value

    # Set multiselect directly, allow upcased values
    ticket.fields = { ticket_field.id.to_s => ['Option1'] }
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 'banko boing hello option1', ticket.reload.current_tags
    assert_equal 'option1', ticket_field.value(ticket)
    events = ticket.audits.last.events
    assert_equal "Change : Tags changed from \"banko boing hello\" to \"banko boing hello option1\" : web form by Admin Man",
      events.find { |e| e.value_reference == "current_tags" }.inspect_me
    assert_equal "option1",
      events.find { |e| e.value_reference == ticket_field.id.to_s }.value

    # Set multiple values, allow upcased values and ignore bad options
    ticket.fields = { ticket_field.id.to_s => ['option1', 'Option2', 'option3', 'bad_option'] }
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 'banko boing hello option1 option2 option3', ticket.reload.current_tags
    assert_equal 'option1 option2 option3', ticket_field.value(ticket)
    events = ticket.audits.last.events
    assert_equal "Change : Tags changed from \"banko boing hello option1\" to \"banko boing hello option1 option2 option3\" : web form by Admin Man",
      events.find { |e| e.value_reference == "current_tags" }.inspect_me
    assert_equal "option1 option2 option3",
      events.find { |e| e.value_reference == ticket_field.id.to_s }.value

    # If we receive NEW options as tags and a direct value for multiselect field in the same request,
    # we merge them and keep both values
    ticket.additional_tags = 'option4 bye'
    ticket.fields = { ticket_field.id.to_s => ['option2'] }
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 'banko boing bye hello option2 option4', ticket.reload.current_tags
    assert_equal 'option2 option4', ticket_field.value(ticket)
    events = ticket.audits.last.events
    assert_equal "Change : Tags changed from \"banko boing hello option1 option2 option3\" to \"banko boing bye hello option2 option4\" : web form by Admin Man",
      events.find { |e| e.value_reference == "current_tags" }.inspect_me
    assert_equal "option2 option4",
      events.find { |e| e.value_reference == ticket_field.id.to_s }.value

    # If we receive OLD options as tags and a direct value for multiselect field in the same request,
    # we keep only the value of the field because we're assuming the user wants to
    # remove option4.
    # This is what happens in lotus when user deselect one of the options from the field
    # and lotus will send the new field selection and the old tags together in the same request
    # in that case we need to do what the user wants and remove the option and the related tag.
    ticket.additional_tags = 'option4 bye'
    ticket.fields = { ticket_field.id.to_s => ['option2'] }
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal 'banko boing bye hello option2', ticket.reload.current_tags
    assert_equal 'option2', ticket_field.value(ticket)
    events = ticket.audits.last.events
    assert_equal "Change : Tags changed from \"banko boing bye hello option2 option4\" to \"banko boing bye hello option2\" : web form by Admin Man",
      events.find { |e| e.value_reference == "current_tags" }.inspect_me
    assert_equal "option2",
      events.find { |e| e.value_reference == ticket_field.id.to_s }.value
  end

  # Tests also code in FieldCheckbox#normalize_value_and_sync_tags
  it "tests checkbox" do
    ticket = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_agent), requester_id: users(:minimum_end_user).id, ticket: {
      submitter: users(:minimum_agent),
      subject: "Public Wilma",
      tags: "three",
      comment: { value: "public Flintstone ticket about end user", public: true }
    }).ticket

    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    ticket_field = FieldCheckbox.create!(title: 'Test checkbox', tag: 'onnn', account: accounts(:minimum))
    assert ticket_field.value(ticket).blank?

    # Set checkbox via tag
    ticket.reload
    ticket.additional_tags = 'onnn'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!

    assert_equal '1', ticket_field.value(ticket)
    assert_equal [
      "Change : Tags changed from \"three\" to \"onnn three\" : rule Notify assignee of comment update by Admin Man",
      "Change : Test Checkbox changed from \"-\" to \"Yes\" : rule Notify assignee of comment update by Admin Man"
    ],
      ticket.audits.last.inspect_events.sort

    ticket.remove_tags = 'onnn'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal '0', ticket_field.value(ticket)
    assert_equal [
      "Change : Tags changed from \"onnn three\" to \"three\" : rule Notify assignee of comment update by Admin Man",
      "Change : Test Checkbox changed from \"Yes\" to \"-\" : rule Notify assignee of comment update by Admin Man"
    ],
      ticket.audits.last.inspect_events.sort

        # Set checkbox directly
    ticket.fields = {ticket_field.id.to_s => '1'}
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal '1', ticket_field.value(ticket)
    assert_equal 'onnn three', ticket.current_tags
    assert_equal [
      "Change : Tags changed from \"three\" to \"onnn three\" : rule Notify assignee of comment update by Admin Man",
      "Change : Test Checkbox changed from \"-\" to \"Yes\" : rule Notify assignee of comment update by Admin Man"
    ],
      ticket.audits.last.inspect_events.sort

    ticket.fields = {ticket_field.id.to_s => '0'}
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal '0', ticket_field.value(ticket)
    assert_equal 'three', ticket.current_tags
    assert_equal [
      "Change : Tags changed from \"onnn three\" to \"three\" : rule Notify assignee of comment update by Admin Man",
      "Change : Test Checkbox changed from \"Yes\" to \"-\" : rule Notify assignee of comment update by Admin Man"
    ],
      ticket.audits.last.inspect_events.sort
  end

  it "tests existing tag for checkboxes" do
    ticket = tickets(:minimum_3)

    # Given a tag before the field is created
    ticket.current_tags = 'onnn'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!

    ticket_field = FieldCheckbox.create!(title: 'Test checkbox',
                                         tag: 'onnn',
                                         account: accounts(:minimum))
    assert ticket_field.value(ticket).blank?

    # When selecting the checkbox
    ticket.fields = {ticket_field.id.to_s => '1'}
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!

    # The checkbox should stay checked
    assert_equal '1', ticket_field.value(ticket)
  end

  it "tests existing tag for taggers" do
    ticket = tickets(:minimum_3)

    # Given a tag before the field is created
    ticket.current_tags = 'foo'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!

    ticket_field = FieldTagger.create!(title: 'Test tagger',
                                       account: accounts(:minimum),
                                       is_active: true,
                                       custom_field_options: [CustomFieldOption.new(name: "foo", value: "foo")])

    ticket.reload

    assert ticket_field.value(ticket).blank?

    ticket.fields = {ticket_field.id.to_s => 'foo'}

    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!

    assert_equal 'foo', ticket_field.value(ticket)
  end

  it "tests multiple tagging when updating" do
    ticket_field = ticket_fields(:field_tagger_custom)

    ticket = tickets(:minimum_2)
    ticket.set_tags = 'dunhammer'
    ticket.additional_tags = 'hilarious' # Manually add tagger field
    ticket.fields = {ticket_field.id.to_s => 'booring'} # Add tagger field though params
    ticket.additional_tags = 'sovs'
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    # Ensure that only the latest tagger field is added
    assert_equal 'booring dunhammer sovs', ticket.current_tags
    assert_equal 'booring', ticket_fields(:field_tagger_custom).value(ticket)
    assert_equal "Change : Tags changed from \"big hello\" to \"booring dunhammer sovs\" : web form by Admin Man",
      ticket.audits.last.events.select { |e| e.type == "Change" && e.value_reference == "current_tags" }.first.inspect_me
  end

  it "tests custom field change" do
    ticket = tickets(:minimum_2)
    ticket.fields = {ticket_fields(:field_text_custom).id.to_s => 'Have a nice day',
                     ticket_fields(:field_textarea_custom).id.to_s => "It' late.\n\nGo home"}
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal [
      %(Change : More changed from "-" to "It' late.\n\nGo home" : web form by Admin Man),
      %(Change : Product Name changed from "here is some text..." to "Have a nice day" : web form by Admin Man)
    ],
      ticket.audits.last.inspect_events.sort
  end

  it "tests solve ticket without comment" do
    ticket = tickets(:minimum_2)
    ticket.status_id = StatusType.SOLVED
    ticket.assignee = users(:minimum_admin)
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.save!
    assert_equal [
      "Change : Assignee changed from - to Admin Man : web form by Admin Man",
      "Change : Status changed from Open to Solved : web form by Admin Man"
    ],
      ticket.audits.last.inspect_events.sort
  end

  it "tests updatable by" do
    @author = users(:minimum_author)

    assert users(:systemuser).can?(:edit, tickets(:minimum_2))
    assert users(:minimum_end_user).can?(:edit, tickets(:minimum_2))
    assert users(:minimum_agent).can?(:edit, tickets(:minimum_2))
    refute @author.can?(:edit, tickets(:minimum_2))
    refute users(:with_groups_agent2).can?(:edit, tickets(:minimum_2))

    refute @author.can?(:view, tickets(:minimum_2))

    @author.organization = organizations(:minimum_organization1)
    @author.save!

    @author.abilities(_reload = true)

    assert @author.reload.can?(:view, tickets(:minimum_2)) # Can be viewed, as ticket org is shared, and user is now part of org

    refute @author.can?(:edit, tickets(:minimum_2)) # ...but still can't be updated

    @author.organization.update_attribute(:is_shared_comments, true)
    tickets(:minimum_2).reload
    @author.abilities(_reload = true)
    assert @author.reload.can?(:edit, tickets(:minimum_2)) # ... now the ticket can be updated, as the organization has shared comments

    refute users(:with_groups_agent_assigned_restricted).can?(:edit, tickets(:with_groups_1))
    refute users(:with_groups_agent_groups_restricted).can?(:edit, tickets(:with_groups_1))
    assert users(:with_groups_agent_groups_restricted).can?(:edit, tickets(:with_groups_2))
    refute users(:with_groups_agent_organization_restricted).can?(:edit, tickets(:with_groups_1))
  end

  it "tests updatable by cc" do
    refute users(:minimum_author).can?(:edit, tickets(:minimum_2))

    tickets(:minimum_2).set_collaborators = users(:minimum_author).id.to_s
    tickets(:minimum_2).will_be_saved_by(users(:minimum_agent))
    tickets(:minimum_2).save!
    assert users(:minimum_author).can?(:edit, tickets(:minimum_2))
  end

  it "tests create via get satisfaction" do
    user = users(:minimum_author)
    ticket = user.account.tickets.new
    ticket.attributes = {description: 'first comment', via_id: ViaType.GET_SATISFACTION}
    ticket.will_be_saved_by(user)
    ticket.save!
    assert ticket.reload.via?(:get_satisfaction)
    assert ticket.via_kind?(:basic)
    assert ticket.audits.last.via?(:get_satisfaction)
  end

  it "tests update via get satisfaction" do
    ticket = tickets(:minimum_1)
    ticket.add_comment(body: 'user comment')
    ticket.will_be_saved_by(users(:minimum_author), via_id: ViaType.GET_SATISFACTION)
    ticket.save!
    assert ticket.audits.last.via?(:get_satisfaction)
  end

  describe "Adding tags to a ticket" do
    before do
      @ticket = tickets(:minimum_1)
    end

    it "does not strip . from tags" do
      @ticket.current_tags = "1.2 1.3.0 foo"
      @ticket.will_be_saved_by(@ticket.submitter)
      @ticket.save!
      @ticket.reload
      assert_equal "1.2 1.3.0 foo", @ticket.current_tags
    end
  end

  describe "Serializing a ticket" do
    before do
      @account = Account.new
      @ticket = Ticket.new(account: @account)
    end

    describe "as JSON" do
      before { @ticket.to_json }

      before_should "call serialization_options" do
        @ticket.expects(:serialization_options).returns(except: [:latest_recipients])
      end
    end
  end

  describe "A ticket's serialization options" do
    before do
      @serialization_options = Ticket.new.send(:serialization_options)
    end

    it "includes comments their attachments" do
      comment_options = Comment.new.send(:serialization_options)[:include]

      assert_equal comment_options, @serialization_options[:include][:comments][:include]
    end

    it "includes comments with their via_id" do
      assert_includes @serialization_options[:include][:comments][:only], :via_id
    end

    it "includes ticket field entries" do
      assert @serialization_options[:include].key?(:ticket_field_entries)
    end

    it "includes linkings" do
      assert @serialization_options[:include].key?(:linkings)
    end

    it "includes priority_id" do
      assert_includes @serialization_options[:methods], :problem_id
    end

    it "only include the appropriate attributes on its comments association" do
      assert_equal Comment.new.send(:serialization_options)[:only],
        @serialization_options[:include][:comments][:only]
    end
  end

  describe "#set_ticket_field_entry" do
    before { @account = Account.new }

    describe "with textfields" do
      before do
        @text_field = FieldText.new(title: "Field Text", account: @account)
        @text_field.id = 12345
        @field_entry = @text_field.ticket_field_entries.new(value: "value", ticket_field_id: @text_field.id, account: @account)
        @ticket = @account.tickets.new(ticket_field_entries: [@field_entry], account: @account)
      end

      describe "with an existing entry" do
        before do
          @field_entry.stubs(:new_record?).returns(false)
        end

        it "does not record change events when nothing changed" do
          @ticket.expects(:add_domain_event).never

          @ticket.send(:set_ticket_field_entry, @text_field, "value")
          assert_equal({}, @ticket.delta_changes)
          refute @ticket.delta_changes?
        end

        it "records change events with changes" do
          @ticket.expects(:add_domain_event)

          @ticket.send(:set_ticket_field_entry, @text_field, "changed value")
          assert_equal({@text_field.id.to_s => ["value", "changed value"]}, @ticket.delta_changes)
          assert @ticket.delta_changes?
        end
      end

      describe "with a new entry" do
        it "doesn't record change events if unset" do
          @field_entry = @text_field.ticket_field_entries.new(value: "", ticket_field_id: @text_field.id, account: @account)
          @ticket = @account.tickets.new(ticket_field_entries: [@field_entry], account: @account)

          @ticket.expects(:add_domain_event).never

          @ticket.send(:set_ticket_field_entry, @text_field, "")
          assert_equal({}, @ticket.delta_changes)
          refute @ticket.delta_changes?
        end

        it "records change events" do
          @ticket.expects(:add_domain_event).once

          @ticket.send(:set_ticket_field_entry, @text_field, "changed value")
          assert_equal({@text_field.id.to_s => ["value", "changed value"]}, @ticket.delta_changes)
          assert @ticket.delta_changes?
        end
      end

      it "still records delta changes for normal attributes" do
        @ticket.status_id = 5
        assert_equal({"status_id" => [0, 5]}, @ticket.delta_changes)
      end
    end

    describe "with checkboxes" do
      before do
        @checked_field = FieldCheckbox.new(title: "Checkbox", account: @account)
        @checked_field.id = 12345
        @checked_field_entry = @checked_field.ticket_field_entries.new(value: 1, ticket_field_id: @checked_field.id)
        @unchecked_field = FieldCheckbox.new(title: "Checkbox", account: @account)
        @unchecked_field.id = 12346
        @unchecked_field_entry = @unchecked_field.ticket_field_entries.new(value: 0, ticket_field_id: @unchecked_field.id)
        @ticket = @account.tickets.new(ticket_field_entries: [@checked_field_entry, @unchecked_field_entry], account: @account)
      end

      describe "with an existing entry" do
        before do
          @checked_field_entry.stubs(:new_record?).returns(false)
          @unchecked_field_entry.stubs(:new_record?).returns(false)
        end

        it "does not record change events when nothing changed" do
          @ticket.expects(:add_domain_event).never

          @ticket.send(:set_ticket_field_entry, @checked_field, "true")
          @ticket.send(:set_ticket_field_entry, @unchecked_field, "false")
          assert_equal({}, @ticket.delta_changes)
          refute @ticket.delta_changes?
        end

        it "records change events" do
          @ticket.expects(:add_domain_event).twice

          @ticket.send(:set_ticket_field_entry, @checked_field, "false")
          @ticket.send(:set_ticket_field_entry, @unchecked_field, "true")
          assert_equal({@checked_field.id.to_s => %w[1 0], @unchecked_field.id.to_s => %w[0 1]}, @ticket.delta_changes)
          assert @ticket.delta_changes?
        end
      end

      it "stills record delta changes for normal attributes" do
        @ticket.status_id = 5
        assert_equal({"status_id" => [0, 5]}, @ticket.delta_changes)
      end
    end
  end

  describe "#ticket_field_entries=" do
    before do
      @ticket = tickets(:minimum_1)
      @field = ticket_fields(:field_text_custom)
    end

    describe "with an array of hashes" do
      it "does not raise an exception" do
        @ticket.ticket_field_entries = [{}, {}]
      end

      it "sets the ticket field value for each ticket field with the provided value" do
        @ticket.ticket_field_entries = [{ticket_field_id: @field.id, value: "Vlad was a bad programmer." }]
        entry = @ticket.ticket_field_entries.detect { |f| f.ticket_field_id == @field.id }
        assert_match(/Vlad/, entry.value)
      end
    end

    describe "with an argument that is not an array of hashes" do
      it "exhibits default Rails behaviour" do
        new_value = [@ticket.ticket_field_entries.new]
        @ticket.ticket_field_entries = new_value
        assert_equal new_value, @ticket.ticket_field_entries
      end
    end
  end

  describe '#add_domain_event' do
    let(:ticket) { tickets(:minimum_1) }

    describe 'with a valid domain event' do
      let(:event) { StatusChangedProtobufEncoder.new(ticket).to_object }

      before do
        ticket.status_id = StatusType.DELETED
        event
      end

      it 'adds the domain event to the ticket' do
        ticket.add_domain_event(event)
        assert_equal [event], ticket.domain_events
      end
    end

    describe 'with nil' do
      let(:event) { nil }

      it 'ignores the domain event' do
        ticket.add_domain_event(event)
        assert_equal [], ticket.domain_events
      end
    end

    describe 'with some other object type' do
      let(:event) { StatusChangedProtobufEncoder.new(ticket) }

      it 'adds the domain event to the ticket' do
        assert_raise ArgumentError do
          ticket.add_domain_event(event)
        end
      end
    end
  end

  describe "#problem_id" do
    before do
      @ticket = Ticket.new
    end

    it "returns the nice id of the associated problem" do
      @ticket.stubs(:problem).returns(mock(nice_id: 1505))

      assert_equal 1505, @ticket.problem_id
    end
  end

  describe "#problem_id=" do
    before do
      @ticket = tickets(:minimum_1)
    end

    describe "when a ticket with a matching nice_id exists on the account" do
      before do
        @ticket.account.tickets.stubs(:find_by_nice_id).with(123).returns(stub(nice_id: 123, id: 456789))
      end

      it "sets linked_id to the id of the matching ticket" do
        @ticket.problem_id = 123

        assert_equal 456789, @ticket.linked_id
      end
    end

    describe "when a ticket with a matching nice_id does not exist on the account" do
      it "sets linked_id to nil" do
        @ticket.problem_id = 123

        assert_nil @ticket.linked_id
      end
    end
  end

  describe "#locale_id" do
    before do
      Resque.inline = true
      available_languages = [translation_locales(:english_by_zendesk), translation_locales(:brazilian_portuguese), translation_locales(:japanese)]
      Account.any_instance.stubs(:available_languages).returns(available_languages)
    end

    after { Resque.inline = false }

    it "sets locale_id based on the ticket requester" do
      new_locale_id = translation_locales(:brazilian_portuguese).id
      user = users(:minimum_author)
      user.update_column(:locale_id, new_locale_id)
      ticket = accounts(:minimum).tickets.new(requester: user, description: 'Hello')
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!

      assert_equal user.translation_locale.id, ticket.locale_id

      user2 = users(:minimum_end_user)
      user2.update_column(:locale_id, 2)

      ticket.requester = user2
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!
      assert_equal user2.locale_id, ticket.locale_id
      assert_equal user2.translation_locale.id, ticket.translation_locale.id
    end

    it "updates locale_id when ticket requester changes their locale" do
      Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
      new_locale_id = translation_locales(:brazilian_portuguese).id
      user = users(:minimum_author)
      user.update_attribute(:locale_id, new_locale_id)
      ticket = accounts(:minimum).tickets.new(requester: user, description: 'Hello')
      ticket.assignee = users(:minimum_agent)
      ticket.status_id = StatusType.SOLVED
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!

      assert_equal user.locale_id, ticket.locale_id

      new_locale_id = translation_locales(:japanese).id
      ticket.requester.update_attributes(locale_id: new_locale_id)
      ticket.reload
      assert_equal new_locale_id, ticket.requester.locale_id
      assert_equal ticket.requester.locale_id, ticket.locale_id
    end
  end

  describe "#additional_tags=" do
    before do
      minimum_end_user = users(:minimum_end_user)
      @ticket = minimum_end_user.account.tickets.new(requester: minimum_end_user, description: 'hello')
      @ticket.will_be_saved_by(minimum_end_user)
      @ticket.additional_tags = "abc def"
      @ticket.save!
    end

    it "sets current_tags on save!" do
      assert_equal @ticket.current_tags, "abc def"
    end
  end

  describe "#external_links=" do
    before do
      @ticket = Ticket.new(account: Account.new)
    end

    it "does not process the arguments on an existing record" do
      @ticket.stubs(:new_record?).returns(false)

      @ticket.external_links = mock("arguments object that shouldn't be touched")
    end

    it "builds an ExternalLink with each attributes hash on a new record" do
      links_attributes = [{type: "JiraIssue", issue_id: "foo-43"},
                          {type: "JiraIssue", issue_id: "ddf-d"}]

      links_attributes.each do |attributes_hash|
        ExternalLink.expects(:build).with(@ticket, attributes_hash)
      end

      @ticket.external_links = links_attributes
    end
  end

  describe "Requester twitter properties for trigger conditions" do
    before do
      @ticket = tickets(:minimum_1)
    end

    describe "#requester_twitter_followers_count" do
      it "should return correct value" do
        @ticket.requester.stubs(:twitter_profile).
          returns(Channels::TwitterUserProfile.new(metadata: {followers_count: 4376 }))
        assert_equal 4376, @ticket.requester_twitter_followers_count
      end

      it "should return 0 when no profile" do
        @ticket.requester.stubs(:twitter_profile) .returns(nil)
        assert_equal 0, @ticket.requester_twitter_followers_count
      end
    end

    describe "#requester_twitter_statuses_count" do
      it "should return correct value" do
        @ticket.requester.stubs(:twitter_profile).
          returns(Channels::TwitterUserProfile.new(metadata: {statuses_count: 25 }))
        assert_equal 25, @ticket.requester_twitter_statuses_count
      end

      it "should return 0 when no profile" do
        @ticket.requester.stubs(:twitter_profile) .returns(nil)
        assert_equal 0, @ticket.requester_twitter_statuses_count
      end
    end

    describe "#requester_twitter_verified" do
      it "should return correct value" do
        @ticket.requester.stubs(:twitter_profile).returns(Channels::TwitterUserProfile.new(metadata: {verified: true }))
        assert_equal 'true', @ticket.requester_twitter_verified
      end

      it "should return nil when false" do
        @ticket.requester.stubs(:twitter_profile).returns(Channels::TwitterUserProfile.new(metadata: {verified: false }))
        assert_nil @ticket.requester_twitter_verified
      end

      it "should return nil when no profile" do
        @ticket.requester.stubs(:twitter_profile).returns(nil)
        assert_nil @ticket.requester_twitter_verified
      end
    end
  end

  describe "#for_user named scope" do
    describe "a restricted agent collaborating on a ticket" do
      before do
        @account = accounts(:with_groups)
        @account.settings.collaboration_enabled = true
        @account.settings.save!
        CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
        @user = users(:with_groups_agent_assigned_restricted)
        @ticket = tickets(:with_groups_1)
        @ticket.set_collaborators = [@user.id]
        @ticket.will_be_saved_by(@ticket.submitter)
        @ticket.save!
        assert @ticket.is_follower?(@user)
      end

      it "has limited access to that ticket" do
        assert @account.tickets.for_user(@user).find_by_id(@ticket.id)
        assert @user.limited_access?(@ticket)
      end

      describe "when the account has ticket collaboration disabled" do
        before do
          @account.settings.collaboration_enabled = false
        end

        describe "and account has CCs/Followers settings enabled" do
          before do
            CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
          end

          it "has limited access to that ticket" do
            assert @account.tickets.for_user(@user).find_by_id(@ticket.id)
            assert @user.limited_access?(@ticket)
          end
        end
      end
    end
  end

  describe "#with_best_index named scope" do
    it "uses the correct index when ordering by id" do
      assert_sql_queries(1, /SELECT  `tickets`.* FROM tickets USE INDEX\(index_tickets_on_account_id_and_nice_id\) WHERE/) do
        accounts(:minimum).tickets.with_best_index(:id).first
      end
    end

    it "uses the correct index when ordering by updated_at" do
      assert_sql_queries(1, /SELECT  `tickets`.* FROM tickets USE INDEX\(index_tickets_on_account_id_and_status_id_and_updated_at\) WHERE/) do
        accounts(:minimum).tickets.with_best_index("updated_at").first
      end
    end

    it "uses the correct index when ordering by created_at" do
      assert_sql_queries(1, /SELECT  `tickets`.* FROM tickets USE INDEX\(index_tickets_on_account_id_and_status_id_and_created_at\) WHERE/) do
        accounts(:minimum).tickets.with_best_index(:created_at).first
      end
    end

    it "uses the correct index when ordering by locale" do
      assert_sql_queries(1, /SELECT  `tickets`.* FROM tickets USE INDEX\(index_tickets_on_account_id_and_status_id_and_locale_id\) WHERE/) do
        accounts(:minimum).tickets.with_best_index("locale").first
      end
    end
  end

  describe '#Regular expression custom field' do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.account = accounts(:minimum)
      @ticket.build_requester(name: 'Sir Skisalot', email: 'ski@example.com', account: @ticket.account)
      @ticket.requester_name  = 'Mr Snowboarder'
      @ticket.requester_email = 'board@example.com'
      user = users(:minimum_agent)
      @ticket.current_user = user
      @ticket_field = ticket_fields(:field_regexp_ssn_custom)
    end

    it 'validates new values on update' do
      @ticket.fields = {@ticket_field.id.to_s => 'sd,fnsdf'}
      refute @ticket.valid?

      @ticket.fields = {@ticket_field.id.to_s => '626-79-7716'}
      assert @ticket.valid?
    end
  end

  describe 'signatures' do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.add_comment(body: 'let us see if this comment is added a signature', is_public: true)
      Brand.any_instance.stubs(:signature_template).returns("Our signature: {{agent.signature}}")
    end

    it 'adds an agent signature to a public web form comment from an agent with a signature' do
      @ticket.will_be_saved_by(users(:minimum_admin), via_id: ViaType.WEB_FORM)
      @ticket.save!
      assert @ticket.reload.comments.last.body.index(users(:minimum_admin).agent_signature(rich: false))
    end

    it 'adds an agent signature to a public comment from an agent with a signature when following up a closed ticket' do
      @ticket.will_be_saved_by(users(:minimum_admin), via_id: ViaType.CLOSED_TICKET)
      @ticket.save!
      assert @ticket.reload.comments.last.body.index(users(:minimum_admin).agent_signature(rich: false))
    end

    it 'interpolates information in an agent signature to a public web form comment from an agent with a signature with interpolations' do
      u = users(:minimum_admin)
      u.signature.update_attributes(value: '{{agent.name}}\n{{agent.email}}\n{{agent.phone}}')
      @ticket.will_be_saved_by(u, via_id: ViaType.WEB_FORM)
      @ticket.save!
      comment_text = @ticket.reload.comments.last.body
      comment_text.must_include u.clean_name
      comment_text.must_include u.email
    end

    it 'does not add an agent signature to a public email comment from an agent with a signature' do
      @ticket.will_be_saved_by(users(:minimum_admin), via_id: ViaType.MAIL)
      @ticket.save!
      refute @ticket.reload.comments.last.body.index(users(:minimum_admin).agent_signature(rich: false))
    end

    it 'does not add a signature to a comment from an agent without a signature' do
      @ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.MAIL)
      @ticket.save!
      assert_equal "let us see if this comment is added a signature", @ticket.reload.comments.last.body
    end

    [ViaType.TWITTER_FAVORITE, ViaType.TWITTER_DM, ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE].each do |type|
      describe "when channelling back via #{type}" do
        it 'does not add a signature if channel_back is present' do
          @ticket.via_id = type
          @ticket.comment.expects(:should_create_channel_back_event?).returns(true)
          @ticket.expects(:create_channel_back_event)
          @ticket.will_be_saved_by(users(:minimum_admin), via_id: ViaType.WEB_FORM)
          @ticket.save!
          refute @ticket.reload.comments.last.body.index(users(:minimum_admin).agent_signature(rich: false))
        end

        it 'adds a signature if channel_back is not present' do
          @ticket.via_id = type
          @ticket.comment.expects(:should_create_channel_back_event?).twice.returns(false)
          @ticket.will_be_saved_by(users(:minimum_admin), via_id: ViaType.WEB_FORM)
          @ticket.save!
          assert @ticket.reload.comments.last.body.index(users(:minimum_admin).agent_signature(rich: false))
        end
      end
    end
  end

  describe "A deleted ticket" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.WEB_SERVICE)
      @ticket_id = @ticket.id
    end

    it "is #deleted?" do
      @ticket.soft_delete!
      assert @ticket.deleted?
    end

    it "is deleted if it was closed" do
      @ticket.update_attributes! status_id: StatusType.CLOSED
      @ticket.will_be_saved_by(@ticket.submitter)
      @ticket.soft_delete!
      assert @ticket.deleted?
      assert_nil Ticket.find_by_id(@ticket_id)
    end

    it "has a status_id of StatusType.DELETED" do
      @ticket.soft_delete!
      assert_equal StatusType.DELETED, @ticket.status_id
    end

    it "is excluded from default named scopes" do
      assert_difference 'Ticket.count(:all)', -1 do
        assert_difference '@ticket.account.tickets.count(:all)', -1 do
          @ticket.soft_delete!
        end
      end
    end

    it "does not *actually* be deleted" do
      refute_difference 'Ticket.count_by_sql "SELECT COUNT(*) FROM tickets"' do
        @ticket.soft_delete!
      end
    end

    it "creates a change event" do
      assert_difference 'Event.where(:ticket_id => @ticket.id).count(:all)', 2 do
        @ticket.soft_delete!
      end
      assert @ticket.events.last.is_a?(Change)
      assert_equal StatusType.DELETED, @ticket.events.last.value.to_i
    end

    it "does not invoke any triggers" do
      Zendesk::Rules::Trigger::Execution.expects(:execute).never
      @ticket.soft_delete!
    end

    it "does not change the solved_at timestamp" do
      @ticket.update_column(:solved_at, Time.now)
      old = @ticket.solved_at
      Timecop.travel(30.minutes.from_now) do
        @ticket.soft_delete!
        assert_equal old, @ticket.solved_at
      end
    end

    it "does not change the solved_at timestamp (when transitioning from SOLVED)" do
      @ticket.status_id = StatusType.SOLVED
      @ticket.add_comment(body: 'Alpine Static', is_public: true)
      @ticket.save!
      old = @ticket.solved_at
      assert_not_nil @ticket.solved_at
      Timecop.travel(30.minutes.from_now) do
        @ticket.will_be_saved_by(@ticket.submitter)
        @ticket.soft_delete!
        new = @ticket.solved_at
        assert_equal old, new
      end
    end

    it "records the user and via type of the deletion" do
      @ticket.soft_delete!
      assert_equal ViaType.WEB_SERVICE, @ticket.audits.last.via_id
      assert_equal users(:minimum_agent).id, @ticket.audits.last.author_id
    end

    it "clears the ticket from the recent ticket cache" do
      @user = users(:minimum_agent)

      manager = stub("recent_ticket_manager")
      manager.expects(:remove_ticket).with(@ticket)

      Zendesk::Tickets::RecentTicketManager.expects(:new).with(@user).returns(manager)

      @ticket.soft_delete!
    end

    it "clears the archived ticket from the recent ticket cache" do
      @user = users(:minimum_agent)
      archive_and_delete(@ticket)
      @archived_ticket = Ticket.find(@ticket.id)
      @archived_ticket.will_be_saved_by(@user)

      manager = stub("recent_ticket_manager")
      manager.expects(:remove_ticket).with(@archived_ticket)

      Zendesk::Tickets::RecentTicketManager.expects(:new).with(@user).returns(manager)

      @archived_ticket.soft_delete!
    end

    it "returns a soft_deletion_event" do
      @ticket.soft_delete!
      event = @ticket.soft_deletion_event
      assert_equal "Change", event.type
      assert_equal StatusType.DELETED, event.value.to_i
      assert_equal "status_id", event.value_reference
    end

    it "returns the correct previous_status" do
      @ticket.update_column(:status_id, StatusType.PENDING)
      @ticket.soft_delete!
      status = @ticket.previous_status
      assert_equal StatusType.PENDING, status
    end

    describe "indexes when querying events table" do
      it "uses the correct index when doing soft_deletion_event" do
        @ticket.soft_delete!
        assert_sql_queries(1, /SELECT  `events`.* FROM events USE INDEX\(index_events_on_ticket_id_and_type\) WHERE/) do
          @ticket.soft_deletion_event
        end
      end

      it "uses the correct index when doing previous_status" do
        @ticket.soft_delete!
        assert_sql_queries(1, /SELECT  `events`.* FROM events USE INDEX\(index_events_on_ticket_id_and_type\) WHERE/) do
          @ticket.previous_status
        end
      end
    end
  end

  describe "#followup_source" do
    before do
      @followup_ticket = tickets(:minimum_5)
      @source_ticket   = tickets(:minimum_4)
      [@followup_ticket, @source_ticket].map do |t|
        t.will_be_saved_by(t.account.owner)
        t.save
      end
      TicketLink.new(source: @source_ticket, target: @followup_ticket, link_type: TicketLink::FOLLOWUP).save!
    end

    it "returns the source from the source_links" do
      assert_equal @source_ticket, @followup_ticket.followup_source
    end

    describe "an archived ticket" do
      before { archive_and_delete(@source_ticket) }

      it "works" do
        assert_equal @source_ticket.id, @followup_ticket.followup_source.id
      end
    end
  end

  describe "#set_followup_source" do
    let(:source_ticket)   { tickets(:minimum_5) }
    let(:followup_ticket) { source_ticket.account.tickets.new }

    before do
      followup_ticket.via_followup_source_id = source_ticket.nice_id
    end

    it "adds the source to the followup_sources" do
      followup_ticket.set_followup_source(users(:minimum_end_user))
      assert_equal source_ticket, followup_ticket.source_links.first.source
    end

    it "does not add the source to the followup_sources for current_user without access" do
      followup_ticket.set_followup_source(users(:minimum_author))
      assert_nil followup_ticket.source_links.first
    end

    it "updates the cached followup_source value" do
      followup_ticket.source_links.expects(:present?).never
      followup_ticket.set_followup_source(users(:minimum_end_user))
      assert_equal source_ticket, followup_ticket.source_links.first.source
    end
  end

  describe "#set_followup_source_ticket" do
    let(:source_ticket)        { tickets(:minimum_5) }
    let!(:default_ticket_form) { ticket_forms(:default_ticket_form) }
    let(:ticket_form1)         { FactoryBot.create(:ticket_form, account: source_ticket.account) }
    let(:ticket_form2)         { FactoryBot.create(:ticket_form, account: source_ticket.account) }
    let(:followup_ticket)      { source_ticket.account.tickets.new(ticket_form: nil) }

    before do
      followup_ticket.via_followup_source_id = source_ticket.nice_id
    end

    describe "ticket_form" do
      before do
        source_ticket.ticket_form = ticket_form1
      end

      it "use the source's form if the source is set" do
        followup_ticket.set_followup_source_ticket(source_ticket)
        assert_equal followup_ticket.ticket_form_id, source_ticket.ticket_form_id
      end

      it "use the targets form if the target is set" do
        followup_ticket.ticket_form = ticket_form2
        followup_ticket.set_followup_source_ticket(source_ticket)
        assert_equal followup_ticket.ticket_form_id, ticket_form2.id
      end
    end

    describe "tags" do
      it "doesn't call #set_tags if it already has tags" do
        followup_ticket.current_tags = "foobar"
        followup_ticket.expects(:set_tags=).never
        followup_ticket.set_followup_source_ticket(source_ticket)
      end

      it "sets the followup tags to source tickets current tags" do
        followup_ticket.set_followup_source_ticket(source_ticket)

        followup_ticket.current_tags.must_equal source_ticket.current_tags
      end
    end

    describe "brand" do
      describe "source ticket brand is inactive" do
        before do
          source_ticket.brand.stubs(:active?).returns(false)
        end

        it "doesn't set the brand" do
          followup_ticket.expects(:brand=).never
          followup_ticket.set_followup_source_ticket(source_ticket)
        end
      end

      describe "source ticket brand is active" do
        it "doesn't set the brand if the new ticket already has one" do
          expected = source_ticket.account.brands.last
          followup_ticket.brand = expected
          source_ticket.brand = Brand.new
          followup_ticket.set_followup_source_ticket(source_ticket)

          followup_ticket.brand.must_equal expected
        end

        it "sets the brand of the new ticket if it isn't set" do
          followup_ticket.set_followup_source_ticket(source_ticket)

          followup_ticket.brand.must_equal source_ticket.brand
        end
      end
    end

    describe "ticket form" do
      let(:non_active_form) do
        params = {
          ticket_form: { name: "non default form", display_name: "non default form", position: 2, default: false, end_user_visible: false }
        } # active & deleted_at can't be set here
        Zendesk::TicketForms::Initializer.new(source_ticket.account, params).ticket_form
      end

      let(:active_form) do
        params = {
          ticket_form: { name: "default wombat", display_name: "default wombat", position: 1, default: true, active: true, deleted_at: nil, end_user_visible: true }
        }
        Zendesk::TicketForms::Initializer.new(source_ticket.account, params).ticket_form
      end

      before do
        form.save!
        non_active_form.soft_delete!
        source_ticket.update_column(:ticket_form_id, form.id)
      end

      describe "source ticket form is inactive" do
        let(:form) { non_active_form }

        it "sets the deleted tickets form" do
          source_ticket.deleted_ticket_form.must_equal non_active_form
          source_ticket.ticket_form.must_be_nil
        end
      end

      describe "source ticket form is inactive" do
        let(:form) { non_active_form }
        it "doesn't set the new tickets form" do
          followup_ticket.expects(:ticket_form=).never

          followup_ticket.set_followup_source_ticket(source_ticket)
          followup_ticket.ticket_form.must_be_nil
        end
      end

      describe "source ticket form is active" do
        let(:form) { active_form }

        it "sets the new ticket form" do
          followup_ticket.ticket_form.must_be_nil
          followup_ticket.set_followup_source_ticket(source_ticket)
          followup_ticket.ticket_form.must_equal form
        end
      end
    end

    describe "ticket fields" do
      before do
        @fields = source_ticket.account.ticket_fields.custom.active
      end

      it "copies the values from all ticket fields" do
        followup_ticket.set_followup_source_ticket(source_ticket)
        expected_values = @fields.map { |f| f.value(source_ticket) }
        actual_values = @fields.map { |f| f.value(followup_ticket) }
        actual_values.must_equal expected_values
      end

      it "records delta changes for set fields" do
        followup_ticket.set_followup_source_ticket(source_ticket)
        change_ids = @fields.select { |f| !f.value(source_ticket).blank? }.map { |i| i.id.to_s }
        assert change_ids.all? { |i| followup_ticket.delta_changes.include? i }
      end

      describe_with_arturo_enabled :followup_only_uses_source_fields do
        it "only uses ticket fields on source ticket" do
          followup_ticket.set_followup_source_ticket(source_ticket)
          assert_equal 1, followup_ticket.ticket_field_entries.length
          assert followup_ticket.ticket_field_entries[0].ticket_field.is_active
          assert_not_includes TicketField::SYSTEM_FIELDS, followup_ticket.ticket_field_entries[0].ticket_field.type
        end
      end
      describe_with_arturo_disabled :followup_only_uses_source_fields do
        it "uses all active ticket fields on account" do
          followup_ticket.set_followup_source_ticket(source_ticket)
          assert_equal 10, followup_ticket.ticket_field_entries.length
        end
      end
    end

    describe "when the account has ticket fields created by Time Tracking app" do
      let(:custom_fields) { source_ticket.account.ticket_fields.custom.active }
      let(:field_created_by_app) { custom_fields.where(type: FieldInteger).first }
      let(:old_value) { "5" }

      before do
        # Let's assume all integer fields were created by TimeTracking app
        # to make things easier to test
        FieldInteger.any_instance.stubs(:created_by_time_tracking_app?).returns(true)
        source_ticket.ticket_field_entries.create!(
          ticket_field: field_created_by_app,
          account: source_ticket.account,
          value: old_value
        )
      end

      describe_with_arturo_enabled :skip_time_tracking_fields_on_followups do
        it "doesn't copy the values of the fields created by TimeTracking app" do
          followup_ticket.set_followup_source_ticket(source_ticket)

          assert_equal old_value, field_created_by_app.value(source_ticket)
          assert_equal "", field_created_by_app.value(followup_ticket)
        end

        it "allows setting values for those fields while we're building a followup" do
          followup_ticket.set_followup_source_ticket(source_ticket)
          followup_ticket.fields = { field_created_by_app.id => "8" }

          assert_equal "8", field_created_by_app.value(followup_ticket)
        end
      end

      describe_with_arturo_disabled :skip_time_tracking_fields_on_followups do
        it "copies the values of the fields created by TimeTracking app" do
          followup_ticket.set_followup_source_ticket(source_ticket)

          assert_equal old_value, field_created_by_app.value(source_ticket)
          assert_equal old_value, field_created_by_app.value(followup_ticket)
        end

        it "allows setting values for those fields while we're building a followup" do
          followup_ticket.set_followup_source_ticket(source_ticket)
          followup_ticket.fields = { field_created_by_app.id => "8" }

          assert_equal "8", field_created_by_app.value(followup_ticket)
        end
      end
    end
  end

  describe 'Given a closed ticket' do
    before do
      @user = users(:minimum_admin)
      @collaborator = users(:minimum_end_user)
      @brand = FactoryBot.create(:brand)

      @ticket = tickets(:minimum_1)
      @ticket.set_collaborators = @collaborator.id.to_s
      @ticket.status_id = StatusType.CLOSED
      @ticket.brand = @brand
      @ticket.will_be_saved_by(@user)
      @ticket.save!

      assert @ticket.closed?
    end

    it 'is able to be forced reopen' do
      @ticket.will_be_saved_by(@user)
      @ticket.force_reopen!
      assert @ticket.status?(:open)
    end

    describe 'with CCed user' do
      before do
        assert_equal 1, @ticket.collaborators.reload.size
      end

      describe 'when creating followup from that ticket' do
        before do
          @followup = @ticket.account.tickets.new(requester: @ticket.requester, description: 'this is a followup to your previous ticket')
          @followup.via_followup_source_id = @ticket.nice_id
          @followup.will_be_saved_by(@collaborator, via_id: ViaType.CLOSED_TICKET)
          @followup.set_followup_source(@collaborator)
          @followup.save!
        end

        it 'has the collaborator as comment author' do
          assert_equal @collaborator.id, @followup.comments.first.author_id
        end
      end
    end

    describe 'with tags' do
      before do
        assert !@ticket.taggings.empty?
      end

      describe 'when creating a followup from that ticket' do
        before do
          @followup = @ticket.account.tickets.new
          @followup.via_followup_source_id = @ticket.nice_id
          @followup.set_followup_source(@user)
          @followup.send(:update_taggings)
        end

        it 'has the same tags as the original ticket' do
          assert_equal(@ticket.tag_array, @followup.tag_array)
          assert_equal(@ticket.current_tags, @followup.current_tags)
        end
      end
    end

    describe 'with valid brand' do
      before do
        assert @ticket.brand_id.present?
      end

      describe 'when creating a followup from that ticket' do
        before do
          @followup = @ticket.account.tickets.new(requester: @ticket.requester, description: 'this is a followup to your previous ticket')
          @followup.via_followup_source_id = @ticket.nice_id
          @followup.set_followup_source(@user)
          @followup.will_be_saved_by(@collaborator, via_id: ViaType.CLOSED_TICKET)
          @followup.save!
        end

        it 'has the same brand as the original ticket' do
          assert_equal(@ticket.brand_id, @followup.brand_id)
        end
      end
    end

    describe 'with invalid brand' do
      before do
        @default_brand = FactoryBot.create(:brand)
        @ticket.account.update_attributes default_brand: @default_brand

        @brand.update_attributes(active: false)
      end

      describe 'when creating a followup from that ticket' do
        before do
          @followup = Ticket.new(account: @ticket.account, requester: @ticket.requester, description: 'this is a followup to your previous ticket')
          @followup.via_followup_source_id = @ticket.nice_id
          @followup.set_followup_source(@user)
          @followup.will_be_saved_by(@collaborator, via_id: ViaType.CLOSED_TICKET)
          @followup.save!
        end

        it 'does not have the same brand as the original ticket, should have the default brand' do
          assert_not_equal(@ticket.brand_id, @followup.brand_id)
          assert_equal(@ticket.account.default_brand_id, @followup.brand_id)
        end
      end
    end

    describe 'with a custom field value' do
      before do
        assert !@ticket.account.ticket_fields.custom.active.empty?
        @custom_field = @ticket.account.ticket_fields.custom.active.detect do |field|
          field.is_a?(FieldTagger)
        end

        @ticket.ticket_field_entries.create!(ticket_field: @custom_field,
                                             account: @ticket.account, value: 'foo')
        assert_not_nil(@custom_field.value(@ticket))
      end

      describe 'when creating a followup from that ticket' do
        before do
          @followup = @ticket.account.tickets.new
          @followup.via_followup_source_id = @ticket.nice_id
          @followup.set_followup_source(@user)
        end

        it 'has the same custom field value as the original ticket' do
          assert_equal(@custom_field.value(@ticket), @custom_field.value(@followup))
        end
      end
    end
  end

  it 'creating an inbound sharing ticket, subsequently updated with a trigger macro reference, should share the macro update to the inbound sharing account' do
    Timecop.freeze
    # Create macro to apply
    Account.any_instance.stubs(:has_rules_can_reference_macros?).returns(true)
    definition = Definition.new
    definition.actions.push(DefinitionItem.new('comment_value', nil, 'Comment by {{current_user.name}}'))
    definition.actions.push(DefinitionItem.new('status_id', nil, StatusType.SOLVED))
    macro = accounts(:minimum).macros.create!(title: 'macro test', definition: definition, is_active: true)

    # Create trigger that references macro
    definition = Definition.new
    definition.conditions_all.push(DefinitionItem.new('update_type', nil, 'Create'))
    definition.actions.push(DefinitionItem.new('macro_id_after', nil, macro.id))
    trigger = accounts(:minimum).triggers.create!(title: 'trigger test', definition: definition, position: 1, is_active: true)

    # Create inbound agreement
    agreement = FactoryBot.create(:agreement, account: accounts(:minimum), direction: :in, status: :accepted)

    # Create new inbound ticket
    ticket = trigger.account.tickets.new(requester: users(:minimum_admin), assignee: users(:minimum_admin), group: groups(:minimum_group), description: 'hejsa')
    ticket.will_be_saved_by(ticket.assignee)
    ticket.shared_tickets.build(agreement: agreement)

    assert_difference "Resque.jobs.size", +1 do
      ticket.save!
    end

    assert_equal("ApplyMacrosJob", Resque.jobs.last[:class])
    assert_equal([ticket.account_id, ticket.id, [macro.id]], Resque.jobs.last[:args][0..-2])

    assert_difference "Resque.jobs.size", +1 do
      ApplyMacrosJob.perform(*Resque.jobs.last[:args])
    end
  end

  describe 'sharing a ticket' do
    before do
      @ticket = tickets(:minimum_1)
      @user = users(:minimum_admin)
      @agreement = FactoryBot.create(:agreement,
        account: @ticket.account,
        direction: :out,
        status: :accepted)

      @ticket.reload
      assert @ticket.account.sharing_agreements.include?(@agreement)

      @ticket.agreement_id = @agreement.id
      @ticket.will_be_saved_by(@user)
    end

    it 'creates a ticket sharing event after save' do
      @ticket.save!
      assert event = @ticket.audits.last.events.detect { |e| e.is_a?(TicketSharingEvent) }
      assert_equal event.ticket, @ticket
    end

    it 'does not create an event if the ticket is already shared with that partner' do
      @ticket.shared_tickets.create!(agreement_id: @agreement.id)

      @ticket.agreement_id = @agreement.id
      refute_difference 'TicketSharingEvent.count(:all)' do
        @ticket.save!
      end
    end

    it "does not enqueue multiple sharing jobs when triggered twice by saving within a save by a triggered macro" do
      Timecop.freeze(10.minutes.ago) do # bypass the update enqueue delay
        @shared_ticket = @ticket.shared_tickets.create!(agreement_id: @agreement.id)
      end
      @ticket.save!
      # Turn on bit
      Account.any_instance.stubs(:has_rules_can_reference_macros?).returns(true)

      # Create macro to apply
      definition = Definition.new
      definition.actions.push(DefinitionItem.new('comment_value', nil, 'Comment by {{current_user.name}}'))
      @macro = @ticket.account.macros.create!(title: 'macro test', definition: definition, is_active: true)

      # Create trigger that references macro
      definition = Definition.new
      definition.conditions_all.push(DefinitionItem.new('update_type', nil, 'Change'))
      definition.actions.push(DefinitionItem.new('macro_id_after', nil, @macro.id))
      @trigger = @ticket.account.triggers.create!(title: 'trigger test', definition: definition, position: 1, is_active: true)

      @ticket.subject = "Shut it down!  Shut it down forever!"
      @ticket.will_be_saved_by(@user)

      assert_difference "Resque.jobs.size", + 2 do
        @ticket.save!
      end

      assert_equal("Sharing::TicketJob", Resque.jobs.last[:class])
      assert_equal([@ticket.account.id, :update_partner, @shared_ticket.id], Resque.jobs.last[:args][0..2])
    end

    it 'does not create an event if the select box is left blank' do
      @ticket.agreement_id = ''
      refute_difference 'TicketSharingEvent.count(:all)' do
        @ticket.save!
      end
    end

    it 'does not create an event if the agreement_id is invalid' do
      invalid_agreement_id = @agreement.id + 1
      refute @ticket.account.sharing_agreements.find_by_id(invalid_agreement_id)

      @ticket.agreement_id = invalid_agreement_id
      refute_difference 'TicketSharingEvent.count(:all)' do
        @ticket.save!
      end
    end

    it 'does not create an event if the agreement is inactive' do
      @agreement = FactoryBot.create(:agreement,
        account: @ticket.account,
        direction: :out,
        status: :inactive)
      @ticket.agreement_id = @agreement.id
      refute_difference 'TicketSharingEvent.count(:all)' do
        @ticket.save!
      end
    end

    it 'coerces a string agreement_id properly' do
      @ticket.agreement_id = @agreement.id.to_s
      @ticket.save!
      event = @ticket.audits.last.events.detect { |e| e.is_a?(TicketSharingEvent) }
      assert_equal event.ticket, @ticket
    end
  end

  describe 'Given an inbound agreement that disallows public comments' do
    before do
      @agreement = FactoryBot.create(:agreement,
        direction: :in,
        allows_public_comments: false)

      @ticket = tickets(:minimum_1)
      @ticket.shared_tickets.create!(agreement: @agreement)
    end

    it 'does not allow users to toggle comment visibility' do
      refute @ticket.allows_comment_visibility_toggle?
    end
  end

  describe 'Marking a ticket as spam' do
    before do
      @ticket = tickets(:minimum_1)
      @user   = users(:minimum_admin)
      refute @ticket.deleted?
      refute @ticket.requester.suspended?
    end

    it 'softs delete the ticket' do
      @ticket.mark_as_spam_by!(@user)
      assert @ticket.deleted?
    end

    describe 'when the requester is an end user' do
      it 'suspends the requester' do
        @ticket.mark_as_spam_by!(@user)
        requester = User.find(@ticket.requester.id) # reload
        assert requester.suspended?
      end
    end

    describe 'when the requester is an agent' do
      before { @ticket.requester.stubs(is_agent?: true) }

      it 'does not suspend the requester' do
        @ticket.mark_as_spam_by!(@user)
        requester = User.find(@ticket.requester.id) # reload
        refute requester.suspended?
      end
    end

    it 'publishes domain events' do
      domain_event_publisher = FakeEscKafkaMessage.new
      @ticket.domain_event_publisher = domain_event_publisher
      @ticket.mark_as_spam_by!(@user)

      events = domain_event_publisher.events.map do |event|
        proto = event.fetch(:value)
        ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(proto)
      end
      assert_equal [:marked_as_spam, :ticket_soft_deleted, :status_changed], events.map(&:event)
    end

    describe_with_arturo_enabled :disable_suspending_email_spam_requesters do
      [true, false].each do |user_edit_permission|
        describe "when it is #{user_edit_permission} that the user can edit the requester" do
          before { @user.stubs(:can?).with(:edit, @ticket.requester).returns(user_edit_permission) }

          it 'soft deletes the ticket' do
            @ticket.mark_as_spam_by!(@user)
            assert @ticket.deleted?
          end

          it 'does not suspend the requester' do
            @ticket.mark_as_spam_by!(@user)
            requester = User.find(@ticket.requester.id) # reload
            refute requester.suspended?
          end

          it 'returns a truthy value upon successful ticket deletion' do
            assert @ticket.mark_as_spam_by!(@user)
          end
        end
      end

      describe 'when the user cannot edit the requester and is not an agent' do
        before do
          @user.stubs(:can?).with(:edit, @ticket.requester).returns(false)
          @user.stubs(:is_agent?).returns(false)
        end

        it 'does not soft delete the ticket' do
          @ticket.mark_as_spam_by!(@user)
          refute @ticket.deleted?
        end

        it 'does not suspend the requester' do
          @ticket.mark_as_spam_by!(@user)
          requester = User.find(@ticket.requester.id) # reload
          refute requester.suspended?
        end

        it 'returns a false-y value upon successful ticket deletion' do
          refute @ticket.mark_as_spam_by!(@user)
        end
      end
    end

    it 'marks the associated email as spam' do
      email = InboundEmail.create!(account: @ticket.account, ticket: @ticket, from: @ticket.requester.email, message_id: '123@example.com')
      refute email.spam?
      @ticket.mark_as_spam_by!(@user)
      assert email.reload.spam?
    end

    it 'reports the misclassification to Cloudmark and Rspamd when there is an analyzable email' do
      author = users(:minimum_end_user)
      ticket = author.account.tickets.new(requester: author, description: 'hejsa')
      ticket.will_be_saved_by(author)
      ticket.recipient = 'test@support.zendesk.com'
      ticket.audit.metadata[:system][:raw_email_identifier] = '1/spam.eml'
      ticket.save!

      CloudmarkFeedbackJob.expects(:report_spam)
      RspamdFeedbackJob.expects(:report_spam)
      ticket.mark_as_spam_by!(@user)
    end

    it 'does not report the misclassification to Cloudmark and Rspamd when there is no analyzable email' do
      assert_equal({}, @ticket.audits.first.metadata[:system])
      CloudmarkFeedbackJob.expects(:report_spam).never
      RspamdFeedbackJob.expects(:report_spam).never
      @ticket.mark_as_spam_by!(@user)
    end

    describe 'when in the Rails development environment' do
      before { Rails.env.stubs(:development?).returns(true) }

      it 'does not report the misclassification to Cloudmark or Rspamd' do
        CloudmarkFeedbackJob.expects(:report_spam).never
        RspamdFeedbackJob.expects(:report_spam).never
        @ticket.mark_as_spam_by!(@user)
      end
    end

    describe_with_arturo_disabled :email_rspamd_training_enabled do
      it 'does not report Rspamd Feedback if arturo is disabled' do
        RspamdFeedbackJob.expects(:report_spam).never
        @ticket.mark_as_spam_by!(@user)
      end
    end

    describe "when the requester is the actor" do
      before do
        @ticket.requester = @user
        @ticket.will_be_saved_by(@user)
        @ticket.save!
      end

      it "cannot be marked as spam" do
        refute @ticket.mark_as_spam_by!(@user)
      end
    end

    describe "when the requester is a shared/foreign user" do
      before do
        @requester = @ticket.requester
        @requester.foreign = true
        @requester.save!
      end

      it "returns false" do
        refute @ticket.mark_as_spam_by!(@user)
      end

      it "does not delete the ticket" do
        @ticket.mark_as_spam_by!(@user)
        refute @ticket.reload.deleted?
      end

      it "does not suspend the user" do
        @ticket.mark_as_spam_by!(@user)
        refute @ticket.reload.requester.suspended?
      end
    end

    describe "when the requester represents a Facebook Page" do
      before do
        @page = facebook_pages(:minimum_facebook_page_1)
        @page.activate; @page.save
        @ticket.requester = users(:minimum_end_user)
        @ticket.requester.identities.facebook.first.update_column(:value, @page.graph_object_id)
        assert @user.can?(:edit, @ticket.requester)
      end

      it "returns false" do
        refute @ticket.mark_as_spam_by!(@user)
      end

      it "does not delete the ticket" do
        @ticket.mark_as_spam_by!(@user)
        refute @ticket.reload.deleted?
      end

      it "does not suspend the user" do
        @ticket.mark_as_spam_by!(@user)
        refute @ticket.reload.requester.suspended?
      end
    end

    describe 'recording metrics about reply comments on tickets marked as spam' do
      def assert_logs_mark_as_spam_replies_info(ticket_id:, user_id:, reply_comment_types:)
        Rails.logger.expects(:info).with(
          "Ticket with more than one comment marked as spam: " \
          "ticket_id: #{ticket_id}, " \
          "spam-marking user_id: #{user_id}, " \
          "reply comment types: [#{reply_comment_types.join(", ")}]"
        ).once
      end

      def refute_logs_mark_as_spam_replies_info(ticket_id:, user_id:, reply_comment_types:)
        Rails.logger.expects(:info).with(
          "Ticket with more than one comment marked as spam: " \
          "ticket_id: #{ticket_id}, " \
          "spam-marking user_id: #{user_id}, " \
          "reply comment types: [#{reply_comment_types.join(", ")}]"
        ).never
      end

      def create_reply_comment(author_is_agent:, is_public:)
        @ticket.comments.create(
          account: @ticket.account,
          audit: Audit.new(ticket: @ticket),
          ticket: @ticket,
          body: 'I am a comment on an existing ticket',
          value_previous: nil,
          value_reference: nil,
          author: author_is_agent ? users(:minimum_agent) : users(:minimum_end_user),
          is_public: is_public
        )
      end

      before do
        Rails.logger.stubs(:info)
        statsd_client.stubs(:increment)

        @ticket = accounts(:minimum).tickets.create! do |t|
          t.description = 'I will be marked as spam'
          t.via_id = ViaType.MAIL
          t.requester = users(:minimum_end_user)
          t.will_be_saved_by(t.requester)
        end
      end

      describe_with_arturo_enabled :email_record_spam_marking_reply_stats do
        describe 'when there is a public agent reply on the ticket' do
          before { create_reply_comment(author_is_agent: true, is_public: true) }

          it 'logs and records metrics about the public agent reply' do
            statsd_client.expects(:increment).with('mark_as_spam_by', tags: ['reply_type:public_agent_reply']).once
            assert_logs_mark_as_spam_replies_info(
              ticket_id: @ticket.id,
              user_id: @user.id,
              reply_comment_types: ['public_agent_reply']
            )

            @ticket.mark_as_spam_by!(@user)
          end
        end

        describe 'when there is a private agent reply on the ticket' do
          before { create_reply_comment(author_is_agent: true, is_public: false) }

          it 'logs and records metrics about the public agent reply' do
            statsd_client.expects(:increment).with('mark_as_spam_by', tags: ['reply_type:private_agent_reply']).once
            assert_logs_mark_as_spam_replies_info(
              ticket_id: @ticket.id,
              user_id: @user.id,
              reply_comment_types: ['private_agent_reply']
            )

            @ticket.mark_as_spam_by!(@user)
          end
        end

        describe 'when there is an end user reply on the ticket' do
          before { create_reply_comment(author_is_agent: false, is_public: true) }

          it 'logs and records metrics about the end user reply' do
            statsd_client.expects(:increment).with('mark_as_spam_by', tags: ['reply_type:end_user_reply']).once
            assert_logs_mark_as_spam_replies_info(
              ticket_id: @ticket.id,
              user_id: @user.id,
              reply_comment_types: ['end_user_reply']
            )

            @ticket.mark_as_spam_by!(@user)
          end
        end

        describe 'when there are multiple types of relevant reply comments on the ticket' do
          before do
            create_reply_comment(author_is_agent: true, is_public: true)
            create_reply_comment(author_is_agent: true, is_public: false)
            create_reply_comment(author_is_agent: false, is_public: true)
          end

          it 'logs and records metrics about all types of relevant reply comments' do
            statsd_client.expects(:increment).with(
              'mark_as_spam_by',
              tags: [
                'reply_type:public_agent_reply',
                'reply_type:private_agent_reply',
                'reply_type:end_user_reply'
              ]
            )
            assert_logs_mark_as_spam_replies_info(
              ticket_id: @ticket.id,
              user_id: @user.id,
              reply_comment_types: [
                'public_agent_reply',
                'private_agent_reply',
                'end_user_reply'
              ]
            )

            @ticket.mark_as_spam_by!(@user)
          end
        end

        describe 'when there are more than 25 reply comments on the ticket' do
          describe 'when the 26th reply comment on the ticket is the only public agent reply' do
            before do
              25.times { create_reply_comment(author_is_agent: false, is_public: true) }
              create_reply_comment(author_is_agent: true, is_public: true)
            end

            it 'does not load more than 25 reply comments into memory and does not log and record metrics about the public agent reply' do
              statsd_client.expects(:increment).with('mark_as_spam_by', tags: ['reply_type:end_user_reply']).once
              assert_logs_mark_as_spam_replies_info(
                ticket_id: @ticket.id,
                user_id: @user.id,
                reply_comment_types: ['end_user_reply']
              )

              statsd_client.expects(:increment).with('mark_as_spam_by', tags: ['reply_type:public_agent_reply']).never
              refute_logs_mark_as_spam_replies_info(
                ticket_id: @ticket.id,
                user_id: @user.id,
                reply_comment_types: ['public_agent_reply']
              )

              @ticket.mark_as_spam_by!(@user)
            end
          end
        end
      end

      describe_with_arturo_disabled :email_record_spam_marking_reply_stats do
        describe 'when there is a public agent reply on the ticket' do
          before { create_reply_comment(author_is_agent: true, is_public: true) }

          it 'does not log or record metrics about the public agent reply' do
            statsd_client.expects(:increment).with('mark_as_spam_by', tags: ['reply_type:public_agent_reply']).never
            refute_logs_mark_as_spam_replies_info(
              ticket_id: @ticket.id,
              user_id: @user.id,
              reply_comment_types: ['public_agent_reply']
            )

            @ticket.mark_as_spam_by!(@user)
          end
        end

        describe 'when there is a private agent reply on the ticket' do
          before { create_reply_comment(author_is_agent: true, is_public: false) }

          it 'does not log or record metrics about the public agent reply' do
            statsd_client.expects(:increment).with('mark_as_spam_by', tags: ['reply_type:private_agent_reply']).never
            refute_logs_mark_as_spam_replies_info(
              ticket_id: @ticket.id,
              user_id: @user.id,
              reply_comment_types: ['private_agent_reply']
            )

            @ticket.mark_as_spam_by!(@user)
          end
        end

        describe 'when there is an end user reply on the ticket' do
          before { create_reply_comment(author_is_agent: false, is_public: true) }

          it 'does not log or record metrics about the end user reply' do
            statsd_client.expects(:increment).with('mark_as_spam_by', tags: ['reply_type:end_user_reply']).never
            refute_logs_mark_as_spam_replies_info(
              ticket_id: @ticket.id,
              user_id: @user.id,
              reply_comment_types: ['end_user_reply']
            )

            @ticket.mark_as_spam_by!(@user)
          end
        end

        describe 'when there are multiple types of relevant reply comments on the ticket' do
          before do
            create_reply_comment(author_is_agent: true, is_public: true)
            create_reply_comment(author_is_agent: true, is_public: false)
            create_reply_comment(author_is_agent: false, is_public: true)
          end

          it 'does not log or record metrics about any of the relevant reply comments' do
            statsd_client.expects(:increment).with(
              'mark_as_spam_by',
              tags: [
                'reply_type:public_agent_reply',
                'reply_type:private_agent_reply',
                'reply_type:end_user_reply'
              ]
            ).never
            refute_logs_mark_as_spam_replies_info(
              ticket_id: @ticket.id,
              user_id: @user.id,
              reply_comment_types: [
                'public_agent_reply',
                'private_agent_reply',
                'end_user_reply'
              ]
            )

            @ticket.mark_as_spam_by!(@user)
          end
        end
      end
    end
  end

  it 'provides a #has_incidents method' do
    ticket = tickets(:minimum_1)
    ticket2 = tickets(:minimum_2)
    user = users(:minimum_admin)

    refute ticket.has_incidents

    assert(ticket.update_columns(ticket_type_id: TicketType.PROBLEM))
    ticket.reload

    assert ticket.problem?

    ticket2.linked_id = ticket.id
    ticket2.will_be_saved_by(user)
    ticket2.save!

    assert ticket.has_incidents
  end

  describe "#shared_with_jira" do
    before do
      @ticket = tickets(:minimum_1)
    end

    it "returns nil if there are no sharing agreements" do
      assert_nil @ticket.shared_with_jira
    end

    it "does not break if there's a shared_ticket with a nil agreement" do
      shared_ticket = stub("non-JIRA shared ticket")
      shared_ticket.stubs(:safe_agreement).returns(nil)
      @ticket.stubs(:shared_tickets).returns([shared_ticket])
      @ticket.shared_with_jira
    end
    it "returns nil if there isn't a sharing agreement with JIRA" do
      agreement = stub("non-JIRA sharing agreement")
      agreement.stubs(:shared_with_id).returns(Sharing::Agreement::SHARED_WITH_MAP[:jira] + 1) # Any id that isn't jira
      shared_ticket = stub("non-JIRA shared ticket")
      shared_ticket.stubs(:safe_agreement).returns(agreement)
      @ticket.stubs(:shared_tickets).returns([shared_ticket])
      assert_nil @ticket.shared_with_jira
    end

    it "returns a SharedTicket if there is a sharing agreement with JIRA" do
      agreement = stub("JIRA sharing agreement")
      agreement.stubs(:shared_with_id).returns(Sharing::Agreement::SHARED_WITH_MAP[:jira])
      shared_ticket = stub("JIRA shared ticket")
      shared_ticket.stubs(:safe_agreement).returns(agreement)
      @ticket.stubs(:shared_tickets).returns([shared_ticket])
      assert_not_nil @ticket.shared_with_jira
    end
  end

  describe "#inject_screencasts_in_metadata" do
    before do
      @user = users(:minimum_admin)
      @ticket = tickets(:minimum_1)
      screencast = {id: 'test1',
                    position: "5",
                    url: "http://supporttest.sfssdev.com/embed/test5",
                    thumbnail: 'http://pic.com/test1.jpg'}
      @screencasts = [screencast]
    end

    it "saves into audit.metadata[:system][:screencasts]" do
      @ticket.will_be_saved_by(@user)
      @ticket.inject_screencasts_in_metadata(@screencasts)
      assert_equal("test1",
        @ticket.audit.metadata[:system][:screencasts].first[:id])
      assert_equal("5",
        @ticket.audit.metadata[:system][:screencasts].first[:position])
      assert_equal("http://supporttest.sfssdev.com/embed/test5",
        @ticket.audit.metadata[:system][:screencasts].first[:url])
      assert_equal("http://pic.com/test1.jpg",
        @ticket.audit.metadata[:system][:screencasts].first[:thumbnail])
    end
  end

  describe 'Given a shared ticket and a soft delted agreement' do
    before do
      @ticket = tickets(:minimum_1)
      @agreement = FactoryBot.create(:agreement)
      @ticket.shared_tickets.create!(agreement: @agreement)
      @agreement.soft_delete!
    end

    describe 'unsharing that ticket' do
      before do
        @ticket.unshare_ticket = '1'
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end

      it 'creates a ticket unshare event with the proper agreement id' do
        event = @ticket.audits.last.events.detect { |e| e.is_a?(TicketUnshareEvent) }
        assert_equal(@agreement.id, event.agreement_id)
      end
    end
  end

  describe "#add_translatable_error_event" do
    before { @ticket = tickets(:minimum_1) }

    def add_event(enforce_unique)
      @ticket.will_be_saved_by(User.system)
      @ticket.add_translatable_error_event({key: 'translation1'}, enforce_unique)
      @ticket.save!
      @ticket.reload
    end

    describe "when enforce_unique is true" do
      before do
        add_event(true)
        add_event(true)
      end

      it "does not add duplicate events" do
        assert_equal 1, @ticket.events.select { |e| e.is_a?(TranslatableError) }.size
      end
    end

    describe "when enforce_unique is false" do
      before do
        add_event(false)
        add_event(false)
      end

      it "adds duplicate events" do
        assert_equal 2, @ticket.events.select { |e| e.is_a?(TranslatableError) }.size
      end
    end
  end

  describe "#requested_by?" do
    before do
      @requester = users(:minimum_agent)
      @ticket = Ticket.new(requester: @requester)
    end

    it "returns true if the user is the same as the requester" do
      assert @ticket.requested_by?(@requester)
    end

    it "returns false if the user is not the same as the requester" do
      refute @ticket.requested_by?(users(:minimum_admin))
    end

    it "always return false if the ticket has no requester set" do
      @ticket.requester = nil
      refute @ticket.requested_by?(nil)
    end
  end

  describe "#submitted_by_requester?" do
    it "returns true if the ticket was submitted by the requester" do
      @user = users(:minimum_end_user)
      @ticket = Ticket.new(requester: @user, submitter: @user)
      assert @ticket.submitted_by_requester?
    end

    it "returns false if the ticket was submitted by someone besides the requester" do
      @user1 = users(:minimum_end_user)
      @user2 = users(:minimum_agent)
      @ticket = Ticket.new(requester: @user1, submitter: @user2)
      refute @ticket.submitted_by_requester?
    end

    it "returns true if the submitter is nil" do
      @ticket = Ticket.new(requester: @user1, submitter: nil)
      assert @ticket.submitted_by_requester?
    end
  end

  describe "default group assignment" do
    before do
      @user = users(:with_groups_agent2) # belongs to with_groups_group1 and with_groups_group2
      @ticket = @user.account.tickets.new(description: "nice", requester: @user, submitter: @user)
      @ticket.will_be_saved_by(@user)
      @ticket.save!
      assert_nil @ticket.group_id
    end

    describe "Assigning a ticket to an agent without declaring a group" do
      before do
        @ticket.will_be_saved_by(@user)
      end

      describe "when the agent has a default_group" do
        before do
          User.any_instance.expects(:default_group_id).returns(groups(:with_groups_group2).id)
          @ticket.update_attributes!(assignee: @user)
        end

        it "assigns the ticket to the user's default group" do
          assert_equal groups(:with_groups_group2).id, @ticket.group_id
        end
      end

      describe "when the agent has an inactive default_group" do
        before do
          @group = groups(:with_groups_group2)
          User.any_instance.expects(:default_group_id).returns(nil).times(2)

          @ticket.update_attributes!(assignee: @user)
        end

        it "does not assign the ticket to the user's default group" do
          assert_not_equal @group.id, @ticket.group_id
        end
      end

      describe "when the agent does not have a default_group" do
        before do
          User.any_instance.expects(:default_group_id).returns(nil)
          @ticket.update_attributes!(assignee: @user)
        end

        should_eventually "assign the ticket to the user's default group" do
          assert_equal groups(:with_groups_group2).id, @ticket.group_id, @ticket.group.name
        end
      end
    end

    describe "Assigning a ticket to an agent email without declaring a group" do
      before do
        @ticket.will_be_saved_by(@user)
      end

      describe "when the agent has a default_group" do
        before do
          User.any_instance.expects(:default_group_id).returns(groups(:with_groups_group2).id)
          @ticket.update_attributes!(assignee_email: @user.email)
        end

        it "assigns the ticket to the user's default group" do
          assert_equal groups(:with_groups_group2).id, @ticket.group_id
        end
      end

      describe "when the agent has an inactive default_group" do
        before do
          @group = groups(:with_groups_group2)
          User.any_instance.expects(:default_group_id).returns(nil).times(2)

          @ticket.update_attributes!(assignee_email: @user.email)
        end

        it "does not assign the ticket to the user's default group" do
          assert_not_equal @group.id, @ticket.group_id
        end
      end

      describe "when the agent does not have a default_group" do
        before do
          User.any_instance.expects(:default_group_id).returns(nil)
          @ticket.update_attributes!(assignee_email: @user.email)
        end

        should_eventually "assign the ticket to the user's default group" do
          assert_equal groups(:with_groups_group2).id, @ticket.group_id, @ticket.group.name
        end
      end
    end
  end

  describe "tag updating" do
    describe "removal" do
      subject { tickets(:minimum_2) }

      before do
        subject.remove_tags = 'hello'
        subject.will_be_saved_by(subject.requester)
      end

      it "removes tagging" do
        assert_difference "Tagging.count(:all)", -1 do
          subject.save!
        end

        assert_equal ['big'], subject.tag_array
      end
    end
  end

  describe "#auto_tag" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.via_id = ViaType.Mail
      @ticket.current_tags = ""

      @agent = users(:minimum_admin)
      @current_user = users(:minimum_end_user)
      @ticket.current_user = @current_user

      @comment = Comment.new(body: "document", author: @current_user, account: @account, ticket: @ticket)
      @ticket.stubs(:comment).returns(@comment)

      @ticket.will_be_saved_by(@agent)
      @ticket.save!
    end

    describe "when ticket tagging is enabled" do
      before do
        @ticket.account.settings.ticket_tagging = true
      end

      it "autotags when auto tagging setting is enabled" do
        @ticket.account.settings.ticket_auto_tagging = true
        @ticket.account.settings.save!
        @ticket.send(:auto_tag)

        assert_equal "document", @ticket.current_tags
      end

      it "does not autotag when auto tagging setting is disabled" do
        @ticket.account.settings.ticket_auto_tagging = false
        @ticket.account.settings.save!
        @ticket.send(:auto_tag)
        Thesaurus.any_instance.expects(:autotag).never
      end
    end

    describe "when ticket tagging is disabled" do
      before do
        @ticket.account.settings.ticket_tagging = false
        @ticket.account.settings.ticket_auto_tagging = true
        @ticket.account.settings.save!
      end

      it "does not autotag even if auto tagging is enabled" do
        @ticket.send(:auto_tag)
        Thesaurus.any_instance.expects(:autotag).never
      end
    end
  end

  describe "Updating group for a solved ticket" do
    before do
      @ticket = tickets(:minimum_4)
      assert_equal StatusType.SOLVED, @ticket.status_id

      @new_group = Group.new(is_active: 1, name: "test group")
      @new_group.account = @ticket.account
      @new_group.save!
    end

    it "makes sure assignee exists" do
      @ticket.group_id = @new_group.id
      refute @ticket.assignee.in_group?(@ticket.group)
      @ticket.assignee = nil
      @ticket.will_be_saved_by(users(:minimum_admin))
      refute @ticket.save
      assert_includes @ticket.errors[:base].join, "#{ticket_fields(:support_field_assignee).title}: is required"
    end
  end

  describe "#build_new_user" do
    before do
      @ticket = tickets(:minimum_1)
      @account = accounts(:minimum)
      @japanese = translation_locales(:japanese)
      @account.translation_locale = @japanese
      @ticket.account = @account
      @user_data = {}
    end

    it "gets the account's locale if there is no current user" do
      new_user = @ticket.send(:build_new_user, @user_data)
      assert_equal @japanese, new_user.translation_locale
    end

    it "creates no-organization user and ticket if agent has full access to end user profiles" do
      @agent = users(:minimum_agent)

      @permission_set = PermissionSet.new
      @agent.account.stubs(:has_permission_sets?).returns(true)
      @agent.stubs(:permission_set).returns(@permission_set)
      @agent.organization = organizations(:minimum_organization1)
      @agent.permission_set.permissions.end_user_profile = 'full'

      new_user = @ticket.send(:build_new_user, @user_data)
      assert @agent.can?(:create, new_user)
      assert_nil new_user.organization

      @ticket.will_be_saved_by(@agent)
      assert @ticket.save!
    end

    it "creates in-organization user and ticket if agent has edit-within-org access to end user profiles" do
      @agent = users(:minimum_agent)

      @permission_set = PermissionSet.new
      @agent.account.stubs(:has_permission_sets?).returns(true)
      @agent.stubs(:permission_set).returns(@permission_set)
      @agent.organizations << organizations(:minimum_organization1)
      @agent.permission_set.permissions.end_user_profile = 'edit-within-org'

      new_user = @ticket.send(:build_new_user, @user_data)
      new_user.organization = organizations(:minimum_organization1)

      assert @agent.can?(:create, new_user)
      assert_equal new_user.organization, @agent.organization

      @ticket.will_be_saved_by(@agent)
      assert @ticket.save!
    end

    it "overrides the requester's organization_id if current_user is org restricted agent" do
      @user_data[:organization_id] = 123
      current_user = users(:minimum_agent)
      current_user.organization = organizations(:minimum_organization1)
      current_user.stubs(:agent_restriction?).returns(true)
      @ticket.current_user = current_user
      new_user = @ticket.send(:build_new_user, @user_data)
      assert_equal current_user.organization_id, new_user.organization_id
    end

    describe "setting user#send_verify_email" do
      before { @user_data = {email: "someone@example.net"} }

      describe "when current_user is an agent" do
        before { @ticket.will_be_saved_by(@account.owner) }

        it "returns true if account.is_welcome_email_when_agent_register_enabled? is true" do
          @account.stubs(is_welcome_email_when_agent_register_enabled?: true)
          new_user = @ticket.send(:build_new_user, @user_data)
          assert new_user.send_verify_email
        end

        it "returns false if account.is_welcome_email_when_agent_register_enabled? is false" do
          @account.stubs(is_welcome_email_when_agent_register_enabled?: false)
          new_user = @ticket.send(:build_new_user, @user_data)
          refute new_user.send_verify_email
        end
      end

      describe "when current_user is an anonymous user" do
        before { @ticket.will_be_saved_by(@account.anonymous_user) }

        it "returns true if account.is_signup_required? is true" do
          @account.stubs(is_signup_required?: true)
          new_user = @ticket.send(:build_new_user, @user_data)
          assert new_user.send_verify_email
        end

        it "returns false if account.is_signup_required? is false" do
          @account.stubs(is_signup_required?: false)
          new_user = @ticket.send(:build_new_user, @user_data)
          refute new_user.send_verify_email
        end
      end
    end

    describe "when request is from web interface and from anonymous user" do
      before do
        Ticket.any_instance.stubs(:via_id).returns(ViaType.WEB_FORM)
        Ticket.any_instance.stubs(:current_user).returns(@account.anonymous_user)
        I18n.translation_locale = @japanese
      end

      it "gets the current locale" do
        new_user = @ticket.send(:build_new_user, @user_data)
        assert_equal @japanese, new_user.translation_locale
      end
    end

    describe "when there is a locale id in the user data hash" do
      before do
        @user_data.merge!(locale_id: ENGLISH_BY_ZENDESK.id)
      end

      it "gets the user's locale if there is a current user" do
        new_user = @ticket.send(:build_new_user, @user_data)
        assert_equal ENGLISH_BY_ZENDESK, new_user.translation_locale
      end
    end
  end

  describe "#add_sharing_event" do
    before { @ticket = tickets(:minimum_1) }

    it "raises without an audit" do
      assert_raise RuntimeError do
        @ticket.add_sharing_event("1", a: 2)
      end
    end

    describe "with an audit" do
      before do
        @ticket.will_be_saved_by(users(:minimum_agent))
      end

      describe "with an account agreement id" do
        before do
          @ticket.stubs(account: stub(id: 949834943, sharing_agreements: [stub(id: 1)]))
          @ticket.stubs(shared_tickets: [])
        end

        it "adds an agreement" do
          assert_difference "@ticket.audit.events.size" do
            @ticket.add_sharing_event("1", a: :b)
          end
        end

        describe "after" do
          before do
            @ticket.add_sharing_event("1", a: :b)
            @event = @ticket.audit.events.last
          end

          it "adds the id correctly" do
            assert_equal 1, @event.agreement_id
          end

          it "adds the custom field correctly" do
            assert_equal({ a: :b }, @event.custom_fields)
          end
        end
      end

      describe "without an account agreement id" do
        it "does not add an agreement" do
          refute_difference "@ticket.audit.events.size" do
            @ticket.add_sharing_event("1", a: :b)
          end
        end
      end

      describe "with a ticket agreement id" do
        before do
          @ticket.stubs(account: stub(sharing_agreements: [stub(id: 1)]))
          @ticket.stubs(shared_tickets: [stub(agreement_id: 1)])
        end

        it "does not add an agreement" do
          refute_difference "@ticket.audit.events.size" do
            @ticket.add_sharing_event("1", a: :b)
          end
        end
      end
    end
  end

  describe "#create_ticket_sharing_event" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.agreement_id = subject
    end

    describe "with agreement_id set" do
      subject { "1" }

      it "add_sharing_events" do
        @ticket.expects(:add_sharing_event).with("1", nil)
        @ticket.create_ticket_sharing_event
      end
    end

    describe "with an agreement_id" do
      subject { nil }

      it "does not add_sharing_event" do
        @ticket.expects(:add_sharing_event).never
        @ticket.create_ticket_sharing_event
      end
    end
  end

  describe '#record_macro_use_by_agent' do
    let(:ticket)    { tickets(:minimum_2) }
    let(:account)   { accounts(:minimum) }
    let(:macro_ids) { [create_macro, create_macro].map(&:id) }
    let(:agent)     { account.agents.first }

    let(:agent_macro_references) do
      AgentMacroReference.where(ticket_id: ticket.id)
    end

    let(:comment) do
      {body: 'You know nothing, Jon Snow.', is_public: true}
    end

    before do
      ticket.stubs(:record_macros_applied).returns(true)
      ticket.audits.delete_all
      ticket.will_be_saved_by(agent)
      ticket.save!
    end

    describe 'when there is no `audit` present' do
      before do
        ticket.macro_ids = macro_ids
        ticket.current_user = agent
        ticket.save!
      end

      it 'does not record any macro agent reference events' do
        assert_empty agent_macro_references
      end
    end

    describe 'when there are multiple macros applied' do
      before do
        ticket.macro_ids = macro_ids
        ticket.add_comment(comment)
        ticket.current_user = agent
        ticket.will_be_saved_by(agent)
        ticket.save!
      end

      it 'creates agent reference events for each macro' do
        assert_equal macro_ids, agent_macro_references.map(&:value).map(&:to_i)
      end
    end

    describe 'when there are no macros applied' do
      before do
        ticket.add_comment(comment)
        ticket.will_be_saved_by(agent)
        ticket.save!
      end

      it 'does not record any macro agent reference events' do
        assert_empty agent_macro_references
      end
    end

    describe 'when a macro with a blank id is applied' do
      before do
        ticket.macro_ids = [""]
        ticket.add_comment(comment)
        ticket.current_user = agent
        ticket.will_be_saved_by(agent)
        Rails.logger.stubs(:info)
      end

      let!(:macro_reference) { AgentMacroReference.new }

      describe_with_arturo_disabled :email_blank_macro_reference do
        describe_with_arturo_enabled :email_log_blank_macro_reference do
          before do
            Rails.logger.expects(:info).with("Attempting to create a new AgentMacroReference but macro_id is blank. account_id: #{account.id}, ticket_id: #{ticket.id}")
            AgentMacroReference.expects(:new).with do |macro_reference|
              macro_reference[:value] == ""
            end.once.returns(macro_reference)
            ticket.save!
          end

          it 'does not record any macro agent reference events' do
            assert_empty agent_macro_references
          end
        end
      end

      describe_with_arturo_enabled :email_blank_macro_reference do
        before do
          Rails.logger.expects(:info).with("Not creating an AgentMacroReference AuditEvent because macro_id is blank. account_id: #{account.id}, ticket_id: #{ticket.id}")
          AgentMacroReference.expects(:new).with do |macro_reference|
            macro_reference[:value] == ""
          end.never
          ticket.save!
        end

        it 'does not record any macro agent reference events' do
          assert_empty agent_macro_references
        end
      end
    end

    describe 'when the macro was applied by a trigger or automation' do
      let(:definition)      { Definition.new }

      let(:definition_item) do
        DefinitionItem.new('macro_id', nil, macro_ids.first)
      end

      before do
        Account.
          any_instance.
          stubs(has_rules_can_reference_macros?: true)

        definition.send(:actions).push(definition_item)

        Rule.new(definition: definition).apply_actions(ticket)

        ticket.save!
      end

      it 'records a `MacroReference` event' do
        assert_equal 1, MacroReference.where(ticket_id: ticket.id).count
      end

      it 'does not record a macro agent reference event' do
        assert_empty agent_macro_references
      end
    end
  end

  describe '#apply_ticket_deflection' do
    before { Resque.inline = true }
    after { Resque.inline = false }

    it 'enqueues a new Ticket Deflection Job' do
      TicketDeflectionJob.expects(:enqueue).once

      # create a ticket which fires the after_commit hook.
      accounts(:minimum).tickets.create! do |t|
        t.description = "Test"
        t.requester = users(:minimum_agent)
        t.deflect_with_rule 9198
        t.will_be_saved_by(t.requester)
      end
    end

    describe 'disable ticket deflection trigger' do
      it 'does not enqueue TicketDeflectionJob' do
        TicketDeflectionJob.expects(:enqueue).never

        accounts(:minimum).tickets.create! do |t|
          t.description = "Test"
          t.requester = users(:minimum_agent)
          t.disable_answer_bot_trigger = true
          t.deflect_with_rule 9198
          t.will_be_saved_by(t.requester)
        end
      end
    end
  end

  describe "#apply_after_save_macros" do
    before { Resque.inline = true }
    after { Resque.inline = false }

    describe "with a new ticket" do
      before do
        @ticket = accounts(:minimum).tickets.create! do |t|
          t.description = "Test"
          t.requester = users(:minimum_agent)

          t.macros_to_be_applied_after_save = [rules(:macro_incident_escalation).id]
          t.will_be_saved_by(t.requester)
        end
      end

      before_should "call #execute on one macro" do
        Macro.any_instance.expects(:execute).once
      end

      it "creates new audit from the system user" do
        audit = @ticket.audits.reload.last

        assert_equal ViaType.MACRO_REFERENCE, audit.via_id
        assert_equal User.system.id, audit.author_id
      end
    end

    describe "with an existing ticket" do
      let(:should_execute) { 0 }
      subject { [] }

      before do
        @ticket = tickets(:minimum_1)
        @ticket.macros_to_be_applied_after_save = subject.try(:map, &:id)
        @ticket.send(:apply_after_save_macros)
      end

      before_should "macro execution times" do
        Macro.any_instance.expects(:execute).times(should_execute)
      end

      describe "with an inactive macro" do
        describe "with only one" do
          let(:should_execute) { 0 }
          subject { [rules(:inactive_macro_for_minimum_group)] }

          it "does not create a new audit" do
            assert_not_equal ViaType.MACRO_REFERENCE, @ticket.audits.reload.last.via_id
          end
        end

        describe "with mixed macros" do
          let(:should_execute) { 1 }
          subject do
            [rules(:inactive_macro_for_minimum_group),
             rules(:macro_incident_escalation)]
          end

          it "creates an audit" do
            assert_equal ViaType.MACRO_REFERENCE, @ticket.audits.reload.last.via_id
          end
        end
      end

      describe "multiple macros" do
        let(:should_execute) { 2 }
        subject do
          [rules(:macro_incident_escalation),
           rules(:macro_for_minimum_agent)]
        end

        it "creates an audit" do
          assert_equal ViaType.MACRO_REFERENCE, @ticket.audits.reload.last.via_id
        end

        it "is saved by assignee" do
          assert_equal @ticket.assignee_id, @ticket.audits.reload.last.author_id
        end
      end

      describe "duplicated macro id" do
        let(:should_execute) { 1 }
        subject do
          macro = rules(:macro_incident_escalation)
          [macro, macro]
        end

        it "clears macros_to_be_applied_after_save" do
          assert_equal [], @ticket.macros_to_be_applied_after_save
        end

        it "creates an audit" do
          assert_equal ViaType.MACRO_REFERENCE, @ticket.audits.reload.last.via_id
        end
      end

      describe "with no macros" do
        let(:should_execute) { 0 }
        subject { nil }

        it "does not add any audit events" do
          assert_not_equal ViaType.MACRO_REFERENCE, @ticket.audits.reload.last.via_id
        end
      end
    end
  end

  describe "#recipient_address" do
    let(:ticket) { tickets(:minimum_1) }

    it "returns the recipient_recipient address record" do
      ticket.original_recipient_address = recipient_addresses(:not_default).email
      assert_equal recipient_addresses(:not_default), ticket.recipient_address
    end

    it "returns nil if no recipient_address record is found" do
      ticket.original_recipient_address = "no_recipient_address@example.net"
      assert_nil ticket.recipient_address
    end
  end

  describe "#public_description" do
    let(:ticket) { tickets(:minimum_1) }

    it "renders the first public comment" do
      ticket.comments.each_with_index do |comment, i|
        assert(comment.update_columns(is_public: (i == 1), value: "minimum <span>ticket1</span> \n\n comment #{i}\n\n"))
      end
      ticket.comments.reload
      refute_equal ticket.public_description, ticket.comments.first.html_body
      assert_equal ticket.public_description, ticket.comments.second.html_body
      refute_equal ticket.public_description(markdown: true), ticket.description
      assert_equal ticket.public_description(markdown: true), ticket.comments.second.body
    end

    it "does not blow up with no public comment" do
      ticket.comments.delete_all
      assert_equal "", ticket.public_description
      assert_equal "", ticket.public_description(markdown: true)
    end

    it "uses the correct index" do
      assert_sql_queries(1, /FROM events USE INDEX\(index_events_on_ticket_id_and_type\) WHERE/) do
        ticket.public_description
      end
    end
  end

  describe "#public_updated_at" do
    let(:ticket) { tickets(:minimum_1) }

    before { Timecop.freeze }

    it "changes public_updated_at when receiving a public comment" do
      ticket.add_comment(body: 'Do not close yet', is_public: true)
      ticket.will_be_saved_by(ticket.requester)
      ticket.save!
      assert_equal ticket.updated_at.to_i, ticket.public_updated_at.to_i
    end

    it "does not change public_updated_at when receiving an internal comment" do
      old_public_updated_at = ticket.public_updated_at
      ticket.add_comment(body: 'Comment by an agent', is_public: false)
      ticket.will_be_saved_by(ticket.assignee)
      ticket.save!
      assert_equal old_public_updated_at, ticket.public_updated_at
    end

    it "changes public_updated_at when status is changed with no comment" do
      old_public_updated_at = ticket.public_updated_at
      ticket.status_id = StatusType.SOLVED
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!
      assert_equal old_public_updated_at, ticket.public_updated_at
    end
  end

  describe "#in_business_hours?" do
    let(:ticket)   { tickets(:minimum_1) }
    let(:account)  { ticket.account }
    let(:schedule) do
      account.schedules.create(name: 'Schedule', time_zone: account.time_zone)
    end

    before { add_schedule_intervals(schedule, [:sun, '02:00', '04:00']) }

    describe "when the 'business hours' feature is active" do
      before { account.stubs(:business_hours_active?).returns(true) }

      describe "and the current time is in business hours" do
        before { Timecop.freeze(Time.zone.local(2012, 1, 1, 2, 30)) }

        it "returns true" do
          assert ticket.in_business_hours?
        end
      end

      describe "and the current time is not in business hours" do
        before { Timecop.freeze(Time.zone.local(2012, 1, 1, 5, 0)) }

        it "returns false" do
          refute ticket.in_business_hours?
        end
      end
    end

    describe "when the 'business hours' feature is not active" do
      before { account.stubs(:business_hours_active?).returns(false) }

      describe "and the current time is in business hours" do
        before { Timecop.freeze(Time.zone.local(2012, 1, 1, 2, 30)) }

        it "returns true" do
          assert ticket.in_business_hours?
        end
      end

      describe "and the current time is not in business hours" do
        before { Timecop.freeze(Time.zone.local(2012, 1, 1, 5, 0)) }

        it "returns true" do
          assert ticket.in_business_hours?
        end
      end
    end
  end

  describe "#safe_update_timestamp=" do
    before do
      @ticket = tickets(:minimum_1)
      @safe_update_timestamp = @ticket.updated_at.to_s
    end

    it "converts serialized times from coming from resque back to time objects" do
      @ticket.safe_update_timestamp = @safe_update_timestamp
      @ticket.safe_update_timestamp.must_equal @ticket.updated_at
    end
  end

  describe "when the ticket has skips" do
    let(:ticket)  { tickets(:minimum_1) }
    let(:account) { ticket.account }

    before do
      account.skips.create(
        ticket_id: ticket.id,
        user_id:   users(:minimum_agent).id,
        reason:    'Why not?'
      )
    end

    describe "and the ticket is destroyed" do
      it "also destroys ticket's skips" do
        assert_equal :destroy, Ticket.reflect_on_association(:skips).options[:dependent]
      end
    end
  end

  describe "#update latest comment date" do
    let(:ticket) { tickets(:minimum_2) }
    before { Timecop.freeze }

    it "should update agent comment date by privacy" do
      ticket.add_comment(body: "A comment", author: users(:minimum_agent), is_public: false)
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!
      assert_equal ticket.latest_agent_comment_added_at, ticket.updated_at
      assert_equal ticket.latest_comment_added_at, ticket.updated_at
    end

    it "should update public comment date if saved by end user" do
      ticket.add_comment(body: "A comment", author: users(:minimum_end_user), is_public: true)
      ticket.will_be_saved_by(users(:minimum_end_user))
      ticket.save!

      assert_equal ticket.updated_at, ticket.latest_public_comment_added_at
      assert_equal ticket.updated_at, ticket.latest_comment_added_at
      assert_nil ticket.latest_agent_comment_added_at
    end

    it "should return the correct latest agent comment" do
      time = Time.now
      ticket.add_comment(body: "A comment", author: users(:minimum_agent))
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!

      Timecop.freeze(Time.now + 1.hour)

      ticket.add_comment(body: "A comment", author: users(:minimum_end_user))
      ticket.will_be_saved_by(users(:minimum_end_user))
      ticket.save!

      assert_equal ticket.agent_comments.size, 2 # ensure we have two agent tickets to compare.
      assert_equal ticket.latest_agent_comment.created_at.to_i, time.to_i
      assert_not_equal ticket.agent_comments[0].created_at.to_i, ticket.agent_comments[1].created_at.to_i
    end
  end

  describe 'recording use of a macro when saving' do
    let(:ticket) { tickets(:minimum_1) }
    let(:agent)  { users(:minimum_agent) }

    let(:macro_usage) { stub('macro usage') }

    before do
      ticket.tap do |t|
        t.current_user = agent
        t.macro_ids    = macro_ids
      end.will_be_saved_by(agent)

      Zendesk::RuleSelection::MacroUsage.stubs(:new).
        with(agent).
        returns(macro_usage)
    end

    describe 'when `macro_ids` is present' do
      describe 'and the macro exists' do
        let(:macro_ids) { [ticket.account.all_macros.first.id] }

        before { ticket.save! }

        before_should 'record the use of the macro' do
          macro_usage.expects(:record_many).with(macro_ids)
        end
      end

      describe 'and the macro does not exist' do
        let(:macro_ids) { [987654321] }

        before { ticket.save! }

        before_should 'not record the use of the macro' do
          macro_usage.expects(:record_many).never
        end
      end
    end

    describe 'when `macro_ids` is empty' do
      let(:macro_ids) { [] }

      before { ticket.save! }

      before_should 'not record a use of a macro' do
        macro_usage.expects(:record_many).never
      end
    end
  end

  describe 'recording use of rule while saving' do
    let(:ticket)  { tickets(:minimum_1) }
    let(:agent)   { users(:minimum_agent) }
    let(:account) { accounts(:minimum) }

    let(:rule_usage) { stub('rule usage') }

    before do
      Account.any_instance.stubs(has_rule_usage_stats?: true)

      Zendesk::RuleSelection::RuleUsage.stubs(:new).
        with(account).
        returns(rule_usage)
    end

    describe 'when the rule is a macro' do
      before do
        ticket.tap do |t|
          t.current_user = agent
          t.macro_ids    = macro_ids
        end.will_be_saved_by(agent)
      end

      describe 'and `macro_ids` is not empty' do
        describe 'and the macro exists' do
          let(:macro_ids) { [ticket.account.all_macros.first.id] }

          describe 'and the ticket is saved' do
            before { ticket.save! }

            before_should 'record the use of the macro' do
              rule_usage.expects(:record_many).with(type: :macro, rule_ids: macro_ids)
            end
          end

          describe 'and `rule_usage_stats` is not enabled' do
            before do
              Account.any_instance.stubs(has_rule_usage_stats?: false)

              ticket.save!
            end

            before_should 'not record the use of the macro' do
              rule_usage.expects(:record_many).never
            end
          end
        end

        describe 'and the macro does not exist' do
          let(:macro_ids) { [987654321] }

          before { ticket.save! }

          before_should 'not record the use of the macro' do
            rule_usage.expects(:record_many).never
          end
        end
      end

      describe 'and `macro_ids` is empty' do
        let(:macro_ids) { [] }

        before { ticket.save! }

        before_should 'not record a use of a macro' do
          rule_usage.expects(:record_many).never
        end
      end
    end

    describe 'when the rule is not a macro or view' do
      let(:rule) { rules(:trigger_notify_requester_of_received_request) }

      before { ticket.will_be_saved_by(agent) }

      describe 'and is applied to the ticket' do
        before { rule.apply_actions(ticket) }

        describe 'and the ticket is saved' do
          before { ticket.save! }

          before_should 'record the use of the trigger' do
            rule_usage. expects(:record_many).
              with(type: :trigger, rule_ids: [rule.id])
          end
        end

        describe 'and `rule_usage_stats` is not enabled' do
          before do
            Account.any_instance.stubs(has_rule_usage_stats?: false)

            ticket.save!
          end

          before_should 'not record the use of the trigger' do
            rule_usage.expects(:record_many).never
          end
        end
      end

      describe 'and is not applied to the ticket' do
        before { ticket.save! }

        before_should 'not record the use of the trigger' do
          rule_usage.expects(:record_many).never
        end
      end
    end

    describe 'when multiple rules are applied to the ticket' do
      let(:triggers) do
        [
          rules(:trigger_notify_requester_of_received_request),
          rules(:trigger_notify_requester_of_comment_update)
        ]
      end

      let(:automations) do
        [
          rules(:automation_close_ticket),
          rules(:automation_group_to_different_group)
        ]
      end

      before do
        ticket.will_be_saved_by(agent)

        [*triggers, *automations].each do |rule|
          rule.apply_actions(ticket)
        end

        ticket.save!
      end

      before_should 'record the use by rule type' do
        rule_usage.expects(:record_many).
          with(type: :trigger, rule_ids: triggers.map(&:id))

        rule_usage.expects(:record_many).
          with(type: :automation, rule_ids: automations.map(&:id))
      end
    end
  end

  describe '#update voice ticket' do
    let(:user) { users(:minimum_admin) }
    let(:target) { tickets(:minimum_1) }
    let(:source1) { tickets(:minimum_2) }

    it 'links target to via_reference_id' do
      source1.update_voice_ticket(target)
      voice_core_ticket = Voice::Core::Ticket.find(source1.id)

      assert_equal voice_core_ticket.via_reference_id, target.id
    end
  end

  describe '#rich?' do
    let(:basic_types) do
      [
        ViaType.TWITTER,
        ViaType.TWITTER_DM,
        ViaType.TWITTER_FAVORITE,
        ViaType.SMS,
        ViaType.ANY_CHANNEL,
        ViaType.FACEBOOK_POST,
        ViaType.FACEBOOK_MESSAGE
      ]
    end
    let(:rich_types) { ViaType.fields - basic_types }

    it 'returns false when the channel does not support rich comments' do
      basic_types.each do |type|
        Ticket.any_instance.stubs(:via_id).returns(type)
        refute Ticket.new.rich?, "#{ViaType[type].name} tickets should not be `rich?`"
      end
    end

    it 'returns true when the channel does support rich comments' do
      rich_types.each do |type|
        Ticket.any_instance.stubs(:via_id).returns(type)
        assert Ticket.new.rich?, "#{ViaType[type].name} tickets should be `rich?`"
      end
    end
  end

  describe "#add_knowledge_event" do
    let(:ticket) { tickets(:minimum_1) }
    let(:user) { users(:minimum_agent) }
    let(:template) { { id: 1, url: 'http://helpcenter.article/1.json', title: 'article title 1', html_url: 'http://helpcenter.article/1'} }
    let(:article) { { id: 1, url: 'http://helpcenter.article/1.json', title: 'article title 1', html_url: 'http://helpcenter.article/1', locale: 'en-US' } }

    %w[
      KnowledgeLinked
      KnowledgeFlagged
      KnowledgeLinkAccepted
      KnowledgeLinkRejected
    ].each do |event_type|
      it "creates an #{event_type} event" do
        ticket.add_knowledge_event(event_type, article, user)
        assert ticket.events.pluck(:type).include? event_type
      end

      it "does not create #{event_type} without an article" do
        ticket.add_knowledge_event(event_type, nil, user)
        refute ticket.events.pluck(:type).include? event_type
      end

      it "does not create #{event_type} without an user" do
        ticket.add_knowledge_event(event_type, article, nil)
        refute ticket.events.pluck(:type).include? event_type
      end
    end

    it "creates an KnowledgeCaptured event only when passed with an article and template" do
      ticket.add_knowledge_event('KnowledgeCaptured', article, user, template)
      assert ticket.events.pluck(:type).include? 'KnowledgeCaptured'
    end

    it "does not create event with an unrecognized event type" do
      ticket.add_knowledge_event('someEvent', article, user)
      refute ticket.events.pluck(:type).include? 'KnowledgeLinked'
    end
  end

  describe "when blocking inbound spam" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_end_user) }
    let(:new_ticket) { ticket_new(user) }

    before do
      # Stub the account `domain_whitelist` because `minimum_user`'s email's
      # domain is on this whitelist in the fixtures
      new_ticket.account.stubs(:domain_whitelist).returns("some.com domains.net here.edu")
      # Stub the account `organization_whitelist` because `minimum_user`'s
      # email's domain is on this whitelist in the fixtures
      new_ticket.account.stubs(:organization_whitelist).returns("other.com ones.net there.edu")

      Datadog.tracer.stubs(:active_span).returns(stub(:active_span))
    end

    describe_with_arturo_enabled :spam_detector_inbound_spam do
      describe "when blocking inbound spam during new ticket creation" do
        before { new_ticket.will_be_saved_by(user) }

        it "suspend_ticket blocks ticket creation and creates a suspended ticket" do
          indicators = ['blacklisted_string']
          Fraud::SpamDetector.any_instance.stubs(:suspend_ticket?).returns(true)
          Fraud::SpamDetector.any_instance.stubs(:spam_indicators).returns(indicators)
          Rails.logger.expects(:info).with("ticket_model_suspended_ticket action fired due to indicators:#{indicators} -- account_id:#{account.id}").once
          Rails.logger.expects(:info).with("SPAM_DETECTOR - skipping_blacklist_pattern_check - #{account.subdomain} (#{account.id})").once
          new_ticket.expects(:rack_request_span).once
          assert_raises ::Fraud::SpamDetector::SpamTicketFoundException do
            old_suspended_count = account.suspended_tickets.size
            refute new_ticket.valid?
            assert_equal account.suspended_tickets.size, old_suspended_count + 1
            assert_equal account.suspended_tickets.last.cause, SuspensionType.MALICIOUS_PATTERN_DETECTED
          end
        end

        it "rejects the ticket" do
          Fraud::SpamDetector.any_instance.stubs(:reject_ticket?).returns(true)
          Rails.logger.expects(:info).with("ticket_model_rejected_ticket action fired due to indicators:[] -- account_id:#{account.id}").once
          Rails.logger.expects(:info).with("SPAM_DETECTOR - skipping_blacklist_pattern_check - #{account.subdomain} (#{account.id})").once
          assert_raises ::Fraud::SpamDetector::SpamTicketFoundException do
            refute new_ticket.valid?
          end
        end
      end

      describe "when a ticket is from a mail via_id" do
        before do
          new_ticket.will_be_saved_by(user)
          Ticket.any_instance.stubs(:via_id).returns(ViaType.MAIL)
        end

        it "does not run spam detection" do
          Fraud::SpamDetector.expects(:new).with(new_ticket).never
          new_ticket.valid?
        end
      end

      describe "when a ticket has a via type of 'closed ticket'" do
        before do
          new_ticket.will_be_saved_by(user)
          new_ticket.stubs(:via_id).returns(ViaType.CLOSED_TICKET)
        end

        describe_with_arturo_enabled :orca_classic_closed_ticket_check do
          it "does not run spam detection" do
            Fraud::SpamDetector.expects(:new).with(new_ticket).never
            Rails.logger.expects(:info).with("TICKET - from_mail_or_followup - skipping spam detection on ticket - #{new_ticket}")
            new_ticket.valid?
          end
        end

        describe_with_arturo_disabled :orca_classic_closed_ticket_check do
          it "runs spam detection" do
            Fraud::SpamDetector.expects(:new).with(new_ticket).once
            new_ticket.valid?
          end
        end
      end

      describe "when a ticket is from a non mail/non-closed-ticket via_id" do
        before do
          new_ticket.will_be_saved_by(user)
          Ticket.any_instance.stubs(:via_id).returns(ViaType.WEB_SERVICE)
        end

        it "runs spam detection" do
          Fraud::SpamDetector.expects(:new).with(new_ticket).once
          new_ticket.valid?
        end
      end

      describe 'when ticket submitter is an agent' do
        before { new_ticket.will_be_saved_by(users(:minimum_agent)) }
        it "does not create a spam_detector object" do
          Fraud::SpamDetector.expects(:new).never
          assert new_ticket.valid?
        end
      end
    end

    describe_with_arturo_disabled :spam_detector_inbound_spam do
      before { new_ticket.will_be_saved_by(user) }
      it "does not create a spam_detector object" do
        Fraud::SpamDetector.expects(:new).never
        assert new_ticket.valid?
      end
    end
  end

  describe '#extended_ticket_types?' do
    let(:ticket) { tickets(:minimum_1) }

    describe 'when the ticket has a ticket type ID' do
      before { ticket.ticket_type_id = TicketType.TASK }

      describe 'and the account has extended ticket types' do
        before { ticket.account.stubs(has_extended_ticket_types?: true) }

        it 'returns true' do
          assert ticket.extended_ticket_types?
        end
      end

      describe 'and the account does not have extended ticket types' do
        before { ticket.account.stubs(has_extended_ticket_types?: false) }

        it 'returns false' do
          refute ticket.extended_ticket_types?
        end
      end
    end

    describe 'when the ticket does not have a ticket type ID' do
      before { ticket.ticket_type_id = nil }

      describe 'and the account has extended ticket types' do
        before { ticket.account.stubs(has_extended_ticket_types?: true) }

        it 'returns false' do
          refute ticket.extended_ticket_types?
        end
      end

      describe 'and the account does not have extended ticket types' do
        before { ticket.account.stubs(has_extended_ticket_types?: false) }

        it 'returns false' do
          refute ticket.extended_ticket_types?
        end
      end
    end
  end

  describe 'associating attribute values with tickets' do
    let(:account) { accounts(:minimum) }
    let(:agent)   { users(:minimum_agent) }

    let(:matching_comment) do
      Comment.new(
        account: account,
        author: agent,
        body:   'match me too!',
        ticket: ticket
      )
    end

    before do
      new_attribute_ticket_map(attribute_value_id: '1', comment: 'match')
      new_attribute_ticket_map(attribute_value_id: '2', comment: 'me too!')
      new_attribute_ticket_map(attribute_value_id: '3', comment: "don't bro")
    end

    describe_with_arturo_enabled :skill_based_ticket_routing do
      describe_with_arturo_enabled :skill_based_attribute_ticket_mapping do
        before do
          ticket.comment      = matching_comment
          ticket.current_user = agent

          ticket.will_be_saved_by(agent)
        end

        describe 'a new ticket is created' do
          let(:ticket) { ticket_new(agent) }

          before { Rails.logger.stubs(:info) }

          it 'enqueues a job to associate the ticket with matching att vals' do
            TicketAttributeValuesSetterJob.expects(:enqueue).with(
              ticket.account_id,
              has_entries(
                ticket_id: is_a(Integer),
                attribute_value_ids: ['1', '2'],
                audit_id: is_a(Integer)
              )
            )

            ticket.save!
          end

          describe_with_arturo_disabled :log_attribute_values_assignment do
            it 'does not log information about the attribute values' do
              Rails.
                logger.
                expects(:info).
                with(regexp_matches(/Investigate attribute values assignment/)).
                never

              ticket.save!
            end
          end

          describe_with_arturo_enabled :log_attribute_values_assignment do
            it 'logs information about the attribute values' do
              Rails.
                logger.expects(:info).
                with(regexp_matches(/Investigate attribute values assignment/)).
                once

              ticket.save!
            end
          end
        end

        describe 'and an existing ticket is updated' do
          let(:ticket) { tickets(:minimum_1) }

          before { ticket.save! }

          before_should 'not associate the ticket with attribute values' do
            Zendesk::Deco::Client.any_instance.expects(:post).never
          end
        end
      end

      describe_with_arturo_disabled :skill_based_attribute_ticket_mapping do
        before do
          ticket.comment      = matching_comment
          ticket.current_user = agent

          ticket.will_be_saved_by(agent)
        end

        describe 'and a new ticket is created' do
          let(:ticket) { ticket_new(agent) }

          before { ticket.save! }

          before_should 'not associate the ticket with attribute values' do
            TicketAttributeValuesSetterJob.expects(:enqueue).never
          end
        end

        describe 'and an existing ticket is updated' do
          let(:ticket) { tickets(:minimum_1) }

          before { ticket.save! }

          before_should 'not associate the ticket with attribute values' do
            TicketAttributeValuesSetterJob.expects(:enqueue).never
          end
        end
      end
    end
  end

  describe '#assign_attribute_values' do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }

    let(:attri_map_1) { new_attribute_ticket_map(attribute_value_id: '1') }
    let(:attri_map_2) { new_attribute_ticket_map(attribute_value_id: '2') }
    let(:attri_map_3) { new_attribute_ticket_map(attribute_value_id: '3') }

    before do
      Zendesk::Rules::Match.stubs(:match?).with(attri_map_1, ticket).returns(true)
      Zendesk::Rules::Match.stubs(:match?).with(attri_map_2, ticket).returns(false)
      Zendesk::Rules::Match.stubs(:match?).with(attri_map_3, ticket).returns(true)
    end

    describe "when the ticket is not closed" do
      before do
        ticket.stubs(status_id: StatusType.OPEN)
      end

      it 'returns the correct attribute_maps' do
        assert_equal(ticket.assign_attribute_values, [attri_map_1, attri_map_3])
      end
    end

    describe "when the ticket is closed" do
      before do
        ticket.stubs(status_id: StatusType.CLOSED)
        ticket.assign_attribute_values
      end

      it 'does not to run routing rules' do
        assert_equal 1, ticket.errors.full_messages.length
        assert_equal "Status: closed prevents ticket update", ticket.errors.full_messages[0]
      end
    end
  end

  describe '#save_manually_set_attribute_values' do
    let(:user) { users(:minimum_admin) }
    let(:ticket) { ticket_create(user).tap { |t| t.will_be_saved_by(user) } }

    describe_with_arturo_enabled :skill_based_ticket_routing do
      describe 'no attribute value IDs set' do
        it 'does not save attribute values to Deco' do
          Zendesk::Deco::Client.expects(:new).never
          ticket.save!
        end
      end

      [
        [['abc', 'def'], ['abc', 'def']],
        [[], []],
        [nil, []]
      ].each do |att_value_ids, expected_result|
        describe "attribute value IDs set to #{att_value_ids}" do
          let (:deco_client) { stub('deco client') }
          let (:att_val_fixtures) do
            YAML.load_file(
              "#{Rails.root}/test/files/deco/attributes_and_values.yml"
            )['attributes']
          end
          let (:mock_response) { stub('response', body: {'items' => att_val_fixtures}) }

          before do
            Zendesk::Deco::Client.expects(:new).
              with(ticket.account).
              returns(deco_client)
            deco_client.expects(:post).
              with("types/10/instances/#{ticket.id}/bulk_instance_values",
                attribute_value_ids: expected_result).
              returns(mock_response)
            ticket.save!
            Timecop.freeze(Time.now + 30.days)
            ticket.will_be_saved_by(ticket.account.agents.first)
            ticket.attribute_value_ids = att_value_ids
          end

          it 'passes attribute value IDs to Deco' do
            ticket.save!
          end

          it 'adds a new audit event to ticket' do
            ticket.save!
            audit = ticket.reload.audits.find do |item|
              item.events.find do |event|
                event.is_a? AssociateAttValsEvent
              end
            end
            assert audit
            assert_equal Time.now.to_i, audit.created_at.to_i
            event = audit.events.find { |item| item.is_a? AssociateAttValsEvent }
            expected = att_val_fixtures.map do |att|
              att['values'].map do |att_val|
                { id: att_val['id'], name: att_val['name'], attribute_id: att_val['attribute_id'] }
              end
            end.flatten
            assert_equal expected, event.attribute_values
          end
        end

        it 'records to StatsD' do
          ticket.attribute_value_ids = ['abc', 'def']
          statsd_client.expects(:time).with('set_attribute_values')
          ticket.save!
        end
      end
    end

    describe_with_arturo_disabled :skill_based_ticket_routing do
      describe 'attribute value IDs set' do
        before do
          Zendesk::Deco::Client.expects(:new).never
          ticket.attribute_value_ids = ['abc', 'def']
        end

        it 'does not pass attribute value IDs to Deco' do
          ticket.save!
        end
      end
    end
  end

  describe "#application_logger" do
    let(:ticket) { ticket_new(users(:minimum_end_user)) }

    it "defaults to Rails.logger" do
      ticket.application_logger.must_equal Rails.logger
    end

    it "can be set to a different logger" do
      ticket.application_logger = Zendesk::Mail.logger
      ticket.application_logger.must_equal Zendesk::Mail.logger
      ticket.application_logger.wont_equal Rails.logger
    end
  end

  describe "#agent_as_end_user_for?" do
    let(:ticket) { tickets(:minimum_1) }
    let(:via_id) { ViaType.WEB_SERVICE }
    let(:via_reference_id) { ViaType.HELPCENTER }
    let(:is_agent) { true }
    let(:is_admin) { false }
    let(:user_id) { ticket.submitter_id }
    let(:user) { stub('user', id: user_id, is_agent?: is_agent, is_admin?: is_admin) }

    before do
      ticket.update_column(:via_id, via_id)
      ticket.update_column(:via_reference_id, via_reference_id)
    end

    describe "matches in via_id, via_reference_id, submitter_id, agent-ness" do
      it "returns true" do
        assert ticket.agent_as_end_user_for?(user)
      end
    end

    describe "matches in via_id (for WEB_FORM), via_reference_id, submitter_id, agent-ness" do
      let(:via_id) { ViaType.WEB_FORM }

      it "returns true" do
        assert ticket.agent_as_end_user_for?(user)
      end
    end

    describe "does not match via_id" do
      let(:via_id) { ViaType.RULE }

      it "returns false" do
        refute ticket.agent_as_end_user_for?(user)
      end
    end

    describe "does not match via_reference_id" do
      let(:via_reference_id) { nil }

      it "returns false" do
        refute ticket.agent_as_end_user_for?(user)
      end
    end

    describe "user is not agent" do
      let(:is_agent) { false }

      it "returns false" do
        refute ticket.agent_as_end_user_for?(user)
      end
    end

    describe "user is admin" do
      let(:is_admin) { true }

      it "returns false" do
        refute ticket.agent_as_end_user_for?(user)
      end
    end

    describe "user is not submitter" do
      let(:user_id) { ticket.submitter_id + 1 }

      it "returns false" do
        refute ticket.agent_as_end_user_for?(user)
      end
    end
  end

  describe "#agent_as_end_user?" do
    let(:ticket) { tickets(:minimum_1) }
    let(:via_id) { ViaType.WEB_SERVICE }
    let(:via_reference_id) { ViaType.HELPCENTER }
    let(:is_agent) { true }
    let(:is_admin) { false }

    before do
      ticket.update_column(:via_id, via_id)
      ticket.update_column(:via_reference_id, via_reference_id)
      ticket.submitter.stubs(:is_agent?).returns(is_agent)
      ticket.submitter.stubs(:is_admin?).returns(is_admin)
    end

    describe "matches in via_id, via_reference_id, submitter_id, agent-ness" do
      it "returns true" do
        assert ticket.agent_as_end_user?
      end
    end

    describe "matches in via_id (as WEB_FORM), via_reference_id, submitter_id, agent-ness" do
      let(:via_id) { ViaType.WEB_FORM }

      it "returns true" do
        assert ticket.agent_as_end_user?
      end
    end

    describe "does not match via_id" do
      let(:via_id) { ViaType.RULE }

      it "returns false" do
        refute ticket.agent_as_end_user?
      end
    end

    describe "does not match via_reference_id" do
      let(:via_reference_id) { nil }

      it "returns false" do
        refute ticket.agent_as_end_user?
      end
    end

    describe "submitter is not agent" do
      let(:is_agent) { false }

      it "returns false" do
        refute ticket.agent_as_end_user?
      end
    end

    describe "submitter is admin" do
      let(:is_admin) { true }

      it "returns false" do
        refute ticket.agent_as_end_user?
      end
    end
  end

  ['save', 'save!'].each do |method|
    describe "##{method}" do
      let(:ticket) { tickets(:minimum_1) }

      before { ticket.will_be_saved_by(users(:minimum_agent)) }

      describe 'when the ticket is not new' do
        before { ticket.send(method) }

        before_should 'not set the nice_id' do
          ticket.expects(:set_nice_id).never
        end
      end

      describe 'when the ticket is new' do
        let(:ticket) { Ticket.new(account: accounts(:minimum)) }

        it 'sets the nice_id' do
          ticket.description = 'new ticket'
          next_nice_id = ticket.account.nice_id_sequence.peek
          ticket.send(method)

          assert_equal ticket.nice_id, next_nice_id
        end
      end

      describe 'when tracing' do
        let(:active_span)      { stub(:active_span) }
        let(:parent_span)      { stub(:parent_span) }
        let(:grandparent_span) { stub(name: 'rack.request') }
        let(:tag_args) do
          ["zendesk.ticket.save#{'_bang' if method == 'save!'}.time", 123.4]
        end

        before { Datadog.tracer.stubs(:active_span).returns(active_span) }

        describe_with_arturo_disabled :instrument_ticket_save do
          before { ticket.send(method) }

          before_should 'not try to instrument ticket save' do
            ticket.expects(:rack_request_span).never
          end
        end

        describe_with_arturo_enabled :instrument_ticket_save do
          before { Benchmark.stubs(:ms).returns(tag_args.last).yields }

          describe 'when the Datadog `active_span` is defined' do
            before { active_span.stubs(parent: parent_span) }

            describe 'and the parent span is defined' do
              before { parent_span.stubs(parent: grandparent_span) }

              describe 'and the grandparent span is defined' do
                describe 'and the grandparent span name is `rack.reqeust`' do
                  before { ticket.send(method) }

                  before_should 'set appropriate tags' do
                    grandparent_span.stubs(:set_tag).with { |args| args != tag_args }
                    grandparent_span.expects(:set_tag).with(*tag_args)
                  end
                end

                describe 'and the grandparent span name is not `rack.request`' do
                  let(:grandparent_span) { stub(name: 'something') }
                  before { ticket.send(method) }

                  before_should 'not try to set tags' do
                    grandparent_span.expects(:set_tag).with(*tag_args).never
                  end
                end
              end

              describe 'and the grandparent span is not defined' do
                let(:grandparent_span) { nil }

                it 'does not try to determine the span name' do
                  ticket.send(method)
                end
              end
            end

            describe 'and the parent span is not defined' do
              let(:parent_span) { nil }

              it 'does not try to access grandparent span' do
                ticket.send(method)
              end
            end
          end

          describe 'when the Datadog `active_span` is not defined' do
            let(:active_span) { nil }

            it 'does not try to access the parent span' do
              ticket.send(method)
            end
          end
        end
      end
    end
  end
end
