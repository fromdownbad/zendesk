require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe TrialCoupon do
  before do
    @coupon = FactoryBot.create(:trial_coupon)
  end

  it 'has a #type of "TrialCoupon"' do
    assert_equal 'TrialCoupon', @coupon.type
  end

  should_validate_presence_of :trial_plan_type

  should validate_presence_of :trial_max_agents
  should validate_numericality_of :trial_max_agents

  describe 'validations' do
    it 'requires #trial_plan_type to be a valid plan_type' do
      SubscriptionPlanType.fields.each do |plan_type|
        @coupon.update_attributes! trial_plan_type: plan_type
      end

      assert_number_fails_validation @coupon, :trial_plan_type, 0
      assert_number_fails_validation @coupon, :trial_plan_type, SubscriptionPlanType.fields.max + 1
    end

    it 'requires trial_max_agents to be greater than zero' do
      assert_validates_range @coupon, :trial_max_agents, 0
    end
  end

  def assert_validates_range(subject, attr, min = nil, max = nil)
    subject.update_attributes! attr => (min.nil? ? max - 100 : min)
    subject.update_attributes! attr => (max.nil? ? min + 100 : max)
    subject.update_attributes! attr => (min + (max - min) / 2) unless min.nil? || max.nil?

    assert_number_fails_validation(subject, attr, min - 1) unless min.nil?
    assert_number_fails_validation(subject, attr, max + 1) unless max.nil?
  end
end
