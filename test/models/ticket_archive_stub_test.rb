require_relative '../support/test_helper'

SingleCov.covered!

describe TicketArchiveStub do
  fixtures :tickets, :accounts, :users, :ticket_fields

  let(:account) { accounts(:minimum) }
  let(:bar) { stub(inc: true, finish: true) }

  before do
    @ticket = tickets(:minimum_3)
    @ticket.add_comment(body: 'hai, friends', is_public: true)
    @ticket.ticket_field_entries.build(ticket_field: ticket_fields(:field_tagger_custom), value: 'booring')
    @ticket.will_be_saved_by(@ticket.requester)
    @ticket.save!
  end

  describe 'saving without delete' do
    it 'does not add deletion audit' do
      archive_and_delete @ticket
      refute @ticket.reload.events.any? { |event| event.value == StatusType.DELETED }
    end
  end

  describe 'account level auditing' do
    before do
      archive_and_delete @ticket
      @archived_ticket = Ticket.find(@ticket.id)
      @archived_ticket.will_be_saved_by(User.find(-1))
      CIA.audit actor: User.system do
        @archived_ticket.soft_delete
      end
    end

    it 'records an account level delete audit' do
      assert CIA::Event.find_by_source_id_and_action(@ticket.id, 'destroy')
    end

    it 'records an account level update for status_id' do
      assert CIA::Event.find_by_source_id_and_action(@ticket.id, 'update')
    end
  end

  describe 'soft deleting' do
    before do
      Timecop.freeze
      archive_and_delete @ticket
      @archived_ticket = Ticket.find(@ticket.id)
      @as_archived = @archived_ticket.as_archived

      Timecop.travel(30.minutes)
      Timecop.freeze # travel unfreezes

      @time_of_delete = Time.now.to_i
      @archived_ticket.will_be_saved_by(User.find(-1))
      @archived_ticket.soft_delete

      @deleted_archived_ticket = Ticket.with_deleted { account.tickets.find_by_id(@archived_ticket.id) }
    end

    it 'updates the generated_timestamp of the stub' do
      stub = TicketArchiveStub.find(@ticket.id)
      assert_equal @time_of_delete, stub.generated_timestamp.to_i
    end

    it 'updates the generated_timestamp of the ticket' do
      assert_equal @time_of_delete, @deleted_archived_ticket.generated_timestamp.to_i
    end

    it 'updates the updated_at' do
      assert_equal @time_of_delete, @deleted_archived_ticket.updated_at.to_i
    end

    it 'sets the ticket status to deleted' do
      assert_equal StatusType.DELETED, @deleted_archived_ticket.status_id
    end

    it 'adds the deletion events to the ticket' do
      assert @deleted_archived_ticket.audits.to_a.size > @archived_ticket.audits.to_a.size
      assert_equal StatusType.DELETED.to_s, @deleted_archived_ticket.events.sort_by(&:created_at).last.value
    end

    it 'maintains integrity of the archive record' do
      deleted_as_archived = @deleted_archived_ticket.as_archived
      (Ticket.archive_associations - ['events']).each do |assoc|
        assert_equal @as_archived[assoc], deleted_as_archived[assoc]
      end

      updated_attributes = %w[generated_timestamp status_id updated_at]
      @ticket.attributes.reject { |key| updated_attributes.include?(key) }.each do |(attr, _)|
        assert_equal_with_nil @as_archived[attr], deleted_as_archived[attr]
      end
    end

    describe 'soft undeleting' do
      before do
        @time_of_undelete = Time.now.to_i
        @deleted_archived_ticket.soft_undelete!
        @restored_ticket = Ticket.find(@deleted_archived_ticket.id)
        @restored_ticket_archive_stub = TicketArchiveStub.find(@restored_ticket.id)
      end

      it 'sets the ticket archive stub status to closed' do
        assert_equal StatusType.CLOSED, @restored_ticket_archive_stub.status_id
      end

      it 'updates the generated_timestamp of the stub' do
        assert_equal @time_of_undelete, @restored_ticket_archive_stub.generated_timestamp.to_i
      end

      it 'updates the updated_at of the stub' do
        assert_equal @time_of_undelete, @restored_ticket_archive_stub.updated_at.to_i
      end

      it 'sets the ticket status to closed' do
        assert_equal StatusType.CLOSED, @restored_ticket.status_id
      end

      it 'adds the undeletion event to the archived ticket' do
        assert @restored_ticket.audits.to_a.size > @deleted_archived_ticket.audits.to_a.size
        assert_equal StatusType.CLOSED.to_s, @restored_ticket.events.sort_by(&:created_at).last.value
      end
    end
  end

  describe '#deleted' do
    before do
      @ticket.will_be_saved_by(User.find(-1))
      @ticket.soft_delete
      archive_and_delete @ticket
    end

    it 'returns all ticket archive stubs that have been deleted' do
      TicketArchiveStub.deleted.count.must_equal 1
      TicketArchiveStub.deleted.first.nice_id.must_equal @ticket.nice_id
    end
  end

  describe '#remove_from_archive' do
    before do
      archive_and_delete @ticket
      @tas_stub = TicketArchiveStub.find(@ticket.id)
    end

    around do |test|
      with_in_memory_archive_router(test)
    end

    it 'calls delete via the router based archiver backend store' do
      ZendeskArchive.router.expects(:delete).with(@tas_stub)
      @tas_stub.remove_from_archive
    end
  end

  describe '#exists_in_archive' do
    before do
      archive_and_delete @ticket
      @tas_stub = TicketArchiveStub.find(@ticket.id)
    end

    it 'checks stub existance via the router based archiver backend store' do
      ZendeskArchive.router.expects(:exists_in_any_store?).with(@tas_stub)
      @tas_stub.exists_in_archive?
    end
  end
end
