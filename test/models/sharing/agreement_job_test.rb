require_relative "../../support/job_helper"
require 'zendesk/radar_factory'

SingleCov.covered! uncovered: 1

describe 'Sharing::AgreementJob' do
  fixtures :accounts

  let(:account)   { accounts(:minimum) }
  let(:response)  { stub(message: "foo".to_json) }
  let(:external_domain) { "yoda.atlassian.net" }
  let(:internal_domain) { "foo." + Zendesk::Net.config.fetch(:zendesk_base_domain) }
  let(:external_agreement) do
    Sharing::Agreement.new(
      account:        account,
      remote_url:     "https://#{external_domain}/",
      shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:jira]
    )
  end
  let(:internal_agreement) do
    Sharing::Agreement.new(
      account:        account,
      remote_url:     "https://#{internal_domain}/sharing",
      shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk]
    )
  end

  before do
    # stub radar
    stub_request(:post, "https://minimum.zendesk-test.com/node/radar/status").
      with(body: "{\"accountName\":\"minimum\",\"scope\":\"invite\",\"key\":\"response\",\"value\":null}",
           headers: {'Accept' => '*/*', 'Content-Type' => 'application/json', 'User-Agent' => 'Ruby'}).to_return(status: 200, body: "", headers: {})
    stub_request(:post, "https://#{internal_domain}/sharing/agreements/").
      with(body: "{\"receiver_url\":\"https://#{internal_domain}/sharing\",\"sender_url\":\"https://minimum.zendesk-test.com/sharing\",\"status\":\"\",\"uuid\":null,\"access_key\":null,\"name\":\"Minimum account @ Zendesk\",\"current_actor\":null,\"sync_tags\":null,\"sync_custom_fields\":null,\"allows_public_comments\":null}",
           headers: {'Accept' => 'application/json', 'Content-Type' => 'application/json', 'User-Agent' => "Faraday v#{Faraday::VERSION}"}).
      to_return(status: 200, body: "", headers: {})
    external_agreement.stubs(save: true)
    internal_agreement.stubs(save: true)
  end

  describe "when response is successful" do
    before do
      response.stubs(status: 200)
    end

    it "loads the agreement and call method passed in" do
      internal_agreement.expects(:send_to_partner).returns(response)
      Sharing::Agreement.expects(:find_by_id).with(1).returns(internal_agreement)
      notification = mock('notification')
      ::RadarFactory.expects(:create_radar_notification).with(account, "invite/#{internal_domain}").returns(notification)
      notification.expects(:send).with("response", code: 200)

      Sharing::AgreementJob.work(internal_agreement.account_id, :send_to_partner, 1)
    end

    it "includes custom port in radar key" do
      external_agreement.expects(:send_to_partner).returns(response)
      external_agreement.remote_url = "https://yoda.atlassian.net:8080/sharing"
      Sharing::Agreement.expects(:find_by_id).with(1).returns(external_agreement)
      notification = mock('notification')
      ::RadarFactory.expects(:create_radar_notification).with(account, "invite/#{external_domain}:8080").returns(notification)
      notification.expects(:send).with("response", code: 200)

      Sharing::AgreementJob.work(external_agreement.account_id, :send_to_partner, 1)
    end
  end

  it "marks sharing agreement in a failed state if invite response is not 200 or 201" do
    Sharing::Agreement.expects(:find_by_id).with(1).returns(external_agreement)
    response.stubs(status: 404)
    external_agreement.expects(:send_to_partner).returns(response)
    notification = mock('notification')
    ::RadarFactory.expects(:create_radar_notification).with(account, "invite/#{external_domain}").returns(notification)
    notification.expects(:send).with("response", code: 404)

    Sharing::AgreementJob.work(external_agreement.account_id, :send_to_partner, 1)
    assert_equal Sharing::Agreement::STATUS_MAP[:failed], external_agreement.status_id
  end

  it "marks agreement in failed state and return 500 to radar if too many redirects" do
    Sharing::Agreement.expects(:find_by_id).with(1).returns(external_agreement)
    external_agreement.expects(:send_to_partner).raises(TicketSharing::TooManyRedirects)
    notification = mock('notification')
    ::RadarFactory.expects(:create_radar_notification).with(account, "invite/#{external_domain}").returns(notification)
    notification.expects(:send).with("response", code: 500, errors: [['base', 'Too many redirects occurred while sending the ticket sharing agreement']])

    Sharing::AgreementJob.work(external_agreement.account_id, :send_to_partner, 1)
    assert_equal Sharing::Agreement::STATUS_MAP[:failed], external_agreement.status_id
  end

  it "calls the method that was passed in" do
    Sharing::Agreement.expects(:find_by_id).with(1).returns(internal_agreement)
    internal_agreement.expects(:update_partner).once
    ::RadarFactory.expects(:create_radar_notification).never

    Sharing::AgreementJob.work(internal_agreement.account_id, :update_partner, 1)
  end

  it "does not call random methods" do
    Sharing::Agreement.expects(:find_by_id).with(1).returns(internal_agreement)
    internal_agreement.expects(:foo).never
    ::RadarFactory.expects(:create_radar_notification).never

    Sharing::AgreementJob.work(internal_agreement.account_id, :foo, 1)
  end

  it "validates method passed in" do
    assert(Sharing::AgreementJob.valid_method?('update_partner'))
    assert_equal false, Sharing::AgreementJob.valid_method?('foo')
  end

  it "accepts optional audit parameter" do
    Sharing::AgreementJob.work(internal_agreement.account_id, :foo, 1, "foo")
  end
end
