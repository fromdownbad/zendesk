require_relative "../../support/job_helper"
require_relative "../../support/sharing_test_helper"

SingleCov.covered! uncovered: 3

describe 'Sharing::TicketJob' do
  include SharingTestHelper
  fixtures :accounts, :subscriptions

  before do
    @account = accounts(:minimum)
    @spoke = accounts(:minimum)
  end

  it 'should work' do
    ticket = SharedTicket.new
    ticket.agreement = valid_agreement(status: :pending)
    SharedTicket.expects(:find_by_id).with(1).returns(ticket)
    ticket.expects(:send_to_partner).returns(200)
    Sharing::TicketJob.work(@account.id, :send_to_partner, 1)
  end

  it 'should call valid methods' do
    ticket = SharedTicket.new
    ticket.agreement = valid_agreement(status: :pending)
    SharedTicket.expects(:find_by_id).with(1).returns(ticket)
    ticket.expects(:update_partner).returns(200)
    Sharing::TicketJob.work(@account.id, :update_partner, 1)
  end

  it 'should raise on 404 for recently created ticket' do
    Timecop.freeze
    ticket = SharedTicket.new
    ticket.created_at = Time.now
    ticket.agreement = valid_agreement(status: :pending)
    SharedTicket.expects(:find_by_id).with(1).returns(ticket)
    ticket.expects(:update_partner).returns(404)
    assert_raise RuntimeError do
      Sharing::TicketJob.work(@account.id, :update_partner, 1)
    end
  end

  describe 'on a 500+ response code' do
    before do
      @ticket = SharedTicket.new
      @ticket.agreement = valid_agreement(status: :pending)
      SharedTicket.expects(:find_by_id).with(1).returns(@ticket)
      @ticket.expects(:update_partner).returns(503)
    end

    it 'raises for zendesk account' do
      @ticket.agreement.expects(:remote_account).returns(@spoke)
      assert_raise RuntimeError do
        Sharing::TicketJob.work(@account.id, :update_partner, 1)
      end
    end

    it 'does nothing for external account' do
      @ticket.agreement.expects(:remote_account).returns(nil)
      Sharing::TicketJob.work(@account.id, :update_partner, 1)
    end
  end

  it 'should raise on random methods' do
    ticket = SharedTicket.new
    ticket.agreement = valid_agreement(status: :pending)
    SharedTicket.expects(:find_by_id).with(1).returns(ticket)
    assert_raise RuntimeError do
      ticket.expects(:foo).never
      Sharing::TicketJob.work(@account.id, :foo, 1)
    end
  end

  it 'should raise if ticket does not exist' do
    SharedTicket.stubs(:find_by_id).returns(nil)
    assert_raises(RuntimeError) do
      Sharing::TicketJob.work(@account.id, :send_to_partner, 1)
    end
  end

  it 'a valid method should be valid' do
    assert Sharing::TicketJob.valid_method?('send_to_partner')
  end

  it 'a random method should not be valid' do
    refute Sharing::TicketJob.valid_method?('foo')
  end

  it 'a shared ticket with no agreement should skip job and not raise an exception' do
    ticket = SharedTicket.new
    ticket.agreement = nil
    Sharing::TicketJob.valid_method?('send_to_partner')
  end

  it 'an account with the skip_ticket_sharing_jobs arturo should skip job and not raise an exception' do
    Account.expects(:find).with(@account.id).returns(@account)
    ticket = SharedTicket.new
    ticket.agreement = valid_agreement(status: :accepted)
    SharedTicket.expects(:find_by_id).with(1).returns(ticket)
    @account.expects(:has_skip_ticket_sharing_jobs?).returns(true)
    @account.subscription.stubs(:hub_account).returns(nil)
    SharedTicket.any_instance.expects(:send_to_partner).never
    Sharing::TicketJob.work(@account.id, :send_to_partner, 1)
  end

  it 'a spoke account that has a hub with skip_ticket_sharing_jobs should skip the job and not raise an exception' do
    Account.expects(:find).with(@spoke.id).returns(@spoke)
    @spoke.subscription.expects(:hub_account).returns(@account)
    @account.expects(:has_skip_ticket_sharing_jobs?).returns(true)
    ticket = SharedTicket.new
    ticket.agreement = valid_agreement(status: :accepted)
    SharedTicket.expects(:find_by_id).with(1).returns(ticket)
    SharedTicket.any_instance.expects(:send_to_partner).never
    Sharing::TicketJob.work(@spoke.id, :send_to_partner, 1)
  end

  it 'a shared ticket that has been deleted and tries to update partner or send to partner' do
    SharedTicket.any_instance.stubs(deleted_at: Time.now)
    ticket = SharedTicket.new
    ticket.agreement = valid_agreement(status: :pending)
    SharedTicket.expects(:find_by_id).with(1).returns(ticket).twice
    ticket.expects(:update_partner).never
    Sharing::TicketJob.work(@account.id, :update_partner, 1)
    Sharing::TicketJob.work(@account.id, :send_to_partner, 1)
  end
end
