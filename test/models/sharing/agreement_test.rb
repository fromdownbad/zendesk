require_relative "../../support/test_helper"
require_relative "../../support/sharing_test_helper"

SingleCov.covered! uncovered: 7

describe 'Sharing::Agreement' do
  include SharingTestHelper

  fixtures :accounts, :tickets, :events, :subscriptions, :payments,
    :remote_authentications, :sequences, :ticket_fields, :users,
    :groups, :memberships, :account_property_sets, :brands, :routes

  resque_inline false

  let(:num_after_commit_enqueues) { 3 }

  before do
    Account.any_instance.stubs(:default_brand).returns(brands('minimum'))
  end

  it 'agreement with pending status is pending?' do
    agreement = valid_agreement(status: :pending)
    assert agreement.pending?
  end

  it 'agreement with accepted status is accepted?' do
    agreement = valid_agreement(status: :accepted)
    assert agreement.accepted?
  end

  it 'agreement with inactive status is inactive? and deletable?' do
    agreement = valid_agreement(status: :inactive)
    assert agreement.inactive?
    assert agreement.deletable?
  end

  it 'agreement with declined status is declined? and cancellable?' do
    agreement = valid_agreement(status: :declined)
    assert agreement.declined?
    assert agreement.cancellable?
  end

  it 'agreement with failed status is deletable?' do
    agreement = valid_agreement(status: :failed)
    assert agreement.deletable?
  end

  it 'agreement with configuration_error status is error and cancellable?' do
    agreement = valid_agreement(status: :configuration_error)
    assert agreement.error?
    assert agreement.cancellable?
  end

  it 'agreement with ssl_error status is error? and cancellable?' do
    agreement = valid_agreement(status: :ssl_error)
    assert agreement.error?
    assert agreement.cancellable?
  end

  it 'should not be susceptible to mysql coercion exploits' do
    FactoryBot.create(:agreement, uuid: 'asdf1234', access_key: 'fdsa4321')

    assert_nil Sharing::Agreement.find_by_uuid(0)
    assert_nil Sharing::Agreement.find_by_uuid_and_access_key(0, 0)
  end

  it 'agreement should allow public comments' do
    assert Sharing::Agreement.new.allows_public_comments?
  end

  it 'expired accounts should not allow partner updates' do
    agreement = valid_agreement(status: :pending)
    agreement.account.expects(:is_serviceable?).returns(false)
    agreement.status = :inactive
    refute agreement.allows_partner_updates?
  end

  it 'inactive agreements should allow partner updates' do
    agreement = valid_agreement(status: :pending)
    refute agreement.allows_partner_updates?

    agreement.status = :inactive
    assert agreement.allows_partner_updates?
  end

  it 'setting a subdomain that does exist' do
    agreement = valid_agreement(account: accounts(:minimum))
    agreement.remote_url = nil
    agreement.subdomain = accounts(:support).subdomain
    assert agreement.valid?
  end

  it 'setting a subdomain that does not exist' do
    account = accounts(:minimum)
    agreement = Sharing::Agreement.new(account: account)
    agreement.subdomain = 'nonexistentsubdomain'
    refute agreement.valid?
  end

  it 'should add a translated remote_url error message if remote_url is invalid' do
    account = accounts(:minimum)
    agreement = Sharing::Agreement.new(account: account)
    agreement.subdomain = accounts(:support).subdomain
    agreement.remote_url = 'invalidremoteurl'
    agreement.valid?
    assert_includes agreement.errors.full_messages, "Remote url:  is invalid"
  end

  it 'should not add an error for remote_url if subdomain is invalid' do
    account = accounts(:minimum)
    agreement = Sharing::Agreement.new(account: account)
    agreement.subdomain = 'nonexistentsubdomain'
    agreement.valid?
    assert_equal 1, agreement.errors.messages.count
    assert_includes agreement.errors, I18n.t('activerecord.attributes.sharing.agreement.subdomain').to_sym
    refute_includes agreement.errors, I18n.t('activerecord.attributes.sharing.agreement.remote_url').to_sym
  end

  it 'should set name from remote_url if not explicitly provided' do
    account = accounts(:minimum)
    agreement = Sharing::Agreement.new(account: account, remote_url: "dogfood.com")
    agreement.save!
    assert_equal "dogfood.com", agreement.name
  end

  it 'should raise error if uri is invalid' do
    account = accounts(:minimum)
    agreement = Sharing::Agreement.new(account: account, remote_url: "http://")
    refute agreement.valid?
    assert_equal "Uri: is invalid", agreement.errors.full_messages.first
  end

  it 'should raise error if uri is has no host' do
    account = accounts(:minimum)
    agreement = Sharing::Agreement.new(account: account, remote_url: "http:///sharing")
    refute agreement.valid?
    assert_equal "Uri: is invalid", agreement.errors.full_messages.first
  end

  it 'should set name from subdomain' do
    account = accounts(:minimum)
    agreement = Sharing::Agreement.new(account: account, subdomain: "dogfood")
    Account.expects(:find_by_subdomain).returns(accounts(:billing))
    agreement.save!
    assert_equal "dogfood @ Zendesk", agreement.name
  end

  it 'should allow setting name for third party sharing agreements' do
    account = accounts(:minimum)
    agreement = Sharing::Agreement.new(account: account, remote_url: "dogfood.com", name: "Dog Food")
    agreement.save!
    assert_equal "Dog Food", agreement.name
  end

  it 'setting a subdomain that points to your own zendesk' do
    account = accounts(:minimum)
    agreement = valid_agreement(account: account)

    agreement.remote_url = nil
    agreement.subdomain = account.subdomain
    refute agreement.valid?
  end

  describe "Given an account with ssl whitelisting" do
    before do
      account = accounts(:minimum)
      account.expects(:has_ticket_sharing_whitelist?).returns(true)
      @agreement = valid_agreement(account: account)
    end

    describe "When building an agreement for our partner" do
      before do
        @ts_agreement = @agreement.for_partner
      end

      it "uses and agreement that does not verify ssl certificates" do
        assert_equal TicketSharing::NonVerifyAgreement, @ts_agreement.class
      end
    end
  end

  describe 'Given an active outgoing agreement' do
    before do
      agreement = FactoryBot.create(
        :agreement,
        remote_url: 'example.com', status: :accepted, direction: :out
      )
      assert agreement.valid?

      @agreement2 = FactoryBot.build(
        :agreement,
        remote_url: 'example.com', status: :accepted, direction: :out
      )
    end

    describe 'An active agreement with the same partner' do
      before { @agreement2.status = :accepted }
      it('be invalid') { assert !@agreement2.valid? }
    end

    describe 'A pending agreement with the same partner' do
      before { @agreement2.status = :pending }
      it('be invalid') { assert !@agreement2.valid? }
    end

    describe 'An inactive agreement with the same partner' do
      before { @agreement2.status = :inactive }
      it('be valid') { assert @agreement2.valid? }
    end

    describe 'An incoming agreement with the same partner' do
      before { @agreement2.direction = :in }
      it('be valid') { assert @agreement2.valid? }
    end
  end

  it 'setting a remote_url without a subdomain should be valid' do
    agreement = valid_agreement(subdomain: nil)
    agreement.remote_url = 'http://example.com/sharing'
    assert agreement.valid?
  end

  it 'setting a remote_url with just a domain should create a sane remote_url for sharing' do
    agreement = valid_agreement(subdomain: nil)
    agreement.remote_url = 'dogs.com'
    agreement.save

    assert_equal 'http://dogs.com/sharing', agreement.remote_url
  end

  it "setting subdomain should initially default to subdomain @ Zendesk" do
    account = accounts(:minimum)
    agreement = FactoryBot.create(:agreement, account: account, subdomain: 'support')
    assert_equal("support @ Zendesk", agreement.name)
  end

  it 'uuid is required when an agreement is remote' do
    agreement = valid_agreement(created_type: :remote)

    agreement.uuid = nil
    refute agreement.valid?
  end

  it 'a locally created agreement should generate a uuid' do
    account = accounts(:minimum)
    agreement = Sharing::Agreement.new(account: account, remote_url: 'donkey.com')

    agreement.save!
    assert agreement.uuid.presence

    agreement.reload
    assert agreement.uuid.presence
  end

  it 'Setting an agreement to incoming' do
    agreement = Sharing::Agreement.new
    refute agreement.in?

    agreement.direction = :in
    assert agreement.in?

    agreement.direction = 'in'
    assert agreement.in?
  end

  it "is local when created *not* through the API" do
    agreement = Sharing::Agreement.new
    assert agreement.created_locally?

    agreement = Sharing::Agreement.new(created_type: :local)
    assert agreement.created_locally?
  end

  it 'Deactivating an agreement locally should set deactivated_locally? to true' do
    agreement = create_valid_agreement(status: :accepted)
    agreement.status = :inactive
    agreement.save!
    assert agreement.deactivated_locally?
  end

  describe 'Deactivating an agreement via the partner' do
    before do
      @agreement = create_valid_agreement(status: :accepted)
      ts_agreement = TicketSharing::Agreement.new('status' => 'inactive')
      @agreement.update_from_partner(ts_agreement)
    end

    it 'sets deactivated_locally? to false' do
      @agreement.save!
      refute @agreement.deactivated_locally?
    end

    it 'sends an e-mail' do
      ActionMailer::Base.perform_deliveries = true
      deliveries = ActionMailer::Base.deliveries = []
      assert_difference('deliveries.size', +1) { @agreement.save! }
      assert_match(/deactivated/i, deliveries.last.subject)
    end
  end

  describe 'Reactivating an agreement via the partner' do
    before do
      @agreement = create_valid_agreement(status: :inactive)
      ts_agreement = TicketSharing::Agreement.new('status' => 'accepted')
      @agreement.update_from_partner(ts_agreement)
    end

    it 'sends an e-mail' do
      ActionMailer::Base.perform_deliveries = true
      deliveries = ActionMailer::Base.deliveries = []
      assert_difference('deliveries.size', +1) { @agreement.save! }
      assert_match(/reactivated/i, deliveries.last.subject)
    end
  end

  it 'Only inactive agreements should be reactivatable' do
    agreement = create_valid_agreement(status: :active, deactivated_type: :local)
    refute agreement.reactivatable?

    agreement.status = :inactive
    assert agreement.reactivatable?
  end

  it 'A locally deactivated agreement should be reactivatable' do
    agreement = create_valid_agreement(status: :inactive, deactivated_type: :local)
    assert agreement.reactivatable?
  end

  it 'A remotely deactivated agreement should not be reactivatable' do
    agreement = create_valid_agreement(status: :inactive, deactivated_type: :remote)
    refute agreement.reactivatable?
  end

  describe 'Given a locally deactivated agreement' do
    before do
      @agreement = create_valid_agreement(status: :inactive, deactivated_type: :local)
    end

    describe 'When a partner tries to accept the agreement' do
      before do
        ts_agreement = TicketSharing::Agreement.new('status' => 'accepted')
        @agreement.update_from_partner(ts_agreement)
      end

      it 'does not allow the status to change to active' do
        refute @agreement.valid?
      end
    end
  end

  describe 'Given a remotely deactivated agreement' do
    before do
      @agreement = create_valid_agreement(status: :inactive, deactivated_type: :remote)
    end

    describe 'When you try to accept the agreement' do
      before do
        @agreement.status = :accepted
      end

      it 'does not allow the status to change to active' do
        refute @agreement.valid?
      end
    end
  end

  it "is not local when created through the API" do
    agreement = Sharing::Agreement.new(created_type: :remote)
    refute agreement.created_locally?
  end

  it 'Setting an agreement to outgoing' do
    agreement = Sharing::Agreement.new
    agreement.direction = :in
    refute agreement.out?

    agreement.direction = :out
    assert agreement.out?

    agreement.direction = 'out'
    assert agreement.out?
  end

  describe "Given the local creation of an agreement" do
    it "generates an access_key" do
      agreement = valid_agreement(created_type: :local)
      agreement.save!
      assert_not_nil(agreement.access_key)
    end

    it 'does not notify the owner of the account' do
      ActionMailer::Base.perform_deliveries = true
      agreement = valid_agreement(created_type: :local)
      refute_difference "ActionMailer::Base.deliveries.size" do
        agreement.save!
      end
    end
  end

  describe "#with_zendesk_account?" do
    it "returns true when partner is another zendesk account" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "http://foo.zendesk-test.com/sharing", shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk])
      assert agreement.with_zendesk_account?
    end

    # Some zendesk-zendesk agreements have been created with nil shared_with_id. See ZD#1755584
    it "returns true when partner is another zendesk account and shared_with_id is nil" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "http://support.zendesk-test.com/sharing", shared_with_id: nil)
      assert agreement.with_zendesk_account?
    end

    it "returns false when partner is JIRA" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "http://foo.zendesk-test.com/sharing", shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:jira])
      refute agreement.with_zendesk_account?
    end

    it "returns false when partner is third party" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "http://foo.zendesk-test.com/sharing", shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:jira])
      refute agreement.with_zendesk_account?
    end
  end

  describe "#zendesk_http_remote_url?" do
    it "returns true when zendesk account and remote_url is http" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "http://foo.zendesk-test.com/sharing", shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk])
      assert agreement.zendesk_http_remote_url?
    end

    it "returns true when zendesk account and remote_url is http but mentions https" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "http://https.zendesk-test.com/sharing", shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk])
      assert agreement.zendesk_http_remote_url?
    end

    it "returns true when zendesk account and remote_url is unknown and mentions https" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "ftp://https.zendesk-test.com/sharing", shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk])
      assert agreement.zendesk_http_remote_url?
    end

    it "returns false when non-zendesk account and remote_url is http" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "http://foo.zendesk-test.com/sharing", shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:jira])
      refute agreement.zendesk_http_remote_url?
    end

    it "returns false when zendesk account and remote_url is https" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "https://foo.zendesk-test.com/sharing", shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk])
      refute agreement.zendesk_http_remote_url?
    end

    it "returns false when non-zendesk account and remote_url is https" do
      agreement = Sharing::Agreement.new(account: accounts(:minimum), remote_url: "https://foo.zendesk-test.com/sharing", shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:jira])
      refute agreement.zendesk_http_remote_url?
    end
  end

  describe 'Given an account that rejects all sharing invites.' do
    before do
      @account = accounts(:minimum)
      @account.reject_sharing_invites!
      assert @account.reject_sharing_invites?
    end

    describe 'When receiving an agreement invite' do
      before do
        @agreement = valid_agreement(account: @account, created_type: :remote, direction: :in)
      end

      it "Enqueues an auto_decline job" do
        assert_difference 'Resque.jobs.size', +1 do
          @agreement.save
        end

        last_job = Resque.jobs.last
        assert_equal("Sharing::AgreementJob", last_job[:class])
        assert_equal([@account.id, :auto_decline, @agreement.id], last_job[:args][0..2])
      end

      it 'does not notify the owner of the account' do
        ActionMailer::Base.perform_deliveries = true
        refute_difference 'ActionMailer::Base.deliveries.size' do
          @agreement.save!
        end
      end
    end

    describe 'When sending an agreement invite' do
      before do
        @agreement = valid_agreement(account: @account, created_type: :local, direction: :out)
        assert @agreement.out?
      end

      it 'does not auto decline the sent invitation' do
        @agreement.expects(:enqueue_auto_decline).never
        @agreement.save!
      end
    end
  end

  describe 'auto decline' do
    before do
      @agreement = FactoryBot.create(:agreement)
      Resque.jobs.clear
      @agreement.auto_decline
    end

    it 'enqueues a partner update' do
      assert last_job = Resque.jobs.last
      assert_equal(:update_partner, last_job[:args][1])
    end

    it 'sets the deleted at flag' do
      assert @agreement.deleted?
    end
  end

  describe "Given the creation of a remote agreement" do
    it "does not enqueue a job" do
      agreement = valid_agreement(created_type: :remote)
      refute_difference "Resque.jobs.size" do
        agreement.save!
      end
    end

    it 'notifys the owner of the account' do
      ActionMailer::Base.perform_deliveries = true
      agreement = valid_agreement(created_type: :remote)
      assert_difference "ActionMailer::Base.deliveries.size", +1 do
        agreement.save!
      end
    end
  end

  describe 'Given a local agreement' do
    before { @agreement = create_valid_agreement }

    describe 'When updated' do
      before do
        @agreement.status = 'accepted'
        @agreement.save!
      end

      should_not_change('the access key') { @agreement.access_key }
    end
  end

  describe 'Generating an agreement for a partner' do
    before do
      @account = accounts(:minimum)
      @admin = users(:minimum_agent)
      @remote_url = 'http://example.com/sharing'
    end

    describe 'inward' do
      before do
        agreement = Sharing::Agreement.new(
          account: @account,
          local_admin: @admin,
          remote_url: @remote_url,
          direction: 'in',
          uuid: '<uuid>',
          access_key: '<access_key>',
          status: 'pending'
        )
        @partners_agreement = agreement.for_partner
      end

      it 'has the correct attributes' do
        assert_equal(@account.sharing_url, @partners_agreement.receiver_url)
        assert_equal(@remote_url, @partners_agreement.sender_url)
        assert_equal('<uuid>', @partners_agreement.uuid)
        assert_equal('<access_key>', @partners_agreement.access_key)
        assert_equal('pending', @partners_agreement.status)
        assert_equal(@account.sharing_name, @partners_agreement.name)
      end
    end

    describe 'outward' do
      before do
        @agreement = Sharing::Agreement.new(
          account: @account,
          local_admin: @admin,
          remote_url: @remote_url,
          direction: 'out',
          uuid: '1234',
          access_key: '<access_key>',
          status: 'pending'
        )
        @jira_suffix = Regexp.escape(Sharing::Agreement::JIRA_URL_SUFFIX)
      end

      it 'has the correct attributes' do
        @partners_agreement = @agreement.for_partner
        assert_equal(@account.sharing_url, @partners_agreement.sender_url)
        assert_equal(@remote_url, @partners_agreement.receiver_url)
        assert_equal('1234', @partners_agreement.uuid)
        assert_equal('<access_key>', @partners_agreement.access_key)
        assert_equal('pending', @partners_agreement.status)
        assert_equal(@account.sharing_name, @partners_agreement.name)
      end

      describe '#append_jira_url_suffix' do
        it "removes a trailing slash before appending" do
          @agreement.remote_url << '/'
          @agreement.shared_with_id = Sharing::Agreement::SHARED_WITH_MAP[:jira]
          @agreement.save
          assert_no_match(/\/#{@jira_suffix}/, @agreement.remote_url)
        end

        it "removes unnecessary space before appending" do
          @agreement.remote_url << "  "
          @agreement.shared_with_id = Sharing::Agreement::SHARED_WITH_MAP[:jira]
          @agreement.save
          assert_no_match(/ #{@jira_suffix}/, @agreement.remote_url)
        end

        it "does not append if suffix is already there" do
          @agreement.remote_url << Sharing::Agreement::JIRA_URL_SUFFIX
          @agreement.shared_with_id = Sharing::Agreement::SHARED_WITH_MAP[:jira]
          @agreement.save
          assert_no_match(/(#{@jira_suffix}){2,2}/, @agreement.remote_url)
        end

        it "appends #{Sharing::Agreement::JIRA_URL_SUFFIX} to remote_url" do
          @agreement.shared_with_id = Sharing::Agreement::SHARED_WITH_MAP[:jira]
          @agreement.save
          assert_match(/#{@jira_suffix}/, @agreement.remote_url)
        end
      end
    end

    describe 'When the agreement was created locally' do
      before { @agreement = valid_agreement(created_type: :local) }

      describe 'When the agreement is pending' do
        before { @agreement.status = :pending }

        it 'includes the terms' do
          partners_agreement = @agreement.for_partner
          assert_not_nil(partners_agreement.sync_tags)
          assert_not_nil(partners_agreement.sync_custom_fields)
          assert_not_nil(partners_agreement.allows_public_comments)
        end
      end

      describe 'When the agreement is not pending' do
        before { @agreement.status = :accepted }

        it 'does not include the agreement terms' do
          partners_agreement = @agreement.for_partner
          assert_nil(partners_agreement.sync_tags)
          assert_nil(partners_agreement.sync_custom_fields)
          assert_nil(partners_agreement.allows_public_comments)
        end
      end
    end

    describe 'When the agreement was created remotely' do
      before { @agreement = valid_agreement(created_type: :remote) }

      it 'does not include the agreement terms' do
        partners_agreement = @agreement.for_partner
        assert_nil(partners_agreement.sync_tags)
        assert_nil(partners_agreement.sync_custom_fields)
        assert_nil(partners_agreement.allows_public_comments)
      end
    end
  end

  it "sending an agreement to a partner" do
    agreement = valid_agreement
    url = agreement.remote_url + '/agreements/' + agreement.uuid
    stub_request(:post, url).to_return(status: 201)
    agreement.send_to_partner

    assert_requested(:post, url, body: hash_including(receiver_url: agreement.remote_url))
  end

  describe "resending an agreement (resend_invite)" do
    it "sets last_sent to the current time" do
      agreement = valid_agreement
      time = Time.now
      Time.stubs(:now).returns(time)
      agreement.resend_invite
      assert_equal time, agreement.last_sent
    end

    describe "twice within a minute" do
      it "does not send an agreement to a partner the second time" do
        agreement = valid_agreement
        url = agreement.remote_url + '/agreements/' + agreement.uuid
        stub_request(:post, url).to_return(status: 201)
        agreement.expects(:save!).once
        agreement.expects(:enqueue_job).with(:send_to_partner).once
        agreement.resend_invite
        agreement.resend_invite
      end
    end
  end

  it "updating a partner's agreement" do
    account = accounts(:minimum)
    admin = users(:minimum_admin)
    agreement = FactoryBot.create(
      :agreement,
      created_type: :local, local_admin: admin, account: account
    )
    assert_not_nil(agreement.access_key)

    url = agreement.remote_url + '/agreements/' + agreement.uuid
    stub_request(:put, url).to_return(status: 201)

    agreement.update_partner

    assert_requested(:put, url,
      body: hash_including("status" => agreement.status.to_s),
      headers: { "X-Ticket-Sharing-Token" => "#{agreement.uuid}:#{agreement.access_key}" })
  end

  it "sending an agreement to a broken url" do
    assert_raise ActiveRecord::RecordInvalid do
      FactoryBot.create(:agreement, name: "bad url", remote_url: "https://example.com /plugins/servlet/zendesk/sharing", uuid: "badurl")
    end
  end

  it "Sending an agreement to a partner, with a bad SSL Certificate" do
    Resolv::DNS.any_instance.stubs(:getaddress).with('bad-ssl-cert.com').returns(Resolv::IPv4.create('1.2.3.4'))
    agreement = FactoryBot.create(:agreement,
      remote_url: "https://bad-ssl-cert.com/sharing",
      uuid: "badcert")

    stub_request(:post, "https://bad-ssl-cert.com/sharing/agreements/badcert").
      to_raise(OpenSSL::SSL::SSLError)

    assert_equal(:pending, agreement.status)
    agreement.send_to_partner
    assert_equal(:ssl_error, agreement.status)
  end

  it "Communicating with a partner, with a bad SSL Certificate" do
    Resolv::DNS.any_instance.stubs(:getaddress).with('bad-ssl-cert.com').returns(Resolv::IPv4.create('1.2.3.4'))
    agreement = FactoryBot.create(:agreement,
      remote_url: "https://bad-ssl-cert.com/sharing",
      uuid: "badcert")

    stub_request(:put, "https://bad-ssl-cert.com/sharing/agreements/badcert").
      to_raise(OpenSSL::SSL::SSLError)

    assert_equal(:pending, agreement.status)
    agreement.update_partner
    assert_equal(:ssl_error, agreement.status)
  end

  it "Given a partner with a bad SSL Certificate, do not enqueue any jobs" do
    Resque.jobs.clear
    agreement = FactoryBot.create(:agreement, status: :ssl_error)

    agreement.enqueue_job(:send_to_partner)
    assert_equal(0, Resque.jobs.size)
  end

  it 'queues the expected jobs after accepting an agreement' do
    Resque.jobs.clear

    agreement = create_valid_agreement
    agreement.accepted = true

    assert_difference "Resque.jobs.size", +num_after_commit_enqueues - 1 do
      assert agreement.save!
    end

    assert_equal("Sharing::AgreementJob", Resque.jobs[-2][:class])
    assert_equal("TicketSharingSupportAddressesJob", Resque.jobs.last[:class])
    assert_equal([agreement.account_id, :update_partner, agreement.id],
      Resque.jobs[-2][:args][0..2])
  end

  it 'queues the expected jobs after declining a remotely created agreement' do
    Resque.jobs.clear

    agreement = create_valid_agreement(created_type: :remote)
    refute agreement.deleted?
    agreement.accepted = false

    assert_difference "Resque.jobs.size", +1 do
      assert agreement.save!
    end

    assert_equal(:declined, agreement.status)
    assert agreement.deleted?
  end

  # context "#destroy" do
  it 'deleting a locally created pending agreement' do
    agreement = create_valid_agreement(status: :pending, created_type: :local)
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries.clear

    assert_difference 'Resque.jobs.size', +2 do
      agreement.destroy
    end

    assert_empty(ActionMailer::Base.deliveries, 'should have no mail')
    assert agreement.deleted?
    assert_equal(:declined, agreement.status)
  end

  it 'deleting a locallly created accepted agreement' do
    agreement = create_valid_agreement(status: :accepted, created_type: :local)
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries.clear

    assert_difference 'Resque.jobs.size', +num_after_commit_enqueues do
      agreement.destroy
    end

    assert_empty(ActionMailer::Base.deliveries, 'should have no mail')
    assert agreement.deleted?
    assert_equal(:declined, agreement.status)
  end

  it 'deleting a locally created declined agreement' do
    agreement = create_valid_agreement(status: :declined, created_type: :local)

    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries.clear
    Resque.jobs.clear

    assert_difference 'Resque.jobs.size', +1 do
      agreement.destroy
    end

    assert_empty(ActionMailer::Base.deliveries, 'should have no mail')

    assert agreement.deleted?
    assert_equal(:declined, agreement.status)
  end

  it 'deleting a remotely created accepted agreement' do
    agreement = create_valid_agreement(status: :accepted, created_type: :remote)

    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries.clear
    Resque.jobs.clear

    assert_difference 'Resque.jobs.size', +num_after_commit_enqueues do
      agreement.destroy
    end

    assert_empty(ActionMailer::Base.deliveries, 'should have no mail')
    assert agreement.deleted?
    assert_equal(:declined, agreement.status)
  end

  it 'deleting a remotely created inactive agreement' do
    agreement = create_valid_agreement(status: :inactive, created_type: :remote)

    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries.clear
    Resque.jobs.clear

    assert_difference 'Resque.jobs.size', +2 do
      agreement.destroy
    end

    assert_equal false, ActionMailer::Base.deliveries.empty?
    assert(agreement.deleted?)
    assert_equal :declined, agreement.status
  end

  it 'deleting an agreement should soft delete all shared_tickets' do
    tickets = [tickets(:minimum_1), tickets(:minimum_2), tickets(:minimum_3)]

    agreement = create_valid_agreement(status: :accepted, created_type: :remote)

    Timecop.freeze(10.minutes.ago) do # bypass the update enqueue delay
      tickets.each do |ticket|
        agreement.shared_tickets << SharedTicket.new(account: agreement.account, ticket: ticket, agreement: agreement)
        ticket.reload
      end
    end

    assert tickets.all? { |ticket| ticket.shared_tickets.size == 1 }

    agreement.save!
    agreement.reload

    Resque.jobs.clear

    # 1 agreement job
    assert_difference 'Resque.jobs.size', +num_after_commit_enqueues do
      agreement.destroy
    end

    assert_includes Resque.jobs.map(&:values).flatten, "SharedTicketBulkDeleteJob"

    assert_equal "Sharing::AgreementJob", Resque.jobs[-2][:class]
    assert_equal :update_partner, Resque.jobs[-2][:args][1]
  end

  # context "#acceptable?" do
  it 'an agreement should not be acceptable if it is local' do
    agreement = create_valid_agreement
    assert agreement.acceptable?

    agreement.created_type = :local
    refute agreement.acceptable?
  end

  it 'an agreement should not be acceptable if its status is not pending' do
    agreement = create_valid_agreement
    assert agreement.acceptable?

    agreement.status = :accepted
    refute agreement.acceptable?
  end

  # context "#setup_from_partner" do
  it 'should be able to initialize from a TS::Agreement' do
    account = accounts(:minimum)
    account.host_mapping = "wherever.example.org"
    account.is_ssl_enabled = true
    account.save validate: false
    ts_agreement = TicketSharing::Agreement.new(
      'name' => 'Example Co.',
      'uuid' => '6c0bfe30',
      'sender_url' => 'http://example.com/sharing',
      'receiver_url' => 'https://wherever.example.org/sharing',
      'access_key' => 'the_access_key',
      'current_actor' => new_ticket_sharing_actor,
      'sync_tags' => true,
      'sync_custom_fields' => true,
      'allows_public_comments' => false
    )
    ar_agreement = account.sharing_agreements.build
    ar_agreement.setup_from_partner(ts_agreement)

    refute ar_agreement.created_locally?
    assert_equal(:pending, ar_agreement.status)
    assert_equal(ts_agreement.sender_url, ar_agreement.remote_url)
    assert ar_agreement.in?
    assert_equal('Example Co.', ar_agreement.name)
    assert_equal('6c0bfe30', ar_agreement.uuid)
    assert ar_agreement.valid?, ar_agreement.errors.to_xml
    assert_equal('the_access_key', ar_agreement.access_key)
    assert_equal('Actor 1', ar_agreement.remote_admin.name)

    # The default is false (as of 2011-03-17)
    # true here means we accepted the partners value
    assert(ar_agreement.sync_tags?)

    # The default is false
    # true here means we accepted the partners value
    assert(ar_agreement.sync_custom_fields?)

    # The default is true (as of 2011-03-17)
    # false here means we accepted the partners value
    assert_equal(false, ar_agreement.allows_public_comments?)
  end

  describe 'Given an agreement' do
    before do
      @agreement = create_valid_agreement(sync_tags: false, sync_custom_fields: false, allows_public_comments: false)
    end

    describe 'When a partner updates the agreement' do
      before do
        ts_agreement = new_ticket_sharing_agreement('sync_tags' => true, 'sync_custom_fields' => true, 'allows_public_comments' => true)
        @agreement.update_from_partner(ts_agreement)
      end

      it "Thens changes to the agreement's terms should not be allowed" do
        assert_equal(false, @agreement.sync_tags?)
        assert_equal(false, @agreement.sync_custom_fields?)
        assert_equal(false, @agreement.allows_public_comments?)
      end
    end
  end

  describe 'Given a local agreement' do
    before do
      account = accounts(:minimum)
      @agreement = FactoryBot.create(
        :agreement,
        account: account, status: 'pending',
        created_type: :local, direction: :out
      )
    end

    describe 'When a partner updates the status' do
      before do
        ts_agreement = TicketSharing::Agreement.new(
          'current_actor' => new_ticket_sharing_actor('name' => 'Remote Admin'),
          'status' => 'accepted',
          'name' => 'Example Co.'
        )
        @agreement.update_from_partner(ts_agreement)
      end

      it 'updates the name' do
        assert_equal('Example Co.', @agreement.name)
      end

      it 'updates the status' do
        assert_equal(:accepted, @agreement.status)
      end

      it 'notifys the owner' do
        ActionMailer::Base.perform_deliveries = true
        assert_difference "ActionMailer::Base.deliveries.size", +1 do
          @agreement.save!
        end
      end

      it 'adds the remote_admin' do
        assert_not_nil(@agreement.remote_admin)
        assert_equal('Remote Admin', @agreement.remote_admin.name)
      end
    end
  end

  describe 'Given a remote agreement that is pending' do
    before do
      @agreement = create_valid_agreement(created_type: :remote, status: :pending)
      @deliveries = ActionMailer::Base.deliveries = []
    end

    describe 'When a partner updates the status to declined' do
      before do
        ts_agreement = new_ticket_sharing_agreement('status' => 'declined')
        @agreement.update_from_partner(ts_agreement)
      end

      it 'sends an e-mail' do
        ActionMailer::Base.perform_deliveries = true
        assert_difference "@deliveries.size", +1 do
          @agreement.save!
        end
        assert_match(/revoked/i, @deliveries.last.subject)
      end

      it 'declines and delete the agreement' do
        assert_equal(:declined, @agreement.status)
        assert @agreement.deleted?
      end
    end
  end

  describe 'Given an outgoing agreement that is accepted' do
    before do
      @agreement = create_valid_agreement(created_type: :local, status: :accepted, direction: :out)
      @deliveries = ActionMailer::Base.deliveries = []
      tickets = [tickets(:minimum_1), tickets(:minimum_2), tickets(:minimum_3)]
      tickets.each do |ticket|
        @agreement.shared_tickets << SharedTicket.new(account: @agreement.account, ticket: ticket, agreement: @agreement)
        ticket.reload
      end
      Resque.jobs.clear
    end

    describe 'When a partner updates the status to declined' do
      before do
        ts_agreement = new_ticket_sharing_agreement('status' => 'declined')
        @agreement.update_from_partner(ts_agreement)
      end

      it 'sends an e-mail' do
        ActionMailer::Base.perform_deliveries = true
        assert_difference "@deliveries.size", +1 do
          @agreement.save!
        end
        assert_match(/deleted/i, @deliveries.last.subject)
      end

      it 'declines and deletes the agreement' do
        assert_equal(:declined, @agreement.status)
        assert @agreement.deleted?
      end

      it 'enqueues a job to soft deletes all shared_tickets' do
        assert_includes Resque.jobs.map(&:values).flatten, "SharedTicketBulkDeleteJob"
      end

      it 'does not enqueue a partner update' do
        refute_includes Resque.jobs.map(&:values).flatten, "Sharing::AgreementJob"
      end
    end
  end

  describe 'Given an incoming agreement that is accepted' do
    before do
      @agreement = create_valid_agreement(status: :accepted, direction: :in)
      @deliveries = ActionMailer::Base.deliveries = []
      tickets = [tickets(:minimum_1), tickets(:minimum_2), tickets(:minimum_3)]
      tickets.each do |ticket|
        @agreement.shared_tickets << SharedTicket.new(account: @agreement.account, ticket: ticket, agreement: @agreement)
        ticket.reload
      end
      Resque.jobs.clear
    end

    describe 'When a partner updates the status to declined' do
      before do
        ts_agreement = new_ticket_sharing_agreement('status' => 'declined')
        @agreement.update_from_partner(ts_agreement)
      end

      it 'sends an e-mail' do
        ActionMailer::Base.perform_deliveries = true
        assert_difference "@deliveries.size", +1 do
          @agreement.save!
        end
        assert_match(/deleted/i, @deliveries.last.subject)
      end

      it 'declines and deletes the agreement' do
        assert_equal(:declined, @agreement.status)
        assert @agreement.deleted?
      end

      it 'enqueues a job to soft deletes all shared_tickets' do
        assert_includes Resque.jobs.map(&:values).flatten, "SharedTicketBulkDeleteJob"
      end

      it 'does not enqueue a partner update' do
        refute_includes Resque.jobs.map(&:values).flatten, "Sharing::AgreementJob"
      end
    end
  end

  it 'A partner updating your local agreement (not status) should not notify the owner' do
    account = accounts(:minimum)
    agreement = FactoryBot.create(
      :agreement,
      account: account, status: 'pending',
      created_type: :local
    )

    ts_agreement = TicketSharing::Agreement.new('status' => 'pending')
    agreement.update_from_partner(ts_agreement)

    ActionMailer::Base.perform_deliveries = true
    refute_difference "ActionMailer::Base.deliveries.size" do
      agreement.save!
    end
  end

  describe "#generic_sharing_url" do
    it "uses the current host, not a hard-coded one" do
      url = Sharing::Agreement.new.generic_sharing_url('foo')
      assert_equal('https://foo.zendesk-test.com/sharing', url)
    end
  end

  describe "#retrieve_jira_projects" do
    before do
      remote_host = "https://test.com"
      @agreement = FactoryBot.build(:agreement, remote_url: remote_host)
      @remote_url = "#{remote_host}/info"
    end

    it "parses projects" do
      stub_request(:get, @remote_url).to_return(body: { projects: {foo: "bar"} }.to_json)
      assert_equal({ "foo" => "bar" }, @agreement.retrieve_jira_projects)
    end

    it "does not explode when something goes wrong" do
      stub_request(:get, @remote_url).to_return(body: "FOOO")
      assert_nil @agreement.retrieve_jira_projects
    end
  end

  describe "Given a SocketError during send_to_partner" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      @ticket = tickets(:minimum_1)
      @original_status = @agreement.status
    end

    describe "with a Temporary failure in name resolution exception message" do
      before do
        TicketSharing::Client.any_instance.stubs(:post).raises(SocketError.new("getaddrinfo: Temporary failure in name resolution"))
      end

      it "does not raise an exception and not change agreement status" do
        response = @agreement.send_to_partner
        assert response.is_a?(TicketSharing::FakeHttpResponse)
        assert_equal @original_status, @agreement.status
      end
    end

    describe "with a Name or service not known exception message" do
      before do
        TicketSharing::Client.any_instance.stubs(:post).raises(SocketError.new("getaddrinfo: Name or service not known"))
        ActionMailer::Base.perform_deliveries = true
        ActionMailer::Base.deliveries.clear
      end

      it "does not raise an exception and change agreement status and send an email" do
        response = @agreement.send_to_partner
        assert response.is_a?(TicketSharing::FakeHttpResponse)
        assert_equal :configuration_error, @agreement.status
        assert_equal 1, ActionMailer::Base.deliveries.size
      end
    end

    describe "with a cancelled status" do
      before do
        TicketSharing::Client.any_instance.stubs(:post).raises(SocketError)
        @agreement.status = :failed
      end

      it "stays cancelled" do
        @agreement.send_to_partner
        assert_equal :failed, @agreement.status
        assert @agreement.failed?
      end
    end
  end

  describe "Given a SocketError during update_partner" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      @ticket = tickets(:minimum_1)
      @original_status = @agreement.status
    end

    describe "with a Temporary failure in name resolution exception message" do
      before do
        TicketSharing::Client.any_instance.stubs(:put).raises(SocketError.new("getaddrinfo: Temporary failure in name resolution"))
      end

      it "does not raise an exception and not change agreement status" do
        response = @agreement.update_partner
        assert response.is_a?(TicketSharing::FakeHttpResponse)
        assert_equal @original_status, @agreement.status
      end
    end

    describe "with a Name or service not known exception message" do
      before do
        TicketSharing::Client.any_instance.stubs(:put).raises(SocketError.new("getaddrinfo: Name or service not known"))
        ActionMailer::Base.perform_deliveries = true
        ActionMailer::Base.deliveries.clear
      end

      it "does not raise an exception and change agreement status and send an email" do
        response = @agreement.update_partner
        assert response.is_a?(TicketSharing::FakeHttpResponse)
        assert_equal :configuration_error, @agreement.status
        assert_equal 1, ActionMailer::Base.deliveries.size
      end
    end

    describe "with a cancelled status" do
      before do
        TicketSharing::Client.any_instance.stubs(:put).raises(SocketError)
        @agreement.status = :failed
      end

      it "stays cancelled" do
        @agreement.update_partner
        assert_equal :failed, @agreement.status
        assert @agreement.failed?
      end
    end
  end

  describe "Given a Errno::ETIMEDOUT during update_partner" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      @ticket = tickets(:minimum_1)
      @original_status = @agreement.status
    end

    describe "with a Temporary failure in name resolution exception message" do
      before do
        Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(Errno::ETIMEDOUT)
      end

      it "does not raise an exception and retry a total of #{Zendesk::Net::MultiSSL::MAX_RETRIES} times" do
        ZendeskExceptions::Logger.expects(:record).once
        Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES)
        response = @agreement.update_partner
        assert response.is_a?(TicketSharing::FakeHttpResponse)
        assert_equal :pending, @agreement.status
      end
    end
  end

  describe "Given a Errno::ETIMEDOUT during send_to_partner" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      @ticket = tickets(:minimum_1)
      @original_status = @agreement.status
    end

    describe "with a Temporary failure in name resolution exception message" do
      before do
        Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(Errno::ETIMEDOUT)
      end

      it "does not raise an exception and retry a total of #{Zendesk::Net::MultiSSL::MAX_RETRIES} times" do
        ZendeskExceptions::Logger.expects(:record).once
        Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES)
        response = @agreement.send_to_partner
        assert response.is_a?(TicketSharing::FakeHttpResponse)
        assert_equal :pending, @agreement.status
      end
    end
  end

  describe "Given a Errno::EHOSTUNREACH during update_partner" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      @ticket = tickets(:minimum_1)
      @original_status = @agreement.status
    end

    describe "with a Temporary failure in name resolution exception message" do
      before do
        Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(Errno::EHOSTUNREACH)
      end

      it "does not raise an exception and retry a total of #{Zendesk::Net::MultiSSL::MAX_RETRIES} times" do
        ZendeskExceptions::Logger.expects(:record).once
        Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES)
        response = @agreement.update_partner
        assert response.is_a?(TicketSharing::FakeHttpResponse)
        assert_equal :pending, @agreement.status
      end
    end
  end

  describe "Given a Errno::ECONNRESET during update_partner" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      @ticket = tickets(:minimum_1)
      @original_status = @agreement.status
      Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(Errno::ECONNRESET)
    end

    it "does not raise an exception and retry a total of #{Zendesk::Net::MultiSSL::MAX_RETRIES} times" do
      ZendeskExceptions::Logger.expects(:record).once
      Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES)
      response = @agreement.update_partner
      assert response.is_a?(TicketSharing::FakeHttpResponse)
      assert_equal :pending, @agreement.status
    end
  end

  describe "Given a Errno::ECONNRESET during send_to_partner" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      @ticket = tickets(:minimum_1)
      @original_status = @agreement.status
      Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(Errno::ECONNRESET)
    end

    it "does not raise an exception and retry a total of #{Zendesk::Net::MultiSSL::MAX_RETRIES} times" do
      ZendeskExceptions::Logger.expects(:record).once
      Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES)
      response = @agreement.send_to_partner
      assert response.is_a?(TicketSharing::FakeHttpResponse)
      assert_equal :pending, @agreement.status
    end
  end

  describe "Given a Errno::EHOSTUNREACH during send_to_partner" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      @ticket = tickets(:minimum_1)
      @original_status = @agreement.status
    end

    describe "with a Temporary failure in name resolution exception message" do
      before do
        Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(Errno::EHOSTUNREACH)
      end

      it "does not raise an exception and retry a total of #{Zendesk::Net::MultiSSL::MAX_RETRIES} times" do
        ZendeskExceptions::Logger.expects(:record).once
        Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES)
        response = @agreement.send_to_partner
        assert response.is_a?(TicketSharing::FakeHttpResponse)
        assert_equal :pending, @agreement.status
      end
    end
  end

  describe "Given an OpenSSL::SSL::SSLError" do
    before do
      @account = accounts(:minimum)
      @agreement = create_valid_agreement(account: @account, shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk])
      Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(OpenSSL::SSL::SSLError)
    end

    describe "on an agreement that is shared with zendesk" do
      describe "during send_to_partner" do
        it "changes the remote url to https" do
          refute @agreement.remote_url =~ /https/
          @agreement.send_to_partner
          @agreement.reload
          assert_equal 0, @agreement.remote_url =~ /https/
        end
      end

      describe "during update_partner" do
        it "changes the remote url to https" do
          refute @agreement.remote_url =~ /https/
          @agreement.update_partner
          @agreement.reload
          assert_equal 0, @agreement.remote_url =~ /https/
        end
      end

      it "doesn't change the agreement status" do
        @agreement.send_to_partner
        @agreement.reload
        assert_equal :pending, @agreement.status
      end
    end

    describe "with cancelled status" do
      before do
        @agreement.status = :failed
      end

      describe "during send_to_partner" do
        it "stays cancelled" do
          @agreement.send_to_partner
          assert_equal :failed, @agreement.status
          assert @agreement.failed?
        end
      end

      describe "during update_partner" do
        it "stays cancelled" do
          @agreement.update_partner
          assert_equal :failed, @agreement.status
          assert @agreement.failed?
        end
      end
    end
  end

  describe "Given a URI::InvalidURIError" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      TicketSharing::Client.any_instance.stubs(:post).raises(URI::InvalidURIError)
      TicketSharing::Client.any_instance.stubs(:put).raises(URI::InvalidURIError)
    end

    describe "during send_to_partner" do
      it "becomes failed" do
        @agreement.send_to_partner
        assert_equal :failed, @agreement.status
        assert @agreement.failed?
      end
    end

    describe "during update_partner" do
      it "becomes failed" do
        @agreement.update_partner
        assert_equal :failed, @agreement.status
        assert @agreement.failed?
      end
    end
  end

  describe "Given a Faraday::RestrictIPAddresses::AddressNotAllowed error" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      address_not_allowed_exception = Faraday::RestrictIPAddresses::AddressNotAllowed.new("Address not allowed for")
      TicketSharing::Client.any_instance.stubs(:post).raises(address_not_allowed_exception)
      TicketSharing::Client.any_instance.stubs(:put).raises(address_not_allowed_exception)
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries.clear
    end

    [:send_to_partner, :update_partner].each do |method|
      describe "during #{method}" do
        it "does not raise an exception and change agreement status and send an email" do
          response = @agreement.send(method)
          assert response.is_a?(TicketSharing::FakeHttpResponse)
          assert_equal :configuration_error, @agreement.status
          assert_equal 1, ActionMailer::Base.deliveries.size
        end
      end
    end
  end

  describe "Given an internal authentication failure" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account)
      TicketSharing::Client.any_instance.stubs(:post).raises(error_type.new(500))
      TicketSharing::Client.any_instance.stubs(:put).raises(error_type.new(500))
    end

    [:send_to_partner, :update_partner].each do |method|
      describe "during #{method}" do
        describe "with an internal warning" do
          let(:error_type) { TicketSharing::InternalSharingWarning }

          it "retries the specified number of times but does not deactivate" do
            # The check runs one fewer times since it's total attempts, not retries
            Sharing::Agreement.any_instance.expects(:failed?).times(Sharing::Agreement::MAX_INTERNAL_ATTEMPTS - 1)
            response = @agreement.send(method)
            assert_equal 500, response.status
            assert_equal :pending, @agreement.status
          end
        end

        describe "with an internal error" do
          let(:error_type) { TicketSharing::InternalSharingError }

          it "retries the specified number of times and then deactivates" do
            Sharing::Agreement.any_instance.expects(:failed?).times(Sharing::Agreement::MAX_INTERNAL_ATTEMPTS)
            response = @agreement.send(method)
            assert_equal 500, response.status
            assert_equal :configuration_error, @agreement.status
          end
        end
      end
    end
  end

  describe ".remote_accounts_for" do
    let(:account) { accounts(:minimum) }

    describe "when the remote account does not have host_mapping" do
      let!(:agreement) { valid_agreement(remote_url: "http://minimum.zendesk-test.com") }

      it "returns the right account" do
        assert_equal({ "http://minimum.zendesk-test.com" => account }, Sharing::Agreement.remote_accounts_for([agreement]))
      end
    end
  end

  describe "#remote_account" do
    let(:account) { accounts(:minimum) }

    describe "when the remote account does not have host_mapping" do
      let(:agreement) { valid_agreement(remote_url: "http://minimum.zendesk-test.com") }

      it "returns the right account" do
        assert_equal account, agreement.remote_account
      end
    end

    describe "when the remote account is host_mapped" do
      let(:agreement) { valid_agreement(remote_url: account.host_mapping) }

      before { account.update_attribute(:host_mapping, "support.test.com") }

      it "returns the right account" do
        assert_equal account, agreement.remote_account
      end
    end

    describe "when the remote account cannot be found" do
      let(:agreement) { valid_agreement(remote_url: "support.google.com") }

      it "returns nil" do
        assert_nil agreement.remote_account
      end
    end

    describe "when the remote url is invalid" do
      let(:agreement) do
        valid_agreement.remote_url = "http://https://support.google.com"
        valid_agreement
      end

      it "does not raise an exception" do
        agreement.remote_account
      end
    end
  end

  describe "#support_addresses" do
    let(:account) { accounts(:minimum) }
    let(:agreement) { valid_agreement(status_id: Sharing::Agreement::STATUS_MAP[:accepted]) }

    before do
      @remote_account_double = Minitest::Mock.new
      setup_remote_account_expects
      Sharing::Agreement.any_instance.stubs(:remote_account).returns(@remote_account_double)
    end

    def setup_remote_account_expects
      @remote_account_double.expect(:subdomain, 'jackpot')
      @remote_account_double.expect(:id, 100)
    end

    describe "without a remote_account" do
      before do
        Sharing::Agreement.any_instance.stubs(:remote_account).returns(nil)
      end

      it "returns an empty array" do
        assert_equal [], agreement.support_addresses
      end
    end

    describe "with 200 response" do
      before do
        mock_response_body = {
          "recipient_addresses" => [
            {
              "id" => 275609,
              "brand_id" => 2051086,
              "name" => "No Reply",
              "email" => "someone@example.com",
              "forwarding_status" => "verified",
            }
          ],
          "next_page" => nil,
          "previous_page" => nil,
          "count" => 1
        }.to_json
        stub_request(:get, "https://jackpot.zendesk-test.com/api/v2/recipient_addresses.json").to_return(body: mock_response_body, status: 200, headers: {'Content-Type' => 'application/json'})
      end

      it "returns the recipient addresses for the remote account" do
        assert_equal ["someone@example.com"], agreement.support_addresses
      end
    end

    describe "with 500 response" do
      before do
        stub_request(:get, "https://jackpot.zendesk-test.com/api/v2/recipient_addresses.json").to_return(body: "Something went wrong", status: 500)
      end

      it "returns error response" do
        response = agreement.support_addresses
        assert response.is_a?(TicketSharing::FakeHttpResponse)
      end
    end

    describe "with timeout during request" do
      before do
        stub_request(:get, "https://jackpot.zendesk-test.com/api/v2/recipient_addresses.json").to_timeout
      end

      it "returns error response" do
        response = agreement.support_addresses
        assert response.is_a?(TicketSharing::FakeHttpResponse)
      end
    end
  end

  describe "#refresh_support_addresses" do
    let(:account) { accounts(:minimum) }
    let(:agreement) { create_valid_agreement }

    before do
      agreement.stubs(:enqueue_job)
    end

    describe "when agreement is accepted" do
      def change_accepted_status
        agreement.accepted = true
        agreement.save
      end

      it "is called" do
        agreement.expects(:refresh_support_addresses)
        change_accepted_status
      end

      it "enqueues a TicketSharingSupportAddressesJob" do
        TicketSharingSupportAddressesJob.expects(:enqueue).with(account.id)
        change_accepted_status
      end
    end

    describe "when agreement is deactivated" do
      before do
        agreement.accepted = true
        agreement.save
      end

      def deactivate_agreement
        agreement.accepted = false
        agreement.save
      end

      it "is called" do
        agreement.expects(:refresh_support_addresses)
        deactivate_agreement
      end

      it "enqueues a TicketSharingSupportAddressesJob" do
        TicketSharingSupportAddressesJob.expects(:enqueue).with(account.id)
        agreement.accepted = false
        agreement.save
      end
    end

    describe "when agreement status changes but neither old nor new value is accepted" do
      before do
        agreement.status = :pending
        agreement.save
      end

      def change_non_accepted_status
        agreement.status = :declined
        agreement.save
      end

      it "is called" do
        agreement.expects(:refresh_support_addresses)
        change_non_accepted_status
      end

      it "does not enqueue TicketSharingSupportAddressesJob" do
        TicketSharingSupportAddressesJob.expects(:enqueue).never
        change_non_accepted_status
      end
    end

    describe "when agreement changes but status does not" do
      before do
        agreement.sync_tags = false
        agreement.save
      end

      def change_non_status_field
        agreement.sync_tags = true
        agreement.save
      end

      it "is called" do
        agreement.expects(:refresh_support_addresses)
        change_non_status_field
      end

      it "does not enqueue TicketSharingSupportAddressesJob" do
        TicketSharingSupportAddressesJob.expects(:enqueue).never
        change_non_status_field
      end
    end
  end
end
