require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe TicketDeflection do
  fixtures :tickets, :users, :accounts, :ticket_deflections, :ticket_deflection_articles

  let(:account) { accounts(:minimum) }
  let(:ticket) do
    ticket = tickets(:minimum_1)
    ticket.via_id = ViaType.MAIL
    ticket.will_be_saved_by User.system
    ticket.save!
    ticket
  end
  let(:brand) { ticket.brand }
  let(:deflection_channel_id) { ViaType.WEB_WIDGET }
  let(:enquiry) { 'Unicorns like toast. Do you like toast?' }
  let(:user) { users(:minimum_agent) }
  let(:deflection) do
    email_deflection = ticket_deflections(:minimum_ticket_deflection)
    email_deflection.ticket = ticket
    email_deflection.update_attributes!(brand: brand)
    email_deflection
  end
  let(:deflection_article) { ticket_deflection_articles(:minimum_ticket_deflection_article) }
  let(:base_time) { Time.utc(2017, 8, 30) }

  before do
    Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
    AnswerBot::Deflectability.any_instance.stubs(:deflectable?).returns(true)
    AnswerBot::Deflectability.any_instance.stubs(:deflectable_via_webform?).returns(false)
    Timecop.freeze(base_time)
  end

  should belong_to(:account)
  should belong_to(:ticket)
  should belong_to(:user)
  should belong_to(:brand)
  should have_many(:ticket_deflection_articles)

  describe '#validations' do
    let(:ticket_deflection) do
      TicketDeflection.new(
        brand: brand,
        deflection_channel_id: deflection_channel_id,
        enquiry: enquiry,
        user: user,
        account: account
      )
    end
    let(:errors) { ticket_deflection.errors.full_messages }

    describe 'when required fields are all present' do
      it "is valid" do
        assert ticket_deflection.valid?
      end
    end

    describe 'when brand id is not given' do
      let(:brand) { nil }

      it "is not valid" do
        refute ticket_deflection.valid?
        assert_includes(errors, "Brand cannot be blank")
      end
    end

    describe 'when deflection channel id is not given' do
      let(:deflection_channel_id) { nil }

      it "is not valid" do
        refute ticket_deflection.valid?
        assert_includes(errors, "Deflection channel cannot be blank")
      end
    end

    describe 'when enquiry is not given' do
      let(:enquiry) { nil }

      it "is not valid" do
        refute ticket_deflection.valid?
        assert_includes(errors, "Enquiry cannot be blank")
      end
    end

    describe 'when enquiry length is invalid' do
      let(:enquiry) { "a" * 65_536 }

      it "is not valid" do
        refute ticket_deflection.valid?
        assert_includes(
          errors,
          "Enquiry is too long (maximum is 65535 characters after encoding)"
        )
      end
    end

    describe 'when detected_locale length is invalid' do
      it "is not valid" do
        ticket_deflection.detected_locale = "a" * 4
        refute ticket_deflection.valid?
        assert_includes(
          errors,
          "Detected locale is too long (maximum is 3 characters after encoding)"
        )
      end
    end

    describe 'when model_version length is invalid' do
      it "is not valid" do
        ticket_deflection.model_version = "a" * 21
        refute ticket_deflection.valid?
        assert_includes(
          errors,
          "Model version is too long (maximum is 20 characters after encoding)"
        )
      end
    end

    describe 'when interaction_reference length is invalid' do
      it "is not valid" do
        ticket_deflection.interaction_reference = "a" * 65_536
        refute ticket_deflection.valid?
        assert_includes(
          errors,
          "Interaction reference is too long (maximum is 65535 characters after encoding)"
        )
      end
    end

    describe 'when account is not given' do
      let(:account) { nil }

      it 'is not valid' do
        refute ticket_deflection.valid?
        assert_includes(errors, "Account cannot be blank")
      end
    end

    describe 'when an existing deflection record has the ticket_id' do
      before do
        # make sure existing deflection has the same ticket
        deflection.ticket = ticket
        deflection.save!
      end

      it 'is not valid' do
        ticket_deflection.ticket = ticket
        refute ticket_deflection.valid?

        error_message = /Ticket \d+ has already been taken/
        result = errors.find { |e| error_message =~ e }
        assert result, "Expected #{errors} to include #{error_message.inspect}"
      end

      describe 'and deflection channel is AB4A' do
        let(:deflection_channel_id) { ViaType.ANSWER_BOT_FOR_AGENTS }

        it 'is valid' do
          ticket_deflection.ticket = ticket
          assert ticket_deflection.valid?
        end
      end
    end

    describe 'when ticket is nil' do
      let(:ticket) { nil }
      let(:brand) { brands(:minimum) }

      it 'is valid' do
        assert ticket_deflection.valid?
      end
    end
  end

  describe '#mark_as_not_solved' do
    let(:resolution_channel_id) { ViaType.WEB_WIDGET }

    describe 'when the state is solved' do
      before do
        deflection.state = TicketDeflection::STATE_SOLVED
        deflection.save!
      end

      it 'does nothing' do
        TicketDeflection.any_instance.expects(:update_attributes).never
        deflection.mark_as_not_solved(resolution_channel_id)
      end
    end

    it 'updates state to not solved' do
      deflection.mark_as_not_solved(resolution_channel_id)
      assert_equal TicketDeflection::STATE_NOT_SOLVED, deflection.reload.state
    end

    it 'updates resolution channel id' do
      deflection.mark_as_not_solved(resolution_channel_id)
      assert_equal resolution_channel_id, deflection.reload.resolution_channel_id
    end
  end

  describe '#solve_with_article' do
    let(:resolution_channel_id) { ViaType.WEB_WIDGET }
    let(:solved_article_id) { 123 }
    let(:unsuggested_article_id) { 456 }

    before do
      article_info = { 'article_id' => solved_article_id, 'score' => 0.192819, 'locale' => 'en-us' }
      @deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(deflection, article_info)
      @deflection_article.save!
    end

    describe '#solve_with_article' do
      it 'sets the solved_article_id' do
        deflection.solve_with_article(solved_article_id, resolution_channel_id)
        assert_equal solved_article_id, deflection.reload.solved_article_id
      end

      it 'sets the via_id from ticket' do
        deflection.solve_with_article(solved_article_id, resolution_channel_id)
        assert_equal ticket.via_id, deflection.reload.via_id
      end

      it 'sets the resolution channel id' do
        deflection.solve_with_article(solved_article_id, resolution_channel_id)
        assert_equal resolution_channel_id, deflection.reload.resolution_channel_id
      end

      it 'sets the state to STATE_SOLVED' do
        deflection.solve_with_article(solved_article_id, resolution_channel_id)
        assert_equal TicketDeflection::STATE_SOLVED, deflection.reload.state
      end

      it 'sets the state_updated_at to now' do
        Timecop.freeze(2016, 5, 1, 13, 0) do
          deflection.solve_with_article(solved_article_id, resolution_channel_id)

          assert_equal Time.now, deflection.reload.state_updated_at
        end
      end

      it 'updates solved_at on the deflection article to now' do
        Timecop.freeze(2016, 5, 1, 13, 0) do
          deflection.solve_with_article(solved_article_id, resolution_channel_id)
          assert_equal Time.now, @deflection_article.reload.solved_at
        end
      end

      describe 'when the ticket is solved' do
        before do
          ticket.status_id = StatusType.SOLVED
          ticket.will_be_saved_by(ticket.requester)
          ticket.save!
        end

        it 'sets the state_updated_at to now' do
          Timecop.freeze(2016, 5, 1, 13, 0) do
            deflection.solve_with_article(solved_article_id, resolution_channel_id)

            assert_equal Time.now, deflection.reload.state_updated_at
          end
        end
      end
    end

    describe 'updating TicketDeflectionArticle' do
      describe 'when the article_id matches a previously suggested article' do
        it 'passes the correct solved time to the related deflection article' do
          Timecop.freeze(2016, 5, 1, 13, 0) do
            deflection.solve_with_article(solved_article_id, resolution_channel_id)

            assert_equal deflection.state_updated_at, @deflection_article.reload.solved_at
          end
        end
      end

      describe 'when the article_id does not match a previously suggested article' do
        before do
          Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).with([{'article_id' => 456, 'locale' => 'en-us'}]).returns([])
          Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:default_locale).returns('en-us')
        end

        it 'does nothing' do
          deflection.solve_with_article(unsuggested_article_id, resolution_channel_id)

          assert_nil @deflection_article.reload.solved_at
        end
      end
    end
  end
  describe '#solve_with_article_no_via_id(' do
    let(:resolution_channel_id) { ViaType.ANSWER_BOT_FOR_SLACK }
    let(:solved_article_id) { 123 }
    let(:unsuggested_article_id) { 456 }

    before do
      article_info = { 'article_id' => solved_article_id, 'score' => 0.192819, 'locale' => 'en-us' }
      @deflection_article = TicketDeflectionArticle.build_from_deflection_and_article(deflection, article_info)
      @deflection_article.save!
    end

    describe '#solve_with_article' do
      it 'sets the solved_article_id' do
        deflection.solve_with_article_no_via_id(solved_article_id, resolution_channel_id)
        assert_equal solved_article_id, deflection.reload.solved_article_id
      end

      it 'sets the resolution channel id' do
        deflection.solve_with_article_no_via_id(solved_article_id, resolution_channel_id)
        assert_equal resolution_channel_id, deflection.reload.resolution_channel_id
      end

      it 'sets the state to STATE_SOLVED' do
        deflection.solve_with_article_no_via_id(solved_article_id, resolution_channel_id)
        assert_equal TicketDeflection::STATE_SOLVED, deflection.reload.state
      end

      it 'sets the state_updated_at to now' do
        Timecop.freeze(2016, 5, 1, 13, 0) do
          deflection.solve_with_article_no_via_id(solved_article_id, resolution_channel_id)

          assert_equal Time.now, deflection.reload.state_updated_at
        end
      end

      it 'updates solved_at on the deflection article to now' do
        Timecop.freeze(2016, 5, 1, 13, 0) do
          deflection.solve_with_article_no_via_id(solved_article_id, resolution_channel_id)
          assert_equal Time.now, @deflection_article.reload.solved_at
        end
      end
    end
  end

  describe '#add_solved_information_to_ticket' do
    let(:solved_article_id) { 123 }
    let(:solved_article_content) do
      {id: solved_article_id,
       title: 'solved all the question',
       url: 'http://magic_url',
       html_url: 'http://html_for_magic_url',
       body: 'This is so a magic url.', locale: 'en-us'}
    end

    before do
      Zendesk::HelpCenter::InternalApiClient.any_instance.
        stubs(:fetch_article).
        with(solved_article_id, 'en-us').
        returns(solved_article_content)

      article_info = { 'article_id' => solved_article_id, 'score' => 0.192819, 'locale' => 'en-us' }
      TicketDeflectionArticle.build_from_deflection_and_article(deflection, article_info).save!
    end

    it 'sets the deflection brand in the help center request' do
      internal_api_client = stub('hc_internal_api_client', :fetch_article)
      Zendesk::HelpCenter::InternalApiClient.expects(:new).
        with(account: account, requester: ticket.requester, brand: deflection.brand).
        returns(internal_api_client)

      deflection.add_solved_information_to_ticket(solved_article_id)
    end

    it 'create a solve event' do
      deflection.add_solved_information_to_ticket(solved_article_id)
      ticket.reload.events.where(type: 'AutomaticAnswerSolve').count.must_be :>, 0
    end

    describe 'answer_bot_for_agents' do
      let(:deflection) do
        deflection = ticket_deflections(:minimum_ticket_deflection)
        deflection.update_attributes!(deflection_channel_id: ViaType.ANSWER_BOT_FOR_AGENTS)
        deflection
      end

      it 'adds AutomaticAnswerSolve event (ie. answer bot behaviour)' do
        deflection.add_solved_information_to_ticket(solved_article_id)
        assert_equal(1, ticket.reload.events.where(type: 'AutomaticAnswerSolve').count)
      end
    end
  end

  describe '#deconstruct_enquiry' do
    let(:deflection) { ticket_deflections(:minimum_ticket_deflection) }
    let(:subject) { 'The Phrase' }
    let(:description) { 'A brown fox jumps over the lazy dog' }

    it 'splits into subject and description' do
      enquiry = subject + ' ' + TicketDeflection::ENQUIRY_DELIMITER + ' ' + description
      deflection.stubs(:enquiry).returns(enquiry)

      assert_equal(
        { subject: subject, description: description },
        deflection.deconstruct_enquiry
      )
    end

    describe 'blank after split' do
      it 'both subject and description are enquiry without the delimiter' do
        enquiry = subject + TicketDeflection::ENQUIRY_DELIMITER + ' '
        deflection.stubs(:enquiry).returns(enquiry)

        assert_equal(
          { subject: subject, description: subject },
          deflection.deconstruct_enquiry
        )
      end
    end

    describe 'when no deliminator found' do
      it 'both subject and description are enquiry without the delimiter' do
        enquiry = subject
        deflection.stubs(:enquiry).returns(enquiry)

        assert_equal(
          { subject: subject, description: subject },
          deflection.deconstruct_enquiry
        )
      end
    end
  end
end
