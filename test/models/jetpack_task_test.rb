require_relative "../support/test_helper"

SingleCov.covered!

describe JetpackTask do
  before do
    @account = stub('account', id: 123)
    @user = stub('user', id: 456, account: @account, class: stub('class', sti_name: 'User'))
    @task = JetpackTask.new(owner: @user)
  end

  it "provides a default 'status' value" do
    assert_equal 'new', @task.status
  end

  it "responds to 'section'" do
    assert_respond_to @task, :section
    assert_respond_to @task, :section=
  end

  it "responds to 'enabled'" do
    assert_respond_to @task, :enabled
    assert_respond_to @task, :enabled=
  end

  it "has the appropriate status_id from status" do
    @task.status = 'started'
    assert_equal 2, @task.read_attribute(:status_id)
    assert_equal 'started', @task.status
  end

  it "raises an exception if an status is set to an unknown exception" do
    assert_raises(JetpackTask::UnknownStatusError) { @task.status = 'fig_newton' }
  end

  it "has the appropriate owner_id from owner" do
    assert_equal 456, @task.owner_id
  end

  it "has the appropriate owner_type from owner" do
    assert_equal 'User', @task.owner_type
  end
end
