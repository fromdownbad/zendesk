require_relative "../support/test_helper"

SingleCov.covered!

describe 'AgentDowngradeAudit' do
  fixtures :accounts, :users

  let(:user) do
    FactoryBot.build_stubbed(
      :agent,
      account_id:         123,
      permission_set_id:  123456
    )
  end

  let(:account) { accounts(:minimum) }
  let(:groups)  { stub(pluck: [111, 222]) }
  let(:rules)   { stub(pluck: [333, 444]) }

  before do
    user.stubs(:groups).returns(groups)
    user.stubs(:rules).returns(rules)
    user.stubs(:default_group_id).returns(111)
  end

  describe '.from_user' do
    it 'copies the user data correctly' do
      agent_downgrade_audit = AgentDowngradeAudit.from_user(user)

      assert agent_downgrade_audit.persisted?

      assert_equal agent_downgrade_audit.account_id, user.account_id
      assert_equal agent_downgrade_audit.user_id, user.id
      assert_equal agent_downgrade_audit.roles, user.roles
      assert_equal agent_downgrade_audit.default_group_id, user.default_group_id
      assert_equal agent_downgrade_audit.permission_set_id, user.permission_set_id
      assert_equal agent_downgrade_audit.group_ids, user.groups.pluck(:id)
      assert_equal agent_downgrade_audit.rule_ids, user.rules.pluck(:id)
    end
  end

  describe '#remediate_agent' do
    let!(:agent_downgrade_audit) { AgentDowngradeAudit.from_user(user) }

    before do
      agent_downgrade_audit.stubs(:user).returns(user)
    end

    it 'calls all remediation methods' do
      agent_downgrade_audit.expects(:remediate_agent_roles).once
      agent_downgrade_audit.expects(:remediate_agent_permission_set_id).once
      agent_downgrade_audit.expects(:remediate_agent_memberships).once
      agent_downgrade_audit.expects(:remediate_agent_rules).once

      agent_downgrade_audit.remediate_agent
    end
  end

  describe '#remediate_agent_roles' do
    let(:user)  { users(:minimum_end_user) }
    let(:roles) { 4 }

    let(:agent_downgrade_audit) do
      AgentDowngradeAudit.new(
        user: user,
        account: user.account,
        roles:  roles
      )
    end

    it 'changes user roles back to Agent' do
      assert_change -> { user.roles } do
        agent_downgrade_audit.remediate_agent_roles
      end

      assert_equal user.roles, roles
    end
  end

  describe '#remediate_agent_permission_set_id' do
    let(:user)              { users(:minimum_end_user) }
    let(:permission_set_id) { 123456 }

    let(:agent_downgrade_audit) do
      AgentDowngradeAudit.new(
        user: user,
        account: user.account,
        permission_set_id: permission_set_id
      )
    end

    it 'reassigns user permission_set_id' do
      assert_change -> { user.permission_set_id } do
        agent_downgrade_audit.remediate_agent_permission_set_id
      end

      assert_equal user.permission_set_id, permission_set_id
    end
  end

  describe '#remediate_agent_memberships' do
    let(:group_ids)        { [111, 222] }
    let(:default_group_id) { 111 }
    let(:memberships)      { stub }

    let(:agent_downgrade_audit) do
      AgentDowngradeAudit.new(
        user: user,
        account: account,
        group_ids: group_ids,
        default_group_id: default_group_id
      )
    end

    before do
      account.stubs(:memberships).returns(memberships)
    end

    it 'restores memberships' do
      group_ids.each do |group_id|
        account.memberships.expects(:find_or_create_by!).with(
          user_id: user.id,
          group_id: group_id,
          default: default_group_id == group_id
        )
      end

      agent_downgrade_audit.remediate_agent_memberships
    end
  end

  describe '#remediate_agent_rules' do
    let(:rule_ids) { [111, 222] }
    let(:rules)    { stub }

    let(:agent_downgrade_audit) do
      AgentDowngradeAudit.new(account: account, rule_ids: rule_ids)
    end

    before do
      Rule.stubs(:unscoped).returns(rules)
      rules.stubs(:where).with(
        account_id: account.id,
        id:         rule_ids
      ).returns(rules)
    end

    it 'restores rules' do
      rules.expects(:update_all).with(deleted_at: nil)

      agent_downgrade_audit.remediate_agent_rules
    end
  end

  def assert_change(block)
    before = block.call

    yield

    refute_equal before, block.call
  end
end
