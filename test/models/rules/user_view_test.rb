require_relative "../../support/test_helper"
require_relative '../../support/rule'

SingleCov.covered! uncovered: 10

describe UserView do
  include TestSupport::Rule::Helper

  fixtures :accounts, :rules, :cf_fields

  before do
    Arturo.enable_feature!(:user_views_negative_operators_enabled)
  end

  describe "validations" do
    it "rejects invalid definitions" do
      subject { rules(:user_view_date_custom_field).tap { |view| view.attributes = { definition: nil } } }
      refute subject.valid?
      assert_equal I18n.t('txt.models.rules.rule.invalid_conditions_you_must_select_condition'), subject.errors[:base].first
    end

    it "rejects invalid operators" do
      subject = rules(:user_view_date_custom_field).tap { |view| view.definition.conditions_all.first.operator = "sbsdds" }
      refute subject.valid?
      assert_equal I18n.t('txt.models.rules.rule.invalid_operator',
        condition: "Date sbsdds 2012-12-12", operator: "sbsdds"), subject.errors[:base].first
    end

    it "rejects invalid sources" do
      subject = rules(:user_view_date_custom_field).tap do |view|
        condition = view.definition.conditions_all.first
        condition.source   = "custom_fields.plan_type"
        condition.operator = "is"
        condition.value    = "gold"
      end
      refute subject.valid?
      assert_equal I18n.t('txt.models.rules.rule.invalid_source',
        condition: "custom_fields.plan_type is gold", source: "custom_fields.plan_type"), subject.errors[:base].first
    end

    it "rejects invalid columns" do
      subject = rules(:user_view_date_custom_field).tap do |view|
        view.output.columns = ["abc"]
      end
      refute subject.valid?
      assert_equal I18n.t('txt.models.rules.rule.invalid_columns', field: "abc"), subject.errors[:base].first
    end

    it "rejects created_at is nil" do
      subject = rules(:user_view_date_custom_field).tap do |view|
        condition = view.definition.conditions_all.first
        condition.source   = "created_at"
        condition.operator = "is"
        condition.value    = nil
      end
      refute subject.valid?
      assert_equal I18n.t('txt.models.rules.rule.invalid_value',
        condition: "Created is ", value: nil), subject.errors[:base].first
    end

    it "rejects created_at is_not nil" do
      subject = rules(:user_view_date_custom_field).tap do |view|
        condition = view.definition.conditions_all.first
        condition.source   = "created_at"
        condition.operator = "is_not"
        condition.value    = nil
      end
      refute subject.valid?
      assert_equal I18n.t('txt.models.rules.rule.invalid_value',
        condition: "Created is not ", value: nil), subject.errors[:base].first
    end

    it "rejects created_at is_present" do
      subject = rules(:user_view_date_custom_field).tap do |view|
        condition = view.definition.conditions_all.first
        condition.source   = "created_at"
        condition.operator = "is_present"
        condition.value    = nil
      end
      refute subject.valid?
      assert_equal I18n.t('txt.models.rules.rule.invalid_operator',
        condition: "Created is_present ", operator: "is_present"), subject.errors[:base].first
    end

    it "rejects created_at is_not_present" do
      subject = rules(:user_view_date_custom_field).tap do |view|
        condition = view.definition.conditions_all.first
        condition.source   = "created_at"
        condition.operator = "is_not_present"
        condition.value    = nil
      end
      refute subject.valid?
      assert_equal I18n.t('txt.models.rules.rule.invalid_operator',
        condition: "Created is_not_present ", operator: "is_not_present"), subject.errors[:base].first
    end

    ["group", "sort"].each do |type|
      it "rejects invalid #{type}" do
        subject = rules(:user_view_date_custom_field).tap do |view|
          view.output.send(type)[:id] = "sbd"
        end
        refute subject.valid?
        assert_equal I18n.t("txt.models.rules.rule.invalid_#{type}", field: "sbd", order: "asc"), subject.errors[:base].first
      end

      it "rejects invalid #{type} order" do
        subject = rules(:user_view_date_custom_field).tap do |view|
          view.output.send(type)[:id] = "created"
          view.output.send(type)[:order] = "sbd"
        end
        refute subject.valid?
        assert_equal I18n.t("txt.models.rules.rule.invalid_#{type}", field: "created", order: "sbd"), subject.errors[:base].first
      end
    end

    describe "sorted by User ID" do
      ['id', :id].each do |sort_id|
        describe "and sort id saved as a #{sort_id.class.to_s.downcase}" do
          subject { rules(:minimum_custom_fields_view).tap { |view| view.output.sort[:id] = sort_id } }

          it "defaults to created_at" do
            assert subject.valid?
            assert_equal 'created_at', subject.output.sort[:id]
          end
        end
      end
    end

    describe "with system field" do
      let(:account) { accounts(:minimum) }

      before do
        cf = account.custom_fields.build(key: "system::nps_rating", title: "txt.nps.internal_api_client.system_nps_rating", owner: "User")
        cf.type = "Integer"
        cf.is_system = true
        cf.save!
      end

      it "accepts valid value" do
        subject = rules(:user_view_date_custom_field).tap do |view|
          condition = view.definition.conditions_all.first
          condition.source   = "custom_fields.system::nps_rating"
          condition.operator = "greater_than_or_equal_to"
          condition.value    = 2
        end
        assert subject.valid?
      end

      it "rejects invalid value" do
        subject = rules(:user_view_date_custom_field).tap do |view|
          condition = view.definition.conditions_all.first
          condition.source   = "custom_fields.system::nps_rating"
          condition.operator = "greater_than_or_equal_to"
          condition.value    = nil
        end
        refute subject.valid?
        assert_equal I18n.t('txt.models.rules.rule.invalid_value',
          condition: "NPS rating greater than or equal to ", value: nil), subject.errors[:base].first
      end
    end

    describe "with dc field title" do
      let(:account) { accounts(:minimum) }

      before do
        cf = account.custom_fields.build(key: "best_superhero", title: "{{dc.best_superhero}}", owner: "User")
        cf.type = "Text"
        cf.save!
      end

      should_eventually "reject invalid operator" do
        Zendesk::Liquid::DcContext.expects(:render).with("{{dc.best_superhero}}", @account, @user, "text/plain", nil, {}).returns('Thor')

        subject = rules(:user_view_date_custom_field).tap do |view|
          condition = view.definition.conditions_all.first
          condition.source   = "custom_fields.best_superhero"
          condition.operator = "greater_than_or_equal_to"
          condition.value    = nil
        end
        refute subject.valid?
        assert_equal I18n.t('txt.models.rules.rule.invalid_operator',
          condition: "Thor greater_than_or_equal_to ", operator: 'greater_than_or_equal_to'), subject.errors[:base].first
      end
    end
  end

  describe "update_position_if_activating" do
    before do
      @account = accounts(:minimum)
      @active_lists = @account.user_views.active
      @active_lists.each_with_index do |uv, i|
        uv.position = i
        uv.save
      end
      assert_equal @active_lists.size, 5
    end

    it "updates position if activating" do
      uv = @active_lists[2]
      uv.is_active = false
      uv.save
      assert_equal uv, @account.user_views.order(:position).last
      uv.is_active = true
      uv.save
      assert_equal uv, @account.user_views.active.order(:position).last
    end

    it "does not update position if not changing active status" do
      uv = @active_lists[2]
      uv.title = "blah"
      uv.save
      assert_equal uv.position, 2
    end
  end

  describe "custom_field?" do
    it "detects custom field" do
      assert UserView.custom_field?('custom_fields.test'.to_sym)
    end

    it "detects non-custom field" do
      refute UserView.custom_field?(:non_custom_field)
    end
  end

  describe "custom_field_key" do
    it "formats custom field" do
      assert_equal 'test', UserView.custom_field_key('custom_fields.test'.to_sym)
    end

    it "returns false for non-custom field" do
      refute UserView.custom_field_key(:non_custom_field)
    end
  end

  describe 'verify_table_limit' do
    let(:account)    { accounts(:minimum) }
    let(:limit)      { account.rules.active.where(type: 'UserView').count }
    let(:active_user_view)   { rules(:user_view_date_custom_field) }
    let(:inactive_user_view) { rules(:inactive_global_user_view) }

    let(:user_view) do
      account.user_views.build(
        owner: account,
        owner_type: 'Account',
        definition: active_user_view[:definition],
        output: active_user_view[:output],
        title: 'New user view',
        account: account
      )
    end

    let(:log_message) do
      "Rules table limit exceeded: account_id: #{account.id}, "\
      "rule_type: #{user_view.type}, current limit: #{limit}"
    end

    before do
      Rule.any_instance.stubs(soft_limit_for_rule: limit)
    end

    describe_with_arturo_enabled :apply_rules_table_limits do
      describe 'when the account has reached the limit' do
        it 'does not allow creating new user view' do
          Rails.logger.expects(:info).with(log_message).once

          assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
            user_view.save
          end
        end

        it 'allows an active user view to be deactivated' do
          assert active_user_view.update_attribute(:is_active, false)
        end

        it 'allows an active user view to be updated' do
          assert active_user_view.update_attribute(:title, 'new title')
        end

        it 'does not allow an inactive user view to be activated' do
          assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
            inactive_user_view.update_attribute(:is_active, true)
          end
        end

        it 'allows user views to be deleted' do
          assert active_user_view.destroy
        end
      end

      describe 'when the account has not met the limit' do
        let(:limit) { 10 }

        it 'persists the user view successfully' do
          assert user_view.save
        end
      end
    end

    describe_with_arturo_disabled :apply_rules_table_limits do
      it 'persists the user_view successfully' do
        assert user_view.save
      end
    end
  end
end
