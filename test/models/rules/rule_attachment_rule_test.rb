require_relative '../../support/test_helper'

SingleCov.covered!

describe RuleAttachmentRule do
  fixtures :all

  let(:user)    { users(:minimum_agent) }
  let(:account) { user.account }
  let(:macro)   { rules :macro_incident_escalation }

  let(:rule_attachment) do
    RuleAttachment.create! do |attachment|
      attachment.account_id   = account.id
      attachment.user_id      = user.id
      attachment.size         = 1
      attachment.content_type = 'application/unknown'
      attachment.filename     = 'hello_world'
    end
  end

  before { rule_attachment.associate(macro) }

  it 'creates a rule attachment rule' do
    assert_equal 1, rule_attachment.rule_attachment_rules.count
  end
end
