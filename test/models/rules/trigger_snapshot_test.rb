require_relative '../../support/test_helper'
require_relative '../../support/rule'

SingleCov.covered!

describe TriggerSnapshot do
  include TestSupport::Rule::Helper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  describe 'serializing the `definition` attribute' do
    let(:snapshot) do
      create_trigger.revisions.first.snapshot.tap do |trigger|
        trigger.definition = definition
      end
    end

    describe 'when saving' do
      let(:raw_definition) do
        ActiveRecord::Base.connection.execute(
          "SELECT definition FROM trigger_snapshots WHERE id = #{snapshot.id}"
        ).first.first
      end

      describe 'and the value is a `Definition` instance' do
        let(:definition) { build_trigger_definition }

        before { snapshot.save! }

        it 'persists a YAML serialized string representing the instance' do
          assert_equal YAML.dump(definition), raw_definition
        end
      end

      describe 'and the value is a `String` instance' do
        let(:definition) { 'x' }

        before { snapshot.save! }

        it 'persists the value' do
          assert_equal definition, raw_definition
        end
      end
    end

    describe 'when fetching' do
      let(:definition) { YAML.dump(build_trigger_definition) }

      before do
        ActiveRecord::Base.connection.execute(
          "UPDATE trigger_snapshots " \
            "SET definition = '#{new_definition}' " \
            "WHERE id = #{snapshot.id}"
        )
      end

      describe 'and the value is a YAML serialized `Definition` instance' do
        let(:new_definition) { definition }

        before { snapshot.save! }

        it 'deserializes the string into a `Definition` instance' do
          assert snapshot.reload.definition.is_a?(Definition)
        end
      end

      describe 'and the value does not represent a serialized YAML object' do
        let(:new_definition) { 'x' }

        it 'returns the value' do
          assert_equal new_definition, snapshot.reload.definition
        end
      end
    end
  end
end
