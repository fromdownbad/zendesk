require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 7

describe 'CachedRuleTicketCount' do
  fixtures :accounts, :users, :rules, :suspended_tickets, :tickets

  def self.should_enqueue_job
    before_should "enqueue a RuleTicketCountJob" do
      Timecop.travel(Time.now + 1.seconds)
      Resque.expects(:enqueue)
    end
  end

  def self.should_not_enqueue_job
    before_should "not enqueue a RuleTicketCountJob" do
      Resque.expects(:enqueue).never
    end
  end

  def self.should_do_a_count_on_the_db
    before_should "do a count on the db" do
      Timecop.travel(Time.now + 1.seconds)
      @rule.expects(:count_tickets).once
    end
  end

  def self.should_not_do_a_count_on_the_db
    before_should "not do a count on the db" do
      Timecop.travel(Time.now + 1.seconds)
      @rule.expects(:count_tickets).never
    end
  end

  let(:count_from_occam) { 0 }

  before do
    stub_occam_count(count_from_occam)
  end

  describe "update_now" do
    let(:rule) { rules(:view_my_working_tickets) }
    let(:user) { users(:minimum_agent) }

    before do
      @cached_count = CachedRuleTicketCount.new(rule: rule, user: user, value: 100)
      @job = RuleTicketCountJob.new(rule, user, @cached_count, false)
      RuleTicketCountJob.stubs(:new).returns(@job)
    end

    it "runs a count job on self" do
      @job.expects(:perform)
      @cached_count.update_now
    end

    it 'passes a caller to occam' do
      RuleTicketCountJob.expects(:new).with(
        rule, user, @cached_count, false, caller: 'inband-count'
      ).returns(@job)

      @cached_count.update_now
    end

    describe 'when `caller` is passed in during initialization' do
      let(:request_options) { {caller: 'something'} }

      let(:cached_count) do
        CachedRuleTicketCount.new(
          rule: rule, user: rule.owner, options: request_options
        )
      end

      before { cached_count.update_now }

      it 'pass proper caller to occam' do
        RuleTicketCountJob.expects(:new).with(
          anything, anything, anything, false, caller: 'something'
        ).returns(@job)

        cached_count.update_now
      end
    end

    describe "when the job times out with fallback_after" do
      it "returns the cached value and enqueues a job" do
        @job.expects(:perform).raises(Occam::Client::QueryTimeoutError)
        RuleTicketCountJob.expects(:enqueue)
        assert_equal(100, @cached_count.update_now(fallback_after: 2.seconds))
      end

      it "should write a flag to cache" do
        @job.expects(:perform).raises(Occam::Client::QueryTimeoutError)
        RuleTicketCountJob.expects(:enqueue)

        @cached_count.update_now(fallback_after: 2.seconds)
        assert(Rails.cache.read(@cached_count.key + "/timing_out"))
      end

      describe "after the flag is written to cache" do
        before do
          Rails.cache.write(@cached_count.key + "/timing_out", true)
        end

        it "returns the value and enqueues an update job in the background" do
          @job.expects(:perform).never
          RuleTicketCountJob.expects(:enqueue)
          assert_equal(100, @cached_count.update_now(fallback_after: 2.seconds))
        end
      end
    end

    describe 'when there is an error' do
      before do
        @job.stubs(:perform).raises(Occam::Client::QueryTimeoutError)
      end

      describe 'and there is no fallback_after' do
        describe_with_arturo_enabled :occam_slow_rule_handler do
          before { RuleTicketCountJob.expects(:enqueue) }

          it 'sets value as BROKEN_COUNT' do
            assert ::View::BROKEN_COUNT, @cached_count.update_now
          end
        end

        describe_with_arturo_disabled :occam_slow_rule_handler do
          it 'raises an error' do
            assert_raises Occam::Client::QueryTimeoutError do
              @cached_count.update_now
            end
          end
        end
      end
    end

    describe "the second time we get an update_now that way" do
      before do
        @job.expects(:perform).raises(Occam::Client::QueryTimeoutError)
      end
    end
  end

  describe "#max_age" do
    before do
      rule = rules(:view_my_working_tickets)
      user = users(:minimum_agent)
      @cached_count = CachedRuleTicketCount.new(
        rule: rule,
        user: user,
        value: 1,
        updated_at: 2.days.ago,
        update_requested_at: rand(1000).seconds.ago,
        execution_time: 0.000001,
        exactness_key: rule.account.scoped_cache_key(:views)
      )
    end

    describe "for time-based rules" do
      before do
        @cached_count.rule.stubs(:is_time_based?).returns(true)
      end

      it "is one hour" do
        assert_equal(1.hour, @cached_count.max_age)
      end
    end
  end

  describe "#exact?" do
    before do
      rule = rules(:view_my_working_tickets)
      user = users(:minimum_agent)
      @cached_count = CachedRuleTicketCount.new(
        rule: rule,
        user: user,
        value: 1,
        updated_at: 2.days.ago,
        update_requested_at: rand(1000).seconds.ago,
        execution_time: 0.000001,
        exactness_key: rule.account.scoped_cache_key(:views)
      )
    end

    describe "on non-time-based rules" do
      it "only check the exactness_key" do
        assert(@cached_count.exact?)
      end
    end

    describe "on time-based rules" do
      before do
        @cached_count.rule.stubs(:is_time_based?).returns(true)
        @cached_count.exactness_key = nil
      end

      it "always be false" do
        assert(!@cached_count.exact?)
      end
    end
  end

  describe "#save" do
    before do
      rule = rules(:view_my_working_tickets)
      user = users(:minimum_agent)
      @cached_count = CachedRuleTicketCount.new(
        rule: rule,
        user: user,
        value: rand(10000),
        updated_at: rand(1000).seconds.ago,
        update_requested_at: rand(1000).seconds.ago,
        execution_time: rand * 10,
        exactness_key: 'ABC'
      )
      @cached_count.save

      @stored_hash = Rails.cache.read(@cached_count.key)
      assert(@stored_hash)
    end

    it "stores the value" do
      assert_equal(@cached_count.value, @stored_hash[:value])
    end

    it "stores the update time" do
      assert_equal(@cached_count.updated_at, @stored_hash[:updated_at])
    end

    it "stores the execution time" do
      assert_equal(@cached_count.execution_time, @stored_hash[:execution_time])
    end

    it "stores the time a job was enqueued" do
      assert_equal(@cached_count.update_requested_at, @stored_hash[:update_requested_at])
    end

    it "stores the exactness_key" do
      assert_equal(@cached_count.exactness_key, @stored_hash[:exactness_key])
    end
  end

  describe "#update_delayed" do
    before do
      Timecop.freeze
      rule = rules(:view_my_working_tickets)
      user = users(:minimum_agent)
      @cached_count = CachedRuleTicketCount.new(rule: rule, user: user)
    end

    it "enqueues an update" do
      Resque.expects(:enqueue)
      @cached_count.update_delayed
    end

    it "updates the time a job was enqueued" do
      assert_nil(@cached_count.update_requested_at)
      @cached_count.update_delayed
      assert(@cached_count.update_requested_at)
      assert(Time.now - @cached_count.update_requested_at < 1)
    end

    it "saves updates to cache store" do
      @cached_count.expects(:save)
      @cached_count.update_delayed
    end
  end

  describe "lookup single rule" do
    before do
      @rule = rules(:view_my_working_tickets)
      @user = users(:minimum_agent)
    end

    describe "when not found" do
      describe "with :refresh => false" do
        before do
          CachedRuleTicketCount.lookup(@rule, @user, refresh: false)
        end
        should_not_enqueue_job
        should_not_do_a_count_on_the_db
      end

      describe "with :refresh => nil" do
        before do
          CachedRuleTicketCount.lookup(@rule, @user)
        end
        should_not_enqueue_job
        should_do_a_count_on_the_db
      end

      describe "with :refresh => :delayed" do
        before do
          CachedRuleTicketCount.lookup(@rule, @user, refresh: :delayed)
        end
        should_enqueue_job
        should_not_do_a_count_on_the_db
      end

      describe "with :refresh => true" do
        before do
          CachedRuleTicketCount.lookup(@rule, @user, refresh: true)
        end
        should_not_enqueue_job
        should_do_a_count_on_the_db
      end
    end

    describe "when old entry found" do
      describe "with no enqueued job" do
        before do
          @cached_count = CachedRuleTicketCount.new(
            rule: @rule,
            user: @user,
            value: rand(10000),
            updated_at: 2.days.ago
          )
          @cached_count.save
        end

        describe "and no ticket activity" do
          describe "with :refresh => false" do
            before do
              CachedRuleTicketCount.lookup(@rule, @user, refresh: false)
            end
            should_not_enqueue_job
            should_not_do_a_count_on_the_db
          end

          describe "with :refresh => nil" do
            before do
              CachedRuleTicketCount.lookup(@rule, @user)
            end
            should_not_enqueue_job
            should_not_do_a_count_on_the_db
          end

          describe "with :refresh => :delayed" do
            before do
              CachedRuleTicketCount.lookup(@rule, @user, refresh: :delayed)
            end
            should_not_enqueue_job
            should_not_do_a_count_on_the_db
          end

          describe "with :refresh => true" do
            describe "without forcing it" do
              before do
                CachedRuleTicketCount.lookup(@rule, @user, refresh: true)
              end
              should_not_enqueue_job
              should_not_do_a_count_on_the_db
            end

            describe "and forcing it" do
              before do
                CachedRuleTicketCount.lookup(@rule, @user, refresh: true, force: true)
              end
              should_not_enqueue_job
              should_do_a_count_on_the_db
            end
          end
        end

        describe "and recent ticket activity" do
          before do
            @user.account.expire_scoped_cache_key(:views)
          end

          describe "with :refresh => false" do
            before do
              CachedRuleTicketCount.lookup(@rule, @user, refresh: false)
            end
            should_not_enqueue_job
            should_not_do_a_count_on_the_db
          end

          describe "with :refresh => nil" do
            before do
              CachedRuleTicketCount.lookup(@rule, @user)
            end
            should_not_enqueue_job
            should_do_a_count_on_the_db
          end

          describe "with :refresh => :delayed" do
            before do
              CachedRuleTicketCount.lookup(@rule, @user, refresh: :delayed)
            end
            should_enqueue_job
            should_not_do_a_count_on_the_db
          end

          describe "with :refresh => true" do
            before do
              CachedRuleTicketCount.lookup(@rule, @user, refresh: true)
            end
            should_not_enqueue_job
            should_do_a_count_on_the_db
          end
        end
      end

      describe "with a recent enqueued update" do
        before do
          @cached_count = CachedRuleTicketCount.new(
            rule: @rule,
            user: @user,
            value: rand(10000),
            updated_at: 2.days.ago,
            update_requested_at: 30.seconds.ago
          )
          @cached_count.save
          @user.account.expire_scoped_cache_key(:views)
        end

        describe "with :refresh => false" do
          before do
            CachedRuleTicketCount.lookup(@rule, @user, refresh: false)
          end
          should_not_enqueue_job
          should_not_do_a_count_on_the_db
        end

        describe "with :refresh => nil" do
          before do
            CachedRuleTicketCount.lookup(@rule, @user)
            Timecop.travel(Time.now + 1.seconds)
          end
          should_not_enqueue_job
          should_do_a_count_on_the_db
        end

        describe "with :refresh => :delayed" do
          before do
            CachedRuleTicketCount.lookup(@rule, @user, refresh: :delayed)
          end
          should_not_enqueue_job
          should_not_do_a_count_on_the_db
        end

        describe "with :refresh => true" do
          before do
            CachedRuleTicketCount.lookup(@rule, @user, refresh: true)
          end
          should_not_enqueue_job
          should_do_a_count_on_the_db
        end
      end

      describe "with an old enqueued update" do
        before do
          @cached_count = CachedRuleTicketCount.new(
            rule: @rule,
            user: @user,
            value: rand(10000),
            updated_at: 2.days.ago,
            update_requested_at: 30.minutes.ago
          )
          @cached_count.save
          @user.account.expire_scoped_cache_key(:views)
        end

        describe "with :refresh => false" do
          before do
            CachedRuleTicketCount.lookup(@rule, @user, refresh: false)
          end
          should_not_enqueue_job
          should_not_do_a_count_on_the_db
        end

        describe "with :refresh => nil" do
          before do
            CachedRuleTicketCount.lookup(@rule, @user)
          end
          should_not_enqueue_job
          should_do_a_count_on_the_db
        end

        describe "with :refresh => :delayed" do
          before do
            CachedRuleTicketCount.lookup(@rule, @user, refresh: :delayed)
          end
          should_enqueue_job
          should_not_do_a_count_on_the_db
        end

        describe "with :refresh => true" do
          before do
            CachedRuleTicketCount.lookup(@rule, @user, refresh: true)
          end
          should_not_enqueue_job
          should_do_a_count_on_the_db
        end
      end
    end

    describe "when recent entry found" do
      before do
        @cached_count = CachedRuleTicketCount.new(
          rule: @rule,
          user: @user,
          value: rand(10...10_010),
          updated_at: Time.now,
          execution_time: 1.minute
        )
        @cached_count.save
        @user.account.expire_scoped_cache_key(:views)
      end

      describe "with :refresh => false" do
        before do
          CachedRuleTicketCount.lookup(@rule, @user, refresh: false)
        end
        should_not_enqueue_job
        should_not_do_a_count_on_the_db
      end

      describe "with :refresh => nil" do
        before do
          CachedRuleTicketCount.lookup(@rule, @user)
        end
        should_not_enqueue_job
        should_not_do_a_count_on_the_db
      end

      describe "with :refresh => :delayed" do
        before do
          CachedRuleTicketCount.lookup(@rule, @user, refresh: :delayed)
        end
        should_not_enqueue_job
        should_not_do_a_count_on_the_db
      end

      describe "with :refresh => true" do
        before do
          CachedRuleTicketCount.lookup(@rule, @user, refresh: true)
        end
        should_not_enqueue_job
        should_do_a_count_on_the_db
      end
    end
  end

  describe "lookup multiple rules" do
    before do
      @rules = [rules(:view_my_working_tickets), rules(:view_assigned_working_tickets)]
      @user = users(:minimum_agent)
    end

    it "uses read_multi to look them up" do
      Rails.cache.expects(:read_multi).with(*@rules.map { |rule| CachedRuleTicketCount.key_for(rule, @user) }).returns({})
      CachedRuleTicketCount.lookup(@rules, @user, refresh: false)
    end

    describe "with one entry found" do
      before do
        found_entry = {
          rule: @rules[0],
          user: @user,
          value: rand(10...10_010),
          updated_at: Time.now,
          execution_time: 1.minute
        }
        Rails.cache.expects(:read_multi).returns(CachedRuleTicketCount.key_for(@rules[0], @user) => found_entry)
      end

      it "creates the other entry" do
        CachedRuleTicketCount.expects(:create).with(
          rule: @rules[1],
          user: @user,
          options: { refresh: false, caller: 'inband-count' }
        )
        CachedRuleTicketCount.expects(:create).never
        CachedRuleTicketCount.lookup(@rules, @user, refresh: false)
      end
    end

    describe "with all entries found" do
      before do
        found_entries = {}
        @rules.each do |rule|
          found_entries[CachedRuleTicketCount.key_for(rule, @user)] = {
            rule: rule,
            user: @user,
            value: rand(10...10_010),
            updated_at: Time.now,
            execution_time: 1.minute
          }
        end
        Rails.cache.expects(:read_multi).returns(found_entries)
      end

      it "does not create any entries" do
        CachedRuleTicketCount.expects(:create).never
        CachedRuleTicketCount.lookup(@rules, @user, refresh: false)
      end
    end

    describe "with suspended option" do
      it "includes suspended ticket count" do
        counts = CachedRuleTicketCount.lookup(@rules, @user, refresh: false, suspended: true)
        count  = counts.detect { |c| c.rule_id == 'suspended' }
        assert count.present?
        assert_equal 8, count.value
      end
    end

    describe "with deleted option" do
      it "includes deleted ticket count" do
        # Delete and Scrub one of the tickets to be sure we're not including srubbed tickets in view count
        scrubbed_ticket = tickets(:minimum_2)
        scrubbed_ticket.will_be_saved_by(@user)
        scrubbed_ticket.soft_delete!
        Zendesk::Scrub.scrub_ticket_and_associations(scrubbed_ticket)

        counts = CachedRuleTicketCount.lookup(@rules, @user, refresh: false, deleted: true)
        count  = counts.detect { |c| c.rule_id == 'deleted' }
        assert count.present?
        assert_equal 1, count.value
      end
    end
  end

  describe "lookup multiple by cache_key map" do
    before do
      @rules = [rules(:view_my_working_tickets), rules(:view_assigned_working_tickets)]
      @user = users(:minimum_agent)
      @cache_key_map = {}
      @rules.each { |rule| @cache_key_map[rule.id] = CachedRuleTicketCount.key_for(rule, @user) }
    end

    describe "with all entries found" do
      before do
        found_entries = {}
        @rules.each do |rule|
          found_entries[CachedRuleTicketCount.key_for(rule, @user)] = {
            rule: rule,
            user: @user,
            value: rand(10...10_010),
            updated_at: Time.now,
            execution_time: 1.minute
          }
        end
        Rails.cache.expects(:read_multi).returns(found_entries)
      end

      it "does not create any entries" do
        CachedRuleTicketCount.expects(:create).never
        CachedRuleTicketCount.lookup(@cache_key_map, @user, refresh: false)
      end

      it "does not lookup rules in the database" do
        Rule.expects(:find).never
        CachedRuleTicketCount.lookup(@cache_key_map, @user, refresh: false).map(&:as_json)
      end
    end
  end
end
