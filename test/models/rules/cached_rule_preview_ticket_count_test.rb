require_relative "../../support/test_helper"

SingleCov.covered!

describe CachedRulePreviewTicketCount do
  fixtures :users

  let(:options) { {} }
  let(:rule) { preview }

  subject { CachedRulePreviewTicketCount.new(options.merge(rule: rule, user: rule.owner)) }

  before { stub_occam_count(0) }

  describe "initialization" do
    it "does not set a rule_id" do
      assert_nil subject.rule_id
    end

    it "sets the rule" do
      assert_not_nil subject.rule
      assert subject.rule.new_record?
    end
  end

  describe "#update_delayed" do
    before do
      Timecop.freeze
      subject.update_delayed
    end

    it "updates requested_at" do
      assert_equal Time.now, subject.update_requested_at
    end

    before_should "save" do
      subject.expects(:save).once
    end

    before_should "enqueue a RulePreviewTicketCountJob" do
      RulePreviewTicketCountJob.expects(:enqueue)
    end
  end

  describe "#updated_now" do
    before { subject.update_now }
    let(:job) { mock('RulePreviewTicketCountJob', perform: {}) }

    before_should "perform a RulePreviewTicketCountJob" do
      RulePreviewTicketCountJob.any_instance.expects(:perform)
    end

    before_should 'pass proper caller to occam' do
      RulePreviewTicketCountJob.expects(:new).with(
        anything, anything, anything, true, caller: 'inband-count'
      ).returns(job)
    end
  end

  describe 'when `caller` is passed in during initialization' do
    let(:request_options) { {caller: 'something'} }
    let(:job) { mock('RulePreviewTicketCountJob', perform: {}) }

    let(:preview_count) do
      CachedRulePreviewTicketCount.new(
        rule: rule, user: rule.owner, options: request_options
      )
    end

    before { preview_count.update_now }

    before_should 'pass proper caller to occam' do
      RulePreviewTicketCountJob.expects(:new).with(
        anything, anything, anything, true, caller: 'something'
      ).returns(job)
    end
  end

  describe "#serialized_rule" do
    it "is a YAML dump of the definition" do
      serialized = subject.serialized_rule

      assert_instance_of String, serialized
      assert_equal YAML.dump(rule.definition), serialized
    end
  end

  describe "#save" do
    before do
      subject.value = 1
      subject.save
    end

    it "always write to the cache" do
      assert_not_nil Rails.cache.read(subject.key)
    end

    it "calls super" do
      assert_not_nil subject.exactness_key
    end
  end

  describe "#max_age" do
    describe "with a time_based_rule" do
      before do
        subject.stubs(time_based_rule: true)
      end

      it "returns 1.hour" do
        assert_equal 1.hour, subject.max_age
      end
    end

    describe "without execution time" do
      let(:options) { { value: 1000 } }

      it "returns 30.minutes" do
        assert_equal 30.minutes, subject.max_age
      end
    end

    describe "without value" do
      let(:options) { { execution_time: 1000 } }

      it "returns 30.minutes" do
        assert_equal 30.minutes, subject.max_age
      end
    end

    describe "with a high execution time" do
      let(:options) { { value: 100, execution_time: 0.4 } }

      it "returns 30.minutes" do
        assert_equal 30.minutes, subject.max_age
      end
    end

    describe "with a high ticket count" do
      let(:options) { { value: 12_000, execution_time: 0.2 } }

      it "returns 30.minutes" do
        assert_equal 30.minutes, subject.max_age
      end
    end

    describe "with a low ticket count" do
      let(:options) { { value: 4, execution_time: 0 } }

      it "returns 2^(4/930)" do
        assert_equal 1, subject.max_age
      end
    end

    describe "with a low execution time" do
      let(:options) { { value: 0, execution_time: 0.005 } }

      it "returns 1000*0.005^2" do
        assert_equal 25, subject.max_age
      end
    end
  end

  describe ".key_for" do
    before do
      @left_rule = preview
      @right_rule = preview
    end

    it "takes into account the View#preview_id" do
      assert_equal CachedRulePreviewTicketCount.key_for(@left_rule, @left_rule.owner),
        CachedRulePreviewTicketCount.key_for(@right_rule, @right_rule.owner)

      @right_rule = preview(all: [{ operator: "less_than", field: "status_id", value: StatusType.CLOSED }])

      assert_not_equal CachedRulePreviewTicketCount.key_for(@left_rule, @left_rule.owner),
        CachedRulePreviewTicketCount.key_for(@right_rule, @right_rule.owner)

      @right_rule = preview
      @right_rule.owner = users(:minimum_admin)

      assert_equal CachedRulePreviewTicketCount.key_for(@left_rule, @left_rule.owner),
        CachedRulePreviewTicketCount.key_for(@right_rule, @right_rule.owner)

      @right_rule.owner = users(:with_groups_agent1)

      assert_not_equal CachedRulePreviewTicketCount.key_for(@left_rule, @left_rule.owner),
        CachedRulePreviewTicketCount.key_for(@right_rule, @right_rule.owner)
    end

    it "takes into account the include_archive" do
      @left_rule.include_archived_tickets = false
      @right_rule.include_archived_tickets = true
      assert_not_equal CachedRulePreviewTicketCount.key_for(@left_rule, preview.owner),
        CachedRulePreviewTicketCount.key_for(@right_rule, preview.owner)
    end
  end

  private

  def preview(options = {})
    View.new do |view|
      view.definition = Definition.build(
        conditions_all: options.fetch(:all, [
                                        { operator: "is", field: "status_id", value: StatusType.OPEN }
                                      ]),
        conditions_any: options.fetch(:any, []),
        actions: []
      )

      view.output = Output.default
      view.owner = users(:minimum_agent)
      view.include_archived_tickets = true
    end
  end
end
