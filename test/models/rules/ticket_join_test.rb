require_relative "../../support/test_helper"

SingleCov.covered!

describe 'RuleTicketJoin' do
  fixtures :accounts, :subscriptions, :groups, :memberships, :tickets, :rules, :users

  describe "When rule usage stats are enabled" do
    before do
      Subscription.any_instance.stubs(:has_rule_usage_stats?).returns(true)
    end

    describe "joining some Rules to a Ticket" do
      before do
        @ticket = tickets(:minimum_1)
        @trigger = rules(:trigger_notify_requester_of_received_request)
        @automation = rules(:automation_close_ticket)
        @ticket.rules << @trigger
        @ticket.rules << @automation
        @ticket.will_be_saved_by(@ticket.submitter)
        @ticket.save!
        @ticket.reload
      end

      it "appends the rules to the ticket's :rules association" do
        assert_equal [@trigger, @automation], @ticket.rules
      end

      it "appends the trigger to the ticket's :triggers association" do
        assert_equal [@trigger], @ticket.triggers
      end

      it "appends the automation to the ticket's :automation association" do
        assert_equal [@automation], @ticket.automations
      end

      it "appends the ticket to the rule's :tickets association" do
        assert_equal [@ticket], @trigger.rule_ticket_joins.tickets
        assert_equal [@ticket], @automation.rule_ticket_joins.tickets
      end
    end

    describe "when a Trigger updates a Ticket" do
      before do
        @ticket = tickets(:minimum_1)
        @trigger = rules(:trigger_notify_requester_of_received_request)
        @trigger.apply_actions(@ticket)
      end

      it "creates the ticket join association" do
        assert_difference 'TicketJoin.count(:all)', 1 do
          @ticket.save!
        end
      end

      it "creates a relation between the ticket and the Trigger" do
        @ticket.save!
        assert_equal [@trigger], @ticket.triggers
      end
    end

    describe "when an Automation updates a Ticket" do
      before do
        @ticket = tickets(:minimum_1)
        @automation = rules(:automation_close_ticket)
        @automation.apply_actions(@ticket)
      end

      it "creates the ticket join association" do
        assert_difference 'TicketJoin.count(:all)', 1 do
          @ticket.save!
        end
      end

      it "creates a relation between the ticket and the Automation" do
        @ticket.save!
        assert_equal [@automation], @ticket.automations
      end
    end

    describe ".cleanup" do
      before do
        Timecop.freeze(Time.at(Time.now.to_i))
        17.times do |i|
          Timecop.travel(i.days.ago) do
            TicketJoin.create!(ticket: tickets(:minimum_1),
                               rule_id: 1,
                               created_at: Time.now)
          end
        end
        assert_equal 17, TicketJoin.count(:all)
        assert_equal 8, TicketJoin.where('created_at < ?', 8.days.ago).count(:all)
        assert_equal 9, TicketJoin.where('created_at >= ?', 8.days.ago).count(:all)
        TicketJoin.cleanup
      end

      it "deletes all items older then 8 days" do
        assert_equal 9, TicketJoin.count(:all)
        assert_equal 0, TicketJoin.where('created_at < ?', 8.days.ago).count(:all)
        assert_equal 9, TicketJoin.where('created_at >= ?', 8.days.ago).count(:all)
        assert TicketJoin.all.all? { |x| x.created_at >= 8.days.ago }
      end
    end
  end

  describe "When rule usage stats are not enabled" do
    before { Subscription.any_instance.stubs(:has_rule_usage_stats?).returns(true) }
    describe "and a Trigger updates a Ticket" do
      before do
        @ticket = tickets(:minimum_1)
        @trigger = rules(:trigger_notify_requester_of_received_request)
        @trigger.apply_actions(@ticket)
      end
      it "thes join should not be created." do
        assert_difference 'TicketJoin.count(:all)', 1 do
          @ticket.save!
        end
      end
    end
  end
end
