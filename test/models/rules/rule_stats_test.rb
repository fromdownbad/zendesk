require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'RuleStats' do
  fixtures :accounts, :users, :rules, :subscriptions, :ticket_fields, :custom_field_options

  before do
    @minimum_account = accounts(:minimum)
    @minimum_agent = users(:minimum_admin)
  end

  before do
    # Create a rule the reference a custom field
    definition = Definition.new
    definition.conditions_all.push(DefinitionItem.new('status_id', 'value', [StatusType.PENDING]))
    definition.actions.push(DefinitionItem.new("ticket_fields_#{ticket_fields(:field_tagger_custom).id}", 'is', [custom_field_options(:field_tagger_custom_option_1).id]))
    Trigger.create!(account: accounts(:minimum), title: 'test', definition: definition, owner: accounts(:minimum))

    # Generate the rule analysis stats
    @rule_stats = RuleStats.new(@minimum_account)
  end

  it "contains valid tag stats" do
    assert_equal ["cascade1", "cascade2"].sort, Rule.find(@rule_stats.tag['hereiam'].rules).map(&:title).sort
    assert_equal 5, @rule_stats.tag.count
  end

  it "contains valid assignee stats" do
    assert_equal ["Auto-assign to first email responding agent", "Inactive Macro for Minimum Account", "Incident escalation", "Minimum Account's Active View", "Minimum Account's Inactive View", "My Solved Tickets", "My working tickets", "Notify assignee of assignment", "Notify assignee of comment update", "Notify blank email", "Your unsolved tickets"].sort, Rule.find(@rule_stats.assignee_id['current_user'].rules).map(&:title).sort
    assert_equal 3, @rule_stats.assignee_id.count
    assert_equal 8, @rule_stats.assignee_id['current_user'].condition_rules.count
    assert_equal 3, @rule_stats.assignee_id['current_user'].action_rules.count
  end

  it "does not marshal account" do
    @rule_stats.instance_variables.each do |name|
      variable = @rule_stats.instance_variable_get(name)
      refute(variable.instance_of?(Account))
    end
  end
end
