require_relative "../../support/test_helper"
require_relative "../../support/rules_test_helper"

SingleCov.covered! uncovered: 33

describe Rule do
  include CustomFieldsTestHelper
  include RulesTestHelper

  fixtures :rules, :accounts, :subscriptions, :users, :tickets, :groups,
    :ticket_fields, :custom_field_options, :targets, :taggings,
    :events, :organizations, :sequences, :groups, :user_identities,
    :memberships

  let(:account) { accounts(:minimum) }

  describe 'validations' do
    subject do
      Rule.new(definition: Definition.new)
    end
    should_validate_presence_of :account
    should_validate_presence_of :owner
    should_serialize :definition, :output
  end

  #######################################################
  # Test actions
  #######################################################

  before do
    @account = accounts(:minimum)
  end

  describe '.group_owner_id' do
    it 'returns the highest positive bigint value' do
      assert_equal 256**8 / 2 - 1, Rule.group_owner_id
    end
  end

  describe ".with_ids" do
    let(:rule_1) { rules(:view_my_working_tickets) }
    let(:rule_2) { rules(:view_assigned_working_tickets) }

    it "returns rules with the specified IDs in no particular order" do
      assert_equal(
        [rule_2, rule_1],
        account.rules.with_ids([rule_1.id, rule_2.id])
      )
    end
  end

  describe ".with_ordered_ids" do
    let(:rule_1) { rules(:view_my_working_tickets) }
    let(:rule_2) { rules(:view_assigned_working_tickets) }

    it "returns rules with the specified IDs in the provided order" do
      assert_equal(
        [rule_1, rule_2],
        account.rules.with_ordered_ids([rule_1.id, rule_2.id])
      )
    end
  end

  describe ".with_title" do
    let(:rule_1) { rules(:view_assigned_working_tickets) }
    let(:rule_3) { rules(:view_my_working_tickets) }

    describe "when there are matching rules" do
      it "returns the matching rules" do
        assert_equal [rule_1, rule_3], account.rules.with_title("working")
      end
    end

    describe "when there are no matching rules" do
      it "returns no rules" do
        assert_equal [], account.rules.with_title("test")
      end
    end
  end

  describe ".in_group" do
    let(:group_1) { groups(:minimum_group) }
    let(:group_2) { groups(:support_group) }
    let(:group_3) { groups(:billing_group) }

    let(:rule_1) { rules(:view_for_minimum_agent) }
    let(:rule_2) { rules(:view_for_minimum_admin) }
    let(:rule_3) { rules(:active_view_for_minimum_group) }

    before do
      account.all_views.each do |rule|
        rule.update_attributes(owner_type: 'Group', owner_id: group_2.id)
      end

      rule_1.update_attributes(owner_type: 'Group', owner_id: group_1.id)
      rule_2.update_attributes(owner_type: 'Group', owner_id: group_2.id)
      rule_3.update_attributes(owner_type: 'Group', owner_id: group_3.id)
    end

    it "returns rules in the specified groups" do
      assert_equal(
        [rule_1, rule_3],
        View.where(account_id: account.id).in_group([group_1, group_3].map(&:id))
      )
    end
  end

  describe ".csv_threshold" do
    after { Rails.unstub(:env) }

    it "returns 5 if development" do
      Rails.stubs(:env).returns('development')
      assert_equal 5, Rule.csv_threshold
    end

    it "returns 20 if master" do
      Rails.stubs(:env).returns('master')
      assert_equal 20, Rule.csv_threshold
    end

    it "returns 50 if staging" do
      Rails.stubs(:env).returns('staging')
      assert_equal 50, Rule.csv_threshold
    end

    it "returns 500 if anything other than dev, master or staging" do
      Rails.stubs(:env).returns('mojn')
      assert_equal 500, Rule.csv_threshold
    end
  end

  describe ".find_for_user" do
    before do
      @user = users(:minimum_admin_not_owner)
      @view = rules(:view_assigned_working_tickets)
    end

    describe "when the rule is personal" do
      before do
        @view.update_attributes(owner_type: User.name, owner_id: @user.id)
      end

      describe "and the user is the rule owner" do
        it "returns the rule" do
          assert_equal @view, Rule.find_for_user(@user, @view.id)
        end
      end

      describe "and the user is not the rule owner" do
        describe "and the user can assume the owner" do
          before { @other = users(:minimum_admin) }

          it "returns the rule" do
            assert_equal @view, Rule.find_for_user(@user, @view.id)
          end
        end

        describe "and the user cannot assume the owner" do
          before { @other = users(:minimum_agent) }

          it "does not return the rule" do
            assert_nil Rule.find_for_user(@other, @view.id)
          end
        end
      end
    end

    describe "when the rule is a group rule" do
      before do
        @group = groups(:minimum_group)

        @view.update_attributes(owner_type: Group.name, owner_id: @group.id)
      end

      describe "and the user is in the group" do
        before { @user = users(:minimum_admin) }

        it "returns the rule" do
          assert_equal @view, Rule.find_for_user(@user, @view.id)
        end
      end

      describe "and the user is not in the group" do
        before { @user = users(:minimum_admin_not_owner) }

        describe "and the user is an admin" do
          it "returns the rule" do
            assert_equal @view, Rule.find_for_user(@user, @view.id)
          end
        end

        describe "and the user is an agent" do
          before { @user.stubs(:is_admin?).returns(false) }

          it "does not return the rule" do
            assert_nil Rule.find_for_user(@user, @view.id)
          end
        end
      end
    end

    describe "when the rule is an account rule" do
      before do
        @view.update_attributes(
          owner_type: Account.name,
          owner_id:   @account.id
        )
      end

      it "returns the rule" do
        assert_equal @view, Rule.find_for_user(@user, @view.id)
      end
    end
  end

  describe "validation" do
    describe "title is empty" do
      it "does not allow a rule to be saved" do
        rule = build_rule(type: "View", conditions_all: ['status_id', 'less_than', [StatusType.OPEN]])
        rule.title = '';
        refute rule.save
        assert_includes rule.errors.full_messages, I18n.t('txt.view.form.error_message.title_needed')
      end
    end

    describe "invalid conditions" do
      it "does not allow a rule with a definition of status id less than new" do
        rule = build_rule(type: "View", conditions_all: ['status_id', 'less_than', [StatusType.NEW]])
        refute rule.save
        assert_includes rule.errors.full_messages, "The condition Status Less Than New is not a valid status"
      end

      ["Trigger", "Automation"].each do |rule_type|
        describe rule_type.to_s do
          let(:rule) do
            build_rule(
              type: rule_type,
              conditions_all: ['status_id', 'greater_than', [StatusType.NEW]],
              actions: [action, ['status_id', 'is', [StatusType.PENDING]]]
            )
          end

          ["notification_user", "notification_group", "deflection"].each do |action_source|
            describe action_source.to_s do
              let(:action) do
                [
                  action_source,
                  'is',
                  ["person@zendesk.com", "Email subject", "c" * bytes]
                ]
              end

              describe "with a an email bigger than 8192 bytes" do
                let(:bytes) { 8193 }

                it "adds email size error message" do
                  assert_includes rule.tap(&:save).errors.full_messages, "Email size cannot exceed 8192 bytes"
                end
              end

              describe "with an email 8192 bytes or less" do
                let(:bytes) { 8192 }

                it "does not add email size error message" do
                  refute_includes rule.tap(&:save).errors.full_messages, "Email size cannot exceed 8192 bytes"
                end
              end
            end
          end

          describe "notification_target" do
            let(:action) { ['notification_target', 'is', action_value] }

            describe "when the target format is parameterized" do
              let(:action_value) { [1, [[key, "value"]]] }

              describe "and the URL parameter key is empty" do
                let(:key) { '' }

                it "adds invalid URL parameters error" do
                  assert_includes(
                    rule.tap(&:save).errors.full_messages,
                    I18n.t('txt.error_message.rules.rule.invalid_url_target_parameters')
                  )
                end
              end

              describe "and the URL parameter key is valid" do
                let(:key) { 'key' }

                it "does not add invalid URL parameters error" do
                  refute_includes(
                    rule.tap(&:save).errors.full_messages,
                    I18n.t('txt.error_message.rules.rule.invalid_url_target_parameters')
                  )
                end
              end
            end

            describe "when the target format is a string" do
              let(:action_value) { [1, 'a' * request_body_length] }

              describe "and the request body exceeds 8192 characters" do
                let(:request_body_length) { 8193 }

                it "adds external target size error" do
                  assert_includes rule.tap(&:save).errors.full_messages, I18n.t('txt.error_message.rules.rule.external_target_size_error')
                end
              end

              describe "and the request body is 8192 characters or fewer" do
                let(:request_body_length) { 8192 }

                it "does not add external target size error" do
                  refute_includes rule.tap(&:save).errors.full_messages, I18n.t('txt.error_message.rules.rule.external_target_size_error')
                end
              end
            end
          end
        end
      end

      it "does not allow a definition with size greater than 65535" do
        rule = build_rule(type: "Macro", actions: [["comment_value", "is", ["channel:all", "X" * 70000]]])
        refute rule.save
        assert_equal 1, rule.errors.full_messages.length
        assert_equal "is too long (maximum is 65535 characters after encoding)", rule.errors.full_messages[0]
      end

      it "does not allow a title with length greater than 255" do
        rule = build_rule(type: "Macro", title: "X" * 256, actions: [["comment_value", "is", ["channel:all", "X" * 555]]])
        refute rule.save
        assert_equal 1, rule.errors.full_messages.length
        assert_equal "Invalid title. Title must not exceed 255 characters.", rule.errors.full_messages[0]
      end

      it "does not allow a description with length greater than 512" do
        rule = build_rule(
          type:        "Macro",
          description: "X" * 513,
          actions:     [["comment_value", "is", ["channel:all", "X" * 555]]]
        )

        refute rule.save

        assert_equal 1, rule.errors.full_messages.length
        assert_equal "Invalid description. Description must not exceed 512 characters.", rule.errors.full_messages[0]
      end

      it "does not allow a rule with a definition of status id greater than closed" do
        rule = build_rule(type: "View", conditions_all: ['status_id', 'greater_than', [StatusType.CLOSED]])
        refute rule.save
        assert_includes rule.errors.full_messages, "The condition Status Greater Than Closed is not a valid status"
      end

      it "does not allow a rule with a definition of group_id from a deleted group" do
        rule = build_rule(type: "View", conditions_all: ['group_id', 'is', 123456])
        refute rule.save
        assert_includes rule.errors.full_messages, "Group 123456 was deleted and cannot be used"
      end

      it "does not allow a rule with a definition of requester_id from a deleted user" do
        user = users(:minimum_end_user)
        user.update_attribute(:is_active, false)
        rule = build_rule(type: "View", conditions_all: ['requester_id', 'is', user.id])
        refute rule.save
        assert_includes rule.errors.full_messages, "User #{user.id} was deleted and cannot be used"
      end

      it "does not allow a rule with a definition of assignee_id from a deleted user" do
        user = users(:minimum_end_user)
        user.update_attribute(:is_active, false)
        rule = build_rule(type: "View", conditions_all: ['assignee_id', 'is', user.id])
        refute rule.save
        assert_includes rule.errors.full_messages, "User #{user.id} was deleted and cannot be used"
      end

      it "allows a rule with a definition of assignee_id from an agent that is not assignable if she is assigned any tickets (only applies to closed tickets)" do
        user = users(:minimum_agent)
        ActiveRecord::Relation.any_instance.stubs(:pluck).returns([])
        rule = build_rule(type: "View", conditions_all: ['assignee_id', 'is', user.id])
        assert rule.save
      end

      it "does not bypass all other validations when assignee_id validation is skipped" do
        user = users(:minimum_agent)
        ActiveRecord::Relation.any_instance.stubs(:pluck).returns([])
        rule = build_rule(type: "View", conditions_all: [['assignee_id', 'is', user.id], ['group_id', 'is', 123456]])
        refute rule.save
      end

      it "does not allow a rule with a definition of organization_id from a deleted organization" do
        organization = organizations(:minimum_organization1)
        organization.update_attribute(:deleted_at, Time.now)
        rule = build_rule(type: "View", conditions_all: ['organization_id', 'is', organization.id])
        refute rule.save
        assert_includes rule.errors.full_messages, "Organization #{organization.id} was deleted and cannot be used"
      end

      it 'does not allow a notify user trigger of email user blank' do
        trigger = build_rule(type: "Trigger", status: :closed,
                             actions: ['notification_user', 'is', ["", "Email subject", "Email body"]])

        refute trigger.save
        assert_equal ['The <strong>Email user</strong> cannot be blank'], trigger.errors.full_messages
      end

      it 'does not blow up on the usage of now as a date' do
        trigger = build_rule(type: "Trigger", status: :closed, actions: ['notification_user', 'is', ["current_user", "Email subject", "{{'now' | date: \"%Y\" }}"]])
        assert trigger.save
      end

      it "allows improperly terminated template" do
        # this liquid template is a variation of a problem template submitted by uber
        rule = build_rule(type: "Macro", actions: [["comment_value", "is", ["channel:web", "Hi {{ticket.requester.first_name | capitalize}},Uber is a technology platform.{% if ticket.id == '298' %}{% comment%} TOLEDO {% endcomment %}Full vehicle list can be found at http://uberohio.com/vehicles\r\n\r\n{% elsif ticket.id == '142' %}{% comment%} CLEVELAND {% endcomment %Full vehicle list at }cleveland\r\n\r\n{% elsif ticket.id == '33' %}{% comment%} NAPA, CA {% endcomment %}napa{% endif %}We look forward to getting you on the road!"]]])
        assert rule.save
      end
    end

    describe "invalid force_key" do
      let(:rule) { build_rule(type: "View", conditions_all: ['status_id', 'less_than', [StatusType.OPEN]]) }

      describe 'with an invalid force_key' do
        before { rule.force_key = 'nothing_here' }

        it 'returns invalid' do
          refute rule.save
          assert_includes rule.errors.full_messages, 'Invalid index added to rule.'
        end
      end

      describe 'with a valid force_key in negative form' do
        before { rule.force_key = '-group_id' }

        it 'returns valid' do
          assert rule.save
        end
      end
    end

    describe "new validations" do
      describe "with the arturo flag on" do
        it "runs" do
          rule = build_rule(conditions_all: [:status_id, :is, 123456])

          refute rule.save
          assert rule.errors.full_messages.grep(/Invalid value/).any?
        end

        it "is bypassed for hidden targets" do
          rule = build_rule(status: :NEW, conditions_all: [:group_is_set_with_no_assignee, :is, 123456])
          assert rule.save!
        end
      end
    end
  end

  describe "#invalid_recipient_email" do
    it "returns is valid if it rule is a Macro" do
      macro = rules(:macro_incident_escalation)
      assert macro.send(:invalid_recipient_email)
    end

    it "returns is valid if we dont detect an invalid recipient" do
      trigger = rules(:trigger_notify_requester_of_received_request)
      trigger.stubs(:conditions_detects_invalid_recipient).returns(false)
      assert trigger.send(:invalid_recipient_email)
    end

    it "returns error if we detect invalid recepient" do
      trigger = rules(:trigger_notify_requester_of_received_request)
      trigger.stubs(:conditions_detects_invalid_recipient).returns(true)
      trigger.send(:invalid_recipient_email)

      assert_equal ["Failed to add new email address in 'Ticket received at'"], trigger.errors.full_messages
    end
  end

  describe "#conditions_detects_invalid_recipient" do
    before do
      @trigger = rules(:trigger_notify_requester_of_received_request)
    end

    it "returns false if all_and_any_conditions is empty" do
      @trigger.stubs(:all_and_any_conditions).returns(nil)
      refute @trigger.send(:conditions_detects_invalid_recipient)
    end

    it "returns false if no condition with recipient is found" do
      all_and_any_conditions = @trigger.send(:all_and_any_conditions)
      refute all_and_any_conditions.detect { |c| c.source == 'recipient' }
      refute @trigger.send(:conditions_detects_invalid_recipient)
    end

    it "returns false if email is valid" do
      @trigger.definition = Definition.new
      @trigger.definition.conditions_all.push DefinitionItem.new('recipient', nil, ["valid@email.com"])
      refute @trigger.send(:conditions_detects_invalid_recipient)
    end

    it "returns true if email is empty" do
      @trigger.definition = Definition.new
      @trigger.definition.conditions_all.push DefinitionItem.new('recipient', nil, [""])
      assert @trigger.send(:conditions_detects_invalid_recipient)
    end

    it "allows saving if emails is not complete and support_incomplete_emails is true" do
      @trigger.definition = Definition.new
      @trigger.definition.conditions_all.push DefinitionItem.new('recipient', nil, ["invalid"])
      @trigger.stubs(:support_incomplete_emails?).returns(true)
      ZendeskExceptions::Logger.expects(:record).once
      refute @trigger.send(:conditions_detects_invalid_recipient)
    end

    it "does not allow saving is email is not complete and support_incomplete_emails is false" do
      @trigger.definition = Definition.new
      @trigger.definition.conditions_all.push DefinitionItem.new('recipient', nil, ["invalid"])
      @trigger.expects(:support_incomplete_emails?).returns(false)
      ZendeskExceptions::Logger.expects(:record).never
      assert @trigger.send(:conditions_detects_invalid_recipient)
    end
  end

  describe "#support_incomplete_emails?" do
    before do
      @trigger = rules(:trigger_notify_requester_of_received_request)
    end

    it "returns false if env is not production" do
      refute @trigger.send(:support_incomplete_emails?)
    end

    it "returns false if the trigger it is created after april 4" do
      Rails.env.stubs(:production?).returns(true)
      @trigger.stubs(:created_at).returns(Time.new(2013, 5, 20))
      refute @trigger.send(:support_incomplete_emails?)
    end

    it "returns true if env is production and it was created before april 4" do
      Rails.env.stubs(:production?).returns(true)
      @trigger.stubs(:created_at).returns(Time.new(2012, 5, 20))
      assert @trigger.send(:support_incomplete_emails?)
    end

    it "does not explode if the rule is new" do
      Rails.env.stubs(:production?).returns(true)
      @create_trigger = Rule.new
      refute @create_trigger.send(:support_incomplete_emails?)
    end
  end

  describe "#account" do
    it "returns the account if the owner is an account" do
      rule = Automation.new
      rule.owner = accounts(:minimum)
      assert_equal accounts(:minimum), rule.account
      assert_equal accounts(:minimum), rule.account_without_owner_backstop
    end

    it "returns the account of the owner if owner isn't an account" do
      rule = Automation.new
      rule.owner = users(:minimum_admin)
      assert_equal users(:minimum_admin).account, rule.account
      assert_equal users(:minimum_admin).account, rule.account_without_owner_backstop
    end

    it "returns nil if owner is not set" do
      rule = Automation.new
      assert_nil rule.account
    end
  end

  describe "#group" do
    it "is set from the owner if the owner is a group" do
      rule = Automation.new
      rule.owner = groups(:with_groups_group1)
      assert_equal rule.group, groups(:with_groups_group1)
      assert_equal rule.account, groups(:with_groups_group1).account
    end

    it "does not be set if the owner is a user" do
      rule = Automation.new
      rule.owner = users(:minimum_admin)
      assert_nil rule.group
    end
  end

  describe "#user" do
    it "is set from the owner if the owner is a user" do
      rule = Automation.new
      rule.owner = users(:minimum_admin)
      assert_equal rule.user, users(:minimum_admin)
    end
  end

  describe "#rendered_title" do
    let(:params) do
      {
        name: "Foo Bar",
        account: accounts(:minimum),
        fallback_attributes: {
          is_fallback: "true",
          nested: true,
          value: "English Title",
          translation_locale_id: 1
        }
      }
    end
    let(:cms_text) { Cms::Text.create!(params) }
    let(:cms_variant) do
      cms_text.variants.create(
        value: "Spanish Title",
        translation_locale_id: 2
      )
    end
    let(:rule) do
      build_rule(
        status: :NEW,
        title: "{{dc.foo_bar}}",
        type: rule_type,
        conditions_all: %i[group_is_set_with_no_assignee is 123_456]
      )
    end
    let(:rule_type) { "View" }

    it "evaluates dynamic content" do
      assert_equal cms_text.fallback.value, rule.rendered_title
    end

    describe "macros" do
      let(:rule_type) { "Macro" }
      describe "with the dc_in_macro_titles arturo flag off" do
        before do
          Account.any_instance.expects(:has_dc_in_macro_titles?).returns(false)
        end

        it "renders the title as it is in the db" do
          assert_equal "{{dc.foo_bar}}", rule.rendered_title
        end
      end

      describe "with the dc_in_macro_titles arturo flag on" do
        it "evaluates dynamic content" do
          assert_equal cms_text.fallback.value, rule.rendered_title
        end
      end
    end
  end

  describe "app_installation" do
    let(:rule) { rules(:view_assigned_working_tickets) }

    describe "when set" do
      before do
        rule.app_installation = {
          'id'       => 42,
          'settings' => {'title' => 'AAA'}
        }
      end

      it "returns the app installation" do
        assert_equal 'AAA', rule.app_installation['settings']['title']
      end
    end

    describe "when not set" do
      let(:zam_client) { mock('ZAM client') }

      before do
        zam_client.
          stubs(:installations).
          with([rule.id]).
          returns(installations)

        Zendesk::AppMarketClient.stubs(:new).
          with(account.subdomain).
          returns(zam_client)
      end

      describe "and it has an app installation" do
        let(:installations) do
          {rule.id => {'settings' => {'title' => 'BBB'}}}
        end

        it "returns the app installation" do
          assert_equal 'BBB', rule.app_installation['settings']['title']
        end
      end

      describe "and it does not have an app installation" do
        let(:installations) { {} }

        it "returns a null app installation" do
          assert_equal(
            Zendesk::Rules::NullAppInstallation,
            rule.app_installation
          )
        end
      end
    end
  end

  describe "#lotus_permalink" do
    let(:domain) { 'https://non-hostmapped.zendesk.com' }
    let(:rule)   { account.views.first }

    before do
      Account.
        any_instance.
        stubs(:url).
        with(mapped: false, ssl: true).
        returns(domain)
    end

    it "returns a non-hostmapped Lotus permalink" do
      assert_equal(
        "#{domain}/agent/admin/views/#{rule.id}",
        rule.lotus_permalink
      )
    end
  end

  describe "#lotus_relative_link" do
    let(:rule) { account.triggers.first }

    it "returns the relative Lotus link" do
      assert_equal(
        "/agent/admin/triggers/#{rule.id}",
        rule.lotus_relative_link
      )
    end
  end

  it "provides its state" do
    @rule = rules(:view_my_working_tickets)

    @rule.is_active = true
    assert_equal 'activated', @rule.state

    @rule.is_active = false
    assert_equal 'deactivated', @rule.state
  end

  describe "toggle activation" do
    before do
      @rule = rules(:view_my_working_tickets)
    end

    it "changes active to inactive" do
      @rule.is_active = true
      @rule.toggle_activation

      assert_equal false, @rule.is_active
    end

    it "changes inactivate to active" do
      @rule.class.expects(:where).returns(stub(maximum: 10000))
      @rule.is_active = false
      @rule.toggle_activation

      assert(@rule.is_active)
      assert_equal 10001, @rule.position
    end
  end

  describe "position" do
    before do
      @account = accounts('minimum')
    end

    describe "without a position set" do
      before do
        @rule = View.new(account: @account)
        @rule.send :run_callbacks, :create
      end

      it "is last for a new rule" do
        maximum = View.where(account_id: @account.id).maximum(:position)
        assert_equal maximum + 1, @rule.position
      end
    end

    describe "with a position set" do
      before do
        @rule = View.new(account: accounts('minimum'), position: 34)
        @rule.send :run_callbacks, :create
      end

      it "is fixed for a new rule with position set" do
        assert_equal 34, @rule.position
      end
    end

    it "is allowed to be updated" do
      rule = rules(:view_my_working_tickets)
      initial_position = rule.position

      rule.position = 1
      assert rule.save!
      refute_equal initial_position, rule.position
    end
  end

  describe "sorting" do
    before do
      Zendesk.stubs(:current_account).returns(@account)
      @rules = @account.views.sort_by(&:id)
      assert_not_equal @rules.reverse, @account.views(order: 'position')
    end

    describe "and ids is an array of strings" do
      before do
        @account.views.update_position(@rules.reverse.map { |r| r.id.to_s })
      end

      it "updates position based on the provided ids" do
        assert_equal @rules.reverse.map(&:id), @account.views(order: 'position').map(&:id)
      end
    end

    describe "and ids is an array of integers" do
      before do
        @account.views.update_position(@rules.reverse.map(&:id))
      end

      it "updates position based on the provided ids" do
        assert_equal @rules.reverse.map(&:id), @account.views(order: 'position').map(&:id)
      end
    end

    it "does not generate N+1 queries" do
      shuffled_ids = @rules.map(&:id).shuffle
      assert_sql_queries 1, /UPDATE `rules`/ do
        @account.views.update_position(shuffled_ids)
      end
      assert_equal shuffled_ids, @account.reload.views.map(&:id)
    end

    describe "with system rules" do
      before do
        @system_rule = Rule.new(
          title: "Poke Salesforce when ticket is created",
          definition: Definition.new,
          owner: @account,
          is_active: true,
          position: 3,
          feature_identifier: 'SalesforceIntegration'
        )
      end

      it "is able to make changes when writable is passed" do
        @system_rule.update_attribute(:is_locked, true)

        refute @system_rule.new_record?
        assert @system_rule.is_locked?
        assert @system_rule.is_system_rule?

        assert @system_rule.readonly?
        assert_raise ActiveRecord::ReadOnlyRecord do
          @system_rule.update_attribute(:title, "This shouldn't be possible")
        end

        @system_rule.writable = true
        @system_rule.update_attribute(:position, 9999)
      end
    end
  end

  describe "copying" do
    before do
      @rule = rules(:view_my_working_tickets)
      @rule.save!
    end

    it "copies basic attributes" do
      assert @rule.owner
      assert @rule.per_page
      assert @rule.title = "<script>alert(1);</script>"

      copy = @rule.copy
      assert_equal @rule.owner,     copy.owner
      assert_equal @rule.is_active, copy.is_active
      assert_equal @rule.per_page,  copy.per_page
      assert_equal 'Copy of &lt;script&gt;alert(1);&lt;/script&gt;', copy.title

      assert_nil copy.id
      assert_nil copy.created_at
    end
  end

  describe "#find_ticket" do
    subject { rules(:view_my_working_tickets) }

    before do
      @ticket = tickets(:minimum_1)
      @agent = users(:minimum_agent)

      Zendesk::Rules::OccamClient.
        any_instance.
        stubs(:find).
        returns('ids' => [@ticket.id])
    end

    it "does not modify the conditions" do
      old = subject.definition.conditions_all.dup
      subject.find_ticket(@ticket.nice_id, @agent)
      assert_equal old, subject.definition.conditions_all
    end

    describe "with a valid ticket id" do
      it "returns ticket" do
        assert_equal @ticket, subject.find_ticket(@ticket.nice_id, @agent)
      end
    end

    describe "with an invalid ticket id" do
      before do
        Zendesk::Rules::OccamClient.
          any_instance.
          stubs(:find).
          returns('ids' => [])
      end

      it "returns nil" do
        assert_nil subject.find_ticket(999999, @agent)
      end
    end
  end

  describe "#matches_ticket?" do
    subject { rules(:automation_close_ticket) }
    let(:ticket) { tickets(:minimum_4) }

    it "returns true if ticket matches" do
      Zendesk::Rules::OccamClient.
        any_instance.
        stubs(:find).
        returns('ids' => [ticket.id])

      assert(subject.matches_ticket?(ticket.nice_id))
    end

    it "returns false if ticket does not match" do
      ticket.will_be_saved_by(User.system)
      ticket.update_attributes!(status_id: 4)

      Zendesk::Rules::OccamClient.
        any_instance.
        stubs(:find).
        returns('ids' => [])

      assert_equal false, subject.matches_ticket?(ticket.nice_id)
    end
  end

  describe "#find_tickets" do
    it "throttles requests that are out of bounds on pages" do
      user    = users(:minimum_agent)
      rule    = rules(:view_my_working_tickets)
      tickets = []
      Zendesk::Rules::RuleExecuter.any_instance.stubs(:find_tickets).returns(tickets)

      49.times do
        Zendesk::Rules::RuleExecuter::EmptyPageThrottling.send(:throttle!, rule, user)
      end

      assert_equal [], rules(:view_assigned_working_tickets).find_tickets(user, paginate: true, page: 3)
    end

    it "does not throttle unpaged requests when already throttled" do
      user    = users(:minimum_agent)
      rule    = rules(:view_my_working_tickets)
      tickets = []
      Zendesk::Rules::RuleExecuter.any_instance.stubs(:find_tickets).returns(tickets)

      50.times do
        Zendesk::Rules::RuleExecuter::EmptyPageThrottling.send(:throttle!, rule, user)
      end
      assert Zendesk::Rules::RuleExecuter::EmptyPageThrottling.send(:throttled?, rule, user)

      assert rule.find_tickets(user)
      assert rule.find_tickets(user)
    end

    it "does not throttle queries with results" do
      user    = users(:minimum_agent)
      rule    = rules(:view_my_working_tickets)
      tickets = [tickets(:minimum_1)]
      Zendesk::Rules::RuleExecuter.any_instance.stubs(:find_tickets).returns(tickets)

      49.times do
        Zendesk::Rules::RuleExecuter::EmptyPageThrottling.send(:throttle!, rule, user)
      end

      assert rule.find_tickets(user, paginate: true, page: 3)
      assert rule.find_tickets(user, paginate: true, page: 3)
    end

    it "includes extra associations if :requestor, :assignee or :submitter are included" do
      user    = users(:minimum_agent)
      rule    = rules(:view_my_working_tickets)

      executer = Zendesk::Rules::RuleExecuter.new(rule, user, {})
      exec_includes = executer.execution_options.includes
      assert exec_includes.is_a?(Hash)

      expected_includes = { photo: {}, identities: {}, taggings: {}, groups: {} }
      assert_equal expected_includes, exec_includes[:requester]
    end

    it "does not include two order modifiers that are the same" do
      user    = users(:minimum_agent)
      rule    = rules(:view_my_working_tickets)
      rule.output.stubs(:order).returns(:nice_id)

      executer = Zendesk::Rules::RuleExecuter.new(rule, user, {})
      assert_equal 1, executer.query.order.join(" ").downcase.scan(/`nice_id` desc/).size
    end
  end

  it "tests user specific?" do
    user_definition_item = DefinitionItem.new('assignee_id', 'is', 'current_user')
    assert(user_definition_item.user_specific?)

    no_user_definition_item = DefinitionItem.new('assignee_id', 'is', '4')
    assert(!no_user_definition_item.user_specific?)

    definition = Definition.new
    assert(!definition.user_specific?)

    definition.conditions_all.push(no_user_definition_item)
    definition.conditions_any.push(no_user_definition_item)
    assert(!definition.user_specific?)

    definition = Definition.new
    definition.conditions_all.push(user_definition_item)
    assert(definition.user_specific?)

    definition = Definition.new
    definition.conditions_any.push(user_definition_item)
    assert(definition.user_specific?)
  end

  it "tests group specific?" do
    group_definition_item = DefinitionItem.new('group_id', 'not_includes', 'current_groups')
    assert(group_definition_item.group_specific?)

    no_group_definition_item = DefinitionItem.new('group_id', 'is', '4')
    assert(!no_group_definition_item.group_specific?)

    definition = Definition.new
    assert(!definition.group_specific?)

    definition.conditions_all.push(no_group_definition_item)
    definition.conditions_any.push(no_group_definition_item)
    assert(!definition.group_specific?)

    definition = Definition.new
    definition.conditions_all.push(group_definition_item)
    assert(definition.group_specific?)

    definition = Definition.new
    definition.conditions_any.push(group_definition_item)
    assert(definition.group_specific?)
  end

  describe "Rule#shared?" do
    before { @rule = Rule.new }

    it "returns true when the owner is an Account" do
      @rule.owner = Account.new

      assert @rule.shared?
    end

    it "returns true when the owner is a Group" do
      @rule.owner = Group.new

      assert @rule.shared?
    end

    it "returns false when the owner is a User" do
      @rule.owner = User.new

      refute @rule.shared?
    end
  end

  describe "Rule#serialization_options" do
    before do
      @rule = Rule.new
      @rule.owner = Account.new
    end

    [:id, :per_page, :position, :title, :is_active].each do |serializable_attribute|
      it "includes the #{serializable_attribute} attribute" do
        assert @rule.send(:serialization_options)[:only].include?(serializable_attribute)
      end
    end
  end

  describe "Macro sorting" do
    before do
      @account = accounts(:minimum)
      sorted_macros = @account.shared_macros.sort_by(&:title)
      sorted_macros.first.update_attribute(:position, sorted_macros.last.position + 1)
      @account.reload
    end

    describe "when the `macro_order` setting is `alphabetical`" do
      before do
        @account.settings.set(macro_order: "alphabetical")

        @account.settings.save!
      end

      it "sorts macros by title" do
        assert_equal(
          @account.shared_macros.sort_by(&:title).map(&:title),
          @account.shared_macros.map(&:title)
        )
      end
    end

    describe "when the `macro_order` setting is not `alphabetical`" do
      before do
        @account.settings.set(macro_order: "position")

        @account.settings.save!
      end

      it "sorts macros by position" do
        assert_equal(
          @account.shared_macros.sort_by(&:position).map(&:position),
          @account.shared_macros.map(&:position)
        )
      end
    end
  end

  describe "View sorting" do
    before do
      @account = accounts(:minimum)
      sorted_views = @account.shared_views.sort_by(&:position)
      assert sorted_views.size > 1
      sorted_views.first.update_attribute(:position, sorted_views.last.position + 1)
      @account.reload
    end

    it "sorts by position when group rules are enabled" do
      @account.subscription.stubs(:has_group_rules?).returns(true)
      assert_equal @account.shared_views.sort_by(&:position).map(&:position), @account.shared_views.map(&:position)
    end

    it "sorts by position when group rules are disabled" do
      @account.subscription.stubs(:has_group_rules?).returns(false)
      assert_equal @account.shared_views.sort_by(&:position).map(&:position), @account.shared_views.map(&:position)
    end
  end

  describe "#query_cache_key" do
    let(:user) { users(:minimum_agent) }
    let(:rule_1) { new_rule(DefinitionItem.new("until_sla_next_breach_at", "greater_than", "4"), type: :conditions_all) }
    let(:rule_2) { new_rule(DefinitionItem.new("until_sla_next_breach_at", "greater_than", "4"), type: :conditions_all) }

    it "is same for same user" do
      rule = new_rule(DefinitionItem.new('priority_id', nil, PriorityType.HIGH))
      assert_equal rule.query_cache_key(user), rule.query_cache_key(user)
    end

    it "is different for different accounts and absolute rule" do
      rule = new_rule(DefinitionItem.new('priority_id', nil, PriorityType.HIGH))
      assert_not_equal rule.query_cache_key(user), rule.query_cache_key(users(:support_agent))
    end

    it "is same for different users in absolute rule" do
      rule = new_rule(DefinitionItem.new('priority_id', nil, PriorityType.HIGH))
      assert_equal rule.query_cache_key(user), rule.query_cache_key(users(:minimum_admin))
    end

    it "is different for different users in relative rule" do
      rule = rules(:view_my_working_tickets)
      assert_not_equal rule.query_cache_key(user), rule.query_cache_key(users(:minimum_admin))
    end

    it "is same for time-sensitive rule" do
      rule = new_rule(DefinitionItem.new("updated_at", "less_than", "24"), type: :conditions_all)
      old = rule.query_cache_key(user)
      Timecop.freeze 1.hour.from_now do
        assert_equal old, rule.query_cache_key(user)
      end
    end

    describe "invalid conditions" do
      let(:rule) do
        definition = Definition.new
        definition.conditions_all.push(DefinitionItem.new("status_id", "is", "3"))
        definition.conditions_all.push(DefinitionItem.new("status_id", "is", "4"))
        View.new(
          definition: definition,
          owner: accounts(:minimum),
          output: Output.create(columns: View::COLUMNS)
        )
      end

      it "returns cache key" do
        assert_equal 'invalid-conditions', rule.query_cache_key(user)
      end
    end

    it "does not mutate the key with a long format time" do
      sample_1 = rule_1.query_cache_key(user)
      Timecop.travel(Time.now + 1)
      sample_2 = rule_1.query_cache_key(user)
      assert_equal sample_1, sample_2
    end

    it "avoids collision by seeding with id" do
      sample_1 = rule_1.query_cache_key(user, id: 4321)
      sample_2 = rule_2.query_cache_key(user, id: 1234)
      assert_not_equal sample_1, sample_2
    end
  end

  describe "#cia_changes" do
    let(:rule) { Rule.new }

    it "includes normal changes" do
      rule.title = "xxx"
      assert_equal({"title" => ["", "xxx"]}, rule.cia_changes)
    end

    it "shows creation of an empty definition" do
      rule.definition = Definition.new
      assert_equal({"definition" => [nil, "[[],[],[]]"]}, rule.cia_changes)
    end

    it "records definition changes" do
      params = {
        conditions_all: [
          source: "group_id",
          operator: "is_not",
          value: [1]
        ]
      }
      new_definition = Definition.build(params)

      rule = rules(:view_unassigned_to_group)
      refute_equal new_definition, rule.definition
      rule.definition = new_definition

      expected_changes = {"definition" => ["[[\"group_id is \"],[],[]]",
                                           "[[\"group_id is_not 1\"],[],[]]"]}

      assert_equal expected_changes, rule.cia_changes
    end

    it "does not record definition changes when the definition is unchanged" do
      params = {
        conditions_all: [
          source: "group_id",
          operator: "is",
          value: [""]
        ]
      }
      new_definition = Definition.build(params)

      rule = rules(:view_unassigned_to_group)

      assert_equal new_definition, rule.definition

      rule.is_active = false
      rule.definition = new_definition

      assert_equal({"is_active" => [true, false]}, rule.cia_changes)
    end
  end

  describe "#prepare_for_editing!" do
    it "converts assignee_id .. not changed to ... current_assignee to 'not_changed'" do
      rule = new_rule(DefinitionItem.new("assignee_id#all_agents", "not_value", "assignee_id"), type: 'conditions_all')
      rule.prepare_for_editing!

      assert rule.definition.conditions_all.map { |c| [c.source, c.operator, c.value] }.include?(["assignee_id", "not_changed", []])
    end

    it "eats group_id is/is_not group_id nonsense in the middle of the night" do
      # (like a chupacabra would a goat)

      rule = rules(:trigger_notify_assignee_of_comment_update)
      rule.definition.conditions_all[0] = DefinitionItem.new("group_id", "is", "group_id")
      old_size = rule.definition.conditions_all.size
      rule.prepare_for_editing!
      assert_equal old_size - 1, rule.definition.conditions_all.size
    end

    it "does not crash when given an integer value" do
      rule = rules(:trigger_notify_assignee_of_comment_update)
      rule.definition.conditions_all.first.value = 123123
      rule.prepare_for_editing!
    end

    it "does not crash when given a string value" do
      rule = rules(:trigger_notify_assignee_of_comment_update)
      rule.definition.conditions_all.first.value = "112358"
      rule.prepare_for_editing!
    end

    it "prepare_recipient_emails if source is recipient and email is not complete" do
      rule = rules(:trigger_notify_assignee_of_comment_update)
      rule.definition.conditions_all[0] = DefinitionItem.new("recipient", "is", "testemail")
      rule.prepare_for_editing!
      assert_equal ["testemail@minimum.zendesk-test.com"], rule.definition.conditions_all.first.value
    end

    it "does not prepare_recipient_email if source is recipient and email is complete" do
      rule = rules(:trigger_notify_assignee_of_comment_update)
      rule.definition.conditions_all[0] = DefinitionItem.new("recipient", "is", "testemail@minimum.zendesk-test.com")
      rule.prepare_for_editing!
      assert_equal ["testemail@minimum.zendesk-test.com"], rule.definition.conditions_all.first.value
    end

    it "removes references to deleted TicketFields" do
      rule = rules(:trigger_notify_assignee_of_comment_update)
      TicketField.expects(:exists?).with("12345").returns(false)
      rule.definition.conditions_all[0] = DefinitionItem.new("ticket_fields_12345", "is", "abc")
      old_size = rule.definition.conditions_all.size
      rule.prepare_for_editing!
      assert_equal old_size - 1, rule.definition.conditions_all.size
    end

    it "does not remove references to things that look kind of like TicketFields, but are not" do
      rule = rules(:trigger_notify_assignee_of_comment_update)
      TicketField.expects(:exists?).never
      rule.definition.conditions_all[0] = DefinitionItem.new("ticket_fields_12345x", "is", "abc")
      old_size = rule.definition.conditions_all.size
      rule.prepare_for_editing!
      assert_equal old_size, rule.definition.conditions_all.size
    end
  end

  describe "SUBTYPES" do
    it "includes all targets" do
      files = Dir[Rails.root.join("app/models/rules/*")]
      files.each { |file| require File.expand_path(file, Rails.root) }
      assert_equal Rule.send(:descendants).map(&:name).sort, Rule::SUBTYPES.sort
    end
  end

  describe '#via_reference' do
    let(:rule) { Rule.new }

    it 'returns via reference corresponding to the rule' do
      assert_equal(
        {via_id: ViaType.RULE, via_reference_id: rule.id},
        rule.via_reference
      )
    end
  end

  describe '#watchable?' do
    let(:rule)         { Rule.new }
    let(:executer)     { stub('rule executer') }
    let(:watchability) { {cacheable: true} }

    before do
      Zendesk::Rules::RuleExecuter.
        stubs(:new).
        returns(executer)

      executer.stubs(:watchability).returns(watchability)
    end

    describe 'when view is watchable' do
      it 'returns true' do
        assert rule.watchable?
      end
    end

    describe 'when view is not watchable' do
      let(:watchability) { {cacheable: false, message: 'error'} }

      it 'returns false' do
        refute rule.watchable?
      end
    end
  end
end
