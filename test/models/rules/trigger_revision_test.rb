require_relative '../../support/test_helper'
require_relative '../../support/rule'

SingleCov.covered!

describe TriggerRevision do
  include TestSupport::Rule::Helper

  fixtures :accounts

  let(:account)  { accounts(:minimum) }

  let!(:trigger) { create_trigger(description: 'old') }

  should validate_uniqueness_of(:nice_id).scoped_to(:rule_id)

  describe 'when saving a trigger' do
    before { trigger.update_attribute(:description, 'new') }

    should_change('trigger snapshots', by: 1) do
      TriggerSnapshot.
        where(revision_id: trigger.revisions.map(&:id)).
        count(:all)
    end
  end

  describe 'when destroying a trigger revision' do
    let(:revision) { trigger.revisions.last }
    let(:snapshot) { revision.snapshot }

    before { revision.destroy }

    it 'destroys the associated snapshot' do
      refute TriggerSnapshot.exists?(snapshot.id)
    end
  end
end
