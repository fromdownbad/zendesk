require_relative '../../support/test_helper'
require_relative '../../support/rule'

SingleCov.covered!

describe RuleAttachment do
  include TestSupport::Rule::Helper

  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:macro)   { create_macro }
  let(:user)    { users(:minimum_agent) }

  let(:rule_attachment) do
    RuleAttachment.create! do |ra|
      ra.account_id   = account.id
      ra.user_id      = user.id
      ra.size         = 1
      ra.content_type = 'application/unknown'
      ra.filename     = 'hello_world'
    end
  end

  describe '.limit' do
    it 'returns the per-macro attachment limit' do
      assert_equal 5, RuleAttachment.limit
    end
  end

  describe '#ensure_token' do
    it 'has a token attribute' do
      assert rule_attachment.token
    end
  end

  describe '#set_thumbnail_inherits' do
    let(:filename)      { "#{Rails.root}/test/files/normal_1.jpg" }
    let(:uploaded_file) { Zendesk::Attachments::CgiFileWrapper.new(filename) }

    let(:rule_attachment) do
      RuleAttachment.create! do |ra|
        ra.account_id    = account.id
        ra.user_id       = user.id
        ra.uploaded_data = uploaded_file
      end
    end

    it 'creates thumbnails for image uploads' do
      assert_equal 1, rule_attachment.thumbnails.size
    end
  end

  describe '#associate' do
    before { rule_attachment.associate(macro) }

    it 'associates the attachment with the macro' do
      assert_includes rule_attachment.rules, macro
    end
  end
end
