require_relative '../../support/test_helper'
require_relative '../../support/rule'

SingleCov.covered!

describe RuleCategory do
  should_validate_presence_of :account, :name, :rule_type, :position
  should validate_inclusion_of(:rule_type).in_array(%w[Trigger])

  let(:account) { accounts(:minimum) }
  let(:category) { RuleCategory.new(rule_type: 'Trigger', name: 'Foo', account: account) }

  include TestSupport::Rule::Helper

  describe 'hooks' do
    before(:each) { RuleCategory.destroy_all }

    describe 'before_validation' do
      it 'should assign 1 if there are no categories' do
        category.save!

        assert_equal 1, category.position
      end

      it 'should assign max + 1 position if none is given' do
        3.times { |t| create_category(position: t + 1) }
        category.save!

        assert_equal 4, category.position
      end
    end

    describe 'before_save' do
      describe '#verify_table_limit' do
        describe 'when the limit is not exceeded' do
          it 'persists the trigger category successfully' do
            assert category.save
          end
        end

        describe 'when the limit is exceeded' do
          it 'fails with a Zendesk::ProductLimit::TableLimitExceededError' do
            create_category

            RuleCategory.any_instance.stubs(:usage_limit).returns(1)

            log_message = 'RuleCategory table limit exceeded: '\
              "account_id: #{account.id} rule_type: Trigger "\
              'current limit: 1'
            Rails.logger.expects(:info).with(log_message).once

            assert_raises Zendesk::ProductLimit::TableLimitExceededError do
              category.save
            end
          end
        end
      end
    end

    describe '#update_position' do
      let(:ordered_categories) do
        RuleCategory.where(account_id: account.id, rule_type: 'Trigger').order(:position)
      end

      before do
        @category_1 = create_category(position: 1)
        @category_2 = create_category(position: 2)
        @category_3 = create_category(position: 3)
      end

      it 'reorders provided rule categories correctly' do
        new_categories_order = [@category_2, @category_3, @category_1]
        RuleCategory.update_position(account, new_categories_order.map(&:id))

        assert_equal new_categories_order, ordered_categories
      end
    end
  end
end
