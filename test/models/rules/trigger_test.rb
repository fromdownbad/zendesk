require_relative '../../support/test_helper'
require_relative '../../support/cms_references_helper'
require_relative '../../support/rule'
require_relative '../../support/rules_test_helper'
require_relative '../../support/sharing_test_helper'
require_relative "../../support/collaboration_settings_test_helper"

SingleCov.covered!

describe Trigger do
  include TestSupport::Rule::Helper
  include RulesTestHelper
  include CustomFieldsTestHelper
  include SharingTestHelper

  extend CmsReferencesHelper

  fixtures :all

  def self.should_not_match(*args)
    it "does not match #{args.inspect}" do
      refute rule_matches(*args)
    end
  end

  def self.should_match(*args)
    it "matches #{args.inspect}" do
      assert rule_matches(*args)
    end
  end

  def rule_matches(*args)
    item = DefinitionItem.new(*args)
    Rule.match_condition?(@ticket, item, nil, @ticket.audit.try(:events))
  end

  let(:account) { accounts(:minimum) }
  let(:user)    { users(:minimum_admin) }

  creates_cms_references(
    type: :trigger,
    actions: %w[notification_user notification_group notification_target]
  )

  describe 'hooks' do
    describe_with_arturo_setting_enabled :trigger_categories_api do
      before(:each) { RuleCategory.trigger_categories(account).destroy_all }

      describe '#ensure_valid_rules_category_id' do
        it 'adds error if category id is not valid' do
          create_category

          trigger = new_trigger
          trigger.rules_category_id = '12345'

          assert_predicate trigger, :invalid?
          assert_equal ['category_id does not exist'], trigger.errors.messages[:category_id]
        end
      end

      describe '#ensure_valid_category' do
        it 'adds error if category is not valid' do
          trigger = new_trigger
          trigger.category = {}

          assert_predicate trigger, :invalid?
          assert_equal ['cannot be blank'], trigger.errors.messages[:category_name]
        end
      end

      describe '#ensure_categories_exist' do
        it 'adds error if there are no categories in the account' do
          trigger = new_trigger

          assert_predicate trigger, :invalid?
          assert_equal ['A category must exist before creating a trigger'], trigger.errors.messages[:category]
        end
      end

      describe '#set_category_from_position' do
        before do
          @category_1 = create_category(position: 1)
          @category_2 = create_category(position: 2)
          @category_3 = create_category(position: 3)

          create_trigger(rules_category_id: @category_1.id, position: 1)
          create_trigger(rules_category_id: @category_1.id, position: 2)

          create_trigger(rules_category_id: @category_2.id, position: 1)
          create_trigger(rules_category_id: @category_2.id, position: 2)
          create_trigger(rules_category_id: @category_2.id, position: 3)
          create_trigger(rules_category_id: @category_2.id, position: 4)

          create_trigger(rules_category_id: @category_3.id, position: 1)
          create_trigger(rules_category_id: @category_3.id, position: 2)
          create_trigger(rules_category_id: @category_3.id, position: 3)
          create_trigger(rules_category_id: @category_3.id, position: 4)
          create_trigger(rules_category_id: @category_3.id, position: 5)
        end

        let(:last_category_id) { RuleCategory.trigger_categories(account).order(position: :desc).first.id }

        describe 'when a position is provided' do
          describe 'and the position is within bounds' do
            describe 'and there are no active triggers to compare to' do
              before { Trigger.destroy_all }

              it 'sets the trigger to the end of the last category' do
                trigger = new_trigger(position: 1)
                trigger.save!

                assert_equal last_category_id, trigger.rules_category_id
                assert_equal 1, trigger.position
              end
            end

            it 'sets the trigger to the correct category and position' do
              trigger = new_trigger(position: 4)
              trigger.save!

              assert_equal @category_2.id, trigger.rules_category_id
              assert_equal 2, trigger.position
            end
          end

          describe 'and the position is not within bounds' do
            it 'sets the trigger to the end of the last category' do
              trigger = new_trigger(position: 0)
              trigger.save!

              assert_equal last_category_id, trigger.rules_category_id
              assert_equal 6, trigger.position
            end
          end
        end

        describe 'when a position is not provided' do
          it 'sets the trigger to the end of the last category' do
            trigger = new_trigger
            trigger.save!

            assert_equal last_category_id, trigger.rules_category_id
            assert_equal 6, trigger.position
          end
        end
      end

      describe '#create_category_and_set_rules_category_id' do
        before { RuleCategory.destroy_all }

        it 'saves the category and sets the rules_category_id' do
          trigger = new_trigger
          trigger.category = { name: 'asdf' }
          trigger.save!

          assert_equal RuleCategory.all.first.id, trigger.rules_category_id
        end
      end

      describe '#set_default_category_position' do
        before do
          RuleCategory.destroy_all
          Trigger.destroy_all
        end

        it 'does not set position value to the end of the category if position is specified' do
          category_1 = create_category(position: 1)
          category_2 = create_category(position: 2)

          create_trigger(rules_category_id: category_1.id, position: 1)
          create_trigger(rules_category_id: category_1.id, position: 2)

          trigger = create_trigger(rules_category_id: category_2.id, position: 1)

          trigger.rules_category_id = category_1.id
          trigger.position = 2
          trigger.save!

          assert_equal category_1.id, trigger.rules_category_id
          assert_equal 2, trigger.position
        end

        it 'sets position value to the end of the category if position is not specified' do
          category_1 = create_category(position: 1)
          category_2 = create_category(position: 2)

          create_trigger(rules_category_id: category_1.id, position: 1)
          create_trigger(rules_category_id: category_1.id, position: 2)

          trigger = create_trigger(rules_category_id: category_2.id, position: 1)

          trigger.rules_category_id = category_1.id
          trigger.save!

          assert_equal category_1.id, trigger.rules_category_id
          assert_equal 3, trigger.position
        end
      end

      describe '#ensure_category_params_exist' do
        it 'saves the trigger with a rules_category_id' do
          category_1 = create_category
          trigger = new_trigger(rules_category_id: category_1.id)
          trigger.save!

          assert_equal category_1.id, trigger.rules_category_id
        end

        it 'saves the trigger with a category' do
          trigger = new_trigger(category: { name: 'New category' })
          trigger.save!

          assert_equal RuleCategory.trigger_categories(account).first.id, trigger.rules_category_id
        end

        it 'errors without a rules_category_id or category param' do
          category_1 = create_category
          trigger = create_trigger(rules_category_id: category_1.id)

          trigger.rules_category_id = nil

          assert_predicate trigger, :invalid?
          assert_equal ['Add a category_id or category parameter'], trigger.errors.messages[:category]
        end
      end
    end
  end

  describe_with_arturo_enabled :tweeet_requester_action do
    before { account.stubs(created_at: Time.new(2016).utc) }

    creates_cms_references(type: :trigger, actions: %w[tweet_requester])
  end

  it 'prevents collection modification' do
    assert_includes(
      Trigger.
        included_modules, Zendesk::PreventCollectionModificationMixin
    )
  end

  it 'defines the `APM_SERVICE_NAME` constant' do
    assert_equal 'classic-trigger-execution', Trigger::APM_SERVICE_NAME
  end

  #######################################################
  # Test conditions
  #######################################################

  describe "#fire" do
    let(:trigger) { create_trigger }

    describe "when there's a match" do
      let(:ticket) { tickets(:minimum_1) }

      before do
        ticket.ticket_form_id = 4
        ticket.will_be_saved_by(user)

        trigger.stubs(condition_match?: :match)
      end

      it "collects and returns changes when there's a condition match" do
        trigger.expects(:apply_and_collect_changes).returns(:changes)
        assert_equal [:match, :changes], trigger.fire(nil, nil, nil)
      end

      it "maps a changes value of false to status :aborted" do
        trigger.expects(:apply_and_collect_changes).returns(false)
        assert_equal [:aborted, nil], trigger.fire(nil, nil, nil)
      end

      it 'records an event with `RULE_REVISION` via type' do
        trigger.fire(ticket, ticket.audits.first.events, nil)

        assert_equal ViaType.RULE_REVISION, ticket.audit.events.first.via_id
      end

      describe 'when a trigger adds a follower' do
        let(:minimum_agent) { users(:minimum_agent) }
        let(:actions) { [DefinitionItem.new('cc', nil, [user.id])] }
        let(:conditions_all) { [DefinitionItem.new('update_type', 'is', 'Create')] }

        before do
          CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
          trigger.definition = Definition.new(conditions_all, [], actions)
        end

        describe 'and the follower is not an email CC' do
          it 'records a follower change event' do
            trigger.fire(ticket, ticket.audits.first.events, nil)

            assert_equal FollowerChange.name, ticket.audit.events.last.type
          end
        end

        describe_with_arturo_setting_disabled :ticket_followers_allowed do
          it 'does not record a follower change event' do
            trigger.fire(ticket, ticket.audits.first.events, nil)

            assert_empty ticket.audit.events
          end
        end

        describe 'and the follower is an email CC' do
          def assert_email_cc_audit_events
            email_cc_audit_events = ticket.audits.last.events.select { |e| e.type == EmailCcChange.name }

            assert_equal 1, email_cc_audit_events.length
          end

          before do
            ticket.set_email_ccs = [{ email: user.email, name: user.name, action: :put }]
            ticket.save!
            assert_email_cc_audit_events
          end

          it 'records a follower change event' do
            trigger.fire(ticket, ticket.audits.first.events, nil)

            assert_equal FollowerChange.name, ticket.audit.events.last.type
          end

          describe 'and the follower is deleted' do
            let(:zendesk_agent) do
              FactoryBot.create(:agent, account: account, name: 'Double Agent')
            end

            before do
              user.current_user = zendesk_agent
              user.delete!
            end

            it 'does not record a follower change event' do
              trigger.fire(ticket, ticket.audits.first.events, nil)

              assert_empty ticket.audit.events
            end
          end

          describe 'and the follower is suspended' do
            # Explicitly set user because owners cannot be suspended
            let(:user) { users(:minimum_admin_not_owner) }

            before { user.suspend! }

            it 'records a follower change event' do
              trigger.fire(ticket, ticket.audits.first.events, nil)

              assert_equal FollowerChange.name, ticket.audit.events.last.type
            end
          end

          describe 'when there is a pending followers update' do
            let(:user_2) { users(:minimum_admin_not_owner) }

            it 'records a single follower change event' do
              ticket.set_followers = [{ email: user_2.email, name: user_2.name, action: :put }]
              trigger.fire(ticket, ticket.audits.first.events, nil)
              follower_audit_events = ticket.audit.events.select { |e| e.type == FollowerChange.name }

              assert_equal 1, follower_audit_events.length
              assert_equal [user.email, user_2.email], follower_audit_events.first.value
              assert_equal [user_2.email], follower_audit_events.first.value_previous
            end
          end

          describe 'and the trigger adds multiple followers' do
            let(:minimum_agent) { users(:minimum_agent) }
            let(:actions) do
              [
                DefinitionItem.new('cc', nil, [user.id]),
                DefinitionItem.new('cc', nil, [minimum_agent.id])
              ]
            end

            it 'records a single follower change event' do
              trigger.fire(ticket, ticket.audits.first.events, nil)
              follower_audit_events = ticket.audit.events.select { |e| e.type == FollowerChange.name }

              assert_equal 1, follower_audit_events.length
              assert_equal [user.email, minimum_agent.email], follower_audit_events.first.value
              assert_equal [], follower_audit_events.first.value_previous
            end
          end
        end
      end
    end

    describe "when there's not a match" do
      before { trigger.stubs(:condition_match?).returns(:hello) }

      it "relays the status from the condition_match? method" do
        assert_equal [:hello, nil], trigger.fire(nil, nil, nil)
      end
    end
  end

  describe '#notification_change?' do
    let(:trigger) do
      create_trigger(
        definition: Definition.new(
          [DefinitionItem.new('update_type', 'is', 'Create')],
          [],
          [
            DefinitionItem.new(
              'notification_user',
              'is',
              %w[requester_id body subject]
            )
          ]
        )
      ).tap(&:save!)
    end

    let(:actions) do
      [
        DefinitionItem.new(
          'notification_user',
          'is',
          %w[requester_id body subject]
        )
      ]
    end

    let(:conditions_all) { [DefinitionItem.new('update_type', 'is', 'Create')] }

    before { trigger.definition = Definition.new(conditions_all, [], actions) }

    describe 'when conditions are changed' do
      let(:conditions_all) do
        [
          DefinitionItem.new('update_type', 'is', 'Create'),
          DefinitionItem.new('current_tags', 'is', 'fidelio')
        ]
      end

      it 'returns false' do
        refute trigger.notification_change?
      end
    end

    describe 'when a new action is added' do
      let(:actions) do
        [
          DefinitionItem.new(
            'notification_user',
            'is',
            %w[requester_id body subject]
          ),
          DefinitionItem.new('current_tags', 'is', 'rainbow')
        ]
      end

      it 'returns false' do
        refute trigger.notification_change?
      end
    end

    describe 'when an existing action is replaced' do
      let(:actions) { [DefinitionItem.new('current_tags', 'is', 'silencio')] }

      it 'returns false' do
        refute trigger.notification_change?
      end
    end

    describe 'when the notification recipient is changed' do
      let(:actions) do
        [
          DefinitionItem.new('notification_user', 'is', %w[bill body subject])
        ]
      end

      it 'returns false' do
        refute trigger.notification_change?
      end
    end

    describe 'when the notification content is changed' do
      let(:actions) do
        [
          DefinitionItem.new(
            'notification_user',
            'is',
            [
              'requester_id',
              'What is the second password?',
              '#didntshowupinalimo'
            ]
          )
        ]
      end

      it 'returns true' do
        assert trigger.notification_change?
      end
    end

    describe 'when another field is changed' do
      before { trigger.title = 'Eyes Wide Shut' }

      it 'returns false' do
        refute trigger.notification_change?
      end
    end
  end

  it "tests present" do
    assert_equal 14, account.triggers.count(:all)
  end

  describe '#match?' do
    let(:actions) do
      [DefinitionItem.new('ticket_type_id', 'is', TicketType.QUESTION)]
    end

    let(:ticket) do
      tickets(:minimum_1).tap do |ticket|
        ticket.will_be_saved_by(ticket.requester)
        ticket.priority_id = PriorityType.HIGH
        ticket.status_id = StatusType.PENDING
        ticket.audit.grow
      end
    end

    describe 'and there are only ANY or ALL conditions' do
      let(:conditions) do
        [
          DefinitionItem.new('priority_id', 'is', PriorityType.HIGH),
          DefinitionItem.new('status_id', 'is', StatusType.OPEN)
        ]
      end

      it 'does not match the `ALL` conditions' do
        refute(
          create_trigger(
            definition: Definition.new(conditions, [], actions)
          ).match?(ticket)
        )
      end

      it 'matches the `ANY` conditions' do
        assert(
          create_trigger(
            definition: Definition.new([], conditions, actions)
          ).match?(ticket)
        )
      end
    end

    describe 'and there are combined ALL and ANY conditions' do
      let(:conditions_1) do
        [DefinitionItem.new('ticket_type_id', 'is', TicketType.INCIDENT)]
      end

      let(:conditions_2) do
        [
          DefinitionItem.new('priority_id', 'is', PriorityType.HIGH),
          DefinitionItem.new('status_id', 'is', StatusType.OPEN)
        ]
      end

      it 'matches the correct ALL and ANY conditions' do
        assert(
          create_trigger(
            definition: Definition.new(conditions_1, conditions_2, actions)
          ).match?(ticket)
        )
      end

      it 'does not match the invalid ALL and ANY conditions' do
        refute(
          create_trigger(
            definition: Definition.new(conditions_2, conditions_1, actions)
          ).match?(ticket)
        )
      end
    end
  end

  describe "attribute operator tests" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.priority_id = PriorityType.HIGH
      @ticket.audit.grow
    end

    should_match     'priority_id', 'is', PriorityType.HIGH
    should_not_match 'priority_id', 'is_not', PriorityType.HIGH
    should_not_match 'priority_id', 'greater_than', PriorityType.HIGH
    should_match     'priority_id', 'less_than', PriorityType.URGENT

    should_match     'priority_id', 'changed'
    should_not_match 'priority_id', 'not_changed'
    should_match     'status_id',   'not_changed'

    # Changed to
    should_match     'priority_id', 'value', PriorityType.HIGH
    should_not_match 'priority_id', 'value', PriorityType.URGENT

    # Changed from
    should_match     'priority_id', 'value_previous', PriorityType.NORMAL
    should_not_match 'priority_id', 'value_previous', PriorityType.URGENT

    # Not changed to
    should_not_match 'priority_id', 'not_value', PriorityType.HIGH
    should_match     'priority_id', 'not_value', PriorityType.URGENT
    should_match     'status_id', 'not_value', StatusType.NEW

    # Not changed from
    should_not_match 'priority_id', 'not_value_previous', PriorityType.NORMAL
    should_match     'priority_id', 'not_value_previous', PriorityType.URGENT
  end

  describe "status ordering" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.audit.grow
    end

    describe "new" do
      before { @ticket.status_id = StatusType.NEW }
      should_match     'status_id', 'less_than', StatusType.CLOSED
      should_match     'status_id', 'less_than', StatusType.SOLVED
      should_match     'status_id', 'less_than', StatusType.HOLD
      should_match     'status_id', 'less_than', StatusType.PENDING
      should_match     'status_id', 'less_than', StatusType.OPEN
      should_not_match 'status_id', 'less_than', StatusType.NEW
    end

    describe "open" do
      before { @ticket.status_id = StatusType.OPEN }
      should_match     'status_id', 'less_than', StatusType.CLOSED
      should_match     'status_id', 'less_than', StatusType.SOLVED
      should_match     'status_id', 'less_than', StatusType.HOLD
      should_match     'status_id', 'less_than', StatusType.PENDING
      should_not_match 'status_id', 'less_than', StatusType.OPEN
      should_not_match 'status_id', 'less_than', StatusType.NEW
    end

    describe "pending" do
      before { @ticket.status_id = StatusType.PENDING }
      should_match     'status_id', 'less_than', StatusType.CLOSED
      should_match     'status_id', 'less_than', StatusType.SOLVED
      should_match     'status_id', 'less_than', StatusType.HOLD
      should_not_match 'status_id', 'less_than', StatusType.PENDING
      should_not_match 'status_id', 'less_than', StatusType.OPEN
      should_not_match 'status_id', 'less_than', StatusType.NEW
    end

    describe "on hold" do
      before { @ticket.status_id = StatusType.HOLD }
      should_match     'status_id', 'less_than', StatusType.CLOSED
      should_match     'status_id', 'less_than', StatusType.SOLVED
      should_not_match 'status_id', 'less_than', StatusType.HOLD
      should_not_match 'status_id', 'less_than', StatusType.PENDING
      should_not_match 'status_id', 'less_than', StatusType.OPEN
      should_not_match 'status_id', 'less_than', StatusType.NEW
    end

    describe "solved" do
      before { @ticket.status_id = StatusType.SOLVED }
      should_match     'status_id', 'less_than', StatusType.CLOSED
      should_not_match 'status_id', 'less_than', StatusType.SOLVED
      should_not_match 'status_id', 'less_than', StatusType.HOLD
      should_not_match 'status_id', 'less_than', StatusType.PENDING
      should_not_match 'status_id', 'less_than', StatusType.OPEN
      should_not_match 'status_id', 'less_than', StatusType.NEW
    end

    describe "closed" do
      before { @ticket.status_id = StatusType.CLOSED }
      should_not_match 'status_id', 'less_than', StatusType.CLOSED
      should_not_match 'status_id', 'less_than', StatusType.SOLVED
      should_not_match 'status_id', 'less_than', StatusType.HOLD
      should_not_match 'status_id', 'less_than', StatusType.PENDING
      should_not_match 'status_id', 'less_than', StatusType.OPEN
      should_not_match 'status_id', 'less_than', StatusType.NEW
    end
  end

  describe "current tags operator" do
    before do
      @ticket = tickets(:minimum_2)
    end

    should_match     'current_tags', 'includes', 'hello'
    should_not_match 'current_tags', 'includes', 'tivoli'
    should_match     'current_tags', 'not_includes', 'tivoli'
    should_not_match 'current_tags', 'not_includes', 'hello'
  end

  describe "ticket form id" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.account.stubs(:has_ticket_forms?).returns(true)
    end

    describe "is set" do
      before do
        @ticket.ticket_form_id = 4
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.audit.grow
      end

      should_match     'ticket_form_id', 'is', '4'
      should_not_match 'ticket_form_id', 'is_not', '4'

      should_match     'ticket_form_id', 'changed'
      should_not_match 'ticket_form_id', 'not_changed'
    end

    describe "is nil" do
      before do
        @default_form = Zendesk::TicketForms::Initializer.new(@ticket.account, ticket_form: {name: "Wombat default", display_name: "default", default: true, position: 2}).ticket_form
        @default_form.save!
        @ticket.ticket_form_id = nil
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.audit.grow
      end

      it "matchs to the default form condition" do
        @condition = DefinitionItem.new('ticket_form_id', 'is', @default_form.id.to_s)
        assert Rule.match_condition?(@ticket, @condition, @ticket.audits.first.events)
      end
    end
  end

  describe "multi brand id" do
    before do
      @ticket = tickets(:minimum_1)
      Account.any_instance.stubs(:has_multibrand?).returns(true)
    end

    describe "is set" do
      before do
        @ticket.brand_id = 4
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.audit.grow
      end

      should_match     'brand_id', 'is', '4'
      should_not_match 'brand_id', 'is_not', '4'

      should_match     'brand_id', 'changed'
      should_not_match 'brand_id', 'not_changed'
    end
  end

  describe "recipient target" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.recipient = "support@minimum.zendesk-test.com"
      @ticket.original_recipient_address = "hello@example.com"
    end

    should_match 'recipient', nil, 'support'
    should_match 'recipient', nil, 'hello@example.com'
    should_match 'recipient', nil, 'support@minimum.zendesk-test.com'
    should_not_match 'recipient', nil, 'hello'
    should_not_match 'current_tags', nil, 'tivoli'
  end

  describe "collaboration_thread" do
    describe "when a collaboration_thread is created" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.audit.events << CollabThreadCreated.new
      end

      should_match "collaboration_thread", "is", "created"
    end

    describe "when a thread is closed" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.audit.events << CollabThreadClosed.new
      end

      should_match "collaboration_thread", "is", "closed"
    end

    describe "when a thread is reopened" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.audit.events << CollabThreadReopened.new
      end

      should_match "collaboration_thread", "is", "reopened"
    end

    describe "when a thread gets a reply" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.audit.events << CollabThreadReply.new
      end

      should_match "collaboration_thread", "is", "reply"
    end
  end

  describe "subject_includes_word" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.subject = "Jeg har slået en @prut."
      @ticket.will_be_saved_by(@ticket.requester)
    end

    describe "with no subject" do
      before do
        @ticket.subject = nil
      end

      should_not_match "subject_includes_word", "includes",     "hej @prut"
      should_not_match "subject_includes_word", "is",           "hej"
      should_match     "subject_includes_word", "is_not",       "hej"
      should_match     "subject_includes_word", "not_includes", "hej"
    end

    describe "with subject" do
      should_match     "subject_includes_word", "includes", "har øv"
      should_match     "subject_includes_word", "includes", "a     har    b"
      should_match     "subject_includes_word", "includes", "jeg øv"
      should_match     "subject_includes_word", "includes", "prut"
      should_match     "subject_includes_word", "includes", "@prut"
      should_not_match "subject_includes_word", "includes", "slå"
      should_not_match "subject_includes_word", "includes", "[foo"

      should_match     "subject_includes_word", "not_includes", "hest"
      should_not_match "subject_includes_word", "not_includes", "har"

      should_match     "subject_includes_word", "is", "Jeg har slået en @prut."
      should_not_match "subject_includes_word", "is", "Jeg har slået en fis."
    end
  end

  describe "ticket_is_public" do
    before do
      @ticket = tickets(:minimum_1)
    end

    describe "matches trigger with 0/1 for false/true values" do
      before do
        @ticket.is_public = false
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.audit.grow
      end

      # Note: ticket_is_public only supports the 'is' operator
      should_match 'ticket_is_public', 'is', 'private'
    end
  end

  describe "comment_includes_word" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
    end

    describe "with no comment" do
      should_not_match "comment_includes_word", "includes",     "hej @prut"
      should_not_match "comment_includes_word", "is",           "hej"
      should_match     "comment_includes_word", "is_not",       "hej"
      should_match     "comment_includes_word", "not_includes", "hej"
    end

    describe "with a comment" do
      before do
        @ticket.add_comment(body: "Jeg har slået en @prut.")
        @ticket.set_comment
      end

      should_match     "comment_includes_word", "includes", "har øv"
      should_match     "comment_includes_word", "includes", "a     har    b"
      should_match     "comment_includes_word", "includes", "jeg øv"
      should_match     "comment_includes_word", "includes", "prut"
      should_match     "comment_includes_word", "includes", "@prut"
      should_not_match "comment_includes_word", "includes", "slå"
      should_not_match "comment_includes_word", "includes", "[foo"

      should_match     "comment_includes_word", "not_includes", "hest"
      should_not_match "comment_includes_word", "not_includes", "har"

      should_match     "comment_includes_word", "is", "Jeg har slået en @prut."
      should_not_match "comment_includes_word", "is", "Jeg har slået en fis."
    end
  end

  describe "comment_is_public" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
    end

    describe "with no comment" do
      should_not_match "comment_is_public", nil, "not_relevant"
      should_not_match "comment_is_public", nil, "true"
      should_not_match "comment_is_public", nil, "false"
      should_not_match "comment_is_public", nil, "requester_can_see_comment"
    end

    describe "with an empty comment" do
      before do
        @ticket.add_comment(body: "")
        @ticket.set_comment
      end
      should_not_match "comment_is_public", nil, "not_relevant"
      should_not_match "comment_is_public", nil, "true"
      should_not_match "comment_is_public", nil, "false"
      should_not_match "comment_is_public", nil, "requester_can_see_comment"
    end

    describe "with a public comment" do
      before do
        @ticket.add_comment(body: "foo", is_public: true)
        @ticket.set_comment
      end
      should_match     "comment_is_public", nil, "not_relevant"
      should_match     "comment_is_public", nil, "true"
      should_not_match "comment_is_public", nil, "false"

      describe "as an agent" do
        before { @ticket.requester = users(:minimum_agent) }
        should_match "comment_is_public", nil, "requester_can_see_comment"
      end

      describe "as an end-user" do
        before { refute @ticket.requester.is_agent? }
        should_match "comment_is_public", nil, "requester_can_see_comment"
      end
    end

    describe "with a private comment" do
      before do
        @ticket.add_comment(body: "foo", is_public: false)
        @ticket.set_comment
      end
      should_match     "comment_is_public", nil, "not_relevant"
      should_not_match "comment_is_public", nil, "true"
      should_match     "comment_is_public", nil, "false"

      describe "as an agent" do
        before { @ticket.requester = users(:minimum_agent) }
        should_match "comment_is_public", nil, "requester_can_see_comment"
      end

      describe "as an end-user" do
        before { refute @ticket.requester.is_agent? }
        should_not_match "comment_is_public", nil, "requester_can_see_comment"
      end
    end
  end

  describe 'requester twitter attributes' do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.requester = users(:minimum_end_user)
      @profile = channels_user_profiles(:minimum_twitter_user_profile_1)
      @ticket.requester.identities.twitter.first.update_column(:value, @profile.external_id)
      @ticket.will_be_saved_by(@ticket.requester)
    end

    describe "requester_twitter_followers_count" do
      should_not_match 'requester_twitter_followers_count', 'greater_than', '100'

      describe "with more followers" do
        before { @ticket.stubs(:requester_twitter_followers_count).returns(10) }
        should_not_match 'requester_twitter_followers_count', 'greater_than', '100'
      end

      describe "with even more followers" do
        before { @ticket.stubs(:requester_twitter_followers_count).returns(200) }
        should_match 'requester_twitter_followers_count', 'greater_than', '100'
      end
    end

    describe "requester_twitter_statuses_count" do
      should_not_match 'requester_twitter_statuses_count', 'greater_than', '500'

      describe "with more statuses" do
        before { @ticket.stubs(:requester_twitter_statuses_count).returns(10) }

        should_not_match 'requester_twitter_statuses_count', 'greater_than', '100'
      end

      describe "with even more statuses" do
        before { @ticket.stubs(:requester_twitter_statuses_count).returns(200) }
        should_match 'requester_twitter_statuses_count', 'greater_than', '100'
      end
    end

    describe "requester_twitter_verified" do
      should_not_match 'requester_twitter_verified', 'is', 'true'

      describe "when not verified" do
        before do
          @ticket.requester.stubs(:twitter_profile).returns(Channels::TwitterUserProfile.new(metadata: {verified: false }))
        end

        should_not_match 'requester_twitter_verified', 'is', 'true'
      end

      describe "when verified" do
        before do
          @ticket.requester.stubs(:twitter_profile).returns(Channels::TwitterUserProfile.new(metadata: {verified: true }))
        end

        should_match 'requester_twitter_verified', 'is', 'true'
      end
    end
  end

  describe "status changed from new" do
    before do
      @ticket = Ticket.new(account: account, subject: "New ticket subject", description: "New ticket description")
      @ticket.will_be_saved_by(account.owner)
      @ticket.save!
    end
    it "does not match on new tickets" do
      @condition = DefinitionItem.new('status_id', 'value_previous', '0')
      refute Rule.match_condition?(@ticket, @condition, @ticket.audits.first.events)
    end
  end

  describe "update_type target" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.priority_id = PriorityType.HIGH
    end

    should_not_match 'update_type', nil, 'create'
    should_not_match 'update_type', nil, 'change'
  end

  describe "role target" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.priority_id = PriorityType.HIGH
      @ticket.audit.grow
    end

    it "operates in this particular way" do
      condition = ['role', nil, 'agent']

      @ticket.current_user = users(:minimum_agent)
      assert rule_matches(*condition)

      @ticket.current_user = users(:minimum_end_user)
      refute rule_matches(*condition)

      condition = ['role', nil, users(:minimum_agent).id]
      @ticket.current_user = users(:minimum_end_user)

      refute rule_matches(*condition)

      @ticket.current_user = users(:minimum_agent)
      assert rule_matches(*condition)
    end
  end

  describe "number_of_incidents target" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.audit.grow
    end

    # TODO: I'm pretty sure this test is invalid because its changing the in-memory copy of @ticket
    # but we use an archive support method to do counts which goes to the database. This only works
    # because there were never an incidents for this ticket persisted to the database.
    describe "with a problem ticket having no incident" do
      before do
        @ticket.ticket_type_id = TicketType.PROBLEM
        @ticket.incidents = []
      end

      should_not_match "number_of_incidents", "is", "3"
      should_not_match "number_of_incidents", "greater_than", "0"
      should_not_match "number_of_incidents", "greater_than_equal", "1"
      should_match "number_of_incidents", "less_than", "3"
      should_match "number_of_incidents", "less_than_equal", "5"
    end

    describe "with a problem ticket having five incidents" do
      # add some incidents to the ticket"
      before do
        Account.any_instance.stubs(has_extended_ticket_types?: true)
        @ticket.ticket_type_id = TicketType.PROBLEM
        @ticket.save!

        5.times do
          Ticket.new(
            requester: users(:minimum_end_user),
            description: "Incident 1",
            linked_id: @ticket.id,
            via_id: ViaType.WEB_FORM,
            ticket_type_id: TicketType.INCIDENT,
            account: accounts(:minimum)
          ).tap do |incident|
            incident.will_be_saved_by(users(:minimum_agent))
            incident.save!
          end
        end
      end

      should_match "number_of_incidents", "is", "5"
      should_match "number_of_incidents", "greater_than", "4"
      should_match "number_of_incidents", "greater_than_equal", "5"
      should_match "number_of_incidents", "less_than", "6"
      should_match "number_of_incidents", "less_than_equal", "5"

      should_not_match "number_of_incidents", "is", "6"
      should_not_match "number_of_incidents", "greater_than", "5"
      should_not_match "number_of_incidents", "greater_than_equal", "6"
      should_not_match "number_of_incidents", "less_than", "5"
      should_not_match "number_of_incidents", "less_than_equal", "4"

      it "only matches problem tickets" do
        ticket_types = TicketType.list([TicketType.PROBLEM])
        item = DefinitionItem.new("number_of_incidents", "is", "5")
        ticket_types.each do |t|
          @ticket.ticket_type_id = t
          refute Rule.match_condition?(@ticket, item, @ticket.audit.try(:events))
        end
      end
    end
  end

  describe 'sharing-based condition' do
    let(:definition) do
      Definition.new.tap do |definition|
        definition.conditions_all << DefinitionItem.new('received_from', 'is', @agreement_1.id)
        definition.actions << DefinitionItem.new('current_tags', 'is', 'no_more_bad_jokes')
      end
    end

    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.audit.grow

      @agreement_1 = create_valid_agreement(status: :accepted, direction: :in)
      @agreement_2 = create_valid_agreement(status: :accepted, direction: :out)

      @shared_ticket_1 = SharedTicket.new(account: @ticket.account, ticket: @ticket, agreement: @agreement_1)
      @shared_ticket_2 = SharedTicket.new(account: @ticket.account, ticket: @ticket, agreement: @agreement_2)
    end

    it "ticket is not shared" do
      refute rule_matches("received_from", "is", @shared_ticket_1.agreement.id)
      refute rule_matches("sent_to", "is", @shared_ticket_1.agreement.id)
    end

    it "received_from target" do
      @ticket.shared_tickets << @shared_ticket_1
      assert rule_matches("received_from", "is", @shared_ticket_1.agreement.id)
      refute rule_matches("received_from", "is", @shared_ticket_2.agreement.id)
    end

    it "sent_to target" do
      sharing_request = stub_request(:post, %r{http://example.com/sharing/tickets/[0-9a-f]+})
      @ticket.shared_tickets << @shared_ticket_2
      assert rule_matches("sent_to", "is", @shared_ticket_2.agreement.id)
      refute rule_matches("sent_to", "is", @shared_ticket_1.agreement.id)
      assert_requested(sharing_request)
    end

    it "should match if a new ticket is created with an agreement" do
      create_trigger(definition: definition)

      ts = new_ticket_sharing_ticket
      shared_ticket = @agreement_1.shared_tickets.build
      shared_ticket.from_partner(ts)
      new_ticket = shared_ticket.ticket.reload

      assert_equal new_ticket.current_tags, 'no_more_bad_jokes'
    end

    it "should skip shared tickets which agreements were removed" do
      stub_request(:put, %r{/sharing/tickets/[0-9a-f]{40}})

      # Problem: shared ticket is using an agreement(id) which has been removed.
      agreement = create_valid_agreement(status: :accepted, direction: :in)
      shared_ticket_with_agreement = SharedTicket.new(account: @ticket.account, ticket: @ticket, agreement: agreement)
      @ticket.shared_tickets << shared_ticket_with_agreement
      @ticket.save!

      shared_ticket_with_agreement.agreement = nil
      @ticket.shared_tickets << shared_ticket_with_agreement
      @ticket.save!

      @ticket.stubs(:audit).returns(Audit.new.tap { |audit| audit.events = [TicketSharingEvent.new] })
      refute rule_matches("received_from", "is", agreement.id) # as long as it does not throw an error
    end
  end

  describe "via_id target" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.MAIL)
      @ticket.audit.grow
    end

    should_match 'via_id', 'is', ViaType.WEB_FORM
    should_not_match 'via_id', 'is', ViaType.MAIL
  end

  describe "current_via_id target" do
    before do
      @ticket = tickets(:minimum_1)
    end

    describe "via mail" do
      before { @ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.MAIL) }

      should_not_match 'current_via_id', 'is', ViaType.WEB_FORM
      should_match 'current_via_id', 'is', ViaType.MAIL
    end

    describe "via a rule" do
      before do
        @ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.RULE)
      end

      should_not_match 'current_via_id', 'is_not', ViaType.RULE
      should_match 'current_via_id', 'is', ViaType.RULE
    end

    describe "when a trigger has changed something?" do
      before do
        @ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.WEB_FORM)
        @ticket.status_id = 4
        @ticket.audit.grow
        @ticket.audit.events.first.via_id = ViaType.RULE
      end

      should_match 'current_via_id', 'is', ViaType.RULE
      should_not_match 'current_via_id', 'is_not', ViaType.RULE
    end
  end

  describe "in_business_hours target" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.audit.grow
    end

    describe "when in business hours" do
      before do
        Ticket.any_instance.stubs(:in_business_hours?).returns(true)
      end

      should_match     'in_business_hours', 'is', 'true'
      should_not_match 'in_business_hours', 'is', 'false'
    end

    describe "when not in business hours" do
      before { Ticket.any_instance.stubs(:in_business_hours?).returns(false) }

      should_not_match 'in_business_hours', 'is', 'true'
      should_match 'in_business_hours', 'is', 'false'
    end
  end

  describe "triggers using metric set conditions" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.audit.grow

      TicketMetricSet.any_instance.stubs(:replies).returns(10)
      TicketMetricSet.any_instance.stubs(:should_increment_replies?).returns(true)

      TicketMetricSet.any_instance.stubs(:reopens).returns(10)
      TicketMetricSet.any_instance.stubs(:should_increment_reopens?).returns(true)

      TicketMetricSet.any_instance.stubs(:group_stations).returns(10)
      TicketMetricSet.any_instance.stubs(:should_increment_group_stations?).returns(true)

      TicketMetricSet.any_instance.stubs(:assignee_stations).returns(10)
      TicketMetricSet.any_instance.stubs(:should_increment_assignee_stations?).returns(true)
    end

    should_match 'replies', 'greater_than', 10
    should_not_match 'replies', 'greater_than', 11

    should_match 'reopens', 'greater_than', 10
    should_not_match 'reopens', 'greater_than', 11

    should_match 'group_stations', 'greater_than', 10
    should_not_match 'group_stations', 'greater_than', 11

    should_match 'agent_stations', 'greater_than', 10
    should_not_match 'agent_stations', 'greater_than', 11
  end

  describe "requester_language target" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.audit.grow

      User.any_instance.stubs(:locale_id).returns(32)
    end

    describe "without individual language selection" do
      should_match 'locale_id', 'is', 1
    end

    describe "with individual language selection" do
      before { Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true) }

      should_not_match 'locale_id', 'is', 1
      should_match 'locale_id', 'is', 32
    end
  end

  describe "requester_default_locale target" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.audit.grow
    end

    it "fallbacks the condition to the account's language" do
      user = users(:minimum_end_user)
      assert_nil(user.locale_id)

      Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)

      assert rule_matches('locale_id', 'is', 1)
    end
  end

  describe 'when validating' do
    let(:definition) { build_trigger_definition }

    let(:trigger) do
      account.triggers.build(
        definition: definition,
        title: 'New trigger',
        account: account
      )
    end

    describe 'verify_table_limit' do
      let(:limit) { account.rules.active.where(type: 'Trigger').count }

      let(:log_message) do
        "Rules table limit exceeded: account_id: #{trigger.account.id}, "\
        "rule_type: #{trigger.type}, current limit: #{limit}"
      end

      before do
        Rule.any_instance.stubs(soft_limit_for_rule: limit)
      end

      describe_with_arturo_enabled :apply_rules_table_limits_for_triggers do
        describe 'when the account has reached the limit' do
          let(:active_trigger)   { rules(:trigger_notify_requester_of_received_request) }
          let(:inactive_trigger) { rules(:trigger_set_tag_for_recipient) }

          it 'does not allow creating new triggers' do
            Rails.logger.expects(:info).with(log_message).once

            assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
              trigger.save
            end
          end

          it 'allows an active trigger to be deactivated' do
            assert active_trigger.update_attribute(:is_active, false)
          end

          it 'allows an active trigger to be updated' do
            assert active_trigger.update_attribute(:title, 'new title')
          end

          it 'does not allow an inactive trigger to be activated' do
            assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
              inactive_trigger.update_attribute(:is_active, true)
            end
          end

          it 'allows triggers to be deleted' do
            assert active_trigger.destroy
          end
        end

        describe 'when the account has not met the limit' do
          before { account.triggers.delete_all }

          it 'persists the trigger successfully' do
            assert trigger.save
          end
        end

        describe_with_arturo_enabled :higher_triggers_table_limit do
          it 'persists the trigger successfully' do
            assert trigger.save
          end

          describe 'when the account meets the hard limit' do
            before do
              Trigger.
                any_instance.
                stubs(hard_limit_for_rule: limit)
            end

            it 'fails with an error' do
              Rails.logger.expects(:info).with(log_message).once

              assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
                trigger.save
              end
            end
          end
        end
      end

      describe_with_arturo_disabled :apply_rules_table_limits_for_triggers do
        it 'persists the trigger successfully' do
          assert trigger.save
        end
      end
    end

    describe 'and the definition is valid' do
      it 'is valid' do
        assert trigger.valid?
      end
    end

    describe 'and the trigger does not contain conditions' do
      let(:definition) do
        Definition.new.tap do |definition|
          definition.actions.push(
            DefinitionItem.new('current_tags', nil, 'first_trigger')
          )
        end
      end

      before { trigger.save }

      it 'is invalid' do
        refute trigger.valid?
      end

      it 'adds an `at_least_one_condition_trigger` error' do
        assert_equal(
          I18n.t('txt.admin.models.rules.rule.at_least_one_condition_trigger'),
          trigger.errors.to_a.last
        )
      end
    end

    describe 'and the trigger contains an SMS notification action' do
      let(:notification_action) do
        DefinitionItem.new(
          'notification_sms_user',
          nil,
          ['assignee_id', 1, 'message body']
        )
      end

      let(:definition) do
        Definition.new.tap do |definition|
          definition.conditions_all.push(
            DefinitionItem.new('status_id', 'value', [StatusType.PENDING])
          )
          definition.actions.push(notification_action)
        end
      end

      describe 'and the account has the SMS voice feature' do
        before do
          account.stubs(:voice_feature_enabled?).with(:sms).returns(true)
        end

        describe 'with valid SMS notification parameters' do
          it 'is valid' do
            assert trigger.valid?
          end
        end

        describe 'with invalid SMS notification parameters' do
          let(:notification_action) do
            DefinitionItem.new('notification_sms_user', nil, ['assignee_id', 1])
          end

          before { trigger.save }

          it 'is invalid' do
            refute trigger.valid?
          end

          it 'adds an `invalid_sms_notification` error' do
            assert_equal(
              I18n.t('txt.admin.models.rules.rule.invalid_sms_notification'),
              trigger.errors.to_a.last
            )
          end
        end
      end

      describe 'and the account does not have the SMS voice feature' do
        before do
          account.stubs(:voice_feature_enabled?).with(:sms).returns(false)

          trigger.save
        end

        it 'is invalid' do
          refute trigger.valid?
        end

        it 'adds an `invalid_target` error' do
          assert_equal(
            I18n.t(
              'txt.admin.models.rules.rule.invalid_target',
              target: 'notification_sms_user'
            ),
            trigger.errors.to_a.last
          )
        end
      end
    end
  end

  describe "Custom field matching" do
    [true, false].each do |enhanced_value_variant|
      describe "while enhanced_value is #{enhanced_value_variant}" do
        before do
          Account.any_instance.stubs(:has_enhanced_values?).returns(enhanced_value_variant)
          @ticket = tickets(:minimum_1)
        end

        describe "non-taggers" do
          before do
            @fields = [:field_decimal_custom, :field_integer_custom, :field_text_custom, :field_textarea_custom, :field_regexp_custom]
          end

          it "#not_present" do
            @fields.each do |field|
              id = ticket_fields(field).id
              assert rule_matches("ticket_fields_#{id}", "not_present", "")
              assert rule_matches("ticket_fields_#{id}", "not_present", nil)
            end
          end

          it "#present" do
            @fields.each do |field|
              id = ticket_fields(field).id
              @ticket.ticket_field_entries.create(value: "1", ticket_field_id: id)
              assert rule_matches("ticket_fields_#{id}", "present", "")
              assert rule_matches("ticket_fields_#{id}", "present", nil)
            end
          end
        end

        describe "taggers" do
          before do
            @tagger_id = ticket_fields(:field_tagger_custom).id
            @custom_field_option = custom_field_options(:field_tagger_custom_option_2)
          end

          it "detects value match" do
            @ticket.ticket_field_entries.create(value: @custom_field_option.value, ticket_field_id: @tagger_id)
            assert rule_matches("ticket_fields_#{@tagger_id}", 'is', @custom_field_option.id)
          end

          it "detects blank match" do
            assert rule_matches("ticket_fields_#{@tagger_id}", 'is', '')
          end

          it "non-values match" do
            @ticket.ticket_field_entries.create(value: "hovzat", ticket_field_id: @tagger_id)
            refute rule_matches("ticket_fields_#{@tagger_id}", 'is', @custom_field_option.id)
          end

          it "non-values match for non existing entry" do
            refute rule_matches("ticket_fields_#{@tagger_id}", 'is', @custom_field_option.id)
          end

          describe "not_present" do
            it "matches nil tagger" do
              assert rule_matches("ticket_fields_#{@tagger_id}", 'not_present', '')
              assert rule_matches("ticket_fields_#{@tagger_id}", 'not_present', nil)
            end

            it "matches empty tagger value" do
              @ticket.ticket_field_entries.create(value: '', ticket_field_id: @tagger_id)
              assert rule_matches("ticket_fields_#{@tagger_id}", 'not_present', '')
              assert rule_matches("ticket_fields_#{@tagger_id}", 'not_present', nil)
            end
          end

          describe "present" do
            before do
              @ticket.ticket_field_entries.create(value: @custom_field_option.value, ticket_field_id: @tagger_id)
            end

            it "matches any value besides empty and nil" do
              assert rule_matches("ticket_fields_#{@tagger_id}", 'present', '')
              assert rule_matches("ticket_fields_#{@tagger_id}", 'present', nil)
              refute rule_matches("ticket_fields_#{@tagger_id}", 'not_present', '')
              refute rule_matches("ticket_fields_#{@tagger_id}", 'not_present', nil)
            end

            it "gets invalidated(condition returns false) if deleting a selected dropdown option" do
              assert rule_matches("ticket_fields_#{@tagger_id}", 'present', nil)
              @ticket.ticket_field_entries.delete_all
              refute rule_matches("ticket_fields_#{@tagger_id}", 'present', nil)
              assert rule_matches("ticket_fields_#{@tagger_id}", 'not_present', nil)
            end
          end
        end

        describe "checkboxes" do
          before do
            @checkbox_id = ticket_fields(:field_checkbox_custom).id
          end

          it "detects value match" do
            @ticket.ticket_field_entries.create(value: 1, ticket_field_id: @checkbox_id)
            assert rule_matches("ticket_fields_#{@checkbox_id}", 'is', true)
          end

          it "detects blank match" do
            assert rule_matches("ticket_fields_#{@checkbox_id}", 'is', '')
          end

          it "non-values match" do
            @ticket.ticket_field_entries.create(value: 0, ticket_field_id: @checkbox_id)
            refute rule_matches("ticket_fields_#{@checkbox_id}", 'is', true)
          end

          it "evaluates absent checkbox value as false" do
            assert rule_matches("ticket_fields_#{@checkbox_id}", 'is', "false")
          end

          it "does not die horribly if referencing a bad id" do
            refute rule_matches("ticket_fields_999999999", 'is', '0')
          end
        end

        describe "dates" do
          describe "with UTC account timezone" do
            before do
              account = accounts(:minimum)
              account.time_zone = "UTC"
              account.save!

              @date_field = FieldDate.create!(account: account, title: "Custom wombat date", is_active: true)
              @day = Time.parse("2014-02-18")
              @ticket.ticket_field_entries.create(value: "2014-02-18", ticket_field_id: @date_field.id)
            end

            it "detects value match" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'is', "2014-02-18")
            end

            it "detects non-value match" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'is_not', "2014-02-12")
            end

            it "supports inequality operators" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'greater_than_equal', @day.advance(seconds: -1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'greater_than_equal', @day.to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'greater_than',       @day.advance(seconds: -1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than', @day.advance(seconds: 1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than_equal',   @day.advance(seconds: 1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than_equal',   @day.to_s)
            end

            it "supports present, not_present operators" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'present', nil)
              refute   rule_matches("ticket_fields_#{@date_field.id}", 'not_present', nil)
            end

            it "supports within_previous_n_days operator" do
              Timecop.freeze(@day + 2.days.to_i)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 1)
            end

            it "supports within_next_n_days operator" do
              Timecop.freeze(@day - 2.days.to_i + 1.second)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 1)
            end
          end

          describe "with +ve (+0100) account timezone" do
            before do
              account = accounts(:minimum)
              account.time_zone = "Copenhagen" # +1 or +2 based on DST
              account.save!

              @date_field = FieldDate.create!(account: account, title: "Custom wombat date", is_active: true)
              @day_utc = Time.parse("2014-02-18") # this is UTC, since server time is UTC
              @day_in_zone = Time.use_zone("Copenhagen") { Time.zone.parse("2014-02-18") }
              @ticket.ticket_field_entries.create(value: "2014-02-18", ticket_field_id: @date_field.id)
            end

            it "detects value match" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'is', "2014-02-18")
            end

            it "detects non-value match" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'is_not', "2014-02-12")
            end

            it "supports inequality operators" do
              refute rule_matches("ticket_fields_#{@date_field.id}", 'greater_than', @day_utc.to_s)

              assert rule_matches("ticket_fields_#{@date_field.id}", 'greater_than', @day_in_zone.advance(seconds: -1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'greater_than_equal', @day_in_zone.to_s)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'greater_than', @day_in_zone.to_s)

              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than', @day_utc.to_s)

              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than', @day_in_zone.advance(seconds: 1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than_equal', @day_in_zone.advance(seconds: 1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than_equal', @day_in_zone.to_s)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'less_than', @day_in_zone.to_s)
            end

            it "supports present, not_present operators" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'present', nil)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'not_present', nil)
            end

            it "supports within_previous_n_days operator" do
              Timecop.freeze(@day_in_zone + 2.days.to_i)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 1)

              # c |     c |     c |
              # c-|-----c-|-----c-|
              # c |     c |     c |
              # v                 ^
              #
              #  v - value, ^ - now, | - utc day boundary, c - Copenhagen day boundary

              Timecop.freeze(@day_utc + 2.days.to_i)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 1)
            end

            it "supports within_next_n_days operator" do
              Timecop.freeze(@day_in_zone - 2.days.to_i + 1.second)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 1)

              # c |     c |     c |
              # c-|-----c-|-----c-|
              # c |     c |     c |
              #    ^            v
              #
              #  v - value, ^ - now, ! - utc day boundary, | - Copenhagen day boundary

              Timecop.freeze(@day_utc - 2.days.to_i + 1.second)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 1)
            end
          end

          describe "with -ve (-0700) account timezone" do
            before do
              account = accounts(:minimum)
              account.time_zone = "Pacific Time (US & Canada)" # -7 or -8
              account.save!

              @date_field = FieldDate.create!(account: account, title: "Custom wombat date", is_active: true)
              @day_utc = Time.parse("2014-02-18") # this is UTC, since server time is UTC
              @day_in_zone = Time.use_zone("Pacific Time (US & Canada)") { Time.zone.parse("2014-02-18") }
              @ticket.ticket_field_entries.create(value: "2014-02-18", ticket_field_id: @date_field.id)
            end

            it "detects value match" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'is', "2014-02-18")
            end

            it "detects non-value match" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'is_not', "2014-02-12")
            end

            it "supports inequality operators" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'greater_than', @day_utc.to_s)

              assert rule_matches("ticket_fields_#{@date_field.id}", 'greater_than_equal', @day_in_zone.to_s)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'greater_than', @day_in_zone.to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'greater_than_equal', @day_in_zone.advance(seconds: -1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'greater_than', @day_in_zone.advance(seconds: -1).to_s)

              refute rule_matches("ticket_fields_#{@date_field.id}", 'less_than', @day_utc.to_s)

              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than', @day_in_zone.advance(seconds: 1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than_equal', @day_in_zone.advance(seconds: 1).to_s)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'less_than_equal', @day_in_zone.to_s)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'less_than', @day_in_zone.to_s)
            end

            it "supports present, not_present operators" do
              assert rule_matches("ticket_fields_#{@date_field.id}", 'present', nil)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'not_present', nil)
            end

            it "supports within_previous_n_days operator" do
              Timecop.freeze(@day_in_zone + 2.days.to_i)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 1)

              # | p     | p     | p
              # |-p-----|-p-----|-p
              # | p     | p     | p
              #   v             ^
              #
              #  v - value, ^ - now, | - utc day boundary, p - Pacific day boundary

              Timecop.freeze(@day_utc + 2.days.to_i)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_previous_n_days', 1)
            end

            it "supports within_next_n_days operator" do
              Timecop.freeze(@day_in_zone - 2.days.to_i + 1.second)
              assert rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 1)

              # | p     | p     | p
              # |-p-----|-p-----|-p
              # | p     | p     | p
              #  ^                v
              #
              #  v - value, ^ - now, | - utc day boundary, p - Pacific day boundary

              Timecop.freeze(@day_utc - 2.days.to_i + 1.second)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 2)
              refute rule_matches("ticket_fields_#{@date_field.id}", 'within_next_n_days', 1)
            end
          end
        end

        describe "with user fields" do
          before do
            @account = @ticket.account
          end

          describe "and decimals" do
            before do
              create_user_custom_field!("field_decimal", "decimal field", "Decimal")
              create_user_custom_field!("field_decimal2", "decimal field", "Decimal")
              @ticket.requester.custom_field_values.update_from_hash("field_decimal" => 123.45)
              @ticket.requester.save!
              @ticket.requester.reload
            end

            it "matchs properly" do
              assert rule_matches("requester.custom_fields.field_decimal", 'is', "123.45")
              assert rule_matches("requester.custom_fields.field_decimal", 'greater_than', "123")
              assert rule_matches("requester.custom_fields.field_decimal", 'greater_than_equal', "123.45")
              refute   rule_matches("requester.custom_fields.field_decimal", 'is', "123.42")
              refute   rule_matches("requester.custom_fields.field_decimal", 'less_than', "123.42")
              refute   rule_matches("requester.custom_fields.field_decimal", 'less_than_equal', "123.42")
            end

            it "matchs properly when field or value is not set" do
              assert rule_matches("requester.custom_fields.field_decimal", 'is_not', "")
              refute rule_matches("requester.custom_fields.field_decimal", 'is', nil)
              refute rule_matches("requester.custom_fields.field_decimal2", 'is_not', nil)
              assert rule_matches("requester.custom_fields.field_decimal2", 'is', nil)
            end

            it "supports present, not_present" do
              assert rule_matches("requester.custom_fields.field_decimal", 'present', nil)
              refute rule_matches("requester.custom_fields.field_decimal", 'not_present', nil)

              @ticket.requester.custom_field_values.update_from_hash("field_decimal" => nil)
              @ticket.requester.save!

              refute rule_matches("requester.custom_fields.field_decimal", 'present', nil)
              assert rule_matches("requester.custom_fields.field_decimal", 'not_present', nil)
            end
          end

          describe "and integers" do
            before do
              create_user_custom_field!("field_integer", "integer field", "Integer")
              @ticket.submitter.custom_field_values.update_from_hash("field_integer" => 123)
              @ticket.submitter.save!
              @ticket.submitter.reload
            end

            it "matchs properly" do
              assert rule_matches("submitter.custom_fields.field_integer", 'is', "123")
              assert rule_matches("submitter.custom_fields.field_integer", 'greater_than', "121")
            end
          end

          describe "and dates" do
            before do
              create_user_custom_field!("field_date", "date field", "Date")
              @day = Time.parse("2012-11-06")
              @ticket.assignee.custom_field_values.update_from_hash("field_date" => @day)
              @ticket.assignee.save!
              @ticket.assignee.reload
            end

            it "supports is, is_not" do
              assert rule_matches("assignee.custom_fields.field_date", 'is', "2012-11-06")
              assert rule_matches("assignee.custom_fields.field_date", 'is_not', "2012-11-08")
            end

            it "supports inequality operators" do
              assert rule_matches("assignee.custom_fields.field_date", 'greater_than_equal', @day.advance(seconds: -1).to_s)
              assert rule_matches("assignee.custom_fields.field_date", 'greater_than_equal', @day.to_s)
              assert rule_matches("assignee.custom_fields.field_date", 'greater_than',       @day.advance(seconds: -1).to_s)
              assert rule_matches("assignee.custom_fields.field_date", 'less_than', @day.advance(seconds: 1).to_s)
              assert rule_matches("assignee.custom_fields.field_date", 'less_than_equal',   @day.advance(seconds: 1).to_s)
              assert rule_matches("assignee.custom_fields.field_date", 'less_than_equal',   @day.to_s)
            end

            it "supports present, not_present operaetors" do
              assert rule_matches("assignee.custom_fields.field_date", 'present', nil)
              refute rule_matches("assignee.custom_fields.field_date", 'not_present', nil)
              @ticket.assignee.custom_field_values.update_from_hash("field_date" => nil)
              @ticket.assignee.save!
              refute rule_matches("assignee.custom_fields.field_date", 'present', nil)
              assert rule_matches("assignee.custom_fields.field_date", 'not_present', nil)
            end
          end

          describe "and text fields" do
            before do
              create_user_custom_field!("field_text", "text field", "Text")
              @ticket.assignee.custom_field_values.update_from_hash("field_text" => "Hello!")
              @ticket.assignee.save!
              @ticket.assignee.reload
            end

            it "supports :is operator" do
              assert rule_matches("assignee.custom_fields.field_text", 'is', "Hello!")
            end

            it "supports :is_not operator" do
              refute rule_matches("assignee.custom_fields.field_text", 'is_not', "Hello!")
            end

            it "strips whitespace from both operands" do
              @ticket.assignee.custom_field_values.update_from_hash("field_text" => " Hello! ")
              @ticket.assignee.save!
              assert rule_matches("assignee.custom_fields.field_text", 'is', " Hello!   ")
            end

            it "supports :includes_words, :not_includes_words operators" do
              @ticket.assignee.custom_field_values.update_from_hash("field_text" => "foo bar baz")
              @ticket.assignee.save!
              assert rule_matches("assignee.custom_fields.field_text", 'includes_words', "bar")
              assert rule_matches("assignee.custom_fields.field_text", 'includes_words', "baz")
              assert rule_matches("assignee.custom_fields.field_text", 'includes_words', "foo")
              refute rule_matches("assignee.custom_fields.field_text", 'includes_words', "carson")

              assert rule_matches("assignee.custom_fields.field_text", 'not_includes_words', "carson")
              assert rule_matches("assignee.custom_fields.field_text", 'not_includes_words', "ba")
            end

            it "supports ;includes_string, :not_includes_string operators" do
              @ticket.assignee.custom_field_values.update_from_hash("field_text" => "foo bar baz joe")
              @ticket.assignee.save!

              assert rule_matches("assignee.custom_fields.field_text", 'includes_string', "bar")
              assert rule_matches("assignee.custom_fields.field_text", 'includes_string', "baz")
              assert rule_matches("assignee.custom_fields.field_text", 'includes_string', "bar baz")
              refute rule_matches("assignee.custom_fields.field_text", 'includes_string', "alskdjfad")
            end

            it "supports :present, :not_present operator" do
              assert rule_matches("assignee.custom_fields.field_text", :present, "")
              refute rule_matches("assignee.custom_fields.field_text", :not_present, "")

              @ticket.assignee.custom_field_values.update_from_hash("field_text" => nil)
              @ticket.assignee.save!
              @ticket.assignee.reload

              refute rule_matches("assignee.custom_fields.field_text", :present, "")
              assert rule_matches("assignee.custom_fields.field_text", :not_present, "")
            end
          end

          describe "and checkboxes" do
            before do
              create_user_custom_field!("field_checkbox", "checkbox field", "Checkbox")
              @ticket.assignee.custom_field_values.update_from_hash("field_checkbox" => "true")
              @ticket.assignee.save!
              @ticket.assignee.reload
            end

            it "matchs properly" do
              assert rule_matches("assignee.custom_fields.field_checkbox", 'is', "true")
              assert rule_matches("assignee.custom_fields.field_checkbox", 'is_not', "0")
              assert rule_matches("assignee.custom_fields.field_checkbox", 'is_not', nil)
            end
          end

          describe "and dropdowns" do
            before do
              @cf = create_user_custom_field!("field_dropdown", "dropdown field", "Dropdown")
              @cf.dropdown_choices.build(name: "Choice 1", value: "choice_1")
              @choice_2 = @cf.dropdown_choices.build(name: "Choice 2", value: "choice_2")
              @cf.dropdown_choices.build(name: "Choice 3", value: "choice_3")
              @cf.dropdown_choices.build(name: "Choice 4", value: "choice_4")
              @cf.save!

              @cfx = create_user_custom_field!("field_dropdownx", "dropdown field", "Dropdown")
              @cfx.dropdown_choices.build(name: "Choice 1", value: "choice_1x")
              @cfx.save!

              @ticket.requester.custom_field_values.update_from_hash("field_dropdown" => "choice_2")
              @ticket.requester.save!
              @ticket.requester.reload
            end

            it "supports :is, :is_not" do
              assert rule_matches("requester.custom_fields.field_dropdown", 'is', "choice_2")
              assert rule_matches("requester.custom_fields.field_dropdown", 'is', @choice_2.id.to_s)
              assert rule_matches("requester.custom_fields.field_dropdown", 'is_not', "choice_3")
              assert rule_matches("requester.custom_fields.field_dropdown", 'is_not', nil)
            end

            it "matchs properly when field or value is not set" do
              refute rule_matches("requester.custom_fields.field_dropdownx", 'is_not', nil)
              assert rule_matches("requester.custom_fields.field_dropdownx", 'is', nil)
            end

            it "supports :present, :not_present" do
              assert rule_matches("requester.custom_fields.field_dropdown",  "present", nil)
              refute rule_matches("requester.custom_fields.field_dropdownx", "present", nil)
              assert rule_matches("requester.custom_fields.field_dropdownx", "not_present", nil)
              refute rule_matches("requester.custom_fields.field_dropdown",  "not_present", nil)
            end
          end
        end

        describe "with organization fields" do
          before do
            @account = @ticket.account
            @org = organizations(:minimum_organization1)
          end

          describe "with an unspecified source" do
            before do
              create_organization_custom_field!("field_decimal", "decimal field", "Decimal")
              @ticket.requester.organization = @org
              @ticket.requester.organization.custom_field_values.update_from_hash("field_decimal" => 123.45)
              @ticket.requester.organization.save!
              @ticket.requester.save!
              @ticket.requester.reload
            end

            describe "on a ticket with an organization" do
              before do
                @ticket.organization = organizations(:minimum_organization2)
                @ticket.organization.custom_field_values.update_from_hash("field_decimal" => 98.76)
                @ticket.organization.save!
                @ticket.organization.reload
              end

              it "refers to ticket's organization" do
                assert rule_matches("requester.organization.custom_fields.field_decimal", 'is', "123.45")
                assert rule_matches("organization.custom_fields.field_decimal", 'is', "98.76")
              end
            end

            describe "on a ticket without an organization" do
              it "should not match anything" do
                assert rule_matches("requester.organization.custom_fields.field_decimal", 'is', "123.45")
                refute rule_matches("organization.custom_fields.field_decimal", 'is_not', nil)
                refute rule_matches("organization.custom_fields.field_decimal", 'is', nil)
              end
            end
          end

          describe "and decimals" do
            before do
              create_organization_custom_field!("field_decimal", "decimal field", "Decimal")
              create_organization_custom_field!("field_decimal2", "decimal field", "Decimal")
              @ticket.requester.organization = @org
              @ticket.requester.organization.custom_field_values.update_from_hash("field_decimal" => 123.45)
              @ticket.requester.organization.save!
              @ticket.requester.save!
              @ticket.requester.reload
            end

            it "matchs properly" do
              assert rule_matches("requester.organization.custom_fields.field_decimal", 'is', "123.45")
              assert rule_matches("requester.organization.custom_fields.field_decimal", 'greater_than', "123")
            end

            it "matchs properly when field or value is not set" do
              assert rule_matches("requester.organization.custom_fields.field_decimal", 'is_not', "")
              refute rule_matches("requester.organization.custom_fields.field_decimal", 'is', nil)
              refute rule_matches("requester.organization.custom_fields.field_decimal2", 'is_not', nil)
              assert rule_matches("requester.organization.custom_fields.field_decimal2", 'is', nil)
            end
          end

          describe "and integers" do
            before do
              create_organization_custom_field!("field_integer", "integer field", "Integer")
              @ticket.submitter.organization = @org
              @ticket.submitter.organization.custom_field_values.update_from_hash("field_integer" => 123)
              @ticket.submitter.organization.save!
              @ticket.submitter.save!
              @ticket.submitter.reload
            end

            it "matchs properly" do
              assert rule_matches("submitter.organization.custom_fields.field_integer", 'is', "123")
              assert rule_matches("submitter.organization.custom_fields.field_integer", 'greater_than', "121")
            end
          end

          describe "and dates" do
            before do
              create_organization_custom_field!("field_date", "date field", "Date")
              @day = Time.parse("2012-11-06")
              @ticket.assignee.organization = @org
              @ticket.assignee.organization.custom_field_values.update_from_hash("field_date" => @day)
              @ticket.assignee.organization.save!
              @ticket.assignee.save!
              @ticket.assignee.reload
            end

            it "matchs properly" do
              assert rule_matches("assignee.organization.custom_fields.field_date", 'is', "2012-11-06")
              assert rule_matches("assignee.organization.custom_fields.field_date", 'greater_than_equal', @day.advance(seconds: -1).to_s)
            end
          end

          describe "and text fields" do
            before do
              create_organization_custom_field!("field_text", "text field", "Text")
              @ticket.assignee.organization = @org
              @ticket.assignee.organization.custom_field_values.update_from_hash("field_text" => "Hello!")
              @ticket.assignee.organization.save!
              @ticket.assignee.save!
              @ticket.assignee.reload
            end

            it "matchs properly" do
              assert rule_matches("assignee.organization.custom_fields.field_text", 'is', "Hello!")
              assert rule_matches("assignee.organization.custom_fields.field_text", 'greater_than_equal', "Goodbye.")
            end
          end

          describe "and checkboxes" do
            before do
              create_organization_custom_field!("field_checkbox", "checkbox field", "Checkbox")
              @ticket.assignee.organization = @org
              @ticket.assignee.organization.custom_field_values.update_from_hash("field_checkbox" => "true")
              @ticket.assignee.organization.save!
              @ticket.assignee.save!
              @ticket.assignee.reload
            end

            it "matchs properly" do
              assert rule_matches("assignee.organization.custom_fields.field_checkbox", 'is', "true")
              assert rule_matches("assignee.organization.custom_fields.field_checkbox", 'is_not', "0")
              assert rule_matches("assignee.organization.custom_fields.field_checkbox", 'is_not', nil)
            end
          end

          describe "and dropdowns" do
            before do
              @cf = create_organization_custom_field!("field_dropdown", "dropdown field", "Dropdown")
              @cf.dropdown_choices.build(name: "Choice 1", value: "choice_1")
              @choice_2 = @cf.dropdown_choices.build(name: "Choice 2", value: "choice_2")
              @cf.dropdown_choices.build(name: "Choice 3", value: "choice_3")
              @cf.dropdown_choices.build(name: "Choice 4", value: "choice_4")
              @cf.save!

              @cfx = create_organization_custom_field!("field_dropdownx", "dropdown field", "Dropdown")
              @cfx.dropdown_choices.build(name: "Choice 1", value: "choice_1x")
              @cfx.save!

              @ticket.requester.organization = @org
              @ticket.requester.organization.custom_field_values.update_from_hash("field_dropdown" => "choice_2")
              @ticket.requester.organization.save!
              @ticket.requester.save!
              @ticket.requester.reload
            end

            it "matchs properly" do
              assert rule_matches("requester.organization.custom_fields.field_dropdown", 'is', "choice_2")
              assert rule_matches("requester.organization.custom_fields.field_dropdown", 'is', @choice_2.id.to_s)
              assert rule_matches("requester.organization.custom_fields.field_dropdown", 'is_not', "choice_3")
              assert rule_matches("requester.organization.custom_fields.field_dropdown", 'is_not', nil)
            end

            it "matchs properly when field or value is not set" do
              refute rule_matches("requester.organization.custom_fields.field_dropdownx", 'is_not', nil)
              assert rule_matches("requester.organization.custom_fields.field_dropdownx", 'is', nil)
            end
          end
        end
      end
    end

    describe "Permissions" do
      describe "a trigger set to assign a ticket to an unassignable agent" do
        before do
          @trigger = new_rule(DefinitionItem.new('assignee_id', nil, users(:minimum_agent).id))
          @ticket = tickets(:minimum_1)
          @ticket.assignee_id = users(:minimum_admin).id
          @ticket.will_be_saved_by(@ticket.assignee)
          @ticket.save!
          Access::Policies::TicketPolicy.any_instance.stubs(:edit?).returns(false)
        end

        it "does not do that" do
          assert_not_equal(@ticket.assignee_id, users(:minimum_agent).id)
          @trigger.apply_actions(@ticket)
          assert_not_equal(@ticket.assignee_id, users(:minimum_agent).id)
        end

        it "creates an error audit" do
          @trigger.apply_actions(@ticket)
          assert @ticket.audit.events.first
          assert @ticket.audit.events.first.is_a?(TranslatableError)
        end

        describe "the audit itself" do
          it "expands properly" do
            @trigger.apply_actions(@ticket)
            expanded = @ticket.audit.events.first.to_s
            _(expanded).must_include "Unable to apply action"
            assert_match /rule #{@trigger.id}/, expanded
          end
        end

        describe "when hit repeatedly" do
          before do
            Access::Policies::TicketPolicy.any_instance.stubs(:be_assigned_to?).returns(false)
            Access::Validations::TicketValidator.any_instance.stubs(:check_edit_access)

            @trigger.apply_actions(@ticket)
            assert @ticket.audit.events.first
            assert @ticket.audit.events.first.is_a?(TranslatableError)
            @ticket.save!

            @ticket.reload
            @ticket.assignee_id = users(:minimum_admin).id
            @ticket.will_be_saved_by(@ticket.assignee)
            @trigger.apply_actions(@ticket)
            @ticket.save!
            @ticket.reload
          end

          it "does not add a duplicate error audit" do
            assert_equal 1, @ticket.events.select { |e| e.is_a?(TranslatableError) }.size
          end
        end
      end
    end
  end

  describe "multiple trigger actions" do
    describe "a trigger that assigns multiple CCs" do
      before do
        @ticket = tickets(:minimum_1)
        assert_equal @ticket.priority_id, PriorityType.NORMAL
        assert_equal @ticket.collaborators.size, 0
        @cc_user_ids = [
          users(:minimum_end_user).id,
          users(:minimum_end_user2).id,
          users(:minimum_agent).id,
          users(:minimum_admin).id
        ]
        definition = Definition.new
        definition.conditions_all.push(DefinitionItem.new('priority_id', 'value', [PriorityType.HIGH]))
        @cc_user_ids.each do |cc_user_id|
          definition.actions.push(DefinitionItem.new('cc', nil, cc_user_id))
        end
        @trigger = Trigger.create!(
          account: accounts(:minimum),
          title: 'multiple-cc-trigger-test',
          definition: definition,
          owner: accounts(:minimum)
        )
      end

      it "fired the trigger" do
        @ticket.priority_id = PriorityType.HIGH
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal @cc_user_ids.sort, @ticket.collaborators.map { |x| x[:id] }.sort
      end
    end
  end

  describe "Ticket.save" do
    before do
      account.triggers.destroy_all

      @ticket = tickets(:minimum_2)

      @matching_trigger = begin
        create_trigger(
          definition: Definition.new(
            [DefinitionItem.new('status_id', 'is', StatusType.OPEN)],
            [],
            [DefinitionItem.new('satisfaction_score', 'is', SatisfactionType.GOOD)]
          )
        )
      end

      @matching_trigger.title = "Matching Trigger"
      @matching_trigger.owner = account

      assert @ticket.status?(:open)
    end

    it "runs and apply matching triggers" do
      @matching_trigger.definition.actions << DefinitionItem.new('set_tags', nil, "matching_trigger")
      @matching_trigger.save!

      @ticket.current_tags = ""
      @ticket.will_be_saved_by(account.owner)
      @ticket.save!
      @ticket.reload

      assert_equal "matching_trigger", @ticket.current_tags
    end

    it "updates tags and ticket_field_entries when adding a tag" do
      @matching_trigger.definition.actions << DefinitionItem.new('current_tags', nil, "hilarious")
      @matching_trigger.save!

      @ticket.current_tags = ""
      @ticket.will_be_saved_by(account.owner)
      @ticket.save!
      @ticket.reload

      assert_equal "hilarious", @ticket.current_tags

      ticket_field = ticket_fields(:field_tagger_custom)
      ticket_field_entry = @ticket.ticket_field_entries.find_by_ticket_field_id(ticket_field.id)
      assert_equal "hilarious", ticket_field_entry.value

      # The above created a new ticket_field_entry. The bellow updated that entry.

      Timecop.freeze(1.second.from_now) # force a new updated_at to miss cache

      @matching_trigger.definition.actions.last.value = "booring"
      @matching_trigger.save!

      @ticket.current_tags = ""
      @ticket.will_be_saved_by(account.owner)
      @ticket.save!
      @ticket.reload

      assert_equal "booring", @ticket.current_tags

      ticket_field_entry = @ticket.ticket_field_entries.find_by_ticket_field_id(ticket_field.id)
      assert_equal "booring", ticket_field_entry.value
    end

    it "updates tags and ticket_field_entries when changing a field tagger" do
      ticket_field = ticket_fields(:field_tagger_custom)
      hilarious    = ticket_field.custom_field_options.find_by_value('hilarious')
      booring      = ticket_field.custom_field_options.find_by_value('booring')

      @matching_trigger.definition.actions << DefinitionItem.new("ticket_fields_#{ticket_field.id}", nil, hilarious.id)
      @matching_trigger.save!

      @ticket.current_tags = ""
      @ticket.will_be_saved_by(account.owner)
      @ticket.save!
      @ticket.reload

      assert_equal "hilarious", @ticket.current_tags

      ticket_field_entry = @ticket.ticket_field_entries.find_by_ticket_field_id(ticket_field.id)
      assert_equal "hilarious", ticket_field_entry.value

      # The above created a new ticket_field_entry. The bellow updated that entry.
      Timecop.freeze(1.second.from_now) # force a new updated_at to miss cache

      @matching_trigger.definition.actions.last.value = booring.id
      @matching_trigger.save!

      @ticket.current_tags = ""
      @ticket.will_be_saved_by(account.owner)
      @ticket.save!
      @ticket.reload

      assert_equal "booring", @ticket.current_tags

      ticket_field_entry = @ticket.ticket_field_entries.find_by_ticket_field_id(ticket_field.id)
      assert_equal "booring", ticket_field_entry.value
    end
  end

  describe 'when saving' do
    let(:trigger) do
      create_trigger(
        definition: Definition.new.tap do |definition|
          definition.conditions_all.push(
            DefinitionItem.new('status_id', 'is', StatusType.Open)
          )
          definition.actions.push(
            DefinitionItem.new('current_tags', nil, 'Burr')
          )
        end
      )
    end

    let(:revision) { trigger.revisions.last }

    it 'has an initial revision' do
      assert trigger.revisions.one?
    end

    it 'sets the initial revision `nice_id` to 1' do
      assert_equal 1, trigger.revisions.first.nice_id
    end

    it 'relates revisions to the trigger account' do
      assert_equal trigger.account.id, revision.account_id
    end

    describe 'when the system edits a trigger' do
      it 'sets the system as the revision author' do
        assert_equal User.system_user_id, revision.author_id
      end
    end

    describe 'when a user edits a trigger' do
      before do
        trigger.current_user = account.owner
        trigger.title        = 'Burr, sir'

        trigger.save!
      end

      it 'sets the user as the revision author' do
        assert_equal trigger.current_user.id, revision.author_id
      end
    end

    describe 'and there are multiple revisions' do
      let!(:first_revision) { trigger.revisions.last }
      let(:second_revision) { trigger.revisions.last }

      before do
        trigger.title = 'Alexander Hamilton'

        trigger.save!

        first_revision.update_column(:id, 2)
        second_revision.update_column(:id, 1)
      end

      it 'increments the `nice_id`' do
        assert_equal first_revision.nice_id.next, second_revision.nice_id
      end

      it 'returns the revisions in `nice_id` order' do
        assert_equal(
          TriggerRevision.where(rule_id: trigger.id).order(:nice_id).map(&:id),
          trigger.reload.revisions.map(&:id)
        )
      end
    end

    describe 'and a non-revision attribute changes' do
      before do
        trigger.position = 1_776

        trigger.save!
      end

      should_not_change('revisions') do
        trigger.revisions.count(:all)
      end
    end

    describe 'and a revision attribute changes' do
      describe 'and the attribute is `title`' do
        before do
          trigger.title = 'Alexander Hamilton'

          trigger.save!
        end

        should_change('revisions', by: 1) do
          trigger.revisions.count(:all)
        end
      end

      describe 'and the attribute is `description`' do
        before do
          trigger.description = 'Founding Father'

          trigger.save!
        end

        should_change('revisions', by: 1) do
          trigger.revisions.count(:all)
        end
      end

      describe 'and the attribute is `is_active`' do
        before do
          trigger.is_active = false

          trigger.save!
        end

        should_change('revisions', by: 1) do
          trigger.revisions.count(:all)
        end
      end

      describe 'and the attibute is `definition`' do
        before do
          trigger.definition = Definition.new.tap do |definition|
            definition.conditions_all.push(
              DefinitionItem.new('status_id', 'is', StatusType.Pending)
            )
            definition.actions.push(
              DefinitionItem.new('current_tags', nil, 'Aaron')
            )
          end

          trigger.save!
        end

        should_change('revisions', by: 1) do
          trigger.revisions.count(:all)
        end
      end
    end
  end

  describe 'when destroying' do
    let(:trigger)   { create_trigger }
    let!(:revision) { trigger.revisions.last }

    before { trigger.destroy }

    it 'destroys associated revisions' do
      refute TriggerRevision.exists?(revision.id)
    end
  end

  describe "triggers and datefields with timezones" do
    before do
      @date_field = ticket_fields(:field_date_custom)
      definition = Definition.new
      definition.conditions_all.push(DefinitionItem.new('update_type', 'is', 'Change'))
      definition.actions.push(DefinitionItem.new("ticket_fields_#{@date_field.id}", 'is', ['days_from_now', '0']))
      @trigger = Trigger.create!(account: account, title: 'date field checker', definition: definition, position: 10000, owner: account)

      account.time_zone = "Copenhagen"
      account.save!
    end

    describe "when account timezone is not UTC" do
      describe "timezones eastward to UTC" do
        before do
          account.time_zone = "Copenhagen"
          account.save!
        end

        it "should have the correct date field value when we cross to next day in account timezone, but not in UTC" do
          Timecop.freeze("2016-02-10 00:00:01 +0100") do
            @ticket = tickets(:minimum_1)
            @ticket.assignee_id = users(:minimum_admin).id
            @ticket.will_be_saved_by(@ticket.assignee)
            @ticket.save!

            # In account's timezone 'Copenhagen',
            # time is now, 2016-02-10 00:00:01 +0100
            # which is UTC: 2016-02-09 23:00:01 UTC
            # and ticket field must be 2016-02-10
            ticket_field = @ticket.ticket_field_entries.find_by_ticket_field_id @date_field.id
            assert_equal ticket_field.value, "2016-02-10"
          end
        end
      end

      describe "timezones westward to UTC" do
        before do
          account.time_zone = "Pacific Time (US & Canada)"
          account.save!
        end
        it "should have the correct date field value when we cross to next day in UTC, but not in account timezone" do
          Timecop.freeze("2016-02-10 00:00:01 +0000") do
            @ticket = tickets(:minimum_1)
            @ticket.assignee_id = users(:minimum_admin).id
            @ticket.will_be_saved_by(@ticket.assignee)
            @ticket.save!

            # In account's timezone 'Pacific Time (US & Canada)',
            # time is now, 2016-02-09 16:00:01 -0700
            # which is UTC: 2016-02-10 00:00:01 UTC
            # and ticket field must be 2016-02-09
            ticket_field = @ticket.ticket_field_entries.find_by_ticket_field_id @date_field.id
            assert_equal ticket_field.value, "2016-02-09"
          end
        end
      end
    end
  end

  describe '#raw_definition' do
    let(:trigger)    { create_trigger }
    let(:definition) { trigger.definition }

    it 'returns the raw value from the database' do
      assert_equal YAML.dump(definition), trigger.raw_definition
    end
  end

  describe '#routing_action' do
    let(:trigger) { Trigger.new }
    let(:actions) { [] }

    before { trigger.definition = Definition.new([], [], actions) }

    describe 'when action is on assignee_id' do
      let(:actions) do
        [
          DefinitionItem.new('assignee_id', 'is', users(:minimum_agent).id)
        ]
      end

      it 'returns assignee_id' do
        assert_equal 'assignee_id', trigger.routing_action
      end
    end

    describe 'when action is on group_id' do
      let(:actions) do
        [
          DefinitionItem.new('group_id', 'is', groups(:minimum_group).id)
        ]
      end

      it 'returns group_id' do
        assert_equal 'group_id', trigger.routing_action
      end
    end

    describe 'when action is on assignee_id and group_id' do
      let(:actions) do
        [
          DefinitionItem.new('assignee_id', 'is', users(:minimum_agent).id),
          DefinitionItem.new('group_id', 'is', groups(:minimum_group).id)
        ]
      end

      it 'returns both' do
        assert_equal 'both', trigger.routing_action
      end
    end

    describe 'when multiple actions on assignee_id and group_id' do
      let(:actions) do
        [
          DefinitionItem.new('assignee_id', 'is', users(:minimum_agent).id),
          DefinitionItem.new('assignee_id', 'is', users(:minimum_agent).id),
          DefinitionItem.new('group_id', 'is', groups(:minimum_group).id),
          DefinitionItem.new('group_id', 'is', groups(:minimum_group).id)
        ]
      end

      it 'returns both' do
        assert_equal 'both', trigger.routing_action
      end
    end

    describe 'when action is not on assignee_id or group_id' do
      let(:actions) do
        [
          DefinitionItem.new('notification_user', 'is', %w[bill body subject])
        ]
      end

      it 'returns false' do
        refute trigger.routing_action
      end
    end
  end

  describe '#via_reference' do
    let(:trigger) { create_trigger }

    it 'returns via reference corresponding to `RULE_REVISION`' do
      assert_equal(
        {via_id: ViaType.RULE_REVISION, via_reference_id: trigger.revisions.last.id},
        trigger.via_reference
      )
    end

    describe 'when trigger does not have any revisions' do
      before { trigger.revisions = [] }

      it 'returns via reference corresponding to `RULE`' do
        assert_equal(
          {via_id: ViaType.RULE, via_reference_id: trigger.id},
          trigger.via_reference
        )
      end
    end
  end

  describe '.update_position_for_categories' do
    before do
      Trigger.destroy_all

      @category_1 = create_category(position: 1)
      @category_2 = create_category(position: 2)

      @trigger_1 = create_trigger(rules_category_id: @category_1.id, position: 1)
      @trigger_2 = create_trigger(rules_category_id: @category_1.id, position: 2)
      @trigger_3 = create_trigger(rules_category_id: @category_1.id, position: 3)

      @trigger_4 = create_trigger(rules_category_id: @category_2.id, position: 1)
      @trigger_5 = create_trigger(rules_category_id: @category_2.id, position: 2)
      @trigger_6 = create_trigger(rules_category_id: @category_2.id, position: 3)

      @values = {}
      @values.store(@category_1.id, [@trigger_2.id, @trigger_3.id, @trigger_1.id])
      @values.store(@category_2.id, [nil, @trigger_6.id, @trigger_5.id])
    end

    it 'updates position based on the provided values' do
      account.triggers.update_position_for_categories(account, @values)

      assert_equal(
        [@trigger_2, @trigger_3, @trigger_1, @trigger_4, @trigger_6, @trigger_5],
        account.triggers_with_category_position
      )
    end

    it 'generates N queries for number of categories' do
      assert_sql_queries 2, /UPDATE `rules`/ do
        account.triggers.update_position_for_categories(account, @values)
      end
    end
  end
end
