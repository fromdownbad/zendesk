require_relative '../../support/test_helper'
require_relative '../../support/cms_references_helper'
require_relative '../../support/rule'

SingleCov.covered!

describe Macro do
  include TestSupport::Rule::Helper

  extend CmsReferencesHelper

  fixtures :all

  let(:account) { accounts(:minimum) }

  creates_cms_references(
    type:    :macro,
    actions: %w[comment_value comment_value_html]
  )

  it 'prevents collection modification' do
    assert_includes(
      Macro.
          included_modules, Zendesk::PreventCollectionModificationMixin
    )
  end

  describe 'when validating' do
    describe 'before_validation hook' do
      it 'squishes the macro title' do
        new_macro = macro_with_actions('comment_value' => 'Some comment').tap do |macro|
          macro.owner = account

          macro.title = "   This is a   Macro \n with stuff in it    \n"
        end

        new_macro.save!

        assert_equal('This is a Macro with stuff in it', new_macro.title)
      end
    end

    describe 'verify_table_limit' do
      let(:limit) { account.rules.active.where(type: 'Macro').count }

      let(:log_message) do
        "Rules table limit exceeded: account_id: #{account.id}, "\
        "rule_type: #{macro_2.type}, current limit: #{limit}"
      end

      let!(:macro_1) do
        macro_with_actions(
            'comment_value' => 'Some comment',
            'comment_value_html' => '<p>So rich</p>'
          ).tap do |macro|
          macro.owner = account

          macro.title = 'New macro 1'
        end.save
      end

      let(:macro_2) do
        macro_with_actions(
          'comment_value'      => 'Some comment',
          'comment_value_html' => '<p>No rich</p>'
        ).tap do |macro|
          macro.owner = account

          macro.title = 'Copy of macro'
        end
      end

      before do
        Rule.any_instance.stubs(soft_limit_for_rule: limit)
      end

      describe_with_arturo_enabled :apply_rules_table_limits do
        describe 'when the account has reached the limit' do
          let(:active_macro)   { rules(:active_macro_for_minimum_group) }
          let(:inactive_macro) { rules(:inactive_macro_for_minimum_group) }

          it 'does not allow creating new macros' do
            Rails.logger.expects(:info).with(log_message).once

            assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
              macro_2.save
            end
          end

          it 'allows an active macro to be deactivated' do
            assert active_macro.update_attribute(:is_active, false)
          end

          it 'allows an active macro to be updated' do
            assert active_macro.update_attribute(:title, 'new title')
          end

          it 'does not allow an inactive macro to be activated' do
            assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
              inactive_macro.update_attribute(:is_active, true)
            end
          end

          it 'allows macros to be deleted' do
            assert active_macro.destroy
          end
        end

        describe 'when the account has not met the limit' do
          before { account.macros.delete_all }

          it 'persists the macro successfully' do
            assert macro_2.save
          end
        end

        describe_with_arturo_enabled :higher_macros_table_limit do
          it 'persists the macro successfully' do
            assert macro_2.save
          end

          describe 'and when the account exceeds hard limit' do
            before do
              Macro.
                any_instance.
                stubs(hard_limit_for_rule: limit)
            end

            it 'fails with an error' do
              Rails.logger.expects(:info).with(log_message).once

              assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
                macro_2.save
              end
            end
          end
        end
      end

      describe_with_arturo_disabled :apply_rules_table_limits do
        it 'persists the macro successfully' do
          assert macro_2.save
        end
      end
    end

    describe 'and the macro contains no actions' do
      let(:macro) { macro_with_actions }

      before { macro.valid? }

      it 'is not valid' do
        refute macro.valid?
      end

      it 'adds an error' do
        assert_includes(
          'cannot be blank',
          macro.errors[:actions].first
        )
      end
    end

    describe 'and the macro does not have an owner' do
      let(:macro) do
        macro_with_actions('comment_value' => 'Some comment').tap do |macro|
          macro.owner = nil

          macro.owner_type = owner_type
          macro.owner_id   = owner_id

          macro.title = 'New macro'
        end
      end

      describe 'and the owner type is `Group`' do
        let(:owner_type) { 'Group' }

        let(:group) { groups(:minimum_group) }

        before do
          GroupMacro.destroy_all

          macro.group_owner_ids = group_owner_ids

          macro.groups_macros.build do |group_macro|
            group_macro.account_id = account.id
            group_macro.group_id   = group.id
          end
        end

        describe 'and the macro belongs to multiple groups' do
          let(:group_2) { groups(:support_group) }

          let(:owner_id)        { group.id }
          let(:group_owner_ids) { [group, group_2].map(&:id) }

          before { macro.save! }

          it 'is valid' do
            assert macro.valid?
          end

          it 'sets the owner ID to the sentinel value' do
            assert_equal Rule.group_owner_id, macro.owner_id
          end

          it 'associates the groups with the macro' do
            assert_equal(
              [group, group_2].map(&:id),
              GroupMacro.where(
                account_id: account.id,
                macro_id:   macro.id
              ).pluck(:group_id)
            )
          end
        end

        describe 'and the macro belongs to a single group' do
          let(:owner_id)        { group.id }
          let(:group_owner_ids) { [group.id] }

          before { macro.save! }

          it 'is valid' do
            assert macro.valid?
          end

          it 'sets the owner ID' do
            assert_equal group.id, macro.owner_id
          end

          it 'associates the group with the macro' do
            assert(
              GroupMacro.where(
                account_id: account.id,
                group_id:   group.id,
                macro_id:   macro.id
              ).one?
            )
          end
        end

        describe 'and an owner ID is not present' do
          let(:owner_id)        { nil }
          let(:group_owner_ids) { nil }

          before { macro.valid? }

          it 'is not valid' do
            refute macro.valid?
          end

          it 'adds an error' do
            assert_equal ['cannot be blank'], macro.errors.messages[:owner_id]
          end
        end
      end
    end

    describe 'and the macro has an owner' do
      let(:macro) do
        macro_with_actions('comment_value' => 'Some comment').tap do |macro|
          macro.owner = account

          macro.title = 'New macro'
        end
      end

      before { macro.save! }

      it 'is valid' do
        assert macro.valid?
      end

      it 'sets the owner ID' do
        assert_equal account.id, macro.owner_id
      end
    end

    describe 'and the macro contains a single rich comment action' do
      let(:macro) do
        macro_with_actions('comment_value_html' => '<p>So rich</p>').tap do |macro|
          macro.owner = account

          macro.title = 'New macro'
        end
      end

      it 'is valid' do
        assert macro.valid?
      end
    end

    describe 'and the macro contains a single basic comment action' do
      let(:macro) do
        macro_with_actions('comment_value' => 'Some comment').tap do |macro|
          macro.owner = account

          macro.title = 'New macro'
        end
      end

      it 'is valid' do
        assert macro.valid?
      end
    end

    describe 'and the macro contains multiple comment action types' do
      let(:macro) do
        macro_with_actions(
          'comment_value'      => 'Some comment',
          'comment_value_html' => '<p>So rich</p>'
        ).tap do |macro|
          macro.owner = account

          macro.title = 'New macro'
        end
      end

      it 'is valid' do
        assert macro.valid?
      end
    end

    describe 'and the macro contains multiple rich content actions' do
      let(:definition) do
        Definition.new.tap do |definition|
          definition.actions.push(
            DefinitionItem.new('comment_value_html', 'is', '<p>Comment</p>'),
            DefinitionItem.new('comment_value_html', 'is', '<p>No comment</p>')
          )
        end
      end

      let(:macro) do
        Macro.new(
          title:      'Macro X',
          account:    account,
          owner:      account,
          definition: definition
        )
      end

      before { macro.valid? }

      it 'is not valid' do
        refute macro.valid?
      end

      it 'adds an error' do
        assert_equal(
          'Invalid comment action. There can be only one comment action.',
          macro.errors.messages[:base].first
        )
      end
    end

    describe 'and the macro has multiple basic comment actions' do
      let(:definition) do
        Definition.new.tap do |definition|
          definition.actions.push(
            DefinitionItem.new('comment_value', 'is', 'Some comment'),
            DefinitionItem.new('comment_value', 'is', 'Another comment')
          )
        end
      end

      let(:macro) do
        Macro.new(
          title:      'Macro X',
          account:    account,
          owner:      account,
          definition: definition
        )
      end

      describe 'and it is being created' do
        before { macro.valid? }

        it 'is not valid' do
          refute macro.valid?
        end

        it 'adds an error' do
          assert_equal(
            'Invalid comment action. There can be only one comment action.',
            macro.errors.messages[:base].first
          )
        end
      end

      describe 'and it is being updated' do
        before { macro.save!(validate: false) }

        it 'is valid' do
          assert macro.valid?
        end

        describe 'and adding another basic comment' do
          before do
            macro.definition = begin
              Definition.new.tap do |definition|
                definition.actions.push(
                  DefinitionItem.new('comment_value', 'is', 'Some comment'),
                  DefinitionItem.new('comment_value', 'is', 'Another comment'),
                  DefinitionItem.new('comment_value', 'is', 'Third comment')
                )
              end
            end

            macro.valid?
          end

          it 'is not valid' do
            refute macro.valid?
          end

          it 'adds an error' do
            assert_equal(
              'Invalid comment action. There can be only one comment action.',
              macro.errors.messages[:base].first
            )
          end
        end
      end
    end
  end

  describe ".in_group" do
    let(:group_1) { groups(:minimum_group) }
    let(:group_2) { groups(:support_group) }
    let(:group_3) { groups(:billing_group) }

    let!(:macro_1) do
      macro_with_actions('comment_value' => 'Some comment').tap do |macro|
        macro.title = 'Macro 1'

        macro.owner_type = 'Group'
        macro.owner_id   = group_1.id
      end.tap(&:save!)
    end

    let!(:macro_2) do
      macro_with_actions('comment_value' => 'Some comment').tap do |macro|
        macro.title = 'Macro 2'

        macro.owner_type = 'Group'
        macro.owner_id   = group_2.id
      end.tap(&:save!)
    end

    let!(:macro_3) do
      macro_with_actions('comment_value' => 'Some comment').tap do |macro|
        macro.title = 'Macro 3'

        macro.owner_type = 'Group'
        macro.owner_id   = group_1.id

        macro.group_owner_ids = [group_1, group_3].map(&:id)
      end.tap(&:save!)
    end

    it "returns rules in the specified groups" do
      assert_equal(
        [macro_1, macro_3],
        Macro.where(account_id: account.id).in_group([group_1, group_3].map(&:id))
      )
    end
  end

  describe ".categories" do
    let(:definition) do
      Definition.new.tap do |defn|
        defn.actions.push(DefinitionItem.new("set_tags", nil, ["test"]))
      end
    end

    before do
      account.macros.create!(
        [
          { title: "Beta",                 definition: definition },
          { title: "Beta::One",            definition: definition },
          { title: "Beta::Two",            definition: definition },
          { title: "Beta::Epsilon::Three", definition: definition },
          { title: "Theta::Iota::Four",    definition: definition },
          { title: "Zeta::Five",           definition: definition },
          { title: "Alpha::Six Seven",     definition: definition },
          { title: "Gamma Delta::Eight",   definition: definition },
          { title: "Nine",                 definition: definition },
          { title: "Eta::",                definition: definition },
          { title: "::Ten",                definition: definition }
        ]
      )
    end

    it "returns a list of all macro categories" do
      assert_equal(
        [
          "",
          "Alpha",
          "Beta",
          "Eta",
          "Gamma Delta",
          "Theta",
          "Zeta"
        ],
        account.macros.categories
      )
    end
  end

  describe '.update_position' do
    let(:reversed_macros) { account.macros.order('position').reverse_order }

    let(:definition) do
      Definition.new.tap do |definition|
        definition.actions.push(
          DefinitionItem.new('set_tags', nil, ['test'])
        )
      end
    end

    before do
      Rule.destroy_all

      account.macros.create(
        account:    account,
        title:      'Rule 1',
        definition: definition,
        position:   3
      )

      Timecop.travel(2.minutes.ago) do
        account.macros.create(
          account:    account,
          title:      'Rule 2',
          definition: definition,
          position:   4
        )
      end
    end

    describe 'when `ids` is an array of strings' do
      let!(:reversed_ids)         { reversed_macros.map(&:id) }
      let!(:original_updated_ats) { reversed_macros.map(&:updated_at) }

      let!(:old_macros_key) { account.scoped_cache_key(:macros) }
      let!(:old_rules_key)  { account.scoped_cache_key(:rules) }

      before do
        @sql_queries = sql_queries do
          account.macros.update_position(reversed_ids.map(&:to_s))
        end
      end

      before_should 'expire the radar cache' do
        expiry_for(account).expects(:expire_radar_cache_key).with(:macros)
      end

      it 'updates positions based on the provided ids' do
        assert_equal reversed_ids, account.macros.order('position').pluck(:id)
      end

      it 'does not change the `updated_at` timestamps' do
        assert_equal(
          original_updated_ats,
          account.macros.order('position').pluck(:updated_at)
        )
      end

      it 'does not generate N+1 queries' do
        assert_sql_queries 1, /UPDATE `rules`/
      end

      it 'expires the macros cache key' do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal old_macros_key, account.scoped_cache_key(:macros)
      end

      it 'expires the rules cache key' do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal old_rules_key, account.scoped_cache_key(:rules)
      end
    end

    describe 'when `ids` is an array of integers' do
      let!(:reversed_ids)         { reversed_macros.map(&:id) }
      let!(:original_updated_ats) { reversed_macros.map(&:updated_at) }

      let!(:old_macros_key) { account.scoped_cache_key(:macros) }
      let!(:old_rules_key)  { account.scoped_cache_key(:rules) }

      before do
        @sql_queries = sql_queries do
          account.macros.update_position(reversed_ids)
        end
      end

      before_should 'expire the radar cache' do
        expiry_for(account).expects(:expire_radar_cache_key).with(:macros)
      end

      it 'updates positions based on the provided ids' do
        assert_equal reversed_ids, account.macros.order('position').pluck(:id)
      end

      it 'does not change the `updated_at` timestamps' do
        assert_equal(
          original_updated_ats,
          account.macros.order('position').pluck(:updated_at)
        )
      end

      it 'does not generate N+1 queries' do
        assert_sql_queries 1, /UPDATE `rules`/
      end

      it 'expires the macros cache key' do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal old_macros_key, account.scoped_cache_key(:macros)
      end

      it 'expires the rules cache key' do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal old_rules_key, account.scoped_cache_key(:rules)
      end
    end
  end

  describe '.find_for_user' do
    describe 'when the macro belongs to multiple groups' do
      let(:user) { users(:minimum_agent) }

      let(:group_1) { groups(:minimum_group) }
      let(:group_2) { groups(:support_group) }

      let(:macro) do
        macro_with_actions('comment_value' => 'Some comment').tap do |macro|
          macro.title = 'New macro'

          macro.owner_type = 'Group'
          macro.owner_id   = group_1.id

          macro.group_owner_ids = [group_1, group_2].map(&:id)
        end.tap(&:save!)
      end

      describe 'and the user is in one of the groups' do
        it 'returns the macro' do
          assert_equal macro, Macro.find_for_user(user, macro.id)
        end
      end

      describe 'and the user is not in one of the groups' do
        before { user.memberships.destroy_all }

        it 'does not return the macro' do
          assert_nil Macro.find_for_user(user, macro.id)
        end
      end
    end
  end

  describe "Availability Type" do
    it "returns proper availability type" do
      assert_equal "personal", rules(:macro_for_minimum_agent).availability_type
      assert_equal "group", rules(:macro_for_minimum_group).availability_type
      assert_equal "everyone", rules(:macro_incident_escalation).availability_type
    end
  end

  [
    ["a macro that sets assignee", { 'assignee_id#all_agents' => 'current_user' }],
    ["a macro that sets the group", { 'group_id' => 'current_groups' }]
  ].each do |ctx, macro_definition|
    describe ctx do
      before do
        @user = users(:with_groups_agent2)
        @group = groups(:with_groups_group2)
        @ticket = tickets(:with_groups_1)
        @macro = macro_with_actions(macro_definition)
      end

      describe "when applied to a ticket with no group" do
        before do
          @ticket.group = nil
        end

        describe "and user has default group which is different from account's default group" do
          before do
            @user.account.groups.first.update_attributes!(default: false)
            @result = @macro.apply(@user, @ticket)
          end

          it "sets the group_id to the current user's default group" do
            assert_equal @user.default_group_id, @result['group_id']
          end
        end

        describe "and user has no default group" do
          before do
            @user.memberships.where(default: true).first.update_attributes!(default: false)
            @result = @macro.apply(@user, @ticket)
          end

          it "sets the group_id to the current user's first group" do
            assert_equal @user.groups.first.id, @result['group_id']
          end
        end
      end

      describe "when applied to a ticket with a group" do
        before do
          @ticket.group = @group
        end

        describe "to which the current user belongs" do
          before do
            assert @user.in_group?(@group)
            @result = @macro.apply(@user, @ticket)
          end

          it "returns that group_id" do
            assert_equal @group.id, @result['group_id']
          end
        end

        describe "to which the current user does not belong" do
          before do
            @user.groups.delete(@group)
            refute @user.in_group?(@group)
            @result = @macro.apply(@user, @ticket)
          end

          it "sets the group_id to the current user's default group if exists" do
            @user.memberships.first.update_attributes!(default: true)
            assert @user.default_group_id
            assert_equal @user.default_group_id, @result['group_id']
          end

          it "sets the group_id to the current user's first group if there is no default group" do
            assert_nil @user.default_group_id
            assert_equal @user.groups.first.id, @result['group_id']
          end
        end
      end

      describe 'when applied in the bulk context (to a nil ticket)' do
        before do
          @user.memberships.where(default: true).first.update_attributes!(default: false)
          @result = @macro.apply(@user, nil)
        end

        it "sets the group_id to the current user's first group" do
          assert_equal @user.groups.first.id, @result['group_id']
        end
      end

      describe 'when the account does not have the groups feature' do
        before do
          account.stubs(has_groups?: false)

          account.groups.first.update_attributes!(default: true)
        end

        it 'sets the `group_id` to the account default group' do
          assert_equal(
            account.default_group.id,
            @macro.apply(@user, @ticket)['group_id']
          )
        end
      end
    end
  end

  describe 'a macro that sets a comment body not stored as an array' do
    let(:comment) { "my test comment" }

    before do
      definition = Definition.new
      definition.actions.push(DefinitionItem.new('comment_value', nil, comment))
      @macro = Macro.new(definition: definition)
      @macro.account = account
      @result = @macro.apply(@user, @ticket)
    end

    it 'returns the specified comment' do
      assert_equal comment, @result['comment']['value']
    end

    it 'returns the specified comment scoped by channel' do
      assert_equal [['channel:all', comment]], @result['comment']['scoped_value']
    end
  end

  describe 'a macro that sets a comment body as an array of channel and comment' do
    let(:comment) { "my test comment" }
    let(:channel) { "channel:test" }
    let(:comment_with_scope) { [channel, comment] }

    before do
      definition = Definition.new
      definition.actions.push(DefinitionItem.new('comment_value', nil, comment_with_scope))
      @macro = Macro.new(definition: definition)
      @macro.account = account
      @result = @macro.apply(@user, @ticket)
    end

    it 'returns the specified comment' do
      assert_equal comment, @result['comment']['value']
    end

    it 'returns the specified comment scoped by channel' do
      assert_equal [comment_with_scope], @result['comment']['scoped_value']
    end
  end

  describe 'a macro with multiple comment actions' do
    let(:comment) { "my test comment" }
    let(:channel) { "channel:test" }
    let(:comment_with_scope) { [channel, comment] }

    before do
      definition = Definition.new
      definition.actions.push(DefinitionItem.new('comment_value', nil, comment_with_scope))
      definition.actions.push(DefinitionItem.new('comment_value', nil, comment))
      definition.actions.push(DefinitionItem.new('comment_value', nil, comment_with_scope))
      @user = users(:with_groups_agent2)
      @macro = Macro.new(definition: definition)
      @macro.account = account
      @result = @macro.apply(@user, @ticket)
    end

    it 'returns the specified comments concatenated' do
      assert_equal comment * 3, @result['comment']['value']
    end

    it 'returns the specified comments scoped by channel' do
      assert_equal [comment_with_scope, ['channel:all', comment], comment_with_scope], @result['comment']['scoped_value']
    end
  end

  describe "a macro with whitespace" do
    before do
      @account = accounts(:minimum)
      @actions = [DefinitionItem.new("comment_value", "is", "Hi\r\n\r\n\r\n\r\n\r\n\r\n")]
      @definition = Definition.new([], [], @actions)
      @macro = Macro.new(title: "Hello from space", account: @account, owner: @account, definition: @definition)
      @macro.save!
      @macro.reload
    end
    it "retains its whitespace" do
      assert_equal @macro.definition.actions, @actions
    end
  end

  describe 'a macro that sets assignee to current_user and sets group_id' do
    before do
      @user = users(:with_groups_agent2)
      @group = groups(:with_groups_group2)
      @ticket = tickets(:with_groups_1)
      @macro = macro_with_actions('assignee_id' => 'current_user', 'group_id' => @group.id)
      assert @user.in_group?(@group)
      @ticket.group = nil
      @result = @macro.apply(@user, @ticket)
    end

    it 'uses the specified group_id' do
      assert_equal @group.id, @result['group_id']
    end
  end

  describe 'a macro that unsets assignee' do
    before do
      @account = accounts(:with_groups)
      @user = users(:with_groups_agent2)
      @group = groups(:with_groups_group1)
      @ticket = tickets(:with_groups_1)
      @macro = macro_with_actions('assignee_id' => '')
      @macro.account = @account

      Zendesk::Rules::MacroApplication.any_instance.stubs(:ticket_has_group?).returns(true)
      Zendesk::Rules::MacroApplication.any_instance.stubs(:ticket).returns(@ticket)
    end

    it "sets group_id to nil when change_assignee_to_general group setting is disabled" do
      @account.settings.stubs(:change_assignee_to_general_group?).returns(false)
      @result = @macro.apply(@user)

      assert_equal({"assignee_id" => nil, "group_id" => nil}, @result)
    end

    it "retains group_id when change_assignee_to_general group setting is enabled" do
      @account.settings.stubs(:change_assignee_to_general_group?).returns(true)
      @result = @macro.apply(@user)

      assert_equal({"assignee_id" => nil, "group_id" => @group.id}, @result)
    end
  end

  describe 'a macro that sets assignee to requester_id' do
    before do
      @user = users(:with_groups_agent2)
      @requester = @user
      @ticket = tickets(:with_groups_1)
      @ticket.requester = @requester
      @macro = macro_with_actions('assignee_id' => 'requester_id')
      @result = @macro.apply(@user, @ticket)
    end

    it 'finds the correct requester_id' do
      assert_equal @requester.id, @result['assignee_id']
    end

    describe 'when the ticket has not yet been created' do
      it 'does not blow up' do
        @macro.apply(@user, nil)
      end
    end
  end

  describe 'a macro that sets ticket form to default_ticket_form' do
    before do
      @ticket = tickets(:with_groups_1)
      @user = @ticket.assignee
      @account = @ticket.account

      params = {
        ticket_form: {
          name: "default wombat",
          display_name: "Default wombat",
          position: 1,
          default: true,
          active: true,
          end_user_visible: false
        }
      }
      @default_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
      @default_form.save!

      params = {
        ticket_form: {
          name: "non default wombat",
          display_name: "Non default wombat",
          position: 2,
          default: false,
          active: true,
          end_user_visible: false
        }
      }
      @non_default_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
      @non_default_form.save!

      @account.reload
      @ticket.ticket_form = @non_default_form
      @macro = macro_with_actions('ticket_form_id' => 'default_ticket_form')
      @result = @macro.apply(@user, @ticket)
    end

    it 'finds the correct ticket form ID' do
      assert_equal @default_form.id, @result['ticket_form_id']
    end

    it 'does not blow up when ticket is nil' do
      @ticket = nil
      @result = @macro.apply(@user, @ticket)
      assert_equal @default_form.id, @result['ticket_form_id']
    end
  end

  describe 'a macro with attachments' do
    let(:account) { accounts(:minimum) }
    let(:user)    { users(:minimum_agent) }

    let(:actions) do
      [DefinitionItem.new('comment_value_html', nil, 'Nice tie!')]
    end

    let(:macro) do
      create_macro(
        title: 'Some::Macro',
        owner: user,
        definition: Definition.new.tap do |definition|
          definition.actions.push(*actions)
        end
      )
    end

    let(:attachments) do
      Array.new(2) do
        RuleAttachment.create! do |attachment|
          attachment.account_id   = account.id
          attachment.user_id      = user.id
          attachment.size         = 1
          attachment.content_type = 'application/unknown'
          attachment.filename     = 'hello_world'
        end
      end
    end

    before do
      attachments.each { |attachment| attachment.associate(macro) }
    end

    it 'returns attachments' do
      assert_equal attachments, macro.attachments
    end

    it 'updates updated_at when attachment is added' do
      initial_updated_at = macro.updated_at

      Timecop.travel(Time.now + 1.seconds)

      new_attachment = RuleAttachment.create! do |attachment|
        attachment.account_id = account.id
        attachment.user_id = user.id
        attachment.size = 1
        attachment.content_type = 'application/unknown'
        attachment.filename = 'funky_file'
      end

      new_attachment.associate(macro)

      macro.reload

      assert_not_equal(initial_updated_at, macro.updated_at)
    end
  end

  it "sets assignee_id to nil when unsetting assignee_id" do
    user = users(:with_groups_agent2)
    result = macro_with_actions('assignee_id' => '').apply(user)
    assert_equal({"assignee_id" => nil, "group_id" => nil}, result)
  end

  [["Applying Macros to a ticket", :ticket], ["Applying Macros to tickets in bulk", :bulk]].each do |ctx, mode|
    describe ctx do
      before do
        @user = users(:minimum_agent)
        if mode == :ticket
          @ticket = tickets(:minimum_1)
        end
      end

      it "sets simple properties" do
        macro_result = macro_with_actions('priority_id' => PriorityType.HIGH).apply(@user, @ticket)
        assert_equal PriorityType.HIGH, macro_result['priority_id']

        macro_result = macro_with_actions('status_id' => StatusType.Pending).apply(@user, @ticket)
        assert_equal StatusType.Pending, macro_result['status_id']
      end

      it "sets a subject" do
        if @ticket
          @ticket.subject = "foo bar"
        end

        macro_result = macro_with_actions('subject' => 'Pretty good. Prettaaaaaay prettaaaay pretty good.').apply(@user, @ticket)
        assert_equal "Pretty good. Prettaaaaaay prettaaaay pretty good.", macro_result['subject']
      end

      it "does not have any #all_agents in the keys" do
        macro_result = macro_with_actions('assignee_id#all_agents' => users(:minimum_agent).id).apply(@user, @ticket)
        assert_equal 'assignee_id', macro_result.keys.first
      end

      it "interprets current_user and current_group placeholders" do
        macro_result = macro_with_actions('assignee_id' => 'current_user').apply(@user, @ticket)
        assert_equal @user.id, macro_result['assignee_id']

        macro_result = macro_with_actions('group_id' => 'current_groups').apply(@user, @ticket)
        assert_equal @user.groups.first.id, macro_result['group_id']
      end

      it "sets tags" do
        macro_result = macro_with_actions('current_tags' => 'foo bar').apply(@user, @ticket)
        if @ticket
          assert_equal 'hello foo bar', macro_result['current_tags']
        else
          assert_equal 'foo bar', macro_result['current_tags']
        end

        macro_result = macro_with_actions('set_tags' => 'foo bar').apply(@user, @ticket)
        assert_equal 'foo bar', macro_result['current_tags']

        macro_result = macro_with_actions('remove_tags' => 'hello').apply(@user, @ticket)
        if @ticket
          assert_equal 'foo bar', macro_result['current_tags']
        else
          assert_equal '', macro_result['current_tags']
        end
        if @ticket.nil? # bulk update
          assert_equal 'hello', macro_result['remove_tags']
        end

        macro_result = macro_with_actions('set_tags' => 'hello world', 'remove_tags' => 'hello').apply(@user, @ticket)
        assert_equal 'world', macro_result['current_tags']
      end

      it "does not set tags if user cannot edit tags" do
        if @ticket # bulk update disregards permissions for the preview
          @user.expects(:can?).with(:edit_tags, Ticket).returns(false).times(4)
          @user.expects(:can?).with(:edit_tags, @ticket).returns(false).times(4)

          macro_result = macro_with_actions('set_tags' => 'foo bar').apply(@user, @ticket)
          assert_nil macro_result['current_tags']

          macro_result = macro_with_actions('remove_tags' => 'hello').apply(@user, @ticket)
          assert_nil macro_result['current_tags']

          macro_result = macro_with_actions('set_tags' => 'hello world', 'remove_tags' => 'hello').apply(@user, @ticket)
          assert_nil macro_result['current_tags']
        end
      end

      it "normalizes tags" do
        macro_result = macro_with_actions('current_tags' => 'HELP').apply(@user, @ticket)
        if @ticket
          assert_equal 'hello help', macro_result['current_tags']
        else
          assert_equal 'help', macro_result['current_tags']
        end

        macro_result = macro_with_actions('remove_tags' => 'HELP').apply(@user, @ticket)
        if @ticket
          assert_equal 'hello', macro_result['current_tags']
        else
          assert_equal '', macro_result['current_tags']
        end

        macro_result = macro_with_actions('set_tags' => 'abc@ d%e>f #').apply(@user, @ticket)
        assert_equal 'abc def ', macro_result['current_tags']
      end

      it "sets comment visibility" do
        macro_result = macro_with_actions('comment_mode_is_public' => 'true').apply(@user, @ticket)
        assert_equal 'true', macro_result['comment']['is_public']
      end

      it "interprets liquid templates" do
        template = <<~LIQUID
          [{{ticket.account}}] {{ticket.requester.name}},

          Your request has been escalated to incident.

          Cheers,
          {{current_user.name}}, {{ticket.assignee.name}}

          --------------------
          View status of your ticket at https://{{ticket.url}}
        LIQUID

        evaluated = if @ticket
          <<~EVALUATED_LIQUID
            [Minimum account] Author Minimum,

            Your request has been escalated to incident.

            Cheers,
            Agent Minimum, Agent Minimum

            --------------------
            View status of your ticket at #{@ticket.url(for_agent: true)}
          EVALUATED_LIQUID
        else
          template
        end

        macro = macro_with_actions('assignee_id' => users(:minimum_agent).id, 'ticket_type_id' => TicketType.Incident, 'comment_value' => template)

        macro_result = macro.apply(@user, @ticket)

        assert_equal TicketType.Incident, macro_result['ticket_type_id']
        assert_equal evaluated, macro_result['comment']['value']
      end

      describe "interpret liquid templates in the requester language" do
        before do
          @spanish = translation_locales(:spanish)
          @user = users(:minimum_admin)
          @user.translation_locale = @spanish
          @ticket = tickets(:minimum_1)
          @ticket.requester = @user
        end

        it "resolves in spanish" do
          template = "Template con ayuda liquida, este un ticket de tipo {{ticket.ticket_type}}"
          macro = macro_with_actions('assignee_id' => users(:minimum_agent).id, 'ticket_type_id' => TicketType.Incident, 'comment_value' => template)
          macro_result = macro.apply(@user, @ticket)

          assert_equal "Template con ayuda liquida, este un ticket de tipo Incidente", macro_result['comment']['value']
        end
      end

      it "handles multiple comments" do
        macro = macro_with_actions('assignee_id' => users(:minimum_agent).id, 'ticket_type_id' => TicketType.Incident, 'comment_mode_is_public' => true, 'comment_value' => "Hello")
        macro.actions.push(DefinitionItem.new('comment_value', nil, ['World']))
        macro_result = macro.apply(@user, @ticket)

        assert_equal 'HelloWorld', macro_result['comment']['value']
      end

      it "handles multiple tags" do
        macro = macro_with_actions('current_tags' => 'baz xyzzy')

        if @ticket
          @ticket.current_tags = "foo bar"
        end

        macro_result = macro.apply(@user, @ticket)

        if @ticket
          assert_equal "foo bar baz xyzzy", macro_result['current_tags']
        else
          assert_equal "baz xyzzy", macro_result['current_tags']
        end
      end

      describe "handles cc_agent: " do
        before do
          @ticket = tickets(:minimum_1)
          macro_1 = macro_with_actions('cc' => 'current_user')
          macro_1.user = @user
          macro_1.account = accounts(:minimum)
          @macro_result_1 = macro_1.apply(@user, @ticket)

          macro_2 = macro_with_actions('cc' => @user.id)
          macro_2.user = @user
          macro_2.account = accounts(:minimum)
          @macro_result_2 = macro_2.apply(@user, @ticket)

          @ticket.collaborations.build(user: @user)
          @macro_result_3 = macro_2.apply(@user, @ticket)
        end

        it "current_user" do
          assert_equal @macro_result_1["collaborator_list"].first[:id], @user.id
          assert_equal @macro_result_1["collaborator_list"].first[:cced], false
          assert_equal @macro_result_1["collaborator_list"].first[:name], @user.name
        end

        it "a not-current-user agent" do
          assert_equal @macro_result_2["collaborator_list"].first[:id], @user.id
          assert_equal @macro_result_2["collaborator_list"].first[:cced], false
          assert_equal @macro_result_2["collaborator_list"].first[:name], @user.name
        end

        it "ignores duplicate cc_agent action" do
          assert_equal @macro_result_3["collaborator_list"].first[:cced], true
        end
      end

      it "handles bogus source fields" do
        macro = macro_with_actions('comment_value' => "Foo", 'priority_id' => PriorityType.HIGH, 'PICKACTION' => "foo")
        macro.apply(@user, @ticket)
      end

      it "handles custom fields" do
        tagger_id = ticket_fields(:field_tagger_custom).id
        option_id = custom_field_options(:field_tagger_custom_option_1).id

        macro = macro_with_actions("ticket_fields_#{tagger_id}" => option_id)
        macro.account = accounts(:minimum)

        macro_result = macro.apply(@user, @ticket)
        assert_equal custom_field_options(:field_tagger_custom_option_1).value, macro_result["ticket_fields_#{tagger_id}"]

        macro = macro_with_actions("ticket_fields_#{tagger_id}" => "")
        macro.account = accounts(:minimum)

        macro_result = macro.apply(@user, @ticket)
        assert_equal "", macro_result["ticket_fields_#{tagger_id}"]

        checkbox_id = ticket_fields(:field_checkbox_custom).id

        macro = macro_with_actions("ticket_fields_#{checkbox_id}" => "true")
        macro.account = accounts(:minimum)

        macro_result = macro.apply(@user, @ticket)
        assert macro_result["ticket_fields_#{checkbox_id}"]

        macro = macro_with_actions("ticket_fields_#{checkbox_id}" => "false")
        macro.account = accounts(:minimum)

        macro_result = macro.apply(@user, @ticket)
        refute macro_result["ticket_fields_#{checkbox_id}"]
      end

      it "handles interpolating custom fields into comments" do
        tagger_id = ticket_fields(:field_tagger_custom).id
        option_id = custom_field_options(:field_tagger_custom_option_1).id
        count = TicketFieldEntry.count(:all)

        macro = macro_with_actions(
          "ticket_fields_#{tagger_id}" => option_id,
          "comment_value" => "Custom: {{ticket.ticket_field_#{tagger_id}}}"
        )

        macro.account = accounts(:minimum)
        macro_result = macro.apply(@user, @ticket)
        assert_equal custom_field_options(:field_tagger_custom_option_1).value, macro_result["ticket_fields_#{tagger_id}"]
        if @ticket
          assert_equal "Custom: #{custom_field_options(:field_tagger_custom_option_1).value}", macro_result['comment']['value']
        else
          assert_equal "Custom: {{ticket.ticket_field_#{tagger_id}}}", macro_result['comment']['value']
        end
        assert_equal count, TicketFieldEntry.count(:all)
      end
    end
  end

  describe "#category" do
    let(:definition) do
      Definition.new.tap do |definition|
        definition.actions.push(DefinitionItem.new("set_tags", nil, ["test"]))
      end
    end

    let(:macro) do
      accounts(:minimum).macros.create!(title: title, definition: definition)
    end

    describe "when macro does not have a category delimiter" do
      let(:title) { "No Delimiter" }

      it "returns title as category" do
        assert_equal "No Delimiter", macro.category
      end
    end

    describe "when macro has a single category delimiter" do
      describe "and characters are only after delimiter" do
        let(:title) { "::Category" }

        it "returns empty string as category" do
          assert_equal "", macro.category
        end
      end

      describe "and characters are only before delimiter" do
        let(:title) { "There Can Only B1::" }

        it "returns category" do
          assert_equal "There Can Only B1", macro.category
        end
      end

      describe "and characters are before and after delimiter" do
        let(:title) { "There Can Only B1::Category" }

        it "returns category" do
          assert_equal "There Can Only B1", macro.category
        end
      end
    end

    describe "when macro has multiple category delimiters" do
      let(:title) { "More Categories::More Problems::Yo" }

      it "returns top level category" do
        assert_equal "More Categories", macro.category
      end
    end
  end

  describe "#position_after" do
    before do
      @account = accounts(:minimum)
      @first = @account.all_macros.active.first
      @second = @account.all_macros.active.last
      @original_positions = @account.all_macros.active.ordered.pluck(:title)
      @original_position = @second.position
    end

    describe "when macro with given id is active" do
      before do
        @second.position_after(@first.id)
      end

      it "updates position to be directly after macro with given id" do
        assert_equal @first.reload.position.succ, @second.reload.position
      end

      it "bumps up position of macros with same position or greater" do
        new_positions = @account.all_macros.active.ordered.pluck(:title)

        refute_equal @original_positions, new_positions
        assert_equal(
          [
            "Incident escalation",
            "Incident escalation for minimum group",
            "Incident escalation for minimum agent",
            "Active Macro for Minimum Group",
            "Incident escalation for minimum admin"
          ],
          new_positions
        )
      end
    end

    describe "when macro with given id is inactive" do
      before do
        @first.update_attribute(:is_active, false)
        @original_positions.delete("Incident escalation")

        @second.position_after(@first.id)
      end

      it "does not update position" do
        assert_equal @original_position, @second.reload.position
      end

      it "does not update position of other macros" do
        assert_equal(
          @original_positions,
          @account.all_macros.active.ordered.pluck(:title)
        )
      end
    end

    describe "when macro with given id is deleted" do
      before do
        @first.soft_delete!
        @original_positions.delete("Incident escalation")

        @second.position_after(@first.id)
      end

      it "does not update position" do
        assert_equal @original_position, @second.reload.position
      end

      it "does not update position of other macros" do
        assert_equal(
          @original_positions,
          @account.all_macros.active.ordered.pluck(:title)
        )
      end
    end

    describe "when macro with given id belongs to another account" do
      let(:unowned_macro) { rules(:active_macro_for_with_groups_group2) }

      before { @second.position_after(unowned_macro.id) }

      it "does not update position" do
        assert_equal @original_position, @second.reload.position
      end

      it "does not update position of other macros" do
        assert_equal(
          @original_positions,
          @account.all_macros.active.ordered.pluck(:title)
        )
      end
    end
  end

  describe "handle custom user/org fields" do
    [
      ["user1", :minimum_end_user],
      ["user2", :minimum_end_user2]
    ].each do |ctx, user_fixture|
      describe ctx do
        let(:user)    { users(user_fixture) }
        let(:account) { user.account }

        let(:user_fields) { user.custom_field_values.as_json(account: account) }

        let(:organization_fields) do
          user.organization.custom_field_values.as_json(account: account)
        end

        let(:ticket) do
          Ticket.new(
            requester:   user,
            account:     account,
            subject:     'ticket subject',
            description: 'testing ticket',
            status_id:   1
          )
        end

        let(:macro) do
          macro_with_actions(
            'comment_value' => "#{user.name} -- User.Text1: " \
              "{{ticket.requester.custom_fields.text1}}, " \
              "User.Date1: {{ticket.requester.custom_fields.date1}}, " \
              "User.Integer1: {{ticket.requester.custom_fields.integer1}}, " \
              "User.Checkbox1: {{ticket.requester.custom_fields.checkbox1}}, " \
              "User.Dropdown1: {{ticket.requester.custom_fields.dropdown1}}, " \
              "User.Decimal1: {{ticket.requester.custom_fields.decimal1}}, " \
              "Org.Decimal1: {{ticket.requester.organization.custom_fields" \
              ".decimal1}}"
          ).tap do |macro|
            macro.account = account
          end
        end

        let (:macro_result) { macro.apply(user, ticket) }

        before do
          ticket.will_be_saved_by(user)

          ticket.save!
        end

        it "handles liquid templates" do
          assert_equal(
            "#{user.name} -- User.Text1: #{user_fields['text1']}, " \
              "User.Date1: #{user_fields['date1']}, " \
              "User.Integer1: #{user_fields['integer1']}, " \
              "User.Checkbox1: #{user_fields['checkbox1']}, " \
              "User.Dropdown1: #{user_fields['dropdown1']}, " \
              "User.Decimal1: #{user_fields['decimal1']}, " \
              "Org.Decimal1: #{organization_fields['decimal1']}",
            macro_result['comment']['value']
          )
        end
      end
    end
  end

  private

  def expiry_for(account)
    mock('Zendesk::RadarExpiry').tap do |expiry|
      Zendesk::RadarExpiry.stubs(:get).with(account).returns(expiry)
    end
  end

  def macro_with_actions(actions = {})
    definition = Definition.new
    actions.each do |attribute, value|
      definition.actions.push(DefinitionItem.new(attribute, nil, [value]))
    end
    Macro.new(account: account, definition: definition)
  end
end
