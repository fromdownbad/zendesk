require_relative '../../support/test_helper'

SingleCov.covered!

describe NullRevision do
  describe '.nice_id' do
    it 'returns `nil`' do
      assert_nil NullRevision.nice_id
    end
  end

  describe '.snapshot' do
    describe '#title' do
      it 'returns `nil`' do
        assert_nil NullRevision.snapshot.title
      end
    end

    describe '#is_active' do
      it 'returns `nil`' do
        assert_nil NullRevision.snapshot.is_active
      end
    end

    describe '#description' do
      it 'returns `nil`' do
        assert_nil NullRevision.snapshot.description
      end
    end

    describe '#definition' do
      it 'returns an empty `Definition` instance' do
        assert_equal(
          Definition.build(
            conditions_all: [],
            conditions_any: [],
            actions:        []
          ),
          NullRevision.snapshot.definition
        )
      end
    end
  end
end
