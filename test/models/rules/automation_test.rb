require_relative '../../support/test_helper'
require_relative '../../support/business_hours_test_helper'
require_relative '../../support/cms_references_helper'
require_relative '../../support/rule'

SingleCov.covered! uncovered: 21

describe Automation do
  include TestSupport::Rule::Helper

  extend CmsReferencesHelper

  fixtures :all

  let(:automation) { rules(:automation_request_csr) }

  creates_cms_references(
    type:    :automation,
    actions: %w[notification_user notification_group notification_target]
  )

  describe_with_arturo_enabled :tweeet_requester_action do
    before { account.stubs(created_at: Time.new(2016).utc) }

    creates_cms_references(type: :automation, actions: %w[tweet_requester])
  end

  it 'prevents collection modification' do
    assert(
      Automation.
        included_modules.
        include?(Zendesk::PreventCollectionModificationMixin)
    )
  end

  describe '#validate_nullifying_actions' do
    describe 'when the `group_id` is changed' do
      let(:group_1) { accounts(:minimum).groups.create(name: 'from') }
      let(:group_2) { accounts(:minimum).groups.create(name: 'to') }

      let(:automation) { rules(:automation_group_to_different_group) }

      before do
        automation.tap do |automation|
          automation.definition.conditions_all.first.value = [group_1.id]
          automation.definition.actions.first.value        = [group_2.id]
        end
      end

      it 'is valid' do
        assert_valid automation
      end
    end

    describe 'when adding a tag to the `not_includes` condition' do
      before do
        automation.definition.actions.replace(
          [DefinitionItem.new('current_tags', 'is', 'shoop')]
        )
        automation.definition.conditions_all.replace(
          [
            DefinitionItem.new('current_tags', 'not_includes', 'shoop woop'),
            DefinitionItem.new('status_id', 'less_than', 4),
          ]
        )
      end

      it 'is valid' do
        assert_valid automation
      end
    end

    describe 'when the source is `until_due_date`' do
      before do
        automation.definition.actions.replace(
          [DefinitionItem.new('current_tags', 'is', 'shoop')]
        )
        automation.definition.conditions_all.replace(
          [
            DefinitionItem.new('until_due_date', 'is', '20'),
            DefinitionItem.new('status_id', 'less_than', 4),
          ]
        )
      end

      it 'is valid' do
        assert_valid automation
      end
    end

    describe 'when the source is `sla_next_breach_at`' do
      before do
        automation.account.stubs(has_service_level_agreements?: true)

        automation.definition.actions.replace(
          [DefinitionItem.new('current_tags', 'is', 'shoop')]
        )
        automation.definition.conditions_all.replace(
          [
            DefinitionItem.new('sla_next_breach_at', 'is', '20'),
            DefinitionItem.new('status_id', 'less_than', 4),
          ]
        )
      end

      it 'is valid' do
        assert_valid automation
      end
    end

    describe 'when the source is `until_sla_next_breach_at`' do
      before do
        automation.account.stubs(has_service_level_agreements?: true)

        automation.definition.actions.replace(
          [DefinitionItem.new('current_tags', 'is', 'shoop')]
        )
        automation.definition.conditions_all.replace(
          [
            DefinitionItem.new('until_sla_next_breach_at', 'is', '20'),
            DefinitionItem.new('status_id', 'less_than', 4),
          ]
        )
      end

      it 'is valid' do
        assert_valid automation
      end
    end

    describe 'when csr is unoffered' do
      describe 'and the action offers to requester' do
        it 'is valid' do
          assert_valid automation
        end
      end

      describe 'and the action does not offer to requester' do
        before { automation.definition.actions.pop } # score condition

        it 'is invalid' do
          refute_valid automation
        end
      end
    end

    describe 'when the status is less than solved' do
      let(:automation) { rules(:automation_request_csr) }

      before do
        automation.definition.actions.replace(
          [
            DefinitionItem.new(
              'notification_user',
              'is',
              ['requester_id', '1', '1']
            )
          ]
        )
        automation.definition.conditions_all.replace([DefinitionItem.new('status_id', 'less_than', '3')])
      end

      describe 'and sets the status to solved' do
        let(:solve_action) { DefinitionItem.new('status_id', 'is', '3') }

        before { automation.definition.actions << solve_action }

        it 'is valid' do
          assert_valid automation
        end
      end

      describe 'and sets the status to open' do
        let(:open_action) { DefinitionItem.new('status_id', 'is', '1') }

        before { automation.definition.actions << open_action }

        it 'is invalid' do
          refute_valid automation
        end
      end
    end

    describe 'when an any condition nullifies, but an all condition does not' do
      let(:score_condition) { automation.definition.conditions_all.pop }

      before { automation.definition.conditions_any << score_condition }

      it 'is invalid' do
        refute_valid automation
      end
    end

    describe 'when a ticket field nullifies' do
      let(:field) { "ticket_fields_#{ticket_fields(:field_date_custom).id}" }

      before do
        automation.definition.conditions_all.replace(
          [
            DefinitionItem.new(field, 'is', ['2001-01-01']),
            DefinitionItem.new('status_id', 'less_than', ['3'])
          ]
        )
        automation.definition.actions.replace(
          [DefinitionItem.new(field, 'is', ['specific_date', '2014-09-03'])]
        )
      end

      it 'is valid' do
        assert_valid automation
      end
    end

    describe 'when a custom user field nullifies' do
      let(:field) { "requester.custom_fields.#{cf_fields(:integer1).key}" }

      before do
        automation.definition.conditions_all.replace(
          [
            DefinitionItem.new(field, 'not_present'),
            DefinitionItem.new('status_id', 'less_than', ['3'])
          ]
        )
        automation.definition.actions.replace(
          [DefinitionItem.new(field, 'is', ['1'])]
        )
      end

      it 'is valid' do
        assert_valid automation
      end
    end

    describe 'when a custom organization field nullifies' do
      let(:field) do
        "organization.custom_fields.#{cf_fields(:org_integer1).key}"
      end

      before do
        automation.definition.conditions_all.replace(
          [
            DefinitionItem.new(field, 'not_present'),
            DefinitionItem.new('status_id', 'less_than', ['3'])
          ]
        )
        automation.definition.actions.replace(
          [DefinitionItem.new(field, 'is', ['1'])]
        )
      end

      it 'is valid' do
        assert_valid automation
      end
    end

    describe 'when there are not any property-changing actions' do
      before do
        automation.definition.actions.replace(
          [
            DefinitionItem.new(
              'notification_user',
              'is',
              [users(:minimum_agent), 'h', 'h']
            )
          ]
        )
      end

      it 'is invalid' do
        refute_valid automation
      end
    end

    describe 'when there is a time-based condition that is true once' do
      before do
        automation.definition.conditions_all.replace(
          [
            DefinitionItem.new('status_id', 'less_than', ['3']),
            DefinitionItem.new('NEW', 'is_business_hours', 8)
          ]
        )
      end

      it 'is valid' do
        assert_valid automation
      end
    end

    describe 'when there are no actions' do
      before { automation.definition.actions.clear }

      it 'is invalid' do
        refute_valid automation
      end
    end
  end

  describe 'verify_table_limit' do
    let(:limit)      { account.rules.active.where(type: 'Automation').count }
    let(:account)    { automation.account }
    let(:definition) { build_automation_definition }

    let(:new_automation) do
      account.automations.build(
        definition: definition,
        title: 'New automation',
        account: account
      )
    end

    let(:log_message) do
      "Rules table limit exceeded: account_id: #{account.id}, "\
      "rule_type: #{automation.type}, current limit: #{limit}"
    end

    before do
      Rule.any_instance.stubs(:soft_limit_for_rule).returns(limit)
    end

    describe_with_arturo_enabled :apply_rules_table_limits do
      describe 'when the account has reached the limit' do
        let(:inactive_automation) { rules(:inactive_automation_for_minimum_account) }

        it 'does not allow creating new automations' do
          Rails.logger.expects(:info).with(log_message).once

          assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
            new_automation.save
          end
        end

        it 'allows an active automation to be deactivated' do
          assert automation.update_attribute(:is_active, false)
        end

        it 'allows an active automation to be updated' do
          assert automation.update_attribute(:title, 'new title')
        end

        it 'does not allow an inactive automation to be activated' do
          assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
            inactive_automation.update_attribute(:is_active, true)
          end
        end

        it 'allows automations to be deleted' do
          assert automation.destroy
        end
      end

      describe 'when the account has not met the limit' do
        before { account.automations.delete_all }

        it 'persists the automation successfully' do
          assert new_automation.save
        end
      end

      describe_with_arturo_enabled :higher_automations_table_limit do
        it 'persists the automation successfully' do
          assert new_automation.save
        end

        describe 'and when the account exceeds hard limit' do
          before do
            Automation.
              any_instance.
              stubs(hard_limit_for_rule: limit)
          end

          it 'fails with an error' do
            Rails.logger.expects(:info).with(log_message).once

            assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
              new_automation.save
            end
          end
        end
      end
    end

    describe_with_arturo_disabled :apply_rules_table_limits do
      it 'persists the automation successfully' do
        assert automation.save
      end
    end
  end
end
