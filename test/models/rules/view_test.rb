require_relative "../../support/test_helper"
require_relative '../../support/rule'

SingleCov.covered! uncovered: 4

describe 'View' do
  include TestSupport::Rule::Helper

  fixtures :all

  before do
    @minimum_account = accounts(:minimum)
    @minimum_agent = users(:minimum_agent)

    stub_occam_count(0)
  end

  it 'prevents collection modification' do
    assert(
      View.
        included_modules.
        include?(Zendesk::PreventCollectionModificationMixin)
    )
  end

  describe "validations" do
    it "strips out bad columns before saving" do
      tf = ticket_fields(:field_tagger_custom)
      view = rules(:view_assigned_working_tickets)
      view.save!

      old_columns = view.output.columns.dup
      view.output.columns << tf.id
      view.save!

      assert(tf.update_columns(account_id: 123456))
      view.reload.save!
      assert_equal old_columns, view.output.columns

      # custom date field is deprecated in rules.
    end

    describe 'verify_table_limit' do
      let(:account)    { accounts(:minimum) }
      let(:limit)      { account.rules.active.where(type: 'View').count }
      let(:definition) { build_view_definition }
      let(:output)     { build_view_output }

      let(:view) do
        account.views.build(
          definition: definition,
          output: output,
          title: 'New view',
          account: account
        )
      end

      let(:log_message) do
        "Rules table limit exceeded: account_id: #{account.id}, "\
        "rule_type: #{view.type}, current limit: #{limit}"
      end

      before do
        Rule.any_instance.stubs(:soft_limit_for_rule).returns(limit)
      end

      describe_with_arturo_enabled :apply_rules_table_limits do
        describe 'when the account has reached the limit' do
          let(:active_view)   { rules(:view_assigned_working_tickets) }
          let(:inactive_view) { rules(:inactive_view_for_minimum_group) }

          it 'does not allow creating new views' do
            Rails.logger.expects(:info).with(log_message).once

            assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
              view.save
            end
          end

          it 'allows an active view to be deactivated' do
            assert active_view.update_attribute(:is_active, false)
          end

          it 'allows an active view to be updated' do
            assert active_view.update_attribute(:title, 'new title')
          end

          it 'does not allow an inactive view to be activated' do
            assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
              inactive_view.update_attribute(:is_active, true)
            end
          end

          it 'allows views to be deleted' do
            assert active_view.delete
          end
        end

        describe 'when the account has not met the limit' do
          before { account.views.delete_all }

          it 'persists the view successfully' do
            assert view.save
          end
        end

        describe_with_arturo_enabled :higher_views_table_limit do
          it 'persists the view successfully' do
            assert view.save
          end

          describe 'and when the account exceeds hard limit' do
            before do
              View.
                any_instance.
                stubs(hard_limit_for_rule: limit)
            end

            it 'fails with an error' do
              Rails.logger.expects(:info).with(log_message).once

              assert_raises (Zendesk::ProductLimit::TableLimitExceededError) do
                view.save
              end
            end
          end
        end
      end

      describe_with_arturo_disabled :apply_rules_table_limits do
        it 'persists the view successfully' do
          assert view.save
        end
      end
    end

    describe 'when view can be filtered' do
      let(:view) { rules(:view_assigned_working_tickets) }

      before do
        @minimum_account.settings.skill_based_filtered_views = [view.id]

        @minimum_account.settings.save!
      end

      describe_with_arturo_enabled :skill_based_view_filters do
        describe 'and view is cacheable' do
          it 'saves the view' do
            assert view.save!
          end
        end

        describe 'and view is not cacheable' do
          let(:error_messsage) do
            I18n.t("txt.admin.models.rules.rule.skill_based_view.unsupported_properties")
          end

          before do
            view.
              definition.
              conditions_all << DefinitionItem.new(
                'status_id',
                'is',
                '4'
              )
          end

          it 'raises validation error' do
            assert_raises ActiveRecord::RecordInvalid do
              view.save!
            end

            assert_equal(view.errors.messages[:base], [error_messsage])
          end
        end
      end

      describe_with_arturo_disabled :skill_based_view_filters do
        it 'saves the view' do
          assert view.save!
        end

        it 'does not throw validation error' do
          assert_nil view.errors.messages[:base]
        end

        describe 'when view becomes inactive' do
          before do
            @view = rules(:view_assigned_working_tickets)
            @view.is_active = false
            @view.save!
          end

          it 'removes the view from `skill_based_filtered_views`' do
            refute @minimum_account.
              settings.
              reload.
              skill_based_filtered_views.
              include?(@view.id)
          end
        end
      end
    end

    describe 'when saving view with owner type `Group`' do
      let(:view)       { rules(:view_assigned_working_tickets) }
      let(:owner_type) { 'Group' }
      let(:group)      { groups(:minimum_group) }
      let(:group_2)    { groups(:support_group) }

      describe_with_arturo_enabled :multiple_group_views_writes do
        before do
          GroupView.destroy_all
          group_2.update_columns(is_active: 1)
          view.owner           = group
          view.group_owner_ids = group_owner_ids
          view.save!
        end

        describe 'and the view belongs to multiple groups' do
          let(:group_owner_ids) { [group, group_2].map(&:id) }

          it 'sets the owner ID' do
            assert_equal group.id, view.owner_id
          end

          it 'associates the groups with the view' do
            assert_equal(
              [group, group_2].map(&:id),
              GroupView.
                where(account_id: view.account_id, view_id: view.id).
                pluck(:group_id)
            )
          end
        end

        describe 'and the view belongs to a single group' do
          let(:group_owner_ids) { [group.id] }

          it 'sets the owner ID' do
            assert_equal group.id, view.owner_id
          end

          it 'associates the group with the view' do
            assert(
              GroupView.where(
                account_id: view.account_id,
                group_id:   group.id,
                view_id:    view.id
              ).one?
            )
          end
        end
      end

      describe_with_arturo_disabled :multiple_group_views_writes do
        before do
          view.owner = group
          view.save!
        end

        it 'sets the owner ID' do
          assert_equal group.id, view.owner_id
        end

        it 'does not create associated groups views' do
          assert_empty view.groups
        end

        it 'does not create an entry in `groups_views` table' do
          refute GroupView.find_by(view_id: view.id)
        end
      end
    end

    describe 'when view is not a filterable view' do
      let(:view) { rules(:view_assigned_working_tickets) }

      before do
        @minimum_account.settings.skill_based_filtered_views = []

        @minimum_account.settings.save!
      end

      it 'saves the view' do
        assert view.save!
      end
    end
  end

  describe '.incoming' do
    describe 'when account does not have `groups` feature' do
      let(:account) { @minimum_agent.account }
      let(:view)    { View.incoming(@minimum_agent) }

      before { account.stubs(has_groups?: false) }

      it 'does not include `group` column in output' do
        refute_includes view.as_json[:output][:columns], name: 'group'
      end

      describe 'and account is upgraded' do
        before { account.stubs(has_groups?: true) }

        it 'includes `group` column in output' do
          assert_includes view.as_json[:output][:columns], name: 'group'
        end
      end
    end
  end

  describe ".preview" do
    describe "with scope" do
      subject { @minimum_account.views.preview(title: "preview", all: [{operator: "is", value: "open", field: "status"}]) }

      it "picks up account scope" do
        assert_equal @minimum_account, subject.owner
      end
    end

    describe "with :conditions key" do
      subject do
        @minimum_account.views.preview(title: "preview", conditions: {
          all: [{ operator: "is", field: "status", value: "open" }],
          any: [{ operator: "is", field: "priority", value: "low" }]
        })
      end

      it "is valid" do
        assert subject.valid?
      end

      it "includes all and any" do
        assert subject.definition.conditions_all.any?
        assert subject.definition.conditions_any.any?
      end
    end

    describe "#preview_id" do
      subject { @minimum_account.views.preview(title: "preview", all: [{operator: "is", value: "open", field: "status"}]) }

      it "is the definiton hash" do
        assert_equal subject.definition.cache_key, subject.preview_id
      end
    end

    describe "#ticket_count" do
      describe "with a preview" do
        subject { @minimum_account.views.preview(title: "preview", all: [{operator: "is", value: "open", field: "status"}]) }
        before { subject.ticket_count(@minimum_agent) }

        before_should "call CachedRulePreviewTicketCount" do
          CachedRulePreviewTicketCount.expects(:lookup)
        end
      end

      describe "with a view" do
        subject { rules(:view_assigned_working_tickets) }
        before { subject.ticket_count(@minimum_agent) }

        before_should "call CachedRuleTicketCount" do
          CachedRuleTicketCount.expects(:lookup)
        end
      end
    end

    describe "with incoming view" do
      subject { View.preview(id: "incoming", owner: @minimum_agent) }

      it "returns incoming view" do
        assert_equal "Incoming", subject.title
      end
    end

    describe "with my view" do
      subject { View.preview(id: "my", owner: @minimum_agent) }

      it "returns my view" do
        assert_equal "My Tickets", subject.title
      end
    end

    describe "with my_groups view" do
      subject { View.preview(id: "my_groups", owner: @minimum_agent) }

      it "returns my_groups view" do
        assert_equal "My Groups Tickets", subject.title
      end
    end

    describe "with inactive view" do
      before do
        @view = rules(:view_assigned_working_tickets)
        @view.is_active = false
        @view.save!
      end

      it "returns incoming view" do
        assert_raises ActiveRecord::RecordNotFound do
          View.preview(id: @view.id, owner: @minimum_agent)
        end
      end
    end

    describe "with existing view" do
      subject { View.preview(id: @view.id) }

      before do
        @view = rules(:view_assigned_working_tickets)
      end

      it "returns view" do
        assert_equal @view, subject
      end

      describe "with different output" do
        subject { View.preview(id: @view.id, output: { columns: [:subject] }) }

        it "returns view with merged output" do
          assert_equal [:subject], subject.output.columns

          assert_equal @view.output.group, subject.output.group
          assert_nil subject.output.order # this might be wrong ...

          assert_equal @view.output.group_order, subject.output.group_order
          assert_equal @view.output.sort_order, subject.output.sort_order
        end
      end

      describe "with different definition" do
        subject { View.preview(id: @view.id, all: [{ operator: "is", field: "status", value: "open" }]) }

        it "returns view with definition unchanged" do
          @view.definition.items.each do |item|
            assert subject.definition.items.detect { |sit| sit.source == item.source && sit.operator == item.operator && sit.value == item.value },
              "missing #{item.inspect} from #{subject.definition.inspect}"
          end
        end
      end
    end
  end

  describe "serialization" do
    it "serializes to legacy XML format" do
      @view = rules(:view_assigned_working_tickets)
      xml = Nokogiri::XML(@view.to_xml(root: "view", count_for_user: users(:minimum_agent)))

      ["owner-type", "owner-id", "id", "per-page", "position", "title", "is-active"].each do |k|
        assert_equal @view[k.underscore.to_sym].to_s, xml.at(k).text, "#{k} in XML should match value in view"
      end

      assert_equal @view.ticket_count(users(:minimum_agent)).value.to_s, xml.at("ticket-count").text, "Ticket count in XML should match #ticket_count"

      keys = xml.root.children.select { |c| c.is_a?(Nokogiri::XML::Element) }.map(&:name)
      assert_equal [], keys - ["owner-type", "owner-id", "id", "per-page", "position", "title", "is-active", "ticket-count"], "Unexpected data in XML output"
    end

    it "serializes to legacy JSON format" do
      @view = rules(:view_assigned_working_tickets)
      json = JSON.parse(@view.to_json(count_for_user: users(:minimum_agent)))

      ["owner_type", "owner_id", "id", "per_page", "position", "title", "is_active"].each do |k|
        assert_equal_with_nil @view[k.to_sym], json[k], "#{k} in JSON should match value in view"
      end

      assert_equal @view.ticket_count(users(:minimum_agent)).value, json["ticket_count"], "Ticket count in XML should match #ticket_count"

      assert_equal [], json.keys - ["owner_type", "owner_id", "id", "per_page", "position", "title", "is_active", "ticket_count"], "Unexpected data in JSON output"
    end

    it "dos delayed ticket counts for high volume accounts when serializing to legacy format" do
      @view = rules(:view_assigned_working_tickets)

      @view.expects(:ticket_count).with(@minimum_agent, refresh: :delayed).returns(stub(value: "..."))

      json = JSON.parse(@view.to_json(count_for_user: @minimum_agent, delay_counts: true))
      assert_equal("...", json["ticket_count"])
    end

    it "serializes to XML format" do
      @view = rules(:view_assigned_working_tickets)
      xml = Nokogiri::XML(@view.to_xml(root: "view"))

      ["owner-type", "owner-id", "id", "per-page", "position", "title", "is-active"].each do |k|
        assert_equal @view[k.underscore.to_sym].to_s, xml.at(k).text, "#{k} in XML should match value in view"
      end

      ["order", "group", "type", "order-asc"].each do |k|
        assert_equal @view.output.send(k.underscore.to_sym).to_s, xml.at("output/#{k}").text, "/output/#{k} in XML should match value in view"
      end

      assert_equal @view.output.columns.map(&:to_s), xml.xpath("view/output/columns/column/name").map(&:text)

      keys = xml.root.children.select { |c| c.is_a?(Nokogiri::XML::Element) }.map(&:name)
      refute keys.include?("ticket-count"), "Shouldn't emit ticket count in XML output"

      assert_equal [], keys - ["owner-type", "owner-id", "id", "per-page", "position", "title", "is-active", "output"], "Unexpected data in XML output"
    end

    it "serializes to JSON format" do
      @view = rules(:view_assigned_working_tickets)
      json = JSON.parse(@view.to_json)

      ["owner_type", "owner_id", "id", "per_page", "position", "title", "is_active"].each do |k|
        assert_equal_with_nil @view.send(k.to_sym), json[k], "#{k} in JSON should match value in view"
      end

      ["order", "group", "type", "order_asc"].each do |k|
        val = @view.output.send(k.underscore.to_sym)
        val = val.is_a?(Symbol) ? val.to_s : val
        assert_equal_with_nil val, json["output"][k], "output[#{k}] in JSON should match value in view"
      end

      refute json.keys.include?("ticket_count"), "Shouldn't emit ticket count in XML output"

      assert_equal @view.output.columns.map(&:to_s), json["output"]["columns"].map { |c| c["name"] }

      assert_equal [], json.keys - ["owner_type", "owner_id", "id", "per_page", "position", "title", "is_active", "output"], "Unexpected data in JSON output"
    end
  end

  describe '#output' do
    let(:view) do
      view = rules(:view_assigned_working_tickets)
      view.output.columns.push(:attributes_match)
      view
    end
    let(:output) { view.output }
    before { Account.any_instance.stubs(has_skill_based_ticket_routing?: true) }

    describe 'when account has groups feature' do
      before { Account.any_instance.stubs(has_groups?: true) }

      it 'does not filter out group column' do
        expected_output = {
          'columns' => %i[
            submitter description created requester priority group assignee attributes_match
          ],
          'group'     => :status,
          'group_asc' => false,
          'order'     => nil,
          'order_asc' => false,
          'type'      => 'table'
        }

        expected_output['columns'].map!(&:to_s)
        expected_output['group'] = expected_output['group'].to_s

        assert_equal(
          expected_output,
          output.as_json
        )
      end
    end

    describe 'when account does not have groups feature' do
      before { Account.any_instance.stubs(has_groups?: false) }

      it 'filters out group column' do
        expected_output = {
          'columns' => %i[
            submitter description created requester priority assignee attributes_match
          ],
          'group'     => :status,
          'group_asc' => false,
          'order'     => nil,
          'order_asc' => false,
          'type'      => 'table'
        }

        expected_output['columns'].map!(&:to_s)
        expected_output['group'] = expected_output['group'].to_s

        assert_equal(
          expected_output,
          output.as_json
        )
      end
    end

    describe 'when account does not have skill_based_ticket_routing feature' do
      before { Account.any_instance.stubs(has_skill_based_ticket_routing?: false) }

      it 'filters out attributes_match column' do
        expected_output = {
          'columns' => %i[
            submitter description created requester priority group assignee
          ],
          'group'     => :status,
          'group_asc' => false,
          'order'     => nil,
          'order_asc' => false,
          'type'      => 'table'
        }

        expected_output['columns'].map!(&:to_s)
        expected_output['group'] = expected_output['group'].to_s

        assert_equal(
          expected_output,
          output.as_json
        )
      end
    end
  end

  describe '#find_tickets_with_ids' do
    describe 'when ticket_ids are provided' do
      let(:params)     { {paginate: true} }
      let(:ticket_ids) { [1, 2, 3] }
      let(:view)       { rules(:view_assigned_working_tickets) }

      before do
        Zendesk::Rules::RuleExecuter.
          any_instance.
          expects(:find_tickets_with_occam_client).
          returns([])

        view.find_tickets_with_ids(accounts(:minimum).owner, params, ticket_ids)
      end

      it 'removes the scope after query' do
        condition = view.definition.conditions_all.find do |item|
          item.source == :id
        end

        assert_nil condition
      end
    end
  end
end
