require_relative "../support/test_helper"

SingleCov.covered!

describe Screencast do
  let(:screencast_params) do
    {
      "id"        => "fe5c7c84303e471da8adc649b4e33b14",
      "position"  => "1",
      "url"       => "http://support83.sfssdev.test/embed/fe5c7c84303e471da8adc649b4e33b14",
      "thumbnail" => "http://pic.test/test1.jpg"
    }
  end

  let(:screencast) do
    Screencast.new(screencast_params)
  end

  it "returns url" do
    screencast.url.must_equal("http://support83.sfssdev.test/embed/fe5c7c84303e471da8adc649b4e33b14")
  end

  it "returns position" do
    screencast.position.must_equal("1")
  end

  it "returns an id" do
    screencast.id.must_equal("fe5c7c84303e471da8adc649b4e33b14")
  end

  it "returns a thumbnail url" do
    screencast.thumbnail.must_equal("http://pic.test/test1.jpg")
  end

  it "returns a display name" do
    I18n.stubs(:t).with('txt.models.screencast.screencast_display_name').returns('Screencast')
    screencast.display_name.must_equal("Screencast 1")
  end

  it "render for_partner" do
    screencast.for_partner['position'].must_equal('1')
    screencast.for_partner['url'].must_equal('http://support83.sfssdev.test/embed/fe5c7c84303e471da8adc649b4e33b14')
    screencast.for_partner['id'].must_equal('fe5c7c84303e471da8adc649b4e33b14')
    screencast.for_partner['thumbnail'].must_equal('http://pic.test/test1.jpg')
  end

  it "generates an array of Screencasts from metadata" do
    metadata = { system: { screencasts: [
      {"position"  => "999",
       "url"       => "http://support83.sfssdev.test/embed/bar",
       "id"        => "foo",
       "thumbnail" => "http://pic.test/foo.jpg"}
    ]}}
    screencasts = Screencast.screencasts_from_metadata(metadata)

    screencasts.first.position.must_equal('999')
    screencasts.first.url.must_equal("http://support83.sfssdev.test/embed/bar")
    screencasts.first.id.must_equal("foo")
    screencasts.first.thumbnail.must_equal("http://pic.test/foo.jpg")
  end

  it "generates an array for metadata from a Ticket Sharing Comment" do
    screencasts = [{"position"  => "2",
                    "url"       => "http://support83.sfssdev.test/embed/foo",
                    "id"        => "foo",
                    "thumbnail" => "http://pic.test/bar.jpg"}]

    custom_fields = {'zendesk' => {'metadata' => {'screencasts' => screencasts}}}
    ts_comment = stub(custom_fields: custom_fields)

    expected = [
      {"position"  => "2",
       "url"       => "http://support83.sfssdev.test/embed/foo",
       "id"        => "foo",
       "thumbnail" => "http://pic.test/bar.jpg"}
    ]
    Screencast.screencasts_from_ticket_sharing(ts_comment).must_equal(expected)
  end
end
