require_relative "../support/test_helper"
require_relative "../support/sharing_test_helper"
require_relative "../support/attachment_test_helper"
require "faraday_middleware/response/follow_redirects"

SingleCov.covered! uncovered: 4

describe 'SharedTicket' do
  include SharingTestHelper
  include PageNotFoundRenderingSupport

  fixtures :accounts, :tickets, :events, :subscriptions, :payments,
    :remote_authentications, :sequences, :ticket_fields, :users,
    :groups, :memberships, :account_property_sets, :taggings, :monitored_twitter_handles,
    :ticket_fields, :ticket_field_entries, :custom_field_options, :facebook_pages

  resque_inline false

  before do
    Account.any_instance.stubs(:default_brand).returns(brands('minimum'))
  end

  it 'should not be susceptible to mysql coercion exploits' do
    ticket        = tickets(:minimum_1)
    agreement     = FactoryBot.create(:agreement)
    SharedTicket.create!(ticket: ticket, agreement: agreement, uuid: 'asdf123')

    assert_nil SharedTicket.find_by_uuid(0)
  end

  it 'should generate a uuid on validation on create' do
    account = accounts(:minimum)
    ticket = SharedTicket.new(account: account, ticket: account.tickets.first)
    ticket.valid?
    assert_not_nil(ticket.uuid)
  end

  it 'should validate uniquness on uuid' do
    account = accounts(:minimum)
    agreement = FactoryBot.create(:agreement)
    SharedTicket.create!(account: account, ticket: account.tickets.first, agreement: agreement, uuid: 'uuid')
    assert_raise ActiveRecord::RecordInvalid do
      SharedTicket.create!(account: account, ticket: account.tickets.last, agreement: agreement, uuid: 'uuid')
    end
  end

  it 'should use a ticket UUID from the past when it can' do
    account = accounts(:minimum)
    ticket  = account.tickets.first
    agreement1 = FactoryBot.create(:agreement, name: 'Rackspace', remote_url: 'http://www.rackspace.fake')
    agreement2 = FactoryBot.create(:agreement, name: 'Laughing Squid', remote_url: 'http://www.squid.fake')

    SharedTicket.create!(
      ticket: ticket,
      agreement: agreement1,
      uuid: 'previous_uuid'
    )
    second_shared_ticket = SharedTicket.new(
      account: account,
      agreement: agreement2,
      ticket: ticket
    )

    assert second_shared_ticket.valid?
    assert_equal('previous_uuid', second_shared_ticket.uuid)
  end

  it 'should not be able to create multiple shared tickets with the same partner' do
    ticket = tickets(:minimum_1)
    agreement = FactoryBot.create(:agreement)
    SharedTicket.create!(ticket: ticket, agreement: agreement)
    second_shared_ticket = SharedTicket.new(ticket: ticket)
    refute second_shared_ticket.valid?
  end

  it 'should provide an agreement_name' do
    ticket = tickets(:minimum_1)
    agreement = FactoryBot.create(:agreement, name: 'Name')
    shared_ticket = SharedTicket.new(agreement: agreement, ticket: ticket)
    assert_equal('Name', shared_ticket.agreement_name)
  end

  it 'should provide an agreement_name if the agreement has been deleted' do
    ticket = tickets(:minimum_1)
    agreement = FactoryBot.create(:agreement, name: 'Name')
    shared_ticket = SharedTicket.create!(agreement: agreement, ticket: ticket)

    agreement.destroy
    shared_ticket.reload
    assert_equal('Name (deactivated)', shared_ticket.agreement_name)
  end

  it 'should provide a safe_agreement method which finds deleted agreements' do
    ticket = tickets(:minimum_1)
    agreement = FactoryBot.create(:agreement, name: 'Name')
    shared_ticket = agreement.shared_tickets.create!(ticket: ticket)

    agreement.soft_delete!
    shared_ticket.reload
    assert_equal(agreement, shared_ticket.safe_agreement)
  end

  it 'creating an outbound ticket should send to partner' do
    Resque.jobs.clear
    account = accounts(:minimum)
    agreement = FactoryBot.create(:agreement, account: account, direction: :out, status: :accepted)
    ticket = SharedTicket.new(account: account, agreement: agreement)

    assert_difference "Resque.jobs.size", +1 do
      ticket.save!
    end

    assert_equal("Sharing::TicketJob", Resque.jobs.last[:class])
    assert_equal([account.id, :send_to_partner, ticket.id], Resque.jobs.last[:args][0..2])
  end

  it 'creating an inbound ticket should not send to partner' do
    Resque.jobs.clear
    agreement = FactoryBot.create(:agreement, direction: :in)
    ticket = SharedTicket.new(
      account: accounts(:minimum),
      agreement: agreement
    )

    refute_difference "Resque.jobs.size" do
      ticket.save!
    end
  end

  it "Sending a ticket to a partner, with a bad SSL Certificate" do
    Resolv::DNS.any_instance.stubs(:getaddress).with('bad-ssl-cert.com').returns(Resolv::IPv4.create('1.2.3.4'))
    agreement = FactoryBot.create(:agreement,
      remote_url: "https://bad-ssl-cert.com/sharing")
    ticket = SharedTicket.new(
      agreement: agreement,
      uuid: "1234",
      ticket: tickets(:minimum_1)
    )

    stub_request(:post, "https://bad-ssl-cert.com/sharing/tickets/1234").
      to_raise(OpenSSL::SSL::SSLError)

    assert_not_equal(:ssl_error, agreement.status)
    ticket.send_to_partner
    assert_equal(:ssl_error, agreement.status)
  end

  it "Sending an update to a partner, with a bad SSL Certificate" do
    Resolv::DNS.any_instance.stubs(:getaddress).with('bad-ssl-cert.com').returns(Resolv::IPv4.create('1.2.3.4'))
    agreement = FactoryBot.create(:agreement,
      remote_url: "https://bad-ssl-cert.com/sharing")
    ticket = SharedTicket.new(
      agreement: agreement,
      uuid: "1234",
      ticket: tickets(:minimum_1)
    )

    stub_request(:put, "https://bad-ssl-cert.com/sharing/tickets/1234").
      to_raise(OpenSSL::SSL::SSLError)

    assert_not_equal(:ssl_error, agreement.status)
    ticket.update_partner
    assert_equal(:ssl_error, agreement.status)
  end

  it "Given a partner with a bad SSL Certificate, do not enqueue any jobs" do
    Resque.jobs.clear
    agreement = FactoryBot.create(:agreement, status: :ssl_error)
    ticket = SharedTicket.new(
      agreement: agreement,
      account: accounts(:minimum)
    )

    ticket.enqueue_job(:send_to_partner)
    assert_equal(0, Resque.jobs.size)
  end

  describe '#add_new_comment' do
    before do
      @account = accounts(:minimum)
      @link = '<a href="http://google.com">google</a>'
      @link_re = /<a href="http:\/\/google.com"/ # text processing adds noreferrer, so we can't match on @link
      @ts_comment = new_ticket_sharing_comment(
        'public' => false,
        'html_body' => @link,
        'author' => new_ticket_sharing_actor(role: 'agent')
      )

      @shared_ticket = FactoryBot.create(:agreement, account: accounts(:minimum)).
        shared_tickets.
        build(
          account: accounts(:minimum),
          ticket: tickets(:minimum_1)
        )
    end

    it 'passes html_body' do
      comment = @shared_ticket.send(:add_new_comment, @ts_comment)

      assert_match @link_re, comment.html_body
    end
  end

  it 'setup from partner #image attachment' do
    created_at = 2.days.ago
    ts_ticket = new_ticket_sharing_ticket('requested_at' => created_at.in_time_zone.to_s)
    ts_ticket.comments << new_ticket_sharing_comment('public' => true)

    agent_actor = new_ticket_sharing_actor('role' => 'agent')
    ts_attachment = TicketSharing::Attachment.new('url' => 'http://example.com/foo.jpg', 'filename' => 'bar.jpg',
                                                  'content_type' => 'image/jpg')
    ts_ticket.comments << new_ticket_sharing_comment('public' => false, 'author' => agent_actor, 'attachments' => [ts_attachment])

    account = accounts(:minimum)
    agreement = FactoryBot.create(:agreement, account: account)
    shared_ticket = agreement.shared_tickets.build
    shared_ticket.from_partner(ts_ticket)

    assert_equal(ts_ticket.original_id, shared_ticket.original_id)

    new_ticket = shared_ticket.ticket.reload

    assert_not_nil(new_ticket.id)
    assert_equal(ts_ticket.uuid, shared_ticket.uuid)
    assert_equal(created_at.to_s, new_ticket.created_at.to_s)

    assert_equal(ts_ticket.comments.first.body, new_ticket.description)
    assert_equal(ts_ticket.comments.first.body, new_ticket.comments.first.body)

    # assert comment correctness
    assert_equal(ts_ticket.comments.size, new_ticket.comments.size)
    ts_ticket.comments.size.times do |index|
      ts_comment  = ts_ticket.comments[index]
      new_comment = new_ticket.comments[index]

      assert_equal(ts_comment.body, new_comment.body)
      assert_not_nil(new_comment.shared_comment)
      assert_equal(ts_comment.uuid, new_comment.shared_comment.uuid)
      assert_equal(ts_comment.author.name, new_comment.author.name)
      assert_equal(ts_comment.authored_at.to_time.to_s, new_comment.created_at.to_s)
      assert_equal(ts_comment.public?, new_comment.is_public?)

      assert_equal(ts_comment.author.agent?, new_comment.author.foreign_agent?)
    end

    assert new_attachment = new_ticket.comments.last.attachments.last
    assert new_attachment.external_url.url
    assert_equal('bar.jpg', new_attachment.filename)
    assert_equal('image/jpg', new_attachment.content_type)
    assert_equal(ts_attachment.url, new_attachment.external_url.url)
    assert_equal(new_ticket.account, new_attachment.account)

    # The requester and first comment's author should be the same
    assert_equal(new_ticket.requester, new_ticket.comments.first.author)
  end

  describe 'Given a ticket with tags' do
    before do
      @ticket = tickets(:minimum_1)
      assert !@ticket.taggings.empty?
    end

    describe 'Given an agreement that syncs tags' do
      before do
        @agreement = FactoryBot.create(:agreement, sync_tags: true)
      end

      describe 'When sending a ticket' do
        before do
          @shared_ticket = SharedTicket.new(
            account: accounts(:minimum),
            ticket: @ticket,
            agreement: @agreement
          )
        end

        it 'includes the list of tags' do
          ts_ticket = @shared_ticket.for_partner
          assert_equal(@ticket.tag_array, ts_ticket.tags)
        end
      end
    end

    describe 'Given an agreement that does not sync tags' do
      before do
        @agreement = FactoryBot.create(:agreement, sync_tags: false)
      end

      describe 'When sending a ticket' do
        before do
          @shared_ticket = SharedTicket.new(
            account: accounts(:minimum),
            ticket: @ticket,
            agreement: @agreement
          )
        end

        it 'does not include the list of tags' do
          ts_ticket = @shared_ticket.for_partner
          assert_nil(ts_ticket.tags)
        end
      end
    end
  end

  describe 'Given an agreement that syncs tags' do
    before do
      @ticket = tickets(:minimum_1)
      @old_tags = @ticket.tag_array

      agreement = FactoryBot.create(:agreement, sync_tags: true)
      @shared_ticket = SharedTicket.new(account: accounts(:minimum), agreement: agreement)
    end

    describe 'Given a payload that includes tags' do
      before do
        @new_tags = ['foo', 'bar']
        @ts_ticket = new_ticket_sharing_ticket('tags' => @new_tags)
      end

      describe 'setup from partner' do
        before do
          @shared_ticket.from_partner(@ts_ticket)
        end

        it 'includes the tags' do
          assert @shared_ticket.id
          assert_equal(@new_tags.sort, @shared_ticket.ticket.tag_array)
          assert_equal(@new_tags.sort.join(' '), @shared_ticket.ticket.current_tags)
        end
      end

      describe 'update from partner' do
        before do
          @shared_ticket.ticket = @ticket
          @shared_ticket.from_partner(@ts_ticket)
        end

        it 'updates the tags' do
          @ticket.reload
          assert_equal(@new_tags.sort, @ticket.tag_array)
          assert_equal(@new_tags.sort.join(' '), @ticket.current_tags)
        end
      end

      describe 'update from parter with only a tag change' do
        before do
          @ts_ticket.comments = []
          @shared_ticket.ticket = @ticket
          @shared_ticket.from_partner(@ts_ticket)
        end

        it 'updates the tags' do
          @ticket.reload
          assert_equal(@new_tags.sort, @ticket.tag_array)
          assert_equal(@new_tags.sort.join(' '), @ticket.current_tags)
        end
      end
    end

    describe 'with a payload that did not provide any tags' do
      before do
        @ts_ticket = new_ticket_sharing_ticket
      end

      describe 'setup from partner' do
        before do
          @shared_ticket.from_partner(@ts_ticket)
        end

        it 'does not create any tags' do
          assert @shared_ticket.id
          assert_equal([], @shared_ticket.ticket.tag_array)
          assert_nil(@shared_ticket.ticket.current_tags)
        end
      end

      describe 'update from partner' do
        before do
          @shared_ticket.ticket = @ticket
          @shared_ticket.from_partner(@ts_ticket)
        end

        it 'does not alter the tags' do
          @ticket.reload
          assert_equal(@old_tags.sort, @ticket.tag_array)
          assert_equal(@old_tags.sort.join(' '), @ticket.current_tags)
        end
      end
    end
  end

  describe 'given an account with a ticket update and ticket create trigger' do
    before do
      definition = Definition.new
      definition.conditions_all.push(DefinitionItem.new('update_type', nil, 'Create'))
      definition.actions.push(DefinitionItem.new('current_tags', nil, 'first_trigger'))
      accounts(:minimum).triggers.create!(title: 'trigger test', definition: definition, position: 1, is_active: true)

      definition = Definition.new
      definition.conditions_all.push(DefinitionItem.new('update_type', nil, 'Change'))
      definition.actions.push(DefinitionItem.new('current_tags', nil, 'second_trigger'))
      accounts(:minimum).triggers.create!(title: 'trigger test', definition: definition, position: 1, is_active: true)

      @agreement = FactoryBot.create(
        :agreement,
        direction: :out,
        allows_public_comments: true
      )

      @ticket = tickets(:minimum_1)
      @old_comments = @ticket.comments.size
      @shared_ticket = SharedTicket.new(account: accounts(:minimum), agreement: @agreement)
      @ts_ticket = new_ticket_sharing_ticket('status' => 'new')
      @ts_ticket.current_actor = new_ticket_sharing_actor('name' => 'Updater', 'role' => 'agent')
      @ts_ticket.comments << new_ticket_sharing_comment('author' => @ts_ticket.current_actor, 'public' => true)
      @ts_ticket.comments << new_ticket_sharing_comment('author' => @ts_ticket.current_actor, 'public' => true)
      @ts_ticket.comments << new_ticket_sharing_comment('author' => @ts_ticket.current_actor, 'public' => true)
    end

    it 'creations should fire just the creation trigger' do
      @shared_ticket.from_partner(@ts_ticket)

      trigger_events = @shared_ticket.ticket.events.select { |event| event.value_reference == "current_tags" }

      assert_equal 1, trigger_events.size
      assert_equal 'Rule Revision', trigger_events.first.via
      assert_includes @shared_ticket.ticket.tag_array, "first_trigger"
      refute_includes @shared_ticket.ticket.tag_array, "second_trigger"
    end
  end

  describe 'Given an agreement that does not sync tags' do
    before do
      @ticket = tickets(:minimum_1)
      @old_tags = @ticket.taggings.map(&:tag_name)

      agreement = FactoryBot.create(:agreement, sync_tags: false)
      @shared_ticket = SharedTicket.new(account: accounts(:minimum), agreement: agreement)
    end

    describe 'and a payload that includes tags' do
      before do
        @ts_ticket = new_ticket_sharing_ticket('tags' => ['foo', 'bar'])
      end

      describe 'setup from partner' do
        before do
          @shared_ticket.from_partner(@ts_ticket)
        end

        it 'does not create any tags' do
          assert @shared_ticket.id
          assert_equal([], @shared_ticket.ticket.tag_array)
          assert_nil(@shared_ticket.ticket.current_tags)
        end
      end

      describe 'update from partner' do
        before do
          @shared_ticket.ticket = @ticket
          @shared_ticket.from_partner(@ts_ticket)
        end

        it 'does not alter the tags' do
          @ticket.reload
          assert_equal(@old_tags.sort, @ticket.tag_array)
          assert_equal(@old_tags.sort.join(' '), @ticket.current_tags)
        end
      end
    end
  end

  describe "Given an agreement that syncs tags" do
    before do
      @ticket = tickets(:minimum_1)
      agreement = FactoryBot.create(:agreement, sync_tags: true)
      @shared_ticket = SharedTicket.new(account: accounts(:minimum), agreement: agreement)
      @old_comments = @ticket.comments.map
    end
    describe "an update from partner with no new comments" do
      before do
        @ts_ticket = new_ticket_sharing_ticket('tags' => ['foo', 'bar'])
        @ts_ticket.comments = []
        @shared_ticket.ticket = @ticket
        @shared_ticket.from_partner(@ts_ticket)
      end
      it "stills share tags" do
        @ticket.reload
        assert_equal 'bar foo', @ticket.current_tags
      end
    end
  end

  describe 'Sync custom fields' do
    before do
      @account = accounts(:minimum)
      @ticket = tickets(:minimum_1)

      @agreement = FactoryBot.create(:agreement, sync_custom_fields: true)
      @shared_ticket = SharedTicket.new(
        account: accounts(:minimum),
        ticket: @ticket,
        agreement: @agreement
      )

      @field_text_custom = ticket_fields(:field_text_custom)
      @field_tagger_custom = ticket_fields(:field_tagger_custom)
      @field_integer_custom = ticket_fields(:field_integer_custom)
      @field_decimal_custom = ticket_fields(:field_decimal_custom)
      @field_checkbox_custom = ticket_fields(:field_decimal_custom)
      @valid_tagger_value = custom_field_options(:field_tagger_custom_option_1).value
    end

    describe '#store_custom_fields' do
      before do
        assert_empty(@ticket.ticket_field_entries)
        @custom_fields = { @field_decimal_custom.title => '123.456' }
      end

      it 'only try to store the valid fields and skip invalid fields' do
        @custom_fields[@field_integer_custom.title] = 'not an integer'
        @ts_ticket = new_ticket_sharing_ticket('custom_fields' => { 'zd_custom_fields' => @custom_fields })

        @shared_ticket.send(:store_custom_fields, @ts_ticket)

        assert @ticket.ticket_field_entries.map(&:value).include?('123.456')
        refute @ticket.ticket_field_entries.map(&:value).include?('not an integer')
      end

      describe 'with a valid field stored and saved' do
        before do
          @ts_ticket = new_ticket_sharing_ticket('custom_fields' => { 'zd_custom_fields' => @custom_fields })

          @shared_ticket.send(:store_custom_fields, @ts_ticket) && @ticket.save

          assert @ticket.reload.ticket_field_entries.any?
        end

        it 'updates that existing valid field entry' do
          @custom_fields = { @field_decimal_custom.title => '234.567' }
          @ts_ticket = new_ticket_sharing_ticket('custom_fields' => { 'zd_custom_fields' => @custom_fields })

          @shared_ticket.send(:store_custom_fields, @ts_ticket)

          assert @ticket.ticket_field_entries.map(&:value).include?('234.567')
          refute @ticket.ticket_field_entries.map(&:value).include?('123.456')
        end

        it 'does not try to store an invalid field entry over a valid one' do
          @custom_fields = { @field_decimal_custom.title => 'not a decimal' }
          @ts_ticket = new_ticket_sharing_ticket('custom_fields' => { 'zd_custom_fields' => @custom_fields })

          @shared_ticket.send(:store_custom_fields, @ts_ticket)

          assert @ticket.ticket_field_entries.map(&:value).include?('123.456')
          refute @ticket.ticket_field_entries.map(&:value).include?('not a decimal')
        end
      end
    end

    describe 'Given a payload that includes custom fields' do
      before do
        custom_fields = { @field_text_custom.title => 'Product', @field_tagger_custom.title => @valid_tagger_value }
        @ts_ticket = new_ticket_sharing_ticket('custom_fields' => { 'zd_custom_fields' => custom_fields })
      end

      describe 'setup from partner' do
        before do
          @shared_ticket.from_partner(@ts_ticket)
          assert @shared_ticket.save!
        end

        it 'syncs the custom fields' do
          assert_equal('Product', @field_text_custom.reload.value(@shared_ticket.ticket))
          assert_equal(@valid_tagger_value, @field_tagger_custom.reload.value(@shared_ticket.ticket))
        end
      end

      describe 'update from partner' do
        it 'syncs the custom fields' do
          @shared_ticket.from_partner(@ts_ticket)
          @ticket.reload
          assert_equal('Product', @field_text_custom.value(@ticket))
          assert_equal(@valid_tagger_value, @field_tagger_custom.value(@ticket))
        end

        describe 'on account with a trigger that sets another (unshared) FieldTagger' do
          before do
            @tagger = FieldTagger.create!(
              title: 'Test tagger',
              account: accounts(:minimum),
              is_active: true,
              custom_field_options: [CustomFieldOption.new(name: "Foo", value: "foo"), CustomFieldOption.new(name: "Bar", value: "bar")]
            )

            definition = Definition.new
            definition.conditions_all.push(DefinitionItem.new('comment_includes_word', 'is', ["comment"]))
            definition.actions.push(DefinitionItem.new("ticket_fields_#{@tagger.id}", nil, [@tagger.custom_field_options.first.id]))
            accounts(:minimum).triggers.create!(
              title: 'trigger test',
              position: 1,
              is_active: true,
              definition: definition
            )
            Timecop.travel(Time.now + 1.seconds)
          end

          it 'sets all field taggers appropriately' do
            @shared_ticket.from_partner(@ts_ticket)
            assert_equal "booring foo hello", @shared_ticket.ticket.reload.current_tags
            tfe_scope = @shared_ticket.ticket.ticket_field_entries
            assert_equal ["booring"], tfe_scope.where(ticket_field_id: ticket_fields(:field_tagger_custom).id).map(&:value)
            assert_equal ["foo"], tfe_scope.where(ticket_field_id: @tagger.id).map(&:value)
          end

          describe "that is already set" do
            before do
              @ticket.fields = {@tagger.id => "bar"}
              @ticket.will_be_saved_by(@ticket.account.owner)
              @ticket.save!
              assert_equal "bar hello", @ticket.reload.current_tags
            end

            it "alsos work" do
              @shared_ticket.from_partner(@ts_ticket)
              assert_equal "booring foo hello", @shared_ticket.ticket.reload.current_tags
              tfe_scope = @shared_ticket.ticket.ticket_field_entries
              assert_equal ["booring"], tfe_scope.where(ticket_field_id: ticket_fields(:field_tagger_custom).id).map(&:value)
              assert_equal ["foo"], tfe_scope.where(ticket_field_id: @tagger.id).map(&:value)
            end
          end
        end
      end

      describe 'for a new ticket with an agreement that does not sync tags' do
        before do
          @agreement.stubs(:sync_tags?).returns(false)
          @shared_ticket = SharedTicket.new(account: accounts(:minimum), agreement: @agreement)
        end

        it 'stills save FieldTagger fields' do
          @shared_ticket.from_partner(@ts_ticket)
          assert @shared_ticket.save!
          assert_equal "booring", @shared_ticket.ticket.reload.current_tags
          assert_equal "booring", @shared_ticket.ticket.ticket_field_entries.detect { |tfe| tfe.ticket_field == @field_tagger_custom }.value
        end

        describe 'with a trigger that adds a tag' do
          before do
            definition = Definition.new
            definition.conditions_all.push(DefinitionItem.new('update_type', 'is', ['Create']))
            definition.actions.push(DefinitionItem.new('current_tags', 'is', ['first_trigger']))
            accounts(:minimum).triggers.create!(title: 'trigger test', definition: definition, position: 1, is_active: true)
          end

          it 'has custom_field and triggered tags' do
            @shared_ticket.from_partner(@ts_ticket)
            assert @shared_ticket.save!
            assert_equal "booring first_trigger", @shared_ticket.ticket.reload.current_tags
            assert_equal "booring", @shared_ticket.ticket.ticket_field_entries.detect { |tfe| tfe.ticket_field == @field_tagger_custom }.value
          end
        end
      end

      describe 'with an agreement that does not sync custom fields' do
        before do
          @agreement.stubs(:sync_custom_fields?).returns(false)
        end

        describe 'setup from partner' do
          before do
            @shared_ticket.from_partner(@ts_ticket)
            assert @shared_ticket.save!
          end

          it 'does not sync the custom fields' do
            assert_equal('', @field_text_custom.reload.value(@shared_ticket.ticket))
            assert_equal('', @field_tagger_custom.reload.value(@shared_ticket.ticket))
          end
        end

        describe 'update from partner' do
          before do
            @shared_ticket.from_partner(@ts_ticket)
          end

          it 'does not sync the custom fields' do
            @ticket.reload
            assert_equal('', @field_text_custom.value(@ticket))
            assert_equal('', @field_tagger_custom.value(@ticket))
          end
        end
      end
    end

    describe 'Given unassignable values for a matching custom field' do
      describe 'drop downs' do
        before do
          unassignable_tagger_field = { @field_tagger_custom.title => 'invalid' }
          @ts_ticket = new_ticket_sharing_ticket('custom_fields' => { 'zd_custom_fields' => unassignable_tagger_field })
        end

        it 'does not sync the drop down on from_partner creation' do
          @shared_ticket.from_partner(@ts_ticket)
          assert @shared_ticket.save!

          assert_equal('', @field_tagger_custom.value(@shared_ticket.ticket))
        end

        it 'does not sync the drop down on from_partner update' do
          @shared_ticket.from_partner(@ts_ticket)

          assert_equal('', @field_tagger_custom.value(@shared_ticket.ticket))
        end
      end

      describe 'checkboxes' do
        before do
          unassignable_checkbox_field = { @field_tagger_custom.title => 'invalid' }
          @ts_ticket = new_ticket_sharing_ticket('custom_fields' => { 'zd_custom_fields' => unassignable_checkbox_field })
        end

        it 'does not sync the drop down on from_partner creation' do
          @shared_ticket.from_partner(@ts_ticket)
          assert @shared_ticket.save!

          assert_equal('', @field_checkbox_custom.value(@shared_ticket.ticket))
        end

        it 'does not sync the drop down on from_partner update' do
          @shared_ticket.from_partner(@ts_ticket)

          assert_equal('', @field_checkbox_custom.value(@shared_ticket.ticket))
        end
      end
    end

    describe 'Given a payload that did not provide any custom fields' do
      before do
        @ts_ticket = new_ticket_sharing_ticket
      end

      describe 'setup from partner' do
        before do
          @shared_ticket.from_partner(@ts_ticket)
          assert @shared_ticket.save!
        end

        it 'does not update any custom fields' do
          assert_equal('', @field_text_custom.value(@ticket))
          assert_equal('', @field_tagger_custom.value(@ticket))
        end
      end

      describe 'update from partner' do
        before do
          @shared_ticket.from_partner(@ts_ticket)
        end

        it 'does not update any custom fields' do
          @ticket.reload
          assert_equal('', @field_text_custom.value(@ticket))
          assert_equal('', @field_tagger_custom.value(@ticket))
        end
      end
    end

    describe "#custom_fields_for_sync" do
      it "returns an array with active custom fields" do
        @shared_ticket.custom_fields_for_sync.sort.must_equal [
          ["SSN", ""],
          ["Fun factor {{dc.welcome_wombat}}", ""],
          ["Product name", ""],
          ["Price", ""],
          ["Premium", ""],
          ["Date", ""],
          ["{{dc.welcome_wombat}}", ""],
          ["BAC", ""],
          ["More", ""],
          ["Age", ""]
        ].sort
      end

      it "does not include FieldPartialCreditCard custom fields" do
        field = FieldPartialCreditCard.create!(
          title: 'Credit Card',
          account: accounts(:minimum),
          is_active: true
        )

        refute_includes @shared_ticket.custom_fields_for_sync.map(&:first), field.title
      end
    end

    describe 'outbound' do
      before do
        @shared_ticket.stubs(custom_fields: {})
        @shared_ticket.stubs(custom_fields_for_sync: {'Product' => 'test value'})
      end

      describe 'with custom field sync on' do
        before { @agreement.stubs(sync_custom_fields?: true) }

        it 'adds custom fields to the ticket sharing ticket' do
          ticket_sharing_value = @shared_ticket.for_partner.custom_fields["zd_custom_fields"]

          assert_equal 'test value', ticket_sharing_value['Product']
        end
      end

      describe 'with custom field sync off' do
        before { @agreement.stubs(sync_custom_fields?: false) }

        it 'does not add custom fields to the ticket sharing ticket' do
          ticket_sharing_value = @shared_ticket.for_partner.custom_fields["zd_custom_fields"]

          assert_nil ticket_sharing_value
        end
      end
    end
  end

  describe 'Given a ticket with public and private comments' do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.add_comment(body: 'hello', is_public: false)
      @ticket.will_be_saved_by(@ticket.submitter)
      @ticket.save!
      assert @ticket.comments.any?(&:is_public?)
      assert @ticket.comments.any? { |comment| !comment.is_public? }
    end

    describe 'Given an outward agreement that does not allow public comments' do
      before do
        @agreement = FactoryBot.create(:agreement,
          direction: :out,
          allows_public_comments: false)
      end
      describe 'When sending a ticket' do
        before do
          @shared_ticket = SharedTicket.new(account: accounts(:minimum),
                                            ticket: @ticket, agreement: @agreement)
        end

        it 'includes all of the comments' do
          ts_ticket = @shared_ticket.for_partner
          assert_equal(@ticket.comments.size, ts_ticket.comments.size)
        end

        it 'does not include the status' do
          ts_ticket = @shared_ticket.for_partner
          assert_nil(ts_ticket.status)
        end
      end
    end

    describe 'Given an inward agreement that does not allow public comments' do
      before do
        @agreement = FactoryBot.create(:agreement,
          direction: :in,
          allows_public_comments: false)
      end

      describe 'When sending a ticket' do
        before do
          @shared_ticket = SharedTicket.new(account: accounts(:minimum),
                                            ticket: @ticket, agreement: @agreement)
        end

        it 'does not include the public comments' do
          ts_ticket = @shared_ticket.for_partner
          assert ts_ticket.comments.all? { |comment| !comment.public? }
        end

        it 'does not include the status' do
          ts_ticket = @shared_ticket.for_partner
          assert_nil(ts_ticket.status)
        end
      end
    end
  end

  describe 'Given an outward agreement that allows public comments' do
    before do
      @agreement = FactoryBot.create(:agreement,
        direction: :out,
        allows_public_comments: true)
    end

    describe 'Given a shared ticket' do
      before do
        @ticket = tickets(:minimum_1)
        @shared_ticket = SharedTicket.new(account: accounts(:minimum),
                                          ticket: @ticket, agreement: @agreement)
      end

      describe 'Given a payload with comments and a new status' do
        before do
          @ts_ticket = new_ticket_sharing_ticket('status' => 'pending')
        end

        describe 'setup from partner' do
          before do
            @shared_ticket.from_partner(@ts_ticket)
            assert @shared_ticket.save!
          end
          it 'syncs status' do
            assert_equal('Pending', @shared_ticket.ticket.status)
          end
        end
      end

      describe 'Given a payload with comments and an "On-Hold" status' do
        before do
          @ts_ticket = new_ticket_sharing_ticket('status' => 'hold')
        end

        describe 'setup from partner' do
          before do
            @shared_ticket.from_partner(@ts_ticket)
            assert @shared_ticket.save!
          end
          it 'syncs status as Open' do
            assert_equal('Open', @shared_ticket.ticket.status)
          end
        end
      end
    end
  end

  describe "Given a shared_ticket that is soft deleted" do
    before do
      @ticket = tickets(:minimum_1)
      @agreement = FactoryBot.create(:agreement, sync_custom_fields: true)
      @shared_ticket = SharedTicket.new(account: accounts(:minimum), ticket: @ticket, agreement: @agreement)
    end

    it "fails to soft_delete on subsequent soft_deletions" do
      assert @shared_ticket.soft_delete!
      assert_raise(ActiveRecord::RecordNotSaved, 'invalid to soft_delete! again') { @shared_ticket.soft_delete! }
    end
  end

  describe "Given a ticket with a deleted ticket" do
    before do
      @ticket = tickets(:minimum_1)
      @agreement = FactoryBot.create(:agreement, sync_custom_fields: true)
      @shared_ticket = SharedTicket.new(account: accounts(:minimum), ticket: @ticket, agreement: @agreement)
      @shared_ticket.ticket = nil
    end

    [:update_partner, :send_to_partner, :unshare].each do |method|
      describe "when calling #{method}" do
        it "nevers do any ticket sharing actions" do
          TicketSharing::Ticket.any_instance.expects(:send_to).never
          TicketSharing::Ticket.any_instance.expects(:update_partner).never
          TicketSharing::Ticket.any_instance.expects(:unshare).never
          @shared_ticket.send(method)
        end
      end
    end
  end

  describe "Given a new shared ticket with a non-payload (because Jira)" do
    before do
      @agreement = FactoryBot.create(:agreement)
      @shared_ticket = SharedTicket.new(account: accounts(:minimum), agreement: @agreement, uuid: "foobar")
    end

    it "does not blow up" do
      ts_ticket = TicketSharing::Ticket.parse('{"uuid":"foobar"}')
      refute @shared_ticket.from_partner(ts_ticket)
      assert_equal "Unable to create ticket for foobar, Enter your request: cannot be blank", @shared_ticket.error_message
    end
  end

  describe "Given a ticket with a soft_deleted ticket" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.soft_delete!
      @agreement = FactoryBot.create(:agreement, sync_custom_fields: true)
      @shared_ticket = SharedTicket.new(account: accounts(:minimum), ticket: @ticket, agreement: @agreement)
      TicketSharing::Client.any_instance.stubs(:post).returns(TicketSharing::FakeHttpResponse.new(200, "body"))
      TicketSharing::Client.any_instance.stubs(:put).returns(TicketSharing::FakeHttpResponse.new(200, "body"))
      TicketSharing::Client.any_instance.stubs(:delete).returns(TicketSharing::FakeHttpResponse.new(200, "body"))
    end

    [:update_partner, :send_to_partner, :unshare].each do |method|
      describe "when calling #{method}" do
        it "does not raise an exception" do
          @shared_ticket.send(method)
        end
      end
    end
  end

  describe 'Given an outward agreement that does not allow public comments' do
    before do
      @agreement = FactoryBot.create(:agreement,
        direction: :out,
        allows_public_comments: false)
    end

    describe 'Given a shared ticket' do
      before do
        @ticket = tickets(:minimum_1)
        @old_comments = @ticket.comments.size
        @shared_ticket = SharedTicket.new(account: accounts(:minimum), agreement: @agreement)
      end

      describe 'Given a payload with comments and a new status' do
        before do
          @ts_ticket = new_ticket_sharing_ticket('status' => 'pending')
          @ts_ticket.current_actor = new_ticket_sharing_actor('name' => 'Updater', 'role' => 'agent')
          @ts_ticket.comments << new_ticket_sharing_comment('author' => @ts_ticket.current_actor, 'public' => true)
          @ts_ticket.comments << new_ticket_sharing_comment('author' => @ts_ticket.current_actor, 'public' => false)
          @ts_ticket.comments << new_ticket_sharing_comment('author' => @ts_ticket.current_actor, 'public' => nil)

          assert_not_equal(@ticket.status.downcase, @ts_ticket.status.downcase)
        end

        describe 'setup from partner' do
          before do
            @shared_ticket.from_partner(@ts_ticket)
            assert @shared_ticket.reload.id
          end

          it 'stores only the private comments' do
            assert @shared_ticket.ticket.comments.all? { |comment| !comment.is_public? }

            private_comments = @ts_ticket.comments.count { |c| !c.public? }
            assert_equal(private_comments, @shared_ticket.ticket.comments.size)
          end

          it 'does not sync status' do
            assert_equal("New", @shared_ticket.ticket.status)
          end
        end

        describe 'update from partner' do
          before do
            # set the ticket status to solved to exercise method open_working_ticket_upon_end_user_update
            @ticket.status_id = StatusType.solved
            @ticket.will_be_saved_by(@ticket.assignee)
            @ticket.save!
            @shared_ticket.ticket = @ticket
            @shared_ticket.from_partner(@ts_ticket)
          end

          it 'stores only the private comments' do
            @ticket.reload
            new_comments = @ticket.comments[@old_comments..-1]
            private_comments = @ts_ticket.comments.count { |c| !c.public? }

            assert_equal(private_comments, new_comments.size)
            assert new_comments.all? { |comment| !comment.is_public? }
          end

          it 'does not change the status' do
            # especially status should not be reset to OPEN as if the comment was made by an end-user
            assert_equal('Solved', @ticket.status)
          end
        end

        describe 'closed ticket' do
          before do
            @ticket.force_status_change = true
            @ticket.status_id = StatusType.closed
            @ticket.will_be_saved_by(@ticket.requester)
            @ticket.save!
            @ticket.force_status_change = false

            @ts_ticket.comments << new_ticket_sharing_comment('public' => false)
            @shared_ticket.from_partner(@ts_ticket)
          end

          it 'does not change the status' do
            @ticket.reload
            assert_equal('Closed', @ticket.status)
          end
        end
      end
    end
  end

  describe 'Given a ticket that has been shared' do
    before do
      agreement = FactoryBot.create(:agreement, direction: :out)
      @ticket = tickets(:minimum_1)

      @ticket.add_comment(body: 'hi', is_public: false)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.save!
      refute @ticket.comments.last.is_public?

      attachment = create_attachment("#{Rails.root}/test/files/normal_1.jpg", users(:minimum_admin))
      @ticket.comments.last.attachments << attachment
      assert @ticket.comments.last.attachments.any?

      screencasts = [
        stub(for_partner: {position: "1", url: "http://foo.com/abd123", thumbnail: "foo", id: "567"}),
        stub(for_partner: {position: "2", url: "http://foo.com/abd456", thumbnail: "bar", id: "789"})
      ]
      Comment.any_instance.stubs(:screencasts).returns(screencasts)

      @shared_ticket = SharedTicket.new(
        account: accounts(:minimum),
        agreement: agreement,
        ticket: @ticket,
        uuid: 'dfd603dd'
      )
    end

    describe '#send_to_partner' do
      before do
        @url = @shared_ticket.agreement.remote_url + '/tickets/' + @shared_ticket.uuid
        stub_request(:post, @url)
      end

      it 'sends the ticket attributes' do
        @shared_ticket.send_to_partner
        assert_requested(:post, @url, body: hash_including(
          uuid:         @shared_ticket.uuid,
          subject:      @ticket.subject,
          requested_at: @ticket.created_at.strftime('%Y-%m-%d %H:%M:%S %z'),
          status:       "open",
          original_id:  @ticket.nice_id
        ))
      end

      it 'sends the requester information' do
        @shared_ticket.send_to_partner
        assert_requested(:post, @url, body: hash_including(
          requester: hash_including(
            uuid: @ticket.requester.generate_uuid,
            name: @ticket.requester.name,
            role: "user"
          )
        ))
      end

      it "sends all comments" do
        assert_difference "SharedComment.count(:all)", @shared_ticket.ticket.comments.count(:all) do
          @shared_ticket.send_to_partner
        end
      end

      it 'sends the comments' do
        @shared_ticket.send_to_partner
        comment_expectations = @ticket.comments.map do |comment|
          hash_including(
            uuid:   comment.shared_comment.uuid,
            body:   comment.body,
            public: comment.is_public?,
            author: hash_including(
              uuid: comment.author.generate_uuid,
              name: comment.author.name,
              role: comment.author.is_agent? ? "agent" : "user"
            )
          )
        end
        assert_requested(:post, @url, body: hash_including(
          comments: comment_expectations
        ))
      end

      it 'sends the attachments' do
        @shared_ticket.send_to_partner
        comment_expectations = @ticket.comments.map do |comment|
          hash_including(
            attachments: comment.attachments.map do |attachment|
              hash_including(url: attachment.url)
            end
          )
        end
        assert_requested(:post, @url, body: hash_including(
          comments: comment_expectations
        ))
      end

      it 'sends the screencasts' do
        @shared_ticket.send_to_partner
        comment_expectations = @ticket.comments.map do |_comment|
          hash_including(
            custom_fields: {
              zendesk: {
                metadata: {
                  screencasts: [
                    { position: "1", url: "http://foo.com/abd123", id: "567", thumbnail: "foo" },
                    { position: "2", url: "http://foo.com/abd456", id: "789", thumbnail: "bar" }
                  ]
                }
              }
            }
          )
        end
        assert_requested(:post, @url, body: hash_including(
          comments: comment_expectations
        ))
      end
    end

    describe 'And the ticket was already sent to partner' do
      before do
        # nothing changes upon sharing except shared_comments (atm)
        @ticket.comments.first.create_shared_comment
      end

      describe 'When #update_partner' do
        before do
          @url = @shared_ticket.agreement.remote_url + '/tickets/' + @shared_ticket.uuid
          stub_request(:put, @url)
        end

        it "shares all new/unset comments" do
          assert_difference "SharedComment.count(:all)", 3 do
            @shared_ticket.update_partner
          end
        end

        it 'provides a current actor' do
          @shared_ticket.update_partner
          assert_requested(:put, @url, body: hash_including(
            current_actor: hash_including(name: @ticket.audits.last.author.name)
          ))
        end

        # These next two 'shoulds' are abbreviated copies of the tests above
        # in 'send_to_partner'
        it 'sends the ticket attributes' do
          @shared_ticket.update_partner
          assert_requested(:put, @url, body: hash_including(
            status: "open"
          ))
        end

        it 'sends the comments' do
          @shared_ticket.update_partner
          assert_requested(:put, @url, body: hash_including(
            comments: @ticket.comments.map do |comment|
              hash_including(body: comment.body)
            end
          ))
        end
      end

      describe 'When #update_partner returns a 410 Gone' do
        before do
          Resque.jobs.clear
          url = @shared_ticket.agreement.remote_url + '/tickets/' + @shared_ticket.uuid
          stub_request(:put, url).to_return(status: 410)

          @shared_ticket.update_partner
        end

        it 'deactivates the agreement' do
          assert @shared_ticket.agreement.inactive?
        end

        it 'does not enqueue a partner update' do
          assert_equal(0, Resque.jobs.size)
        end
      end
    end
  end

  describe 'Given a ticket that has been shared' do
    before do
      account = accounts(:minimum)
      agreement = FactoryBot.create(:agreement, account: account)
      @ticket = tickets(:minimum_1)
      @shared_ticket = account.shared_tickets.create!(ticket: @ticket,
                                                      agreement: agreement)
      @old_num_comments = @shared_ticket.ticket.comments.size

      # Setup ticket state to have already been shared and sent to partner
      @shared_ticket.send(:setup_shared_comments)
    end

    describe 'if the ticket is via_twitter?' do
      before do
        @ticket.update_column(:via_id, ViaType.TWITTER)
        @ticket.update_column(:requester_id, users(:minimum_end_user).id)
        @ticket.requester.identities.twitter.first.update_column(:value, 110542029)
        @old_num_events = @shared_ticket.ticket.events.size
        @data = {
          metadata: {
            external_id: "external_id",
            source: {
              external_id: "106686396330930",
              name: "Twitter handle"
            },
            text: "Outgoing text"
          },
          source_id: monitored_twitter_handles(:minimum_monitored_twitter_handle_1).id,
          via_id: @ticket.via_id
        }
      end

      describe 'updating that ticket with a shared comment' do
        before do
          stub_request(:get, "https://api.twitter.com/1.1/users/show.json?user_id=110542029").
            to_return(body: read_test_file('twitter_users_show_110542029_1.1.json'))
        end

        it 'sends out a tweet if the comment is public' do
          ts_ticket = @shared_ticket.send(:for_partner)
          ts_ticket.status = 'solved'
          ts_ticket.current_actor = new_ticket_sharing_actor('name' => 'Updater', 'role' => 'agent')
          ts_ticket.comments << new_ticket_sharing_comment(
            'uuid' => 'new_comment_uuid',
            'author' => ts_ticket.current_actor,
            'body' => 'The last comment',
            'public' => true
          )

          @data[:metadata][:text] = '@kbucklertest The last comment'
          ::Channels::Converter::ReplyInitiator.any_instance.expects(:reply)
          ::Channels::Converter::ReplyParametersValidator.any_instance.expects(:validate).returns(@data)

          assert @shared_ticket.from_partner(ts_ticket)

          tweet_event = @shared_ticket.ticket.events(true).select { |e| e.type == 'ChannelBackEvent' }[0]
          assert_not_nil tweet_event
          assert_equal tweet_event.value[:text], "@kbucklertest The last comment"
        end

        it 'does not send out a tweet if the comment is not public' do
          ts_ticket = @shared_ticket.send(:for_partner)
          ts_ticket.comments << new_ticket_sharing_comment(
            'uuid' => 'newer_comment_uuid',
            'author' => ts_ticket.current_actor,
            'body' => 'New last comment',
            'public' => false
          )
          @data[:metadata][:text] = 'New last comment'
          ::Channels::Converter::ReplyParametersValidator.any_instance.expects(:validate).never

          assert @shared_ticket.from_partner(ts_ticket)
          tweet_event = @shared_ticket.ticket.events(true).select { |e| e.type == 'Tweet' }[1]
          assert_nil tweet_event
        end

        it 'sends a DM if the ticket was created originally from a DM' do
          @ticket.update_column(:via_id, ViaType.TWITTER_DM)
          ts_ticket = @shared_ticket.send(:for_partner)
          ts_ticket.current_actor = new_ticket_sharing_actor('name' => 'Updater', 'role' => 'agent')
          ts_ticket.comments << new_ticket_sharing_comment(
            'uuid' => 'newer_comment_uuid',
            'author' => ts_ticket.current_actor,
            'body' => 'New last comment',
            'public' => true
          )

          @data[:metadata][:text] = 'New last comment'
          ::Channels::Converter::ReplyInitiator.any_instance.expects(:reply)
          ::Channels::Converter::ReplyParametersValidator.any_instance.expects(:validate).returns(@data)

          assert @shared_ticket.from_partner(ts_ticket)
          dm_event = @shared_ticket.ticket.events(true).select { |e| e.type == 'ChannelBackEvent' }[0]
          assert_not_nil dm_event
          assert_equal dm_event.value[:text], "New last comment"
        end
      end
    end

    describe 'if the ticket is via facebook?' do
      before do
        page = facebook_pages(:minimum_facebook_page_1)
        @ticket.stubs(:facebook_source).returns(page)
        @ticket.update_column(:via_id, ViaType.FACEBOOK_POST)
        @ticket.update_column(:requester_id, users(:minimum_end_user).id)
        @old_num_events = @shared_ticket.ticket.events.size
      end

      describe 'updating that ticket with a shared comment' do
        it 'facebooks if the comment is public' do
          ::Channels::Converter::ReplyInitiator.any_instance.expects(:reply)
          ::Channels::Converter::ReplyParametersValidator.any_instance.expects(:validate).returns(
            metadata: {
              external_id: "external_id",
              source: {
                external_id: '123123',
                name: "Facebook page"
              },
              text: "Outgoing text"
            },
            source_id: facebook_pages(:minimum_facebook_page_1).id,
            via_id: @ticket.via_id
          )
          ts_ticket = @shared_ticket.send(:for_partner)
          ts_ticket.status = 'solved'
          ts_ticket.current_actor = new_ticket_sharing_actor('name' => 'Updater', 'role' => 'agent')
          ts_ticket.comments << new_ticket_sharing_comment(
            'uuid' => 'new_comment_uuid',
            'author' => ts_ticket.current_actor,
            'body' => 'The last comment',
            'public' => true
          )

          assert @shared_ticket.from_partner(ts_ticket)

          event = @shared_ticket.ticket.events(true).select { |e| e.type == 'ChannelBackEvent' }[0]
          assert_not_nil event
          assert_match /Commented on.*Wall Post.*as Facebook Page/, event.to_s
        end

        it 'does not facebook if the comment is not public' do
          ::Channels::Converter::ReplyParametersValidator.any_instance.expects(:validate).never
          ts_ticket = @shared_ticket.send(:for_partner)
          ts_ticket.comments << new_ticket_sharing_comment(
            'uuid' => 'newer_comment_uuid',
            'author' => ts_ticket.current_actor,
            'body' => 'New last comment',
            'public' => false
          )
          assert @shared_ticket.from_partner(ts_ticket)
          refute @shared_ticket.ticket.events(true).any? { |e| e.type == 'ChannelBackEvent' }
        end
      end
    end

    describe 'updating that ticket' do
      before do
        ts_ticket = @shared_ticket.send(:for_partner)
        ts_ticket.status = 'solved'
        ts_ticket.current_actor = new_ticket_sharing_actor('name' => 'Updater', 'role' => 'agent')
        ts_ticket.comments << new_ticket_sharing_comment(
          'uuid' => 'new_comment_uuid',
          'author' => ts_ticket.current_actor,
          'body' => 'The last comment',
          'attachments' => [new_ticket_sharing_attachment]
        )

        assert @shared_ticket.from_partner(ts_ticket)
      end

      it 'updates its attributes' do
        assert_equal('Solved', @shared_ticket.ticket.status)
      end

      it 'sets the via type to ticket sharing' do
        assert_equal(ViaType.TICKET_SHARING, @ticket.audits.last.via_id)
      end

      it 'sets the via reference to the agreement' do
        assert_equal(@shared_ticket.agreement.id, @ticket.audits.last.via_reference_id)
      end

      it 'adds comment(s) that it doesnt know about' do
        assert_equal(@old_num_comments + 1, @shared_ticket.ticket.comments(true).size)
        assert_equal('The last comment', @shared_ticket.ticket.comments.last.body)

        assert_not_nil(@shared_ticket.ticket.comments.last.shared_comment)
        assert_equal('new_comment_uuid', @shared_ticket.ticket.comments.last.shared_comment.uuid)
      end

      it "stores the role of the comment's author" do
        @shared_ticket.reload
        assert @shared_ticket.ticket.comments.last.author.foreign_agent?
      end

      it 'creates attachments if necessary' do
        @shared_ticket.reload
        assert @shared_ticket.ticket.comments.last.attachments.present?
      end
    end

    describe 'And the ticket is closed' do
      before do
        @ticket.force_status_change = true
        @ticket.status_id = StatusType.closed
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!

        assert_equal(StatusType.closed, @ticket.status_id)
      end

      describe 'Trying to change the status via ticket sharing' do
        before do
          ts_ticket = @shared_ticket.send(:for_partner)
          ts_ticket.status = 'open'
          ## should not be able to update a ticket once it is closed
          assert_equal false, @shared_ticket.from_partner(ts_ticket)
        end

        it 'does not be allowed' do
          assert_equal(StatusType.closed, @ticket.status_id)
        end
      end
    end
  end

  describe 'Given a shared ticket' do
    before do
      agreement = FactoryBot.create(:agreement, status: :accepted)
      ticket = tickets(:minimum_1)
      @shared_ticket = ticket.shared_tickets.create!(agreement: agreement)
    end

    describe "soft deleting that ticket when the agreement is accepted" do
      before do
        Resque.jobs.clear
        @shared_ticket.soft_delete!
      end

      it 'enqueues an unshare job' do
        last_job = Resque.jobs.last
        expected = [@shared_ticket.account.id, :unshare, @shared_ticket.id]

        assert_equal(expected, last_job[:args][0..2])
        assert_equal("Sharing::TicketJob", last_job[:class])
      end
    end

    describe "soft deleting that ticket when the agreement is not accepted" do
      before do
        # should check all other status'?
        @shared_ticket.agreement.status = :pending
        Resque.jobs.clear
        @shared_ticket.soft_delete!
      end

      it 'does not enqueue an unshare job' do
        assert_equal(0, Resque.jobs.size)
      end
    end

    describe 'unsharing that ticket' do
      before do
        @remote_url = @shared_ticket.agreement.remote_url + '/tickets/' + @shared_ticket.uuid
        stub_request(:delete, @remote_url)

        @shared_ticket.unshare
      end

      it 'sends a delete request to our sharing partner' do
        assert_requested(:delete, @remote_url)
      end

      it 'softs delete the shared ticket when its done' do
        refute SharedTicket.find_by_id(@shared_ticket.id)
        assert SharedTicket.with_deleted { SharedTicket.find_by_id(@shared_ticket.id) }
      end
    end
  end

  describe 'Given a shared ticket under a deleted agreement' do
    before do
      agreement = FactoryBot.create(:agreement, status: :declined)
      ticket = tickets(:minimum_1)
      @shared_ticket = ticket.shared_tickets.create!(agreement: agreement)
      @remote_url = agreement.remote_url + '/tickets/' + @shared_ticket.uuid

      agreement.soft_delete!
      @shared_ticket.reload
      assert_nil(@shared_ticket.agreement)
    end

    describe 'unsharing that ticket' do
      before do
        stub_request(:delete, @remote_url)

        @shared_ticket.unshare
      end

      it 'sends a delete request to our sharing partner' do
        assert_requested(:delete, @remote_url)
      end

      it 'hards delete the shared ticket when its done' do
        refute SharedTicket.find_by_id(@shared_ticket.id)
      end
    end
  end

  describe 'Given a shared ticket with a different requester and submitter' do
    before do
      @submitter = new_ticket_sharing_actor('name' => 'submitter')
      @requester = new_ticket_sharing_actor('name' => 'requester')
      @ts_ticket = new_ticket_sharing_ticket('requester' => @requester)
      @ts_ticket.comments = [new_ticket_sharing_comment('author' => @submitter)]

      @account = accounts(:minimum)
      @agreement = FactoryBot.create(:agreement, account: @account)
    end

    describe 'When receiving the shared ticket' do
      before do
        @shared_ticket = @agreement.shared_tickets.build
        @shared_ticket.from_partner(@ts_ticket)
        @new_ticket = @shared_ticket.ticket.reload
      end

      should_change("The user count", by: 2) { User.count(:all) }

      it 'sets the requester and submitter and comment.author properly' do
        assert_equal(@requester.name, @new_ticket.requester.name)
        assert_equal(@submitter.name, @new_ticket.submitter.name)
        assert_equal(@new_ticket.submitter, @new_ticket.comments.first.author)
      end
    end
  end

  describe 'Given an existing foreign user' do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
      @uuid = @user.generate_uuid
      @user.identities << UserForeignIdentity.new(
        user: @user,
        value: @uuid,
        is_verified: true
      )
      @user.save!
    end

    describe 'When a shared ticket is setup with that user' do
      before do
        actor = TicketSharing::Actor.new(
          'uuid' => @uuid,
          'name' => 'New User'
        )

        @ts_ticket = TicketSharing::Ticket.new(
          'uuid' => 't1',
          'requester' => actor,
          'subject' => 'the shared ticket subject',
          'comments' => [new_ticket_sharing_comment('author' => actor)]
        )

        agreement = FactoryBot.create(:agreement, account: @account)
        @ticket = agreement.shared_tickets.build
        @ticket.from_partner(@ts_ticket)
      end

      should_not_change('user identity count') { UserIdentity.count(:all) }
      should_change("that users tickets", by: 1) { @user.tickets.count(:all) }

      it 'sets up the ticket properly' do
        ticket = @user.tickets.last
        assert_equal('the shared ticket subject', ticket.subject)
      end

      describe "Updating the ticket if the foreign user's name changes" do
        before do
          actor = TicketSharing::Actor.new(
            'uuid' => @uuid,
            'name' => 'Changed Name'
          )

          ts_ticket = @ticket.send(:for_partner)
          ts_ticket.status = 'solved'
          ts_ticket.current_actor = actor
          ts_ticket.comments << new_ticket_sharing_comment(
            'uuid' => 'new_comment_uuid',
            'author' => ts_ticket.current_actor,
            'body' => 'The last comment'
          )

          assert @ticket.from_partner(ts_ticket)
        end

        should_not_change('user identity count') { UserIdentity.count(:all) }

        it "changes the user's name" do
          assert_equal 'Changed Name', @user.reload.name
        end
      end
    end
  end

  describe 'Given a user identity this system does not know about' do
    before do
      actor = TicketSharing::Actor.new(
        'uuid' => 'a1',
        'name' => 'New User'
      )

      @ts_ticket = TicketSharing::Ticket.new(
        'uuid' => 't1',
        'requester' => actor,
        'comments' => [new_ticket_sharing_comment('author' => actor)]
      )

      account = accounts(:minimum)
      agreement = FactoryBot.create(:agreement, account: account)
      @shared_ticket = agreement.shared_tickets.build
    end

    describe 'setup from partner' do
      before do
        @shared_ticket.from_partner(@ts_ticket)
      end

      should_change("user identity count", by: 1) do
        UserForeignIdentity.count(:all)
      end

      it 'sets up the user and identity properly' do
        identity = UserForeignIdentity.last
        assert_equal('a1', identity.value)
        assert_equal('New User', identity.user.name)
        assert identity.user.foreign?
      end

      should_change("the number of Tickets", by: 1) { Ticket.count(:all) }
      it 'associates the ticket to the shared ticket' do
        assert_equal(Ticket.last, @shared_ticket.ticket)
      end
    end
  end

  describe '#allows_comment_visibility_toggle' do
    it 'does not blow up when no agreement is present' do
      shared_ticket = SharedTicket.new
      shared_ticket.allows_comment_visibility_toggle?
    end

    it 'allows senders to publicly comment on deactivated agreements' do
      agreement = FactoryBot.create(:agreement, direction: :out)
      ticket = tickets(:minimum_1)
      shared_ticket = SharedTicket.create(agreement: agreement, ticket: ticket)

      assert shared_ticket.allows_comment_visibility_toggle?
      agreement.soft_delete!
      shared_ticket.reload
      assert shared_ticket.allows_comment_visibility_toggle?
    end
  end

  describe 'soft deletion' do
    before do
      @agreement = FactoryBot.create(:agreement)
      @ticket = tickets(:minimum_1)
      @shared_ticket = @ticket.shared_tickets.create!(agreement: @agreement)
      @shared_ticket.soft_delete!
    end

    it 'softs delete a shared ticket' do
      assert @shared_ticket.deleted?
    end

    it 'allows the creation of a new shared ticket' do
      assert(@shared_ticket = @ticket.shared_tickets.create!(agreement: @agreement))
    end
  end

  describe "#retrieve_jira_ticket" do
    before do
      remote_host    = "https://test.com"
      agreement      = FactoryBot.build(:agreement, remote_url: remote_host, uuid: "uuid")
      ticket         = tickets(:minimum_1)
      @shared_ticket = SharedTicket.create(agreement: agreement, ticket: ticket)
      @remote_url    = "#{remote_host}/tickets/#{@shared_ticket.uuid}"
    end

    describe "error handling" do
      it "catchs all on generic exceptions and log these" do
        ZendeskExceptions::Logger.expects(:record).once
        stub_request(:get, @remote_url).to_raise(StandardError)
        @shared_ticket.retrieve_jira_ticket
      end

      it "shoulds ignore bad response errors" do
        ZendeskExceptions::Logger.expects(:record).never
        stub_request(:get, @remote_url).to_raise(Net::HTTPBadResponse)
        @shared_ticket.retrieve_jira_ticket
      end
    end

    it "returns parsed JSON" do
      stub_request(:get, @remote_url).to_return(body: { foo: "bar" }.to_json)
      assert_equal({ "foo" => "bar" }, @shared_ticket.retrieve_jira_ticket)
    end

    it "returns parsed json on http" do
      @shared_ticket.agreement.update_attribute(:remote_url, @shared_ticket.agreement.remote_url.sub('https', 'http'))
      stub_request(:get, @remote_url.sub('https', 'http')).to_return(body: { foo: "bar" }.to_json)
      assert_equal({ "foo" => "bar" }, @shared_ticket.retrieve_jira_ticket)
    end

    it "follows redirects" do
      redirect_url = @remote_url + 'anything'

      stub_request(:get, @remote_url).to_return(status: 301, headers: { location: redirect_url })
      stub_request(:get, redirect_url).to_return(body: {"foo" => "bar"}.to_json)

      assert_equal({ "foo" => "bar" }, @shared_ticket.retrieve_jira_ticket)
    end
  end

  describe 'Given a shared ticket with a comment with screencasts' do
    before do
      screencasts = [{
        "position"  => "1",
        "url"       => "http://bar.foo/embed/abcefg",
        "id"        => "9999",
        "thumbnail" => "bar"
      }]

      ts_ticket = new_ticket_sharing_ticket

      comment = new_ticket_sharing_comment(
        'custom_fields' => {'zendesk' => {'metadata' => {'screencasts' => screencasts}}}
      )
      ts_ticket.comments << comment

      agreement = FactoryBot.create(:agreement, account: accounts(:minimum))
      @shared_ticket = agreement.shared_tickets.build
      @shared_ticket.from_partner(ts_ticket)
    end

    it 'Creates a Ticket with a comment with screencasts' do
      assert_equal(1, @shared_ticket.ticket.reload.comments.last.screencasts.size)
    end
  end

  describe 'When updating a ticket with a new comment with a screencats' do
    before do
      @ticket = tickets(:minimum_1)
      agreement = FactoryBot.create(:agreement, account: accounts(:minimum))
      @shared_ticket = SharedTicket.new(
        account: accounts(:minimum),
        ticket: @ticket,
        agreement: agreement
      )

      screencasts = [{
        "position"  => "1",
        "url"       => "http://bar.foo/embed/abcefg",
        "id"        => "9999",
        "thumbnail" => "bar"
      }]

      @ts_ticket = new_ticket_sharing_ticket
      comment = new_ticket_sharing_comment(
        'custom_fields' => {'zendesk' => {'metadata' => {'screencasts' => screencasts}}}
      )
      @ts_ticket.comments << comment

      @shared_ticket.from_partner(@ts_ticket)
    end

    it 'adds a new comment with a screencasts' do
      assert_not_nil(@shared_ticket.ticket.reload.comments.last.screencasts.last)
    end

    it 'adds a new comment with a screencasts with correct position' do
      assert_equal("1",
        @shared_ticket.ticket.reload.comments.last.screencasts.last.position)
    end

    it 'adds a new comment with a screencasts with correct url' do
      assert_equal("http://bar.foo/embed/abcefg",
        @shared_ticket.ticket.reload.comments.last.screencasts.last.url)
    end

    it 'adds a new comment with a screencasts with correct id' do
      assert_equal("9999",
        @shared_ticket.ticket.reload.comments.last.screencasts.last.id)
    end

    it 'adds a new comment with a screencasts with correct thumbnail' do
      assert_equal("bar",
        @shared_ticket.ticket.reload.comments.last.screencasts.last.thumbnail)
    end
  end

  describe "Tickets with status hold" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.status_id = StatusType.HOLD
      @shared_ticket = SharedTicket.new(ticket: @ticket, account: accounts(:minimum))
      @agreement = Sharing::Agreement.new

      @agreement.stubs(:account).returns(stub(
        sharing_name: "huh",
        sharing_url: "foo",
        has_ticket_sharing_whitelist?: false
      ))
      @shared_ticket.stubs(:safe_agreement).returns(@agreement)

      @ticket_sharing_ticket = @shared_ticket.for_partner
    end

    it "reports the ticket's status as OPEN" do
      assert_equal "open", @ticket_sharing_ticket.status
    end
  end

  describe "Shared ticket status handling" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.status_id = StatusType.NEW
      @shared_ticket = SharedTicket.new(ticket: @ticket)
      @agreement = Sharing::Agreement.new

      @agreement.stubs(:account).returns(stub(
        sharing_name: "huh",
        sharing_url: "foo",
        allows_public_comments?: true
      ))
      @shared_ticket.stubs(:agreement).returns(@agreement)

      @ticket_sharing_ticket = TicketSharing::Ticket.new(
        uuid: '1234',
        subject: 'subject',
        requested_at: Time.now,
        original_id: @ticket.nice_id
      )
    end

    describe "when the incoming update as status=hold" do
      before do
        @ticket_sharing_ticket.status = "hold"
        @shared_ticket.from_partner(@ticket_sharing_ticket)
      end

      it "sets the associated ticket's status to 'open'" do
        assert_equal StatusType.OPEN, @ticket.status_id
      end
    end

    describe "when the incoming update as status=open" do
      before do
        @ticket_sharing_ticket.status = "hold"
        @shared_ticket.from_partner(@ticket_sharing_ticket)
      end

      it "sets the associated ticket's status to 'open'" do
        assert_equal StatusType.OPEN, @ticket.status_id
      end
    end

    describe "when the incoming update as status=solved" do
      before do
        @ticket_sharing_ticket.status = "solved"
        @shared_ticket.from_partner(@ticket_sharing_ticket)
      end

      it "sets the associated ticket's status to 'solved'" do
        assert_equal StatusType.SOLVED, @ticket.status_id
      end
    end
  end

  describe "Given an account with ssl whitelisting" do
    before do
      @account = accounts(:minimum)
      @ticket = tickets(:minimum_1)
      @agreement = valid_agreement(account: @account)
      @shared_ticket = SharedTicket.new(
        account: @account,
        ticket: @ticket,
        agreement: @agreement
      )
      @account.stubs(:has_ticket_sharing_whitelist?).returns(true)
    end

    describe "When building a ticket for our partner" do
      before do
        # some other test here stubs on TicketSharing::Ticket, which changes first_ancestor
        # to reproduce: run the whole file without this patch
        TicketSharing::Ticket.expects(:first_ancestor).returns(TicketSharing::Base)

        @ticket_sharing_ticket = @shared_ticket.for_partner
      end

      it "uses an ticket that does not verify ssl certificates" do
        assert_equal TicketSharing::NonVerifyTicket, @ticket_sharing_ticket.class
      end
    end
  end

  describe "#handle_network_errors" do
    before do
      @account = accounts(:minimum)
      @agreement = valid_agreement(account: @account, remote_url: 'http://dev.zendesk.com/sharing')
      @ticket = tickets(:minimum_1)
      @shared_ticket = SharedTicket.new(account: @account, ticket: @ticket, agreement: @agreement)
    end

    it "returns 499 when ticket is unavailable" do
      @shared_ticket.expects(:for_partner).returns(nil)
      assert_equal 499, @shared_ticket.send_to_partner
    end

    [
      {exception: :redirect_limit, response: 404, status: :configuration_error},
      {exception: :restricted_ip, response: 404, status: :configuration_error},
      {exception: SocketError.new("getaddrinfo: Temporary failure in name resolution"), response: 408, status: :pending, check_retries: true},
      {exception: SocketError.new("getaddrinfo: Name or service not known"), response: 408, status: :pending, check_retries: true},
      {exception: Net::OpenTimeout, response: 408, status: :pending, check_retries: true},
      {exception: Errno::ETIMEDOUT, response: 408, status: :pending, check_retries: true},
      {exception: Errno::ECONNRESET, response: 408, status: :pending, check_retries: true},
      {exception: Errno::EHOSTUNREACH, response: 408, status: :pending, check_retries: true},
      {exception: Faraday::TimeoutError, response: 408, status: :pending, check_retries: true},
      {exception: Faraday::Error::TimeoutError, response: 408, status: :pending, check_retries: true},
      {exception: OpenSSL::SSL::SSLError, response: 412, status: :ssl_error},
      {exception: :internal_sharing_warning, response: 405, status: :pending},
      {exception: :internal_sharing_warning, response: 422, status: :pending},
      {exception: :internal_sharing_warning, response: 422, status: :pending},
      {exception: :internal_sharing_error, response: 401, status: :configuration_error},
      {exception: :internal_sharing_error, response: 503, status: :configuration_error}
    ].each do |config|
      describe "with #{config.inspect}" do
        before do
          config[:exception] =
            case config[:exception]
            when :redirect_limit
              FaradayMiddleware::RedirectLimitReached.new("too many redirects; last one to: /notfound")
            when :restricted_ip
              Faraday::RestrictIPAddresses::AddressNotAllowed.new("Address not allowed for https://whatever")
            when :internal_sharing_warning
              TicketSharing::InternalSharingWarning.new(config[:response])
            when :internal_sharing_error
              TicketSharing::InternalSharingError.new(config[:response])
            else
              config[:exception]
            end
          Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(config[:exception])
        end

        [:send_to_partner, :update_partner, :unshare].each do |method|
          it "handles #{method}" do
            if config[:check_retries]
              ZendeskExceptions::Logger.expects(:record).once
              Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES)
            end

            assert_equal config[:response], @shared_ticket.send(method)

            assert_equal config[:status], @agreement.status
          end
        end
      end
    end
  end

  describe "Given an OpenSSL::SSL::SSLError" do
    before do
      @account = accounts(:minimum)
      @agreement = create_valid_agreement(account: @account, shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk])
      @ticket = tickets(:minimum_1)
      @shared_ticket = SharedTicket.new(
        account: @account,
        ticket: @ticket,
        agreement: @agreement
      )
      Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(OpenSSL::SSL::SSLError)
    end

    describe "on an agreement that is shared with zendesk" do
      describe "during send_to_partner" do
        it "changes the remote url to https" do
          refute @agreement.remote_url =~ /https/
          @shared_ticket.send_to_partner
          @agreement.reload
          assert_equal 0, @agreement.remote_url =~ /https/
        end
      end

      describe "during update_partner" do
        it "changes the remote url to https" do
          refute @agreement.remote_url =~ /https/
          @shared_ticket.update_partner
          @agreement.reload
          assert_equal 0, @agreement.remote_url =~ /https/
        end
      end

      describe "during unshare" do
        it "changes the remote url to https" do
          refute @agreement.remote_url =~ /https/
          @shared_ticket.unshare
          @agreement.reload
          assert_equal 0, @agreement.remote_url =~ /https/
        end
      end

      describe "with an ssl error" do
        it "doesn't change the agreement status" do
          @shared_ticket.send_to_partner
          @agreement.reload
          assert_equal :pending, @agreement.status
        end

        it "adds an error event to the ticket and doesn't re-share" do
          @shared_ticket.expects(:enqueue_job).never
          @shared_ticket.send_to_partner
          @ticket.reload
          assert_includes @ticket.audits.last.events.to_a.to_s, 'ticket_fields.ticket_sharing.ssl_failure'
        end
      end
    end
  end

  describe "Given a Faraday::RestrictIPAddresses::AddressNotAllowed" do
    before do
      @account = accounts(:minimum)
      @agreement = create_valid_agreement(account: @account, shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk])
      @agreement.remote_url = @agreement.remote_url.sub(/^http:/, "https:")
      @agreement.save!
      @ticket = tickets(:minimum_1)
      @shared_ticket = SharedTicket.new(
        account: @account,
        ticket: @ticket,
        agreement: @agreement
      )
      Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(Faraday::RestrictIPAddresses::AddressNotAllowed.new("Address not allowed for https://whatever"))
    end

    describe "on an agreement that is shared with zendesk" do
      it "doesn't change the agreement status" do
        @shared_ticket.send_to_partner
        @agreement.reload
        assert_equal :pending, @agreement.status
      end

      it "adds an error event to the ticket and doesn't re-share" do
        @shared_ticket.expects(:enqueue_job).never
        @shared_ticket.send_to_partner
        @ticket.reload
        assert_includes @ticket.audits.last.events.to_a.to_s, 'ticket_fields.ticket_sharing.dns_failure'
      end

      it "does not try to update closed tickets" do
        @ticket.expects(:closed?).returns(true)
        @ticket.expects(:add_translatable_error_event).never
        @ticket.expects(:save!).never
        @shared_ticket.send_to_partner
      end

      describe "and the other zendesk is closed" do
        before do
          msg = "Address not allowed for #{PageNotFoundRenderingSupport::MARKETING_URL}"
          Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(Faraday::RestrictIPAddresses::AddressNotAllowed.new(msg))
        end

        it "deactivates the agreement" do
          @shared_ticket.send_to_partner
          @agreement.reload
          assert_equal :configuration_error, @agreement.status
        end
      end
    end
  end

  describe "Given an internal authentication error" do
    before do
      @account = accounts(:minimum)
      @agreement = create_valid_agreement(account: @account, shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:zendesk])
      @agreement.save!
      @ticket = tickets(:minimum_1)
      @shared_ticket = SharedTicket.new(
        account: @account,
        ticket: @ticket,
        agreement: @agreement
      )
      Zendesk::Net::MultiSSL.stubs(:http_request_with).raises(TicketSharing::InternalSharingError.new(500))
    end

    describe "on an agreement that is zendesk to zendesk" do
      it "retries the specified number of times" do
        @agreement.expects(:failed?).times(Sharing::Agreement::MAX_INTERNAL_ATTEMPTS)
        @shared_ticket.send_to_partner
      end

      it "deactivates the agreement after the fail threshold is reached" do
        @shared_ticket.send_to_partner
        @agreement.reload
        assert_equal :configuration_error, @agreement.status
      end
    end
  end

  describe "#ticket" do
    let(:agreement) { create_valid_agreement }
    let(:archived_ticket) do
      t = tickets(:minimum_5)
      archive_and_delete(t)
      t
    end
    let(:shared_ticket) do
      SharedTicket.create!(
        ticket: archived_ticket,
        agreement: agreement,
        uuid: '123ABC',
        original_id: 123
      )
    end

    it "returns the archived ticket" do
      assert_equal archived_ticket.id, shared_ticket.reload.ticket.id
    end
  end
end
