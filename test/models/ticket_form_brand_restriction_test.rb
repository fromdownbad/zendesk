require_relative "../support/test_helper"

SingleCov.covered!

describe TicketFormBrandRestriction do
  fixtures :accounts, :brands, :account_settings

  let(:account) { accounts(:minimum) }
  let(:brand)   { account.default_brand }
  let(:ticket_form) do
    ticket_form = TicketForm.init_default_form(account)
    ticket_form.save!

    ticket_form
  end
  let(:ticket_form_brand_restriction) do
    TicketFormBrandRestriction.new do |tfbr|
      tfbr.account = account
      tfbr.brand = brand
      tfbr.ticket_form = ticket_form
    end
  end

  subject { ticket_form_brand_restriction }
  should_validate_presence_of :brand, :account, :ticket_form
  should validate_uniqueness_of(:ticket_form_id).scoped_to(:brand_id)

  it "sets the account to the brand's account" do
    ticket_form_brand_restriction.account = accounts(:support)

    ticket_form_brand_restriction.valid?

    assert ticket_form_brand_restriction.account
    assert_equal ticket_form_brand_restriction.account, brand.account
  end

  it "sets the appropriate error message if ticket form and brand have a different account" do
    ticket_form_brand_restriction.ticket_form.account = accounts(:support)
    ticket_form_brand_restriction.valid?

    assert_match(/must.*same account/, ticket_form_brand_restriction.errors[:ticket_form].join)
  end
end
