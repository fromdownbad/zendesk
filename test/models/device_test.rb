require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'Device' do
  fixtures :accounts, :users, :devices

  describe "#Device" do
    before do
      Zendesk::GeoLocation.stubs(:locate).returns(city: 'Oconto', country: 'USA')
      @account = accounts(:minimum)
      @user    = users(:minimum_agent)
      agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 Safari/537.31'
      @params = {ip: '1.2.3.4', user_agent: agent}
    end

    describe 'creating a device' do
      it "throws validation error if user is not set" do
        assert_raise(ActiveRecord::RecordInvalid) do
          Device.create!(@params.merge(account: @account))
        end
      end

      it "sets last_active_at" do
        device = Device.new(@params.merge(account: @account))
        device.user = @user
        device.save!
        assert_not_nil device.last_active_at
      end

      it "handles invalid utf8 in the user agent" do
        device = Device.create!(@params.merge(user_agent: "hi \xAD")) do |d|
          d.user = @user
          d.account = @account
        end

        assert_equal "hi �", device.user_agent
      end
    end

    describe '#register_activity!' do
      before do
        @device = devices(:minimum)
      end

      describe 'when there has not been recent activity' do
        before do
          Timecop.travel(1.day.from_now)
        end

        it 'touchs without any changes' do
          Zendesk::GeoLocation.stubs(:locate).returns(nil)
          @device.expects(:update_activity!)
          @device.register_activity!
        end

        it 'saves with changes' do
          @device.expects(:save)
          @device.register_activity!
        end
      end

      describe 'when there has been recent activity' do
        before do
          Timecop.travel(1.minute.from_now)
        end

        it 'does not register activity' do
          @device.expects(:touch).never
          @device.register_activity!
        end
      end
    end

    describe 'geolocation' do
      it "is local machine in development" do
        Device.any_instance.stubs(:force_local_machine?).returns(true)

        device = @user.devices.create!(@params.merge(account: @account))
        assert_equal 'Local Machine', device.location
      end

      it "sets the device location" do
        device = @user.devices.create!(@params.merge(account: @account))
        assert_equal 'Oconto, USA', device.location
      end

      it "presents a city without country" do
        Zendesk::GeoLocation.stubs(:locate).returns(city: 'Oconto')
        device = @user.devices.create!(@params.merge(account: @account))
        assert_equal 'Oconto', device.location
      end

      it "presents a country without city" do
        Zendesk::GeoLocation.stubs(:locate).returns(country: 'USA')
        device = @user.devices.create!(@params.merge(account: @account))
        assert_equal 'USA', device.location
      end

      it "updates location on any device update" do
        device = @user.devices.create!(@params.merge(account: @account))
        device.stubs(:throttled?).returns(false)
        Zendesk::GeoLocation.stubs(:locate).returns(city: 'Middelfart', country: 'Denmark')
        device.register_activity!
        assert_equal 'Middelfart, Denmark', device.reload.location
      end

      it "handles a failed gelocation" do
        Zendesk::GeoLocation.stubs(:locate).returns(nil)
        device = @user.devices.create!(@params.merge(account: @account))
        assert_nil device.location
      end
    end

    describe 'name extraction' do
      describe 'on creation' do
        it "parses the useragent" do
          device = @user.devices.create!(@params.merge(account: @account, token: 'abc123'))
          assert_equal 'Macintosh, Chrome', device.reload.name
        end
      end

      describe 'on update' do
        before do
          @device = @user.devices.create!(@params.merge(account: @account))
        end

        it 'does not update activity' do
          timestamp = @device.last_active_at
          @device.update_attribute(:name, 'Work Phone')
          assert_equal timestamp.to_s, @device.reload.last_active_at.to_s
        end

        it 'does not overwrite a name' do
          @device.update_attribute(:name, 'Work Phone')
          @device.update_attribute(:ip, '1.2.3.4')
          assert_equal 'Work Phone', @device.reload.name
        end

        it 'parses over an empty name' do
          @device.update_attribute(:name, '')
          assert_equal 'Macintosh, Chrome', @device.reload.name
        end

        it 'parses over a nil name' do
          @device.update_attribute(:name, nil)
          assert_equal 'Macintosh, Chrome', @device.reload.name
        end
      end
    end

    describe 'token generation' do
      it "allows token assignment" do
        device = @user.devices.create!(@params.merge(account: @account, token: 'abc123'))
        assert_equal 'abc123', device.token
      end

      it "generates a token" do
        device = @user.devices.create!(@params.merge(account: @account))
        assert device.token.present?
      end

      it 'does not allow duplicate tokens' do
        device = @user.devices.create!(@params.merge(account: @account))
        device2 = @user.devices.create!(@params.merge(account: @account))
        assert_raise(ActiveRecord::RecordInvalid) do
          device2.token = device.token
          device2.save!
        end
      end
    end

    describe '#mobile?' do
      before do
        @device = devices(:minimum)
      end

      it 'is false' do
        refute @device.mobile?
      end
    end
  end

  describe "NilDevice" do
    before do
      @device = NilDevice.new
    end

    it "responds to register_activity!" do
      assert @device.respond_to?(:register_activity!)
    end

    describe '#mobile?' do
      it 'is false' do
        refute @device.mobile?
      end
    end
  end
end
