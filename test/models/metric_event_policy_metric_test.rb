require_relative '../support/test_helper'
require_relative '../support/ticket_metric_helper'

SingleCov.covered!

describe MetricEventPolicyMetric do
  include TicketMetricHelper

  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }

  let(:policy)  { new_sla_policy }
  let(:policy_metric) do
    new_sla_policy_metric(
      policy: policy,
      priority_id: PriorityType.LOW,
      metric_id: Zendesk::Sla::TicketMetric::FirstReplyTime.id,
      target: 10
    )
  end

  let(:metric_event) do
    TicketMetric::ApplySla.create(
      account: ticket.account,
      ticket: ticket,
      metric: 'agent_work_time',
      time: Time.now
    )
  end

  before do
    metric_event.create_metric_event_policy_metric(
      account: ticket.account,
      policy_metric: policy_metric
    )
  end
end
