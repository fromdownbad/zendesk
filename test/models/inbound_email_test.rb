require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe InboundEmail do
  fixtures :users, :accounts

  let(:account) { accounts(:minimum) }
  let!(:inbound) { InboundEmail.create!(from: 'from@example.com', message_id: '<12345@example.com>', account: account) }

  describe "pre validation truncation" do
    let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
    let(:message_id_limit) { Zendesk::Extensions::TruncateAttributesToLimit.limit(InboundEmail.columns_hash.fetch('message_id')) }
    let(:from_limit) { Zendesk::Extensions::TruncateAttributesToLimit.limit(InboundEmail.columns_hash.fetch('from')) }
    let(:message_id) { 'a' * (message_id_limit + 1) + '@foo.com' }
    let(:from) { 'a' * (from_limit + 1) + '@example.com' }

    let(:inbound_email) do
      InboundEmail.new.tap do |inbound_email|
        inbound_email.account = account
        inbound_email.message_id = message_id
        inbound_email.from = from
      end
    end

    subject { inbound_email.valid? }

    it { subject; assert message_id_limit, inbound_email.message_id.length }

    it { subject; assert from_limit, inbound_email.from.length }

    it "logs metrics" do
      statsd_client.expects(:increment).with(:truncated_attribute, tags: ["model:InboundEmail", "attr:message_id"]).once
      statsd_client.expects(:increment).with(:truncated_attribute, tags: ["model:InboundEmail", "attr:from"]).once
      statsd_client.expects(:histogram).with(:truncated_attribute_length, message_id.size, tags: ["model:InboundEmail", "attr:message_id"]).once
      statsd_client.expects(:histogram).with(:truncated_attribute_length, from.size, tags: ["model:InboundEmail", "attr:from"]).once

      subject
    end

    describe "from_name and from_mail are under the limit" do
      let(:message_id) { 'a_message_id@foobar' }
      let(:from) { 'john@example.com' }

      it { subject; assert message_id, inbound_email.message_id }
      it { subject; assert from, inbound_email.from }
      it { statsd_client.expects(:increment).never; subject }
      it { statsd_client.expects(:histogram).never; subject }
    end
  end

  describe "#failed?" do
    it "is failed when uncategorized for longer than 15 minutes" do
      assert_nil   inbound.category_id
      assert_equal false, inbound.failed?

      Timecop.travel(16.minutes.from_now) do
        assert(inbound.failed?)
      end
    end

    it "does not be failed when categorized within 15 minutes" do
      inbound.mark_as_ham!
      assert_equal false, inbound.failed?

      Timecop.travel(16.minutes.from_now) do
        assert_equal false, inbound.failed?
      end
    end
  end

  describe "#mark_as_ham!" do
    it "changes the category to ham on behalf of the user" do
      refute inbound.ham?
      inbound.mark_as_ham!
      assert inbound.ham?
    end
  end

  describe "#mark_as_spam!" do
    it "changes the category to spam on behalf of the user" do
      refute inbound.spam?
      inbound.mark_as_spam!
      assert inbound.spam?
    end
  end

  describe "#mark_by_system" do
    it "changes the category on behalf o the system user" do
      refute inbound.spam?
      inbound.mark_by_system(InboundEmail.categories[:spam])
      assert inbound.spam?
    end

    it "submits a stat with the right category" do
      assert_nil inbound.category_id
      inbound.mark_by_system(InboundEmail.categories[:spam])
    end
  end

  describe "#spam?" do
    it "querys if the email is spam" do
      refute inbound.spam?
      inbound.category_id = InboundEmail.categories[:spam]
      assert inbound.spam?
    end
  end

  describe "#ham?" do
    it "querys if the email is ham" do
      refute inbound.ham?
      inbound.category_id = InboundEmail.categories[:ham]
      assert inbound.ham?
    end
  end

  describe "#category_name" do
    it "is a human friendly name for suspension types" do
      inbound.category_id = InboundEmail.categories[:spam]
      assert_equal 'spam', inbound.category_name

      inbound.category_id = InboundEmail.categories[:ham]
      assert_equal 'ham', inbound.category_name
    end

    it "is a human friendly name for unknown categories" do
      inbound.category_id = nil
      assert_equal 'unknown', inbound.category_name
    end
  end

  describe "#references" do
    it "is empty" do
      assert_equal [], inbound.references
    end

    it "assigns empty" do
      inbound.references = []
      assert_equal [], inbound.references
    end

    it "assigns nil" do
      inbound.references = nil
      assert_equal [], inbound.references
    end

    it "assigns one" do
      inbound.references = ["<xxxyy@yyyxxx.com>"]
      assert_equal ["<xxxyy@yyyxxx.com>"], inbound.references
    end

    it "does not assign invalid" do
      inbound.references = ["<x@y>"]
      assert_equal [], inbound.references
    end

    it "does not assign multiple" do
      inbound.references = ["<valid@valid>", "<x@y>", "<alsovalid@alsovalid>"]
      assert_equal ["<valid@valid>", "<alsovalid@alsovalid>"], inbound.references
    end

    it "shoulds leave origin and newest when assigning too long value" do
      inbound.references = ["<valid@valid>", "<#{"a" * 500}@b.com>", "<alsovalid@alsovalid>", "<#{"b" * 500}@c.com>"]
      assert_equal ["<valid@valid>", "<alsovalid@alsovalid>", "<#{"b" * 500}@c.com>"], inbound.references
    end

    it "is exact" do
      fit = ["a" * 500, "b" * 500, "c" * 22] # 500 + 500 + 21 + 2 spaces = 1024
      inbound.references = fit
      assert_equal fit, inbound.references

      fit.last << "c"
      inbound.references = fit
      assert_equal ["a" * 500, "c" * 23], inbound.references
    end

    it "rejects individual items over column size but keep others" do
      inbound.references = ["<#{"a" * 1100}@b.com>", "<aaa@bbb.com>", "<#{"b" * 1100}@b.com>"]
      assert_equal ["<aaa@bbb.com>"], inbound.references
    end

    it "does not store duplicates" do
      inbound.references = ["<aaa@bbb.com>", "<ccc@ddd.com>", "<aaa@bbb.com>"]
      assert_equal ["<aaa@bbb.com>", "<ccc@ddd.com>"], inbound.references
    end
  end

  describe ".cleanup" do
    it "includes a limit in the statement" do
      sql = sql_queries { InboundEmail.cleanup }
      assert_includes sql.first, "LIMIT 1000"
    end

    it "includes a timestamp in the statement" do
      Timecop.freeze
      sql = sql_queries { InboundEmail.cleanup }
      assert_includes sql.first, "created_at < '#{InboundEmail.expiration.ago.to_s(:db)}'"
    end

    it "leaves a recent record untouched" do
      sql_queries { InboundEmail.cleanup }
      assert_equal 1, InboundEmail.count
    end

    it "deletes all records older than expiration days ago" do
      Timecop.freeze
      inbound.update_column(:created_at, 2.months.ago)
      ZendeskDatabaseSupport::SlaveDelay.any_instance.expects(:max_slave_delay)
      InboundEmail.expects(:sleep)
      sql = sql_queries { InboundEmail.cleanup }
      assert_equal 0, InboundEmail.count
      assert_equal 2, sql.size
    end
  end

  describe ".categories" do
    it "has categories that map to suspension types" do
      assert_equal SuspensionType.SPAM, InboundEmail.categories[:spam]
    end

    it "has a 'ham' category that designates an email wasn't suspended" do
      assert_equal 1000, InboundEmail.categories[:ham]
    end
  end
end
