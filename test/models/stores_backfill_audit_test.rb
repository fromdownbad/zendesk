require_relative "../support/test_helper"

SingleCov.covered!

describe StoresBackfillAudit do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:account_id) { accounts(:minimum).id }
  let(:region) { [:a] }

  # audit for attachment; does not initialize progress to be consistent on invocation
  let(:att_audit) do
    audit = StoresBackfillAudit.new_audit(account_id, Attachment.name, region)
    audit.last_id = audit.max_id = audit.total_evaluated = audit.total_updated = 0
    audit
  end

  def create_attachment(acct = account)
    attachment = Attachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/normal_1.jpg"))
    attachment.author = acct.users.last
    attachment.account = acct
    attachment.save!
    attachment
  end

  def create_voice_upload(acct = account)
    upload = Voice::Upload.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new('test/files/dtmf-1.mp3'))
    upload.account = acct
    upload.stubs(:ensure_valid_audio_file).returns(true)
    upload.save!
    upload
  end

  before do
    stub_request(:put, %r{\.amazonaws.com/data/.*})
    StoresBackfillAudit.delete_all(account_id: account.id)
    Attachment.delete_all(account_id: account.id)
    BrandLogo.delete_all(account_id: account.id)
    Voice::Upload.delete_all(account_id: account.id)
  end

  describe '.account_audits' do
    let(:model_regions) do
      { BrandLogo.name => [:s3], Attachment.name => [:s3], Voice::Upload.name => [:s3] }
    end

    let(:audits) { StoresBackfillAudit.account_audits(account.id, model_regions) }

    before do
      create_attachment
      create_attachment
      @last_attachment = create_attachment

      create_voice_upload
      @last_upload = create_voice_upload
    end

    it "will create new audits as needed" do
      assert_equal 3, audits.size

      audits.each do |audit|
        refute audit.new_record?
        refute audit.changed?
        assert_equal account.id, audit.account_id
        assert_equal [:s3], audit.region
        assert_equal [:s3], audit.region
        if audit.model_class == BrandLogo.name
          assert_equal 0, audit.max_id
          assert_equal 0, audit.last_id
        elsif audit.model_class == Attachment.name
          assert_equal Attachment.name, audit.model_class
          assert_equal @last_attachment.id, audit.max_id
          assert_equal audit.max_id + 1, audit.last_id
        else
          assert_equal Voice::Upload.name, audit.model_class
          assert_equal @last_upload.id, audit.max_id
          assert_equal audit.max_id + 1, audit.last_id
        end
        assert_equal 0, audit.total_evaluated
        assert_equal 0, audit.total_updated
      end
    end

    it "will return existing audits" do
      attaudit = audits.find { |a| a.model_class == Attachment.name }
      attaudit.update_progress(2, 1, attaudit.max_id - 2)

      audits.each(&:save!)
      attaudit.reload

      audits = StoresBackfillAudit.account_audits(account.id, model_regions)

      audits.each do |audit|
        refute audit.new_record?
        refute audit.changed?
        assert_equal [:s3], audit.region
      end

      attaudit = audits.find { |a| a.model_class == Attachment.name }

      assert_equal 2, attaudit.total_evaluated
      assert_equal 1, attaudit.total_updated
      assert_equal attaudit.max_id - 2, attaudit.last_id
    end

    it "will reset progress on region change" do
      audits = StoresBackfillAudit.account_audits(account.id, model_regions)

      attaudit = audits.find { |a| a.model_class == Attachment.name }
      attaudit.update_progress(2, 1, attaudit.last_id - 2)

      audits.each(&:save!)

      new_regions = model_regions.map { |mc, r| mc == Attachment.name ? [mc, [:s3eu]] : [mc, r] }.to_h

      audits = StoresBackfillAudit.account_audits(account.id, new_regions)

      assert_equal 3, audits.size

      audits.each do |audit|
        refute audit.new_record?
        refute audit.changed?
        if audit.model_class != Attachment.name
          assert_equal [:s3], audit.region
        end
      end

      attaudit = audits.find { |a| a.model_class == Attachment.name }
      assert_equal [:s3eu], attaudit.region
      assert_equal 0, attaudit.total_evaluated
      assert_equal 0, attaudit.total_updated
      assert_equal attaudit.max_id + 1, attaudit.last_id
    end

    it "will not reset progress if the region changed to a scoped store" do
      StoresBackfillAudit.account_audits(account.id, model_regions).each(&:save!)

      scoped_store = Technoweenie::AttachmentFu::ScopedStores.scope_store_name(:s3)
      new_regions = model_regions.map { |mc, _| [mc, [scoped_store]] }.to_h

      audits = StoresBackfillAudit.account_audits(account.id, new_regions)

      assert_equal 3, audits.size

      audits.each do |audit|
        refute audit.new_record?
        refute audit.changed?
        assert_equal [:s3], audit.region
      end
    end

    it "will not reset progress if the region changed to a scoped store" do
      scoped_store = Technoweenie::AttachmentFu::ScopedStores.scope_store_name(:s3)
      new_regions = model_regions.map { |mc, _| [mc, [scoped_store]] }.to_h

      StoresBackfillAudit.account_audits(account.id, new_regions).each(&:save!)

      audits = StoresBackfillAudit.account_audits(account.id, model_regions)

      assert_equal 3, audits.size

      audits.each do |audit|
        refute audit.new_record?
        refute audit.changed?
        assert_equal [scoped_store], audit.region
      end
    end
  end

  describe 'progress' do
    # logo_audit will initialize on instantiation; remember that let is lazy and memoizing
    let(:logo_audit) { StoresBackfillAudit.new_audit(account_id, BrandLogo.name, region) }
    let(:max_logo_id) { BrandLogo.where(account_id: account_id, parent_id: nil).maximum(:id) }

    before do
      Brand.any_instance.expects(:under_active_brands_limit).returns(true).at_least_once
      10.times do
        logo = FactoryBot.create(:brand_logo, filename: "small.png")
        logo.account_id = account_id
        logo.save!
      end

      assert max_logo_id >= 10, "there should be at least 10 logos"
    end

    it 'is initialized in new_audit' do
      assert_equal max_logo_id, logo_audit.max_id
      assert_equal max_logo_id + 1, logo_audit.last_id
      assert_equal 0, logo_audit.total_evaluated
      assert_equal 0, logo_audit.total_updated
    end

    it 'will initialize zeroed out if no associated records' do
      new_audit = StoresBackfillAudit.new_audit(0, BrandLogo.name, region)
      assert_equal 0, new_audit.max_id
      assert_equal 0, new_audit.last_id
      assert_equal 0, new_audit.total_evaluated
      assert_equal 0, new_audit.total_updated
    end

    it 'is updatable' do
      logo_audit.update_progress(1, 0, max_logo_id)

      assert_equal max_logo_id, logo_audit.last_id
      assert_equal 1, logo_audit.total_evaluated
      assert_equal 0, logo_audit.total_updated
    end

    it 'can be marked as done' do
      logo_audit.done!
      assert logo_audit.done?
      assert_equal 0, logo_audit.last_id
    end

    it 'updates cumulatively' do
      logo_audit.update_progress(1, 0, max_logo_id)
      logo_audit.update_progress(3, 1, max_logo_id - 5)
      logo_audit.update_progress(2, 2, max_logo_id - 8)

      assert_equal max_logo_id - 8, logo_audit.last_id
      assert_equal 6, logo_audit.total_evaluated
      assert_equal 3, logo_audit.total_updated

      assert_equal max_logo_id, logo_audit.max_id
    end

    describe '#reset_progress' do
      before do
        logo_audit.update_progress(2, 1, max_logo_id - 5)

        new_logo = FactoryBot.create(:brand_logo, filename: "small.png")
        new_logo.account_id = account_id
        new_logo.save!

        @new_max = BrandLogo.where(account_id: account_id, parent_id: nil).maximum(:id)
        assert @new_max > max_logo_id
      end

      it 'can be done w/o region change' do
        logo_audit.reset_progress

        assert_equal @new_max, logo_audit.max_id
        assert_equal @new_max + 1, logo_audit.last_id
        assert_equal 0, logo_audit.total_evaluated
        assert_equal 0, logo_audit.total_updated
      end

      it 'can be done w/region change' do
        logo_audit.reset_progress([:b, :a, :c])

        assert_equal [:b, :a, :c], logo_audit.region
        assert_equal @new_max, logo_audit.max_id
        assert_equal @new_max + 1, logo_audit.last_id
        assert_equal 0, logo_audit.total_evaluated
        assert_equal 0, logo_audit.total_updated
      end

      it 'will ignore thumbnails' do
        audit = StoresBackfillAudit.new_audit(account.id, Attachment.name, region)

        create_attachment
        create_attachment
        last_attachment = create_attachment

        last_thumb = Attachment.where(parent_id: last_attachment.id).first
        assert_not_nil last_thumb, "there's a thumbnail for last_attachment"
        assert last_thumb.id > last_attachment.id, "with a higher id"

        audit.reset_progress
        assert_equal last_attachment.id, audit.max_id
      end

      it 'will work for class without thumbnails' do
        audit = StoresBackfillAudit.new_audit(account.id, Voice::Upload.name, region)

        create_voice_upload
        last_upload = create_voice_upload

        audit.reset_progress
        assert_equal last_upload.id, audit.max_id
      end
    end
  end

  describe '#region' do
    it 'presents as a list' do
      att_audit.region = [:a, :b]
      assert_equal [:a, :b], att_audit.region
      assert_equal "a,b", att_audit.read_attribute(:region)
    end

    it 'keeps order' do
      att_audit.region = [:c, :a, :b]
      assert_equal [:c, :a, :b], att_audit.region
      assert_equal "c,a,b", att_audit.read_attribute(:region)
    end

    it 'stores empty list as nil' do
      att_audit.region = []
      assert_equal [], att_audit.region
      assert_nil att_audit.read_attribute(:region)
    end

    it 'presents nil as empty list' do
      att_audit.region = nil
      assert_equal [], att_audit.region
      assert_nil att_audit.read_attribute(:region)
    end

    it 'saves as comma-delimited string' do
      regions = [:arfland, :meowzerville]
      att_audit.region = regions
      att_audit.save!
      assert_equal "arfland,meowzerville", StoresBackfillAudit.where(id: att_audit.id).pluck(:region).first

      att_audit.reload
      assert_equal regions, att_audit.region
    end
  end

  describe 'scopes' do
    let(:other_account) { accounts(:support) }
    let(:model_regions) do
      { BrandLogo.name => [:s3], Attachment.name => [:s3], Voice::Upload.name => [:s3] }
    end

    before do
      create_attachment
      create_attachment
      create_voice_upload(account)
      create_attachment(other_account)
      StoresBackfillAudit.account_audits(other_account.id, model_regions)
      StoresBackfillAudit.account_audits(account.id, model_regions)
    end

    it '.for_account_id works' do
      [account, other_account].each do |acct|
        audits = StoresBackfillAudit.for_account_id(acct.id)
        assert_equal 3, audits.count
        assert audits.all? { |a| a.account_id == acct.id }
      end

      # show that using an array works to grab multiple account audits
      assert_equal (StoresBackfillAudit.for_account_id(other_account.id) + StoresBackfillAudit.for_account_id(account.id)).sort,
        StoresBackfillAudit.for_account_id([account.id, other_account.id]).sort
    end

    it '.unfinished returns unfinished audits' do
      unfinished = StoresBackfillAudit.unfinished.to_a.map { |a| [a.account_id, a.model_class] }

      assert_includes unfinished, [account.id, Attachment.name]
      assert_includes unfinished, [account.id, Voice::Upload.name]
      assert_includes unfinished, [other_account.id, Attachment.name]

      assert_equal 2, unfinished.select { |acct_id, _| acct_id == account.id }.count
      assert_equal 1, unfinished.select { |acct_id, _| acct_id == other_account.id }.count

      finish_all_audits(account.id)

      unfinished = StoresBackfillAudit.unfinished.to_a.map { |a| [a.account_id, a.model_class] }
      assert_equal 0, unfinished.select { |acct_id, _| acct_id == account.id }.count
      assert_equal 1, unfinished.select { |acct_id, _| acct_id == other_account.id }.count
    end

    it '.done returns completed audits' do
      done = StoresBackfillAudit.done.to_a.map { |a| [a.account_id, a.model_class] }

      assert_includes done, [account.id, BrandLogo.name]
      assert_includes done, [other_account.id, Voice::Upload.name]
      assert_includes done, [other_account.id, BrandLogo.name]

      assert_equal 1, done.select { |acct_id, _| acct_id == account.id }.count
      assert_equal 2, done.select { |acct_id, _| acct_id == other_account.id }.count

      finish_all_audits(account.id)

      done = StoresBackfillAudit.done.to_a.map { |a| [a.account_id, a.model_class] }
      assert_equal 3, done.select { |acct_id, _| acct_id == account.id }.count
      assert_equal 2, done.select { |acct_id, _| acct_id == other_account.id }.count
    end

    it '.unfinished/.done returns exclusive sets' do
      unfinished = StoresBackfillAudit.unfinished.to_a
      done = StoresBackfillAudit.done.to_a

      assert_empty unfinished & done
      assert_equal StoresBackfillAudit.count, (unfinished | done).uniq.count
    end

    def finish_all_audits(acct_id)
      StoresBackfillAudit.for_account_id(acct_id).each do |audit|
        audit.done!
        audit.save!
      end
    end
  end

  describe 'validations' do
    before(:all) do
      @audit = StoresBackfillAudit.create! do |audit|
        audit.account_id = accounts(:minimum).id
        audit.model_class = Attachment.name
        audit.region = [:region]
        audit.max_id = 5
        audit.last_id = 1
        audit.total_evaluated = 2
        audit.total_updated = 1
      end
    end

    it 'is valid' do
      assert @audit.valid?
    end

    it 'saves' do
      @audit.save!
    end

    it 'throws error when last_id > max_id+1' do
      @audit.max_id = 5
      @audit.last_id = 7

      assert !@audit.valid?

      assert_raises ActiveRecord::RecordInvalid do
        @audit.save!
      end
    end
  end
end
