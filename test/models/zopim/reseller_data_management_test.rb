require_relative "../../support/test_helper"

SingleCov.covered!

describe Zopim::ResellerDataManagement do
  subject { mixin_host.new }

  describe "a model mixed-in with a ResellerDataManagement concern" do
    let(:mixin_host) do
      Class.new do
        extend ActiveModel::Callbacks
        define_model_callbacks :save

        def save
          run_callbacks(:save)
        end

        def account_id
          42
        end

        def agent_id
          101
        end

        include Zopim::ResellerDataManagement
      end
    end

    describe "when the reseller_data_reader is not declared" do
      it "does not acquire trial management related methods" do
        refute subject.respond_to?(:account_reseller_data)
      end

      describe "saving the model's state" do
        it "clears the reseller data cache" do
          subject.expects(:instance_variable_set).
            with(:@__account_reseller_data, nil).never
          subject.save
        end
      end
    end

    describe "when the reseller_data_reader is declared" do
      before do
        mixin_host.class_eval do
          reseller_data_reader :account!,
            params: { id: :account_id }
        end
      end

      it "acquires the named reseller data method" do
        assert subject.respond_to?(:account_reseller_data)
      end

      describe "the generated method" do
        before do
          Zopim::Reseller.client.expects(:account!).
            with(id: 42).
            returns(:value).once
        end

        it "fetchs the appropriate reseller data" do
          assert_equal :value, subject.account_reseller_data
        end

        it "returns the cached reseller data on the 2nd invocation" do
          2.times.each { assert_equal :value, subject.account_reseller_data }
        end
      end

      describe "saving the model's state" do
        describe "when a reseller data value has been fetched" do
          before do
            Zopim::Reseller.client.stubs(:account!).with(id: 42).returns(:value)
            assert_equal :value, subject.account_reseller_data
          end

          it "clears the reseller data cache" do
            subject.expects(:instance_variable_set).
              with(:@__account_reseller_data, nil).once
            subject.save
          end
        end

        describe "when no reseller data value has been fetched" do
          it "does not clear the reseller data cache" do
            subject.expects(:instance_variable_set).
              with(:@__account_reseller_data, nil).never
            subject.save
          end
        end
      end

      describe "declaring another reseller_data_reader" do
        before do
          mixin_host.class_eval do
            reseller_data_reader :account_agent!,
              params: { id: :account_id, agent_id: :agent_id }
          end
        end

        it "acquires the named reseller data method" do
          assert subject.respond_to?(:account_agent_reseller_data)
        end

        it "fetchs the appropriate reseller data" do
          Zopim::Reseller.client.expects(:account_agent!).
            with(id: 42, agent_id: 101).
            returns(:value).once
          assert_equal :value, subject.account_agent_reseller_data
        end

        describe "saving the model's state" do
          before do
            Zopim::Reseller.client.stubs(:account!).
              with(id: 42).returns(:value)
            Zopim::Reseller.client.stubs(:account_agent!).
              with(id: 42, agent_id: 101).returns(:value)
            assert_equal :value, subject.account_reseller_data
            assert_equal :value, subject.account_agent_reseller_data
          end

          it "clears all of the reseller data cache" do
            subject.expects(:instance_variable_set).
              with(:@__account_reseller_data, nil).once
            subject.expects(:instance_variable_set).
              with(:@__account_agent_reseller_data, nil).once
            subject.save
          end
        end
      end
    end
  end
end
