require_relative "../../support/test_helper"

SingleCov.covered!

describe Zopim::Trial do
  fixtures :accounts

  describe 'chat_product_payload' do
    let(:account) do
      account = MiniTest::Mock.new
      account.expect :trial_extras_alternative_length?, trial_extras_alternative_length
      account.expect :multiproduct?, multiproduct
      account.expect :has_ocp_support_shell_account_creation?, ocp_support_shell_account_creation
      account
    end
    let(:ocp_support_shell_account_creation) { false }
    let(:suite) { false }
    let(:multiproduct) { false }
    let(:trial_extras_alternative_length) { false }
    let(:max_agents) { 23 }
    let(:expires_at) { '2018-01-31T09:14:23+00:00' }
    let(:expected_payload) do
      {
        product: {
          state: :trial,
          trial_expires_at: expires_at,
          plan_settings: {
            plan_type: 0,
            suite: suite,
            phase: 3,
            max_agents: max_agents,
            agent_workspace: suite
          }
        }
      }
    end

    before do
      Timecop.freeze(2018, 1, 1, 9, 14, 23)
    end

    it 'returns the correct product payload to send to Account Service' do
      assert_equal(expected_payload, Zopim::Trial.chat_product_payload(account, max_agents: max_agents))
    end

    describe 'shell account' do
      let(:multiproduct) { true }

      describe 'with ocp_support_shell_account_creation enabled' do
        let(:ocp_support_shell_account_creation) { true }

        it 'sets the correct phase in plan settings' do
          assert_equal(4, Zopim::Trial.chat_product_payload(account, max_agents: max_agents)[:product][:plan_settings][:phase])
        end
      end

      describe 'with ocp_support_shell_account_creation disabled' do
        let(:ocp_support_shell_account_creation) { false }

        it 'sets the correct phase in plan settings' do
          assert_equal(3, Zopim::Trial.chat_product_payload(account, max_agents: max_agents)[:product][:plan_settings][:phase])
        end
      end
    end

    describe 'with suite:true' do
      let(:suite) { true }

      it 'returns the correct product payload to send to Account Service' do
        assert_equal(expected_payload, Zopim::Trial.chat_product_payload(account, suite: suite, max_agents: max_agents))
      end
    end

    describe 'with an account in 14-day trial treatment group' do
      let(:expires_at) { '2018-01-15T09:14:23+00:00' }
      let(:trial_extras_alternative_length) { true }

      it 'returns the correct product payload to send to Account Service' do
        assert_equal(expected_payload, Zopim::Trial.chat_product_payload(account, max_agents: max_agents))
      end
    end

    describe 'for an account with the classic_14_day_trial arturo enabled' do
      let(:expires_at) { '2018-01-15T09:14:23+00:00' }
      let(:account) do
        account = accounts(:minimum)

        account.add_trial_extras(alternative_length: false)
        Arturo.enable_feature!(:classic_14_day_trial)

        account.save!
        account
      end

      it 'returns the correct product payload to send to Account Service' do
        assert_equal(expected_payload, Zopim::Trial.chat_product_payload(account, max_agents: max_agents))
      end
    end
  end
end
