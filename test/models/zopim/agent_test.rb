require_relative '../../support/test_helper'
require_relative '../../support/zopim_test_helper'
require_relative '../../support/chat_phase_3_helper'
require_relative '../../support/suite_test_helper'

SingleCov.covered! uncovered: 14

describe Zopim::Agent do
  include ZopimTestHelper
  include ChatPhase3Helper
  include SuiteTestHelper

  fixtures :all

  before do
    Timecop.freeze('2014-12-23')
    Account.any_instance.stubs(has_chat_permission_set?: true)
    Account.any_instance.stubs(zopim_identity_available?: true)

    Zopim::AgentSyncJob.stubs(:work)
    User.any_instance.stubs(is_verified?: true)
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(
      is_serviceable?: true,
      ensure_not_phase_four: nil
    )

    SecureRandom.stubs(hex: 'random-password')

    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )

    Zopim::Reseller.client.stubs(
      account!: stub(trial_end_date: Time.now),
      create_account!: Hashie::Mash.new(id: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      update_account!: true,
      find_accounts!: []
    )
  end

  describe ".unserviceable_agent" do
    let(:zopim_agent) do
      Zopim::Agent.unserviceable_agent
    end

    it "returns a zopim agent" do
      assert zopim_agent.is_a? Zopim::Agent
    end

    it "returns an unserviceable zopim agent" do
      refute zopim_agent.is_serviceable?
    end
  end

  describe "#is_serviceable?" do
    let(:zopim_agent) { Zopim::Agent.new }

    describe "when linked and enabled" do
      before do
        zopim_agent.zopim_agent_id = 123456
        zopim_agent.is_enabled     = true
      end

      it "returns true" do
        assert zopim_agent.is_serviceable?
      end
    end

    describe "when linked but not enabled" do
      before do
        zopim_agent.zopim_agent_id = 123456
        zopim_agent.is_enabled     = false
      end

      it "returns false" do
        refute zopim_agent.is_serviceable?
      end
    end

    describe "when not linked but enabled" do
      before do
        zopim_agent.zopim_agent_id = nil
        zopim_agent.is_enabled     = true
      end

      it "returns false" do
        refute zopim_agent.is_serviceable?
      end
    end
  end

  describe "#is_linked?" do
    let(:zopim_agent) { Zopim::Agent.new }

    describe "when the zopim_agent_id is not set" do
      before { zopim_agent.zopim_agent_id = nil }

      it "returns false" do
        refute zopim_agent.is_linked?
      end
    end

    describe "when the zopim_account_id is set" do
      before { zopim_agent.zopim_agent_id = 123456 }

      it "returns true" do
        assert zopim_agent.is_linked?
      end
    end
  end

  describe "synchronizable attributes" do
    it "is well-known" do
      expected = %w[email is_enabled is_administrator display_name]
      actual   = Zopim::Agent::SYNCHRONIZABLE_ATTRIBUTES
      assert_equal expected, actual
    end
  end

  describe ".destroy_agents" do
    let(:account) { accounts(:minimum) }

    let(:zendesk_agent) do
      agent = users(:minimum_agent)
      setup_zopim_identity agent
      agent
    end

    let(:zopim_owner) { account.owner }

    before do
      setup_zopim_subscription(account)
      zopim_agents = account.zopim_agents
      Zopim::AgentDeactivator.any_instance.stubs(:trashed_email).returns("trashed@email")
      Zopim::Reseller.client.expects(:update_account_agent!).with(
        id: account.zopim_subscription.zopim_account_id,
        agent_id: account.owner.zopim_identity.zopim_agent_id,
        data: { email: "trashed@email" }
      )
      Zopim::Agent.destroy_agents(zopim_agents)
    end

    it "destroys all zopim agent records" do
      assert_empty(account.reload.zopim_agents)
    end
  end

  describe "zopim identity" do
    let(:account) { accounts(:minimum) }
    let(:zendesk_agent) { account.owner }
    let(:zopim_agent) { zendesk_agent.zopim_identity }

    before do
      Zopim::AgentSyncJob.stubs(:work)
    end

    describe "when updated" do
      describe "when the account is not chat phase 3" do
        before do
          setup_zopim_subscription(account)
        end
        it "executes a sync job" do
          payload = { display_name: 'cuba libre' }
          Zopim::AgentSyncJob.expects(:work).with(account.id, zendesk_agent.id, payload)
          zopim_agent.update_attribute(:display_name, 'cuba libre')
        end

        describe "and default configuration is applied" do
          before do
            zopim_agent.is_administrator = true
            zopim_agent.display_name     = 'trinity'
            zopim_agent.save!
          end

          it "does not override the attribute values that were updated" do
            assert(zopim_agent.is_administrator?)
            assert_equal 'trinity', zopim_agent.display_name
          end
        end

        describe "and the email address has changed" do
          let(:zopim_subscription) { zopim_agent.zopim_subscription }

          describe "and it marked as the zopim-account owner" do
            before { zopim_agent.stubs(is_owner?: true) }

            it "updates the zopim-account's owner_email field" do
              zopim_subscription.expects(:update_attribute).
                with(:owner_email, 'acme+1234@example.com').once
              zopim_agent.update_attribute(:email, 'acme+1234@example.com')
            end
          end

          describe "and it is not marked as the zopim-account owner" do
            before { zopim_agent.stubs(is_owner?: false) }

            it "does not update the zopim-account's owner_email field" do
              zopim_subscription.expects(:update_attribute).never
              zopim_agent.update_attribute(:email, 'acme+1234@example.com')
            end
          end
        end
      end
    end

    describe "when the account has chat phase 3" do
      let(:account) { accounts(:multiproduct) }
      let(:zendesk_agent) { account.owner }

      before do
        create_phase_3_zopim_subscription(account)
      end

      it "does not execute a sync job" do
        Zopim::AgentSyncJob.expects(:work).never
        zopim_agent.update_attribute(:display_name, 'cuba libre')
      end
    end
  end

  describe "#reseller_data" do
    let(:account) { accounts(:minimum) }

    let(:zopim_subscription) { account.zopim_subscription }
    let(:zopim_agent) { zopim_subscription.owner_agent }

    before do
      setup_zopim_subscription(account)
    end

    it "has a method to fetch reseller agent data" do
      assert zopim_agent.respond_to?(:account_agent_reseller_data)
    end

    it "fetchs the account agent data from zopim" do
      account_id = zopim_subscription.zopim_account_id
      agent_id   = zopim_agent.zopim_agent_id
      Zopim::Reseller.client.expects(:account_agent!).
        with(id: account_id, agent_id: agent_id)
      zopim_agent.account_agent_reseller_data
    end
  end

  describe "zopim agent record updates requirements" do
    let(:account) do
      accounts(:minimum).tap do |account|
        setup_zopim_subscription(account)
      end
    end

    let(:zendesk_agent) { account.owner }
    let(:zopim_agent) { zendesk_agent.zopim_identity }

    before do
      Zopim::AgentSyncJob.stubs(:work)
    end

    describe "when the account's chat permission is not set" do
      before { Account.any_instance.stubs(has_chat_permission_set?: false) }

      it "raises an error" do
        message = 'Validation failed: AccountChatAccessNotEnabled'
        error = assert_raises ActiveRecord::RecordInvalid do
          zopim_agent.display_name = 'player-one'
          zopim_agent.save!
        end
        assert_equal message, error.message
      end
    end

    describe_with_arturo_enabled :ocp_chat_only_agent_deprecation do
      before do
        Account.any_instance.stubs(has_chat_permission_set?: false)
        Account.any_instance.stubs(zopim_identity_available?: false)
      end

      it "when zopim identity is not available it raises an error" do
        message = 'Validation failed: AccountChatAccessNotEnabled'
        error = assert_raises ActiveRecord::RecordInvalid do
          zopim_agent.display_name = 'player-one'
          zopim_agent.save!
        end
        assert_equal message, error.message
      end
    end

    describe "when the chat account is not serviceable" do
      before { ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_serviceable?: false) }

      it "raises an error" do
        message = 'Validation failed: ServiceableChatAccountRequired'
        error = assert_raises ActiveRecord::RecordInvalid do
          zopim_agent.display_name = 'player-one'
          zopim_agent.save!
        end
        assert_equal message, error.message
      end
    end
  end

  describe "#destroy" do
    let(:user) { account.owner }
    let(:account) { accounts(:with_trial_zopim_subscription) }
    let(:zopim_agent) { account.owner.create_zopim_identity! }

    before do
      Zopim::Reseller.client.stubs(create_account_agent!: stub(id: 1))
      Zopim::AgentSyncJob.stubs(:work)
      Zopim::AgentDeactivator.any_instance.stubs(:deactivate!)
      UserEmailIdentity.create!(value: "user@example.com", account: account, user: user, priority: 1)
      User.any_instance.stubs(zopim_email: 'user@example.com')
    end

    it "destroys the zopim agent record" do
      zopim_agent.destroy
      assert zopim_agent.destroyed?
    end

    it "clears the chat_permission_set_id flag of the zendesk user" do
      zopim_agent.user.expects(:remove_chat_permission_set_id).once
      zopim_agent.destroy
    end

    it "skips user downgrade if not-serviceable" do
      User.any_instance.stubs(:is_chat_agent?).returns(true)
      Account.any_instance.stubs(:is_serviceable?).returns(false)
      zopim_agent.user.expects(:save!).never
      zopim_agent.destroy
    end

    it "downgrades user if serviceable" do
      User.any_instance.stubs(:is_chat_agent?).returns(true)
      User.any_instance.stubs(:account_is_serviceable).returns(true)
      zopim_agent.user.expects(:save!).once
      zopim_agent.destroy
    end

    describe "deactivating the reseller chat-agent record" do
      before do
        Zopim::AgentDeactivator.expects(:new).
          with(zopim_agent).
          returns(deactivator)
      end

      describe "when deactivation succeeds" do
        let(:deactivator) { stub }

        before do
          deactivator.expects(:deactivate!)
          zopim_agent.destroy
        end

        it "destroys the zopim agent record" do
          assert zopim_agent.destroyed?
        end
      end

      describe "when deactivation fails" do
        let(:deactivator) { stub }

        before do
          deactivator.expects(:deactivate!).raises('some error')
          assert_raise(RuntimeError) { zopim_agent.destroy }
        end

        it "does not destroy the zopim agent record" do
          refute zopim_agent.destroyed?
        end
      end
    end
  end

  describe "destroying a user with a zopim identity" do
    let(:account) { accounts(:with_trial_zopim_subscription) }

    let(:user) { account.owner }

    let(:zopim_agent) { account.owner.create_zopim_identity! }

    before do
      UserEmailIdentity.create!(value: "user@example.com", account: account, user: user, priority: 1)
      User.any_instance.stubs(zopim_email: 'user@example.com')
      Zopim::Reseller.client.stubs(create_account_agent!: stub(id: 1))
      Zopim::AgentSyncJob.stubs(:work)
    end

    it "alsos destroy the zopim identity" do
      zopim_agent.expects(:destroy).once
      user.destroy
    end
  end

  describe "disabling the zopim owner" do
    describe "for a phase 2 integration" do
      let(:account) { accounts(:minimum) }
      let(:zopim_agent) { account.owner.zopim_identity }

      before do
        setup_zopim_subscription(account)
      end

      it "is not allowed" do
        assert_equal zopim_agent.disable!, false
      end
    end

    describe "for a phase 3 integration" do
      let(:account) { accounts(:multiproduct) }
      let(:zopim_agent) { account.owner.zopim_identity }

      before do
        create_phase_3_zopim_subscription(account)
      end

      it "is allowed" do
        assert_equal zopim_agent.disable!, true
      end

      describe "when the identity is destroyed" do
        before do
          zopim_agent.destroy
        end

        it "doesn't blow up" do
          zopim_agent.disable!
        end
      end
    end
  end
end
