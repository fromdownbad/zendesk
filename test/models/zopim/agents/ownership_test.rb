require_relative '../../../support/test_helper'
require_relative '../../../support/zopim_test_helper'
require_relative '../../../support/suite_test_helper'

SingleCov.covered!

describe Zopim::Agents::Ownership do
  include SuiteTestHelper
  include ZopimTestHelper

  fixtures :all

  before do
    Timecop.freeze('2014-12-23')
    SecureRandom.stubs(hex: 'random-password')
    Account.any_instance.stubs(has_chat_permission_set?: true)
    Zopim::AgentSyncJob.stubs(:work)
    ZopimIntegration.any_instance.stubs(
      app_installed?: true,
      ensure_not_phase_four: nil
    )
    Zopim::Reseller.client.stubs(
      create_account!: Hashie::Mash.new(id: 1, account_key: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      find_accounts!: []
    )
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)

    stub_request(:post, %r{embeddable/api/internal/config_sets.json})
    account.create_zopim_subscription!
  end

  describe "#transfer_ownership!" do
    let(:account) { accounts(:minimum) }

    let(:zopim_subscription) { account.zopim_subscription }

    describe "when the candidate is not administrator" do
      let(:candidate) do
        account.agents.detect { |agent| !agent.is_admin? }
      end

      before do
        Zopim::Reseller.client.expects(:update_account_agent!).never
        Zopim::Reseller.client.expects(:update_account!).never
        ZendeskBillingCore::Zopim::Subscription.any_instance.expects(:update_column).never
        Zopim::Agent.any_instance.expects(:update_column).never
      end

      it "returns false" do
        refute Zopim::Agent.transfer_ownership!(zopim_subscription, candidate)
      end
    end

    describe "when the candidate is already the owner-agent" do
      let(:candidate) do
        zopim_subscription.owner_agent.user
      end

      before do
        Zopim::Reseller.client.expects(:update_account_agent!).never
        Zopim::Reseller.client.expects(:update_account!).never
        ZendeskBillingCore::Zopim::Subscription.any_instance.expects(:update_column).never
        Zopim::Agent.any_instance.expects(:update_column).never
      end

      it "returns true" do
        assert Zopim::Agent.transfer_ownership!(zopim_subscription, candidate)
      end
    end

    describe "when the candidate is an administrator" do
      let(:incumbent) { zopim_subscription.owner_agent }

      let(:incumbent_remote) { Hashie::Mash.new(id: 1) }

      let(:candidate) do
        account.admins.detect { |admin| admin.zopim_identity.blank? }
      end

      let(:candidate_remote) { Hashie::Mash.new(id: 2) }

      before do
        # Irrelevant since we are stubbing the API data
        Zopim::Agent.stubs(agents_in_sync?: true)
      end

      describe "and not a zopim-agent" do
        describe "and there's a vacant zopim-agent seat" do
          before do
            zopim_subscription.stubs(max_agents: account.zopim_agents.count(:all) + 1)

            Zopim::Reseller.client.stubs(:account_agent!).
              with(id: 1, agent_id: 1).returns(stub(id: 1))

            Zopim::Reseller.client.stubs(:account_agent!).
              with(id: 1, agent_id: 2).returns(stub(id: 2))

            Zopim::Reseller.client.expects(:create_account_agent!).
              with(id: 1, data: {
                email:         'non_verified_admin+91449@zendesk.com',
                password:      'random-password',
                first_name:    'No',
                last_name:     'Man',
                display_name:  'No Verified Admin Man',
                administrator: true,
                enabled:       true
              }).returns(stub(id: 2))

            Zopim::Reseller.client.expects(:update_account_agent!).
              with(id: 1, agent_id: 2, data: {
                display_name: 'tmp_No Verified Admin Man',
                email:        'tmp_non_verified_admin+91449@zendesk.com'
              })

            Zopim::Reseller.client.expects(:update_account_agent!).
              with(id: 1, agent_id: 1, data: {
                first_name:   'No',
                last_name:    'Man',
                display_name: 'No Verified Admin Man',
                email:        'non_verified_admin+91449@zendesk.com'
              })

            Zopim::Reseller.client.expects(:update_account_agent!).
              with(id: 1, agent_id: 2, data: {
                first_name:   'Admin',
                last_name:    'Man',
                display_name: 'Admin Man',
                email:        'minimum_admin+86133@aghassipour.com'
              })

            Zopim::Reseller.client.expects(:update_account!).
              with(id: 1, data: {
                first_name: 'No',
                last_name:  'Man',
                email:      'non_verified_admin+91449@zendesk.com'
              })

            assert_equal 1, account.zopim_agents.count(:all)
            assert Zopim::Agent.transfer_ownership!(zopim_subscription, candidate)
          end

          it "makes the candidate the owner-agent" do
            expected = zopim_subscription.owner_agent
            actual   = candidate.reload.zopim_identity
            assert_equal expected, actual
          end

          it "increases the number of zopim-agents" do
            assert_equal 2, account.zopim_agents.count(:all)
          end

          it "makes the incumbent no longer an owner-agent" do
            refute account.owner.zopim_identity.is_owner?
          end
        end

        describe "and there is no zopim-agent seat vacant" do
          before do
            zopim_subscription.stubs(max_agents: account.zopim_agents.count(:all))

            Zopim::Reseller.client.expects(:account_agent!).
              with(id: 1, agent_id: incumbent.zopim_agent_id).
              returns(incumbent_remote)

            Zopim::Reseller.client.expects(:update_account_agent!).
              with(id: 1, agent_id: 1, data: {
                first_name:   'No',
                last_name:    'Man',
                display_name: 'No Verified Admin Man',
                email:        'non_verified_admin+91449@zendesk.com'
              })

            Zopim::Reseller.client.expects(:update_account!).
              with(id: 1, data: {
                first_name: 'No',
                last_name:  'Man',
                email:      'non_verified_admin+91449@zendesk.com'
              })

            assert_equal 1, account.zopim_agents.count(:all)
            assert Zopim::Agent.transfer_ownership!(zopim_subscription, candidate)
          end

          it "makes the candidate the owner-agent" do
            expected = zopim_subscription.owner_agent
            actual   = candidate.reload.zopim_identity
            assert_equal expected, actual
          end

          it "does not increase number of zopim-agents" do
            assert_equal 1, account.zopim_agents.count(:all)
          end

          it "removes the zopim-identiy of the incumbent" do
            assert account.owner.zopim_identity.blank?
          end
        end
      end

      describe "and already a zopim-agent" do
        before do
          Zopim::Reseller.client.stubs(:create_account_agent!).
            with(id: 1, data: {
              email:         'non_verified_admin+91449@zendesk.com',
              password:      'random-password',
              first_name:    'No',
              last_name:     'Man',
              display_name:  'No Verified Admin Man',
              administrator: true,
              enabled:       true
            }).returns(stub(id: 2))
          candidate.create_zopim_identity!

          Zopim::Reseller.client.expects(:account_agent!).
            with(id: 1, agent_id: incumbent.zopim_agent_id).
            returns(incumbent_remote)

          Zopim::Reseller.client.expects(:account_agent!).
            with(id: 1, agent_id: candidate.zopim_agent_id).
            returns(candidate_remote)

          Zopim::Reseller.client.expects(:update_account_agent!).
            with(id: 1, agent_id: 2, data: {
                display_name: 'tmp_No Verified Admin Man',
                email:        'tmp_non_verified_admin+91449@zendesk.com'
              })

          Zopim::Reseller.client.expects(:update_account_agent!).
            with(id: 1, agent_id: 1, data: {
              first_name:   'No',
              last_name:    'Man',
              display_name: 'No Verified Admin Man',
              email:        'non_verified_admin+91449@zendesk.com'
            })

          Zopim::Reseller.client.expects(:update_account_agent!).
            with(id: 1, agent_id: 2,
                 data: {
                first_name:   'Admin',
                last_name:    'Man',
                display_name: 'Admin Man',
                email:        'minimum_admin+86133@aghassipour.com'
              })

          Zopim::Reseller.client.expects(:update_account!).
            with(id: 1, data: {
              first_name: 'No',
              last_name:  'Man',
              email:      'non_verified_admin+91449@zendesk.com'
            })

          assert_equal 2, account.zopim_agents.count(:all)
          assert Zopim::Agent.transfer_ownership!(zopim_subscription, candidate)
        end

        describe "and there's a vacant zopim-agent seat" do
          it "makes the candidate the owner-agent" do
            expected = zopim_subscription.owner_agent
            actual   = candidate.reload.zopim_identity
            assert_equal expected, actual
          end

          it "does not increase number of zopim-agents" do
            assert_equal 2, account.zopim_agents.count(:all)
          end

          it "turns the incumbent to not an owner-agent" do
            refute account.owner.zopim_identity.is_owner?
          end
        end

        describe "and there is no zopim-agent seat vacant" do
          before do
            zopim_subscription.stubs(max_agents: account.zopim_agents.count(:all))
          end

          it "makes the candidate the owner-agent" do
            expected = zopim_subscription.owner_agent
            actual   = candidate.reload.zopim_identity
            assert_equal expected, actual
          end

          it "does not increase number of zopim-agents" do
            assert_equal 2, account.zopim_agents.count(:all)
          end

          it "turns the incumbent to not an owner-agent" do
            refute account.owner.zopim_identity.is_owner?
          end
        end
      end
    end

    describe "::agents_in_sync?" do
      let(:previous_owner) do
        account.owner.zopim_identity
      end

      let(:remote_previous_owner) do
        Hashie::Mash.new(id: previous_owner.zopim_agent_id, email: previous_owner.email, owner: true)
      end

      let(:remote_out_of_sync_owner) do
        Hashie::Mash.new(id: previous_owner.zopim_agent_id, email: 'wrong@company.com', owner: true)
      end

      let(:remote_out_of_sync_agent) do
        Hashie::Mash.new(id: previous_owner.zopim_agent_id, email: 'wrong@company.com', owner: false)
      end

      describe "when Zendesk/Zopim agents are in sync" do
        let(:agents_remote) { [remote_previous_owner] }

        before do
          Zopim::Reseller.client.stubs(account_agents!: agents_remote)
        end

        it "is true" do
          assert Zopim::Agent.send(:agents_in_sync?, zopim_subscription)
        end
      end

      describe "when Zendesk/Zopim agents fell out of sync" do
        let(:remote_out_of_sync_agents) { [remote_previous_owner, remote_out_of_sync_agent] }

        before do
          Zopim::Reseller.client.stubs(account_agents!: remote_out_of_sync_agents)
        end

        it "is false" do
          refute Zopim::Agent.send(:agents_in_sync?, zopim_subscription)
        end
      end

      describe "when the owner email fell out of sync" do
        let(:remote_agents_owner_out_of_sync) { [remote_out_of_sync_owner] }

        before do
          Zopim::Reseller.client.stubs(account_agents!: remote_agents_owner_out_of_sync)
        end

        it "is false" do
          refute Zopim::Agent.send(:agents_in_sync?, zopim_subscription)
        end
      end
    end
  end
end
