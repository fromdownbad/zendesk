require_relative '../../../support/test_helper'
require_relative '../../../support/zopim_test_helper'
require_relative '../../../support/chat_phase_3_helper'
require_relative '../../../support/entitlement_test_helper'
require_relative '../../../support/suite_test_helper'

SingleCov.covered!

describe Zopim::Agents::PhaseThree do
  include ZopimTestHelper
  include ChatPhase3Helper
  include EntitlementTestHelper
  include SuiteTestHelper

  fixtures :all

  let(:account) { accounts(:multiproduct) }
  let(:zopim_subscription) { account.zopim_subscription }
  let(:user) { users(:multiproduct_support_agent) }
  let(:zopim_identity) { user.zopim_identity }

  describe '#update_staff_service' do
    describe 'for an account that is not chat phase 3' do
      before do
        setup_zopim_stubs
        setup_zopim_subscription(account)
      end

      it 'does not enequeue a ChatPhaseThreeEntitlementJob' do
        ChatPhaseThree::EntitlementSyncJob.expects(:enqueue).never
        user.create_zopim_identity!
      end
    end

    describe 'for an account that is chat phase 3' do
      before do
        create_phase_3_zopim_subscription(account)
        ChatPhaseThree::EntitlementSyncJob.expects(:enqueue).twice
        user.create_zopim_identity!
      end

      describe 'when a chat agent is made into a chat admin' do
        it 'updates the staff service' do
          zopim_identity.is_administrator = true
          zopim_identity.save!
        end
      end

      describe 'when the chat identity is disabled' do
        it 'updates the staff service' do
          user.zopim_identity.disable!
        end
      end

      describe 'when the chat identity is destroyed' do
        it 'updates the staff service' do
          user.zopim_identity.destroy
        end
      end
    end
  end
end
