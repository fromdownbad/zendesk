require_relative '../../../support/test_helper'
require_relative '../../../support/zopim_test_helper'
require_relative '../../../support/suite_test_helper'

SingleCov.covered! uncovered: 3

describe Zopim::Agents::Creation do
  include SuiteTestHelper
  include ZopimTestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:zopim_subscription) { account.zopim_subscription }

  before do
    Timecop.freeze('2014-12-23')
    ZopimIntegration.any_instance.stubs(
      app_installed?: false,
      install_app: true,
      ensure_not_phase_four: nil
    )
    Zopim::Reseller.client.stubs(
      create_account!: Hashie::Mash.new(id: 1, account_key: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      find_accounts!:  []
    )
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)
    ::ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
    Account.any_instance.stubs(has_chat_permission_set_or_active_suite?: true)
    stub_request(:post, %r{embeddable/api/internal/config_sets.json})
    account.create_zopim_subscription!
  end

  describe "creating a user's zopim-identity" do
    let(:eligible_user) do
      account.agents.detect do |agent|
        !agent.is_account_owner? && agent.zopim_identity.blank?
      end
    end

    let(:ineligible_user) do
      account.end_users.first
    end

    describe "when the account does not have chat access" do
      let(:agent) { eligible_user }

      before do
        Account.any_instance.stubs(has_chat_permission_set_or_active_suite?: false)
      end

      it "raises AccountChatAccessNotEnabled" do
        error = assert_raises ActiveRecord::RecordInvalid do
          agent.create_zopim_identity!
        end
        error.message.must_include "AccountChatAccessNotEnabled"
      end

      before do
        Account.any_instance.stubs(zopim_identity_available?: false)
      end

      it "raises AccountChatIntegrationNotEnabled when no zopim identity available" do
        error = assert_raises ActiveRecord::RecordInvalid do
          agent.create_zopim_identity!
        end
        error.message.must_include "AccountChatAccessNotEnabled"
      end
    end

    describe "when the chat-account is not serviceable" do
      let(:agent) { eligible_user }

      before do
        ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_serviceable?: false)
      end

      it "raises ServiceableChatAccountRequired" do
        error = assert_raises ActiveRecord::RecordInvalid do
          agent.create_zopim_identity!
        end
        error.message.must_include "ServiceableChatAccountRequired"
      end
    end

    describe "when the chat-account is not in trial" do
      let(:agent) { eligible_user }

      before do
        ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_trial?: false)
      end

      describe "and all the agent seats are filled" do
        before do
          ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(max_agents: 1)
        end

        it "raises TooManyAgents" do
          error = assert_raises ActiveRecord::RecordInvalid do
            agent.create_zopim_identity!
          end
          error.message.must_include "TooManyAgents"
        end
      end

      describe 'and account has active suite' do
        describe 'and all seats were filled by support and chat together' do
          before do
            Account.any_instance.stubs(has_active_suite?: true)
            ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(max_agents: 4)
          end

          describe_with_arturo_enabled :ocp_cp3_suite_check_support_for_zopim_agent_creation do
            it 'raises TooManyAgents' do
              error = assert_raises ActiveRecord::RecordInvalid do
                agent.create_zopim_identity!
              end
              error.message.must_include "TooManyAgents"
            end
          end

          describe_with_arturo_disabled :ocp_cp3_suite_check_support_for_zopim_agent_creation do
            before do
              stub_request(:post, "https://reseller.chat.zd-dev.com/api/accounts/1/agents").to_return(status: 200, body: "", headers: {})
              ::Zopim::Agent.any_instance.stubs(:link_to_remote_account_record)
            end

            it 'does not raise error' do
              agent.create_zopim_identity!
            end
          end
        end
      end
    end

    describe "when the chat-account is in trial" do
      let(:agent) { eligible_user }

      before do
        ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_trial?: true)
      end

      describe "and all the agent seats are filled" do
        before do
          do_agent_bump_first = sequence('agent_bump')
          ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(max_agents: 1)
          ZendeskBillingCore::Zopim::Subscription.any_instance.expects(:update_attribute).
            with(:max_agents, 2).once.in_sequence(do_agent_bump_first)
          Zopim::Reseller.client.expects(create_account_agent!: stub(:id)).
            once.in_sequence(do_agent_bump_first)
        end

        it "should adjust the chat-account's max_agents settings" do
          assert agent.create_zopim_identity!
        end
      end
    end

    describe "when the user does not have a chat-privilege" do
      let(:agent) { ineligible_user }

      it "raises UserChatPrivilegeRequired" do
        error = assert_raises ActiveRecord::RecordInvalid do
          agent.create_zopim_identity!
        end
        error.message.must_include "UserChatPrivilegeRequired"
      end
    end

    describe "when the user does not have a primary email address" do
      let(:agent) { eligible_user }

      before do
        UserEmailIdentity.any_instance.stubs(:primary?).returns(false)
      end

      it "raises UserPrimaryEmailRequired" do
        error = assert_raises ActiveRecord::RecordInvalid do
          agent.create_zopim_identity!
        end
        error.message.must_include "UserPrimaryEmailRequired"
      end
    end

    describe "when the user is eligible" do
      let(:agent) { eligible_user }
      let(:identity) { agent.zopim_identity }
      let(:remote_account_record) do
        Hashie::Mash.new(id: 1234)
      end

      let(:account_create_payload) do
        {
          email:         'minimum_agent+57888@aghassipour.com',
          password:      'random-password',
          first_name:    'Agent',
          last_name:     'Minimum',
          display_name:  'Agent Minimum',
          administrator: false,
          enabled:       true
        }
      end

      before do
        SecureRandom.stubs(hex: 'random-password')
      end

      before do
        Zopim::Reseller.client.expects(:create_account_agent!).
          with(id: zopim_subscription.zopim_account_id, data: account_create_payload).
          returns(remote_account_record).once
        agent.create_zopim_identity!
      end

      it "should configure the zopim-identity" do
        expected = {
          is_owner:         false,
          is_administrator: false,
          display_name:     'Agent Minimum',
          email:            'minimum_agent+57888@aghassipour.com'
        }
        actual = identity.attributes.symbolize_keys.
          slice(:is_owner, :email, :display_name, :is_administrator)
        assert_equal actual, expected
      end
    end
  end
end
