require_relative '../../support/test_helper'
require_relative '../../support/classic_test_helper'

SingleCov.covered!

describe 'Report' do
  include ClassicTestHelper

  fixtures :accounts

  let(:account) { create_account_from_hash(default_account_hash) }

  describe 'when a trial is created' do
    describe 'with arturo enabled :orca_classic_replace_fraud_report_job_with_fraud_score_job' do
      before do
        Arturo.enable_feature!(:orca_classic_replace_fraud_report_job_with_fraud_score_job)
      end

      it 'executes the fraud score job' do
        FraudScoreJob.expects(:account_creation_enqueue)
        account.save!
      end
    end
  end

  describe 'when a fraud report is retrieved' do
    let(:account) { accounts(:minimum) }

    before do
      account.fraud_reports.create!(
        user: account.owner,
        source_event: Fraud::SourceEvent::TRIAL_SIGNUP
      )
    end

    it 'converts the source event back to a symbol' do
      assert_equal Fraud::SourceEvent::TRIAL_SIGNUP,
        account.fraud_reports.first.source_event
    end
  end

  describe 'default scope' do
    let(:account) { accounts(:minimum) }

    let(:initial_fraud_report) do
      {
        account:      account,
        user:         account.owner,
        source_event: Fraud::SourceEvent::TRIAL_SIGNUP,
        risk_score:   nil
      }
    end

    let(:latest_fraud_report) do
      {
        account:      account,
        user:         account.owner,
        source_event: Fraud::SourceEvent::PURCHASE_WITH_NEW_CC,
        risk_score:   nil
      }
    end

    before do
      Timecop.freeze
      Fraud::Report.create!(initial_fraud_report)

      Timecop.travel(1.hour.from_now)
      Fraud::Report.create!(latest_fraud_report)
    end

    it 'orders by creation date ASC' do
      assert_equal latest_fraud_report[:source_event],
        account.fraud_reports.last.source_event
    end
  end

  describe '#notify_voice' do
    let(:account) { accounts(:minimum) }
    let(:risk_score) { 90 }

    it 'enqueue job if feature is enabled' do
      Voice::SendFraudReportJob.expects(:enqueue).with(account.id, risk_score)
      Fraud::Report.create!(account: account, user: account.owner, risk_score: risk_score)
    end

    it 'send default score if fraud score missing' do
      Voice::SendFraudReportJob.expects(:enqueue).with(account.id, 0)
      Fraud::Report.create!(account: account, user: account.owner, risk_score: nil)
    end
  end
end
