require_relative "../support/test_helper"
require_relative "../support/business_hours_test_helper"
require_relative "../support/ticket_metric_helper"

SingleCov.covered! uncovered: 7

describe TicketMetricSet do
  include BusinessHoursTestHelper
  include TicketMetricHelper

  fixtures :all

  [:ticket, :dirty_ticket].each do |attribute|
    should allow_mass_assignment_of(attribute)
  end if RAILS4

  let(:before_save_callbacks) { [:set_resolution_time, :set_assignee_stations, :set_group_stations, :set_defaults] }
  let(:before_update_callbacks) { [:set_reopens, :set_replies, :set_first_reply_time, :set_wait_time, :set_on_hold_time] }

  before do
    @ticket = tickets(:minimum_1)
    @ticket_metric_set = TicketMetricSet.new(ticket: @ticket, dirty_ticket: @ticket)
  end

  describe "callbacks" do
    describe "before_save" do
      before do
        (before_save_callbacks + before_update_callbacks).each do |callback|
          @ticket_metric_set.expects(callback).once
        end
      end

      it "all get called on save" do
        @ticket_metric_set.save!
      end
    end
  end

  describe "#update_metrics" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload
    end

    it "sets the new dirty_ticket" do
      @ticket_metric_set.expects(:save!).once
      @ticket_metric_set.update_metrics(tickets(:minimum_2))
      assert_equal tickets(:minimum_2), @ticket_metric_set.dirty_ticket
    end

    describe "when ticket is being created via ticket sharing" do
      before do
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.audit.ticket_sharing_create = true
      end

      it "doesn't set ticket metrics" do
        @ticket.ticket_metric_set.expects(:update_metrics).never
        @ticket.save!
      end
    end
  end

  describe "#set_account_from_ticket" do
    it "sets the right account" do
      assert_equal @ticket.account, @ticket_metric_set.send(:set_account_from_ticket)
    end
  end

  describe "#set_resolution_time" do
    before do
      Timecop.freeze
      @ticket_metric_set.save!
      @ticket_metric_set.reload
    end

    describe "when solving a ticket for the first time" do
      [StatusType.SOLVED, StatusType.CLOSED].each do |status_id|
        describe "with status_id of #{status_id}" do
          before do
            @ticket.status_id = status_id
            @ticket.updated_at = Time.now

            @ticket_metric_set.update_metrics(@ticket)
          end

          it "sets the proper attributes" do
            minutes = @ticket_metric_set.send(:minutes_since, @ticket.created_at)

            assert_equal  minutes[0],             @ticket_metric_set.full_resolution_time_in_minutes
            assert_equal  minutes[1],             @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
            assert_equal (minutes[0] / 60.0).round, @ticket_metric_set.full_resolution_time_in_hours
            assert_equal  minutes[0],             @ticket_metric_set.first_resolution_time_in_minutes
            assert_equal  minutes[1],             @ticket_metric_set.first_resolution_time_in_minutes_within_business_hours
          end
        end

        describe "with status_id of #{status_id} and ticket is imported(update_time != to solved_at time)" do
          before do
            @ticket.status_id = status_id
            @ticket.created_at = 7.days.ago
            @ticket.solved_at = 6.days.ago
            @ticket.updated_at = Time.now
          end

          describe_with_arturo_enabled(:resolution_uses_solved_at) do
            it "sets resolution time to time between created at and solved_at" do
              @ticket_metric_set.update_metrics(@ticket)
              minutes = @ticket_metric_set.send(:minutes_since, @ticket.created_at, @ticket.solved_at)

              # Resolution time should be measured between created_at -> solved_at
              assert_equal 1.day.seconds / 60, minutes[0]
              assert_equal minutes[0], @ticket_metric_set.full_resolution_time_in_minutes
              assert_equal (minutes[0] / 60.0).round, @ticket_metric_set.full_resolution_time_in_hours
              assert_equal minutes[0], @ticket_metric_set.first_resolution_time_in_minutes
              assert_equal minutes[1], @ticket_metric_set.first_resolution_time_in_minutes_within_business_hours
            end

            it "sets resolution to time between created_at and updated_at if solved_at is nil" do
              @ticket.solved_at = nil

              @ticket_metric_set.update_metrics(@ticket)
              minutes = @ticket_metric_set.send(:minutes_since, @ticket.created_at, @ticket.updated_at)

              # Resolution time should be measured between created_at -> updated_at
              assert_equal 7.day.seconds / 60, minutes[0]
              assert_equal minutes[0], @ticket_metric_set.full_resolution_time_in_minutes
              assert_equal (minutes[0] / 60.0).round, @ticket_metric_set.full_resolution_time_in_hours
              assert_equal minutes[0], @ticket_metric_set.first_resolution_time_in_minutes
              assert_equal minutes[1], @ticket_metric_set.first_resolution_time_in_minutes_within_business_hours
            end
          end

          describe_with_arturo_disabled(:resolution_uses_solved_at) do
            it "sets resolution time to time between created at and updated_at" do
              @ticket_metric_set.update_metrics(@ticket)
              minutes = @ticket_metric_set.send(:minutes_since, @ticket.created_at)

              # Resolution time should be measured between created_at -> updated_at
              assert_equal 7.day.seconds / 60, minutes[0]
              assert_equal minutes[0], @ticket_metric_set.full_resolution_time_in_minutes
              assert_equal (minutes[0] / 60.0).round, @ticket_metric_set.full_resolution_time_in_hours
              assert_equal minutes[0], @ticket_metric_set.first_resolution_time_in_minutes
              assert_equal minutes[1], @ticket_metric_set.first_resolution_time_in_minutes_within_business_hours
            end
          end
        end
      end

      describe "with a non-solved status" do
        describe "with status_id of #{StatusType.PENDING}" do
          before do
            @ticket.status_id = StatusType.PENDING
            @ticket.save

            @ticket_metric_set.update_metrics(@ticket)
          end

          it "does not set the attributes" do
            refute @ticket_metric_set.full_resolution_time_in_minutes
            refute @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
            refute @ticket_metric_set.full_resolution_time_in_hours
            refute @ticket_metric_set.first_resolution_time_in_minutes
            refute @ticket_metric_set.first_resolution_time_in_minutes_within_business_hours
          end
        end
      end
    end

    describe "when solving a ticket the second time" do
      before do
        @ticket.status_id = StatusType.SOLVED
        @ticket.updated_at = Time.now

        @ticket_metric_set.update_metrics(@ticket)

        @original_minutes = @ticket_metric_set.send(:minutes_since, @ticket.created_at)

        Timecop.travel(10.hours)

        @ticket.status_id = StatusType.PENDING
        @ticket.updated_at = Time.now

        @ticket_metric_set.update_metrics(@ticket)

        Timecop.travel(10.hours)

        @ticket.status_id = StatusType.SOLVED
        @ticket.updated_at = Time.now

        @ticket_metric_set.update_metrics(@ticket)
      end

      it "sets the proper attributes" do
        minutes = @ticket_metric_set.send(:minutes_since, @ticket.created_at)

        assert_not_equal @original_minutes, minutes

        assert_equal 20.hours / 60.0, (minutes[0] - @original_minutes[0])

        assert_equal  minutes[0],               @ticket_metric_set.full_resolution_time_in_minutes
        assert_equal  minutes[1],               @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
        assert_equal (minutes[0] / 60.0).round, @ticket_metric_set.full_resolution_time_in_hours
        assert_equal  @original_minutes[0],     @ticket_metric_set.first_resolution_time_in_minutes
        assert_equal  @original_minutes[1],     @ticket_metric_set.first_resolution_time_in_minutes_within_business_hours
      end
    end

    describe "when ticket is updated while closed" do
      before do
        @ticket_metric_set.dirty_ticket.stubs(
          status_id_was: StatusType.CLOSED,
          status_id: StatusType.CLOSED,
          status_id_changed: false
        )
        @ticket_metric_set.send(:set_resolution_time)
      end

      it "does not set full response time" do
        refute @ticket_metric_set.full_resolution_time_in_minutes
        refute @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
        refute @ticket_metric_set.full_resolution_time_in_hours
      end
    end
  end

  describe "#should_increment_assignee_stations?" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload
    end

    describe "when the assignee on the dirty ticket does not change" do
      before do
        @ticket.assignee_id = @ticket.assignee_id
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "returns true" do
        refute @ticket_metric_set.send(:should_increment_assignee_stations?)
      end
    end

    describe "when the assignee on the dirty ticket does change" do
      before do
        @ticket.assignee_id = @ticket.assignee_id + 1
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "returns true" do
        assert @ticket_metric_set.send(:should_increment_assignee_stations?)
      end
    end

    describe "when the assignee on the dirty ticket now does not exist" do
      before do
        @ticket.assignee_id = nil
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "returns true" do
        refute @ticket_metric_set.send(:should_increment_assignee_stations?)
      end
    end
  end

  describe "#current_assignee_stations" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload
    end

    describe "on first save" do
      it "sets the assignee stations correctly" do
        assert_equal 0, @ticket_metric_set.assignee_stations
      end
    end

    describe "when changing the assignee" do
      before do
        @ticket.assignee_id = @ticket.assignee_id + 1
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "sets the assignee stations correctly" do
        assert_equal 1, @ticket_metric_set.assignee_stations
      end
    end

    describe "when unassigning the assignee" do
      before do
        @ticket.assignee_id = nil
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "does not increment the assignee stations" do
        assert_equal 0, @ticket_metric_set.assignee_stations
      end
    end
  end

  describe "#set_assignee_stations" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload

      assert_equal 0, @ticket_metric_set.assignee_stations
    end

    describe "when we should increment assignee stations" do
      before do
        @ticket_metric_set.stubs(should_increment_assignee_stations?: true)
        @ticket_metric_set.send(:set_assignee_stations)
      end

      it "increments the assignee stations" do
        assert_equal 1, @ticket_metric_set.assignee_stations
      end
    end

    describe "when we shouldn't increment assignee stations" do
      before do
        @ticket_metric_set.stubs(should_increment_assignee_stations?: false)
        @ticket_metric_set.send(:set_assignee_stations)
      end

      it "does not increment the assignee stations" do
        assert_equal 0, @ticket_metric_set.assignee_stations
      end
    end
  end

  describe "#should_increment_group_stations?" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload
    end

    describe "when the group on the dirty ticket does not change" do
      before do
        @ticket.group_id = @ticket.group_id
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "returns true" do
        refute @ticket_metric_set.send(:should_increment_group_stations?)
      end
    end

    describe "when the group on the dirty ticket does change" do
      before do
        @ticket.group_id = 1
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "returns true" do
        assert @ticket_metric_set.send(:should_increment_group_stations?)
      end
    end

    describe "when the group on the dirty ticket now does not exist" do
      before do
        @ticket.group_id = nil
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "returns true" do
        refute @ticket_metric_set.send(:should_increment_group_stations?)
      end
    end
  end

  describe "#current_group_stations" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload
    end

    describe "on first save" do
      it "sets the group stations correctly" do
        assert_equal 0, @ticket_metric_set.group_stations
      end
    end

    describe "when changing the group" do
      before do
        @ticket.group_id = 1
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "sets the group stations correctly" do
        assert_equal 1, @ticket_metric_set.group_stations
      end
    end

    describe "when deleting the group from the dirty ticket" do
      before do
        @ticket.group_id = nil
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "does not increment the group stations" do
        assert_equal 0, @ticket_metric_set.group_stations
      end
    end
  end

  describe "#set_group_stations" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload

      assert_equal 0, @ticket_metric_set.group_stations
    end

    describe "when we should increment group stations" do
      before do
        @ticket_metric_set.stubs(should_increment_group_stations?: true)
        @ticket_metric_set.send(:set_group_stations)
      end

      it "increments the assignee stations" do
        assert_equal 1, @ticket_metric_set.group_stations
      end
    end

    describe "when we shouldn't increment group stations" do
      before do
        @ticket_metric_set.stubs(should_increment_group_stations?: false)
        @ticket_metric_set.send(:set_group_stations)
      end

      it "does not increment the group stations" do
        assert_equal 0, @ticket_metric_set.group_stations
      end
    end
  end

  describe "#public_comment_from_agent?" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload
    end

    describe "when there is no comment" do
      before do
        @ticket.comment = nil
        @ticket_metric_set.update_metrics(@ticket)
      end

      it "returns false" do
        refute @ticket_metric_set.send(:public_comment_from_agent?)
      end
    end

    describe "when there is a comment" do
      before do
        @comment = Comment.new do |c|
          c.account    = @ticket.account
          c.created_at = Time.now
        end
      end

      describe "when the comment is not public" do
        before do
          @comment.is_public = false
          @ticket.comment = @comment

          @ticket_metric_set.update_metrics(@ticket)
        end

        it "returns false" do
          refute @ticket_metric_set.send(:public_comment_from_agent?)
        end
      end

      describe "when the comment is public" do
        before do
          @comment.is_public = true
        end

        describe "and the comment has some value" do
          before do
            @comment.body = "foo"
          end

          describe "and the author is an agent" do
            before do
              @comment.author = users(:minimum_agent)
              @ticket.comment = @comment

              @ticket_metric_set.update_metrics(@ticket)
            end

            it "returns true" do
              assert @ticket_metric_set.send(:public_comment_from_agent?)
            end
          end

          describe "and the author is an end user" do
            before do
              @comment.author = users(:minimum_end_user)
              @ticket.comment = @comment

              @ticket_metric_set.update_metrics(@ticket)
            end

            it "returns false" do
              refute @ticket_metric_set.send(:public_comment_from_agent?)
            end
          end

          describe "and the author is a foreign agent" do
            before do
              @foreign_agent = users(:minimum_end_user)
              @foreign_agent.stubs(foreign_agent?: true)
              @comment.author = @foreign_agent
              @ticket.comment = @comment

              @ticket_metric_set.update_metrics(@ticket)
            end

            it "returns true" do
              assert @ticket_metric_set.send(:public_comment_from_agent?)
            end
          end
        end

        describe "and the comment has no value but an attachment" do
          before do
            @comment.body = nil
            @comment.attachments << Attachment.new
          end

          describe "and the author is an agent" do
            before do
              @comment.author = users(:minimum_agent)
              @ticket.comment = @comment

              @ticket_metric_set.update_metrics(@ticket)
            end

            it "returns true" do
              assert @ticket_metric_set.send(:public_comment_from_agent?)
            end
          end

          describe "and the author is an end user" do
            before do
              @comment.author = users(:minimum_end_user)
              @ticket.comment = @comment

              @ticket_metric_set.update_metrics(@ticket)
            end

            it "returns false" do
              refute @ticket_metric_set.send(:public_comment_from_agent?)
            end
          end

          describe "and the author is a foreign agent" do
            before do
              @foreign_agent = users(:minimum_end_user)
              @foreign_agent.stubs(foreign_agent?: true)
              @comment.author = @foreign_agent
              @ticket.comment = @comment

              @ticket_metric_set.update_metrics(@ticket)
            end

            it "returns true" do
              assert @ticket_metric_set.send(:public_comment_from_agent?)
            end
          end
        end

        describe "and the comment has no value and no attachments" do
          before do
            @comment.body = nil
            @comment.attachments = []
          end

          describe "and the author is an agent" do
            before do
              @comment.author = users(:minimum_agent)
              @ticket.comment = @comment

              @ticket_metric_set.update_metrics(@ticket)
            end

            it "returns false" do
              refute @ticket_metric_set.send(:public_comment_from_agent?)
            end
          end

          describe "and the author is an end user" do
            before do
              @comment.author = users(:minimum_end_user)
              @ticket.comment = @comment

              @ticket_metric_set.update_metrics(@ticket)
            end

            it "returns false" do
              refute @ticket_metric_set.send(:public_comment_from_agent?)
            end
          end

          describe "and the author is a foreign agent" do
            before do
              @foreign_agent = users(:minimum_end_user)
              @foreign_agent.stubs(foreign_agent?: true)
              @comment.author = @foreign_agent
              @ticket.comment = @comment

              @ticket_metric_set.update_metrics(@ticket)
            end

            it "returns false" do
              refute @ticket_metric_set.send(:public_comment_from_agent?)
            end
          end
        end
      end
    end
  end

  describe "#should_increment_replies?" do
    describe "when the comment was from an agent" do
      before do
        @ticket_metric_set.stubs(public_comment_from_agent?: true)
        @ticket_metric_set.account = accounts(:minimum)
      end

      describe 'and the ticket is new' do
        before { @ticket.stubs(id_changed?: true) }

        it 'returns false' do
          refute @ticket_metric_set.send(:should_increment_replies?)
        end
      end

      describe 'and the ticket is not new' do
        before { @ticket.stubs(id_changed?: false) }

        it 'returns true' do
          assert @ticket_metric_set.send(:should_increment_replies?)
        end
      end
    end

    describe "when the comment was not from an agent" do
      before do
        @ticket_metric_set.stubs(public_comment_from_agent?: false)
      end

      it "returns false" do
        refute @ticket_metric_set.send(:should_increment_replies?)
      end
    end
  end

  describe "#set_first_reply_time" do
    describe 'when the ticket is new' do
      before do
        @ticket_metric_set.dirty_ticket.stubs(id_changed?: true)
        @ticket_metric_set.account = accounts(:minimum)
      end

      before { @ticket_metric_set.send(:set_first_reply_time) }

      before_should 'return early' do
        @ticket_metric_set.expects(:public_comment_from_agent?).never
      end
    end

    describe 'when the ticket is not new' do
      before do
        @ticket_metric_set.save!
        @ticket_metric_set.reload

        refute @ticket_metric_set.first_reply_time_in_minutes
      end

      describe "for the first reply" do
        let(:account) { accounts(:minimum) }

        before do
          Account.any_instance.stubs(business_hours_active?: true)
          add_schedule_intervals(
            account.schedules.create(
              name:      'Schedule',
              time_zone: account.time_zone
            )
          )

          @ticket_metric_set.stubs(time_customer_first_waiting: Time.new(2014, 8, 4, 23, 50))
          @ticket.stubs(created_at: Time.new(2014, 8, 4, 23, 20))
          @ticket.stubs(updated_at: Time.new(2014, 8, 5, 11, 00))
        end

        describe "when the reply is by an agent" do
          before do
            @ticket_metric_set.stubs(public_comment_from_agent?: true)
          end

          describe "on a normal ticket" do
            before do
              @ticket_metric_set.send(:set_first_reply_time)
            end

            it "sets the right attributes using created_at" do
              assert_equal 700, @ticket_metric_set.first_reply_time_in_minutes
              assert_equal 240, @ticket_metric_set.first_reply_time_in_minutes_within_business_hours
            end
          end

          describe "on a shared ticket" do
            before do
              @sharing_event = TicketSharingEvent.new(agreement_id: 1)
              @sharing_event.stubs(:created_at).returns(Time.new(2014, 8, 5, 3, 00))
              @ticket_metric_set.stubs(:sharing_event).returns(@sharing_event)

              @ticket_metric_set.send(:set_first_reply_time)
            end

            it "sets the right attributes using the event's created_at" do
              assert_equal 480, @ticket_metric_set.first_reply_time_in_minutes
              assert_equal 240, @ticket_metric_set.first_reply_time_in_minutes_within_business_hours
            end

            describe "require_public_comment?" do
              it "return false when agreement is not found" do
                refute @ticket_metric_set.send(:require_public_comment?)
              end
            end
          end
        end

        describe "when the reply is not an agent" do
          before do
            @ticket_metric_set.stubs(public_comment_from_agent?: false)
            @ticket_metric_set.send(:set_first_reply_time)
          end

          it "does not set any attributes" do
            refute @ticket_metric_set.first_reply_time_in_minutes
            refute @ticket_metric_set.first_reply_time_in_minutes_within_business_hours
          end
        end
      end

      describe "for the second reply" do
        before do
          @ticket_metric_set.first_reply_time_in_minutes = 7676
        end

        describe "when the reply is by an agent" do
          before do
            @ticket_metric_set.stubs(public_comment_from_agent?: true)
            @ticket_metric_set.send(:set_first_reply_time)
          end

          it "does not override the attributes" do
            minutes = @ticket_metric_set.send(:minutes_since, @ticket.created_at)

            assert_not_equal minutes[0], @ticket_metric_set.first_reply_time_in_minutes
            assert_not_equal minutes[1], @ticket_metric_set.first_reply_time_in_minutes_within_business_hours
          end
        end

        describe "when the reply is not an agent" do
          before do
            @ticket_metric_set.stubs(public_comment_from_agent?: false)
            @ticket_metric_set.send(:set_first_reply_time)
          end

          it "does not override the attributes" do
            minutes = @ticket_metric_set.send(:minutes_since, @ticket.created_at)

            assert_not_equal minutes[0], @ticket_metric_set.first_reply_time_in_minutes
            assert_not_equal minutes[1], @ticket_metric_set.first_reply_time_in_minutes_within_business_hours
          end
        end
      end
    end
  end

  describe "#set_wait_time" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload
    end

    describe "when status does not change" do
      before do
        @ticket_metric_set.dirty_ticket.stubs(status_id_changed?: false)
        @ticket_metric_set.send(:set_wait_time)
      end

      it "does not set the attributes" do
        refute @ticket_metric_set.agent_wait_time_in_minutes
        refute @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
        refute @ticket_metric_set.requester_wait_time_in_minutes
        refute @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
      end
    end

    describe "when status does change" do
      before do
        Timecop.freeze
        @ticket_metric_set.dirty_ticket.stubs(status_id_changed?: true,
                                              updated_at: 1.hours.ago,
                                              status_updated_at_was: 2.hours.ago)
      end

      describe 'and the ticket is new' do
        before do
          @ticket_metric_set.dirty_ticket.stubs(id_changed?: true)
          @ticket_metric_set.send(:set_wait_time)
        end

        it 'does not set the attributes' do
          refute @ticket_metric_set.agent_wait_time_in_minutes
          refute @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          refute @ticket_metric_set.requester_wait_time_in_minutes
          refute @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
        end
      end

      describe 'and the ticket is not new' do
        describe 'when the status changes from pending' do
          before do
            @ticket_metric_set.dirty_ticket.stubs(status_id_was: StatusType.PENDING)
            @ticket_metric_set.send(:set_wait_time)
          end

          it "does not set the requester attributes" do
            refute @ticket_metric_set.requester_wait_time_in_minutes
            refute @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          end
          it "sets the agent attributes" do
            assert_equal 60, @ticket_metric_set.agent_wait_time_in_minutes
            assert_equal 60, @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          end
        end

        describe 'when the status changes from not pending' do
          before do
            @ticket_metric_set.dirty_ticket.stubs(status_id_was: StatusType.OPEN)
            @ticket_metric_set.send(:set_wait_time)
          end

          it "does not set the agent attributes" do
            refute @ticket_metric_set.agent_wait_time_in_minutes
            refute @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          end

          it "sets the requester attributes" do
            assert_equal 60, @ticket_metric_set.requester_wait_time_in_minutes
            assert_equal 60, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          end
        end

        describe 'when the status changes from solved to closed' do
          before do
            @ticket_metric_set.dirty_ticket.stubs(
              status_id_was: StatusType.SOLVED,
              status_id: StatusType.CLOSED
            )
            @ticket_metric_set.send(:set_wait_time)
          end

          it "does not set any attributes" do
            refute @ticket_metric_set.agent_wait_time_in_minutes
            refute @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
            refute @ticket_metric_set.requester_wait_time_in_minutes
            refute @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          end
        end

        describe 'when the status changes from solved to open' do
          before do
            @ticket_metric_set.dirty_ticket.stubs(
              status_id_was: StatusType.SOLVED,
              status_id: StatusType.OPEN
            )
            @ticket_metric_set.send(:set_wait_time)
          end

          it "does not set any attributes" do
            refute @ticket_metric_set.agent_wait_time_in_minutes
            refute @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
            refute @ticket_metric_set.requester_wait_time_in_minutes
            refute @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          end
        end
      end
    end
  end

  describe "#set_on_hold_time" do
    before do
      @ticket_metric_set.save!
      @ticket_metric_set.reload
    end

    describe "when status does not change" do
      before do
        @ticket_metric_set.dirty_ticket.stubs(status_id_changed?: false)
        @ticket_metric_set.send(:set_on_hold_time)
      end

      it "does not set the attributes" do
        assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes
        assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
      end
    end

    describe "when status does change" do
      before do
        @ticket_metric_set.dirty_ticket.stubs(status_id_changed?: true, updated_at_was: 1.hour.ago)
      end

      describe "from pending" do
        before do
          @ticket_metric_set.dirty_ticket.stubs(status_id_was: StatusType.HOLD)
          @ticket_metric_set.send(:set_on_hold_time)
        end

        it "sets the right attributes" do
          minutes = @ticket_metric_set.send(:minutes_since, @ticket_metric_set.dirty_ticket.updated_at_was)

          assert_equal minutes[0], @ticket_metric_set.on_hold_time_in_minutes
          assert_equal minutes[1], @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
        end
      end

      describe "from not pending" do
        before do
          @ticket_metric_set.dirty_ticket.stubs(status_id_was: StatusType.NEW)
          @ticket_metric_set.send(:set_on_hold_time)
        end

        it "does not set the attributes" do
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
        end
      end
    end
  end

  describe "#set_defaults" do
    before do
      @ticket_metric_set.save!
    end

    describe "when status is not solved or closed" do
      before do
        @ticket_metric_set.send(:set_defaults)
      end

      it "does not set the defaults" do
        refute @ticket_metric_set.agent_wait_time_in_minutes
        refute @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
        refute @ticket_metric_set.requester_wait_time_in_minutes
        refute @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
      end
    end

    [StatusType.SOLVED, StatusType.CLOSED].each do |status_id|
      describe "when the status_id is #{status_id}" do
        before do
          @ticket_metric_set.dirty_ticket.stubs(status_id: status_id)
          @ticket_metric_set.send(:set_defaults)
        end

        it "sets the defaults" do
          assert_equal 0, @ticket_metric_set.agent_wait_time_in_minutes
          assert_equal 0, @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          assert_equal 0, @ticket_metric_set.requester_wait_time_in_minutes
          assert_equal 0, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
        end
      end
    end
  end

  describe "#attributes_for_reports" do
    it "returns the right attributes" do
      assert_equal [
        "group_stations",
        "assignee_stations",
        "reopens",
        "replies",
        "first_reply_time_in_minutes",
        "first_reply_time_in_minutes_within_business_hours",
        "first_resolution_time_in_minutes",
        "first_resolution_time_in_minutes_within_business_hours",
        "full_resolution_time_in_minutes",
        "full_resolution_time_in_minutes_within_business_hours",
        "agent_wait_time_in_minutes",
        "agent_wait_time_in_minutes_within_business_hours",
        "requester_wait_time_in_minutes",
        "requester_wait_time_in_minutes_within_business_hours",
        "on_hold_time_in_minutes",
        "on_hold_time_in_minutes_within_business_hours"
      ], TicketMetricSet.send(:attributes_for_reports)
    end
  end

  describe "old tests" do
    describe "A ticket metric set" do
      it "responds to agent_stations" do
        # the rules expect this instead of assignee_stations
        tms = TicketMetricSet.new
        tms.assignee_stations = 3
        assert_equal(3, tms.agent_stations)
      end
    end

    before do
      Timecop.freeze(Time.utc(2010, 'nov', 9, 23, 30)) # Tuesday 23:30

      @account = accounts(:minimum)
    end

    describe "Default values" do
      it "shoulds yield for ticket that is solved immediately" do
        add_schedule_intervals(
          @account.schedules.create(
            name:      'Schedule',
            time_zone: @account.time_zone
          )
        )

        @ticket = Ticket.new assignee: users(:minimum_agent), description: "hejsa", status_id: StatusType.SOLVED, account: @account
        @ticket.will_be_saved_by(users(:minimum_end_user))
        @ticket.save!

        @ticket_metric_set = @ticket.ticket_metric_set
        assert_equal 0, @ticket_metric_set.agent_wait_time_in_minutes
        assert_equal 0, @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
        assert_equal 0, @ticket_metric_set.requester_wait_time_in_minutes
        assert_equal 0, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
      end
    end

    describe "Metrics" do
      before do
        add_schedule_intervals(
          @account.schedules.create(
            name:      'Schedule',
            time_zone: @account.time_zone
          )
        )

        Account.any_instance.stubs(:has_business_hours?).returns(true)
        Account.any_instance.stubs(:has_multiple_schedules?).returns(true)

        @ticket = @account.tickets.new(description: "hejsa")
        @ticket.will_be_saved_by(users(:minimum_end_user))
        @ticket.save!

        @ticket.created_at = Time.utc(2010, 'nov', 9, 13, 30) # Tuesday 13:30

        @ticket_metric_set = @ticket.ticket_metric_set

        @ticket.metric_events.destroy_all
      end

      describe "for a new ticket" do
        it "has correct default values" do
          assert_equal 1, @ticket_metric_set.group_stations
          assert_equal 0, @ticket_metric_set.assignee_stations
          assert_equal 0, @ticket_metric_set.reopens
          assert_equal 0, @ticket_metric_set.replies
          assert_nil @ticket_metric_set.agent_wait_time_in_minutes
          assert_nil @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          assert_nil @ticket_metric_set.requester_wait_time_in_minutes
          assert_nil @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
          assert_nil @ticket_metric_set.full_resolution_time_in_hours
          assert_nil @ticket_metric_set.full_resolution_time_in_minutes
          assert_nil @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
          assert_nil @ticket_metric_set.first_resolution_time_in_minutes
          assert_nil @ticket_metric_set.first_resolution_time_in_minutes_within_business_hours
        end
      end

      describe "for an existing ticket" do
        before do
          @ticket.current_user = users(:minimum_agent)
        end

        it "increments assignee_stations when assigning agents" do
          @ticket.assignee_id = users(:minimum_agent).id
          @ticket.will_be_saved_by(@ticket.submitter)
          @ticket.save!
          assert_equal 1, @ticket_metric_set.assignee_stations

          @ticket.assignee_id = users(:minimum_admin).id
          @ticket.will_be_saved_by(@ticket.submitter)
          @ticket.save!
          assert_equal 2, @ticket_metric_set.assignee_stations

          @ticket.assignee_id = nil
          @ticket.will_be_saved_by(@ticket.submitter)
          @ticket.save!
          assert_equal 2, @ticket_metric_set.assignee_stations
        end

        it "increments replies when adding a public comment" do
          @ticket.add_comment(body: 'agent comment', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!
          assert_equal 1, @ticket_metric_set.replies
        end

        it "sets resolution time when solving a ticket" do
          @ticket.assignee = users(:minimum_agent)
          @ticket.status_id = StatusType.SOLVED
          @ticket.will_be_saved_by(@ticket.submitter)
          @ticket.save!
          assert_equal 600, @ticket_metric_set.first_resolution_time_in_minutes
          assert_equal 600, @ticket_metric_set.full_resolution_time_in_minutes
          assert_equal 10, @ticket_metric_set.full_resolution_time_in_hours
          assert_equal 600, @ticket_metric_set.requester_wait_time_in_minutes

          assert_equal 150, @ticket_metric_set.first_resolution_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
        end

        it "sets requester wait time when changing the status" do
          assert_nil @ticket_metric_set.requester_wait_time_in_minutes

          @ticket.assignee = users(:minimum_agent)
          @ticket.status_id = StatusType.PENDING

          @ticket.will_be_saved_by(@ticket.submitter)
          @ticket.save!
          assert_equal 600, @ticket_metric_set.requester_wait_time_in_minutes
          assert_nil @ticket_metric_set.agent_wait_time_in_minutes
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes

          assert_equal 150, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          assert_nil @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
        end

        it "sets wait times when changing the status from pending to solved" do
          # Go to Pending first
          Timecop.freeze(Time.utc(2010, 'nov', 9, 20, 30)) # Tuesday 20:30 , or 3 hours ago
          goto_pending(@ticket)

          # go to solved
          Timecop.freeze(Time.utc(2010, 'nov', 9, 23, 30)) # Tuesday 23:30
          goto_solved(@ticket)

          assert_equal 180, @ticket_metric_set.agent_wait_time_in_minutes
          assert_equal 420, @ticket_metric_set.requester_wait_time_in_minutes
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes
          assert_equal 600, @ticket_metric_set.full_resolution_time_in_minutes
          assert_equal 10, @ticket_metric_set.full_resolution_time_in_hours

          assert_equal 0, @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
        end

        it "sets wait times when changing the status from pending to open" do
          # Go to Pending first
          Timecop.freeze(Time.utc(2010, 'nov', 9, 20, 30)) # Tuesday 20:30 , or 3 hours ago
          goto_pending(@ticket)

          # Go to Open
          Timecop.freeze(Time.utc(2010, 'nov', 9, 23, 30)) # Tuesday 23:30
          goto_open(@ticket)

          assert_equal 180, @ticket_metric_set.agent_wait_time_in_minutes
          assert_equal 420, @ticket_metric_set.requester_wait_time_in_minutes
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes
          assert_nil @ticket_metric_set.full_resolution_time_in_minutes
          assert_nil @ticket_metric_set.full_resolution_time_in_hours

          assert_equal 0, @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
          assert_nil @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
        end

        it "sets wait times when changing the status from pending to On-Hold" do
          @ticket.account.stubs(:use_status_hold?).returns(true)

          # Go to Pending first
          Timecop.freeze(Time.utc(2010, 'nov', 9, 20, 30)) # Tuesday 20:30 , or 3 hours ago
          goto_pending(@ticket)

          # Go to on-hold
          Timecop.freeze(Time.utc(2010, 'nov', 9, 23, 30)) # Tuesday 23:30
          goto_hold(@ticket)

          assert_equal 180, @ticket_metric_set.agent_wait_time_in_minutes
          assert_equal 420, @ticket_metric_set.requester_wait_time_in_minutes
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes
          assert_nil @ticket_metric_set.full_resolution_time_in_minutes
          assert_nil @ticket_metric_set.full_resolution_time_in_hours

          assert_equal 0, @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
          assert_nil @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
        end

        it "sets wait times when changing the status from On-Hold to Open" do
          @ticket.account.stubs(:use_status_hold?).returns(true)

          # Go to on-hold first
          Timecop.freeze(Time.utc(2010, 'nov', 9, 20, 30)) # Tuesday 20:30 , or 3 hours ago
          goto_hold(@ticket)

          # Go to Open
          Timecop.freeze(Time.utc(2010, 'nov', 9, 23, 30)) # Tuesday 23:30
          goto_open(@ticket)

          assert_nil @ticket_metric_set.agent_wait_time_in_minutes
          assert_equal 600, @ticket_metric_set.requester_wait_time_in_minutes
          assert_equal 180, @ticket_metric_set.on_hold_time_in_minutes
          assert_nil @ticket_metric_set.full_resolution_time_in_minutes
          assert_nil @ticket_metric_set.full_resolution_time_in_hours

          assert_nil @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
          assert_nil @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
        end

        it "sets wait times when ticket reopens and gets solved" do
          # Ticket goes from New to Open to Pending to Solved to Open to Pending to Solved to Closed

          # created_at = 600 minutes ago or 10 hours ago
          # req waiting   = 0 minutes
          # agent waiting = 0 minutes

          # Go to Open first
          Timecop.freeze(Time.utc(2010, 'nov', 9, 20, 30)) # Tuesday 20:30 , or 3 hours ago
          goto_open(@ticket)

          # req waiting   = 420 minutes (7 hours)
          # agent waiting = 0 minutes

          # Go to Pending
          Timecop.freeze(Time.utc(2010, 'nov', 9, 21, 30)) # Tuesday 21:30 , or 2 hours ago
          goto_pending(@ticket)

          # req waiting   = 480 minutes (8 hours)
          # agent waiting = 0 minutes

          # Go to Solve
          Timecop.freeze(Time.utc(2010, 'nov', 9, 22, 00)) # Tuesday 22:00 , or 1:30 hours ago
          goto_solved(@ticket)

          # req waiting   = 480 minutes
          # agent waiting = 30 minutes

          # Go to Open (Reopen)
          Timecop.freeze(Time.utc(2010, 'nov', 9, 22, 30)) # Tuesday 22:30 , or 1 hours ago
          goto_open(@ticket)

          # req waiting   = 480 minutes
          # agent waiting = 30 minutes
          # uncounted time = 30 minutes

          # Go to Pending again
          Timecop.freeze(Time.utc(2010, 'nov', 9, 22, 40)) # Tuesday 22:40 , or 0:50 hours ago
          goto_pending(@ticket)

          # req waiting   = 490 minutes
          # agent waiting = 30 minutes
          # uncounted time = 30 minutes

          # Go to Solve (re solve)
          Timecop.freeze(Time.utc(2010, 'nov', 9, 22, 50)) # Tuesday 22:50 , or 0:40 hours ago
          goto_solved(@ticket)

          # req waiting   = 490 minutes
          # agent waiting = 40 minutes
          # uncounted time = 30 minutes
          # Final solve happened 40 minutes ago. i.e., 560 minutes after creation

          # Close the ticket
          Timecop.freeze(Time.utc(2010, 'nov', 9, 23, 30)) # Tuesday 23:30 , or Now
          goto_closed(@ticket)

          # req waiting   = 490 minutes
          # agent waiting = 40 minutes
          # uncounted time = 90 minutes

          assert_equal 40, @ticket_metric_set.agent_wait_time_in_minutes
          assert_equal 490, @ticket_metric_set.requester_wait_time_in_minutes
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes
          assert_equal 560, @ticket_metric_set.full_resolution_time_in_minutes
          assert_equal 9, @ticket_metric_set.full_resolution_time_in_hours

          assert_equal 0, @ticket_metric_set.agent_wait_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.requester_wait_time_in_minutes_within_business_hours
          assert_equal 0, @ticket_metric_set.on_hold_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
        end

        it "increments reopens and full_resolution times when re-opening and re-solving ticket" do
          @ticket.assignee = users(:minimum_agent)
          @ticket.status_id = StatusType.SOLVED
          @ticket.will_be_saved_by(@ticket.submitter)
          @ticket.save!

          assert_equal 0, @ticket_metric_set.reopens
          assert_equal 600, @ticket_metric_set.first_resolution_time_in_minutes
          assert_equal 600, @ticket_metric_set.full_resolution_time_in_minutes

          @ticket.status_id = StatusType.OPEN
          @ticket.will_be_saved_by(@ticket.submitter)
          @ticket.save!

          assert_equal 1, @ticket_metric_set.reopens
          assert_equal 600, @ticket_metric_set.first_resolution_time_in_minutes
          assert_nil @ticket_metric_set.full_resolution_time_in_minutes

          @ticket.status_id = StatusType.SOLVED
          @ticket.stubs(:updated_at).returns(Time.now.utc + 6.hours)
          @ticket.will_be_saved_by(@ticket.submitter)
          @ticket.save!

          assert_equal 1, @ticket_metric_set.reopens
          assert_equal 600, @ticket_metric_set.first_resolution_time_in_minutes
          assert_equal 960, @ticket_metric_set.full_resolution_time_in_minutes
          assert_equal 16, @ticket_metric_set.full_resolution_time_in_hours

          assert_equal 150, @ticket_metric_set.first_resolution_time_in_minutes_within_business_hours
          assert_equal 150, @ticket_metric_set.full_resolution_time_in_minutes_within_business_hours
        end

        it "does not crash when ticket is deleted" do
          ZendeskExceptions::Logger.expects(:record).never
          @ticket.will_be_saved_by(@ticket.account.owner)
          @ticket.soft_delete!
        end
      end
    end

    private

    def goto_state_at(ticket, state)
      ticket.assignee = users(:minimum_agent)
      ticket.status_id = state
      ticket.will_be_saved_by(@ticket.submitter)
      ticket.save!
    end

    def goto_open(ticket)
      goto_state_at(ticket, StatusType.OPEN)
    end

    def goto_pending(ticket)
      goto_state_at(ticket, StatusType.PENDING)
    end

    def goto_hold(ticket)
      goto_state_at(ticket, StatusType.HOLD)
    end

    def goto_solved(ticket)
      goto_state_at(ticket, StatusType.SOLVED)
    end

    def goto_closed(ticket)
      goto_state_at(ticket, StatusType.CLOSED)
    end
  end
end
