require_relative '../support/test_helper'

SingleCov.covered!

describe AppLink do
  should serialize(:paths).as(Array)
  should serialize(:fingerprints).as(Array)
  should_validate_presence_of :app_id, :package_name
end
