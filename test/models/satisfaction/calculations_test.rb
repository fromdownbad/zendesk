require_relative "../../support/test_helper"
require_relative "../../support/satisfaction_test_helper"

SingleCov.covered! uncovered: 4

describe 'Satisfaction::Calculations' do
  include SatisfactionTestHelper

  fixtures :tickets, :users, :user_identities, :accounts, :sequences, :groups, :memberships

  describe "CSR calculations" do
    before do
      Account.any_instance.stubs(:is_serviceable?).returns(true)
      Account.any_instance.stubs(:has_extended_ticket_types?).returns(true)
      Satisfaction::Rating.stubs(:minimum_ratings_for_calculation).returns(3)
      @account = accounts(:minimum)
      @agent = users(:minimum_agent)
    end

    describe "for Agents" do
      describe "#current_score" do
        describe "when less than Satisfaction::Rating.minimum_ratings_for_calculation ratings exist" do
          before do
            build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
          end

          it "is nil" do
            assert_nil @agent.satisfaction_ratings_received.current_score(@agent)
          end
        end

        describe "when 3 ratings have been received and 0 of them are positive" do
          before do
            set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.BAD)
            set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.BAD)
            set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.BAD)
          end
          it "is 0%" do
            assert_equal 0, @agent.satisfaction_ratings_received.current_score(@agent)
          end
        end

        describe "when 3 ratings have been received and 2 of them are positive" do
          before do
            set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.BAD)
          end
          it "is 67%" do
            assert_equal 67, @agent.satisfaction_ratings_received.current_score(@agent)
          end

          describe "when the 1 negative rating is changed to positive" do
            before do
              set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.GOOD)
            end

            it "is 100%" do
              assert_equal 100, @agent.satisfaction_ratings_received.current_score(@agent)
            end
          end
        end

        describe "when 3 ratings have been received and 3 of them are positive" do
          before do
            set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.GOOD)
          end
          it "is 100%" do
            assert_equal 100, @agent.satisfaction_ratings_received.current_score(@agent)
          end
        end

        describe "when 5 bad and 5 good ratings have been received, and a ticket with a bad rating then gets a good rating " do
          tickets_total = 0
          good_tickets = []
          bad_tickets = []

          before do
            [:minimum_1, :minimum_2, :minimum_3, :minimum_4, :minimum_6].each do |ticket_id|
              good_tickets.push(set_ticket_satisfaction_rating(ticket_id, SatisfactionType.GOOD))
            end
            5.times do
              bad_tickets.push(build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD))
            end

            # change a bad rating to a good one -- old, bad, rating is then ignored in determing score
            ticket = bad_tickets.pop
            ticket.satisfaction_score = 16
            ticket.will_be_saved_by(@agent)
            ticket.save!
            good_tickets.push(ticket)

            tickets_total = good_tickets.length + bad_tickets.length
          end

          it "is 60% (and the older, bad, rating is ignored)" do
            assert_equal ((good_tickets.length.to_f / tickets_total).round(2) * 100).to_i, @agent.satisfaction_ratings_received.current_score(@agent) # cf. formula in #current_score
          end
        end

        describe "ratings associated with other agents" do
          before do
            set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_4, SatisfactionType.BAD, :minimum_admin)
          end
          it "does not affect this agent's score" do
            assert_equal 100, @agent.satisfaction_ratings_received.current_score(@agent)
          end
        end

        describe "offered ratings" do
          before do
            set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_4, SatisfactionType.OFFERED)
          end

          it "shoulds not affect this user's score" do
            assert_equal 100, @agent.satisfaction_ratings_received.current_score(@agent)
          end
        end

        describe "with start time" do
          before do
            set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.GOOD)

            Timecop.travel(1.day.ago) do
              set_ticket_satisfaction_rating(:minimum_4, SatisfactionType.BAD)
            end
          end

          it "filters older tickets" do
            assert_equal 100, @agent.satisfaction_ratings_received.current_score(@agent, 'start_time' => 1.hour.ago)
          end
        end

        describe "with start and end time" do
          before do
            set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.GOOD)
            Timecop.travel(1.day.ago) do
              set_ticket_satisfaction_rating(:minimum_4, SatisfactionType.BAD)
            end
            Timecop.travel(1.day.from_now) do
              set_ticket_satisfaction_rating(:minimum_6, SatisfactionType.BAD)
            end
          end

          it "filters tickets outside range" do
            assert_equal 100, @agent.satisfaction_ratings_received.current_score(@agent, 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
          end
        end
      end
    end

    describe "for groups" do
      describe "#current_score" do
        describe "all ratings for agents in the group" do
          before do
            set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_4, SatisfactionType.BAD, :minimum_admin)
            set_ticket_satisfaction_rating(:minimum_6, SatisfactionType.BAD, :minimum_admin)

            assert_equal users(:minimum_agent).groups, users(:minimum_admin).groups
          end

          it "affects the group's rating" do
            assert_equal 60, groups(:minimum_group).satisfaction_ratings.current_score(groups(:minimum_group))
          end

          describe "except offered ratings" do
            before { build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.OFFERED, comment: nil) }

            it "shoulds affect this group's score" do
              assert_equal 60, groups(:minimum_group).satisfaction_ratings.current_score(groups(:minimum_group))
            end
          end

          describe "with start time" do
            before do
              Timecop.travel(1.day.ago) do
                build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD)
              end
            end

            it "filters older tickets" do
              assert_equal 60, groups(:minimum_group).satisfaction_ratings.current_score(groups(:minimum_group), 'start_time' => 1.hour.ago)
            end
          end

          describe "with start and end time" do
            before do
              Timecop.travel(1.day.ago) do
                build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD)
              end
              Timecop.travel(1.day.from_now) do
                build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD)
              end
            end

            it "filters tickets outside range" do
              assert_equal 60, groups(:minimum_group).satisfaction_ratings.current_score(groups(:minimum_group), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end
          end
        end
      end
    end

    describe "for accounts" do
      describe "#current_score" do
        describe "when less than Satisfaction::Rating.minimum_ratings_for_calculation ratings exist" do
          before do
            build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
          end

          it "is nil" do
            assert_nil accounts(:minimum).satisfaction_ratings.current_score(accounts(:minimum))
          end
        end

        describe "all ratings" do
          before do
            set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.GOOD)
            set_ticket_satisfaction_rating(:minimum_4, SatisfactionType.BAD, :minimum_admin)
          end
          it "shoulds affect this account's score" do
            assert_equal 75, accounts(:minimum).satisfaction_ratings.current_score(accounts(:minimum))
          end

          describe "except offered ratings" do
            before { build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.OFFERED, comment: nil) }

            it "shoulds affect this account's score" do
              assert_equal 75, accounts(:minimum).satisfaction_ratings.current_score(accounts(:minimum))
            end
          end
        end

        describe "with start time" do
          before do
            3.times do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD)
            end
          end

          it "filters older tickets" do
            assert_equal 100, accounts(:minimum).satisfaction_ratings.current_score(accounts(:minimum), 'start_time' => 1.hour.ago)
          end
        end

        describe "with start and end time" do
          before do
            3.times do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD)
            end
            Timecop.travel(1.day.from_now) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD)
            end
          end

          it "filters tickets outside range" do
            assert_equal 100, accounts(:minimum).satisfaction_ratings.current_score(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
          end
        end
      end
    end

    describe "with satisfaction ratings" do
      before do
        set_ticket_satisfaction_rating(:minimum_1, SatisfactionType.OFFERED)
        set_ticket_satisfaction_rating(:minimum_2, SatisfactionType.BAD)
        set_ticket_satisfaction_rating(:minimum_3, SatisfactionType.BADWITHCOMMENT)
        set_ticket_satisfaction_rating(:minimum_4, SatisfactionType.GOOD)
        set_ticket_satisfaction_rating(:minimum_6, SatisfactionType.GOODWITHCOMMENT)
      end

      describe "#positive_rated_ticket_count" do
        it "is the count of positive rated tickets" do
          assert_equal 2, accounts(:minimum).satisfaction_ratings.positive_rated_ticket_count(accounts(:minimum))
        end

        describe "with start time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
          end

          it "filters older tickets" do
            assert_equal 2, accounts(:minimum).satisfaction_ratings.positive_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago)
          end
        end

        describe "with start and end time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
            Timecop.travel(1.day.from_now) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
          end

          it "filters tickets outside range" do
            assert_equal 2, accounts(:minimum).satisfaction_ratings.positive_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
          end

          describe "with range after ticket solved and before satisfaction rating" do
            before do
              ticket = build_ticket(status_id: StatusType.SOLVED)
              Timecop.travel(3.hours.from_now) do
                build_satisfaction_rating(ticket, satisfaction_score: SatisfactionType.GOOD)
              end
            end

            it "filters satisfaction ratings submitted outside range" do
              Arturo.enable_feature!(:csat_metrics_adjustment)
              assert_equal 2, accounts(:minimum).satisfaction_ratings.positive_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end

            it "legacy includes satisfaction ratings submitted outside range" do
              assert_equal 3, accounts(:minimum).satisfaction_ratings.positive_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end
          end
        end
      end

      describe "#negative_rated_ticket_count" do
        it "is the count of negative rated tickets" do
          assert_equal 2, accounts(:minimum).satisfaction_ratings.negative_rated_ticket_count(accounts(:minimum))
        end

        describe "with start time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD)
            end
          end

          it "filters older tickets" do
            assert_equal 2, accounts(:minimum).satisfaction_ratings.negative_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago)
          end
        end

        describe "with start and end time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD)
            end
            Timecop.travel(1.day.from_now) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.BAD)
            end
          end

          it "filters tickets outside range" do
            assert_equal 2, accounts(:minimum).satisfaction_ratings.negative_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
          end

          describe "with range after ticket solved and before satisfaction rating" do
            before do
              ticket = build_ticket(status_id: StatusType.SOLVED)
              Timecop.travel(3.hours.from_now) do
                build_satisfaction_rating(ticket, satisfaction_score: SatisfactionType.BAD)
              end
            end

            it "filters satisfaction ratings submitted outside range" do
              Arturo.enable_feature!(:csat_metrics_adjustment)
              assert_equal 2, accounts(:minimum).satisfaction_ratings.negative_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end

            it "legacy includes satisfaction ratings submitted outside range" do
              assert_equal 3, accounts(:minimum).satisfaction_ratings.negative_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end
          end
        end
      end

      describe "#total_rated_ticket_count" do
        it "is the count of all rated tickets" do
          assert_equal 4, accounts(:minimum).satisfaction_ratings.total_rated_ticket_count(accounts(:minimum))
        end

        describe "with start time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
          end

          it "filters older tickets" do
            assert_equal 4, accounts(:minimum).satisfaction_ratings.total_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago)
          end
        end

        describe "with start and end time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
            Timecop.travel(1.day.from_now) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
          end

          it "filters tickets outside range" do
            assert_equal 4, accounts(:minimum).satisfaction_ratings.total_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
          end

          describe "with range after ticket solved and before satisfaction rating" do
            before do
              ticket = build_ticket(status_id: StatusType.SOLVED)
              Timecop.travel(3.hours.from_now) do
                build_satisfaction_rating(ticket, satisfaction_score: SatisfactionType.BAD)
              end
            end

            it "filters satisfaction ratings submitted outside range" do
              Arturo.enable_feature!(:csat_metrics_adjustment)
              assert_equal 4, accounts(:minimum).satisfaction_ratings.total_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end

            it "legacy includes satisfaction ratings submitted outside range" do
              assert_equal 5, accounts(:minimum).satisfaction_ratings.total_rated_ticket_count(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end
          end
        end
      end

      describe "#surveys_sent" do
        it "is the count of all surveys sent" do
          assert_equal 5, accounts(:minimum).satisfaction_ratings.surveys_sent(accounts(:minimum))
        end

        describe "with start time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
          end

          it "filters older tickets" do
            assert_equal 5, accounts(:minimum).satisfaction_ratings.surveys_sent(accounts(:minimum), 'start_time' => 1.hour.ago)
          end
        end

        describe "with start and end time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
            Timecop.travel(1.day.from_now) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
          end

          it "filters tickets outside range" do
            assert_equal 5, accounts(:minimum).satisfaction_ratings.surveys_sent(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
          end

          describe "with range after ticket solved and before satisfaction rating" do
            before do
              ticket = build_ticket(status_id: StatusType.SOLVED)
              Timecop.travel(3.hours.from_now) do
                build_satisfaction_rating(ticket, satisfaction_score: SatisfactionType.OFFERED)
              end
            end

            it "filters satisfaction ratings submitted outside range" do
              Arturo.enable_feature!(:csat_metrics_adjustment)
              assert_equal 5, accounts(:minimum).satisfaction_ratings.surveys_sent(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end

            it "legacy includes satisfaction ratings submitted outside range" do
              assert_equal 6, accounts(:minimum).satisfaction_ratings.surveys_sent(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end
          end
        end
      end

      describe "#response_rate" do
        it "is the response rate as an integer" do
          assert_equal 80, accounts(:minimum).satisfaction_ratings.response_rate(accounts(:minimum))
        end

        describe "when rate is a fraction" do
          before do
            build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.OFFERED)
          end

          it "rounds correctly" do
            assert_equal 67, accounts(:minimum).satisfaction_ratings.response_rate(accounts(:minimum))
          end
        end

        describe "with start time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
          end

          it "filters older tickets" do
            assert_equal 80, accounts(:minimum).satisfaction_ratings.response_rate(accounts(:minimum), 'start_time' => 1.hour.ago)
          end
        end

        describe "with start and end time" do
          before do
            Timecop.travel(1.day.ago) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
            Timecop.travel(1.day.from_now) do
              build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
            end
          end

          it "filters tickets outside range" do
            assert_equal 80, accounts(:minimum).satisfaction_ratings.response_rate(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
          end

          describe "with range after ticket solved and before satisfaction rating" do
            before do
              ticket = build_ticket(status_id: StatusType.SOLVED)
              Timecop.travel(3.hours.from_now) do
                build_satisfaction_rating(ticket, satisfaction_score: SatisfactionType.GOOD)
              end
            end

            it "filters satisfaction ratings submitted outside range" do
              Arturo.enable_feature!(:csat_metrics_adjustment)
              assert_equal 80, accounts(:minimum).satisfaction_ratings.response_rate(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end

            it "legacy includes satisfaction ratings submitted outside range" do
              assert_equal 83, accounts(:minimum).satisfaction_ratings.response_rate(accounts(:minimum), 'start_time' => 1.hour.ago, 'end_time' => 1.hour.from_now)
            end
          end
        end
      end
    end
  end
end
