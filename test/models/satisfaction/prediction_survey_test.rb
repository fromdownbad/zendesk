require_relative "../../support/test_helper"

SingleCov.covered!

describe Satisfaction::PredictionSurvey do
  describe '#validations' do
    fixtures :tickets
    let(:account) { accounts(:minimum) }
    let(:agent) { FactoryBot.create(:user, account: account, roles: Role::AGENT.id) }
    let(:ticket) { tickets(:minimum_1) }

    let(:survey) do
      Satisfaction::PredictionSurvey.new(
        reason: 'bad',
        prediction_value: 0.9239
      )
    end

    before do
      survey.account = account
      survey.user    = agent
      survey.ticket  = ticket
    end

    it 'should be valid' do
      assert survey.valid?
    end

    it 'should validate the presence of account_id' do
      survey.account_id = nil
      refute survey.valid?
    end

    it 'should validate the presence of agent_id' do
      survey.agent_id = nil
      refute survey.valid?
    end

    it 'should validate the presence of ticket_id' do
      survey.ticket_id = nil
      refute survey.valid?
    end

    it 'should validate the presence of reason' do
      survey.reason = nil
      refute survey.valid?
    end

    it 'should validate the presence of prediction_value' do
      survey.prediction_value = nil
      refute survey.valid?
    end
  end
end
