require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Satisfaction::Reason do
  fixtures :accounts, :brands, :tickets

  let(:account) { accounts(:minimum) }
  let(:brand) { brands(:minimum) }

  let(:reason) do
    Satisfaction::Reason.create(
      account: account, value: 'Poor quality'
    )
  end

  let(:ticket) { Ticket.all.find(&:satisfaction_reason) }

  before do
    account.default_brand = brand
  end

  describe 'creating a new reason code' do
    it 'is not system by default' do
      refute reason.system?
    end

    it 'draws new reason code starting with 1000' do
      assert_equal 1000, reason.reason_code
    end

    describe 'when there are system reasons for the same account' do
      before do
        Satisfaction::Reason.create_system_reasons_for(account)
      end

      it 'draws new reason code starting with 1000' do
        assert_equal 1000, reason.reason_code
      end
    end

    describe 'when there is an existing reason code for the same account' do
      before do
        Satisfaction::Reason.create(
          account: account, value: 'Slow response'
        )
      end

      it 'draws next available reason code' do
        assert_equal 1001, reason.reason_code
      end
    end
  end

  describe '.create_system_reasons_for' do
    let(:reasons) do
      account.satisfaction_reasons
    end

    before do
      Satisfaction::Reason.create_system_reasons_for(account)
    end

    it 'creates system reasons for account' do
      assert_equal Satisfaction::Reason::SYSTEM_REASONS.count, account.satisfaction_reasons.count(:all)
    end

    it 'creates reasons with system set to true' do
      assert reasons.all?(&:system)
    end

    it 'creates reasons with the default set of codes' do
      assert_equal reasons.pluck(:reason_code), [0, 5, 6, 7, 8, 100]
    end

    it 'creates brand_restrictions for default reasons and default brand' do
      assert_equal Satisfaction::Reason::DEFAULT_REASONS.count, account.default_brand.satisfaction_reasons.count(:all)
    end

    # during sandbox creation the default_brand_id on the account and the
    # account_id on the default brand can be out of sync
    describe 'when the default brand is inconsistent' do
      let(:brand) { brands(:support) }
      let(:account) do
        account = accounts(:minimum)
        account.settings.default_brand_id = brand.id
        account
      end

      it 'creates system reasons for account' do
        assert_equal Satisfaction::Reason::SYSTEM_REASONS.count, account.satisfaction_reasons.count(:all)
      end

      it 'does not create brand_restrictions for default reasons and default brand' do
        assert_equal 0, account.default_brand.satisfaction_reasons.count(:all)
      end
    end
  end

  describe '.shuffled_options_for' do
    let(:options) do
      Satisfaction::Reason.shuffled_options_for(brand, ticket)
    end

    before do
      Satisfaction::Reason.create_system_reasons_for(account)
    end

    it 'includes (shuffled) reasons restricted by brand' do
      reason = brand.satisfaction_reasons.first
      assert options.include?([reason.translated_value, reason.reason_code])
    end

    it 'includes none option in first position' do
      assert_equal options.first[1], Satisfaction::Reason::NONE
    end

    it 'includes other option in last position' do
      assert_equal options.last[1], Satisfaction::Reason::OTHER
    end

    it "doesn't include duplicate reasons" do
      dup_reason = account.satisfaction_reasons.find_by_reason_code(5)
      ticket.stubs(:satisfaction_reason).returns(dup_reason)

      assert_equal options.length, options.uniq.length
    end

    describe "when the reason has been deleted" do
      before do
        reason.soft_delete!
        ticket.stubs(:satisfaction_reason).returns(reason)
      end

      it "is still included in the shuffled options" do
        assert_includes options, [reason.value, reason.reason_code]
      end

      describe_with_arturo_enabled :csat_reason_clone do
        it "does not modify brand.satisfaction_reasons" do
          assert options # call Satisfaction::Reason.shuffled_options_for(brand, ticket)
          refute_includes brand.satisfaction_reasons, reason
        end

        describe "when the limit to the number of in-use reasons has been reached" do
          before do
            Satisfaction::Reason.create(
              account: account, value: 'Poor service'
            ).brand_restrictions.create(account: account, brand: brand)
          end

          it "does not go beyond the limit" do
            assert_equal 5, brand.satisfaction_reasons.size
            assert options # call Satisfaction::Reason.shuffled_options_for(brand, ticket)
            assert_operator 5, :>=, brand.satisfaction_reasons.size
          end
        end
      end
    end

    describe "when the reason has been deactivated for a brand" do
      before do
        reason.deactivate_for!(brand)
        ticket.stubs(:satisfaction_reason).returns(reason)
      end

      it "is still included in the shuffled options" do
        assert_includes options, [reason.value, reason.reason_code]
      end
    end
  end

  describe '#translated_value' do
    it 'returns the value' do
      assert_equal 'Poor quality', reason.translated_value
    end

    describe 'when reason is a system reason' do
      let(:reason) do
        Satisfaction::Reason.create(
          account: account,
          translation_key: 'type.satisfaction_reason_code.some_other_reason',
          reason_code: 100,
          system: true
        )
      end

      it 'returns the translated value' do
        assert_equal 'Some other reason', reason.translated_value
      end
    end

    describe 'when reason is a custom reason' do
      let(:reason) do
        Satisfaction::Reason.create(
          account: account,
          value: 'Custom Reason'
        )
      end

      it 'returns the value' do
        assert_equal 'Custom Reason', reason.translated_value
      end
    end

    describe 'when reason is a custom reason with dynamic content' do
      let(:reason) do
        Satisfaction::Reason.create(
          account: account,
          value: '{{dc.test}}'
        )
      end

      before do
        Zendesk::Liquid::DcContext.stubs(:render).returns('Custom Reason')
      end

      it 'returns the value' do
        assert_equal 'Custom Reason', reason.translated_value
      end
    end
  end

  describe "#activate_for" do
    before do
      reason.activate_for!(brand)
    end

    after do
      reason.deactivate_for!(brand)
    end

    it "activates this reason for a specified brand" do
      assert_includes brand.satisfaction_reasons.reload, reason
    end
  end

  describe "#deactivated?" do
    describe "when the brand is activated" do
      before do
        reason.activate_for!(brand)
      end

      after do
        reason.deactivate_for!(brand)
      end

      it "returns false" do
        assert_equal false, reason.deactivated?(brand)
      end
    end

    describe "when the brand is deactivated" do
      before do
        reason.deactivate_for!(brand)
      end

      it "returns true" do
        assert(reason.deactivated?(brand))
      end
    end
  end

  describe "#deactivate_for" do
    before do
      account.satisfaction_reason_brand_restrictions.create(
        satisfaction_reason_id: reason.id,
        brand: brand
      )
    end

    it "deactivates this reason for a specified brand" do
      reason.deactivate_for!(brand)

      refute_includes brand.satisfaction_reasons, reason
    end
  end

  describe "#next_reason_code" do
    before do
      reason.soft_delete!
    end

    it "does not reuse reason codes of deleted reasons" do
      new_reason = Satisfaction::Reason.create(
        account: account,
        value: 'does not reused the reason code'
      )

      refute_equal new_reason.reason_code, reason.reason_code
    end
  end
end
