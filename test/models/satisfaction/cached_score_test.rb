require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Satisfaction::CachedRatings' do
  fixtures :accounts, :users

  describe Satisfaction::CachedScore do
    describe "#satisfaction_rating" do
      before do
        @account = accounts(:minimum)
      end

      it "calculates without blowing up" do
        assert_nil @account.satisfaction_score
      end

      it "stores in proper cache" do
        @account.stubs(satisfaction_ratings: stub(current_score: 99))
        assert_equal 99, @account.satisfaction_score
        assert_equal 99, Rails.cache.read("#{@account.cache_key}/satisfaction_ratings")
      end

      it "caches nil" do
        @account.stubs(satisfaction_ratings: stub(current_score: nil))
        assert_nil @account.satisfaction_score
        @account.stubs(satisfaction_ratings: stub(current_score: 99))
        assert_nil @account.satisfaction_score
      end

      it "returns score" do
        @account.stubs(satisfaction_ratings: stub(current_score: 99))
        assert_equal 99, @account.satisfaction_score
      end

      it "returns cached score" do
        @account.stubs(satisfaction_ratings: stub(current_score: 99))
        @account.satisfaction_score
        @account.stubs(satisfaction_ratings: stub(current_score: 70))
        assert_equal 99, @account.satisfaction_score
      end

      describe "nested method" do
        before do
          @user = users(:minimum_admin)
        end

        it "returns for nested method" do
          @user.stubs(satisfaction_ratings_received: stub(current_score: 99))
          assert_equal 99, @user.satisfaction_score(:received)
        end

        it "caches nested methods in different caches" do
          @user.stubs(satisfaction_ratings_created: stub(current_score: 10))
          @user.stubs(satisfaction_ratings_received: stub(current_score: 99))
          assert_equal 99, @user.satisfaction_score(:received)
          assert_equal 99, @user.satisfaction_score(:received)
          assert_equal 10, @user.satisfaction_score(:created)
          assert_equal 10, @user.satisfaction_score(:created)
        end
      end
    end
  end
end
