require_relative "../../support/test_helper"
require_relative "../../support/satisfaction_test_helper"

SingleCov.covered! uncovered: 4

describe Satisfaction::Rating do
  include SatisfactionTestHelper
  fixtures :tickets, :events, :users, :accounts, :sequences, :ticket_fields

  let(:account) { accounts(:minimum) }
  let(:ticket) { build_ticket_with_satisfaction_rating }

  describe "Rebuilding from a rating event" do
    before do
      Account.any_instance.stubs(:is_serviceable?).returns(true)
      @event  = ticket.satisfaction_rating_scores.last
      @rating = Satisfaction::Rating.last
      assert_not_nil @event
      assert_not_nil @rating
    end

    it "populates all available attributes" do
      assert_equal @event.id,            @rating.event_id
      assert_equal @event.account_id,    @rating.account_id

      assert_equal ticket.id,                 @rating.ticket_id
      assert_equal ticket.satisfaction_score, @rating.score
      assert_equal ticket.assignee_id,        @rating.agent_user_id
      assert_equal ticket.group_id,           @rating.agent_group_id
    end
  end

  describe "latest scope" do
    before do
      ticket = build_ticket_with_satisfaction_rating
      rating1 = Satisfaction::Rating.last
      add_bad_satisfaction_rating_to_ticket(ticket)
      rating2 = Satisfaction::Rating.last
      rating2.update_attributes(created_at: rating1.created_at, score: SatisfactionType.GOODWITHCOMMENT)
    end

    it "does not duplicate any satisfaction ratings when encountering double post event" do
      assert_equal 2, account.satisfaction_ratings.size
      assert_equal 1, account.satisfaction_ratings.latest.size
    end
  end

  describe "#scrub_data!" do
    before do
      build_ticket_with_satisfaction_rating
      @rating = Satisfaction::Rating.last
    end

    describe "with non archived ticket" do
      it "scrubs the comment" do
        @rating.scrub_data!
        assert_equal Zendesk::Scrub::SCRUB_TEXT, @rating.comment.value
      end
    end

    describe "with archived ticket" do
      it "scrubs the comment" do
        Ticket.any_instance.stubs(:archived?).returns(false)
        @rating.scrub_data!
        assert_equal Zendesk::Scrub::SCRUB_TEXT, @rating.comment.value
      end
    end
  end

  describe ".distribution" do
    before { ticket } # we need to create the ticket

    it "returns the ratings if there are less than 100" do
      assert account.satisfaction_ratings.count(:all) > 0
      assert account.satisfaction_ratings.count(:all) < 100
      assert_equal Satisfaction::Rating.distribution(account).size, account.satisfaction_ratings.count(:all)
    end

    it "returns a distribution of the 100 most recent ratings if there are at least 100 ratings" do
      bad_rating = Satisfaction::Rating.new do |rating|
        rating.created_at = 1.hour.ago
        rating.score      = SatisfactionType.BAD
      end

      good_rating = Satisfaction::Rating.new do |rating|
        rating.created_at = 2.hours.ago
        rating.score      = SatisfactionType.GOOD
      end

      expects_chain(account, :satisfaction_ratings, :non_deleted_with_archived, :received, :latest, :select, :limit).returns(
        Array.new(75, bad_rating) + Array.new(25, good_rating)
      )

      assert_equal Satisfaction::Rating.distribution(account), Array.new(75, 0) + Array.new(25, 1)
    end

    it "does not contain ratings for deleted tickets" do
      assert_equal 1, Satisfaction::Rating.distribution(account).size
      ticket.will_be_saved_by(ticket.submitter)
      ticket.update_attribute(:status_id, StatusType.DELETED)
      assert_equal 0, Satisfaction::Rating.distribution(account).size
    end

    it "contains ratings for archived tickets" do
      assert_equal 1, Satisfaction::Rating.distribution(account).size
      ticket.will_be_saved_by(ticket.submitter)
      assert_not_equal StatusType.ARCHIVED, ticket.status_id
      ticket.update_attribute(:status_id, StatusType.ARCHIVED)
      assert_equal 1, Satisfaction::Rating.distribution(account).size
    end

    describe 'an archived ticket' do
      let(:archived_ticket) { tickets(:minimum_2) }

      before do
        assert_equal 1, Satisfaction::Rating.distribution(account).size
        add_bad_satisfaction_rating_to_ticket(archived_ticket, status_id: StatusType.ARCHIVED)
        archive_and_delete(archived_ticket) # delete from tickets table
        assert_nil Ticket.find_by_id(archived_ticket.id)
      end

      it "uses csrs associated with a ticket_archive_stub in the distribution" do
        assert_equal 2, Satisfaction::Rating.distribution(account).size
      end
    end
  end

  describe "#reason" do
    let(:rating) do
      ticket.satisfaction_rating.tap { |r| r.reason_code = Satisfaction::Reason::OTHER }
    end

    before do
      Satisfaction::Reason.create_system_reasons_for(accounts(:support))
      Satisfaction::Reason.create_system_reasons_for(accounts(:minimum))
    end

    it "returns a reason belonging to the current account" do
      assert_equal rating.reason.account_id, rating.account_id
    end

    it "returns the correct reason" do
      assert_equal rating.reason_code, rating.reason.reason_code
    end
  end

  describe "saving a ticket" do
    describe "on create" do
      let(:ticket) { tickets(:minimum_1) }

      describe "when a rating statistic for the ticket exists" do
        before do
          assert_difference('Satisfaction::Rating.count(:all)') do
            ticket.satisfaction_score = SatisfactionType.GOOD
            ticket.will_be_saved_by(ticket.requester)
            ticket.save!
          end
        end

        it "creates a new rating statistic" do
          assert_difference('Satisfaction::Rating.count(:all)') do
            ticket.satisfaction_score = SatisfactionType.BAD
            ticket.will_be_saved_by(ticket.requester)
            ticket.save!
          end
        end

        it "sets the correct status" do
          ticket.satisfaction_score = SatisfactionType.BAD
          ticket.will_be_saved_by(ticket.requester)
          ticket.save!

          assert_equal ticket.status_id, ticket.satisfaction_rating.status_id
          refute_nil ticket.satisfaction_rating.status_id
        end
      end

      describe "when rating with a comment" do
        before do
          ticket.satisfaction_score = SatisfactionType.GOOD
          ticket.satisfaction_comment = "Awesome!"
          ticket.will_be_saved_by(ticket.requester)
          ticket.save!

          @rating = account.satisfaction_ratings.first
        end

        it "adds a new satisfaction rating" do
          assert_equal SatisfactionType.GOODWITHCOMMENT, @rating.score
        end

        it "adds a comment" do
          assert_equal "Awesome!", @rating.comment.value
        end

        it "adds satisfaction comment" do
          assert_equal "Awesome!", ticket.satisfaction_comment
        end

        # For old satisfaction_ratings
        describe "when the comment_event_id is null" do
          before do
            @original_comment_id = @rating.comment_event_id
            @rating.comment_event_id = nil
            @rating.save!
          end

          describe "and the ticket is in the archive" do
            before do
              archive_and_delete(@rating.ticket)
              @rating.reload
            end

            it "gets the comment off the ticket" do
              assert @rating.comment
            end
          end

          describe_with_arturo_disabled :satisfaction_rating_disable_comment_fallback do
            it "grabs the comment from database" do
              assert_equal "Awesome!", @rating.comment.value
            end

            it "updates the comment_event_id for future use" do
              @rating.comment

              assert_equal @original_comment_id, @rating.comment_event_id
              assert_equal @original_comment_id, @rating.reload.comment_event_id
            end

            it "does not update comment_event_id if it is readonly" do
              @rating.stubs(readonly?: true)
              @rating.comment

              assert_nil @rating.comment_event_id
              assert_nil @rating.reload.comment_event_id
            end
          end

          describe_with_arturo_enabled :satisfaction_rating_disable_comment_fallback do
            it "does not grab the comment from database" do
              assert_nil @rating.comment
            end

            it "does not update the comment_event_id for future use" do
              @rating.comment

              assert_nil @rating.comment_event_id
              assert_nil @rating.reload.comment_event_id
            end
          end

          describe "when the event does not exist" do
            before do
              @rating.ticket.update_column(:status_id, StatusType.DELETED)
              archive_and_delete(@rating.ticket)
              @rating.reload
            end

            it "deletes satisfaction_rating" do
              assert_nil @rating.comment
              assert_equal StatusType.DELETED, @rating.status_id
            end
          end
        end

        describe "then rating without a comment" do
          before do
            ticket.satisfaction_score = SatisfactionType.BAD
            ticket.satisfaction_comment = ""
            ticket.will_be_saved_by(ticket.requester)
            ticket.save!

            @rating = account.satisfaction_ratings.first
          end

          it "adds a new satisfaction rating" do
            assert_equal SatisfactionType.BAD, @rating.score
          end

          it "adds a new comment" do
            assert_equal "", @rating.comment.value
          end

          it "does not reuse old comment" do
            assert_equal "", ticket.satisfaction_comment
          end

          it "sets the correct status" do
            assert_equal ticket.status_id, ticket.satisfaction_rating.status_id
            refute_nil ticket.satisfaction_rating.status_id
          end
        end

        describe "with an existing rating comment" do
          before do
            ticket.satisfaction_score = SatisfactionType.BAD
            ticket.satisfaction_comment = "Terrible!"
            ticket.will_be_saved_by(ticket.requester)
            ticket.save!

            @rating = account.satisfaction_ratings.first
          end

          it "adds a new satisfaction rating" do
            assert_equal SatisfactionType.BADWITHCOMMENT, @rating.score
          end

          it "adds new comment" do
            assert_equal "Terrible!", @rating.comment.value
          end

          it "does not reuse old comment" do
            assert_equal "Terrible!", ticket.satisfaction_comment
          end

          it "sets the correct status" do
            assert_equal ticket.status_id, ticket.satisfaction_rating.status_id
            refute_nil ticket.satisfaction_rating.status_id
          end
        end

        describe "then changing the comment" do
          before do
            ticket.satisfaction_comment = "Awesome comment updated!"
            assert_equal false, ticket.satisfaction_score_changed?
            ticket.will_be_saved_by(ticket.requester)
            ticket.save!

            # setting the instance variable to nil explicitly so that it fetches value from DB
            ticket.instance_variable_set(:@satisfaction_comment, nil)
          end

          it "displays the latest comment to requester" do
            assert_equal "Awesome comment updated!", ticket.latest_satisfaction_rating(ticket.requester)[:comment]
          end
        end
      end

      describe "when a rating statistic for the ticket doesn't exist" do
        it "creates a new rating statistic" do
          assert_difference('Satisfaction::Rating.count(:all)') do
            ticket.satisfaction_score = SatisfactionType.GOOD
            ticket.will_be_saved_by(ticket.requester)
            ticket.save!
          end
          rating = Satisfaction::Rating.last
          rating.reload
          assert_equal ticket.satisfaction_rating_scores.first.id, rating.event_id
          assert_equal SatisfactionType.GOOD, ticket.satisfaction_rating_scores.first.value.to_i
          assert_equal SatisfactionType.GOOD, rating.score
          assert_equal ticket.status_id, rating.status_id
        end
      end

      describe "when rating with a reason" do
        let(:csat_reason_code_enabled) { true }
        let(:score) { SatisfactionType.BAD }
        let(:reason_code) { Satisfaction::Reason::OTHER }

        before do
          Account.any_instance.stubs(:csat_reason_code_enabled?).returns(csat_reason_code_enabled)

          ticket.satisfaction_score = score
          ticket.satisfaction_reason_code = reason_code
          ticket.will_be_saved_by(ticket.requester)
          ticket.save!

          ticket.reload
        end

        describe "when csat_reason_code feature is enabled" do
          describe "when score is bad" do
            it "stores reason code on ticket" do
              assert_equal ticket.satisfaction_reason_code, reason_code
            end

            it "stores reason code on satisfaction rating" do
              assert_equal ticket.satisfaction_rating.reason_code, reason_code
            end
          end

          describe "when score is good" do
            let(:score) { SatisfactionType.GOOD }

            it "defaults reason code to none" do
              assert_equal ticket.satisfaction_reason_code, Satisfaction::Reason::NONE
            end
          end
        end
      end
    end

    describe "on update" do
      describe "when status is changed" do
        it "updates the status on the rating" do
          old_status = ticket.satisfaction_rating.status_id

          ticket.status_id = StatusType.SOLVED
          ticket.will_be_saved_by(ticket.assignee)
          ticket.save!

          assert_not_equal old_status, ticket.satisfaction_rating.reload.status_id
        end
      end

      describe "when the rating is changed from bad with reason code to good" do
        before do
          ticket.satisfaction_score       = SatisfactionType.BAD
          ticket.satisfaction_reason_code = Satisfaction::Reason::OTHER
          ticket.satisfaction_comment     = "Awesome!"
          ticket.will_be_saved_by(ticket.requester)
          ticket.save!

          ticket.satisfaction_score = SatisfactionType.GOOD
          ticket.will_be_saved_by(ticket.requester)
          ticket.save!
        end

        it "changes the reason code to NONE" do
          assert_equal Satisfaction::Reason::NONE, ticket.satisfaction_reason_code
        end
      end
    end
  end
end
