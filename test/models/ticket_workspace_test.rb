require_relative "../support/test_helper"
require_relative "../support/arturo_test_helper"

SingleCov.covered!

describe TicketWorkspace do
  include ArturoTestHelper
  fixtures :accounts, :tickets, :workspaces

  def without_notify_callback
    TicketWorkspace.skip_callback(:commit, :after, :notify_radar)
    yield
    TicketWorkspace.set_callback(:commit, :after, :notify_radar, if: :workspace_has_changed?)
  end

  def new_ticket_workspace
    tw = ticket.ticket_workspaces.new
    tw.workspace_id = workspace.id
    tw
  end

  let(:ticket) { tickets(:minimum_1) }
  let(:workspace) { workspaces(:billing_workspace) }
  let(:ticket_workspace) do
    tw = new_ticket_workspace
    without_notify_callback { tw.save! }
    tw.reload
  end

  describe "uniqueness validation" do
    it "validates uniqueness by ticket_id" do
      new_ticket_workspace = ticket_workspace.dup
      new_ticket_workspace.valid?

      # The error returned for ticket_id validation is on account_id, not ticket_id:
      assert_match 'has already been taken', new_ticket_workspace.errors[:account_id][0]

      new_ticket_workspace.ticket_id = 99999
      assert new_ticket_workspace.valid?, 'expected ticket_worspace to be valid with a unique ticket_id'
    end

    it "uses the correct index" do
      new_ticket_workspace = ticket_workspace.dup

      # query needs to be prepended with `account_id` to take advantage of db index:
      expected_sql = /SELECT  1 AS one FROM `ticket_workspaces` WHERE .*ticket_workspaces`.`account_id` = BINARY \d* AND `ticket_workspaces`.`ticket_id` = \d*\) LIMIT 1/

      assert_sql_queries(1, expected_sql) do
        new_ticket_workspace.valid?
      end
    end
  end

  describe "#workspace_has_changed?" do
    it 'returns true if new record' do
      TicketWorkspace.any_instance.expects(:notify_radar)

      new_ticket_workspace.save!
    end

    it 'returns true if workspace_id is updated' do
      ticket_workspace.expects(:notify_radar)

      ticket_workspace.update_attribute(:workspace_id, 123)
    end

    it 'returns true if workspace is newer' do
      ticket_workspace.expects(:notify_radar)

      without_notify_callback do
        ticket_workspace.update_attribute(:updated_at, 1.day.ago)
      end

      ticket_workspace.save!
    end

    it 'returns true if workspace is newer and ticket workspace is updated' do
      ticket_workspace.expects(:notify_radar)

      without_notify_callback do
        ticket_workspace.update_attribute(:updated_at, 1.day.ago)
      end

      # can't use touch, see https://github.com/rails/rails/issues/30466
      ticket_workspace.update_attribute(:updated_at, Time.now)
    end

    it 'returns true if record is being destroyed' do
      ticket_workspace.expects(:notify_radar)

      ticket_workspace.destroy!
    end

    it 'returns false if there are no relevant updates' do
      ticket_workspace.expects(:notify_radar).never

      without_notify_callback do
        ticket_workspace.update_attribute(:updated_at, workspace.updated_at + 1.day)
      end

      ticket_workspace.save!
    end
  end

  describe "#notify_radar" do
    let(:notification) { mock('notification') }
    let(:account) { accounts(:minimum) }

    it "sends a notification on update" do
      ::RadarFactory.expects(:create_radar_notification).
        with(account, "ticket/#{ticket.nice_id}/contextual_workspaces_reassigned").
        returns(notification)

      notification.expects(:send).with('updated', '')

      new_ticket_workspace.send(:notify_radar)
    end
  end
end
