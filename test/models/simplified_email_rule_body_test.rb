require_relative '../support/test_helper'

SingleCov.covered!

describe SimplifiedEmailRuleBody do
  fixtures :accounts, :rules

  let(:account) { accounts(:minimum) }
  let(:rule)    { rules(:trigger_notify_requester_of_received_request) }

  def create_simplified_email_rule_body(rule, rule_body)
    account.simplified_email_rule_bodies.create(rule_id: rule.id, rule_body: rule_body)
  end

  def rule_body(rule)
    rule.definition.actions[0].value[2]
  end

  it 'creates a simplified email rule' do
    create_simplified_email_rule_body(rule, rule_body(rule))
    assert_equal 1, account.simplified_email_rule_bodies.size
  end

  it 'deletes simplified_email_rule_body if the corresponding rule is deleted' do
    simplified_email_rule_body = create_simplified_email_rule_body(rule, rule_body(rule))
    assert_equal 1, account.simplified_email_rule_bodies.size

    account.rules.find(rule.id).destroy

    assert_raise(ActiveRecord::RecordNotFound) { account.rules.find(simplified_email_rule_body.rule_id) }
    assert_raise(ActiveRecord::RecordNotFound) { account.simplified_email_rule_bodies.find(simplified_email_rule_body.id) }
  end

  it 'does not delete the rule if corresponding simplified_email_rule_body is deleted' do
    simplified_email_rule_body = create_simplified_email_rule_body(rule, rule_body(rule))
    assert_equal 1, account.simplified_email_rule_bodies.size

    account.simplified_email_rule_bodies.find(simplified_email_rule_body.id).destroy

    assert_raise(ActiveRecord::RecordNotFound) { account.simplified_email_rule_bodies.find(simplified_email_rule_body.id) }
    assert account.rules.find(simplified_email_rule_body.rule_id)
  end
end
