require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 6

describe Logo do
  fixtures :logos

  it "is valid" do
    logo = logos(:minimum_logo)
    logo.expects(:valid_dimensions?).returns(true)
    assert logo.valid?
  end

  it "validates large image dimensions" do
    logo = logos(:minimum_logo)
    logo.uploaded_data = Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/lottapixel.jpg", content_type: "image/jpeg")
    refute logo.valid?
    assert_equal ["Image dimensions are invalid. Must be 30..64000000 pixels."], logo.errors[:base]
  end

  it "validates deceptively small image dimensions" do
    logo = logos(:minimum_logo)
    logo.uploaded_data = Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/lottaframes.gif", content_type: "image/gif")
    refute logo.valid?
    assert_equal ["Image dimensions are invalid. Must be 30..64000000 pixels."], logo.errors[:base]
  end
end
