require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Favicon do
  fixtures :logos

  describe "attachment options" do
    subject { Favicon.attachment_options }

    it "allow all of the .ico mime types as a content_type" do
      ["image/vnd.microsoft.icon", "image/ico", "image/icon", "text/ico", "application/ico", "image/x-icon"].each do |mime|
        assert subject[:content_type].include?(mime), "did not include #{mime}"
      end
    end
  end

  it "has a human name 'favicon'" do
    assert_equal 'favicon', Favicon.human_name
  end

  it "sets up account_id on its thumbnails" do
    HeaderLogo.any_instance.stubs(:stores).returns([:fs])
    account = Account.last
    HeaderLogo.set_for_account(account,
      uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png"))
    account.reload
    assert account.header_logo
    assert_equal account.id, account.header_logo.thumbnails.first.account_id
  end

  it "is valid" do
    logo = logos(:minimum_logo)
    logo.expects(:valid_dimensions?).returns(true)
    assert logo.valid?
  end

  it "validates large image dimensions" do
    logo = logos(:minimum_logo)
    logo.uploaded_data = Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/lottapixel.jpg", content_type: "image/jpeg")
    refute logo.valid?
    assert_equal ["Image dimensions are invalid. Must be 30..64000000 pixels."], logo.errors[:base]
  end

  it "validates deceptively small image dimensions" do
    logo = logos(:minimum_logo)
    logo.uploaded_data = Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/lottaframes.gif", content_type: "image/gif")
    refute logo.valid?
    assert_equal ["Image dimensions are invalid. Must be 30..64000000 pixels."], logo.errors[:base]
  end
end
