require_relative "../../support/test_helper"

SingleCov.covered!

describe Guide::Subscription do
  fixtures :accounts

  describe 'associations' do
    let(:account) { accounts(:minimum) }
    let(:subscription) do
      FactoryBot.build(
        :guide_subscription,
        account: account
      )
    end

    it "belongs to an account" do
      assert subscription.account.present?
    end
  end

  describe 'audit log' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { FactoryBot.create(:guide_subscription, account: account) }
    let(:audit)        { CIA::Event.last }

    before do
      assert_nil CIA::Event.last
    end

    describe 'requirements' do
      let(:audited_attributes) do
        [
          'account_id',
          'active',
          'plan_type',
          'max_agents'
        ]
      end

      it 'is required for specific attributes' do
        assert_equal audited_attributes, subscription.audited_attributes
      end
    end

    describe 'updating any of the auditable attributes' do
      before do
        CIA.audit actor: subscription.account.owner do
          subscription.update_attributes!(max_agents: 1)
        end
      end

      it 'generates an audit log' do
        assert_equal 'update', audit.action
        assert_equal subscription.account.owner, audit.actor
        assert_equal subscription, audit.source
      end
    end
  end

  describe 'instance methods' do
    describe '#days_left_in_trial' do
      let(:now) { Time.zone.now.change(usec: 0) }

      let(:subscription) do
        Guide::Subscription.new(trial_expires_at: trial_expires_at)
      end

      before { Timecop.freeze(now) }

      describe 'when the :trial_expires_at attribute is not set' do
        let(:trial_expires_at) { nil }

        it 'returns nil' do
          assert_nil subscription.days_left_in_trial
        end
      end

      describe 'when there are 0 days in trial remaining' do
        let(:trial_expires_at) { now }

        it 'returns 1' do
          assert_equal 0, subscription.days_left_in_trial
        end
      end

      describe 'when there is 0.5 days in trial remaining' do
        let(:trial_expires_at) { now + 12.hours }

        it 'returns 1' do
          assert_equal 1, subscription.days_left_in_trial
        end
      end

      describe 'when there is 1 day in trial remaining' do
        let(:trial_expires_at) { now + 24.hours }

        it 'returns 1' do
          assert_equal 1, subscription.days_left_in_trial
        end
      end

      describe 'when there is 1.5 days in trial remaining' do
        let(:trial_expires_at) { now + 36.hours }

        it 'returns 1' do
          assert_equal 2, subscription.days_left_in_trial
        end
      end
    end

    describe '#is_active?' do
      let(:subscription) do
        FactoryBot.build(
          :guide_subscription,
          account: accounts(:minimum),
          active:  active
        )
      end

      describe 'when the active attribute is true' do
        let(:active) { true }

        it "returns true" do
          assert(subscription.is_active?)
        end
      end

      describe 'when the active attribute is false' do
        let(:active) { false }

        it "returns false" do
          assert_equal false, subscription.is_active?
        end
      end
    end

    describe '#legacy?' do
      let(:subscription) { Guide::Subscription.new(legacy: legacy) }

      describe 'when the legacy attribute is true' do
        let(:legacy) { true }

        it 'returns true' do
          assert subscription.legacy?
        end
      end

      describe 'when the legacy attribute is false' do
        let(:legacy) { false }

        it 'returns false' do
          refute subscription.legacy?
        end
      end
    end

    describe '#deactivate' do
      let(:account) { accounts(:minimum) }
      let(:subscription) do
        FactoryBot.create(
          :guide_subscription,
          active:  true,
          account: account
        )
      end

      it 'changes the active attribute to false' do
        subscription.deactivate
        assert_equal false, subscription.active?
      end
    end

    describe '#plan_name' do
      let(:subscription) { Guide::Subscription.new(plan_type: plan_type) }

      describe "when plan type is 'Lite'" do
        let(:plan_type) { ZBC::Guide::PlanType::Lite.plan_type }

        it "returns 'Lite'" do
          assert_equal 'Lite', subscription.plan_name
        end
      end

      describe "when plan type is 'Professional'" do
        let(:plan_type) { ZBC::Guide::PlanType::Professional.plan_type }

        it "returns 'Professional'" do
          assert_equal 'Professional', subscription.plan_name
        end
      end

      describe "when plan type does not exist" do
        let(:plan_type) { -1 }

        it 'returns nil' do
          assert_nil subscription.plan_name
        end
      end
    end

    describe '#trial?' do
      let(:subscription) { Guide::Subscription.new(trial: trial) }

      describe 'when the trial attribute is true' do
        let(:trial) { true }

        it 'returns true' do
          assert subscription.trial?
        end
      end

      describe 'when the trial attribute is false' do
        let(:trial) { false }

        it 'returns false' do
          refute subscription.trial?
        end
      end
    end
  end
end
