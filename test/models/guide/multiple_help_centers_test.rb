require_relative "../../support/test_helper"

SingleCov.covered!

describe Guide::MultipleHelpCenters do
  let(:account) { accounts(:minimum) }

  let(:account_client) { stub }
  let(:guide_plan_type) { 1 }
  let(:product) do
    Zendesk::Accounts::Product.new(
      'name' => 'guide',
      'state' => 'free',
      'plan_settings' => { 'plan_type' => guide_plan_type },
      'active' => true
    )
  end

  let(:multiple_help_centers) { Guide::MultipleHelpCenters.new(account) }

  before do
    Zendesk::Accounts::Client.stubs(:new).with(account).returns(account_client)
    account_client.stubs(:product).with('guide').returns(product)
  end

  describe 'available?' do
    describe 'without multibrand' do
      before do
        account.stubs(:has_multibrand?).returns(false)
      end

      it 'returns false' do
        refute multiple_help_centers.available?
      end

      describe 'with multibrand_includes_help_centers flag' do
        before do
          account.settings.enable(:multibrand_includes_help_centers)
          account.settings.save!
        end

        it 'returns false' do
          refute multiple_help_centers.available?
        end
      end

      describe 'with a guide plan with multiple_help_centers' do
        let(:guide_plan_type) { 4 }

        it 'returns false' do
          refute multiple_help_centers.available?
        end
      end

      describe 'SP&P account' do
        before do
          account.stubs(:spp?).returns(true)
        end

        it 'returns false' do
          refute multiple_help_centers.available?
        end
      end
    end

    describe 'with multibrand' do
      before do
        account.stubs(:has_multibrand?).returns(true)
      end

      it 'returns false' do
        refute multiple_help_centers.available?
      end

      describe 'with multibrand_includes_help_centers flag' do
        before do
          account.settings.enable(:multibrand_includes_help_centers)
          account.settings.save!
        end

        it 'returns true' do
          assert multiple_help_centers.available?
        end
      end

      describe 'with unlimited_multibrand' do
        before do
          account.stubs(:has_unlimited_multibrand?).returns(true)
        end

        it 'returns true' do
          assert multiple_help_centers.available?
        end
      end

      describe 'with a guide plan with multiple_help_centers' do
        let(:guide_plan_type) { 4 }

        it 'returns true' do
          assert multiple_help_centers.available?
        end
      end

      describe 'without a guide plan' do
        before do
          account_client.stubs(:product).with('guide').returns(nil)
        end

        it 'returns false' do
          refute multiple_help_centers.available?
        end
      end

      describe 'SP&P account' do
        before do
          account.stubs(:spp?).returns(true)
        end

        it 'returns true' do
          assert multiple_help_centers.available?
        end
      end
    end
  end

  describe 'available_via_support?' do
    describe 'without multibrand' do
      before do
        account.stubs(:has_multibrand?).returns(false)
      end

      it 'returns false' do
        refute multiple_help_centers.available_via_support?
      end

      describe 'with multibrand_includes_help_centers flag' do
        before do
          account.settings.enable(:multibrand_includes_help_centers)
          account.settings.save!
        end

        it 'returns false' do
          refute multiple_help_centers.available?
        end
      end
    end

    describe 'with multibrand' do
      before do
        account.stubs(:has_multibrand?).returns(true)
      end

      it 'returns false' do
        refute multiple_help_centers.available_via_support?
      end

      describe 'with multibrand_includes_help_centers flag' do
        before do
          account.settings.enable(:multibrand_includes_help_centers)
          account.settings.save!
        end

        it 'returns true' do
          assert multiple_help_centers.available_via_support?
        end
      end

      describe 'with unlimited_multibrand' do
        before do
          account.stubs(:has_unlimited_multibrand?).returns(true)
        end

        it 'returns true' do
          assert multiple_help_centers.available_via_support?
        end
      end
    end
  end
end
