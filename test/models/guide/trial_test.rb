require_relative "../../support/test_helper"

SingleCov.covered!

describe Guide::Trial do
  fixtures :accounts

  describe 'guide_product_payload' do
    let(:account) do
      account = MiniTest::Mock.new
      account.expect :trial_extras_alternative_length?, false
      account
    end
    let(:suite) { false }
    let(:expires_at) { '2018-01-30T23:59:59+00:00' }
    let(:expected_payload) do
      {
        product: {
          state: :trial,
          trial_expires_at: expires_at,
          plan_settings: {
            plan_type: 2,
            suite: suite
          }
        }
      }
    end

    before do
      Timecop.freeze(2018, 1, 1, 9, 0, 0)
    end

    it 'returns the correct product payload to send to Account Service' do
      assert_equal(expected_payload, Guide::Trial.guide_product_payload(account))
    end

    describe 'with suite:true' do
      let(:suite) { true }

      it 'returns the correct product payload to send to Account Service' do
        assert_equal(expected_payload, Guide::Trial.guide_product_payload(account, suite: true))
      end
    end

    describe 'with an account in 14-day trial treatment group' do
      let(:expires_at) { '2018-01-14T23:59:59+00:00' }
      let(:account) do
        account = MiniTest::Mock.new
        account.expect :trial_extras_alternative_length?, true
        account
      end

      it 'returns the correct product payload to send to Account Service' do
        assert_equal(expected_payload, Guide::Trial.guide_product_payload(account))
      end
    end

    describe 'for an account with the classic_14_day_trial arturo enabled' do
      let(:expires_at) { '2018-01-14T23:59:59+00:00' }
      let(:account) do
        account = accounts(:minimum)

        account.add_trial_extras(alternative_length: false)
        Arturo.enable_feature!(:classic_14_day_trial)

        account.save!
        account
      end

      it 'returns the correct product payload to send to Account Service' do
        assert_equal(expected_payload, Guide::Trial.guide_product_payload(account))
      end
    end
  end
end
