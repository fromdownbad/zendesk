require_relative "../../../support/test_helper"

SingleCov.covered!

describe Guide::Subscription::Previewer do
  let(:described_class) { Guide::Subscription::Previewer }

  describe '.for_account' do
    let(:now)          { Time.zone.now.change(usec: 0) }
    let(:account)      { accounts(:minimum) }
    let(:subscription) { account.subscription }

    subject { described_class.for_account(account) }

    before { Timecop.freeze(now) }

    describe 'for an account with a Guide subscription' do
      let(:guide_subscription) do
        FactoryBot.build(
          :guide_subscription,
          account: account,
          active:  active
        )
      end

      before { account.guide_subscription = guide_subscription }

      describe 'when the subscription is active' do
        let(:active) { true }

        it 'returns the Guide subscription' do
          assert_equal guide_subscription, subject
        end
      end

      describe 'when the subscription is not active' do
        let(:active) { false }

        it 'defers to the account service' do
          described_class.any_instance.
            expects(:preview_account_service_product).once

          subject
        end
      end
    end

    describe 'for an account without a Guide subscription' do
      before { assert_nil account.guide_subscription }

      it 'defers to the account service' do
        described_class.any_instance.
          expects(:preview_account_service_product).once

        subject
      end

      describe_with_arturo_enabled 'guide_account_service_error' do
        before do
          Zendesk::Accounts::Client.any_instance.stubs(:product!)
        end

        it 'calls product! on account service client' do
          Zendesk::Accounts::Client.any_instance.expects(:product!)

          subject
        end
      end

      describe_with_arturo_disabled 'guide_account_service_error' do
        before do
          Zendesk::Accounts::Client.any_instance.stubs(:product).with('guide', use_cache: anything)
        end

        describe 'caching behavior' do
          it 'calls product on account service client using cache' do
            Zendesk::Accounts::Client.any_instance.expects(:product).with('guide', use_cache: true)

            subject
          end
        end
      end

      describe 'when the account service returns a successful response' do
        before do
          Zendesk::Accounts::Client.any_instance.stubs(product!: product)
          Zendesk::Accounts::Client.any_instance.stubs(product: product)
        end

        describe 'for an account with an unstarted trial' do
          let(:product) do
            Zendesk::Accounts::Product.new(
              'state'            => :not_started,
              'name'             => :guide,
              'active'           => true,
              'plan_settings'    => {
                'max_agents' => 10,
                'plan_type'  => 2
              },
              'trial_expires_at' => nil,
              'state_updated_at' => Time.zone.now,
              'created_at'       => Time.zone.now,
              'updated_at'       => Time.zone.now
            )
          end

          it 'returns nil' do
            assert_nil subject
          end
        end

        describe 'for an account with an active trial' do
          let(:product) do
            Zendesk::Accounts::Product.new(
              'state'            => :trial,
              'name'             => :guide,
              'active'           => true,
              'plan_settings'    => {
                'max_agents' => 10,
                'plan_type'  => 2
              },
              'trial_expires_at' => Time.zone.now,
              'state_updated_at' => Time.zone.now,
              'created_at'       => Time.zone.now,
              'updated_at'       => Time.zone.now
            )
          end

          it 'returns a Guide::Subscription with the correct attributes' do
            assert_equal Guide::Subscription, subject.class
            assert(subject.active?)
            assert_equal false,                   subject.legacy?
            assert_equal subscription.max_agents, subject.max_agents
            assert_equal 2,                       subject.plan_type
            assert(subject.trial?)
            assert_equal Time.zone.now, subject.trial_expires_at
            assert_equal 0, subject.days_left_in_trial
          end
        end

        describe 'for an account with an expired trial' do
          let(:product) do
            Zendesk::Accounts::Product.new(
              'state'            => :expired,
              'name'             => :guide,
              'active'           => false,
              'plan_settings'    => {
                'max_agents' => 10,
                'plan_type'  => 2
              },
              'trial_expires_at' => Time.zone.now,
              'state_updated_at' => Time.zone.now,
              'created_at'       => Time.zone.now,
              'updated_at'       => Time.zone.now
            )
          end

          it 'returns a Guide::Subscription with the correct attributes' do
            assert_equal Guide::Subscription,     subject.class
            assert_equal false,                   subject.active?
            assert_equal false,                   subject.legacy?
            assert_equal subscription.max_agents, subject.max_agents
            assert_equal 2,                       subject.plan_type
            assert(subject.trial?)
            assert_equal Time.zone.now, subject.trial_expires_at
            assert_equal 0, subject.days_left_in_trial
          end
        end

        describe 'for an account with a legacy subscription' do
          let(:product) do
            Zendesk::Accounts::Product.new(
              'state'            => :free,
              'name'             => :guide,
              'active'           => true,
              'plan_settings'    => {
                'max_agents' => 10,
                'plan_type'  => 3
              },
              'trial_expires_at' => nil,
              'state_updated_at' => Time.zone.now,
              'created_at'       => Time.zone.now,
              'updated_at'       => Time.zone.now
            )
          end

          it 'returns a Guide::Subscription with the correct attributes' do
            # Note: 'Legacy' (3) plan_types get built without a plan_type

            assert_equal Guide::Subscription, subject.class
            assert(subject.active?)
            assert(subject.legacy?)
            assert_equal subscription.max_agents, subject.max_agents
            assert_nil subject.plan_type
            assert_equal false, subject.trial?
            assert_nil                     subject.trial_expires_at
            assert_nil                     subject.days_left_in_trial
          end
        end
      end

      describe 'when the account service returns an unsuccessful response' do
        let(:uri) do
          "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/" \
          "accounts/#{account.id}/products/guide"
        end

        before { stub_request(:get, uri).to_raise(error) }

        describe 'when the :guide_account_service_error arturo is enabled' do
          before { Arturo.enable_feature!(:guide_account_service_error) }

          describe 'when a Kragle::ResourceNotFound error is raised' do
            # A Kragle::ResourceNotFound error is raised when the account
            # service cannot find a specified product for the account. This may
            # be because the account has not activated the product yet.

            let(:error) { Kragle::ResourceNotFound }

            it 'returns nil' do
              assert_nil subject
            end
          end

          describe 'when a Kragle::ServerError error is raised' do
            let(:error) { Kragle::ServerError }

            it 'propagates the error' do
              assert_raises(Kragle::ServerError) { subject }
            end
          end
        end

        describe 'when the :guide_account_service_error arturo is disabled' do
          before { Arturo.disable_feature!(:guide_account_service_error) }

          describe 'when a Kragle::ResourceNotFound error is raised' do
            # A Kragle::ResourceNotFound error is raised when the account
            # service cannot find a specified product for the account. This may
            # be because the account has not activated the product yet.

            let(:error) { Kragle::ResourceNotFound }

            it 'returns nil' do
              assert_nil subject
            end
          end

          describe 'when a Kragle::ServerError error is raised' do
            let(:error) { Kragle::ServerError }

            it 'returns nil' do
              assert_nil subject
            end
          end
        end
      end
    end
  end
end
