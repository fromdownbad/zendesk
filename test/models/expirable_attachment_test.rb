require_relative "../support/test_helper"
require_relative "../support/attachment_test_helper"

SingleCov.covered! uncovered: 1

class ThreeWeeksAttachment < ExpirableAttachment
  expirable ttl: 3.weeks
end

describe ExpirableAttachment do
  fixtures :accounts
  should_validate_presence_of :account_id, :author_id, :created_via

  before { @account = accounts(:minimum) }

  describe "Creation" do
    it "sets the expire_at (defaults to two weeks)" do
      attachment = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", @account)
      assert_equal 2.weeks.from_now.strftime("%Y/%m/%d"), attachment.expire_at.strftime("%Y/%m/%d")
    end

    it "places the attachment file in the expirable_attachments directory" do
      attachment = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", @account)
      attachment.public_filename.must_include "expirable_attachments"
    end

    it "allows to be overridden" do
      attachment = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", @account, klass: ThreeWeeksAttachment)
      assert_equal 3.weeks.from_now.strftime("%Y/%m/%d"), attachment.expire_at.strftime("%Y/%m/%d")
    end
  end

  describe "#destroy" do
    it "should not attempt to destroy non-existent thumbnails" do
      attachment = create_expirable_attachment("#{Rails.root}/test/files/normal_1.jpg", @account)
      stub_request(:delete, %r{\.amazonaws\.com\/data\/expirable_attachments}).to_return(status: 200, body: "", headers: {})
      attachment.expects(:image?).never
      attachment.expects(:valid_dimensions?).never
      attachment.expects(:thumbnails).never
      assert attachment.destroy
    end
  end

  describe "Class methods" do
    describe "#find_by_token_or_id_and_account_id!" do
      it "finds the first record by token" do
        attachment = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", @account)
        token = attachment.token
        assert_equal attachment, ExpirableAttachment.find_by_token_or_id_and_account_id!(token, @account)
      end

      it "finds the first record by id" do
        attachment = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", @account)
        id = attachment.id
        assert_equal attachment, ExpirableAttachment.find_by_token_or_id_and_account_id!(id, @account)
      end

      it "raises ActiveRecord::RecordNotFound with blank value" do
        assert_raises ActiveRecord::RecordNotFound do
          ExpirableAttachment.find_by_token_or_id_and_account_id!('', @account)
        end
      end

      it "raises ActiveRecord::RecordNotFound for not found id_or_token" do
        assert_raises ActiveRecord::RecordNotFound do
          ExpirableAttachment.find_by_token_or_id_and_account_id!('123', @account)
        end
      end
    end

    describe "#cleanup" do
      it "finds all the expired attachments and delete them" do
        ExpirableAttachment.any_instance.stubs(stores: [:fs])
        create_expirable_attachment("#{Rails.root}/test/files/normal_1.jpg", @account)
        create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", @account)
        original_attachments = ExpirableAttachment.all
        original_attachments.each do |attachment|
          attachment.update_attribute(:expire_at, 1.day.ago)
        end

        create_expirable_attachment("#{Rails.root}/test/files/highrise.xml", @account)
        ExpirableAttachment.cleanup
        remaining_attachments = ExpirableAttachment.all
        assert_equal 1, remaining_attachments.length
        remaining_attachments.first.public_filename.must_include "highrise.xml"
      end
    end
  end

  describe "Associations" do
    describe "latest" do
      it "thes latest created of the given type" do
        attachment = create_expirable_attachment("#{Rails.root}/test/files/normal_1.jpg", @account)
        attachment.update_attribute(:created_at, 1.day.ago)
        latest = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", @account)
        assert_equal latest, @account.expirable_attachments.latest(XmlExportJob.name).first
      end
    end
  end

  describe "#filename" do
    it "stores the in the display_filename field the original_filename (specifically for non-ascii characters that get converted to _)" do
      # SJT - TODO our CI box doesn't handle cyrillic characters at the filesystem level
#      attachment = create_expirable_attachment("#{Rails.root}/test/files/урожай.xml", @account)
#      assert_equal "______.xml", attachment.filename

      attachment = create_expirable_attachment("#{Rails.root}/test/files/normal_1.jpg", @account)
      assert_equal "normal_1.jpg", attachment.filename
    end
  end

  describe "#display_filename" do
    it "shows the display_filename if the field is not empty. Otherwise show the filename" do
      # SJT - TODO our CI box doesn't handle cyrillic characters at the filesystem level
#      attachment = create_expirable_attachment("#{Rails.root}/test/files/урожай.xml", @account)
#      assert_equal "урожай.xml", attachment.display_filename

      attachment = create_expirable_attachment("#{Rails.root}/test/files/normal_1.jpg", @account)
      attachment.update_attribute(:display_filename, nil)
      assert_equal "normal_1.jpg", attachment.display_filename
    end
  end

  describe "#url" do
    it "generates a public url for the attachment" do
      attachment = create_expirable_attachment("#{Rails.root}/test/files/normal_1.jpg", @account)
      assert_equal "https://#{attachment.account.subdomain}.zendesk-test.com/expirable_attachments/token/#{attachment.token}/?name=normal_1.jpg", attachment.url
    end

    it "always be expirable_attachment even if it's subclassed" do
      attachment = create_expirable_attachment("#{Rails.root}/test/files/normal_1.jpg", @account, klass: ThreeWeeksAttachment)
      assert_equal "https://#{attachment.account.subdomain}.zendesk-test.com/expirable_attachments/token/#{attachment.token}/?name=normal_1.jpg", attachment.url
    end

    describe "when host mapping is enabled" do
      let(:attachment) do
        create_expirable_attachment(
          "#{Rails.root}/test/files/normal_1.jpg",
          @account,
          klass: ThreeWeeksAttachment
        )
      end

      before { @account.update_attribute(:host_mapping, 'hello.example.com') }

      it "defaults to not using the host-mapped domain" do
        assert_match(
          %r{^#{attachment.account.url(mapped: false)}},
          attachment.url
        )
      end

      it "respects the 'mapped' option" do
        assert_match(
          %r{^#{attachment.account.url(mapped: true)}},
          attachment.url(mapped: true)
        )
      end
    end
  end

  describe "#to_liquid" do
    it "generates the url in liquid form" do
      attachment = create_expirable_attachment("#{Rails.root}/test/files/normal_1.jpg", @account)
      liquid = {"url" => "https://#{attachment.account.subdomain}.zendesk-test.com/expirable_attachments/token/#{attachment.token}/?name=normal_1.jpg", "filename" => "normal_1.jpg"}
      assert_equal liquid, attachment.to_liquid
    end
  end
end
