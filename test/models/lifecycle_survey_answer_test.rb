require_relative '../support/test_helper'

SingleCov.covered!

describe LifecycleSurveyAnswer do
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:subject) do
    LifecycleSurveyAnswer.new(
      account: account,
      user: account.owner,
      answer_key: 'some.prefix.other',
      survey_name: 'some survey'
    )
  end

  it 'knows which option is "other"' do
    assert subject.send(:option_is_other?)
  end

  it 'does not be valid if account is missing' do
    assert subject.valid?

    subject.account = nil
    refute subject.valid?
  end

  it 'does not be valid if user is missing' do
    assert subject.valid?

    subject.user = nil
    refute subject.valid?
  end

  it 'does not be valid if answer_key is missing' do
    assert subject.valid?

    subject.answer_key = nil
    refute subject.valid?
  end

  it 'does not be valid if survey_name is missing' do
    assert subject.valid?

    subject.survey_name = nil
    refute subject.valid?
  end

  it 'saves answer_text only when option is "other"' do
    subject.update_attribute(:answer_text, 'some text')
    assert_equal 'some text', subject.reload.answer_text

    subject.update_attributes(
      answer_key: 'a.not.end.with.other.option',
      answer_text: 'some text'
    )
    assert_nil subject.reload.answer_text
  end

  it 'creates answers from valid params' do
    answers = LifecycleSurveyAnswer.from_answers(
      account,
      account.owner,
      survey_name: 'test survey',
      answer_keys: %w[key1 key2]
    )

    assert_equal 2, answers.size
    assert_equal %w[key1 key2], answers.map(&:answer_key)
  end

  it 'returns nil from invalid params' do
    answers = LifecycleSurveyAnswer.from_answers(
      account,
      account.owner,
      survey_name: 'test survey'
    )

    assert_nil answers
  end
end
