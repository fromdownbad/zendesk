require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe JetpackTaskList do
  fixtures :accounts, :users

  before do
    subscription = stub('subscription')
    @account = accounts(:minimum)
    @account.stubs(:subscription).returns(subscription)
    @account.stubs(:created_via_google_app_market?).returns(false)
    @admin = users(:minimum_admin)
    @agent = users(:minimum_agent)
  end

  describe "for any user" do
    before do
      @account.subscription.stubs(has_chat?: true)
      @task_list = JetpackTaskList.new(@admin)
    end

    it "has an 'enabled' key for every task" do
      assert @task_list.all.all? { |task| !task.enabled.nil? }
    end

    it "has an owner_id for every task" do
      assert @task_list.all.all? { |task| task.owner_id.present? }
    end

    it "has an owner_type for every task" do
      assert @task_list.all.all? { |task| task.owner_type.present? }
    end

    it "has an account_id for every task" do
      assert @task_list.all.all? { |task| task.account_id.present? }
    end

    it "updates a new JetpackTask record" do
      attributes = {section: 'admin', enabled: true}
      attributes_with_status = attributes.merge(status: 'single')
      task = JetpackTask.new
      task.expects(:update_attributes).with(attributes_with_status)
      JetpackTask.stubs(:for_owner).returns([])
      JetpackTask.expects(:new).returns(task)
      JetpackTaskList::TaskAttributes.any_instance.stubs(:attributes_for_key).returns(attributes)

      @task_list.update('pineapple', 'single')
    end

    it "updates an existing JetpackTask record" do
      attributes = {section: 'admin', enabled: true}
      attributes_with_status = attributes.merge(status: 'married')
      task = JetpackTask.new(key: 'jello')
      task.expects(:update_attributes).with(attributes_with_status)
      JetpackTask.stubs(:for_owner).returns([task])
      JetpackTask.expects(:new).never
      JetpackTaskList::TaskAttributes.any_instance.expects(:attributes_for_key).returns(attributes)

      @task_list.update('jello', 'married')
    end
  end

  describe "#all" do
    before do
      @account.subscription.stubs(has_chat?: true)
    end

    it "provides both user and account task to an admin" do
      JetpackTaskList::TaskAttributes.any_instance.expects(:attribute_collection_for_owner).
        with(:configure).returns([{key: 'mock_configure_task'}])
      JetpackTaskList::TaskAttributes.any_instance.expects(:attribute_collection_for_owner).
        with(:channels).returns([{key: 'mock_channels_task'}])
      JetpackTaskList::TaskAttributes.any_instance.expects(:attribute_collection_for_owner).
        with(:user).returns([{key: 'mock_user_task'}])
      task_list = JetpackTaskList.new(@admin)

      task_keys = task_list.all.map { |task| task[:key] }
      assert task_keys.include?('mock_configure_task')
      assert task_keys.include?('mock_channels_task')
      assert task_keys.include?('mock_user_task')
    end

    it "provides only user tasks to an agent" do
      JetpackTaskList::TaskAttributes.any_instance.
        expects(:attribute_collection_for_owner).
        never.with(:configure)
      JetpackTaskList::TaskAttributes.any_instance.
        expects(:attribute_collection_for_owner).
        never.with(:channels)
      JetpackTaskList::TaskAttributes.any_instance.
        expects(:attribute_collection_for_owner).
        with(:user).returns([{key: 'mock_user_task'}])
      task_list = JetpackTaskList.new(@agent)

      task_keys = task_list.all.map { |task| task[:key] }
      assert task_keys.include?('mock_user_task')
    end
  end

  describe ".TaskAttributes" do
    before do
      @account.subscription.stubs(has_chat?: true)
      @task_attrs = JetpackTaskList::TaskAttributes.new(@admin)
    end

    it "has a collection of dynamically added properties" do
      has_all_keys = @task_attrs.attribute_collection[:configure].all? do |attrs|
        attrs.key?(:enabled) && attrs.key?(:owner) && attrs.key?(:account)
      end

      assert has_all_keys
    end

    describe "#attributes_for_key" do
      it "finds the appropriate attributes for a task" do
        expected_attrs = {
          key: 'add_agents', section: 'admin',
          enabled: true, owner: @account, account: @account
        }
        assert_equal expected_attrs, @task_attrs.attributes_for_key('add_agents')
      end

      it "raises an exception if an unknown key is used" do
        assert_raises JetpackTaskList::TaskAttributes::UnknownTaskTypeError do
          @task_attrs.attributes_for_key('lake_placid')
        end
      end
    end

    describe "customization" do
      describe "with chat" do
        before do
          @admin.account.subscription.stubs(has_chat?: true)
          @task_attrs = JetpackTaskList::TaskAttributes.new(@admin)
        end

        it "has the default configure_email_v2 task for non-G Suite accounts" do
          @task_attrs.attributes_for_key('configure_email_v2')

          assert_raises JetpackTaskList::TaskAttributes::UnknownTaskTypeError do
            @task_attrs.attributes_for_key('configure_google_email')
          end
        end

        it "has the configure_google_email task for G Suite accounts" do
          @admin = users(:minimum_admin)
          @admin.account.subscription.stubs(has_chat?: true)
          @admin.account.stubs(created_via_google_app_market?: true)
          @task_attrs = JetpackTaskList::TaskAttributes.new(@admin)

          @task_attrs.attributes_for_key('configure_google_email')

          assert_raises JetpackTaskList::TaskAttributes::UnknownTaskTypeError do
            @task_attrs.attributes_for_key('configure_email_v2')
          end
        end

        it "has the add_zopim_chat_v2 task for accounts that have chat" do
          @task_attrs = JetpackTaskList::TaskAttributes.new(@admin)

          @task_attrs.attributes_for_key('add_zopim_chat_v2')
        end

        it "does not have an add_chat task" do
          task_attrs = JetpackTaskList::TaskAttributes.new(@admin)

          assert_raises JetpackTaskList::TaskAttributes::UnknownTaskTypeError do
            task_attrs.attributes_for_key('add_chat')
          end
        end

        it "has an add_zopim_chat task" do
          task_attrs = JetpackTaskList::TaskAttributes.new(@admin)
          task_attrs.attributes_for_key('add_zopim_chat_v2')
        end

        describe "#configure_web_portal" do
          it "has the add_help_center task for accounts that has help_center_onboarding_state enabled" do
            @admin.account.stubs(help_center_onboarding_state: :enabled)
            task_attrs = JetpackTaskList::TaskAttributes.new(@admin)
            task_attrs.attributes_for_key('add_help_center')
          end

          describe "when help_center_onboarding_state is disabled" do
            before do
              @admin.account.stubs(help_center_onboarding_state: :disabled)
            end

            describe "and the help_center_state is disabled" do
              it "does not have the add_help_center task for accounts" do
                @admin.account.stubs(help_center_state: :disabled)
                task_attrs = JetpackTaskList::TaskAttributes.new(@admin)

                assert_raises JetpackTaskList::TaskAttributes::UnknownTaskTypeError do
                  task_attrs.attributes_for_key('add_help_center')
                end
              end
            end

            describe "and the help_center_state is not disabled" do
              it "has the add_help_center task for accounts" do
                @admin.account.stubs(help_center_state: :restricted)
                task_attrs = JetpackTaskList::TaskAttributes.new(@admin)
                task_attrs.attributes_for_key('add_help_center')
              end
            end
          end
        end
      end

      describe "without chat" do
        before do
          @admin.account.subscription.stubs(has_chat?: false)
        end

        it "does not have the add_chat task for accounts that do not have chat" do
          task_attrs = JetpackTaskList::TaskAttributes.new(@admin)

          assert_raises JetpackTaskList::TaskAttributes::UnknownTaskTypeError do
            task_attrs.attributes_for_key('add_chat')
          end
        end
      end

      describe "with zendesk_widget" do
        it "has embeddable widget task" do
          task_attrs = JetpackTaskList::TaskAttributes.new(@admin)
          task_attrs.attributes_for_key('add_embeddable_widget')
        end

        it "does not have feedback tab" do
          task_attrs = JetpackTaskList::TaskAttributes.new(@admin)
          assert_raises JetpackTaskList::TaskAttributes::UnknownTaskTypeError do
            task_attrs.attributes_for_key('add_feedback_tab')
          end
        end

        it "orders configure_email_v2 as the first item after register if features is set" do
          tasks = JetpackTaskList::TaskAttributes.new(@admin).all
          first_task = tasks[0]
          second_task = tasks[1]

          assert_equal "register", first_task[:key]
          assert_equal "configure_email_v2", second_task[:key]
        end
      end

      describe "with zero states experiment removed" do
        it "has test ticket task" do
          task_attrs = JetpackTaskList::TaskAttributes.new(@admin)
          task_attrs.attributes_for_key('test_ticket')
        end
      end

      describe "with trial_extras features" do
        before do
          @admin.account.stubs(has_voice_enabled?: true)
          @admin.account.subscription.stubs(has_chat?: true)
        end

        it "prioritizes listed tasks" do
          @admin.account.trial_extras.create(
            key: 'features',
            value: 'help_center,embeddables'
          )
          tasks = JetpackTaskList::TaskAttributes.new(@admin).all

          assert_equal 'register', tasks[0][:key]
          assert_equal 'add_help_center', tasks[1][:key]
          assert_equal 'add_embeddable_widget', tasks[2][:key]
        end

        it "excludes chat if unavailable" do
          @admin.account.subscription.stubs(has_chat?: false)
          @admin.account.trial_extras.create(
            key: 'features',
            value: 'chat,help_center'
          )
          tasks = JetpackTaskList::TaskAttributes.new(@admin).all

          assert_equal 'register', tasks[0][:key]
          assert_equal 'add_help_center', tasks[1][:key]
        end

        it "includes chat only if available" do
          @admin.account.trial_extras.create(
            key: 'features',
            value: 'chat,help_center'
          )
          tasks = JetpackTaskList::TaskAttributes.new(@admin).all

          assert_equal 'register', tasks[0][:key]
          assert_equal 'add_zopim_chat_v2', tasks[1][:key]
          assert_equal 'add_help_center', tasks[2][:key]
        end

        it "does not promote voice if disabled" do
          @admin.account.stubs(has_voice_enabled?: false)
          @admin.account.trial_extras.create(
            key: 'features',
            value: 'voice,help_center'
          )
          tasks = JetpackTaskList::TaskAttributes.new(@admin).all

          assert_equal 'register', tasks[0][:key]
          assert_equal 'add_help_center', tasks[1][:key]
        end

        it "promotes voice if enabled" do
          @admin.account.trial_extras.create(
            key: 'features',
            value: 'voice,help_center'
          )
          tasks = JetpackTaskList::TaskAttributes.new(@admin).all

          assert_equal 'register', tasks[0][:key]
          assert_equal 'add_voice', tasks[1][:key]
          assert_equal 'add_help_center', tasks[2][:key]
        end
      end
    end
  end
end
