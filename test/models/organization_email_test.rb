require_relative '../support/test_helper'

SingleCov.covered!

describe OrganizationEmail do
  fixtures :organization_emails, :organizations

  let(:organization_email) { organization_emails(:organization_email1) }
  let(:account) { organization_email.account }

  should_validate_presence_of :account
  should_validate_presence_of :organization
  should_validate_presence_of :value

  describe '.valid_values' do
    describe 'when there are valid email values' do
      it 'returns the valid email values' do
        values = OrganizationEmail.valid_values(['user@one.com', 'user@two.net'])
        assert_equal ['user@one.com', 'user@two.net'], values
      end
    end

    describe 'when there are invalid email values' do
      it 'does not return the invalid email values' do
        values = OrganizationEmail.valid_values(['one', 'two.net'])
        assert_equal [], values
      end
    end

    describe 'when there are both valid and invalid email values' do
      it 'only returns the valid values' do
        values = OrganizationEmail.valid_values(['user@one.com', 'user@two.net', 'one', 'two.net'])
        assert_equal ['user@one.com', 'user@two.net'], values
      end
    end

    describe 'when passed an empty array' do
      it 'returns an empty array' do
        values = OrganizationEmail.valid_values([])
        assert_equal [], values
      end
    end
  end

  describe '#validate_value' do
    let(:organization) { organizations(:minimum_organization1) }
    before { organization_email.save }

    describe 'for invalid email addresses' do
      let(:organization_email) do
        OrganizationEmail.new(organization: organization, value: 'something')
      end

      it 'adds a validation error' do
        refute_valid organization_email
        assert_includes organization_email.errors, :email
        assert_includes organization_email.errors[:email], "'something' is not a properly formatted email address"
      end
    end

    describe 'for valid email addresses' do
      let(:organization_email) do
        OrganizationEmail.new(organization: organization, value: 'mzaizar@zendesk.com')
      end

      it 'does not add any error' do
        assert_valid organization_email
        refute_includes organization_email.errors, :email
      end
    end
  end

  describe '#email' do
    it 'is an alias of #value' do
      organization_email.expects(:value)
      organization_email.email
    end
  end

  describe 'when given a value with uppercase characters' do
    let(:organization) { organizations(:minimum_organization1) }
    let(:organization_email) do
      OrganizationEmail.create!(organization: organization, value: 'UsEr@ZeNdEsK.cOm')
    end

    it 'downcases the value before validating and saving' do
      assert_equal 'user@zendesk.com', organization_email.value
    end
  end
end
