require_relative "../../support/test_helper"
require_relative "../../support/voice_test_helper"

SingleCov.covered! uncovered: 9

describe 'SubscriptionVoiceIntegration' do
  fixtures :accounts, :subscriptions

  describe "for a subscription with a voice sub account" do
    before do
      @account = accounts(:minimum)
      @account.settings.set(voice: true)
      @account.stubs(:voice_sub_account_active?).returns(true)
      @subscription = @account.subscription
    end

    describe "that enters a state that requires twilio cleanup" do
      before { @subscription.stubs(:should_be_suspended?).returns(true) }

      it "suspends the sub-account on Voice" do
        request = stub_request(:put, %r{api/v2/channels/voice/internal/sub_accounts/suspend})
        @subscription.send :suspend_sub_account
        assert_requested(request)
      end
    end

    describe "that enters a state that does not require twilio cleanup" do
      before { @subscription.stubs(:should_be_suspended?).returns(false) }

      it "does not suspend the sub-account on Voice" do
        request = stub_request(:put, %r{api/v2/channels/voice/internal/sub_accounts/suspend})
        @subscription.send :suspend_sub_account
        assert_not_requested(request)
      end
    end

    describe "#voice_should_do_twilio_cleanup?" do
      it "returns true for an expired trial subscription" do
        @subscription.stubs(:expired_trial?).returns(true)
        assert(@subscription.send(:should_be_suspended?))
      end

      it "returns true for a paying sub that is not active" do
        @subscription.stubs(:paying_subscription_no_longer_active?).returns(true)
        assert(@subscription.send(:should_be_suspended?))
      end
    end

    describe "#paying_subscription_no_longer_active?" do
      it "returns true for an inactive customer subscription" do
        @subscription.stubs(:account_type).returns('customer')
        @subscription.stubs(:account_status).returns('something that isnt active')
        assert(@subscription.send(:paying_subscription_no_longer_active?))
      end

      it "returns true for an inactive sponsored subscription" do
        @subscription.stubs(:account_type).returns('sponsored')
        @subscription.stubs(:account_status).returns('something that isnt active')
        assert(@subscription.send(:paying_subscription_no_longer_active?))
      end

      it "returns false for an inactive subscription that is neither customer nor sponsored" do
        @subscription.stubs(:account_type).returns('something that isnt customer or sponsored')
        @subscription.stubs(:account_status).returns('something that isnt active')
        assert_equal(false, @subscription.send(:paying_subscription_no_longer_active?))
      end

      it "returns false for an active subscription that is customer or sponsored" do
        @subscription.stubs(:account_status).returns('active')

        @subscription.stubs(:account_type).returns('customer')
        assert_equal(false, @subscription.send(:paying_subscription_no_longer_active?))

        @subscription.stubs(:account_type).returns('sponsored')
        assert_equal(false, @subscription.send(:paying_subscription_no_longer_active?))
      end
    end
  end
end
