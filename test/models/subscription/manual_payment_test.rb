require_relative "../../support/test_helper"

SingleCov.not_covered!

describe 'ManualPayment' do
  fixtures :accounts, :subscriptions, :payments
  before do
  end

  describe "A subscription in manual payment mode" do
    before do
      @subscription = subscriptions(:minimum)
      @subscription.payment_method_type = PaymentMethodType.MANUAL
      @subscription.manual_discount = 100
      assert @subscription.save!
    end

    describe "without a paid payment ending in the future" do
      before { Timecop.travel @subscription.payments.pending.last.period_begin_at + 1.day }

      it "is returned by Subscription#without_payments_after" do
        assert_equal 1, Subscription.without_payments_after(Time.now).length
        assert_equal @subscription, Subscription.without_payments_after(Time.now).first
      end
    end

    describe "with a paid payment ending in the future" do
      before { Timecop.travel @subscription.payments.pending.last.period_begin_at - 1.day }
      it "does not be returned by Subscription#without_payments_after" do
        assert_equal 0, Subscription.without_payments_after(Time.now).length
      end
    end
  end
end
