require_relative "../../support/test_helper"

SingleCov.not_covered!

describe 'AgentDowngrade' do
  fixtures :accounts, :subscriptions, :credit_cards, :users, :payments,
    :addresses, :user_identities, :role_settings

  before do
    @account = accounts(:minimum)
    Account.any_instance.stubs(:has_light_agents?).returns(true)
    @permission_set = PermissionSet.create_light_agent!(@account)
  end

  describe "with three admins and one light agent" do
    before do
      @light_agent = users(:minimum_agent)
      @light_agent.assigned.stubs(:not_closed).returns(Ticket.where('id != id'))
      @light_agent.update_attributes! name: "Light Agent", permission_set: @permission_set

      assert_equal 4, @account.agents.count(:all)
      assert_equal 3, @account.billable_agents.count
      assert_equal 1, @account.light_agents.count(:all)

      refute @account.billable_agents.include?(@light_agent)
    end

    describe "converting the light agent to an admin" do
      describe "when the additional agent is not paid for" do
        before do
          @account.subscription.update_attributes! base_agents: 3
          @light_agent.reload
        end

        it "does not be allowed" do
          assert_raise(ActiveRecord::RecordInvalid) do
            @light_agent.roles = Role::ADMIN.id
            @light_agent.save!
          end
        end
      end

      describe "when the additional agent is paid for" do
        before do
          @account.subscription.update_attributes! base_agents: 4
          @light_agent.reload
          CIA.stubs(:current_actor).returns(@light_agent)
        end
        it "is allowed" do
          @light_agent.roles = Role::ADMIN.id
          @light_agent.save!
        end
      end
    end
  end

  describe "with one admin and 3 light agents" do
    before do
      @account.agents.each_with_index do |agent, idx|
        agent.assigned.stubs(:not_closed).returns(Ticket.where('id != id'))
        unless agent.is_account_owner?
          agent.roles = Role::AGENT.id
          agent.update_attributes! name: "Light Agent #{idx}", permission_set: @permission_set
        end
      end
      @account.reload

      assert_equal 3, @account.light_agents.count(:all)
      assert_equal 1, @account.admins.count(:all)
      assert_equal 4, @account.agents.count(:all)
      assert_equal 1, @account.billable_agents.count
    end

    describe "Lowering the number of max agents to 3 (1 less than the number of existing agents)" do
      before do
        @account.subscription.update_attributes! base_agents: 3
        @account.reload
      end

      it "does not downgrade billable agents" do
        assert_equal 1, @account.billable_agents.count
      end

      it "does not modify light agents" do
        assert_equal 3, @account.light_agents.count(:all)
      end

      it "does not change the number of agents" do
        assert_equal 4, @account.agents.count(:all)
      end
    end
  end

  describe "Lowering the number of max agents to 4" do
    before do
      @light_agent = users(:minimum_agent)
      @light_agent.assigned.stubs(:not_closed).returns(Ticket.where('id != id'))
      @light_agent.update_attributes! name: "Light Agent", permission_set: @permission_set
      @account.subscription.update_attributes! base_agents: 4
      @account.reload
    end

    it "does not downgrade billable agents" do
      assert_equal 3, @account.billable_agents.count
    end

    it "leaves light agents alone" do
      assert_equal [@light_agent].map(&:id), (@account.agents - @account.billable_agents).map(&:id)
    end
  end

  describe "Lowering the number of max agents to 1" do
    before do
      @light_agent = users(:minimum_agent)
      @light_agent.assigned.stubs(:not_closed).returns(Ticket.where('id != id'))
      @light_agent.update_attributes! name: "Light Agent", permission_set: @permission_set
      @account.subscription.update_attributes! base_agents: 1
      @account.reload
    end

    it "converts billable agents into endusers" do
      assert_equal 1, @account.billable_agents.count
    end

    it "leaves light agents alone" do
      assert_equal [@account.owner, @light_agent].map(&:id),  @account.agents.map(&:id)
      assert_equal [@light_agent].map(&:id), (@account.agents - @account.billable_agents).map(&:id)
    end

    describe "then adding agents" do
      before { @account.subscription.base_agents = 5 }
      it "works" do
        @account.subscription.save!
      end
    end
  end

  describe "Losing the light_agents feature during a plan change" do
    before do
      @account.subscription.update_attributes! plan_type: SubscriptionPlanType.ExtraLarge
      Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
      @light_agent = users(:minimum_agent)
      @light_agent.assigned.stubs(:not_closed).returns(Ticket.where('id != id'))
      @light_agent.update_attributes! name: "Light Agent", permission_set: @permission_set
    end

    it "converts light agents to end users" do
      assert_equal 4, @account.agents.count(:all)
      assert_equal 1, @account.light_agents.count(:all)
      @account.subscription.update_attributes! plan_type: SubscriptionPlanType.Large
      Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
      @account.reload
      assert_equal 3, @account.agents.count(:all)
      assert_equal 0, @account.light_agents.count(:all)
      assert @account.end_users.include?(users(:minimum_agent))
      assert_nil @light_agent.reload.permission_set
    end
  end
end
