require_relative "../../support/test_helper"

SingleCov.not_covered!

describe 'ZuoraSupport' do
  fixtures :all

  describe "a subscription instance with a Subscription::ZuoraSupport mixin" do
    before do
      @account      = accounts(:minimum)
      @subscription = @account.subscription
      assert @subscription.valid?
    end

    it "has a zuora_managed? helper method" do
      assert @subscription.respond_to?(:zuora_managed?)
    end

    describe "when the payment_method_type is not set to zuora" do
      before do
        assert PaymentMethodType.ZUORA != @subscription.payment_method_type
      end

      it "has zuora_managed? evaluate to false" do
        refute @subscription.zuora_managed?
      end

      it "does not require a zuora subscription" do
        refute @subscription.send(:requires_zuora_subscription?)
      end

      describe "and there is no zuora subscription record associated" do
        before do
          @subscription.payment_method_type = PaymentMethodType.MANUAL
          @subscription.zuora_subscription  = nil
        end

        it "does not trigger validation error on update" do
          @subscription.save!
        end
      end
    end

    describe "when payment_method_type is set to zuora" do
      before do
        @subscription.payment_method_type = PaymentMethodType.ZUORA
      end

      it "has zuora_managed? evaluate to true" do
        assert @subscription.zuora_managed?
      end

      it "requires a zuora subscription" do
        assert @subscription.send(:requires_zuora_subscription?)
      end

      describe "and there is no zuora subscription record associated" do
        before do
          @subscription.zuora_subscription = nil
        end

        it "fails validation" do
          refute @subscription.valid?
        end

        it "triggers validation error on update" do
          assert_raise ActiveRecord::RecordInvalid do
            @subscription.save!
          end
        end
      end

      describe "and a zuora subscription record is associated" do
        before do
          @subscription.build_zuora_subscription(
            pricing_model_revision: @subscription.pricing_model_revision
          )
        end

        it "pass validation" do
          assert @subscription.valid?
        end

        it "does not trigger validation error on update" do
          @subscription.save!
        end
      end
    end
  end
end
