require_relative "../../support/test_helper"
require_relative "../../support/billing_test_helper"

SingleCov.not_covered!

describe 'SubscriptionInvoicing' do
  fixtures :accounts, :subscriptions, :payments, :account_property_sets,
    :users, :user_identities, :groups, :sequences

  include BillingTestHelper

  describe "A Zuora subscription" do
    before do
      @account         = paying_account
      @subscription    = @account.subscription
      @payment         = @subscription.payments.pending.first
      assert_equal PaymentMethodType.ZUORA, @subscription.payment_method_type
    end

    it "is able to be switched into invoicing mode" do
      @subscription.update_attributes! payment_method_type: PaymentMethodType.MANUAL
      assert @subscription.reload.invoicing?
    end
  end
end
