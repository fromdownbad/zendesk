require_relative "../../support/test_helper"
require_relative "../../support/billing_test_helper"

SingleCov.not_covered!

describe 'Subscription::Causeware' do
  include BillingTestHelper

  fixtures :accounts, :subscriptions, :users, :account_property_sets,
    :payments, :sequences, :groups, :user_identities, :remote_authentications, :credit_cards

  describe "#configure_for_ucsf_causeware!" do
    before do
      @subscription = subscriptions(:minimum)
      @subscription.configure_for_ucsf_causeware!
    end

    it "sets the plan type to small" do
      assert_equal SubscriptionPlanType.Small, @subscription.plan_type
    end

    it "sets the billing cycle to annual" do
      assert_equal BillingCycleType.Annually, @subscription.billing_cycle_type
    end

    it "sets the max agent count to three" do
      assert_equal 3, @subscription.max_agents
    end

    it "should evaluate as a UCSF Causeware" do
      assert @subscription.ucsf_causeware?
    end
  end

  describe "configure_for_ucsf_causeware?" do
    before { @subscription = subscriptions(:minimum) }

    [ZBC::Zendesk::PricingModelRevision::INITIAL, ZBC::Zendesk::PricingModelRevision::APRIL_2010, ZBC::Zendesk::PricingModelRevision::NEW_FORUMS].each do |pricing_model|
      describe "on the #{pricing_model} pricing model" do
        before { @subscription.update_attribute(:pricing_model_revision, pricing_model) }

        describe "on starter accounts" do
          before do
            @subscription.plan_type = SubscriptionPlanType.Small
          end
          it "returns true" do
            assert @subscription.configure_for_ucsf_causeware?
          end
        end

        describe "on regular, plus, and enterprise" do
          it "returns false" do
            @subscription.plan_type = SubscriptionPlanType.Medium
            assert_equal false, @subscription.configure_for_ucsf_causeware?
            @subscription.plan_type = SubscriptionPlanType.Large
            assert_equal false, @subscription.configure_for_ucsf_causeware?
            @subscription.plan_type = SubscriptionPlanType.ExtraLarge
            assert_equal false, @subscription.configure_for_ucsf_causeware?
          end
        end
      end
    end

    describe "on the UcsfPricingModel pricing model" do
      before { @subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::UCSF_CAUSEWARE) }

      # NOTE trials can no longer be on UCSF Pricing Model

      describe "on starter, regular, plus, and enterprise" do
        before { @subscription.stubs(:is_trial?).returns(false) }
        it "returns false" do
          @subscription.plan_type = SubscriptionPlanType.Small
          assert_equal false, @subscription.configure_for_ucsf_causeware?
          @subscription.plan_type = SubscriptionPlanType.Medium
          assert_equal false, @subscription.configure_for_ucsf_causeware?
          @subscription.plan_type = SubscriptionPlanType.Large
          assert_equal false, @subscription.configure_for_ucsf_causeware?
          @subscription.plan_type = SubscriptionPlanType.ExtraLarge
          assert_equal false, @subscription.configure_for_ucsf_causeware?
        end
      end
    end
  end
end
