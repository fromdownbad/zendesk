require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 12

describe Subscription::DelegatedFeatures do
  fixtures :subscriptions

  it 'has a class method that returns priceable features' do
    Subscription::DelegatedFeatures.delegated_feature_bits.must_be_instance_of Array
    Subscription::DelegatedFeatures.delegated_feature_bits.size.must_be :>, 0
  end

  it 'has a class method that returns feature groups' do
    Subscription::DelegatedFeatures.delegated_feature_groups.must_be_instance_of Array
  end

  describe '.define_feature_accessor' do
    subject { subscriptions(:minimum) }

    it 'defines .has_{feature}? instance method' do
      subject.wont_respond_to :has_dummy_feature?
      Subscription::DelegatedFeatures.define_feature_accessor :dummy_feature
      subject.must_respond_to :has_dummy_feature?
    end
  end

  describe '.define_group_accessor' do
    subject { subscriptions(:minimum) }

    it 'defines .has_{group}? instance method' do
      subject.wont_respond_to :has_dummy?
      Subscription::DelegatedFeatures.define_group_accessor :dummy
      subject.must_respond_to :has_dummy?
    end

    it 'will not redefine an existing group method' do
      subject.wont_respond_to :has_existing_dummy?
      Subscription::DelegatedFeatures.define_group_accessor :existing_dummy
      subject.must_respond_to :has_existing_dummy?
      assert_raises(RuntimeError, "Method '#has_existing_dummy?' is already defined") { Subscription::DelegatedFeatures.define_group_accessor :existing_dummy }
    end

    it 'will not redefine an existing feature method' do
      Subscription::DelegatedFeatures.define_feature_accessor :existing_dummy_feature
      subject.must_respond_to :has_existing_dummy_feature?
      assert_raises(RuntimeError, "Method '#has_existing_dummy_feature?' is already defined") { Subscription::DelegatedFeatures.define_group_accessor :existing_dummy_feature }
    end
  end

  describe 'for a system account' do
    subject { subscriptions(:systemsubscription) }
    let(:pricing_model) { stub }
    let(:features) { { xml_export: false } }

    it 'responds to the delegated methods' do
      Subscription::DelegatedFeatures.delegated_feature_bits.each do |feature|
        subject.must_respond_to "has_#{feature}?"
      end

      Subscription::DelegatedFeatures.delegated_feature_groups.each do |group|
        subject.must_respond_to "has_#{group}?"
      end
    end

    describe 'for a specific feature' do
      before do
        subject.expects(:features).never
      end

      it 'always returns true' do
        assert subject.has_xml_export?
      end
    end
  end

  describe 'for an account enrolled in a feature beta program' do
    subject { subscriptions(:minimum) }

    let(:arturo_feature) { stub(external_beta_subdomains: [subject.account.subdomain]) }

    before do
      subject.update_column :pricing_model_revision,
        ZBC::Zendesk::PricingModelRevision::PATAGONIA

      subject.update_column :plan_type, SubscriptionPlanType.Large

      Arturo::Feature.stubs(:find_feature).returns(arturo_feature)
    end

    it 'bypasses the database' do
      assert subject.has_light_agents?
    end
  end

  describe 'for a given subscription instance' do
    subject { subscriptions(:minimum) }
    let(:pricing_model) { stub }
    let(:features) { { xml_export: true } }

    before do
      Account.any_instance.stubs(:read_feature_bits_from_db).returns(false)
    end

    it 'responds to the delegated methods' do
      Subscription::DelegatedFeatures.delegated_feature_bits.each do |feature|
        subject.must_respond_to "has_#{feature}?"
      end

      Subscription::DelegatedFeatures.delegated_feature_groups.each do |group|
        subject.must_respond_to "has_#{group}?"
      end
    end

    describe 'when read feature bits from cache is disabled for account' do
      let(:features) { stub(sandbox?: true) }

      before do
        subject.stubs(:pricing_model).returns(pricing_model)
        subject.stubs(:features).returns(features)
        Account.any_instance.stubs(:read_feature_bits_from_db).returns(true)
        Rails.cache.expects(:fetch).never
      end

      it 'returns the value from the subscription_features database' do
        assert(subject.has_sandbox?)
      end
    end
  end
end
