require_relative "../../support/test_helper"

SingleCov.not_covered!

describe 'PricingModelUpgrade' do
  describe "A starter account on the UcsfCausewarePricingModel" do
    before do
      @subscription = subscriptions(:minimum)
      @subscription.plan_type = SubscriptionPlanType.Small
      @subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::UCSF_CAUSEWARE)
      Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
      @subscription.reload
    end

    it "does not have host mapping enabled" do
      assert_equal false, @subscription.account.settings.grandfathered_host_mapping?
      assert_equal false, @subscription.has_host_mapping?
    end
  end

  describe "A starter account on the InitialPricingModel" do
    before do
      @subscription = subscriptions(:minimum)
      @subscription.plan_type = SubscriptionPlanType.Small
      @subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::INITIAL)
    end

    it "has host mapping" do
      assert @subscription.has_host_mapping?
    end

    it "does not have grandfathered host mapping" do
      assert_equal false, @subscription.account.settings.grandfathered_host_mapping?
    end

    describe "upgrading to the UcsfCausewarePricingModel" do
      before do
        @subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::UCSF_CAUSEWARE)
        @subscription.reload
      end

      it "preserves the host_mapping feature" do
        assert @subscription.has_host_mapping?
      end
    end
  end
end
