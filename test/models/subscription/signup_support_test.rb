require_relative "../../support/test_helper"
require_relative "../../support/billing_test_helper"

SingleCov.covered!

describe 'SubscriptionSignupSupport' do
  describe 'A trial subscription' do
    before do
      @subscription = Subscription.new(is_trial: true)
    end

    it '#signup_as_trial?s' do
      assert @subscription.signup_as_trial?
    end
  end

  describe 'A subscription not on trial' do
    before do
      @subscription = Subscription.new(is_trial: false)
    end

    describe 'on invoicing' do
      before do
        @subscription.payment_method_type = PaymentMethodType.MANUAL
      end

      it 'NOTs #signup_as_trial?' do
        refute @subscription.signup_as_trial?
      end
    end

    describe 'on Zuora' do
      before do
        @subscription.payment_method_type = PaymentMethodType.ZUORA
      end

      it 'NOTs #signup_as_trial?' do
        refute @subscription.signup_as_trial?
      end
    end

    describe 'on credit card' do
      before do
        @subscription.payment_method_type = PaymentMethodType.CREDIT_CARD
      end

      describe 'with a credit card' do
        before do
          cc = mock
          cc.expects(:registered?).returns(true)
          @subscription.expects(:credit_card).returns(cc)
        end

        it 'NOTs #signup_as_trial?' do
          refute @subscription.signup_as_trial?
        end
      end

      describe 'without a credit card' do
        it '#signup_as_trial?s' do
          assert @subscription.signup_as_trial?
        end

        describe 'but sponsored' do
          before do
            @subscription.expects(:is_sponsored?).returns(true)
          end

          it 'NOTs #signup_as_trial?' do
            refute @subscription.signup_as_trial?
          end
        end
      end
    end
  end
end
