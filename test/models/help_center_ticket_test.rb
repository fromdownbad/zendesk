require_relative "../support/test_helper"

SingleCov.covered!

describe 'HelpCenterTicket' do
  before do
    metadata = { 'custom' => { 'post_id' => '42', 'post_name' => 'CPH rocks!', 'post_path' => '/foo' } }
    @audit = mock('event', metadata: metadata)
    @ticket = mock('ticket', initial_audit: @audit)

    @help_center_ticket = HelpCenterTicket.new(@ticket)
  end

  it "retrieves the post_id from the metadata of the ticket" do
    assert_equal '42', @help_center_ticket.post_id
  end

  it "retrieves the post_name from the metadata of the ticket" do
    assert_equal 'CPH rocks!', @help_center_ticket.post_name
  end

  it "provides a url to the original post" do
    brand = mock('brand', url: 'https://acme.zendesk.com')
    @ticket.stubs(brand: brand)

    assert_equal 'https://acme.zendesk.com/foo', @help_center_ticket.post_url
  end
end
