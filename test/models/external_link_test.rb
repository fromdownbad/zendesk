require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe ExternalLink do
  fixtures :accounts, :tickets

  describe ".build" do
    it "makes a new object with its arguments and call build on it" do
      ticket = Ticket.new
      params = {foo: "bar"}

      ExternalLink.expects(:new).with(ticket, params).returns(mock(build: true))

      ExternalLink.build(ticket, params)
    end
  end

  describe "Building an ExternalLink" do
    before do
      @params = {type: "JiraIssue", issue_id: "foo-43"}
      @account = accounts(:minimum)
      @ticket = Ticket.new(account: @account)
      @filtered_params = { account: @account, issue_id: "foo-43" }
      @external_link = ExternalLink.new(@ticket, @params)
    end

    it "does not raise when its params are an empty hash" do
      ExternalLink.new(@ticket, {}).build
    end

    it "returns false when its params do not include a type" do
      refute ExternalLink.new(@ticket, anything: :but, the: :necessary).build
    end

    it "returns false when its params include a bogus type" do
      refute ExternalLink.new(@ticket, type: "BOGUS#{Time.now.to_i}").build
    end

    it "returns false when its params include an invalid type" do
      refute ExternalLink.new(@ticket, type: "Account").build
    end

    it "calls find_by_params on the class specified by the type argument in its params" do
      @ticket.jira_issues.expects(:find_from_params).with(@filtered_params).never
      JiraIssue.expects(:find_from_params).with(@filtered_params)

      @external_link.build
    end

    it "builds an external link object on its ticket when one does not exist" do
      @ticket.jira_issues.stubs(:find_from_params).returns(nil)
      JiraIssue.expects(:new).with(@filtered_params)
      @ticket.linkings.expects(:build)

      @external_link.build
    end

    it "builds linkings and external links that contain an account when building an external link" do
      @ticket.jira_issues.stubs(:find_from_params).returns(nil)
      @external_link.build

      assert_equal @ticket.linkings.last.account, @account
    end

    it "builds linkings and external links that contain an account when the external link exists" do
      jira_issue = JiraIssue.new
      JiraIssue.stubs(:find_from_params).returns(jira_issue)

      @external_link.build

      assert_equal @ticket.linkings.last.account, @account
    end

    it "builds a linking object on its ticket when an external link already exists" do
      jira_issue = JiraIssue.new
      JiraIssue.stubs(:find_from_params).returns(jira_issue)

      @ticket.linkings.expects(:build).with(has_entry(external_link: jira_issue))

      @external_link.build
    end

    it "invokes the ticket_callback method on its ticket when an external link already exists" do
      jira_issue = JiraIssue.new
      JiraIssue.stubs(:find_from_params).returns(jira_issue)
      JiraIssue.any_instance.expects(:ticket_callback).with(@ticket)
      @external_link.build
    end

    it "does not build an external link object on its ticket when one already exists" do
      jira_issue = JiraIssue.new
      JiraIssue.stubs(:find_from_params).returns(jira_issue)

      @ticket.jira_issues.expects(:build).never

      @external_link.build
    end
  end

  describe ".create!" do
    it "makes a new object with its arguments and call create! on it" do
      ticket = Ticket.new
      params = {foo: "bar"}

      ExternalLink.expects(:new).with(ticket, params).returns(mock(create!: true))

      ExternalLink.create!(ticket, params)
    end
  end

  describe "Creating an ExternalLink" do
    before do
      @params = {type: "JiraIssue", issue_id: "foo-43"}
      @account = accounts(:minimum)
      @ticket = tickets(:minimum_1)
      @filtered_params = { account: @account, issue_id: "foo-43" }
      @external_link = ExternalLink.new(@ticket, @params)
    end

    it "raises a NameError when its params do not contain a valid :type" do
      assert_raises(NameError) do
        ExternalLink.new(@ticket, {}).create!
      end
    end

    it "raises a NameError when its params contain a forbidden :type" do
      assert_raises(NameError) { ExternalLink.new(@ticket, type: "Account").create! }
    end

    it "calls find_by_params on the class specified by the type argument in its params" do
      @ticket.jira_issues.expects(:find_from_params).with(@filtered_params).never
      JiraIssue.expects(:find_from_params).with(@filtered_params)

      @external_link.create!
    end

    it "create!s an external link object on its ticket when one does not exist" do
      @ticket.jira_issues.stubs(:find_from_params).returns(nil)
      JiraIssue.expects(:create!).with(@filtered_params)
      @ticket.linkings.expects(:create!)

      @external_link.create!
    end

    it "creates linkings that contain an account_id" do
      @external_link.create!

      assert_equal @ticket.linkings.last.account, @account
    end

    it "create!s a linking object on its ticket when an external link already exists" do
      jira_issue = JiraIssue.new
      JiraIssue.stubs(:find_from_params).returns(jira_issue)
      @ticket.linkings.expects(:create!).with(has_entry(external_link: jira_issue))

      @external_link.create!
    end

    it "does not create! an external link object on its ticket when one already exists" do
      jira_issue = JiraIssue.new
      JiraIssue.stubs(:find_from_params).returns(jira_issue)

      @ticket.jira_issues.expects(:create!).never

      @external_link.create!
    end
  end

  describe ".destroy!" do
    it "makes a new object with its arguments and call destroy! on it" do
      ticket = Ticket.new
      params = {foo: "bar"}

      ExternalLink.expects(:new).with(ticket, params).returns(mock(destroy!: true))

      ExternalLink.destroy!(ticket, params)
    end
  end

  describe "Destroying an ExternalLink" do
    it "raises a NameError when its params do not contain a valid :type" do
      assert_raises(NameError) { ExternalLink.new(@ticket, {}).destroy! }
    end

    it "raises a NameError when its params contain a forbidden :type" do
      assert_raises(NameError) { ExternalLink.new(@ticket, type: "Account").destroy! }
    end

    describe "with valid params" do
      before_should "find the linking based on the ticket and specified params" do
        @mock_issue = JiraIssue.new
        @mock_issue.expects(:linkings).returns(mock(find_by_ticket_id!: Linking.new))
      end

      before_should "call destroy on the found linking" do
        @mock_issue = JiraIssue.new
        @mock_issue.linkings.stubs(:find_by_ticket_id!).returns(mock(destroy: true))
      end

      before do
        params = {type: "JiraIssue", issue_id: Time.now.to_i.to_s}
        JiraIssue.expects(:find_from_params).with(params).returns(@mock_issue)

        ExternalLink.new(Ticket.new, params).destroy!
      end
    end
  end
end
