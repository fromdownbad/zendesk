require_relative "../support/test_helper"

SingleCov.covered!

describe 'TicketForm' do
  fixtures :accounts, :account_settings, :ticket_fields, :custom_field_options

  describe "#TicketForm" do
    before do
      @account = accounts(:minimum)
      setup_ticket_forms
    end

    describe "Creating a new ticket form" do
      it "throws validation error if name is not set" do
        assert_raise(ActiveRecord::RecordInvalid) do
          params = {
            ticket_form: {
              name: "",
              display_name: "Inactive wombat",
              position: 4,
              default: false,
              active: false,
              end_user_visible: false
            }
          }
          form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
          form.save!
        end
      end
      it "throws validation error if display name is not set and form is end user visible" do
        assert_raise(ActiveRecord::RecordInvalid) do
          params = {
            ticket_form: {
              name: "Wombat",
              display_name: "",
              position: 4,
              default: false,
              active: false,
              end_user_visible: true
            }
          }
          form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
          form.save!
        end
      end
      it "throws validation error if new form would exceed ticket limit" do
        number_of_forms = @account.ticket_forms.count

        @account.settings.ticket_forms_limit = number_of_forms + 1
        @account.save!

        params = {
          ticket_form: {
            name: "3rd Wombat",
            display_name: "3rd wombat",
            default: false,
            position: 3,
            active: false,
            end_user_visible: false
          }
        }
        form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        form.save!
        # no error yet

        params = {
          ticket_form: {
            name: "4th Wombat",
            display_name: "4th wombat",
            default: false,
            position: 4,
            active: false,
            end_user_visible: false
          }
        }
        form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        # validation error
        refute form.valid?
      end
      it "NOTs throw validation error if at ticket forms account limit and you just want to update an active ticket form" do
        number_of_forms = @account.ticket_forms.count

        @account.settings.ticket_forms_limit = number_of_forms
        @account.save!

        params = {
          ticket_form: {
            name: "Limited Wombat",
            display_name: "Limited wombat",
            default: false,
            position: 3,
            active: false,
            end_user_visible: false
          }
        }
        form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        # Validation error that we are at limit
        refute form.valid?

        old_form = @account.ticket_forms.last
        old_form.update_attribute(:display_name, "Edited wombat")
        old_form.reload
        assert_equal(old_form.display_name, "Edited wombat")
      end
      it "NOTs throw validation error if at ticket forms account limit and you just want to update an inactive ticket form" do
        number_of_forms = @account.ticket_forms.count

        @account.settings.ticket_forms_limit = number_of_forms + 1
        @account.save!

        params = {
          ticket_form: {
            name: "inactive Wombat",
            display_name: "inactive wombat",
            default: false,
            position: 3,
            active: false,
            end_user_visible: false
          }
        }
        valid_inactive_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        assert valid_inactive_form.save!

        form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        # Validation error that we are at limit
        refute form.valid?

        valid_inactive_form.update_attribute(:display_name, "Edited inactive form")
        assert_equal(valid_inactive_form.display_name, "Edited inactive form")
      end
      it "NOTs throw validation error if at ticket forms account limit and you want to delete a ticket form" do
        number_of_forms = @account.ticket_forms.count

        @account.settings.ticket_forms_limit = number_of_forms
        @account.save!

        params = {
          ticket_form: {
            name: "inactive Wombat",
            display_name: "inactive wombat",
            default: false,
            position: 3,
            active: false,
            end_user_visible: false
          }
        }
        form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        # Validation error we are at limit
        refute form.valid?

        form = @account.ticket_forms.last
        assert form.soft_delete!
      end
      it "NOTs throw validation error if at ticket forms account limit and you want to delete an inactive ticket form" do
        number_of_forms = @account.ticket_forms.count

        @account.settings.ticket_forms_limit = number_of_forms + 1
        @account.save!

        params = {
          ticket_form: {
            name: "inactive Wombat",
            display_name: "inactive wombat",
            default: false,
            position: 3,
            active: false,
            end_user_visible: false
          }
        }
        form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        assert form.save!

        new_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        refute new_form.valid?

        assert form.soft_delete!
      end
      it "NOTs throw validation error if position is not set on a deleted form" do
        @non_active_form.soft_delete!
        assert_not_nil @non_active_form.deleted_at
        @non_active_form.name = "updating a deleted form, cuz why not"
        @non_active_form.save!
      end
      it "NOTs throw validation error if position is already taken by a deleted ticket form" do
        @non_active_form.soft_delete!
        assert_not_nil @non_active_form.deleted_at
        params = {
          ticket_form: {
            name: "2nd Wombat",
            display_name: "Inactive wombat",
            position: 2,
            default: false,
            active: false,
            end_user_visible: false
          }
        }
        form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        assert form.save!
      end

      describe 'validate_ticket_field_limits' do
        before do
          @field_tagger_custom = ticket_fields(:field_tagger_custom)
          @field_tagger_custom_2 = FactoryBot.create(:field_tagger, account: @account)

          @params = {
            ticket_form: {
              name: "Penguins",
              display_name: "Penguins are everywhere",
              position: 2,
              default: false,
              active: true,
              end_user_visible: true,
              in_all_brands: true,
              ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id]
            }
          }
        end

        describe_with_arturo_enabled :ticket_field_limits do
          it 'validates for # of ticket_fields' do
            @account.settings.ticket_fields_limit = 1
            @account.save!
            form = Zendesk::TicketForms::Initializer.new(@account, @params).ticket_form
            refute form.valid?
            assert form.errors.full_messages.include?('must have less than 1 ticket fields on a form'), 'error message should contain the correct information'
          end
        end
      end
    end

    describe "Default ticket forms" do
      describe "#validate_default_is_active" do
        it "throws validation error if the ticket form is not already active" do
          assert_raise(ActiveRecord::RecordInvalid) do
            @non_active_form.default = true
            @non_active_form.save!
          end
        end

        it "throws validation error when making a ticket form inactive if the ticket form is the default form" do
          assert_raise(ActiveRecord::RecordInvalid) do
            @default_form.active = false
            @default_form.save!
          end
        end
      end

      describe "#make_default_visible" do
        before do
          @non_default_form.default = true
          @non_default_form.save!
        end

        it "makes the form end_user_visible if the ticket form is not end_user_visible" do
          assert(@non_default_form.end_user_visible)
          assert(@non_default_form.default)
        end

        it "ensures there is a display_name associated with that form" do
          assert @non_default_form.display_name
        end

        describe "attempting to change the existing default form to not be end_user_visible" do
          before do
            @non_default_form.end_user_visible = false
          end

          it "makes the form end_user_visible" do
            @non_default_form.save!
            assert(@non_default_form.end_user_visible)
            assert(@non_default_form.default)
          end

          it "prevents saving a blank display_name" do
            @non_default_form.display_name = ''
            refute @non_default_form.save
            assert_equal ["cannot be blank"], @non_default_form.errors[:display_name]
          end
        end
      end

      describe "#highlander_rules" do
        before do
          @non_default_form.default = true
          @non_default_form.save!
          @default_form.reload
        end

        it "marks all other forms as not default after updating. There can only be one default form" do
          assert_equal false, @default_form.default
          assert_equal false, @non_active_form.default
          assert(@non_default_form.default)
        end
      end

      describe "#validate_one_default_present" do
        it "raises a validation error if the default is set to false and there are no other ticket forms marked as default" do
          assert_raise(ActiveRecord::RecordInvalid) do
            @default_form.default = false
            @default_form.save!
          end
        end
      end
    end

    describe "When deleting a form" do
      describe "#validate_not_default" do
        it "does not delete the form and return false if the ticket form is the default form" do
          refute_difference "TicketForm.count(:all)" do
            refute @default_form.soft_delete
          end
        end
      end

      describe "#reset_position" do
        it "sets position to nil when we soft delete a ticket form" do
          @non_default_form.soft_delete!
          assert_nil @non_default_form.position
        end
      end

      it "soft_deletes ticket_forms" do
        @non_default_form.soft_delete!
        assert_not_nil @non_default_form.deleted_at
      end

      it "deactivates soft_deleted active form" do
        @non_default_form.soft_delete!
        assert_equal false, @non_default_form.active
      end
    end

    describe "When reordering ticket forms" do
      before do
        TicketForm.reorder_forms([@non_default_form.id, @default_form.id, @non_active_form.id])
      end
      it "reorders the forms and set positions into the order they were given" do
        @non_default_form.reload
        @default_form.reload
        @non_active_form.reload
        assert_equal 0, @non_default_form.position
        assert_equal 1, @default_form.position
        assert_equal 2, @non_active_form.position
      end
    end

    describe "When activating a ticket form" do
      before do
        @non_active_form.active = true
        @non_active_form.save!
      end
      it "reorders forms appropriately" do
        expected_order = [
          @default_form,
          @non_default_form,
          @non_active_form
        ].map(&:id)
        assert_equal @account.ticket_forms.map(&:id), expected_order
      end
    end

    describe "When deactivating a ticket form" do
      before do
        @non_default_form.active = false
        @non_default_form.save!
      end
      it "reorders forms appropriately" do
        expected_order = [
          @default_form,
          @non_default_form,
          @non_active_form
        ].map(&:id)
        assert_equal @account.ticket_forms.map(&:id), expected_order
      end
    end

    describe "When not activating or deactivating" do
      before do
        @non_default_form.name = "NEW NAME"
        @non_default_form.save!
      end

      it "does not reorder forms appropriately" do
        expected_order = [
          @default_form,
          @non_active_form,
          @non_default_form
        ].map(&:id)
        assert_equal @account.ticket_forms.map(&:id), expected_order
      end
    end

    describe "#init_default_form" do
      before { @account2 = accounts(:minimum) }

      it "initializes a default ticket form for an account" do
        default_form = TicketForm.init_default_form(@account2)
        assert default_form.save!

        assert_equal "Default Ticket Form", default_form.name
        assert(default_form.default)
        assert_equal @account2.ticket_forms.default.first, default_form
      end
    end

    describe "#brands" do
      before do
        @default_brand = @account.default_brand
        @active_brand = FactoryBot.create(:brand)
        @inactive_brand = FactoryBot.create(:brand, active: false)
        @default_form.ticket_form_brand_restrictions.create! do |tfbr|
          tfbr.account = @account
          tfbr.brand   = @active_brand
        end
        @default_form.ticket_form_brand_restrictions.create! do |tfbr|
          tfbr.account = @account
          tfbr.brand   = @inactive_brand
        end
      end

      it "returns all active brands in account if in_all_brands is true" do
        @default_form.update_attribute(:in_all_brands, true)
        assert_equal [@default_brand, @active_brand], @default_form.brands
      end

      it "returns only active brands associated to the ticket form if in_all_brands is false" do
        @default_form.update_attribute(:in_all_brands, false)
        assert_equal [@active_brand], @default_form.brands
      end
    end

    describe "#ticket_field" do
      it "returns a ticket field object from the id, using the list of ticket_form_fields in memory" do
        ticket_field_id = @default_form.ticket_form_fields.map(&:ticket_field_id).last

        assert_sql_queries(0) do
          assert @default_form.ticket_field(ticket_field_id).present?
        end
      end
    end
  end

  def setup_ticket_forms
    params = {
      ticket_form: {
        name: "default wombat",
        display_name: "default wombat",
        position: 1,
        default: true,
        active: true,
        end_user_visible: true
      }
    }
    @default_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
    @default_form.save!
    params = {
      ticket_form: {
        name: "Wombat Inactive",
        display_name: "Inactive wombat",
        position: 2,
        default: false,
        active: false,
        end_user_visible: false
      }
    }
    @non_active_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
    @non_active_form.save!
    params = {
      ticket_form: {
        name: "non default wombat",
        display_name: nil,
        position: 3,
        default: false,
        active: true,
        end_user_visible: false
      }
    }
    @non_default_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
    @non_default_form.save!
  end
end
