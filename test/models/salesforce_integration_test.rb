require_relative "../support/test_helper"

SingleCov.covered! uncovered: 19

describe 'SalesforceIntegration' do
  fixtures :accounts, :users, :user_identities

  before do
    @salesforce_integration = SalesforceIntegration.new
    @salesforce_integration.account = accounts(:minimum)
    @salesforce_integration.stubs(:is_sandbox?).returns(false)
    @config = Zendesk::Configuration.fetch(:salesforce)
  end

  subject { @salesforce_integration }

  it "uses ssl.salesforce.com instead of na0.salesforce.com if configured" do
    Account.any_instance.stubs(:has_use_salesforce_ssl_pod?).returns(true)
    assert_equal("https://ssl.salesforce.com", @salesforce_integration.send(:salesforce_ui_url, "na0-api.salesforce.com"))
  end

  it "uses emea.salesforce.com instead of emea-api.salesforce.com if configured" do
    Account.any_instance.stubs(:has_use_salesforce_ssl_pod?).returns(true)
    assert_equal("https://emea.salesforce.com", @salesforce_integration.send(:salesforce_ui_url, "emea-api.salesforce.com"))
  end

  it "uses emea.salesforce.com instead of eu0.salesforce.com" do
    assert_equal("https://emea.salesforce.com", @salesforce_integration.send(:salesforce_ui_url, "eu0-api.salesforce.com"))
  end

  it "creates and destroy the target and trigger" do
    SalesforceTarget.destroy_all
    assert_equal 0, SalesforceTarget.count(:all)
    @salesforce_integration.create_target
    assert_equal 1, SalesforceTarget.count(:all)
    target = SalesforceTarget.last
    assert target.is_active?
    refute target.is_locked?
    assert target.is_system_target?
    trigger = Trigger.find_by_title(target.send(:title_for_trigger))
    refute trigger.is_active?

    assert @salesforce_integration.destroy
    assert_equal 0, SalesforceTarget.count(:all)
    refute Trigger.find_by_title(target.send(:title_for_trigger))
  end

  it "does not be configured by default" do
    refute @salesforce_integration.configured?
  end

  describe "encryption" do
    let(:unencrypted_token) { 'unencrypted_token' }
    let(:unencrypted_secret) { 'unencrypted_secret' }
    let(:unencrypted_access_token) { 'unencrypted_access_token' }
    let(:unencrypted_refresh_token) { 'unencrypted_refresh_token' }

    before do
      @salesforce_integration.update_attributes(
        encrypted_access_token: unencrypted_access_token,
        encrypted_refresh_token: unencrypted_refresh_token,
        encrypted_token: unencrypted_token,
        encrypted_secret_value: unencrypted_secret
      )
    end

    it 'encrypts the encrypted_access_token' do
      refute_equal @salesforce_integration.encrypted_access_token, unencrypted_access_token
    end

    it 'encrypts the encrypted_refresh_token' do
      refute_equal @salesforce_integration.encrypted_refresh_token, unencrypted_refresh_token
    end

    it 'encrypts the encrypted_token' do
      refute_equal @salesforce_integration.encrypted_token, unencrypted_token
    end

    it 'encrypts the encrypted_secret_value' do
      refute_equal @salesforce_integration.encrypted_secret_value, unencrypted_secret
    end

    it 'sets the encryption_cipher_name' do
      assert_equal @salesforce_integration.encryption_cipher_name, 'aes-256-gcm'
    end

    it 'sets the encryption_key_name' do
      assert_equal @salesforce_integration.encryption_key_name, 'ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_TEST_KEY'
    end
  end

  describe "decryption" do
    let(:unencrypted_token) { 'unencrypted_token' }
    let(:unencrypted_secret) { 'unencrypted_secret' }
    let(:unencrypted_access_token) { 'unencrypted_access_token' }
    let(:unencrypted_refresh_token) { 'unencrypted_refresh_token' }

    before do
      @salesforce_integration.update_attributes(
        encrypted_access_token: unencrypted_access_token,
        encrypted_refresh_token: unencrypted_refresh_token,
        encrypted_token: unencrypted_token,
        encrypted_secret_value: unencrypted_secret
      )
    end

    it 'decrypted_access_token decrypts the encrypted_access_token value' do
      assert_equal @salesforce_integration.decrypted_access_token, unencrypted_access_token
    end

    it 'decrypted_refresh_token decrypts the encrypted_refresh_token value' do
      assert_equal @salesforce_integration.decrypted_refresh_token, unencrypted_refresh_token
    end

    it 'decrypted_token decrypts the encrypted_token value' do
      assert_equal @salesforce_integration.decrypted_token, unencrypted_token
    end

    it 'decrypted_secret_value decrypts the encrypted_secret_value value' do
      assert_equal @salesforce_integration.decrypted_secret_value, unencrypted_secret
    end

    describe "when the decryption fails when setting unencrypted values" do
      before do
        @unencrypted_salesforce_integration = SalesforceIntegration.new(
          instance_url: @instance_url,
          encrypted_access_token: unencrypted_access_token,
          encrypted_refresh_token: unencrypted_refresh_token,
          encrypted_token: unencrypted_token,
          encrypted_secret_value: unencrypted_secret,
          is_sandbox: false
        )
      end

      it 'decrypted_access_token should return empty value' do
        assert_equal @unencrypted_salesforce_integration.decrypted_access_token, ''
      end

      it 'decrypted_refresh_token should return empty value' do
        assert_equal @unencrypted_salesforce_integration.decrypted_refresh_token, ''
      end

      it 'decrypted_token should return empty value' do
        assert_equal @unencrypted_salesforce_integration.decrypted_token, ''
      end

      it 'decrypted_secret_value_value should return empty value' do
        assert_equal @unencrypted_salesforce_integration.decrypted_secret_value, ''
      end
    end
  end

  describe "Salesforce Requests" do
    describe "when fetching info for user" do
      before do
        @salesforce_integration.stubs(:get_salesforce_session).returns(session_id: "foo", server_url: "http://salesforce.com")
        @response_xml = "<records><record><id>877a76d0</id><url>/id/877a76d0</url><label>Steven Yan (Lead)</label><record_type>Lead</record_type><fields><field><label>Title</label><value>Foo</label></field><field><label>Department</label><value>Bar</label></field></fields></record></records>"
      end

      it "makes SOAP calls" do
        expected = {
          records: [
            {
              fields: [
                {
                  value: "Foo",
                  label: "Title"
                },
                {
                  value: "Bar",
                  label: "Department"
                }
              ],
              label: "Steven Yan (Lead)",
              url: "https://salesforce.com/id/877a76d0",
              id: "877a76d0",
              record_type: "Lead"
            }
          ]
        }

        result = mock
        result.expects(:result).returns(@response_xml)

        driver = mock
        driver.expects(:getUserFields).returns(result)
        driver.stubs(:options).returns({})

        Salesforce::Integration::Driver.expects(:get_driver).returns(driver)

        assert_equal expected, @salesforce_integration.fetch_info_for(users(:minimum_end_user))
      end

      describe "for an account which has the enhanced Salesforce user data lookup" do
        before do
          Account.any_instance.stubs(:has_salesforce_integration_lookup?).returns(true)
          @user = users(:minimum_end_user)
          @empty_expected = {records: []}
        end

        it "returns empty data immediately if the user has no tags and lookup_type=UserTags" do
          SalesforceIntegration.any_instance.stubs(:lookup_type).returns("UserTags")
          @user.stubs(:tags).returns([])
          assert_equal @empty_expected, @salesforce_integration.fetch_info_for(users(:minimum_end_user))
        end

        it "returns empty data immediately if the user has no organization and lookup_type=OrganizationName" do
          SalesforceIntegration.any_instance.stubs(:lookup_type).returns("OrganizationName")
          @user.stubs(:organization).returns(nil)
          assert_equal @empty_expected, @salesforce_integration.fetch_info_for(users(:minimum_end_user))
        end

        it "returns empty data immediately if the user has no email address and lookup_type=EmailAddress" do
          SalesforceIntegration.any_instance.stubs(:lookup_type).returns("EmailAddress")
          @user.stubs(:email).returns(nil)
          assert_equal @empty_expected, @salesforce_integration.fetch_info_for(users(:minimum_end_user))
        end

        it "invokes getUserFieldsByList with an array of user tags if lookup_type=UserTags and user has tags" do
          SalesforceIntegration.any_instance.stubs(:lookup_type).returns("UserTags")
          @user.stubs(:tags).returns(['tag1', 'tag2', 'tag3'])

          result = mock
          result.expects(:result).returns(@response_xml)

          driver = mock
          driver.expects(:getUserFieldsByList).with(['tag1', 'tag2', 'tag3']).returns(result)
          driver.stubs(:options).returns({})

          Salesforce::Integration::Driver.expects(:get_driver).returns(driver)

          @salesforce_integration.fetch_info_for(users(:minimum_end_user))
        end

        it "invokes getUserFieldsByList with organization name if lookup_type=OrganizationName and user has an organization" do
          SalesforceIntegration.any_instance.stubs(:lookup_type).returns("OrganizationName")
          @user.stubs(:organization).returns(stub(name: "LookupThisOrgName"))

          result = mock
          result.expects(:result).returns(@response_xml)

          driver = mock
          driver.expects(:getUserFieldsByList).with(["LookupThisOrgName"]).returns(result)
          driver.stubs(:options).returns({})

          Salesforce::Integration::Driver.expects(:get_driver).returns(driver)

          @salesforce_integration.fetch_info_for(users(:minimum_end_user))
        end
      end
    end
  end
end
