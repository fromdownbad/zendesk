require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 8

describe 'Cms::Text' do
  fixtures :accounts, :translation_locales

  before do
    @account = accounts(:minimum)
    @account.stubs(:cms_texts).returns(Cms::Text)
    Cms::Text.any_instance.stubs(:account).returns(@account)

    @english    = translation_locales(:english_by_zendesk)
    @french     = translation_locales(:french)
    @japanese   = translation_locales(:japanese)
    @spanish_la = translation_locales(:latin_american_spanish)
    @spanish    = translation_locales(:spanish)
    @british    = translation_locales(:british_english)

    @fbattrs = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: @english.id }
  end

  describe "references" do
    before do
      @text = Cms::Text.create!(name: "I haz a reference", fallback_attributes: @fbattrs, account: @account)
      @text.stubs(:references).returns([stub(item_type: "Cms::Text", item_id: @text.id)])
    end

    it "does not delete a text if it has any references" do
      refute @text.destroy
    end
  end

  describe "destroy" do
    before do
      @text = Cms::Text.create!(name: "I have a fallback", fallback_attributes: @fbattrs, account: @account)
    end

    it "also destroys the fallback variant when text is destroyed" do
      fallback_id = @text.fallback.id
      @text.destroy
      assert_raises(ActiveRecord::RecordNotFound) { @text.reload }
      assert_raises(ActiveRecord::RecordNotFound) { Cms::Variant.find(fallback_id) }
    end
  end

  describe "#default_locale_id" do
    let(:text) { Cms::Text.create!(name: "I haz a reference", fallback_attributes: @fbattrs, account: @account) }
    it "returns the locale of the default variant" do
      assert_equal @english.id, text.default_locale_id
    end
  end

  describe "#locales" do
    before do
      @text = Cms::Text.create!(name: "Wibble", fallback_attributes: @fbattrs, account: @account)
      @text.variants.create(value: "blah", translation_locale_id: @english.id)
      @text.variants.create(value: "blah", translation_locale_id: @french.id)
    end

    should_eventually "get the locales for the given text" do
      assert_equal @english.id, @text.locales[0].id
      assert_equal @french.id, @text.locales[1].id
    end
  end

  describe "#identifier" do
    describe "chains of common internal non-alphanumerics" do
      before { @text = Cms::Text.create!(name: "Welcome, Dr. Wombat", fallback_attributes: @fbattrs, account: @account) }

      it "is replaced with underscores" do
        assert_equal "welcome_dr_wombat", @text.identifier
      end
    end

    describe "chains of common non-alphanumerics at the edges" do
      before { @text = Cms::Text.create!(name: "!%Welcome$^", fallback_attributes: @fbattrs, account: @account) }

      it "is removed" do
        assert_equal "welcome", @text.identifier
      end
    end

    describe "double colons" do
      before { @text = Cms::Text.create!(name: "Foo::Bar::Baz", fallback_attributes: @fbattrs, account: @account) }

      it "is replaced with dash" do
        assert_equal "foo-bar-baz", @text.identifier
      end
    end

    describe "with a single colon" do
      before { @text = Cms::Text.create!(name: "Foo:Bar", fallback_attributes: @fbattrs, account: @account) }

      it "is replaced with underscores" do
        assert_equal "foo_bar", @text.identifier
      end
    end

    describe "foreign characters with chains of common internal non-alphanumerics" do
      it "throws an invalid title error" do
        assert_raise ActiveRecord::RecordInvalid do
          Cms::Text.create!(name: "テスト, Dr. テスト", fallback_attributes: @fbattrs, account: @account)
        end
      end
    end

    describe "foreign characters with chains of common non-alphanumerics at the edges" do
      it "throws an invalid title error" do
        assert_raise ActiveRecord::RecordInvalid do
          Cms::Text.create!(name: "!%テスト$^", fallback_attributes: @fbattrs, account: @account)
        end
      end
    end

    describe "hyphens" do
      it "throws an invalid title error if at end of line" do
        assert_raise ActiveRecord::RecordInvalid do
          Cms::Text.create!(name: "Testing-", fallback_attributes: @fbattrs, account: @account)
        end
      end

      it "allows when not at end of line" do
        text = Cms::Text.create!(name: "Testing-here", fallback_attributes: @fbattrs, account: @account)
        assert_equal "testing-here", text.identifier
      end
    end

    describe_with_arturo_enabled :validate_cms_text_update do
      before { @text = Cms::Text.create!(name: "valid title", fallback_attributes: @fbattrs, account: @account) }
      it "throws an error if the title has an invalid hyphen" do
        assert_raise ActiveRecord::RecordInvalid do
          @text.name = "Bad Hyphen-"
          @text.save!
        end
      end

      it "throws an error if the title has an invalid character" do
        assert_raise ActiveRecord::RecordInvalid do
          @text.name = "テスト"
          @text.save!
        end
      end
    end

    describe_with_arturo_disabled :validate_cms_text_update do
      before { @text = Cms::Text.create!(name: "valid title", fallback_attributes: @fbattrs, account: @account) }
      it "allows invalid hyphens in title when updating" do
        @text.name = "Bad Hyphen-"
        assert @text.save!
      end

      it "allows invalid characters in title when updating" do
        @text.name = "テスト"
        assert @text.save!
      end
    end

    describe_with_and_without_arturo_enabled :validate_cms_text_update do
      before do
        @text = Cms::Text.create!(name: "valid title", fallback_attributes: @fbattrs, account: @account)
        @text2 = Cms::Text.create!(name: "valid title2", fallback_attributes: @fbattrs, account: @account)
      end

      it "allows valid name changes" do
        @text.name = "another_title"
        assert @text.save!
      end

      it "raises on name collisions" do
        assert_raise ActiveRecord::RecordInvalid do
          @text.name = "valid title2"
          assert @text.save!
        end
      end

      it "raises on identifier collisions" do
        assert_raise ActiveRecord::RecordInvalid do
          Cms::Text.create!(name: "valid_title2", fallback_attributes: @fbattrs, account: @account)
        end
      end
    end
  end

  describe "#fully_qualified_identifier" do
    before { @text = Cms::Text.new(identifier: "foo-bar-baz") }
    it "returns the fully qualified identifier" do
      assert_equal "dc.foo-bar-baz", @text.fully_qualified_identifier
    end
  end

  describe "#placeholder" do
    let(:text) { Cms::Text.new(identifier: "foo-bar-baz") }

    it "returns the identifier with brackets" do
      assert_equal "{{dc.foo-bar-baz}}", text.placeholder
    end
  end

  describe "#available_languages" do
    before do
      @account.expects(:available_languages).returns(["foo", "bar"])
      @text = Cms::Text.create!(name: "Wibble", fallback_attributes: @fbattrs, account: @account)
      @text.stubs(:account).returns(@account)
    end

    it "delegates to Account#available_languages" do
      assert_equal ["foo", "bar"], @text.available_languages
    end
  end

  describe ".sanitized_ordering_params" do
    it "sanitizes sort and direction parameters" do
      result = Cms::Text.sanitized_ordering_params("name", "ASC")
      assert_equal result, "name ASC"
      result = Cms::Text.sanitized_ordering_params("updated_at", "DESC")
      assert_equal result, "updated_at DESC"
      result = Cms::Text.sanitized_ordering_params("random", "hahaha")
      assert_equal result, "name ASC"
      result = Cms::Text.sanitized_ordering_params("'); DROP TABLE cms_texts;--", "'); DROP TABLE cms_variants;--")
      assert_equal result, "name ASC"
    end
  end

  describe "change fallback" do
    before do
      @cms_text = Cms::Text.create!(name: "Change my fallback", fallback_attributes: @fbattrs, account: @account)
      assert(@cms_text.valid?)
      @new_fallback = @cms_text.variants.create!(value: "The new fallback value",
                                                 translation_locale_id: @french.id,
                                                 active: false,
                                                 is_fallback: true)
      @cms_text = Cms::Text.find(@cms_text.id)
    end

    it "changes the fallback variant for a text and activate it" do
      assert_equal @new_fallback.id, @cms_text.fallback.id
      assert @cms_text.fallback.active
      assert_equal "The new fallback value", @cms_text.fallback.value
      assert_equal @french.id, @cms_text.fallback.translation_locale_id
    end
  end

  describe "create with fallback" do
    it "accepts nested attributes for #fallback" do
      assert_difference '@account.cms_texts.count(:all)' do
        assert_difference '@account.cms_variants.count(:all)' do
          @cms_text = Cms::Text.create!(name: "The Name 2", fallback_attributes: @fbattrs, account: @account)
          assert(@cms_text.valid?)
          assert(@cms_text.fallback.valid?)
        end
      end

      assert_equal "The Name 2", @cms_text.name
      assert_equal "The fallback value", @cms_text.fallback.value
      assert_equal 1, @cms_text.fallback.translation_locale_id
    end

    it "rollbacks and return an errors array on validation error" do
      params = { name: "The Name", fallback_attributes: { value: "No Locale", translation_locale_id: nil }}

      refute_difference 'Cms::Text.where(account_id: 1).count(:all)' do
        refute_difference 'Cms::Variant.where(account_id: 1).count(:all)' do
          @cms_text = Cms::Text.new(params)
          @cms_text.save
        end
      end

      refute @cms_text.fallback.valid?

      assert_equal "The Name", @cms_text.name
      assert_equal "No Locale", @cms_text.fallback.value
      assert_nil @cms_text.fallback.translation_locale_id
    end
  end

  describe "scope :outdated" do
    it "returns a list of texts that have at least one translations that is not current" do
      Cms::Text.outdated.each do |ot|
        flag = false
        ot.variants.each do |v|
          flag = true if v.outdated?
        end
        assert(flag)
      end
    end
  end

  describe "category stats" do
    before do
      Cms::Text.any_instance.stubs(:references).returns([])
      Cms::Text.for_account(@account.id).each(&:destroy)
    end

    it "returns empty ordered hash if there is only one text present" do
      Cms::Text.create!(name: "Fearless::HoneyBadger", fallback_attributes: @fbattrs, account: @account)
      category_hash = Cms::Text.category_stats(@account)
      assert_empty(category_hash)
    end

    it "returns empty ordered hash if there are no texts with a category name" do
      Cms::Text.create(name: "Wombat", fallback_attributes: @fbattrs)
      Cms::Text.create(name: "Penguins", fallback_attributes: @fbattrs)

      category_hash = Cms::Text.category_stats(@account)
      assert_empty(category_hash)
    end

    # This test needs to be able to load the correct translation for the no_category I18n key.
    # This will work once we have the conversion code between our local yml files and the ones I18 expects extracted into zendesk_core
    should_eventually "return ordered hash containing category of text" do
      text1 = Cms::Text.create!(name: "Wombat", fallback_attributes: @fbattrs, account: @account)
      text2 = Cms::Text.create!(name: "Penguins", fallback_attributes: @fbattrs, account: @account)
      text3 = Cms::Text.create!(name: "Fearless::HoneyBadger", fallback_attributes: @fbattrs, account: @account)
      text4 = Cms::Text.create!(name: "Awesome::Wombat", fallback_attributes: @fbattrs, account: @account)

      category_hash = Cms::Text.category_stats(@account)
      assert_equal({"Awesome" => [text4.id], "Fearless" => [text3.id], "(no category)" => [text2.id, text1.id]}, category_hash)
    end
  end

  describe "smart fetch" do
    before do
      @text = Cms::Text.create!(name: "Wombat", fallback_attributes: @fbattrs, account: @account)
      @text.variants.create(value: "english stuffs", translation_locale_id: @english.id, active: true)
      @text.variants.create(value: "japanese stuff", translation_locale_id: @japanese.id, active: true)
      @text.variants.create(value: "Ay Caramba", translation_locale_id: @spanish.id, active: true)
      @text.reload
    end

    it "returns fallback variant if locale is nil" do
      var = @text.smart_fetch(nil)
      assert_equal "The fallback value", var.value
    end

    it "fallback to parent local if the variant exists" do
      var = @text.smart_fetch(@spanish_la)
      assert_equal "Ay Caramba", var.value
    end

    it "returns variant in locale passed in" do
      var = @text.smart_fetch(@japanese)
      assert_equal "japanese stuff", var.value
    end

    it "returns fallback if locale is top level locale" do
      fbattrs = {is_fallback: true, nested: true, value: "Konnichiwa", translation_locale_id: @japanese.id }
      text = Cms::Text.create!(name: "Hello", fallback_attributes: fbattrs, account: @account)
      text.variants.create(value: "Hello US", translation_locale_id: @english.id, active: true)
      var = text.smart_fetch(@french)
      assert_equal "Konnichiwa", var.value
    end

    it "returns english variant english's child" do
      fbattrs = {is_fallback: true, nested: true, value: "Konnichiwa", translation_locale_id: @japanese.id }
      text = Cms::Text.create!(name: "Hello", fallback_attributes: fbattrs, account: @account)
      text.variants.create(value: "Hello US", translation_locale_id: @english.id, active: true)
      var = text.smart_fetch(@british)
      assert_equal "Hello US", var.value
    end

    it "doesn't loop infinitely" do
      fbattrs = {is_fallback: true, nested: true, value: "Konnichiwa", translation_locale_id: @japanese.id }
      text = Cms::Text.create!(name: "Hello", fallback_attributes: fbattrs, account: @account)
      @british.stubs(:parent).returns(@british)
      var = text.smart_fetch(@british)
      assert_equal "Konnichiwa", var.value
    end
  end
end
