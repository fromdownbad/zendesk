require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'Cms::Variant' do
  fixtures :accounts

  before do
    Timecop.freeze
    @account = accounts(:minimum)
    @account.stubs(:cms_texts).returns(Cms::Text)
    Cms::Text.any_instance.stubs(:account).returns(@account)
    params = { name: "EXP", account: @account,
               fallback_attributes: { is_fallback: "true", nested: true, value: "The fallback value", translation_locale_id: 1 }}
    @cms_text = Cms::Text.create!(params)
    @cms_variant = @cms_text.variants.create(value: "The Value", translation_locale_id: 2)
  end

  describe "#is_fallback?" do
    before do
      @cms_text = Cms::Text.find(@cms_text.id)
      @cms_variant = Cms::Variant.find(@cms_variant.id)
      @fallback = @cms_text.fallback
    end

    it "checks if the current text variant is a fallback" do
      assert_equal @cms_text.fallback.id, @fallback.id
      assert_not_equal @cms_text.fallback.id, @cms_variant.id
      assert(@fallback.is_fallback?)
      refute @cms_variant.is_fallback?
    end

    it "checks if translation_locale is validated as present" do
      @fallback.translation_locale_id = nil
      refute @fallback.valid?
      assert_equal @fallback.errors.messages[I18n.t("txt.admin.models.cms.text.Default_Language").to_sym], [I18n.t('txt.admin.models.cms.variant.cant_be_blank')]
    end

    it "checks if translation_locale_id is valid" do
      @fallback.translation_locale_id = 99999999
      refute @fallback.valid?
    end

    it "doesn't destruct when not marked_for_destruction" do
      refute @fallback.destroy
      assert_equal @fallback.errors.messages[:base], [I18n.t('txt.admin.models.cms.variant.default_variant_cannot_be_deleted')]
    end

    it "allows destruction only when marked_for_destruction" do
      @fallback.mark_for_destruction
      assert @fallback.destroy
    end
  end

  describe "is variant" do
    before do
      @cms_text = Cms::Text.find(@cms_text.id)
      @cms_variant = Cms::Variant.find(@cms_variant.id)
    end

    it "checks if translation_locale is validated as present" do
      @cms_text = Cms::Text.find(@cms_text.id)
      @cms_variant.translation_locale_id = nil
      refute @cms_variant.valid?
      assert_equal @cms_variant.errors.messages[I18n.t("txt.admin.models.cms.variant.Language").to_sym], [I18n.t('txt.admin.models.cms.variant.cant_be_blank')]
    end

    it "checks if translation_locale is valid" do
      @cms_text = Cms::Text.find(@cms_text.id)
      @cms_variant.translation_locale_id = 999999
      refute @cms_variant.valid?
    end

    it "isn't outdated if it was created at the same time as its fallback" do
      assert_equal @cms_text.updated_at, @cms_variant.updated_at
      refute @cms_variant.outdated?
    end
  end
end
