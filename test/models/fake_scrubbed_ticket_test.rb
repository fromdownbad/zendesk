require_relative '../support/test_helper'
require_relative '../support/collaboration_settings_test_helper'
require 'preload'

SingleCov.covered! uncovered: 2

describe FakeScrubbedTicket do
  let(:ticket) { FactoryBot.create(:ticket, account: recipient.account) }
  let(:scrubbed_ticket) { ticket.becomes(FakeScrubbedTicket) }

  let(:recipient) { users(:minimum_end_user) }

  before do
    ticket.additional_tags = ['test']
  end

  it 'should X out description' do
    assert_equal 'X', scrubbed_ticket.description
    refute_equal 'X', ticket.description
  end

  it 'should remove current_tags' do
    assert_equal "", scrubbed_ticket.current_tags
    refute ticket.current_tags.blank?, 'This test is not valid if the tags are blank'
    refute_equal "", ticket.current_tags
  end

  describe "with follower_and_email_cc_collaboration enabled" do
    before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

    it 'should remove collaborators' do
      ticket.collaborations.create(user: recipient)
      assert_equal [], scrubbed_ticket.collaborators
      ticket.collaborations.reload
      assert_equal [recipient.id], ticket.collaborators.map(&:id)
    end
  end

  describe "with follower_and_email_cc_collaboration disabled" do
    before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

    it 'should remove collaborators' do
      ticket.collaborations.create(user: recipient)
      assert_equal [], scrubbed_ticket.collaborators
      assert_equal [recipient.id], ticket.collaborators.map(&:id)
    end
  end
end
