require_relative "../../support/test_helper"

SingleCov.covered!

describe 'GroupSetting' do
  fixtures :groups

  before do
    @group = groups(:minimum_group)
  end

  describe '#chat_enabled' do
    it 'defaults to false' do
      assert_equal false, @group.settings.chat_enabled
    end
  end

  describe 'set group settings' do
    it 'touches group' do
      updated_time = @group.updated_at

      group_attr = {
        settings: { chat_enabled: true }
      }

      Timecop.travel(Time.now + 10.seconds) do
        @group.update_attributes!(group_attr)
      end

      assert(@group.reload.settings.chat_enabled)
      assert updated_time < @group.reload.updated_at
    end
  end
end
