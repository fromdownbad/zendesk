require_relative "../support/test_helper"

SingleCov.covered!

describe UserSeat do
  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:end_user) { users(:minimum_end_user) }

  def create_voice_subscription(max_agents:)
    FactoryBot.create(
      :voice_subscription,
      account: account,
      plan_type: ZendeskBillingCore::Voice::PlanType::Advanced.plan_type,
      max_agents: max_agents
    )
  end

  before { ZendeskBillingCore::Zuora::Jobs::AddVoiceJob.stubs(:enqueue) }

  describe '#create' do
    it 'does not create a record when the number of voice user seats is greater than the limit' do
      create_voice_subscription(max_agents: 1)

      agent_1 = FactoryBot.create(:agent, account: account)
      agent_2 = FactoryBot.build(:agent, account: account)

      user_seat_1 = FactoryBot.create(:voice_user_seat, account: account, user: agent_1)
      user_seat_2 = FactoryBot.build(:voice_user_seat, account: account, user: agent_2)

      assert user_seat_1.valid?
      refute user_seat_2.valid?
    end

    it 'does not create a record for an end user' do
      user_seat = FactoryBot.build(:voice_user_seat, account: account, user: end_user)
      refute user_seat.valid?
    end

    it 'ignores deleted users' do
      create_voice_subscription(max_agents: 2)

      agent_1 = FactoryBot.create(:agent, account: account)
      agent_2 = FactoryBot.create(:agent, account: account)
      agent_3 = FactoryBot.create(:agent, account: account)

      FactoryBot.create(:voice_user_seat, account: account, user: agent_1)

      user_seat_2 = FactoryBot.create(:voice_user_seat, account: account, user: agent_2)
      user_seat_3 = FactoryBot.build(:voice_user_seat, account: account, user: agent_3)

      user_seat_2.update_attribute(:user_id, 999999)

      assert user_seat_3.valid?
    end

    it 'does not create a voice seat for a non-assignable agent' do
      light_agent = FactoryBot.create(:agent, account: account)
      light_agent.permission_set = PermissionSet.create_light_agent!(account)
      light_agent.save!

      user_seat = FactoryBot.build(:voice_user_seat, account: account, user: light_agent)
      refute user_seat.valid?
    end

    it 'requires a value for role' do
      assert_raises(ActiveRecord::RecordInvalid) do
        UserSeat.create!(user_id: agent.id, seat_type: 'connet')
      end
    end

    it 'raises an exception when using params that are not permitted' do
      params = ActionController::Parameters.new(account_id: 1)

      assert_raises ActiveModel::ForbiddenAttributesError do
        UserSeat.create(params)
      end
    end

    describe_with_arturo_enabled :enable_voice_after_voice_seat_creation do
      it 'notifies Lotus if the seat type is voice' do
        radar_notification = stub
        ::RadarFactory.expects(:create_radar_notification).with(account, "voice_enabled/#{account.owner.id}").returns(radar_notification).once
        radar_notification.expects(:send).with('message', {})

        UserSeat.create!(account_id: account.id, user_id: agent.id, seat_type: 'voice')
      end

      it 'does not notify Lotus if the seat type is not voice' do
        ::RadarFactory.expects(:create_radar_notification).never

        UserSeat.create(account_id: account.id, user_id: agent.id, seat_type: 'connect')
      end
    end

    describe_with_arturo_disabled :enable_voice_after_voice_seat_creation do
      it 'does not notify Lotus if the seat type is voice' do
        ::RadarFactory.expects(:create_radar_notification).never

        UserSeat.create(account_id: account.id, user_id: agent.id, seat_type: 'voice')
      end
    end
  end

  describe '#destroy' do
    before do
      create_voice_subscription(max_agents: 3)
    end

    let!(:seat) { agent.user_seats.voice.create(account_id: account.id, user_id: agent.id) }

    it 'enqueues a job to notify Voice' do
      Voice::NotifyVoiceSeatIsDestroyed.expects(:enqueue).with(account.id, agent.id)
      assert seat.destroy
    end

    it 'does not enqueue to notify voice if the destroy fails' do
      Voice::NotifyVoiceSeatIsDestroyed.expects(:enqueue).never

      with_rollback(seat) { seat.destroy }
    end
  end

  describe '.can_create_new_voice_seats?' do
    it 'returns true when the number of user seats to be created is less or equal the limit' do
      create_voice_subscription(max_agents: 3)
      user_ids = [1, 2, 3]

      UserSeat.can_create_new_voice_seats?(account, user_ids).must_equal true
    end

    it 'returns false when the number of user seats to be created is greater than the limit' do
      create_voice_subscription(max_agents: 3)
      user_ids = [1, 2, 3, 4]

      UserSeat.can_create_new_voice_seats?(account, user_ids).must_equal false
    end

    it 'excludes overlapping user ids' do
      create_voice_subscription(max_agents: 3)
      agent.user_seats.voice.create(account_id: account.id)
      user_ids = [agent.id, 2, 3]

      UserSeat.can_create_new_voice_seats?(account, user_ids).must_equal true
    end

    it 'returns true when voice subscription does not exist' do
      UserSeat.can_create_new_voice_seats?(account, [1]).must_equal true
    end
  end

  describe '#delete_voice_number' do
    it 'removes voice number identity' do
      Arturo.enable_feature!(:voice_delete_number_on_downgrade)

      stub_request(:put, "https://minimum.zendesk-test.com/api/v2/channels/voice/availabilities/#{agent.id}.json")
      UserVoiceForwardingIdentity.create!(user: agent, value: "14155556789")
      voice_seat = FactoryBot.create(:voice_user_seat, account: account, user: agent)
      agent.reload
      assert_equal agent.voice_number, '+14155556789'

      voice_seat.delete_voice_number
      assert_empty agent.identities.voice_number
    end
  end
end
