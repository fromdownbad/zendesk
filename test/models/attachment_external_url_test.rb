require_relative "../support/test_helper"

SingleCov.covered!

describe AttachmentExternalURL do
  fixtures :accounts, :attachments

  let(:account)    { accounts(:minimum) }
  let(:attachment) { attachments(:serialization_comment_attachment) }

  describe "#truncate_url" do
    describe "when the url is greater than 255 characters" do
      describe "when using multi byte characters with an extension" do
        before do
          url = "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=%D0%97%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5+%D0%BE+%D0%B2%D0%BD%D0%B5%D1%81%D0%B5%D0%BD%D0%B8%D0%B8+%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B9+%D0%9E%D0%9E%D0%9E+%22%D0%90%D0%B3%D0%B5%D0%BD%D1%81%D1%82%D0%B2%D0%BE+%D0%BD%D0%B5%D0%B4%D0%B2%D0%B8%D0%B6%D0%B8%D0%BC%D0%BE%D1%81%D1%82%D0%B8+%E2%84%961204%22.pdf"
          @external_url = AttachmentExternalURL.new(account: account, attachment: attachment, url: url)
          @external_url.save!
        end

        it "adds elipses and truncate the url with the extension" do
          assert_equal "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=%D0%97%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5+%D0%BE+%D0%B2%D0%BD%D0%B5%D1%81%D0%B5%D0%BD%D0%B8%D0%B8+%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B9+%D0%9E%D0%9E....pdf", @external_url.url
          assert_equal "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=Заявление+о+внесении+изменений+ОО....pdf", URI.decode(@external_url.url)
          assert_operator @external_url.url.size, '<=', 255
        end
      end

      describe "when using multi byte characters without an extension" do
        before do
          url = "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=%D0%97%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5+%D0%BE+%D0%B2%D0%BD%D0%B5%D1%81%D0%B5%D0%BD%D0%B8%D0%B8+%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B9+%D0%9E%D0%9E%D0%9E+%22%D0%90%D0%B3%D0%B5%D0%BD%D1%81%D1%82%D0%B2%D0%BE+%D0%BD%D0%B5%D0%B4%D0%B2%D0%B8%D0%B6%D0%B8%D0%BC%D0%BE%D1%81%D1%82%D0%B8+%E2%84%961204%22"
          @external_url = AttachmentExternalURL.new(account: account, attachment: attachment, url: url)
          @external_url.save!
        end

        it "adds elipses and truncate the url" do
          assert_equal "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=%D0%97%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5+%D0%BE+%D0%B2%D0%BD%D0%B5%D1%81%D0%B5%D0%BD%D0%B8%D0%B8+%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B9+%D0%9E%D0%9E%D0%9E...", @external_url.url
          assert_equal "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=Заявление+о+внесении+изменений+ООО...", URI.decode(@external_url.url)
          assert_operator @external_url.url.size, '<=', 255
        end
      end

      describe "when using single byte characters with an extension" do
        before do
          url = "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz.pdf"
          @external_url = AttachmentExternalURL.new(account: account, attachment: attachment, url: url)
          @external_url.save!
        end

        it "adds elipses and truncate the url with the extension" do
          assert_equal "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwx....pdf", @external_url.url
          assert_equal 255, @external_url.url.size
        end
      end

      describe "when using single byte characters without an extension" do
        before do
          url = "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
          @external_url = AttachmentExternalURL.new(account: account, attachment: attachment, url: url)
          @external_url.save!
        end

        it "adds elipses and truncate the url" do
          assert_equal "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzab...", @external_url.url
          assert_equal 255, @external_url.url.size
        end
      end
    end

    describe "when the url is less than or equal to 255 characters" do
      before do
        @url = "https://example.zendesk.com/attachments/token/0j5e5ek9z9r2wv9/?name=%D0%97%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5.pdf"
        @external_url = AttachmentExternalURL.new(account: account, attachment: attachment, url: @url)
        @external_url.save!
      end

      it "dos nothing" do
        assert_equal @url, @external_url.url
      end
    end
  end
end
