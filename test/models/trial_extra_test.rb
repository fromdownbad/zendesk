require_relative "../support/test_helper"

SingleCov.covered!

describe 'TrialExtra' do
  fixtures :accounts

  before do
    @trial_extra_params = {
      "Last_Search_Type__c"        => "val1",
      "Last_Search_String__c"      => "val2",
      "Last_Search_Engine__c"      => "val3",
      "Last_Referrer__c"           => "val4",
      "Last_PPC_Keyword__c"        => "val5",
      "Last_PPC_Campaign__c"       => "val6",
      "Original_Search_Type__c"    => "val7",
      "Original_Search_String__c"  => "val8",
      "Original_Search_Engine2__c" => "val9",
      "Original_Referrer2__c"      => "val10",
      "Original_PPC_Keyword__c"    => "val11",
      "Original_PPC_Campaign__c"   => "val12",
      "Convertro_SID__c"           => "val13",
      "GA_VID__c"                  => "val14",
      "GA_HID__c"                  => "val15",
      "Session_Referrer__c"        => "val16",
      "Session_Count__c"           => "val17",
      "Session_First__c"           => "val18",
      "Session_Landing__c"         => "val19",
      "Session_Last__c"            => "val20",
      "Behavioral_1__c"            => "val21",
      "Behavioral_2__c"            => "val22",
      "Behavioral_3__c"            => "val23",
      "Behavioral_4__c"            => "val24",
      "Partner_ID__c"              => "val25",
      "Country__c"                 => "val26",
      "DB_City__c"                 => "val27",
      "DB_State__c"                => "val28",
      "DB_CName__c"                => "val29",
      "DB_CCode__c"                => "val30",
      "DB_Zip__c"                  => "val31"
    }
  end

  it "converts a hash to a list of objects" do
    objects = TrialExtra.from_hash(@trial_extra_params)
    assert_equal objects.size, @trial_extra_params.size

    @trial_extra_params.each do |key, value|
      object = objects.find { |obj| obj.key == key }
      assert object.present?
      assert_equal object.value, value
    end
  end

  it "sanitizes and remove html tags from keys and values when creating objects from hash" do
    problematic_string = '<a href="javascript:alert(\no!\)">Example</a>'
    sanitized_string = 'Example'
    objects = TrialExtra.from_hash(problematic_string => problematic_string)

    assert_equal objects.first.key, sanitized_string
    assert_equal objects.first.value, sanitized_string
  end

  it "does not fail with long keys and values but truncate them to 255 characters" do
    problematic_hash = { 'a' * 300 => 'a' * 300 }
    truncated_hash = { 'a' * 255 => 'a' * 255 }

    account = accounts(:minimum)
    account.trial_extras = TrialExtra.from_hash(problematic_hash)
    account.save!

    account.reload
    assert_equal truncated_hash, account.trial_extras.to_hash
  end

  it "converts a list of objects to a hash" do
    account = accounts(:minimum)
    account.trial_extras = TrialExtra.from_hash(@trial_extra_params)
    account.save

    account.reload
    assert_equal @trial_extra_params, account.trial_extras.to_hash
  end
end
