require_relative "../support/test_helper"

SingleCov.covered!

describe 'TicketLink' do
  fixtures :tickets, :accounts

  describe "#source_presence_including_archived validation" do
    let(:ticket_link) do
      TicketLink.new(
        target: tickets(:minimum_1),
        source: tickets(:minimum_5)
      )
    end

    before do
      tickets(:minimum_1).update_attribute(:via_id, ViaType.CLOSED_TICKET)
    end

    it "is valid when the source ticket is present" do
      assert ticket_link.valid?
    end

    it "is invalid when the source cannot be found" do
      ticket_link.source = nil
      ticket_link.source_id = -1

      refute ticket_link.valid?
      assert_equal ["cannot be blank"], ticket_link.errors[:source]
    end
  end

  describe "#source_with_archived" do
    let(:ticket_link) do
      TicketLink.create(
        target: tickets(:minimum_1),
        source: tickets(:minimum_5)
      )
    end

    before do
      tickets(:minimum_1).update_attribute(:via_id, ViaType.CLOSED_TICKET)
    end

    it "returns non-archived source if present" do
      source_ticket = tickets(:minimum_5)
      assert_equal ticket_link.source_with_archived.id, source_ticket.id
    end

    it "returns archived source if present" do
      archive_and_delete(tickets(:minimum_5))
      ticket_link.reload # source has been deleted, but association is cached
      source_ticket = ticket_link.source_with_archived
      assert_equal source_ticket.id, tickets(:minimum_5).id
      assert_instance_of TicketArchiveStub, source_ticket
    end
  end

  describe "#save" do
    let(:ticket_link) do
      TicketLink.create(
        target: tickets(:minimum_1), source: tickets(:minimum_2)
      )
    end

    it "shoulds automatically add account from target and proper link type" do
      refute ticket_link.save
      Ticket.any_instance.stubs(:via_id).returns(ViaType.CLOSED_TICKET)
      assert ticket_link.save
      assert_equal tickets(:minimum_1).account, ticket_link.account
      assert_equal TicketLink::FOLLOWUP, ticket_link.link_type
    end

    describe "#link_type" do
      let(:ticket_link) do
        TicketLink.create(
          target: tickets(:minimum_1), source: tickets(:minimum_2)
        )
      end

      it "should correctly save merge link types" do
        Ticket.any_instance.stubs(:via_id).returns(ViaType.WEB_SERVICE)
        ticket_link.link_type = TicketLink::MERGE
        assert ticket_link.save
        assert_equal TicketLink::MERGE, ticket_link.link_type
      end

      it "should correctly save followup link types" do
        Ticket.any_instance.stubs(:via_id).returns(ViaType.CLOSED_TICKET)
        ticket_link.link_type = TicketLink::FOLLOWUP
        assert ticket_link.save
        assert_equal TicketLink::FOLLOWUP, ticket_link.link_type
      end
    end
  end
end
