require_relative "../support/test_helper"

SingleCov.covered! uncovered: 24

describe Entry do
  fixtures :forums, :users, :user_identities, :posts, :entries, :accounts,
    :attachments, :translation_locales, :subscriptions, :tokens, :attachments

  should have_db_column(:title).of_type(:string)
  should have_db_column(:sanitize_on_display).of_type(:boolean)
  should have_db_column(:votes_count).of_type(:integer)

  should_validate_presence_of :account, :title, :submitter, :forum
  should_not allow_mass_assignment_of :sanitize_on_display if RAILS4

  it "knows its forum's type" do
    subject.forum = Forum.new(display_type: ForumDisplayType::QUESTIONS)
    assert_equal 'Questions', subject.forum_type
  end

  describe "Saving an entry" do
    before do
      @agent = users(:minimum_agent)
      @user  = users(:minimum_end_user)
      @html_text = '<strong>htmlok</strong>'
      @script_text = "bad<script>alert('baddie');</script>text"
      @entry = entries(:fish)
    end

    describe 'existing record' do
      it 'sanitizes body and title' do
        @entry.update_attribute(:body, @script_text)
        @entry.update_attribute(:title, @script_text)
        assert_equal "badtext", @entry.reload.body
        assert_equal "badtext", @entry.reload.title
      end
      it 'allows html' do
        @entry.update_attribute(:body, @html_text)
        @entry.update_attribute(:title, @html_text)
        assert_equal @html_text, @entry.reload.title
        assert_equal @html_text, @entry.reload.body
      end
    end
  end

  describe "#find_related" do
    before do
      @account = accounts(:minimum)
      @user    = stub(permissions: "user-permissions", account_id: @account.id)
      @tags    = %w[some tags]
      @md5     = Digest::MD5.hexdigest(@tags.map { |t| "tags:#{t}" }.join(" "))
    end

    it "returns [] when given a blank tags string" do
      assert_equal [], Entry.find_related(nil, nil, nil)
    end

    describe "when a user has accessible forums" do
      before do
        @forums = [stub(id: 3), stub(id: 7)]
        @user.expects(:accessible_forums).returns(@forums)
      end

      it "hits the Rails cache with a predictable cache key" do
        Rails.cache.expects(:fetch).with("#{@account.id}/#{@user.permissions}/#{@forums.map(&:id).join("/")}/#{@md5}", expires_in: 1.hour).returns(["wibble"])
        assert_equal ["wibble"], Entry.find_related(@account, @user, @tags)
      end
    end

    describe "when a user has no accessible forums" do
      before do
        @forums = []
        @user.expects(:accessible_forums).returns(@forums)
      end

      it "hits the Rails cache with a predictable cache key" do
        Rails.cache.expects(:fetch).with("#{@account.id}/#{@user.permissions}/-1/#{@md5}", expires_in: 1.hour).returns(["wibble"])
        assert_equal ["wibble"], Entry.find_related(@account, @user, @tags)
      end
    end
  end

  describe "uploads" do
    before do
      @entry = entries(:fish)
    end

    describe "single upload" do
      before do
        @upload = tokens(:minimum_upload_token)
        @attachments = @upload.attachments.to_a

        @entry.uploads = [@upload.value]
        @entry.send(:add_attachments)
      end

      it "adds attachments" do
        assert_equal @attachments, @entry.attachments.to_a
      end
    end

    describe "with a private comment" do
      before do
        @entry.is_public = false
        @upload = tokens(:minimum_upload_token)
        @entry.uploads = [@upload.value]
        @entry.send(:add_attachments)
      end

      it "makes attachment private" do
        assert @entry.attachments.none?(&:is_public?)
      end
    end

    describe "multiple uploads" do
      before do
        @uploads = [tokens(:minimum_upload_token), tokens(:other_minimum_upload_token)]
        @attachments = @uploads.map(&:attachments).tap(&:flatten!)
        @entry.uploads = @uploads.map(&:value)
        @entry.send(:add_attachments)
      end

      it "adds all attachments" do
        assert_equal @attachments, @entry.attachments
      end
    end
  end

  describe 'An entry for rss' do
    before do
      @entry = entries(:fish)
    end

    it 'provides an rss title' do
      assert_equal(@entry.title, @entry.rss_title)
    end

    it 'provides rss guid parts' do
      assert_equal([@entry.forum_id, @entry.id], @entry.rss_guid_parts)
    end

    it 'provides a creator' do
      assert_equal(@entry.submitter, @entry.creator)
    end

    it 'provides an entry' do
      assert_equal(@entry, @entry.entry)
    end
  end

  describe "#subscribe" do
    before do
      @entry = entries(:sticky)
      @user = users(:minimum_agent)
      @entry.subscribe(@user)
    end
    should_create :watching

    it "recognizes user as a subscriptor" do
      assert @entry.subscribed?(@user)
    end
  end

  describe "unpinned" do
    before do
      @entry = entries(:sticky)
      assert @entry.is_pinned?
    end

    it "does not be pinned" do
      @entry.unpin
      @entry.reload
      assert_equal false, @entry.is_pinned?
    end

    it "clears the forums cache" do
      @entry.account.expects(:expire_scoped_cache_key).with(:forums)
      @entry.unpin
    end
  end

  describe "Moving an entry to another forum" do
    before do
      @entry = entries(:ponies)
      assert @entry.flag.planned?
    end

    it "does not reset the entry flag if the forum is of the same type" do
      assert_equal @entry.forum.display_type, forums(:questionable).display_type
      @entry.forum = forums(:questionable)
      @entry.save
      assert @entry.flag.planned?
    end

    it "resets entry + associated posts flags if the forum is not of the same type" do
      forums(:agents).update_attribute(:display_type_id, ForumDisplayType::QUESTIONS.id)
      @entry.update_attribute(:forum_id, forums(:agents).id)
      @entry.posts.first.update_attribute(:is_informative, true)
      assert @entry.reload.flag.answered?

      @entry.update_attribute(:forum_id, forums(:announcements).id)
      refute @entry.reload.flag.answered?
      refute @entry.posts.detect(&:is_informative?) # No posts are marked as The Answer
    end
  end

  it "tests save should update post id for posts belonging to entry" do
    # checking current forum_id's are in sync
    entry = entries(:sticky)
    post_forums = lambda do
      entry.posts.each { |p| assert_equal p.forum_id, entry.forum_id }
    end
    post_forums.call
    assert_equal forums(:solutions).id, entry.forum_id

    # updating forum_id
    entry.update_attribute :forum_id, forums(:announcements).id
    assert_equal forums(:announcements).id, entry.reload.forum_id
    post_forums.call
  end

  it "tests should require title user and forum" do
    t = Entry.new
    t.current_user = users(:minimum_end_user)
    refute t.valid?
    assert t.errors[:title].any?
    assert t.errors[:forum].any?
    assert t.errors[:account_id].any?
    refute  t.save
    t.submitter = users(:minimum_end_user)
    t.title = "happy life"
    t.forum = forums(:solutions)
    t.account = t.submitter.account
    t.body = 'knud'
    assert t.save
    assert_equal [], t.errors[:title]
    assert_equal [], t.errors[:submitter]
    assert_equal [], t.errors[:forum]
  end

  it "tests should create entry" do
    counts = lambda { [Entry.count(:all), forums(:solutions).entries_count] }
    old = counts.call
    t = forums(:solutions).entries.build(title: 'foo')
    t.submitter = t.current_user = users(:minimum_end_user)
    t.account   = t.submitter.account
    t.body = 'knud'
    assert t.valid?
    t.save
    refute t.is_pinned?
    [forums(:solutions), users(:minimum_end_user)].each(&:reload)
    assert_equal old.collect { |n| n + 1 }, counts.call
  end

  it "tests should delete entry" do
    counts = lambda { [Entry.count(:all), Post.count(:all), forums(:solutions).entries_count] }
    old    = counts.call

    entries(:ponies).current_user = users(:minimum_admin)
    entries(:ponies).destroy
    forums(:solutions).reload

    assert_equal old.collect { |n| n - 1 }, counts.call
  end

  it "tests hits" do
    hits = entries(:sticky).hits
    entries(:sticky).hit!
    entries(:sticky).hit!
    assert_equal(hits + 2, entries(:sticky).reload.hits)
  end

  it "tests create with body and tags" do
    entry = Entry.new do |e|
      e.account      = accounts(:minimum)
      e.submitter    = users(:minimum_admin)
      e.current_user = users(:minimum_admin)
      e.forum        = forums(:agents)
      e.title        = "En titel"
      e.body         = "En krop"
      e.current_tags = "big benny"
    end

    entry.save!

    assert_equal "En krop", entry.body
    assert_equal ['benny', 'big'], entry.tag_array.sort
  end

  it "tests move entry from public to closed forum" do
    t = entries(:sticky)
    assert t.attachments.first.is_public?
    t.current_user = users(:minimum_admin)
    t.forum = forums(:agents)
    t.save!
    refute t.is_public?
    refute t.attachments.first.is_public?
  end

  describe "creation" do
    describe "in a locked forum" do
      before do
        @forum = accounts(:with_groups).forums.create!(
          name: 'test_forum',
          visibility_restriction_id: VisibilityRestriction::EVERYBODY.id,
          is_locked: true
        )
      end
      it "does not be possible for end-users" do
        user = @forum.account.end_users.first
        assert(user)
        entry = @forum.entries.build(account: @forum.account, submitter: user, title: 'title', body: 'body')
        entry.current_user = user
        assert(!entry.save)
      end
      it "does not be possible for suspended accounts" do
        user = @forum.account.agents.where(restriction_id: RoleRestrictionType.NONE).first
        assert(user)
        @forum.account.is_serviceable = false
        @forum.save!

        entry = @forum.entries.build(account: @forum.account, submitter: user, title: 'title', body: 'body')
        entry.current_user = user
        refute entry.save, "entry cannot be created/updated - this Zendesk account has expired"
      end
      it "is possible for unrestricted agent" do
        user = @forum.account.agents.where(restriction_id: RoleRestrictionType.NONE).first
        assert(user)
        entry = @forum.entries.build(account: @forum.account, submitter: user, title: 'title', body: 'body')
        entry.current_user = user
        assert(entry.save)
      end
      it "does not be possible for restricted agent" do
        user = @forum.account.agents.where.not(restriction_id: RoleRestrictionType.NONE).first
        assert(user)
        entry = @forum.entries.build(account: @forum.account, submitter: user, title: 'title', body: 'body')
        entry.current_user = user
        assert(!entry.save)
      end
      it "is possible for moderators" do
        user = @forum.account.agents.where(restriction_id: RoleRestrictionType.NONE).first
        assert(user)
        user.is_moderator = true
        user.save!
        entry = @forum.entries.build(account: @forum.account, submitter: user, title: 'title', body: 'body')
        entry.current_user = user
        assert(entry.save)
      end
    end

    describe "in an unlocked forum" do
      before do
        @forum = accounts(:with_groups).forums.create!(
          name: 'test_forum',
          visibility_restriction_id: VisibilityRestriction::EVERYBODY.id,
          is_locked: false
        )
      end

      it "is possible for end-users" do
        user = @forum.account.end_users.first
        assert(user)
        entry = @forum.entries.build(account: @forum.account, submitter: user, title: 'title', body: 'body')
        entry.current_user = user
        assert(entry.save)
      end

      it "is possible for unrestricted agent" do
        user = @forum.account.agents.where(restriction_id: RoleRestrictionType.NONE).first
        assert(user)
        entry = @forum.entries.build(account: @forum.account, submitter: user, title: 'title', body: 'body')
        entry.current_user = user
        assert(entry.save)
      end

      it "is possible for restricted agent" do
        user = @forum.account.agents.where.not(restriction_id: RoleRestrictionType.NONE).first
        assert(user)
        entry = @forum.entries.build(account: @forum.account, submitter: user, title: 'title', body: 'body')
        entry.current_user = user
        assert(entry.save)
      end

      it "is possible for moderators" do
        user = @forum.account.agents.where(restriction_id: RoleRestrictionType.NONE).first
        assert(user)
        user.is_moderator = true
        user.save!
        entry = @forum.entries.build(account: @forum.account, submitter: user, title: 'title', body: 'body')
        entry.current_user = user
        assert(entry.save)
      end
    end
  end

  it "tests access rights" do
    assert accounts(:minimum).entries.for_user(users(:minimum_end_user)).find_by_id(entries(:sticky).id)
    assert users(:minimum_end_user).can?(:edit, entries(:sticky))

    assert accounts(:minimum).entries.for_user(users(:minimum_end_user)).find_by_id(entries(:ponies).id)
    refute users(:minimum_end_user).can?(:edit, entries(:ponies))
    assert_nil accounts(:minimum).entries.for_user(users(:minimum_end_user)).find_by_id(entries(:apples).id)
    refute users(:minimum_end_user).can?(:edit, entries(:apples))
    refute users(:minimum_end_user).can?(:edit, entries(:fish))
  end

  it "has access on own organisations" do
    assert accounts(:minimum).entries.for_user(users(:minimum_end_user)).find_by_id(entries(:fish).id)
  end

  it "does not have access on other organisations" do
    assert_nil accounts(:minimum).entries.for_user(users(:minimum_end_user)).find_by_id(entries(:oranges).id)
    refute users(:minimum_end_user).can?(:edit, entries(:oranges))
  end

  it "tests access rights on agents" do
    assert accounts(:minimum).entries.for_user(users(:minimum_agent)).find_by_id(entries(:fish).id)
    assert users(:minimum_agent).can?(:edit, entries(:fish)) # entry submitted by someone else

    users(:minimum_agent).update_attribute(:is_moderator, false)
    assert accounts(:minimum).entries.for_user(users(:minimum_agent)).find_by_id(entries(:fish).id)
    refute users(:minimum_agent).can?(:edit, entries(:fish)) # entry submitted by someone else

    assert accounts(:minimum).entries.for_user(users(:minimum_agent)).find_by_id(entries(:apples).id)
    assert users(:minimum_agent).can?(:edit, entries(:apples)) # own entry

    account = accounts(:minimum)
    entry   = entries(:ponies)
    assert account.entries.for_user(account.anonymous_user).find_by_id(entry.id)
    assert account.entries.for_user(users(:minimum_end_user)).find_by_id(entry.id)
    assert account.entries.for_user(users(:minimum_agent)).find_by_id(entry.id)

    entry.forum.visibility_restriction_id = VisibilityRestriction::LOGGED_IN_USERS.id
    entry.forum.save!
    refute account.entries.for_user(account.anonymous_user).find_by_id(entry.id)
    assert account.entries.for_user(users(:minimum_end_user)).find_by_id(entry.id)
    assert account.entries.for_user(users(:minimum_agent)).find_by_id(entry.id)
  end

  describe "#relative_path" do
    it "returns a proper relative path" do
      entry = Entry.new
      entry.stubs(:id).returns(9999)

      assert_equal "/entries/9999", entry.relative_path
    end
  end

  describe "Entry#to_param" do
    before { @entry = Entry.new }

    it "consists of the id and a normalized version of the name separated by a single dash" do
      @entry.stubs(:id).returns(123)
      @entry.title = "The World's Most Wonderful Entry"

      assert_equal "123-The-World-s-Most-Wonderful-Entry", @entry.to_param
    end

    it "allows unicode characters and the id" do
      @entry.stubs(:id).returns(123)
      @entry.title = "Македонски"

      assert_equal "123-Македонски", @entry.to_param
    end

    it "allows unicode alpha characters" do
      @entry.title = "Ein Schöenes Tag"

      assert_match %r{Ein-Schöenes-Tag$}, @entry.to_param
    end

    it "allows unicode alphanumeric characters and remove others" do
      @entry.title = "A Little Кириличен Twist! 123"

      assert_match %r{A-Little-Кириличен-Twist-123$}, @entry.to_param
    end
  end

  describe "Validating an Entry" do
    before do
      @entry = Entry.new(
        account: accounts(:minimum),
        body: "En krop",
        current_tags: "big benny",
        forum: forums(:agents),
        title: "En titel",
        submitter: User.new(account: accounts(:minimum))
      ) do |entry|
        entry.current_user = users(:minimum_admin)
      end
    end

    it "is valid" do
      assert @entry.valid?
    end

    it "sets updater to the submitter" do
      @entry.valid?
      assert_equal @entry.submitter, @entry.updater
    end

    it "raises an error if current_tags overflows" do
      @entry.current_tags = "x" * 5000
      refute @entry.save
      assert @entry.errors[:current_tags].any?
    end
  end

  describe "The Entries for an End User" do
    before do
      @user = users(:minimum_author)
      @user.update_attribute(:translation_locale, translation_locales(:japanese))
      forums(:solutions).update_attribute(:translation_locale, translation_locales(:spanish))
    end

    it "belongs only to Forums whose translation_locale matches or is nil" do
      entries = @user.account.entries.for_user(@user)

      assert entries.present?

      entries.each do |entry|
        assert entry.forum.visibility_restriction.everybody? || entry.forum.visibility_restriction.logged_in_only?
        assert entry.forum.translation_locale == @user.translation_locale || entry.forum.translation_locale.blank?
      end
    end
  end

  describe "Deleting an entry" do
    before do
      @user = users(:minimum_end_user)
      @entry = Entry.create!(
        account: accounts(:minimum),
        body: "En krop",
        current_tags: "big benny",
        forum: forums(:agents),
        submitter: users(:minimum_admin),
        title: "En titel"
      ) do |entry|
        entry.current_user = users(:minimum_admin)
      end
      @vote = Vote.create!(entry: @entry, user: @user)
    end

    it "deletes votes as well" do
      @entry.destroy
      assert_nil Vote.find_by_id(@vote.id)
    end
  end

  describe 'Soft deleting an entry' do
    before do
      entry  = entries(:ponies)
      @forum = entry.forum

      entry.soft_delete!
    end

    it 'resets the forums cached counter' do
      @forum.reload
      assert_equal(@forum.entries.count(:all), @forum.entries_count)
    end
  end

  describe "Stores search phrases individually as records" do
    subject { entries(:ponies) }

    describe "#search_phrases=" do
      before do
        subject.search_phrases = ["red", "yellow"]
      end

      it 'sets phrases on an existing entry on the domain' do
        assert_equal 2, EntrySearchPhrase.count(:all)
      end

      it 'sets phrases on an existing entry through the association' do
        assert_equal 2, subject.reload.entry_search_phrases.count(:all)
      end
    end

    describe "#search_phrase=blank" do
      before do
        subject.search_phrases = ["red", "yellow"]
      end

      it 'returns empty array' do
        subject.search_phrases = nil
        assert_equal [], subject.search_phrases
      end
    end

    describe "#content_mapping" do
      it "returns the content mapping of the entry" do
        entry = entries(:fish)

        mapping = ContentMapping.create! do |cm|
          cm.account_id = entry.account_id
          cm.classic_object_type = "Legacy::Entry"
          cm.classic_object_id = entry.id
          cm.hc_object_type = "Foo"
          cm.hc_object_id = 42
          cm.url = "/foo/bar/baz"
        end

        assert_equal mapping, entry.content_mapping
      end
    end

    describe '#search_phrases' do
      before do
        subject.search_phrases = ["red", "yellow"]
      end

      it 'returns list of phrases' do
        assert_equal ["red", "yellow"], subject.search_phrases
      end

      it 'returns an empty list' do
        subject.entry_search_phrases.destroy_all
        assert_equal [], subject.search_phrases
      end
    end

    it 'sets phrases on a new entry' do
      assert_difference('EntrySearchPhrase.count(:all)', 2) do
        Entry.create!(
          account: accounts(:minimum),
          body: "En krop",
          forum: forums(:agents),
          title: "En titel",
          search_phrases: ["This is a phrase", "This is another phrase"]
        ) do |e|
          e.current_user = users(:minimum_admin)
        end
      end
    end

    it 'creates a new entry even if search_phrases is empty' do
      Entry.create!(
        account: accounts(:minimum),
        body: "En krop",
        forum: forums(:agents),
        title: "En titel",
        search_phrases: ""
      ) do |e|
        e.current_user = users(:minimum_admin)
      end
      assert_equal 0, EntrySearchPhrase.count(:all)
    end

    it 'removes all search phrases from existing entry if search_phrases = ""' do
      entry = Entry.create!(
        account: accounts(:minimum),
        body: "En krop",
        forum: forums(:agents),
        title: "En titel",
        search_phrases: ["This is a phrase", "This is another phrase"]
      ) do |e|
        e.current_user = users(:minimum_admin)
      end
      entry.update_attributes(search_phrases: "")
      assert_equal 0, EntrySearchPhrase.count(:all)
    end
  end

  it "uses Zendesk::ForumSanitizer" do
    assert Entry.include?(Zendesk::ForumSanitizer)
  end

  describe "with entry moderation" do
    let(:account) { accounts(:minimum) }

    let(:forum) do
      account.forums.first.tap do |forum|
        Forum.reset_counters(forum.id, :entries)
      end
    end

    let(:user) do
      users(:minimum_end_user).tap do |user|
        user.stubs(whitelisted_from_moderation?: false)
      end
    end

    let(:valid_attrs) { { title: "test", text: "test", account: account } }

    before do
      account.settings.moderated_entries = true
      account.settings.moderated_posts = true
      account.settings.save!
    end

    describe "on creation" do
      let(:entry) { forum.entries.create!(valid_attrs) { |e| e.current_user = user } }

      describe "with an end user" do
        before { entry }

        before_should "not call store_event" do
          ForumObserver.expects(:store_event).never
        end

        it "is deleted" do
          assert entry.deleted?
        end

        it "is suspended?" do
          assert entry.suspended?
        end

        it "has a correct entries_count" do
          forum.reload
          assert_equal forum.entries.count(:all), forum.entries_count
        end
      end

      describe "with a throttled end user" do
        let(:user) do
          users(:minimum_end_user).tap do |user|
            user.stubs(throttled_posting?: true)
          end
        end

        it "is invalid" do
          assert_raise(ActiveRecord::RecordInvalid) { entry }
        end
      end

      describe "with a whitelisted end user" do
        let(:user) do
          users(:minimum_end_user).tap do |user|
            user.stubs(whitelisted_from_moderation?: true)
          end
        end

        before { entry }

        before_should "call store_event" do
          ForumObserver.expects(:store_event).at_least_once
        end

        it "does not be deleted" do
          refute entry.deleted?
        end

        it "does not be suspended?" do
          refute entry.suspended?
        end

        it "has a correct entries_count" do
          forum.reload
          assert_equal forum.entries.count(:all), forum.entries_count
        end
      end

      describe "with an entry from a ticket" do
        let(:valid_attrs) { { title: "test", text: "test", account: account, from_ticket: 'true' } }
        before { entry }

        before_should "call store_event" do
          ForumObserver.expects(:store_event).at_least_once
        end

        it "does not be deleted" do
          refute entry.deleted?
        end

        it "does not be suspended?" do
          refute entry.suspended?
        end

        it "has a correct entries_count" do
          forum.reload
          assert_equal forum.entries.count(:all), forum.entries_count
        end
      end

      describe "with an agent" do
        let(:user) { users(:minimum_agent) }

        before { entry }

        before_should "call store_event" do
          ForumObserver.expects(:store_event).at_least_once
        end

        it "does not be deleted" do
          refute entry.deleted?
        end

        it "does not be suspended?" do
          refute entry.suspended?
        end

        it "has a correct entries_count" do
          forum.reload
          assert_equal forum.entries.count(:all), forum.entries_count
        end
      end
    end

    describe "on update" do
      let(:entry) { entries(:ponies) }

      before do
        entry.text = "new body"
        entry.save!
      end

      it "does not be deleted" do
        refute entry.deleted?
      end

      it "does not be suspended?" do
        refute entry.suspended?
      end
    end

    describe "on suspend" do
      let(:entry) { entries(:ponies) }
      let(:post) { posts(:ponies_post_1) }

      before do
        post.user = entry.submitter
        post.save!

        entry.soft_delete!
      end

      it "has a correct entries_count" do
        forum.reload
        assert_equal forum.entries.count(:all), forum.entries_count
      end

      it "is deleted" do
        assert entry.deleted?
      end

      it "does not be suspended?" do
        refute entry.suspended?
      end

      it "is findable" do
        Entry.with_deleted do
          assert_equal entry, Entry.find(entry.id)
        end
      end
    end

    describe "on unsuspend" do
      let(:entry) { forum.entries.create!(valid_attrs) { |e| e.current_user = user } }

      before do
        entry.unsuspend!
      end

      before_should "call store_event" do
        ForumObserver.expects(:store_event).with(instance_of(Entry)).once
        ForumObserver.expects(:store_event).with(instance_of(Watching)).once
      end

      it "has a correct entries_count" do
        forum.reload
        assert_equal forum.entries.count(:all), forum.entries_count
      end

      it "does not be deleted" do
        refute entry.deleted?
      end

      it "does not be suspended" do
        refute entry.suspended?
      end
    end

    describe "on deletion" do
      let(:entry) { forum.entries.create!(valid_attrs) { |e| e.current_user = user } }

      before do
        entry.soft_delete!
      end

      it "has a correct entries_count" do
        forum.reload
        assert_equal forum.entries.count(:all), forum.entries_count
      end

      it "is deleted" do
        assert entry.deleted?
      end

      it "does not be suspended" do
        refute entry.suspended?
      end
    end

    describe "on deletion with posts" do
      let(:entry) { forum.entries.create!(valid_attrs) { |e| e.current_user = user } }
      let(:post_creation_time) { Time.local(2007, 1, 1, 10, 5, 0) }

      let(:post) do
        Entry.with_deleted do
          Timecop.travel(post_creation_time) do
            entry.posts.create!(body: "test", user: user)
          end
        end
      end

      before do
        post
      end

      it "has a suspended post" do
        assert post.suspended?
      end

      it "has a deleted post" do
        assert post.deleted?
      end

      describe "after soft_deletion!" do
        before do
          entry.soft_delete!
          post.reload
        end

        it "is deleted" do
          assert entry.deleted?
        end

        it "does not be suspended?" do
          refute entry.suspended?
        end

        it "is findable" do
          Entry.with_deleted do
            assert_equal entry, Entry.find(entry.id)
          end
        end

        it "has a deleted post" do
          assert post.deleted?
        end

        it "does not be a suspended post" do
          refute post.suspended?
        end

        it "updates updated_at on post" do
          assert post.updated_at != post_creation_time
        end
      end

      describe "check un-suspension due to entry soft delete affecting posts on other forums" do
        let(:other_forum) { account.forums.last }
        let(:other_entry) { other_forum.entries.create!(valid_attrs) { |e| e.current_user = user } }
        let(:other_post) do
          Entry.with_deleted do
            other_entry.posts.create!(body: "test", user: user)
          end
        end

        before do
          post
          other_post
        end

        it "does not be the same forum" do
          assert other_forum != forum
        end

        it "does not be the same entry" do
          assert other_entry != entry
        end

        it "boths have suspended posts" do
          assert post.suspended?
          assert other_post.suspended?
        end

        it "boths have deleted posts" do
          assert post.deleted?
          assert other_post.deleted?
        end

        it "leaves untouched suspended post on other forum" do
          entry.soft_delete!
          other_post.reload

          assert other_post.suspended?
        end
      end

      describe "check un-suspension due to entry soft delete affecting posts on same forum" do
        let(:other_entry) { forum.entries.create!(valid_attrs) { |e| e.current_user = user } }
        let(:other_post) do
          Entry.with_deleted do
            other_entry.posts.create!(body: "test", user: user)
          end
        end

        before do
          post
          other_post
        end

        it "does not be the same entry" do
          assert other_entry != entry
        end

        it "boths have suspended posts" do
          assert post.suspended?
          assert other_post.suspended?
        end

        it "boths have deleted posts" do
          assert post.deleted?
          assert other_post.deleted?
        end

        it "leaves untouched suspended post on other entry on same forum" do
          entry.soft_delete!
          other_post.reload

          assert other_post.suspended?
        end
      end
    end
  end
end
