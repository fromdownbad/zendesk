require_relative "../support/test_helper"

SingleCov.covered!

describe 'TicketFieldCondition' do
  fixtures :accounts, :tickets, :ticket_fields, :ticket_field_entries, :custom_field_options, :ticket_forms

  should_validate_presence_of :account, :ticket_form, :parent_field, :child_field, :value, :user_type

  let(:tfc) { FactoryBot.build(:ticket_field_condition) }

  it "is saveable with a factory" do
    assert FactoryBot.build(:ticket_field_condition).save
  end

  describe "scopes" do
    before(:each) { TicketFieldCondition.destroy_all }
    describe "#for_agents" do
      it "should scope on the agent user type" do
        agent_conditions = FactoryBot.create_list(:ticket_field_condition, 2, :agent)
        _end_user_conditions = FactoryBot.create_list(:ticket_field_condition, 2, :end_user)

        assert_equal TicketFieldCondition.for_agents.all.sort, agent_conditions.sort
      end
    end

    describe "#for_end users" do
      it "should scope on the end_user user type" do
        _agent_conditions = FactoryBot.create_list(:ticket_field_condition, 2, :agent)
        end_user_conditions = FactoryBot.create_list(:ticket_field_condition, 2, :end_user)

        assert_equal TicketFieldCondition.for_end_users.all.sort, end_user_conditions.sort
      end
    end

    describe "#by_ticket_field" do
      let(:condition) { FactoryBot.create(:ticket_field_condition) }

      it "should return an empty set when the field doesn't have any conditions" do
        assert_equal TicketFieldCondition.by_ticket_field(FactoryBot.create(:integer_ticket_field)).to_a, []
      end

      it "should return conditions where the field is a parent" do
        field = condition.parent_field

        assert_includes TicketFieldCondition.by_ticket_field(field), condition
      end

      it "should return conditions where the field is a child" do
        field = condition.child_field

        assert_includes TicketFieldCondition.by_ticket_field(field), condition
      end
    end
  end

  describe "#create" do
    it "creates a condition" do
      assert FactoryBot.create(:ticket_field_condition)
    end
  end

  describe "#is_required" do
    let(:account) { accounts(:minimum) }
    let(:ticket_form) { ticket_forms(:minimum_ticket_form) }
    let(:parent_field) { ticket_fields(:field_tagger_custom) }
    let(:child_field) { ticket_fields(:field_textarea_custom) }
    let(:cfo1) { custom_field_options(:field_tagger_custom_option_2) }
    let(:tfc) { FactoryBot.create(:ticket_field_condition, account_id: account.id) }

    before do
      ticket_form.account_id = account.id
      TicketFormField.create(
        ticket_form: ticket_form,
        account: account,
        ticket_field: parent_field,
        position: 1
      )
      TicketFormField.create(
        ticket_form: ticket_form,
        account: account,
        ticket_field: child_field,
        position: 2
      )
    end

    describe "when user_type is agent" do
      it "syncs to the value of required_on_statuses" do
        child_field.is_required = true # misdirection
        child_field.is_required_in_portal = true # misdirection

        condition = TicketFieldCondition.new
        condition.account = account
        condition.ticket_form = ticket_form
        condition.parent_field = parent_field
        condition.child_field = child_field
        condition.value = cfo1.value
        condition.user_type = :agent

        condition.is_required = true # misdirection

        assert condition.valid?
        assert condition.save
        refute condition.is_required?
        assert condition.required_on_statuses.blank?
        assert condition.not_required_on_any_statuses?
        condition.destroy

        child_field.is_required = false # misdirection
        child_field.is_required_in_portal = false # misdirection

        condition = TicketFieldCondition.new
        condition.account = account
        condition.ticket_form = ticket_form
        condition.parent_field = parent_field
        condition.child_field = child_field
        condition.value = cfo1.value
        condition.user_type = :agent

        condition.is_required = false # misdirection
        condition.shaped_required_on_statuses = {
          type: TicketFieldCondition::REQUIRED_ON_SOME_STATUSES,
          statuses: ["SOLVED"]
        }

        assert condition.valid?
        assert condition.is_required?
        assert_equal StatusType.SOLVED.to_s, condition.required_on_statuses
      end
    end

    describe "when user_type is end_user" do
      it "defaults to the child_field's is_required_in_portal value" do
        child_field.is_required_in_portal = true
        child_field.is_required = false # misdirection

        condition = TicketFieldCondition.new
        condition.account = account
        condition.ticket_form = ticket_form
        condition.parent_field = parent_field
        condition.child_field = child_field
        condition.value = cfo1.value
        condition.user_type = :end_user

        assert condition.save
        assert condition.is_required?
        condition.destroy

        child_field.is_required_in_portal = false
        child_field.is_required = true # misdirection

        condition = TicketFieldCondition.new
        condition.account = account
        condition.ticket_form = ticket_form
        condition.parent_field = parent_field
        condition.child_field = child_field
        condition.value = cfo1.value
        condition.user_type = :end_user

        assert condition.valid?
        assert !condition.is_required?
      end

      it "can be explicitly set" do
        child_field.is_required_in_portal = true

        condition = TicketFieldCondition.new
        condition.account = account
        condition.ticket_form = ticket_form
        condition.parent_field = parent_field
        condition.child_field = child_field
        condition.value = cfo1.value
        condition.user_type = :end_user
        condition.is_required = false

        assert condition.save
        assert !condition.is_required?
        condition.destroy

        child_field.is_required = false

        condition = TicketFieldCondition.new
        condition.account = account
        condition.ticket_form = ticket_form
        condition.parent_field = parent_field
        condition.child_field = child_field
        condition.value = cfo1.value
        condition.user_type = :end_user
        condition.is_required = true

        assert condition.valid?
        assert condition.is_required?
      end
    end
  end

  describe "#keep_conditional_requirement_values_in_sync" do
    before { tfc.user_type = :agent }

    it "sets is_required to true when required_on_statuses is ALL_STATUSES" do
      tfc.is_required = false
      tfc.required_on_statuses = TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
      tfc.save

      assert tfc.is_required?
      assert_equal TicketFieldCondition::REQUIRED_ON_ALL_STATUSES, tfc.required_on_statuses
    end

    it "sets is_required to true when required_on_statuses includes solved" do
      tfc.is_required = false
      tfc.required_on_statuses = "#{StatusType.OPEN} #{StatusType.SOLVED}"
      tfc.save

      assert tfc.is_required?
      assert_equal "#{StatusType.OPEN} #{StatusType.SOLVED}", tfc.required_on_statuses
    end

    it "sets is_required to false when required_on_statuses doesn't include solved" do
      tfc.is_required = true
      tfc.required_on_statuses = "#{StatusType.NEW} #{StatusType.OPEN}"
      tfc.save

      refute tfc.is_required?
      assert_equal "#{StatusType.NEW} #{StatusType.OPEN}", tfc.required_on_statuses
    end

    it "sets is_required to false when required_on_statuses is blank" do
      tfc.is_required = true
      tfc.required_on_statuses = nil
      tfc.save

      refute tfc.is_required?
      assert_nil tfc.required_on_statuses

      tfc.is_required = true
      tfc.required_on_statuses = ''
      tfc.save

      refute tfc.is_required?
      assert_equal '', tfc.required_on_statuses
    end

    it "doesn't change is_required on end-user conditions" do
      tfc.user_type = :end_user
      tfc.is_required = false
      tfc.required_on_statuses = TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
      tfc.save

      refute tfc.is_required?
      assert_equal TicketFieldCondition::REQUIRED_ON_ALL_STATUSES, tfc.required_on_statuses
    end
  end

  describe "#end_user?" do
    let(:condition) { FactoryBot.create(:ticket_field_condition) }

    it "returns true for end-user conditions" do
      condition.user_type = :end_user
      assert condition.end_user?
    end

    it "returns false for agent conditions" do
      condition.user_type = :agent
      refute condition.end_user?
    end
  end

  describe "#agent?" do
    let(:condition) { FactoryBot.create(:ticket_field_condition) }

    it "returns false for end-user conditions" do
      condition.user_type = :end_user
      refute condition.agent?
    end

    it "returns true for agent conditions" do
      condition.user_type = :agent
      assert condition.agent?
    end
  end

  describe "#required_for?" do
    let(:condition) { FactoryBot.create(:ticket_field_condition) }
    let(:ticket) { tickets(:minimum_2) }

    describe "for end-user conditions" do
      before do
        condition.user_type = :end_user
        condition.is_required = true
      end

      it "returns is_required without checking required_on_statuses or the ticket status" do
        condition.expects(:required_on_statuses).never
        ticket.expects(:status_id).never
        assert condition.required_for?(ticket)
      end
    end

    describe "for agent conditions" do
      before { condition.user_type = :agent }

      describe "and the condition is required on all statuses" do
        before do
          condition.stubs(:required_on_statuses).returns(TicketFieldCondition::REQUIRED_ON_ALL_STATUSES)
          ticket.status_id = StatusType.PENDING
        end

        it "returns true" do
          assert condition.required_for?(ticket)
        end

        describe "and the ticket is not operable" do
          before { ticket.status_id = StatusType.CLOSED }

          it "returns false" do
            refute condition.required_for?(ticket)
          end
        end
      end

      describe "and the condition is required on specific statuses" do
        before do
          condition.stubs(:required_on_statuses).returns("#{StatusType.NEW} #{StatusType.OPEN}")
          ticket.status_id = StatusType.PENDING
        end

        describe "and the ticket is in one of the required statuses" do
          before { ticket.status_id = StatusType.OPEN }

          it "returns true" do
            assert condition.required_for?(ticket)
          end
        end

        describe "and the ticket is not in one of the required statuses" do
          before { ticket.status_id = StatusType.PENDING }

          it "returns false" do
            refute condition.required_for?(ticket)
          end
        end
      end

      describe "and required_on_statuses is nil" do
        before do
          condition.stubs(:required_on_statuses).returns(nil)
          ticket.status_id = StatusType.PENDING
        end

        it "returns false" do
          refute condition.required_for?(ticket)
        end
      end
    end
  end

  describe "#user_type" do
    it "returns a string instead of a numeric value on get" do
      assert_equal 'agent', tfc.user_type
      tfc.user_type = :end_user
      tfc.save!
      assert_equal 'end_user', tfc.user_type
    end
  end

  describe "#shaped_required_on_statuses" do
    it "shapes 'ALL_STATUSES'" do
      tfc.required_on_statuses = TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
      expected = {
        type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
      }
      assert_equal expected, tfc.shaped_required_on_statuses
    end

    it "shapes 'SOME_STATUSES'" do
      tfc.required_on_statuses = '0 1'
      expected = {
        type: TicketFieldCondition::REQUIRED_ON_SOME_STATUSES,
        statuses: ['new', 'open']
      }
      assert_equal expected, tfc.shaped_required_on_statuses
    end

    it "shapes 'NO_STATUSES'" do
      tfc.required_on_statuses = nil
      expected = {
        type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES
      }
      assert_equal expected, tfc.shaped_required_on_statuses
      tfc.required_on_statuses = ''
      assert_equal expected, tfc.shaped_required_on_statuses
    end

    describe "when account has hold status enabled" do
      before { tfc.account.stubs(:use_status_hold?).returns(true) }

      it "does not filter on_hold status" do
        tfc.required_on_statuses = '0 1 6'
        expected = {
          type: TicketFieldCondition::REQUIRED_ON_SOME_STATUSES,
          statuses: ['new', 'open', 'hold']
        }
        assert_equal expected, tfc.shaped_required_on_statuses
      end
    end

    describe "when account has hold status disabled" do
      before { tfc.account.stubs(:use_status_hold?).returns(false) }

      it "filters on_hold status" do
        tfc.required_on_statuses = '0 1 6'
        expected = {
          type: TicketFieldCondition::REQUIRED_ON_SOME_STATUSES,
          statuses: ['new', 'open']
        }
        assert_equal expected, tfc.shaped_required_on_statuses
      end

      it "returns REQUIRED_ON_NO_STATUSES when on_hold is the only required status" do
        tfc.required_on_statuses = '6'
        expected = {
          type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES
        }
        assert_equal expected, tfc.shaped_required_on_statuses
      end
    end
  end

  describe "#shaped_required_on_statuses=" do
    before do
      Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(true)
    end

    it "parses 'ALL_STATUSES'" do
      tfc.shaped_required_on_statuses = {
        type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
      }
      assert_equal TicketFieldCondition::REQUIRED_ON_ALL_STATUSES, tfc.required_on_statuses
    end

    it "parses 'SOME_STATUSES' and converts the values" do
      tfc.shaped_required_on_statuses = {
        type: TicketFieldCondition::REQUIRED_ON_SOME_STATUSES,
        statuses: ['new', 'open']
      }
      assert_equal '0 1', tfc.required_on_statuses
    end

    it "handles 'SOME_STATUSES' with no 'statuses' or empty array" do
      tfc.shaped_required_on_statuses = {
        type: TicketFieldCondition::REQUIRED_ON_SOME_STATUSES
      }
      assert tfc.not_required_on_any_statuses?

      tfc.shaped_required_on_statuses = {
        type: TicketFieldCondition::REQUIRED_ON_SOME_STATUSES,
        statuses: []
      }
      assert tfc.not_required_on_any_statuses?
    end

    it "lets invalid statuses go through to make the model validations catch them" do
      tfc.shaped_required_on_statuses = {
        type: TicketFieldCondition::REQUIRED_ON_SOME_STATUSES,
        statuses: ['new', 'open', 'invalid']
      }

      assert_equal '0 1 invalid', tfc.required_on_statuses
      refute tfc.valid?
      assert_equal tfc.errors[:child_field].first, "Invalid `required_on_statuses` on child field '#{tfc.child_field.title}'."
    end

    it "parses 'NO_STATUSES'" do
      tfc.shaped_required_on_statuses = {
        type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES
      }
      assert_nil tfc.required_on_statuses
    end

    it "lets invalid types go through to make the model validations catch them" do
      tfc.shaped_required_on_statuses = {
        type: 'invalid'
      }

      refute tfc.valid?
      assert_equal tfc.errors[:child_field].first, "Invalid `required_on_statuses` on child field '#{tfc.child_field.title}'."
    end

    it "lets set required_on_statuses on end-user conditions, the model validations will catch that" do
      tfc.user_type = :end_user
      tfc.shaped_required_on_statuses = {
        type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
      }

      refute tfc.valid?
      assert_equal tfc.errors[:child_field].first, "Invalid use of `required_on_statuses` on child field '#{tfc.child_field.title}'. `required_on_statuses` is only allowed on conditions for agents."
    end
  end

  describe "#mark_account_as_migrated" do
    let(:account) { accounts(:minimum) }
    let(:ticket_form) { ticket_forms(:minimum_ticket_form) }
    let(:parent_field) { ticket_fields(:field_text_custom) }
    let(:child_field) { ticket_fields(:field_textarea_custom) }

    before do
      ticket_form.account_id = account.id
    end

    it "marks account as migrated when a new condition is created" do
      refute account.settings.native_conditional_fields_migrated?
      FactoryBot.create(:ticket_field_condition,
        account: account,
        ticket_form: ticket_form,
        parent_field: parent_field,
        child_field: child_field);
      assert account.reload.settings.native_conditional_fields_migrated?
    end

    it "doesn't mark account as migrated when the new condition is created during migration" do
      refute account.settings.native_conditional_fields_migrated?
      ticket_field_condition = FactoryBot.build(:ticket_field_condition,
        account: account,
        ticket_form: ticket_form,
        parent_field: parent_field,
        child_field: child_field);
      ticket_field_condition.set_migration_in_progress
      ticket_field_condition.save!
      refute account.reload.settings.native_conditional_fields_migrated?
    end
  end

  describe "#user_type=" do
    it "raises an exception if given an invalid numeric value" do
      assert_raises (ArgumentError) { tfc.user_type = 99991 }
    end

    it "raises an exception if given an invalid string value" do
      assert_raises (ArgumentError) { tfc.user_type = 'megaman' }
    end

    it "converts string to its numeric equivalent in the db" do
      tfc.user_type = :end_user
      tfc.save!
      assert_equal 1, tfc.read_attribute(:user_type)
    end

    it "converts symbol to its numeric equivalent in the db" do
      tfc.user_type = :end_user
      tfc.save!
      assert_equal 1, tfc.read_attribute(:user_type)
    end

    it "returns the string form from the setter method when setting the value" do
      ret = (tfc.user_type = 'end_user')
      assert_equal 'end_user', ret
    end

    it "returns the symbol from the setter method when setting the symbol" do
      ret = (tfc.user_type = :end_user)
      assert_equal :end_user, ret
    end
  end

  describe "#value=" do
    describe "for checkbox fields" do
      let(:tfc) { FactoryBot.build(:ticket_field_condition, :checkbox_parent) }

      it "sets the value to '1' when sent 'yes'" do
        tfc.value = "yes"
        assert_equal tfc.value, "1"
        assert tfc.valid?
      end

      it "sets the value to '1' when sent 'Yes'" do
        tfc.value = "Yes"
        assert_equal tfc.value, "1"
        assert tfc.valid?
      end

      it "sets the value to '0' for 'no'" do
        tfc.value = "no"
        assert_equal tfc.value, "0"
        assert tfc.valid?
      end

      it "sets the value to '0' for 'No'" do
        tfc.value = "No"
        assert_equal tfc.value, "0"
        assert tfc.valid?
      end

      it "sets the value to '0' when not given a truthy value" do
        tfc.value = "pikachu"
        assert_equal tfc.value, "0"
        assert tfc.valid?
      end

      it "sets the value to '0' when sent 'nil'" do
        tfc.value = nil
        assert_equal tfc.value, "0"
        assert tfc.valid?
      end
    end

    describe "for priority fields" do
      let(:tfc) { FactoryBot.build(:ticket_field_condition, :priority_parent) }

      it "makes the condition not valid when sent '-'" do
        tfc.value = "-"
        assert_equal tfc.value, "0"
        refute tfc.valid?
      end

      it "sets the value to '1' when sent 'low'" do
        tfc.value = "low"
        assert_equal tfc.value, "1"
        assert tfc.valid?
      end

      it "sets the value to '2' when sent 'normal'" do
        tfc.value = "normal"
        assert_equal tfc.value, "2"
        assert tfc.valid?
      end

      it "sets the value to '3' for 'high'" do
        tfc.value = "high"
        assert_equal tfc.value, "3"
        assert tfc.valid?
      end

      it "sets the value to '4' for 'urgent'" do
        tfc.value = "urgent"
        assert_equal tfc.value, "4"
        assert tfc.valid?
      end

      it "makes the condition not valid when not given a valid priority value" do
        tfc.value = "pikachu"
        assert_equal tfc.value, "pikachu"
        refute tfc.valid?
      end

      it "makes the condition not valid when sent 'nil'" do
        tfc.value = nil
        assert_equal tfc.value, ""
        refute tfc.valid?
      end

      describe "when it's configured as basic in the account" do
        before do
          tfc.account.field_priority.stubs(:sub_type_id).returns(PrioritySet.BASIC)
        end

        it "makes the condition not valid when sent '-'" do
          tfc.value = "-"
          assert_equal tfc.value, "0"
          refute tfc.valid?
        end

        it "makes the condition not valid when sent 'low'" do
          tfc.value = "low"
          assert_equal tfc.value, "1"
          refute tfc.valid?
        end

        it "sets the value to '2' when sent 'normal'" do
          tfc.value = "normal"
          assert_equal tfc.value, "2"
          assert tfc.valid?
        end

        it "sets the value to '3' for 'high'" do
          tfc.value = "high"
          assert_equal tfc.value, "3"
          assert tfc.valid?
        end

        it "makes the condition not valid when sent 'urgent'" do
          tfc.value = "urgent"
          assert_equal tfc.value, "4"
          refute tfc.valid?
        end

        it "makes the condition not valid when not given a valid priority value" do
          tfc.value = "pikachu"
          assert_equal tfc.value, "pikachu"
          refute tfc.valid?
        end

        it "makes the condition not valid when sent 'nil'" do
          tfc.value = nil
          assert_equal tfc.value, ""
          refute tfc.valid?
        end
      end
    end

    describe "for ticket type fields" do
      let(:tfc) { FactoryBot.build(:ticket_field_condition, :ticket_type_parent) }

      it "makes the condition not valid when sent '-'" do
        tfc.value = "-"
        assert_equal tfc.value, "0"
        refute tfc.valid?
      end

      it "sets the value to '1' when sent 'question'" do
        tfc.value = "question"
        assert_equal tfc.value, "1"
        assert tfc.valid?
      end

      it "sets the value to '2' when sent 'incident'" do
        tfc.value = "incident"
        assert_equal tfc.value, "2"
        assert tfc.valid?
      end

      it "sets the value to '3' for 'problem'" do
        tfc.value = "problem"
        assert_equal tfc.value, "3"
        assert tfc.valid?
      end

      it "sets the value to '4' for 'task'" do
        tfc.value = "task"
        assert_equal tfc.value, "4"
        assert tfc.valid?
      end

      it "makes the condition not valid when not given a valid ticket type value" do
        tfc.value = "pikachu"
        assert_equal tfc.value, "pikachu"
        refute tfc.valid?
      end

      it "makes the condition not valid when sent 'nil'" do
        tfc.value = nil
        assert_equal tfc.value, ""
        refute tfc.valid?
      end
    end

    describe "when parent field is nil" do
      it "should set the value to whatever was passed" do
        tfc.stubs(:parent_field).returns(nil)
        tfc.value = new_value = "conditionz rulz"
        assert_equal tfc.value, new_value
      end
    end

    describe "for non special fields" do
      it "should set the value to whatever was passed" do
        tfc.value = new_value = "conditionz rulz"
        assert_equal tfc.value, new_value
      end
    end
  end

  describe "#validations" do
    let(:account) { accounts(:minimum) }
    let(:tfc) { FactoryBot.build(:ticket_field_condition, account_id: account.id) }

    describe "when adding a condition with a parent_field and/or child field not on the ticket form" do
      it "should fail with only one error that form is invalid" do
        account = accounts(:support)
        pf = FactoryBot.create(:field_tagger, account: account)
        cf = account.ticket_fields.find_by_type("FieldPriority")
        params = {
          ticket_form: {
            name: "Wombat",
            display_name: "Wombats are everywhere",
            position: 2,
            default: false,
            active: true,
            end_user_visible: true,
            in_all_brands: true,
            ticket_field_ids: [cf.id],
          }
        }
        ticket_form = Zendesk::TicketForms::Initializer.new(account, params).ticket_form
        ticket_form.save!

        assert_equal false, ticket_form.ticket_fields.include?(pf)

        ticket_form.agent_conditions = [parent_field_id: pf.id, child_fields: [{id: cf.id, is_required: false}], value: pf.custom_field_options.first.value]
        assert_equal false, ticket_form.save
        assert_equal ["is invalid"], ticket_form.errors.messages[:"ticket_field_conditions.parent_field"]
        assert_equal 1, ticket_form.errors.messages[:"ticket_field_conditions.parent_field"].count
      end
    end

    describe "uniqueness of a condition set" do
      it "should not mark a condition as duplicative of itself" do
        tfc = FactoryBot.create(:ticket_field_condition, :checkbox_parent)

        assert tfc.valid?
      end

      it "should mark invalid a condition with duplicate condition set for the form and account" do
        tfc = FactoryBot.create(:ticket_field_condition, :checkbox_parent)

        dup_tfc = tfc.dup

        refute dup_tfc.valid?

        error_string = I18n.t(
          "txt.error_message.models.ticket_field_condition.condition_set_already_exists",
          parent_name: dup_tfc.parent_field.title,
          child_name: dup_tfc.child_field.title,
          user_type: dup_tfc.user_type,
          value: dup_tfc.value
        )
        assert_includes dup_tfc.errors[:value], error_string
      end

      it "should mark as invalid when a migration is in progress" do
        tfc = FactoryBot.create(:ticket_field_condition, :checkbox_parent)

        dup_tfc = tfc.dup
        dup_tfc.set_migration_in_progress

        refute dup_tfc.valid?
      end

      it "should not mark as invalid for different user types" do
        tfc = FactoryBot.create(:ticket_field_condition, :checkbox_parent)

        dup_tfc = tfc.dup
        dup_tfc.user_type = :end_user

        assert dup_tfc.valid?
      end
    end

    describe "value for checkbox parent" do
      let(:tfc) { FactoryBot.build(:ticket_field_condition, :checkbox_parent) }

      it "should allow 1 or 0 as values" do
        tfc.value = "1"
        assert_valid tfc

        tfc.value = "0"
        assert_valid tfc
      end

      it "should not allow values other than 1 or 0" do
        tfc[:value] = "reggie"
        refute tfc.valid?
        assert_includes tfc.errors[:value], "'#{tfc.value}' is not a value of the parent field."
      end
    end

    describe "value for priority parent" do
      let(:tfc) { FactoryBot.build(:ticket_field_condition, :priority_parent) }

      it "should allow all priority possible values except '-'" do
        (PriorityType.fields - [PriorityType.-]).map(&:to_s).each do |value|
          tfc.value = value
          assert_valid tfc
        end
      end

      it "should not allow invalid priority values" do
        tfc[:value] = "reggie"
        refute tfc.valid?
        assert_includes tfc.errors[:value], "'#{tfc.value}' is not a value of the parent field."
      end
    end

    describe "value for ticket type parent" do
      let(:tfc) { FactoryBot.build(:ticket_field_condition, :ticket_type_parent) }

      it "should allow all ticket type possible values except '-'" do
        (TicketType.fields - [TicketType.-]).map(&:to_s).each do |value|
          tfc.value = value
          assert_valid tfc
        end
      end

      it "should not allow invalid ticket type values" do
        tfc[:value] = "reggie"
        refute tfc.valid?
        assert_includes tfc.errors[:value], "'#{tfc.value}' is not a value of the parent field."
      end
    end

    describe "#option_belongs_to_tagger" do
      it "raises record invalid if value is not a custom field option for the parent dropdown field" do
        tfc.parent_field = ticket_fields(:field_tagger_custom)
        assert(tfc.parent_field.is_a?(FieldTagger))

        tfc.value = "not_a_dropdown_value"

        refute tfc.valid?
        assert_includes tfc.errors[:value], "'#{tfc.value}' is not a value of the parent field."
      end

      it "is only called on FieldTagger type fields" do
        text_field = FactoryBot.create(:text_ticket_field)
        TicketFieldCondition.any_instance.expects(:option_belongs_to_tagger).never
        FactoryBot.create(:ticket_field_condition, parent_field: text_field)
      end
    end

    describe "#clear_unowned_associations" do
      it "should clear the parent field if it belongs to an account different than the condition" do
        tfc.parent_field = FactoryBot.create(:text_ticket_field)

        assert tfc.parent_field
        refute tfc.valid?
        assert_nil tfc.parent_field
      end

      it "should clear the child field if it belongs to an account different than the condition" do
        tfc.parent_field = FactoryBot.create(:text_ticket_field)

        assert tfc.child_field
        refute tfc.valid?
        assert_nil tfc.child_field
      end
    end

    describe "#parent_field_is_allowed_field" do
      it "returns invalid if parent field is not a supported field type" do
        parent_system_field = account.field_group
        TicketFormField.create(
          ticket_form: tfc.ticket_form,
          account: account,
          ticket_field: parent_system_field,
          position: 3
        )
        tfc.parent_field = parent_system_field

        refute tfc.valid?
        assert_equal tfc.errors[:parent_field].length, 1
        assert_equal tfc.errors[:parent_field].first, "'#{tfc.parent_field.title}' is an unsupported parent field type."
      end
    end

    describe "#child_field_is_allowed_field" do
      it "returns invalid if child field is not a supported field type" do
        child_system_field = account.field_group
        TicketFormField.create(
          ticket_form: tfc.ticket_form,
          account: account,
          ticket_field: child_system_field,
          position: 3
        )
        tfc.child_field = child_system_field

        refute tfc.valid?
        assert_equal tfc.errors[:child_field].length, 1
        assert_equal tfc.errors[:child_field].first, "'#{tfc.child_field.title}' is an unsupported child field type."
      end
    end

    describe "#parent_field_is_not_app_field" do
      it "returns invalid if parent is an app field" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.parent_field.stubs(:app_field?).returns(true)

        refute tfc.valid?
        assert_equal tfc.errors[:parent_field].length, 1
        assert_equal tfc.errors[:parent_field].first, "'#{tfc.parent_field.title}' cannot be a field created by an app."
      end
    end

    describe "#parent_child_different" do
      it "returns invalid if same field is given for parent and child field" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.child_field_id = tfc.parent_field_id

        refute tfc.valid?
        assert_equal tfc.errors[:child_field].length, 1
        assert_equal tfc.errors[:child_field].first, "'#{tfc.child_field.title}' cannot be the same as the parent field."
      end
    end

    describe "#parent_field_is_end_user_editable" do
      it "returns invalid if parent field of end-user condition is not end-user editable" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.user_type = :end_user
        tfc.parent_field.update_attributes! is_editable_in_portal: false

        refute tfc.valid?
        assert_equal tfc.errors[:parent_field].length, 1
        assert_equal tfc.errors[:parent_field].first, "'#{tfc.parent_field.title}' is not end-user editable."
      end

      it "doesn't cause errors if parent field is not present" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.user_type = :end_user
        tfc.parent_field = nil

        refute tfc.valid?
      end
    end

    describe "#child_field_is_end_user_editable" do
      it "returns invalid if child field of end-user condition is not end-user editable" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.user_type = :end_user
        tfc.child_field.update_attributes! is_editable_in_portal: false

        refute tfc.valid?
        assert_equal tfc.errors[:child_field].length, 1
        assert_equal tfc.errors[:child_field].first, "'#{tfc.child_field.title}' is not end-user editable."
      end

      it "doesn't cause errors if child field is not present" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.user_type = :end_user
        tfc.child_field = nil

        refute tfc.valid?
      end
    end

    describe "#parent_field_is_active" do
      it "returns invalid if parent field is not active" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.parent_field.update_attribute(:is_active, false)

        refute tfc.valid?
        assert_equal tfc.errors[:parent_field].length, 1
        assert_equal tfc.errors[:parent_field].first, "'#{tfc.parent_field.title}' is not active."
      end
    end

    describe "#child_field_is_active" do
      it "returns invalid if child field is not active" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.child_field.update_attribute(:is_active, false)

        refute tfc.valid?
        assert_equal tfc.errors[:child_field].length, 1
        assert_equal tfc.errors[:child_field].first, "'#{tfc.child_field.title}' is not active."
      end
    end

    it "returns invalid if ticket_form is not valid" do
      tfc.ticket_form = TicketForm.new(name: 'Wombat Form', account: account)
      refute tfc.valid?
      tfc.ticket_form_id = 123
      refute tfc.valid?
    end

    it "returns invalid if the parent_field is not valid" do
      tfc.parent_field = FieldDecimal.new(title: "wombat number", account: account)
      refute tfc.valid?
      tfc.parent_field_id = 123
      refute tfc.valid?
    end

    it "returns invalid if the child_field is not valid" do
      tfc.child_field = FieldDecimal.new(title: "wombat number", account: account)
      refute tfc.valid?
      tfc.child_field_id = 123
      refute tfc.valid?
    end

    describe "#only_allow_required_on_statuses_for_agents" do
      let(:tfc) { FactoryBot.create(:ticket_field_condition) }

      it "returns invalid if required_on_statuses is set for non-agent conditions" do
        tfc.user_type = :end_user
        tfc.required_on_statuses = StatusType.PENDING.to_s

        refute tfc.valid?
        assert_equal tfc.errors[:child_field].length, 1
        assert_equal tfc.errors[:child_field].first, "Invalid use of `required_on_statuses` on child field '#{tfc.child_field.title}'. `required_on_statuses` is only allowed on conditions for agents."
      end
    end

    describe "#required_on_statuses_is_valid" do
      let(:tfc) do
        Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(true)
        FactoryBot.create(:ticket_field_condition)
      end

      it "returns invalid if required_on_statuses is set to a string other than REQUIRED_ON_ALL_STATUSES" do
        tfc.required_on_statuses = 'wombats'

        refute tfc.valid?
        assert_equal tfc.errors[:child_field].length, 1
        assert_equal tfc.errors[:child_field].first, "Invalid `required_on_statuses` on child field '#{tfc.child_field.title}'."
      end

      it "returns invalid if required_on_statuses contains a non-operable status ID" do
        tfc.required_on_statuses = "#{StatusType.NEW} #{StatusType.OPEN} #{StatusType.PENDING} #{StatusType.HOLD} #{StatusType.SOLVED} #{StatusType.ARCHIVED}"

        refute tfc.valid?
        assert_equal tfc.errors[:child_field].length, 1
        assert_equal tfc.errors[:child_field].first, "Invalid `required_on_statuses` on child field '#{tfc.child_field.title}'."
      end

      it "returns invalid if required_on_statuses contains duplicate valid status IDs" do
        tfc.required_on_statuses = "#{StatusType.NEW} #{StatusType.OPEN} #{StatusType.NEW}"

        refute tfc.valid?
        assert_equal tfc.errors[:child_field].length, 1
        assert_equal tfc.errors[:child_field].first, "Invalid `required_on_statuses` on child field '#{tfc.child_field.title}'."
      end
    end

    # Validation helpers
    describe "#parent_field_name_for_error" do
      it "returns the parent field title if the parent field is present" do
        tfc = FactoryBot.create(:ticket_field_condition)
        assert_equal tfc.send(:parent_field_name_for_error), tfc.parent_field.title
      end

      it "returns the parent field id if the parent field is nil" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.parent_field_id = 'some_invalid_id'
        assert_equal tfc.send(:parent_field_name_for_error), tfc.parent_field_id
      end
    end

    describe "#child_field_name_for_error" do
      it "returns the child field title if the child field is present" do
        tfc = FactoryBot.create(:ticket_field_condition)
        assert_equal tfc.send(:child_field_name_for_error), tfc.child_field.title
      end

      it "returns the child field id if the child field is nil" do
        tfc = FactoryBot.create(:ticket_field_condition)
        tfc.child_field_id = 'some_invalid_id'
        assert_equal tfc.send(:child_field_name_for_error), tfc.child_field_id
      end
    end
  end
end
