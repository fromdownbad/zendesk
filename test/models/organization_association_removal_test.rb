require_relative "../support/test_helper"

SingleCov.covered!

describe OrganizationAssociationRemoval do
  fixtures :users, :accounts, :organizations, :organization_memberships, :tickets

  let(:account) { accounts(:minimum) }
  let(:organization1) { organizations(:minimum_organization1) }
  let(:organization2) { organizations(:minimum_organization2) }

  describe "with multiple_organizations" do
    before { Account.any_instance.stubs(has_multiple_organizations_enabled?: true) }

    let(:organization3) { organizations(:minimum_organization3) }
    let(:user_with_existing_membership) do
      users(:minimum_end_user).tap do |user|
        user.update_attribute(:organizations, [organization1, organization2])
        user.save!
      end
    end

    let(:user_with_other_memberships) do
      users(:minimum_end_user2).tap do |user|
        user.update_attribute(:organizations, [organization3, organization1])
        user.save!
      end
    end

    let(:batch) do
      [
        user_with_existing_membership,
        user_with_other_memberships,
      ]
    end

    let(:updated_at_existing) { user_with_existing_membership.updated_at }
    let(:updated_at_other) { user_with_other_memberships.updated_at }

    let(:all_org_tickets)  do
      Ticket.where.not(organization_id: nil).where(requester_id: batch.map(&:id))
    end
    let(:closed_tickets) { all_org_tickets.select(&:closed?) }

    describe "a regular organization_membership association" do
      before do
        user1, user2 = batch

        tickets(:minimum_1).will_be_saved_by(user1)
        tickets(:minimum_1).update_attribute(:organization_id, organization2.id)
        tickets(:minimum_3).will_be_saved_by(user2)
        tickets(:minimum_3).update_attribute(:requester_id, user2.id)

        tickets(:minimum_4).will_be_saved_by(user2)
        tickets(:minimum_4).update_attributes!(
          requester_id: user2.id,
          organization_id: organization3.id
        )

        all_org_tickets.each { |ticket| assert ticket.reload.organization_id }
        assert_equal organization1.id, tickets(:minimum_3).reload.organization_id
        assert_equal organization3.id, tickets(:minimum_4).reload.organization_id

        OrganizationAssociationRemoval.new(
          account.id, organization1.id, batch.map(&:id)
        ).call
      end

      it "changes the updated_at value for the user" do
        assert_not_equal updated_at_existing, user_with_existing_membership.reload.updated_at
        assert_not_equal updated_at_other, user_with_other_memberships.reload.updated_at
      end

      it "does not change the user's organization_id" do
        assert_equal organization3.id, user_with_other_memberships.reload.organization_id
      end

      it "deletes only the organization_membership for the organization" do
        assert_equal(
          [organization3.id],
          user_with_other_memberships.reload.organization_memberships.map(&:organization_id)
        )
      end

      it "changes the organization_id on the open tickets associated with this organization" do
        assert_equal organization3.id, tickets(:minimum_3).reload.organization_id
      end

      it "does not unset the organization_id on open tickets that aren't associated with this organization" do
        assert_equal organization3.id, tickets(:minimum_4).reload.organization_id
      end

      it "does not unset the organization_id on the closed tickets" do
        closed_tickets.each(&:reload).each do |ticket|
          assert ticket.organization_id
        end
      end
    end

    describe "a default organization_membership association" do
      let(:user) { user_with_existing_membership }
      let!(:default_organization_membership) { user.organization_memberships.detect(&:default?) }
      let!(:regular_organization_membership) { user.organization_memberships.detect { |om| !om.default? } }
      let(:call) { OrganizationAssociationRemoval.new(account.id, default_organization_membership.organization_id, batch.map(&:id)).call }

      it "destroys the default and spare the rest" do
        call
        assert_raise(ActiveRecord::RecordNotFound) { default_organization_membership.reload }
        regular_organization_membership.reload
      end

      it "falls back to users organization_id" do
        user.organization_id
        call
        assert_equal regular_organization_membership.organization_id, user.reload.organization_id
      end
    end

    describe "database backoff" do
      let(:backoff_duration) { 1.5 }
      let(:organization) { organizations(:minimum_organization1) }

      before do
        Zendesk::DatabaseBackoff.any_instance.stubs(backoff_duration: backoff_duration)

        organization.organization_memberships.delete_all
        organization.organization_memberships.create!(user: users(:minimum_end_user))
        organization.organization_memberships.create!(user: users(:minimum_end_user2))
        organization.reload
      end

      it "sleeps between deletes if database load is high" do
        OrganizationAssociationRemoval.any_instance.expects(:sleep).with(backoff_duration).twice

        OrganizationAssociationRemoval.new(
          account.id,
          organization.id,
          organization.organization_memberships.pluck(:user_id)
        ).call
      end

      describe "backoff duration is higher than max backoff allowed" do
        let(:backoff_duration) { 4.5 }

        it "only sleeps for the max allowed backoff" do
          OrganizationAssociationRemoval.any_instance.expects(:sleep).with(3).twice

          OrganizationAssociationRemoval.new(
            account.id,
            organization.id,
            organization.organization_memberships.pluck(:user_id)
          ).call
        end
      end
    end
  end

  describe "without multiple_organizations" do
    let(:user_without_association) { users(:minimum_search_user) }

    let(:with_organization1) do
      [
        users(:minimum_end_user),
        users(:minimum_end_user2)
      ]
    end

    let(:batch) { with_organization1 + [user_without_association] }

    let(:all_org_tickets) do
      Ticket.where("organization_id IS NOT NULL AND requester_id IN (?)", batch.map(&:id)).to_a
    end
    let(:closed_tickets) { all_org_tickets.select(&:closed?) }

    let(:open_organization1_tickets) do
      all_org_tickets.select do |ticket|
        ticket.organization_id == organization1.id && !ticket.closed?
      end
    end

    before do
      Account.any_instance.stubs(has_multiple_organizations_enabled?: false)

      tickets(:minimum_1).will_be_saved_by(users(:minimum_end_user))
      tickets(:minimum_1).update_attribute(:organization_id, organization2.id)
      tickets(:minimum_3).will_be_saved_by(users(:minimum_end_user2))
      tickets(:minimum_3).update_attribute(:requester_id, users(:minimum_end_user2).id)

      all_org_tickets.each { |ticket| assert ticket.organization_id }

      assert_nil user_without_association.organization_id
      assert_empty(user_without_association.organization_memberships)

      with_organization1.each do |user|
        assert_equal organization1.id, user.organization_id
        assert_equal organization1.id, user.default_organization_membership.organization_id
        assert user.tickets.not_closed.map(&:organization_id).include?(organization1.id)
      end
    end

    describe "when all users have only one membership" do
      before do
        OrganizationAssociationRemoval.new(
          account.id, organization1.id, batch.map(&:id)
        ).call
      end

      it "nullifys the organization_ids" do
        batch.each(&:reload).each do |user|
          assert_nil user.organization_id
        end
      end

      it "deletes the organization memberships" do
        batch.each(&:reload).each do |user|
          assert_empty user.organization_memberships
        end
      end

      it "unsets the organization_id on all of the open tickets" do
        open_organization1_tickets.each(&:reload).each do |ticket|
          assert_nil ticket.organization_id
        end
      end

      it "does not unset the organization_id on the closed tickets" do
        closed_tickets.each(&:reload).each do |ticket|
          assert ticket.organization_id
        end
      end
    end

    describe "when a user has multiple org memberships" do
      let (:user1) { with_organization1.first }

      describe_with_arturo_enabled :org_association_always_remove_with_fallback do
        before do
          user1.organization_memberships.create!(organization: organization2)
          OrganizationAssociationRemoval.new(
            account.id, organization1.id, batch.map(&:id)
          ).call
        end

        it "falls back organization_id to other org membership" do
          assert_equal organization2.id, user1.reload.organization_id
        end
      end

      describe_with_arturo_disabled :org_association_always_remove_with_fallback do
        before do
          user1.organization_memberships.create!(organization: organization2)
          OrganizationAssociationRemoval.new(
            account.id, organization1.id, batch.map(&:id)
          ).call
        end

        it "fails to fall back organization_id" do
          assert_not_equal organization2.id, user1.reload.organization_id
        end
      end
    end
  end

  describe "#call" do
    let(:user) { users(:minimum_end_user) }

    describe "when the organization has been deleted" do
      before { assert organization1.destroy }

      it "returns nil" do
        assert_nil OrganizationAssociationRemoval.new(
          account.id, organization1.id, [user.id]
        ).call
      end
    end

    describe "when the organization has not been deleted" do
      before { refute organization1.deleted? }

      it "returns the organization" do
        assert_equal organization1, OrganizationAssociationRemoval.new(
          account.id, organization1.id, [user.id]
        ).call
      end
    end

    describe "bulk remove without user_ids" do
      it "does not try to remove or unset organization memberships" do
        OrganizationAssociationRemoval.any_instance.expects(:bulk_delete_organization_memberships).never
        OrganizationAssociationRemoval.any_instance.expects(:bulk_unset_organization_id).never
        OrganizationAssociationRemoval.any_instance.expects(:bulk_unset_organization_on_open_ticket).never
        OrganizationAssociationRemoval.new(
          account.id, organization1.id, []
        ).call
      end
    end
  end
end
