require_relative "../support/test_helper"
require_relative "../support/agent_test_helper"

SingleCov.covered!

describe 'Collaboration' do
  include AgentTestHelper

  fixtures :accounts, :tickets, :users
  #
  # should_not_allow_values_for :account_id
  # should_validate_presence_of :ticket, :user

  describe "#validate_on_create" do
    before do
      @collaboration = Collaboration.new
      @ticket        = FactoryBot.build_stubbed(:ticket, account_id: 1, requester: nil)
      @account       = FactoryBot.build(:account, id: 100)
      @account.stubs(:remote_authentication).returns(stub(is_active?: false))
      @user = FactoryBot.build(:user, account: @account)

      @user.stubs(:account).returns(@account)
      @collaboration.ticket_id = 123
      @collaboration.user_id = 123
      @collaboration.account_id = 123
      @collaboration.stubs(:ticket).returns(@ticket)
      @collaboration.stubs(:user).returns(@user)
      @collaboration.stubs(:account).returns(@account)
    end

    it "sets the collaboration_type to 1 if it is nil or false" do
      @collaboration.collaboration_type = nil
      @collaboration.save
      assert_equal '1', @collaboration.reload.collaboration_type
    end

    it "sets the account_id" do
      @collaboration.save
      assert_equal @ticket.account_id, @collaboration.account_id
    end

    it "adds the error 'CC - invalid user' if the user and the ticket requester are the same" do
      @user.stubs(:account_id).returns(@account.id + 1)
      @collaboration.save
      assert_equal ['CC - invalid user'], @collaboration.errors["base"]
    end
  end

  describe "#with_active_users named scope" do
    it "returns only collaborations with active users" do
      ticket = tickets(:minimum_1)

      collaboration = Collaboration.new
      collaboration.user = users(:inactive_user)
      ticket.collaborations << collaboration
      assert_empty ticket.collaborations.with_active_users

      collaboration = Collaboration.new
      collaboration.user = users(:minimum_end_user)
      ticket.collaborations << collaboration
      refute_empty ticket.collaborations.with_active_users
    end
  end

  describe '#transform' do
    let(:end_user_legacy_cc) { FactoryBot.build_stubbed(:end_user_legacy_cc) }
    let(:end_user_email_cc)  { FactoryBot.build_stubbed(:end_user_email_cc) }
    let(:agent_legacy_cc)    { FactoryBot.build_stubbed(:agent_legacy_cc) }
    let(:agent_email_cc)     { FactoryBot.build_stubbed(:agent_email_cc) }
    let(:agent_follower)     { FactoryBot.build_stubbed(:agent_follower) }

    def assert_transforms_to_collaborator_type(collab, end_state)
      assert_equal end_state,
        collab.transform(follower_and_email_cc_collaborations_enabled).collaborator_type
    end

    describe 'when follower_and_email_cc_collaborations setting is enabled' do
      let(:follower_and_email_cc_collaborations_enabled) { true }

      it 'transforms legacy types into modern types' do
        assert_transforms_to_collaborator_type(end_user_legacy_cc, CollaboratorType.EMAIL_CC)
        assert_transforms_to_collaborator_type(agent_legacy_cc, CollaboratorType.FOLLOWER)
      end

      it 'does not transform modern types' do
        assert_transforms_to_collaborator_type(end_user_email_cc, CollaboratorType.EMAIL_CC)
        assert_transforms_to_collaborator_type(agent_email_cc, CollaboratorType.EMAIL_CC)
        assert_transforms_to_collaborator_type(agent_follower, CollaboratorType.FOLLOWER)
      end
    end

    describe 'when follower_and_email_cc_collaborations setting is disabled' do
      let(:follower_and_email_cc_collaborations_enabled) { false }

      it 'transforms modern types into legacy types' do
        assert_transforms_to_collaborator_type(end_user_email_cc, CollaboratorType.LEGACY_CC)
        assert_transforms_to_collaborator_type(agent_email_cc, CollaboratorType.LEGACY_CC)
        assert_transforms_to_collaborator_type(agent_follower, CollaboratorType.LEGACY_CC)
      end

      it 'does not transform legacy types' do
        assert_transforms_to_collaborator_type(end_user_legacy_cc, CollaboratorType.LEGACY_CC)
        assert_transforms_to_collaborator_type(agent_legacy_cc, CollaboratorType.LEGACY_CC)
      end
    end
  end

  describe "#follower?" do
    describe "with collaborator_type of CollaboratorType.FOLLOWER" do
      let(:collaboration) { Collaboration.new(collaborator_type: CollaboratorType.FOLLOWER, user: user) }

      describe "with agent user" do
        let(:user) { users(:minimum_agent) }

        it { assert collaboration.follower? }
      end

      describe "with non-agent user" do
        let(:user) { users(:minimum_end_user) }

        it { refute collaboration.follower? }
      end
    end

    describe "with collaborator_type of CollaboratorType.EMAIL_CC" do
      let(:collaboration) { Collaboration.new(collaborator_type: CollaboratorType.EMAIL_CC) }

      it { refute collaboration.follower? }
    end

    describe "with collaborator_type of CollaboratorType.LEGACY_CC" do
      let(:collaboration) { Collaboration.new(collaborator_type: CollaboratorType.LEGACY_CC, user: user) }

      describe "with agent user" do
        let(:user) { users(:minimum_agent) }

        it { assert collaboration.follower? }
      end

      describe "with non-agent user" do
        let(:user) { users(:minimum_end_user) }

        it { refute collaboration.follower? }
      end
    end

    describe 'when verify_agent is `false`' do
      let(:collaboration) do
        Collaboration.new(
          collaborator_type: collaborator_type,
          user: user
        )
      end

      describe 'when the collaborator_type is `FOLLOWER`' do
        let(:collaborator_type) { CollaboratorType.FOLLOWER }
        let(:user) { users(:minimum_agent) }

        it 'returns true' do
          assert collaboration.follower?(verify_agent: false)
        end

        describe 'and user in collaboration is an end user' do
          let(:user) { users(:minimum_end_user) }

          it 'returns true' do
            assert collaboration.follower?(verify_agent: false)
          end
        end
      end

      describe 'when the collaborator_type is `EMAIL_CC`' do
        let(:collaborator_type) { CollaboratorType.EMAIL_CC }
        let(:user) { users(:minimum_agent) }

        it 'returns false' do
          refute collaboration.follower?(verify_agent: false)
        end
      end

      describe 'when the collaborator_type is `LEGACY_CC`' do
        let(:collaborator_type) { CollaboratorType.LEGACY_CC }

        describe 'and the user is an end user' do
          let(:user) { users(:minimum_end_user) }

          it 'returns false' do
            refute collaboration.follower?(verify_agent: false)
          end
        end

        describe 'and the user is not a end user' do
          let(:user) { users(:minimum_agent) }

          it 'returns true' do
            assert collaboration.follower?(verify_agent: false)
          end
        end
      end
    end
  end

  describe "#email_cc?" do
    let(:account) { accounts(:minimum) }

    describe "with collaborator_type of CollaboratorType.FOLLOWER" do
      let(:collaboration) { Collaboration.new(account: account, collaborator_type: CollaboratorType.FOLLOWER) }

      it { refute collaboration.email_cc? }
    end

    describe "with collaborator_type of CollaboratorType.EMAIL_CC" do
      let(:collaboration) do
        Collaboration.new(
          account: account,
          collaborator_type: CollaboratorType.EMAIL_CC,
          user: user
        )
      end

      describe_with_arturo_disabled :email_ccs_light_agents_v2 do
        describe 'and user has public comment access' do
          let(:user) { users(:minimum_agent) }

          it { assert collaboration.email_cc? }
        end

        describe 'and user does not have public comment access' do
          let(:user) { create_light_agent }

          before do
            Account.any_instance.stubs(has_light_agents?: true)
          end

          it { refute collaboration.email_cc? }
        end
      end

      describe_with_arturo_enabled :email_ccs_light_agents_v2 do
        describe_with_arturo_setting_enabled :light_agent_email_ccs_allowed do
          describe 'and user has public comment access' do
            let(:user) { users(:minimum_agent) }

            it { assert collaboration.email_cc? }
          end

          describe 'and user does not have public comment access' do
            let(:user) { create_light_agent }

            before do
              Account.any_instance.stubs(has_light_agents?: true)
            end

            it { assert collaboration.email_cc? }
          end
        end

        describe_with_arturo_setting_disabled :light_agent_email_ccs_allowed do
          describe 'and user has public comment access' do
            let(:user) { users(:minimum_agent) }

            it { assert collaboration.email_cc? }
          end

          describe 'and user does not have public comment access' do
            let(:user) { create_light_agent }

            before do
              Account.any_instance.stubs(has_light_agents?: true)
            end

            it { refute collaboration.email_cc? }
          end
        end
      end
    end

    describe "with collaborator_type of CollaboratorType.LEGACY_CC" do
      let(:collaboration) { Collaboration.new(account: account, collaborator_type: CollaboratorType.LEGACY_CC, user: user) }

      describe "with agent user" do
        let(:user) { users(:minimum_agent) }

        it { refute collaboration.email_cc? }
      end

      describe "with end user" do
        let(:user) { users(:minimum_end_user) }

        it { assert collaboration.email_cc? }
      end
    end

    describe 'when verify_user is `false`' do
      let(:collaboration) do
        Collaboration.new(
          account: account,
          collaborator_type: collaborator_type,
          user: user
        )
      end

      describe 'when the collaborator_type is `EMAIL_CC`' do
        let(:collaborator_type) { CollaboratorType.EMAIL_CC }
        let(:user) { users(:minimum_agent) }

        describe_with_and_without_arturo_enabled :email_ccs_light_agents_v2 do
          describe_with_and_without_arturo_setting_enabled :light_agent_email_ccs_allowed do
            it 'returns true' do
              assert collaboration.email_cc?(verify_user: false)
            end

            describe 'and user in collaboration is a light agent' do
              let(:user) { create_light_agent }

              it 'returns true' do
                assert collaboration.email_cc?(verify_user: false)
              end
            end
          end
        end
      end

      describe 'when the collaborator_type is `FOLLOWER`' do
        let(:collaborator_type) { CollaboratorType.FOLLOWER }
        let(:user) { users(:minimum_agent) }

        it 'returns false' do
          refute collaboration.email_cc?(verify_user: false)
        end
      end

      describe 'when the collaborator_type is `LEGACY_CC`' do
        let(:collaborator_type) { CollaboratorType.LEGACY_CC }

        describe 'and the user is an end user' do
          let(:user) { users(:minimum_end_user) }

          it 'returns true' do
            assert collaboration.email_cc?(verify_user: false)
          end
        end

        describe 'and the user is not a end user' do
          let(:user) { users(:minimum_agent) }

          it 'returns false' do
            refute collaboration.email_cc?(verify_user: false)
          end
        end
      end
    end
  end

  describe "#legacy_cc?" do
    describe "when the collaborator_type is nil" do
      let(:collaboration) { Collaboration.new(collaborator_type: nil) }

      it { assert collaboration.legacy_cc? }
    end
  end

  describe "#user_valid?" do
    let(:user) { users(:minimum_end_user) }
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }
    let(:collaboration) { Collaboration.new(account: account, ticket: ticket, collaborator_type: CollaboratorType.EMAIL_CC, user: user) }

    describe "when the collaboration's user is valid" do
      it { assert collaboration.user_valid? }
    end

    describe "when the collaboration's user has been deleted" do
      before do
        collaboration.save!
        User.delete(user.id)
        Rails.cache.clear
        collaboration.reload
      end

      it { refute collaboration.user_valid? }
    end

    describe "when the collaboration's user has become invalid" do
      before { UserEmailIdentity.any_instance.stubs(value: "foo") }

      it { refute collaboration.user_valid? }
    end
  end
end
