require_relative "../support/test_helper"

SingleCov.covered!

describe OrganizationMembership do
  fixtures :accounts, :organization_memberships, :organizations, :users, :tickets, :ticket_fields

  let(:organization_membership) { OrganizationMembership.new }
  should_validate_presence_of :organization, :account
  should validate_uniqueness_of(:user_id).scoped_to(:organization_id).with_message(I18n.t("activerecord.errors.models.organization_membership.unique_user"))

  def extract_conditions_from(scope)
    scope.where_values_hash.stringify_keys
  end

  describe "scopes" do
    describe ".regular scope" do
      it "returns non-default organization_memberships" do
        assert_equal(
          {"default" => nil},
          extract_conditions_from(OrganizationMembership.regular)
        )
      end
    end
  end

  describe "without a user" do
    let(:organization) { organizations(:minimum_organization1) }

    before do
      organization_membership.organization = organization
    end

    it "does not validate" do
      refute organization_membership.valid?
    end
  end

  describe "with a user" do
    before do
      organization_membership.build_user
      organization_membership.user.account_id = 1
    end

    it "sets the account to the user's account" do
      organization_membership.valid?

      assert organization_membership.account
      assert_equal organization_membership.account, organization_membership.user.account
    end

    describe "with an organization" do
      before { organization_membership.build_organization }

      it "sets the account" do
        organization_membership.organization.account_id = 1
        organization_membership.valid?

        assert organization_membership.account
        assert_equal organization_membership.account, organization_membership.user.account
      end

      it "sets the appropriate error message if the organization has a different account" do
        organization_membership.organization.account_id = 2
        organization_membership.valid?

        assert_match(/must.*same account/, organization_membership.errors[:user].join)
      end
    end
  end

  describe "in a scenario with some real data" do
    it "does stuff" do
      @agent = users(:minimum_agent)
      @organization = organizations(:minimum_organization1)
      new_membership = @agent.organization_memberships.new(organization_id: @organization.id, user_id: @agent.id)
      new_membership.save!
    end
  end

  describe "without a user, but with an organization" do
    it "sets the account to the organization's account" do
      organization_membership.build_organization
      organization_membership.organization.account_id = 1
      organization_membership.valid?

      refute organization_membership.user
      assert organization_membership.account
      assert_equal organization_membership.account, organization_membership.organization.account
    end
  end

  describe "lifecycle callbacks" do
    let(:account)       { accounts(:minimum) }
    let(:user)          { organization_membership.user }
    let(:organization1) { organizations(:minimum_organization1) }
    let(:organization2) { organizations(:minimum_organization2) }
    let(:organization3) { organizations(:minimum_organization3) }
    let(:organization_id) { organization_membership.organization_id }
    let(:organization_membership) { organization_memberships(:minimum) }
    let(:watching) do
      Watching.create!(
        user: user, source: organization1, account: account
      )
    end
    let(:ticket) { tickets(:minimum_3) }
    let(:non_default) do
      user.organization_memberships.create!(
        account: account,
        organization: organization2,
        created_at: 1.month.ago
      )
    end

    describe "#validate_under_limit" do
      it "allows creating 300 memberships" do
        membership = organization_membership.user.organization_memberships.build(organization: organization3)
        membership.user.organization_memberships.stubs(count: 299)
        assert membership.save
      end

      it "does not allow creating more than 300 memberships" do
        membership = organization_membership.user.organization_memberships.build(organization: organization3)
        membership.user.organization_memberships.stubs(count: 300)
        refute membership.save
        assert_includes membership.errors.full_messages.join(""), "more than 300"
      end
    end

    describe "after destroy" do
      describe "user is watching the associated organization" do
        it "removes the Watching record" do
          assert_equal watching, user.watchings.first

          organization_membership.destroy

          assert_equal 0, user.reload.watchings.size
        end
      end

      describe "and user has another organization membership" do
        it "which gets muddled by racy jobs" do
          other_mem = non_default
          other_mem.default = true
          other_mem.save!
          organization_membership.user.reload
          organization_membership.destroy
          assert_equal 1, user.reload.organization_memberships.size
        end
      end

      describe "and user has one organization_membership" do
        it "unsets the user's organization_id" do
          organization_membership.destroy

          assert_nil user.reload.organization_id
        end

        it "creates a new cache_key" do
          cache_key = user.cache_key
          organization_membership.destroy
          refute_equal cache_key, user.cache_key
        end

        it "unsets the organization_id on all of the user's open tickets" do
          assert_equal user.organization_id, ticket.organization_id
          organization_membership.destroy
          assert_nil ticket.reload.organization_id
        end

        it "does not unset the user's organization_id if the user handled default succession" do
          organization_membership.mark_for_destruction
          organization_membership.destroy

          assert_equal organization1.id, user.reload.organization_id
        end
      end

      describe "and user has multiple organization_memberships" do
        describe "with only one non-default organization_membership" do
          let!(:cache_key) { user.cache_key }

          before do
            non_default

            assert_same_elements(
              [organization1.id, organization2.id],
              user.organization_memberships.map(&:organization_id)
            )

            organization_membership.destroy

            assert_same_elements(
              [organization2.id],
              user.reload.organization_memberships.map(&:organization_id)
            )
          end

          it "promotes the remaining organization_membership to be the default" do
            assert non_default.reload.default?
          end

          it "updates the user's organization_id" do
            assert_equal non_default.organization_id, user.reload.organization_id
          end

          it "changes the user's open tickets to the new default" do
            assert_equal user.reload.organization_id, ticket.organization_id
          end

          it "creates a new cache_key" do
            refute_equal cache_key, user.cache_key
          end
        end

        describe "with more than one non-default organization_membership" do
          let!(:oldest_non_default) do
            user.organization_memberships.create!(
              account: account,
              organization: organization3,
              created_at: 1.year.ago
            )
          end

          let!(:other_non_default) do
            user.organization_memberships.create!(
              account: account,
              organization: organization2,
              created_at: 1.month.ago
            )
          end

          before do
            assert_same_elements(
              [organization1.id, organization2.id, organization3.id],
              user.organization_memberships.map(&:organization_id)
            )
          end

          describe "and the default is destroyed" do
            describe "and the remaining organization is valid" do
              let!(:cache_key) { user.cache_key }

              before do
                Timecop.travel(1.hour.from_now)
                organization_membership.destroy

                assert_same_elements(
                  [organization2.id, organization3.id],
                  user.reload.organization_memberships.map(&:organization_id)
                )
              end

              it "promotes the oldest remaining organization_membership to be the default" do
                assert oldest_non_default.reload.default?
              end

              it "updates the user's organization_id to reflect the new default organization association" do
                assert_equal oldest_non_default.organization_id, user.reload.organization_id
              end

              it "creates a new cache_key" do
                refute_equal cache_key, user.cache_key
              end
            end

            describe "and the oldest organization_membership's organization is not valid" do
              before do
                organization3.update_column(:deleted_at, 1.day.ago)
                organization_membership.destroy
              end

              it "promotes the valid organization_membership to be the default" do
                assert other_non_default.reload.default?, "other_non_default should now be default"
              end

              it "updates the user's organization_id to reflect the new default organization association" do
                assert_equal other_non_default.organization_id, user.reload.organization_id
              end
            end

            describe "and the remaining organization_membership organizations are not valid" do
              before do
                organization2.update_column(:deleted_at, 1.day.ago)
                organization3.update_column(:deleted_at, 1.day.ago)
                organization_membership.destroy
              end

              it "unsets the user's organization_id" do
                assert_nil user.reload.organization_id
              end
            end

            describe_with_arturo_enabled :unset_ticket_org_when_removing_default_user_org do
              before do
                Timecop.travel(1.hour.from_now)
                organization_membership.destroy
              end

              it "sets the user's open tickets to nil" do
                assert_nil ticket.organization_id
              end
            end

            describe_with_arturo_disabled :unset_ticket_org_when_removing_default_user_org do
              before do
                Timecop.travel(1.hour.from_now)
                organization_membership.destroy
              end

              it "sets the user's open tickets to the new default" do
                assert_equal user.reload.organization_id, ticket.organization_id
              end
            end
          end

          describe "the user handled default succession" do
            before do
              organization_membership.mark_for_destruction
              organization_membership.destroy
            end

            it "does not unset the user's organization_id" do
              assert_equal organization1.id, user.reload.organization_id
            end

            it "promotes the oldest remaining organization_membership" do
              refute oldest_non_default.reload.default?
            end
          end

          describe "and a regular organization_membership is destroyed" do
            before do
              Timecop.freeze

              oldest_non_default.destroy

              assert_same_elements(
                [organization1.id, organization2.id],
                user.reload.organization_memberships.map(&:organization_id)
              )
            end

            it "does not change the default" do
              assert organization_membership.reload.default?
            end

            it "does not change the user's organization_id" do
              assert_equal organization_membership.organization_id, user.reload.organization_id
            end

            it "touches user record" do
              assert_equal Time.now.to_i, user.reload.updated_at.to_i
            end
          end

          describe "and the regular organization_membership is made the default" do
            it "handles the transition for each organization_membership correctly" do
              assert organization_membership.default?
              oldest_non_default.update_attributes(default: true)

              refute organization_membership.reload.default?
              assert oldest_non_default.reload.default?
            end
          end
        end
      end
    end

    describe "after create" do
      let!(:user) { accounts(:minimum).end_users.new(name: 'test') }

      describe "when adding a secondary organization_membership" do
        before do
          user.organization_memberships.build(default: true, organization: organizations(:minimum_organization1))
          user.save
        end

        it "does touches the user" do
          Timecop.freeze(1.days.from_now) do
            new_membership = user.organization_memberships.create(organization: organizations(:minimum_organization2))
            refute new_membership.default?
            assert_equal Time.now.to_i, user.reload.updated_at.to_i
          end
        end
      end
    end

    describe "being saved" do
      it "does not be made the default if the user is a new record and already has a default" do
        user = accounts(:minimum).end_users.new(name: 'test')
        user.organization_memberships.build(default: true, organization: organizations(:minimum_organization1))
        new_membership = user.organization_memberships.build(organization: organizations(:minimum_organization2))
        user.save
        refute new_membership.default?
      end

      it "does not change the organization on unclosed tickets for the user" do
        old = ticket.organization_id
        organization_membership.save!
        assert_equal old, ticket.organization_id
      end

      describe "when organization_id is updated" do
        it "removes the Watching record" do
          assert_equal watching, user.watchings.first
          assert_equal organization1.id, user.watchings.first.source_id

          organization_membership.update_attribute(
            :organization_id, organizations(:minimum_organization2).id
          )

          assert_equal 0, user.reload.watchings.size
        end

        it "moves unclosed tickets for the user to the new organization" do
          organization_membership.update_attribute(:organization_id, organization2.id)
          assert_equal organization2.id, ticket.reload.organization_id
        end
      end

      describe "when made default" do
        def assert_no_update(&block)
          queries = sql_queries(&block)
          queries.join(" -- ").wont_include "UPDATE"
        end

        before { non_default }

        it "does not change unclosed tickets" do
          non_default.update_attribute(:default, true)
          user.reload
          assert_equal user.organization_id, non_default.organization_id
          assert_equal organization_membership.organization_id, ticket.reload.organization_id
        end

        it "unsets other organization_memberships default" do
          non_default.update_attribute(:default, true)
          assert non_default.default?
          refute organization_membership.reload.default?
        end

        it "does not unset other organization_memberships default when the updated value is the same" do
          assert organization_membership.default?
          assert_no_update do
            organization_membership.update_attribute(:default, true)
          end
        end
      end
    end
  end

  describe "#organization_id=" do
    let(:organization_membership) { organization_memberships(:minimum) }

    it "sets the default to NULL when it is assigned false to avoid mysql unique errors" do
      organization_membership.update_attribute(:default, false)
      assert_nil organization_membership.default
    end

    it "sets the default to true when it is assigned true" do
      organization_membership.update_attribute(:default, true)
      assert(organization_membership.default)
    end
  end
end
