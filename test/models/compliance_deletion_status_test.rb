require_relative "../support/test_helper"
require 'aws-sdk-sns'

SingleCov.covered! uncovered: 2

describe 'ComplianceDeletionStatus' do
  fixtures :role_settings

  let(:account) { deleted_user.account }
  let(:user)    { users(:minimum_end_user) }
  let(:deleted_user) { users(:minimum_end_user2) }
  let(:deleted_user2) { users(:minimum_sdk_end_user) }
  let(:deleted_user3) { users(:minimum_sdk_end_user2) }
  let(:deleted_user4) { users(:multiproduct_end_user) }
  let(:deleted_user5) { users(:minimum_sdk_end_user3) }
  let(:zendesk_agent) { users(:minimum_agent) }

  let(:initial_request_status) do
    FactoryBot.create(:compliance_deletion_status,
      account: account,
      user: deleted_user,
      executer: zendesk_agent,
      complete: false)
  end

  describe '.create_request!' do
    before do
      ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
      ComplianceDeletionStatus.create_request!(user: deleted_user, executer: user)
    end

    it 'creates request record with correct values' do
      status = ComplianceDeletionStatus.where(account: deleted_user.account, user: deleted_user).last

      assert_equal status.account, deleted_user.account
      assert_equal status.user, deleted_user
      assert_equal status.executer, user
    end

    it 'pre-creates feedback records' do
      num_applications = ComplianceDeletionStatus::APPLICATONS.size
      status = ComplianceDeletionStatus.where(account: deleted_user.account, user: deleted_user).last
      assert_equal status.compliance_deletion_feedback.count, num_applications
      assert_equal status.compliance_deletion_feedback.required.count, ComplianceDeletionStatus::APPLICATIONS_REQUIRING_FEEDBACK.size
    end
  end

  describe 'the required apps constant is getting set correctly' do
    it 'should equal the test constant' do
      # this is prod because the test environment is also using the production constant
      assert_equal ComplianceDeletionStatus::APPLICATIONS_REQUIRING_FEEDBACK, ComplianceDeletionStatus::REQUIRED_APPS_PROD
    end
  end

  describe 'deleting a user' do
    let(:initial_status) { FactoryBot.build(:compliance_deletion_status, account: account, user: deleted_user) }

    before do
      deleted_user.current_user = zendesk_agent
      deleted_user.delete!
    end

    it 'should publish to world' do
      Zendesk::PushNotifications::Gdpr::GdprUserDeletionPublisher.any_instance.expects(:publish).once
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('request_publish', tags: ['success:true']).once

      initial_status.send(:publish_to_world)
    end

    describe 'when error occurs trying to publish to SNS' do
      let(:exception) { Seahorse::Client::NetworkingError.new(RuntimeError.new, 'error') }

      it 'does not raise exception' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('request_publish', tags: ['success:false', "error_type:Seahorse::Client::NetworkingError"]).once
        Zendesk::PushNotifications::Gdpr::GdprUserDeletionPublisher.any_instance.expects(:publish).raises(exception)

        initial_status.send(:publish_to_world)
      end
    end
  end

  describe 'when hard deleting a user' do
    before do
      ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
    end

    describe 'validations' do
      it 'cant create more than one initial request' do
        status = FactoryBot.create(:compliance_deletion_status,
          account: account,
          user: user)
        dup_status = status.dup
        refute dup_status.valid?, 'Only one deletion request needed per user'
      end
    end

    describe '#complete!' do
      let(:elapsed_days) { 3 }

      let(:compliance_deletion_status) do
        FactoryBot.create(:compliance_deletion_status,
          account: account,
          user: deleted_user,
          executer: zendesk_agent,
          created_at: elapsed_days.days.ago,
          complete: false)
      end

      before do
        Timecop.freeze

        compliance_deletion_status # create initial request
      end

      after { Timecop.return }

      it 'should mark initial request complete' do
        refute compliance_deletion_status.reload.complete?
        compliance_deletion_status.complete!
        assert compliance_deletion_status.reload.complete?
      end

      it 'should call statsd_client when complete' do
        tags = ["completion_bucket:within_expected"]
        elapsed_time_seconds = elapsed_days.days.seconds
        Zendesk::StatsD::Client.any_instance.expects(:distribution).with('time_to_complete', elapsed_time_seconds, tags: tags).once
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('request_completed', tags: tags).once

        compliance_deletion_status.complete!
      end
    end

    describe 'incomplete' do
      let!(:old_initial_request_status) do
        FactoryBot.create(:compliance_deletion_status,
          account: account,
          user: deleted_user3,
          executer: zendesk_agent,
          complete: false,
          updated_at: 7.days.ago)
      end

      let!(:recent_initial_request_status) do
        FactoryBot.create(:compliance_deletion_status,
          account: account,
          user: deleted_user2,
          executer: zendesk_agent,
          complete: false,
          updated_at: 1.day.ago)
      end

      it 'should only select incomplete statuses' do
        recent_initial_request_status.update_column(:complete, true)
        status_ids = ComplianceDeletionStatus.incomplete.pluck(:id)
        assert status_ids.include?(old_initial_request_status.id)
        refute status_ids.include?(recent_initial_request_status.id)
      end
    end

    describe 'log_app_isnt_complete_for' do
      let(:initial_status) do
        FactoryBot.create(:compliance_deletion_status,
          account: account,
          user: deleted_user,
          created_at: elapsed_days.days.ago)
      end
      let(:applications) { ComplianceDeletionStatus::APPLICATIONS_REQUIRING_FEEDBACK }
      let(:elapsed_time_seconds) { elapsed_days.days.seconds }
      let(:msg) { "Failed to execute Deletion for user: #{initial_status.user_id}, applications: #{applications}, account: #{initial_status.account_id}:#{initial_status.account.subdomain}, days_since_initial_request: #{elapsed_days}" }

      before { Timecop.freeze }
      after { Timecop.return }

      describe 'when elapsed time is less than 14 days' do
        let(:elapsed_days) { 3 }

        it 'should publish metrics but not events and logs' do
          applications.each do |app|
            tags = ["application:#{app}", "account_id:#{initial_status.account_id}", "sla_bucket:within_expected", "shard_id:#{account.shard_id}"]
            Zendesk::StatsD::Client.any_instance.expects(:distribution).with('time_since_request', elapsed_time_seconds, tags: tags)
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('request_republish', tags: tags)
          end

          Rails.logger.expects(:warn).with(msg).never
          Zendesk::StatsD::Client.any_instance.expects(:event).with('GDPR deletions has not been performed in the expected timeframe', msg, alert_type: 'warning', tags: ['outside_sla:false']).never
          initial_status.log_app_isnt_complete_for(applications, account.shard_id)
        end
      end

      describe 'when elapsed time is 14 or more days' do
        let(:elapsed_days) { 15 }

        it 'should publish metrics and events and logs' do
          applications.each do |app|
            tags = ["application:#{app}", "account_id:#{initial_status.account_id}", "sla_bucket:within_sla", "shard_id:#{account.shard_id}"]
            Zendesk::StatsD::Client.any_instance.expects(:distribution).with('time_since_request', elapsed_time_seconds, tags: tags)
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('request_republish', tags: tags)
          end

          Rails.logger.expects(:warn).with(msg)
          Zendesk::StatsD::Client.any_instance.expects(:event).with('GDPR deletions has not been performed in the expected timeframe', msg, alert_type: 'warning', tags: ['outside_sla:false'])
          initial_status.log_app_isnt_complete_for(applications, account.shard_id)
        end
      end
    end

    describe 'republish_incomplete_statuses!' do
      let(:old_request_updated_at) { 7.days.ago }
      let(:old_initial_request_status) do
        ComplianceDeletionStatus.create_request!(user: deleted_user, executer: zendesk_agent)
        status = ComplianceDeletionStatus.find_by_user_id(deleted_user.id)
        status.update_column(:updated_at, old_request_updated_at)
        status
      end

      let(:recent_request_updated_at) { 1.day.ago }
      let(:recent_initial_request_status) do
        ComplianceDeletionStatus.create_request!(user: deleted_user2, executer: zendesk_agent)
        status = ComplianceDeletionStatus.find_by_user_id(deleted_user2.id)
        status.update_column(:updated_at, recent_request_updated_at)
        status
      end

      let(:now) { Time.now }

      let(:sns_client) { mock }
      let(:applications) { ComplianceDeletionStatus::APPLICATIONS_REQUIRING_FEEDBACK }

      before do
        Timecop.freeze
        Aws::SNS::Client.stubs(:new).returns(sns_client)
        sns_client.stubs(:publish)
        old_initial_request_status
        recent_initial_request_status
      end

      it 'should check and mark initial request complete if incomplete app list is empty' do
        ComplianceDeletionStatus.any_instance.stubs(:incomplete_applications).returns([])
        ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).never
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
        assert old_initial_request_status.reload.complete
      end

      it 'should call publish_to_incomplete! that have not been updated in 7 days' do
        Zendesk::StatsD::Client.any_instance.expects(:distribution).with('time_since_request', anything, anything).times(applications.size)
        Zendesk::StatsD::Client.any_instance.expects(:distribution).with('requests_overdue', 1, tags: ["shard_id:#{account.shard_id}"]).once
        Zendesk::StatsD::Client.any_instance.expects(:distribution).with('requests_incomplete', 0, tags: ["shard_id:#{account.shard_id}"]).once
        Zendesk::StatsD::Client.any_instance.expects(:count).with('unpublished_requests_overdue', 0, tags: ["shard_id:#{account.shard_id}"]).once
        ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).with(applications).once
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
      end

      it 'should touch the republished requests' do
        now = Time.now
        Timecop.freeze(now)
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
        # to_i to compare seconds
        assert_equal now.to_i, old_initial_request_status.reload.updated_at.to_i
      end

      it 'should not call applications that are complete already' do
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
        assert_equal recent_request_updated_at.to_i, recent_initial_request_status.reload.updated_at.to_i
      end

      it 'should log applications that arent complete' do
        ComplianceDeletionStatus.any_instance.expects(:log_app_isnt_complete_for).with(applications, account.shard_id)
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
      end

      it 'should not call publish_to_world' do
        ComplianceDeletionStatus.any_instance.expects(:publish_to_world).never
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
      end

      it 'should skip deleted accounts' do
        ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).never
        old_initial_request_status.account.update_columns(deleted_at: Time.now, is_serviceable: false, is_active: false)
        assert old_initial_request_status.reload.account.nil?
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
      end

      it 'should skip accounts that are inactive && not serviceable' do
        ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).never
        old_initial_request_status.account.update_columns(is_serviceable: false, is_active: false)
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
      end

      it 'should skip accounts that are active but not serviceable' do
        ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).never
        old_initial_request_status.account.update_columns(is_serviceable: false, is_active: true)
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
      end

      it 'should skip accounts that are on a different shard' do
        ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).never
        ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id + 1)
      end

      describe 'when there are many incomplete requests' do
        let(:slightly_older_request) do
          ComplianceDeletionStatus.create_request!(user: user, executer: zendesk_agent)
          status = ComplianceDeletionStatus.find_by_user_id(user.id)
          status.update_column(:updated_at, 178.hours.ago)
          status
        end
        let(:somewhat_older_request) do
          ComplianceDeletionStatus.create_request!(user: deleted_user3, executer: zendesk_agent)
          status = ComplianceDeletionStatus.find_by_user_id(deleted_user3.id)
          status.update_column(:updated_at, 8.days.ago)
          status
        end
        let(:even_older_request) do
          ComplianceDeletionStatus.create_request!(user: deleted_user4, executer: zendesk_agent)
          status = ComplianceDeletionStatus.find_by_user_id(deleted_user4.id)
          status.update_column(:updated_at, 9.days.ago)
          status
        end
        let(:oldest_request) do
          ComplianceDeletionStatus.create_request!(user: deleted_user5, executer: zendesk_agent)
          status = ComplianceDeletionStatus.find_by_user_id(deleted_user5.id)
          status.update_columns(created_at: 20.days.ago, updated_at: 10.days.ago)
          status
        end

        let(:all_requests) { [old_initial_request_status, slightly_older_request, somewhat_older_request, even_older_request, oldest_request] }

        before do
          all_requests
        end

        it 'will republish all if under limit' do
          ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).with(applications).times(all_requests.size)
          ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: account.shard_id)
          all_requests.each do |r|
            assert_equal now.to_i, r.reload.updated_at.to_i
          end
        end

        it 'can enforce a limit' do
          Zendesk::StatsD::Client.any_instance.expects(:distribution).with('time_since_request', anything, anything).times(applications.size * 2)
          Zendesk::StatsD::Client.any_instance.expects(:distribution).with('requests_overdue', 5, tags: ["shard_id:#{account.shard_id}"]).once
          Zendesk::StatsD::Client.any_instance.expects(:distribution).with('requests_incomplete', 1, tags: ["shard_id:#{account.shard_id}"]).once
          Zendesk::StatsD::Client.any_instance.expects(:count).with('unpublished_requests_overdue', 3, tags: ["shard_id:#{account.shard_id}"]).once
          ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).with(applications).times(2)
          ComplianceDeletionStatus.republish_incomplete_statuses!(max_republish: 2, shard_id: account.shard_id)
          [even_older_request, oldest_request].each do |r|
            assert_equal now.to_i, r.reload.updated_at.to_i
          end
          [old_initial_request_status, slightly_older_request, somewhat_older_request].each do |r|
            assert r.reload.updated_at < 6.days.ago
          end
        end

        it 'will cycle through requests by updated_at age' do
          ComplianceDeletionStatus.any_instance.expects(:publish_to_incomplete!).with(applications).times(4)
          ComplianceDeletionStatus.republish_incomplete_statuses!(max_republish: 2, shard_id: account.shard_id)
          ComplianceDeletionStatus.republish_incomplete_statuses!(max_republish: 2, shard_id: account.shard_id)
          [slightly_older_request, somewhat_older_request, even_older_request, oldest_request].each do |r|
            assert_equal now.to_i, r.reload.updated_at.to_i
          end
          assert_equal old_request_updated_at.to_i, old_initial_request_status.reload.updated_at.to_i
        end
      end
    end

    describe 'updating the timestamp' do
      before do
        initial_request_status.reload
      end

      it 'should not call publish_to_world' do
        initial_request_status.expects(:publish_to_world).never
        initial_request_status.save
      end
    end
  end

  describe '.sla_tag' do
    it 'returns within_expected when deletion request completed within 14 days' do
      seconds_to_complete = 2.days.to_i
      assert_equal :within_expected, ComplianceDeletionStatus.send(:sla_tag, seconds_to_complete)
    end

    it 'returns within_sla when deletion request completed within 30 days but more than 14 days' do
      seconds_to_complete = 15.days.to_i
      assert_equal :within_sla, ComplianceDeletionStatus.send(:sla_tag, seconds_to_complete)
    end

    it 'returns outside_sla when deletion request took more than 30 days to complete' do
      seconds_to_complete = 40.days.to_i
      assert_equal :outside_sla, ComplianceDeletionStatus.send(:sla_tag, seconds_to_complete)
    end
  end

  describe '#complete_applications' do
    before { ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true) }

    let!(:pending_feedback) do
      FactoryBot.create(
        :compliance_deletion_feedback,
        compliance_deletion_status: initial_request_status,
        application: ComplianceDeletionStatus::TEXT,
        required: true
      )
    end

    let!(:complete_feedback) do
      feedback = FactoryBot.create(
        :compliance_deletion_feedback,
        compliance_deletion_status: initial_request_status,
        application: ComplianceDeletionStatus::NPS,
        required: true
      )

      feedback.complete!
      feedback
    end

    it 'should select applications with complete statuses' do
      assert_equal [ComplianceDeletionStatus::NPS], initial_request_status.complete_applications
    end
  end

  describe '#incomplete_applications' do
    let(:missing_feedback) { ComplianceDeletionStatus::CHAT }

    before do
      ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)

      ComplianceDeletionStatus::APPLICATIONS_REQUIRING_FEEDBACK.each do |app|
        FactoryBot.create(:compliance_deletion_feedback,
          compliance_deletion_status: initial_request_status,
          application: app,
          state: app == missing_feedback ? ComplianceDeletionFeedback::PENDING : ComplianceDeletionFeedback::COMPLETE)
      end
    end

    it 'returns incompleted apps' do
      assert_equal [missing_feedback], initial_request_status.incomplete_applications
    end

    describe 'when all feedback was received' do
      let(:missing_feedback) { '' }

      it 'returns empty array' do
        assert_equal [], initial_request_status.incomplete_applications
      end
    end

    describe 'when non-required feedback exists' do
      let(:non_required_app) { ComplianceDeletionStatus::CDP }

      before do
        feedback = ComplianceDeletionFeedback.where(
          compliance_deletion_status: initial_request_status,
          application: non_required_app
        ).first

        feedback.update_columns(required: false, state: ComplianceDeletionFeedback::PENDING)
      end

      it 'returns only incomplete required apps' do
        assert_equal [missing_feedback], initial_request_status.incomplete_applications
      end
    end
  end

  describe '#received_required_feedback?' do
    before do
      ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
      ComplianceDeletionStatus::APPLICATIONS_REQUIRING_FEEDBACK.each do |app|
        FactoryBot.create(:compliance_deletion_feedback,
          compliance_deletion_status: initial_request_status,
          application: app)
      end
    end

    describe 'when all required feedback received' do
      before do
        initial_request_status.compliance_deletion_feedback.update_all(state: ComplianceDeletionFeedback::COMPLETE)
      end

      it 'returns true' do
        assert initial_request_status.received_required_feedback?
      end
    end

    describe 'when missing required feedback' do
      before do
        initial_request_status.compliance_deletion_feedback.update_all(state: ComplianceDeletionFeedback::PENDING)
      end

      it 'returns false' do
        refute initial_request_status.received_required_feedback?
      end
    end
  end

  describe '#check_completeness!' do
    before do
      ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
      initial_request_status
    end

    it 'should call complete! when all required feedback was received' do
      ComplianceDeletionStatus.any_instance.stubs(:received_required_feedback?).returns(true)
      ComplianceDeletionStatus.any_instance.expects(:complete!).once
      initial_request_status.send(:check_completeness!)
    end

    it 'should not call complete! when missing feedback' do
      ComplianceDeletionStatus.any_instance.stubs(:received_required_feedback?).returns(false)
      ComplianceDeletionStatus.any_instance.expects(:complete!).never
      initial_request_status.send(:check_completeness!)
    end
  end
end
