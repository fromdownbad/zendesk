require_relative "../support/test_helper"
require 'openssl'

SingleCov.covered! uncovered: 6

describe 'CertificateAuthorities' do
  fixtures :certificate_authorities

  describe "certificate authorities" do
    it "shoulds have the expected number of roots and intermediates" do
      assert_equal 3, CertificateAuthorities.roots.length
      assert_equal 4, CertificateAuthorities.intermediates.length
    end

    it "is able to import new intermediate" do
      ca_cert = OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('ca.crt'))

      old_len = CertificateAuthorities.intermediates.length
      assert CertificateAuthorities.append(ca_cert)
      assert_equal old_len + 1, CertificateAuthorities.intermediates.length

      # check if last item is our ca_cert
      assert_equal ca_cert.to_pem, CertificateAuthorities.intermediates[-1].to_pem
    end
  end

  describe "#find_by_crt_object" do
    it "excludes records with the same subject but different public key" do
      # Generated with: `openssl req -x509 -nodes -sha256 -days 36500 -newkey rsa:2048 -keyout /dev/null -out test/files/certificate/subject_collision.crt`
      subject_collision_cert = OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('subject_collision.crt'))
      existing_cert = CertificateAuthorities.where(subject: subject_collision_cert.subject.to_s).first
      refute_equal existing_cert.crt_object.public_key.to_s, subject_collision_cert.public_key.to_s
      assert_nil CertificateAuthorities.find_by_crt_object(subject_collision_cert)
    end

    it "finds records that match subject and public key" do
      ca = CertificateAuthorities.first
      assert_equal ca, CertificateAuthorities.find_by_crt_object(ca.crt_object)
    end
  end
end
