require_relative "../support/test_helper"

SingleCov.covered!

describe SharedComment do
  fixtures :accounts, :events

  describe "#generate_uuid" do
    it 'generates a uuid' do
      comment = events(:create_comment_for_minimum_ticket_1)
      shared_comment = comment.create_shared_comment
      shared_comment.valid?
      assert_not_nil(shared_comment.uuid)
    end

    it 'does not generate another uuid' do
      account = accounts(:minimum)
      comment = events(:create_comment_for_minimum_ticket_1)

      shared_comment = SharedComment.new(
        account: account,
        comment: comment,
        uuid: 'uuid'
      )

      assert shared_comment.save!
      assert_equal('uuid', shared_comment.uuid)
    end
  end

  describe "#for_partner" do
    it 'basics' do
      comment = events(:create_comment_for_minimum_ticket_1)
      shared_comment = comment.create_shared_comment

      ts_comment = shared_comment.for_partner
      assert_equal(shared_comment.uuid, ts_comment.uuid)
      assert_equal(comment.body, ts_comment.body)
      assert_equal(comment.html_body, ts_comment.html_body)
      assert_equal(comment.is_public?, ts_comment.public?)
      assert_equal(comment.created_at, ts_comment.authored_at.to_time)
      assert_not_nil(ts_comment.author)
      assert_nil(ts_comment.custom_fields)
    end

    it "comments by facebook end users" do
      account = accounts(:minimum)
      requester = user_identities(:minimum_end_user_facebook)
      comment = FacebookComment.create(
        account: account, ticket: account.tickets.first,
        author: requester.user, body: "test comment", value_previous: "152318411530606_198907950204985_890699",
        value_reference: {
          type: "comment",
          via_zendesk: false,
          requester: {
            name: "Zendeskian Zen",
            id: "100003222416535"
          },
          content: "test comment"
        },
        via_id: ViaType.FACEBOOK_POST,
        via_reference_id: 2
      )
      shared_comment = comment.create_shared_comment
      ts_comment = shared_comment.for_partner
      assert_equal "Zendeskian Zen", ts_comment.author.name
    end

    it "facebooks message attachments" do
      account = accounts(:minimum)
      requester = user_identities(:minimum_end_user_facebook)
      comment = FacebookComment.create(
        account: account,
        ticket: account.tickets.first,
        author_id: requester.user.id,
        body: "test comment",
        value_previous: "t.63487562348956",
        value_reference: {type: "message", via_zendesk: false, attachments: [{ 'id' => 'a', 'mime_type' => "image/jpeg", 'name' => "abc.jpg", "size" => "262443"}], content: "test comment"},
        via_id: ViaType.FACEBOOK_MESSAGE,
        via_reference_id: 2
      )
      FacebookComment.any_instance.expects(:shared_attachments).once
      shared_comment = comment.create_shared_comment
      shared_comment.for_partner
    end

    it "facebooks messages without attachments" do
      account = accounts(:minimum)
      requester = user_identities(:minimum_end_user_facebook)
      comment = FacebookComment.create!(
        account: account, ticket: account.tickets.first,
        author: requester.user, body: "test comment", value_previous: "t.63487562348956",
        value_reference: {type: "message", via_zendesk: false, content: "test comment"},
        via_id: ViaType.FACEBOOK_MESSAGE, via_reference_id: 2
      )
      FacebookComment.any_instance.expects(:shared_attachments).never
      shared_comment = comment.create_shared_comment
      shared_comment.for_partner
    end

    it 'withs screencasts' do
      screencasts = [
        stub(for_partner: {'position' => "1", 'url' => "http://foo.com/abd123"}),
        stub(for_partner: {'position' => "2", 'url' => "http://foo.com/abd456"})
      ]
      Comment.any_instance.stubs(:screencasts).returns(screencasts)
      comment = events(:create_comment_for_minimum_ticket_1)
      shared_comment = comment.create_shared_comment

      ts_comment = shared_comment.for_partner
      first_screencast = ts_comment.custom_fields['zendesk']['metadata']['screencasts'].first
      assert_equal("http://foo.com/abd123", first_screencast['url'])
    end
  end
end
