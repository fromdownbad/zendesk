require_relative '../support/test_helper'
require_relative '../support/inbound_mail_rate_limit_test_helper'

SingleCov.covered!

describe InboundMailRateLimit do
  include InboundMailRateLimitTestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:inbound_mail_rate_limit) { create_inbound_mail_rate_limit }

  describe 'when retrieving inbound mail rate limits for an account' do
    let(:email) { "#{account.admins.first.email}_2" }

    before do
      inbound_mail_rate_limit.soft_delete
      create_inbound_mail_rate_limit(email: email)
    end

    it 'excludes deleted inbound mail rate limits' do
      assert_equal 1, account.inbound_mail_rate_limits.length
      assert_equal email, account.inbound_mail_rate_limits.first.email
      refute account.inbound_mail_rate_limits.any?(&:deleted?)
    end
  end
end
