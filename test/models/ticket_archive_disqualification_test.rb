require_relative "../support/test_helper"

SingleCov.covered!

describe TicketArchiveDisqualification do
  fixtures :tickets

  describe '#disqualify_ticket!' do
    let(:ticket) { tickets(:minimum_1) }
    it 'creates a new record' do
      disqualification = TicketArchiveDisqualification.disqualify_ticket!(
        ticket,
        metric: TicketArchiveDisqualification::BYTESIZE,
        value: 3.megabytes
      )

      disqualification.id.must_equal         ticket.id
      disqualification.account_id.must_equal ticket.account_id
      disqualification.metric.must_equal     1
      disqualification.value.must_equal      3.megabytes

      ticket.ticket_archive_disqualification.wont_be_nil
    end
  end
end
