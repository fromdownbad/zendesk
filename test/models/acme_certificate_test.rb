require_relative '../support/test_helper'
require_relative "../support/certificate_test_helper"
# To record the VCR tests:
# This is a pain since you need to host a challenge on a server that LE can access
# This is hard to do when running classic tests since:
#   - Typically your classic is not a public address with port 80 open to the world
#   - Tests run in a DB transation so even if it was classic doesn't have access to the test DB
#
# Therefore when recording these tests you need to have a seperate server
# that LE can access. When recording these tests will pause and let you
# transfer the challenge to that server
#
# 1. Setup an A record to point home.zd-test.com to your public ip address
# 2. Verify that http://home.zd-test.com is mapped through nginx to classic

SingleCov.covered! uncovered: 12

describe "AcmeCertificate" do
  include CertificateTestHelper
  fixtures :accounts, :certificate_authorities

  let(:account) { accounts(:minimum) }
  let(:registration) { account.acme_registrations.first || AcmeRegistration.new(account: account) }
  let(:authorization) { registration.authorization_for_domain(account.host_mapping) }
  let(:former_certificate) { create_certificate!(state: 'active', create_ip: true, account: account) }
  subject { AcmeCertificate.new(account: account, client: registration.client) }
  let(:is_recording?) { VCR.current_cassette.recording? }

  describe 'with let_encrypt_acme_v2 arturo off' do
    before do
      Arturo.disable_feature!(:lets_encrypt_acme_v2)
      account.update_attribute(:host_mapping, '2321863f.ngrok.io')
      assert_equal 'active', former_certificate.state
    end

    describe "#request_certificate!" do
      before do
        VCR.turn_on!
        VCR.insert_cassette("acme_certificate_request_v2")
        Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)

        register_and_authorize!
      end

      after do
        VCR.eject_cassette
      end

      it "installs the intermediate if needed" do
        assert_empty CertificateAuthorities.where(is_root: false, subject: '/CN=Fake LE Intermediate X1')
        subject.request_certificate!
        subject.install_certificate!

        assert_equal 1, CertificateAuthorities.where(is_root: false, subject: '/CN=Fake LE Intermediate X1').count
      end

      it "marks the certificate as from Let's Encrypt and saves it" do
        subject.request_certificate!
        subject.install_certificate!

        assert(subject.certificate.autoprovisioned)
        assert(subject.certificate.sni_enabled)
        assert_equal "https://acme-staging.api.letsencrypt.org/acme/cert/fa72900eead3229045be76670264f4d7f026", subject.certificate.provisioning_url
        assert_equal false, subject.certificate.changed?
      end

      it "requests a certificate from LE and provisions it" do
        subject.request_certificate!
        subject.install_certificate!

        refute_nil subject.certificate.crt
        assert_equal 'active', subject.certificate.state
      end

      it "creates a sni certificate ip record" do
        subject.request_certificate!
        subject.install_certificate!

        assert_equal 1, subject.certificate.certificate_ips.size
        assert(subject.certificate.certificate_ips[0].sni)
        assert_equal account.pod_id, subject.certificate.certificate_ips[0].pod_id
      end

      it "removes any active certificates" do
        subject.request_certificate!
        subject.install_certificate!
        former_certificate.reload

        assert_equal 'active', subject.certificate.state
        assert_equal 'revoked', former_certificate.state
        assert_empty former_certificate.certificate_ips
      end
    end

    describe "with an existing LE certificate" do
      before do
        account.update_attribute(:host_mapping, '12d024ea.ngrok.io')
        VCR.turn_on!
        VCR.insert_cassette("acme_certificate_renew_v2")
        Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)

        register_and_authorize!
        subject.request_certificate!
        subject.install_certificate!
        @previous_certificate = account.certificates.active.first
      end

      after do
        VCR.eject_cassette
      end

      it "cannot be renewed until 15 before it expires" do
        Timecop.freeze(subject.certificate.crt_object.not_after - 16.days) do
          assert_equal false, subject.needs_renewal?
        end
      end

      it "can be renewed 14 days before it expires" do
        Timecop.freeze(subject.certificate.crt_object.not_after - 14.days) do
          assert(subject.needs_renewal?)
        end
      end

      it "can be renewed after it expires" do
        Timecop.freeze(subject.certificate.crt_object.not_after + 1.day) do
          assert(subject.needs_renewal?)
        end
      end

      it "renews an existing certificate and installs it" do
        acme_certificate = AcmeCertificate.new(account: account, client: registration.client, certificate: subject.certificate)
        Timecop.travel(Time.now + 10) if is_recording?
        acme_certificate.request_certificate!
        acme_certificate.install_certificate!

        renewed_certificate = account.certificates.active.first

        refute_equal @previous_certificate.crt_object.serial.to_s, renewed_certificate.crt_object.serial.to_s
        assert_equal @previous_certificate.crt_object.public_key.to_s, renewed_certificate.crt_object.public_key.to_s
        assert_equal 'active', renewed_certificate.state
      end
    end

    describe "#generate_csr!" do
      before { subject.generate_csr! }

      it "creates a new temporary certificate hosted via sni and autoprovisioned" do
        assert_equal 'temporary', subject.certificate.state
        assert(subject.certificate.sni_enabled)
        assert(subject.certificate.autoprovisioned)
        refute_nil subject.certificate.csr_object
      end

      it "creates a CSR that covers the hostmapping" do
        assert_equal "/C=/ST=City/L=City/O=Foo/OU=Foo/CN=#{account.host_mapping}", subject.certificate.csr_object.subject.to_s
      end
    end

    def replace_key(certificate)
      old_csr = R509::CSR.new(csr: certificate.csr)
      key = FixtureHelper::Certificate.read('account.key')
      certificate.key = Certificate.encrypt_field(key)
      certificate.csr = R509::CSR.new(subject: old_csr.subject, key: key, message_digest: 'sha256').to_pem
    end

    def register_and_authorize!
      registration.register!
      authorization.attempt_to_authorize!(verify_now: false)
      if is_recording?
        puts "Requested authorization for #{authorization.identifier}"
        puts "Host this challenge:"
        puts "\nURL: http://#{authorization.identifier}/.well-known/acme-challenge/#{authorization.challenge_token}"
        puts "Token: #{authorization.challenge_content}"
        puts "\nSleeping for 1 minute (use ctrl+c to interrupt)"
        done = false
        Signal.trap("SIGINT") do
          puts "\nrequesting verification"
          done = true
        end

        Timecop.travel(Time.now + 1) until done
      end

      authorization.attempt_to_authorize!(verify_now: true)
      Timecop.travel(Time.now + 2)
      authorization.increment!(:accesses)

      assert authorization.is_authorized?, "Let's Encrypt failed to authorize #{authorization.identifier}"

      # we need to replace the random private key in the CSR with a fixed key
      # so that the certificate issued in the VCR cassette matches the
      # key we have.
      subject.generate_csr!
      replace_key(subject.certificate)
    end
  end

  describe 'with let_encrypt_acme_v2 arturo on' do
    let(:status) { AcmeCertificateJobStatus.create!(account_id: account.id) }
    let(:activate_certificate) { true }
    let(:job) { AcmeCertificateJob.new(account.id, status.id, activate_certificate, SecureRandom.hex) }
    subject { job.send(:acme_certificate) }

    before do
      Arturo.enable_feature!(:lets_encrypt_acme_v2)
      account.update_attribute(:host_mapping, '1b6c463f.ngrok.io')
      assert_equal 'active', former_certificate.state
    end

    describe "#request_certificate!" do
      before do
        VCR.turn_on!
        VCR.insert_cassette("acme_certificate_request_v2-4")
        Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)

        register_and_authorize_v2!
      end

      after do
        VCR.eject_cassette
      end

      it "installs the intermediate if needed" do
        assert_empty CertificateAuthorities.where(is_root: false, subject: '/CN=Fake LE Intermediate X1')
        subject.request_certificate!
        subject.install_certificate!

        assert_equal 1, CertificateAuthorities.where(is_root: false, subject: '/CN=Fake LE Intermediate X1').count
      end

      it "marks the certificate as from Let's Encrypt and saves it" do
        subject.request_certificate!
        subject.install_certificate!

        assert(subject.certificate.autoprovisioned)
        assert(subject.certificate.sni_enabled)
        assert_equal "https://acme-staging-v02.api.letsencrypt.org/acme/cert/fa286e8029fd0c76800e75206f2884942087", subject.certificate.provisioning_url
        assert_equal false, subject.certificate.changed?
      end

      it "requests a certificate from LE and provisions it" do
        subject.request_certificate!
        subject.install_certificate!

        refute_nil subject.certificate.crt
        assert_equal 'active', subject.certificate.state
      end

      it "creates a sni certificate ip record" do
        subject.request_certificate!
        subject.install_certificate!

        assert_equal 1, subject.certificate.certificate_ips.size
        assert(subject.certificate.certificate_ips[0].sni)
        assert_equal account.pod_id, subject.certificate.certificate_ips[0].pod_id
      end

      it "removes any active certificates" do
        subject.request_certificate!
        subject.install_certificate!
        former_certificate.reload

        assert_equal 'active', subject.certificate.state
        assert_equal 'revoked', former_certificate.state
        assert_empty former_certificate.certificate_ips
      end
    end

    describe "with an existing LE certificate" do
      before do
        account.update_attribute(:host_mapping, 'daba514b.ngrok.io')
        VCR.turn_on!
        VCR.insert_cassette("acme_certificate_renew_v2-1")
        Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)

        register_and_authorize_v2!
        subject.request_certificate!
        subject.install_certificate!
        @previous_certificate = account.certificates.active.first
      end

      after do
        VCR.eject_cassette
      end

      it "cannot be renewed until 15 before it expires" do
        Timecop.freeze(subject.certificate.crt_object.not_after - 16.days) do
          assert_equal false, subject.needs_renewal?
        end
      end

      it "can be renewed 14 days before it expires" do
        Timecop.freeze(subject.certificate.crt_object.not_after - 14.days) do
          assert(subject.needs_renewal?)
        end
      end

      it "can be renewed after it expires" do
        Timecop.freeze(subject.certificate.crt_object.not_after + 1.day) do
          assert(subject.needs_renewal?)
        end
      end

      it "creates a new order and installs a new certificate" do
        register_and_authorize_v2!
        subject.request_certificate!
        subject.install_certificate!

        renewed_certificate = account.certificates.active.first

        refute_equal @previous_certificate.crt_object.serial.to_s, renewed_certificate.crt_object.serial.to_s
        assert_equal @previous_certificate.crt_object.public_key.to_s, renewed_certificate.crt_object.public_key.to_s
        assert_equal 'active', renewed_certificate.state
      end
    end

    describe "#generate_csr!" do
      before do
        VCR.turn_on!
        VCR.insert_cassette("acme_certificate_generate_csr")
        Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)
        subject.generate_csr!
      end

      after do
        VCR.eject_cassette
      end

      it "creates a new temporary certificate hosted via sni and autoprovisioned" do
        assert_equal 'temporary', subject.certificate.state
        assert(subject.certificate.sni_enabled)
        assert(subject.certificate.autoprovisioned)
        refute_nil subject.certificate.csr_object
      end

      it "creates a CSR that covers the hostmapping" do
        assert_equal "/C=/ST=City/L=City/O=Foo/OU=Foo/CN=#{account.host_mapping}", subject.certificate.csr_object.subject.to_s
      end
    end

    def replace_key(certificate)
      old_csr = R509::CSR.new(csr: certificate.csr)
      key = FixtureHelper::Certificate.read('account.key')
      certificate.key = Certificate.encrypt_field(key)
      certificate.csr = R509::CSR.new(subject: old_csr.subject, key: key, message_digest: 'sha256').to_pem
    end

    def register_and_authorize_v2!
      job.send(:register_and_authorize_order!)
      job.send(:check_pending_authorizations!)

      Timecop.travel(Time.now + 2)
      subject.generate_csr!
      replace_key(subject.certificate)
    end
  end
end
