require_relative "../support/test_helper"

SingleCov.covered! uncovered: 10

describe SugarCrmIntegration do
  fixtures :accounts, :users, :user_identities, :tickets
  should_validate_presence_of :url, :username, :password

  before do
    @account = accounts(:minimum)
    @user = users(:minimum_agent)
    @url = "http://sugar.something.com/sugarcrm"
    @username = "username"
    @password = "password"
    @sugar_crm_integration = SugarCrmIntegration.new(url: @url, username: @username, password: @password, account: @account)
    @user_info_xml = <<-XML
      <records>
        <record>
          <id>86d00708-378e-e244-8ead-4db6bb86e89d</id>
          <url>/index.php?module=Users&amp;action=DetailView&amp;record=86d00708-378e-e244-8ead-4db6bb86e89d</url>
          <label>Agent Minimum</label>
          <record_type>User</record_type>
          <fields>
            <field>
              <label>Title</label>
              <value>The Title with trademark&reg;</value>
            </field>
            <field>
              <label>Email</label>
            </field>
          </fields>
        </record>
        <record>
          <id>54386e5a-bee3-9fdf-a58b-4dd58549b6df</id>
          <url>/index.php?module=Accounts&amp;action=DetailView&amp;record=54386e5a-bee3-9fdf-a58b-4dd58549b6df</url>
          <label>Slender Broadband Inc</label>
          <record_type>Account</record_type>
          <fields>
            <field>
              <label>Office Phone:</label>
              <value>(748) 386-4842</value>
            </field>
          </fields>
        </record>
      </records>
    XML
    @hash = {
      records: [
        {
          label: "Agent Minimum",
          record_type: "User",
          fields: [
            {
              value: "The Title with trademark",
              label: "Title"
            },
            {
              label: "Email",
              value: ""
            }
          ],
          url: "#{@url}/index.php?module=Users&action=DetailView&record=86d00708-378e-e244-8ead-4db6bb86e89d",
          id: "86d00708-378e-e244-8ead-4db6bb86e89d"
        },
        {
          label: "Slender Broadband Inc",
          record_type: "Account",
          fields: [
            {
              value: "(748) 386-4842",
              label: "Office Phone:"
            }
          ],
          url: "#{@url}/index.php?module=Accounts&action=DetailView&record=54386e5a-bee3-9fdf-a58b-4dd58549b6df",
          id: "54386e5a-bee3-9fdf-a58b-4dd58549b6df"
        }
      ]
    }
  end

  subject { @sugar_crm_integration }

  describe "#fetch_info_for" do
    it "converts incoming xml to label/value hash" do
      @sugar_crm_integration.stubs(:get_user_info).returns(@user_info_xml)
      @sugar_crm_integration.stubs(:get_session_id).returns("1234")
      data = @sugar_crm_integration.fetch_info_for(@user)
      assert_equal(@hash, data)
    end
  end

  describe "Create and destroy the integration" do
    it "creates and destroy the target and trigger" do
      SugarCrmTarget.destroy_all
      assert_equal 0, SugarCrmTarget.count(:all)
      assert @sugar_crm_integration.save
      assert_equal 1, SugarCrmTarget.count(:all)
      target = SugarCrmTarget.last
      assert target.is_active?
      trigger = Trigger.find_by_title(target.send(:title_for_trigger))
      refute trigger.is_active?

      assert @sugar_crm_integration.destroy
      assert_equal 0, SugarCrmTarget.count(:all)
      refute Trigger.find_by_title(target.send(:title_for_trigger))
    end
  end

  describe "#create_case" do
    before do
      @sugar_crm_integration.stubs(:get_session_id).returns("1234")
    end

    it "hits expected endpoint" do
      @response = stub(status: 200, body: "OK 12AB")
      Zendesk::Net::MultiSSL.expects(:post).with("http://sugar.something.com/sugarcrm/custom/zendeskapi/v2/rest.php", anything).returns(@response)
      @sugar_crm_integration.create_case(tickets(:minimum_1))
    end

    describe "when the case is successfully created" do
      before do
        @response = stub(status: 200, body: "OK 12AB")
        Zendesk::Net::MultiSSL.expects(:post).returns(@response)
      end

      it "receives an 'OK CASEID'" do
        response = @sugar_crm_integration.create_case(tickets(:minimum_1))
        assert_equal "OK 12AB", response
      end
    end

    describe "when there's an error" do
      before do
        @response = stub(status: 200, body: "Bad data passed")
        Zendesk::Net::MultiSSL.expects(:post).returns(@response)
      end

      it "raises an exception" do
        assert_raise RuntimeError do
          @sugar_crm_integration.create_case(tickets(:minimum_1))
        end
      end
    end
  end

  describe "#fetch_info_for" do
    describe "when everything works fine" do
      before do
        @sugar_crm_integration.stubs(:get_user_info).returns(@user_info_xml)
      end

      it "fetchs the label/value hash" do
        data = @sugar_crm_integration.fetch_info_for(@user)
        assert_equal(@hash, data)
      end
    end

    describe "when user has no email" do
      before do
        @user.stubs(:email).returns(nil)
      end

      it "fetchs an empty hash" do
        data = @sugar_crm_integration.fetch_info_for(@user)
        assert_equal({records: []}, data)
      end
    end

    describe "when we found no records" do
      before do
        no_records = '<?xml version=\"1.0\"?>\n<records/>\n'
        @sugar_crm_integration.stubs(:get_user_info).returns(no_records)
      end

      it "fetchs an empty hash" do
        data = @sugar_crm_integration.fetch_info_for(@user)
        assert_equal({records: []}, data)
      end
    end

    describe "when there's a login error" do
      before do
        login_error = '{"name":"Invalid Login","number":"10","description":"Login attempt failed please check the username and password"}'
        @sugar_crm_integration.stubs(:send_login).returns(login_error)
      end

      it "raises an exception" do
        assert_raise RuntimeError do
          @sugar_crm_integration.fetch_info_for(@user)
        end
      end
    end

    describe "when there's a connection problem during login" do
      before do
        @response = stub(status: 500, body: "Internal Server Error")
        Zendesk::Net::MultiSSL.expects(:post).times(5).returns(@response)
      end

      it "retrys 5 times and finally raise an exception" do
        assert_raise RuntimeError do
          @sugar_crm_integration.fetch_info_for(@user)
        end
      end
    end

    describe "login path" do
      before do
        @response = stub(status: 200, body: "")
        Zendesk::Net::MultiSSL.expects(:post).with('http://sugar.something.com/sugarcrm/service/v2/rest.php', anything).returns(@response)
      end

      it "sends login with the correct path" do
        @sugar_crm_integration.send(:send_login)
      end
    end
  end
end
