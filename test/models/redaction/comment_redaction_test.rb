require_relative "../../support/test_helper"
require_relative "../../support/attachment_test_helper"
require 'attachment_fu'

SingleCov.covered! uncovered: 3

describe Redaction::CommentRedaction do
  fixtures :accounts, :events, :tickets, :users, :attachments

  let(:ticket) { tickets(:minimum_1) }
  let(:user) { users(:minimum_agent) }
  let(:path_prefix) { "#{Rails.root}/test/files" }

  def add_comment(body)
    ticket.add_comment(is_public: true, body: body)
    ticket.will_be_saved_by(user)
    ticket.save!
    yield ticket.comments.last
  end

  def new_comment_redaction(comment)
    @redaction = Redaction::CommentRedaction.new
    @redaction.user = user
    @redaction.ticket = ticket
    @redaction.comment = comment
    ticket.will_be_saved_by(user)
    @redaction.audit = ticket.audit
    @redaction.external_attachment_urls = []
  end

  describe "execute!" do
    it "returns the original html string if there is nothing to be redact" do
      text = "sample text"
      add_comment("sample text") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text)
        assert_equal comment.body, redacted[:text]
      end
    end

    it "returns the original html string with an error if the redaction comment is different from the original comment text" do
      text_for_redaction = "<div class=\"zd-comment\" dir=\"auto\">Hello <bInternal</b> Att<redact>ach</redact><br><br><div class=\"redacted-image-content\">Image redacted</div><br></div>"
      add_comment("<div class=\"zd-comment\" dir=\"auto\">Hello <b>Internal</b> Attachment<br><br><div class=\"redacted-image-content\">Image redacted</div><br></div>") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal text_for_redaction, redacted[:text]
        assert_equal "Comment does not match the redaction text", redacted[:error][:message]
      end

      text_for_redaction = "Hello <b>Internal</b><img src=\"some-image.png\" redact />"
      add_comment("Hello <b>Interl </b><img src=\"some-image.png\" redact />") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal text_for_redaction, redacted[:text]
        assert_equal "Comment does not match the redaction text", redacted[:error][:message]
      end

      text_for_redaction = "Hello <b>Internal</b><a href=\"some-image.png\" redact />"
      add_comment("Hello <b>Interl </b><a href=\"some-image.png\" redact />") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal text_for_redaction, redacted[:text]
        assert_equal "Comment does not match the redaction text", redacted[:error][:message]
      end
    end

    it "returns the html string with filled redaction characters for the element with redact" do
      text = "sam<redact>ple</redact> text"
      add_comment("sample text") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text)
        assert_equal "sam▇▇▇ text", redacted[:text]
      end
    end

    it "returns the html string with filled redaction characters for the redact element wrapped by bold" do
      text = "sam<b><redact>ple</redact></b> text"
      add_comment("sam<b>ple</b> text") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text)
        assert_equal "sam<b>▇▇▇</b> text", redacted[:text]
      end
    end

    it "returns the html string with filled redaction characters for the redact element containing bold" do
      text = "sam<redact><b>ple</b></redact> text"
      add_comment("sam<b>ple</b> text") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text)
        assert_equal "sam<b>▇▇▇</b> text", redacted[:text]
      end
    end

    it "returns the html string properly redacted when the redaction is in the beginning" do
      text = "<redact>sample</redact> text"
      add_comment("sample text") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text)
        assert_equal "▇▇▇▇▇▇ text", redacted[:text]
      end
    end

    it "returns the html string properly redacted when the redaction is in the ending" do
      text = "sample<redact> text</redact>"
      add_comment("sample text") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text)
        assert_equal "sample▇▇▇▇▇", redacted[:text]
      end
    end

    it "returns the html string properly redacted when the redaction text containing an apostrophe" do
      text = "sample<redact>` text</redact>"
      add_comment("sample` text") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text)
        assert_equal "sample▇▇▇▇▇▇", redacted[:text]
      end
    end

    it "returns the html string properly redacted when the redaction text is multiline" do
      text = "this is a <redact>multi line</redact>  <br> <redact>sample</redact> text <br> for redaction"
      add_comment("this is a multi line  <br> sample text <br> for redaction") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text)
        assert_equal "this is a ▇▇▇▇▇▇▇▇▇▇  <br> ▇▇▇▇▇▇ text <br> for redaction", redacted[:text]
      end
    end

    it "returns the html string properly redacted when the redaction text span multiple list items" do
      text = <<~TEXT
        <ul>
        <li>Image should not be editable.</li>
        <li>Attachment should not be editable. I.e. `x` icon (delete function) should not be present.</li>
        </ul>
      TEXT

      text_for_redaction = <<~TEXT
        <ul>
        <li>Image should not be <redact>editable.</redact></li>
        <li><redact>Attachment</redact> should not be editable. I.e. `x` icon (delete function) should not be present.</li>
        </ul>
      TEXT

      expected_text = <<~TEXT
        <ul>
        <li>Image should not be ▇▇▇▇▇▇▇▇▇</li>
        <li>▇▇▇▇▇▇▇▇▇▇ should not be editable. I.e. `x` icon (delete function) should not be present.</li>
        </ul>
      TEXT

      add_comment(text) do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal expected_text, redacted[:text]
      end
    end

    it "returns the html for chinese text " do
      text_for_redaction = "this is a chinese text 中國哲學書<redact>電子化</redact>"
      add_comment("this is a chinese text 中國哲學書電子化") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal "this is a chinese text 中國哲學書▇▇▇", redacted[:text]
      end
    end

    it "returns the redacted html for japanese text - kanji" do
      text_for_redaction = "this is a japanese text 金魚<redact>金魚</redact>"
      add_comment("this is a japanese text 金魚金魚") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal "this is a japanese text 金魚▇▇", redacted[:text]
      end
    end

    it "returns the redacted html for japanese text - Hiragana" do
      text_for_redaction = "this is a japanese text きんぎょ<redact>きんぎょ</redact>"
      add_comment("this is a japanese text きんぎょきんぎょ") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal "this is a japanese text きんぎょ▇▇▇▇", redacted[:text]
      end
    end

    it "returns the redacted html for japanese text - Katakana" do
      text_for_redaction = "this is a japanese text キンギョ<redact>キンギョ</redact>"
      add_comment("this is a japanese text キンギョキンギョ") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal "this is a japanese text キンギョ▇▇▇▇", redacted[:text]
      end
    end

    it "returns the redacted html for korean text " do
      text_for_redaction = "this is a korean text (국<redact>민 0125</redact>02 04 001501 채경희)"
      add_comment("this is a korean text (국민 012502 04 001501 채경희)") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal "this is a korean text (국▇▇▇▇▇▇02 04 001501 채경희)", redacted[:text]
      end
    end

    it "returns redacted html for code element" do
      text = "<code><redact>createPopper(re</redact>ference, popper, {<br> placement:'right-start', });<br></code>"
      text_for_redaction = "<code><redact>createPopper(re</redact>ference, popper, {<br> placement:'right-start', });<br></code>"
      expected_text = "<code>▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ference, popper, {<br> placement:'right-start', });<br></code>"

      add_comment(text) do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal expected_text, redacted[:text]
      end
    end

    it "returns redacted html for table element" do
      text = <<~TEXT
        <table>
        <tbody>
        <tr>
        <td>none</td>
        <td>Default value</td>
        </tr>
        </tbody>
        </table>"
      TEXT
      text_for_redaction = <<~TEXT
        <table>
        <tbody>
        <tr>
        <td><redact>none</redact></td>
        <td><redact>Default value</redact></td>
        </tr>
        </tbody>
        </table>"
      TEXT
      expected_text = <<~TEXT
        <table>
        <tbody>
        <tr>
        <td>▇▇▇▇</td>
        <td>▇▇▇▇▇▇▇▇▇▇▇▇▇</td>
        </tr>
        </tbody>
        </table>"
      TEXT

      add_comment(text) do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal expected_text, redacted[:text].to_s
      end
    end

    it 'parses bad html and returns html redacted' do
      redaction_text = "sa<redact>m<b>p</redact>le</b> text"
      add_comment("sample text") do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(redaction_text)

        assert_equal "sa▇<b>▇</b><b>le</b> text", redacted[:text]
      end
    end

    it "replaces the a tag if it has to be redacted with the redacted text version of the element" do
      text = "<a href='www.google.com'>Link to Google</a>"
      text_for_redaction = "<a href='www.google.com' redact>Link to <redact>Google</redact></a>"
      expected_text = "Link to ▇▇▇▇▇▇"

      add_comment(text) do |comment|
        new_comment_redaction(comment)
        redacted = @redaction.execute!(text_for_redaction)
        assert_equal expected_text, redacted[:text]
      end
    end

    describe "while redacting attachments" do
      it "returns the inline images redacted and adds an AttachmentRedactionEvent if the image has redact" do
        attachment =  attachments(:inline)
        text = <<~TEXT
          <img src="#{attachment.url}" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined" redact="">
          <br>
          <img src="https://z3n-orchid-prod.zendesk.com/attachments/token/WHrGw3oT4ExnJbO3SW3J2Tjqi/?name=Screen+Shot+2020-08-19+at+11.08.27+AM.png" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined">
          <br>
        TEXT

        text_for_redaction = <<~TEXT
          <img src="#{attachment.url}" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined" redact="">
          <br>
          <img src="https://z3n-orchid-prod.zendesk.com/attachments/token/WHrGw3oT4ExnJbO3SW3J2Tjqi/?name=Screen+Shot+2020-08-19+at+11.08.27+AM.png" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined">
          <br>
        TEXT

        expected_text = <<~TEXT
          #{Redaction::CommentRedaction::REDACTED_INLINE_IMAGE_NODE}
          <br>
          <img src="https://z3n-orchid-prod.zendesk.com/attachments/token/WHrGw3oT4ExnJbO3SW3J2Tjqi/?name=Screen+Shot+2020-08-19+at+11.08.27+AM.png" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined">
          <br>
        TEXT

        add_comment(text) do |comment|
          attachment.source = comment
          attachment.save!

          stub_request(:put, %r{/redacted.txt})

          attachment.source.stubs(:try).with(:add_redacted_attachment!).returns(true)
          Attachment.any_instance.stubs(:destroy).returns(true)

          new_comment_redaction(comment)

          redacted = @redaction.execute!(text_for_redaction)

          assert ticket.audit.events.detect { |e| e.class == AttachmentRedactionEvent }
          assert_equal expected_text, redacted[:text].to_s
        end
      end

      it "returns the inline images redacted and does not add AttachmentRedactionEvent for inline images pointing outside of zendesk cdn " do
        attachment =  attachments(:inline)
        text = <<~TEXT
          <img src="https://www.apple.com/newsroom/images/product/mac/standard/Apple_next-generation-mac-macbookair-macbookpro-mac-mini_11102020_Full-Bleed-Image.jpg.large.jpg" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined" redact="">
          <br>
          <img src="https://z3n-orchid-prod.zendesk.com/attachments/token/WHrGw3oT4ExnJbO3SW3J2Tjqi/?name=Screen+Shot+2020-08-19+at+11.08.27+AM.png" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined">
          <br>
        TEXT

        text_for_redaction = <<~TEXT
          <img src="https://www.apple.com/newsroom/images/product/mac/standard/Apple_next-generation-mac-macbookair-macbookpro-mac-mini_11102020_Full-Bleed-Image.jpg.large.jpg" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined" redact="">
          <br>
          <img src="https://z3n-orchid-prod.zendesk.com/attachments/token/WHrGw3oT4ExnJbO3SW3J2Tjqi/?name=Screen+Shot+2020-08-19+at+11.08.27+AM.png" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined">
          <br>
        TEXT

        expected_text = <<~TEXT
          #{Redaction::CommentRedaction::REDACTED_INLINE_IMAGE_NODE}
          <br>
          <img src="https://z3n-orchid-prod.zendesk.com/attachments/token/WHrGw3oT4ExnJbO3SW3J2Tjqi/?name=Screen+Shot+2020-08-19+at+11.08.27+AM.png" style="height:auto; width:undefinedpx" data-original-width="undefined" data-original-height="undefined">
          <br>
        TEXT

        add_comment(text) do |comment|
          attachment.source = comment
          attachment.save!

          stub_request(:put, %r{/redacted.txt})

          new_comment_redaction(comment)

          redacted = @redaction.execute!(text_for_redaction)

          refute ticket.audit.events.detect { |e| e.class == AttachmentRedactionEvent }
          assert_equal expected_text, redacted[:text].to_s
        end
      end

      it "does not apply redacted to existing redacted inline images or its text" do
        attachment = attachments(:inline)
        text = <<~TEXT
          <div class="redacted-image-content">Image redacted</div>
          <br>
          Hello
          <br>
        TEXT

        text_for_redaction = <<~TEXT
          <div class="redacted-image-content"> Image <redact>redacted</redact></div>
          <br>
          <redact>Hello</redact>
          <br>
        TEXT

        expected_text = <<~TEXT
          #{Redaction::CommentRedaction::REDACTED_INLINE_IMAGE_NODE}
          <br>
          ▇▇▇▇▇
          <br>
        TEXT

        add_comment(text) do |comment|
          attachment.source = comment
          attachment.save!

          stub_request(:put, %r{/redacted.txt})

          new_comment_redaction(comment)

          redacted = @redaction.execute!(text_for_redaction)

          refute ticket.audit.events.detect { |e| e.class == AttachmentRedactionEvent }
          assert_equal expected_text, redacted[:text].to_s
        end
      end
      it "redacts external attachments and adds an attachment redaction event" do
        attachment = attachments(:attachment_entry)
        redaction_text = "sa<redact>mple</redact> text"
        add_comment("sample text") do |comment|
          attachment.source = comment
          attachment.save!

          stub_request(:put, %r{/redacted.txt})

          attachment.source.stubs(:try).with(:add_redacted_attachment!).returns(true)
          Attachment.any_instance.stubs(:destroy).returns(true)

          new_comment_redaction(comment)

          @redaction.external_attachment_urls = [attachment.url]
          redacted = @redaction.execute!(redaction_text)

          assert ticket.audit.events.detect { |e| e.class == AttachmentRedactionEvent }
          assert_equal "sa▇▇▇▇ text", redacted[:text].to_s
        end
      end
    end
  end
end
