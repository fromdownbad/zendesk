require_relative "../support/test_helper"
require_relative "../support/voice_test_helper"

SingleCov.covered! uncovered: 8

describe Group do
  fixtures :groups, :organizations, :memberships, :users, :user_identities, :tickets

  should_validate_presence_of :name

  before do
    @organization = organizations(:group_organization)
    @user         = users(:with_groups_admin)
    @group        = groups(:with_groups_group1)
    @account      = accounts(:with_groups)

    @group.current_user = @user
  end

  it "fetchs users via preload" do
    Group.includes(:users).to_a
  end

  describe 'create' do
    describe 'with group name uniqueness checking' do
      let(:existing_group) { Group.first }
      let(:last_group) { Group.last }
      before do
        object = mock.tap { |settings| settings.stubs(:check_group_name_uniqueness?).returns true }
        Account.any_instance.stubs(:settings).returns object
      end

      it 'checks name uniqueness for new records' do
        new_group = existing_group.dup
        assert new_group.invalid?
        assert_includes new_group.errors, :name
      end

      describe 'for existing records' do
        before { existing_group.update_column(:account_id, last_group.account_id) }

        it 'allows duplicate name for existing group' do
          existing_group.update_column(:name, last_group.name)
          assert existing_group.valid?
        end

        it 'disallows duplicate name if changed for existing group' do
          existing_group.name = last_group.name
          assert existing_group.invalid?
          assert_includes existing_group.errors, :name
        end
      end

      describe 'for deleted records' do
        let(:old_group) { @account.groups.create!(name: 'test_deleted_records') }

        before do
          old_group.stubs(:remove_from_phone_group_routing!)
          old_group.current_user = @user
          old_group.deactivate!
        end

        it 'allows a new group with same name' do
          new_group = @account.groups.new(name: old_group.name)
          assert new_group.valid?
        end

        it 'validates when activated' do
          @account.groups.create!(name: old_group.name)
          assert_raise(ActiveRecord::RecordInvalid) { old_group.activate! }
        end

        it 'allows deletion of group with same name' do
          new_group = @account.groups.create!(name: old_group.name)
          new_group.stubs(:remove_from_phone_group_routing!)
          new_group.current_user = @user
          assert new_group.deactivate!
        end
      end
    end
  end

  describe "update" do
    it "fails validation if the name is too long" do
      @group.update_attributes(name: 'a' * 101)
      assert_equal [I18n.t('txt.admin.models.group.name_invalid')], @group.errors["base"]
    end

    it 'fails validation if the description is too long' do
      @group.update_attributes(description: 'a' * 1025)
      assert_equal [I18n.t('activerecord.errors.messages.too_long.other', count: 1024)], @group.errors['description']
    end

    describe 'when there is already a default group for the account' do
      describe 'and another group is set as the default' do
        it 'all other groups are unset as the default group' do
          @group = groups(:with_groups_group2)
          @group.default = true;
          @group.save!
          refute @account.groups.where.not(id: @group.id).first.default
        end
      end
    end
  end

  describe "deletion" do
    before do
      @group.memberships.update_all(default: false)
      @group.stubs(:remove_from_phone_group_routing!)
    end

    describe "when default" do
      before do
        @group.update_attributes!(default: true)
      end

      it "is not deleteable" do
        @group.deactivate

        assert @group.errors[:default].any?
      end
    end

    describe "when not default" do
      before do
        @group.update_attributes!(default: false)
        assert @group.is_active?
      end

      it "is not deleteable if it's the only group" do
        group   = Group.new
        account = Account.new

        groups = []
        groups.stubs(:count).returns(1)
        account.stubs(:groups).returns(groups)
        group.stubs(:account).returns(account)
        group.stubs(:is_active?).returns(false)

        group.send :validate_is_active
        assert_equal [I18n.t('txt.admin.models.group.cannot_delete_last_group')], group.errors["base"]
      end

      it "is not deletable if current_user is not set on the group" do
        @group.current_user = nil
        assert_raise(RuntimeError) { @group.deactivate! }
        @group.current_user = @user
        assert @group.deactivate!
      end

      it "is not deletable if it's the default group for any agents" do
        @group.agents = @account.agents.pluck(:id)
        @group.save!
        @group.memberships.update_all(default: true)
        @group.current_user = @user
        assert_raise(ActiveRecord::RecordInvalid) { @group.deactivate! }
        expected = {default_group: ["John Doe", "with_groups_admin", "with_groups_agent_assigned_restricted", "..."]}
        assert_equal expected, @group.errors.messages
      end

      it "does not generate Np1 membership removals" do
        @group.agents = @account.agents.pluck(:id)
        @group.save!
        @group.current_user
        assert_sql_queries 5, /`memberships`/i do
          @group.deactivate!
        end
      end

      it "gets marked deleted rather than really get deleted" do
        assert @group.deactivate!
        refute @group.reload.is_active?
      end

      it "enqueues a job to resque to drop associations to tickets and organizations when deleted" do
        Resque.expects(:enqueue).with(GroupDeleteJob, @group.account_id, anything, @group.id, anything)

        assert_equal @group, @organization.group
        assert_equal 3, @group.tickets.count(:all)
        assert @group.deactivate!
      end
    end
  end

  describe '#description' do
    before { @group = Group.new }

    it 'returns an empty string' do
      assert_equal '', @group.description
    end
  end

  describe "#to_liquid" do
    before { @group = Group.new }

    it "calls #name" do
      @group.expects :name
      @group.to_liquid
    end
  end

  describe '#settings=' do
    it 'delegates setting assignment to .set' do
      group = @account.groups.new.tap do |new_group|
        new_group.attributes = { name: 'new group from chat', settings: { chat_enabled: true } }
      end
      group.save!

      assert_equal false, GroupSetting.default(:chat_enabled)
      assert_equal true, group.reload.settings.chat_enabled
    end
  end

  describe "Removing users from a group" do
    before do
      @group = groups(:with_groups_group1)
      @group.current_user = users(:with_groups_admin)
    end

    it "creates an UnassignTicketsJob with the appropriate attributes for each removed users" do
      assert @group.users.length > 1, "In this context, the Group must have at least two users"
      @group.users.each do |user|
        Resque.expects(:enqueue).with(UnassignTicketsJob, @group.account_id, @group.id, user.id, @group.current_user.id, ViaType.GROUP_CHANGE, anything, anything)
      end

      @group.agents = []
      @group.save!
    end
  end

  describe "#agents=" do
    it "will add agents to group" do
      group = groups(:with_groups_group1)
      group.current_user = users(:with_groups_admin)
      agent_one = users(:with_groups_agent1)
      agent_two = users(:with_groups_agent2)
      group.agents = [agent_one.id, agent_two.id]
      group.save!
      assert_equal 2, Membership.where(group_id: group.id).to_a.size
    end

    it "does not be destructive until save" do
      group = groups(:with_groups_group1)
      group.current_user = users(:with_groups_admin)
      current_memberships = Membership.where(group_id: group.id).to_a
      group.agents = []

      assert_equal current_memberships, Membership.where(group_id: group.id).to_a

      group.save!
      assert_equal [], Membership.where(group_id: group.id).to_a
    end
  end

  describe "removing group from phone number group routing after deactivating a group" do
    before do
      @group = groups(:with_groups_group1)
      @group.stubs(:deactivate)
      Voice::PhoneNumber.any_instance.stubs(:unassign_group)
    end

    it "calls #remove_from_phone_group_routing! after deactivation" do
      @group.expects(:remove_from_phone_group_routing!).once
      @group.deactivate!
    end
  end

  describe '.assignable' do
    describe_with_arturo_enabled :groups do
      it 'returns all of the groups in an account' do
        @user = users(:with_groups_agent1)
        @account = accounts(:with_groups)
        assert_equal @account.groups.size, @account.groups.assignable(@user).count(:all)
      end

      describe 'Given a groups role restriction' do
        before do
          @user = users(:with_groups_agent1)
          @user.restriction_id = RoleRestrictionType.GROUPS
          @account = accounts(:with_groups)
        end

        describe '#assignable_groups' do
          it 'returns only the users groups' do
            assert_equal(@user.groups.size, Group.assignable(@user).count(:all))
          end
        end

        describe "with a user that can assign to any group" do
          before { @user.stubs(:can?).with(:assign_to_any_group, Ticket).returns(true) }

          it "returns all groups" do
            assert_equal @account.groups.count(:all), Group.assignable(@user).count(:all)
          end
        end
      end

      describe 'Given an agent restricted to assigned tickets only' do
        before do
          @user = users(:with_groups_agent1)
          @user.restriction_id = RoleRestrictionType.Assigned
          @account = accounts(:with_groups)
        end

        describe '#assignable_groups' do
          it 'returns only the users groups' do
            assert_equal(@user.groups.size, Group.assignable(@user).count(:all))
          end
        end
      end
    end
  end

  describe ".with_name_like" do
    it "find group like" do
      @group.name = "New group name"
      @group.save
      assert_includes Group.with_name_like("new"), @group
    end

    it "find group with escape character" do
      @group.name = "Gro%up"
      @group.save
      assert_includes Group.with_name_like("gro%"), @group
    end
  end
end
