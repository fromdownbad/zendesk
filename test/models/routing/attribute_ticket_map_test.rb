require_relative '../../support/test_helper'
require_relative '../../support/routing'

SingleCov.covered!

describe AttributeTicketMap do
  include TestSupport::Routing::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }

  before { account.attribute_ticket_maps.destroy_all }

  describe 'when deleted' do
    let(:map) { new_attribute_ticket_map }

    before { CIA.audit(actor: account.owner) { map.soft_delete } }

    it 'is audited' do
      assert map.cia_events.where(action: 'destroy').any?
    end

    describe 'with an invalid condition' do
      let!(:invalid_attribute_ticket_map) do
        atm = new_attribute_ticket_map(
          attribute_id: 'mock_attribute_id',
          attribute_value_id: 'mock_attribute_value_id'
        )
        atm.definition = Definition.build(
          conditions_all: [
            {
              field:    'status_id',
              operator: 'less_than',
              value:    StatusType.NEW
            }
          ]
        )
        atm.save!(validate: false)
        atm
      end

      it "deletes the attribute_ticket_map" do
        refute invalid_attribute_ticket_map.valid?
        assert_sql_queries(1, /UPDATE `attribute_ticket_maps` SET `deleted_at` =/) do
          invalid_attribute_ticket_map.soft_delete
        end
        assert invalid_attribute_ticket_map.deleted?
      end
    end
  end

  describe 'default scope' do
    before do
      new_attribute_ticket_map.soft_delete
      new_attribute_ticket_map(attribute_id: '2')

      account.reload
    end

    it 'excludes deleted maps' do
      refute account.attribute_ticket_maps.any?(&:deleted?)
    end
  end

  describe 'validation' do
    describe 'definition' do
      describe 'when invalid' do
        it 'is invalid' do
          assert_raises ActiveRecord::RecordInvalid do
            new_attribute_ticket_map(
              conditions_all: [
                {
                  field:    'status_id',
                  operator: 'less_than',
                  value:    StatusType.NEW
                }
              ]
            )
          end
        end
      end

      describe 'when too large' do
        it 'is invalid' do
          assert_raises ActiveRecord::RecordInvalid do
            new_attribute_ticket_map(comment: '.' * 70_000)
          end
        end
      end

      describe 'when valid' do
        it 'is valid' do
          assert new_attribute_ticket_map(comment: 'blah').valid?
        end
      end
    end
  end

  describe '#match?' do
    let(:ticket) { tickets(:minimum_1) }

    let(:map) { new_attribute_ticket_map(comment: 'match') }

    describe 'when the ticket matches the conditions' do
      before { ticket.comment = Comment.new(body: 'match') }

      it 'returns true' do
        assert map.match?(ticket)
      end
    end

    describe 'when the ticket does not match the conditions' do
      before { ticket.comment = Comment.new(body: 'no_match') }

      it 'returns false' do
        refute map.match?(ticket)
      end
    end
  end

  describe '#rule_type' do
    let(:map) { new_attribute_ticket_map }

    it 'returns the rule type' do
      assert_equal 'attribute_ticket_map', map.rule_type
    end
  end
end
