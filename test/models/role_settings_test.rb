require_relative '../support/test_helper'

SingleCov.covered!

describe RoleSettings do
  fixtures :accounts

  before do
    @account       = accounts(:minimum)
    @role_settings = @account.build_role_settings
  end

  describe "#disable_agent_social_login!" do
    before do
      @role_settings.update_attributes!(
        agent_twitter_login: true,
        agent_facebook_login: true
      )

      @role_settings.disable_agent_social_login!
    end

    it "disables agent login with Twitter" do
      refute @role_settings.agent_twitter_login
    end

    it "disables agent login with Facebook" do
      refute @role_settings.agent_facebook_login
    end
  end

  describe "#agent_security_policy_id" do
    describe "when the policy ID is not valid" do
      before { @role_settings.agent_security_policy_id = 999 }

      it "fails to save" do
        refute @role_settings.save
      end
    end

    describe "when the policy ID is valid" do
      before { @role_settings.agent_security_policy_id = 100 }

      it "saves successfully" do
        assert @role_settings.save
      end
    end
  end

  describe "#end_user_security_policy_id" do
    describe "when the policy ID is not valid" do
      before { @role_settings.end_user_security_policy_id = 999 }

      it "fails to save" do
        refute @role_settings.save
      end
    end

    describe "when the policy ID is valid" do
      before { @role_settings.end_user_security_policy_id = 100 }

      it "saves successfully" do
        assert @role_settings.save
      end
    end
  end

  describe "#security_policy_id_for_role" do
    before { @role_settings.agent_security_policy_id = 300 }

    it "fetchs the security policy ID for the role" do
      assert_equal 300, @role_settings.security_policy_id_for_role(:agent)
    end
  end

  describe "#login_allowed_for_role?" do
    before { @role_settings.agent_facebook_login = true }

    it "returns whether login is allowed for the role/service pair" do
      assert @role_settings.login_allowed_for_role?(:facebook, :agent)
    end
  end

  describe "#no_login_service_allowed_for_role?" do
    it 'is false by default' do
      refute @role_settings.no_login_service_allowed_for_role?(:end_user)
    end

    describe 'with no end user login settings' do
      before do
        RoleSettings::SERVICES.each do |service|
          @role_settings.stubs(
            "end_user_#{service}_login" => false
          )
        end
      end

      it 'is true' do
        assert @role_settings.no_login_service_allowed_for_role?(:end_user)
      end
    end
  end

  describe "#login_allowed_for_any_role?" do
    describe "when login is allowed with the service for all roles" do
      before do
        @role_settings.agent_facebook_login    = true
        @role_settings.end_user_facebook_login = true
      end

      it "returns true" do
        assert @role_settings.login_allowed_for_any_role?(:facebook)
      end
    end

    describe "when login is allowed with the service for one role" do
      before { @role_settings.agent_facebook_login = true }

      it "returns true" do
        assert @role_settings.login_allowed_for_any_role?(:facebook)
      end
    end

    describe "when login is allowed with the service for no role" do
      before do
        @role_settings.agent_facebook_login    = false
        @role_settings.end_user_facebook_login = false
      end

      it "returns false" do
        refute @role_settings.login_allowed_for_any_role?(:facebook)
      end
    end
  end

  describe "#login_allowed_only_for_role?" do
    describe "when login is allowed with the service only for the role" do
      before do
        @role_settings.agent_facebook_login    = true
        @role_settings.end_user_facebook_login = false
      end

      it "returns true" do
        assert @role_settings.login_allowed_only_for_role?(:facebook, :agent)
      end
    end

    describe "when login is allowed with the service for no role" do
      before do
        @role_settings.agent_facebook_login    = false
        @role_settings.end_user_facebook_login = false
      end

      it "returns false" do
        refute @role_settings.login_allowed_only_for_role?(:facebook, :agent)
      end
    end

    describe "when login is allowed with the service for all roles" do
      before do
        @role_settings.agent_facebook_login    = true
        @role_settings.end_user_facebook_login = true
      end

      it "returns false" do
        refute @role_settings.login_allowed_only_for_role?(:facebook, :agent)
      end
    end
  end

  describe "#only_login_service_allowed_for_role?" do
    before do
      @role_settings.agent_facebook_login = false
      @role_settings.agent_zendesk_login  = false
      @role_settings.agent_google_login   = false
      @role_settings.agent_twitter_login  = false
      @role_settings.agent_remote_login   = false
    end

    describe "when login is allowed only with the service for the role" do
      before { @role_settings.agent_facebook_login = true }

      it "returns true" do
        assert(
          @role_settings.only_login_service_allowed_for_role?(
            :facebook,
            :agent
          )
        )
      end
    end

    describe "when login is allowed with more than the service for the role" do
      before do
        @role_settings.agent_facebook_login = true
        @role_settings.agent_zendesk_login  = true
      end

      it "returns false" do
        refute(
          @role_settings.only_login_service_allowed_for_role?(
            :facebook,
            :agent
          )
        )
      end
    end
  end

  describe "#only_login_service_allowed_for_user?" do
    before do
      @role_settings.agent_facebook_login = false
      @role_settings.agent_zendesk_login  = false
      @role_settings.agent_google_login   = false
      @role_settings.agent_twitter_login  = false
      @role_settings.agent_remote_login   = false
    end

    describe "when login is allowed only with the service for the role" do
      before { @role_settings.agent_facebook_login = true }

      it "returns true" do
        assert(
          @role_settings.only_login_service_allowed_for_user?(
            :facebook, @account.owner
          )
        )
      end
    end

    describe "when login is allowed with more than the service for the role" do
      before do
        @role_settings.agent_facebook_login = true
        @role_settings.agent_zendesk_login  = true
      end

      it "returns false" do
        refute(
          @role_settings.only_login_service_allowed_for_user?(
            :facebook, @account.owner
          )
        )
      end
    end
  end

  describe "#role_specific_logins?" do
    it "returns false when both roles only use zendesk to login" do
      refute @role_settings.role_specific_logins?
    end

    it 'returns false when both roles use google to login' do
      @role_settings.end_user_google_login = true
      @role_settings.agent_google_login = true
      refute @role_settings.role_specific_logins?
    end

    it "returns true when agents login via zendesk and end users can use google" do
      @role_settings.end_user_google_login = true
      assert @role_settings.role_specific_logins?
    end

    it "returns true when agents login via remote and end users only use zendesk" do
      @role_settings.agent_remote_login = true
      assert @role_settings.role_specific_logins?
    end

    it "returns true when agents use zendesk and end users can use twitter" do
      @role_settings.end_user_twitter_login = true
      assert @role_settings.role_specific_logins?
    end
  end

  describe "#legacy_login?" do
    before do
      @role_settings.agent_zendesk_login  = false
      @role_settings.agent_google_login   = false
      @role_settings.agent_twitter_login  = false
      @role_settings.agent_facebook_login = false
      @role_settings.agent_remote_login   = false
    end

    describe "when remote login is not allowed for agents" do
      before { @role_settings.agent_remote_login = false }

      describe "and twitter login is allowed for agents" do
        before { @role_settings.agent_twitter_login = true }

        it "returns true" do
          assert @role_settings.legacy_login?
        end
      end

      describe "and facebook login is allowed for agents" do
        before { @role_settings.agent_facebook_login = true }

        it "returns true" do
          assert @role_settings.legacy_login?
        end
      end

      describe "and zendesk and google login is allowed for agents" do
        before do
          @role_settings.agent_zendesk_login = true
          @role_settings.agent_google_login  = true
        end

        it "returns false" do
          refute @role_settings.legacy_login?
        end
      end

      describe "and zendesk login allowed but google is not for agents" do
        before do
          @role_settings.agent_zendesk_login = true
          @role_settings.agent_google_login  = false
        end

        it "returns false" do
          refute @role_settings.legacy_login?
        end
      end

      describe "and google login allowed but zendesk is not for agents" do
        before do
          @role_settings.agent_zendesk_login = false
          @role_settings.agent_google_login  = true
        end

        it "returns false" do
          refute @role_settings.legacy_login?
        end
      end
    end

    describe "and remote login is allowed" do
      before { @role_settings.agent_remote_login = true }

      it "returns false" do
        refute @role_settings.legacy_login?
      end
    end
  end

  describe "passwords" do
    before do
      @role_settings.agent_zendesk_login = false
      @role_settings.agent_remote_login = true
      @role_settings.agent_password_allowed = false
      @role_settings.agent_remote_bypass = RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER
      @role_settings.end_user_zendesk_login = false
      @role_settings.end_user_remote_login = true
      @role_settings.end_user_password_allowed = false
      @role_settings.save!
    end

    it "enables passwords when zendesk login is enabled" do
      @role_settings.update_attributes!(agent_zendesk_login: true, end_user_zendesk_login: true)
      assert(@role_settings.agent_password_allowed)
      assert(@role_settings.end_user_password_allowed)
    end
  end

  describe "agent_remote_bypass_allowed?" do
    it "is allowed if an admin can use sso bypass" do
      @role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ADMINS)
      assert(@role_settings.agent_remote_bypass_allowed?)
    end

    it "is allowed if the owner can use sso bypass" do
      @role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER)
      assert(@role_settings.agent_remote_bypass_allowed?)
    end

    it "is not allowed if sso bypass is disabled" do
      @role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_DISABLED)
      assert_equal false, @role_settings.agent_remote_bypass_allowed?
    end

    it "is not allowed if agents can have a password" do
      @role_settings.update_attributes!(agent_zendesk_login: true, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ADMINS)
      assert_equal false, @role_settings.agent_remote_bypass_allowed?
    end
  end

  describe "sso_bypass_allowed_for_user?" do
    it "allows nobody to bypass when sso_bypass is disabled" do
      @role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_DISABLED)

      assert_empty users_who_can_bypass
    end

    it "allows the acount owner to bypass when configured" do
      @role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER)

      assert_equal [:minimum_admin], users_who_can_bypass
    end

    it "allows admins to bypass when sso_bypass when configured" do
      @role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ADMINS)
      Zendesk::StaffClient.any_instance.stubs(:admin?).returns(false)

      assert_equal [:minimum_admin, :minimum_admin_not_owner], users_who_can_bypass
    end

    it "allows nobody when admins can have passwords" do
      @role_settings.update_attributes!(agent_zendesk_login: true, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ADMINS)

      assert_empty users_who_can_bypass
    end

    it "allows admins of other products to bypass when configured" do
      @role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ADMINS)
      Zendesk::StaffClient.any_instance.stubs(:admin?).returns(false)
      agent = users(:minimum_agent)
      Zendesk::StaffClient.any_instance.stubs(:admin?).with(agent.id).returns(true)

      assert_equal [:minimum_admin, :minimum_admin_not_owner, :minimum_agent], users_who_can_bypass
    end

    it "allows owner even if it is not an admin" do
      @role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ADMINS)
      owner = users(:minimum_admin)
      assert owner.is_account_owner?
      owner.stubs(:is_admin?).returns(false)

      Zendesk::StaffClient.any_instance.stubs(:admin?).with(users(:minimum_agent).id).returns(false)
      Zendesk::StaffClient.any_instance.stubs(:admin?).with(users(:minimum_end_user).id).returns(false)

      assert_equal [:minimum_admin, :minimum_admin_not_owner], users_who_can_bypass
    end
  end

  def users_who_can_bypass
    [:minimum_admin, :minimum_admin_not_owner, :minimum_agent, :minimum_end_user].select do |fixture|
      @role_settings.sso_bypass_allowed_for_user?(users(fixture))
    end
  end
end
