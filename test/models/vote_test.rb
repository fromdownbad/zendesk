require_relative "../support/test_helper"

SingleCov.covered!

describe Vote do
  fixtures :entries, :users, :accounts

  let(:user)  { users(:minimum_end_user) }
  let(:entry) { entries(:ponies) }
  let(:vote)  { Vote.create!(entry: entry, user: user) }
  should_validate_presence_of :entry_id, :user_id, :account_id

  describe "A non-first Vote" do
    before { vote }
    should validate_uniqueness_of(:user_id).scoped_to(:entry_id)
  end

  describe "A Vote" do
    it "has the same account_id as the user" do
      assert_equal vote.account, user.account
    end

    it "has the same account_id as the entry" do
      assert_equal vote.account, entry.account
    end
  end

  describe "Creating a Vote" do
    it "increments the votes_count on its Entry" do
      vote.entry.reload.votes_count.must_equal(1)
    end
  end

  describe "Destroying a Vote" do
    it "decrements the votes_count on its Entry" do
      vote.destroy
      entry.reload.votes_count.must_equal(0)
    end
  end
end
