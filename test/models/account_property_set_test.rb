require_relative "../support/test_helper"

SingleCov.covered! uncovered: 27

describe 'AccountPropertySet' do
  fixtures :accounts, :account_property_sets

  describe 'Email template' do
    it 'uses the accessible template' do
      account = accounts(:minimum)
      account.account_property_set.set_defaults
      assert_equal(Zendesk::OutgoingMail::Variables::ACCESSIBLE_HTML_MAIL_TEMPLATE_V2, account.account_property_set.html_mail_template)
    end
  end

  describe "Given an account with a property set" do
    before do
      Timecop.freeze(Time.local(1994, 9, 16, 0, 0, 0))
      @account       = accounts(:minimum)
      @property_set  = @account.account_property_set
      @branding      = @property_set.branding
      @old_cache_key = @account.cache_key
    end

    describe "Updating an account property" do
      before do
        Timecop.travel(Time.local(1994, 9, 16, 0, 0, 13))
        @branding.header_color = "FFFFFF"
        @property_set.branding = @branding
        @property_set.save!
      end

      it "updates the account's cache key" do
        assert_not_equal @old_cache_key, @account.reload.cache_key
      end
    end

    describe "Updating an account with invalid syntax in the html email template" do
      it "throws a clean error message" do
        invalid_email_template = "<html><head>{{styles</head><body></body></html>"
        @property_set.html_mail_template = invalid_email_template
        @property_set.valid?
        assert_operator @property_set.errors.full_messages.size, :>, 0
        assert @property_set.errors.full_messages.include?(I18n.t("txt.admin.settings.email.errors.syntax.variable_termination", template_type: 'html'))
      end
    end

    describe "Updating an account with invalid syntax in the text email template" do
      it "throws a clean error message" do
        invalid_email_template = "{{styles"
        @property_set.text_mail_template = invalid_email_template
        @property_set.valid?
        assert_operator @property_set.errors.full_messages.size, :>, 0
        assert @property_set.errors.full_messages.include?(I18n.t("txt.admin.settings.email.errors.syntax.variable_termination", template_type: 'text'))
      end
    end
  end
end
