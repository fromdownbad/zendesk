require_relative "../support/test_helper"

SingleCov.covered! uncovered: 11

describe 'Forum' do
  fixtures :categories, :forums, :entries, :posts, :users, :user_identities, :accounts, :subscriptions, :organizations

  should_validate_presence_of :name

  describe "with a set translation_locale" do
    subject { Forum.new(translation_locale_id: 1) }
    should_allow_values_for :visibility_restriction_id, VisibilityRestriction::LOGGED_IN_USERS.id
    should_not_allow_values_for :visibility_restriction_id, VisibilityRestriction::AGENTS_ONLY.id
  end

  describe "without a set translation_locale" do
    subject { Forum.new }
    should_allow_values_for :visibility_restriction_id,
      [VisibilityRestriction::EVERYBODY, VisibilityRestriction::LOGGED_IN_USERS, VisibilityRestriction::AGENTS_ONLY].map(&:id)
  end

  it "is able to access organizations by name" do
    forum        = forums(:announcements)
    organization = forum.account.organizations.create!(name: 'Buddhists')
    forum.organization_name = organization.name

    assert_equal organization,      forum.organization
    assert_equal organization.name, forum.organization_name
  end

  it "forces the forum display type for small accounts" do
    account      = accounts(:minimum)
    forum        = Forum.new(display_type: ForumDisplayType::IDEAS, account: account)

    Zendesk::Features::SubscriptionFeatureService.new(account).execute!

    assert_equal forum.display_type_id, ForumDisplayType::IDEAS.id
    forum.send(:force_forum_type)
    assert_equal forum.display_type_id, ForumDisplayType::IDEAS.id

    account.subscription.plan_type = SubscriptionPlanType.Small
    Zendesk::Features::SubscriptionFeatureService.new(account).execute!
    account.reload

    forum.send(:force_forum_type)
    assert_equal forum.display_type_id, ForumDisplayType::ARTICLES.id
  end

  describe ".with_entries" do
    before do
      @account = accounts(:minimum)
    end

    it "does not return empty forums" do
      forum = Forum.create(account: @account, name: "bar")
      assert_nil Forum.with_entries.find_by_id(forum.id)
    end

    it "returns forums with entries" do
      forum = Forum.create(account: @account, name: "bar")
      forum.entries.create!(
        account: @account,
        submitter: users(:minimum_agent),
        title: "foo",
        text: "bar"
      ) do |entry|
        entry.current_user = users(:minimum_agent)
      end

      assert_equal forum, Forum.with_entries.find_by_id(forum.id)
    end
  end

  describe "#subscribe" do
    before do
      @forum = forums(:announcements)
      @user = users(:minimum_agent)
      @forum.subscribe(@user)
    end
    should_create :watching

    it "recognizes user as a subscriptor" do
      assert @forum.subscribed?(@user)
    end

    it "recognizes user as subscribing forum with comments" do
      assert @forum.subscribed_with_posts?(@user)
    end
  end

  describe "#content_mapping" do
    it "returns the content mapping of the forum" do
      forum = forums(:announcements)

      mapping = ContentMapping.create! do |cm|
        cm.account_id = forum.account_id
        cm.classic_object_type = "Legacy::Forum"
        cm.classic_object_id = forum.id
        cm.hc_object_type = "Foo"
        cm.hc_object_id = 42
        cm.url = "/foo/bar/baz"
      end

      assert_equal mapping, forum.content_mapping
    end
  end

  describe "Unsubscribing posts" do
    before do
      @forum = forums(:announcements)
      @user = users(:minimum_agent)
      @forum.subscribe(@user)
      Watching.last.unsubscribe_posts!
    end

    it "does not recognize user as subscribing forum with comments" do
      assert_equal false, @forum.subscribed_with_posts?(@user)
    end
  end

  describe "Sort order for forum types" do
    before do
      @forum = forums(:announcements)
      @forum.save
      assert_equal 'entries.is_highlighted DESC, entries.updated_at DESC', @forum.sorting
      assert @forum.display_type.articles?
    end

    it "alls have values" do
      Forum::SORT_OPTIONS.each do |pair|
        assert_equal 2, pair.size
        assert pair.first.present?
        assert pair.last.present?
      end
    end

    it "uses default votes_count sorting for forums not of type Articles" do
      @forum.display_type_id = ForumDisplayType::QUESTIONS.id
      assert_equal 'entries.votes_count DESC, entries.posts_count DESC', @forum.sorting
    end

    it "allows most helpful sorting for forums2" do
      assert_equal Forum::DEFAULT_SORT_FOR_VOTING, 'entries.votes_count DESC, entries.posts_count DESC'
      @forum.account.subscription.expects(:has_forums2?).returns(true)
      assert @forum.legal_sorting?(Forum::DEFAULT_SORT_FOR_VOTING)
      @forum.account.subscription.expects(:has_forums2?).returns(false)
      refute @forum.legal_sorting?(Forum::DEFAULT_SORT_FOR_VOTING)
    end
  end

  describe "#reset_flags_on_display_type_change" do
    before do
      @forum = forums(:announcements)
      @forum.update_attribute(:display_type_id, ForumDisplayType::QUESTIONS.id)
      @entry = @forum.entries.first
      @entry.update_attribute(:flag_type_id, EntryFlagType::ANSWERED.id)
      @entry.posts.first.update_attribute(:is_informative, true)
      assert @entry.flag.answered?
    end

    it "resets all associated entry + post flags when forum type is changed" do
      @forum.display_type_id = ForumDisplayType::ARTICLES.id
      @forum.save
      refute @entry.reload.flag.answered? # Entry is no longer Answered
      assert_empty(@entry.posts.find_all(&:is_informative?)) # No posts are marked as The Answer
    end
  end

  describe "#propagate_access_settings_after_update" do
    before do
      @forum = forums(:solutions)
      @entry = entries(:sticky)
      @attachment = @entry.attachments.first
      @organization = organizations(:minimum_organization1)
    end

    it "updates public status on entries and attachments" do
      assert @entry.is_public
      assert @attachment.is_public
      @forum.visibility_restriction_id = 3
      @forum.save
      refute @entry.reload.is_public
      refute @entry.attachments.first.is_public
    end

    it "updates organization ownership on entries" do
      assert_nil @entry.organization_id
      @forum.organization = @organization
      @forum.save
      assert_equal @organization.id, @entry.reload.organization_id
    end

    it "does not have N+1s" do
      Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
      assert_sql_queries 10..11 do
        @forum.visibility_restriction_id = 3
        @forum.organization = @organization
        @forum.save
      end
    end
  end

  describe "Forum#to_param" do
    before { @forum = Forum.new }

    it "consists of the id and a normalized version of the name separated by a single dash" do
      @forum.stubs(:id).returns(123)
      @forum.name = "The World's Most Wonderful Forum"

      assert_equal "123-The-World-s-Most-Wonderful-Forum", @forum.to_param
    end

    it "allows unicode characters and the id" do
      @forum.stubs(:id).returns(123)
      @forum.name = "Македонски"

      assert_equal "123-Македонски", @forum.to_param
    end

    it "allows unicode alpha characters" do
      @forum.name = "Ein Schöenes Tag"

      assert_match /Ein-Schöenes-Tag$/, @forum.to_param
    end

    it "allows unicode alphanumeric characters and remove others" do
      @forum.name = "A Little Кириличен Twist! 123"

      assert_match /A-Little-Кириличен-Twist-123$/, @forum.to_param
    end
  end

  describe "Soft delete for forums" do
    it "does not be fetchable after soft_delete! and then fetchable after soft_undelete!" do
      forum   = forums(:announcements)
      account = forum.account
      entries = forum.entries

      refute forum.deleted?
      assert account.forums.include?(forum)
      refute forum.entries.empty?

      assert_respond_to forum, :soft_delete!
      forum.soft_delete!
      assert forum.deleted?
      refute account.forums.include?(forum)
      assert_nil Entry.find_by_id(entries[0].id)

      assert_respond_to forum, :soft_undelete!
      Forum.with_deleted { forum.soft_undelete! }
      refute forum.deleted?
      assert account.forums.include?(forum)
      refute forum.entries.empty?
    end
  end

  describe "with entry moderation" do
    let(:account) { accounts(:minimum) }

    let(:forum) { account.forums.first }

    let(:user) do
      users(:minimum_end_user).tap do |user|
        user.stubs(whitelisted_from_moderation?: false)
      end
    end

    let(:valid_attrs) { { title: "test", text: "test", account: account } }

    before do
      account.settings.moderated_entries = true
      account.settings.save!
    end

    describe "on deletion with entries" do
      let(:entry_creation_time) { Time.local(2007, 1, 1, 10, 5, 0) }

      let(:entry) do
        Timecop.travel(entry_creation_time) do
          forum.entries.create!(valid_attrs) { |e| e.current_user = user }
        end
      end

      before do
        entry
      end

      describe "after soft_deletion!" do
        before do
          forum.soft_delete!
          entry.reload
        end

        it "has a deleted entry" do
          assert entry.deleted?
        end

        it "does not be a suspended entry" do
          refute entry.suspended?
        end

        it "updates updated_at on entry" do
          assert entry.updated_at != entry.created_at
        end
      end

      describe "check un-suspension due to forum soft delete affecting entries on other forums" do
        let(:other_forum) { account.forums.last }
        let(:other_entry) { other_forum.entries.create!(valid_attrs) { |e| e.current_user = user } }

        before do
          entry
          other_entry
        end

        it "does not be the same forum" do
          assert other_forum != forum
        end

        it "boths have suspended entries" do
          assert entry.suspended?
          assert other_entry.suspended?
        end

        it "boths have deleted entries" do
          assert entry.deleted?
          assert other_entry.deleted?
        end

        it "leaves entry on the other forum suspended" do
          forum.soft_delete!
          other_entry.reload

          assert other_entry.suspended?
        end
      end
    end
  end

  describe "#matches_user_tags?" do
    let(:forum) { Forum.new }
    let(:user) { users(:minimum_end_user) }

    before do
      Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
    end

    describe "when forum has tags" do
      before do
        forum.stubs(:tag_array).returns([user.all_tags.first])
      end

      describe "and matches user tags" do
        it "returns true" do
          assert forum.matches_user_tags?(user)
        end
      end

      describe "does not match user tags" do
        before do
          forum.stubs(:tag_array).returns(user.all_tags + ['foo', 'bar'])
        end

        it "returns false" do
          refute forum.matches_user_tags?(user)
        end
      end
    end

    describe "when forum does not have tags" do
      it "returns true" do
        assert forum.matches_user_tags?(user)
      end
    end
  end
end
