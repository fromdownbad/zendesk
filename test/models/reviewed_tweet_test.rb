require_relative "../support/test_helper"

SingleCov.covered!

describe 'ReviewedTweet' do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
  end

  it 'marks as reviewed' do
    assert_equal([4, 5], ReviewedTweet.mark_as_reviewed(@account, '4,5'))
    assert_equal([4, 5], ReviewedTweet.reviewed(@account, '4,5,6'))
  end

  it 'does not blow up when passed existing values' do
    ReviewedTweet.mark_as_reviewed(@account, '1,2')
    ReviewedTweet.mark_as_reviewed(@account, '2,3')
  end
end
