require_relative '../support/test_helper'

SingleCov.covered!

describe SimplifiedEmailOptInSetting do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  def create_setting!(name, value)
    account.simplified_email_opt_in_settings.create!(name: name, value: value)
  end

  describe 'when retrieving simplified_email_opt_in_settings for an account' do
    before do
      create_setting!('name1', 'value1')
      create_setting!('name2', 'value2')
    end

    it 'returns the list of simplified_email_opt_in_settings' do
      settings = account.simplified_email_opt_in_settings
      assert_equal 2, settings.length

      names = settings.each_with_object([]) do |setting, arr|
        arr << setting.name
      end

      assert_same_elements ['name1', 'name2'], names

      values = settings.each_with_object([]) do |setting, arr|
        arr << setting.value
      end

      assert_same_elements ['value1', 'value2'], values
    end
  end
end
