require_relative "../support/test_helper"

SingleCov.covered! uncovered: 8

describe RemoteAuthentication do
  fixtures :accounts, :account_settings, :remote_authentications, :translation_locales

  describe "switching auth_mode" do
    subject { remote_authentications(:minimum_remote_authentication) }

    before do
      Account.any_instance.stubs(has_saml?: true)

      subject.update_external_ids = true
      subject.auth_mode = RemoteAuthentication::JWT
      subject.save!

      subject.auth_mode = RemoteAuthentication::SAML
      subject.save!
    end

    it "does not allow SAML to update external ids" do
      refute subject.reload.allow_external_id_updates
    end
  end

  describe "uniqueness of auth_mode" do
    subject do
      accounts(:minimum).remote_authentications.
        build(auth_mode: RemoteAuthentication::JWT)
    end

    before do
      auth = remote_authentications(:minimum_remote_authentication)
      auth.auth_mode = RemoteAuthentication::JWT
      auth.save!

      subject.valid?
    end

    it "errors on auth_mode" do
      assert subject.errors[:auth_mode].present?
    end
  end

  describe "#saml_authenticates?" do
    it "returns true of auth_mode is SAML and the IP address is handled" do
      @r = RemoteAuthentication.new(is_active: true)
      refute @r.saml_authenticates?("hello")
      @r.auth_mode = RemoteAuthentication::SAML
      assert @r.saml_authenticates?("hello")
      @r.ip_ranges = "127.0.0.1"
      refute @r.saml_authenticates?("hello")
      assert @r.saml_authenticates?("127.0.0.1")
      @r.auth_mode = RemoteAuthentication::JWT
      refute @r.saml_authenticates?("127.0.0.1")
    end
  end

  describe "#native_authenticates?" do
    it "returns true of auth_mode is native and the IP address is handled" do
    end
  end

  describe "#self.key" do
    it "calculates the MD5 of the params" do
      assert_equal Digest::MD5.hexdigest("abcd"), RemoteAuthentication.key({ name: "a", email: "b", token: "c", timestamp: "d"}, true, 1)
    end

    it "supports v2" do
      # "name|email|||||token|timestamp"
      assert_equal Digest::MD5.hexdigest("a|b|||||c|d"), RemoteAuthentication.key({ name: "a", email: "b", token: "c", timestamp: "d"}, true, 2)
    end

    it "supports escaping delimiters" do
      # "name|email|||||token|timestamp"
      assert_equal Digest::MD5.hexdigest("%7Ca%7C|b%7C|||||c|d"), RemoteAuthentication.key({ name: "|a|", email: "b|", token: "c", timestamp: "d"}, true, 2)
    end

    it "supports non-escaping delimiters" do
      # "name|email|||||token|timestamp"
      assert_equal Digest::MD5.hexdigest("|a||b||||||c|d"), RemoteAuthentication.key({ name: "|a|", email: "b|", token: "c", timestamp: "d"}, true, 2, escaped_delimiters: false)
    end
  end

  describe ".logout_redirect" do
    subject do
      Zendesk::RemoteAuthentications.logout_redirect(authentication: auth, request: nil, user: user, remote_params: params)
    end

    let(:auth) { remote_authentications(:minimum_remote_authentication) }
    let(:user) { nil }
    let(:params) { {} }
    let(:logout_url) { nil }

    before { auth.stubs(remote_logout_url: logout_url) }

    describe "nil params" do
      let(:params) { nil }

      it "returns logout_url" do
        assert_nil subject
      end
    end

    describe "blank remote_logout_url" do
      let(:logout_url) { "" }

      it "returns logout_url" do
        assert_equal auth.remote_logout_url, subject
      end
    end

    describe "with a remote_logout_url" do
      let(:logout_url) { "https://www.google.com" }

      before do
        auth.stubs(auth_mode: RemoteAuthentication::SAML)
      end

      describe "with a user" do
        let(:user) { users(:minimum_agent) }

        it "returns a SAMLRequest" do
          assert_match /#{auth.remote_logout_url}\?SAMLRequest=[a-z0-9]+/, subject
        end
      end

      describe "without a user" do
        it "returns a SAMLRequest" do
          assert_match /#{auth.remote_logout_url}\?SAMLRequest=[a-z0-9]+/, subject
        end
      end
    end
  end

  describe ".login_redirect" do
    let(:env) do
      env = Rack::MockRequest.env_for(url)
      env["warden.options"] = {attempted_path: env["PATH_INFO"]} # it is called for a request that failed in warden
      env
    end
    let(:auth) { remote_authentications(:minimum_remote_authentication) }
    let(:request) { ActionDispatch::Request.new(env) }

    subject { Zendesk::RemoteAuthentications.login_redirect(auth, request, auth.account.default_brand.id) }

    describe "nil" do
      describe "no param" do
        let(:url) { "https://www.example.com/" }

        before { subject }

        it "sets session return_to" do
          assert_equal url, request.session[:return_to]
        end
      end

      describe "return_to param" do
        let(:url) { "https://www.example.com?return_to=/hello" }

        before { subject }

        it "sets session return_to" do
          assert_equal "/hello", request.session[:return_to]
        end
      end

      describe "return_to /access" do
        let(:url) { "https://www.example.com?return_to=/access" }

        before { subject }

        it "sets session return_to to root" do
          assert_equal "https://www.example.com", request.session[:return_to]
        end
      end

      describe "return_to /login" do
        let(:url) { "https://www.example.com?return_to=/login" }

        before { subject }

        it "sets session return_to to root" do
          assert_equal "https://www.example.com", request.session[:return_to]
        end
      end

      describe "attempted_path is /access" do
        let(:url) { "https://www.example.com/access" }

        before { subject }

        it "sets session return_to to root" do
          assert_equal "https://www.example.com", request.session[:return_to]
        end
      end

      describe "attempted_path is /login" do
        let(:url) { "https://www.example.com/login" }

        before { subject }

        it "sets session return_to to root" do
          assert_equal "https://www.example.com", request.session[:return_to]
        end
      end
    end

    describe "with SAML" do
      before do
        auth.stubs(auth_mode: RemoteAuthentication::SAML)
      end

      describe "with a return_to" do
        let(:url) { "https://www.example.com?return_to=/hello" }

        it "returns to remote login url" do
          assert_match auth.remote_login_url, subject
        end

        it "returns to RelayState" do
          assert_match /RelayState=%2Fhello/, subject
        end

        it "has a SAMLRequest" do
          assert_match /SAMLRequest=[A-Za-z0-9]+/, subject
        end
      end

      describe "with no return_to" do
        let(:url) { "https://www.example.com/hi" }

        it "returns to remote login url" do
          assert_match auth.remote_login_url, subject
        end

        it "returns to RelayState" do
          assert_match /RelayState=#{CGI.escape(url)}/, subject
        end

        it "has a SAMLRequest" do
          assert_match /SAMLRequest=[A-Za-z0-9]+/, subject
        end
      end
    end

    describe "with JWT" do
      before do
        auth.stubs(auth_mode: RemoteAuthentication::JWT)
      end

      describe "when remote_login_url has parameters" do
        before do
          auth.stubs(remote_login_url: "https://www.example.com/login?test=123")
        end
      end

      describe "when remote_login_url has no parameters" do
        before do
          auth.stubs(remote_login_url: "https://www.example.com/")
        end

        describe "with return_to param" do
          let(:url) { "https://www.example.com/?return_to=/hi" }

          it "returns to remote login url" do
            assert_match auth.remote_login_url, subject
          end

          it "returns to return_to" do
            assert_match /return_to=%2Fhi/, subject
          end

          it "has a timestamp" do
            assert_match /timestamp=[0-9]+/, subject
          end

          it "has no locale_id" do
            assert_no_match /locale_id=[0-9]+/, subject
          end
        end

        describe "with no return_to" do
          let(:url) { "https://www.example.com/hi" }

          it "returns to remote login url" do
            assert_match auth.remote_login_url, subject
          end

          it "returns to url" do
            assert_match /return_to=#{CGI.escape(url)}/, subject
          end

          it "has a timestamp" do
            assert_match /timestamp=[0-9]+/, subject
          end

          it "has no locale_id" do
            assert_no_match /locale_id=[0-9]+/, subject
          end
        end

        describe "when session has locale" do
          let(:locale) { translation_locales(:brazilian_portuguese).id }
          let(:url) { "https://www.example.com/hi" }

          before do
            request.shared_session[:locale_id] = locale
          end

          it "has locale_id" do
            assert_match /locale_id=#{locale}/, subject
          end
        end
      end
    end
  end

  describe "#fingerprint" do
    before do
      @r = RemoteAuthentication.new
      @r.token = "fingerprint"
    end

    it "returns nil when auth_mode is not SAML" do
      assert @r.fingerprint.nil?
    end

    it "returns the token value when auth_mode is SAML" do
      @r.auth_mode = RemoteAuthentication::SAML
      assert_equal "fingerprint", @r.fingerprint
    end
  end

  let(:remote_auth) { remote_authentications(:minimum_remote_authentication) }

  it "removes extraneous whitespace" do
    remote_auth.update_attributes(is_active: true, remote_login_url: " https://host/moo  ")

    assert_equal "https://host/moo", remote_auth.remote_login_url
  end

  it "does not accept empty login urls" do
    remote_auth.update_attributes(is_active: true, remote_login_url: "")

    refute_empty remote_auth.errors
    assert_equal 'Enter a valid login URL', remote_auth.errors.first[1]
  end

  it "allows empty logout urls" do
    remote_auth.update_attributes(is_active: true, remote_logout_url: "")

    assert_empty remote_auth.errors
  end

  it "only validates if value changed" do
    remote_auth.expects(:valid_url?).never

    remote_auth.update_attributes(is_active: true)
  end

  describe "when a remote login/logout HTTP url is already set" do
    before do
      remote_auth.update_attribute(:remote_login_url, "http://example.com")
      remote_auth.update_attribute(:remote_logout_url, "http://example1.com")
    end

    it "allows remote login/logout HTTP url to be updated (grandfathered) " do
      remote_auth.update_attributes(is_active: true, remote_login_url: "http://example.com/update", remote_logout_url: "http://example1.com/update")
      assert_empty remote_auth.errors
    end
  end

  def error_message_for(attribute, error_type)
    case error_type
    when :is_zendesk then I18n.t("txt.admin.models.remote_authentication.#{attribute}_is_not_zendesk_v3")
    when :is_http then I18n.t("txt.admin.models.remote_authentication.#{attribute}_is_http_v3")
    when :is_not_url then I18n.t("txt.admin.models.remote_authentication.#{attribute}_is_not_url_v3")
    when :is_too_long
      I18n.t("txt.admin.models.remote_authentication.#{attribute}_is_too_long_v4",
        char_limit: RemoteAuthentication::MAXIMUM_URL_LENGTH)
    end
  end

  def validate_error(result, attribute, error_type, actual_error)
    if result == :fail
      refute_empty remote_auth.errors
      assert_equal error_message_for(attribute, error_type), actual_error
    elsif result == :pass
      assert_empty remote_auth.errors
    end
  end

  def self.assert_remote_auth_validation!(result, error_type)
    it "validates the attributes" do
      validate_error(result, 'remote_login_url', error_type, remote_auth.errors[:base][0])
      validate_error(result, 'remote_logout_url', error_type, remote_auth.errors[:base][1])
    end
  end

  initial_urls = ['http://example\invalid.com', nil]
  remote_auth_test_cases = [
    # url                            result   error_type
    ["https://zendesk.com",          :fail,   :is_zendesk],
    ["http://example.com",           :fail,   :is_http],
    ["ftp://localhost.com",          :fail,   :is_not_url],
    ["blah",                         :fail,   :is_not_url],
    ["https://host/moo",             :pass,   nil],
    ["https://#{"long" * 2048}.com", :fail,   :is_too_long],
  ]

  initial_urls.each do |initial_url|
    describe "with the initial URL #{initial_url}" do
      before { remote_auth.update_attribute(:remote_login_url, initial_url) }

      remote_auth_test_cases.each do |test_case_url, result, error_type|
        describe "when the URL is changed to #{test_case_url}" do
          before { remote_auth.update_attributes(is_active: true, remote_login_url: test_case_url, remote_logout_url: test_case_url) }

          assert_remote_auth_validation!(result, error_type)
        end
      end
    end
  end

  describe "when validating remote authentications" do
    it "validates remote login" do
      remote_auth.update_attributes(remote_login_url: 'blah', is_active: false)

      refute_empty remote_auth.errors
    end

    it "validates remote logout" do
      remote_auth.update_attributes(remote_logout_url: 'https://zendesk.com', is_active: false)

      refute_empty remote_auth.errors
    end

    it "validates remote ip ranges" do
      remote_auth.update_attributes(ip_ranges: '2.2.2', is_active: false)

      refute_empty remote_auth.errors
    end

    it "validates fingerprint" do
      remote_auth.update_attributes(auth_mode: RemoteAuthentication::SAML, fingerprint: 'invalid_fingerprint', is_active: false)

      refute_empty remote_auth.errors
    end

    it "validates next_token" do
      remote_auth.update_attributes(auth_mode: RemoteAuthentication::JWT, next_token: 'invalid_next_token', is_active: false)

      refute_empty remote_auth.errors
    end
  end

  describe "#handles_ip?" do
    before do
      @r = RemoteAuthentication.new
    end

    it "returns true when ip_ranges is blank" do
      assert @r.ip_ranges.blank?
      assert @r.handles_ip?("hello")
    end

    it "returns true when given an IP that's match by specificed IP ranges" do
      @r.ip_ranges = "128.0.0.1"
      assert @r.handles_ip?("128.0.0.1")
      refute @r.handles_ip?("128.0.0.2")

      @r.ip_ranges = "128.0.0.*"
      assert @r.handles_ip?("128.0.0.1")
      assert @r.handles_ip?("128.0.0.2")
      refute @r.handles_ip?("128.0.1.2")

      @r.ip_ranges = "128.0.*.*"
      assert @r.handles_ip?("128.0.0.1")
      assert @r.handles_ip?("128.0.0.2")
      assert @r.handles_ip?("128.0.1.2")

      @r.ip_ranges = "128.0.0.1/32"
      assert @r.handles_ip?("128.0.0.1")
      refute @r.handles_ip?("128.0.0.2")
      refute @r.handles_ip?("128.0.1.2")

      @r.ip_ranges = "199.16.156.0/22"
      assert @r.handles_ip?("199.16.157.88")
      refute @r.handles_ip?("128.0.0.2")
      refute @r.handles_ip?("128.0.1.2")
    end

    it "returns false when given a zendesk-internal IP not in the range" do
      @r.ip_ranges = "128.0.0.1"
      refute @r.handles_ip?('127.0.0.1')
    end
  end

  describe "#validate_token" do
    before do
      @r = RemoteAuthentication.new
      @r.account = accounts(:minimum)
      @r.fingerprint = "fingerprint"
      @r.next_token  = "next_token"

      assert @r.errors.blank?
      assert @r.token.nil?
    end

    it "reads the fingerprint attribute when auth_mode is SAML" do
      @r.auth_mode = RemoteAuthentication::SAML.to_s
      @r.send(:set_token)
      assert_equal "fingerprint", @r.token
    end

    it "reads the next_token when auth_mode is native" do
      @r.auth_mode = RemoteAuthentication::JWT
      @r.send(:set_token)
      assert_equal "next_token", @r.token
    end

    it "still sets a newly provided token when not active" do
      @r.token     = "disabled"
      @r.is_active = false
      @r.valid?
      assert_equal "next_token", @r.token
    end

    it "only change the token if there is a next_token" do
      @r.auth_mode  = RemoteAuthentication::JWT
      @r.token      = "hello"
      @r.next_token = nil
      @r.send(:validate_token)
      assert_equal "hello", @r.token
    end

    it "reports an error if token has >40 chars and auth_mode is saml" do
      @r.auth_mode   = RemoteAuthentication::SAML.to_s
      @r.token       = SecureRandom.base64(27)
      @r.fingerprint = nil
      @r.send(:validate_token)
      assert_equal "Fingerprint:  is invalid", @r.errors.full_messages.first
    end

    it "reports an error if token has 255> chars and auth_mode is saml" do
      @r.auth_mode   = RemoteAuthentication::SAML.to_s
      @r.token       = SecureRandom.base64(256)
      @r.fingerprint = nil
      @r.send(:validate_token)
      assert_equal "Fingerprint:  is invalid", @r.errors.full_messages.first
    end

    it "validates without an errror" do
      @r.auth_mode   = RemoteAuthentication::SAML.to_s
      @r.token       = SecureRandom.base64(40)
      @r.fingerprint = nil
      @r.send(:validate_token)
      assert @r.errors.blank?
    end
  end

  describe "#validate_feature_permissions" do
    describe "non multiproduct account" do
      before do
        @r = RemoteAuthentication.new
        @r.account = accounts(:minimum)
        @r.fingerprint = "fingerprint"
        @r.is_active = true

        assert @r.errors.blank?
      end

      it "reports an error if auth_mode is SAML and account doesn't have saml enabled" do
        Account.any_instance.stubs(has_saml?: false)
        @r.auth_mode = RemoteAuthentication::SAML
        @r.send(:validate_feature_permissions)
        assert_equal I18n.t("txt.admin.models.remote_authentication.profile_does_not_support_saml"), @r.errors.full_messages.first
      end

      it "reports an error if auth_mode is JWT and account doesn't have jwt enabled" do
        Account.any_instance.stubs(has_jwt?: false)
        @r.auth_mode = RemoteAuthentication::JWT
        @r.send(:validate_feature_permissions)
        assert_equal I18n.t("txt.admin.models.remote_authentication.profile_does_not_support_jwt"), @r.errors.full_messages.first
      end
    end

    describe "multiproduct account" do
      before do
        @r = RemoteAuthentication.new
        account = accounts(:shell_account_without_support)
        @r.account = account
        @r.fingerprint = "fingerprint"
        @r.is_active = true

        assert @r.errors.blank?
      end

      it "reports an error if auth_mode is SAML and account doesn't have saml enabled" do
        @r.account.stubs(has_saml?: false)
        @r.auth_mode = RemoteAuthentication::SAML
        @r.send(:validate_feature_permissions)
        refute @r.errors.blank?
      end

      it "no error if auth_mode is SAML and account has saml enabled" do
        @r.account.stubs(has_saml?: true)
        @r.auth_mode = RemoteAuthentication::SAML
        @r.send(:validate_feature_permissions)
        assert @r.errors.blank?
      end
    end
  end

  describe "#redact_token" do
    before do
      @r = RemoteAuthentication.new(account: Account.new)
    end

    it "returns the fingerprint if auth_mode is SAML" do
      @r.auth_mode = RemoteAuthentication::SAML
      token = 'fingerprint'

      assert @r.send(:redact_token, token) == token
    end

    it "returns the redacted secret showing only first 6 characters if auth_mode is JWT" do
      @r.auth_mode = RemoteAuthentication::JWT
      token = 'asdf12foobarbaz'

      assert @r.send(:redact_token, token) == 'asdf12*********'
    end
  end

  describe "#cia_changes" do
    fixtures :accounts
    before do
      @r = RemoteAuthentication.new(account: accounts(:minimum))
    end

    describe_with_arturo_enabled :redact_jwt_token_in_audit_log do
      it "returns the changed attributes registered in audit_attribute" do
        @r.is_active = true

        assert @r.cia_changes == { "account_id" => [nil, @r.account.id], "is_active" => [false, true] }
      end

      it "redacts the token if the token has been changed on JWT mode" do
        @r.auth_mode = RemoteAuthentication::JWT
        @r.token = 'asdf12foobarbaz'

        assert_same_elements(@r.cia_changes["token"], [nil, 'asdf12*********'])
      end

      it "does not modify the token if the token has been changed on SAML mode" do
        @r.auth_mode = RemoteAuthentication::SAML
        @r.token = 'fingerprint'

        assert @r.cia_changes["token"] == [nil, 'fingerprint']
      end
    end

    describe_with_arturo_disabled :redact_jwt_token_in_audit_log do
      it "returns the changed attributes registered in audit_attribute" do
        @r.is_active = true

        assert @r.cia_changes == { "account_id" => [nil, @r.account.id], "is_active" => [false, true] }
      end

      it "returns the full token if the token has been changed on JWT mode" do
        @r.auth_mode = RemoteAuthentication::JWT
        @r.token = 'asdf12foobarbaz'

        assert @r.cia_changes["token"] == [nil, 'asdf12foobarbaz']
      end

      it "does not modify the token if the token has been changed on SAML mode" do
        @r.auth_mode = RemoteAuthentication::SAML
        @r.token = 'fingerprint'

        assert @r.cia_changes["token"] == [nil, 'fingerprint']
      end
    end
  end
end
