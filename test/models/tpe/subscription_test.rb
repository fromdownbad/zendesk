require_relative "../../support/test_helper"

SingleCov.covered!

describe Tpe::Subscription do
  def behaves_like_updated_subscription
    Voice::PartnerEditionAccount.
      any_instance.
      expects(:updated_subscription!).
      with(kind_of(Tpe::Subscription))
  end

  fixtures :accounts, :subscriptions, :role_settings

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
  end

  describe 'associations' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { FactoryBot.build(:tpe_subscription, account: account) }

    it "belongs to an account" do
      assert subscription.account.present?
    end
  end

  describe 'audit log' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { FactoryBot.create(:tpe_subscription, account: account) }
    let(:audit)        { CIA::Event.last }

    before do
      assert_nil CIA::Event.last
    end

    describe 'requirements' do
      let(:audited_attributes) do
        [
          'account_id',
          'active',
          'plan_type',
          'max_agents'
        ]
      end

      it 'is required for specific attributes' do
        assert_equal audited_attributes, subscription.audited_attributes
      end
    end

    describe 'updating any of the auditable attributes' do
      before do
        CIA.audit actor: subscription.account.owner do
          subscription.update_attributes!(max_agents: 1)
        end
      end

      it 'generates an audit log' do
        assert_equal 'update', audit.action
        assert_equal subscription.account.owner, audit.actor
        assert_equal subscription, audit.source
      end
    end
  end

  describe 'aliased attributes for #active?' do
    let(:original) { :active? }

    let(:subscription) do
      FactoryBot.build(
        :tpe_subscription,
        account: nil # Accounts factory is slooooooow!
      )
    end

    describe '#include_in_easy_agent_add?' do
      let(:aliased) { :include_in_easy_agent_add? }

      it 'equals #active?' do
        assert_equal subscription.send(original), subscription.send(aliased)
      end
    end

    describe '#is_active?' do
      let(:aliased) { :is_active? }

      it 'equals #active?' do
        assert_equal subscription.send(original), subscription.send(aliased)
      end
    end
  end

  describe '#deactivate' do
    let(:account) { accounts(:minimum) }
    let(:subscription) do
      FactoryBot.build(
        :tpe_subscription,
        active:  true,
        account: account
      )
    end

    it 'changes the active attribute to false' do
      subscription.deactivate
      assert_equal false, subscription.active?
    end
  end

  describe "#plan_name" do
    let(:subscription) do
      FactoryBot.build(
        :tpe_subscription,
        account:   nil, # Accounts factory is slooooooow!
        plan_type: plan_type
      )
    end

    describe "when plan type is 'Partner edition'" do
      let(:plan_type) { ZBC::Tpe::PlanType::PartnerEdition.plan_type }

      it "returns 'Partner Edition'" do
        assert_equal 'Partner Edition', subscription.plan_name
      end
    end

    describe "when plan type does not exist" do
      let(:plan_type) { -1 }

      it "returns nil" do
        assert_nil subscription.plan_name
      end
    end
  end

  describe '#is_trial?' do
    let(:subscription) { FactoryBot.build(:tpe_subscription) }

    it 'returns false' do
      assert_equal false, subscription.is_trial?
    end
  end

  describe '#update_partner_edition_account' do
    let(:account) { accounts(:minimum) }
    describe 'when creating a new subscription' do
      it 'calls updated_subscription! on voice partner edition account' do
        Voice::PartnerEditionAccount.
          any_instance.
          expects(:updated_subscription!).
          with(kind_of(Tpe::Subscription))

        FactoryBot.create(:tpe_subscription, account: account, active: true)
      end
    end

    describe 'when updating a subscription' do
      let!(:tpe_subscription) { FactoryBot.create(:tpe_subscription, account: account, active: true, max_agents: 1) }

      it 'calls updated_subscription! when an attribute is updated' do
        behaves_like_updated_subscription

        tpe_subscription.update(max_agents: 10)
      end

      it 'calls updated_subscription! when max_agents changes' do
        behaves_like_updated_subscription

        tpe_subscription.update(max_agents: 5)
      end

      it 'calls updated_subscription! when plan_type changes' do
        behaves_like_updated_subscription

        tpe_subscription.update(plan_type: 0)
      end

      it 'calls updated_subscription! when active changes' do
        behaves_like_updated_subscription

        tpe_subscription.update(active: false)
      end

      it 'does not call updated_subscription! when updated_at changes' do
        Voice::PartnerEditionAccount.
          any_instance.
          expects(:updated_subscription!).never

        tpe_subscription.update(updated_at: Time.now)
      end
    end
  end
end
