require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 66

describe Users::Identification do
  fixtures :accounts, :users, :user_identities

  let(:user) { users(:minimum_end_user) }

  describe "scopes queries to account ID" do
    it "for identities association" do
      assert_sql_queries(1, /`user_identities`\.`account_id` IN \(#{user.account_id}, -1\)/) do
        user.identities.to_a
      end
    end

    it "for email identities association" do
      assert_sql_queries(1, /`user_identities`\.`account_id` IN \(#{user.account_id}, -1\)/) do
        user.identities.email.to_a
      end
    end
  end
end
