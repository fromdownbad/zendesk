require_relative '../../support/test_helper'
require_relative '../../support/zopim_test_helper'

SingleCov.covered!

describe 'Users::SuiteAgentSupport' do
  include ZopimTestHelper

  fixtures :accounts, :zopim_subscriptions

  describe '#make_suite_agent!' do
    let(:account)             { accounts(:with_trial_zopim_subscription) }
    let(:chat_only_role_type) { PermissionSet::Type::CHAT_AGENT }

    let(:custom_permission_set) do
      build_valid_permission_set(account).tap do |permission_set|
        permission_set.permissions.set(ticket_access: 'within-groups')
        permission_set.save!
      end
    end

    let(:group_restriction_type) { Zendesk::Types::RoleRestrictionType.GROUPS }
    let(:none_restriction_type)  { Zendesk::Types::RoleRestrictionType.NONE }

    before do
      Account.any_instance.stubs(has_active_suite?: true)
      PermissionSet.create_chat_agent!(account)
      setup_stubs_for_adding_agents
    end

    describe 'for a Chat-only Agent' do
      let(:user) { FactoryBot.create(:chat_only_agent, account: account) }

      it "removes the user's chat_only permission_set" do
        refute_nil user.permission_set(true)
        assert_equal chat_only_role_type, user.permission_set(true).role_type

        user.make_suite_agent!

        assert_nil user.permission_set(true)
      end

      it "removes the user's Ticket Groups restriction" do
        assert_equal group_restriction_type, user.reload.restriction_id

        user.make_suite_agent!

        assert_equal none_restriction_type, user.reload.restriction_id
      end
    end

    describe 'for a Support-only Agent' do
      let(:user) { FactoryBot.create(:agent, account: account) }

      before { user.update_attributes(permission_set: custom_permission_set) }

      it "does not modify the user's permission_set" do
        assert_equal custom_permission_set, user.permission_set(true)

        user.make_suite_agent!

        assert_equal custom_permission_set, user.permission_set(true)
      end

      it "does not modify the user's restriction" do
        assert_equal group_restriction_type, user.reload.restriction_id

        user.make_suite_agent!

        assert_equal group_restriction_type, user.reload.restriction_id
      end
    end

    describe 'for a Chat-enabled Support Agent' do
      let(:user) { FactoryBot.create(:chat_enabled_agent, account: account) }

      before { user.update_attributes(permission_set: custom_permission_set) }

      it "does not modify the user's permission_set" do
        refute_nil user.permission_set(true)

        user.make_suite_agent!

        refute_nil user.permission_set(true)
      end

      it "does not modify the user's restriction" do
        assert_equal group_restriction_type, user.reload.restriction_id

        user.make_suite_agent!

        assert_equal group_restriction_type, user.reload.restriction_id
      end
    end

    def setup_stubs_for_adding_agents
      User.any_instance.stubs(encrypt_password: true)
      User.any_instance.stubs(password_security_policy_enforced: true)

      Zopim::Agent.any_instance.stubs(:link_to_remote_account_record)

      SecurityMailer.stubs(send: true)
    end
  end
end
