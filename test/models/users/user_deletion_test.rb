require_relative '../../support/test_helper'
require_relative '../../support/zopim_test_helper'
require_relative '../../support/entitlement_test_helper'
require_relative '../../support/suite_test_helper'
require_relative "../../support/satisfaction_test_helper"
require_relative '../../support/rule'

SingleCov.not_covered!

describe 'UserDeletion' do
  include ZopimTestHelper
  include EntitlementTestHelper
  include SuiteTestHelper
  include SatisfactionTestHelper
  include TestSupport::Rule::Helper

  fixtures :all
  resque_inline false

  let(:entitlements_get_response) { make_entitlements_response }
  let(:patch_status) { 200 }
  let(:get_status) { 200 }

  before do
    @account = accounts(:minimum)
    @admin = users(:minimum_admin)
    @agent = users(:minimum_agent)
    @agent.external_id = "foo"
    @agent.phone = "555"
    @agent.save
    UnverifiedEmailAddress.create!(account: @account, user: @agent, email: "foo@foo.com", expires_at: Time.now + 20.minutes)
    Collaboration.create!(account: @account, user: @agent, ticket: tickets(:minimum_1))
    Collaboration.create!(account: @account, user: @agent, ticket: tickets(:minimum_2))
    Collaboration.create!(account: @account, user: @agent, ticket: tickets(:minimum_3))
    @suspended_ticket = suspended_tickets(:minimum_spam)
    @suspended_ticket.update_attribute(:author_id, @agent.id)
    @token = @agent.tokens.new(
      account: @account,
      client_id: 1,
      expires_at: Time.now + 2.weeks
    )
    @verification_token = VerificationToken.create!(account: @account, source: @agent)
    @agent.current_user = @admin
    FactoryBot.create(:session_record, user: @agent)

    stub_staff_service_patch_request('minimum', patch_status)
  end

  describe "#cannot_update_deleted_external_id" do
    it "prevents changing the external_id of an inactive (deleted) user" do
      @agent.delete!
      @agent.external_id = '12345'
      refute @agent.save
      refute_equal @agent.reload.external_id, '12345'
    end

    it "allows changing the external_id of an active user" do
      @agent.external_id = '12345'
      assert @agent.save
      assert_equal @agent.reload.external_id, '12345'
    end
  end

  describe ".inactive_and_not_redacted" do
    let(:default_options) do
      {
        executer: @admin
      }
    end

    before do
      @agent.delete!
    end

    it 'should return the inactive user' do
      assert User.inactive_and_not_redacted.map(&:id).include?(@agent.id)
    end

    it 'should not return the ultra_deleted user' do
      ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
      @agent.mark_for_ultra_deletion!(default_options)
      refute User.inactive_and_not_redacted.map(&:id).include?(@agent.id)
    end
  end

  describe "#mark_for_ultra_deletion!" do
    let(:default_options) do
      {
        executer: @admin
      }
    end

    describe "with a deleted agent" do
      before do
        @agent.delete!
      end

      it 'should raise an error for an active user' do
        assert_raise ComplianceUserDeletionError do
          @admin.mark_for_ultra_deletion!(default_options)
        end
      end

      it "should create a compliance_deletion_status" do
        ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true).once

        assert_difference -> { ComplianceDeletionStatus.where(user_id: @agent.id).count }, 1 do
          @agent.mark_for_ultra_deletion!(default_options)
        end
        status = ComplianceDeletionStatus.where(user_id: @agent.id).last

        assert_equal @account.owner.id, status.executer_id
      end

      describe "#ultra_deleted?" do
        it "should be true after calling mark_for_ultra_deletion!" do
          ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true).once

          refute @agent.ultra_deleted?
          @agent.mark_for_ultra_deletion!(default_options)
          assert @agent.reload.ultra_deleted?
        end
      end
    end
  end

  describe "#ultra_delete!" do
    let(:default_options) do
      {
        executer: @admin
      }
    end

    describe "update_gdpr_status" do
      it 'should call GdprSnsFeedbackPublisher#publish' do
        Zendesk::PushNotifications::Gdpr::GdprSnsFeedbackPublisher.any_instance.expects(:publish).
          with(message: { test: 'test', account_id: @agent.account_id, account_subdomain: @agent.account.subdomain, user_id: @agent.id, action: 'complete', application: 'classic', pod: @agent.account.pod_id }).once
        @agent.delete!
        @agent.send(:update_gdpr_status, @agent.account, { test: 'test' }, 'complete')
      end
    end

    describe 'active vs inactive accounts' do
      it 'should not call GdprSnsFeedbackPublisher#publish when inactive' do
        @agent.expects(:update_gdpr_status).never
        Arturo.enable_feature!(:gdpr_user_pii_compliance)
        @agent.delete!
        Account.any_instance.stubs(:is_active?).returns(false)
        Account.any_instance.stubs(:is_serviceable?).returns(false)
        @agent.expects(:delete).never
        @agent.ultra_delete!
      end

      it 'should call GdprSnsFeedbackPublisher#publish when active' do
        @agent.expects(:update_gdpr_status).once
        Arturo.enable_feature!(:gdpr_user_pii_compliance)
        @agent.delete!
        Account.any_instance.stubs(:is_active?).returns(true)
        @agent.expects(:delete).once
        @agent.ultra_delete!
      end
    end

    describe 'users satisfaction ratings' do
      let(:ticket) { build_ticket_with_satisfaction_rating }
      let(:enduser) { ticket.satisfaction_ratings.first.enduser }
      let(:agent_user) { ticket.satisfaction_ratings.first.agent }

      before do
        enduser.stubs(:cannot_delete_with_unclosed_tickets).returns(true)
        agent_user.stubs(:cannot_delete_with_unclosed_tickets).returns(true)
        Channels::AnyChannel::UserDestroyEventHandler.stubs(:user_deleted)

        enduser.current_user = @admin
        enduser.delete!

        agent_user.current_user = @admin
        agent_user.delete!
      end

      it "should remove the enduser ratings" do
        enduser.expects(:update_gdpr_status).once
        enduser.ultra_delete!(default_options)
        assert enduser.satisfaction_ratings_received.count(:all) == 0
        assert enduser.satisfaction_ratings_created.first.comment.value == Zendesk::Scrub::SCRUB_TEXT
      end

      it 'should remove custom_field_values' do
        enduser.custom_field_values.update_from_hash("age" => "30.5")
        assert enduser.custom_field_values.count(:all) >= 1
        enduser.expects(:update_gdpr_status).once
        enduser.ultra_delete!(default_options)
        assert enduser.custom_field_values.count(:all) == 0
      end

      it "should remove the agent_user ratings" do
        agent_user.expects(:update_gdpr_status).once
        agent_user.ultra_delete!(default_options)
        assert agent_user.satisfaction_ratings_received.first.comment.value == Zendesk::Scrub::SCRUB_TEXT
        assert agent_user.satisfaction_ratings_created.count(:all) == 0
      end

      it "should remove the ratings for comments with archived tickets" do
        Ticket.any_instance.stubs(:archived?).returns(true)
        agent_user.expects(:update_gdpr_status).once
        agent_user.ultra_delete!(default_options)
        assert agent_user.satisfaction_ratings_received.first.comment.value == Zendesk::Scrub::SCRUB_TEXT
        assert agent_user.satisfaction_ratings_created.count(:all) == 0
      end
    end

    describe "users photo" do
      let(:url) { "http://example.com/foo.png" }

      before do
        @agent.delete!
        @agent.photo = FactoryBot.create(:photo, filename: "large_1.png", original_url: url)
      end

      it 'should raise an error for an active user' do
        @admin.expects(:update_gdpr_status).with(@account, default_options, 'error').once
        assert_raise ComplianceUserDeletionError do
          @admin.ultra_delete!(default_options)
        end
      end

      it "should remove the users photo" do
        @agent.expects(:update_gdpr_status).once
        assert @agent.reload.photo
        @agent.ultra_delete!(default_options)
        refute @agent.reload.photo.present?
      end
    end

    describe 'with a deleted user' do
      before do
        @agent.delete!
        @agent.expects(:update_gdpr_status).once
      end

      it "should change the name to Permanently deleted user" do
        @agent.ultra_delete!(default_options)
        assert_equal I18n.t('txt.admin.views.settings.people.compliance.user_name'), @agent.name
      end

      it 'should remove all associations to organizations' do
        @agent.organization = organizations(:minimum_organization1)
        @agent.organizations << organizations(:minimum_organization2)
        @agent.save!

        assert @agent.organizations.count > 1
        @agent.ultra_delete!(default_options)
        assert @agent.organizations.count == 0
        assert @agent.organization.nil?
      end

      describe 'deleted agent role validations' do
        before do
          @account.permission_sets.stubs(:exists?).returns(false)
          @agent.stubs(:permission_set_id).returns(1)
        end

        it 'should delete the user despite the users permission set id not existing in the account' do
          assert @agent.ultra_delete!(default_options)
        end
      end
    end
  end

  describe "#prepare_for_deletion" do
    it "randomize_passwords" do
      password = @agent.password
      @agent.delete!
      refute_equal password, @agent.password
    end

    it "sets external_id to nil" do
      @agent.delete!
      refute_equal "foo", @agent.external_id
    end

    it "removes all identities" do
      @agent.delete!
      assert_empty(@agent.identities)
    end

    describe "with any_channels indentities" do
      let(:external_id) { 'integration123' }
      let(:ris_id) { '123' }

      before do
        @agent.identities.delete_all

        @agent.identities << UserAnyChannelIdentity.create!(
          user: @agent,
          account: @agent.account,
          value: "#{ris_id}:#{external_id}"
        )
        Channels::AnyChannel::UserDestroyEventHandler.stubs(:user_deleted)
      end

      it "deletes all related any channels profiles" do
        ::Channels::AnyChannelUserProfile.create!(
          account: @agent.account, external_id: external_id, name: 'screen_name'
        )

        @agent.delete!
        assert_empty(::Channels::AnyChannelUserProfile.where(user: @agent))
      end

      it "notifies channels of the identity soft-delete" do
        Channels::AnyChannel::UserDestroyEventHandler.expects(:user_deleted).once
        @agent.delete!
      end
    end

    it "deletes unverified_email_addresses" do
      @agent.delete!
      assert_empty(UnverifiedEmailAddress.where(account_id: @account.id, user_id: @agent.id))
    end

    it "deletes legacy phone number" do
      @agent.delete!
      assert_nil @agent.phone
    end

    it "deletes all sessions" do
      assert @agent.shared_sessions.any?
      @agent.delete!
      assert_empty(@agent.shared_sessions)
    end

    it "deletes all oauth tokens" do
      @agent.delete!
      assert_empty(Zendesk::OAuth::Token.where(account_id: @account.id, user_id: @agent.id))
    end

    it "deletes all verification tokens" do
      @agent.reload.delete!
      assert_empty(@agent.verification_tokens)
    end

    let(:bookmark) { @agent.bookmarks.new(ticket: tickets(:minimum_1), account: @account) }
    it "deletes all bookmarks" do
      bookmark.save!
      @agent.delete!
      assert_empty(Ticket::Bookmark.where(account_id: @account_id, user_id: @agent.id))
    end

    let(:entry) { entries(:ponies) }
    let(:source_type) { "Entry" }
    let(:watching) do
      @agent.watchings.new(source: entry, source_type: source_type, user: @agent, account: @account)
    end
    it "deletes all watchings" do
      watching.save!
      @agent.delete!
      assert_equal 0, Watching.where(account_id: @account.id, user_id: @agent.id).count(:all)
    end

    describe "when a user has a zopim identity" do
      before do
        cp_set = permission_sets(:chat_agent_permission_set)
        cp_set.update_column(:account_id, @agent.account_id)

        @agent.permission_set = cp_set
        @agent.save!

        Account.any_instance.stubs(has_chat_permission_set?: true)

        Zopim::AgentSyncJob.stubs(:work)
        ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
        ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)
        ZopimIntegration.stubs(where: stub(:first_or_create!))
        ZopimIntegration.any_instance.stubs(:ensure_not_phase_four)
        Zopim::Reseller.client.stubs(
          create_account!: Hashie::Mash.new(id: 1),
          account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
          create_account_agent!: stub(id: 1234),
          find_accounts!: [],
          delete_account_agent!: stub
        )

        @agent.account.create_zopim_subscription!.tap do |subscription|
          subscription.zopim_account_id = 1
          subscription.syncstate        = ZendeskBillingCore::Zopim::SyncState::Ready
          subscription.save!
        end
        @agent.account.zopim_subscription.stubs(is_serviceable?: true)

        @agent.enable_chat_identity!.tap do |identity|
          identity.zopim_agent_id = 2
          identity.save!
        end
      end

      describe "for an agent" do
        it "removes the agent's zopim identity upon deletion" do
          assert @agent.delete!
          assert @agent.zopim_identity.destroyed?
        end
      end

      describe "for an end user" do
        describe "that deletes" do
          it "removes the user's zopim record upon deletion" do
            User.any_instance.stubs(is_end_user?: true)
            Zopim::Reseller.client.expects(:delete_account_agent!).once
            assert @agent.delete!
            assert @agent.zopim_identity.destroyed?
          end
        end

        describe "that rolls back" do
          it "does not remove the user's zopim record and does not delete" do
            User.any_instance.stubs(is_end_user?: true)
            Zopim::AgentDeactivator.any_instance.expects(:deactivate!).never
            with_rollback(@agent) { @agent.delete! }
            refute @agent.zopim_identity.destroyed?
          end
        end
      end

      describe "when the zopim identity is the owner" do
        before do
          @agent.zopim_identity.update_column(:is_owner, true)
        end

        it "reports a useful error message" do
          assert_raise(ActiveRecord::RecordInvalid) { @agent.delete! }
          assert_equal I18n.t('txt.admin.model.user.deletion.zopim_identity_issue'), @agent.errors.messages[:base][0]
        end
      end

      describe "when an upstream error occurs" do
        before do
          @agent.zopim_identity.expects(:destroy).raises(StandardError)
        end

        it "logs the exception and reraises" do
          @agent.expects(:enable_chat_identity!).never
          ZendeskExceptions::Logger.expects(:record)
          assert_raises StandardError do
            @agent.delete!
          end
        end
      end

      describe "when the zopim identity is not enabled" do
        before do
          @agent.zopim_identity.update_column(:is_enabled, false)
        end

        it "does not try to enable it" do
          @agent.zopim_identity.expects(:enable!).never

          @agent.delete!
        end
      end
    end
  end

  describe "#record_destroy_via_cia" do
    it "generates a CIA record of the user deletion" do
      CIA.expects(:record).with(anything, anything).at_least_once
      CIA.expects(:record).with(:destroy, @agent)
      @agent.delete!
    end
  end

  describe "#remove_collaborations" do
    it "removes collaborations affiliated with the user" do
      assert_difference "Collaboration.count(:all)", -3 do
        @agent.delete!
      end
    end
  end

  describe "#remove_suspended_tickets" do
    it "clears suspended tickets affiliated with the user" do
      assert_difference "SuspendedTicket.where(author_id: @agent.id).count(:all)", -1 do
        @agent.delete!
      end
    end
  end

  describe "#enqueue_unassign_ticket_job" do
    it "enqueues the UnassignTicketsJob after agent has been deleted" do
      Resque.expects(:enqueue).with(UnassignTicketsJob, @agent.account_id, nil, @agent.id, @agent.current_user.id, ViaType.USER_DELETION, anything, anything)
      @agent.delete!
    end

    it "creates an UnassignTicketsJob if a foreign agent was deleted" do
      @agent.stubs(:is_agent?).returns(false)
      @agent.stubs(:foreign_agent?).returns(true)
      Resque.expects(:enqueue).with(UnassignTicketsJob, @agent.account_id, nil, @agent.id, @agent.current_user.id, ViaType.USER_DELETION, anything, anything)
      @agent.delete!
    end
  end

  describe "#enqueue_gooddata_destroy_job" do
    before do
      @gooddata_user = GooddataUser.new do |u|
        u.account_id = @agent.account_id
        u.gooddata_user_id = '1'
        u.gooddata_project_id = '11'
        u.user_id = @agent.id
      end
      @gooddata_user.save!
    end

    it "enqueues the GooddataUserDestroyJob after agent has been deleted" do
      GooddataUserDestroyJob.expects(:enqueue).with(@agent.account_id, @gooddata_user.id)
      @agent.delete!
    end
  end

  it "does not attempt to clean tickets for a user that was already deleted" do
    @agent.update_column(:is_active, false)
    @agent.expects(:record_destroy_via_cia).never
    @agent.expects(:remove_collaborations).never
    @agent.expects(:remove_suspended_tickets).never
    @agent.expects(:enqueue_unassign_ticket_job).never
    @agent.delete!
  end

  it "does not enqueue jobs if we had to rollback" do
    Resque.jobs.clear
    with_rollback(@agent) { @agent.delete! }
    refute @agent.reload.deleted?
    assert_equal 0, Resque.jobs.size
  end

  describe "#remove_user_seats" do
    it "removes user seats" do
      FactoryBot.create(:voice_user_seat, account: @account, user: @agent)

      assert_difference "@agent.user_seats.count", -1 do
        @agent.delete!
      end
    end
  end

  describe "#delete_personal_macros" do
    let(:agent)   { users(:minimum_agent) }
    let(:account) { agent.account }
    let(:group)   { groups(:minimum_group) }

    before do
      Macro.destroy_all

      Subscription.any_instance.stubs(has_group_rules?: true)

      create_macro(title: 'ZB', owner: account, active: false, position: 2)
      create_macro(title: 'ZA', owner: account, active: true, position: 1)
      create_macro(title: 'CB', owner: group, active: false, position: 6)
      create_macro(title: 'CA', owner: group, active: true, position: 5)
    end

    describe_with_arturo_enabled :delete_personal_macros_when_delete_agent do
      describe "and agent has personal macros" do
        before do
          create_macro(title: 'AB', owner: agent, active: false, position: 4)
          create_macro(title: 'AA', owner: agent, active: true, position: 3)

          agent.is_active = false
          agent.save!
        end

        it "delete personal macros" do
          assert_equal 0, agent.macros.count
        end
      end

      describe "and agent does not have personal macros" do
        before do
          agent.is_active = false
          agent.save!
        end

        before_should "should not call delete on personal macros" do
          Macro.expects(:soft_delete_all!).never
        end
      end

      describe "when an agent is updated" do
        before do
          agent.name = "new name"
          agent.save!
        end

        before_should "should not call the delete method" do
          agent.expects(:delete_personal_macros).never
        end
      end
    end

    describe_with_arturo_disabled :delete_personal_macros_when_delete_agent do
      before do
        create_macro(title: 'AB', owner: agent, active: false, position: 4)
        create_macro(title: 'AA', owner: agent, active: true, position: 3)

        agent.is_active = false
        agent.save!
      end

      before_should "should not call delete on personal macros" do
        Macro.expects(:delete_personal_macros).never
      end
    end
  end
end
