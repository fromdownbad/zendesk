require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 13

require 'users/phone_number_behavior'
require 'users/phone_number_validator'

# TODO: too much stubbing happening in this file, use realistic data.
describe Users::PhoneNumberBehavior do
  fixtures :accounts

  let (:account) { accounts(:minimum) }

  module Fake1
    class User
      include ActiveModel::Validations
      include ActiveModel::Validations::Callbacks

      def initialize; end
      attr_accessor :phone, :id

      def self.validate(*_args)
        true
      end

      def new_record?
        true
      end

      include ::Users::PhoneNumberBehavior
    end
  end

  let(:user) { Fake1::User.new }
  let(:user_phone_identity) { UserPhoneNumberIdentity.new }
  let(:voice_forwarding_identity) { UserVoiceForwardingIdentity.new }

  let (:user_phone_1) { UserPhoneNumberIdentity.new(user_id: user.id, value: "+15551234567") }
  let (:user_phone_2) { UserPhoneNumberIdentity.new(user_id: user.id, value: "+15559876543") }
  let(:user_phone_extension) { UserPhoneExtension.new(user_id: user.id, value: '+123') }

  before do
    Fake1::User.stubs(:validate).returns(true)
    Fake1::User.any_instance.stubs(:account).returns(accounts(:minimum))
  end

  describe "#update_phone_and_identity" do
    describe "when a phone identity already exists" do
      before do
        user.stubs(:phone_identity).returns(user_phone_identity)
      end

      describe "when direct_line is true and number is a valid identity" do
        before { user_phone_identity.stubs(:save).returns(true) }

        it "sets phone field and updates identity" do
          user.update_phone_and_identity('+353851234567', true)
          user_phone_identity.value.must_equal '+353851234567'
        end

        describe "when end user phone validation is enabled" do
          before do
            Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
            user_phone_identity.expects(:save).returns(true)
          end

          it "saves the identity" do
            user.update_phone_and_identity('+353851234567', true)
          end
        end
      end

      describe "when direct_line is true and number is an invalid identity" do
        before { user_phone_identity.stubs(:save).returns(false) }

        it "doesn't change anything" do
          user.update_phone_and_identity('123', true)

          user.phone.must_be_nil
        end
      end

      describe "when direct_line is false" do
        it "sets the phone field and destroys identity" do
          user_phone_identity.expects(:destroy).once

          user.update_phone_and_identity('123', false)

          user.phone.must_equal '123'
        end
      end
    end

    describe "without an existing phone identiy" do
      before do
        user.stubs(:identities).returns([])
        user.stubs(:phone_identity).returns(nil)
        user.stubs(:build_phone_identity).returns(user_phone_identity)
      end

      describe "when new end user phone validation is enabled and new record" do
        before do
          Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
        end

        it "does not save the identity when user is new record" do
          user.stubs(:new_record?).returns(true)
          user_phone_identity.expects(:save).never
          user.update_phone_and_identity('+353851234567', true)
        end

        it "does not save the identity when user is not new record" do
          user.stubs(:new_record?).returns(false)
          user_phone_identity.expects(:save).once
          user.update_phone_and_identity('+353851234567', true)
        end
      end

      describe "when direct_line is true and number is a valid identity" do
        before do
          user_phone_identity.stubs(:save).returns(true)
          user_phone_identity.value = '353851234567'
        end

        it "sets the phone field and creates a new identity" do
          user.update_phone_and_identity('+353851234567', true)
          user.identities.must_include(user_phone_identity)
        end
      end

      describe "when direct_line is true and number is an invalid identity" do
        before { user_phone_identity.stubs(:save).returns(false) }

        it "doesn't change anything" do
          user.phone = '000'

          user.update_phone_and_identity('123', true)

          user.phone.must_equal '000'
        end
      end

      describe "when direct_line is false" do
        it "sets the phone field" do
          user.phone = '000'

          user.update_phone_and_identity('123', false)

          user.phone.must_equal '123'
        end

        describe "and number contains an extension" do
          before do
            user.expects(:add_shared_phone_number_extension).with('x123')
          end

          it "saves the identity" do
            user.update_phone_and_identity('123x123', false)
          end
        end
      end
    end
  end

  describe "#direct_number" do
    describe "with phone identity" do
      before do
        user_phone_identity.stubs(:value).returns('123')
        user.stubs(:phone_identity).returns(user_phone_identity)
      end

      it "returns direct number" do
        user.direct_number.must_equal '123'
      end
    end

    describe "with no phone identity" do
      before do
        user.stubs(:identities).returns([])
      end

      it "returns nil" do
        user.direct_number.must_be_nil
      end
    end

    describe "with destroyed phone identity" do
      before do
        user_phone_identity.stubs(:destroyed?).returns(true)
        user_phone_identity.stubs(:value).returns('123')
        user.stubs(:identities).returns([user_phone_identity])
      end

      it "returns nil" do
        user.direct_number.must_be_nil
      end
    end

    describe "with voice forwarding identity" do
      before do
        voice_forwarding_identity.stubs(:value).returns('123')
        user.stubs(:identities).returns([voice_forwarding_identity])
      end

      it "returns nil" do
        user.direct_number.must_be_nil
      end
    end
  end

  describe "#phone_number" do
    describe "with shared_phone_number only" do
      before do
        user.stubs(:shared_phone_number).returns(true)
        user.stubs(:phone).returns('+15551231234')
        user.stubs(:user_phone_extension)
      end

      it "returns shared_phone_number" do
        user.phone_number.must_equal '+15551231234'
      end
    end

    describe "with direct line only" do
      before do
        user.stubs(:shared_phone_number).returns(false)
        user.stubs(:first_direct_line_number_by_priority).returns('+15551231234')
      end

      it "returns direct line number" do
        user.phone_number.must_equal '+15551231234'
      end
    end

    describe "with (legacy) shared_number containing direct-line and extension" do
      before do
        user.stubs(:shared_phone_number).returns(false)
        user.stubs(:phone).returns('+15551231234x2334')
        user.stubs(:first_direct_line_number_by_priority).returns('+15551231234')
      end

      it "returns number and extension" do
        user.phone_number.must_equal '+15551231234x2334'
      end
    end
  end

  describe "#rendered_phone" do
    describe "when no phone identity exists for this user" do
      before { user.stubs(:direct_number).returns(nil) }

      it "returns user.phone" do
        user.phone = '1112223333'

        user.rendered_phone.must_equal user.phone
      end
    end

    describe "when a phone identity exists for this user" do
      before do
        user.phone = "111"
        user_phone_identity.value = "222"
        user.stubs(:direct_number).returns(user_phone_identity.value)
      end

      it "returns a properly rendered number" do
        user.expects(:render_phone_number).with(user.direct_number, inline: false).returns(user.direct_number)

        user.rendered_phone.must_equal user.direct_number
      end
    end
  end

  describe "#shared_phone_number" do
    describe "when phone attribute is empty and no phone identities" do
      before do
        user.stubs(:phone).returns(nil)
        user.stubs(:first_direct_line_number_by_priority).returns(nil)
      end

      it "returns nil" do
        user.shared_phone_number.must_be_nil
      end
    end

    describe "when no phone identity exists for this user" do
      before do
        user.phone = "+1 555-123-4567"
        user.stubs(:has_direct_number?).returns(false)
      end

      it "returns true" do
        user.shared_phone_number.must_equal true
      end
    end

    describe "when a phone identity exists for this user" do
      before do
        user.phone = "+1 555-123-4567"
        user.stubs(:has_direct_number?).returns(true)
      end

      it "returns false" do
        user.shared_phone_number.must_equal false
      end
    end
  end

  describe "#phone_identity" do
    describe "when user has multiple phone identities" do
      before do
        user.stubs(:identity_sorted_by_priority).returns(user_phone_1)
        user.stubs(:first_identity_by_number).with(user_phone_2.value).returns(user_phone_2)
        user.stubs(:first_identity_by_number).with(nil).returns(user_phone_2)
        user.stubs(:identities).returns([user_phone_1])
        user.stubs(:reset_phone_identities_cache).returns(true)
      end

      it "returns first phone_identity when no number passed" do
        user.phone_identity.must_equal user_phone_1
      end

      it "returns matching phone_identity when no number passed" do
        user.phone_identity(user_phone_2.value).must_equal user_phone_2
      end
    end
  end

  describe "#remove_phone_number_identity" do
    describe "with a phone identity" do
      before do
        user_phone_identity.stubs(:value).returns('123')
        user.stubs(:phone_identity).with('123').returns(user_phone_identity)
        user_phone_identity.expects(:destroy)
      end

      it "returns direct number" do
        user.remove_phone_number_identity('123')
      end
    end

    describe "with no phone identity" do
      before do
        user.stubs(:identities).returns([])
      end

      it "returns nil" do
        user.direct_number.must_be_nil
      end
    end
  end

  describe "#has_direct_number" do
    describe "when user has no shared_phone_number or direct lines" do
      before do
        user.stubs(:phone).returns(nil)
        user.stubs(:phone_identities).returns([])
      end
      it "returns false if no number passed in" do
        user.has_direct_number?.must_equal false
      end

      it "returns false if any number passed in" do
        user.has_direct_number?('123').must_equal false
      end
    end

    describe "when user has direct line number" do
      before do
        user_phone_identity.stubs(:destroyed?).returns(true)
        user_phone_identity.stubs(:value).returns('123')
        user.stubs(:phone_identities).returns([user_phone_identity])
      end

      describe "when account has phone number validation disabled" do
        before do
          Account.any_instance.stubs(:apply_phone_number_validation?).returns(false)
        end

        it "returns false if no number passed in" do
          user.has_direct_number?.must_equal false
        end

        it "returns true when identity with same number exists" do
          user.has_direct_number?('123').must_equal true
        end
      end

      describe "when account has phone number validation enabled" do
        before do
          Account.any_instance.stubs(:apply_phone_number_validation?).returns(true)
          user_phone_identity.stubs(:value).returns('+123')
        end

        it "returns false if no number passed in" do
          user.has_direct_number?.must_equal false
        end

        it "returns true when identity with same number exists" do
          user.has_direct_number?('+123').must_equal true
        end
      end
    end
  end

  describe "#add_shared_phone_number_extension" do
    describe "adds extension" do
      before do
        user_phone_extension = UserPhoneExtension.new(user_id: user.id, value: '+123')
        user.expects(:build_user_phone_extension).returns(user_phone_extension)
        user.stubs(:user_phone_extension).returns(user_phone_extension)
      end

      it "should add the extension" do
        user.add_shared_phone_number_extension('+123')

        user.user_phone_extension.value.must_equal('+123')
      end
    end
  end
end
