require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Users::ChatSupport' do
  fixtures :users
  describe "Given a User" do
    before do
      @user = users(:minimum_agent)
      @user.account.stubs(chat_welcome_message: 'Howdy', maximum_chat_requests: 3)
    end

    it "has chat settings" do
      assert_equal 'Howdy', @user.chat_settings[:welcome_message]
      assert_equal 3,       @user.chat_settings[:maximum_requests]
    end
  end
end
