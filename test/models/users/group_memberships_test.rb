require_relative "../../support/test_helper"

SingleCov.covered!

describe 'UserGroupMemberships' do
  fixtures :accounts, :users, :memberships, :groups, :tickets, :role_settings
  include DomainEventsHelper

  describe Users::GroupMemberships do
    let(:account)  { accounts(:with_groups) }
    let(:agent)    { users(:with_groups_agent1) } # has one membership
    let(:agent2)   { users(:with_groups_agent2) } # has two memberships
    let(:end_user) { users(:minimum_end_user) }
    let(:admin)    { users(:with_groups_admin) }
    let(:group1)   { groups(:with_groups_group1) }
    let(:group2)   { groups(:with_groups_group2) }

    describe "scopes" do
      describe ".in_group" do
        it "returns users that are members of the group with the provided id" do
          actual = User.in_group(group1.id).to_sql
          actual.gsub!(/ +/, " ")
          actual.must_include("SELECT `users`.")
          actual.must_include("WHERE `memberships`.`group_id` = #{group1.id}")
        end

        it "is able generate the scope conditions from a group" do
          actual = User.in_group(group1).to_sql
          actual.gsub!(/ +/, " ")
          actual.must_include("SELECT `users`.")
          actual.must_include("WHERE `memberships`.`group_id` = #{group1.id}")
        end
      end
    end

    describe "validations" do
      describe "removal of group memberships" do
        describe "when upgrading agent to admin" do
          it "does not remove memberships" do
            assert_equal 1, agent.memberships.size
            agent.role = 'admin'
            agent.valid?
            assert_equal 1, agent.memberships.size
          end
        end

        describe "when downgrading agent to end-user" do
          it "removes memberships" do
            assert_equal 1, agent.memberships.size
            agent.role = 'end-user'
            agent.valid?
            assert_empty agent.memberships
          end
        end

        describe "when role is unchanged on end-user" do
          it "does not remove memberships" do
            end_user.memberships.build
            assert_equal 1, end_user.memberships.size
            end_user.valid?
            assert_not_empty end_user.memberships
          end
        end
      end

      describe "on an agent" do
        let(:new_agent) do
          account.users.new do |user|
            user.name  = "new agent"
            user.roles = Role::AGENT.id
            user.email = "someone@example.com"
          end
        end

        it "ensures new agents have a group membership" do
          assert_difference("new_agent.memberships.size", 1) do
            new_agent.save!
          end
          assert_equal account.default_group.id, new_agent.default_group_id
        end

        it "ensures existing agents have a group membership" do
          agent.memberships.delete_all

          assert_difference("agent.memberships.size", 1) do
            agent.save!
          end
          assert_equal account.default_group.id, agent.default_group_id
        end

        it "validates the associated memberships" do
          assert agent.memberships.all?(&:valid?)
          agent.memberships.build
          refute agent.valid?

          assert_includes agent.errors[:memberships].join, "is invalid"
        end

        describe "validate_default_group_id" do
          it "is valid if default_group_id is not set" do
            agent.memberships.delete_all
            assert_nil agent.default_group_id
            assert agent.valid?
          end

          it "is invalid if the default_group is removed" do
            agent.current_user = admin
            agent.groups = []
            refute agent.valid?

            assert_equal(
              ["The agent must be a member of the group that has been selected as the default group."],
              agent.errors[:default_group_id]
            )
          end
        end
      end
    end

    describe "soft deleting a user" do
      let(:domain_event_publisher) { FakeEscKafkaMessage.new }

      it "does not publish group_membership_removed events" do
        agent.current_user = agent.account.owner
        agent.delete!
        assert_equal decode_user_events(domain_event_publisher.events, :user_group_removed).size, 0
      end
    end

    describe "after save" do
      describe "on an agent without a default membership" do
        let(:domain_event_publisher) { FakeEscKafkaMessage.new }

        describe "with multiple memberships" do
          let(:agent) { users(:with_groups_agent2) }

          it "updates the first membership to be the default" do
            agent.memberships.detect(&:default).update_attribute(:default, false)

            assert_nil agent.default_group_id
            agent.save!
            assert agent.reload.default_group_id
          end
        end

        describe "with one membership" do
          it "updates it to be the default" do
            agent = users(:minimum_agent)
            refute agent.memberships.first.default?
            agent.save!
            assert agent.reload.memberships.first.default?
          end
        end

        it "does not publish group_membership_added event" do
          agent = users(:minimum_agent)
          agent.save!
          assert_equal decode_user_events(domain_event_publisher.events, :user_group_added).size, 0
        end
      end

      describe "on an agent with a default membership" do
        it "does not attempt to update the default membership" do
          Membership.any_instance.expects(:update_attributes!).never
          assert agent.memberships.first.default?
          agent.save!
        end
      end
    end

    describe "#default_group_id=" do
      it "shoulds call to_i on the argument" do
        argument = "1"
        argument.expects(:to_i).once
        agent.default_group_id = argument
      end
    end

    describe "after commit" do
      let(:agent) { users(:with_groups_agent2) }
      before { agent.current_user = admin }

      it "does not enqueue an UnassignTicketsJob if no groups were removed" do
        UnassignTicketsJob.expects(:enqueue).never
        agent.save!
      end

      it "enqueues an UnassignTicketsJob for each removed group" do
        expect_removed = agent.memberships.reject(&:default?).map(&:group_id)
        expect_removed.each do |group_id|
          UnassignTicketsJob.expects(:enqueue).with(
            agent.account_id, group_id, agent.id, admin.id, ViaType.USER_CHANGE
          )
        end
        agent.groups = [agent.default_group_id]

        agent.save!
      end
    end

    describe "#groups=" do
      describe "adding groups" do
        before do
          assert_equal 1, agent.memberships.size
          assert_equal group1.id, agent.memberships.first.group_id
        end

        it "adds new groups to the user's memberships" do
          agent.groups = [group1.id, group2.id]
          agent.save!

          assert_equal 2, agent.memberships.size
          assert_equal group2.id, agent.memberships.last.group_id
        end
      end

      describe "removing groups" do
        it "does not remove any groups if attempting to remove the default group" do
          assert agent2.groups = [group1.id]
          assert_equal [group2.id, group1.id], agent2.memberships.map(&:group_id)
        end

        it "invalidates the user if attempting to remove the default group" do
          agent2.current_user = agent
          assert agent2.groups = [group1.id]
          refute agent2.valid?
        end

        it "raises an error if the current_user is not set" do
          agent2.current_user = nil

          assert_raise RuntimeError, "Missing current_user" do
            agent2.groups = [group2.id]
            agent2.save!
          end
        end

        it "removes memberships" do
          agent2.current_user = users(:with_groups_admin)
          agent2.groups = [group2.id]
          agent2.save!

          assert_equal 1, agent2.memberships.size
          assert_equal group2.id, agent2.memberships.last.group_id
        end

        it "does not remove all memberships when an empty string is assigned" do
          agent2.current_user = users(:with_groups_admin)
          agent2.groups = ""

          assert_equal 2, agent2.memberships.size
        end
      end

      describe "with an empty string in the array" do
        it "rejects that element" do
          agent2.groups = ["", "123456"]

          assert_equal ["123456"], agent2.instance_variable_get("@groups_or_ids_being_set")
        end
      end
    end

    describe "#default_group_id=" do
      describe "for a user with a single group membership" do
        before do
          assert_equal [group1.id], agent.memberships.map(&:group_id)
          agent.default_group_id = group2.id
        end

        it "raises a validation error" do
          assert_raise ActiveRecord::RecordInvalid do
            agent.save!
          end
        end
      end

      describe "for a user with multiple group memberships" do
        before do
          assert_same_elements [group1.id, group2.id], agent2.memberships.map(&:group_id)
          assert_equal group2.id, agent2.default_group_id
        end

        it "updates the corresponding membership to be the default" do
          agent2.default_group_id = group1.id
          agent2.save!
          assert_equal group1.id, agent2.reload.default_group_id
        end

        it "fails if the group_id is not present in the user's memberships" do
          refute_includes agent2.memberships.map(&:group_id), 999
          agent2.default_group_id = 999

          assert_raises(ActiveRecord::RecordInvalid) do
            agent2.save!
          end
        end
      end
    end

    describe "#default_group_id" do
      describe "when the only default membership is for an inactive group" do
        before do
          assert_equal 1, agent.memberships.size
          assert agent.memberships.first.default?
          agent.memberships.first.group.update_attribute(:is_active, false)
        end

        it "returns nil" do
          assert_nil agent.default_group_id
        end
      end

      describe "when the user has default memberships for active and inactive groups" do
        before do
          assert_equal 2, agent2.memberships.size
          agent2.memberships.first.group.update_attribute(:is_active, false)
          agent2.memberships.update_all('`memberships`.default = 1')
        end

        it "returns the membership for the active group" do
          default_membership = agent2.memberships.detect { |membership| membership.group.is_active == true }
          assert_equal default_membership.group_id, agent2.default_group_id
        end
      end
    end

    describe "#in_group?" do
      it "returns true if the user is in the group" do
        assert agent.in_group?(group1)
        refute agent.in_group?(group2)
      end

      it "returns true if the user is in a group with the id provided" do
        assert agent.in_group?(group1.id)
        refute agent.in_group?(group2.id)
      end

      it "avoids N+1 membership count queries" do
        agent; group1; group2
        assert_sql_queries 1 do
          agent.in_group?(group1)
          agent.in_group?(group2)
        end
      end
    end

    describe "groups= and default_group_id= matrix test" do
      before do
        @agent = agent
        @agent.memberships.each(&:destroy)
        @agent.reload
        @agent.current_user = users(:with_groups_admin)
      end
      # current groups | current default group | groups being set | default group being set | expectation
      [
        [[],                  nil,    [],                 nil,      :build_default_group],
        [[],                  nil,    nil,                nil,      :build_default_group],
        [[],                  nil,    nil,                :group1,  :build_default_group],
        [[],                  nil,    [],                 :group1,  :invalid],
        [[],                  nil,    [:group1],          nil,      :set_default_group_id],
        [[],                  nil,    [:group1, :group2], nil,      :set_default_group_id],
        [[],                  nil,    [:group2],          nil,      :set_default_group_id],
        [[],                  nil,    [:group1],          :group2,  :invalid],
        [[],                  nil,    [:group1],          :group1,  :valid],
        [[],                  nil,    [:group2],          :group1,  :invalid],
        [[:group1, :group2], :group1, [:group1, :group2], :group1,  :valid],
        [[:group1, :group2], :group1, [:group1, :group2], :group2,  :valid],
        [[:group1, :group2], :group1, [:group2],           nil,     :invalid],
        [[:group1, :group2], :group1, [:group2],           :group1, :invalid],
        [[:group1, :group2], :group1, [],                  :group1, :invalid],
        [[:group1, :group2], :group1, nil,                 :group1, :valid],
        [[:group1, :group2], :group1, nil,                 nil,     :valid],
        [[:group1, :group2], :group1, nil,                 :group2, :valid],
        [[:group1, :group2], :group1, [],                  nil,     :invalid]
      ].each do |config|
        describe "with #{config.inspect}" do
          before do
            config.first.each do |group|
              Membership.create!(account: account, group: send(group), user: @agent, default: (config.second == group))
            end

            @agent.reload
          end

          it "expectations:s #{config.last}" do
            if config.third
              @agent.groups = config.third.map { |sym| send(sym) }
            end
            if config.fourth
              @agent.default_group_id = send(config.fourth).id
            end

            case config.last
            when :valid
              assert @agent.save!
            when :set_default_group_id
              @agent.save!
              assert @agent.reload.default_group_id
            when :invalid
              assert_raise(ActiveRecord::RecordInvalid) do
                @agent.save!
              end
            when :build_default_group
              @agent.save!
              @agent.reload
              assert_equal 1, @agent.memberships.size
              assert_equal account.default_group.id, @agent.memberships.first.group_id
            else
              raise "Unknown expectation"
            end
          end
        end
      end
    end

    describe "dispatch event at default group updated" do
      let(:new_group_id) { agent2.memberships.reject(&:default?).first.group_id }
      let(:domain_event_publisher) { FakeEscKafkaMessage.new }

      before do
        User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
      end

      it "dispatches the event when default group is changed" do
        User.any_instance.expects(:publish_user_events_to_bus!).once
        agent2.set_default_group(new_group_id)
      end

      it "dispatches the event when the membership with the default group is deleted" do
        membership = agent2.memberships.detect(&:default?)
        membership.destroy
        assert_equal decode_user_events(domain_event_publisher.events, :user_default_group_changed).size, 1
      end
    end

    describe "account ID scope inheritance" do
      it "groups relation" do
        assert_sql_queries(1, Regexp.new(Regexp.escape("`groups`.`account_id` = #{agent2.account_id}"))) do
          agent2.groups.to_a
        end
      end

      it "group memberships relation" do
        assert_sql_queries(1, Regexp.new(Regexp.escape("`memberships`.`account_id` = #{agent2.account_id}"))) do
          agent2.memberships.to_a
        end
      end
    end
  end
end
