require_relative '../../support/test_helper'
require_relative '../../support/zopim_test_helper'
require_relative '../../support/suite_test_helper'

SingleCov.covered!

describe 'Users::ChatAgentSupport' do
  include ZopimTestHelper
  include SuiteTestHelper

  fixtures :all

  describe "given a user" do
    let(:account) { accounts(:minimum) }

    describe "#assign_chat_permission_set_id" do
      describe "when the user is an end-user" do
        let(:user) { account.users.detect(&:is_end_user?) }

        it "turns the user into a chat-agent" do
          user.expects(:make_chat_agent!).once
          user.assign_chat_permission_set_id
        end
      end

      describe "when the user is not an end-user" do
        let(:user) { account.users.detect(&:is_agent?) }

        it "does not turn the user into a chat-agent" do
          user.expects(:make_chat_agent!).never
          user.assign_chat_permission_set_id
        end
      end
    end

    describe "#make_chat_agent!" do
      let(:user) { account.users.detect(&:is_agent?) }

      describe "when the account has the chat-agent permission set" do
        before do
          Account.any_instance.stubs(
            has_chat_permission_set?: true,
            has_permission_sets?: true
          )
          ::PermissionSet.create_chat_agent!(account)
        end

        describe "and the user is not associated with a zopim identity" do
          before do
            user.expects(:create_zopim_identity!).once
            user.make_chat_agent!
          end

          it "assigns the agent role to the user" do
            assert_equal Role::AGENT.id, user.roles
          end

          it "assigns the account's permission set to the user" do
            assert_equal account.chat_permission_set, user.permission_set
          end

          it "evaluates the user as a chat-agent" do
            assert user.is_chat_agent?
          end
        end

        describe "and the user is associated with a zopim identity" do
          let(:zopim_identity) do
            stub(
              zopim_agent_id: 1234,
              update_attribute: true,
              is_owner?: false
            )
          end

          before do
            User.any_instance.stubs(zopim_identity: zopim_identity)
          end

          describe "and the zopim_identity is enabled" do
            before { zopim_identity.expects(:is_enabled?).returns(true) }

            it "leave the zopim_identity alone" do
              zopim_identity.expects(:enable!).never
              user.make_chat_agent!
            end
          end

          describe "and the zopim_identity is disabled" do
            before { zopim_identity.expects(:is_enabled?).returns(false) }

            it "enables the current zopim_identity" do
              zopim_identity.expects(:enable!).once
              user.make_chat_agent!
            end
          end
        end
      end

      describe "when the account does not have the chat-agent permission set" do
        before do
          Account.any_instance.stubs(has_chat_permission_set?: false)
        end

        it "raises an error" do
          error = assert_raises ActiveRecord::RecordNotSaved do
            user.make_chat_agent!
          end
          assert_equal 'AccountChatAccessNotEnabled', error.message
        end
      end
    end

    describe "#remove_chat_permission_set_id" do
      let(:user) { account.users.detect(&:is_agent?) }

      let(:zopim_identity) do
        stub(
          zopim_agent_id: 1234,
          update_attribute: true,
          is_owner?: false,
          is_enabled?: true
        )
      end

      before do
        user.stubs(zopim_identity: zopim_identity)
      end

      describe "when the user is a chat-agent" do
        before do
          Account.any_instance.stubs(
            has_chat_permission_set?: true,
            has_permission_sets?:     true
          )
          ::PermissionSet.create_chat_agent!(account)
          user.make_chat_agent!

          zopim_identity.expects(:disable!).once
          user.remove_chat_permission_set_id
        end

        it "clears the user's permission set" do
          assert_nil user.permission_set
        end

        it "assigns the end-user role to the user" do
          assert_equal Role::END_USER.id, user.roles
        end

        it "evaluates the user as an end-user" do
          assert user.is_end_user?
        end
      end

      describe "when the user is not a chat-agent" do
        before do
          zopim_identity.expects(:disable!).never
          user.remove_chat_permission_set_id
        end

        it "does not assign the end-user role to the user" do
          assert_not_equal Role::END_USER.id, user.roles
        end

        it "does not evaluate the user as an end-user" do
          refute user.is_end_user?
        end
      end
    end

    describe 'saving a chat-only agent record' do
      let(:user) { account.users.detect(&:is_agent?) }

      before do
        User.any_instance.stubs(is_chat_agent?: true)
      end

      describe "when there is no zopim identity associated" do
        it "assigns a new zopim identity to the user" do
          user.expects(:create_zopim_identity!).once
          user.save!
        end
      end
    end

    describe 'saving an end-user record' do
      let(:user) { account.users.detect(&:is_end_user?) }

      let(:zopim_identity) do
        stub(
          zopim_agent_id: 1234,
          update_attribute: true,
          is_enabled?: true
        )
      end

      before do
        User.any_instance.stubs(zopim_identity: zopim_identity)
      end

      describe "when there is a zopim identity associated" do
        it "disables the zopim identity" do
          zopim_identity.expects(:disable!).once
          user.save!
        end
      end
    end

    describe '#remove_zopim_agent_record!' do
      let(:user) { account.users.detect(&:is_end_user?) }
      let(:zopim_identity) { stub }

      before { user.stubs(zopim_identity: zopim_identity) }

      it 'calls destroy on the zopim_identity when present' do
        zopim_identity.expects(:destroy)
        user.remove_zopim_agent_record!
      end
    end

    describe "for a non-enterprise account with zopim enabled" do
      let(:zopim_identity) do
        stub(
          zopim_agent_id: 1234,
          update_attribute: true,
          is_owner?: false,
          is_enabled?: true
        )
      end

      before do
        Account.any_instance.stubs(
          has_chat_permission_set?: true,
          has_permission_sets?: false
        )
        user.stubs(zopim_identity: zopim_identity)
        ::PermissionSet.create_chat_agent!(account)
        user.stubs(:create_zopim_identity!)
      end

      describe "when the user is a chat-agent" do
        let(:user) { account.users.detect(&:is_agent?) }

        before do
          user.make_chat_agent!
        end

        describe "and its role is changed" do
          before { CIA.stubs(:current_actor).returns(user) }

          it "clears the permission_set_id flag" do
            user.expects(:permission_set_id=).with(nil).twice
            user.role = 'admin'
            Zendesk::SupportUsers::User.new(user).save!
          end
        end
      end

      describe "when the user is an end-user" do
        let(:user) { account.users.detect(&:is_end_user?) }

        describe "and its role is changed to chat-only agent" do
          before do
            user.make_chat_agent!
          end

          it "sets the permission_set_id flag" do
            assert_equal account.chat_permission_set.id, user.permission_set_id
          end
        end
      end
    end
  end

  describe "for a non-enterprise account with zopim enabled" do
    let(:account) { accounts(:minimum) }

    let(:zopim_identity) do
      stub(
        zopim_agent_id: 1234,
        update_attribute: true,
        is_owner?: false,
        is_enabled?: true
      )
    end

    before do
      Account.any_instance.stubs(
        has_chat_permission_set?: true,
        has_permission_sets?: false
      )
      user.stubs(zopim_identity: zopim_identity)
      ::PermissionSet.create_chat_agent!(account)
    end

    describe "when the user is a chat-agent" do
      let(:user) { account.users.detect(&:is_agent?) }

      before do
        user.make_chat_agent!
        CIA.stubs(:current_actor).returns(user)
      end

      describe "and its role is changed" do
        it "clears the permission_set_id flag" do
          user.expects(:permission_set_id=).with(nil).twice
          user.role = 'admin'
          Zendesk::SupportUsers::User.new(user).save!
        end
      end
    end

    describe "when the user is an end-user" do
      let(:user) { account.users.detect(&:is_end_user?) }

      describe "and its role is changed to chat-only agent" do
        before do
          user.make_chat_agent!
        end

        it "sets the permission_set_id flag" do
          assert_equal account.chat_permission_set.id, user.permission_set_id
        end
      end
    end
  end

  describe "#enable_chat_identity!" do
    let(:account) { accounts(:minimum) }

    let(:user) { account.users.detect(&:is_agent?) }

    before do
      Account.any_instance.stubs(
        has_chat_permission_set?: true,
        has_permission_sets?: false
      )
      User.any_instance.stubs(is_chat_agent?: true)
    end

    describe "with ongoing zopim trial" do
      let(:zopim_subscription) do
        stub(
          is_trial?: true,
          is_serviceable?: false
        )
      end

      before do
        user.build_zopim_identity
        user.zopim_identity.stubs(zopim_subscription: zopim_subscription)
      end

      it "bypass the agent population validation" do
        refute user.zopim_identity.valid?
        refute_includes user.zopim_identity.errors.messages[:base], 'TooManyAgents'
      end
    end

    describe "with expired zopim trial" do
      let(:zopim_subscription) do
        stub(
          is_trial?: false,
          expired_trial?: true,
          is_serviceable?: false,
          max_agents: 1
        )
      end

      it "doesn't let you create additional agents" do
        assert_raises ActiveRecord::RecordInvalid do
          user.create_zopim_identity!
        end
      end
    end
  end
end
