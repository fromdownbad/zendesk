require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Suspension' do
  fixtures :all

  describe "An Owner" do
    before do
      @account = accounts(:minimum)
      @owner = @account.owner
      refute @owner.settings.suspended?
    end

    it "does not be able to be suspended" do
      @owner.settings.suspended = true
      @owner.save
      refute @owner.reload.settings.suspended?
    end
  end

  describe "#throttled_posting?" do
    let(:user) { users(:minimum_end_user) }

    before do
      @account = accounts(:minimum)
      @account.settings.moderation_throttle = 2
      @account.save!
    end

    it "does not be throttled by default" do
      refute user.throttled_posting?
    end

    describe "with suspended posts and suspended entry" do
      before do
        entry = entries(:technology)
        post = posts(:ponies_post_1)

        [post, entry].each do |obj|
          obj.send(:suspend)
          obj.save!
        end
      end

      it "is throttled" do
        assert user.throttled_posting?
      end
    end
  end

  describe "whitelisted_from_moderation?" do
    describe "on creation" do
      let(:user) do
        accounts(:minimum).users.create!(
          name: "Test User",
          email: "hello@example.com"
        )
      end

      it "is true" do
        assert(user.whitelisted_from_moderation?)
      end
    end

    describe "default" do
      let(:user) { users(:minimum_end_user) }

      it "is true" do
        assert user.whitelisted_from_moderation?
      end
    end

    describe "agent" do
      let(:user) { users(:minimum_agent) }

      before do
        user.stubs(settings: stub(whitelisted_from_moderation?: false))
      end

      it "is true" do
        assert user.whitelisted_from_moderation?
      end
    end
  end

  describe "on whitelisted_from_moderation=" do
    let(:user) { users(:minimum_end_user) }

    describe "true" do
      before do
        user.suspended = true
        user.whitelisted_from_moderation = true
      end

      it "resets the user suspension" do
        refute user.suspended?
        assert user.whitelisted_from_moderation?
      end
    end

    describe "false" do
      before do
        user.suspended = true
        user.whitelisted_from_moderation = false
      end

      it "does not touch suspended" do
        assert user.suspended?
        refute user.whitelisted_from_moderation?
      end
    end
  end

  describe "#suspend" do
    before do
      @user = users(:minimum_end_user)
      @user.save
      @user.reload

      refute @user.suspended?
    end

    it "suspends the user" do
      assert @user.suspend
      @user.reload
      assert_equal "true", @user.settings.suspended
    end
  end

  describe "#suspended=" do
    before do
      @user = users(:minimum_end_user)
      @user.save
      @user.reload

      refute @user.suspended?
      Timecop.freeze
    end

    describe "when user is not suspended" do
      it "updates the updated_at when suspending" do
        original_updated_at = @user.updated_at
        Timecop.travel(5.hours)

        @user.attributes = { suspended: true }
        @user.save!
        @user.reload

        assert original_updated_at < @user.updated_at
      end

      it "does not update the updated_at when unsuspending" do
        original_updated_at = @user.updated_at
        Timecop.travel(5.hours)

        @user.attributes = { suspended: false }
        @user.save

        assert_equal original_updated_at, @user.updated_at
      end
    end

    describe "when user is already suspended" do
      before do
        @user.settings.suspended = true
        @user.settings.save
      end

      it "does not update the updated_at when suspending" do
        original_updated_at = @user.updated_at
        Timecop.travel(5.hours)

        @user.attributes = { suspended: true }
        @user.save

        assert_equal original_updated_at, @user.updated_at
      end

      it "updates the updated_at when unsuspending" do
        original_updated_at = @user.updated_at
        Timecop.travel(5.hours)

        @user.attributes = { suspended: false }
        @user.save!
        @user.reload

        assert original_updated_at < @user.updated_at
      end
    end
  end

  it "suspends user calling #suspend!" do
    user = users(:minimum_end_user)
    refute user.suspended?
    user.suspend!
    assert user.suspended?
  end
end
