require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 14

describe 'Localization' do
  fixtures :all

  describe "#tranlation_locale" do
    let(:end_user) { users(:minimum_end_user) }
    before do
      @brazilian_portuguese = translation_locales(:brazilian_portuguese)
      @brazilian_portuguese.localized_agent = true
      @brazilian_portuguese.save!

      @spanish = translation_locales(:spanish)
      @agent = users(:minimum_agent)
    end

    describe "available languages for a user" do
      describe "when localized_for_agents => true" do
        before do
          @localized_for_agents = true
        end

        it "gets all available translations" do
          @agent.account.subscription.expects(:has_individual_language_selection?).never
          assert_equal 7, @agent.available_languages_for_subscription(@localized_for_agents).size
        end
      end

      describe "when localized_for_agents => false" do
        before do
          @localized_for_agents = false
        end

        it "is the ones in available_languages for an account" do
          @agent.account.subscription.expects(:has_individual_language_selection?).returns(true)
          @agent.account.expects(:available_languages).returns([ENGLISH_BY_ZENDESK])

          assert_equal [ENGLISH_BY_ZENDESK], @agent.available_languages_for_subscription(@localized_for_agents)
        end
      end

      describe "with default localized_for_agents value" do
        it "is the ones in available_languages for an account" do
          @agent.account.subscription.expects(:has_individual_language_selection?).returns(true)
          @agent.account.expects(:available_languages).returns([ENGLISH_BY_ZENDESK])

          assert_equal [ENGLISH_BY_ZENDESK], @agent.available_languages_for_subscription
        end
      end

      describe "when translation locale doesn't exists" do
        it "fallbacks to the account locale" do
          end_user.locale_id = 12345
          end_user.account.expects(translation_locale: @brazilian_portuguese)
          assert_equal @brazilian_portuguese, end_user.translation_locale
        end
      end

      it "is memoized" do
        @agent.locale_id = @brazilian_portuguese.id
        assert_sql_queries 1 do
          2.times do
            @agent.translation_locale
          end
        end
      end
    end

    describe "validate locale_id" do
      it "does not allow to save an invalid locale id" do
        @agent.locale_id = 123345
        refute @agent.save
        assert_equal "Locale: is invalid", @agent.errors.full_messages.first
      end

      it "allows nil for locale id" do
        @agent.locale_id = nil
        assert @agent.save
        assert_nil @agent.locale_id
      end
    end

    describe "#update_not_closed_tickets_locale" do
      let(:agent) { users(:minimum_agent) }

      it "updates the tickets if the locale_id was changed" do
        User.any_instance.expects(:update_not_closed_tickets_locale)

        agent.update_attributes(locale_id: @brazilian_portuguese.id)
      end

      it "does not update tickets if the locale_id did not change" do
        User.any_instance.expects(:update_not_closed_tickets_locale).never

        agent.update_attributes(name: "Jane Doe")
      end

      describe_with_arturo_enabled :describe_with_arturo_enabled do
        it "does not enqueue the LocaleBulkUpdateJob if the user has < 1k tickets" do
          LocaleBulkUpdateJob.expects(:enqueue).never

          agent.update_attributes(locale_id: @brazilian_portuguese.id)
        end

        it "enqueues the LocaleBulkUpdateJob if the user has > 1k tickets" do
          LocaleBulkUpdateJob.expects(:enqueue)
          agent.tickets.expects(:count).with(:all).returns(1500)

          agent.update_attributes(locale_id: @brazilian_portuguese.id)
        end

        it "updates the locale_id of all the requested tickets" do
          ticket_count = agent.tickets.count(:all)
          agent.tickets.update_all(locale_id: @spanish.id)

          agent.update_attributes(locale_id: @brazilian_portuguese.id)

          assert_equal 0, Ticket.where(requester_id: agent.id, locale_id: @spanish.id).count(:all)
          assert_equal ticket_count, Ticket.where(requester_id: agent.id, locale_id: @brazilian_portuguese.id).count(:all)
        end
      end

      describe_with_arturo_disabled :skip_locale_bulk_update_job do
        it "enqueues a bulk update job" do
          LocaleBulkUpdateJob.expects(:enqueue)

          ticket = agent.account.tickets.new(requester: agent, description: 'ticket 1')
          ticket.will_be_saved_by(agent)
          ticket.save!
          agent.update_attribute('locale_id', @brazilian_portuguese.id)
        end

        it "does not enqueue the LocaleBulkUpdateJob if the user has 0 tickets" do
          LocaleBulkUpdateJob.expects(:enqueue).never

          agent.tickets.destroy_all
          agent.update_attributes(locale_id: @brazilian_portuguese.id)
        end

        it "does not enqueue a bulk update job when the update rolls back" do
          LocaleBulkUpdateJob.expects(:enqueue).never
          @agent.locale_id = @brazilian_portuguese.id
          with_rollback(@agent) { @agent.save! }
        end
      end
    end

    describe "#ensure_proper_locale" do
      describe "when the role changes" do
        describe "to end user" do
          before do
            force_locale_of_user(@agent) # Set locale to portuguese
            @agent.roles = Role::END_USER.id # Downgrade user
            @agent.account.subscription.expects(:has_individual_language_selection?).returns(true)
            assert_equal @brazilian_portuguese.id, @agent.locale_id
          end

          it "clears locale is locale is not available for selection" do
            @agent.account.expects(:available_languages).returns([ENGLISH_BY_ZENDESK]).at_least_once
            assert @agent.save
            assert_nil @agent.locale_id
          end

          it "does not clear locale if locale if available for selection" do
            @agent.account.expects(:available_languages).returns([@brazilian_portuguese]).at_least_once
            assert @agent.save
            assert_equal @brazilian_portuguese.id, @agent.locale_id
          end
        end

        describe "to agent" do
          before do
            force_locale_of_user(end_user) # Set locale to portuguese
            end_user.roles = Role::AGENT.id # Upgrade user
            assert_equal @brazilian_portuguese.id, end_user.locale_id
          end

          let(:spanish) { translation_locales(:spanish) }
          let(:japanese) { translation_locales(:japanese) }

          it "is set to english if locale is not available for selection and account's default it not an agent language" do
            TranslationLocale.expects(:available_for_agents).with(end_user.account).returns([spanish, ENGLISH_BY_ZENDESK]).at_least_once

            assert end_user.save
            assert_equal ENGLISH_BY_ZENDESK.id, end_user.locale_id
          end

          it "is set to the account default if it an agent language" do
            TranslationLocale.expects(:available_for_agents).with(end_user.account).returns([japanese, ENGLISH_BY_ZENDESK]).at_least_once
            end_user.account.expects(:translation_locale).returns(japanese).at_least_once

            assert end_user.save
            assert_equal japanese.id, end_user.locale_id
          end

          it "does not be set if locale is an agent language" do
            TranslationLocale.expects(:available_for_agents).with(end_user.account).returns([@brazilian_portuguese, ENGLISH_BY_ZENDESK]).at_least_once

            assert end_user.save
            assert_equal @brazilian_portuguese.id, end_user.locale_id
          end
        end

        describe "to admin" do
          before do
            force_locale_of_user(end_user) # Set locale to portuguese
            CIA.stubs(:current_actor).returns(end_user)
            end_user.roles = Role::ADMIN.id # Upgrade user
            assert_equal @brazilian_portuguese.id, end_user.locale_id
          end

          let(:spanish) { translation_locales(:spanish) }
          let(:japanese) { translation_locales(:japanese) }

          it "is set to english if locale is not available for selection and account's default it not an agent language" do
            TranslationLocale.expects(:available_for_agents).with(end_user.account).returns([spanish, ENGLISH_BY_ZENDESK]).at_least_once

            assert end_user.save
            assert_equal ENGLISH_BY_ZENDESK.id, end_user.locale_id
          end

          it "is set to the account default if it an agent language" do
            TranslationLocale.expects(:available_for_agents).with(end_user.account).returns([japanese, ENGLISH_BY_ZENDESK]).at_least_once
            end_user.account.expects(:translation_locale).returns(japanese).at_least_once

            assert end_user.save
            assert_equal japanese.id, end_user.locale_id
          end

          it "does not be set if locale is an agent language" do
            TranslationLocale.expects(:available_for_agents).with(end_user.account).returns([@brazilian_portuguese, ENGLISH_BY_ZENDESK]).at_least_once

            assert end_user.save
            assert_equal @brazilian_portuguese.id, end_user.locale_id
          end
        end
      end
    end

    describe "#locale_changed_to_an_unavailable_language" do
      it "clears locale if it is an end user in a starter account" do
        force_locale_of_user(end_user)

        assert_equal @brazilian_portuguese.id, end_user.locale_id
        Subscription.any_instance.stubs(:plan_type).returns(SubscriptionPlanType.Small)
        end_user.name = "Change for test"
        end_user.save

        assert_equal "Change for test", end_user.name
        assert_nil end_user.locale_id
      end

      it "does not clear the locale if it is an agent in a starter account" do
        force_locale_of_user(@agent)

        assert_equal @brazilian_portuguese, @agent.translation_locale
        Subscription.any_instance.stubs(:plan_type).returns(SubscriptionPlanType.Small)

        @agent.name = "Change for test"
        @agent.save

        assert_equal "Change for test", @agent.name
        assert_equal @brazilian_portuguese, @agent.translation_locale
      end

      it "clears the locale if it is an end user with an unavailable locale" do
        force_locale_of_user(end_user)

        assert_equal @brazilian_portuguese, end_user.translation_locale
        end_user.stubs(:locale_unavailable?).returns(true)
        end_user.name = "Change for test"
        end_user.save

        assert_equal "Change for test", end_user.name
        assert_nil end_user.locale_id
      end

      it "does not clear the locale if it is an end user with an avaible locale" do
        force_locale_of_user(end_user)

        assert_equal @brazilian_portuguese, end_user.translation_locale
        end_user.account.subscription.stubs(:has_individual_language_selection?).returns(true)
        end_user.stubs(:locale_unavailable?).returns(false)
        end_user.name = "Change for test"
        end_user.save

        assert_equal "Change for test", end_user.name
        assert_equal @brazilian_portuguese, end_user.translation_locale
      end

      it "does not clear the locale if it is an agent" do
        force_locale_of_user(@agent)

        assert_equal @brazilian_portuguese, @agent.translation_locale

        @agent.name = "Change for test"
        @agent.save

        assert_equal "Change for test", @agent.name
        assert_equal @brazilian_portuguese, @agent.translation_locale
      end
    end
  end

  describe "#start_of_week" do
    let(:end_user) { users(:minimum_end_user) }

    before do
      Timecop.freeze('2015-01-01')
      @account_start = end_user.account.start_of_week.utc
      assert_equal Time.parse("2014-12-28 23:00:00 UTC"), @account_start
    end

    describe "for a user with no explicit time_zone" do
      before do
        end_user.update_column(:time_zone, nil)
      end

      it "defaults to account's time_zone" do
        assert_equal @account_start, end_user.start_of_week
      end
    end

    describe "for a user with a time_zone different from account" do
      before do
        end_user.expects(:time_zone).returns('Pacific Time (US & Canada)')
      end

      it "uses user's time_zone" do
        @user_start = end_user.start_of_week
        refute_equal @account_start, @user_start
        assert_equal Time.parse("2014-12-29 08:00:00 UTC"), @user_start
      end
    end
  end

  def force_locale_of_user(user)
    assert(user.update_columns(locale_id: @brazilian_portuguese.id))
  end
end
