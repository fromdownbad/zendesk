require_relative "../../support/test_helper"

SingleCov.covered!

describe 'CustomerLists' do
  fixtures :accounts

  describe User do
    let(:account) { accounts(:minimum) }

    describe "sample" do
      describe "when creating a new user" do
        before do
          @user = FactoryBot.create(:user)
        end

        it "sets sample value" do
          assert @user.sample.present?, "should have sample value"
          assert_includes (1..UserView::SAMPLING_RANGE), @user.sample
        end
      end

      describe "when updating a user" do
        before do
          @user = users(:minimum_agent)
          refute @user.sample.present?
        end

        it "sets sample value" do
          @user.name = '1'
          @user.save!
          assert @user.sample.present?, "should have sample value"
          assert_includes (1..UserView::SAMPLING_RANGE), @user.sample
        end
      end

      describe "when user has sample value" do
        before do
          @user = users(:minimum_agent)
          refute @user.sample.present?
          @user.name = '1'
          @user.save!
          @sample = @user.sample
          assert @sample.present?, "should have sample value"
        end

        it "does not set new sample value" do
          @user.name = '2'
          @user.save!
          assert_equal @sample, @user.sample
        end
      end
    end

    describe "onboarding state" do
      describe "when creating user" do
        before do
          @user = FactoryBot.create(:user)
        end

        it "sets the onboarding state to tour" do
          assert_equal UserView::OnboardingStates::TOUR, @user.settings.customer_list_onboarding_state
        end
      end

      describe "when updating user" do
        before do
          @user = users(:minimum_agent)
          @user.settings.customer_list_onboarding_state = UserView::OnboardingStates::OFF
          @user.save!
          @user.reload
        end

        it "does not change the onboarding state" do
          @user.name = '1'
          @user.save!
          assert_equal UserView::OnboardingStates::OFF, @user.settings.customer_list_onboarding_state
        end
      end
    end
  end
end
