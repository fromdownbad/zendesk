require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'Users::TicketSharingSupport' do
  it 'ticket sharing properties should play nice with password properties' do
    @user = FactoryBot.create(:end_user)
    @user.foreign_agent = true
    assert @user.foreign_agent?

    assert @user.save!, @user.errors.to_xml
    @user.reload
    assert @user.foreign_agent?
  end

  it '#for_partner should use display names if available' do
    @user = FactoryBot.create(:agent)

    @user.agent_display_name = 'Hobo #23'

    @user.account.stubs(:has_agent_display_names?).returns(true)

    assert_equal 'Hobo #23', @user.for_partner.name

    @user.agent_display_name = nil
    assert_equal @user.name, @user.for_partner.name
  end
end
