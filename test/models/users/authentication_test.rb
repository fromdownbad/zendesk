require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 8

describe 'UserAuthentication' do
  include DomainEventsHelper
  fixtures :all
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }
  before { User.any_instance.stubs(domain_event_publisher: domain_event_publisher) }

  class TestSecurityPolicy
    include Zendesk::SecurityPolicy::Enforcement

    class_attribute :id
    self.id = 99

    attr_accessor :password

    validations[:unencrypted_password] << :short_password_length

    def policy_level
      id
    end

    def short_password_length
      Zendesk::SecurityPolicy::Validation::Length.new(minimum: 4)
    end

    def unencrypted_password
      password.unencrypted
    end
  end

  describe "#password_expirable?" do
    let(:settings) { {} }

    before do
      assert user.is_verified?
      assert user.crypted_password.present?

      user.account.role_settings.update_attributes!({
        agent_zendesk_login: false,
        agent_google_login: false,
        agent_twitter_login: false,
        agent_facebook_login: false,
        agent_remote_login: false,
        end_user_zendesk_login: false,
        end_user_google_login: false,
        end_user_twitter_login: false,
        end_user_facebook_login: false,
        end_user_remote_login: false,
        agent_password_allowed: false,
        end_user_password_allowed: false
      }.merge(settings))
    end

    describe "agent" do
      let(:user) { users(:minimum_agent) }

      describe "remote auth" do
        let(:settings) do
          {
            agent_remote_login: true
          }
        end

        it "does not be expirable" do
          refute user.password_expirable?
        end
      end

      describe "zendesk" do
        let(:settings) do
          {
            agent_zendesk_login: true
          }
        end

        it "is expirable" do
          assert user.password_expirable?
        end
      end

      describe "google" do
        let(:settings) do
          {
            agent_google_login: true
          }
        end

        before do
          user.stubs(can_authenticate_without_password?: true)
        end

        it "does not be expirable" do
          refute user.password_expirable?
        end
      end
    end

    describe "end user" do
      let(:user) { users(:minimum_end_user) }

      describe "remote auth" do
        let(:settings) do
          {
            end_user_remote_login: true
          }
        end

        it "does not be expirable" do
          refute user.password_expirable?
        end
      end

      describe "zendesk + google" do
        let(:settings) do
          {
            end_user_zendesk_login: true,
            end_user_google_login: true
          }
        end

        it "is expirable" do
          assert user.password_expirable?
        end
      end
    end
  end

  describe "Validation" do
    before do
      @user   = users(:minimum_agent)
      @policy = TestSecurityPolicy.new
      @user.stubs(:security_policy).returns(@policy)
    end

    it "validates blank passwords" do
      @user.password = ''
      @user.valid?

      assert @user.errors[:password].any?
    end

    it "adds errors on their password if it doesn't meet security policy requirements" do
      @user.password = '123'
      @user.valid?

      assert @user.errors[:password].any?
    end

    it "does not add errors on their password if it meets security policy requirements" do
      @user.password = '1234'
      @user.valid?

      assert_equal [], @user.errors[:password]
    end
  end

  describe "#find_or_create_challenge_token" do
    let(:user) { users(:minimum_agent) }
    let(:shared_session) { stub(session_key: '2:46:e54266850598213599620037522a6ca97c32fbd1b7bba2910688253a2d01afb1') }

    it "creates a token from the session if has multibrand_sso enabled" do
      token = user.find_or_create_challenge_token('127.0.0.1', shared_session)

      assert_equal shared_session.session_key, token.shared_session_key
    end

    it "creates a token with no session if there is no shared session" do
      token = user.find_or_create_challenge_token('127.0.0.1')

      assert_nil token.shared_session_key
    end
  end

  describe "#change_password!" do
    before do
      @user   = users(:minimum_agent)
      @policy = TestSecurityPolicy.new
      @user.stubs(:security_policy).returns(@policy)
      @user.stubs(:allowed_to_login_with_password?).returns(true)
    end

    it "changes the user's password" do
      @user.change_password!("wookiee")
      assert @user.authenticated?("wookiee")
    end

    it "raises Users::PasswordInvalid if the password is empty" do
      assert_raise Users::PasswordInvalid do
        @user.change_password!('')
      end
    end

    it "raises Users::InvalidState if the user object is dirty" do
      @user.name = "Hansi Hinterseer"

      assert_raise Users::InvalidState do
        @user.change_password!('wookiee')
      end
    end

    it "is able to clear password and expiry when skip password validation is enabled" do
      @user.stubs(:has_twitter_identity?).returns(true)
      assert(@user.password_expirable?)

      @user.skip_password_validation = true
      @user.change_password!('')
      @user.reload

      assert_equal false, @user.password_expirable?
    end

    it "does not store blank passwords with encryption" do
      assert @user.crypted_password

      @user.skip_password_validation = true
      @user.change_password!('')
      @user.reload

      assert_nil @user.crypted_password
      assert_nil @user.salt
    end

    it "stores the security policy used for the password" do
      assert @policy.id
      assert_nil @user.settings.password_security_policy_id
      @user.change_password!('1234')
      @user.reload

      assert_equal @policy.id, @user.settings.password_security_policy_id.to_i
    end

    it "replaces the older security policy used for the previous password" do
      assert @policy.id
      @user.change_password!('1234')
      @user.reload
      assert_equal @policy.id, @user.settings.password_security_policy_id.to_i

      new_policy_id = @policy.id + 1
      @policy.stubs(:id).returns(new_policy_id)
      @user.change_password!('12345')
      @user.reload
      assert_equal new_policy_id, @user.settings.password_security_policy_id.to_i
    end

    it "doesn't encrypt the password again after a password change" do
      old_salt = @user.salt
      old_crypted_password = @user.crypted_password

      @user.change_password!("foobar")

      refute_equal old_salt, @user.salt
      refute_equal old_crypted_password.to_s, @user.crypted_password.to_s

      salt = @user.salt
      crypted_password = @user.crypted_password
      @user.save!

      assert_equal salt, @user.salt
      assert_equal crypted_password.to_s, @user.crypted_password.to_s
    end
  end

  describe "authenticating" do
    describe "when a user has a nil password" do
      before do
        @user = users(:minimum_agent)
        @user.update_attribute(:crypted_password, nil)
      end

      it "does not succeed when passed a nil password parameter" do
        refute @user.authenticated?(nil)
      end
    end

    describe "against a password encrypted with BCrypt" do
      before do
        @user = users(:minimum_agent)
        @user.change_password!('123456')
        @user.reload

        assert_equal Users::Password::BCrypt, @user.crypted_password.class
      end

      it "succeeds when the password matches its encrypted version" do
        assert(@user.authenticated?('123456'))
        assert_equal false, @user.authenticated?('1234567')
      end

      it "does not accept the crypted password" do
        assert_equal false, @user.authenticated?(@user.salt + @user.read_attribute(:crypted_password))
      end

      it "does not update the password" do
        User.expects(:update_all).never
        assert(@user.authenticated?('123456'))
      end
    end

    describe "against a password encrypted with SHA1" do
      before do
        @user = users(:minimum_agent)
        @user.update_attribute(:salt, nil)
        @user.update_attribute(:crypted_password, '9d82ff01a1c4f46027387130cd970f4480e108fe')
        @user.reload

        assert_equal Users::Password::SHA1, @user.crypted_password.class
      end

      it "succeeds when the password matches its encrypted version" do
        assert(@user.authenticated?('123456'))
        assert_equal false, @user.authenticated?('1234567')
      end

      it "updates the password to BCrypt on success" do
        assert(@user.authenticated?('123456'))
        @user.reload
        assert_equal Users::Password::BCrypt, @user.crypted_password.class
        assert(@user.authenticated?('123456'))
      end

      it "does not update the password on failure" do
        assert_equal false, @user.authenticated?('1234567')
        @user.reload
        assert_equal Users::Password::SHA1, @user.crypted_password.class
        assert(@user.authenticated?('123456'))
      end
    end
  end

  describe "#password_required?" do
    before do
      @user = accounts(:minimum).users.new(email: 'mick@zendesk.com')
      @user.stubs(:is_verified?).returns(true)
    end

    it "returns true when a password is present" do
      @user.password = 'hello'
      assert(@user.send(:password_required?))
    end

    it "returns false when no password is present" do
      @user.password = nil
      assert_equal false, @user.send(:password_required?)
    end

    it "returns false when a password is present and skip validation is specified" do
      @user.password = 'hello'
      @user.skip_password_validation = true

      assert_equal false, @user.send(:password_required?)
    end
  end

  describe "password expires at" do
    before do
      @user = users(:minimum_agent)
      @user.stubs(:security_policy).returns(stub(password_expires_at: 5.days.from_now))
      @user.stubs(:allowed_to_login_with_password?).returns(true)
      assert @user.password_expires_at
    end

    it "does not be present when unverified" do
      @user.stubs(:is_verified?).returns(false)
      assert_nil @user.password_expires_at
    end

    it "nevers apply when not allowed to login with password" do
      @user.stubs(:uses_password_for_authentication?).returns(false)
      assert_nil @user.password_expires_at
    end

    describe "for users who authenticate without passwords" do
      before do
        @user.stubs(:can_authenticate_without_password?).returns(true)
      end

      it "is present when there is a password" do
        assert @user.crypted_password
        assert @user.password_expires_at
      end

      it "does not be present when there is no password" do
        @user.password = ''
        @user.skip_password_validation = true
        @user.valid?

        assert_nil @user.password_expires_at
      end
    end

    describe "for users who only authenticate with passwords" do
      before do
        @user.stubs(:can_authenticate_without_password?).returns(false)
      end

      it "is present even when there is no password" do
        @user.crypted_password = nil
        assert @user.password_expires_at
      end
    end
  end

  describe "randomize password" do
    before do
      @user = users(:minimum_agent)
    end

    it "sets the password to a randomly generated string" do
      assert((@user.crypted_password == '123456'))
      @user.randomize_password
      @user.save!

      assert_equal false, (@user.crypted_password == '123456')
    end

    it "skips password validation" do
      @user.randomize_password
      assert(@user.skip_password_validation)
      @user.expects(:password_security_policy_enforced).never

      @user.save!
    end
  end

  describe "#update_last_login" do
    before do
      @user = users(:minimum_agent)
      Timecop.freeze
    end

    describe "when user is a system user" do
      it "returns nil" do
        assert_nil User.system.update_last_login
      end

      it "does not emit the last_login_changed event" do
        assert_equal decode_user_events(domain_event_publisher.events, :last_login_changed).size, 0
      end
    end

    describe "when user is suspended" do
      before { @user.stubs(suspended?: true) }

      it "returns nil" do
        assert_nil @user.update_last_login
      end

      it "does not emit the last_login_changed event" do
        assert_equal decode_user_events(domain_event_publisher.events, :last_login_changed).size, 0
      end
    end

    describe "for a normal user" do
      describe "when current last_login is more than 20 minutes ago" do
        before { @user.last_login = 30.minutes.ago }

        it "updates last_login" do
          @user.update_last_login
          @user.reload
          assert_equal Time.now.utc.to_i, @user.last_login.to_i
        end

        it "updates updated_at" do
          old_updated_at = @user.updated_at
          Timecop.freeze(1.minute.from_now)
          @user.update_last_login
          @user.reload

          assert_equal Time.now.to_i, @user.updated_at.to_i
          assert old_updated_at < @user.updated_at
        end

        it "emits a last_login_changed event" do
          @user.update_last_login
          assert_equal decode_user_events(domain_event_publisher.events, :last_login_changed).size, 1
        end
      end

      describe "when current last_login is less than 20 minutes ago" do
        before { @user.last_login = 10.minutes.ago }

        it "does not update last_login" do
          @user.update_last_login
          @user.reload
          assert_not_equal Time.now.utc.to_i, @user.last_login.to_i
        end

        it "does not emit the last_login_changed event" do
          @user.update_last_login
          assert_equal decode_user_events(domain_event_publisher.events, :last_login_changed).size, 0
        end
      end

      describe "when current last_login is nil" do
        before { @user.last_login = nil }

        it "updates last_login" do
          @user.update_last_login
          @user.reload
          assert_equal Time.now.to_i, @user.last_login.to_i
        end

        it "updates updated_at" do
          old_updated_at = @user.updated_at
          Timecop.freeze(1.minute.from_now)
          @user.update_last_login
          @user.reload

          assert_equal Time.now.to_i, @user.updated_at.to_i
          assert old_updated_at < @user.updated_at
        end

        it "emits a last_login_changed event" do
          @user.update_last_login
          assert_equal decode_user_events(domain_event_publisher.events, :last_login_changed).size, 1
        end
      end
    end
  end
end
