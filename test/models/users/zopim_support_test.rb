require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Users::ZopimSupport' do
  fixtures :accounts, :users

  describe "a user" do
    let(:user) { users(:minimum_end_user) }

    it "should have a zopim_identity association" do
      assert user.respond_to? :zopim_identity
    end

    it "should have a zopim_agent_id attribute" do
      assert user.respond_to? :zopim_agent_id
    end

    it "should have a zopim_email attribute" do
      assert user.respond_to? :zopim_agent_id
    end

    describe "when a zopim_identity is present" do
      let(:zopim_identity) do
        stub(
          zopim_agent_id:   1234,
          update_attribute: true
        )
      end

      before do
        user.stubs(zopim_identity: zopim_identity)
      end

      describe "zopim owner" do
        let(:user) { users(:minimum_admin_not_owner) }

        before do
          zopim_identity.stubs(:is_owner?).returns(true)
        end

        describe 'setting its role to an agent' do
          before { user.role_or_permission_set = 'agent' }

          describe 'on phase 2' do
            it 'should not allow the change' do
              err = -> { user.save! }.must_raise(ActiveRecord::RecordInvalid)
              err.message.must_match /You cannot remove the admin role from a Zendesk Chat owner./i
              assert user.reload.is_admin?
            end
          end

          describe 'on phase 3' do
            before do
              user.account.stubs(:zopim_integration).returns(
                stub(phase_three?: true)
              )
            end

            describe 'when the zopim_identity is enabled' do
              before do
                zopim_identity.stubs(:is_enabled?).returns(true)
              end

              it 'allows the change' do
                user.save!
                refute user.reload.is_admin?
              end
            end

            describe 'when the zopim_identity is not enabled' do
              before do
                zopim_identity.stubs(:is_enabled?).returns(false)
              end

              it 'allows the change' do
                user.save!
                refute user.reload.is_admin?
              end
            end
          end
        end
      end

      describe "and it is not the zopim owner" do
        before do
          zopim_identity.stubs(:is_owner?).returns(false)
        end

        describe "when it is an agent" do
          let(:user) { users(:minimum_agent) }

          describe "setting its role to an administrator" do
            before do
              CIA.stubs(:current_actor).returns(user)
              user.role_or_permission_set = 'admin'
            end

            it "should update the zopim_identity's is_administrator flag" do
              zopim_identity.expects(:update_attribute).
                with(:is_administrator, true).once
              user.role_or_permission_set = 'admin'
              user.save!
            end

            it "should become an administrator" do
              user.save!
              assert user.is_admin?
            end
          end
        end

        describe "when it is an administrator" do
          let(:user) { users(:minimum_admin_not_owner) }

          describe "setting its role to an agent" do
            before { user.role_or_permission_set = 'agent' }

            it "should update the zopim_identity's is_administrator flag" do
              zopim_identity.expects(:update_attribute).
                with(:is_administrator, false).once
              user.save!
            end

            it "should no longer be an administrator" do
              user.save!
              refute user.is_admin?
            end
          end
        end
      end
    end
  end

  describe "#zopim_email" do
    let(:user) { users(:minimum_agent) }

    it "should delegate to #zopim_email_format_for_value" do
      user.expects(:zopim_email_format_for_value).
        with('minimum_agent@aghassipour.com').
        returns(anything).once
      assert user.zopim_email
    end

    it "returns a zopim email formatted value based on its primary email" do
      expected = "minimum_agent+#{user.id}@aghassipour.com"
      actual   = user.zopim_email
      assert_equal expected, actual
    end
  end

  describe "#zopim_email_format_for_value" do
    let(:user) { users(:minimum_agent) }

    it "returns a zopim email formatted value based on the argument" do
      expected = "foo+#{user.id}@example.com"
      actual   = user.zopim_email_format_for_value('foo@example.com')
      assert_equal expected, actual
    end

    describe "when Rails.env.development? is true" do
      before { Rails.env.stubs(development?: true) }

      it "should include the hostname of the dev machine" do
        expected = /^foo\+#{user.id}\-[a-zA-Z]+@example\.com$/
        actual   = user.zopim_email_format_for_value('foo@example.com')
        assert expected.match(actual)
      end
    end
  end
end
