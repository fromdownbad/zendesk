require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 7

describe Signature do
  fixtures :accounts, :users, :tickets

  describe "rich signatures" do
    subject { Signature.create(user: users(:minimum_end_user), html_value: "<h2>Thanks</h2>") }

    it "has is_rich true" do
      subject.is_rich.must_equal true
    end

    it "stores the rich value in the database" do
      subject.read_attribute(:value).must_equal "<h2>Thanks</h2>"
    end
  end

  describe "plain signatures" do
    subject { Signature.create(user: users(:minimum_end_user), value: "## Thanks") }

    it "has is_rich false" do
      subject.is_rich.must_equal false
    end

    it "stores the plain value in the database" do
      subject.read_attribute(:value).must_equal "## Thanks"
    end
  end

  describe "#save" do
    before do
      @signature = Signature.create(user: users(:minimum_end_user))
    end

    it "shoulds automatically add account from user" do
      assert @signature.save
      assert_equal tickets(:minimum_1).account, @signature.account
    end
  end

  describe "value length validation" do
    let(:signature) { Signature.create(user: users(:minimum_end_user)) }

    it "validates length of signature value" do
      signature.value = "a" * 65_536
      refute signature.valid?
    end
  end
end
