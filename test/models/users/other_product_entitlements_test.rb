require_relative '../../support/test_helper'

SingleCov.covered!

describe 'Users::SuiteAgentSupport' do
  fixtures :users

  let(:user) { users(:minimum_agent) }

  describe '#other_products_agent?' do
    describe 'when user settings for other product entitlements are missing' do
      it 'default to false' do
        refute user.other_products_agent?
      end
    end

    describe 'when user is connect agent' do
      it 'returns true' do
        user.settings.connect_entitlement = 'agent'
        assert user.other_products_agent?
      end
    end

    describe 'when user is chat agent' do
      it 'returns true' do
        user.settings.chat_entitlement = 'agent'
        assert user.other_products_agent?
      end
    end

    describe 'when user is chat admin' do
      it 'returns true' do
        user.settings.chat_entitlement = 'admin'
        assert user.other_products_agent?
      end
    end
  end

  describe '#connect_agent?' do
    describe 'when user settings for connect entitlement is missing' do
      it 'default to false' do
        refute user.connect_agent?
      end
    end

    describe 'when user settings for connect entitlement is empty string' do
      it 'returns false' do
        user.settings.connect_entitlement = ''
        refute user.connect_agent?
      end
    end

    describe 'when user settings for chat entitlement is agent' do
      it 'returns true' do
        user.settings.connect_entitlement = 'agent'
        assert user.connect_agent?
      end
    end
  end

  describe '#cp4_chat_agent?' do
    describe 'when user settings for chat entitlement is missing' do
      it 'default to false' do
        refute user.cp4_chat_agent?
      end
    end

    describe 'when user settings for chat entitlement is empty string' do
      it 'returns false' do
        user.settings.chat_entitlement = ''
        refute user.cp4_chat_agent?
      end
    end

    describe 'when user settings for chat entitlement is admin' do
      it 'returns true' do
        user.settings.chat_entitlement = 'admin'
        assert user.cp4_chat_agent?
      end
    end

    describe 'when user settings for chat entitlement is agent' do
      it 'returns true' do
        user.settings.chat_entitlement = 'agent'
        assert user.cp4_chat_agent?
      end
    end
  end

  describe '#update_entitlement' do
    let(:product) { :chat }
    subject { user.update_entitlement(product: product, new_entitlement: new_entitlement) }

    describe 'when product has not entitlement property defined' do
      let(:product) { 'acme_app' }
      let(:new_entitlement) { 'admin' }

      it 'does not set entitlement' do
        user.settings.expects(:set).never
        subject
      end
    end

    describe 'when user does not have entitlement and new entitlement is admin' do
      let(:new_entitlement) { 'admin' }
      it 'updates cached product entitlement in user settings' do
        subject
        assert_equal new_entitlement, user.settings.send("#{product}_entitlement")
        assert user.cp4_chat_agent?
      end
    end

    describe 'when user is chat admin and new entitlement is empty' do
      before do
        user.settings.chat_entitlement = 'admin'
        user.save
      end

      let(:new_entitlement) { '' }

      it 'updates cached product entitlement in user settings' do
        subject
        assert_equal new_entitlement, user.settings.send("#{product}_entitlement")
        refute user.cp4_chat_agent?
      end
    end
  end
end
