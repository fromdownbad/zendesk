require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Users::AnswerBot' do
  let(:user) { FactoryBot.create(:end_user) }

  describe '#answer_bot' do
    it 'sets the answer bot user setting' do
      user.answer_bot = true
      user.save!

      assert user.answer_bot?
    end
  end

  describe '#answer_bot?' do
    it 'returns the answer bot user setting' do
      refute user.answer_bot?
    end
  end
end
