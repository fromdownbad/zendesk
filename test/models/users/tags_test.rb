require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 5

describe 'UserTags' do
  fixtures :users
  describe Users::Tags do
    let(:user) { users(:minimum_end_user) }

    before do
      Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
    end

    it "has existing tags" do
      assert_equal "beta.test superuser", user.current_tags
    end

    it "is able to set tags" do
      user.update_attribute(:current_tags, "hello there")
      assert_equal ["hello", "there"], user.tag_array
    end

    it "is able to add tags" do
      user.update_attribute(:additional_tags, "ib")
      assert_equal ["beta.test", "superuser", "ib"].sort, user.tag_array.sort
    end

    it "is able to remove tags" do
      user.update_attribute(:remove_tags, "beta.test")
      assert_equal ["superuser"], user.tag_array
    end

    it "adds to the permissions cache" do
      tags = user.permissions(false).match("tags-([^/$]+)")[1]
      assert_same_elements(
        tags.split("-"),
        ["superuser", "beta.test", "premium"]
      )
    end
  end
end
