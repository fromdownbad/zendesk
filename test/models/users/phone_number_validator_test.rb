require_relative "../../support/test_helper"

SingleCov.covered!

class FakeUser
  include ActiveModel::Validations
  attr_accessor :phone, :skip_phone_number_validation
  validates :phone, 'users/phone_number': true
end

class FakeUserPhoneNumberIdentity
  include ActiveModel::Validations
  attr_accessor :value, :skip_phone_number_validation
  validates :value, 'users/phone_number': true
end

describe Users::PhoneNumberValidator do
  fixtures :accounts
  let (:account) { accounts(:minimum) }
  let (:user) { FakeUser.new }
  let (:user_phone_identity) { FakeUserPhoneNumberIdentity.new }

  before do
    FakeUser.any_instance.stubs(:account).returns(account)
    FakeUserPhoneNumberIdentity.any_instance.stubs(:account).returns(account)
  end

  it 'is valid when phone number is NIl' do
    user.phone = nil
    assert user.valid?
  end

  it 'is valid when phone number is EMPTY' do
    user.phone = ''
    assert user.valid?
  end

  it 'is invalid for emergency numbers' do
    ["911", "999", "112"].each do |number|
      user.phone = number
      refute user.valid?
    end
  end

  it 'is valid for +00' do
    user.phone = '+00'
    assert user.valid?
  end

  describe "when account setting is ON" do
    before do
      Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
    end

    it "should validate User phone number with E164 validation" do
      user.phone = '+123'
      refute user.valid?

      user.phone = '+14151234567'
      assert user.valid?
    end

    it "should validation Phone Number Identity with regex" do
      user_phone_identity.value = '+123'
      refute user_phone_identity.valid?

      user_phone_identity.value = '+14151234567'
      assert user_phone_identity.valid?
    end
  end

  describe "when account setting is OFF" do
    before do
      Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(false)
      @new_identity = UserPhoneNumberIdentity.new(account: account)
      @new_identity.stubs(:set_lowest_priority).returns(1)
    end

    it "should NOT validate User phone number" do
      user.phone = '+1'
      assert user.valid?
    end

    describe "when arturo apply_prefix_and_normalize_phone_numbers is OFF" do
      before do
        Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(false)
      end

      it "should not apply any validation of number" do
        @new_identity.value = '08612312'
        refute @new_identity.valid?
      end
    end

    describe "when arturo user_identity_limit is enabled" do
      before do
        @new_identity.value = '08612312'
        Arturo.enable_feature!(:user_identity_limit)
      end

      it "should run validation" do
        refute @new_identity.valid?
      end

      it "should run validation without a user" do
        UserIdentity.any_instance.expects(:enforce_user_identity_limit).with(nil)
        refute @new_identity.valid?
      end
    end

    describe "when arturo apply_prefix_and_normalize_phone_numbers is ON" do
      before do
        Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true)
      end

      it "should validation Phone Number Identity with regex" do
        @new_identity.value = '+123'
        refute @new_identity.valid?
      end
    end
  end
end
