require_relative "../../../support/test_helper.rb"
require_relative "../../../support/fake_esc_kafka_message"

SingleCov.covered!

describe UserIdentityEventObserver do
  include DomainEventsHelper
  fixtures :user_identities, :users, :accounts

  let(:account) { accounts(:minimum) }
  let(:user) { accounts(:minimum).users.new(name: 'Argonauts') }
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  describe "when user identity is destroyed" do
    let(:user_identity) { user_identities(:minimum_end_user) }

    it "publish user_identity_removed event" do
      user_identity.user.domain_event_publisher = domain_event_publisher
      user_identity.destroy!

      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 1
    end
  end

  describe "when user identity is created" do
    it "publish user_identity_created event" do
      user.domain_event_publisher = domain_event_publisher
      user.identities << UserIdentity.create!(account: account, user: user, value: 'test@test.com')

      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 1
      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
    end
  end

  describe "when user identity value is changed" do
    it "publish user_identity_changed event" do
      user_identity = UserIdentity.create!(account: account, user: user, value: 'test2@test.com')
      user_identity.user.domain_event_publisher = domain_event_publisher
      user_identity.value = 'test3@test.com'
      user_identity.save!

      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 1
      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
    end
  end

  describe "when user identity value is not changed" do
    it "does not publish user_identity_changed event" do
      user_identity = UserIdentity.create!(account: account, user: user, value: 'test3@test.com')
      user_identity.user.domain_event_publisher = domain_event_publisher
      user_identity.make_primary!

      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
      assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
    end
  end
end
