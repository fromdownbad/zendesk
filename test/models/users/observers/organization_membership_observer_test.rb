require_relative '../../../support/test_helper.rb'

SingleCov.covered!

describe OrganizationMembershipObserver do
  fixtures :users, :organizations, :organization_memberships
  include DomainEventsHelper
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }
  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_end_user) }
  let(:organization) { organizations(:minimum_organization2) }
  let(:organization1) { organizations(:minimum_organization3) }

  before do
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  describe 'after_create' do
    it 'should add an event UserOrganizationAdded to user' do
      user.organization_memberships.create!(organization: organization, account: account, default: true)
      assert_equal decode_user_events(domain_event_publisher.events, :user_organization_added).size, 1
    end
  end

  describe 'after_destroy' do
    let(:membership) { organization_memberships(:minimum) }
    it 'should add an event UserOrganizationRemoved to user' do
      membership.destroy!
      assert_equal decode_user_events(domain_event_publisher.events, :user_organization_removed).size, 1
    end
  end

  describe 'change organizations' do
    it 'should add both UserOrganizationAdded and UserOrganizationRemoved to user' do
      user.organizations = []
      user.save!
      user.organization_memberships.create!(organization: organization1, account: account, default: true)
      assert_equal decode_user_events(domain_event_publisher.events, :user_organization_added).size, 2
      assert_equal decode_user_events(domain_event_publisher.events, :user_organization_removed).size, 1
    end
  end
end
