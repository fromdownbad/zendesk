require_relative '../../../support/test_helper'

SingleCov.covered!

describe UserEventBusObserver do
  fixtures :users

  let(:user) do
    users(:minimum_end_user).dup.tap do |u|
      u.domain_event_publisher = domain_event_publisher
    end
  end

  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  describe 'on_save' do
    describe 'with events to emit' do
      before do
        user = users(:minimum_admin)
        user.name = 'test name'
      end

      it 'publishes protobuf events' do
        user.publish_user_events_to_bus!
        domain_event_publisher.events.length.must_equal 3
      end

      describe 'with an publishing error' do
        it 'logs and supresses exceptions' do
          user.expects(:user_events_encoder).raises(RuntimeError)
          Zendesk::StatsD::Client.any_instance.expects(:increment).once
          user.publish_user_events_to_bus! # implicit wont_raise(RuntimeError)
        end
      end

      describe 'statsd reporting' do
        it 'records domain event counts' do
          Zendesk::ModelChangeInstrumentation.any_instance.stubs(:report_change_source_metrics)
          Zendesk::StatsD::Client.any_instance.expects(:increment).times(3)
          user.publish_user_events_to_bus!
        end

        it 'records message size stats' do
          Zendesk::StatsD::Client.any_instance.expects(:histogram).times(3)
          user.publish_user_events_to_bus!
        end
      end
    end

    describe 'with no events to emit' do
      before do
        UserEventsProtobufEncoder.any_instance.stubs(:to_a).returns([])
      end

      it 'does not publish protobuf events' do
        user.publish_user_events_to_bus!
        domain_event_publisher.events.length.must_equal 0
      end
    end
  end
end
