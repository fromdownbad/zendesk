require_relative '../../../support/test_helper.rb'

SingleCov.covered!

describe GroupMembershipObserver do
  fixtures :users, :groups, :memberships
  include DomainEventsHelper
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }
  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:group) { groups(:support_group) }

  before do
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  describe 'after_create' do
    it 'should add an event UserGroupAdded to user' do
      user.memberships.create!(account: account, group: group, default: true)
      assert_equal decode_user_events(domain_event_publisher.events, :user_group_added).size, 1
    end
  end

  describe 'after_destroy' do
    let(:membership) { memberships(:minimum) }
    it 'should add an event UserGroupRemoved to user' do
      membership.destroy!
      assert_equal decode_user_events(domain_event_publisher.events, :user_group_removed).size, 1
    end
  end

  describe 'change groups' do
    it 'should add both UserGroupAdded and UserGroupRemoved to user' do
      user.memberships = []
      user.save!
      user.memberships.create!(account: account, group: group, default: true)
      assert_equal decode_user_events(domain_event_publisher.events, :user_group_removed).size, 1
      assert_equal decode_user_events(domain_event_publisher.events, :user_group_added).size, 1
    end
  end
end
