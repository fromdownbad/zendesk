require_relative '../../../support/test_helper.rb'

SingleCov.covered!

describe UserEntityObserver do
  fixtures :users
  let(:user) { users(:minimum_end_user) }

  describe 'on_save' do
    before do
      UserProtobufEncoder.any_instance.stubs(:any?).returns(true)
    end

    it 'publishes protobuf user' do
      UserEntityPublisher.any_instance.expects(:publish_for_user).once
      user.publish_user_entity_snapshot!
    end

    describe 'when updates an user' do
      it 'publishes user message when name is updated' do
        UserEntityPublisher.any_instance.expects(:publish_for_user).once
        new_user = users(:minimum_end_user)
        new_user.name = 'new name'
        new_user.save!
      end

      it 'publishes user message when email identity is created' do
        UserEntityPublisher.any_instance.expects(:publish_for_user).once

        UserEmailIdentity.create!(user: users(:minimum_end_user), value: "john@example.com", is_verified: true)
      end

      it 'publishes user message when email identity is updated' do
        # we will trigger the publisher twice because we will be interacting the identities via the user.
        # meaning that besides the save on the association model we will also get a updated_at change for the user.
        UserEntityPublisher.any_instance.expects(:publish_for_user).twice

        new_user = users(:minimum_end_user)
        new_user.email = "new@example.com"
        new_user.save!
      end

      it 'publishes user message when photo is updated' do
        # Publisher is called twice since we add two different photos.
        UserEntityPublisher.any_instance.expects(:publish_for_user).twice
        new_user = users(:minimum_end_user)
        new_user.reload.set_photo(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png")).save!
      end

      it 'publishes user message when photo is deleted' do
        new_user = users(:minimum_end_user)
        new_user.reload.set_photo(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png")).save!
        photo = Photo.find_by(user_id: new_user.id)

        # Publisher is called twice since we delete two different photos.
        UserEntityPublisher.any_instance.expects(:publish_for_user).twice
        photo.destroy
      end

      it 'publishes user message when identity is deleted' do
        email = UserEmailIdentity.create!(user: users(:minimum_end_user), value: "john@example.com", is_verified: true)

        UserEntityPublisher.any_instance.expects(:publish_for_user).once
        email.destroy
      end

      it 'published user message when user is suspended' do
        UserEntityPublisher.any_instance.expects(:publish_for_user).twice
        users(:minimum_end_user).suspend!
      end
    end
  end
end
