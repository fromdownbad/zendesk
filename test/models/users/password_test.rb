require_relative "../../support/test_helper"

SingleCov.covered!

describe 'UserPassword' do
  describe "A SHA1 password" do
    describe "created" do
      before do
        @password = Users::Password::SHA1.create('hello')
      end

      it "is encrypted" do
        assert_not_equal 'hello', @password
      end

      it "provides the unencrypted version" do
        assert_equal 'hello', @password.unencrypted
      end

      it "has a salt" do
        assert @password.salt.present?
      end

      it "is comparable with other passwords" do
        assert_equal @password, 'hello'
        assert_equal @password, Users::Password::SHA1.new(@password.to_s)
        assert_not_equal @password, 'hellO'
        # different salts
        assert_not_equal @password, Users::Password::SHA1.create('hello')
      end
    end

    describe "new" do
      it "uses the provided salt when available" do
        password = Users::Password::SHA1.new('hello', salt: 'salty')
        assert_equal 'salty', password.salt

        password = Users::Password::SHA1.new('hello', salt: '')
        assert_equal '', password.salt

        password = Users::Password::SHA1.new('hello', salt: nil)
        assert_nil password.salt
      end
    end
  end

  describe "A BCrypt password" do
    it "raises an invalid hash exception when not encrypted using BCrypt" do
      sha1_encrypted = Users::Password::SHA1.create('hello').to_s

      assert_raise(Users::Password::BCrypt::InvalidHash) { Users::Password::BCrypt.new(sha1_encrypted) }
    end

    describe "created" do
      before do
        @password = Users::Password::BCrypt.create('hello')
      end

      it "is encrypted" do
        assert_not_equal 'hello', @password
      end

      it "provides the unencrypted version" do
        assert_equal 'hello', @password.unencrypted
      end

      it "has a salt" do
        assert @password.salt.present?
      end

      it "is comparable with other passwords" do
        assert_equal @password, 'hello'
        assert_equal @password.to_s, Users::Password::BCrypt.new(@password.checksum, salt: @password.salt)
        assert_not_equal @password, 'hellO'
        assert_not_equal @password.to_s, Users::Password::BCrypt.create('hello')
      end
    end

    describe "duplicate?" do
      before do
        @password = Users::Password::BCrypt.create('hello')
      end

      it "is true when compared to the same password encrypted with the same salt and cost" do
        assert(@password.duplicate?(@password.to_s))
      end

      it "is true when compared to the same password encrypted with a different salt and cost" do
        encrypted_with_different_salt_and_cost = Users::Password::BCrypt.create('hello', cost: 5).to_s
        assert(@password.duplicate?(encrypted_with_different_salt_and_cost))
      end

      it "is false when compared to a different encrypted password" do
        different_password = Users::Password::BCrypt.create('helloO').to_s
        assert_equal false, @password.duplicate?(different_password)
      end

      it "is false compared to the same password encrypted with SHA1" do
        sha1_password = Users::Password::SHA1.create('hello').to_s
        assert_equal false, @password.duplicate?(sha1_password)
      end
    end

    describe "generation time" do
      it "is fast for test environments" do
        assert_equal 1, Users::Password::BCrypt.cost
      end

      it "is slower for non test environments" do
        Rails.env.stubs(:test?).returns(false)
        cost = Users::Password::BCrypt.cost
        assert_operator 2, :<=, cost, "Cost #{cost} is too low"
      end
    end
  end
end
