require_relative "../../support/test_helper"

SingleCov.covered!

describe Users::PhoneNumber do
  fixtures :accounts
  let (:account) { accounts(:minimum) }

  describe "#valid?" do
    before { Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true) }

    describe "when apply_prefix_and_normalize_phone_numbers arturo is disabled" do
      before { Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(false) }

      it "returns true always" do
        assert Users::PhoneNumber.new(account, nil).valid?
      end
    end

    it "does not blow up when the number is nil" do
      refute Users::PhoneNumber.new(account, nil).valid?
    end

    describe "When account setting is_end_user_phone_number_validation_enabled is TRUE" do
      before { Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true) }

      it "should return true when E164 validation it TRUE" do
        assert Users::PhoneNumber.new(account, '+15556439870').valid?
      end

      it "should return false when E164 validation it FALSE" do
        refute Users::PhoneNumber.new(account, '+155').valid?
      end
    end

    describe "When account setting is_end_user_phone_number_validation_enabled is FALSE" do
      before { Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(false) }

      it "should return true when regex validation is TRUE" do
        assert Users::PhoneNumber.new(account, '+155').valid?
      end

      it "should return true when regex validation is FALSE" do
        refute Users::PhoneNumber.new(account, '+1').valid?
      end
    end
  end

  describe "#unique?" do
    def new_invalid_number
      pn = account.user_identities.phone.new
      pn.user_id = account.users.first.id
      pn.value = "+155"
      pn.save(validate: false)
    end

    describe "when apply_prefix_and_normalize_phone_numbers arturo is disabled" do
      before { Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(false) }

      it "returns false if the account contains an identity matching the number" do
        new_invalid_number

        refute Users::PhoneNumber.new(account, '+155').unique?
      end

      it "returns true if there is no matching identity for the account" do
        account.user_identities.phone.delete_all

        assert Users::PhoneNumber.new(account, '+155').unique?
      end

      it "doesn't perform normalization of the number and returns true if there is no matching identity for the account" do
        new_invalid_number

        assert Users::PhoneNumber.new(account, '+1 5 5').unique?
      end

      describe "When account setting end_user_phone_number_validation is TRUE" do
        before { Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true) }

        it "returns false if the account contains a normalized number identity matching a normalized number" do
          new_invalid_number

          refute Users::PhoneNumber.new(account, '+155').unique?
        end

        it "returns false if the account contains a normalized number identity matching an unnormalized number" do
          new_invalid_number

          refute Users::PhoneNumber.new(account, '155').unique?
        end

        it "returns true if there is no matching identity for the account" do
          account.user_identities.phone.delete_all

          assert Users::PhoneNumber.new(account, '+155').unique?
        end
      end
    end

    describe "when apply_prefix_and_normalize_phone_numbers arturo is enabled" do
      before { Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true) }
      it "returns false if the account contains an identity matching the normalized number" do
        new_invalid_number

        refute Users::PhoneNumber.new(account, '+155').unique?
      end

      it "returns false if the account contains an identity matching the normalized number" do
        new_invalid_number

        refute Users::PhoneNumber.new(account, '+1 5 5').unique?
      end

      it "returns true if there is no matching identity for the account" do
        account.user_identities.phone.delete_all

        assert Users::PhoneNumber.new(account, '+155').unique?
      end
    end
  end

  describe "#valid_and_unique?" do
    it "calls valid and unique" do
      Users::PhoneNumber.any_instance.expects(:valid?).returns(true)
      Users::PhoneNumber.any_instance.expects(:unique?).returns(true)

      assert Users::PhoneNumber.new(account, '+155').valid_and_unique?
    end
  end
end
