require_relative '../../support/test_helper'
require_relative '../../support/suite_test_helper'
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered! uncovered: 41

describe Users::Roles do
  include SuiteTestHelper
  include MultiproductTestHelper

  fixtures :accounts, :subscriptions, :users, :user_identities, :user_settings, :rules, :zopim_subscriptions, :role_settings, :permission_sets
  resque_inline false
  let(:admin)           { users(:minimum_admin_not_owner) }
  let(:owner)           { users(:minimum_admin) }
  let(:agent)           { users(:minimum_agent) }
  let(:user)            { users(:minimum_end_user) }
  let(:permission_set)  { build_valid_permission_set }

  before do
    User.any_instance.stubs(encrypt_password: true)
    User.any_instance.stubs(password_security_policy_enforced: true)

    Zopim::Agent.any_instance.stubs(:link_to_remote_account_record)

    SecurityMailer.stubs(send: true)
  end

  describe 'when CP3 account with Chat enabled' do
    let(:account)             { accounts(:with_paid_zopim_subscription) }
    let(:admin)               { users(:with_paid_zopim_subscription_owner) }
    let!(:chat_enabled_agent) { FactoryBot.create(:chat_enabled_agent, account: account) }
    let!(:chat_only_agent)    { FactoryBot.create(:chat_only_agent,    account: account) }
    let!(:light_agent)        { FactoryBot.create(:light_agent,        account: account) }
    let!(:support_only_agent) { FactoryBot.create(:agent,              account: account) }

    describe '#chat_enabled_support_agents' do
      subject { account.users.chat_enabled_support_agents }

      it 'includes the correct users' do
        assert_includes subject, chat_enabled_agent
        refute_includes subject, admin
        refute_includes subject, chat_only_agent
        refute_includes subject, support_only_agent
      end
    end

    describe '#chat_only_agents' do
      subject { account.users.chat_only_agents }

      it 'includes the correct users' do
        assert_includes subject, chat_only_agent
        refute_includes subject, chat_enabled_agent
        refute_includes subject, admin
        refute_includes subject, support_only_agent
      end
    end

    describe '#non_chat_only_non_light_agents' do
      subject { account.users.non_chat_only_non_light_agents }

      it 'includes the correct users' do
        assert_includes subject, admin
        assert_includes subject, chat_enabled_agent
        refute_includes subject, chat_only_agent
        refute_includes subject, light_agent
        assert_includes subject, support_only_agent
      end
    end

    describe '#support_only_agents' do
      subject { account.users.support_only_agents }

      it 'includes the correct users' do
        refute_includes subject, chat_enabled_agent
        refute_includes subject, chat_only_agent
        assert_includes subject, support_only_agent
        assert_includes subject, admin
      end
    end

    describe '#billable_agents' do
      subject { account.users.billable_agents }

      it 'includes the correct users' do
        refute_includes subject, chat_only_agent
        assert_includes subject, chat_enabled_agent
        assert_includes subject, support_only_agent
      end
    end

    describe '#unbillable_agent?' do
      it 'Chat only agent is unbillable agent' do
        assert chat_only_agent.unbillable_agent?
      end

      it 'Light agent is unbillable agent' do
        assert light_agent.unbillable_agent?
      end

      it 'Support legacy agent is not unbillable agent' do
        refute support_only_agent.unbillable_agent?
      end

      it 'Admin is not unbillable agent' do
        refute admin.unbillable_agent?
      end

      it 'Support agnet with chat enabled is not unbillable agent' do
        refute chat_enabled_agent.unbillable_agent?
      end
    end

    describe '.assignable' do
      subject { User.assignable(account) }

      it 'includes the correct users' do
        refute_includes subject, chat_only_agent
        assert_includes subject, admin
        assert_includes subject, chat_enabled_agent
        assert_includes subject, support_only_agent
      end
    end
  end

  describe 'for an multiproduct account with Contributor permission set' do
    let(:account) { accounts(:multiproduct) }

    describe '.contributors' do
      subject { account.users.contributors }

      it 'includes the correct users' do
        assert_includes subject, users(:multiproduct_contributor)
        refute_includes subject, users(:multiproduct_owner)
        refute_includes subject, users(:multiproduct_support_agent)
        refute_includes subject, users(:multiproduct_light_agent)
        refute_includes subject, users(:multiproduct_support_admin)
        refute_includes subject, users(:multiproduct_custom_agent)
      end
    end

    describe '#billable_agents' do
      subject { account.users.billable_agents }

      it 'includes the correct users' do
        assert_includes subject, users(:multiproduct_owner)
        assert_includes subject, users(:multiproduct_support_agent)
        refute_includes subject, users(:multiproduct_contributor)
        refute_includes subject, users(:multiproduct_light_agent)
      end
    end

    describe '#unbillable_agent?' do
      it 'Contributor is unbillable agent' do
        assert users(:multiproduct_contributor).unbillable_agent?
      end

      it 'Light agent is unbillable agent' do
        assert users(:multiproduct_light_agent).unbillable_agent?
      end

      it 'Support agent is not unbillable agent' do
        refute users(:multiproduct_support_agent).unbillable_agent?
      end

      it 'Admin is not unbillable agent' do
        refute users(:multiproduct_owner).unbillable_agent?
      end
    end

    describe '.assignable' do
      subject { User.assignable(account) }

      it 'includes correct agents, and does not include Contributor' do
        assert_includes subject, users(:multiproduct_owner)
        assert_includes subject, users(:multiproduct_support_agent)
        refute_includes subject, users(:multiproduct_contributor)
        refute_includes subject, users(:multiproduct_light_agent)
      end
    end
  end

  describe '#answer_bot_users' do
    let(:account) { accounts(:minimum_answer_bot) }
    let!(:answer_bot_user) { users(:minimum_answer_bot_user) }
    let!(:answer_bot_user_setting) { user_settings(:minimum_answer_bot_user_1) }
    let!(:end_user) { users(:minimum_answer_bot_user_2) }

    subject { account.answer_bot_users }

    it 'includes the correct users' do
      refute_includes subject, end_user
      assert_includes subject, answer_bot_user
    end
  end

  describe '#scrubbed_ticket_anonymous_users' do
    let(:account) { accounts(:minimum) }
    let!(:anonymous_user) do
      Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account)
    end

    it 'includes the correct users' do
      assert_equal 1, account.scrubbed_ticket_anonymous_users.size, "Expected only one anonymous user per account"
    end
  end

  describe '.assignable' do
    subject { User.assignable(account) }
    let(:account) { accounts(:minimum) }
    let(:agent) { users(:minimum_agent) }
    let(:admin) { users(:minimum_admin) }
    let(:end_user) { users(:minimum_end_user) }

    describe 'for an account without permission sets' do
      let(:inactive_agent) { FactoryBot.create(:agent, account: account) }
      before do
        account.stubs(:has_permission_sets?).returns(false)
        # need to update this without triggering callbacks:
        User.connection.execute "update users set is_active = 0 where id = #{inactive_agent.id.to_i}"
      end

      it "includes the correct agents" do
        subject.must_include admin
        subject.must_include agent
        subject.wont_include inactive_agent
        subject.wont_include end_user
      end
    end

    describe 'for an account with light agent add-on' do
      let!(:light_agent) { FactoryBot.create(:light_agent, account: account) }

      it "does not include light agent" do
        subject.must_include admin
        subject.must_include agent
        subject.wont_include light_agent
      end
    end

    describe 'for an account with custom role that `ticket_editing` permission is false' do
      let(:custom_role) { PermissionSet.build_new_permission_set(account) }
      let(:custom_agent) { FactoryBot.create(:agent, account: account) }

      before do
        custom_role.permissions.ticket_editing = false
        custom_role.name = 'someone cannot edit tickets'
        custom_role.save!
        custom_agent.update_attribute(:permission_set_id, custom_role.id)
      end

      it 'does not include agent with custom role that has cannot edit tickets' do
        refute custom_role.permissions.ticket_editing
        subject.wont_include custom_agent
      end
    end
  end

  describe "#is_audited_by_cia?" do
    it "does not audit users" do
      assert_equal false, user.is_audited_by_cia?
    end

    it "audits agents" do
      assert(agent.is_audited_by_cia?)
    end

    it "audits ex-agents" do
      agent.roles = Role::END_USER.id
      assert(agent.is_audited_by_cia?)
    end
  end

  describe "#was_agent?" do
    it "does not be for ex-users" do
      user.roles = Role::AGENT.id
      assert_equal false, user.was_agent?
    end

    it "is for ex-agents" do
      agent.roles = Role::END_USER.id
      assert(agent.was_agent?)
    end

    it "is for ex-admins" do
      admin.roles = Role::END_USER.id
      assert(admin.was_agent?)
    end
  end

  describe "#was_admin?" do
    it "does not be for ex-agents" do
      agent.roles = Role::END_USER.id
      assert_equal false, user.was_admin?
    end

    it "is for ex-admins" do
      admin.roles = Role::END_USER.id
      assert(admin.was_admin?)
    end
  end

  describe "#is_zendesk_admin?" do
    it "does not be for support agents" do
      agent.account.stubs(:subdomain).returns('support')
      assert_equal false, agent.is_zendesk_admin?
    end

    it "does not be for non-support admins" do
      assert_equal false, admin.is_zendesk_admin?
    end

    it "is for support admins" do
      admin.account.stubs(:subdomain).returns('support')
      assert(admin.is_zendesk_admin?)
    end
  end

  describe "#role_name" do
    describe "when user is account owner" do
      it "returns 'Account owner'" do
        assert_equal 'Account owner', owner.role_name
      end
    end

    describe "when user is chat agent" do
      before do
        user.stubs(is_chat_agent?: true)
      end

      it "returns 'Chat agent'" do
        assert_equal 'Chat agent', user.role_name
      end
    end

    describe "when user is admin" do
      it "returns 'Administrator'" do
        assert_equal 'Administrator', admin.role_name
      end
    end

    describe "when subscription plan type is not extra large" do
      before do
        agent.account.subscription.stubs(plan_type: SubscriptionPlanType.Small)
      end

      describe "when user is non-restricted agent" do
        it "returns 'Agent'" do
          assert_equal 'Agent', agent.role_name
        end
      end

      describe "when user is restricted agent" do
        before do
          agent.stubs(:agent_restriction?).with(:none).returns(false)
        end

        it "returns 'Restricted agent'" do
          assert_equal 'Restricted agent', agent.role_name
        end
      end
    end

    describe "when subscription plan type is extra large" do
      before do
        agent.account.subscription.stubs(plan_type: SubscriptionPlanType.ExtraLarge)
      end

      describe "when user is light agent" do
        before do
          agent.stubs(is_light_agent?: true)
        end

        it "returns 'Light agent'" do
          assert_equal 'Light agent', agent.role_name
        end
      end

      describe "when user is agent" do
        before do
          agent.stubs(is_light_agent?: false)
        end

        it "returns 'Agent'" do
          assert_equal 'Agent', agent.role_name
        end
      end
    end

    describe "when user is end user" do
      it "returns 'End user'" do
        assert_equal 'End user', user.role_name
      end
    end
  end

  describe "#role_name_short" do
    describe "when user is account owner" do
      it "returns 'Owner'" do
        assert_equal 'Owner', owner.role_name_short
      end
    end

    describe "when user is chat agent" do
      before do
        user.stubs(is_chat_agent?: true)
      end

      it "returns 'Chat-only agent'" do
        assert_equal 'Chat-only agent', user.role_name_short
      end
    end

    describe "when user is admin" do
      it "returns 'Admin'" do
        assert_equal 'Admin', admin.role_name_short
      end
    end

    describe "when subscription plan type is not extra large" do
      before do
        agent.account.subscription.stubs(plan_type: SubscriptionPlanType.Small)
      end

      describe "when user is non-restricted agent" do
        it "returns 'Agent'" do
          assert_equal 'Agent', agent.role_name_short
        end
      end

      describe "when user is restricted agent" do
        before do
          agent.stubs(:agent_restriction?).with(:none).returns(false)
        end

        it "returns 'Restricted agent'" do
          assert_equal 'Restricted agent', agent.role_name_short
        end
      end
    end

    describe "when subscription plan type is extra large" do
      before do
        agent.account.subscription.stubs(plan_type: SubscriptionPlanType.ExtraLarge)
      end

      describe "when user is light agent" do
        before do
          agent.stubs(is_light_agent?: true)
        end

        it "returns 'Light agent'" do
          assert_equal 'Light agent', agent.role_name_short
        end
      end

      describe "when user is agent" do
        before do
          agent.stubs(is_light_agent?: false)
        end

        it "returns 'Agent'" do
          assert_equal 'Agent', agent.role_name_short
        end
      end

      describe "when user is end user" do
        it "returns 'User'" do
          assert_equal 'User', user.role_name_short
        end
      end
    end
  end

  describe "#is_zendesk_agent?" do
    it "does not be for support users" do
      user.account.stubs(:subdomain).returns('support')
      assert_equal false, user.is_zendesk_agent?
    end

    it "does not be for non-support agents" do
      assert_equal false, agent.is_zendesk_agent?
    end

    it "is for support agents" do
      agent.account.stubs(:subdomain).returns('support')
      assert(agent.is_zendesk_agent?)
    end

    it "is for support admins" do
      admin.account.stubs(:subdomain).returns('support')
      assert(admin.is_zendesk_agent?)
    end
  end

  describe "#is_agent?" do
    it "is not for users" do
      assert_equal false, user.is_agent?
    end

    it "is for agents" do
      assert(agent.is_agent?)
    end

    it "is for admins" do
      assert(admin.is_agent?)
    end
  end

  describe "#is_admin?" do
    it "does not be for agents" do
      assert_equal false, agent.is_admin?
    end

    it "is for admins" do
      assert(admin.is_agent?)
    end
  end

  describe "private rules" do
    describe "when an agent has private rules" do
      let(:agent_view) { rules(:view_for_minimum_agent) }
      let(:agent_macro) { rules(:macro_for_minimum_agent) }
      before do
        agent.roles = Role::END_USER.id
        agent.save!
      end

      it "soft deletes private views when the agent is downgraded" do
        assert(agent_view.reload.deleted?)
      end

      it "softs deletes private macros when the agent is downgraded" do
        assert(agent_macro.reload.deleted?)
      end
    end

    describe "when an agent doesn't have private rules" do
      let(:support_agent) { users(:support_agent) }

      before do
        View.expects(:soft_delete_all!).never
        Macro.expects(:soft_delete_all!).never
      end

      it "doesn't attempt to soft delete private rules when the agent is downgraded" do
        support_agent.roles = Role::END_USER.id
        support_agent.save!
      end
    end
  end

  describe "#permissions" do
    describe "for agents without permission_set" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(false)
        agent.stubs(:permission_set).returns(nil)
      end
      it "does not include the string that defines the permission set" do
        refute agent.permissions(false).include? "role-"
      end
    end

    describe "for agents with permission_set" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
        agent.stubs(:permission_set).returns(permission_set)
      end
      it "includes the string that defines the permission set" do
        assert agent.permissions(false).include? "role-#{agent.permission_set.to_md5}"
      end
    end
  end

  describe "#unassign_working_tickets" do
    it "enqueues the UnassignTicketsJob" do
      UnassignTicketsJob.expects(:enqueue)
      agent.roles = Role::END_USER.id
      agent.save!
    end

    it "enqueues the TicketBulkUpdateJob if this was the second-to-last agent" do
      agent.account.agents.reject { |u| [agent.id, admin.id].include?(u.id) }.each do |dropped_agent|
        dropped_agent.current_user = owner
        dropped_agent.delete!
      end
      TicketBulkUpdateJob.expects(:enqueue)
      agent.roles = Role::END_USER.id
      agent.save!
    end
  end

  describe "downgrading" do
    it "does not enqueue jobs if we had to rollback" do
      Resque.jobs.clear
      with_rollback(agent) do
        agent.roles = Role::END_USER.id
        agent.save!
      end
      assert agent.reload.is_agent?
      assert_equal 0, Resque.jobs.size
    end

    describe "revere subscription" do
      it "removes notification subscription" do
        agent.settings.revere_subscription = true
        agent.settings.save!

        agent.roles = Role::END_USER.id
        agent.save!

        refute agent.reload.settings.revere_subscription
      end

      it "enqueues update job" do
        agent.settings.revere_subscription = true
        agent.settings.save!

        RevereSubscriberUpdateJob.expects(:enqueue).with(agent.account.id, agent.id)

        agent.roles = Role::END_USER.id
        agent.save!
      end

      it "ignores an unsubscribed user" do
        agent.settings.revere_subscription = false
        agent.settings.save!

        agent.roles = Role::END_USER.id
        agent.save!

        RevereSubscriberUpdateJob.expects(:enqueue).never
        refute agent.reload.settings.revere_subscription
      end

      it "ignores user with no subscription user_setting" do
        # users start with no user setting for revere
        refute agent.settings.revere_subscription.present?

        agent.roles = Role::END_USER.id
        agent.save!

        RevereSubscriberUpdateJob.expects(:enqueue).never
        refute agent.reload.settings.revere_subscription.present?
      end
    end
  end

  describe "#agent_restriction?" do
    describe "in non enterprise plans" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(false)
        agent.stubs(:permission_set).returns(nil)
      end

      it "returns false for end users" do
        refute user.agent_restriction?(:none)
      end

      describe "for unrestricted agents" do
        before { agent.stubs(:restriction_id).returns(RoleRestrictionType.None) }

        it "returns true for :none and false for the rest" do
          assert agent.agent_restriction?(:none)
          refute agent.agent_restriction?(:organization)
          refute agent.agent_restriction?(:assigned)
          refute agent.agent_restriction?(:groups)
        end
      end

      describe "for agents restricted to organization tickets" do
        before { agent.stubs(:restriction_id).returns(RoleRestrictionType.Organization) }

        it "returns true for :organization and false for the rest" do
          refute agent.agent_restriction?(:none)
          assert agent.agent_restriction?(:organization)
          refute agent.agent_restriction?(:assigned)
          refute agent.agent_restriction?(:groups)
        end
      end

      describe "for agents restricted to assigned tickets" do
        before { agent.stubs(:restriction_id).returns(RoleRestrictionType.Assigned) }

        it "returns true for :assigned and false for the rest" do
          refute agent.agent_restriction?(:none)
          refute agent.agent_restriction?(:organization)
          assert agent.agent_restriction?(:assigned)
          refute agent.agent_restriction?(:groups)
        end
      end

      describe "for agents restricted to groups tickets" do
        before { agent.stubs(:restriction_id).returns(RoleRestrictionType.Groups) }

        it "returns true for :groups and false for the rest" do
          refute agent.agent_restriction?(:none)
          refute agent.agent_restriction?(:organization)
          refute agent.agent_restriction?(:assigned)
          assert agent.agent_restriction?(:groups)
        end
      end
    end

    describe "in enterprise plan" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
        agent.stubs(:permission_set).returns(permission_set)
      end

      it "returns false for end users" do
        refute user.agent_restriction?(:none)
      end

      describe "for agents with 'all' access" do
        before { permission_set.permissions.ticket_access = 'all' }

        it "returns true for :none and false for the rest" do
          assert agent.agent_restriction?(:none)
          refute agent.agent_restriction?(:organization)
          refute agent.agent_restriction?(:assigned)
          refute agent.agent_restriction?(:groups)
        end

        it "coerces User#restriction_id to RoleRestrictionType.None" do
          agent.restriction_id = RoleRestrictionType.Assigned
          agent.save!
          assert_equal RoleRestrictionType.None, agent.restriction_id
        end
      end

      describe "for agents with 'within-organization' access" do
        before { permission_set.permissions.ticket_access = 'within-organization' }

        it "returns true for :organization and false for the rest" do
          refute agent.agent_restriction?(:none)
          assert agent.agent_restriction?(:organization)
          refute agent.agent_restriction?(:assigned)
          refute agent.agent_restriction?(:groups)
        end

        it "coerces User#restriction_id to RoleRestrictionType.Organization" do
          agent.restriction_id = RoleRestrictionType.Assigned
          agent.save!
          assert_equal RoleRestrictionType.Organization, agent.restriction_id
        end
      end

      describe "for agents with 'within-groups' access" do
        before { permission_set.permissions.ticket_access = 'within-groups' }

        it "returns true for :groups and false for the rest" do
          refute agent.agent_restriction?(:none)
          refute agent.agent_restriction?(:organization)
          refute agent.agent_restriction?(:assigned)
          assert agent.agent_restriction?(:groups)
        end

        it "coerces User#restriction_id to RoleRestrictionType.Groups" do
          agent.restriction_id = RoleRestrictionType.Assigned
          agent.save!
          assert_equal RoleRestrictionType.Groups, agent.restriction_id
        end
      end

      describe "for agents with 'assigned-only' access" do
        before { permission_set.permissions.ticket_access = 'assigned-only' }

        it "returns true for :groups and false for the rest" do
          refute agent.agent_restriction?(:none)
          refute agent.agent_restriction?(:organization)
          assert agent.agent_restriction?(:assigned)
          refute agent.agent_restriction?(:groups)
        end

        it "coerces User#restriction_id to RoleRestrictionType.Assigned" do
          agent.restriction_id = RoleRestrictionType.None
          agent.save!
          assert_equal RoleRestrictionType.Assigned, agent.restriction_id
        end
      end
    end
  end

  describe "ticket_access" do
    [
      [:none,         "all"],
      [:assigned,     "assigned-only"],
      [:groups,       "within-groups"],
      [:organization, "within-organization"]
    ].each do |restriction, expected|
      describe "in non enterprise plans" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(false)
          agent.stubs(:permission_set).returns(nil)
        end

        describe "for agents restricted by #{restriction}" do
          let(:restriction_id) { RoleRestrictionType.find!(restriction) }
          before { agent.stubs(:restriction_id).returns(restriction_id) }

          it "returns #{expected}" do
            assert_equal expected, agent.ticket_access
          end
        end
      end

      describe "in enterprise plan" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          agent.stubs(:permission_set).returns(permission_set)
        end

        describe "for agents restricted by #{restriction}" do
          before { permission_set.permissions.ticket_access = expected }

          it "returns #{expected}" do
            assert_equal expected, agent.ticket_access
          end
        end
      end
    end

    describe "remove_revere_subscription" do
      it 'is ignored if not provided' do
        refute admin.settings.revere_subscription.present?
        admin.roles = Role::END_USER.id
        admin.save!
        refute admin.reload.settings.revere_subscription.present?
      end

      it 'is set to false when agent becomes user.' do
        admin.settings.revere_subscription = true
        admin.settings.save!

        admin.roles = Role::END_USER.id
        admin.save!
        refute admin.reload.settings.revere_subscription
      end

      it 'is left unchanged when role changes to agent' do
        admin.settings.revere_subscription = true
        admin.settings.save!

        admin.roles = Role::AGENT.id
        admin.save!
        assert admin.reload.settings.revere_subscription
      end
    end

    describe 'voice forwarding number' do
      it 'is deleted when the role is downgraded to a non-voice role' do
        ident = Zendesk::Users::Identities::Initializer.new(admin.account, admin, admin, type: UserVoiceForwardingIdentity.identity_type, value: '+353851234567x(null)').identity
        ident.save!

        admin.reload.voice_number.wont_be_nil

        admin.roles = Role::END_USER.id
        admin.save!

        admin.reload.voice_number.must_be_nil
      end
    end

    describe 'voice seat' do
      it 'is deleted when role changes to end user' do
        admin.account.user_seats.voice.create(user: admin)
        admin.roles = Role::END_USER.id
        admin.save!
        assert_equal admin.user_seats.voice.count(:all), 0
      end

      it 'is not deleted when role changes to an agent with no voice permissions' do
        admin.account.user_seats.voice.create(user: admin)

        permission_set.permissions.stubs(:voice_availability_access?).returns(false)
        admin.stubs(:permission_set).returns(permission_set)
        admin.roles = Role::AGENT.id
        admin.save!
        assert_equal admin.user_seats.voice.count(:all), 1
      end

      it 'is not deleted when role changes to admin' do
        agent = users(:minimum_agent)
        agent.account.user_seats.voice.create(user: agent)

        agent.stubs(:permission_set).returns(nil)
        agent.roles = Role::ADMIN.id
        UserObserver.stubs(:deliver_profile_admin_user_added)
        agent.save!
        assert_equal agent.user_seats.voice.count(:all), 1
      end

      it 'is not deleted when a role changes to an agent with voice permissions' do
        admin.account.user_seats.voice.create(user: admin)

        admin.stubs(:permission_set).returns(permission_set)
        admin.roles = Role::AGENT.id
        admin.save!
        assert_equal admin.user_seats.voice.count(:all), 1
      end

      it 'is not deleted when a role changes to an agent with no permissions set' do
        admin.account.user_seats.voice.create(user: admin)

        admin.stubs(:permission_set).returns(nil)
        admin.roles = Role::AGENT.id
        admin.save!
        assert_equal admin.user_seats.voice.count(:all), 1
      end

      it 'is not deleted when a role changes to an agent with no permissions' do
        admin.account.user_seats.voice.create(user: admin)

        admin.stubs(:permission_set).returns(permission_set)
        permission_set.stubs(:permissions).returns(nil)
        admin.roles = Role::AGENT.id
        admin.save!
        assert_equal admin.user_seats.voice.count(:all), 1
      end

      it 'is deleted when a role changes from an agent with voice permissions to an agent without voice permissions' do
        permission_set.stubs(:max_limit).returns(true)
        agent = users(:minimum_agent)
        agent.account.user_seats.voice.create(user: agent)
        agent.stubs(:has_permission_set?).returns(true)
        permission_set.permissions.stubs(:voice_availability_access?).returns(false)
        agent.permission_set = permission_set
        agent.save!
        assert_equal agent.user_seats.voice.count(:all), 0
      end
    end
  end

  describe '#valid_billable_agent' do
    describe 'trial account' do
      let(:trial_account) { accounts(:trial) }
      let(:new_max_agent_count) { trial_account.subscription.max_agents + 1 }
      let(:current_agent_count) { trial_account.agents.count(:all) }

      it 'increments and syncs max_agents' do
        create_agents(trial_account, new_max_agent_count - current_agent_count)
        assert_equal trial_account.subscription.reload.max_agents, new_max_agent_count
      end

      it 'syncs product info with account service' do
        Omnichannel::AccountProductSyncJob.expects(:enqueue).with(account_id: trial_account.id)
        create_agents(trial_account, new_max_agent_count - current_agent_count)
      end
    end

    describe 'subscribed account' do
      describe 'legacy (non-shell) account' do
        let(:support_account) { accounts(:support) }

        it 'does not call the staff service' do
          Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).never
          create_agents(support_account, 1)
        end
      end

      describe 'shell account' do
        let(:shell_account) { accounts(:multiproduct) }

        describe 'without suite enabled' do
          it 'does not call the staff service' do
            Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).never
            create_agents(shell_account, 1)
          end
        end

        describe 'with suite enabled' do
          before { Account.any_instance.stubs(:has_active_suite?).returns(true) }

          it 'calls the staff service' do
            Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).returns(1)
            create_agents(shell_account, 1)
          end

          it 'is invalid if max_agents is exceeded' do
            Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).returns(0)

            assert_raises ActiveRecord::RecordInvalid do
              create_agents(shell_account, 1)
            end
          end

          it 'is invalid if staff service is unavailable' do
            Zendesk::StaffClient.any_instance.stubs(:seats_remaining_support_or_suite!).raises(Kragle::ClientError)
            assert_raises ActiveRecord::RecordInvalid do
              create_agents(shell_account, 1)
            end
          end

          it 'is valid if max_agents is not exceeded' do
            Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).returns(1)
            create_agents(shell_account, 1)
            assert agent.valid?
          end

          it 'is valid if the user is already a cp4 agent' do
            agent = shell_account.users.new(name: "CP4 Agent", email: "other@someexample.org")
            agent.roles = Role::AGENT.id
            agent.settings.chat_entitlement = "agent"
            agent.save!

            assert agent.valid?
          end

          it 'is valid if the user is updated during reverse-sync' do
            Zendesk::StaffClient.any_instance.stubs(:seats_remaining_support_or_suite!).returns(0)
            agent = shell_account.users.new(name: "Agent", email: "other@someexample.org")
            agent.roles = Role::AGENT.id
            agent.reverse_sync = true

            agent.save!
            assert agent.valid?
          end

          it 'is valid if account has synchronous entitlement update and staff management features on' do
            Zendesk::StaffClient.any_instance.stubs(:seats_remaining_support_or_suite!).returns(0)
            Account.any_instance.stubs(:has_ocp_synchronous_entitlements_update?).returns(true)
            Account.any_instance.stubs(:has_central_admin_staff_mgmt_roles_tab?).returns(true)

            create_agents(shell_account, 1)
            assert agent.valid?
          end
        end
      end
    end

    def create_agents(account, num)
      num.times do |i|
        agent = account.users.new(name: "Other Agent #{i}", email: "other@someexample#{i}.org")
        agent.roles = Role::AGENT.id
        agent.save!
      end
    end
  end

  describe '#valid_owner_role' do
    let(:validation_message) do
      "Validation failed: Invalid role combination for account owner. Account owner is always admin."
    end

    before do
      User.any_instance.stubs(:cannot_remove_last_agent_or_admin)
    end

    describe 'for new user validation (with no id) and an account with no owner id' do
      # owner_id is saved as nil temporarily during shell account creation
      # When creating new users, their IDs are also nil, causing this validation to be run unnecessarily
      let(:account) { accounts(:minimum) }

      it 'does not raise validation errors requiring that the owner must be an admin' do
        account.update_column(:owner_id, nil)
        account.users.new.tap do |user|
          user.name = 'test-user'
          user.email = 'testtesttest@example.com'
        end.save!
      end
    end

    describe 'for a non-shell account' do
      let(:owner) { users(:minimum_admin) }

      it 'validates that the owner must be an admin' do
        owner.roles = Role::END_USER.id
        e = assert_raises ActiveRecord::RecordInvalid do
          owner.save!
        end
        assert_equal validation_message, e.message
      end
    end

    describe 'for a shell account with support' do
      let(:account) { setup_shell_account_with_support }

      it 'validates that the owner must be an admin' do
        account.owner.roles = Role::END_USER.id
        e = assert_raises ActiveRecord::RecordInvalid do
          account.owner.save!
        end
        assert_equal validation_message, e.message
      end
    end

    describe 'for a shell account without support' do
      let(:account) { setup_shell_account }

      it 'it validates owner is not an end user' do
        account.owner.roles = Role::END_USER.id
        refute account.owner.valid?
      end

      it 'allows owner to be an agent' do
        account.owner.roles = Role::AGENT.id
        assert account.owner.valid?
      end
    end
  end

  describe '#has_permission_set?' do
    subject { user.has_permission_set? }

    describe 'for admin' do
      let(:user) { users(:minimum_admin) }

      it 'is false' do
        refute subject
      end

      describe 'when permissions_allow_admin_permission_sets arturo is enabled' do
        before do
          Arturo.enable_feature!(:permissions_allow_admin_permission_sets)
          user.account.stubs(:has_permission_sets?).returns(true)
          user.permission_set = PermissionSet.build_default_permission_sets(user.account).first
        end

        it 'is true' do
          assert subject
        end
      end
    end

    describe 'for end user' do
      let(:user) { users(:minimum_end_user) }

      it 'is false' do
        refute subject
      end
    end

    describe 'for agent' do
      let(:user) { users(:minimum_agent) }

      describe 'without permission set' do
        it 'is false' do
          refute subject
        end
      end

      describe 'with permission set' do
        let(:custom_roles_enabled) { false }

        before { user.account.stubs(has_permission_sets?: custom_roles_enabled) }

        describe 'with custom roles disabled' do
          let(:custom_roles_enabled) { false }
          let(:custom_role) { PermissionSet.build_default_permission_sets(user.account).first }

          before { user.permission_set = custom_role }

          it 'is false' do
            refute subject
          end
        end

        describe 'with custom roles enabled' do
          let(:custom_roles_enabled) { true }
          let(:custom_role) { PermissionSet.build_default_permission_sets(user.account).first }

          before { user.permission_set = custom_role }

          it 'is true' do
            assert subject
          end
        end

        describe 'as chat agent' do
          let(:user) { users(:with_paid_zopim_subscription_chat_agent) }

          it 'is true' do
            assert subject
          end
        end

        describe 'as light agent' do
          let(:user) { users(:multiproduct_light_agent) }

          before { user.account.stubs(has_light_agents?: true) }

          it 'is true' do
            assert subject
          end
        end

        describe 'as contributor' do
          let(:user) { users(:multiproduct_contributor) }

          it 'is true' do
            assert subject
          end
        end
      end
    end
  end

  describe '#was_unbillable_agent?' do
    subject { user.was_unbillable_agent? }

    before do
      change_user
    end

    describe 'was end user' do
      let(:user) { users(:support_sample_customer) }
      let(:change_user) do
        user.roles = Role::AGENT.id
      end

      it 'is false' do
        refute subject
      end
    end

    describe 'was admin' do
      let(:user) { users(:support_admin) }
      let(:change_user) do
        user.roles = Role::AGENT.id
      end

      it 'is false' do
        refute subject
      end
    end

    describe 'was agent' do
      let(:user) { users(:support_agent) }
      let(:change_user) do
        user.roles = Role::ADMIN.id
      end

      it 'is false' do
        refute subject
      end
    end

    describe 'was custom agent' do
      let(:user) { users(:multiproduct_custom_agent) }
      let(:change_user) do
        user.roles = Role::ADMIN.id
        user.permission_set = nil
      end

      it 'is false' do
        refute subject
      end
    end

    describe 'was light agent' do
      let(:user) { users(:multiproduct_light_agent) }
      let(:change_user) do
        user.roles = Role::ADMIN.id
        user.permission_set = nil
      end

      it 'is true' do
        assert subject
      end
    end

    describe 'was chat agent' do
      let(:user) { users(:with_paid_zopim_subscription_chat_agent) }
      let(:change_user) do
        user.roles = Role::ADMIN.id
        user.permission_set = nil
      end

      it 'is true' do
        assert subject
      end
    end

    describe 'was contributor' do
      let(:user) { users(:multiproduct_contributor) }
      let(:change_user) do
        user.roles = Role::ADMIN.id
        user.permission_set = nil
      end

      it 'is true' do
        assert subject
      end
    end

    describe 'was nothing' do
      let(:user) { User.new(account: accounts(:minimum)) }
      let(:change_user) {}

      it 'is false' do
        refute subject
      end
    end
  end

  def setup_stubs_for_adding_agents(account)
    PermissionSet.create_chat_agent!(account)

    User.any_instance.stubs(encrypt_password: true)
    User.any_instance.stubs(password_security_policy_enforced: true)

    Zopim::Agent.any_instance.stubs(:link_to_remote_account_record)

    SecurityMailer.stubs(send: true)
  end
end
