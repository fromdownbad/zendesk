require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 24

describe Users::Access do
  fixtures :plans, :accounts, :account_property_sets, :subscriptions,
    :users, :remote_authentications, :attachments, :posts, :tickets, :groups,
    :organizations, :memberships, :organization_memberships, :users, :forums, :translation_locales

  class << self
    def should_be_able_to_manage_email_addresses
      it "is able to manage email addresses" do
        assert(@user.can?(:manage_identities_of, @user), "can not manage their identities")
      end
    end

    def should_not_be_able_to_manage_email_addresses
      it "does not be able to manage email addresses" do
        assert(!@user.can?(:manage_identities_of, @user), "can manage their identities")
      end
    end

    def should_be_able_to_manage_external_accounts
      it "is able to manage external accounts" do
        assert(@user.can?(:manage_external_accounts_of, @user), "can not manage their external accounts")
      end
    end

    def should_not_be_able_to_manage_external_accounts
      it "does not be able to manage external accounts" do
        assert(!@user.can?(:manage_external_accounts_of, @user), "can manage their external accounts")
      end
    end

    def should_be_able_to_manually_verify_an_email_address
      it "is able to manually verify an email address" do
        assert(@user.can?(:manually_verify_identities_of, @user), "can not manually verify their identities")
      end
    end

    def should_not_be_able_to_manually_verify_an_email_address
      it "does not be able to manually verify an email address" do
        assert(!@user.can?(:manually_verify_identities_of, @user), "can manually verify their identities")
      end
    end

    def should_be_able_to_manage_email_addresses_of_another_user
      it "is able to manage other user's email addresses" do
        assert(@user.can?(:manage_identities_of, @other_user), "can not manage identities of #{@other_user}")
      end
    end

    def should_not_be_able_to_manage_email_addresses_of_another_user
      it "does not be able to manage other user's email addresses" do
        assert(!@user.can?(:manage_identities_of, @other_user), "can manage identities of #{@other_user}")
      end
    end

    def should_be_able_to_view_attachment
      it "is able to view the attachment" do
        assert(@user.can?(:view, @attachment), "can not view attachment")
      end
    end

    def should_not_be_able_to_view_attachment
      it "does not be able to view the attachment" do
        assert(!@user.can?(:view, @attachment), "can view attachment")
      end
    end
  end

  describe "on an account without remote authentication" do
    before do
      @other_user = users(:minimum_author)
      @other_user.stubs(:login_allowed?).with(:remote).returns(false)
    end

    describe "an end-user" do
      before do
        @user = users(:minimum_end_user)
        @user.stubs(:login_allowed?).with(:remote).returns(false)
      end

      should_be_able_to_manage_email_addresses
      should_be_able_to_manage_external_accounts
      should_not_be_able_to_manually_verify_an_email_address
      should_not_be_able_to_manage_email_addresses_of_another_user
    end

    describe "an agent" do
      before do
        @user = users(:minimum_agent)
        @user.stubs(:login_allowed?).with(:remote).returns(false)
      end

      should_be_able_to_manage_email_addresses
      should_be_able_to_manage_external_accounts
      should_be_able_to_manually_verify_an_email_address
      should_be_able_to_manage_email_addresses_of_another_user
    end

    describe "an admin" do
      before do
        @user = users(:minimum_admin)
        @user.stubs(:login_allowed?).with(:remote).returns(false)
      end

      should_be_able_to_manage_email_addresses
      should_be_able_to_manage_external_accounts
      should_be_able_to_manually_verify_an_email_address
      should_be_able_to_manage_email_addresses_of_another_user
    end
  end

  describe "on an account with remote authentication" do
    before { @other_user = users(:minimum_end_user) }

    describe "an end-user" do
      before do
        @user = users(:minimum_end_user)
        @user.stubs(:login_allowed?).with(:remote).returns(true)
      end

      should_not_be_able_to_manage_email_addresses
      should_be_able_to_manage_external_accounts
      should_not_be_able_to_manually_verify_an_email_address
      should_not_be_able_to_manage_email_addresses_of_another_user
    end

    describe "an agent" do
      before do
        @user = users(:minimum_agent)
        @user.stubs(:login_allowed?).with(:remote).returns(true)
      end

      should_be_able_to_manage_email_addresses
      should_be_able_to_manage_external_accounts
      should_be_able_to_manually_verify_an_email_address
      should_be_able_to_manage_email_addresses_of_another_user
    end

    describe "an admin" do
      before do
        @user = users(:minimum_admin)
        @user.stubs(:login_allowed?).with(:remote).returns(true)
      end

      should_be_able_to_manage_email_addresses
      should_be_able_to_manage_external_accounts
      should_be_able_to_manually_verify_an_email_address
      should_be_able_to_manage_email_addresses_of_another_user
    end
  end

  describe "accessing attachments" do
    before { @attachment = attachments(:attachment_entry) }

    describe "an agent" do
      before { @user = users(:minimum_agent) }
      should_be_able_to_view_attachment
    end

    describe "with Post as source" do
      before do
        @attachment.stubs(:source).returns(posts(:ponies_post_1))
        @attachment.stubs(:is_public?).returns(false)
      end

      describe "an end user" do
        before { @user = users(:minimum_end_user) }

        describe "with access to the post" do
          before { @user.abilities.expects(:can?).with(:view, anything).returns(true) }
          should_be_able_to_view_attachment
        end
        describe "without access to the post" do
          before { @user.abilities.expects(:can?).with(:view, anything).returns(false) }
          should_not_be_able_to_view_attachment
        end
      end
    end
  end

  describe "#end_user_viewable_organizations" do
    let(:organization1) { organizations(:minimum_organization1) }
    let(:organization2) { organizations(:minimum_organization2) }
    let(:user) { users(:minimum_end_user) }

    before do
      Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
      organization1.update_attribute(:is_shared, true)
      user.update_attribute(:organizations, [organization1, organization2])
    end

    describe "when the end_user can view all their own orgs" do
      before do
        user.stubs(:end_user_restriction?).with(:organization).returns(true)
      end

      it "returns all their organizations" do
        assert_equal [organization1, organization2], user.end_user_viewable_organizations
      end
    end

    describe "when the user cannot view all their orgs" do
      before do
        user.stubs(:end_user_restriction?).with(:organization).returns(false)
      end

      it "only return shared orgs" do
        assert_equal [organization1], user.end_user_viewable_organizations
      end
    end
  end

  describe '#assignable_groups' do
    let(:account) { accounts(:with_groups) }

    before do
      @user = users(:with_groups_agent2)
      @user.add_group_memberships([account.default_group.id])
    end

    it 'returns all of the groups in an account' do
      assert_equal(account.groups.size, account.groups.assignable(@user).size)
    end

    describe 'when account does not have the groups feature' do
      before do
        Account.any_instance.stubs(:has_groups?).returns(false)
      end

      it "returns only the account's default group" do
        assert_equal([account.default_group], account.groups.assignable(@user))
      end
    end
  end

  describe 'Given a groups role restriction' do
    before do
      @user = users(:with_groups_agent1)
      @user.restriction_id = RoleRestrictionType.GROUPS
    end

    describe '#assignable_groups' do
      it 'returns only the users groups' do
        assert_equal(@user.groups.size, @user.account.groups.assignable(@user).size)
      end
    end

    describe 'and assign_tickets_to_any_group enabled' do
      before do
        @user.stubs(:can?).with(:assign_to_any_group, Ticket).returns(true)
        @account = accounts(:with_groups)
      end

      it 'returns all of the groups in an account' do
        assert_equal(@account.groups.size, @account.groups.assignable(@user).size)
      end
    end
  end

  describe '#assignable_agents' do
    before do
      @admin = users(:with_groups_admin)
      @agent1 = users(:with_groups_agent1)
      @agent2 = users(:with_groups_agent2)
      @agent3 = users(:with_groups_agent_groups_restricted)
    end

    describe "For agents without group memberships" do
      before do
        @agent1.groups.clear
        @assignable_agents = @admin.account.memberships.assignable(@admin).map(&:user)
      end

      it 'does not return agents that do not belong to any groups' do
        refute @assignable_agents.include?(@agent1), @assignable_agents.inspect
      end
    end

    describe "with agents in multiple groups" do
      before do
        groups(:with_groups_group1)
        groups(:with_groups_group2)

        @expected_assignable_agents = [@agent1, @agent2, @agent3, @admin].sort_by(&:name)
        @assignable_agents = @admin.account.memberships.assignable(@admin).map(&:user).uniq.sort_by(&:name)
      end

      it "returns agents" do
        assert_equal @expected_assignable_agents, @assignable_agents
      end
    end

    describe "with agents in a single group" do
      before do
        @group = groups(:with_groups_group1)
        @assignable_agents = @admin.account.memberships.
          assignable(@admin).
          where(group_id: @group.id).
          includes(:user).
          map(&:user).
          sort_by(&:name)
      end

      it "returns agents" do
        assert_equal [@agent1, @agent2], @assignable_agents
      end
    end

    describe "(performance)" do
      before do
        groups(:with_groups_group1)
        @admin.account
        @admin.account.has_permission_sets?
      end

      it "does not N+1 over group memberships/users" do
        assert_sql_queries 3 do
          @admin.assignable_groups_and_agents
        end
      end
    end
  end

  describe '#assignable_groups_and_agents' do
    before do
      @admin = users(:minimum_admin)
      @groups = @admin.groups
      @expected_response = @groups.map do |group|
        {
          id: group.id,
          name: group.name,
          agents: group.users.map do |user|
            {
              id: user.id,
              name: user.name,
              email: user.email,
              group_id: group.id
            }
          end
        }
      end
    end

    it "returns a list of assignable groups with nested agents" do
      assert_equal @expected_response, @admin.assignable_groups_and_agents
    end
  end

  describe "#entry_conditions" do
    describe "for an end-user" do
      let(:user) { users(:minimum_end_user) }

      describe "when requesting entries or forums" do
        let(:common_forums_beginning) { 'forums.account_id = 90538 AND ' }
        let(:common_forums_ending) { ' AND forums.deleted_at IS NULL AND forums.visibility_restriction_id != 3 AND (forums.translation_locale_id = 1 OR forums.translation_locale_id IS NULL)' }
        let(:common_entries_beginning) { 'entries.account_id = 90538 AND ' }
        let(:common_entries_ending) { ' AND entries.is_public = 1' }

        describe "and is part of a single organization" do
          # :minimum_end_user fixture is already part of an org

          it "returns a condition which includes that organization" do
            entries_condition = common_entries_beginning + '(entries.organization_id IS NULL OR entries.organization_id IN (74202))' + common_entries_ending
            forums_condition  = common_forums_beginning + '(forums.organization_id IS NULL OR forums.organization_id IN (74202))' + common_forums_ending

            assert_equal entries_condition, user.entry_conditions
            assert_equal forums_condition, user.entry_conditions('forums')
          end
        end

        describe "and is part of multiple organizations" do
          before do
            Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
            user.update_attribute(:organizations, [organizations(:minimum_organization1), organizations(:minimum_organization2)])
          end

          it "returns a condition which includes those organizations" do
            entries_condition = common_entries_beginning + '(entries.organization_id IS NULL OR entries.organization_id IN (74202, 62916))' + common_entries_ending
            forums_condition  = common_forums_beginning + '(forums.organization_id IS NULL OR forums.organization_id IN (74202, 62916))' + common_forums_ending

            assert_equal entries_condition, user.entry_conditions
            assert_equal forums_condition, user.entry_conditions('forums')
          end
        end

        describe "and is part of NO organization" do
          before do
            user.stubs(organization_ids: [])
          end

          it "returns a condition which specifically excludes organizations" do
            entries_condition = common_entries_beginning + 'entries.organization_id IS NULL' + common_entries_ending
            forums_condition  = common_forums_beginning + 'forums.organization_id IS NULL' + common_forums_ending

            assert_equal entries_condition, user.entry_conditions
            assert_equal forums_condition, user.entry_conditions('forums')
          end
        end
      end
    end

    describe "for an agent" do
      let(:user) { users(:minimum_agent) }

      it "returns a condition which does not include tags or organizations, as these restrictions do not apply to agents" do
        entries_condition = 'entries.account_id = 90538'
        forums_condition  = 'forums.account_id = 90538'

        assert_equal entries_condition, user.entry_conditions
        assert_equal forums_condition, user.entry_conditions('forums')
      end
    end
  end

  describe "Accessing a ticket" do
    let(:organization) { organizations(:minimum_organization1) }
    let(:account)      { user.account }
    let(:requester_clause)    { "tickets.requester_id = #{user.id}" }
    let(:collaborator_clause) { "tickets.id IN (SELECT ticket_id FROM collaborations WHERE user_id = #{user.id})" }

    describe "as an admin" do
      it "has no conditions" do
        assert_nil users(:minimum_admin).ticket_conditions
      end
    end

    describe "as an agent" do
      let(:user) { users(:minimum_agent) }
      let(:assignee_clause) { "tickets.assignee_id = #{user.id}" }

      def conditions_wrapper(conditions)
        "((#{conditions.join(' OR ')}) OR #{collaborator_clause})"
      end

      before do
        user.stubs(:agent_restriction?).with(:groups).returns(false)
        user.stubs(:agent_restriction?).with(:organization).returns(false)
        user.stubs(:agent_restriction?).with(:assigned).returns(false)
      end

      describe "restricted by groups" do
        before { user.stubs(:agent_restriction?).with(:groups).returns(true) }

        it "scopes the conditions by the agent's groups" do
          group_ids = user.groups.map(&:id).join(",")
          conditions = ["tickets.group_id IN (#{group_ids})", requester_clause]

          assert_equal conditions_wrapper(conditions), user.ticket_conditions
        end

        it "uses an id of 0 for agent's conditions if they don't belong to any groups" do
          user.stubs(groups: [])
          conditions = ["tickets.group_id IN (0)", requester_clause]

          assert_equal conditions_wrapper(conditions), user.ticket_conditions
        end
      end

      describe "restricted by organization" do
        before { user.stubs(:agent_restriction?).with(:organization).returns(true) }

        it "uses an id of 0 for agent's conditions if they don't belong to an organization" do
          user.stubs(organization_id: nil)
          conditions = ["tickets.organization_id IN (0)", requester_clause]

          assert_equal conditions_wrapper(conditions), user.ticket_conditions
        end

        it "scopes the conditions to the agent's organizations" do
          user.update_attribute(:organization, organization)
          org_ids = user.reload.organization_ids.join(",")
          conditions = ["tickets.organization_id IN (#{org_ids})", requester_clause]

          assert_equal conditions_wrapper(conditions), user.ticket_conditions
        end
      end

      describe "restricted by assignee" do
        before { user.stubs(:agent_restriction?).with(:assigned).returns(true) }

        it "uses the agent's id in the assignee conditions" do
          conditions = [assignee_clause, requester_clause]

          assert_equal conditions_wrapper(conditions), user.ticket_conditions
        end
      end

      describe "with account collaboration disabled" do
        before do
          user.stubs(:agent_restriction?).with(:assigned).returns(true)
          account.stubs(all_collaboration_disabled?: true)
        end

        it "does not include a collaboration condition clause" do
          conditions = [assignee_clause, requester_clause]
          assert_equal "(#{conditions.join(' OR ')})", user.ticket_conditions
        end
      end
    end

    describe "as an end user" do
      let(:user)                { users(:minimum_end_user) }
      let(:organization_ids)    { user.organization_ids.join(",") }
      let(:organization_clause) { "tickets.organization_id IN (#{organization_ids})" }
      let(:expected) { [requester_clause, organization_clause, collaborator_clause].join(' OR ') }

      before { user.stubs(:end_user_restriction?).with(:organization).returns(true) }

      it "uses 0 for ticket.requester_id if id isn't present" do
        user.stubs(end_user_viewable_organizations: [])
        user.stubs(id: nil)
        assert_equal "(tickets.requester_id = 0)", user.ticket_conditions
      end

      it "includes tickets associated with user's organization" do
        assert_equal "(#{expected})", user.ticket_conditions
      end

      it "does not include tickets associated with user's organization" do
        user.stubs(end_user_viewable_organizations: [])
        expected = [requester_clause, collaborator_clause].join(" OR ")
        assert_equal "(#{expected})", user.ticket_conditions
      end

      describe 'when legacy CCs are enabled' do
        before { account.stubs(is_collaboration_enabled?: true) }

        it 'includes tickets user is copied on' do
          assert_equal "(#{expected})", user.ticket_conditions
        end
      end

      describe 'when email CCs are enabled' do
        before { account.stubs(has_comment_email_ccs_allowed_enabled?: true) }

        it 'includes tickets user is copied on' do
          assert_equal "(#{expected})", user.ticket_conditions
        end
      end

      describe 'when legacy CCs is disabled, but email CCs is enabled' do
        before do
          account.stubs(has_collaboration_enabled_enabled?: false, has_comment_email_ccs_allowed_enabled?: true)
        end

        it 'includes tickets user is copied on' do
          assert_equal "(#{expected})", user.ticket_conditions
        end
      end

      describe 'when all collaborations are disabled' do
        before do
          account.stubs(has_collaboration_enabled_enabled?: false, has_comment_email_ccs_allowed_enabled?: false)
        end

        it 'does not include tickets user is copied on' do
          expected = [requester_clause, organization_clause].join(' OR ')
          assert_equal "(#{expected})", user.ticket_conditions
        end
      end
    end

    it "allows a foreign user to edit tickets" do
      ticket = tickets(:minimum_1)

      user = FactoryBot.create(:end_user, account: ticket.account)
      user.foreign = true
      user.save!

      assert user.can?(:edit, ticket)
    end
  end

  describe "#search_access_control_filters" do
    describe "for an agent" do
      let(:user) { users(:minimum_agent) }

      describe "with no filter" do
        let(:options) { {forum_id: nil} }

        describe "and no restrictions" do
          it "does not modify ACL conditions" do
            filters = user.search_access_control_filters(options)
            assert_equal [], filters
          end
        end

        describe "and group ticket access restrictions" do
          before { user.restriction_id = RoleRestrictionType.GROUPS }

          it "modifys the ACL conditions to return only tickets in their group or that reference the user" do
            filters = user.search_access_control_filters(options)
            assert_equal ["group_id:(66248) OR requester_id:57888 OR cc_id:57888 OR (-_type:ticket)"], filters
          end
        end

        describe "and no groups ticket access restrictions" do
          before { user.restriction_id = RoleRestrictionType.GROUPS }

          it "where agent has no group" do
            user.stubs(groups: [])
            filters = user.search_access_control_filters(options)
            assert_equal ["group_id:(0) OR requester_id:57888 OR cc_id:57888 OR (-_type:ticket)"], filters
          end
        end

        describe "and organization ticket access restrictions" do
          before { user.restriction_id = RoleRestrictionType.ASSIGNED }

          it "modifys the ACL conditions to only return tickets for which user is the requester/assignee/cc" do
            filters = user.search_access_control_filters(options)
            assert_equal ["assignee_id:57888 OR requester_id:57888 OR cc_id:57888 OR _type:(entry OR user OR article)"], filters
          end
        end

        describe "and organization ticket access restrictions" do
          before { user.restriction_id = RoleRestrictionType.ORGANIZATION }

          describe "where the agent has no organizations" do
            it "modifys the ACL conditions to filter organization_id to 0" do
              filters = user.search_access_control_filters(options)
              assert_equal ["organization_id:(0) OR requester_id:57888 OR cc_id:57888 OR _type:article"], filters
            end
          end

          describe "where agent has a single organization" do
            before { user.update_attribute(:organizations, [organizations(:minimum_organization1)]) }

            it "modifys the ACL conditions to filter organization_id by their single organization ID" do
              filters = user.search_access_control_filters(options)
              assert_equal ["organization_id:(74202) OR requester_id:57888 OR cc_id:57888 OR _type:article"], filters
            end
          end

          describe "where agent has multiple organizations" do
            before do
              Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
              user.update_attribute(:organizations, [organizations(:minimum_organization1), organizations(:minimum_organization2)])
            end

            it "modifys the ACL conditions to filter organization_id by their multiple organization IDs" do
              filters = user.search_access_control_filters(options)
              assert_equal ["organization_id:(74202 OR 62916) OR requester_id:57888 OR cc_id:57888 OR _type:article"], filters
            end
          end
        end
      end
    end

    describe "for an end-user" do
      let(:user) { users(:minimum_end_user) }

      describe "with an organization_id filter" do
        describe "organization_id filter is an integer" do
          let(:options) { {with: {organization_id: 99}, without: {}} }

          describe "for a user that has one organization and that is the one searched" do
            before { user.stubs(end_user_viewable_organizations: [stub('organization', id: 99)]) }

            it "does not modify the organization_id filter" do
              user.search_access_control_filters(options)
              assert_equal 99, options[:with][:organization_id]
            end

            it "sets acl_conditions" do
              filters = user.search_access_control_filters(options)
              assert_equal ["requester_id:88493 OR cc_id:88493 OR organization_id:(99) OR _type:(entry OR article)"], filters
            end
          end

          describe "for a user that has many organizations and that includes the one searched" do
            before { user.stubs(end_user_viewable_organizations: [stub('organization', id: 99), stub("organization", id: 100)]) }

            it "does not modify the organization_id filter" do
              user.search_access_control_filters(options)
              assert_equal 99, options[:with][:organization_id]
            end

            it "sets acl_conditions" do
              filters = user.search_access_control_filters(options)
              assert_equal ["requester_id:88493 OR cc_id:88493 OR organization_id:(99 OR 100) OR _type:(entry OR article)"], filters
            end
          end
        end
      end

      describe "with a forum_id filter" do
        let(:options) { {forum_id: 94526} }

        it "restricts to the specified forum" do
          filters = user.search_access_control_filters(options)
          assert_equal ["requester_id:88493 OR cc_id:88493 OR organization_id:(74202) OR _type:(entry OR article)"], filters
          assert_equal [94526], options[:with][:forum_id]
          assert_nil options[:without][:forum_id]
        end
      end

      describe "without a forum_id filter" do
        let(:options) { {} }

        it "is not able to access tag/org restricted forums" do
          filters = user.search_access_control_filters(options)
          assert_equal ["requester_id:88493 OR cc_id:88493 OR organization_id:(74202) OR _type:(entry OR article)"], filters
          assert_nil options[:with][:forum_id]
          assert_equal [58780, 79471], options[:without][:forum_id]
        end
      end
    end
  end

  describe "The forum id hash" do
    before do
      @user = users(:minimum_end_user)
      @user.stubs(:all_tags).returns(["this"])
    end
    describe "with no tag restricted forums" do
      it "is created correctly" do
        assert_equal "{72703=>true, 94526=>true, 41867=>true, 58780=>true, 79471=>true, 97985=>true}", @user.forum_id_hash.inspect
      end
    end

    describe "with one tag restricted forum" do
      before do
        @user.forums[0].stubs(:tag_array).returns(["that"])
      end

      it "is created correctly with one false id value" do
        assert_equal "{72703=>false, 94526=>true, 41867=>true, 58780=>true, 79471=>true, 97985=>true}", @user.forum_id_hash.inspect
      end
    end
  end

  describe "An anonymous user of a non-English account" do
    before do
      @translation = TranslationLocale.last
      @account = accounts(:minimum)
      @account.update_attribute(:translation_locale, @translation)
      @account.forums.update_all(translation_locale_id: @translation.id)
      @user = @account.anonymous_user
    end

    it "sees the public forums for the account's locale" do
      assert_equal 4, @user.accessible_forums_count
    end
  end

  describe "A minimum user" do
    before do
      @user = users(:minimum_end_user)
    end

    it "does not have any tag restricted forums" do
      assert @user.tag_restricted_forum_ids.blank?
    end

    describe "with a tag that matches all forums" do
      before do
        @user.stubs(:all_tags).returns(['beep', 'boop'])
        Forum.any_instance.stubs(:tag_array).returns(['beep'])
      end

      it "shoulds again not have any tag restricted forums" do
        assert @user.tag_restricted_forum_ids.blank?
      end
    end
  end

  describe "#search_access_control_filters" do
    before do
      @user = mock('test_user').extend(Users::Access)
      @user.stubs(
        id: 123,
        agent_restriction?: false,
        is_agent?: false,
        is_end_user?: false,
        is_restricted_agent?: false,
        is_anonymous_user?: false,
        forums: [],
        account: accounts(:minimum),
        account_id: accounts(:minimum).id
      )
      @user.stubs(:can?).with(:access_restricted_content, anything).returns(true)
    end

    describe "Agents that can?(:search, User)" do
      before do
        @user.stubs(:is_admin?).returns(false)
        @user.stubs(:is_agent?).returns(true)
        @user.stubs(:is_restricted_agent?).returns(false)
        @user.stubs(:can?).with(:search, User).returns(true)
      end

      it "does not add any filters" do
        assert_equal([], @user.search_access_control_filters({}))
      end
    end

    describe "Agents that !can?(:search, User)" do
      before do
        @user.stubs(:is_admin?).returns(false)
        @user.stubs(:is_agent?).returns(true)
        @user.stubs(:can?).with(:search, User).returns(false)
      end

      describe "that have unrestricted ticket access" do
        before { @user.stubs(:is_restricted_agent?).returns(false) }

        it "negates User type results in the query options" do
          assert_equal(["-_type:user"], @user.search_access_control_filters({}))
        end
      end

      describe "that have restricted ticket access" do
        before do
          @user.stubs(:is_restricted_agent?).returns(true)
          @user.stubs(:agent_restriction?).with(:organization).returns(false)
          @user.stubs(:agent_restriction?).with(:groups).returns(false)
          @user.stubs(:agent_restriction?).with(:assigned).returns(true)
        end

        it "produces well formed acl_query strings" do
          assert_equal(
            ["-_type:user", "assignee_id:123 OR requester_id:123 OR cc_id:123 OR _type:(entry OR user OR article)"],
            @user.search_access_control_filters({})
          )
        end
      end

      describe "when query source is autocomplete" do
        before { @opts = {source: 'autocomplete'} }

        describe "that can?(:lookup, User)" do
          before { @user.stubs(:can?).with(:lookup, User).returns(true) }

          it "does not negate User type results in the query options" do
            assert_equal([], @user.search_access_control_filters(@opts))
          end
        end

        describe "that !can?(:lookup, User)" do
          before { @user.stubs(:can?).with(:lookup, User).returns(false) }

          it "negates User type results in the query options" do
            assert_equal(["-_type:user"], @user.search_access_control_filters(@opts))
          end
        end
      end
    end
  end
end
