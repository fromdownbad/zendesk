require_relative "../../support/test_helper"
require_relative "../../support/attachment_test_helper"
require 'zendesk/radar_factory'

SingleCov.covered! uncovered: 3

describe 'Users::Merge' do
  fixtures :accounts, :role_settings, :subscriptions, :remote_authentications, :sequences, :ticket_fields, :account_property_sets, :organizations, :organization_memberships
  include DomainEventsHelper
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  after do
    Users::Merge.redis_client.flushall
  end

  def create_ticket(user)
    ticket = Ticket.new(requester: user, account: user.account, subject: 'ticket subject', description: 'testing ticket')
    ticket.will_be_saved_by(user)
    ticket.save!
    ticket
  end

  def merge_and_reload(winner = @winner, loser = @loser)
    user_merge = Users::Merge.new(winner: winner, loser: loser)
    assert user_merge.merge!

    winner.reload
    loser.reload
    user_merge
  end

  def create_forum_entry(_user, _forum, *voters)
    @entry = @forum.entries.create!(
      submitter: @loser,
      account: @account,
      title: 'test entry one',
      text: 'this is the body of the entry.'
    ) do |entry|
      entry.current_user = @loser
    end

    voters.each { |voter| @entry.votes.create!(user: voter, entry: @entry) }
    @entry
  end

  def new_user_twitter_identity(user, options = {})
    twitter_user_id = options[:value] || options[:twitter_user_id] || rand(10**19)
    is_verified     = options[:is_verified] || true
    user.identities << identity = UserTwitterIdentity.new(
      user: user,
      value: twitter_user_id, is_verified: is_verified
    )
    identity
  end

  def create_user_twitter_identity(user, options = {})
    identity = new_user_twitter_identity(user, options)
    identity.save
    identity
  end

  describe "#self.user_key" do
    it "generates a unique redis key for each user" do
      user1 = users(:minimum_end_user)
      user2 = users(:minimum_end_user2)
      user1.name = user2.name

      refute_equal Users::Merge.user_key(user1), Users::Merge.user_key(user2)
    end
  end

  describe "#self.lock_users" do
    it "sets the redis key expiry" do
      user = users(:minimum_end_user)
      Users::Merge.lock_users(user)

      assert Users::Merge.redis_client.exists(Users::Merge.user_key(user))
      assert_operator Users::Merge.redis_client.ttl(Users::Merge.user_key(user)), :>, 0
    end
  end

  describe "#self.unlock_users" do
    it "deletes the redis key" do
      user = users(:minimum_end_user)
      Users::Merge.lock_users(user)
      Users::Merge.unlock_users(user)

      refute Users::Merge.redis_client.exists(Users::Merge.user_key(user))
    end
  end

  describe "Given two users from different accounts" do
    before do
      @loser = FactoryBot.create(:end_user, account: accounts(:minimum))
      @winner = FactoryBot.create(:end_user, account: accounts(:with_groups))
    end

    it "mergings the users should fail" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
    end

    it "should not push domain_events to loser" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
      assert_equal @loser.domain_events.size, 0
    end
  end

  describe "Given two users where one isn't an end user" do
    before do
      account = accounts(:minimum)
      @loser  = FactoryBot.create(:end_user, account: account)
      @winner = FactoryBot.create(:end_user, account: account)
      @winner.stubs(is_end_user?: false)
    end

    it "mergings the users should fail" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
    end

    it "should not push domain_events to loser" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
      assert_equal @loser.domain_events.size, 0
    end
  end

  describe "Given two users where one isn't active" do
    before do
      account = accounts(:minimum)
      @loser  = FactoryBot.create(:end_user, account: account)
      @winner = FactoryBot.create(:end_user, account: account)
      @loser.stubs(is_active?: false)
    end

    it "mergings the users should fail" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
    end

    it "should not push domain_events to loser" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
      assert_equal @loser.domain_events.length, 0
    end
  end

  describe "Given two users where at least one is locked" do
    before do
      account = accounts(:minimum)
      @loser  = FactoryBot.create(:end_user, account: account)
      @winner = FactoryBot.create(:end_user, account: account)
      Users::Merge.stubs(:user_locked?).returns(true)
    end

    it "succesfully merges the two users" do
      assert Users::Merge.new(winner: @winner, loser: @loser).merge!
    end

    it "should push domain_event to loser with correct winner_id" do
      Users::Merge.new(winner: @winner, loser: @loser).merge!
      domain_events = decode_user_events(domain_event_publisher.events)
      domain_events.last.user_merged.winner.id.value.must_equal @winner.id
    end
  end

  describe "Given two users where one is anonymous" do
    before do
      account = accounts(:minimum)
      @loser  = FactoryBot.create(:end_user, account: account)
      @winner = User.new(account: account)
    end

    it "mergings the users should fail" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
    end

    it "should not push domain_events to loser" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
      assert_equal @loser.domain_events.length, 0
    end
  end

  describe "Given two users where the loser is a shared user" do
    before do
      account = accounts(:minimum)
      @loser  = FactoryBot.create(:end_user, account: account)
      @loser.stubs(foreign_agent?: true)
      @winner = FactoryBot.create(:end_user, account: account)
    end

    it "mergings the users should fail" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
    end

    it "should not push domain_events to loser" do
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
      assert_equal @loser.domain_events.length, 0
    end
  end

  describe "Given two users where the loser's account has remote_authentication enabled" do
    before do
      account = accounts(:minimum)

      @loser = FactoryBot.create(:end_user, account: account)
      @loser.stubs(:login_allowed?).with(:remote).returns(true)

      @winner = FactoryBot.create(:end_user, account: account)
    end

    it "mergings the users should fail if the loser has an external ID" do
      @loser.external_id = "12345"
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
    end

    it "should not push domain_events to loser if the loser has an external ID" do
      @loser.external_id = "12345"
      refute Users::Merge.new(winner: @winner, loser: @loser).merge!
      assert_equal @loser.domain_events.length, 0
    end

    it "mergings the users should succeed if the loser has no external ID" do
      assert Users::Merge.new(winner: @winner, loser: @loser).merge!
    end

    it "should push domain_event to loser with correct winner_id if the loser has no external ID" do
      Users::Merge.new(winner: @winner, loser: @loser).merge!
      domain_events = decode_user_events(domain_event_publisher.events, :user_merged)
      assert_equal domain_events.length, 1
      domain_events.first.user_merged.winner.id.value.must_equal @winner.id
    end
  end

  describe "Given two users" do
    before do
      @account = accounts(:minimum)
      @loser = FactoryBot.create(:end_user, name: 'The Loser', account: @account)
      @winner = FactoryBot.create(:end_user, name: 'The Winner', account: @account)
    end

    describe "merging into a winner with no identities" do
      before do
        UserIdentity.without_arsi.delete_all
        UserEmailIdentity.create!(user: @loser, value: "userA@example.com", is_verified: true)
        UserEmailIdentity.create!(user: @loser, value: "userA.notverified@example.com")
        UnverifiedEmailAddress.create!(user: @loser, email: 'userA.unverified@example.com')
        merge_and_reload
      end

      it "transfers all identities to winners" do
        assert_equal 0, @loser.identities.size
        assert_equal 2, @winner.identities.size
        assert_equal 0, @loser.unverified_email_addresses.size
        assert_equal 1, @winner.unverified_email_addresses.size
      end
    end

    describe "each with identities" do
      before do
        UserIdentity.without_arsi.delete_all # fixture cleanup

        create_user_twitter_identity(@loser, value: '12345')
        @winner.identities << UserEmailIdentity.create!(:user => @winner, 'value' => 'two@example.com')
      end

      describe "When merging one into two" do
        before do
          merge_and_reload
        end

        should_change("User::Merge.count(:all)", by: 1) { Users::Merge.count(:all) }
        it "has all identities in the winner" do
          assert_equal(0, @loser.identities.size)
          assert_equal(2, @winner.identities.size)
        end
      end

      describe "When merging with an existing identity" do
        before do
          @identity = new_user_twitter_identity(@winner, value: '12345')
        end

        describe "pre job execution" do
          before do
            UserMergeJob.expects(:enqueue).with(@winner.account_id, @winner.id, @loser.id)
            Users::Merge.merge_with_existing_identity(@winner, @identity)
          end

          it "does not have lost that identity on the loser" do
            assert_equal(1, @loser.identities(true).size)
          end
        end

        describe "post job execution" do
          before do
            Users::Merge.merge_with_existing_identity(@winner, @identity)
          end

          it "no longer has the identity on the loser" do
            assert_equal(0, @loser.identities(true).size)
          end
        end
      end

      describe "When checking if a merge will happen" do
        before do
          @identity = new_user_twitter_identity(@winner, value: '12345')
        end

        it "is true" do
          assert Users::Merge.existing_identity(@winner, @identity).present?
        end
      end
    end

    describe "each watching the same ticket" do
      before do
        Watching.without_arsi.delete_all
        @ticket = create_ticket(@winner)
        @watching_one = @loser.watchings.create!(source: @ticket, account: @account)
        @watching_two = @winner.watchings.create!(source: @ticket, account: @account)
      end

      describe "When merging one into two" do
        before do
          merge_and_reload
        end

        it "Thens one should have its watching removed" do
          assert_equal(0, @loser.watchings.size)
          assert_equal(1, @winner.watchings.size)
        end
      end
    end

    describe "with the loser watching a ticket" do
      before do
        Watching.without_arsi.delete_all
        @ticket = create_ticket(@loser)
        @watching_one = @loser.watchings.create!(source: @ticket, account: @account)
      end

      describe "When merging one into two" do
        before do
          merge_and_reload
        end

        it "Thens one should have its watching moved" do
          assert_equal(0, @loser.watchings.size)
          assert_equal(1, @winner.watchings.size)
        end
      end
    end

    describe "each with tickets" do
      around do |test|
        with_in_memory_archive_router(test)
      end

      before do
        Ticket.without_arsi.delete_all

        @ticket1 = create_ticket(@loser)

        @closed_ticket = create_ticket(@loser)
        @closed_ticket.status_id = StatusType.CLOSED
        @closed_ticket.force_status_change = true
        @closed_ticket.will_be_saved_by(@closed_ticket.submitter)
        @closed_ticket.save!
        assert @closed_ticket.closed?

        @ticket3 = create_ticket(@winner)
        @archived_ticket = create_ticket(@loser)
        @archived_ticket.archive!
      end

      describe "When merging one into two" do
        before do
          merge_and_reload
        end

        it "moves the loser's tickets (but not events), including the closed one" do
          assert_equal(0, @loser.tickets.count_with_archived)
          assert_equal(4, @winner.tickets.count_with_archived)

          @ticket1.reload
          old_event_owners = @ticket1.events[0..-3].map(&:author).uniq
          assert_equal(1, old_event_owners.size)
          assert_equal(@loser, old_event_owners.first)

          @closed_ticket.reload
          assert_equal(@winner, @closed_ticket.requester)
        end

        it "moves the loser's archived tickets 123" do
          updated_ticket = Ticket.find(@archived_ticket.id)
          assert_equal(@winner.id, updated_ticket.requester_id)

          assert_equal 0, TicketArchiveStub.where(requester_id: @loser.id, account_id: @loser.account_id).count(:all)
          assert_equal 1, TicketArchiveStub.where(requester_id: @winner.id, account_id: @winner.account_id).count(:all)
        end

        it "does not update assignee field for a closed ticket" do
          merge_events = @closed_ticket.events.select { |event| event['via_id'] == Zendesk::Types::ViaType.MERGE }
          assert_equal [], merge_events.select { |merge_event| merge_event['value_reference'] == 'assignee_id' }
        end
      end

      describe "When merging one into two with audits" do
        describe_with_arturo_enabled :user_merge_job_audit do
          before do
            merge_and_reload
          end

          it "adds a requester change audit to non-archived tickets that were moved" do
            @ticket1.reload
            assert_equal ViaType.USER_MERGE, @ticket1.audits.last.via_id
            assert_equal @winner.id.to_s, @ticket1.audits.last.events.last.value

            @closed_ticket.reload
            assert_equal ViaType.USER_MERGE, @closed_ticket.audits.last.via_id
            assert_equal @winner.id.to_s, @closed_ticket.audits.last.events.last.value

            updated_ticket = Ticket.find(@archived_ticket.id)
            assert_not_equal ViaType.USER_MERGE, updated_ticket.audits.last.via_id
          end
        end
      end

      describe "When merging one into two with tickets in batches" do
        describe_with_arturo_enabled :user_merge_move_tickets_in_batches do
          before do
            Users::Merge::DEFAULT_BATCH = 1
            merge_and_reload
          end

          it "sets the ticket's requester_id and submitter_id to the winner" do
            @ticket1.reload
            @closed_ticket.reload
            assert_equal(@winner.id, @ticket1.requester_id)
            assert_equal(@winner.id, @ticket1.submitter_id)
            assert_equal(@winner.id, @closed_ticket.requester_id)
            assert_equal(@winner.id, @closed_ticket.submitter_id)
          end

          it "sets a different generated_timestamp value for each batch of tickets" do
            refute_equal @ticket1.reload.generated_timestamp, @closed_ticket.reload.generated_timestamp
          end
        end

        describe_with_arturo_disabled :user_merge_move_tickets_in_batches do
          before do
            Users::Merge::DEFAULT_BATCH = 1
            merge_and_reload
          end

          it "sets the ticket's requester_id and submitter_id to the winner" do
            @ticket1.reload
            @closed_ticket.reload
            assert_equal(@winner.id, @ticket1.requester_id)
            assert_equal(@winner.id, @ticket1.submitter_id)
            assert_equal(@winner.id, @closed_ticket.requester_id)
            assert_equal(@winner.id, @closed_ticket.submitter_id)
          end
        end
      end
    end

    describe "with the winner or loser collaborating on tickets" do
      before do
        @other_user = FactoryBot.create(:end_user, account: @account)
        Ticket.without_arsi.delete_all
        @ticket1 = create_ticket(@other_user)
        @ticket2 = create_ticket(@winner)
        @ticket3 = create_ticket(@loser)

        @ticket1.collaborations.create!(account: @account, user: @loser)
        @ticket2.collaborations.create!(account: @account, user: @loser)
        @ticket3.collaborations.create!(account: @account, user: @winner)
      end

      describe "When the loser is merge into the winner" do
        before do
          merge_and_reload
        end

        it "thens have one collaboration for the other ticket" do
          @ticket1.reload
          assert_equal(1, @ticket1.collaborations.size)
          assert_equal(@winner, @ticket1.collaborators.first)

          # and the other collaboration should be deleted
          @ticket2.reload
          assert_equal(0, @ticket2.collaborations.size)

          @ticket3.reload
          assert_equal(0, @ticket3.collaborations.size)
        end

        it "does not add CC errors on ticket update" do
          @ticket3.reload
          @ticket3.status_id = StatusType.OPEN
          @ticket3.will_be_saved_by(@ticket3.submitter)

          assert @ticket3.save!
          refute_includes @ticket3.errors.full_messages, 'CC cannot be ticket requester'
        end
      end

      describe 'and one of those tickets has been deleted' do
        before do
          @ticket1.will_be_saved_by(@other_user, via_id: Zendesk::Types::ViaType.WEB_FORM)
          @ticket1.soft_delete!
          merge_and_reload
        end

        it 'does not bother merging collaborators on the deleted tickets' do
          refute @ticket1.collaborators.include?(@winner)
        end
      end
    end

    describe "each with entries, posts and votes" do
      before do
        # fixture cleanup
        Vote.without_arsi.delete_all
        Post.without_arsi.delete_all
        Entry.without_arsi.delete_all
        Forum.without_arsi.delete_all

        @forum = @account.forums.create!(name: 'test forum',
                                         visibility_restriction: VisibilityRestriction::EVERYBODY)
        # The loser's vote should be deleted
        @entry_one = create_forum_entry(@loser, @forum, @loser, @winner)
        @entry_one.posts.create!(user: @loser, body: 'this is my post')

        # The loser's vote should be moved
        @entry_two = create_forum_entry(@winner, @forum, @loser)

        merge_and_reload
      end

      it "moves the loser's entries, posts and votes to the winner " do
        assert_equal(0, @loser.entries.size)
        assert_equal(2, @winner.entries.size)

        @winner.entries.each do |entry|
          assert_equal(1, entry.votes(true).size)
        end

        @entry_one.reload
        assert_equal(@entry_one.posts.first.user, @winner)
      end
    end

    describe "with the loser having an attachment" do
      before do
        without_foreign_key_checks { Attachment.without_arsi.delete_all }
        @attachment = create_attachment(File.join(Rails.root, 'test/files/small.png'), @loser)
      end

      describe "When the loser is merged into the winner" do
        before do
          merge_and_reload
        end

        it "changes the ownership of the Attachment" do
          @attachment.reload
          assert_equal(@winner, @attachment.author)
        end
      end
    end

    describe "with the loser having suspended tickets" do
      before do
        @ticket = @loser.suspended_tickets.create!(
          account: @loser.account, content: 'content',
          from_name: 'from name', from_mail: 'from@mail.com', cause: 'whatever'
        )
      end

      describe "When merging the loser into the winner" do
        before do
          merge_and_reload
        end

        it "Thens the suspended ticket should move to the winner" do
          @ticket.reload
          assert_equal(@winner, @ticket.author)
        end
      end
    end

    describe "When the loser is merged into the winner" do
      before do
        @user_merge = Users::Merge.new(winner: @winner, loser: @loser)
      end

      it "Thens delete the loser properly" do
        @user_merge.loser.expects(:delete!).at_least_once
        assert @user_merge.merge!
      end

      it "Thens mark the loser as inactive" do
        assert @user_merge.merge!
        @loser.reload
        refute @loser.is_active
      end
    end

    describe ".validate_merge" do
      let(:result) { Users::Merge.validate_merge(winner, loser) }

      describe "when arguments are the same user" do
        let(:winner) { 1 }
        let(:loser) { 1 }

        it "returns :winner_is_loser" do
          assert_equal :winner_is_loser, result
        end
      end

      describe "when users are from different accounts" do
        let(:winner) { stub(account_id: 1) }
        let(:loser) { stub(account_id: 2) }

        it "returns :account_mismatch" do
          assert_equal :account_mismatch, result
        end
      end
    end

    describe ".validate_winner" do
      let(:result) { Users::Merge.validate_winner(winner) }

      describe "when user is inactive" do
        let(:winner) { users(:inactive_user) }

        it "returns :winner_invalid" do
          assert_equal :winner_invalid, result
        end
      end

      describe "when user is valid" do
        let(:winner) { users(:minimum_end_user) }

        it "returns :winner_valid" do
          assert_equal :winner_valid, result
        end
      end

      describe "when user is an agent" do
        let(:winner) { users(:minimum_agent) }

        it "returns :winner_non_enduser" do
          assert_equal :winner_non_enduser, result
        end
      end

      describe "when user has a blacklisted email identity" do
        let(:winner) { users(:minimum_end_user) }

        before do
          identity = UserEmailIdentity.new(account: winner.account, user: winner, value: "noreply@zopim.com")
          identity.save(validate: false)
        end

        it "returns :winner_email_blacklisted" do
          assert_equal :winner_email_blacklisted, result
        end
      end
    end

    describe ".validate_loser" do
      let(:result) { Users::Merge.validate_loser(loser) }

      describe "when user is inactive" do
        let(:loser) { stub(is_anonymous_user?: false, is_active?: false) }

        it "returns :loser_invalid" do
          assert_equal :loser_invalid, result
        end
      end

      describe "when user is valid" do
        let(:loser) { users(:minimum_end_user) }

        it "returns :loser_valid" do
          assert_equal :loser_valid, result
        end
      end

      describe "when user is an agent" do
        let(:loser) { users(:minimum_agent) }

        it "returns :loser_non_enduser" do
          assert_equal :loser_non_enduser, result
        end
      end

      describe "when user has a blacklisted email identity" do
        let(:loser) { users(:minimum_end_user) }

        before do
          identity = UserEmailIdentity.new(account: loser.account, user: loser, value: "noreply@zopim.com")
          identity.save(validate: false)
        end

        it "returns :loser_email_blacklisted" do
          assert_equal :loser_email_blacklisted, result
        end
      end

      describe "when user has sso enabled and an external id" do
        let(:loser) { users(:minimum_end_user) }
        before do
          loser.external_id = "42"
          loser.stubs(:login_allowed?).with(:remote).returns(true)
        end

        it "returns :loser_has_sso_external_id" do
          assert_equal :loser_has_sso_external_id, result
        end
      end
    end

    describe "#can_be_merged_as_loser?" do
      let(:valid_user) { users(:minimum_end_user) }

      it "returns true for a valid loser" do
        assert valid_user.can_be_merged_as_loser?
      end
    end

    describe "#can_be_merged_as_winner?" do
      let(:valid_user) { users(:minimum_end_user) }

      it "returns true for a valid winner" do
        assert valid_user.can_be_merged_as_winner?
      end
    end

    describe "#can_be_merged_into?" do
      let(:user) { users(:minimum_end_user) }
      let(:winner) { users(:minimum_end_user2) }

      it "returns true for a valid user and winner" do
        assert user.can_be_merged_into?(winner)
      end
    end

    describe "User validity helpers" do
      let(:valid_user) { users(:minimum_end_user) }

      it "both return true for a valid loser" do
        assert Users::Merge.valid_loser?(valid_user)
        assert Users::Merge.valid_winner?(valid_user)
      end
    end

    describe "When validating and merging" do
      it "returns :merge_complete and schedule the job when valid" do
        UserMergeJob.expects(:enqueue).with(@winner.account_id, @winner.id, @loser.id)
        assert_equal(:merge_complete, Users::Merge.validate_and_merge(@winner, @loser))
      end

      it "returns :loser_invalid and not schedule the job when user is invalid" do
        @loser.expects(:is_active?).returns(false)
        UserMergeJob.expects(:enqueue).never
        assert_equal(:loser_invalid, Users::Merge.validate_and_merge(@winner, @loser))
      end

      it "returns false if an existing merge is in progress with the users" do
        UserMergeJob.expects(:enqueue).with(@winner.account_id, @winner.id, @loser.id).once
        assert_equal(:merge_complete, Users::Merge.validate_and_merge(@winner, @loser))
        assert_equal(false, Users::Merge.validate_and_merge(@winner, @loser))
      end
    end

    describe "When validating and merging tickets immediately" do
      before do
        Ticket.without_arsi.delete_all
        @ticket1 = create_ticket(@loser)
        @ticket2 = create_ticket(@winner)
      end

      it "returns :merge_complete and schedule the job" do
        UserMergeJob.expects(:enqueue).with(@winner.account_id, @winner.id, @loser.id)
        assert_equal(:merge_complete, Users::Merge.validate_and_merge_with_tickets_now(@winner, @loser))
      end

      it "returns false if an existing merge is in progress with the users" do
        UserMergeJob.expects(:enqueue).with(@winner.account_id, @winner.id, @loser.id).once
        assert_equal(:merge_complete, Users::Merge.validate_and_merge(@winner, @loser))
        assert_equal(false, Users::Merge.validate_and_merge(@winner, @loser))
      end

      it "moves the tickets immediately" do
        UserMergeJob.expects(:enqueue).with(@winner.account_id, @winner.id, @loser.id)
        Users::Merge.validate_and_merge_with_tickets_now(@winner, @loser)

        @loser.reload
        @winner.reload

        assert_equal(0, @loser.tickets.size)
        assert_equal(2, @winner.tickets.size)
      end
    end

    describe "When the loser has been the winner of a previous merge" do
      before do
        old_user   = FactoryBot.create(:end_user, name: 'Old User', account: accounts(:minimum))
        @old_merge = Users::Merge.create!(winner: @loser, loser: old_user)
      end

      describe "When the loser is merged into the winner" do
        before do
          @user_merge = Users::Merge.new(winner: @winner, loser: @loser)
          assert @user_merge.merge!
        end

        it "Updates the previous merge to flatten the users" do
          @old_merge.reload
          assert_equal(@winner, @old_merge.winner)
          assert_equal(@loser.id, @old_merge.original_winner_id)
        end
      end
    end

    describe "#remove_zopim_agent_records" do
      let(:winner) { users(:minimum_end_user) }
      let(:loser) { users(:minimum_end_user2) }
      let(:zopim_identity_1) { stub }
      let(:zopim_identity_2) { stub }

      before do
        winner.stubs(:zopim_identity).returns(zopim_identity_1)
        loser.stubs(:zopim_identity).returns(zopim_identity_2)
        winner.expects(:remove_zopim_agent_record!)
        loser.expects(:remove_zopim_agent_record!)
        zopim_identity_1.stubs(:disable!)
        zopim_identity_2.stubs(:disable!)
        zopim_identity_1.stubs(:destroyed?).returns(true)
        zopim_identity_2.stubs(:destroyed?).returns(true)
        @user_merge = Users::Merge.new(winner: winner, loser: loser)
      end

      it "removes the zopim_agent_record" do
        assert @user_merge.merge!
      end
    end

    describe "#move_phone_number" do
      before do
        @phone_1 = "+12223334444"
        @phone_2 = "+13334445555"
      end

      describe "when both winner and loser have phone identities" do
        before do
          @winner.update_attribute(:phone, @phone_1)
          @winner.identities << UserPhoneNumberIdentity.create(account: @account, user: @winner, value: @phone_1)

          @loser.update_attribute(:phone, @phone_2)
          @loser.identities << UserPhoneNumberIdentity.create(account: @account, user: @loser, value: @phone_2)
        end

        it "moves the loser's phone identity to the winner" do
          loser_identity_id = UserPhoneNumberIdentity.find_by_user_id(@loser).id
          merge_and_reload
          moved_identity = UserPhoneNumberIdentity.find_by_id(loser_identity_id)
          assert_equal 2, @winner.identities.phone.count(:all)
          assert_equal @winner.id, moved_identity.user.id
        end
      end

      describe "when loser has shared number" do
        before do
          @winner.update_attribute(:phone, @phone_1)
          @winner.identities << UserPhoneNumberIdentity.create(account: @account, user: @winner, value: @phone_1)
          @loser.update_attribute(:phone, @phone_2)
        end

        it "moves the loser's shared number to the winner" do
          merge_and_reload
          assert_equal 1, @winner.identities.phone.count(:all)
          assert_equal @winner.phone, @phone_2
        end
      end

      describe "when winner has e-mail and phone identity and loser has phone identity" do
        before do
          @winner.update_attribute(:phone, @phone_1)
          @winner.identities << UserPhoneNumberIdentity.create(account: @account, user: @winner, value: @phone_1)

          @loser.update_attribute(:phone, @phone_2)
          @loser.identities.delete_all
          @loser.identities << UserPhoneNumberIdentity.create(account: @account, user: @loser, value: @phone_2)
        end

        it "preserves the winner's phone identity, but uses loser's shared number" do
          old_winner_identity = UserPhoneNumberIdentity.find_by_user_id(@winner)
          merge_and_reload
          new_winner_identity = UserPhoneNumberIdentity.find_by_user_id(@winner)
          assert_equal(old_winner_identity.value, new_winner_identity.value)
          assert_equal(@phone_2, @winner.phone)
        end

        it "preserves both phone identities" do
          merge_and_reload
          assert_equal 2, @winner.identities.phone.count(:all)
        end
      end

      describe "when the winner has a phone identity and loser does not" do
        before do
          @winner.update_attribute(:phone, @phone_1)
          @winner.identities << UserPhoneNumberIdentity.create(account: @account, user: @winner, value: @phone_1)
        end

        it "preserves the winner's phone identity, but uses loser's (non-existent) shared number" do
          old_winner_identity = UserPhoneNumberIdentity.find_by_user_id(@winner)
          merge_and_reload
          new_winner_identity = UserPhoneNumberIdentity.find_by_user_id(@winner)
          assert_equal(old_winner_identity.value, new_winner_identity.value)
          assert_nil @winner.phone
        end
      end

      describe "when the winner has a shared number and loser has a phone identity" do
        before do
          @winner.update_attribute(:phone, @phone_1)
          @loser.identities << UserPhoneNumberIdentity.create(account: @account, user: @loser, value: @phone_2)
        end

        it "gives the winner the loser's phone identity, but leaves winner's shared number" do
          old_loser_identity = UserPhoneNumberIdentity.find_by_user_id(@loser)
          merge_and_reload
          new_winner_identity = UserPhoneNumberIdentity.find_by_user_id(@winner)
          assert_equal 1, @winner.identities.phone.count(:all)
          assert_equal(new_winner_identity.value, old_loser_identity.value)
          assert_equal(@phone_1, @winner.phone)
        end
      end

      describe "when both winner and loser have phone numbers" do
        before do
          @winner.update_attribute(:phone, @phone_1)
          @loser.update_attribute(:phone, @phone_2)
        end

        it "preserves the winner's phone" do
          old_winner_phone = @winner.phone
          merge_and_reload
          assert_equal(@winner.phone, old_winner_phone)
        end
      end

      describe "when the winner has a phone number and loser has none" do
        before do
          @winner.update_attribute(:phone, @phone_1)
        end

        it "preserves the winner's phone" do
          old_winner_phone = @winner.phone
          merge_and_reload
          assert_equal(@winner.phone, old_winner_phone)
        end
      end

      describe "when the winner has nothing" do
        it "uses the loser's shared number if both are present" do
          @loser.update_attribute(:phone, @phone_2)
          @loser.identities << UserPhoneNumberIdentity.create(account: @account, user: @loser, value: @phone_1)
          old_loser_identity = UserPhoneNumberIdentity.find_by_user_id(@loser)
          merge_and_reload
          new_winner_identity = UserPhoneNumberIdentity.find_by_user_id(@winner)
          assert_not_nil(new_winner_identity)
          assert_equal(new_winner_identity.value, old_loser_identity.value)
          assert_equal(@phone_2, @winner.phone)
        end

        it "uses the loser's phone number if present" do
          @loser.update_attribute(:phone, "anything here")
          old_loser_phone = @loser.phone
          merge_and_reload
          assert_equal(@winner.phone, old_loser_phone)
        end
      end
    end

    describe "#move_organizations" do
      let(:ticket_with_loser_organiation) do
        ticket = create_ticket(@loser)
        loser_org = @loser.organization_id
        ticket.update_column(:organization_id, loser_org)
        ticket
      end

      before do
        @winner.update_attributes!(organization: organizations(:minimum_organization2)) # do not use the same ...
        OrganizationDomain.destroy_all # avoid default organization assignment callbacks
        OrganizationEmail.destroy_all # avoid default organization assignment callbacks
      end

      it "does not move when both have nothing" do
        @winner.update_attributes!(organization: nil)
        @loser.update_attributes!(organization: nil)

        merge_and_reload

        assert_nil @loser.organization_id
        assert_nil @winner.organization_id
        assert_equal 0, @loser.organization_memberships.size
        assert_equal 0, @winner.organization_memberships.size
      end

      it "moves 1 from loser to winner with nothing" do
        @winner.update_attributes!(organization: nil)

        merge_and_reload

        assert @winner.organization_id
        assert_equal 1, @winner.organization_memberships.size
      end

      it "does not overwrite existing" do
        merge_and_reload

        refute_equal @winner.organization_id, @loser.organization_id
        assert_equal [@winner.organization_id], @winner.organization_memberships.map(&:organization_id)
      end

      describe "with multiple organizations" do
        let(:org1) { organizations(:minimum_organization1).id }
        let(:org2) { organizations(:minimum_organization2).id }
        let(:org3) { organizations(:minimum_organization3).id }

        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
          @loser.organization_memberships.create!(organization_id: org3)
        end

        it "moves many to winner with nothing" do
          @winner.update_attributes!(organization: nil)
          merge_and_reload

          assert_equal org1, @winner.organization_id
          assert_equal(
            [[org1, true], [org3, false]].sort_by(&:first),
            @winner.organization_memberships.map { |om| [om.organization_id, om.default?] }.sort_by(&:first)
          )
        end

        it "moves many to winner with some" do
          merge_and_reload

          assert_equal org2, @winner.organization_id
          assert_equal(
            [[org1, false], [org2, true], [org3, false]].sort_by(&:first),
            @winner.organization_memberships.map { |om| [om.organization_id, om.default?] }.sort_by(&:first)
          )
        end

        it "moves many to winner with same without duplicates" do
          @loser.organization_memberships.create!(organization: organizations(:minimum_organization2))
          merge_and_reload

          assert_equal org2, @winner.organization_id
          assert_equal(
            [[org1, false], [org2, true], [org3, false]].sort_by(&:first),
            @winner.organization_memberships.map { |om| [om.organization_id, om.default?] }.sort_by(&:first)
          )
        end

        it "keeps ticket organization when moving" do
          ticket = ticket_with_loser_organiation
          merge_and_reload
          assert_equal org1, ticket.reload.organization_id
        end
      end
    end
  end
end
