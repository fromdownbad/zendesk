require_relative '../../support/test_helper'

SingleCov.covered!

describe Users::Properties do
  fixtures :accounts, :users, :user_settings, :user_texts, :rules

  let(:account)  { accounts(:minimum) }
  let!(:user)    { users(:minimum_agent) }
  let!(:setting) { user_settings(:minimum_agent_1) }

  describe 'the settings property_set' do
    it 'sets up `#settings` for the User model' do
      assert user.settings.show_welcome_dialog, 'defined settings must return a value'
    end
  end

  describe 'the texts property_set' do
    it 'sets up `#texts` for the User model' do
      assert_equal user.texts.shared_views_order, []
    end
  end

  describe 'associations' do
    it 'associates UserSetting with an account' do
      assert_equal user.account, user.settings.first.account
    end

    it 'associates UserSetting with a user' do
      assert_equal user, user.settings.first.user
    end

    it 'associates UserText with an account' do
      assert_equal user.account, user.texts.first.account
    end

    it 'associates UserText with a user' do
      assert_equal user.account, user.texts.first.account
    end
  end

  describe '#set_new_settings: When a new user is created' do
    def generate_user
      user.dup.tap do |u|
        u.id    = nil
        u.email = 'agent+new@zendesk.com'
        u.name  = 'My New User'
      end
    end

    it 'sets initial setting values for show_filter_options_tooltip' do
      new_user = generate_user

      assert(new_user.settings.show_filter_options_tooltip)

      new_user.save!

      assert_equal false, new_user.settings.show_filter_options_tooltip
    end

    it 'sets initial setting values for first comment private (FCP)' do
      new_user = generate_user
      new_user.save!

      refute_equal new_user.account.is_first_comment_private_enabled,
        new_user.settings.show_first_comment_private_tooltip
    end

    it 'turns off conflicting onboarding experiences when polaris is enabled' do
      Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation)
      new_user = generate_user
      new_user.account.settings.enable(:polaris)
      new_user.account.stubs(has_active_suite?: true)
      new_user.account.settings.save
      assert new_user.account.has_polaris?, "Account should have had polaris enabled."

      new_user.save!

      assert new_user.settings.show_agent_workspace_onboarding, "New user should have had `show_agent_workspace_onboarding` toggled to true."
      assert_equal false, new_user.settings.show_zero_state_tour_ticket
    end

    describe_with_arturo_disabled(:polaris) do
      before { user.account.settings.polaris = false }

      it "does NOT change onboarding experience values when polaris is disabled" do
        new_user = generate_user
        old_polaris_value = new_user.settings.show_agent_workspace_onboarding
        old_tour_value = new_user.settings.show_zero_state_tour_ticket

        new_user.save!

        assert_equal old_polaris_value, new_user.settings.show_agent_workspace_onboarding
        assert_equal old_tour_value, new_user.settings.show_zero_state_tour_ticket
      end
    end
  end

  describe Users::Properties::SharedViewsOrderValidator do
    let!(:personal_view) { rules(:view_for_minimum_agent) }

    describe '#validate' do
      it 'respects max_views_for_display on the account' do
        user.texts.shared_views_order = (1..(user.account.max_views_for_display + 1)).to_a

        refute_valid user
      end

      it 'only allows shared views in the setting' do
        user.texts.shared_views_order = [personal_view.id]

        refute_valid user
      end
    end
  end
end
