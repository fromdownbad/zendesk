require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 5

describe Users::OnboardingSupport do
  fixtures :accounts, :users, :brands, :account_settings

  let(:end_user) { users(:minimum_end_user) }
  let(:admin_user) { users(:minimum_admin) }

  before do
    Facebook::Page.where(account_id: end_user.account_id).delete_all
    MonitoredTwitterHandle.where(account_id: end_user.account_id).delete_all
    Voice::Core::SubAccount.where(account_id: end_user.account_id).delete_all
    end_user.account.recipient_addresses.delete_all
  end

  describe "#get_channel_instances" do
    it 'uses the account association when available' do
      end_user.account.stubs(:account_channel)
      end_user.account.expects(:account_channel)
      end_user.send(:get_channel_instances, :account_channel)
    end

    it 'delegates to brand attributes when account method does not exist' do
      end_user.account.default_brand.expects(:brand_channel)
      end_user.send(:get_channel_instances, :brand_channel)
    end

    it 'returns an array' do
      end_user.account.stubs(:account_channel).returns('singleton')
      assert end_user.send(:get_channel_instances, :account_channel).is_a?(Array)
    end
  end

  describe '#twitter_handles_active?' do
    it 'returns false if twitter integration is inactive but some options are enabled' do
      FactoryBot.create(:monitored_twitter_handle, :favoriting_enabled)
      refute end_user.twitter_handles_active?
    end

    it 'returns false if twitter integration is active but none of the options are enabled' do
      monitored_twitter_handle = FactoryBot.create(:monitored_twitter_handle, :active)
      monitored_twitter_handle.direct_messages_enabled_at = nil
      monitored_twitter_handle.mention_autoconversion_enabled_at = nil
      monitored_twitter_handle.save
      refute end_user.twitter_handles_active?
    end

    it 'return true if favoriting is enabled' do
      FactoryBot.create(:monitored_twitter_handle, :active, :favoriting_enabled)
      assert end_user.twitter_handles_active?
    end

    it 'return true if dm is enabled' do
      FactoryBot.create(:monitored_twitter_handle, :active, :dm_enabled)
      assert end_user.twitter_handles_active?
    end

    it 'return true if mentions is enabled' do
      FactoryBot.create(:monitored_twitter_handle, :active, :mention_enabled)
      assert end_user.twitter_handles_active?
    end
  end

  describe '#facebook_pages_active?' do
    it 'returns false if facebook integration is inactive but some options are enabled' do
      FactoryBot.create(:facebook_page, :include_posts_by_page)
      refute end_user.facebook_pages_active?
    end

    it 'returns false if facebook integration is active but none of the options are enabled' do
      FactoryBot.create(:facebook_page, :active)
      refute end_user.facebook_pages_active?
    end

    it 'return true if page posts is enabled' do
      FactoryBot.create(:facebook_page, :active, :include_posts_by_page)
      assert end_user.facebook_pages_active?
    end

    it 'return true if wall posts is enabled' do
      FactoryBot.create(:facebook_page, :active, :include_wall_posts)
      assert end_user.facebook_pages_active?
    end

    it 'return true if messages is enabled' do
      FactoryBot.create(:facebook_page, :active, :include_messages)
      assert end_user.facebook_pages_active?
    end
  end

  describe '#voice_sub_account_active?' do
    it 'returns false if voice subscription exists and is inactive' do
      FactoryBot.create(:voice_sub_account, account: end_user.account)
      refute end_user.voice_sub_account_active?
    end

    it 'returns true if voice subscription exists and is active' do
      FactoryBot.create(:voice_sub_account, :active, account: end_user.account)
      assert end_user.voice_sub_account_active?
    end
  end

  describe '#onboarding_config_string' do
    it 'loads support configs' do
      configs = admin_user.send(:onboarding_config_string, type: 'support')
      assert_match /invite_team/, configs
    end
  end

  describe '#onboarding' do
    describe 'when type is support' do
      describe 'admin account has access to support v2 onboarding' do
        before do
          @support_task_names = %w[
            setup_support_email
            invite_team
            how_ticketing_works
            enable_features
          ]
        end

        it 'returns support tasks' do
          tasks = admin_user.onboarding('support')
          assert_equal @support_task_names, tasks.map { |task| task['name'] }
        end
      end
    end
  end

  describe '#invited_team?' do
    it 'returns false if there is only 1 agent/admin/owner' do
      admin_user.account.stubs(:agents).returns([1])
      refute admin_user.invited_team?
    end

    it 'returns true if there is more than 1 agent/admin/owner' do
      admin_user.account.stubs(:agents).returns([1, 2])
      assert admin_user.invited_team?
    end
  end
end
