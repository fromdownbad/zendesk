require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 9

describe Users::Photo do
  fixtures :accounts, :users, :user_identities, :account_property_sets, :subscriptions, :organizations, :photos

  let(:user) { users(:minimum_end_user) }

  before do
    SyncChatAgentAvatarJob.stubs(:enqueue)
  end

  describe "#remote_photo_url=" do
    describe "with a valid URL" do
      let(:url) { "http://example.com/foo.png" }
      before { user.remote_photo_url = url }

      describe "that is not currently the user's photo original_url" do
        before do
          FetchProfileImageJob.expects(:enqueue).with(user.account_id, user.id, url)
          assert_not_equal user.remote_photo_url, user.photo.try(:original_url)
        end

        it "enqueues job to fetch the remote image" do
          user.save!
        end
      end

      describe "that is currently the user's photo original_url" do
        before do
          FetchProfileImageJob.expects(:enqueue).never
          user.photo = FactoryBot.create(:photo, filename: "large_1.png", original_url: url)
        end

        it "does not enqueue job to fetch the remote image" do
          user.save!
        end
      end
    end

    describe "with a valid utf-8 URL" do
      let(:url) { "http://a3.twimg.com/profile_images/79072341/l_1b82927_-_копия.png" }

      before do
        user.remote_photo_url = url
      end

      it "enqueues job to fetch the remote image" do
        FetchProfileImageJob.expects(:enqueue).with(user.account_id, user.id, url)

        user.save!
      end

      describe "when capability 'has_sync_support_agent_avatar_to_chat' is enabled" do
        before do
          user.account.stubs(:has_sync_support_agent_avatar_to_chat?).returns(true)
        end

        describe "and user is an agent with a chat entitlement" do
          before do
            user.stubs(:is_agent?).returns(true)
            user.settings.chat_entitlement = 'admin'
            user.save!
          end

          it "enqueues agent chat avatar synchronization" do
            SyncChatAgentAvatarJob.expects(:enqueue).with(user.account_id, user.id)

            user.save!
          end
        end
      end
    end

    describe "with an invalid URL" do
      before do
        FetchProfileImageJob.expects(:enqueue).never
        user.remote_photo_url = "bad url"
      end

      it "does not enqueue job to fetch the remote image" do
        assert_raises(ActiveRecord::RecordInvalid) { user.save! }
      end
    end

    describe "with a URL that has a host but no path" do
      before do
        FetchProfileImageJob.expects(:enqueue).never
        user.remote_photo_url = "http://zendesk.com"
      end

      it "does not enqueue job to fetch the remote image" do
        assert_raises(ActiveRecord::RecordInvalid) { user.save! }
      end
    end

    describe "with a nil URL" do
      before do
        FetchProfileImageJob.expects(:enqueue).never
        user.remote_photo_url = nil
      end

      it "does not enqueue job to fetch the remote image" do
        user.save!
      end
    end

    describe "with a URL that is too damn long" do
      it "does not enqueue job to fetch the remote image" do
        FetchProfileImageJob.expects(:enqueue).never
        user.remote_photo_url = "http://zendesk.com/#{"a" * Users::Photo::MAX_URL_SIZE}"
        assert_raises(ActiveRecord::RecordInvalid) { user.save! }
      end
    end
  end

  describe "#validate_new_photo" do
    let(:photo) { photos(:minimum_photo) }

    it "displays meaningful error message about file type" do
      photo.filename = "abc.doc"
      refute photo.valid?
      assert_equal ["extension doc is not allowed. Please use jpg, jpeg, gif, or png for your photo."], photo.errors[:extension]
    end
  end

  describe "#dispatch_event_after_photo_deletion" do
    let(:photo) { photos(:minimum_photo) }
    let(:user) { photo.user }
    let(:url) { 'https://zendesk-test.s3.us-west-2.amazonaws.com/public/system/photos/20090590/minimum_photo.jpg' }

    before do
      stub_request(:delete, url).to_return(status: 200, body: "", headers: {})
    end

    it "dispatches protopuf event after photo deletion" do
      User.any_instance.expects(:add_domain_event).once
      photo.destroy!
      photo.user.dispatch_event_after_photo_deletion
    end

    it "changes user.photo_url to default image after delition" do
      photo.destroy!
      user.dispatch_event_after_photo_deletion
      assert_not_equal user.photo_url, user.photo_url_before_update
    end

    describe "when capability 'has_sync_support_agent_avatar_to_chat' is enabled" do
      before do
        user.account.stubs(:has_sync_support_agent_avatar_to_chat?).returns(true)
      end

      describe "and user is an agent with a chat entitlement" do
        before do
          user.stubs(:is_agent?).returns(true)
          user.settings.chat_entitlement = 'agent'
          user.save!
        end

        it "enqueues agent chat avatar synchronization" do
          SyncChatAgentAvatarJob.expects(:enqueue).with(user.account_id, user.id)
          photo.destroy!
          user.dispatch_event_after_photo_deletion
        end
      end
    end
  end

  describe "#enqueue_agent_chat_avatar_synchronization" do
    describe "when capability 'has_sync_support_agent_avatar_to_chat' is enabled" do
      before do
        user.account.stubs(:has_sync_support_agent_avatar_to_chat?).returns(true)
      end

      it "does not enqueue agent chat avatar synchronization" do
        SyncChatAgentAvatarJob.expects(:enqueue).never
        user.enqueue_agent_chat_avatar_synchronization
      end

      describe "and user is an end user" do
        it "does not enqueue agent chat avatar synchronization" do
          SyncChatAgentAvatarJob.expects(:enqueue).never
          user.enqueue_agent_chat_avatar_synchronization
        end
      end

      describe "and user is an agent" do
        let(:user) { users(:minimum_agent) }

        describe "without a chat entitlement" do
          before do
            user.settings.chat_entitlement = nil
            user.save!
          end

          it "does not enqueues agent chat avatar synchronization" do
            SyncChatAgentAvatarJob.expects(:enqueue).never

            user.enqueue_agent_chat_avatar_synchronization
          end
        end

        describe "with a chat entitlement" do
          before do
            user.settings.chat_entitlement = 'agent'
            user.save!
          end

          it "enqueues agent chat avatar synchronization" do
            SyncChatAgentAvatarJob.expects(:enqueue).with(user.account_id, user.id)

            user.enqueue_agent_chat_avatar_synchronization
          end
        end
      end
    end
  end
end
