require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'UserOrganizationMemberships' do
  fixtures :accounts, :users, :organization_memberships, :organizations, :tickets, :ticket_fields

  describe Users::OrganizationMemberships do
    let(:account)       { accounts(:minimum) }
    let(:user)          { users(:minimum_end_user) }
    let(:author)        { users(:minimum_author) } # user without organizations
    let(:organization1) { organizations(:minimum_organization1) }
    let(:organization2) { organizations(:minimum_organization2) }
    let(:organization3) { organizations(:minimum_organization3) }
    let(:closed_ticket) { tickets(:minimum_5) }

    describe "validations" do
      it "validates the associated organization_memberships" do
        user.organization_memberships.build
        refute user.valid?

        assert_equal ["cannot be blank"], user.errors[:"organization_memberships.organization"]
      end

      it "validates the number of organizations assigned" do
        user.organizations = [3, 4]
        refute user.valid?

        assert_equal(
          ["Multiple organizations were provided. Only one or none is accepted."],
          user.errors[:base]
        )
      end
    end

    describe "being saved" do
      it "sets the default_organization" do
        assert_nil author.default_organization
        author.save!
        assert_equal organization1, author.default_organization
      end
    end

    describe "being updated" do
      before do
        author.organization_memberships.create!(organization: organization1, account: account, default: true)
        @new_default = author.organization_memberships.create!(organization: organization2, account: account, default: nil)
      end

      it "sets organization_id" do
        @new_default.default = true

        refute_equal @new_default.organization_id, author.organization_id
        author.save!
        assert_equal @new_default.organization_id, author.organization_id
        assert_equal @new_default.organization_id, author.reload.organization_id
      end
    end

    describe "#set_default_organization" do
      describe "for a user with no organization" do
        before do
          assert_empty(author.organization_memberships)
          assert_nil author.organization
        end

        describe "when the account has a default organization for the user" do
          it "uses it to set the user's default_organization " do
            author.account.expects(:default_organization_for).with(author.email_domain).returns(organization2)
            author.set_default_organization
            assert_equal organization2, author.default_organization
          end
        end

        describe "when a different organization is provided" do
          it "uses the organization provided" do
            account.expects(:default_organization_for).never
            new_user = account.users.build(
              name: "Monica Bellis",
              email: "m@aghassipour.com",
              organization: organization3
            )

            assert_equal organization3, new_user.default_organization
          end
        end

        describe "when the account has no default organization for the user" do
          it "does not set a default_organization" do
            author.account.expects(:default_organization_for).with(author.email_domain).returns(nil)
            author.set_default_organization
            assert_nil author.organization
          end
        end

        describe "when the membership already exists" do
          let(:organization_membership) do
            author.organization_memberships.create(account: account, organization: organization1, default: false)
          end

          it "promotes the membership to default" do
            organization_membership.update_column(:default, nil)
            author.update_column(:organization_id, nil)
            author.set_default_organization
            author.save!
            assert_equal 1, author.organization_memberships.count
            assert(author.organization_memberships.last.default)
          end
        end

        describe "when setting via organization_id=" do
          before do
            Arturo.enable_feature! :invalid_organization_logging
          end

          it "logs an exception" do
            ZendeskExceptions::Logger.expects(:record).with(anything) do |exception|
              exception.message.match? 'User.organization_id was set without updating organization memberships'
            end
            author.organization_id = 1000
          end
        end
      end

      describe "for a user with only an organization membership" do
        before do
          OrganizationMembership.create!(account: account, organization: organization1, user: author, default: true)
          author.update_column(:organization_id, nil)
        end

        it "sets the organization_id" do
          assert author.reload.organization_memberships.any?
          assert_nil author.organization_id
          author.set_default_organization
          refute_nil author.organization_id
        end
      end
    end

    describe "#default_organization_membership" do
      it "does not return the default if it is marked for destruction" do
        assert user.default_organization_membership
        user.default_organization_membership.mark_for_destruction

        assert_nil user.default_organization_membership
      end
    end

    describe "#regular_organization_memberships" do
      describe "on an account without multiple_organizations" do
        it "returns an empty set" do
          assert_empty user.regular_organization_memberships
        end
      end

      describe "on an account with multiple_organizations" do
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
          user.update_attribute(:organizations, [organization1, organization2, organization3])
        end

        it "returns all organization_memberships except the default" do
          assert_equal(
            [organization2, organization3].map(&:id),
            user.regular_organization_memberships.map(&:organization_id)
          )
        end

        it "does not include organization_memberships marked_for_destruction" do
          user.organization_memberships.detect do |organization_membership|
            organization_membership.organization_id == organization2.id
          end.mark_for_destruction

          assert_equal(
            [organization3].map(&:id),
            user.regular_organization_memberships.map(&:organization_id)
          )
        end
      end
    end

    describe "#move_tickets_to_organization" do
      before do
        assert_equal [organization1.id], user.tickets.not_closed.map(&:organization_id).uniq
        user.organization_memberships.create!(organization: organization2)
      end

      it "changes organization_id on all unclosed tickets" do
        user.move_tickets_to_organization(organization1.id, organization2.id)
        assert_equal [organization2.id], user.tickets.not_closed(:reload).map(&:organization_id).compact.uniq
      end

      it "logs ticket change as coming from the user causing it" do
        admin = users(:minimum_admin)
        CIA.audit actor: admin do
          user.move_tickets_to_organization(organization1.id, organization2.id)
        end
        assert_equal [admin], user.tickets.not_closed(:reload).map { |t| t.audits.last.author }.uniq.compact
      end

      it "does not change the organization_id on closed tickets" do
        user.move_tickets_to_organization(organization1.id, organization2.id)
        assert_equal [organization1.id], user.reload.tickets.where(status_id: StatusType.CLOSED).map(&:organization_id).uniq
      end
    end

    describe "#organization" do
      it "is valid with an organization" do
        assert user.organization
        assert user.valid?
      end

      it "is valid without an organization" do
        refute author.organization
        assert author.valid?
      end

      it "returns nil for a new user" do
        assert account.users.new.organization.nil?
      end

      it "returns the organization from the default_organization_membership" do
        user.default_organization_membership.send(:unset_user_organization_id)
        assert_nil user.reload.organization_id

        assert_equal(
          user.default_organization_membership.organization,
          user.organization
        )
      end
    end

    describe "#organization_name" do
      it "returns organization name if it has an organization" do
        assert_equal "minimum organization", user.organization_name
      end

      it "returns account name if user is agent with no organization" do
        user.expects(:is_agent?).returns(true)
        user.organization_id = nil
        assert_equal "Minimum account", user.organization_name
      end

      it "returns nil sometimes" do
        user.organization_id = nil
        assert_nil user.organization_name
      end
    end

    describe "#organization=" do
      describe "for a user without an organization" do
        describe "when the user has no tickets" do
          let(:user) { users(:minimum_search_user) }

          before do
            user.organization = organization3
            user.save!
          end

          it "sets the user's default organization" do
            assert_equal organization3, user.organization
          end

          it "synchronizes the user's organization_id value" do
            assert_equal organization3.id, user.organization_id
          end
        end

        describe "when the user has unclosed tickets" do
          it "sets the organization on the unclosed tickets" do
            assert_empty author.tickets.not_closed.map(&:organization_id).compact
            author.update_attribute(:organization, organization2)

            assert_equal(
              [organization2.id],
              author.tickets.reload.not_closed.map(&:organization_id)
            )
          end

          describe "#assign_unclosed_tickets_to_organization" do
            before do
              author.organization = organization2
              author.save!
            end

            before_should "bulk update ticket" do
              ticket_ids = author.tickets.not_closed.map(&:nice_id)
              author.expects(:bulk_update_tickets).with(ticket_ids, organization2.id)
            end
          end
        end
      end

      describe "for a user with an organization" do
        let(:initial_default) { user.default_organization_membership }

        describe "assigning a different organization" do
          describe "on an account with multiple organizations" do
            before do
              Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
              user.update_attribute(:organizations, [organization1, organization3])
              assert_equal(organization1, initial_default.organization)
            end

            describe "and it is not present in the user's current organizations" do
              before { user.organization = organization2 }

              it "adds the new organization and make it the default" do
                assert_equal 3, user.organization_memberships.size
                assert_equal(
                  organization2.id,
                  user.default_organization_membership.organization_id
                )
              end

              it "unsets the previous default and synchronize the organization_id" do
                user.save!
                refute initial_default.reload.default?
                assert_equal(organization2.id, user.reload.organization_id)
              end
            end

            describe "and it is present in the user's current organizations" do
              before { user.organization = organization3 }

              it "promotes the existing organization to be the default" do
                assert_equal 2, user.organization_memberships.size

                assert_equal(
                  organization3.id,
                  user.default_organization_membership.organization_id
                )
              end

              it "unsets the previous default" do
                user.save!
                refute initial_default.reload.default?
              end

              it "synchronizes the organization_id" do
                user.save!
                assert_equal(organization3.id, user.reload.organization_id)
              end
            end
          end

          describe "on an account without multiple organizations" do
            before do
              Account.any_instance.stubs(has_multiple_organizations_enabled?: false)
              assert_equal organization1, user.organization
              assert_same_elements organization1.tickets.map(&:id), user.tickets.map(&:id)

              user.organization = organization2
              user.save!
            end

            it "updates the default_organization_membership" do
              assert_equal 1, user.reload.organization_memberships.size
              assert_equal(
                organization2.id,
                user.default_organization_membership.organization_id
              )
            end

            it "updates the organization_id" do
              assert_equal organization2.id, user.reload.organization_id
            end

            it "changes the organization on the user's unclosed tickets" do
              assert_equal [user.organization_id], user.reload.tickets.not_closed.map(&:organization_id).uniq
              assert_equal [closed_ticket.id], organization1.reload.tickets.map(&:id)
            end
          end
        end

        describe "assigning nil" do
          describe "on an account without a default organization" do
            before do
              user.account.stubs(default_organization_for: nil)

              assert_equal organization1, user.organization
              assert_same_elements organization1.tickets.map(&:id), user.tickets.map(&:id)

              user.organization = nil
              user.save!
            end

            it "removes the default_organization_membership" do
              assert_nil user.organization_memberships.where(default: true).first
            end

            it "unsets the organization on the user's unclosed tickets" do
              assert_empty user.tickets.not_closed.map(&:organization_id).compact
              assert_equal organization1, closed_ticket.organization
            end

            it "unsets the organization_id" do
              assert_nil user.reload.organization_id
            end
          end

          describe "on an account with a default organization" do
            before do
              assert_equal organization1, account.default_organization_for(user.email_domain)

              user.update_attribute(:organization, organization2)
              assert_equal organization2, user.reload.organization
              assert_equal organization2.id, user.organization_id

              user.organization = nil
              user.save!
            end

            it "resets the organization to the account default" do
              assert_equal organization1, user.reload.organization
            end

            it "reloads so that the organization has all attributes available" do
              assert_equal organization1.name, user.organization.name
            end

            it "synchronizes the organization_id value" do
              assert_equal organization1.id, user.reload.organization_id
            end
          end
        end
      end
    end

    describe "#organizations=" do
      def organization_membership_for(user, organization)
        user.organization_memberships.detect do |organization_membership|
          organization_membership.organization_id == organization.id
        end
      end

      describe "on an account without multiple organizations" do
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: false)
          assert_equal 1, user.organizations.size
        end

        describe "with an empty set" do
          it "marks organization_memberships for destruction" do
            user.organizations = []
            assert user.organization_memberships.all?(&:marked_for_destruction?)
          end
        end

        describe "with a single organization id" do
          let(:organization_ids) { [organization2.id] }

          before do
            assert_equal [organization1.id], user.organizations.map(&:id)
            user.organizations = organization_ids
          end

          it "marks the existing organization_membership for destruction" do
            assert organization_membership_for(user, organization1).marked_for_destruction?
          end

          it "initializes a new organization membership for the new organization" do
            assert organization_membership_for(user, organization2).new_record?
          end

          it "does not change the number of organization_memberships" do
            assert_difference("user.organization_memberships.count(:all)", 0) do
              user.save!
            end
          end

          it "synchronizes the organization_id" do
            user.save!
            assert_equal organization2.id, user.reload.organization_id
          end

          describe "when there is no organization record for the given id" do
            it "raises an OrganizationNotFound error" do
              assert_raise(Users::OrganizationMemberships::OrganizationNotFound) do
                user.organizations = [organization1.id + organization2.id + organization3.id]
              end
            end
          end
        end

        describe "with mutiple organization ids" do
          let(:organization_ids) { [organization2.id, organization3.id] }

          before do
            assert_equal [organization1.id], user.organizations.map(&:id)
            user.organizations = organization_ids
          end

          it "does not mark the existing organization membership for destruction" do
            refute organization_membership_for(user, organization1).marked_for_destruction?
          end

          it "does not initialize any new organization memberships" do
            assert_equal 1, user.organization_memberships.size
          end

          it "raises a validation error on save" do
            assert_raise(ActiveRecord::RecordInvalid) { user.save! }

            assert_includes(
              user.errors[:base],
              "Multiple organizations were provided. Only one or none is accepted."
            )
          end
        end

        describe "when reducing the number of organizations" do
          let(:organization_ids_1) { [organization1.id, organization2.id, organization3.id] }
          let(:organization_ids_2) { [organization1.id, organization2.id] }

          before do
            Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
            user.organizations = organization_ids_1
            user.save!
            assert_equal organization_ids_1, user.organizations.map(&:id)
            Account.any_instance.stubs(has_multiple_organizations_enabled?: false)
          end

          it "always validates" do
            user.organizations = organization_ids_2
            assert user.save!
          end
        end
      end

      describe "on an account with multiple organizations" do
        before { Account.any_instance.stubs(has_multiple_organizations_enabled?: true) }

        describe "and existing organization_memberships" do
          describe "adding new organization ids" do
            let(:organization_ids) do
              [
                organization1.id, # user has this organization_membership
                organization2.id, # new organization
                organization3.id  # new organization
              ]
            end

            it "adds an organization_membership for each" do
              assert_difference("user.organization_memberships.size", 2) do
                user.organizations = organization_ids
              end

              assert_same_elements(
                organization_ids,
                user.organization_memberships.map(&:organization_id)
              )
            end

            describe "when there is no organization record for one of the given ids" do
              it "raises an OrganizationNotFound error" do
                assert_raise(Users::OrganizationMemberships::OrganizationNotFound) do
                  user.organizations = [
                    organization1.id,
                    organization2.id,
                    organization1.id + organization2.id + organization3.id
                  ]
                end
              end
            end
          end

          describe "removing all ids" do
            let(:organization_ids) { [] }

            before do
              user.account.stubs(default_organization_for: nil)
              user.organizations = organization_ids
            end

            it "marks all organization_memberships for destruction" do
              assert user.organization_memberships.all?(&:marked_for_destruction?)
            end

            it "unsets the user's organization_id" do
              user.save!
              assert_nil user.reload.organization_id
            end
          end

          describe "adding and removing organizations by id" do
            let(:organization_ids) { [organization1.id, organization3.id] }

            before do
              user.organization_memberships.create!(organization: organization2)
              user.organization_memberships.reload
              assert_equal 2, user.organization_memberships.size
              assert_equal organization1.id, user.organization_id
              assert_equal organization1, user.default_organization
            end

            it "marks the correct organization_memberships for destruction" do
              user.organizations = organization_ids

              assert organization_membership_for(user, organization2).marked_for_destruction?
              refute organization_membership_for(user, organization1).marked_for_destruction?
              refute organization_membership_for(user, organization3).marked_for_destruction?
            end

            it "does not change the default_organization" do
              user.update_attribute(:organizations, organization_ids)
              assert_equal(organization1, user.reload.default_organization)
            end

            it "does not change the organization_id" do
              user.update_attribute(:organizations, organization_ids)
              assert_equal organization1.id, user.organization_id
            end
          end

          describe "removing the organization_id for the default_organization_membership" do
            let(:organization_ids) { [organization2.id, organization3.id] }

            before do
              assert_equal organization1, user.default_organization
              user.organizations = organization_ids
            end

            it "marks the correct organization_membership for destruction" do
              assert organization_membership_for(user, organization1).marked_for_destruction?
            end

            it "sets the first id to be the new default_organization_membership" do
              assert organization_membership_for(user, organization2).default?
            end

            it "persists the new default_organization_membership" do
              user.save!
              assert_equal(
                organization2.id,
                user.reload.default_organization_membership.organization_id
              )
            end

            it "synchronizes the organization_id" do
              user.save!
              assert_equal organization2.id, user.reload.organization_id
            end
          end
        end

        describe "and there are no existing organization_memberships" do
          let(:organization_ids) do
            [organization1.id, organization2.id, organization3.id]
          end

          before do
            assert_equal 0, author.organization_memberships.size
            author.organizations = organization_ids
          end

          it "adds an organization_membership for each id" do
            assert_equal 3, author.organization_memberships.size
          end

          it "ensures that the user has a default_organization_membership" do
            assert organization_membership_for(author, organization1).default?
            assert_equal 1, author.organization_memberships.select(&:default?).size
          end

          it "persists the new default_organization_membership" do
            author.save!
            assert_equal(
              organization1.id,
              author.reload.default_organization_membership.organization_id
            )
          end

          it "synchronizes the organization_id" do
            author.save!
            assert_equal organization1.id, author.reload.organization_id
          end

          it "sets the organization on the unclosed tickets" do
            assert_empty author.tickets.not_closed.map(&:organization_id).compact
            author.update_attribute(:organizations, [organization2])

            assert_equal(
              [organization2.id],
              author.tickets.reload.not_closed.map(&:organization_id)
            )
          end
        end

        describe "and there are no existing organization_memberships and none are provided" do
          it "does not attempt to set the organization on the unclosed tickets" do
            author.account.stubs(default_organization_for: nil)
            author.tickets.expects(:update_all).with(:updated_at).never
            author.update_attribute(:organizations, [])
          end
        end
      end
    end

    describe "#organization_name=" do
      let(:organization) { account.organizations.create!(name: "Buddhists") }

      it "assigns the correct organization" do
        author.organization_name = organization.name
        assert_equal organization, author.organization
      end

      it "ignores whitespace in the name string" do
        author.organization_name = " #{organization.name}  "
        assert_equal organization, author.organization
      end

      it "unsets the organization when the name is empty" do
        author.organization_name = " "
        assert_nil author.organization
      end

      it "validates the existence of an organization with the given name" do
        author.organization_name = "Rubyists"
        author.valid?

        assert_includes author.errors[:base].join, "does not exist"
      end

      it "unsets the organization even when there are organizations with invalid names" do
        organization.update_attribute(:name, "")
        refute organization.valid?

        author.organization_name = " "
        assert_nil author.organization
      end
    end

    describe "#organization_names=" do
      let(:organization) { account.organizations.create!(name: "Buddhists") }

      describe "with multiple organizations off" do
        it "assigns the correct organizations, ignoring excess whitespace" do
          author.account.expects(:default_organization_for).returns(nil)
          author.organization_names = [" #{organization.name}  "]
          assert_equal(
            organization.id,
            author.organization_memberships.first.organization_id
          )
        end

        it "ignores empty strings" do
          author.account.expects(:default_organization_for).returns(nil)
          organization_names = ["  ", organization.name]
          assert_difference "author.organization_memberships.size", 1 do
            author.organization_names = organization_names
          end
        end

        it "validates the existence of organizations for each name provided" do
          author.organization_names = ["Non Praesto", "Inexistante", organization.name]

          refute author.valid?
          assert_includes author.errors[:base].join, "'Non Praesto, Inexistante' do not exist"
        end
      end

      describe "with multiple organizations on" do
        let(:domain_organization) { account.organizations.create!(name: "Hindus") }

        before do
          Account.any_instance.stubs(:has_multiple_organizations_enabled?).returns(true)
        end

        it "maintains the email domain org" do
          author.account.expects(:default_organization_for).returns(domain_organization)
          author.organization_names = [" #{organization.name} "]
          assert_equal(
            [organization.id, domain_organization.id],
            author.organization_memberships.map(&:organization_id)
          )
        end
      end
    end

    describe "#has_organizations?" do
      describe "with new user" do
        let(:user) { User.new }

        it "is true for nothing" do
          assert user.has_organizations?([])
          assert user.has_organizations?(nil)
        end

        it "works on new with ids" do
          user.stubs(organization_memberships: [1, 2, 3].map { |i| stub(organization_id: i) })

          assert user.has_organizations?([1, 3])
          assert user.has_organizations?([1, 3, 3])
          refute user.has_organizations?([4])
          refute user.has_organizations?([3, 4])

          assert user.has_organizations?(1)
          refute user.has_organizations?(4)
        end
      end

      describe "with persisted user" do
        let(:user) { users(:minimum_end_user) }

        it "is true for nothing" do
          assert user.has_organizations?([])
          assert user.has_organizations?(nil)
        end

        it "works with 1 / via exists?" do
          refute user.organization_ids.empty?

          assert user.has_organizations?(user.organization_ids.first)
          assert user.has_organizations?([user.organization_ids.first])

          refute user.has_organizations?(1234)
          refute user.has_organizations?([1234])
        end

        it "works with many" do
          user = users(:minimum_end_user)
          refute user.organization_ids.empty?

          assert user.has_organizations?([user.organization_ids.first, user.organization_ids.first])
          assert user.has_organizations?([user.organization_ids.first])

          refute user.has_organizations?(1234)
          refute user.has_organizations?([1234])
        end
      end
    end
  end
end
