require_relative "../support/test_helper"
require_relative "../support/arturo_test_helper"

SingleCov.covered! uncovered: 18

describe User do
  include ArturoTestHelper
  fixtures :all

  let(:statsd_client_trial_limit) { Zendesk::StatsD::Client.new(namespace: 'trial_limit') }

  before do
    @account = accounts(:minimum)
    Account.any_instance.stubs(:ticket_sharing_partner_support_addresses).returns([])
    Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true)
    @log_hash = {
      message: "sanitizing_requester_names, removing URL",
      account_id: @account.id,
      old: "Ricky Rocket https://www.zendesk.com",
      sanitized: "Ricky Rocket"
    }
  end

  # FIXME: move these into the outside describe
  # FYI: the User.stubs(:new).returns(@user) will break some tests that try to create new users
  describe User do
    before do
      account = Account.new
      account.stubs(:remote_authentication).returns(stub(is_active?: false))
      account.stubs(:is_serviceable?).returns(true)
      account.stubs(:role_settings).returns(role_settings(:minimum))

      @user = User.new
      @user.stubs(:account).returns(account)

      User.stubs(:new).returns(@user)
    end

    subject { @user }

    should_validate_presence_of :time_zone
    should validate_length_of(:name).is_at_least(1)

    describe "when the name includes an email address enclosed in angle brackets" do
      let(:user) do
        user = users(:minimum_end_user)
        user.name = "Joe Doe <joe@doe.com>"
        user
      end

      it "removes email address" do
        user.save
        assert_equal 'Joe Doe', user.name
      end

      describe "when the entire name is an email address enclosed in angle brackets" do
        let(:email_address_in_angle_brackets) { "<user.name@example.com>" }
        let(:user) do
          user = users(:minimum_end_user)
          user.name = email_address_in_angle_brackets
          user
        end

        it "saves successfully, setting the user's name based on local part of the email address" do
          assert_equal email_address_in_angle_brackets, user.name
          assert user.save
          assert_equal "User Name", user.name
        end
      end
    end

    it "validates the time_zone" do
      user = users(:minimum_end_user)
      assert(user.valid?)

      user.time_zone = 'Copenhagen'
      assert(user.valid?)

      user.time_zone = 'Monkey'
      assert(!user.valid?)
      assert(user.errors[:time_zone].any?)

      user.time_zone = false
      refute(user.valid?)

      user.time_zone = nil
      assert(user.valid?)
    end

    it 'supports the "Puerto Rico" time zone' do
      assert_not_nil ActiveSupport::TimeZone['Puerto Rico'].tzinfo
      user = users(:minimum_end_user)
      assert(user.valid?)

      user.time_zone = 'Puerto Rico'
      assert(user.valid?)
    end

    describe_with_arturo_disabled :switch_kyev_to_kyiv do
      it "does not switch user's time zone from Kyev to Kyiv" do
        user = users(:minimum_end_user)

        user.time_zone = 'Kyev'
        user.valid?

        assert_equal 'Kyev', user.time_zone
      end
    end

    describe_with_arturo_enabled :switch_kyev_to_kyiv do
      it "switches user's time zone from Kyev to Kyiv" do
        user = users(:minimum_end_user)

        user.time_zone = 'Kyev'
        user.valid?

        assert_equal 'Kyiv', user.time_zone
      end
    end

    describe "remove urls from name" do
      it "does nothing on existing records when name ok" do
        user = users(:minimum_end_user)
        user.name = "Ricky Rocket"
        user.save!
        user.reload
        assert_equal "Ricky Rocket", user.name
      end

      it "does nothing on new records when name ok" do
        user = User.new
        user.account = @account
        user.time_zone = 'Copenhagen'
        user.email = 'horse@exmple.com'
        user.name = "Ricky Rocket"
        user.save!
        user.reload
        assert_equal "Ricky Rocket", user.name
      end

      it "cleans urls before save on existing records" do
        user = users(:minimum_end_user)
        Rails.logger.expects(:info).with(@log_hash).at_least_once
        Rails.logger.expects(:info).with { |message| message.include? "updated_at was" }
        Rails.logger.expects(:info).with { |message| message.include? "[UserEntityPublisher]" }
        user.name = "Ricky Rocket https://www.zendesk.com"
        user.save!
        user.reload
        assert_equal "Ricky Rocket", user.name
      end

      it "cleans urls before save on new records" do
        user = User.new
        user.account = @account
        user.time_zone = 'Copenhagen'
        user.email = 'horse@exmple.com'
        user.name = "Ricky Rocket https://www.zendesk.com"
        user.save!
        user.reload
        assert_equal "Ricky Rocket", user.name
      end

      it "cleans multiple urls before save on new records" do
        user = User.new
        user.account = @account
        user.time_zone = 'Copenhagen'
        user.email = 'horse@exmple.com'
        user.name = "https://www.zendesk.com Ricky Rocket https://www.zendesk.com"
        user.save!
        user.reload
        assert_equal "Ricky Rocket", user.name
      end

      it "cleans multiple urls with no protocol header" do
        user = User.new
        user.account = @account
        user.time_zone = 'Copenhagen'
        user.email = 'horse@exmple.com'
        user.name = "www.zendesk.com/sneakdakeek Ricky Rocket www.zendesk.com/snoopdog/bowwow"
        user.save!
        user.reload
        assert_equal "www.zendesk.com/sneakdakeek Ricky Rocket www.zendesk.com/snoopdog/bowwow", user.name
      end

      it "cleans single urls with no protocol header" do
        user = User.new
        user.account = @account
        user.time_zone = 'Copenhagen'
        user.email = 'horse@exmple.com'
        user.name = "e40.com/rapsnak"
        user.save!
        user.reload
        assert_equal "e40.com/rapsnak", user.name
      end

      it "cleans multiple urls and email before save on new records" do
        user = User.new
        user.account = @account
        user.time_zone = 'Copenhagen'
        user.email = 'horse@exmple.com'
        user.name = "https://www.zendesk.com <ricky@example.com> Ricky Rocket https://www.zendesk.com"
        user.save!
        user.reload
        assert_equal "Ricky Rocket", user.name
      end

      it "cleans urls to unknown before save on existing records" do
        user = users(:minimum_end_user)
        user.name = "https://www.zendesk.com"
        user.save!
        user.reload
        assert_equal "Unknown", user.name
      end

      it "cleans multiple urls to Foo Foo before save on existing records" do
        user = users(:minimum_end_user)
        user.name = "https://www.zendesk.com Foo Foo https://www.zendesk.com"
        user.save!
        user.reload
        assert_equal "Foo Foo", user.name
      end

      it "cleans urls to unknown before save on new records" do
        user = User.new
        user.account = @account
        user.time_zone = 'Copenhagen'
        user.email = 'horse@exmple.com'
        user.name = "https://www.zendesk.com"
        user.save!
        user.reload
        assert_equal "Unknown", user.name
      end
    end

    it "always follow the account time format setting" do
      account = accounts(:minimum)
      user    = users(:minimum_end_user)
      account.update_attributes!(uses_12_hour_clock: false)
      assert_equal false, user.reload.uses_12_hour_clock
      assert_equal false, user.reload.uses_12_hour_clock?
      user.update_attributes!(uses_12_hour_clock: true)
      assert_equal false, user.reload.uses_12_hour_clock
      assert_equal false, user.reload.uses_12_hour_clock?
      account.update_attributes!(uses_12_hour_clock: true)
      assert(user.reload.uses_12_hour_clock)
      assert(user.reload.uses_12_hour_clock?)
    end

    describe "changing roles" do
      it "is possible for a normal user" do
        user = users(:minimum_end_user)
        assert user.role_can_be_changed?
      end

      it "does not be possible for an account owner" do
        @user.stubs(:is_account_owner?).returns(true)
        refute @user.role_can_be_changed?
      end

      it "does not be possible for an user without an email identitiy" do
        user = users(:minimum_end_user)
        UserTwitterIdentity.create!(user: user, account: user.account, value: 123)

        user.identities.email.each(&:destroy)

        assert_empty(user.identities.email)
        refute user.role_can_be_changed?
      end
    end

    describe "password validation" do
      it "does not crash if password is nil" do
        @user.password = nil
        @user.stubs(:password_required?).returns(true)
        @user.valid?
      end
    end

    describe "column length validations" do
      it "does not allow ridiculously long names" do
        @user.name = "x" * 9999
        refute @user.valid?
        assert_equal ["is too long (maximum is 255 characters after encoding)"], @user.errors[:name]
      end

      it "does not allow too long external_id" do
        @user.external_id = "a" * 256
        refute @user.valid?
        assert_equal ["is too long (maximum is 255 characters after encoding)"], @user.errors[:external_id]
      end

      it "does not allow too long details" do
        @user.details = "a" * 65536
        refute @user.valid?
        assert_equal ["is too long (maximum is 65535 characters after encoding)"], @user.errors[:details]
      end

      it "does not allow too long notes" do
        @user.notes = "a" * 65536
        refute @user.valid?
        assert_equal ["is too long (maximum is 65535 characters after encoding)"], @user.errors[:notes]
      end
    end

    describe "SDK identities validation" do
      before do
        @user = users(:minimum_end_user)
        @identity_limit = 2
        @user.account.settings.stubs(:max_sdk_identities).returns(@identity_limit)
        @identity_limit.times do |i|
          identity = UserSdkIdentity.new(user: @user, value: "111a1111-b11c-11d1-e111-1111111111#{i + 10}", account: @user.account, is_verified: true)
          identity.save
        end
      end

      describe "SDK Identities limits" do
        before do
          @user.identities << UserSdkIdentity.new(user: @user, value: "111a1111-b11c-11d1-e111-999999999999")
        end

        it "prevents the saving of the user adding an error to the user" do
          refute @user.valid?, "User should not be valid because the sdk limit has been reached."
          refute @user.save, "User should be saved because the sdk limit has been reached."

          expected_error_msg = "sdk identities limit for user_id: #{@user.id}, account_id: #{@user.account.id} exceeded"
          assert @user.errors.full_messages.join.must_include(expected_error_msg)
        end

        it "prevents the creation of the SDK identity" do
          assert_no_difference -> { UserSdkIdentity.count } do
            @user.save
          end
        end
      end
    end

    describe "user identity limit validation" do
      before do
        User.any_instance.stubs(:whitelisted_from_user_limits?).returns(false)
        @user = users(:minimum_end_user)
        @identity_limit = 1
        @user.account.settings.stubs(:max_identities).returns(@identity_limit)
        @user.identities.delete_all
        @user.identities << UserEmailIdentity.new(user: @user, value: 'first@identity.com')
        @user.save
      end

      describe_with_arturo_enabled :user_identity_limit do
        before { @user.identities << UserEmailIdentity.new(user: @user, value: 'aa@example.com') }

        describe "when a user is not whitelisted" do
          it "prevents the saving of the user by adding an error to the user when whitelisted" do
            refute @user.valid?, "User should not be valid because the user identity limit has been reached."
            refute @user.save, "User should not be saved because the user identity limit has been reached."

            expected_error_msg = "The number of identities for this user has exceeded the limit of #{@account.settings.max_identities}. An identity can be an email address, a phone number, a Twitter account, or a Facebook account."
            assert @user.errors.full_messages.join.must_include(expected_error_msg)
          end

          it "prevents the creation of the identity" do
            assert_no_difference -> { UserIdentity.count } do
              @user.save
            end
          end
        end

        describe "when a user is whitelisted" do
          before do
            User.any_instance.stubs(:whitelisted_from_user_limits?).returns(true)
            @user = users(:minimum_end_user)
            @identity_limit = 1
            @user.account.settings.stubs(:max_identities).returns(@identity_limit)
            @user.identities.delete_all
            @user.identities << UserEmailIdentity.new(user: @user, value: 'first@identity.com')
            @user.save
          end

          it "allows the saving of the user" do
            assert @user.valid?, "User should be valid because the user identity limit is not being checked."
            assert @user.save, "User should be valid because the user identity limit is not being checked."
          end
        end
      end

      describe_with_arturo_disabled :user_identity_limit do
        before { @user.identities << UserEmailIdentity.new(user: @user, value: 'aa@example.com') }

        it "allows the saving of the user" do
          assert @user.valid?, "User should be valid because the user identity limit is not being checked."
          assert @user.save, "User should be valid because the user identity limit is not being checked."
        end
      end
    end

    describe "#email_address_with_name" do
      before do
        @user = User.new
        @user.email = 'horse@example.com'
      end

      it "has the name when the address is parsable" do
        @user.name = 'Mr. Ed'
        assert_equal '"Mr. Ed" <horse@example.com>', @user.email_address_with_name
      end

      it "removes unparsable characters from the address" do
        @user.name = "Mr.\n Ed"
        assert_equal '"Mr. Ed" <horse@example.com>', @user.email_address_with_name
      end

      it "does not add name when email_name is blank" do
        Rails.logger.expects(:warn).never
        @user.name = "."

        assert_equal "horse@example.com", @user.email_address_with_name
      end

      it "logs and counts when the address isn't parsable" do
        Zendesk::Mail::Address.expects(:parse).returns(nil)

        # Logs the issue with all the details
        Rails.logger.expects(:warn).once

        # Tracks this incident in `classic.app.errors.invalid_email_address`
        Zendesk::StatsD::Client.any_instance.
          expects(:increment).
          with(:invalid_email_address).
          once

        @user.name = "Mr.\nEd"
        assert_equal 'horse@example.com', @user.email_address_with_name
      end

      it "gracefully handles a catastrophic parse failure" do
        Zendesk::Mail::Address.expects(:parse).raises(NoMethodError)
        @user.name = "boom"
        assert_equal "horse@example.com", @user.email_address_with_name
      end

      it "does not include stray quotes" do
        @user.name = 'Mr."Ed'
        assert_equal '"Mr.Ed" <horse@example.com>', @user.email_address_with_name
      end

      it "does not include quote escapes" do
        @user.name = "ООО \\\"Тех кра\\\""
        assert_equal "\"ООО Тех кра\" <horse@example.com>", Mail::Address.new(@user.email_address_with_name).to_s
        assert_equal "\"=?UTF-8?B?0J7QntCe?= =?UTF-8?B?INCi0LXRhQ==?= =?UTF-8?B?INC60YDQsA==?=\" <horse@example.com>", @user.email_address_with_name
      end

      it "does not include unquoted named" do
        @user.name = ""
        assert_equal 'horse@example.com', @user.email_address_with_name
      end
    end

    describe "#email_domain" do
      let(:user) { User.new }

      it "returns the domain part of the user's email address" do
        user.email = "horse@example.com"
        assert_equal "example.com", user.email_domain
      end

      it "returns nil if the email address is an empty string" do
        assert_nil user.email_domain
      end
    end

    describe "that is verified" do
      before do
        @user.stubs(:is_verified?).returns(true)
        @user.stubs(:password_required?).returns(true)
      end

      subject { @user }

      it "ensures the password length is at least 5" do
        @user.password = '1234'
        @user.valid?
        assert @user.errors[:password].any?

        @user.password = '12345'
        @user.valid?
        assert_equal [], @user.errors[:password]
      end
    end

    describe "with an external id" do
      before do
        @user = users(:minimum_agent)
        @user_2 = @user.dup
        @user.update_attribute(:external_id, "foo")
        @user_2.external_id = "foo"
        User.any_instance.stubs(:email).returns("foo@foo.com")
      end

      it "validates uniqueness of external_id" do
        refute @user_2.valid?
      end

      it "does not cause errors if user is inactive" do
        @user.update_column(:is_active, false)
        assert @user_2.valid?
      end

      it "does not cause errors if the users are in different accounts" do
        @user.update_attribute(:account_id, accounts(:support).id)
        @user_2.update_attribute(:account_id, accounts(:minimum).id)

        assert @user_2.account_id != @user.account_id
        assert @user_2.external_id == @user.external_id
        assert @user_2.valid?
      end

      it "correctly handles an existing external_id on another user" do
        @user_2.update_attributes(external_id: "Foo")
        assert_equal "Foo", @user_2.external_id
      end
    end
  end

  describe "auditing attributes" do
    let!(:account_owner) { users(:minimum_admin) }
    let!(:permission_set) { account_owner.account.permission_sets.create!({name: 'Test Example Permission Set'}) }
    let(:user) do
      User.create!(name: "test user", email: "test92ab0dc0@example.com", roles: Role::ADMIN.id, account: account_owner.account)
    end
    let(:cia_attribute_changes) { user.cia_events.last.attribute_changes }

    before do
      CIA.audit(actor: account_owner) { user }
      CIA::Event.destroy_all
      user.reload
    end

    describe_with_arturo_disabled :central_admin_staff_mgmt_roles_tab do
      describe_with_arturo_disabled :audit_logs_staff_service_entitlements do
        before do
          CIA.audit(actor: account_owner) { user.update!(roles: Role::AGENT.id, permission_set: permission_set, password: 'XXXXXXXXXX') }
        end

        it 'adds an attribute change record for auditable attributes' do
          assert_equal user.cia_events.count, 1
          %w[roles crypted_password permission_set_id].each do |attribute_name|
            assert_equal cia_attribute_changes.where(attribute_name: attribute_name).count, 1
          end
        end
      end
    end

    describe_with_arturo_enabled :central_admin_staff_mgmt_roles_tab do
      describe_with_arturo_enabled :audit_logs_staff_service_entitlements do
        before do
          CIA.audit(actor: account_owner) { user.update!(roles: Role::AGENT.id, permission_set: permission_set, password: 'XXXXXXXXXX') }
        end

        it 'adds an attribute change record for auditable attributes' do
          assert_equal user.cia_events.count, 1
          %w[crypted_password].each do |attribute_name|
            assert_equal cia_attribute_changes.where(attribute_name: attribute_name).count, 1
          end
        end

        it 'does not audit role attributes with arturo enabled' do
          assert_equal user.cia_events.count, 1

          %w[roles permission_set_id].each do |attribute_name|
            assert_equal cia_attribute_changes.where(attribute_name: attribute_name).count, 0
          end
        end
      end
    end
  end

  describe "A trial account" do
    before do
      @account = accounts(:trial)
      assert @account.subscription.is_trial?
    end

    it "increments max_agents to keep up with total agents as they are added" do
      old_max_agents = @account.subscription.max_agents

      8.times do |_i|
        FactoryBot.create(:user, account: @account, roles: Role::AGENT.id)
        @account.reload
      end

      assert_equal @account.agents.count(:all), @account.subscription.max_agents
      assert old_max_agents < @account.agents.count(:all)
    end
  end

  describe "A subscribed account with :max_agents set to #{Subscription::DEFAULT_AGENTS}" do
    before do
      @account = accounts(:trial)
      Account.trial_limits.each { |model| TrialLimit.reset_limit_count(@account, model) }
      Subscription.any_instance.stubs(:is_trial?).returns(false)
      (Subscription::DEFAULT_AGENTS - @account.agents.count(:all)).times do |_i|
        FactoryBot.create(:user, account: @account, roles: Role::AGENT.id)
      end
      @account.reload

      assert_equal Subscription::DEFAULT_AGENTS, @account.subscription.max_agents
    end

    it "does not allow more than the max_agents to be created" do
      assert_raise ActiveRecord::RecordInvalid do
        FactoryBot.create(:user, account: @account, roles: Role::AGENT.id)
      end

      assert @account.subscription.max_agents >= @account.agents.count(:all)
    end

    describe "with light agents" do
      before do
        Account.any_instance.stubs(:has_light_agents?).returns(true)
        @permission_set = PermissionSet.create_light_agent!(@account)
        @user = @account.end_users.create! name: "foo", email: "foo@bar.com"
      end

      it "allows light agents to be created in excess of the max_agent limit" do
        @user.roles = Role::AGENT.id
        @user.permission_set = @permission_set
        @user.save!
      end

      it "does not allow light agents to be converted into legacy agents" do
        @user.roles = Role::AGENT.id
        @user.update_attributes! permission_set: @account.permission_sets.find_by_role_type(PermissionSet::Type::LIGHT_AGENT)
        assert_raise(ActiveRecord::RecordInvalid) do
          @user.roles = Role::AGENT.id
          @user.update_attributes! permission_set: nil
        end
      end

      it "does not allow light agents to be converted into full agents" do
        @user.roles = Role::AGENT.id
        @user.update_attributes! permission_set: @account.permission_sets.find_by_role_type(PermissionSet::Type::LIGHT_AGENT)
        assert_raise(ActiveRecord::RecordInvalid) do
          @user.roles = Role::AGENT.id
          @user.update_attributes! permission_set: @account.permission_sets.custom.first
        end
      end
    end
  end

  describe "#enqueue_photo_fetching_job" do
    it "does not enqueue a job for invalid URLs" do
      FetchProfileImageJob.expects(:enqueue).never
      User.new(remote_photo_url: "monkey", account: Account.new).send(:enqueue_photo_fetching_job)
    end

    it "enqueues a job for valid URLs" do
      FetchProfileImageJob.expects(:enqueue).once
      User.new(remote_photo_url: "http://example.com/image.png", account: Account.new).send(:enqueue_photo_fetching_job)
    end
  end

  describe "#to_xml" do
    it "only hit the DB a limited number of times when finding users using Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES" do
      assert_sql_queries 12..14 do
        # If you feel the urge to change this test, please make sure it's not because you're introducing an N+1 query
        # This is scoped to the :minimum account because with predictive_load it isn't safe to load from unsharded collections (accounts) to sharded collections (account_settings, subscription_features)
        users = accounts(:minimum).users.includes(Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES).all.to_a
        # call the to_xml method with all the optional options to trigger the most number of queries
        users.to_xml(methods: [:current_tags], include: [:groups])
      end
    end
  end

  describe "#end_users" do
    before do
      @users = User.end_users
    end

    it "includes end users" do
      assert @users.include?(users(:minimum_end_user))
    end

    it "does not include agents" do
      refute @users.include?(users(:minimum_agent))
    end
  end

  describe "#agents" do
    before do
      @users = User.agents
    end

    it "includes agents" do
      assert @users.include?(users(:minimum_agent))
    end

    it "includes admins" do
      assert @users.include?(users(:minimum_admin))
    end

    it "does not include end users" do
      refute @users.include?(users(:minimum_end_user))
    end
  end

  describe "#admins" do
    before do
      @users = User.admins
    end

    it "includes admins" do
      assert @users.include?(users(:minimum_admin))
    end

    it "does not include agents" do
      refute @users.include?(users(:minimum_agent))
    end

    it "does not include end users" do
      refute @users.include?(users(:minimum_end_user))
    end
  end

  describe "#active" do
    before do
      @users = User.active
    end

    it "includes active users" do
      assert @users.include?(users(:minimum_end_user))
    end

    it "does not include inactive users" do
      refute @users.include?(users(:inactive_user))
    end
  end

  describe "#inactive" do
    before do
      @users = User.inactive
    end

    it "includes inactive users" do
      assert @users.include?(users(:inactive_user))
    end

    it "does not include active users" do
      refute @users.include?(users(:minimum_end_user))
    end
  end

  describe "saving a user" do
    before do
      @user = accounts(:minimum).users.new(name: 'Bud Dha', email: 'mentor@example.com')
    end

    it "does not attempt to store a twitter identity when there isn't a new twitter identity" do
      @user.new_twitter_identity = ''
      @user.expects(:store_new_twitter_identity).never
      @user.save
    end

    it "attempts to store a twitter identity when there's a new twitter identity" do
      @user.new_twitter_identity = 'zdtest'
      @user.expects(:store_new_twitter_identity).once
      @user.save
    end
  end

  describe "#clean_name" do
    it "strips carriage return and newlines from name" do
      assert_equal "hellothere", User.new(name: "hello\r\nthere").clean_name
    end

    it "strips quotes" do
      assert_equal "hellothere", User.new(name: "hello\"there").clean_name
    end

    it "does not change a valid name" do
      assert_equal "hello there", User.new(name: "hello there").clean_name
    end

    it "works when their name is just @" do
      assert_equal "@", User.new(name: "@").clean_name
    end

    it "does not change a valid twitter handle" do
      assert_equal "@hellothere", User.new(name: "@hellothere").clean_name
    end

    it "returns a full name if the name has an @ in the middle" do
      assert_equal "firstname@lastname", User.new(name: "firstname@lastname").clean_name
    end
  end

  describe "account has_agent_display_names? enabled" do
    before do
      Account.any_instance.stubs(:has_agent_display_names?).returns(true)
      @agent = users(:minimum_agent)
      @agent.name = "Jim <script>alert('shotgun')</script> Beam"
      @agent.agent_display_name = "Hobo with a <script>alert('shotgun')</script>"
      @agent.save!
    end

    describe "#agent_display_name=" do
      it "strips <'s from the name" do
        assert_equal "Hobo with a script>alert('shotgun')/script>", @agent.agent_display_name
      end
    end

    describe "#safe_name" do
      it "returns full names for agent consumers" do
        assert_equal "Jim script>alert('shotgun')/script> Beam", @agent.safe_name(true)
      end

      it "returns safe names for end-user consumers" do
        assert_equal "Hobo with a script>alert('shotgun')/script>", @agent.safe_name(false)
      end
    end
  end

  describe "#default show_first_comment_private_tooltip setting" do
    let(:account)       { FactoryBot.create(:account) }
    let(:existing_user) { FactoryBot.create(:user, account: account, roles: Role::AGENT.id) }
    let(:new_user)      { FactoryBot.create(:user, account: account, roles: Role::AGENT.id) }

    before do
      # FCP is off
      account.stubs(:is_first_comment_private_enabled).returns(false)
      # user is created before FCP is enabled
      existing_user
      # FCP is turned on
      account.stubs(:is_first_comment_private_enabled).returns(true)
      # user is created after FCP is enabled
      new_user
    end

    it 'should be false for new_users' do
      assert_equal false, new_user.settings.show_first_comment_private_tooltip
    end

    it 'should be true for existing users' do
      assert(existing_user.settings.show_first_comment_private_tooltip)
    end
  end

  describe "#ensure_locale" do
    before { @available_languages = [translation_locales(:english_by_zendesk), translation_locales(:brazilian_portuguese)] }
    describe "when updating a user" do
      before { @user = users(:minimum_end_user) }
      describe "with an allowed locale" do
        before do
          Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
          Account.any_instance.stubs(:available_languages).returns(@available_languages)
          @user.locale_id = translation_locales(:brazilian_portuguese).id
          @user.save
        end
        it "sets the user default language to brazilian" do
          assert_equal translation_locales(:brazilian_portuguese), @user.translation_locale
        end
      end

      describe "with a non allowed locale" do
        before do
          Account.any_instance.stubs(:available_languages).returns([])
          @user.locale_id = translation_locales(:english_by_zendesk).id
          @user.save
        end
        it "resets the user default language" do
          assert_nil @user.locale_id
        end
      end
    end

    describe "#cc_blocklisted?" do
      before { @user = users(:minimum_admin) }

      describe "when user is blocklisted" do
        before { @user.account.cc_blacklist = @user.email }

        it "returns true" do
          assert @user.cc_blocklisted?
        end
      end

      describe "when user is not blocklisted" do
        before { @user.account.cc_blacklist = "" }

        it "returns false" do
          refute @user.cc_blocklisted?
        end
      end
    end

    describe "#device_notification?" do
      describe 'with an admin' do
        before { @user = users(:minimum_admin) }

        it 'defaults to true' do
          assert @user.device_notification?
        end

        it 'respects selection' do
          @user.settings.device_notification = false
          @user.settings.save
          refute @user.device_notification?
        end
      end

      describe "with an agent" do
        before { @user = users(:minimum_agent) }

        it 'defaults to false' do
          refute @user.device_notification?
        end

        it 'respects selection' do
          @user.settings.device_notification = true
          @user.settings.save
          assert @user.device_notification?
        end
      end

      describe "with an end-user" do
        before { @user = users(:minimum_end_user) }

        it 'defaults to false' do
          refute @user.device_notification?
        end

        it 'returns false regardless of setting' do
          @user.settings.device_notification = true
          @user.settings.save
          refute @user.device_notification?
        end
      end
    end

    describe "when creating a user" do
      before { @user = accounts(:minimum).users.new(name: 'Bud Dha', email: 'mentor@example.com') }
      describe "with an allowed locale" do
        before do
          Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
          Account.any_instance.stubs(:available_languages).returns(@available_languages)
          @user.locale_id = translation_locales(:brazilian_portuguese).id
          @user.save
        end
        it "sets the user default language to brazilian" do
          assert_equal translation_locales(:brazilian_portuguese), @user.translation_locale
        end
      end

      describe "with a non allowed locale" do
        before do
          Account.any_instance.stubs(:available_languages).returns([])
          @user.locale_id = translation_locales(:english_by_zendesk).id
          @user.save
        end
        it "resets the user default language" do
          assert_nil @user.locale_id
        end
      end
    end
  end

  describe "#user_handle" do
    let(:user) { users(:minimum_agent) }
    let(:account) { user.account }
    describe "before June 1, 2016" do
      before { account.expects(:created_at).returns(Date.new(2015, 6, 30)) }
      it "shows the account subdomain followed by the user id" do
        assert_equal "minimum_#{user.id}", user.user_handle
      end
    end

    describe "after June 1, 2016" do
      before { account.expects(:created_at).returns(Date.new(2018, 6, 30)) }
      it "shows the account id followed by the user id" do
        assert_equal "#{account.id}_#{user.id}", user.user_handle
      end
    end
  end

  describe "#report_csv" do
    describe "given an end user" do
      let(:user) { users(:minimum_end_user) }

      it "returns false" do
        refute user.report_csv
      end
    end

    describe "given an agent" do
      let(:user) { users(:minimum_agent) }

      it "returns false" do
        refute user.report_csv
      end
    end

    describe "given the account owner" do
      let(:user) { accounts(:minimum).owner }

      it "returns true" do
        assert user.report_csv
      end
    end

    describe "given a whitelisted admin" do
      let(:user) { users(:minimum_admin_not_owner) }

      before do
        Account.any_instance.stubs(:settings).returns(stub(export_whitelisted_domain: nil))
      end

      it "returns true" do
        assert user.report_csv
      end
    end
  end

  describe 'Given an account with no english by zendesk' do
    before do
      @agent = users(:minimum_agent)
      @account = @agent.account
      @locale = translation_locales(:japanese)

      @account.stubs(:available_languages).returns([@locale])
    end

    describe 'An agent trying to change their locale' do
      before do
        @agent.locale_id = @locale.id
        @agent.save!
      end

      it 'actually gets their requested locale' do
        @agent.reload
        assert_equal(@locale.id, @agent.locale_id)
      end
    end
  end

  describe "#ensure_time_zone" do
    before do
      @user = accounts(:minimum).users.new(name: 'Bud Dha', email: 'mentor@example.com')
    end

    it "clobbers custom timezones unless the feature is enabled" do
      @user.account.subscription.stubs(:has_individual_time_zone_selection?).returns(false)
      @user.time_zone = 'Copenhagen'
      @user.save
      @user.reload
      assert @user.read_attribute(:time_zone).nil?
    end

    it "does not clobber custom timezones if the feature is enabled" do
      @user.account.subscription.stubs(:has_individual_time_zone_selection?).returns(true)
      @user.time_zone = 'Copenhagen'
      @user.save
      @user.reload
      assert 'Copenhagen', @user.time_zone
    end
  end

  describe "#log_previous_updated_at" do
    let(:user) { users(:minimum_end_user) }

    it "logs previous updated_at" do
      Rails.logger.expects(:info).with("user #{user.id} updated_at was #{user.updated_at.utc.to_s(:db)}")
      user.updated_at = Time.now
      user.send(:log_previous_updated_at)
    end

    it "logs when updated_at was suspiciously not updated after an update" do
      Rails.logger.expects(:info).with("user #{user.id} updated_at was unchanged after an update... suspicious!")
      user.send(:log_previous_updated_at)
    end

    it "logs when previous updated_at was nil" do
      Rails.logger.expects(:info).with("user  updated_at was nil")
      user = User.new
      user.updated_at = Time.now
      user.send(:log_previous_updated_at)
    end
  end

  describe "#has_verified_email?" do
    before do
      @user = users(:minimum_end_user)
      @user.identities.delete_all
      @user.identities << UserEmailIdentity.new(user: @user, value: "hello_a@example.com", is_verified: true, priority: 1)
      @user.identities << UserEmailIdentity.new(user: @user, value: "hello_b@example.com", is_verified: false, priority: 2)
      @user.identities << UserEmailIdentity.new(user: @user, value: "hello_c@example.com", is_verified: true, priority: 3)
      @user.identities << UserEmailIdentity.new(user: @user, value: "hello_d@example.com", is_verified: false, priority: 4)
    end

    it "only say it has email when the email is verified" do
      assert @user.has_verified_email?("hello_a@example.com")
      refute @user.has_verified_email?("hello_b@example.com")
    end
  end

  describe "#primary_email_identity" do
    before do
      @user = users(:minimum_end_user)
      @user.identities.delete_all
      @user.identities << UserEmailIdentity.new(user: @user, value: "hello_1@example.com", priority: 1)
      @user.identities << UserEmailIdentity.new(user: @user, value: "hello_2@example.com", priority: 2)
      @user.identities << UserEmailIdentity.new(user: @user, value: "hello_3@example.com", priority: 3)
    end

    it "returns the first priority identity" do
      assert_equal @user.primary_email_identity.value, "hello_1@example.com"
    end
  end

  describe "#display_name" do
    before { user.alias = 'Baby Face Nelson' }

    describe "when the alias should be used" do
      let(:user) { users(:minimum_agent) }
      before { Account.any_instance.stubs(has_agent_display_names?: true) }

      it { refute_nil user.display_name }
      it { assert_equal user.alias, user.display_name }
    end

    describe "when the name should be used" do
      let(:user) { users(:minimum_end_user) }

      it { refute_nil user.display_name }
      it { assert_equal user.name, user.display_name }
    end
  end

  describe "#prefer_alias_over_name?" do
    before do
      User.any_instance.stubs(agent_display_name: 'Baby Face Nelson')
      Account.any_instance.stubs(has_agent_display_names?: true)
    end

    it { assert users(:minimum_admin).prefer_alias_over_name? }
    it { assert users(:minimum_agent).prefer_alias_over_name? }
    it { assert users(:multiproduct_owner).prefer_alias_over_name? }
    it { assert users(:multiproduct_light_agent).prefer_alias_over_name? }

    it { refute users(:minimum_end_user).prefer_alias_over_name? }
  end

  describe "#email=" do
    before do
      @user = users(:minimum_end_user)
    end

    it "adds an identity if the user does not have it" do
      number_of_identities = @user.identities.count(:all)
      @user.email = 'mick@zendesk.com'
      @user.save!
      @user.reload
      assert_equal(number_of_identities + 1, @user.identities.count(:all))
      assert @user.identities.email.detect { |i| i.value == "mick@zendesk.com" }
    end

    it "dos nothing if the user already has the email" do
      number_of_identities = @user.identities.count(:all)
      @user.email = 'minimum_end_user@aghassipour.com'
      @user.save
      @user.reload
      assert_equal(number_of_identities, @user.identities.count(:all))
      assert_equal('minimum_end_user@aghassipour.com', @user.identities.email.first.value)
    end

    describe "if the primary identity is unverified" do
      before do
        UserTwitterIdentity.any_instance.stubs(:is_verified?).returns(false)
        UserEmailIdentity.any_instance.stubs(:is_verified?).returns(false)
      end

      describe "Adding a new email " do
        before do
          @primary_identity = @user.identities.select { |i| i.priority == 1 }.first
          @user.email = 'andreanowzick@zendesk.com'
        end

        it "does not change the primary identity" do
          @user.save
          assert_equal 1, @primary_identity.reload.priority
        end
      end
    end

    describe "when the existing identity is lower priority" do
      before do
        @user.email.identities.destroy_all
        @user.email.identities.create! value: "old_primary@bar.com", priority: 1
        @user.email.identities.create! value: "new_primary@bar.com", priority: 2
        @user.reload
        @user.email = 'new_primary@bar.com'
        @user.save!

        assert_equal('new_primary@bar.com', @user.email)
        assert_equal(1, @user.email.identities.detect { |i| i.value == "new_primary@bar.com" }.priority)
        assert_equal(2, @user.email.identities.detect { |i| i.value == "old_primary@bar.com" }.priority)
      end
    end
  end

  describe "Deleting an End User" do
    before do
      @user = users(:minimum_end_user)
      @user.tickets.update_all(status_id: StatusType.CLOSED)

      # Stubs the event call to the Channels Framework
      Channels::AnyChannel::UserDestroyEventHandler.stubs(:user_deleted)
    end

    describe "without current_user set" do
      it "does not change important fields" do
        old_active = @user.is_active
        assert_nil @user.password
        old_email = @user.email.dup
        refute_difference 'User.count(:all)' do
          assert_raises(RuntimeError) { @user.delete! }
        end
        @user.is_active.must_equal old_active
        assert_nil @user.password
        @user.email.must_equal old_email
      end
    end

    it "changes the password" do
      @user.current_user = users(:minimum_admin)
      assert @user.crypted_password == '123456'
      @user.delete!

      refute (@user.crypted_password == '123456')
    end

    describe "with current_user set" do
      before do
        @user.current_user = users(:minimum_admin)
        @user.delete!
      end

      should_not_change("the number of users") { User.count(:all) }
      should_change("the email address") { @user.email }

      should_change("is_active", from: true, to: false) { @user.is_active }
      it "resets the external_id" do
        assert_nil @user.external_id
      end

      before_should "not create an UnassignTicketsJob" do
        UnassignTicketsJob.expects(:create!).never
      end
    end

    describe "with user created content" do
      it "deletes the content" do
        @user = users(:minimum_author)
        @user.tickets.update_all(status_id: StatusType.CLOSED)

        assert @user.posts.any?
        assert @user.entries.any?

        @user.current_user = users(:minimum_admin)
        @user.delete!(remove_content: true)
        @user.reload

        assert_empty(@user.posts)
        assert_empty(@user.entries)
      end
    end

    describe "with active subscriptions" do
      it "destroys the subscriptions" do
        @forum = forums(:announcements)
        @entry = entries(:sticky)
        @forum.subscribe(@user)
        @entry.subscribe(@user)
        @user.current_user = users(:minimum_admin)
        @user.delete!
        @user.reload

        assert_empty(@user.watchings)
        refute @forum.subscribed?(@user)
        refute @entry.subscribed?(@user)
      end
    end

    describe "with user_identities (emails)" do
      it "destroys all the user_identities (emails)" do
        assert @user.identities.count(:all) > 0
        @user.current_user = users(:minimum_admin)
        @user.delete!
        assert_equal 0, @user.identities.count(:all)
      end
    end

    describe "with unverified email addresses" do
      before do
        @user.current_user = users(:minimum_admin)
        UnverifiedEmailAddress.create!(user: @user, email: 'email@example.com')
      end

      it "destroys the unverified email address" do
        assert_difference 'UnverifiedEmailAddress.count(:all)', -1 do
          @user.delete!
        end
      end
    end
  end

  describe "Removing all email identities from a user" do
    it "does not be allowed for agents" do
      user = users(:minimum_agent)
      user.stubs(:email).returns(nil)
      refute user.save
    end

    it "is allowed for end-users" do
      user = users(:minimum_end_user)
      user.stubs(:email).returns(nil)
      assert user.save
    end
  end

  describe "#first_name and #last_name" do
    describe "with 'John Doe' format" do
      before { @user = User.new(name: 'John Doe') }

      it "identifys the correct first and last name" do
        assert_equal 'John', @user.first_name
        assert_equal 'Doe', @user.last_name
      end
    end

    describe "with 'Doe, John' format" do
      before { @user = User.new(name: 'Doe, John') }

      it "identifys the correct first and last name" do
        assert_equal 'John', @user.first_name
        assert_equal 'Doe', @user.last_name
      end
    end

    describe "with 'John Doe | TAMPA' format" do
      before { @user = User.new(name: 'John Doe | TAMPA') }

      it "identifys the correct first and last name" do
        assert_equal 'John Doe ', @user.first_name
        assert_equal ' TAMPA', @user.last_name
      end
    end
  end

  describe "#has_last_name?" do
    describe "with 'John Doe' format" do
      before { @user = User.new(name: 'John Doe') }

      it "returns true" do
        assert @user.has_last_name?
      end
    end

    describe "with 'John' format" do
      before { @user = User.new(name: 'John') }

      it "returns false" do
        refute @user.has_last_name?
      end
    end
  end

  describe "A new, unverified user" do
    before do
      @user = accounts(:minimum).users.new(
        name: 'Bud Dha', email: 'mentor@example.com'
      )
    end

    describe "with send verify email enabled" do
      before do
        @user.send_verify_email = true
      end

      it "enables verification emails" do
        assert(@user.send_verify_email)
      end

      it "disables verification emails when skipping verification" do
        @user.skip_verification = true
        assert_equal false, @user.send_verify_email
      end
    end
  end

  describe "#last_login" do
    before do
      @user = users(:minimum_end_user)
    end

    it "sets the last_login attribute if it doesn't exist" do
      @user.update_attribute(:last_login, nil)
      assert_nil @user.last_login
      @user.update_last_login
      assert @user.reload.last_login
    end

    it "does not update the attribute if found and updated less than 20 minutes ago" do
      @user.update_attribute(:last_login, 10.minutes.ago)
      last_login = @user.last_login
      @user.update_last_login
      assert_equal last_login.to_i, @user.reload.last_login.to_i
    end

    it "updates the attribute if found and updated less than 20 minutes ago" do
      @user.update_attribute(:last_login, 30.minutes.ago)
      last_login = @user.last_login
      @user.update_last_login
      assert last_login < @user.reload.last_login
    end

    it "dos nothing for the system user" do
      user = User.find(-1)
      assert_nil user.last_login
      user.update_last_login
      assert_nil user.last_login
    end
  end

  describe "user merge helpers" do
    let(:agent)  { users(:minimum_agent) }
    let(:user) { users(:minimum_end_user) }
    let(:other_user) { users(:minimum_end_user2) }

    it "can_be_merged_as_loser? on a valid loser succeeds" do
      assert user.can_be_merged_as_loser?
    end
    it "can_be_merged_as_loser? on an invalid loser fails" do
      refute agent.can_be_merged_as_loser?
    end
    it "can_be_merged_as_winner? on a valid winner succeeds" do
      assert user.can_be_merged_as_winner?
    end
    it "can_be_merged_as_winner? on an invalid winner fails" do
      refute agent.can_be_merged_as_winner?
    end
    it "can_be_merged_with? on a valid loser passing a valid winner succeeds" do
      assert user.can_be_merged_into?(other_user)
    end
    it "can_be_merged_with? on an invalid loser passing a valid winner fails" do
      refute agent.can_be_merged_into?(user)
    end
    it "can_be_merged_with? on a valid loser passing an invalid winner fails" do
      refute user.can_be_merged_into?(agent)
    end
  end

  describe "#should_ask_for_email?" do
    it "does not ask anonymous users" do
      refute User.new.should_ask_for_email?
    end

    it "does not ask users with any email identity" do
      user = FactoryBot.create(:end_user, account: accounts(:minimum))
      assert !user.identities.email.empty?
      refute user.should_ask_for_email?
    end

    it "does not ask users with any unverified email identities" do
      user = FactoryBot.create(:end_user, account: accounts(:minimum))
      UserIdentity.delete(user.identities.map(&:id))
      user.unverified_email_addresses.create!(email: 'user@example.com')
      user.reload
      refute user.should_ask_for_email?
    end

    it "asks if non-anonymous and without any email" do
      user = FactoryBot.create(:end_user, account: accounts(:minimum))
      UserIdentity.delete(user.identities.map(&:id))
      user.reload
      assert user.should_ask_for_email?
    end
  end

  describe "#available_languages" do
    before do
      @account = accounts(:minimum)
      @locale1 = translation_locales(:spanish)
      @locale2 = translation_locales(:japanese)
      Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
      Account.any_instance.stubs(:available_languages).returns([@locale2, @locale1])
    end
    it "returns the accounts available languages for end users when subscription has individual language selection" do
      user = FactoryBot.create(:end_user, account: @account)
      assert_equal @account.available_languages, user.available_languages
    end
    it "returns only the accounts default language for end users when subscription doesn't have individual language selection" do
      Subscription.any_instance.stubs(:has_individual_language_selection?).returns(false)
      user = FactoryBot.create(:end_user, account: @account)
      assert_equal [@account.translation_locale], user.available_languages
    end
    it "returns only english for agents" do
      user = FactoryBot.create(:agent, account: @account)
      assert_equal [ENGLISH_BY_ZENDESK], user.available_languages
    end
  end

  describe 'Given an account with two available languages' do
    before do
      @account = accounts(:minimum)
      @account.stubs(:available_languages).returns(
        [
          translation_locales(:english_by_zendesk),
          translation_locales(:japanese)
        ]
      )

      locale = @account.available_languages.first

      @user = User.create!(
        account: @account,
        name: "Yuki Wombat",
        email: 'yuki@whistler.com',
        locale_id: locale.id
      )

      assert_equal(2, @account.available_languages.size)
      assert_equal(2, @user.account.available_languages.size)
    end

    describe 'And a user with two tickets in one of the locales' do
      before do
        Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
        ticket1 = @account.tickets.build(requester: @user, description: 'Snow!')
        ticket2 = @account.tickets.build(requester: @user, description: 'Board!')
        ticket1.will_be_saved_by(@user)
        ticket2.will_be_saved_by(@user)
        ticket1.save
        ticket2.save
      end

      describe 'Changing that users locale' do
        before do
          @user.update_attributes(locale_id: 4)
          @user.reload
        end

        it "also changes their tickets’ locales" do
          @user.tickets.each do |t|
            assert_equal 4, t.locale_id
          end
        end
      end
    end
  end

  it "tests user for minimum account" do
    # Attempt new user with existing email
    user = User.new name: 'Monica Bellis', email: 'minimum_end_user@aghassipour.com', account: accounts(:minimum)
    refute user.save

    # End user
    user = User.new name: 'Monica Bellis', email: 'mb@aghassipour.com', account: accounts(:minimum), notes: 'nice!', details: 'none'
    assert user.save!, user.errors.full_messages.join('; ')
    assert_equal user.account, accounts(:minimum)
    assert user.is_active?

    assert_equal 0, user.roles
    assert_empty(user.groups)
    refute user.is_verified?
    assert user.created_at
    assert_equal organizations(:minimum_organization1), user.organization # Organization set according to default rules for organization

    # Operator
    user = User.new do |u|
      u.name = 'Monica Bell'
      u.email = 'm@aghassipour.com'
      u.account = accounts(:minimum)
      u.roles = Role::END_USER.id
    end
    assert user.save!, user.errors.full_messages.join('; ')
    assert_equal user.account, accounts(:minimum)
    assert user.is_active?
    assert_equal 0, user.roles
    refute user.is_verified?
    assert_equal 0, user.groups.length

    user.update_last_login
    assert user.reload.last_login
  end

  describe "tests role combinations" do
    let(:account) { accounts(:minimum) }
    let(:user) do
      User.new do |u|
        u.name = 'John Dolk'
        u.email = 'jd@monsterpig.com'
        u.account = account
        u.roles = -4
      end
    end

    before do
      CIA.stubs(:current_actor).returns(user)
      user.current_user = user
    end

    it 'fails to save user with invalid roles' do
      refute user.save
    end

    it 'saves user as ADMIN' do
      user.roles = Role::ADMIN.id
      assert user.save
      assert_equal 0, user.groups(true).size
      assert user.is_moderator?
    end

    it 'saves user as end user' do
      user.roles = Role::END_USER.id
      assert user.save
      assert_equal 0, user.groups(true).size
      refute user.is_moderator?
    end

    describe 'saves user as agent' do
      it 'does not enforce is_moderator value' do
        user.roles = Role::AGENT.id

        user.is_moderator = false
        assert user.save
        refute user.is_moderator?

        user.is_moderator = true
        assert user.save
        assert user.is_moderator?
      end
    end

    describe 'when multiproduct has Contributor role' do
      let(:account) { accounts(:multiproduct) }
      let(:contributor_permission_set) { account.permission_sets.where(role_type: PermissionSet::Type::CONTRIBUTOR).first }

      before { account.stubs(:has_active_suite?).returns(false) }

      it 'saving user as contributor enforces is_moderator to be false' do
        user.permission_set = contributor_permission_set
        user.roles = Role::AGENT.id

        user.is_moderator = true
        assert user.save
        refute user.is_moderator?
      end
    end
  end

  describe "Downgrade agent to end-user" do
    before do
      @agent = users(:minimum_agent)
      assert_equal 1, @agent.views.count(:all)
      assert_equal 1, @agent.macros.count(:all)

      # Ticket unassignment expectation
      @agent.current_user = users(:minimum_admin)
      Resque.expects(:enqueue).with(UnassignTicketsJob, @agent.account_id, nil, @agent.id, @agent.current_user.id, ViaType.USER_CHANGE, anything, anything)

      # Downgrade to end user
      @agent.roles = Role::END_USER.id
      @agent.save!
    end

    should_destroy :view
    should_destroy :macro

    it "clears the views and macros in this user" do
      assert_empty(@agent.views.reload)
      assert_empty(@agent.macros.reload)
    end
  end

  describe "#Demote agent to Light Agent" do
    before do
      @account = accounts(:minimum)
      Account.any_instance.stubs(:has_light_agents?).returns(true)
      @permission_set = PermissionSet.create_light_agent!(@account)

      @agent = users(:minimum_agent)
      assert_not_nil @agent.assigned.not_closed
    end

    it "throws an error when the agent has assigned tickets" do
      assert_raise(ActiveRecord::RecordInvalid) do
        @agent.update_attributes! name: "Light Agent", permission_set: @permission_set
      end
    end
  end

  describe "User#is_verified = true" do
    it "verifys the primary identity on existing users" do
      user = users(:to_be_verified)
      refute user.is_verified?
      refute user.identities.first.is_verified?
      user.is_verified = true
      user.save!
      assert user.is_verified?
      assert user.identities.first.is_verified?
    end

    it "ignores attempts to set is_verified to nil" do
      unverified_user = users(:to_be_verified)
      verified_user = users(:minimum_end_user)
      verified_user.is_verified = true
      verified_user.save!

      refute unverified_user.is_verified?
      refute unverified_user.identities.first.is_verified?
      assert verified_user.is_verified?
      assert verified_user.identities.first.is_verified?

      unverified_user.is_verified = nil
      unverified_user.save!
      verified_user.is_verified = nil
      verified_user.save!

      refute unverified_user.is_verified?
      refute unverified_user.identities.first.is_verified?
      assert verified_user.is_verified?
      assert verified_user.identities.first.is_verified?
    end

    it "autoverifys new identities if user is verified after setting email" do
      user = accounts(:minimum).users.new(name: 'Mick Staugaard')
      user.email = 'mick1@zendesk.com'
      user.password = '123456'
      user.is_verified = true
      user.save!
      assert user.is_verified?
      assert user.identities.first.is_verified?
    end

    it "autoverifys new identities if user is verified before setting email" do
      user = accounts(:minimum).users.new(name: 'Mick Staugaard')
      user.is_verified = true
      user.password = '123456'
      user.email = 'mick2@zendesk.com'
      user.save!
      assert user.is_verified?
      assert user.identities.first.is_verified?
    end
  end

  describe "#is_verified?" do
    it "only operate on persisted records" do
      user = users(:to_be_verified)
      user.stubs(:identities).returns([stub(is_verified?: false, new_record?: false)])
      refute user.is_verified?
      user.stubs(:identities).returns([stub(is_verified?: false, new_record?: false), stub(is_verified?: true, new_record?: true)])
      refute user.is_verified?
    end
  end

  %w[macros views].each do |rule_type|
    describe "User#shared_#{rule_type}" do
      before do
        @user = users(:minimum_agent)

        assert @user.groups.present?, "In this context, the user must belong to at least one group"

        @active_group_rules, @inactive_group_rules = @user.groups.map(&rule_type.to_sym).flatten.partition(&:is_active?)
        @active_account_rules, @inactive_account_rules = @user.account.send(rule_type).partition(&:is_active?)

        if rule_type == 'macros'
          GroupMacro.destroy_all

          [*@active_group_rules, *@inactive_group_rules].each do |group_rule|
            GroupMacro.create! do |group_macro|
              group_macro.account_id = @account.id
              group_macro.group_id   = Group.find(group_rule.owner_id).id
              group_macro.macro_id   = group_rule.id
            end
          end
        end
      end

      it "includes active #{rule_type} owned by the account that the user belongs to" do
        assert @active_account_rules.present?,
          "For this test, the user's account must have at least one active #{rule_type.singularize}"

        @active_account_rules.each do |rule_owned_by_account|
          assert @user.send("shared_#{rule_type}").include?(rule_owned_by_account),
            "This user's shared #{rule_type} should include #{rule_owned_by_account.title} (#{rule_owned_by_account.owner_type}, #{rule_owned_by_account.owner_id})."
        end
      end

      it "includes inactive #{rule_type} owned by the account that the user belongs to" do
        assert @inactive_account_rules.present?,
          "For this test, the user's account must have at least one inactive #{rule_type.singularize}"

        @inactive_account_rules.each do |rule_owned_by_account|
          assert @user.send("shared_#{rule_type}").include?(rule_owned_by_account)
        end
      end

      describe "for a user whose account supports group #{rule_type}" do
        before { @user.account.subscription.stubs(:has_group_rules?).returns(true) }

        it "includes active #{rule_type} owned by any group that the user is in" do
          assert @active_group_rules.present?,
            "For this test, at least one of the user's groups must have an active #{rule_type.singularize}"

          @active_group_rules.each do |rule_owned_by_group|
            assert @user.send("shared_#{rule_type}").include?(rule_owned_by_group),
              "This user's shared #{rule_type} should include #{rule_owned_by_group.title} (#{rule_owned_by_group.owner_type}, #{rule_owned_by_group.owner_id})."
          end
        end

        it "includes inactive #{rule_type} owned by any group that the user is in" do
          assert @inactive_group_rules.present?,
            "For this test, at least one of the user's groups must have an inactive #{rule_type.singularize}"

          @inactive_group_rules.each do |rule_owned_by_group|
            assert @user.send("shared_#{rule_type}").include?(rule_owned_by_group)
          end
        end
      end

      describe "for a user whose account does not support group #{rule_type}" do
        before { @user.account.subscription.stubs(:has_group_rules?).returns(false) }

        it "does not include any #{rule_type} owned by a Group" do
          assert @active_group_rules.present? || @inactive_group_rules.present?,
            "For this test, at least one of the user's groups must have a #{rule_type.singularize}"

          @user.send("shared_#{rule_type}").each { |rule| refute rule.owner.is_a?(Group) }
        end
      end
    end
  end

  describe 'shared and personal macros' do
    let(:user)  { users(:minimum_agent) }
    let(:macro) { rules(:macro_incident_escalation) }

    it 'supports pagination' do
      assert user.shared_and_personal_macros.paginate(page: 1)
    end

    describe 'when the macro is shared' do
      before do
        macro.owner = user.account
        macro.save!
      end

      it 'includes the macro' do
        assert user.shared_and_personal_macros.include?(macro)
      end
    end

    describe 'when the macro is personal' do
      before do
        macro.owner = user
        macro.save!
      end

      it 'includes the macro' do
        assert user.shared_and_personal_macros.include?(macro)
      end
    end

    it 'orders by position' do
      assert_equal(
        user.shared_and_personal_macros.sort_by(&:position).map(&:position),
        user.shared_and_personal_macros.map(&:position)
      )
    end

    it 'orders by shared, then personal macros' do
      assert_equal(
        user.shared_and_personal_macros.sort_by do |macro|
          %w[Account Group User].index(macro.owner_type)
        end,
        user.shared_and_personal_macros
      )
    end

    describe 'when the `macro_order` setting is `alphabetical`' do
      before do
        @account.settings.set(macro_order: 'alphabetical')

        @account.settings.save!
      end

      it 'sorts shared macros alphabetically' do
        assert_equal(
          user.shared_and_personal_macros.sort_by(&:title).map(&:title),
          user.shared_and_personal_macros.map(&:title)
        )
      end
    end

    it 'sorts shared macros alphabetically' do
      assert_equal(
        user.shared_macros.sort_by(&:title).map(&:title),
        user.shared_macros.map(&:title)
      )
    end

    describe 'for group macros' do
      let(:macro) { rules(:macro_for_minimum_group) }

      let(:account) { user.account }
      let(:group)   { groups(:minimum_group) }

      before do
        GroupMacro.destroy_all

        GroupMacro.create! do |group_macro|
          group_macro.account_id = account.id
          group_macro.group_id   = group.id
          group_macro.macro_id   = macro.id
        end
      end

      describe 'when the `group_rules` feature is enabled' do
        before { Subscription.any_instance.stubs(has_group_rules?: true) }

        it 'includes the macro' do
          assert_includes user.shared_and_personal_macros, macro
        end

        describe 'and the user does not belong to the group' do
          before { user.groups.clear }

          it 'does not include the macro' do
            refute_includes user.shared_and_personal_macros, macro
          end
        end
      end

      describe 'without the `groups_rules` feature' do
        before { Subscription.any_instance.stubs(has_group_rules?: false) }

        it 'does not include the macro' do
          refute_includes user.shared_and_personal_macros, macro
        end
      end
    end
  end

  describe 'Shared macros' do
    let(:user) { users(:minimum_agent) }

    before do
      GroupMacro.destroy_all

      sorted_macros = user.shared_macros.sort_by(&:title)
      sorted_macros.first.update_attribute(:position, sorted_macros.last.position + 1)
      assert sorted_macros.size >= 2
      user.reload
    end

    describe 'when the `macro_order` setting is `alphabetical`' do
      before do
        @account.settings.set(macro_order: 'alphabetical')

        @account.settings.save!
      end

      it 'sorts shared macros alphabetically' do
        assert_equal(
          user.shared_macros.sort_by(&:title).map(&:title),
          user.shared_macros.map(&:title)
        )
      end
    end

    describe 'when the `macro_order` setting is `position`' do
      before do
        @account.settings.set(macro_order: 'position')

        @account.settings.save!
      end

      it 'sorts macros by position' do
        assert_equal(
          user.shared_macros.sort_by(&:position).map(&:position),
          user.shared_macros.map(&:position)
        )
      end
    end

    describe 'when the account has group rules' do
      before do
        Subscription.any_instance.stubs(has_group_rules?: true)
      end

      describe 'when there are macros with `Rule.group_owner_id` as the owner id' do
        let(:account)        { user.account }
        let(:user_group)     { account.groups.first }
        let(:non_user_group) { groups(:billing_group) }

        let!(:user_group_macro) do
          definition = Definition.new.tap do |new_definition|
            new_definition.actions.push(
              DefinitionItem.new('set_tags', nil, ['test'])
            )
          end

          Macro.create!(
            account:    account,
            title:      'Macro 1',
            owner_id:   Rule.group_owner_id,
            owner_type: 'Group',
            definition: definition,
            is_active:  true,
            group_id:   user_group.id
          )
        end

        let!(:non_user_group_macro) do
          definition = Definition.new.tap do |new_definition|
            new_definition.actions.push(
              DefinitionItem.new('set_tags', nil, ['test2'])
            )
          end

          Macro.create!(
            account:    account,
            title:      'Other macro',
            owner_id:   Rule.group_owner_id,
            owner_type: 'Group',
            definition: definition,
            is_active:  true,
            group_id:   non_user_group.id
          )
        end

        before do
          Macro.where(owner_type: 'Account').destroy_all

          GroupMacro.create! do |group_macro|
            group_macro.account_id = account.id
            group_macro.group_id   = user_group.id
            group_macro.macro_id   = user_group_macro.id
          end

          GroupMacro.create! do |group_macro|
            group_macro.account_id = account.id
            group_macro.group_id   = non_user_group.id
            group_macro.macro_id   = non_user_group_macro.id
          end
        end

        it "includes macros associated with one of the users's groups" do
          assert_includes user.shared_macros, user_group_macro
        end

        it "does not include macros not associated with one of the users's groups" do
          refute_includes user.shared_macros, non_user_group_macro
        end
      end

      describe 'when the owner is an inactive group the user is a member of' do
        let(:macro) { user.shared_macros.first }

        let(:account) { user.account }
        let(:group)   { groups(:inactive_group) }

        before do
          Membership.create! do |m|
            m.account = account
            m.user    = user
            m.group   = group
          end

          macro.update_attributes(owner_type: 'Group', owner_id: group.id)

          user.reload
        end

        it 'does not include the macro' do
          refute_includes user.shared_macros, macro
        end

        describe 'and the group becomes active' do
          before do
            group.is_active = true

            group.save!
          end

          it 'includes the macro' do
            assert_includes user.shared_macros, macro
          end
        end
      end
    end
  end

  describe 'Personal macros' do
    let(:user) { users(:minimum_agent) }

    before do
      user.macros.create!(user.macros.first.attributes)
      user.reload

      sorted_macros = user.personal_macros.sort_by(&:title)
      sorted_macros.first.update_attribute(:position, user.personal_macros.last.position + 1)

      assert sorted_macros.size >= 2

      user.reload
    end

    it 'sorts the macros alphabetically' do
      assert_equal(
        user.personal_macros.sort_by(&:title).map(&:title),
        user.personal_macros.map(&:title)
      )
    end
  end

  describe "View sorting" do
    before do
      @user = users(:minimum_agent)
      sorted_views = @user.shared_views.sort_by(&:title)
      sorted_views.first.update_attribute(:position, sorted_views.last.position + 1)
      assert sorted_views.size >= 2
      @user.reload
    end

    it "sorts shared views by position" do
      assert_equal @user.shared_views.sort_by(&:position).map(&:position), @user.shared_views.map(&:position)
    end

    describe "user_level_shared_view_ordering" do
      describe "disabled" do
        it "maintains the default behavior" do
          assert_equal @user.shared_views.sort_by(&:position).map(&:position), @user.shared_views.map(&:position)
        end
      end

      describe "enabled" do
        before do
          Arturo.enable_feature!(:user_level_shared_view_ordering)
          UserText.where(name: 'shared_views_order').without_arsi.delete_all
        end

        it "doesn't order the views at all if skip_ordering is passed in" do
          User.any_instance.expects(:order_shared_views).never
          @user.shared_views(skip_ordering: true)
        end

        describe "shared_views_order user setting" do
          let(:position_sorted_view_ids) { @user.shared_views.sort_by(&:position).map(&:id) }

          describe "not set" do
            it "keeps default ordering" do
              assert_equal position_sorted_view_ids, @user.shared_views.map(&:id)
            end
          end

          describe "set to nil" do
            before do
              @user.texts.shared_views_order = nil
              @user.save!
            end

            it "does not blow up and defaults to an empty array" do
              assert_equal [], @user.shared_views_order
            end
          end

          describe "set to a subset of available views" do
            before do
              @user.texts.shared_views_order = []
              @user.save!
            end

            it "renders the subset in the desired order and adds the remaining ordered views after" do
              current_shared_views = @user.shared_views.map(&:id)
              views_to_reorder = current_shared_views.pop(3).reverse
              new_order = views_to_reorder + current_shared_views

              @user.texts.shared_views_order = views_to_reorder

              assert_equal new_order, @user.shared_views.map(&:id)
            end
          end
        end
      end
    end
  end

  %w[macros views].each do |rule_type|
    describe "User#personal_#{rule_type}" do
      before { @user = users(:minimum_agent) }

      describe "for a user whose account supports personal #{rule_type}" do
        before do
          @user.account.subscription.stubs(:has_personal_rules?).returns(true)
          @rule = @user.send(rule_type).first.dup
          @rule.account_id = accounts(:support).id
          @rule.definition.actions.delete_if { |a| a.source == "group_id" } if rule_type == 'macros'
          @rule.save!
        end

        it "returns the user's #{rule_type} from the same account" do
          expected_rules = @user.send(rule_type)
          assert expected_rules.present?, "For this test, the user must have some #{rule_type}"

          personal_rules = @user.send("personal_#{rule_type}")

          personal_rules.each do |personal_rule|
            assert expected_rules.include?(personal_rule)
            assert_equal @user.account.id, personal_rule.account_id
          end
        end
      end

      describe "for a user whose account does not support personal #{rule_type}" do
        before { @user.account.subscription.stubs(:has_personal_rules?).returns(false) }

        it "returns an empty array" do
          assert_equal [], @user.send("personal_#{rule_type}")
        end
      end
    end
  end

  describe "aggregate views" do
    before do
      @user = users(:minimum_agent)
      @view = rules(:view_my_working_tickets)
    end

    it "includes shared views" do
      @view.owner = @user.account
      @view.save!
      views = @user.aggregate_views(types: [:shared])

      assert views.include?(@view), views.inspect
    end

    it "includes personal views" do
      @view.owner = @user
      @view.save!
      views = @user.aggregate_views(types: [:personal])

      assert views.include?(@view), views.inspect
    end

    it "orders by shared, then personal" do
      views = @user.aggregate_views(types: [:shared, :personal]).map(&:owner_type)

      assert_equal views.sort_by { |v| %w[Account Group User].index(v) }, views
    end

    it "supports pagination" do
      assert @user.aggregate_views(types: [:shared, :personal]).paginate(page: 1)
    end

    it "allows reducing the result set" do
      @view.owner = @user
      @view.save!
      views = @user.aggregate_views(types: [:shared, :personal], only: [@view.id + 1, @view.id - 1])
      refute views.include?(@view), views.inspect
    end

    describe "with admin" do
      before do
        Subscription.any_instance.stubs(:has_group_rules?).returns(true)
        @user = users(:minimum_admin)
        @user.groups.clear
        @view = rules(:active_view_for_minimum_group)
      end

      describe "with only_viewable unset" do
        it "does not include all group macros" do
          refute @user.aggregate_views(types: [:shared, :personal]).include?(@view)
        end
      end

      describe "with only_viewable => true" do
        it "does not include all group macros" do
          refute @user.aggregate_views(types: [:shared, :personal], only_viewable: true).include?(@view)
        end
      end

      describe "with only_viewable => false" do
        it "includes all group macros" do
          assert @user.aggregate_views(types: [:shared, :personal], only_viewable: false).include?(@view)
        end
      end
    end
  end

  describe "has identity" do
    before do
      @user = users(:minimum_agent)
    end

    it "checks facebook" do
      assert_equal false, @user.has_facebook_identity?
      UserFacebookIdentity.create!(user: @user, account: @user.account, value: 123)

      assert(@user.has_facebook_identity?)
    end

    it "checks google" do
      assert_equal false, @user.has_google_identity?
      identity = @user.identities.email.first
      identity.build_google_profile
      identity.save!

      assert(@user.has_google_identity?)
    end

    it "checks twitter" do
      assert_equal false, @user.has_twitter_identity?
      UserTwitterIdentity.any_instance.stubs(:twitter_user_profile).returns(stub)
      UserTwitterIdentity.create!(user: @user, account: @user.account, value: 123)

      assert(@user.has_twitter_identity?)
    end

    it "checks AnyChannel" do
      assert_equal false, @user.has_any_channel_identity?
      UserAnyChannelIdentity.create!(user: @user, account: @user.account, value: '12:34')

      assert(@user.has_any_channel_identity?)
    end
  end

  describe "#is_email_google?" do
    before do
      @user = users(:minimum_agent)
    end

    it "returns true if email identity is google identity" do
      @user.identities.first.build_google_profile.save!
      assert @user.is_email_google?
    end

    it "returns false if other email identity is google identity" do
      @identity = UserEmailIdentity.create(account: @account, user: @user, value: "auxillary-email@example.com")
      @identity.build_google_profile.save!
      refute @user.is_email_google?
    end

    it "returns false if email identity is not google identity" do
      refute @user.is_email_google?
    end
  end

  describe "#machine?" do
    let(:email_identity) { @user.identities.email.first }

    before { @user = users(:minimum_end_user) }

    it "returns false by default" do
      refute @user.machine?
    end

    it "returns true if primary email identity is a machine" do
      # marking an identity will drop its priority for being the primary identity. see User#email_identity_for_email_accessor in zendesk_core user.rb
      @user.identities.email.each { |identity| identity.delete if identity != email_identity }
      email_identity.mark_as_machine
      assert @user.reload.machine?
      assert_equal 1, email_identity.priority
    end

    describe "when non primary email identity is a machine" do
      before do
        @new_identity = UserEmailIdentity.create(account: @account, user: @user, value: "another-example@somewhere.com", priority: 2)
        @new_identity.mark_as_machine
      end

      describe "when user setting for machine is true" do
        before do
          @user.settings.machine = true
          @user.save!
        end

        it { refute @user.machine? }
      end
    end
  end

  describe "#email_is_deliverable?" do
    before { @user = users(:minimum_end_user) }

    it "returns true by default" do
      assert @user.email_is_deliverable?
    end

    it "returns true if email identity is not deliverable but an alternate deliverable identity exists" do
      assert @user.identities.email.size > 1
      @user.identities.first.mark_as_ticket_sharing_partner
      assert @user.email_is_deliverable?
    end

    it "returns false if all email identities are not deliverable" do
      @user.identities.email.each(&:mark_as_ticket_sharing_partner)
      refute @user.email_is_deliverable?
    end

    describe 'when there is no identities' do
      before { @user.stubs(:identities).returns([]) }

      it "returns nil (won't raise any exception)" do
        assert_nil @user.email_is_deliverable?
      end
    end
  end

  describe "User#can_view_forum?" do
    describe "with a Forum whose visibility_restriction is everybody" do
      before do
        @forum = Forum.new(visibility_restriction: VisibilityRestriction::EVERYBODY)
      end

      it "returns true for an anonymous user" do
        assert accounts(:minimum).anonymous_user.can?(:view, @forum)
      end

      it "returns true for an end user" do
        assert users(:minimum_end_user).can?(:view, @forum)
      end

      it "returns true for an agent" do
        assert users(:minimum_agent).can?(:view, @forum)
      end
    end

    describe "with a Forum whose visibility_restriction is logged_in_only" do
      before do
        @forum = Forum.new(visibility_restriction: VisibilityRestriction::LOGGED_IN_USERS)
      end

      it "returns false for an anonymous user" do
        refute accounts(:minimum).anonymous_user.can?(:view, @forum)
      end

      it "returns true for an end user" do
        assert users(:minimum_end_user).can?(:view, @forum)
      end

      it "returns true for an agent" do
        assert users(:minimum_agent).can?(:view, @forum)
      end
    end

    describe "with a Forum whose visibility_restriction is agents_only" do
      before do
        @forum = Forum.new(visibility_restriction: VisibilityRestriction::AGENTS_ONLY)
      end

      it "returns false for an anonymous user" do
        refute accounts(:minimum).anonymous_user.can?(:view, @forum)
      end

      it "returns false for an end user" do
        refute users(:minimum_end_user).can?(:view, @forum)
      end

      it "returns true for an agent" do
        assert users(:minimum_agent).can?(:view, @forum)
      end
    end

    describe "with a tag restricted Forum" do
      before do
        @forum = forums(:solutions)
        accounts(:minimum).settings.has_user_tags = '1'
        accounts(:minimum).settings.save!
      end

      it "returns false for an anonymous user" do
        refute accounts(:minimum).anonymous_user.can?(:view, @forum)
      end

      it "returns false for an end user without proper tags" do
        refute users(:minimum_author).can?(:view, @forum)
      end

      it "returns true for an end user with proper tags" do
        assert users(:minimum_end_user).can?(:view, @forum)
      end

      it "returns true for an agent" do
        assert users(:minimum_agent).can?(:view, @forum)
      end
    end
  end

  describe 'User#accessible_forums' do
    before do
      accounts(:minimum).settings.has_user_tags = '1'
      accounts(:minimum).settings.save!
    end

    it "returns only forums with no tag restrictions for an anonymous user" do
      assert_equal 2, accounts(:minimum).anonymous_user.accessible_forums.count(:all)
    end

    it "includes relevant tag restricted forums for a user with matching tags" do
      assert_equal 4, users(:minimum_end_user).accessible_forums.count(:all)
    end

    it "includes not include any tag restricted forums for a user with no forum matching tags" do
      User.any_instance.stubs(:all_tags).returns(['mogens'])
      assert_equal 3, users(:minimum_end_user).accessible_forums.count(:all)
    end

    it "returns all forums for an agent" do
      assert_equal 6, users(:minimum_agent).accessible_forums.count(:all)
    end
  end

  describe "#to_liquid" do
    describe "language" do
      before do
        @user = users(:minimum_end_user)
        @brazilian = translation_locales(:brazilian_portuguese)
        @user.stubs(:translation_locale).returns(@brazilian)
      end
      it "returns user translation locale name" do
        assert_equal @brazilian.name, @user.to_liquid['language'].call(nil)
      end
    end

    describe "locale" do
      before do
        @user = users(:minimum_end_user)
        @brazilian = translation_locales(:brazilian_portuguese)
        @user.stubs(:translation_locale).returns(@brazilian)
      end
      it "returns the locale of the user translation" do
        assert_equal @brazilian.locale, @user.to_liquid['locale'].call(nil)
      end
    end

    describe 'tags' do
      before do
        @user = users(:minimum_end_user)
        @user.account.settings.stubs(:has_user_tags?).returns(true)
        assert @user.update_attributes(set_tags: 'foo bar baz')
      end

      it 'returns the tags' do
        assert_equal @user.current_tags, @user.to_liquid['tags']
      end
    end
  end

  describe "#main_identity" do
    before { @user = users(:minimum_end_user) }
    it "returns (deleted) if not active" do
      @user.expects(:is_active?).returns(false)
      assert_equal "(deleted)", @user.main_identity
    end
    it "returns the user's email if present" do
      @user.expects(:email).twice.returns("foo@bar.com")
      assert_equal "foo@bar.com", @user.main_identity
    end
    it "returns the user's Twitter screen name if email is not present and a Twitter identity is present" do
      @user.expects(:email).returns(nil)
      @user.expects(:has_twitter_identity?).returns(true)
      @user.stubs(:twitter_profile).returns(mock(screen_name: 'foo'))
      assert_equal "@foo", @user.main_identity
    end
  end

  describe "signatures" do
    it "returns a signature for an an agent with a signature" do
      assert_equal 'Admin name signature', users(:minimum_admin).agent_signature(rich: false)
    end
    it "returns a nil for an agent without a signature" do
      assert_nil users(:minimum_agent).agent_signature(rich: false)
    end
    it "returns a nil for a user" do
      assert_nil users(:minimum_end_user).agent_signature(rich: false)
    end
  end

  describe "#photo_url" do
    it "returns a placeholder image if no photo is associated with the user" do
      assert_match(/default-avatar/, users(:minimum_agent).photo_url)
    end
  end

  describe "#large_photo_url" do
    before { @user = users(:minimum_end_user) }
    it "returns large photo url" do
      @photo = FactoryBot.create(:photo, filename: "large_1.png")
      @user.photo = @photo
      assert_match(/large_1\.png/, @user.large_photo_url)
      assert_match(/#{@user.account.url(mapped: false)}/, @user.large_photo_url)
    end
  end

  it 'is able to suspend a user' do
    user = users(:minimum_end_user)
    refute user.suspended?

    user.suspended = true
    user.save!
    user = User.find(user.id) # reloading
    assert user.suspended?
  end

  describe '#role_name' do
    describe 'on an account owner' do
      before do
        @user = accounts(:minimum).owner
      end

      it 'returns "Owner"' do
        assert_equal 'Owner', @user.role_name_short
      end
    end

    describe 'on an admin' do
      before do
        @user = users(:minimum_admin_not_owner)
      end

      it 'returns "Admin"' do
        assert_equal 'Admin', @user.role_name_short
      end
    end

    describe 'on a restricted agent' do
      before do
        @user = users(:with_groups_agent_groups_restricted)
      end

      it 'returns "Agent" when !care_about_restricted' do
        assert_equal 'Agent', @user.role_name_short(false)
      end

      it 'returns "Restricted agent" when care_about_restricted' do
        assert_equal 'Restricted agent', @user.role_name_short
        assert_equal 'Restricted agent', @user.role_name_short(true)
      end

      it 'simply return "Agent" when on Enterprise (x-large) plan' do
        Subscription.any_instance.stubs(:plan_type).returns(SubscriptionPlanType.ExtraLarge)
        assert_equal 'Agent', @user.role_name_short
        assert_equal 'Agent', @user.role_name
      end
    end

    describe 'on a unrestricted agent' do
      before do
        @user = users(:minimum_agent)
      end

      it 'returns "Agent" when !care_about_restricted' do
        assert_equal 'Agent', @user.role_name_short(false)
      end

      it 'returns "Agent" when care_about_restricted' do
        assert_equal 'Agent', @user.role_name_short
        assert_equal 'Agent', @user.role_name_short(true)
      end
    end

    describe 'on a light agent on Enterprise plan' do
      before do
        @user = users(:with_groups_agent_groups_restricted)
        @user.stubs(:is_light_agent?).returns(true)
        Subscription.any_instance.stubs(:plan_type).returns(SubscriptionPlanType.ExtraLarge)
      end

      it 'returns "Light agent"' do
        assert_equal 'Light agent', @user.role_name_short
        assert_equal 'Light agent', @user.role_name
      end
    end

    describe 'on a chat agent' do
      before do
        @user = users(:with_groups_agent_groups_restricted)
        @user.stubs(:is_chat_agent?).returns(true)
      end

      it 'returns "Chat agent"' do
        assert_equal 'Chat-only agent', @user.role_name_short
        assert_equal 'Chat agent', @user.role_name
      end
    end

    describe 'on a end user' do
      before do
        @user = users(:minimum_end_user)
      end

      it 'returns "User"' do
        assert_equal 'User', @user.role_name_short
      end
    end
  end

  describe '#permission_name' do
    before do
      @agent = users(:minimum_agent)
    end

    describe "in enterprise plan" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
        @agent.stubs(:permission_set).returns(build_valid_permission_set(@agent.account, 'Some role name'))
      end

      it 'returns permission name' do
        assert_equal 'Some role name', @agent.permission_name
      end
    end

    describe "not in enterprise plan" do
      it 'returns role name' do
        assert_equal 'Agent', @agent.permission_name
      end
    end
  end

  describe "#shared_phone_number" do
    before do
      @user = users(:minimum_end_user)
    end

    it "returns nil if phone number has not been passed" do
      @user.phone = nil
      assert_nil @user.shared_phone_number
    end

    it "returns nil if phone number is an empty string" do
      @user.phone = ""
      assert_nil @user.shared_phone_number
    end

    it "returns true if phone number is not identity" do
      @user.phone = "14155556666"
      assert_equal @user.shared_phone_number, true
    end

    it "returns false if phone number is an identity" do
      @user.phone = "14155556666"
      @user.identities << UserPhoneNumberIdentity.create(user: @user, value: "14155556666")
      assert_equal @user.shared_phone_number, false
    end
  end
  describe "#phone=" do
    before do
      @user = users(:minimum_end_user)
      @user.phone = nil
    end

    describe "when account setting is true" do
      before do
        refute(@user.phone)
        Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
      end

      describe "shows errors when voice number is not E164 international formatted" do
        it "contains  non digits" do
          @user.phone = 'invalid'
          refute(@user.valid?)
          refute(@user.errors.empty?)
          assert_equal(Api.error('activerecord.errors.models.user.phone.attributes.value.invalid_e164_format', error: "InvalidFormat", value: @user.phone), @user.errors[:phone][0])
        end

        it "is national format" do
          @user.phone = '(555) 123-4567890'
          refute(@user.valid?)
          refute(@user.errors.empty?)
          assert_equal(Api.error('activerecord.errors.models.user.phone.attributes.value.invalid_e164_format', error: "InvalidFormat", value: @user.phone), @user.errors[:phone][0])
        end

        it "is local format" do
          @user.phone = '123-4567890'
          refute(@user.valid?)
          refute(@user.errors.empty?)
          assert_equal(Api.error('activerecord.errors.models.user.phone.attributes.value.invalid_e164_format', error: "InvalidFormat", value: @user.phone), @user.errors[:phone][0])
        end
      end

      it "shows errors when voice number is an emergency number" do
        @user.phone = '999'
        refute(@user.valid?)
        refute(@user.errors.empty?)
        assert_equal(I18n.t('activerecord.errors.voice.user.phone.no_emergency_numbers', value: @user.phone), @user.errors[:phone][0])
      end
    end
  end

  describe "#voice_number=" do
    before do
      @user = users(:minimum_end_user)
      @user.identities.delete_all
    end

    describe "without existing voice number" do
      before do
        refute(@user.has_voice_number?)
      end

      it "shows errors when voice number is invalid" do
        @user.voice_number = 'invalid'
        @user.save
        refute(@user.errors.empty?)

        # Left-to-right override characters are wrapped around phone numbers in this string,
        # to ensure that they remain LTR even in RTL contexts.
        # Key: activerecord.errors.models.user_identity.attributes.value.invalid
        assert_equal(["cannot be blank", "\u202D\u202C is not properly formatted"], @user.errors[:agent_forwarding])
        refute(@user.has_voice_number?)
      end
    end

    describe "with existing voice number" do
      before do
        @user.identities << UserVoiceForwardingIdentity.create(user: @user, value: "14155556666")
        assert(@user.has_voice_number?)
      end

      it "shows errors when the number is invalid" do
        @user.voice_number = 'invalid'
        @user.save
        refute(@user.errors.empty?)

        # Left-to-right override characters are wrapped around phone numbers in this string,
        # to ensure that they remain LTR even in RTL contexts.
        # Key: activerecord.errors.models.user_identity.attributes.value.invalid
        assert_equal(["cannot be blank", "\u202D\u202C is not properly formatted"], @user.errors[:agent_forwarding])
        assert_nil @user.voice_number
        assert_equal('+14155556666', @user.reload.voice_number)
      end
    end
  end

  describe "#tickets_solved_this_week_received_X_rating" do
    before do
      @agent = users(:minimum_agent)
      @end_user = users(:minimum_end_user)
      @account.tickets.where(assignee_id: @agent.id).each(&:delete)
    end

    it "uses user time_zone not account's" do
      Timecop.freeze('2015-01-01')
      @account.update_column(:time_zone, 'Melbourne')
      @agent.update_column(:time_zone, 'Pacific Time (US & Canada)')

      Timecop.freeze(@account.start_of_week + 1.hour)

      [{satisfaction_score: SatisfactionType.GOOD},
       {satisfaction_score: SatisfactionType.BAD},
       {satisfaction_score: SatisfactionType.GOODWITHCOMMENT, satisfaction_comment: "good comment"},
       {satisfaction_score: SatisfactionType.BADWITHCOMMENT, satisfaction_comment: "bad comment"}].each do |rating_attributes|
        t = @account.tickets.new(subject: "foo1", description: "bar1", requester: @end_user, assignee: @agent, status_id: StatusType.SOLVED)
        t.will_be_saved_by(@account.owner)
        t.save!
        t.attributes = rating_attributes
        t.will_be_saved_by(@end_user)
        t.save!
      end

      assert_equal 2, @agent.tickets_solved_this_week_received_good_rating
      assert_equal 2, @agent.tickets_solved_this_week_received_bad_rating

      Timecop.travel(3.days)

      assert_equal 0, @agent.tickets_solved_this_week_received_good_rating
      assert_equal 0, @agent.tickets_solved_this_week_received_bad_rating
    end
  end

  describe "#tickets_solved_this_week" do
    before do
      @agent = users(:minimum_agent)
      @account.tickets.where(assignee_id: @agent.id).each(&:delete)
    end

    it "uses the user time_zone not the account" do
      Timecop.freeze('2015-01-01')
      @account.update_column(:time_zone, 'Melbourne')
      @agent.update_column(:time_zone, 'Pacific Time (US & Canada)')

      Timecop.freeze(@account.start_of_week + 1.hour)

      t = @account.tickets.new(subject: "foo1", description: "bar1", requester: @account.owner, assignee: @agent, status_id: StatusType.SOLVED)
      t.will_be_saved_by(@account.owner)
      t.save!

      assert_equal 1, @account.tickets_solved_this_week
      assert_equal 1, @agent.tickets_solved_this_week

      Timecop.travel(3.days)

      assert_equal 1, @account.tickets_solved_this_week
      assert_equal 0, @agent.tickets_solved_this_week
    end

    it "reflects the number of tickets solved within the last week" do
      Timecop.freeze(@account.start_of_week.utc - 1.day)

      assert_equal 0, @agent.tickets_solved_this_week

      t = @account.tickets.new(subject: "foo1", description: "bar1", requester: @account.owner, assignee: @agent, status_id: StatusType.SOLVED)
      t.will_be_saved_by(@account.owner)
      t.save!

      assert_equal 1, @agent.tickets_solved_this_week

      Timecop.travel(3.days)

      assert_equal 0, @agent.tickets_solved_this_week

      t = @account.tickets.new(subject: "foo2", description: "bar2", requester: @account.owner, assignee: @agent, status_id: StatusType.SOLVED)
      t.will_be_saved_by(@account.owner)
      t.save!

      assert_equal 1, @agent.tickets_solved_this_week
    end

    it "reflects the number of solved tickets as would be reported by other mechanisms" do
      t1 = @account.tickets.new(subject: "foo1", description: "bar1", requester: @account.owner, assignee: @agent)
      t1.will_be_saved_by(@account.owner)
      t1.save!

      t2 = @account.tickets.new(subject: "foo2", description: "bar2", requester: @account.owner, assignee: @agent)
      t2.will_be_saved_by(@account.owner)
      t2.save!

      assert_equal 0, @agent.tickets_solved_this_week

      Zendesk::Tickets::Merger.new(user: @agent).merge(
        target: t1,
        sources: [t2],
        target_comment: "winner",
        source_comment: "losers",
        target_is_public: true,
        source_is_public: true
      )

      assert_equal StatusType.CLOSED, t2.reload.status_id

      t1.status_id = StatusType.SOLVED
      t1.will_be_saved_by(@account.owner)
      t1.save!

      assert_equal 2, @agent.tickets_solved_this_week
    end
  end

  describe "#translation_locale" do
    before do
      @spanish = translation_locales(:spanish)
      @spanish.localized_agent = true
      @account = accounts(:minimum)
      @end_user = users(:minimum_end_user)
      @end_user.translation_locale = @spanish
      @account.translation_locale = @spanish
      @end_user.account = @account

      @agent = users(:minimum_agent)
      @agent.translation_locale = @spanish
      @agent.account = @account
    end

    it "returns the locale is the locale is set" do
      assert_equal @spanish, @end_user.translation_locale
      assert_equal @spanish, @agent.translation_locale
    end

    describe "when the user's locale doesn't exitst" do
      before do
        @end_user.locale_id = 12345
        @agent.locale_id = 123456
        @end_user.save(validate: false)
        @agent.save(validate: false)
      end

      it "returns the account locale if the user is an end user" do
        @end_user.account.expects(translation_locale: @spanish)
        assert_equal @spanish, @end_user.translation_locale
      end

      it "returns the account locale if the user is an agent and the account locale is available for admin" do
        @agent.account.expects(translation_locale: @spanish).at_least_once
        assert_equal @spanish, @agent.translation_locale
      end
    end

    describe "when user locale is empty" do
      before do
        @end_user.translation_locale = nil
        @agent.translation_locale = nil
        @end_user.save!
        @agent.save!
      end

      it "returns the account locale if the user is an end user" do
        assert_equal @spanish, @end_user.translation_locale
      end

      it "returns the account locale if the user is an agent and the account locale is available for admin" do
        assert_equal @spanish, @agent.translation_locale
      end

      describe "when the account locale is not availble for admin and the user is an agent" do
        before do
          @spanish.localized_agent = false
          @spanish.save!
          @account.translation_locale = @spanish
          @account.save!
          @agent.account = @account
          @agent.save!
        end

        it "returns ENGLISH_BY_ZENDESK if the user is an agent and the account locales is not available for admin" do
          assert_equal ENGLISH_BY_ZENDESK, @agent.translation_locale
        end
      end
    end
  end

  describe "#create_with_email" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
    end

    it "pass name and email to new users" do
      assert_difference "User.count(:all)", 1 do
        @user = @account.users.create_with_email("fizzle@example.com", name: "Abe Bar")
      end
      assert_equal "fizzle@example.com", @user.email
      assert_equal "Abe Bar", @user.name
    end

    it "uses a default name if no :name option is passed" do
      @returned_user = @account.users.create_with_email("fizzle@example.com")
      assert_equal "Unknown name", @returned_user.name
    end

    it "uses a default name if :name is nil" do
      @returned_user = @account.users.create_with_email("fizzle@example.com", name: nil)
      assert_equal "Unknown name", @returned_user.name
    end
  end

  describe "#find_best_locale" do
    before do
      @user = users(:minimum_end_user)
      @available_locales = ["es", "en-us", "ja"]
    end

    it "returns English by zendesk if the locale of the user is English by zendesk" do
      @user.stubs(:translation_locale).returns(ENGLISH_BY_ZENDESK)
      assert_equal ENGLISH_BY_ZENDESK, @user.find_best_locale(@available_locales)
    end

    it "returns the locale of the user if it is available" do
      japanese = translation_locales(:japanese)
      @user.stubs(:translation_locale).returns(japanese)
      assert_equal japanese, @user.find_best_locale(@available_locales)
    end

    it "returns the fallback of the locale if the locale is not present" do
      spanish = translation_locales(:spanish)
      latin_american_spanish = translation_locales(:latin_american_spanish)
      @user.stubs(:translation_locale).returns(latin_american_spanish)
      assert_equal spanish, @user.find_best_locale(@available_locales)
    end

    it "returns english by zendesk if the fallback is not present at all" do
      portuguese = translation_locales(:brazilian_portuguese)
      @user.stubs(:translation_locale).returns(portuguese)
      assert_equal ENGLISH_BY_ZENDESK, @user.find_best_locale(@available_locales)
    end

    it "returns english_by_zendesk if no available locale is passed" do
      portuguese = translation_locales(:brazilian_portuguese)
      @user.stubs(:translation_locale).returns(portuguese)
      assert_equal ENGLISH_BY_ZENDESK, @user.find_best_locale
    end
  end

  describe "#previous_primary_email" do
    before do
      @user = users(:minimum_end_user)

      # delete existing fixtures that might be auxiliary identities for this user
      @user.identities.where(type: 'UserEmailIdentity').each do |identity|
        identity.delete if identity.priority > 1
      end
    end

    describe "when there is no auxillary email associated with the user" do
      it "returns nothing" do
        assert_nil @user.previous_primary_email
      end
    end

    describe "when there is an auxillary email associated with the user" do
      before do
        @identity = UserEmailIdentity.create(account: @account, user: @user, value: "auxillary-email@example.com")
      end

      it "returns something" do
        assert @user.previous_primary_email
      end

      it "returns the identity for the auxillary email" do
        assert_equal @identity, @user.previous_primary_email
      end
    end
  end

  describe "#emails_include?" do
    let(:user) { users(:minimum_end_user) }

    it "returns true for default email identity's address" do
      assert user.emails_include?("minimum_end_user@aghassipour.com")
    end

    it "returns true for a secondary email identity's address" do
      assert user.emails_include?("deliverable@example.com")
    end

    it "returns true when casing is different for an otherwise matching address" do
      assert user.emails_include?("Minimum_end_user@Aghassipour.com")
    end

    it "returns false for another user's email address" do
      refute user.emails_include?("minimum_search_user@aghassipour.com")
    end
  end

  describe "tooltip defaults" do
    it "is set to false for new admins" do
      CIA.stubs(:current_actor).returns(@account.owner)
      user = @account.admins.create!(name: 'foo', email: 'fo123o@bar.com')
      assert user.settings.detect { |s| s.name == 'show_new_search_tooltip' }
    end

    it "is not set for new end_users (to save tons of user_settings space)" do
      user = @account.end_users.create!(name: 'footwassle')
      refute user.settings.detect { |s| s.name == 'show_new_search_tooltip' }
    end
  end

  describe 'Trial Account User Identity limit' do
    let(:new_user) { User.new(account: @account, email: 'limit@example.com', name: 'random') }
    let(:model) { :user_identity }

    describe 'when below limit' do
      before do
        refute TrialLimit.reached_trial_limit?(@account, model, 1000)
      end

      it 'creates new user' do
        assert new_user.save
      end

      it 'does not include the user identity trial limit error' do
        new_user.save
        refute new_user.errors.full_messages.join.include?(I18n.t('txt.error_message.models.user_identity_limit_v3'))
      end

      it 'does not disable account' do
        assert @account.is_serviceable
        new_user.save
        assert @account.is_serviceable
      end

      it 'has is_abusive set to false' do
        refute @account.settings.is_abusive
        new_user.save
        refute @account.settings.is_abusive
      end
    end

    describe 'when user actions increment trial limit' do
      before do
        User.any_instance.stubs(:whitelisted_from_user_limits?).returns(false)
        TrialLimit.stubs(:has_trial_limit?).with(@account, 'user_identity'.to_sym).returns(true)

        @original_trial_limit_count = TrialLimit.trial_limit_count(@account, 'user_identity'.to_sym)
      end

      describe_with_arturo_enabled :orca_classic_increase_model_count_on_update do
        it 'increments the trial_limit_count when a users identity is updated' do
          @account.users.last.identities.first.update_attributes!(value: 'updated-test-email@example.com')
          assert_not_equal @original_trial_limit_count, TrialLimit.trial_limit_count(@account, 'user_identity'.to_sym)
        end
      end

      describe_with_arturo_disabled :orca_classic_increase_model_count_on_update do
        it 'does not increment the trial_limit_count when a users identity is updated' do
          @account.users.last.identities.first.update_attributes!(value: 'updated-test-email@example.com')
          assert_equal @original_trial_limit_count, TrialLimit.trial_limit_count(@account, 'user_identity'.to_sym)
        end
      end
    end

    describe 'when exceeded limit' do
      before do
        TrialLimit.stubs(:reached_trial_limit?).with(@account, model, 1000).returns(true)
        FraudScoreJob.stubs(:enqueue_account)
      end

      describe 'when an account is not #whitelisted_from_user_limits' do
        before do
          TrialLimit.stubs(:statsd_client_trial_limit).returns(statsd_client_trial_limit)
        end

        it 'enqueues a fraud score job' do
          FraudScoreJob.expects(:enqueue_account).with(@account, :user_identity_trial_limit_exceeded).once
          new_user.save
        end

        it 'fails to create new user' do
          refute new_user.save
        end

        it 'includes the expected user identity trial limit error' do
          new_user.save
          assert new_user.errors.full_messages.join.include?(I18n.t('txt.error_message.models.user_identity_limit_v3'))
        end

        it 'increments #suspended when a new user is created' do
          statsd_client_trial_limit.expects(:increment).with('suspended', tags: ["subdomain:#{@account.subdomain}"]).at_least_once
          new_user.save
        end
      end

      describe 'when an account is #whitelisted_from_user_limits' do
        before do
          TrialLimit.stubs(:statsd_client_trial_limit).returns(statsd_client_trial_limit)
          User.any_instance.stubs(:whitelisted_from_user_limits?).returns(true)
        end

        it 'does not enqueue a fraud score job' do
          FraudScoreJob.expects(:enqueue_account).never
          new_user.save
        end
      end
    end
  end

  describe "#find_by_external_id" do
    let(:user_1) { User.new(account: @account, email: 'user_1@example.com', name: 'random', external_id: 'user_1') }
    let(:user_2) { User.new(account: @account, email: 'user_2@example.com', name: 'random', external_id: 'USER_2') }

    before do
      user_1.save
      user_2.save
    end

    it "finds by different case external_id" do
      assert_equal user_1, @account.users.find_by_external_id(user_1.external_id.swapcase)
      assert_equal user_2, @account.users.find_by_external_id(user_2.external_id.swapcase)
    end
  end

  describe '#iana_time_zone' do
    let(:user) { users(:minimum_end_user) }

    it 'returns the IANA time zone identifier' do
      assert_equal 'Europe/Copenhagen', user.iana_time_zone
    end
  end

  describe "Identities saved as child records through relationships" do
    it "Saves identities when new user" do
      user_1 = User.new(account: @account, name: 'random', external_id: 'user_1')
      identity = UserEmailIdentity.new(user: @user, value: 'first@identity.com')
      user_1.identities << identity
      users_cnt = User.count
      user_1.save!
      user_1.reload
      assert_equal User.count, users_cnt + 1
      assert_equal user_1.identities.count, 1
      assert_equal user_1.identities.first.value, 'first@identity.com'
    end

    it "Saves identities when new user but nothing changes on user directly" do
      user_1 = User.new(account: @account, name: 'random', external_id: 'user_1')
      user_1.save!
      user_1.reload
      identity_count = user_1.identities.count
      identity = UserEmailIdentity.new(user: @user, value: 'first@identity.com')
      user_1.identities << identity
      user_1.save!
      user_1.reload
      assert_equal user_1.identities.count, identity_count + 1
      assert_equal user_1.identities.first.value, 'first@identity.com'
    end
  end
end
