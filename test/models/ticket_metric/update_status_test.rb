require_relative '../../support/test_helper'
require_relative '../../support/business_hours_test_helper'

SingleCov.covered!

describe TicketMetric::UpdateStatus do
  include BusinessHoursTestHelper

  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }

  let(:schedule) do
    ticket.account.schedules.create(name: 'Schedule', time_zone: 'UTC')
  end

  let(:event) do
    TicketMetric::UpdateStatus.create(
      account: ticket.account,
      ticket:  ticket,
      metric:  'reply_time',
      time:    Time.utc(2006, 1, 2, 13, 21)
    )
  end

  before do
    add_schedule_intervals(
      schedule,
      [:mon, '09:00', '12:00'],
      [:mon, '13:00', '17:00']
    )

    ticket.create_ticket_schedule(account: ticket.account, schedule: schedule)

    ticket.metric_events.reply_time.activate.create(
      account: ticket.account,
      time:    Time.utc(2006, 1, 2, 9, 7)
    )
    ticket.metric_events.reply_time.fulfill.create(
      account: ticket.account,
      time:    Time.utc(2006, 1, 2, 19, 23)
    )
  end

  describe 'on creation' do
    it 'creates ticket metric statuses' do
      assert_equal(
        [
          {unit: 'calendar', value: 254},
          {unit: 'business', value: 194}
        ],
        event.statuses.map do |status|
          {unit: status.unit.to_s, value: status.value}
        end
      )
    end
  end

  describe '.precedence' do
    it 'follows `fulfill` events' do
      assert_equal TicketMetric::Fulfill.precedence.succ, event.precedence
    end
  end

  describe '#metadata' do
    it 'returns the metric status' do
      assert_equal({status: {calendar: 254, business: 194}}, event.metadata)
    end
  end
end
