require_relative '../../support/test_helper'
require_relative '../../support/ticket_metric_helper'

SingleCov.covered!

describe TicketMetric::ApplySla do
  include TicketMetricHelper

  fixtures :accounts,
    :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }

  let(:policy_metric) do
    new_sla_policy_metric(
      policy:    new_sla_policy,
      metric_id: Zendesk::Sla::TicketMetric::FirstReplyTime.id,
      target:    100
    )
  end

  let(:event) do
    TicketMetric::ApplySla.create(
      account: ticket.account,
      ticket:  ticket,
      metric:  'reply_time',
      time:    Time.now
    )
  end

  before { assign_policy(policy: policy_metric.policy) }

  describe '.precedence' do
    it 'follows `pause` events' do
      assert_equal TicketMetric::Pause.precedence.succ, event.precedence
    end
  end

  describe '#policy_metric' do
    it 'returns the policy metric' do
      assert_equal policy_metric, event.policy_metric
    end

    describe 'when the policy metric has been soft-deleted' do
      let!(:event) do
        TicketMetric::ApplySla.create(
          account: ticket.account,
          ticket:  ticket,
          metric:  'reply_time',
          time:    Time.now
        )
      end

      before { policy_metric.soft_delete }

      it 'returns the policy metric' do
        assert_equal policy_metric, event.policy_metric
      end
    end

    describe 'when there is no associated policy metric' do
      before { event.metric_event_policy_metric.destroy }

      it 'returns nil' do
        assert_nil event.policy_metric
      end
    end
  end

  describe '#metadata' do
    it 'returns the SLA details' do
      assert_equal(
        {
          sla: {
            target: policy_metric.target,
            business_hours: policy_metric.business_hours,
            policy: {
              id: policy_metric.policy.id,
              title: policy_metric.policy.title,
              description: policy_metric.policy.description
            }
          }
        },
        event.metadata
      )
    end

    describe 'when there is no associated policy metric' do
      before { event.metric_event_policy_metric.destroy }

      it 'returns no details' do
        assert_equal({sla: nil}, event.metadata)
      end
    end
  end
end
