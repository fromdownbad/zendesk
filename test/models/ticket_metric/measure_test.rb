require_relative '../../support/test_helper'

SingleCov.covered!

describe TicketMetric::Measure do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }

  let(:event) do
    TicketMetric::Measure.create(
      account: ticket.account,
      ticket:  ticket,
      metric:  'requester_wait_time',
      time:    Time.now
    )
  end

  describe '.precedence' do
    it 'follows undefined events' do
      assert_equal TicketMetric::Event.precedence.succ, event.precedence
    end
  end

  describe 'when creating a new event' do
    it "sets the 'instance_id' to zero" do
      assert_equal 0, event.instance_id
    end
  end
end
