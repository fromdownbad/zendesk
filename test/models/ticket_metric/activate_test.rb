require_relative '../../support/test_helper'

SingleCov.covered!

describe TicketMetric::Activate do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }

  let(:event) do
    TicketMetric::Activate.create(
      account: ticket.account,
      ticket:  ticket,
      metric:  'reply_time',
      time:    Time.now
    )
  end

  describe '.precedence' do
    it 'follows `measure` events' do
      assert_equal TicketMetric::Measure.precedence.succ, event.precedence
    end
  end

  describe '#to_state' do
    it "returns 'activated'" do
      assert_equal :activated, event.to_state
    end
  end

  describe '#to_state?' do
    describe "when passed 'activated'" do
      it 'returns true' do
        assert event.to_state?(:activated)
      end
    end

    describe "when not passed 'activated'" do
      it 'returns false' do
        refute event.to_state?(:deactivated)
      end
    end
  end
end
