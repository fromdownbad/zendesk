require_relative '../../support/test_helper'

SingleCov.covered!

describe TicketMetric::Breach do
  fixtures :tickets

  let(:ticket)     { tickets(:minimum_1) }
  let(:event_time) { Time.now }

  let(:event) do
    TicketMetric::Breach.create(
      account: ticket.account,
      ticket:  ticket,
      metric:  'reply_time',
      time:    event_time
    )
  end

  describe '.precedence' do
    it 'follows `apply_sla` events' do
      assert_equal TicketMetric::ApplySla.precedence.succ, event.precedence
    end
  end

  describe '#time' do
    it 'returns the event time' do
      assert_equal event_time, event.time
    end

    describe 'when the event is soft-deleted' do
      before { Timecop.freeze(event_time + 1.minute) { event.soft_delete } }

      it 'returns the last update time' do
        assert_equal event_time + 1.minute, event.time
      end
    end
  end

  describe '#metadata' do
    it 'shows the event is not deleted' do
      assert_equal({deleted: false}, event.metadata)
    end

    describe 'when the event is soft-deleted' do
      before { event.soft_delete }

      it 'shows the event is deleted' do
        assert_equal({deleted: true}, event.metadata)
      end
    end
  end
end
