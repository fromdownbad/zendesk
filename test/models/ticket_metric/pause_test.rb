require_relative '../../support/test_helper'

SingleCov.covered!

describe TicketMetric::Pause do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }

  let(:event) do
    TicketMetric::Pause.create(
      account: ticket.account,
      ticket:  ticket,
      metric:  'reply_time',
      time:    Time.now
    )
  end

  describe '.precedence' do
    it 'follows `activate` events' do
      assert_equal TicketMetric::Activate.precedence.succ, event.precedence
    end
  end

  describe '#to_state' do
    it "returns 'paused'" do
      assert_equal :paused, event.to_state
    end
  end

  describe '#to_state?' do
    describe "when passed 'paused'" do
      it 'returns true' do
        assert event.to_state?(:paused)
      end
    end

    describe "when not passed 'paused'" do
      it 'returns false' do
        refute event.to_state?(:active)
      end
    end
  end
end
