require_relative '../../support/test_helper'

SingleCov.covered!

describe TicketMetric::Fulfill do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }

  let(:event) do
    TicketMetric::Fulfill.create(
      account: ticket.account,
      ticket:  ticket,
      metric:  'requester_wait_time',
      time:    Time.now
    )
  end

  describe '.precedence' do
    it 'follows `breach` events' do
      assert_equal TicketMetric::Breach.precedence.succ, event.precedence
    end
  end

  describe '#to_state' do
    it "returns 'deactivated'" do
      assert_equal :deactivated, event.to_state
    end
  end

  describe '#to_state?' do
    describe "when passed 'deactivated'" do
      it 'returns true' do
        assert event.to_state?(:deactivated)
      end
    end

    describe "when not passed 'deactivated'" do
      it 'returns false' do
        refute event.to_state?(:activated)
      end
    end
  end
end
