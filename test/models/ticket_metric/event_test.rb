require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 1

describe TicketMetric::Event do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }

  let(:event) do
    ticket.metric_events.reply_time.update_status.create(
      account: ticket.account,
      time:    Time.new(2015)
    )
  end

  describe 'scope' do
    describe 'by default' do
      before do
        ticket.metric_events.agent_work_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2016)
        )
        ticket.metric_events.requester_wait_time.activate.create(
          account: ticket.account,
          time:    Time.new(2014)
        )
      end

      it 'orders by time, ascending' do
        assert_equal(
          [Time.new(2014), Time.new(2015), Time.new(2016)],
          ticket.metric_events.pluck(:time)
        )
      end
    end

    describe 'agent_work_time' do
      before do
        2.times do
          ticket.metric_events.agent_work_time.activate.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'agent work time' events" do
        assert_equal 2, ticket.metric_events.agent_work_time.count(:all)
      end
    end

    describe 'reply_time' do
      before do
        2.times do
          ticket.metric_events.reply_time.activate.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.agent_work_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'first reply time' events" do
        assert_equal 2, ticket.metric_events.reply_time.count(:all)
      end
    end

    describe 'requester_wait_time' do
      before do
        2.times do
          ticket.metric_events.requester_wait_time.activate.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.agent_work_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'requester wait time' events" do
        assert_equal 2, ticket.metric_events.requester_wait_time.count(:all)
      end
    end

    describe 'periodic_update_time' do
      before do
        2.times do
          ticket.metric_events.periodic_update_time.activate.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'periodic update time' events" do
        assert_equal 2, ticket.metric_events.periodic_update_time.count
      end
    end

    describe 'activate' do
      before do
        2.times do
          ticket.metric_events.reply_time.activate.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.reply_time.fulfill.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'activate' events" do
        assert_equal 2, ticket.metric_events.activate.count(:all)
      end
    end

    describe 'apply_sla' do
      before do
        2.times do
          ticket.metric_events.reply_time.apply_sla.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.reply_time.fulfill.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'apply SLA' events" do
        assert_equal 2, ticket.metric_events.apply_sla.count(:all)
      end
    end

    describe 'breach' do
      before do
        2.times do
          ticket.metric_events.reply_time.breach.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.reply_time.fulfill.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'breach' events" do
        assert_equal 2, ticket.metric_events.breach.count(:all)
      end
    end

    describe 'fulfill' do
      before do
        2.times do
          ticket.metric_events.reply_time.fulfill.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'fulfill' events" do
        assert_equal 2, ticket.metric_events.fulfill.count(:all)
      end
    end

    describe 'pause' do
      before do
        2.times do
          ticket.metric_events.reply_time.pause.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'pause' events" do
        assert_equal 2, ticket.metric_events.pause.count(:all)
      end
    end

    describe 'update_status' do
      before do
        2.times do
          ticket.metric_events.reply_time.update_status.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it "scopes to 'update status' events" do
        assert_equal 2, ticket.metric_events.update_status.count(:all)
      end
    end

    describe 'transition' do
      before do
        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
        ticket.metric_events.reply_time.apply_sla.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
        ticket.metric_events.reply_time.breach.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
        ticket.metric_events.reply_time.fulfill.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
        ticket.metric_events.reply_time.pause.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
        ticket.metric_events.reply_time.update_status.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it 'scopes to transition events' do
        assert(
          ticket.metric_events.transition.count(:all) == 3 &&
            ticket.metric_events.transition.all? do |event|
              %w[activate fulfill pause].include?(event.type)
            end
        )
      end
    end

    describe 'before' do
      before do
        (2014..2016).each do |year|
          ticket.metric_events.reply_time.activate.create(
            account:     ticket.account,
            instance_id: 1,
            time:        Time.new(year)
          )
        end
      end

      describe 'when the events occurred before the specified time' do
        let(:timestamp) { Time.new(2015, 6) }

        it 'includes those events' do
          assert_equal 2, ticket.metric_events.before(timestamp).count(:all)
        end
      end

      describe 'when an event occurs at the specified time' do
        let(:timestamp) { Time.new(2015) }

        it 'does not include the event' do
          assert_equal 1, ticket.metric_events.before(timestamp).count(:all)
        end
      end

      describe 'when the events occurred after the specified time' do
        let(:timestamp) { Time.new(2013) }

        it 'does not include those events' do
          assert_equal 0, ticket.metric_events.before(timestamp).count(:all)
        end
      end
    end

    describe 'after' do
      before do
        (2014..2016).each do |year|
          ticket.metric_events.reply_time.activate.create(
            account:     ticket.account,
            instance_id: 1,
            time:        Time.new(year)
          )
        end
      end

      describe 'when the events occurred before the specified time' do
        let(:timestamp) { Time.new(2017) }

        it 'does not include those events' do
          assert_equal 0, ticket.metric_events.after(timestamp).count(:all)
        end
      end

      describe 'when an event occurs at the specified time' do
        let(:timestamp) { Time.new(2015) }

        it 'includes the event' do
          assert_equal 2, ticket.metric_events.after(timestamp).count(:all)
        end
      end

      describe 'when the events occurred after the specified time' do
        let(:timestamp) { Time.new(2014, 6) }

        it 'includes those events' do
          assert_equal 2, ticket.metric_events.after(timestamp).count(:all)
        end
      end
    end
  end

  describe '.precedence' do
    it 'has the highest precedence' do
      assert_equal 0, TicketMetric::Event.precedence
    end
  end

  describe '.latest_instance_id' do
    describe 'when there are no events' do
      before { ticket.metric_events.destroy_all }

      it 'returns zero' do
        assert_equal 0, ticket.metric_events.reply_time.latest_instance_id
      end
    end

    describe 'when there are events for one instance' do
      before do
        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it 'returns one' do
        assert_equal 1, ticket.metric_events.reply_time.latest_instance_id
      end
    end

    describe 'when there are events for more than one instance' do
      before do
        ticket.metric_events.reply_time.activate.create(
          account:     ticket.account,
          instance_id: 1,
          time:        Time.new(2015)
        )
        ticket.metric_events.reply_time.activate.create(
          account:     ticket.account,
          instance_id: 2,
          time:        Time.new(2015)
        )
        ticket.metric_events.reply_time.activate.create(
          account:     ticket.account,
          instance_id: 3,
          time:        Time.new(2015)
        )
      end

      it "returns the current 'instance_id'" do
        assert_equal 3, ticket.metric_events.reply_time.latest_instance_id
      end
    end
  end

  describe '.next_instance_id' do
    describe 'when there are no events' do
      before { ticket.metric_events.destroy_all }

      it 'returns one' do
        assert_equal 1, ticket.metric_events.reply_time.next_instance_id
      end
    end

    describe 'when there are events for one instance' do
      before do
        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    Time.new(2015)
        )
      end

      it 'returns two' do
        assert_equal 2, ticket.metric_events.reply_time.next_instance_id
      end
    end

    describe 'when there are events for more than one instance' do
      before do
        ticket.metric_events.reply_time.activate.create(
          account:     ticket.account,
          instance_id: 1,
          time:        Time.new(2015)
        )
        ticket.metric_events.reply_time.activate.create(
          account:     ticket.account,
          instance_id: 2,
          time:        Time.new(2015)
        )
        ticket.metric_events.reply_time.activate.create(
          account:     ticket.account,
          instance_id: 3,
          time:        Time.new(2015)
        )
      end

      it "returns the next 'instance_id'" do
        assert_equal 4, ticket.metric_events.reply_time.next_instance_id
      end
    end
  end

  describe '#type' do
    it 'returns a prettified version of the type' do
      assert_equal 'update_status', event.type
    end
  end

  describe '#metric?' do
    describe 'when the event matches the name' do
      it 'returns true' do
        assert event.metric?(:reply_time)
      end
    end

    describe 'when the event does not match the metric name' do
      it 'returns false' do
        refute event.metric?(:requester_wait_time)
      end
    end
  end

  describe '#metadata' do
    it 'returns an empty hash' do
      exp = {status: {calendar: 0, business: 0}}
      assert_equal exp, event.metadata
    end
  end

  describe '#precedence' do
    it 'returns the event precedence' do
      assert_equal TicketMetric::UpdateStatus.precedence, event.precedence
    end
  end
end
