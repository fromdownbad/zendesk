require_relative "../support/test_helper"

SingleCov.covered!

describe Membership do
  should_validate_presence_of :group, :user
  should validate_uniqueness_of(:user_id).scoped_to(:group_id)

  describe ".assignable" do
    let(:user) { memberships(:minimum).user }

    it "returns 'none' if user is an end user" do
      assert_equal [], Membership.assignable(users(:minimum_end_user))
    end

    it "returns all memberships for groups that the agent belongs to" do
      assert_equal groups(:minimum_group).memberships.sort_by(&:id), Membership.assignable(user).sort_by(&:id)
    end

    describe "when the agent can only assign tickets to their own groups" do
      it "returns all memberships within this group" do
        user.expects(:agent_restriction?).with(:groups).returns(true)
        user.expects(:can?).with(:assign_to_any_group, Ticket).returns(false)

        assert_equal [memberships(:minimum), memberships(:minimum_admin)].sort_by(&:id), Membership.assignable(user).sort_by(&:id)
      end
    end

    describe "when the account has permission_sets enabled" do
      before do
        user.account.stubs(:has_permission_sets?).returns(true)
      end

      it "the associated group is present" do
        assert Membership.assignable(user).first.group.present?
      end

      it "the associated user is present" do
        assert Membership.assignable(user).first.user.present?
      end
    end

    describe "more stuff" do # FIXME: integrate with other tests
      let(:user) { memberships(:minimum).user }

      before do
        @account = accounts(:with_groups)
        @admin = users(:with_groups_admin)
        @agent1 = users(:with_groups_agent1)
        @agent2 = users(:with_groups_agent2)
        @agent3 = users(:with_groups_agent_groups_restricted)
        @minimum_end_user = users(:minimum_end_user)
      end

      it "does not return anything for an end user" do
        assert_empty(Membership.assignable(@minimum_end_user).where("1=1").to_a)
      end

      it "returns all memberships for an admin" do
        assert_equal @account.memberships.map(&:id).sort, @account.memberships.assignable(@admin).map(&:id).sort
      end

      it "returns all group memberships for a restricted agent (an agent who can only assign tickets to their own group)" do
        assert_equal [memberships(:minimum), memberships(:minimum_admin)].sort_by(&:id), Membership.assignable(user).sort_by(&:id)
      end
    end
  end

  describe "with a user" do
    let(:membership) { Membership.new }
    before { membership.build_user }

    describe "that is not an agent" do
      it "sets the appropriate error message" do
        membership.user.stubs(:is_agent?).returns(false)
        membership.valid?

        assert_match(/must be an agent/, membership.errors[:user].join)
      end
    end

    describe "that is an agent" do
      before do
        membership.user.account_id = 1
        membership.user.stubs(:is_agent?).returns(true)
      end

      it "sets the account to the user's account" do
        membership.valid?

        assert membership.account
        assert_equal membership.account, membership.user.account
      end

      describe "with a group" do
        before { membership.build_group }

        it "sets the account" do
          membership.group.account_id = 1
          membership.valid?

          assert membership.account
          assert_equal membership.account, membership.user.account
        end

        it "sets the appropriate error message if the group has a different account" do
          membership.group.account_id = 2
          membership.valid?

          assert_match(/must.*same account/, membership.errors[:user].join)
        end
      end

      describe "that is a deleted chat-agent" do
        before do
          membership.build_group
          membership.group.account_id = 1
        end

        it "skips validation" do
          # chat agents get downgraded to endusers, not sure why?
          membership.user.stubs(:is_agent?).returns(false)
          membership.user.stubs(:is_active).returns(false)

          assert membership.valid?
        end
      end
    end
  end

  describe "without a user, but with an group" do
    let(:membership) { Membership.new }

    it "sets the account to the group's account" do
      membership.build_group
      membership.group.account_id = 1
      membership.valid?

      refute membership.user
      assert membership.account
      assert_equal membership.account, membership.group.account
    end
  end

  describe "being saved" do
    let(:membership) { memberships(:minimum) }
    let(:user) { membership.user }

    describe 'and user has other memberships' do
      let(:support_group) { groups(:support_group) }
      let(:support_membership) { user.memberships.create(group: support_group, account: user.account, default: true) }

      before { support_membership }

      it "sets other memberships default to false for this user when default is true" do
        membership.default = true
        membership.save!
        refute support_membership.reload.default?
      end
    end

    describe 'and user has no other memberships' do
      it "does not set other memberships default to false" do
        membership.default = true
        user.memberships.expects(:update_all_with_updated_at).never
        membership.save!
      end
    end

    it "does not set other memberships default to false for this user when default is false" do
      membership.default = false
      user.memberships.expects(:update_all_with_updated_at).never
      membership.save!
    end

    it "updates user cache key" do
      user.expects(:touch_without_callbacks)

      membership.save!
    end
  end

  describe "#invalidate_phone_number_routing" do
    let(:membership) { memberships(:minimum) }

    it "calls invalidate_phone_number_routing on update" do
      membership.expects(:invalidate_phone_number_routing)
      membership.updated_at = Time.now
      membership.save!
    end

    it "calls invalidate_phone_number_routing on destroy" do
      membership.expects(:invalidate_phone_number_routing)
      membership.destroy
    end
  end

  describe "account ID scope inheritance" do
    let(:account) { accounts(:minimum) }
    let(:membership) { memberships(:minimum) }

    it "group relation" do
      assert_sql_queries(1, Regexp.new(Regexp.escape("`groups`.`account_id` = #{membership.account_id}"))) do
        membership.group
      end
    end

    it "active scope" do
      assert_sql_queries(1, Regexp.new(Regexp.escape("`groups`.`account_id` = #{membership.account_id}"))) do
        account.memberships.active(account.id).to_a
      end
    end

    it "user relation" do
      assert_sql_queries(1, Regexp.new(Regexp.escape("`users`.`account_id` IN (#{membership.account_id}, -1)"))) do
        membership.user
      end
    end
  end
end
