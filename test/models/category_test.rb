require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe Category do
  fixtures :accounts, :categories, :translation_locales

  should have_db_column(:name).of_type(:string)
  should have_db_column(:description).of_type(:text)

  should have_db_index :account_id

  should_validate_presence_of :account_id, :name

  describe "A non-first Category" do
    before { Account.first.categories.create!(name: "Category #{Time.now.to_i}") }
    should validate_uniqueness_of(:name).scoped_to(:account_id, :deleted_at).case_insensitive
  end

  describe "Category#to_param" do
    before { @category = Category.new }

    it "consists of the id and a normalized version of the name separated by a single dash" do
      @category.stubs(:id).returns(123)
      @category.name = "The World's Most Wonderful Category"

      assert_equal "123-The-World-s-Most-Wonderful-Category", @category.to_param
    end

    it "allows unicode characters and the id" do
      @category.stubs(:id).returns(123)
      @category.name = "Македонски"

      assert_equal "123-Македонски", @category.to_param
    end

    it "allows unicode alpha characters" do
      @category.name = "Ein Schöenes Tag"

      assert_match /Ein-Schöenes-Tag$/, @category.to_param
    end

    it "allows unicode alphanumeric characters and remove others" do
      @category.name = "A Little Кириличен Twist! 123"

      assert_match /A-Little-Кириличен-Twist-123$/, @category.to_param
    end
  end

  describe "Soft-deleting a category" do
    before do
      @category = accounts(:minimum).categories.build(name: "Test Category Name")
      @category.save!
      @category.soft_delete!
    end

    describe "when adding a new category with the same name" do
      it "does not throw a validation error" do
        category = accounts(:minimum).categories.build(name: "Test Category Name")
        category.save!
      end
    end
  end

  describe "serialisation" do
    before do
      @category = Category.new
      @category.stubs(:new_record).returns false
      @category.id = 12345
      @expected = ["id", "name", "description", "position"].sort
    end

    describe "#to_json" do
      it "includes the right attributes" do
        assert_equal @expected, JSON.parse(@category.to_json).keys.sort
      end
    end

    describe "#to_xml" do
      it "includes the right attributes" do
        assert_equal @expected, Hash.from_xml(@category.to_xml)["category"].keys.sort
      end
    end
  end

  describe "#content_mapping" do
    it "returns the content mapping of the category" do
      category = categories(:minimum_category)

      mapping = ContentMapping.create! do |cm|
        cm.account_id = category.account_id
        cm.classic_object_type = "Legacy::category"
        cm.classic_object_id = category.id
        cm.hc_object_type = "Foo"
        cm.hc_object_id = 42
        cm.url = "/foo/bar/baz"
      end

      assert_equal mapping, category.content_mapping
    end
  end
end
