require_relative "../../support/test_helper"

SingleCov.covered!

describe Outbound::Subscription do
  let(:described_class) { Outbound::Subscription }
  let(:now)             { Time.zone.now.change(usec: 0) }
  let(:account)         { accounts(:minimum) }
  let(:client)          { stub }
  let(:product_name)    { described_class::PRODUCT_NAME }
  let(:product)         { product_with_plan_settings }

  let(:product_with_plan_settings) do
    Zendesk::Accounts::Product.new(
      'id'               => 123456,
      'state'            => :subscribed,
      'name'             => :outbound,
      'active'           => true,
      'trial_expires_at' => nil,
      'state_updated_at' => Time.zone.now,
      'created_at'       => Time.zone.now,
      'updated_at'       => Time.zone.now,
      'plan_settings'    => {
        'billing_cycle_day'      => 30,
        'monthly_messaged_users' => 2_000
      }
    )
  end

  let(:product_without_plan_settings) do
    Zendesk::Accounts::Product.new(
      'state'            => :trial,
      'name'             => :outbound,
      'active'           => true,
      'trial_expires_at' => 10.days.from_now,
      'state_updated_at' => Time.zone.now,
      'created_at'       => Time.zone.now,
      'updated_at'       => Time.zone.now
    )
  end

  before do
    Timecop.freeze(now)
    Zendesk::Accounts::Client.stubs(new: client)
    client.stubs(product: product)
  end

  subject { described_class.find_for_account(account) }

  it 'inherits from AccountServiceSubscription' do
    assert_equal AccountServiceSubscription, described_class.superclass
  end

  it 'has the expected product name' do
    assert_equal 'outbound', described_class::PRODUCT_NAME
  end

  it 'specifies the expected valid attributes' do
    expected_attrs = [:monthly_messaged_users, :billing_cycle_day]
    assert_equal expected_attrs, described_class::PLAN_SETTINGS
  end

  describe '#include_in_easy_agent_add?' do
    let(:original) { :active? }
    let(:aliased)  { :include_in_easy_agent_add? }

    it 'equals #active?' do
      assert_equal subject.method(original), subject.method(aliased)
    end
  end

  describe '#monthly_messaged_users' do
    describe 'when not set in account service' do
      let(:product) { product_without_plan_settings }

      it 'defaults to 0' do
        assert_equal 0, subject.monthly_messaged_users
      end
    end

    describe 'when set in account service' do
      let(:product) { product_with_plan_settings }

      it 'returns current monthly_messaged_users' do
        assert_equal 2_000, subject.monthly_messaged_users
      end
    end
  end

  describe '#billing_cycle_day' do
    let(:product) { product_with_plan_settings }

    it 'returns correct billing_cycle_day' do
      assert_equal 30, subject.billing_cycle_day
    end
  end

  describe '#plan_type' do
    it 'defaults to 1' do
      assert_equal 1, subject.plan_type
    end
  end

  describe '#active?' do
    describe 'when state subscribed?' do
      let(:product) do
        product_without_plan_settings.tap do |settings|
          settings.state = :subscribed
        end
      end

      it 'return true' do
        assert subject.active?
      end
    end

    describe 'when state trial? and trial is not expired' do
      let(:product) do
        product_without_plan_settings.tap do |settings|
          settings.state = :trial
        end
      end

      it 'return true' do
        assert subject.active?
      end
    end

    describe 'when state expired?' do
      let(:product) do
        product_without_plan_settings.tap do |settings|
          settings.state = :expired
        end
      end

      it 'return false' do
        refute subject.active?
      end
    end
  end

  describe '#inspect' do
    let(:expected) do
      "#<#{described_class}:0x#{(subject.object_id << 1).to_s(16)} " \
        "state=#{subject.state} " \
        "monthly_messaged_users=#{subject.monthly_messaged_users} " \
        "billing_cycle_day=#{subject.billing_cycle_day}>"
    end

    it 'displays the expected attributes' do
      assert_equal expected, subject.inspect
    end
  end
end
