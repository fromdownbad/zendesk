require_relative "../../support/test_helper"

SingleCov.covered!

describe Outbound::Plan do
  fixtures :accounts, :tickets

  let(:account) { accounts(:multiproduct) }
  let(:ticket) { tickets(:minimum_1) }
  let(:account_id) { account.id }
  let(:state) { "trial" }
  let(:outbound_active_state) { true }
  let(:trial_expires_at) { 30.days.from_now.to_datetime.to_s }
  let(:monthly_messaged_users) { 1000 }
  let(:boosted_messaged_users) { 500 }
  let(:billing_cycle_day) { 5 }
  let(:boost_expires_at) { "2017-06-09T04:40:10Z" }
  let(:state_updated_at) { "2017-05-09T04:40:10Z" }
  let(:outbound_plan_response) do
    {
      "id" => 4,
      "account_id" => account_id,
      "name" => "outbound",
      "active" => outbound_active_state,
      "state" => state,
      "state_updated_at" => state_updated_at,
      "trial_expires_at" => trial_expires_at,
      "plan_settings" => {
        "monthly_messaged_users" => monthly_messaged_users,
        "billing_cycle_day" => billing_cycle_day
      },
      "created_at" => "2017-05-09T04:40:10Z",
      "updated_at" => "2017-05-09T04:40:10Z"
    }
  end
  let(:outbound_plan_response_with_boost) do
    outbound_plan_response.merge(
      "plan_settings" => {
        "monthly_messaged_users" => monthly_messaged_users,
        "billing_cycle_day" => billing_cycle_day,
        "boosted_messaged_users" => boosted_messaged_users,
        "boost_expires_at" => boost_expires_at
      }
    )
  end

  let(:outbound_plan) { Outbound::Plan.new(account) }

  describe '#active_subscription?' do
    let(:state) { 'subscribed' }
    describe 'when plan exists' do
      before do
        stub_product_response('outbound', body: outbound_plan_response, status: 200)
      end

      it 'should be active' do
        assert outbound_plan.active_subscription?
      end

      it 'should cache the outbound plan' do
        assert_nil Rails.cache.read(account.scoped_cache_key(:outbound_plan))
        assert outbound_plan.active_subscription?
        assert_not_nil Rails.cache.read(account.scoped_cache_key(:outbound_plan))
      end
    end
  end

  describe '#active_trial' do
    let(:state) { 'trial' }
    let(:trial_expires_at) { 1.day.from_now }

    describe 'when plan exists' do
      describe 'when account has account service product' do
        before do
          stub_product_response('outbound', body: outbound_plan_response, status: 200)
        end

        describe 'when trial has not expired' do
          let(:state) { 'trial' }
          let(:trial_expires_at) { 30.days.from_now }

          it 'is active' do
            assert outbound_plan.active_trial?
          end
        end

        describe 'when trial has expired' do
          describe 'when state is trial' do
            let(:state) { 'trial' }
            let(:trial_expires_at) { 1.day.ago }

            it 'is not active' do
              refute outbound_plan.active_trial?
            end
          end

          describe 'when state is expired' do
            let(:state) { 'expired' }
            let(:trial_expires_at) { 1.day.ago }

            it 'is not active' do
              refute outbound_plan.active_trial?
            end
          end
        end

        describe 'when outbound is not active' do
          let(:outbound_active_state) { false }

          it 'is not active' do
            refute outbound_plan.active_trial?
          end
        end
      end
    end

    describe '#messaged_users_cap' do
      let(:messaged_users_cap) { outbound_plan.messaged_users_cap }
      let(:state) { 'subscribed' }

      before do
        stub_product_response('outbound', body: outbound_plan_response_with_boost, status: 200)
      end

      describe 'when the outbound boosted resolutions feature is enabled' do
        before { Arturo.enable_feature!(:outbound_boosted_messaged_users) }

        describe 'and a boost expiry date exists in the plan settings' do
          describe 'and the boost has not expired' do
            it 'returns the value of monthly messaged users plus boosted resolutions' do
              Timecop.freeze(2017, 5, 9, 10) do
                assert_equal monthly_messaged_users + boosted_messaged_users, messaged_users_cap
              end
            end
          end

          describe 'and the boost has expired' do
            it 'returns the value of monthly messaged users' do
              Timecop.freeze(2017, 7, 9, 10) do
                assert_equal monthly_messaged_users, messaged_users_cap
              end
            end
          end
        end

        describe 'and a boost expiry date does not exist in the plan settings' do
          before do
            stub_product_response('outbound', body: outbound_plan_response, status: 200)
          end

          it 'returns the value of monthly messaged users' do
            assert_equal monthly_messaged_users, messaged_users_cap
          end
        end
      end

      describe 'when the outbound boosted resolutions feature is disabled' do
        it 'returns the value of monthly messaged users' do
          assert_equal monthly_messaged_users, messaged_users_cap
        end
      end

      describe 'when an outbound plan does not exist' do
        before { stub_product_response('outbound', body: outbound_plan_response, status: 404) }

        it 'returns nil' do
          assert_nil messaged_users_cap
        end
      end

      describe 'when an outbound plan is in trial' do
        let(:state) { 'trial' }

        it 'returns nil' do
          assert_nil messaged_users_cap
        end
      end
    end

    describe '#exists?' do
      it 'returns true when there is an outbound plan' do
        stub_product_response('outbound', body: outbound_plan_response, status: 200)
        assert outbound_plan.exists?
      end

      it 'returns false when there is no outbound plan' do
        stub_product_response('outbound', body: outbound_plan_response, status: 404)
        refute outbound_plan.exists?
      end
    end
  end

  describe '#state_updated_at' do
    before do
      stub_product_response('outbound', body: outbound_plan_response, status: 200)
    end

    it 'returns the "state_updated_at" value from the Pravda plan object' do
      assert_equal DateTime.parse('Tue, 09 May 2017 04:40:10 +0000'), outbound_plan.state_updated_at
    end
  end

  describe '#first_subscribed_period?' do
    describe 'when account is not in "subscribed" state' do
      before do
        Timecop.freeze(2017, 5, 9, 10)
        stub_product_response('outbound', body: outbound_plan_response, status: 200)
      end

      it 'returns false' do
        refute outbound_plan.first_subscribed_period?
      end
    end

    describe 'when account is in "subscribed" state' do
      let(:state) { 'subscribed' }

      describe 'when account has moved from trial to subscribed on current day' do
        before do
          Timecop.freeze(2017, 5, 9, 10)
          stub_product_response('outbound', body: outbound_plan_response, status: 200)
        end

        it 'returns true' do
          assert outbound_plan.first_subscribed_period?
        end
      end

      describe 'when account has moved from trial to subscribed within the current period' do
        before do
          Timecop.freeze(2017, 6, billing_cycle_day - 1, 10)
          stub_product_response('outbound', body: outbound_plan_response, status: 200)
        end

        it 'returns true' do
          assert outbound_plan.first_subscribed_period?
        end
      end

      describe 'when state_updated_at was in previous year' do
        before do
          Timecop.freeze(2018, 1, 2)
          stub_product_response('outbound', body: outbound_plan_response, status: 200)
        end

        it 'returns false' do
          refute outbound_plan.first_subscribed_period?
        end
      end

      describe 'when state_updated_at was in previous year and billing_cycle_day is before now' do
        let(:state_updated_at) { "2017-05-04T04:40:10Z" }
        before do
          Timecop.freeze(2018, 1, 10)
          stub_product_response('outbound', body: outbound_plan_response, status: 200)
        end

        it 'returns false' do
          refute outbound_plan.first_subscribed_period?
        end
      end

      describe 'when account moved from trial to subscribed last period' do
        before do
          Timecop.freeze(2017, 6, billing_cycle_day + 5, 10)
          stub_product_response('outbound', body: outbound_plan_response, status: 200)
        end

        it 'returns false' do
          refute outbound_plan.first_subscribed_period?
        end
      end
    end
  end

  describe '#billing cycle' do
    before do
      Timecop.freeze(2017, 11, 10)
      stub_product_response('outbound', body: outbound_plan_response, status: 200)
    end

    describe 'billing cycle day is in the past for current month' do
      let(:billing_cycle_day) { 5 }

      it 'returns a start date in this month and an end in the next' do
        expected = [DateTime.new(2017, 11, 5), DateTime.new(2017, 12, 4).end_of_day]
        assert_equal expected, outbound_plan.billing_cycle
      end
    end

    describe 'billing cycle day is the current day' do
      let(:billing_cycle_day) { 10 }

      it 'returns a start date of the current day and an end in the next month' do
        expected = [DateTime.new(2017, 11, 10), DateTime.new(2017, 12, 9).end_of_day]
        assert_equal expected, outbound_plan.billing_cycle
      end
    end

    describe 'billing cycle day is greater than current calendar day' do
      describe 'and a valid day of the current month' do
        let(:billing_cycle_day) { 29 }

        it 'returns a start date in the previous month and an end in the current' do
          expected = [DateTime.new(2017, 10, 29), DateTime.new(2017, 11, 28).end_of_day]
          assert_equal expected, outbound_plan.billing_cycle
        end
      end

      describe 'and not a valid day of current month' do
        let(:billing_cycle_day) { 31 }

        it 'returns, if available, the billing day in the previous month
            and the last day in the current' do
          expected = [DateTime.new(2017, 10, 31), DateTime.new(2017, 11, 29).end_of_day]
          assert_equal expected, outbound_plan.billing_cycle
        end
      end
    end

    describe 'billing cycle end in next year and in current year' do
      let(:billing_cycle_day) { 5 }
      before do
        Timecop.freeze(2017, 12, 10)
      end

      it 'returns a start date of the current year and an end in the next' do
        expected = [DateTime.new(2017, 12, 5), DateTime.new(2018, 1, 4).end_of_day]
        assert_equal expected, outbound_plan.billing_cycle
      end
    end

    describe 'billing cycle end in next year and in next year' do
      let(:billing_cycle_day) { 5 }
      before do
        Timecop.freeze(2018, 1, 2)
      end

      it 'returns a start date of the current year and an end in the next' do
        expected = [DateTime.new(2017, 12, 5), DateTime.new(2018, 1, 4).end_of_day]
        assert_equal expected, outbound_plan.billing_cycle
      end
    end

    describe 'billing cycle start is in previous year' do
      let(:billing_cycle_day) { 15 }
      before do
        Timecop.freeze(2017, 1, 10)
      end

      it 'returns a start date in the previous year and an end in the current' do
        expected = [DateTime.new(2016, 12, 15), DateTime.new(2017, 1, 14).end_of_day]
        assert_equal expected, outbound_plan.billing_cycle
      end
    end
  end

  def stub_product_response(product, body:, status:)
    stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account_id}/products/#{product}").
      to_return(
        status:  status,
        body:    { product: body }.to_json,
        headers: { content_type: 'application/json; charset=utf-8' }
      )
  end
end
