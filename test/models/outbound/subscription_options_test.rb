require_relative '../../support/test_helper'
SingleCov.covered!

describe Outbound::SubscriptionOptions do
  let(:described_class) { Outbound::SubscriptionOptions }
  let(:account)         { accounts(:minimum) }

  let(:support_params) do
    {
      max_agents:             5,
      billing_cycle_type:     4,
      plan_type:              3,
      pricing_model_revision: 7
    }
  end

  let(:subscription_options) do
    ZBC::Zuora::SubscriptionOptions.new(account.id, params)
  end

  describe '::build_for' do
    subject { described_class.build_for(account, subscription_options) }

    describe 'Outbound with 0 MMUs passed as option' do
      let(:params) do
        support_params.merge(
          outbound_monthly_messaged_users: 0,
          outbound_plan_type:              1
        )
      end

      describe 'not subscribed to Outbound' do
        let(:outbound_subscription) { stub(subscribed?: false) }

        before do
          Outbound::Subscription.expects(:find_for_account).
            returns(outbound_subscription)
        end

        it 'removes outbound from options' do
          assert_nil subject.outbound_monthly_messaged_users
          assert_nil subject.outbound_plan_type
        end
      end

      describe 'subscribed to Outbound' do
        let(:outbound_subscription) { stub(subscribed?: true) }

        before do
          Outbound::Subscription.expects(:find_for_account).
            returns(outbound_subscription)
        end

        it 'does not touch the options' do
          assert_equal 0, subject.outbound_monthly_messaged_users
        end
      end
    end

    describe 'Outbound with > 0 monthly messaged users passed as option' do
      let(:monthly_messaged_users) { 50 }

      let(:params) do
        support_params.merge(
          outbound_monthly_messaged_users: monthly_messaged_users,
          outbound_plan_type:              1
        )
      end

      it 'does not touch the options' do
        assert_equal 50, subject.outbound_monthly_messaged_users
      end
    end

    describe 'Outbound with < 0 monthly messaged users passed as option' do
      let(:monthly_messaged_users) { -1 }

      let(:params) do
        support_params.merge(
          outbound_monthly_messaged_users: monthly_messaged_users,
          outbound_plan_type:              1
        )
      end

      it 'raises InvalidMonthlyMessagedUsers' do
        assert_raises(Outbound::SubscriptionOptions::InvalidMonthlyMessagedUsers) { subject }
      end
    end
  end
end
