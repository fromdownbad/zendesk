require_relative "../support/test_helper"

SingleCov.covered!

describe EscKafkaMessage do
  describe ".create!" do
    it 'does not error' do
      EscKafkaMessage.create!(
        account_id:    1,
        topic:         'example_topic',
        partition_key: 1,
        key:           1,
        value:         'this is a message'
      )
    end
  end
end
