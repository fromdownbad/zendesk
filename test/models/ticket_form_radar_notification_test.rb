require_relative "../support/test_helper"
require 'zendesk/radar_factory'

SingleCov.not_covered!

describe TicketForm do
  fixtures :accounts, :users, :tickets

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:ticket) { tickets(:minimum_1) }

  let(:ticket_field) { FactoryBot.create(:field_tagger, account: account) }
  let(:new_ticket_field) { FactoryBot.create(:decimal_ticket_field, account: account) }
  let(:ticket_form) { Zendesk::TicketForms::Initializer.new(account, ticket_form: { ticket_field_ids: [ticket_field.id], position: 1, default: true, name: "foo", in_all_brands: true }).ticket_form }
  let(:now) { Time.now }
  let(:notification) { mock('notification') }

  describe "#notify_radar" do
    describe "sending notifications" do
      it "does send on ticket form update" do
        ticket_form.save!
        ::RadarFactory.expects(:create_radar_notification).with(account, "ticket_form/#{ticket_form.id}").returns(notification)
        expected_message = {updated_at: now.to_i}.to_json
        notification.expects(:send).with('updated', expected_message)

        ticket_form.name = 'Change da name'
        ticket_form.save!
      end

      it "does not send on ticket form create" do
        ::RadarFactory.expects(:create_radar_notification).never
        ticket_form.save!
      end
    end
  end
end
