require_relative '../../../support/test_helper.rb'

SingleCov.covered!

describe OrganizationEntityObserver do
  fixtures :organizations

  let(:organization) { organizations(:minimum_organization1) }

  describe 'on_save' do
    before do
      OrganizationProtobufEncoder.any_instance.stubs(:any?).returns(true)
    end

    describe_with_arturo_enabled :publish_organization_entity do
      it 'with arturo enabled it publishes protobuf organization' do
        EscKafkaMessage.expects(:create!).once
        organization.publish_organization_entity_snapshot!
      end
    end

    describe_with_arturo_disabled :publish_organization_entity do
      it 'with arturo disabled it does not publishes organization' do
        EscKafkaMessage.expects(:create!).never
        organization.publish_organization_entity_snapshot!
      end
    end
  end
end
