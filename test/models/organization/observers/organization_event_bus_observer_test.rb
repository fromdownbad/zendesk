require_relative '../../../support/test_helper'

SingleCov.covered!

describe OrganizationEventBusObserver do
  fixtures :organizations

  let(:organization) do
    organizations(:minimum_organization1).tap do |org|
      org.domain_event_publisher = domain_event_publisher
    end
  end

  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  describe 'on_save' do
    describe 'with events to emit' do
      before do
        organization.external_id = 'external_id'
      end

      it 'does create events' do
        organization.publish_organization_events_to_bus!
        domain_event_publisher.events.length.must_equal 1
      end

      describe 'with a publishing error' do
        it 'logs and supresses exceptions' do
          organization.expects(:organization_events_encoder).raises(RuntimeError)
          Zendesk::StatsD::Client.any_instance.expects(:increment).once
          organization.publish_organization_events_to_bus!
        end
      end

      describe 'statsd reporting' do
        it 'records domain event counts' do
          Zendesk::ModelChangeInstrumentation.any_instance.stubs(:report_change_source_metrics)
          Zendesk::StatsD::Client.any_instance.expects(:increment).once
          organization.publish_organization_events_to_bus!
        end

        it 'records mesage size stats' do
          Zendesk::StatsD::Client.any_instance.expects(:histogram).once
          organization.publish_organization_events_to_bus!
        end
      end
    end

    describe 'with no events to emit' do
      before do
        OrganizationEventsProtobufEncoder.any_instance.stubs(:to_a).returns([])
      end

      it 'does not create events' do
        organization.publish_organization_events_to_bus!
        domain_event_publisher.events.length.must_equal 0
      end
    end
  end
end
