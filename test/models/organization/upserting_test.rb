require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Organization::Upserting' do
  fixtures :accounts, :organizations, :users

  describe Organization do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_admin) }
    let(:organization) { organizations(:minimum_organization1) }

    describe "#update_existing_or_initialize_new_organization" do
      it "will create a new organization" do
        params = {
          organization: {
            name: "New Org",
            organization_fields: {
              decimal1: '5.2'
            }
          }
        }

        new_organization = Organization.update_existing_or_initialize_new_organization(account, user, params)

        assert new_organization.new_record?
        assert_includes new_organization.custom_field_values.map(&:value), '5.2'
      end

      it "will update an existing organization" do
        params = {
          organization: {
            id: organization.id,
            name: "Excellent Org",
            organization_fields: {
              decimal1: '5.2'
            }
          }
        }

        updated_organization = Organization.update_existing_or_initialize_new_organization(account, user, params)

        refute updated_organization.new_record?
        assert_equal organization.id, updated_organization.id
        assert_equal "Excellent Org", updated_organization.name
        assert_includes updated_organization.custom_field_values.map(&:value), '5.2'
      end
    end
  end
end
