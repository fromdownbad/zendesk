require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe SatisfactionRatingIntention do
  fixtures :accounts, :users, :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:user) { ticket.requester }
  let(:intention) { SatisfactionRatingIntention.new(score: SatisfactionType.GOOD, user: user, ticket: ticket) }

  describe "validations" do
    it "is valid" do
      assert intention.valid?
    end

    it "is valid with good/bad score" do
      intention.score = SatisfactionType.GOOD
      assert intention.valid?
      intention.score = SatisfactionType.BAD
      assert intention.valid?
    end

    it "is invalid with other score" do
      intention.score = SatisfactionType.GOODWITHCOMMENT
      refute intention.valid?
    end
  end

  describe "creation" do
    it "tracks via stats" do
      stats = mock
      stats.expects(:increment).with('created')
      SatisfactionRatingIntention.stubs(stats: stats)
      intention.account = accounts(:minimum)
      intention.save!
    end
  end

  describe "#rate!" do
    before do
      assert_equal SatisfactionType.Unoffered, ticket.satisfaction_score
    end

    it "creates a rating for a ticket" do
      intention.rate!
      assert_equal SatisfactionType.GOOD, ticket.reload.satisfaction_score
    end

    it "changes a rating for a ticket" do
      # add score
      ticket.attributes = {satisfaction_score: SatisfactionType.BAD}
      ticket.will_be_saved_by(user, via_id: ViaType.WEB_FORM)
      ticket.save!
      assert_equal SatisfactionType.BAD, ticket.satisfaction_score

      # change it
      intention.rate!
      assert_equal SatisfactionType.GOOD, ticket.reload.satisfaction_score
    end

    it "changes the bad score on ticket with comment" do
      # add score
      ticket.attributes = {satisfaction_score: SatisfactionType.BAD, satisfaction_comment: "Blah"}
      ticket.will_be_saved_by(user, via_id: ViaType.WEB_FORM)
      ticket.save!
      assert_equal SatisfactionType.BADWITHCOMMENT, ticket.satisfaction_score

      # change it
      intention.rate!
      assert_equal SatisfactionType.GOODWITHCOMMENT, ticket.reload.satisfaction_score
    end

    it "changes the good score on ticket with comment" do
      # add score
      ticket.attributes = {satisfaction_score: SatisfactionType.GOOD, satisfaction_comment: "Blah"}
      ticket.will_be_saved_by(user, via_id: ViaType.WEB_FORM)
      ticket.save!
      assert_equal SatisfactionType.GOODWITHCOMMENT, ticket.satisfaction_score

      # change it
      intention.score = SatisfactionType.BAD
      intention.rate!
      assert_equal SatisfactionType.BADWITHCOMMENT, ticket.reload.satisfaction_score
    end

    it "does not blow up with suspended account" do
      intention.ticket.account.is_serviceable = false
      intention.rate!
      assert_equal SatisfactionType.UNOFFERED, ticket.reload.satisfaction_score
    end

    it "does not blow up with deleted tickets" do
      intention.ticket = nil
      intention.rate!
      assert_equal SatisfactionType.UNOFFERED, ticket.reload.satisfaction_score
    end
  end
end
