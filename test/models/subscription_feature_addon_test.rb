require_relative '../support/test_helper'

SingleCov.covered! uncovered: 1

describe SubscriptionFeatureAddon do
  fixtures :accounts, :subscriptions

  describe 'A SubscriptionFeatureAddon' do
    should_validate_presence_of :name, :account_id

    let(:account) { accounts(:minimum) }

    let(:grace_period) { SubscriptionFeatureAddon::TEMPORARY_ZENDESK_AGENTS_GRACE_PERIOD }

    let(:boosted_addon) do
      account.subscription_feature_addons.create!(
        name: 'light_agents',
        boost_expires_at: 10.days.from_now
      )
    end

    let(:purchased_addon) do
      account.subscription_feature_addons.create!(
        name: 'nps',
        zuora_rate_plan_id: 'rate_plan_id'
      )
    end

    let(:standard_addon)              { { name: 'my_feature', zuora_rate_plan_id: '123456' } }
    let(:duplicate_standard_addon)    { { name: 'my_feature', zuora_rate_plan_id: '654321' } }
    let(:temp_agents_addon)           { { name: 'temporary_zendesk_agents', zuora_rate_plan_id: '987654', quantity: 5, expires_at: 3.months.from_now } }
    let(:duplicate_temp_agents_addon) { { name: 'temporary_zendesk_agents', zuora_rate_plan_id: '987654', quantity: 10, expires_at: 1.month.from_now } }

    describe 'SubscriptionFeatureService invocation' do
      before do
        account.subscription.update_attributes(
          plan_type:              SubscriptionPlanType.Large,
          pricing_model_revision: ZBC::Zendesk::PricingModelRevision::PATAGONIA
        )
      end

      it 'runs in an after_commit callback to pick up the newly created add-on' do
        refute account.subscription.has_light_agents?
        assert boosted_addon
        assert Subscription.find(account.subscription.id).has_light_agents?
      end
    end

    describe 'Creating an add-on' do
      describe '#require_zuora_id_or_valid_expiry_date!' do
        let(:blank_addon)          { { name: 'my_feature' } }
        let(:valid_zuora_id_addon) { { name: 'my_feature', zuora_rate_plan_id: '123456' } }
        let(:valid_addon_boost)    { { name: 'my_feature', boost_expires_at: 2.days.from_now } }

        let(:addon_boost_with_expiration_in_past) do
          { name: 'my_feature', boost_expires_at: 2.days.ago }
        end

        let(:addon_boost_with_expiration_beyond_limit) do
          { name: 'my_feature', boost_expires_at: 3.months.from_now }
        end

        it 'validates presence of either zuora_rate_plan_id or boost_expires_at' do
          refute account.subscription_feature_addons.create(blank_addon).valid?
          assert_equal 0, account.subscription_feature_addons.count
        end

        it 'creates a purchased add-on with a zuora_rate_plan_id' do
          assert account.subscription_feature_addons.create(valid_zuora_id_addon).valid?
          assert_equal 1, account.subscription_feature_addons.count
        end

        it 'validates that the boost_expires_at is in the future' do
          refute account.subscription_feature_addons.create(addon_boost_with_expiration_in_past).valid?
          assert account.subscription_feature_addons.create(valid_addon_boost).valid?
          assert_equal 1, account.subscription_feature_addons.count
        end

        it 'validates that the boost expiration date is not past the max limit' do
          refute account.subscription_feature_addons.create(addon_boost_with_expiration_beyond_limit).valid?
        end

        describe_with_arturo_enabled :remote_support_promo_boost_extension_addon do
          let(:valid_addon_boost) { { name: 'my_feature', boost_expires_at: 3.months.from_now } }

          let(:addon_boost_with_expiration_beyond_limit) do
            { name: 'my_feature', boost_expires_at: 7.months.from_now }
          end

          it 'allows boost expiration date past 30 days, but less than 6 months' do
            assert account.subscription_feature_addons.create(valid_addon_boost).valid?
          end

          it 'validates that the boost expiration date is not past the max limit' do
            refute account.subscription_feature_addons.create(addon_boost_with_expiration_beyond_limit).valid?
          end
        end
      end

      describe 'require unique-ish name' do
        it 'validates that non-special case addon names are unique' do
          assert account.subscription_feature_addons.create(standard_addon).valid?
          refute account.subscription_feature_addons.create(duplicate_standard_addon).valid?
          assert_equal 1, account.subscription_feature_addons.count
        end

        it 'scopes uniqueness constraint to account_id' do
          account2 = accounts(:trial)
          assert account.subscription_feature_addons.create(standard_addon).valid?
          assert account2.subscription_feature_addons.create(duplicate_standard_addon).valid?
          assert_equal 2, SubscriptionFeatureAddon.count
        end

        it 'allows duplicate special case addons' do
          assert account.subscription_feature_addons.create(temp_agents_addon).valid?
          assert account.subscription_feature_addons.create(duplicate_temp_agents_addon).valid?
          assert_equal 2, account.subscription_feature_addons.count
        end
      end

      it 'calls subscription_feature_service when creating a boosted add-on' do
        SubscriptionFeatureAddon.any_instance.expects(:update_subscription_features!)
        assert boosted_addon
      end

      it 'never calls subscription_feature_service when creating a purchased add-on' do
        SubscriptionFeatureAddon.any_instance.expects(:update_subscription_features!).never
        assert purchased_addon
      end

      describe '#is_boosted?' do
        it 'returns true for boosted add-ons' do
          assert boosted_addon.is_boosted?
        end

        it 'returns false for non-boosted add-ons' do
          refute purchased_addon.is_boosted?
        end
      end

      describe '#active?' do
        it 'returns true for boosted add-ons' do
          assert boosted_addon.active?
        end

        it 'returns true for purchased add-ons' do
          assert purchased_addon.active?
        end

        it 'returns false for boosted and expired' do
          boosted_addon.boost_expires_at = Time.now - 1.day
          refute boosted_addon.active?
        end

        it 'returns false for purchased and expired add-ons' do
          purchased_addon.expires_at = Time.now - 1.day
          refute purchased_addon.active?
        end
      end

      describe 'with other boosted addons present' do
        let(:enterprise_productivity_pack_addon_boost) { { name: :enterprise_productivity_pack, boost_expires_at: 5.days.from_now } }

        describe 'for a valid add-on boost' do
          before { account.subscription_feature_addons.create!(enterprise_productivity_pack_addon_boost) }

          it 'calls the co-terminus trigger' do
            SubscriptionFeatureAddon.any_instance.expects(:update_co_terminus_expiry_date!).once
            assert boosted_addon
          end

          it 'updates the boost_expires_at for previously boosted add-ons' do
            expected = boosted_addon.reload.boost_expires_at
            actual   = account.subscription_feature_addons.where(name: enterprise_productivity_pack_addon_boost[:name]).first.boost_expires_at

            assert_equal expected, actual
          end
        end

        describe 'for an add-on boost with an invalid expiry date' do
          let(:addon_boost) { account.subscription_feature_addons.build(enterprise_productivity_pack_addon_boost) }

          it 'validates that the boost expiry date is no more than 30 days' do
            addon_boost.boost_expires_at = 40.days.from_now
            refute addon_boost.save
            addon_boost.boost_expires_at = 30.days.from_now
            assert addon_boost.save
          end

          describe 'for a sandbox account' do
            before { Account.any_instance.stubs(is_sandbox?: true) }

            it 'bypasses the expiry date validation' do
              addon_boost.boost_expires_at = 40.days.from_now
              assert addon_boost.save
            end
          end
        end

        describe 'for a valid purchased add-on' do
          before { account.subscription_feature_addons.create!(enterprise_productivity_pack_addon_boost) }

          it 'never calls the co-terminus trigger' do
            SubscriptionFeatureAddon.any_instance.expects(:update_co_terminus_expiry_date!).never
            assert purchased_addon
          end
        end
      end
    end

    describe 'Updating an add-on' do
      let(:other_addon) do
        account.subscription_feature_addons.create!(
          name: :enterprise_productivity_pack, boost_expires_at: 5.days.from_now
        )
      end

      before do
        other_addon
        boosted_addon.update_attributes(boost_expires_at: 20.days.from_now)
      end

      it 'updates the boost_expires_at for previously boosted add-ons' do
        expected = boosted_addon.reload.boost_expires_at
        actual   = other_addon.reload.boost_expires_at

        assert_equal expected, actual
      end
    end

    describe 'Destroying an add-on' do
      before do
        boosted_addon
        purchased_addon
      end

      it 'calls subscription_feature_service when destroying an existing add-on boost' do
        SubscriptionFeatureAddon.any_instance.expects(:update_subscription_features!)
        assert boosted_addon.destroy
      end

      it 'never calls subscription_feature_service when destroying a purchased add-on boost' do
        SubscriptionFeatureAddon.any_instance.expects(:update_subscription_features!).never
        assert purchased_addon.destroy
      end
    end

    describe 'add-on #status' do
      let(:started_temporary_zendesk_agent) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'started',
          expires_at: 7.days.from_now,
          starts_at: 30.days.ago,
          quantity: 1,
          status_id: 1
        }
      end
      let(:expired_temporary_zendesk_agent) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'expired',
          expires_at: 10.days.ago,
          starts_at: 30.days.ago,
          quantity: 1,
          status_id: 2
        }
      end
      let(:future_temporary_zendesk_agent) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'future',
          expires_at: 60.days.from_now,
          starts_at: 30.days.from_now,
          quantity: 1
        }
      end

      before do
        account.subscription_feature_addons.create!(
          [
            started_temporary_zendesk_agent,
            expired_temporary_zendesk_agent,
            future_temporary_zendesk_agent
          ]
        )
      end

      it 'returns expired' do
        expired_addon = account.subscription_feature_addons.where(zuora_rate_plan_id: 'expired').first
        assert_equal :expired, expired_addon.status
      end

      it 'returns started' do
        started_addon = account.subscription_feature_addons.where(zuora_rate_plan_id: 'started').first
        assert_equal :started, started_addon.status
      end

      it 'returns nil' do
        future_addon = account.subscription_feature_addons.where(zuora_rate_plan_id: 'future').first
        assert_nil future_addon.status
      end
    end

    describe 'add-on scoping' do
      let(:temporary_zendesk_agent_1) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'started a month ago expires 7 days in future',
          expires_at: 7.days.from_now,
          starts_at: 30.days.ago,
          quantity: 1
        }
      end

      let(:temporary_zendesk_agent_2) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'active today and expires 6 days future',
          starts_at: Time.zone.now,
          expires_at: 6.days.from_now,
          quantity: 2,
          status_id: SubscriptionFeatureAddon::STATUS[:started]
        }
      end

      let(:temporary_zendesk_agent_3) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'in grace period',
          expires_at: 7.days.ago,
          starts_at: 14.days.ago,
          quantity: 3
        }
      end

      let(:temporary_zendesk_agent_4) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'expired_example 8 days ago',
          expires_at: 8.days.ago,
          starts_at: 14.days.ago,
          quantity: 4,
          status_id: SubscriptionFeatureAddon::STATUS[:started]
        }
      end

      let(:temporary_zendesk_agent_5) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'expired_example processed 9 days ago',
          expires_at: 9.days.ago,
          starts_at: 30.days.ago,
          quantity: 4,
          status_id: SubscriptionFeatureAddon::STATUS[:expired]
        }
      end

      let(:temporary_zendesk_agent_6) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'future subscription',
          expires_at: 60.days.from_now,
          starts_at: 30.days.from_now,
          quantity: 4
        }
      end

      let(:temporary_zendesk_agent_7) do
        {
          name: 'temporary_zendesk_agents',
          zuora_rate_plan_id: 'expires today',
          expires_at: Time.zone.now,
          starts_at: 30.days.ago,
          quantity: 4,
          status_id: SubscriptionFeatureAddon::STATUS[:started]
        }
      end

      let(:light_agents) do
        {
          name: 'light_agents',
          zuora_rate_plan_id: 'light_agents_example'
        }
      end

      before do
        account.subscription_feature_addons.create!(
          [
            temporary_zendesk_agent_1,
            temporary_zendesk_agent_2,
            temporary_zendesk_agent_3,
            temporary_zendesk_agent_4,
            temporary_zendesk_agent_5,
            temporary_zendesk_agent_6,
            temporary_zendesk_agent_7,
            light_agents
          ]
        )
        @active_addon = SubscriptionFeatureAddon.where(zuora_rate_plan_id: 'active_example').first
        @light_agents = SubscriptionFeatureAddon.where(name: 'light_agents').first
      end

      describe '#active add-ons' do
        let(:active_addons) { account.subscription_feature_addons.active }

        describe 'add-on that starts on a future date' do
          it 'does not return the future dated add-on' do
            assert_empty active_addons.where(zuora_rate_plan_id: 'future subscription')
          end
        end

        describe 'add-on that starts today (not expired)' do
          it 'includes add-ons that start today' do
            assert active_addons.where(zuora_rate_plan_id: 'active today and expires 6 days future').any?
          end
        end

        describe 'add-on that started a month ago (not expired)' do
          it 'includes add-ons that already started and have not expired' do
            assert active_addons.where(zuora_rate_plan_id: 'started a month ago expires 7 days in future').any?
          end
        end

        describe 'add-on that expired today (started three months ago)' do
          it 'excludes add-ons that expire today' do
            assert_empty active_addons.where(zuora_rate_plan_id: 'expires today')
          end
        end

        describe 'add-on that expired a month ago (started three months ago' do
          it 'does not include expired add-ons' do
            assert_empty active_addons.where(zuora_rate_plan_id: 'expired_example 8 days ago')
          end
        end

        describe 'add-on that expire within the 7 day grace period' do
          it 'excludes add-ons that expire in the grace period' do
            assert_empty active_addons.where(zuora_rate_plan_id: 'in grace period')
          end
        end
      end

      describe '#active_plus_grace_period' do
        let(:active_addons_with_zendesk_grace_period) do
          account.subscription_feature_addons.active_plus_grace_period(grace_period)
        end

        describe 'add-on that starts on a future date' do
          it 'does not return the future dated add-on' do
            assert_empty active_addons_with_zendesk_grace_period.where(zuora_rate_plan_id: 'future subscription')
          end
        end

        describe 'add-on that starts today (not expired)' do
          it 'includes add-ons that start today' do
            assert active_addons_with_zendesk_grace_period.where(zuora_rate_plan_id: 'active today and expires 6 days future').any?
          end
        end

        describe 'add-on that started a month ago (not expired)' do
          it 'includes add-ons that already started and have not expired' do
            assert active_addons_with_zendesk_grace_period.where(zuora_rate_plan_id: 'started a month ago expires 7 days in future').any?
          end
        end

        describe 'add-on that expired today (started three months ago)' do
          it 'includes add-ons that expire today' do
            assert active_addons_with_zendesk_grace_period.where(zuora_rate_plan_id: 'expires today').any?
          end
        end

        describe 'add-on that expire within the 7 day grace period' do
          it 'includes add-ons that expire in the grace period' do
            assert active_addons_with_zendesk_grace_period.where(zuora_rate_plan_id: 'in grace period').any?
          end
        end

        describe 'add-on that expired a month ago (started three months ago' do
          it 'does not include expired add-ons outside of grace period' do
            assert_empty active_addons_with_zendesk_grace_period.where(zuora_rate_plan_id: 'expired_example 8 days ago')
          end
        end
      end

      it 'scopes #by_name' do
        assert_equal 7, account.subscription_feature_addons.by_name(:temporary_zendesk_agents).count
        assert_equal 1, account.subscription_feature_addons.by_name(:light_agents).count
        assert_equal @light_agents, account.subscription_feature_addons.by_name(:light_agents).first
      end

      it 'scopes #expired_addons' do
        expired_addons = account.subscription_feature_addons.expired_addons(grace_period)
        assert expired_addons.where(zuora_rate_plan_id: 'expired_example 8 days ago').any?
        assert expired_addons.where(zuora_rate_plan_id: 'expired_example processed 9 days ago').any?
      end

      it 'scopes #expires_at_unprocessed' do
        unprocessed_expired_addons = account.subscription_feature_addons.expired_addons(0).expires_at_unprocessed
        assert unprocessed_expired_addons.where(zuora_rate_plan_id: 'expired_example 8 days ago').any?
        assert_empty unprocessed_expired_addons.where(zuora_rate_plan_id: 'expired_example processed 9 days ago')
      end

      it 'scopes #starts_at_unprocessed' do
        active_addons = account.subscription_feature_addons.active
        assert active_addons.where(zuora_rate_plan_id: 'active today and expires 6 days future').any?

        started_unprocessed_addons = active_addons.starts_at_unprocessed

        assert started_unprocessed_addons.where(zuora_rate_plan_id: 'started a month ago expires 7 days in future').any?
        assert_empty started_unprocessed_addons.where(zuora_rate_plan_id: 'active today and expires 6 days future')
      end

      it 'scopes #non_secondary_subscriptions' do
        addons = account.subscription_feature_addons.non_secondary_subscriptions
        assert_equal 1, addons.where(name: :light_agents).count
      end
    end
  end
end
