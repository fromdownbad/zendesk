require_relative "../support/test_helper"

SingleCov.covered!

describe RateLimit do
  fixtures :all

  let(:account) { accounts(:minimum) }

  describe "validation" do
    let(:rate_limit) do
      RateLimit.new(
        account: account,
        value: 30
      )
    end

    should_validate_presence_of :account, :value

    describe "on value" do
      it "allows a percentage value" do
        assert_valid rate_limit
      end

      it "catches an invalid percentage" do
        rate_limit.value = 200
        refute_valid rate_limit
      end
    end
  end
end
