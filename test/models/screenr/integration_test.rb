require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'Screenr::Integration' do
  fixtures :accounts, :users

  before do
    @params_from_api = {domain: "minimum",
                        host: "minimum.example.com",
                        upgraded_account: false,
                        upgrade_url: "https://screenr-upgrade.com/test",
                        recorders: {tickets_recorder_id: "a947c6",
                                    forums_recorder_id: "e041c9"}}
    @account = accounts(:minimum)
    @screenr_integration = Screenr::Integration.new(@account)
  end

  describe "Given a screenr_integration" do
    it "returns correct base_uri" do
      assert_equal "https://api.example.com", @screenr_integration.base_uri
    end
  end

  describe "When providing a Tenant coming from Screenr API" do
    before do
      client = stub("client", create_tenant: @params_from_api)
      Screenr::BusinessPartnerClient.stubs(:new).returns(client)
      @screenr_tenant = @screenr_integration.provide_tenant('minimum')
    end

    it "returns not nil" do
      assert @screenr_tenant.present?
    end

    it "marks as completed provisioning" do
      assert @screenr_integration.has_completed_provisioning?
    end

    it "returns correct domain" do
      assert_equal 'minimum', @screenr_tenant.domain
    end

    it "returns domain_url" do
      assert_equal 'minimum.example.com', @screenr_integration.host
    end

    it "returns upgraded_account" do
      assert_equal false, @screenr_tenant.upgraded_account
    end

    it "returns upgrade_url" do
      assert_equal 'https://screenr-upgrade.com/test', @screenr_tenant.upgrade_url
    end

    it "returns tickets_recorder_id" do
      assert_equal 'a947c6', @screenr_integration.tickets_recorder_id
    end

    describe " and then unlinking the Tenant" do
      before do
        @account.settings.screencasts_for_tickets = true
        @account.save!
      end

      it "removes Tenant" do
        assert_difference("Screenr::Tenant.count(:all)", -1) do
          @screenr_integration.unlink_tenant!
        end
      end

      it "deactivates screencasts_for_tickets" do
        @screenr_integration.unlink_tenant!
        assert_equal false, @account.reload.settings.screencasts_for_tickets?
      end
    end
  end

  describe "When somethings wrong comes back from Screenr's API" do
    before do
      wrong_params_from_api = {domain: ""}
      client = stub("client", create_tenant: wrong_params_from_api)
      Screenr::BusinessPartnerClient.stubs(:new).returns(client)
    end

    it "raises a RecordInvalid exception" do
      assert_raise(ActiveRecord::RecordInvalid) do
        @screenr_integration.provide_tenant('minimum')
      end
    end
  end

  describe "When forcing activate_screencasts_for_tickets" do
    before do
      client = stub("client", create_tenant: @params_from_api)
      Screenr::BusinessPartnerClient.stubs(:new).returns(client)
      @screenr_tenant = @screenr_integration.provide_tenant('minimum', true)
    end

    it "activates settings for screencasts_for_tickets" do
      assert @account.settings.screencasts_for_tickets
    end
  end

  describe "When not forcing activate_screencasts_for_tickets" do
    before do
      client = stub("client", create_tenant: @params_from_api)
      Screenr::BusinessPartnerClient.stubs(:new).returns(client)
      @screenr_tenant = @screenr_integration.provide_tenant('minimum', false)
    end

    it "does not activate settings for screencasts_for_tickets" do
      refute @account.settings.screencasts_for_tickets
    end
  end

  describe "When updating a tenant and all is fine" do
    before do
      # First we ensure that we have a Tenant
      client = stub("client", create_tenant: @params_from_api)
      Screenr::BusinessPartnerClient.stubs(:new).returns(client)
      @screenr_integration.provide_tenant('minimum')

      # Then we update.
      @client = stub("client", update_tenant: true)
      Screenr::BusinessPartnerClient.stubs(:new).returns(@client)

      @user = users(:minimum_admin)
    end

    it "sets personal_data_sent_to_screenr to true" do
      @screenr_tenant = @screenr_integration.send_personal_data_to_screenr_for(@user)
      assert @screenr_tenant.personal_data_sent_to_screenr?
    end

    it "sends right parameters to client" do
      @client.expects(:update_tenant).with('minimum', last_name: 'Man', first_name: 'Admin', email: 'minimum_admin@aghassipour.com')
      @screenr_integration.send_personal_data_to_screenr_for(@user)
    end
  end

  describe "When Account has not completed provisioning" do
    it "does not report it as completed provisioning" do
      refute @screenr_integration.has_completed_provisioning?
    end

    it "returns empty string for #tickets_recorder_id" do
      assert_nil(@screenr_integration.tickets_recorder_id)
    end

    it "returns empty string for #forums_recorder_id" do
      assert_nil(@screenr_integration.forums_recorder_id)
    end

    it "returns empty string for #host" do
      assert_nil(@screenr_integration.host)
    end

    it "returns empty string for #upgrade_url" do
      assert_nil(@screenr_integration.upgrade_url)
    end
  end
end
