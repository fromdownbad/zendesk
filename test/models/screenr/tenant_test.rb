require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Screenr::Tenant' do
  fixtures :accounts

  describe "When creating based on a Tenant coming from Screenr API" do
    before do
      params = {domain: "minimum",
                host: "minimum.sfssdev.com",
                upgraded_account: false,
                upgrade_url: "https://screenr-upgrade.com/test",
                recorders: {tickets_recorder_id: "a947c6",
                            forums_recorder_id: "e041c9"}}
      @account = accounts(:minimum)
      @screenr_tenant = Screenr::Tenant.create(params.merge(account: @account))
    end

    it "does not be nil" do
      assert_not_nil @screenr_tenant
    end

    it "saves domain" do
      assert_equal 'minimum', @screenr_tenant.domain
    end

    it "saves domain_url" do
      assert_equal 'minimum.sfssdev.com', @screenr_tenant.host
    end

    it "saves upgraded_account" do
      assert_equal false, @screenr_tenant.upgraded_account
    end

    it "saves upgrade_url" do
      assert_equal 'https://screenr-upgrade.com/test', @screenr_tenant.upgrade_url
    end

    it "saves tickets_recorder_id" do
      assert_equal 'a947c6', @screenr_tenant.recorders[:tickets_recorder_id]
    end

    it "saves forums_recorder_id" do
      assert_equal 'e041c9', @screenr_tenant.recorders[:forums_recorder_id]
    end
  end
end
