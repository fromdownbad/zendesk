require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Screenr::BusinessPartnerClient' do
  describe "A client instance" do
    before do
      @client = Screenr::BusinessPartnerClient.new
    end

    describe "when looking up a non existing tenant name" do
      before do
        stub_request(:get, "https://api.example.com/tenants/NOT-VALID").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":404,"Message":"Resource not found."}'
          )
      end

      it "raises a TenantNotFound exception" do
        assert_raise(Screenr::BusinessPartnerClient::TenantNotFound) do
          @client.lookup_tenant('NOT-VALID')
        end
      end
    end

    describe "when looking up a valid tenant's name" do
      before do
        stub_request(:get, "https://api.example.com/tenants/minimum").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"Domain":"minimum",
                      "DomainUrl":"https://minimum.example.com",
                      "UpgradedAccount":false,
                      "UpgradeUrl":"https://screenr-upgrade.com/test",
                      "Recorders":{"TicketsRecorderId":"TICKETS-ID","ForumsRecorderId":"RECORDERS-ID"}}'
          )
        @result = @client.lookup_tenant('minimum')
      end

      it "returns correct domain" do
        assert_equal "minimum", @result[:domain]
      end

      it "returns correct domain_url" do
        assert_equal "https://minimum.example.com", @result[:domain_url]
      end

      it "returns false for upgraded_account" do
        assert_equal false, @result[:upgraded_account]
      end

      it "returns correct upgrade_url" do
        assert_equal 'https://screenr-upgrade.com/test', @result[:upgrade_url]
      end
    end

    describe "when looking up a valid paying tenant's name" do
      before do
        stub_request(:get, "https://api.example.com/tenants/minimum").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"Domain":"minimum",
                      "DomainUrl":"https://minimum.example.com",
                      "UpgradedAccount":true,
                      "UpgradeUrl":"",
                      "Recorders":{"TicketsRecorderId":"TICKETS-ID","ForumsRecorderId":"RECORDERS-ID"}}'
          )
        @result = @client.lookup_tenant('minimum')
      end

      it "returns true for upgraded_account" do
        assert(@result[:upgraded_account])
      end
    end

    describe "when creating a new tenant with valid parameters" do
      before do
        stub_request(:post, "https://api.example.com/tenants").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"Domain":"minimum",
                      "DomainUrl":"https://minimum.example.com",
                      "UpgradedAccount":false,
                      "UpgradeUrl":"https://screenr-stage.chargify.com/TEST/subscriptions/new?reference=libotest1234",
                      "Recorders":{"TicketsRecorderId":"TICKETS-ID","ForumsRecorderId":"RECORDERS-ID"}}'
          )
      end

      it "does not raise anything" do
        @client.create_tenant(domain: 'minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com')
      end

      it "returns correct domain" do
        result = @client.create_tenant(domain: 'minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com')
        assert_equal "minimum", result[:domain]
      end

      it "returns a tickets recorder id" do
        result = @client.create_tenant(domain: 'minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com')
        assert_equal "TICKETS-ID", result[:recorders][:tickets_recorder_id]
      end

      it "returns a forums recorder id" do
        result = @client.create_tenant(domain: 'minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com')
        assert_equal "RECORDERS-ID", result[:recorders][:forums_recorder_id]
      end

      it "returns an upgrade_url" do
        result = @client.create_tenant(domain: 'minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com')
        assert_equal "https://screenr-stage.chargify.com/TEST/subscriptions/new?reference=libotest1234", result[:upgrade_url]
      end
    end

    describe "when creating a new tenant using an existing tenant's name" do
      before do
        stub_request(:post, "https://api.example.com/tenants").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":409,
                      "Message":"Domain is already registered.",
                      "AvailableDomains":["minimum1","minimum2","minimum3","minimum4","minimum5"]}'
          )
      end

      it "raises Screenr::BusinessPartnerClient::DomainAlreadyInUse" do
        assert_raises(Screenr::BusinessPartnerClient::DomainAlreadyInUse) do
          @client.create_tenant(domain: 'minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com')
        end
      end

      it "returns a list of available domains in Screenr::BusinessPartnerClient::DomainAlreadyInUse exception" do
        e = assert_raises Screenr::BusinessPartnerClient::DomainAlreadyInUse do
          @client.create_tenant(domain: 'minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com')
        end
        assert_equal 5, e.available_domains.count
        assert_equal "minimum1", e.available_domains.first
      end
    end

    describe "when creating a new tenant using an existing tenant's name and we force registration" do
      before do
        stub_request(:post, "https://api.example.com/tenants").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"Domain":"minimum2",
                      "DomainUrl":"https://minimum.example.com",
                      "UpgradedAccount":false,
                      "Recorders":{"TicketsRecorderId":"TICKETS-ID","ForumsRecorderId":"RECORDERS-ID"}}'
          )
      end

      it "does not raise anything" do
        @client.create_tenant(domain: 'minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com', force_registration: true)
      end

      it "returns correct domain" do
        result = @client.create_tenant(domain: 'minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com', force_registration: true)
        assert_equal "minimum2", result[:domain]
      end
    end

    describe "when creating a new tenant without a first_name" do
      before do
        stub_request(:post, "https://api.example.com/tenants").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":400,
                      "Message":[{"Key":"OwnerFirstName","Value":"The OwnerFirstName field is required."}]}'
          )
        @expected_errors = [{"first_name" => "The OwnerFirstName field is required."}]
      end

      it "raises Screenr::BusinessPartnerClient::ValidationFailed" do
        assert_raises(Screenr::BusinessPartnerClient::ValidationFailed) do
          @client.create_tenant(domain: 'minimum', first_name: '', last_name: 'Madsen', email: 'sm@test.com')
        end
      end

      it "includes errors information in ValidationFailed" do
        e = assert_raises Screenr::BusinessPartnerClient::ValidationFailed do
          @client.create_tenant(domain: 'minimum', first_name: '', last_name: 'Madsen', email: 'sm@test.com')
        end
        assert_equal @expected_errors, e.errors
        assert_equal "first_name", e.first_error_on
        assert_equal "The OwnerFirstName field is required.", e.first_error_messages
      end
    end

    describe "when updating an existing Tenant with correct first_name, last_name, email" do
      before do
        stub_request(:post, "https://api.example.com/tenants/minimum/users/updateowner").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"Success":true}'
          )
      end

      it "does not raise anything" do
        @client.update_tenant('minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com')
      end

      it "returns true" do
        assert @client.update_tenant('minimum', first_name: 'Søren', last_name: 'Madsen', email: 'sm@test.com')
      end
    end

    describe "when updating an NOT existing Tenant with correct first_name, last_name, email" do
      before do
        stub_request(:post, "https://api.example.com/tenants/notexisting/users/updateowner").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":400,"Message":[{"Key":"Tenant","Value":"Tenant not found."}]}'
          )
      end

      it "raises Screenr::BusinessPartnerClient::ValidationFailed" do
        assert_raises(Screenr::BusinessPartnerClient::ValidationFailed) do
          @client.update_tenant('notexisting', first_name: 'Test', last_name: 'Madsen', email: 'sm@test.com')
        end
      end

      it "includes errors information in ValidationFailed" do
        e = assert_raises Screenr::BusinessPartnerClient::ValidationFailed do
          @client.update_tenant('notexisting', first_name: 'Test', last_name: 'Madsen', email: 'sm@test.com')
        end
        expected_errors = [{"domain" => "Tenant not found."}]
        assert_equal expected_errors, e.errors
        assert_equal "domain", e.first_error_on
        assert_equal "Tenant not found.", e.first_error_messages
      end
    end

    describe "when updating an existing Tenant with missing first_name" do
      before do
        stub_request(:post, "https://api.example.com/tenants/minimum/users/updateowner").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":400,"Message":[{"Key":"FirstName","Value":"The FirstName field is required."}]}'
          )
      end

      it "raises Screenr::BusinessPartnerClient::ValidationFailed" do
        assert_raises(Screenr::BusinessPartnerClient::ValidationFailed) do
          @client.update_tenant('minimum', first_name: '', last_name: 'Madsen', email: 'sm@test.com')
        end
      end

      it "includes errors information in ValidationFailed" do
        e = assert_raises Screenr::BusinessPartnerClient::ValidationFailed do
          @client.update_tenant('minimum', first_name: '', last_name: 'Madsen', email: 'sm@test.com')
        end
        expected_errors = [{"first_name" => "The FirstName field is required."}]
        assert_equal expected_errors, e.errors
        assert_equal "first_name", e.first_error_on
        assert_equal "The FirstName field is required.", e.first_error_messages
      end
    end

    describe "when updating an existing Tenant with wrong email" do
      before do
        stub_request(:post, "https://api.example.com/tenants/minimum/users/updateowner").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":400,"Message":[{"Key":"Email","Value":"Email is not a valid email address."}]}'
          )
      end

      it "raises Screenr::BusinessPartnerClient::ValidationFailed" do
        assert_raises(Screenr::BusinessPartnerClient::ValidationFailed) do
          @client.update_tenant('minimum', first_name: 'Test', last_name: 'Madsen', email: 'smtest.com')
        end
      end

      it "includes errors information in ValidationFailed" do
        e = assert_raises Screenr::BusinessPartnerClient::ValidationFailed do
          @client.update_tenant('minimum', first_name: 'Test', last_name: 'Madsen', email: 'smtest.com')
        end
        expected_errors = [{"email" => "Email is not a valid email address."}]
        assert_equal expected_errors, e.errors
        assert_equal "email", e.first_error_on
        assert_equal "Email is not a valid email address.", e.first_error_messages
      end
    end

    describe "when downgrading a Tenant that has been upgraded" do
      before do
        stub_request(:post, "https://api.example.com/tenants/downgrade").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"Success":true}'
          )
      end

      it "does not raise anything" do
        @client.downgrade_tenant('minimum')
      end

      it "returns true" do
        assert @client.downgrade_tenant('minimum')
      end
    end

    describe "when downgrading a Tenant that has NOT been upgraded" do
      before do
        stub_request(:post, "https://api.example.com/tenants/downgrade").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":400,"Success":false}'
          )
      end

      it "raises DowngradeFailed" do
        assert_raise(Screenr::BusinessPartnerClient::DowngradeFailed) do
          @client.downgrade_tenant('minimum')
        end
      end
    end
  end
end
