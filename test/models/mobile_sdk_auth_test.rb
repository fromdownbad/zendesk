require_relative "../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"

SingleCov.covered! uncovered: 2

describe MobileSdkAuth do
  fixtures :accounts, :account_settings, :brands, :mobile_sdk_apps

  let(:account) { accounts(:minimum) }
  let(:app_identifier) { "987654wsdfghjkliuytr65" }
  let(:mobile_sdk_app) { mobile_sdk_apps(:minimum) }
  let(:oauth_client) { FactoryBot.create(:client, account: account) }

  before do
    brands(:minimum)
  end

  describe 'creation' do
    before do
      @endpoint = "https://somesite.com/zd/auth"
      @shared_secret = "aub7qw64ci7467c64738iubyret"
    end

    it 'creates with no errors' do
      record = account.mobile_sdk_auths.create(
        endpoint: @endpoint,
        shared_secret: @shared_secret,
        name: "My Auth",
        client: oauth_client
      )
      assert record.save!
    end

    # This simulates the SDK Config panel
    # The basic info that is provided is Name and Endpoint
    # all the other stuff is being generated
    it 'creates with the basic info' do
      auth_name = "Auth for App One"
      auth_endpoint = @endpoint
      auth = account.mobile_sdk_auths.build(
        "name" => auth_name,
        "endpoint" => auth_endpoint,
        "shared_secret" => "QsufToKkXnEu5EDQGcxxmDnRKUI04IDHQvt4KEpQJKUgRorM"
      )
      # create the OAuth Client
      auth.client = account.clients.build("secret" => "23ca8485781644eec073428ec713181b585ab12daf71b9a95c08cf9b31e3fd44")
      auth.client.name = "Client for #{auth_name}"
      auth.client.identifier = auth_name.downcase.gsub(/\s/, "_")
      assert auth.client.save!
      auth.generate_shared_secret
      assert auth.save!
    end

    it 'creates anonymous auth with the basic info' do
      auth_name = "Auth for App Two"
      auth = account.mobile_sdk_auths.build("name" => auth_name)
      # create the OAuth Client
      auth.client = account.clients.build("secret" => "23ca8485781644eec073428ec713181b585ab12daf71b9a95c08cf9b31e3fd44")
      auth.client.name = "Client for #{auth_name}"
      auth.client.identifier = auth_name.downcase.gsub(/\s/, "_")
      assert auth.client.save!
      assert auth.save!
      auth.endpoint = ""
      assert auth.save!
    end

    describe "endpoint validation" do
      it 'checks the endpoint protocol' do
        assert_raise(ActiveRecord::RecordInvalid) do
          account.mobile_sdk_auths.create(
            endpoint: "http://somesite.com/zd/auth",
            shared_secret: @shared_secret,
            name: "My Auth",
            client: oauth_client
          ).save!
        end
      end
    end
  end

  should_validate_presence_of :name, :client

  describe 'access' do
    before do
      @sdk_auth = FactoryBot.create(:mobile_sdk_auth, account: account, client: oauth_client)
      @sdk_auth.shared_secret
    end

    it 'not update shared_secret' do
      current_secret = @sdk_auth.shared_secret
      assert_raise(StandardError) { @sdk_auth.shared_secret = "newsecret" }
      assert_equal current_secret, @sdk_auth.shared_secret
    end

    it 'updated shared_secret via next_secret' do
      current_secret = @sdk_auth.shared_secret
      next_secret = "newsecret"
      @sdk_auth.next_secret = next_secret
      @sdk_auth.save
      assert_equal next_secret, @sdk_auth.shared_secret
      refute_equal current_secret, @sdk_auth.shared_secret
    end

    it 'return cropped secret by default' do
      assert_equal @sdk_auth.shared_secret.length, 10
    end

    it 'return a full secret only on :full' do
      assert @sdk_auth.shared_secret(true).length > 10
    end
  end

  describe 'destroy' do
    before do
      @sdk_auth = FactoryBot.create(:mobile_sdk_auth, account: account, client: oauth_client)
      @sdk_auth.shared_secret
    end

    describe "auth" do
      before { @sdk_auth.destroy }

      it 'it destroy client' do
        assert oauth_client.destroyed?
      end
    end

    describe "client" do
      before { oauth_client.destroy }

      it 'it not destroy auth' do
        assert account.mobile_sdk_auths.find_by_id(@sdk_auth.id)
      end
    end
  end
end
