require_relative "../support/test_helper"

SingleCov.covered!

describe TranslationLocale do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  it 'has kasket indices' do
    TranslationLocale.kasket_indices.must_include [:id]
    TranslationLocale.kasket_indices.must_include [:deleted_at, :id]
  end

  subject { account.available_languages.first }

  describe '#top_level?' do
    let(:english_by_zendesk) { ENGLISH_BY_ZENDESK }

    describe "when the parent is ENGLISH_BY_ZENDESK but the code doesn't starts with EN" do
      subject { TranslationLocale.new(name: 'Deutsch', locale: 'de', parent: english_by_zendesk) }
      it { assert subject.top_level? }
    end

    describe "when the parent is ENGLISH_BY_ZENDESK but the code starts with EN" do
      subject { TranslationLocale.new(name: 'English (UK)', locale: 'en-gb', parent: english_by_zendesk) }
      it { refute subject.top_level? }
    end
  end

  describe '#locale_code' do
    it 'returns the first part of the locale' do
      assert_equal 'en-US', subject.locale
      assert_equal 'en', subject.locale_code
    end
  end
end
