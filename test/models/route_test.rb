require_relative "../support/test_helper"

SingleCov.covered!

describe Route do
  fixtures :accounts, :account_settings, :brands, :routes, :recipient_addresses

  let(:account) { accounts(:minimum) }
  let(:account_route) { FactoryBot.create(:route, account: account) }

  describe '.find_by_fqdn' do
    it 'finds route by subdomain' do
      assert_equal account_route, Route.find_by_fqdn("#{account_route.subdomain}.#{Zendesk::Configuration.fetch(:host)}")
    end

    it 'finds a route by hostmapping' do
      account_route.update_column(:host_mapping, "foo.bar.com")
      assert_equal account_route, Route.find_by_fqdn("foo.bar.com")
    end
  end

  describe "changes to subdomain and host_mapping" do
    before do
      Route.any_instance.stubs(:valid_cname_record?).returns(true)
    end

    describe "on agent route" do
      let(:agent_route) { account.route }

      it 'propagates the change to the account' do
        agent_route.subdomain = "foo"
        agent_route.host_mapping = "foo.bar.com"
        agent_route.save!
        account.reload
        assert_equal "foo", account.subdomain
        assert_equal "foo.bar.com", account.host_mapping
      end

      it 'touches the brand' do
        brand = FactoryBot.create(:brand)
        agent_route.stubs(brand: brand)

        agent_route.subdomain = "foo"
        agent_route.host_mapping = "foo.bar.com"
        agent_route.brand.expects(:touch_without_callbacks).returns(true)
        agent_route.save!
      end
    end

    describe "on another route" do
      let(:another_route) { FactoryBot.create(:route, account: account) }

      it "shouldn't propagate the change to the account" do
        another_route.subdomain = "foo"
        another_route.host_mapping = "foo.bar.com"
        another_route.save!
        account.reload
        assert_not_equal "foo", account.subdomain
        assert_not_equal "foo.bar.com", account.host_mapping
      end

      it 'touches the brand' do
        brand = FactoryBot.create(:brand)
        another_route.stubs(brand: brand)

        another_route.subdomain = "foo"
        another_route.host_mapping = "foo.bar.com"
        another_route.brand.expects(:touch_without_callbacks).returns(true)
        another_route.save!
      end
    end
  end

  describe "#destroy" do
    it "takes recipient_addresses with it" do
      recipient_address = recipient_addresses(:without_name)
      recipient_address.update_column(:email, "foo@#{account_route.default_host}")
      assert_difference("RecipientAddress.count(:all)", -1) { account_route.destroy }
      assert_raise(ActiveRecord::RecordNotFound) { recipient_address.reload }
    end
  end

  describe "#active_brand" do
    let(:brand) { FactoryBot.create(:brand) }
    let(:account_route) { brand.route }

    it "is brand when active" do
      assert account_route.active_brand
    end

    it "is nil when inactive" do
      brand.update_column(:active, false)
      refute account_route.active_brand
    end

    it "is nil when nil" do
      brand.destroy
      refute account_route.reload.active_brand
    end
  end

  describe "#update_recipient_address_host" do
    it "changes recipient addresses to new host" do
      recipient_addresses(:not_default).update_column(:email, "foo@#{account_route.default_host}") # not valid since route is not on a brand
      account_route.update_attributes!(subdomain: "newsubdomain")
      assert_equal [
        "foo@newsubdomain.zendesk-test.com",
        "support@minimum.zendesk-test.com",
        "support@zdtestlocalhost.com",
        "without_name@example.net"
      ], account.recipient_addresses.map(&:email).sort
    end
  end

  describe "kasket" do
    before do
      account.stubs(:pod_local?).returns(true)
      Route.any_instance.stubs(:account).returns(account)
    end

    it "caches on id" do
      account
      assert_sql_queries(1) { assert account.route }
      account.reload
      assert_sql_queries(0) { assert account.route }
    end

    it "caches on subdomain" do
      assert_sql_queries(1) do
        2.times { assert Route.find_by_subdomain("minimum") }
      end
    end

    it "caches on host_mapping" do
      account.route.update_column(:host_mapping, "foobar.com")
      assert_sql_queries 1 do
        2.times { assert Route.find_by_host_mapping("foobar.com") }
      end
    end
  end

  describe "#audit_attribute" do
    let(:brand) { FactoryBot.create(:brand, account_id: account.id) }

    before do
      account_route.stubs(brand: brand)
      assert_equal 0, account_route.cia_events.size
      account.stubs(:has_multiple_brands?).returns(true)
    end

    it "audits subdomain changes" do
      CIA.audit actor: users(:minimum_agent) do
        account_route.update_attribute(:subdomain, "changed")
      end

      cia_event = account_route.reload.cia_events.first
      assert cia_event.present?
      assert_equal account_route.brand.name, cia_event.source_display_name
    end

    it "audits host_mapping changes" do
      account.stubs(:valid_cname_record?).returns(true)

      CIA.audit actor: users(:minimum_agent) do
        account_route.update_attribute(:host_mapping, "foo.bar.com")
      end

      cia_event = account_route.reload.cia_events.first
      assert cia_event.present?
    end

    it "does not audit in accounts without multiple brands" do
      account.stubs(:has_multiple_brands?).returns(false)

      CIA.audit actor: users(:minimum_agent) do
        account_route.update_attribute(:subdomain, "changed")
      end

      assert_equal 0, account_route.reload.cia_events.size
    end

    it "does not audit when there's no brand, for example on subdomain rename when we soft delete a brand" do
      account_route.stubs(brand: nil)

      CIA.audit actor: users(:minimum_agent) do
        account_route.update_attribute(:subdomain, "changed")
      end

      assert_equal 0, account_route.reload.cia_events.size
    end
  end
end
