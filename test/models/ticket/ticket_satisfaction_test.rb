require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 5

describe Ticket do
  fixtures :accounts, :ticket_fields, :custom_field_options, :tickets, :users, :ticket_forms, :groups, :organizations, :account_settings, :events

  describe "#satisfaction_rating_comment" do
    let(:ticket) { tickets(:minimum_1) }

    it "does not use an index on an archived (v1) ticket" do
      ticket.stubs(:archived?).returns(true)
      ticket.stubs(satisfaction_rating_comments: stub(first: 1, loaded?: false))
      assert_equal 1, ticket.satisfaction_comment_event
    end

    it "does not use an index if the comment is loaded" do
      ticket.stubs(satisfaction_rating_comments: stub(first: 1, loaded?: true))
      assert_equal 1, ticket.satisfaction_comment_event
    end

    it "uses an index on normal ticket" do
      sql_queries = sql_queries do
        ticket.satisfaction_comment_event
      end
      assert_includes sql_queries.last, "SELECT  `events`.* FROM events USE INDEX(index_events_on_ticket_id_and_type)"
    end
  end

  describe "#satisfaction_reason" do
    let(:ticket) { tickets(:minimum_1) }

    before do
      Satisfaction::Reason.create_system_reasons_for(accounts(:support))
      Satisfaction::Reason.create_system_reasons_for(accounts(:minimum))
      ticket.satisfaction_score = SatisfactionType.BAD
      ticket.satisfaction_reason_code = Satisfaction::Reason::OTHER
    end

    it "returns the correct reason for the current account" do
      assert_equal ticket.satisfaction_reason.reason_code, Satisfaction::Reason::OTHER
      assert_equal ticket.satisfaction_reason.account_id, ticket.account_id
    end
  end

  describe "#satisfaction_changed?" do
    let(:ticket) { tickets(:minimum_1) }

    describe "when the satisfaction_score changes" do
      before do
        assert_not_equal ticket.satisfaction_score, SatisfactionType.GOOD
        ticket.satisfaction_score = SatisfactionType.GOOD
      end

      it "returns true" do
        assert ticket.satisfaction_changed?
      end
    end

    describe "when the satisfaction_reason_code changes" do
      before do
        assert_not_equal ticket.satisfaction_reason_code, Satisfaction::Reason::OTHER
        ticket.satisfaction_reason_code = Satisfaction::Reason::OTHER
      end

      it "returns true" do
        assert ticket.satisfaction_changed?
      end
    end

    describe "when score does not change" do
      describe "and the comment changes" do
        before do
          assert_not_equal ticket.satisfaction_comment, "new comment"
          ticket.satisfaction_comment = "new comment"
        end

        it "returns true" do
          assert ticket.satisfaction_changed?
        end
      end

      describe "and the comment does not change" do
        before do
          ticket.satisfaction_comment = ticket.satisfaction_comment
        end

        it "returns false" do
          refute ticket.satisfaction_changed?
        end
      end
    end
  end

  describe "#satisfaction_score" do
    before do
      @ticket = tickets(:minimum_1)
    end

    describe "rating the ticket with an empty comment" do
      before do
        @ticket.satisfaction_comment = ""
        @ticket.satisfaction_score = SatisfactionType.GOOD
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end

      it "sets the satisfaction score to GOOD" do
        assert_equal SatisfactionType.GOOD, @ticket.satisfaction_score
      end
    end

    describe "Rating the ticket as good" do
      before do
        @ticket.satisfaction_score = SatisfactionType.GOOD
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end

      it "sets the satisfaction score to GOOD" do
        assert_equal SatisfactionType.GOOD, @ticket.satisfaction_score
      end

      it "sets the satisfaction reason code to NONE" do
        assert_equal Satisfaction::Reason::NONE, @ticket.satisfaction_reason_code
      end

      describe "And then adding a comment to the ticket" do
        before do
          @ticket.satisfaction_comment = 'Service rocks'
          assert_equal false, @ticket.satisfaction_score_changed?
          @ticket.will_be_saved_by(@ticket.requester)
          @ticket.save!
        end

        it "sets the satisfaction score to GOODWITHCOMMENT" do
          assert_equal SatisfactionType.GOODWITHCOMMENT, @ticket.satisfaction_score
        end

        describe "and then adding another comment with the same satisfaction_score" do
          before do
            @ticket.satisfaction_comment = 'Service rocks version 2.0'
            assert_equal false, @ticket.satisfaction_score_changed?
            @ticket.will_be_saved_by(@ticket.requester)
          end

          it "creates a new satisfaction_rating with the new comment" do
            assert_difference("@ticket.reload.satisfaction_ratings.size") do
              @ticket.save!
            end

            assert_equal 'Service rocks version 2.0', @ticket.satisfaction_comment
          end
        end
      end

      describe "And then rating the ticket as bad with comment" do
        before do
          @ticket.satisfaction_score = SatisfactionType.BAD
          @ticket.satisfaction_comment = 'Service sucks'
          @ticket.will_be_saved_by(@ticket.requester)
          @ticket.save!
        end

        it "sets the satisfaction score to BADWITHCOMMENT" do
          assert_equal SatisfactionType.BADWITHCOMMENT, @ticket.satisfaction_score
        end
      end
    end

    describe "Rating the ticket as bad" do
      before do
        @ticket.satisfaction_score = SatisfactionType.BAD
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end

      it "sets the satisfaction score to BAD" do
        assert_equal SatisfactionType.BAD, @ticket.satisfaction_score
      end

      it "sets the satisfaction reason code to NONE" do
        assert_equal Satisfaction::Reason::NONE, @ticket.satisfaction_reason_code
      end

      describe "And then rating the ticket as good with comment" do
        before do
          @ticket.satisfaction_score = SatisfactionType.GOOD
          @ticket.satisfaction_comment = 'Service rocks'
          @ticket.will_be_saved_by(@ticket.requester)
          @ticket.save!
        end

        it "sets the satisfaction score to GOODWITHCOMMENT" do
          assert_equal SatisfactionType.GOODWITHCOMMENT, @ticket.satisfaction_score
        end
      end
    end

    describe "Rating the ticket as good with comment " do
      before do
        @ticket.satisfaction_score = SatisfactionType.GOOD
        @ticket.satisfaction_comment = "service rocks"
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end

      it "Sets the satisfaction score to GOODWITHCOMMENT" do
        assert_equal SatisfactionType.GOODWITHCOMMENT, @ticket.satisfaction_score
      end

      describe "And then rating it as bad" do
        before do
          @ticket.satisfaction_score = SatisfactionType.BAD
          @ticket.will_be_saved_by(@ticket.requester)
          @ticket.save!
        end

        it "Sets the satisfaction score to BAD" do
          assert_equal SatisfactionType.BAD, @ticket.satisfaction_score
        end
      end
    end

    describe "Rating the ticket as bad with comment " do
      before do
        @ticket.satisfaction_score = SatisfactionType.BAD
        @ticket.satisfaction_comment = "service sucks"
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end

      it "Sets the satisfaction score to BADWITHCOMMENT" do
        assert_equal SatisfactionType.BADWITHCOMMENT, @ticket.satisfaction_score
      end

      describe "And then rating it as good" do
        before do
          @ticket.satisfaction_score = SatisfactionType.GOOD
          @ticket.will_be_saved_by(@ticket.requester)
          @ticket.save!
        end

        it "Sets the satisfaction score to GOOD" do
          assert_equal SatisfactionType.GOOD, @ticket.satisfaction_score
        end
      end
    end
  end

  describe "saving ticket satisfaction score" do
    before do
      @ticket                    = tickets(:minimum_1)
      @token                     = Tokens::SatisfactionRatingToken.create!(ticket: @ticket)
      @ticket.satisfaction_score = SatisfactionType.GOOD
      @ticket.will_be_saved_by(@ticket.requester)
    end

    describe "with csr token retention feature" do
      before do
        Account.any_instance.stubs(:has_csr_token_retention?).returns(true)
        @ticket.save!
      end
      it 'retains the token' do
        assert Tokens::SatisfactionRatingToken.find_by_id(@token.id).present?
      end
    end

    describe "without csr token retention feature" do
      before do
        Account.any_instance.stubs(:has_csr_token_retention?).returns(false)
        @ticket.save!
      end
      it 'consumes the token' do
        refute Tokens::SatisfactionRatingToken.find_by_id(@token.id).present?
      end
    end
  end
end
