require_relative "../../support/test_helper"

SingleCov.covered!

describe Ticket::Bookmark do
  fixtures :all

  before do
    @ticket = tickets(:minimum_1)
    @bookmark = Ticket::Bookmark.create!(ticket: @ticket, user: @ticket.assignee, account: @ticket.account)
  end

  describe "with active ticket" do
    before { @bookmark.reload }

    it "retains ticket association" do
      assert_equal @ticket, @bookmark.ticket
    end
  end

  describe "ticket deletion" do
    it "is deleted when the ticket gets deleted" do
      @bookmark.ticket.will_be_saved_by(@bookmark.ticket.account.owner)
      @bookmark.ticket.soft_delete!

      assert_nil Ticket::Bookmark.find_by_id(@bookmark.id)
    end

    it "there are no deletion queries when no ticket bookmark exists" do
      @bookmark.destroy
      @sql_queries = sql_queries do
        @ticket.will_be_saved_by(@ticket.account.owner)
        @ticket.soft_delete!
      end
      assert_sql_queries(0, /DELETE FROM `ticket_bookmarks`/)
    end
  end

  it "is deleted when the archived ticket gets deleted" do
    archive_and_delete(@ticket)
    @archived_ticket = Ticket.find(@ticket.id)
    @archived_ticket.will_be_saved_by(@archived_ticket.account.owner)
    @archived_ticket.soft_delete!

    assert_nil Ticket::Bookmark.find_by_id(@bookmark.id)
  end

  it "is deleted when the user gets deleted" do
    @bookmark.user.current_user = @bookmark.user.account.owner
    @bookmark.user.delete!

    assert_nil Ticket::Bookmark.find_by_id(@bookmark.id)
  end
end
