require_relative "../../support/test_helper"

SingleCov.not_covered!

describe 'Ticket::AssignableAgents' do
  fixtures :accounts, :subscriptions, :users, :user_identities,
    :memberships, :groups, :tickets

  describe '#assignable_agents' do
    describe 'for an account without permission sets' do
      before do
        @account = accounts(:with_groups)
        Account.any_instance.stubs(:has_permission_sets?).returns(false)

        @ticket = tickets(:with_groups_1)
        @non_restricted = users(:with_groups_agent1)
        refute @non_restricted.restriction_id == RoleRestrictionType.Groups, 'Fixture problem'
        @group_restricted = users(:with_groups_agent_groups_restricted)
        assert_equal RoleRestrictionType.Groups, @group_restricted.restriction_id, 'Fixture problem'
        refute @group_restricted.in_group?(@ticket.group), 'Fixture problem'
        @inactive = users(:with_groups_agent2)
        # need to update this without triggering callbacks:
        User.connection.execute "update users set is_active = 0 where id = #{@inactive.id.to_i}"
      end

      it "includes the correct agents" do
        users = User.assignable(@account).in_group(@ticket.group_id)
        users.must_include @non_restricted
        users.wont_include @group_restricted
        users.wont_include @inactive
      end
    end

    describe 'for an account with permission sets' do
      before do
        @account = accounts(:minimum)
        Account.any_instance.stubs(:has_permission_sets?).returns(true)

        @light_agents = PermissionSet.create_light_agent!(@account)
      end

      subject { User.assignable(@account).in_group(tickets(:minimum_1).group_id) }

      describe "with a real light agent set" do
        before do
          @light = users(:minimum_agent)
          @light.update_attribute(:permission_set_id, @light_agents.id)
        end

        it "includes the correct agents" do
          subject.must_include users(:minimum_admin)
          subject.wont_include @light
        end
      end
    end

    describe 'for a new ticket' do
      before do
        @account = accounts(:with_groups)

        @ticket = Ticket.new(
          account: @account,
          group: groups(:with_groups_group1)
        )

        @agent = users(:with_groups_agent_groups_restricted)

        refute @agent.in_group?(@ticket.group), 'Fixture problem'
      end

      subject { User.assignable(@account).in_group(@ticket.group_id) }

      it "doesn not include non-matching group-restricted agents" do
        subject.wont_include @agent
      end
    end
  end
end
