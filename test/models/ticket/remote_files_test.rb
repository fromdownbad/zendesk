require_relative "../../support/test_helper"
require_relative "../../support/attachment_test_helper"

SingleCov.covered!

describe "when deleting a ticket" do
  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_end_user) }
  let(:agent) { users(:minimum_agent) }
  let(:raw_email_identifier) { '1/msg.eml' }
  let(:json_email_identifier) { '1/msg.json' }
  let(:ticket) do
    ticket = account.tickets.new(requester: user, description: "Help me")
    ticket.will_be_saved_by(user)
    ticket.audit.metadata[:system][:raw_email_identifier] = raw_email_identifier
    ticket.audit.metadata[:system][:json_email_identifier] = json_email_identifier
    ticket.audit.via_id = ViaType.MAIL
    ticket.save!
    ticket
  end
  let(:statsd_client) { ticket.send(:statsd_client) }

  before { account.stubs(nice_id_sequence: stub(next: 123)) }

  describe "when deleting attachments" do
    before do
      @attachment = create_attachment("#{Rails.root}/test/files/normal_1.jpg", ticket.requester)
      @attachment.update_attribute :ticket_id, ticket.id
      @attachment.update_column :source_type, "Comment"
      @attachment.update_column :source_id, ticket.comments.last.id
    end

    it "deletes all attachments on ticket" do
      ticket.delete_attachments
      ticket.save!
      @attachment.reload
      assert ticket.comments.any? { |comment| comment.attachments.any? }
      assert_empty @attachment.stores
    end
  end

  describe "when deleting raw emails" do
    it "deletes the remote files and updates the audit metadata" do
      RemoteFiles::File.any_instance.expects(:delete_now!).twice
      assert_equal "{\"system\":{\"raw_email_identifier\":\"1/msg.eml\",\"json_email_identifier\":\"1/msg.json\"},\"custom\":{}}", ticket.audits.first.value_previous
      ticket.delete_raw_emails
      ticket.reload
      assert_equal "{\"system\":{\"raw_email_identifier\":null,\"json_email_identifier\":null},\"custom\":{}}", ticket.audits.first.value_previous
    end

    describe "and eml_identifier is blank" do
      let(:raw_email_identifier) { '' }

      it "deletes the remote json file and updates the audit metadata" do
        RemoteFiles::File.any_instance.expects(:delete_now!).once
        assert_equal "{\"system\":{\"raw_email_identifier\":\"\",\"json_email_identifier\":\"1/msg.json\"},\"custom\":{}}", ticket.audits.first.value_previous
        ticket.delete_raw_emails
        ticket.reload
        assert_equal "{\"system\":{\"raw_email_identifier\":\"\",\"json_email_identifier\":null},\"custom\":{}}", ticket.audits.first.value_previous
      end

      describe "and json_identifier is blank" do
        let(:json_email_identifier) { '' }

        it "does not delete remote files" do
          RemoteFiles::File.any_instance.expects(:delete_now!).never
          assert_equal "{\"system\":{\"raw_email_identifier\":\"\",\"json_email_identifier\":\"\"},\"custom\":{}}", ticket.audits.first.value_previous
          ticket.delete_raw_emails
          ticket.reload
          assert_equal "{\"system\":{\"raw_email_identifier\":\"\",\"json_email_identifier\":\"\"},\"custom\":{}}", ticket.audits.first.value_previous
        end
      end
    end
  end
end
