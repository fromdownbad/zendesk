require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 34

describe Ticket do
  fixtures :accounts, :ticket_fields, :custom_field_options, :tickets, :users, :ticket_forms, :groups, :organizations, :account_settings, :events

  describe Ticket do
    before do
      @ticket = Ticket.new
      @ticket.account = accounts(:minimum)
      @ticket.account.stubs(:is_serviceable?).returns(true)
    end

    subject { @ticket }

    describe "with a new requester that is invalid" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.id = nil

        @ticket.build_requester(name: 'Sir Skisalot', email: 'ski@example.com', account: @ticket.account)
        @ticket.current_user = users(:minimum_agent)

        assert @ticket.valid?

        User.any_instance.stubs(:valid?).returns(false)
        User.any_instance.stubs(:errors).returns([["foo", "is not valid"]])

        refute @ticket.valid?
      end

      describe "for a non-system via type" do
        before do
          @ticket.via_id = ViaType.WEB_FORM
        end

        it "is invalid" do
          refute @ticket.valid?
        end
      end

      describe "for a system via type" do
        before do
          @ticket.via_id = 21
        end

        it "is invalid" do
          refute @ticket.valid?
        end
      end
    end

    describe "when the via_id is invalid" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.via_id = 127
      end

      it "does not validate" do
        refute @ticket.valid?
        assert_equal 1, @ticket.errors.full_messages.length
        assert_equal "The via_id specified is invalid", @ticket.errors.full_messages[0]
      end
    end

    describe "when creating a followup ticket" do
      before do
        account = accounts(:minimum)
        agent = users(:minimum_agent)

        @closed_ticket_initializer = Zendesk::Tickets::Initializer.new(account, agent, ticket: { subject: "Hello", description: "Goodbye", status_id: StatusType.CLOSED})
        @closed_ticket = @closed_ticket_initializer.ticket
        @closed_ticket.will_be_saved_by(agent)
        @closed_ticket.save!

        @valid_ticket_initializer = Zendesk::Tickets::Initializer.new(account, agent, ticket: { subject: "Hello", description: "Goodbye", via_followup_source_id: @closed_ticket.nice_id })
        @new_valid_ticket = @valid_ticket_initializer.ticket
        @new_valid_ticket.will_be_saved_by(agent)

        # Validation occurs on :create, so we need a new ticket rather than a fixture
        @invalid_ticket_initializer = Zendesk::Tickets::Initializer.new(account, agent, ticket: { subject: "Hello", description: "Goodbye", via_followup_source_id: -1})
        @new_invalid_ticket = @invalid_ticket_initializer.ticket
        @new_invalid_ticket.will_be_saved_by(agent)
      end

      describe "successfully creates a followup ticket with a valid followup source ticket id" do
        it "results in a valid ticket" do
          assert @new_valid_ticket.save!
        end
      end

      describe "with an invalid followup source ticket id" do
        it "is not valid" do
          assert_raise ActiveRecord::RecordInvalid do
            @new_invalid_ticket.save!
          end
        end

        describe_with_arturo_disabled :validate_closed_ticket_for_followup_tickets do
          it "results in a valid ticket" do
            assert @new_invalid_ticket.save!
          end
        end
      end
    end

    describe "tickets with status hold" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.status_id = StatusType.HOLD
        @ticket.will_be_saved_by(@ticket.assignee)
      end

      describe "on accounts that do not have the setting" do
        before { @ticket.account.stubs(:use_status_hold?).returns(false) }

        it "results in a validation error" do
          refute @ticket.valid?
        end
      end

      describe "on accounts that have the setting" do
        before { @ticket.account.stubs(:use_status_hold?).returns(true) }

        it "results in a validation" do
          assert @ticket.valid?
        end
      end
    end
  end

  describe "organization validation" do
    let(:ticket) { tickets(:minimum_2).dup }
    let(:requester) { ticket.requester }
    let(:organization) { organizations(:minimum_organization3) }
    let(:invalid_organization) { organizations(:minimum_organization2) }

    before { ticket.will_be_saved_by(users(:minimum_agent)) }

    it "is valid if it is being unset" do
      requester.stubs(organization: nil)
      ticket.organization = nil
      assert ticket.valid?
    end

    it "is invalid if the new organization is not in the requesters' organizations" do
      refute requester.organizations.include?(organization)
      ticket.organization = organization

      refute ticket.valid?
      assert_equal(
        ["is not a valid organization for the requester."],
        ticket.errors[:organization_id]
      )
    end

    it 'is valid if skip_organization_validations is enabled' do
      requester.stubs(organization_id: 123)
      ticket.organization = organization
      Account.any_instance.stubs(has_skip_organization_validation?: true)

      assert ticket.valid?
    end

    it 'is invalid if skip_organization_validations is disabled' do
      requester.stubs(organization_id: 123)
      ticket.organization = organization
      Account.any_instance.stubs(has_skip_organization_validation?: false)
      Zendesk::StatsD::Client.any_instance.expects(:increment).
        with('invalid_organization', tags: ["account_id:#{ticket.account_id}", "type:validation_error_added"]).
        once

      refute ticket.valid?
    end

    describe 'logging around invalid organizations' do
      describe_with_arturo_enabled :invalid_organization_logging do
        it 'with requester.organization_id not in requester.organization_memberships organization_ids' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).
            with('invalid_organization', tags: ["account_id:#{ticket.account_id}", "type:invalid_requester"]).
            once
          ticket.requester.stubs(:organization_id).returns 123
          ticket.valid?
        end

        it 'with ticket.organization_id not in requester.organization_memberships organization_ids' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).
            with('invalid_organization', tags: ["account_id:#{ticket.account_id}", "type:invalid_ticket"]).
            once
          Zendesk::StatsD::Client.any_instance.expects(:increment).
            with('invalid_organization', tags: ["account_id:#{ticket.account_id}", "type:validation_error_added"]).
            once
          ticket.stubs(:organization_id).returns 123
          ticket.valid?
        end
      end
    end

    describe_with_arturo_disabled :endusers_org_active_fix do
      it "does not fix the organizations when they are invalid" do
        ticket.stubs(:valid_organization?).returns(false)
        ticket.stubs(:valid_user_organization?).returns(false)
        ticket.expects(:fix_ticket_organization).never
        ticket.expects(:fix_user_organization).never
        ticket.valid?
      end
    end

    describe_with_arturo_enabled :endusers_org_active_fix do
      [{ticket_org_valid: true, requester_org_valid: true},
       {ticket_org_valid: false, requester_org_valid: false}].each do |scenario|
        it "fixes when false. Ticket org: #{scenario[:ticket_org_valid]} Requester org: #{scenario[:requester_org_valid]}" do
          # we only want to call the fix methods when the org is invalid
          ticket_fix_call_count = scenario[:ticket_org_valid] ? 0 : 1
          requester_fix_call_count = scenario[:requester_org_valid] ? 0 : 1
          ticket.expects(:valid_organization?).returns(scenario[:ticket_org_valid])
          ticket.expects(:valid_user_organization?).returns(scenario[:requester_org_valid])
          ticket.expects(:fix_ticket_organization).times(ticket_fix_call_count)
          ticket.expects(:fix_user_organization).times(requester_fix_call_count)
          ticket.valid?
        end
      end

      describe "with an invalid ticket organization" do
        it "fixes the ticket organization" do
          ticket.organization = invalid_organization
          refute_equal ticket.organization_id, requester.organization_id
          ticket.expects(:fix_user_organization).never
          assert ticket.valid?
          assert_equal ticket.organization_id, requester.organization_id
        end
      end

      describe "with an invalid requester organization" do
        it "fixes the requester's organization" do
          requester.update_column(:organization_id, invalid_organization.id)
          refute_equal ticket.organization_id, requester.organization_id
          ticket.expects(:fix_ticket_organization).never
          assert ticket.valid?
          assert_equal ticket.organization_id, requester.reload.organization_id
        end
      end
    end
  end

  describe "#check_assignee_belongs_to_group" do
    before do
      @ticket = tickets(:with_groups_1)
    end

    describe "for a ticket whose assignee is not in the ticket's group" do
      # with_groups_admin is only in group 2
      before { @ticket.assignee_id = users(:with_groups_admin).id }

      it "adds an error on the assignee" do
        @ticket.send(:check_assignee_belongs_to_group)

        assert @ticket.errors[:assignee].any?
      end
    end

    describe "for a ticket whose assignee is in the ticket's group" do
      before { @ticket.assignee_id = users(:with_groups_agent1).id }

      it "does not add an error on the assignee" do
        @ticket.send(:check_assignee_belongs_to_group)

        assert_equal [], @ticket.errors[:assignee]
      end
    end
  end

  describe "#validate_deletion_rights" do
    it "adds an error if there are incidents attached" do
      Account.any_instance.stubs(:has_extended_ticket_types?).returns(true)
      problem_ticket = tickets(:minimum_1)
      problem_ticket.ticket_type_id = TicketType.PROBLEM
      problem_ticket.will_be_saved_by problem_ticket.account.owner
      problem_ticket.save!

      incident = tickets(:minimum_2)
      incident.ticket_type_id = TicketType.INCIDENT
      incident.linked_id = problem_ticket.id
      incident.will_be_saved_by incident.account.owner
      incident.save!

      problem_ticket.will_be_saved_by problem_ticket.account.owner
      problem_ticket.soft_delete
      problem_ticket.reload
      refute_equal StatusType.DELETED, problem_ticket.status_id
    end
  end

  describe "agent required fields" do
    it "validates agent required fields when solving" do
      ticket_fields(:field_integer_custom).update_attribute(:is_required, true)
      ticket_fields(:minimum_field_priority).update_attribute(:is_required, true)

      ticket = tickets(:minimum_2)
      ticket.assignee = users(:minimum_admin)
      ticket.status_id = StatusType.SOLVED
      ticket.priority_id = 0
      ticket.will_be_saved_by(users(:minimum_admin))
      refute ticket.save # Priority and custom integer field required
      assert ticket.errors[:base].join =~ /#{ticket_fields(:field_integer_custom).title}: is required/

      ticket.priority_id = PriorityType.Normal
      ticket.will_be_saved_by(users(:minimum_admin))
      refute ticket.save # Custom integer field still required
      assert ticket.errors[:base].join =~ /#{ticket_fields(:field_integer_custom).title}: is required/

      ticket.fields = {ticket_fields(:field_integer_custom).id.to_s => '12'}
      ticket.will_be_saved_by(users(:minimum_admin))
      assert ticket.save
    end

    it "validates agent required fields when solving with ticket forms" do
      Account.any_instance.stubs(:has_ticket_forms?).returns(true)
      ticket_field_1 = ticket_fields(:field_integer_custom)
      ticket_field_2 = ticket_fields(:minimum_field_priority)
      ticket = tickets(:minimum_2)
      account = ticket.account
      ticket_field_1.update_attribute(:is_required, true)
      ticket_field_2.update_attribute(:is_required, true)

      # create a ticket form with required field
      params = { ticket_form: { name: "Wombat", display_name: "Wombats are everywhere", position: 2,
                                default: false, active: true, end_user_visible: true, ticket_field_ids: [ticket_field_1.id] } }
      ticket_form = Zendesk::TicketForms::Initializer.new(account, params).ticket_form

      ticket.ticket_form_id = ticket_form.id
      ticket.will_be_saved_by(users(:minimum_admin))
      assert ticket.save # set ticket form on the ticket

      ticket.assignee = users(:minimum_admin)
      ticket.status_id = StatusType.SOLVED
      ticket.will_be_saved_by(users(:minimum_admin))
      refute ticket.save # custom integer field required
      assert_includes ticket.errors[:base].join, "#{ticket_fields(:field_integer_custom).title}: is required"

      ticket.fields = {ticket_fields(:field_integer_custom).id.to_s => '12'}
      ticket.will_be_saved_by(users(:minimum_admin))
      assert ticket.save!

      # Can't change required field to nil after it is solved
      ticket.fields = {ticket_fields(:field_integer_custom).id.to_s => nil}
      ticket.will_be_saved_by(users(:minimum_admin), via_id: ViaType.WEB_FORM)
      refute ticket.save
      assert_includes ticket.errors[:base].join, "#{ticket_fields(:field_integer_custom).title}: is required"

      # Can't change required field to nil after it is solved via non-web_form
      ticket.fields = {ticket_fields(:field_integer_custom).id.to_s => nil}
      ticket.will_be_saved_by(users(:minimum_admin), via_id: ViaType.TWITTER)
      refute ticket.save
      assert_includes ticket.errors[:base].join, "#{ticket_fields(:field_integer_custom).title}: is required"
    end

    it "uses #agent_required_fields to check agent required fields when solving" do
      ticket_field_1 = ticket_fields(:field_integer_custom)
      ticket = tickets(:minimum_2)
      ticket.stubs(:agent_required_fields).returns([ticket_field_1])

      ticket.assignee = users(:minimum_admin)
      ticket.status_id = StatusType.SOLVED
      ticket.will_be_saved_by(users(:minimum_admin))
      refute ticket.save # custom integer field required
      assert_includes ticket.errors[:base].join, "#{ticket_fields(:field_integer_custom).title}: is required"
    end

    it "doesn't add errors if #agent_required_fields returns an empty array" do
      ticket = tickets(:minimum_2)
      ticket.stubs(:agent_required_fields).returns([])

      ticket.assignee = users(:minimum_admin)
      ticket.status_id = StatusType.SOLVED
      ticket.will_be_saved_by(users(:minimum_admin))
      assert ticket.save
    end

    it "tests inactive form fields are validated" do
      Account.any_instance.stubs(:has_ticket_forms?).returns(true)
      end_user = users(:minimum_end_user)
      account = end_user.account

      # Create a default form with a required custom field which should not be validated
      default_form = ticket_forms(:default_ticket_form)
      default_form.update_attribute(:account_id, account.id)
      custom_field = ticket_fields(:field_text_custom)
      custom_field.update_attribute(:is_required, true)
      default_form.ticket_form_fields.create!(
        account: account,
        ticket_field: custom_field
      )

      # Create a custom form for the ticket to use
      custom_form = ticket_forms(:minimum_ticket_form)
      custom_form.update_attribute(:account_id, account.id)

      ticket = account.tickets.new(requester: end_user, description: 'hello', ticket_form_id: custom_form.id)
      ticket.will_be_saved_by(end_user, via_id: ViaType.WEB_FORM)

      # Assert the ticket using the custom form saves
      assert ticket.save!

      # Set the custom form to inactive
      custom_form.update_attribute(:active, false)

      # Assert the ticket using the inactive custom form solves/saves
      agent = users(:minimum_agent)
      ticket.will_be_saved_by(agent, via_id: ViaType.WEB_FORM)
      ticket.status_id = StatusType.SOLVED
      assert ticket.save!
    end

    describe "with updates from specific channels" do
      let(:account) { accounts(:minimum) }
      let(:user) { users(:minimum_admin) }

      before do
        @ticket = account.tickets.new(account_id: account.id, description: "No assignee")
        @ticket.assignee = nil
        @ticket.status_id = StatusType.SOLVED
        @ticket.via_id = via_id
        @ticket.will_be_saved_by(user)
      end

      [ViaType.LINKED_PROBLEM, ViaType.BATCH].each do |via_type|
        describe "current via is #{via_type}" do
          let(:via_id) { via_type }

          it "doesn't validate" do
            assert_valid @ticket
          end
        end
      end

      [ViaType.CLOSED_TICKET, ViaType.WEB_SERVICE, ViaType.MOBILE].each do |via_type|
        describe "current via is #{via_type}" do
          let(:via_id) { via_type }

          it "validates" do
            refute_valid @ticket
          end
        end
      end
    end

    describe 'when the update is via email' do
      let(:account)       { accounts(:minimum) }
      let(:agent)         { users(:minimum_agent) }
      let(:end_user)      { users(:minimum_end_user) }
      let(:ticket_field)  { FactoryBot.create(:field_tagger, is_required: true, is_required_in_portal: true, account: account) }
      let(:ticket_form1)  { Zendesk::TicketForms::Initializer.new(account, ticket_form: { ticket_field_ids: [ticket_field.id], position: 1, default: false, name: "foo1", in_all_brands: true }).ticket_form }
      let(:ticket_form2)  { Zendesk::TicketForms::Initializer.new(account, ticket_form: { ticket_field_ids: [], position: 2, default: true, name: "foo2", in_all_brands: true }).ticket_form }

      before do
        ticket_form1.save!
        ticket_form2.save!
      end

      it "skips validation for required fields" do
        ticket = account.tickets.new(requester: end_user, assignee: agent, description: 'hello', ticket_form_id: ticket_form2.id)
        ticket.will_be_saved_by(agent, via_id: ViaType.WEB_FORM)
        assert ticket.save

        ticket.update_column(:ticket_form_id, ticket_form1.id)
        ticket.update_column(:status_id, StatusType.SOLVED)

        ticket.reload
        ticket.add_comment(body: 'user comment', via_id: ViaType.MAIL)
        ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)

        assert ticket.save!
      end

      it "skips required fields validation when an agent is auto assigned because of a mail" do
        account.settings.assign_tickets_upon_solve = true
        account.save

        ticket = account.tickets.new(requester: end_user, assignee: agent, description: 'hello', ticket_form_id: ticket_form2.id)
        ticket.will_be_saved_by(agent, via_id: ViaType.WEB_FORM)
        assert ticket.save

        ticket.update_column(:ticket_form_id, ticket_form1.id)
        ticket.update_column(:status_id, StatusType.SOLVED)
        ticket.update_column(:assignee_id, nil)
        ticket.update_column(:group_id, nil)

        ticket.reload
        ticket.add_comment(body: 'user comment', via_id: ViaType.MAIL)
        ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
        assert ticket.save!
      end

      it "validates required fields when the ticket is solved in the context of a mail" do
        account.settings.assign_tickets_upon_solve = true
        account.save

        ticket = account.tickets.new(requester: end_user, assignee: agent, description: 'hello', ticket_form_id: ticket_form1.id)
        ticket.will_be_saved_by(agent, via_id: ViaType.WEB_FORM)
        assert ticket.save

        ticket.add_comment(body: 'user comment', via_id: ViaType.MAIL)
        ticket.status_id = StatusType.SOLVED
        ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
        assert_raise ActiveRecord::RecordInvalid do
          ticket.save!
        end
      end
    end
    # Mail API can set tags that change tagger ticket fields.
    # That can cause undesired mail rejections if the change reveals an always required field.
    describe "Email channel updating relevant fields on tickets with empty required fields" do
      let(:account) { accounts(:minimum) }
      let(:agent) { users(:minimum_agent) }
      let(:ticket) { tickets(:minimum_2) }
      let(:integer_ticket_field) { FactoryBot.create(:integer_ticket_field, account: account) }
      let(:checkbox_ticket_field) { FactoryBot.create(:checkbox_ticket_field, account: account) }

      before do
        ticket.update_column(:status_id, StatusType.SOLVED)
        ticket.reload
        ticket.stubs(:agent_required_fields).returns([integer_ticket_field])
        ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
      end

      it "skips validation when the update doesn't change fields" do
        ticket.add_comment(body: 'user comment', via_id: ViaType.MAIL)
        assert ticket.save!
      end

      it "skips validating fields when the update changes custom fields" do
        ticket.fields = { checkbox_ticket_field.id.to_s => true }
        assert ticket.save
      end

      it "skips validating fields when the update changes priority" do
        ticket.priority_id = PriorityType.High
        assert ticket.save
      end

      it "skips validating fields when the update changes type" do
        ticket.ticket_type_id = TicketType.Question
        assert ticket.save
      end
    end

    describe "Non web form channel updating solved tickets with empty required fields" do
      let(:account) { accounts(:minimum) }
      let(:agent) { users(:minimum_agent) }
      let(:ticket) { tickets(:minimum_2) }
      let(:integer_ticket_field) { FactoryBot.create(:integer_ticket_field, account: account) }
      let(:checkbox_ticket_field) { FactoryBot.create(:checkbox_ticket_field, account: account) }

      before do
        ticket.update_column(:status_id, StatusType.SOLVED)
        ticket.reload
        ticket.stubs(:agent_required_fields).returns([integer_ticket_field])
        ticket.will_be_saved_by(agent, via_id: ViaType.API_VOICEMAIL)
      end

      it "skips validation when the update doesn't change fields" do
        ticket.add_comment(body: 'user comment', via_id: ViaType.API_VOICEMAIL)
        assert ticket.save!
      end

      it "validates fields when the update changes custom fields" do
        ticket.fields = { checkbox_ticket_field.id.to_s => true }
        assert_raise ActiveRecord::RecordInvalid do
          ticket.save!
        end
      end

      it "validates fields when the update changes priority" do
        ticket.priority_id = PriorityType.High
        assert_raise ActiveRecord::RecordInvalid do
          ticket.save!
        end
      end

      it "validates fields when the update changes type" do
        ticket.ticket_type_id = TicketType.Question
        assert_raise ActiveRecord::RecordInvalid do
          ticket.save!
        end
      end
    end
  end

  describe "end-user required fields" do
    it "uses #end_user_required_fields to validate required in portal fields" do
      custom_ticket_field = ticket_fields(:field_text_custom)
      end_user = users(:minimum_end_user)
      account = end_user.account

      ticket = account.tickets.new(requester: end_user, description: 'hello')
      ticket.stubs(:end_user_required_fields).returns([custom_ticket_field])

      ticket.will_be_saved_by(end_user, via_id: ViaType.WEB_FORM)
      refute ticket.save
      assert_includes ticket.errors[:base].join, "#{custom_ticket_field.title_in_portal}: cannot be blank"
    end

    it "doesn't add errors if #end_user_required_fields returns an empty array" do
      end_user = users(:minimum_end_user)
      account = end_user.account

      ticket = account.tickets.new(requester: end_user, description: 'hello')
      ticket.stubs(:end_user_required_fields).returns([])

      ticket.will_be_saved_by(end_user, via_id: ViaType.WEB_FORM)
      assert ticket.save
    end

    it "validates required in portal field" do
      ticket_fields(:field_text_custom).update_attribute(:is_required_in_portal, true)
      requester = users(:minimum_end_user)
      ticket = requester.account.tickets.new(requester: requester, description: 'hello')

      ticket.will_be_saved_by(users(:minimum_end_user), via_id: ViaType.WEB_FORM)
      refute ticket.save

      ticket.will_be_saved_by(users(:minimum_end_user), via_id: ViaType.MAIL)
      assert ticket.save

      # followups
      ticket = requester.account.tickets.new(requester: requester, description: 'hello', via_followup_source_id: requester.account.closed_tickets[0].nice_id)

      ticket.original_via_id = ViaType.WEB_FORM
      ticket.will_be_saved_by(users(:minimum_end_user), via_id: ViaType.CLOSED_TICKET)
      refute ticket.save

      ticket.original_via_id = ViaType.MAIL
      ticket.will_be_saved_by(users(:minimum_end_user), via_id: ViaType.CLOSED_TICKET)
      ticket.set_followup_source(requester)
      assert ticket.save
    end

    it "validates required in portal field with ticket forms" do
      Account.any_instance.stubs(:has_ticket_forms?).returns(true)
      custom_ticket_field = ticket_fields(:field_text_custom)
      custom_ticket_field.update_attribute(:is_required_in_portal, true)
      ticket_fields(:field_textarea_custom).update_attribute(:is_required_in_portal, true)
      end_user = users(:minimum_end_user)
      account = end_user.account

      # create a ticket form with required field
      params = { ticket_form: { name: "Wombat", display_name: "Wombats are everywhere", position: 2,
                                default: false, active: true, end_user_visible: true, ticket_field_ids: [custom_ticket_field.id] } }
      ticket_form = Zendesk::TicketForms::Initializer.new(account, params).ticket_form

      ticket = account.tickets.new(requester: end_user, description: 'hello', ticket_form_id: ticket_form.id)

      ticket.will_be_saved_by(end_user, via_id: ViaType.WEB_FORM)
      refute ticket.save

      ticket.will_be_saved_by(end_user, via_id: ViaType.MAIL)
      assert ticket.save!

      # followups
      ticket = account.tickets.new(requester: end_user, description: 'hello', ticket_form_id: ticket_form.id, via_followup_source_id: account.closed_tickets[0].nice_id)

      ticket.original_via_id = ViaType.WEB_FORM
      ticket.will_be_saved_by(end_user, via_id: ViaType.CLOSED_TICKET)
      refute ticket.save

      ticket.original_via_id = ViaType.MAIL
      ticket.will_be_saved_by(end_user, via_id: ViaType.CLOSED_TICKET)
      ticket.set_followup_source(end_user)
      assert ticket.save!
    end

    it "doesn't add errors for fields not required in portal field" do
      requester = users(:minimum_end_user)
      ticket = requester.account.tickets.new(requester: requester, description: 'hello')
      ticket.will_be_saved_by(users(:minimum_end_user), via_id: ViaType.WEB_FORM)
      assert ticket.save
    end
  end
end
