require_relative "../../support/test_helper"

SingleCov.not_covered!

describe "Ticket Archive V2 Test" do
  fixtures :all

  describe Ticket do
    before do
      @account = accounts(:minimum)
      @ticket = @account.tickets.build(
        requester: users(:minimum_end_user),
        assignee: users(:minimum_agent),
        subject: "Archivable Ticket",
        description: "This is a description",
        organization: organizations(:minimum_organization1),
        external_id: 5,
        group: users(:minimum_agent).groups.first
      )

      @ticket.add_comment(body: "Hello, cleveland.", is_public: true)
      @ticket.ticket_field_entries.build(ticket_field: ticket_fields(:field_tagger_custom), value: 'booring')
      @ticket.current_tags = "honey badger hella"
      @ticket.will_be_saved_by(@ticket.assignee)
      @ticket.ticket_type_id = TicketType.PROBLEM
      @ticket.save!
    end

    around do |test|
      with_in_memory_archive_router(test)
    end

    it "supports as_archived" do
      data = @ticket.as_archived
      assert_equal data['nice_id'], @ticket.nice_id
      assert_equal data['assignee_id'], @ticket.assignee_id
    end

    it "includes events" do
      data = @ticket.as_archived
      assert data['events']
    end

    it "includes ticket_field entries" do
      data = @ticket.as_archived
      assert data['ticket_field_entries']
    end

    it "includes taggings" do
      data = @ticket.as_archived
      assert_equal 3, data['taggings'].size
    end

    describe "a ticket coming back from the archive" do
      before do
        archive_and_delete @ticket
        @archived = Ticket.where(id: @ticket.id).all_with_archived.first
      end

      it "is marked as archived" do
        assert @archived.archived?
      end

      it "fills out the comments association" do
        assert !@archived.comments.empty?
        assert @archived.comments.all { |c| c.is_a?(Comment) }
      end

      it "fills out the conversation_items association" do
        refute @archived.conversation_items.empty?, 'Archived tickets must contain conversation items'
      end

      it "fills out the audits association" do
        assert_equal @ticket.audits, @archived.audits
      end

      describe "the ticket's events" do
        it "fills out their ticket" do
          @archived.events.each do |e|
            assert_equal @archived, e.ticket
          end
        end

        it "fills out their audit has_one" do
          @archived.events.each do |e|
            if e.parent_id
              assert e.audit
            end
          end
        end

        describe "audits" do
          it "fills out their events has-many" do
            audit = @archived.audits.first
            assert !audit.events.empty?
          end
        end
      end

      it "supports #soft_delete!, marking the ticket_archive_stubs row as deleted" do
        @archived.will_be_saved_by(User.find(-1))
        @archived.soft_delete!
        stub = TicketArchiveStub.find(@archived.id)
        assert_equal StatusType.DELETED, stub.status_id
      end

      it "supports #soft_delete, marking the ticket_archive_stubs row as deleted" do
        @archived.will_be_saved_by(User.find(-1))
        @archived.soft_delete(validate: false)
        stub = TicketArchiveStub.find(@archived.id)
        assert_equal StatusType.DELETED, stub.status_id
      end

      describe "when an Audit has a parent_id (corrupt data, afaict)" do
        before do
          json = ZendeskArchive.router.get(@ticket.ticket_archive_stub).data
          audit = json["events"].find { |e| e["type"] == "Audit" }
          audit["parent_id"] = 12345
          ZendeskArchive.router.set(@ticket.ticket_archive_stub, json)
        end

        it "does not crash" do
          @archived = Ticket.where(id: @ticket.id).all_with_archived.first
        end
      end
    end

    describe "incidents" do
      before do
        tix = Array.new(2) do
          @ticket.update_column(:ticket_type_id, TicketType.PROBLEM)
          incident = @ticket.incidents.build(
            requester: users(:minimum_end_user),
            account: accounts(:minimum),
            assignee: users(:minimum_agent),
            subject: "Archivable Ticket",
            description: "This is a description",
            organization: organizations(:minimum_organization1),
            external_id: 5,
            group: users(:minimum_agent).groups.first
          )
          incident.add_comment(body: "Hello, cleveland.", is_public: true)
          incident.will_be_saved_by incident.assignee
          incident.save!
          incident
        end
        archive_and_delete(tix.first)
      end

      describe "count_incidents" do
        it "counts archived incidents" do
          assert_equal 2, @ticket.count_incidents
        end
      end
    end

    describe "creating followups" do
      it "is able to create a follow up to an archived ticket" do
        @ticket.status_id = StatusType.CLOSED
        @ticket.will_be_saved_by(User.find(-1))
        @ticket.save!

        archive_and_delete(@ticket)

        @followup = @account.tickets.build(
          requester: users(:minimum_end_user),
          assignee: users(:minimum_agent),
          subject: "followup",
          description: "description",
          via_followup_source_id: @ticket.nice_id
        )
        @followup.add_comment(body: "HI", is_public: true)
        @followup.will_be_saved_by @followup.requester
        @followup.set_followup_source(@followup.requester)
        @followup.save!
      end
    end

    describe "followups" do
      before do
        @ticket.status_id = StatusType.CLOSED
        @ticket.will_be_saved_by(User.find(-1))
        @ticket.save!

        @followup = @account.tickets.build(
          requester: users(:minimum_end_user),
          assignee: users(:minimum_agent),
          subject: "followup",
          description: "description",
          via_followup_source_id: @ticket.nice_id
        )
        @followup.add_comment(body: "HI", is_public: true)
        @followup.will_be_saved_by @followup.requester
        @followup.set_followup_source(@followup.requester)
        @followup.save!
      end

      describe "with the source in the archive" do
        before do
          archive_and_delete(@ticket)
        end

        it "is able to access the followup source" do
          assert_equal @ticket.id, @followup.followup_source.id
        end

        it "is able to access the followups from the source" do
          assert_equal @ticket.followups.first, @followup
        end
      end

      describe "with the followup in the archive" do
        before do
          archive_and_delete(@followup)
        end

        it "is able to the access the followups via followups_with_archived" do
          assert_equal 1, @ticket.followups_with_archived.size
        end
      end
    end

    describe "a ticket missing from the archive" do
      before do
        archive_and_delete @ticket
        ZendeskArchive.router.delete(@ticket.ticket_archive_stub)
      end

      it "records or raises an exception" do
        Rails.application.config.statsd.client.expects(:increment)
        @archived = Ticket.where(id: @ticket.id).all_with_archived.first
      end
    end

    describe "a ticket with wrong account_id" do
      before do
        archive_and_delete @ticket
        blob = ZendeskArchive.router.get(@ticket.ticket_archive_stub).data
        blob['account_id'] = @ticket.account_id * 10
        ZendeskArchive.router.set(@ticket.ticket_archive_stub, blob)
      end

      it "records or raises an exception" do
        Rails.application.config.statsd.client.expects(:increment)
        @archived = Ticket.where(id: @ticket.id).all_with_archived.first
      end
    end
  end
end
