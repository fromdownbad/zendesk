require_relative "../../support/test_helper"
require_relative '../../support/ticket_test_helper'

SingleCov.covered! uncovered: 43

describe Ticket do
  include TicketTestHelper

  fixtures :accounts, :ticket_fields, :custom_field_options, :tickets, :users, :ticket_forms, :groups, :organizations, :account_settings, :events

  describe "#fix_due_date" do
    before do
      @ticket = Ticket.new
      @ticket.account = accounts(:minimum)
      @ticket.account.stubs(:is_serviceable?).returns(true)
      @user = users(:minimum_agent)
    end

    describe "fixup_ticket_due_date_v2 feature disabled" do
      before do
        Arturo.disable_feature!(:fix_ticket_due_date_v2)
        @ticket.will_be_saved_by(@user)
      end

      describe "ticket is not a task" do
        before do
          @ticket.due_date = Time.new(2014, 01, 01, 15, 14, 13, 0)
          @ticket.ticket_type_id = TicketType.QUESTION
          @ticket.valid?
        end

        it "stores nil due date" do
          assert_nil @ticket.due_date
        end
      end

      describe "ticket is a task" do
        before do
          @ticket.ticket_type_id = TicketType.TASK
        end

        describe "ticket hour is zero" do
          before do
            @ticket.due_date = Time.new(2014, 01, 01, 0, 14, 13, 0)
            @ticket.valid?
          end

          it "'correct's by adding 12 hours" do
            assert_equal Time.new(2014, 01, 01, 12, 14, 13, 0), @ticket.due_date
          end
        end

        describe "ticket hour is not zero" do
          before do
            @ticket.due_date = Time.new(2014, 01, 01, 15, 14, 13, 0)
            @ticket.valid?
          end

          it "does not 'correct'" do
            assert_equal Time.new(2014, 01, 01, 15, 14, 13, 0), @ticket.due_date
          end
        end
      end
    end

    describe "fixup_ticket_due_date_v2 feature enabled" do
      before do
        Arturo.enable_feature!(:fix_ticket_due_date_v2)
      end

      describe "user not set" do
        before do
          @ticket.account.time_zone = 'Midway Island'
          @ticket.due_date = Time.new(2014, 01, 01, 23, 45, 0, 0)
          @ticket.ticket_type_id = TicketType.TASK
          @ticket.valid?
        end

        it "uses timezone from account" do
          assert_equal Time.new(2014, 1, 1, 23), @ticket.due_date
        end
      end

      describe "user is set" do
        before do
          @ticket.will_be_saved_by(@user)
        end

        describe "ticket is not a task" do
          before do
            @ticket.due_date = Time.new(2014, 01, 01, 15, 14, 13, 0)
            @ticket.ticket_type_id = TicketType.QUESTION
            @ticket.valid?
          end

          it "stores nil due date" do
            assert_nil @ticket.due_date
          end
        end

        describe "ticket is a task" do
          before do
            @ticket.ticket_type_id = TicketType.TASK
          end

          test_cases = [
            # Time near middle of day (UTC)
            {
              due_date: Time.new(2014, 01, 01, 15, 14, 13, 0),
              account_timezone: 'Copenhagen', # UTC + 1
              user_timezone: 'Copenhagen',
              expected_time: Time.new(2014, 1, 1, 11)
            },
            {
              due_date: Time.new(2014, 01, 01, 15, 14, 13, 0),
              account_timezone: 'Abu Dhabi', # UTC + 4
              user_timezone: 'Copenhagen', # UTC + 1
              expected_time: Time.new(2014, 1, 1, 8)
            },
            {
              due_date: Time.new(2014, 01, 01, 15, 14, 13, 0),
              account_timezone: 'Copenhagen', # UTC + 1
              user_timezone: 'Abu Dhabi', # UTC + 4
              expected_time: Time.new(2014, 1, 1, 11)
            },
            {
              due_date: Time.new(2014, 01, 01, 15, 14, 13, 0),
              account_timezone: 'Fiji', # UTC + 12
              user_timezone: 'Copenhagen', # UTC + 1
              expected_time: Time.new(2013, 12, 31, 23)
            },
            {
              due_date: Time.new(2014, 01, 01, 15, 14, 13, 0),
              account_timezone: 'Copenhagen', # UTC + 1
              user_timezone: 'Fiji', # UTC + 12
              expected_time: Time.new(2014, 1, 1, 11)
            },
            {
              due_date: Time.new(2014, 01, 01, 15, 14, 13, 0),
              account_timezone: 'Midway Island', # UTC - 11
              user_timezone: 'Wellington', # UTC + 13
              expected_time: Time.new(2014, 1, 1, 23)
            },
            {
              due_date: Time.new(2014, 01, 01, 15, 14, 13, 0),
              account_timezone: 'Wellington', # UTC + 13
              user_timezone: 'Midway Island', # UTC - 11
              expected_time: Time.new(2014, 1, 1, 23)
            },

            # Time at start of day (UTC)
            {
              due_date: Time.new(2014, 01, 01, 0, 0, 0, 0),
              account_timezone: 'Copenhagen', # UTC + 1
              user_timezone: 'Copenhagen',
              expected_time: Time.new(2014, 1, 1, 11)
            },
            {
              due_date: Time.new(2014, 01, 01, 0, 0, 0, 0),
              account_timezone: 'Abu Dhabi', # UTC + 4
              user_timezone: 'Copenhagen', # UTC + 1
              expected_time: Time.new(2014, 1, 1, 8)
            },
            {
              due_date: Time.new(2014, 01, 01, 0, 0, 0, 0),
              account_timezone: 'Copenhagen', # UTC + 1
              user_timezone: 'Abu Dhabi', # UTC + 4
              expected_time: Time.new(2014, 1, 1, 11)
            },
            {
              due_date: Time.new(2014, 01, 01, 0, 0, 0, 0),
              account_timezone: 'Kamchatka', # UTC + 12
              user_timezone: 'Copenhagen', # UTC + 1
              expected_time: Time.new(2014, 1, 1, 0)
            },
            {
              due_date: Time.new(2014, 01, 01, 0, 0, 0, 0),
              account_timezone: 'Copenhagen', # UTC + 1
              user_timezone: 'Kamchatka', # UTC + 12
              expected_time: Time.new(2014, 1, 1, 11)
            },
            {
              due_date: Time.new(2014, 01, 01, 0, 0, 0, 0),
              account_timezone: 'Midway Island', # UTC - 11
              user_timezone: 'Wellington', # UTC + 13
              expected_time: Time.new(2013, 12, 31, 23)
            },
            {
              due_date: Time.new(2014, 01, 01, 0, 0, 0, 0),
              account_timezone: 'Wellington', # UTC + 13
              user_timezone: 'Midway Island', # UTC - 11
              expected_time: Time.new(2013, 12, 31, 23)
            },

            # Time near end of day (UTC)
            {
              due_date: Time.new(2014, 01, 01, 23, 45, 0, 0),
              account_timezone: 'Copenhagen', # UTC + 1
              user_timezone: 'Copenhagen',
              expected_time: Time.new(2014, 1, 2, 11)
            },
            {
              due_date: Time.new(2014, 01, 01, 23, 45, 0, 0),
              account_timezone: 'Abu Dhabi', # UTC + 4
              user_timezone: 'Copenhagen', # UTC + 1
              expected_time: Time.new(2014, 1, 2, 8)
            },
            {
              due_date: Time.new(2014, 01, 01, 23, 45, 0, 0),
              account_timezone: 'Copenhagen', # UTC + 1
              user_timezone: 'Abu Dhabi', # UTC + 4
              expected_time: Time.new(2014, 1, 2, 11)
            },
            {
              due_date: Time.new(2014, 01, 01, 23, 45, 0, 0),
              account_timezone: 'Kamchatka', # UTC + 12
              user_timezone: 'Copenhagen', # UTC + 1
              expected_time: Time.new(2014, 1, 2, 0)
            },
            {
              due_date: Time.new(2014, 01, 01, 23, 45, 0, 0),
              account_timezone: 'Copenhagen', # UTC + 1
              user_timezone: 'Kamchatka', # UTC + 12
              expected_time: Time.new(2014, 1, 2, 11)
            },
            {
              due_date: Time.new(2014, 01, 01, 23, 45, 0, 0),
              account_timezone: 'Midway Island', # UTC - 11
              user_timezone: 'Wellington', # UTC + 13
              expected_time: Time.new(2014, 1, 1, 23)
            },
            {
              due_date: Time.new(2014, 01, 01, 23, 45, 0, 0),
              account_timezone: 'Wellington', # UTC + 13
              user_timezone: 'Midway Island', # UTC - 11
              expected_time: Time.new(2014, 1, 1, 23)
            },
          ]

          test_cases.each do |test_case|
            test_case = Hashie::Mash.new(test_case)
            describe "account time zone is #{test_case.account_timezone}, user time zone is #{test_case.user_timezone}, and due date is #{test_case.due_date}" do
              before do
                @ticket.due_date = test_case.due_date
                @ticket.account.time_zone = test_case.account_timezone
                @user.time_zone =           test_case.user_timezone
                @ticket.valid?
              end

              it "converts to noon in account timezone" do
                assert_equal test_case.expected_time, @ticket.due_date
              end
            end
          end

          describe "account is not available" do
            before do
              @ticket.account = nil
              @ticket.due_date = Time.new(2014, 01, 01, 23, 45, 0, 0)
              @user.time_zone =  'Midway Island'
              @user.account.time_zone = 'Wellington'
              @ticket.valid?
            end

            it "uses account from current user" do
              assert_equal Time.new(2014, 1, 1, 23), @ticket.due_date
            end
          end
        end
      end

      describe "due date not set" do
        before do
          @ticket.expects(:due_date=).never
        end

        it "does not attempt to fix up due date" do
          @ticket.valid?
        end
      end
    end

    describe "when the ticket type was a Task and then is changed to a different type and is trying to set the same due date it had before" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.account.stubs(:has_extended_ticket_types?).returns(true) # needed to avoid 'cleanup_attributes' before_save from changing the type
        @ticket.update_attributes("ticket_type_id" => TicketType.TASK, "due_date" => "2017-08-30T10:00:00Z")
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
      end

      it "nullifys the due_date on save" do
        @ticket.ticket_type_id = TicketType.QUESTION
        @ticket.due_date = "2017-08-30T10:00:00Z"
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal TicketType.QUESTION, @ticket.reload.ticket_type_id
        assert_nil @ticket.due_date
      end
    end
  end

  describe "#enforce_proper_assignee_group_relationship" do
    before do
      @ticket = tickets(:with_groups_2)
    end

    describe "if the group changes and makes the ticket invalid" do
      before do
        @ticket.assignee = users(:with_groups_admin) # only in group 2
        @ticket.group = groups(:with_groups_group1)
      end

      it "unsets the assignee" do
        @ticket.send(:enforce_proper_assignee_group_relationship)
        assert_nil @ticket.assignee
      end
    end

    describe "when the group has not changed (or assignee has changed), but the ticket is invalid" do
      before do
        @ticket.assignee = users(:with_groups_agent1) # only in group 2
      end

      it "sets the group to the assignee's default group" do
        @ticket.send(:enforce_proper_assignee_group_relationship)
        assert_equal groups(:with_groups_group1), @ticket.group.reload
      end
    end

    describe 'for a closed ticket with requester update (during user merge)' do
      before do
        @ticket.assignee_id = users(:with_groups_agent1).id
        @ticket.status_id = StatusType.CLOSED
        @requester_to_update_to = users(:minimum_search_user)
        assert_not_equal @requester_to_update_to, @ticket.requester
        @ticket.requester_id = @requester_to_update_to.id
      end

      it 'does not update assignee even if assignee is no longer is an agent' do
        @ticket.assignee_id = users(:minimum_end_user).id
        @ticket.send(:enforce_proper_assignee_group_relationship)
        assert_equal users(:minimum_end_user).id, @ticket.assignee_id
      end
    end

    # Tricky corner case, see ZD#2138142 for details
    describe "for a group change where only one agent is in the new group" do
      let(:group_1) { @ticket.account.groups.first }

      before do
        group_1.users = [group_1.users.last]
        @ticket.assignee_id = users(:with_groups_agent1).id
        @ticket.group_id = group_1.id
        @ticket.enforce_integrity_upon_attribute_change
      end

      it "assigns the ticket to the agent in the new group" do
        assert_equal group_1.users.first.id, @ticket.assignee_id
      end
    end
  end

  describe "#assign_tickets_upon_solve" do
    before do
      @admin_setting_id = 10057
      @ticket = tickets(:minimum_2)
      @ticket.status_id = StatusType.SOLVED
      assert_nil @ticket.assignee
    end

    describe "solving an unassigned ticket" do
      it "throws an error" do
        @ticket.will_be_saved_by(users(:minimum_admin))
        refute @ticket.save
      end
    end

    describe "solving an unassigned ticket with auto-assign-solved-tickets enabled" do
      before do
        @ticket.account.settings.assign_tickets_upon_solve = true
      end

      it "creates an audit event should one not exist when auto-assigning" do
        @ticket.stubs(:setting_id).returns(@admin_setting_id)
        @ticket.set_assignee_upon_solve
        assert @ticket.assignee
      end

      it "does not create an audit event if the ticket does not auto-assign" do
        @ticket.stubs(:setting_id).returns(@admin_setting_id)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.assignee = users(:minimum_admin)
        @ticket.status_id = StatusType.SOLVED
        @ticket.save!
        refute @ticket.audits.last.events.any? { |event| event.via_id == ViaType.ADMIN_SETTING && event.via_reference_id == @admin_setting_id }, "event was incorrectly created"
      end

      it "should auto-assign to current user" do
        @ticket.stubs(:setting_id).returns(@admin_setting_id)

        @ticket.will_be_saved_by(users(:minimum_admin))
        assert @ticket.save!
        assert_equal @ticket.assignee, users(:minimum_admin)
      end

      it "should not auto-assign to current system user" do
        @user = User.system
        @ticket.will_be_saved_by(@user)
        @ticket.set_assignee_upon_solve
        assert_nil @ticket.assignee
      end

      it "does not auto-assign when current user is light agent" do
        create_light_agent
        @ticket.will_be_saved_by(@light_agent)
        @ticket.set_assignee_upon_solve
        assert_nil @ticket.assignee
      end

      it "does not auto-assign when current user is end user" do
        @ticket.will_be_saved_by(users(:minimum_end_user))
        @ticket.set_assignee_upon_solve
        assert_nil @ticket.assignee
      end

      describe "and the ticket is solved by a trigger" do
        before do
          @ticket.stubs(:setting_id).returns(@admin_setting_id)

          definition = Definition.new
          definition.conditions_all.push(DefinitionItem.new('status_id', 'less_than', StatusType.SOLVED))
          definition.conditions_any.push(DefinitionItem.new('current_tags', 'includes', 'solve_now'))
          definition.actions.push(DefinitionItem.new('status_id', 'is', StatusType.SOLVED))
          accounts(:minimum).triggers.create!(title: 'solve ticket', definition: definition, position: 1, is_active: true)
          @ticket.status_id = StatusType.OPEN
          @ticket.additional_tags = 'solve_now'
          @user = users(:minimum_admin)
        end

        it "records the via type for assignment as Admin Setting" do
          @ticket.will_be_saved_by(@user, via_id: ViaType.WEB_SERVICE)
          @ticket.save!
          assert_equal @user, @ticket.assignee
          assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.ADMIN_SETTING && event.via_reference_id == @admin_setting_id }
        end

        describe "and the ticket group was changed to a group the user was not a member of" do
          before do
            @group1 = @ticket.account.groups.create!(name: 'Zgroup1', is_active: true)
            @ticket.group = @group1
            @user.set_default_group(groups(:minimum_group).id)
            @ticket.will_be_saved_by(@user, via_id: ViaType.WEB_SERVICE)
            @ticket.save!
          end

          it "changes the ticket group to the user's default group" do
            assert_equal @user, @ticket.assignee
            assert_equal @user.default_group_id, @ticket.group_id
          end

          it "shows both group changes in the audits" do
            assert_equal @user, @ticket.assignee
            assert_equal @user.default_group_id, @ticket.group_id
            assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.WEB_SERVICE && event.value_reference == 'group_id' }
            assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.ADMIN_SETTING && event.via_reference_id == @admin_setting_id && event.value_reference == 'group_id' }
          end
        end
      end
    end

    describe "solving an already-solved ticket with auto-assign-solved-tickets enabled" do
      before do
        @ticket.stubs(:setting_id).returns(@admin_setting_id)

        # admin solves ticket
        @ticket.account.settings.assign_tickets_upon_solve = true
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.save!
      end

      it "should auto-assign an unassigned, already-solved ticket to subsequent solver" do
        # agent completely unassigns ticket and solves again
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.assignee = nil;
        @ticket.status_id = StatusType.SOLVED
        @ticket.save!
        # ticket should now be assigned to the agent
        assert_equal @ticket.assignee, users(:minimum_agent), "didn't auto-assign ticket to subsequent solver"
      end

      it "should not re-assign an assigned, already-solved ticket to subsequent solver" do
        # agent opens ticket and hits solve
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.status_id = StatusType.SOLVED
        @ticket.save!
        # ticket should still be assigned to the admin
        assert_equal @ticket.assignee, users(:minimum_admin), "re-assigned ticket to subsequent solver"
      end
    end
  end

  describe "#assign_tickets_upon_solve_group_changes" do
    before do
      @admin_setting_id = 10057
      @ticket = Ticket.new(account: accounts(:with_groups))
      @ticket.description = "test"
      @ticket.stubs(:setting_id).returns(@admin_setting_id)
      @user = users(:with_groups_agent2) # belongs to with_groups_group1 and with_groups_group2
      @ticket.will_be_saved_by(@user)
      @ticket.save!
      @ticket.account.settings.assign_tickets_upon_solve = true
    end

    describe "when the ticket is solved by a user" do
      describe "and the group is not set" do
        it "sets the group to the assignee's default group" do
          @ticket.status_id = StatusType.SOLVED
          @ticket.will_be_saved_by(@user, via_id: ViaType.WEB_SERVICE)
          @ticket.save!
          assert_equal @user, @ticket.assignee, "didn't auto-assign ticket to the solver"
          assert_equal @user.default_group_id, @ticket.group_id, "didn't auto-assign the correct group"
          # events should be attributed to the web service instead of the admin setting
          assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.WEB_SERVICE && event.value_reference == 'assignee_id' }
          assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.WEB_SERVICE && event.value_reference == 'group_id' }
          refute @ticket.audits.last.events.any? { |event| event.via_id == ViaType.ADMIN_SETTING && ['assignee_id', 'group_id'].include?(event.value_reference) }
        end
      end

      describe "and the solving agent is not a member of the ticket group" do
        it "sets the group to the assignee's default group" do
          @group1 = @ticket.account.groups.create!(name: 'Zgroup1', is_active: true)
          @ticket.group = @group1
          @ticket.will_be_saved_by(@user)
          @ticket.save!
          @ticket.status_id = StatusType.SOLVED
          @ticket.will_be_saved_by(@user, via_id: ViaType.WEB_SERVICE)
          @ticket.save!
          assert_equal @ticket.assignee, @user, "didn't auto-assign ticket to the solver"
          assert_equal @ticket.group_id, @user.default_group_id, "didn't auto-assign the correct group"
          # events should be attributed to the web service instead of the admin setting
          assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.WEB_SERVICE && event.value_reference == 'assignee_id' }
          assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.WEB_SERVICE && event.value_reference == 'group_id' }
          refute @ticket.audits.last.events.any? { |event| event.via_id == ViaType.ADMIN_SETTING && ['assignee_id', 'group_id'].include?(event.value_reference) }
        end
      end
    end

    describe "when the ticket is solved by a trigger" do
      before do
        definition = Definition.new
        definition.conditions_all.push(DefinitionItem.new('status_id', 'less_than', StatusType.SOLVED))
        definition.conditions_any.push(DefinitionItem.new('current_tags', 'includes', 'solve_now'))
        definition.actions.push(DefinitionItem.new('status_id', 'is', StatusType.SOLVED))
        accounts(:with_groups).triggers.create!(title: 'solve ticket', definition: definition, position: 1, is_active: true)
        Timecop.travel(Time.now + 1.seconds)
      end

      describe "and the ticket group is not set" do
        it "sets the group to the assignee's default group" do
          @ticket.additional_tags = 'solve_now'
          @ticket.will_be_saved_by(@user, via_id: ViaType.WEB_SERVICE)
          @ticket.save!
          assert_equal @ticket.assignee, @user, "didn't auto-assign ticket to the solver"
          assert_equal @ticket.group_id, @user.default_group_id, "didn't auto-assign the correct group"
          # events should be attributed to the admin setting and not the rule
          assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.ADMIN_SETTING && event.via_reference_id == @admin_setting_id && event.value_reference == 'assignee_id' }
          assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.ADMIN_SETTING && event.via_reference_id == @admin_setting_id && event.value_reference == 'group_id' }
          refute @ticket.audits.last.events.any? { |event| event.via_id == ViaType.RULE && ['assignee_id', 'group_id'].include?(event.value_reference) }
        end
      end

      describe "and the solving agent is not a member of the ticket group" do
        it "sets the group to the assignee's default group" do
          @group1 = @ticket.account.groups.create!(name: 'Zgroup1', is_active: true)
          @ticket.group = @group1
          @ticket.will_be_saved_by(@user)
          @ticket.save!
          @ticket.additional_tags = 'solve_now'
          @ticket.will_be_saved_by(@user, via_id: ViaType.WEB_SERVICE)
          @ticket.save!
          assert_equal @ticket.assignee, @user, "didn't auto-assign ticket to the solver"
          assert_equal @ticket.group_id, @user.default_group_id, "didn't auto-assign the correct group"
          # events should be attributed to the admin setting and not the rule
          assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.ADMIN_SETTING && event.via_reference_id == @admin_setting_id && event.value_reference == 'assignee_id' }
          assert @ticket.audits.last.events.any? { |event| event.via_id == ViaType.ADMIN_SETTING && event.via_reference_id == @admin_setting_id && event.value_reference == 'group_id' }
          refute @ticket.audits.last.events.any? { |event| event.via_id == ViaType.RULE && ['assignee_id', 'group_id'].include?(event.value_reference) }
        end
      end

      describe "and a second trigger changes the group" do
        let(:group2) { groups(:with_groups_group1) }
        before do
          conditions_all = [
            DefinitionItem.new('status_id', 'less_than', StatusType.CLOSED),
            DefinitionItem.new('current_tags', 'includes', 'group_change')
          ]
          actions = [
            DefinitionItem.new('group_id', 'is', [group2.id]),
            DefinitionItem.new('assignee_id', 'is', group2.users.first.id)
          ]
          definition2 = Definition.new(conditions_all, [], actions)
          @trigger = accounts(:with_groups).triggers.create!(title: 'change group', definition: definition2, position: 2, is_active: true)
        end

        it "adds an event for the group change attributed to the second trigger" do
          @ticket.additional_tags = 'solve_now'
          @ticket.additional_tags = 'group_change'
          @ticket.will_be_saved_by(@user, via_id: ViaType.WEB_SERVICE)
          @ticket.save!
          # there's two group change events
          # one of them is attributed to auto_assign_on_solve
          # the other is attributed to the trigger (definition2)
          assert_equal @ticket.group_id, group2.id
          assert_equal @ticket.assignee, group2.users.first
          group_change_events = @ticket.audits.last.events.select { |event| event.value_reference == 'group_id' }
          assignee_change_events = @ticket.audits.last.events.select { |event| event.value_reference == 'assignee_id' }
          assert_equal 2, group_change_events.size
          assert_equal 2, assignee_change_events.size
          assert group_change_events.first.via_id == ViaType.ADMIN_SETTING && group_change_events.first.via_reference_id == @admin_setting_id
          assert group_change_events.last.via_id == ViaType.RULE_REVISION && group_change_events.last.via_reference_id == @trigger.revisions.last.id
          assert assignee_change_events.first.via_id == ViaType.ADMIN_SETTING && assignee_change_events.first.via_reference_id == @admin_setting_id
          assert assignee_change_events.last.via_id == ViaType.RULE_REVISION && assignee_change_events.last.via_reference_id == @trigger.revisions.last.id
        end

        describe "and a third trigger sets the group and assignee back to nil" do
          before do
            conditions_all = [
              DefinitionItem.new('status_id', 'less_than', StatusType.CLOSED),
              DefinitionItem.new('current_tags', 'includes', 'solve_now')
            ]
            actions = [
              DefinitionItem.new('group_id', 'is', nil),
              DefinitionItem.new('assignee_id', 'is', nil)
            ]
            definition3 = Definition.new(conditions_all, [], actions)
            @trigger2 = accounts(:with_groups).triggers.create!(title: 'change group 2', definition: definition3, position: 3, is_active: true)
            Timecop.travel(Time.now + 1.seconds)
          end

          it "adds an event for the group change attributed to the third trigger" do
            @ticket.additional_tags = 'solve_now'
            @ticket.additional_tags = 'group_change'
            @ticket.will_be_saved_by(@user, via_id: ViaType.WEB_SERVICE)
            @ticket.save!
            assert_nil @ticket.group_id
            assert_nil @ticket.assignee
            group_change_events = @ticket.audits.last.events.select { |event| event.value_reference == 'group_id' }
            assignee_change_events = @ticket.audits.last.events.select { |event| event.value_reference == 'assignee_id' }
            assert_equal 3, group_change_events.size
            assert_equal 3, assignee_change_events.size
            assert group_change_events.first.via_id == ViaType.ADMIN_SETTING && group_change_events.first.via_reference_id == @admin_setting_id
            assert group_change_events.last.via_id == ViaType.RULE_REVISION && group_change_events.last.via_reference_id == @trigger2.revisions.last.id
            assert assignee_change_events.first.via_id == ViaType.ADMIN_SETTING && assignee_change_events.first.via_reference_id == @admin_setting_id
            assert assignee_change_events.last.via_id == ViaType.RULE_REVISION && assignee_change_events.last.via_reference_id == @trigger2.revisions.last.id
          end
        end
      end
    end
  end

  describe "#infer_group_from_assignee" do
    before do
      @ticket = Ticket.new
      @ticket.assignee = users(:with_groups_agent1)
    end

    it "sets the group to the first group of the assignee" do
      @ticket.send(:infer_group_from_assignee)

      assert_equal @ticket.assignee.groups.first, @ticket.group
    end

    it "handles invalid/deleted organization_id's" do
      @ticket.organization_id = 0
      @ticket.send(:infer_group_from_assignee)
      assert_equal @ticket.assignee.groups.first, @ticket.group
    end
  end

  describe "#fix_organization" do
    before do
      @ticket = tickets(:minimum_2)
      @requester_to_update_to = users(:minimum_search_user)
      @requester_to_update_to.update_attribute(:organization, organizations(:minimum_organization1))
      assert_not_equal @requester_to_update_to, @ticket.requester
      @ticket.requester_id = @requester_to_update_to.id
      @ticket.send(:fix_organization)
    end

    describe "when updating with a nil organization" do
      before do
        @ticket.organization = nil
        @ticket.stubs(:requester_has_changed?).returns(false)
      end

      describe_with_arturo_enabled :unset_ticket_org_when_removing_default_user_org do
        it "allows a ticket to have nil organization" do
          @ticket.send(:fix_organization)

          assert_nil @ticket.organization
        end
      end

      describe_with_arturo_disabled :unset_ticket_org_when_removing_default_user_org do
        it "does not allow a ticket to have nil organization" do
          @ticket.send(:fix_organization)

          assert_not_nil @ticket.organization
        end
      end
    end

    it "updates the organization_id to that of the newly-set requester" do
      assert_equal @requester_to_update_to.organization, @ticket.organization
    end

    it "updates the organization id for new requesters on tickets" do
      @ticket.requester = accounts(:minimum).users.new
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.requester_id = nil
      @ticket.reset_delta_changes!

      # Prepare a new requester with a default organization
      organization = Organization.new(name: "New requester's organization")
      User.any_instance.stubs(:organization).returns(organization)
      @ticket.requester_email = 'new@example.com'
      @ticket.requester_name  = 'New Requester'
      # Runs Ticket#set_requester
      @ticket.valid?
      @ticket.send(:fix_organization)

      assert_equal organization, @ticket.organization
    end

    it "keeps the organization if we're creating a ticket that alredy has a valid one" do
      organization = organizations(:minimum_organization2)
      @requester_to_update_to.organizations << organization

      @ticket = Ticket.new
      @ticket.requester_id = @requester_to_update_to.id
      @ticket.organization_id = organization.id

      @ticket.send(:fix_organization)

      assert_equal organization, @ticket.organization
      assert_not_equal @requester_to_update_to.organization, @ticket.organization
    end

    # In this case, requester is assigned using build_requester instead of set_requester
    it "updates the organization id for new requesters on new tickets" do
      account = accounts(:minimum)
      ticket  = account.tickets.new
      # Prepare a new requester with a default organization
      organization = account.organizations.create!(name: "New requester's organization")
      User.any_instance.stubs(:organization).returns(organization)

      requester = ticket.build_requester(name: 'New Requester', email: 'new@example.com', account: account)
      ticket.will_be_saved_by(requester)
      ticket.valid?
      ticket.send(:fix_organization)

      assert_equal organization, ticket.organization
    end

    describe "for an account with many groups and one agent only" do
      before do
        @account = accounts(:minimum)
        @group1 = @account.groups.create!(name: 'Zgroup1', is_active: true)
        @group2 = @account.groups.create!(name: 'Zgroup2', is_active: true)
        @organization = @account.organizations.create!(domain_names: 'zebra.com', name: 'Zebra', group: @group1)
        @organization.save!

        ActiveRecord::Base.connection.execute("update users set roles = 0 where id <> #{users(:minimum_admin).id}")
        @group1.memberships.create!(user: users(:minimum_admin))
        assert_not_equal 1, @account.groups.count(:all)
        assert_equal 1, @account.agents.count(:all)

        @user = @account.users.new(name: 'Zebra Man', email: 'user@zebra.com')
        @user.save!
      end

      it "assigns to the mapped group" do
        ticket = @account.tickets.new(recipient: 'support@zebra.com', requester: @user, description: 'I should have my organization group')
        ticket.will_be_saved_by(@user)

        assert_sql_queries(2, /SELECT  `users`.* FROM `users` WHERE `users`.`account_id` = \d+ AND `users`.`is_active` = 1 AND `users`.`roles` IN \(4, 2\) ORDER BY `users`.`id` ASC LIMIT 1/) do
          ticket.save!
        end

        assert_equal ticket.group, @organization.group
      end
    end

    describe "for an account with many groups and one of the groups has one agent only" do
      before do
        @account = accounts(:with_groups)
        @organization = @account.organizations.first
        @user = users(:with_groups_agent_organization_restricted)
        ActiveRecord::Base.connection.execute("delete from memberships where user_id = #{users(:with_groups_agent2).id}")

        assert_not_equal 1, @account.groups.count(:all)
        assert_equal 1, @account.groups.first.users.count(:all)

        @ticket = @account.tickets.new(recipient: 'support@zebra.com', requester: @user, description: 'I should have my organization group')
        @ticket.will_be_saved_by(@user)
        @ticket.save!
      end

      it "assigns to the mapped group" do
        assert_equal @ticket.group, @organization.group
      end

      it "sets the assignee" do
        assert_not_nil @ticket.assignee
        assert_equal @ticket.assignee, @ticket.group.users.first
      end
    end
  end

  describe "#set_organization_group" do
    describe "for an account with a default group" do
      before do
        @account = accounts(:with_groups)
        assert @account.default_group
      end

      describe "on a ticket whose requester's organization's group is not the default group" do
        before do
          @requester = users(:with_groups_agent_organization_restricted)
          @requester.organization.group = groups(:with_groups_group2)
          @requester.organization.save!
          assert_not_equal @account.default_group, @requester.organization.group
          @ticket = @account.tickets.new(requester: @requester, description: 'Boop')
          @ticket.will_be_saved_by(@requester)
        end

        it "sets the group to the requester's organization's group" do
          @ticket.save!
          assert_equal @requester.organization.group, @ticket.group
        end

        describe "and the organization_id is set on the ticket" do
          before { @ticket.organization_id = @requester.organization.id }

          it "sets the group to the requester's organization's group" do
            @ticket.save!
            assert_equal @requester.organization.group, @ticket.group
          end
        end
      end
    end
  end

  describe "#open_working_ticket_upon_end_user_update and ticket status is 'solved'" do
    [true, false].each do |arturo_enabled|
      describe "agent_as_end_user Arturo is #{arturo_enabled ? 'en' : 'dis'}abled" do
        let(:ticket) { tickets(:minimum_1) }
        before do
          Arturo.set_feature!(:agent_as_end_user, arturo_enabled)
          ticket.status_id = StatusType.SOLVED
          ticket.will_be_saved_by(users(:minimum_agent))
          ticket.save!
          assert ticket.status?(:solved)
        end

        it "changes the ticket status to 'open' when receiving an end user comment" do
          ticket.add_comment(body: 'Do not close yet', is_public: true)
          ticket.will_be_saved_by(ticket.requester)
          ticket.save!

          assert_equal [
            "Change : Status changed from Solved to Open : web form by Author Minimum",
            "Comment : Do not close yet : web form by Author Minimum",
            "Notification : Agent Minimum : rule Notify assignee of reopened ticket by Author Minimum"
          ], ticket.audits.last.inspect_events.sort
          assert ticket.status?(:open)
          assert_nil ticket.solved_at
        end

        it "does not change the ticket status to 'open' if the commenter is an agent" do
          ticket.add_comment(body: 'Comment by an agent', is_public: false)
          ticket.will_be_saved_by(ticket.assignee)
          ticket.save!

          assert_equal(
            ["Comment : Comment by an agent : web form by Agent Minimum"],
            ticket.audits.last.inspect_events.sort
          )
          assert ticket.status?(:solved)
        end

        it "does not change the ticket status to 'open' if there is no comment" do
          ticket.will_be_saved_by(ticket.requester)
          ticket.save!
          assert ticket.status?(:solved)
        end

        it "does not change the ticket status when receiving an end user comment if force_status_change is true" do
          original_solved_at = ticket.solved_at
          # See:
          # app/views/requests/portal/show.mobile_v2.erb#L54-L63
          # app/views/requests/portal/show.html.erb#L125
          # app/views/requests/portal/_form.mobile_v2.erb#L11
          ticket.force_status_change = true
          ticket.add_comment(body: 'Do not close yet', is_public: true)
          ticket.will_be_saved_by(ticket.requester)
          ticket.save!

          assert_equal [
            "Comment : Do not close yet : web form by Author Minimum",
            "Notification : Agent Minimum : rule Notify assignee of comment update by Author Minimum"
          ], ticket.audits.last.inspect_events.sort
          assert ticket.status?(:solved)
          assert_equal original_solved_at, ticket.solved_at
        end

        describe "updated by agent-as-end-user agent" do
          [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
            describe "via_id is #{via_id}" do
              before do
                @agent = users(:minimum_agent)
                ticket.update_column(:via_id, via_id)
                ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
                ticket.update_column(:submitter_id, @agent.id)
                ticket.add_comment(body: 'some stuff', is_public: true)
                ticket.will_be_saved_by(@agent)
                ticket.save!
              end

              if arturo_enabled
                it "reopens ticket" do
                  assert ticket.status?(:open)
                  assert_nil ticket.solved_at
                end
              else
                it "does not reopen ticket" do
                  assert ticket.status?(:solved)
                  refute ticket.solved_at.nil?
                end
              end
            end
          end
        end
      end
    end
  end

  describe "#check_safe_update_timestamp" do
    let(:ticket) { tickets(:minimum_1) }

    it "adds an error if safe_update_timestamp is outdated" do
      ticket.safe_update_timestamp = ticket.updated_at - 3
      ticket.will_be_saved_by(ticket.requester)

      refute_valid ticket
      assert ticket.errors[:safe_update_timestamp].present?
    end

    it "does not have safe_update_timestamp error if timestamp is correct" do
      ticket.safe_update_timestamp = ticket.updated_at
      ticket.will_be_saved_by(ticket.requester)

      assert_valid ticket
    end

    it "does not raise an error for archived tickets" do
      archive_and_delete(ticket)
      ticket.safe_update_timestamp = ticket.updated_at
      ticket.will_be_saved_by(ticket.requester)

      refute_valid ticket
    end

    it "will lock the current ticket" do
      Account.any_instance.stubs(:tickets).returns(stub(where: ticket))
      ticket.expects(:lock).with(true).returns(stub(pluck: [ticket.updated_at]))

      ticket.safe_update_timestamp = ticket.updated_at
      ticket.will_be_saved_by(ticket.requester)

      assert_valid ticket
    end

    it "saves correctly if safe_update_timestamp is newer than updated_at" do
      ticket.safe_update_timestamp = ticket.updated_at + 3
      ticket.will_be_saved_by(ticket.requester)

      assert_valid ticket
    end

    describe "for whatsapp tickets" do
      before do
        ticket.current_tags = 'whatsapp_support'
      end

      describe "when arturo `skip_safe_update_check` is enabled" do
        before do
          Arturo.enable_feature!(:skip_safe_update_check)
        end

        it "skips the validation for whatsapp tickets" do
          ticket.safe_update_timestamp = ticket.updated_at - 3
          ticket.will_be_saved_by(ticket.requester)

          assert_valid ticket
          refute ticket.errors[:safe_update_timestamp].present?
        end

        it "saves correctly if safe_update_timestamp is newer than updated_at" do
          ticket.safe_update_timestamp = ticket.updated_at + 3
          ticket.will_be_saved_by(ticket.requester)

          assert_valid ticket
        end
      end

      describe "when arturo `skip_safe_update_check` is disabled" do
        before do
          Arturo.disable_feature!(:skip_safe_update_check)
        end

        it "adds an error if safe_update_timestamp is outdated" do
          ticket.safe_update_timestamp = ticket.updated_at - 3
          ticket.will_be_saved_by(ticket.requester)

          refute_valid ticket
          assert ticket.errors[:safe_update_timestamp].present?
        end

        it "saves correctly if safe_update_timestamp is newer than updated_at" do
          ticket.safe_update_timestamp = ticket.updated_at + 3
          ticket.will_be_saved_by(ticket.requester)

          assert_valid ticket
        end
      end
    end
  end

  describe "#group_id_is_valid" do
    let(:ticket)  { tickets(:minimum_1) }
    let(:account) { ticket.account }
    let(:group_id) { 12345 }

    before do
      # sets the current user and prevent the no current user/account error
      ticket.will_be_saved_by(ticket.requester)
    end

    it "does not have an error if group is nil" do
      ticket.stubs(:group_id).returns(nil)
      assert_valid ticket
    end

    it "does not have an error if group_id_changed? == false" do
      ticket.stubs(:group_id_changed?).returns(false)
      assert_valid ticket
    end

    describe "when group has changed" do
      before do
        # required to pass the proc validations
        ticket.stubs(:group_id).returns(group_id)
        ticket.stubs(:group_id_changed?).returns(true)
      end

      it "does not have an error if group exist" do
        account.groups.stubs(:where).returns(account.groups)

        assert_valid ticket
      end

      it "adds error if there are no groups" do
        account.groups.stubs(:where).returns([])

        refute_valid ticket
        assert ticket.errors[:group_id].present?
      end

      describe "and if there is only 1 group" do
        before do
          Group.destroy_all
          group = account.groups.create!(name: 'test')
          @id = group.id
        end

        let (:id) { @id }

        before(:each) do
          assert Group.count == 1
        end

        it "adds error if there are no matching groups" do
          ticket.stubs(:group_id).returns(group_id) # the previous group_id should no longer match
          refute_valid ticket
          assert ticket.errors[:group_id].present?
        end

        it "does not have an error if the group_id exist" do
          ticket.stubs(:group_id).returns(id)
          assert_valid ticket
        end
      end
    end
  end
end
