require_relative "../../support/test_helper"

SingleCov.covered!

describe Ticket::TicketFieldEntries do
  fixtures :users, :tickets, :accounts, :ticket_fields, :account_property_sets, :ticket_field_entries

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_admin) }
  let(:ticket) { account.tickets.new(requester: user, description: 'hello') }
  let(:tagger) { ticket_fields(:field_tagger_custom) }
  let(:multiselect) do
    FieldMultiselect.create!(
      title: 'Multiselect',
      account: account,
      is_active: true,
      custom_field_options: [
        CustomFieldOption.new(name: "Option1", value: "option1"),
        CustomFieldOption.new(name: "Option2", value: "option2")
      ]
    )
  end
  let(:text_field) { ticket_fields(:field_text_custom) }
  let(:checkbox) { ticket_fields(:field_checkbox_custom) }

  describe "Setting field values through tags" do
    it "generates correct entry and audit for checkboxes" do
      ticket.tags = checkbox.tag

      ticket.will_be_saved_by(user)
      assert ticket.save!
      ticket.reload

      assert_equal '1', ticket.ticket_field_entries.detect { |f| f.ticket_field_id == checkbox.id }.value
      assert_equal '1', ticket.audits.first.events.detect { |e| e.value_reference.to_i == checkbox.id }.value
    end

    it "generates correct entry and audit for dropdowns" do
      tag = tagger.custom_field_options.first.value
      ticket.tags = tag

      ticket.will_be_saved_by(user)
      assert ticket.save!
      ticket.reload

      assert_equal tag, ticket.ticket_field_entries.detect { |f| f.ticket_field_id == tagger.id }.value
      assert_equal tag, ticket.audits.first.events.detect { |e| e.value_reference.to_i == tagger.id }.value
    end

    it "generates correct entry and audit for multiselect" do
      tag1 = multiselect.custom_field_options.first.value
      tag2 = multiselect.custom_field_options.second.value
      tags = "#{tag1} #{tag2}"
      ticket.tags = tags

      ticket.will_be_saved_by(user)
      assert ticket.save!
      ticket.reload

      assert_equal tags, ticket.ticket_field_entries.detect { |f| f.ticket_field_id == multiselect.id }.value
      assert_equal tags, ticket.audits.first.events.detect { |e| e.value_reference.to_i == multiselect.id }.value
    end
  end

  describe "Setting empty values for fields" do
    it "doesn't generate ticket_field_entries with NULL values" do
      ticket.fields = {
        tagger.id.to_s => '',
        multiselect.id.to_s => '',
        text_field.id.to_s => ''
      }

      ticket.will_be_saved_by(user)
      assert ticket.save!
      ticket.reload
      entries = ticket.ticket_field_entries

      refute entries.detect { |f| f.ticket_field_id == tagger.id }
      refute entries.detect { |f| f.ticket_field_id == multiselect.id }
      refute entries.detect { |f| f.ticket_field_id == text_field.id }
    end

    it "generates entry with value '0' and the correct audit for checkboxes" do
      ticket.fields = {
        checkbox.id.to_s => ''
      }

      ticket.will_be_saved_by(user)
      assert ticket.save!
      ticket.reload

      assert_equal '0', ticket.ticket_field_entries.detect { |f| f.ticket_field_id == checkbox.id }.value
      assert_equal '0', ticket.audits.first.events.detect { |e| e.value_reference.to_i == checkbox.id }.value
    end

    describe "on tickets with existing entries for those fields" do
      before do
        ticket.fields = {
          tagger.id.to_s => 'booring',
          multiselect.id.to_s => 'option1',
          text_field.id.to_s => 'Some text',
          checkbox.id.to_s => '1'
        }

        ticket.will_be_saved_by(user)
        assert ticket.save!
        ticket.reload
        entries = ticket.ticket_field_entries

        assert_equal 'booring', entries.detect { |f| f.ticket_field_id == tagger.id }.value
        assert_equal 'option1', entries.detect { |f| f.ticket_field_id == multiselect.id }.value
        assert_equal 'Some text', entries.detect { |f| f.ticket_field_id == text_field.id }.value
        assert_equal '1', entries.detect { |f| f.ticket_field_id == checkbox.id }.value
      end

      it "correctly resets the values of the ticket_field_entries" do
        ticket.fields = {
          tagger.id.to_s => '',
          multiselect.id.to_s => '',
          text_field.id.to_s => '',
          checkbox.id.to_s => ''
        }

        ticket.will_be_saved_by(user)
        assert ticket.save!
        ticket.reload
        entries = ticket.ticket_field_entries

        assert_equal '', entries.detect { |f| f.ticket_field_id == tagger.id }.value
        assert_equal '', entries.detect { |f| f.ticket_field_id == multiselect.id }.value
        assert_equal '', entries.detect { |f| f.ticket_field_id == text_field.id }.value
        assert_equal '0', entries.detect { |f| f.ticket_field_id == checkbox.id }.value
      end
    end
  end

  describe "#ticket_field_entries_changed?" do
    it "marks ticket field entries as changed after setting fields" do
      refute ticket.ticket_field_entries_changed?
      ticket.fields = { tagger.id.to_s => 'booring' }
      assert ticket.ticket_field_entries_changed?
    end

    it "resets ticket_field_entries_changed after saving ticket" do
      refute ticket.ticket_field_entries_changed?
      ticket.fields = { tagger.id.to_s => 'booring' }
      assert ticket.ticket_field_entries_changed?

      ticket.will_be_saved_by(user)
      assert ticket.save!
      refute ticket.ticket_field_entries_changed?
    end

    it "marks ticket field entries as changed only when the value of the fields have changed" do
      value = 'some text'
      ticket.fields = { text_field.id.to_s => value }

      assert ticket.ticket_field_entries_changed?
      ticket.will_be_saved_by(user)
      assert ticket.save!
      ticket.reload

      ticket.fields = { text_field.id.to_s => value }
      refute ticket.ticket_field_entries_changed?
    end
  end
end
