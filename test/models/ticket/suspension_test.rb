require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Ticket::Suspension' do
  let(:account) { accounts(:minimum) }
  let(:end_user) { users(:minimum_end_user) }

  let(:ticket) do
    Ticket.new(account: account, subject: 'Ticket Subject', description: 'Ticket description', requester: end_user, via_id: ViaType.WEB_FORM, client: 'client', recipient: 'agent@zd-dev.com', original_recipient_address: 'agent@zd-dev.com', current_tags: 'hello', due_date: Time.new(2020, 1, 1, 12, 0, 0, 0), brand_id: 123, via_reference_id: ViaType.HELPCENTER)
  end

  describe '#suspend' do
    it 'creates new suspended ticket with correct properties' do
      suspended_ticket = ticket.suspend(SuspensionType.SIGNUP_REQUIRED)

      assert_equal SuspendedTicket, suspended_ticket.class
      assert_equal 'Ticket Subject', suspended_ticket.subject
      assert_equal 'Ticket description', suspended_ticket.content
      assert_equal SuspensionType.SIGNUP_REQUIRED, suspended_ticket.cause
      assert_equal end_user.name, suspended_ticket.from_name
      assert_equal end_user.email, suspended_ticket.from_mail
      assert_equal ViaType.WEB_FORM, suspended_ticket.via_id
      assert_equal end_user, suspended_ticket.author
      assert_equal 'client', suspended_ticket.client
      assert_equal 'agent@zd-dev.com', suspended_ticket.recipient
      assert_equal 'agent@zd-dev.com', suspended_ticket.original_recipient_address
      assert_equal 'hello', suspended_ticket.properties[:tags]
      assert_equal Time.new(2020, 1, 1, 12, 0, 0, 0), suspended_ticket.properties[:due_date]
      assert_equal 123, suspended_ticket.properties[:brand_id]
      assert_equal ViaType.HELPCENTER, suspended_ticket.properties[:via_reference_id]
    end
  end
end
