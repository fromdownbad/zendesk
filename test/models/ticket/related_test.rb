require_relative "../../support/test_helper"
require 'ticket/related'
require 'ostruct'

SingleCov.covered!

describe Ticket::Related do
  # TODO: if you're working with these tests, try making this less Mock and more real use-case.
  module Fake
    class Ticket
      attr_accessor :nice_id, :entry_id, :shared_tickets, :twitter,
        :followup_sources, :followups, :twitter_ticket_source, :jira_issues, :from_archive,
        :satisfaction_score, :satisfaction_comment, :via_twitter, :twitter_direct_message

      def initialize(followup_stub)
        # Has many associations will return an empty collection by default
        self.shared_tickets   = []

        self.followup_sources = followup_stub
        stubs(:followups_with_archived).returns([])
        self.jira_issues = []
      end

      def archived?
        !!from_archive
      end

      def via_twitter?
        !!via_twitter
      end

      def via?(type)
        type == :twitter_dm
      end
    end
  end

  let(:ticket) { Fake::Ticket.new(stub(all_with_archived: [])) }
  let(:related) { Ticket::Related.new(ticket) }
  let(:json) { related.as_json.with_indifferent_access }

  it "responds to to json" do
    related.to_json.wont_be_nil
  end

  it "includes the associated topic" do
    ticket.entry_id = 7
    json[:topic].must_equal 7
  end

  it "includes the shared tickets" do
    agreement = stub("agreement",
      name: "hello",
      in?: true,
      status: "accepted",
      id: 1234)
    ticket.shared_tickets = [stub(agreement: agreement)]

    shared = json[:shared].first

    shared[:name].must_equal "hello"
    shared[:inbound].must_equal true
    shared[:status].must_equal "accepted"
    shared[:id].must_equal 1234
  end

  it "doesn't blow up when agreements get deleted" do
    ticket.shared_tickets = [stub(agreement: nil)]
    assert json
  end

  it "includes the followup sources" do
    source_ticket = Fake::Ticket.new(stub(all_with_archived: []))
    source_ticket.nice_id = 4
    ticket.followup_sources = stub(all_with_archived: [source_ticket])

    json[:followupSources].must_equal [source_ticket.nice_id]
  end

  it "includes the followup ticket ids" do
    closed_ticket = Fake::Ticket.new(stub(all_with_archived: []))
    closed_ticket.nice_id = 9
    ticket.stubs(:followups_with_archived).returns([closed_ticket])

    json[:followupIds].must_equal [closed_ticket.nice_id]
  end

  it 'includes the tweet' do
    ticket.via_twitter = true
    ticket.twitter_ticket_source = stub(id: 12, screen_name: '@blue_angels', twitter_profile: { twitter_user_id: "wibbles"})
    ticket.stubs(:twitter_target).returns(stub(nice_id: 123456))

    json[:twitter].must_equal("handle" => {"id" => 12},
                              "profile" => {"twitter_user_id" => "wibbles"},
                              "direct" => true,
                              "parent" => 123456)
  end

  it 'includes the facebook parent information if available' do
    ticket.stubs(:via?).with(:facebook_post).returns(true)
    ticket.stubs(:facebook_target).returns(stub(nice_id: 123456))

    json[:facebook].must_equal("parent" => 123456)
  end

  it "includes the jira issue" do
    ticket.jira_issues = [stub(issue_id: 8910)]

    json[:jiraIssues].must_equal [8910]
  end
end
