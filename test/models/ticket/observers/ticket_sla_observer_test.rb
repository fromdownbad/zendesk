require_relative '../../../support/test_helper'
require_relative '../../../support/ticket_metric_helper'

SingleCov.covered!

describe TicketSlaObserver do
  include TicketMetricHelper

  fixtures :accounts, :tickets

  let(:account)  { accounts(:minimum) }
  let(:ticket)   { tickets(:minimum_1) }
  let(:apm_stub) { stub(:apm_stub) }

  before do
    Account.any_instance.stubs(has_service_level_agreements?: slas_enabled)
  end

  describe 'when the account has the SLA feature' do
    let(:slas_enabled) { true }

    before do
      apm_stub.
        expects(:set_tag).
        with(Datadog::Ext::Analytics::TAG_ENABLED, true)

      apm_stub.
        expects(:set_tag).
        with('zendesk.account_id', account.id)

      apm_stub.
        expects(:set_tag).
        with('zendesk.ticket.id', ticket.id)

      apm_stub.
        expects(:set_tag).
        with('zendesk.account_subdomain', account.subdomain)

      ticket.send(:assess_sla)
    end

    before_should 'be subject to SLA policy assignment' do
      ZendeskAPM.
        expects(:trace).
        with(
          'ticket_metric.sla_policy_assignment',
          service: Zendesk::TicketMetric::APM_SERVICE_NAME
        ).
        yields(apm_stub)

      Zendesk::Sla::PolicyAssignment.any_instance.expects(:assign).once
    end
  end

  describe 'when the account does not have the SLA feature' do
    let(:slas_enabled) { false }

    before { ticket.send(:assess_sla) }

    before_should 'not be subject to SLA policy assignment' do
      ZendeskAPM.expects(:trace).never
      Zendesk::Sla::PolicyAssignment.any_instance.expects(:assign).never
    end
  end
end
