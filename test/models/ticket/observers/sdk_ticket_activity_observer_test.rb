require_relative "../../../support/test_helper"

SingleCov.covered!

describe SdkTicketActivityObserver do
  fixtures :accounts, :tickets, :mobile_sdk_apps, :users, :mobile_apps

  before do
    PushNotifications::DeviceIdentifier.without_arsi.delete_all
    PushNotificationSdkSendJob.stubs(:work)
  end

  describe "Anonymous user" do
    before do
      @user_sdk_identity = user_identities(:mobile_sdk_identity)
      @requester_identifier = PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sdk_anonymous_device_identifier|
        sdk_anonymous_device_identifier.token = "foo"
        sdk_anonymous_device_identifier.device_type = "iphone"
        sdk_anonymous_device_identifier.account = accounts(:minimum_sdk)
        sdk_anonymous_device_identifier.user_sdk_identity = @user_sdk_identity
        sdk_anonymous_device_identifier.mobile_sdk_app = mobile_sdk_apps(:minimum_sdk)
      end
      @ticket = tickets(:minimum_mobile_sdk)
      @request_token = RequestToken.new(ticket: @ticket)
      @request_token.save!
      @user = @ticket.requester
      @ticket.requester.identities << @user_sdk_identity
      @ticket.requester.save!
      @requester_identifier.subscribe_to_ticket(@ticket)
      @ticket.will_be_saved_by(users(:minimum_sdk_agent))
      @ticket.account.stubs(:has_sdk_manage_requests?).returns(true)

      assert @ticket.requester.is_end_user?
      assert_not_empty @ticket.sdk_anonymous_device_identifiers
    end

    describe "public comment" do
      before do
        @ticket.add_comment(body: "Foo", is_public: true)
      end

      it "sends a push notifications for the requester" do
        job_args = {
          account_id: accounts(:minimum_sdk).id,
          device_ids: [@requester_identifier.id],
          mobile_sdk_app_id: @requester_identifier.mobile_sdk_app_id,
          payload: {
            'user_id' => @user.id,
            'msg' => I18n.t("txt.mobile_sdk.push_notifications.comment"),
            'msg_short' => I18n.t("txt.mobile_sdk.push_notifications.comment"),
            'ticket_id' => @request_token.value
          }
        }

        PushNotificationSdkSendJob.expects(:enqueue).once.with(job_args)
        @ticket.save!
      end
    end

    describe "private comment" do
      before do
        @ticket.add_comment(body: "Foo", is_public: false)
      end

      it "does not send a push notification" do
        PushNotificationSdkSendJob.expects(:enqueue).never
        @ticket.save!
      end
    end

    describe "Patagonia" do
      it "does not send a push notification if the 'sdk_manage_requests' feature is disabled" do
        @ticket.account.stubs(:has_sdk_manage_requests?).returns(false)
        PushNotificationSdkSendJob.expects(:enqueue).never
        @ticket.save!
      end
    end
  end

  describe "first party user" do # Mobile Apps
    before do
      PushNotificationRegistrationJob.stubs(:enqueue)
      @ticket = tickets(:minimum_2)
      @ticket.via_id = ViaType.MOBILE
      assert @ticket.requester.is_end_user?
      @user = @ticket.requester
      PushNotifications::FirstPartyDeviceIdentifier.create!(
        user: @user,
        token: "bar",
        device_type: "iphone",
        account: accounts(:minimum),
        mobile_app: mobile_apps(:com_zendesk_agent)
      )
      assert_not_empty @user.device_identifiers.active
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.account.stubs(:has_sdk_manage_requests?).returns(true)
      @ticket.account.stubs(:has_mobile_sdk_notify_non_sdk_updates?).returns(true)
    end

    it "does not send a push notification" do
      @ticket.add_comment(body: "Foo", is_public: true)
      PushNotificationSdkSendJob.expects(:enqueue).never
      @ticket.save!
    end
  end

  describe "JWT user" do
    before do
      @ticket = tickets(:minimum_2)
      @ticket.via_id = ViaType.MOBILE_SDK
      assert @ticket.requester.is_end_user?
      @user = @ticket.requester
      @requester_identifier = PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(
        token: "foofoo",
        device_type: "iphone",
        account: accounts(:minimum),
        user: @user,
        mobile_sdk_app: mobile_sdk_apps(:minimum)
      )
      assert_not_empty @user.device_identifiers.active
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.account.stubs(:has_sdk_manage_requests?).returns(true)

      @job_args = {
          account_id: @ticket.account.id,
          device_ids: [@requester_identifier.id],
          mobile_sdk_app_id: @requester_identifier.mobile_sdk_app_id,
          payload: {
            'user_id' => @user.id,
            'msg' => I18n.t("txt.mobile_sdk.push_notifications.comment"),
            'msg_short' => I18n.t("txt.mobile_sdk.push_notifications.comment"),
            'ticket_id' => @ticket.nice_id.to_s
          }
        }
    end

    describe "public comment" do
      before do
        @ticket.add_comment(body: "Foo", is_public: true)
      end

      it "sends a push notifications for the requester" do
        PushNotificationSdkSendJob.expects(:enqueue).once.with(@job_args)
        @ticket.save!
      end
    end

    describe "private comment" do
      before do
        @ticket.add_comment(body: "Foo", is_public: false)
      end

      it "does not send a push notification" do
        PushNotificationSdkSendJob.expects(:enqueue).never
        @ticket.save!
      end
    end

    describe "Patagonia" do
      it "does not send a push notification if the 'sdk_manage_requests' feature is disabled" do
        @ticket.account.stubs(:has_sdk_manage_requests?).returns(false)
        PushNotificationSdkSendJob.expects(:enqueue).never
        @ticket.save!
      end
    end

    describe "non sdk update" do
      before do
        @ticket.via_id = ViaType.WEB_FORM
        @ticket.add_comment(body: "Foo", is_public: true)
      end

      it "sends a push notification if the 'mobile_sdk_notify_non_sdk_updates' feature is enabled" do
        @ticket.account.stubs(:has_mobile_sdk_notify_non_sdk_updates?).returns(true)
        PushNotificationSdkSendJob.expects(:enqueue).once.with(@job_args)
        @ticket.save!
      end

      it "does not send a push notification if the 'mobile_sdk_notify_non_sdk_updates' feature is disabled" do
        @ticket.account.stubs(:has_mobile_sdk_notify_non_sdk_updates?).returns(false)
        PushNotificationSdkSendJob.expects(:enqueue).never
        @ticket.save!
      end
    end
  end

  describe "Non SDK user" do
    before do
      # Don't associate and device ids with user
      @user_identity = user_identities(:minimum_end_user)
      @ticket = tickets(:minimum_1)
      @request_token = RequestToken.new(ticket: @ticket)
      @request_token.save!
      @user = @ticket.requester
      @ticket.requester.identities << @user_identity
      @ticket.requester.save!
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.account.stubs(:has_sdk_manage_requests?).returns(true)

      assert @ticket.requester.is_end_user?
      assert_empty @ticket.sdk_anonymous_device_identifiers
      @ticket.via_id = ViaType.WEB_FORM
      @ticket.add_comment(body: "Foo", is_public: true)
    end

    it "does not send a push notification if the 'mobile_sdk_notify_non_sdk_updates' is enabled" do
      @ticket.account.stubs(:has_mobile_sdk_notify_non_sdk_updates?).returns(true)
      PushNotificationSdkSendJob.expects(:enqueue).never
      @ticket.save!
    end

    it "does not send a push notification if the 'mobile_sdk_notify_non_sdk_updates' is disabled" do
      @ticket.account.stubs(:has_mobile_sdk_notify_non_sdk_updates?).returns(false)
      PushNotificationSdkSendJob.expects(:enqueue).never
      @ticket.save!
    end
  end
end
