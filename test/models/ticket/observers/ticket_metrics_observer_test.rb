require_relative '../../../support/test_helper'
require_relative '../../../support/ticket_metric_helper'

SingleCov.covered!

describe TicketMetricsObserver do
  include TicketMetricHelper

  fixtures :accounts, :tickets, :users, :events

  let(:ticket)  { tickets(:minimum_1) }
  let(:account) { accounts(:minimum) }
  let(:agent)   { users(:minimum_agent) }

  let(:policy) { new_sla_policy }

  describe '.record_metric_events' do
    let(:slas_enabled) { true }

    before do
      Account.any_instance.stubs(has_service_level_agreements?: slas_enabled)
    end

    describe 'with instrumentation' do
      let(:span)        { stub(name: 'rack.request') }
      let(:tag_args)    { ['zendesk.ticket.metric_events.time', 123.4] }

      describe_with_arturo_enabled :instrument_ticket_save do
        before { Benchmark.expects(:ms).returns(tag_args.last) }

        describe 'and the rack_request_span is defined' do
          before do
            ticket.stubs(:rack_request_span).returns(span)
            ticket.send(:record_metric_events)
          end

          before_should 'set appropriate tags' do
            span.expects(:set_tag).with(*tag_args)
          end
        end

        describe 'and the rack_request_span is not defined' do
          it 'does not try to set tags' do
            ticket.send(:record_metric_events)
          end
        end
      end

      describe_with_arturo_disabled :instrument_ticket_save do
        before { ticket.send(:record_metric_events) }

        before_should 'not try to instrument duration of method' do
          ticket.expects(:rack_request_span).never
        end
      end
    end

    describe 'when the account has SLAs enabled' do
      describe 'and persisting the SLA ticket policy' do
        let(:policy_persistence) { stub(:policy_persistence) }

        before { ticket.send(:record_metric_events) }

        before_should 'perist the SLA ticket policy' do
          Zendesk::Sla::PolicyAssignment.
            expects(:persist).
            with(ticket).
            returns(policy_persistence)
        end
      end

      describe 'and caching SLA ticket policy statuses' do
        describe 'and the ticket is assigned a policy' do
          before do
            ticket.create_sla_ticket_policy(
              account: ticket.account,
              policy:  policy
            )

            ticket.send(:record_metric_events)
          end

          before_should 'cache the statuses and calculate the SLA' do
            Sla::TicketPolicy.any_instance.expects(:cache_statuses).once

            Zendesk::Sla::TicketStatus.any_instance.expects(:calculate).once
          end
        end

        describe 'and the ticket is not assigned a policy' do
          before { ticket.send(:record_metric_events) }

          before_should 'not cache the statuses but calculate the SLA' do
            Sla::TicketPolicy.any_instance.expects(:cache_statuses).never

            Zendesk::Sla::TicketStatus.any_instance.expects(:calculate).once
          end
        end
      end

      describe 'and evaluating ticket metric events' do
        let(:state_machine_mock) { mock('state_machine_mock') }

        describe 'and the ticket is new' do
          before do
            ticket.stubs(id_changed?: true)

            Zendesk::TicketMetric::StateMachine.
              stubs(:all).
              returns([state_machine_mock])

            state_machine_mock.stubs(:build).returns(state_machine_mock)

            ticket.send(:record_metric_events)
          end

          before_should 'advance the state machine' do
            state_machine_mock.expects(:advance).once
          end
        end

        describe 'and the ticket has metric events' do
          before do
            ticket.stubs(id_changed?: false)

            ticket.metric_events.agent_work_time.activate.create(
              account: ticket.account,
              time:    Time.new(2015)
            ).save!

            Zendesk::TicketMetric::StateMachine.
              stubs(:all).
              returns([state_machine_mock])

            state_machine_mock.stubs(:build).returns(state_machine_mock)

            ticket.send(:record_metric_events)
          end

          before_should 'advance the state machine' do
            state_machine_mock.expects(:advance).once
          end
        end

        describe 'and the ticket is not new or has any metric events' do
          before do
            ticket.stubs(id_changed?: false)

            Zendesk::TicketMetric::StateMachine.
              stubs(:all).
              returns([state_machine_mock])

            state_machine_mock.stubs(:build).returns(state_machine_mock)

            ticket.send(:record_metric_events)
          end

          before_should 'not advance the state machine' do
            state_machine_mock.expects(:advance).never
          end
        end
      end

      describe 'and instrumenting' do
        let(:evaluate_stub)           { stub(:evaluate_stub) }
        let(:cache_stub)              { stub(:cache_stub) }
        let(:policy_assignment_stub)  { stub(:policy_assignment_stub) }

        before do
          ticket.stubs(id_changed?: true)

          policy_assignment_stub.
            expects(:set_tag).
            with(Datadog::Ext::Analytics::TAG_ENABLED, true)

          policy_assignment_stub.
            expects(:set_tag).
            with('zendesk.account_id', ticket.account_id)

          policy_assignment_stub.
            expects(:set_tag).
            with('zendesk.ticket.id', ticket.id)

          policy_assignment_stub.
            expects(:set_tag).
            with('zendesk.account_subdomain', ticket.account.subdomain)

          policy_assignment_stub.
            expects(:set_tag).
            with('zendesk.sla.policy_id', policy_id)

          evaluate_stub.
            expects(:set_tag).
            with(Datadog::Ext::Analytics::TAG_ENABLED, true)

          evaluate_stub.
            expects(:set_tag).
            with('zendesk.account_id', ticket.account_id)

          evaluate_stub.
            expects(:set_tag).
            with('zendesk.ticket.id', ticket.id)

          evaluate_stub.
            expects(:set_tag).
            with('zendesk.account_subdomain', ticket.account.subdomain)

          evaluate_stub.
            expects(:set_tag).
            with('zendesk.sla.policy_id', policy_id)

          cache_stub.
            expects(:set_tag).
            with(Datadog::Ext::Analytics::TAG_ENABLED, true)

          cache_stub.
            expects(:set_tag).
            with('zendesk.account_id', ticket.account_id)

          cache_stub.
            expects(:set_tag).
            with('zendesk.ticket.id', ticket.id)

          cache_stub.
            expects(:set_tag).
            with('zendesk.account_subdomain', ticket.account.subdomain)

          cache_stub.
            expects(:set_tag).
            with('zendesk.sla.policy_id', policy_id)

          Zendesk::Sla::PolicyAssignment.stubs(
            persist: policy_assignment_stub
          )
        end

        describe 'and the ticket is assigned a policy' do
          let(:policy_id) { policy.id }

          before do
            ticket.create_sla_ticket_policy(
              account: ticket.account,
              policy:  policy
            )

            ticket.send(:record_metric_events)
          end

          before_should 'record the appropriate traces' do
            ZendeskAPM.
              expects(:trace).
              with(
                'ticket_metric.sla_policy_persistence',
                service: Zendesk::TicketMetric::APM_SERVICE_NAME
              ).
              yields(policy_assignment_stub)

            ZendeskAPM.
              expects(:trace).
              with(
                'ticket_metric.evaluate',
                service: Zendesk::TicketMetric::APM_SERVICE_NAME
              ).
              yields(evaluate_stub)

            ZendeskAPM.
              expects(:trace).
              with(
                'ticket_metric.cache_statuses',
                service: Zendesk::TicketMetric::APM_SERVICE_NAME
              ).
              yields(cache_stub)
          end
        end

        describe 'and the ticket is not assigned a policy' do
          let(:policy_id) { nil }

          before { ticket.send(:record_metric_events) }

          before_should 'record the appropriate traces' do
            ZendeskAPM.
              expects(:trace).
              with(
                'ticket_metric.sla_policy_persistence',
                service: Zendesk::TicketMetric::APM_SERVICE_NAME
              ).
              yields(policy_assignment_stub)

            ZendeskAPM.
              expects(:trace).
              with(
                'ticket_metric.evaluate',
                service: Zendesk::TicketMetric::APM_SERVICE_NAME
              ).
              yields(evaluate_stub)

            ZendeskAPM.
              expects(:trace).
              with(
                'ticket_metric.cache_statuses',
                service: Zendesk::TicketMetric::APM_SERVICE_NAME
              ).
              yields(cache_stub)
          end
        end
      end
    end

    describe 'when the account does not have SLAs enabled' do
      let(:slas_enabled) { false }

      describe 'and persisting the SLA ticket policy' do
        before { ticket.send(:record_metric_events) }

        before_should 'not perist' do
          Zendesk::Sla::PolicyAssignment.expects(:persist).never
        end
      end

      describe 'and caching SLA ticket policy statuses' do
        before do
          ticket.create_sla_ticket_policy(
            account: ticket.account,
            policy:  policy
          )

          ticket.send(:record_metric_events)
        end

        before_should 'not cache the statuses or calculate the SLA' do
          Sla::TicketPolicy.any_instance.expects(:cache_statuses).never

          Zendesk::Sla::TicketStatus.any_instance.expects(:calculate).never
        end
      end

      describe 'and evaluating the ticket metric events' do
        let(:state_machine_mock) { mock('state_machine_mock') }

        describe 'and the ticket is new' do
          before do
            ticket.stubs(id_changed?: true)

            Zendesk::TicketMetric::StateMachine.
              stubs(:all).
              returns([state_machine_mock])

            state_machine_mock.stubs(build: state_machine_mock)

            ticket.send(:record_metric_events)
          end

          before_should 'advance each state machine' do
            state_machine_mock.expects(:advance).once
          end
        end

        describe 'and the ticket has metric events' do
          before do
            ticket.stubs(id_changed?: false)

            ticket.metric_events.agent_work_time.activate.create(
              account: ticket.account,
              time:    Time.new(2015)
            ).save!

            Zendesk::TicketMetric::StateMachine.
              stubs(:all).
              returns([state_machine_mock])

            state_machine_mock.stubs(:build).returns(state_machine_mock)

            ticket.send(:record_metric_events)
          end

          before_should 'advance each metric state machine' do
            state_machine_mock.expects(:advance).once
          end
        end

        describe 'and the ticket is not new or has any metric events' do
          before do
            ticket.stubs(id_changed?: false)

            Zendesk::TicketMetric::StateMachine.
              stubs(:all).
              returns([state_machine_mock])

            state_machine_mock.stubs(:build).returns(state_machine_mock)

            ticket.send(:record_metric_events)
          end

          before_should 'not advance each metric state machine' do
            state_machine_mock.expects(:advance).never
          end
        end
      end

      describe 'and instrumenting' do
        let(:span) { stub(:span) }

        before do
          ticket.stubs(id_changed?: true)

          span.
            expects(:set_tag).
            with(Datadog::Ext::Analytics::TAG_ENABLED, true)

          span.expects(:set_tag).with('zendesk.account_id', ticket.account_id)
          span.expects(:set_tag).with('zendesk.ticket.id', ticket.id)

          span.
            expects(:set_tag).
            with('zendesk.account_subdomain', ticket.account.subdomain)

          span.expects(:set_tag).with('zendesk.sla.policy_id', policy_id)
        end

        describe 'and the ticket is assigned a policy' do
          let(:policy_id) { policy.id }

          before do
            ticket.create_sla_ticket_policy(
              account: ticket.account,
              policy:  policy
            )

            ticket.send(:record_metric_events)
          end

          before_should 'record a ticket metric evaluate trace' do
            ZendeskAPM.
              expects(:trace).
              with(
                'ticket_metric.evaluate',
                service: Zendesk::TicketMetric::APM_SERVICE_NAME
              ).
              yields(span)
          end
        end

        describe 'and the ticket is not assigned a policy' do
          let(:policy_id) { nil }

          before { ticket.send(:record_metric_events) }

          before_should 'record a ticket metric evaluate trace' do
            ZendeskAPM.
              expects(:trace).
              with(
                'ticket_metric.evaluate',
                service: Zendesk::TicketMetric::APM_SERVICE_NAME
              ).
              yields(span)
          end
        end
      end
    end
  end

  describe '.update_ticket_metrics' do
    describe 'when updating the metric set' do
      let(:is_new)        { false }
      let(:shared_ticket) { false }

      let(:ticket_metric_set) do
        TicketMetricSet.new(ticket: ticket, dirty_ticket: ticket)
      end

      before do
        ticket.audit = events(:create_audit_for_minimum_ticket_1)

        ticket.audit.stubs(ticket_sharing_create: shared_ticket)

        ticket.stubs(id_changed?: is_new)

        ticket.stubs(creating_shared_ticket?: shared_ticket)

        ticket.send(:update_ticket_metrics)
      end

      before_should 'update the ticket metric set' do
        ticket_metric_set.expects(:update_metrics).once

        TicketMetricSet.expects(:create).never
      end

      describe 'and the ticket is deleted' do
        let(:ticket)  { tickets(:minimum_deleted) }

        before_should 'not update or create a ticket metric set' do
          ticket_metric_set.expects(:update_metrics).never

          TicketMetricSet.expects(:create).never
        end
      end

      describe 'and the ticket is new' do
        let(:is_new) { true }

        before_should 'create a ticket metric set' do
          ticket_metric_set.expects(:update_metrics).never

          TicketMetricSet.expects(:create).once
        end
      end

      describe 'and a ticket metric set does not already exist' do
        let(:ticket_metric_set) { TicketMetricSet.new }

        before_should 'not update or create a ticket metric set' do
          ticket_metric_set.expects(:update_metrics).never

          TicketMetricSet.expects(:create).never
        end
      end

      describe 'and it is a shared ticket' do
        let(:shared_ticket) { true }

        before_should 'does not update or create a ticket metric set' do
          ticket_metric_set.expects(:update_metrics).never

          TicketMetricSet.expects(:create).never
        end
      end
    end

    describe 'when instrumenting' do
      let(:span) { stub(:span) }

      before do
        span.
          expects(:set_tag).
          with(Datadog::Ext::Analytics::TAG_ENABLED, true)

        span.expects(:set_tag).with('zendesk.account_id', ticket.account_id)
        span.expects(:set_tag).with('zendesk.ticket.id', ticket.id)

        span.
          expects(:set_tag).
          with('zendesk.account_subdomain', ticket.account.subdomain)

        ticket.send(:update_ticket_metrics)
      end

      before_should 'record the `ticket_metric_set` trace' do
        ZendeskAPM.
          expects(:trace).
          with(
            'ticket_metric_sets.record_metric_sets',
            service: TicketMetricSet::APM_SERVICE_NAME
          ).
          yields(span)
      end
    end
  end
end
