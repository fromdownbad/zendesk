require_relative '../../../support/test_helper'

SingleCov.covered!

describe TicketEventBusObserver do
  fixtures :tickets, :users

  let(:ticket) do
    tickets(:minimum_1).tap do |t|
      t.domain_event_publisher = domain_event_publisher
    end
  end

  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  describe 'on_save' do
    describe_with_arturo_enabled 'publish_ticket_events_to_bus' do
      describe 'with events to emit' do
        before do
          ticket.assignee = users(:minimum_admin)
          ticket.status_id = 1
        end

        it 'publishes protobuf events' do
          ticket.publish_ticket_events_to_bus!
          domain_event_publisher.events.length.must_equal 2
        end

        describe 'with an publishing error' do
          it 'logs and supresses exceptions' do
            ticket.expects(:ticket_events_encoder).raises(RuntimeError)
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('publish.errors', tags: ["exception:RuntimeError"])
            ticket.publish_ticket_events_to_bus! # implicit wont_raise(RuntimeError)
          end
        end

        describe 'statsd reporting' do
          it 'times the encoding process' do
            Zendesk::StatsD::Client.any_instance.expects(:time).once
            ticket.publish_ticket_events_to_bus!
          end

          it 'records domain event counts' do
            Zendesk::StatsD::Client.any_instance.expects(:increment).twice
            ticket.publish_ticket_events_to_bus!
          end

          it 'records message size stats' do
            Zendesk::StatsD::Client.any_instance.expects(:histogram).times(3)
            ticket.publish_ticket_events_to_bus!
          end
        end
      end

      describe 'with no events to emit' do
        before do
          TicketEventsProtobufEncoder.any_instance.stubs(:to_a).returns([])
        end

        it 'does not publish protobuf events' do
          ticket.publish_ticket_events_to_bus!
          domain_event_publisher.events.length.must_equal 0
        end
      end
    end

    describe_with_arturo_disabled 'publish_ticket_events_to_bus' do
      it 'does not create events' do
        ticket.publish_ticket_events_to_bus!
        domain_event_publisher.events.length.must_equal 0
      end
    end
  end
end
