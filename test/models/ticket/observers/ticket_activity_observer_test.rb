require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'TicketActivityObserver' do
  fixtures :all

  before do
    @ticket = tickets(:minimum_1)

    Activity.without_arsi.delete_all
  end

  describe "Assigning a ticket to an agent" do
    describe "on creating the ticket" do
      before do
        @ticket = accounts(:minimum).tickets.new
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.assignee = users(:minimum_agent)
        @ticket.subject = "New Ticket"
        @ticket.description = "New Ticket description"
        @ticket.save!
      end

      should_change("the number of TicketActivities", by: 1) { TicketActivity.count(:all) }

      it "creates a tickets.assignment TicketActivity for the assignee" do
        activity = @ticket.assignee.activities.order("created_at DESC").last
        assert_equal @ticket, activity.activity_target
        assert_equal @ticket, activity.activity_object
        assert_equal @ticket.audits.last.author, activity.actor
        assert_equal @ticket.assignee, activity.user
        assert_equal "tickets.assignment", activity.verb
      end
    end

    describe "on updating the ticket" do
      before do
        @ticket = accounts(:minimum).tickets.new
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.assignee = users(:minimum_agent)
        @ticket.subject = "New Ticket"
        @ticket.description = "New Ticket description"
        @ticket.save!
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.assignee = users(:minimum_agent)
        @ticket.save!
      end

      should_change("the number of TicketActivities", by: 1) { TicketActivity.count(:all) }

      it "creates a tickets.assignment TicketActivity for the assignee" do
        activity = @ticket.assignee.activities.order("created_at DESC").last
        assert_equal @ticket, activity.activity_target
        assert_equal @ticket, activity.activity_object
        assert_equal @ticket.audits.last.author, activity.actor
        assert_equal @ticket.assignee, activity.user
        assert_equal "tickets.assignment", activity.verb
      end
    end
  end

  describe "Commenting on a ticket" do
    before do
      @collaborator = users(:minimum_admin_not_owner)

      @ticket.will_be_saved_by(users(:minimum_admin))
      @ticket.set_collaborators = [@collaborator.id]
      @ticket.save!

      @ticket.will_be_saved_by(users(:minimum_admin))
      @ticket.add_comment(body: "Foo", is_public: false)
      @ticket.save!
    end

    should_change("the number of TicketActivities", by: 1) { TicketActivity.count(:all) }

    it "creates a tickets.comment TicketActivity for the assignee" do
      activity = @ticket.assignee.activities.order("created_at DESC").last

      assert_equal @ticket, activity.activity_target
      assert_equal @ticket.comments.last, activity.activity_object
      assert_equal @ticket.audits.last.author, activity.actor
      assert_equal @ticket.assignee, activity.user
      assert_equal "tickets.comment", activity.verb
    end
  end

  describe "Commenting and adding CCs on a ticket" do
    before do
      @collaborator = users(:minimum_admin_not_owner)
      @ticket.will_be_saved_by(users(:minimum_admin))
      @ticket.add_comment(body: "Foo", is_public: false)
      @ticket.set_collaborators = [@collaborator.id]
      @ticket.save!
    end

    should_change("the number of TicketActivities", by: 1) { TicketActivity.count(:all) }

    it "creates a tickets.comment TicketActivity for the assignee" do
      activity = @ticket.assignee.activities.order("created_at DESC").last

      assert_equal @ticket, activity.activity_target
      assert_equal @ticket.comments.last, activity.activity_object
      assert_equal @ticket.audits.last.author, activity.actor
      assert_equal @ticket.assignee, activity.user
      assert_equal "tickets.comment", activity.verb
    end
  end

  describe "Increasing priority of a ticket" do
    before do
      @ticket.will_be_saved_by(users(:minimum_admin))
      @ticket.set_collaborators = [users(:minimum_admin_not_owner).id]
      @ticket.priority_id += 1
      @ticket.save!
    end

    should_change("the number of TicketActivities", by: 1) { TicketActivity.count(:all) }

    it "creates a tickets.priority_increase TicketActivity for the assignee" do
      activity = @ticket.assignee.activities.order("created_at DESC").last
      change = @ticket.audits.last.events.all.find { |e| e.is_a?(Change) && e.field_name == 'priority_id' }

      assert_equal @ticket, activity.activity_target
      assert_equal change, activity.activity_object
      assert_equal @ticket.audits.last.author, activity.actor
      assert_equal @ticket.assignee, activity.user
      assert_equal "tickets.priority_increase", activity.verb
    end
  end
end
