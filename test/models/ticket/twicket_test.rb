require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Ticket do
  fixtures :accounts, :ticket_fields, :custom_field_options, :tickets, :users, :ticket_forms, :groups, :organizations, :account_settings, :events

  describe "#twickets" do
    before do
      @ticket = tickets(:minimum_1)
      @mth    = MonitoredTwitterHandle.last
      @ticket.stubs(:monitored_twitter_handle).returns(@mth)
    end

    describe "#create_twitter_action" do
      it "creates a TwitterAction" do
        @ticket.update_column(:via_id, ViaType.TWITTER)
        Channels::Actions::ActionJob.expects(:work).with(@ticket.account.id, anything)
        User.any_instance.stubs(:twitter_profile).returns(stub(screen_name: "receiver"))
        assert_difference('TwitterAction.count(:all)', 1) do
          @result = @ticket.create_twitter_action(:action, @ticket.comments.first.id, users(:minimum_agent), @ticket.requester)
        end
        action = TwitterAction.last
        assert_equal action.action, "action"
        assert_equal action.comment_id, @ticket.comments.first.id.to_s
        source = @ticket.ticket_source
        user_info = {
          monitored_handle_id: source.id,
          monitored_handle_name: source.twitter_screen_name,
          requester_handle_name: "receiver"
        }
        assert_equal user_info, action.user_info
        assert_equal :ok, @result[:status]
      end
    end

    describe "#twitter_action_status_message" do
      describe "event is an error" do
        before do
          @message = "Error!"
          @event = ::Error.new(ticket: @ticket, value: @message)
          @result = @ticket.send(:twitter_action_status_message, @event)
        end

        it "returns information about the the error" do
          assert_equal @result[:status], :error
          assert_equal @result[:message], @message
        end
      end

      describe "event isn't an error" do
        before do
          @action = :retweet!
          @event = TwitterAction.new(ticket: @ticket, action: @action)
          @result = @ticket.send(:twitter_action_status_message, @event)
        end

        it "returns success message" do
          assert_equal @result[:status], :ok
        end
      end
    end
  end

  describe "#twicket_url" do
    let(:ticket) { tickets(:minimum_1) }
    let(:account) { ticket.account }
    let(:brand) { ticket.brand }

    before do
      account.update_attribute(:host_mapping, "www.account.com")
      brand.route.update_attribute(:host_mapping, "www.brand.com")
    end

    describe "with multiple brands" do
      before { account.stubs(:has_multiple_active_brands?).returns(false) }

      it "returns the mapped account url" do
        assert_equal "http://www.account.com/twickets/#{ticket.nice_id}", ticket.twicket_url
      end
    end

    describe "without multiple brands" do
      before { account.stubs(:has_multiple_active_brands?).returns(true) }

      it "returns the mapped brand url" do
        assert_equal "http://www.brand.com/twickets/#{ticket.nice_id}", ticket.twicket_url
      end
    end
  end
end
