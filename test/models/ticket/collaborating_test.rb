require_relative "../../support/test_helper"
require_relative "../../support/collaboration_settings_test_helper"
require_relative "../../support/agent_test_helper"
require_relative "../../support/collaboration_test_helper"

SingleCov.covered! uncovered: 11

describe 'Ticket::Collaborating' do
  include AgentTestHelper
  include CollaborationTestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:end_user) { users(:minimum_end_user) }
  let(:persisted_end_user) { users(:minimum_search_user) }
  let(:agent) { users(:minimum_agent) }
  let(:persisted_agent) { users(:minimum_admin) }
  let(:persisted_collaborators) { [] }
  let(:all_display_names) { "#{persisted_end_user.name}, #{persisted_agent.name}, #{end_user.name}, #{agent.name}, Karla Dominguez" }

  let(:ticket) do
    Ticket.new(account: account, description: 'Old Ticket').tap do |ticket|
      ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
      ticket.save!
    end
  end

  # Ticket hasn't been saved, and includes 3 in flight LEGACY_CC collaborators.
  let(:unsaved_ticket) do
    Ticket.new(account: account, description: 'New Ticket').tap do |ticket|
      ticket.set_collaborators = persisted_collaborators.map(&:id)
      ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
      ticket.save!
      ticket.additional_collaborators = [agent.id, end_user.id, 'karla_dominguez@example.com']
    end
  end

  before { CollaborationSettingsTestHelper.disable_all_new_collaboration_settings }

  describe '#current_collaborator_ids_added/removed' do
    let(:agent_1) { FactoryBot.create(:agent, account: account) }
    let(:agent_2) { FactoryBot.create(:agent, account: account) }
    let(:agent_3) { FactoryBot.create(:agent, account: account) }
    let(:agent_4) { FactoryBot.create(:agent, account: account) }

    let(:ticket) do
      ticket = tickets(:minimum_1)
      ticket.set_collaborators_with_type([agent_1.id, agent_2.id, agent_3.id], CollaboratorType.EMAIL_CC)
      ticket.set_collaborators_with_type([agent_4.id], CollaboratorType.FOLLOWER)
      ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
      ticket
    end

    before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

    it 'lists follower and email_cc ids being added in this ticket' do
      new_collaborations = ticket.send(:in_flight_collaborations).select(&:new_record?)
      email_cc_ids = ticket.collaborator_ids_added(new_collaborations, CollaboratorType.EMAIL_CC)
      follower_ids = ticket.collaborator_ids_added(new_collaborations, CollaboratorType.FOLLOWER)
      assert_equal [agent_1.id, agent_2.id, agent_3.id].sort, email_cc_ids.sort
      assert_equal [agent_4.id], follower_ids
    end

    it 'lists follower and email_cc ids being removed' do
      ticket.save!
      ticket.reload
      ticket.set_collaborators = [agent_2.id, agent_3.id]
      ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)

      email_cc_ids = ticket.collaborator_ids_removed(CollaboratorType.EMAIL_CC)
      follower_ids = ticket.collaborator_ids_removed(CollaboratorType.FOLLOWER)
      assert_equal [agent_1.id], email_cc_ids
      assert_equal [agent_4.id], follower_ids
    end

    it 'saves the day if stuff gets weird and we get a bad collaboration' do
      bad_collab = Collaboration.new(collaborator_type: CollaboratorType.LEGACY_CC, user: nil)
      ticket.instance_variable_set(:@collaborations_for_removal, [bad_collab])
      assert_equal ticket.collaborator_ids_removed(CollaboratorType.LEGACY_CC), []
    end
  end

  describe "#add_ccs_changed_domain_events" do
    let(:follower) { FactoryBot.create(:agent, account: account) }
    let(:new_cc)   { users(:minimum_end_user2) }
    let(:account)  { accounts(:minimum) }
    let(:cc2)      { FactoryBot.create(:end_user, account: account) }
    let(:ticket) do
      account.tickets.new(
        requester: users(:minimum_agent),
        description: 'grow wombat',
        status_id: 0
      )
    end
    let(:domain_event_publisher) { FakeEscKafkaMessage.new }

    describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
      describe_with_arturo_enabled(:publish_cc_events_to_bus) do
        describe 'with CCs/Followers enabled' do
          before do
            CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
            ticket.domain_event_publisher = domain_event_publisher
          end

          it "publishes EmailCcChange and FollowerChange domain events" do
            ticket.set_collaborators_with_type([cc2.id], CollaboratorType.EMAIL_CC)
            ticket.set_collaborators_with_type([follower.id], CollaboratorType.FOLLOWER)
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.save!

            events = ticket.domain_event_publisher.events.map do |event|
              ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value])
            end
            assert_includes events.map(&:event), :ccs_changed
            assert_includes events.map(&:event), :followers_changed
            assert_equal [cc2.id], events.select { |e| e.event == :ccs_changed }.first.ccs_changed.users_added.user_ids
            assert_equal [follower.id], events.select { |e| e.event == :followers_changed }.first.followers_changed.users_added.user_ids
          end

          it 'does not publish an empty event' do
            ticket.set_collaborators_with_type([], CollaboratorType.FOLLOWER)
            ticket.set_collaborators_with_type([], CollaboratorType.EMAIL_CC)
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.expects(:add_domain_event).with(EmailCcsChangedProtobufEncoder.any_instance).never
            ticket.expects(:add_domain_event).with(FollowersChangedProtobufEncoder.any_instance).never
            ticket.save!
          end

          describe "creating a new ticket with a new follower" do
            let(:new_follower) { users(:minimum_agent) }
            let(:new_ticket) do
              ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
                requester_id: users(:minimum_author).id,
                followers: [{user_id: new_follower.id, user_email: "minimum_agent@aghassipour.com", user_name: "Agent Minimum", action: "put"}],
                comment: { body: "wombats"}
              })
              ticket_initializer.ticket
            end

            it 'ONLY issues a follower_changed event' do
              new_ticket.domain_event_publisher = domain_event_publisher

              new_ticket.will_be_saved_by(users(:minimum_admin))
              new_ticket.save!

              events = new_ticket.domain_event_publisher.events.map do |event|
                ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value])
              end
              assert_not_includes events.map(&:event), :ccs_changed
              assert_includes events.map(&:event), :followers_changed
              assert_equal [new_follower.id], events.select { |e| e.event == :followers_changed }.first.followers_changed.users_added.user_ids
            end
          end

          it 'publishes event for changing legacycc to follower' do
            Account.any_instance.stubs(:has_email_ccs_transform_collaborations?).returns(true)
            ticket = tickets(:minimum_1)
            ticket.domain_event_publisher = domain_event_publisher
            ticket.collaborations.create!(user: follower, collaborator_type: CollaboratorType.LEGACY_CC)
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.save!

            events = ticket.domain_event_publisher.events.map do |event|
              ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value])
            end
            assert_includes events.map(&:event), :ccs_changed
            assert_includes events.map(&:event), :followers_changed
            assert_equal [follower.id], events.select { |e| e.event == :ccs_changed }.first.ccs_changed.users_removed.user_ids
            assert_equal [follower.id], events.select { |e| e.event == :followers_changed }.first.followers_changed.users_added.user_ids
          end
        end

        describe 'with legacy ccs enabled' do
          before do
            CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
            ticket.domain_event_publisher = domain_event_publisher
          end

          it "publishes a Ccs Changed event" do
            ticket.domain_event_publisher = domain_event_publisher
            ticket.set_collaborators = [follower.id]
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.save!

            events = ticket.domain_event_publisher.events.map do |event|
              ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value])
            end
            assert_includes events.map(&:event), :ccs_changed
            assert_equal [follower.id], events.select { |e| e.event == :ccs_changed }.first.ccs_changed.users_added.user_ids
          end

          it 'does not publish an empty event' do
            ticket.domain_event_publisher = domain_event_publisher
            ticket.set_collaborators = []
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.expects(:add_domain_event).with(EmailCcsChangedProtobufEncoder.any_instance).never
            ticket.save!
          end

          it 'publishes event for changing follower to legacycc' do
            Account.any_instance.stubs(:has_email_ccs_transform_collaborations?).returns(true)
            ticket = tickets(:minimum_1)
            ticket.domain_event_publisher = domain_event_publisher
            ticket.collaborations.create!(user: follower, collaborator_type: CollaboratorType.FOLLOWER)
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.save!

            events = ticket.domain_event_publisher.events.map do |event|
              ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value])
            end
            assert_includes events.map(&:event), :ccs_changed
            assert_includes events.map(&:event), :followers_changed
            assert_equal [follower.id], events.select { |e| e.event == :ccs_changed }.first.ccs_changed.users_added.user_ids
            assert_equal [follower.id], events.select { |e| e.event == :followers_changed }.first.followers_changed.users_removed.user_ids
          end
        end
      end

      describe_with_arturo_disabled(:publish_cc_events_to_bus) do
        before do
          CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
          ticket.domain_event_publisher = domain_event_publisher
        end

        it "does not add cc events to bus" do
          Account.any_instance.expects(:add_ccs_changed_domain_events).never

          ticket.set_collaborators_with_type([cc2.id], CollaboratorType.EMAIL_CC)
          ticket.set_collaborators_with_type([follower.id], CollaboratorType.FOLLOWER)
          ticket.will_be_saved_by(users(:minimum_agent))
          ticket.save!
        end
      end
    end
  end

  describe '#ccs' do
    let(:agent_1) { FactoryBot.create(:agent, account: account) }
    let(:agent_2) { FactoryBot.create(:agent, account: account) }
    let(:agent_3) { FactoryBot.create(:agent, account: account) }
    let(:agent_4) { FactoryBot.create(:agent, account: account) }

    let(:ticket) do
      ticket = Ticket.new(account: account, description: 'New Ticket')
      ticket.set_collaborators_with_type([agent_1.id, agent_2.id, agent_3.id], CollaboratorType.EMAIL_CC)
      ticket.set_collaborators_with_type([agent_3.id], CollaboratorType.FOLLOWER)
      ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
      ticket.save!

      # include agent_1 in collaborations_for_removal
      ticket.reload.set_collaborators = [agent_2.id, agent_3.id, agent_4.id]
      ticket
    end

    before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      it { ticket.ccs.sort.must_equal [agent_2, agent_3, agent_4].sort }
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it { ticket.ccs.sort.must_equal [agent_2, agent_3, agent_4].sort }
    end
  end

  describe '#cc_names' do
    subject { unsaved_ticket.cc_names }

    before do
      unsaved_ticket.stubs(ccs: [agent, persisted_end_user])
      unsaved_ticket.stubs(email_ccs: [persisted_end_user])
      unsaved_ticket.stubs(in_flight_collaborators: [agent, end_user])
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      it { subject.must_equal persisted_end_user.name }
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it { subject.must_equal "#{agent.name}, #{persisted_end_user.name}" }
    end
  end

  describe "#collaborators" do
    let(:end_user) { users(:minimum_end_user) }
    let(:agent) { users(:minimum_agent) }

    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.account.owner)
    end

    it "returns the User associated with the ticket's collaborations" do
      @ticket.collaborations.build(user: end_user, ticket: @ticket, collaborator_type: CollaboratorType.EMAIL_CC)
      @ticket.save
      @ticket.collaborators.must_equal [end_user]
    end

    describe "when ticket has multiple collaborations for the same User but with different types" do
      before do
        @ticket.collaborations.build(user: agent, ticket: @ticket, collaborator_type: CollaboratorType.EMAIL_CC)
        @ticket.collaborations.build(user: agent, ticket: @ticket, collaborator_type: CollaboratorType.FOLLOWER)
        @ticket.save
      end

      it "includes the User just once" do
        assert_equal 1, @ticket.collaborators.size
        @ticket.collaborators.first.must_equal agent
      end
    end
  end

  describe "#collaboration_changes" do
    let(:email_cc) { users(:minimum_end_user) }
    let(:follower) { users(:minimum_admin) }

    before do
      CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.description = "foo"
    end

    describe "with no new collaborations" do
      it { @ticket.collaboration_changes.must_be_empty }
    end

    describe "with a new email CC collaboration and a new follower collaboration" do
      before do
        @ticket.collaborations.build(ticket: @ticket, user: email_cc, collaborator_type: CollaboratorType.EMAIL_CC)
        @ticket.collaborations.build(ticket: @ticket, user: follower, collaborator_type: CollaboratorType.FOLLOWER)
      end

      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          describe_with_arturo_setting_disabled :ticket_followers_allowed do
            it { @ticket.collaboration_changes.must_be_empty }
          end
        end
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it "checks to see if the EmailCcChange's email_cc_addresses need to be truncated" do
            Zendesk::Events::AuditEvents::ArrayValueTruncation.expects(:truncate_value).with([email_cc.email], EmailCcChange).once

            @ticket.collaboration_changes
          end

          it "contains an EmailCcChange" do
            assert_equal 1, @ticket.collaboration_changes.size
            assert @ticket.collaboration_changes.first.is_a? EmailCcChange
          end

          it "records the addition of the email CC collaboration" do
            @ticket.collaboration_changes.first.value.must_equal [email_cc.email]
            @ticket.collaboration_changes.first.value_previous.must_equal []
          end

          describe "when a persisted email CC collaboration is deleted" do
            before do
              @ticket.save!
              @ticket.stubs(in_flight_collaborations: [])
            end

            it "contains an EmailCcChange" do
              assert_equal 1, @ticket.collaboration_changes.size
              assert @ticket.collaboration_changes.first.is_a? EmailCcChange
            end

            it "records the deletion of the email CC collaboration" do
              @ticket.collaboration_changes.first.value.must_equal []
              @ticket.collaboration_changes.first.value_previous.must_equal [email_cc.email]
            end
          end
        end
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        describe_with_arturo_setting_enabled :ticket_followers_allowed do
          it "checks to see if the FollowerChange's follower_addresses need to be truncated" do
            Zendesk::Events::AuditEvents::ArrayValueTruncation.expects(:truncate_value).with([follower.email], FollowerChange).once

            @ticket.collaboration_changes
          end

          it "contains a FollowerChange" do
            assert_equal 1, @ticket.collaboration_changes.size
            assert @ticket.collaboration_changes.first.is_a? FollowerChange
          end

          it "records the addition of the follower collaboration" do
            @ticket.collaboration_changes.first.value.must_equal [follower.email]
            @ticket.collaboration_changes.first.value_previous.must_equal []
          end

          describe "when a persisted follower collaboration is deleted" do
            before do
              @ticket.save!
              @ticket.stubs(in_flight_collaborations: [])
            end

            it "contains an FollowerChange" do
              assert_equal 1, @ticket.collaboration_changes.size
              assert @ticket.collaboration_changes.first.is_a? FollowerChange
            end

            it "records the deletion of the email CC collaboration" do
              @ticket.collaboration_changes.first.value.must_equal []
              @ticket.collaboration_changes.first.value_previous.must_equal [follower.email]
            end
          end
        end
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          describe_with_arturo_setting_enabled :ticket_followers_allowed do
            it "contains both an EmailCcChange and a FollowerChange" do
              assert_equal 2, @ticket.collaboration_changes.size
              assert @ticket.collaboration_changes.first.is_a? EmailCcChange
              assert @ticket.collaboration_changes.last.is_a? FollowerChange
            end
          end
        end
      end
    end
  end

  describe "#notify_followers" do
    let(:author) { users(:minimum_agent) }
    let(:recipient) { users(:minimum_admin) }
    let(:ticket) { tickets(:minimum_1) }
    let(:collaborators) { recipient.email }

    # TODO: CCs - End-users will not be able to "follow" a ticket, re-write test when email_ccs are added
    describe 'end user collaborators' do
      let(:recipient) { users(:minimum_end_user) }

      describe 'for public tickets' do
        before do
          ticket.add_comment(body: "Koyaanisqatsi!", is_public: true)
          ticket.set_collaborators = collaborators
          ticket.will_be_saved_by(author)

          Account.any_instance.stubs(:cc_email_template).returns("{{ticket.comments_formatted}}")
          ActionMailer::Base.perform_deliveries = true
          ActionMailer::Base.deliveries.clear
        end

        it "are notified on public tickets" do
          ticket.save!
          assert(ticket.is_public)
          assert_equal 2, ActionMailer::Base.deliveries.size
          assert_includes ActionMailer::Base.deliveries.last.to_s, "Koyaanisqatsi"
        end
      end
    end

    describe "zendesk to zendesk mail" do
      let(:notify) { ticket.send(:notify_followers) }

      [true, false].each do |enabled|
        describe "when ticket_followers_allowed and comment_email_ccs_allowed capabilities are #{enabled ? 'on' : 'off'}" do
          before do
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: enabled,
              followers_setting: enabled,
              email_ccs_setting: enabled
            ).apply

            ticket.add_comment(body: "Koyaanisqatsi!", is_public: true)
            ticket.set_collaborators = collaborators
            ticket.will_be_saved_by(author)

            Account.any_instance.stubs(:cc_email_template).returns("{{ticket.comments_formatted}}")
            ActionMailer::Base.perform_deliveries = true
            ActionMailer::Base.deliveries.clear

            ticket.audit.comment = Comment.new(body: "My comment")
            ticket.collaborations.create(user: recipient)
          end

          it "sends from human to human" do
            notify
            assert_equal 2, ticket.audit.events.size
          end

          it "sends from human to machine" do
            recipient.identities.first.mark_as_machine
            notify
            assert_equal 2, ticket.audit.events.size
          end

          it "sends from machine to human" do
            author.identities.first.mark_as_machine
            notify
            assert_equal 2, ticket.audit.events.size
          end

          it "does not send from machine to machine to avoid loops" do
            author.identities.first.mark_as_machine
            recipient.identities.first.mark_as_machine
            notify
            assert_equal 0, ticket.audit.events.size
          end
        end
      end
    end
  end

  describe '#follower_names' do
    # Adds 2 persisted LEGACY_CC collaborators to the ticket - 1 agent and 1 end user
    let(:persisted_collaborators) { [persisted_end_user, persisted_agent] }

    describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
      describe_with_arturo_setting_enabled(:comment_email_ccs_allowed) do
        describe_with_arturo_setting_enabled(:ticket_followers_allowed) do
          it { unsaved_ticket.follower_names.must_equal "#{persisted_agent.name}, #{agent.name}" }
        end

        describe_with_arturo_setting_disabled(:ticket_followers_allowed) do
          it { unsaved_ticket.follower_names.must_equal "" }
        end
      end

      describe_with_arturo_setting_disabled(:comment_email_ccs_allowed) do
        describe_with_arturo_setting_enabled(:ticket_followers_allowed) do
          it { unsaved_ticket.follower_names.must_equal "#{persisted_agent.name}, #{agent.name}" }
        end

        describe_with_arturo_setting_disabled(:ticket_followers_allowed) do
          it { unsaved_ticket.follower_names.must_equal "" }
        end
      end
    end

    describe_with_arturo_setting_disabled(:follower_and_email_cc_collaborations) do
      it { unsaved_ticket.follower_names.must_equal "" }
    end
  end

  describe '#email_cc_names' do
    # Adds 2 persisted LEGACY_CC collaborators to the ticket - 1 agent and 1 end user
    let(:persisted_collaborators) { [persisted_end_user, persisted_agent] }

    describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
      describe_with_arturo_setting_enabled(:comment_email_ccs_allowed) do
        describe_with_arturo_setting_enabled(:ticket_followers_allowed) do
          it { unsaved_ticket.email_cc_names.must_equal "Karla Dominguez, #{end_user.name}, #{persisted_end_user.name}" }
        end

        describe_with_arturo_setting_disabled(:ticket_followers_allowed) do
          it { unsaved_ticket.email_cc_names.must_equal "Karla Dominguez, #{end_user.name}, #{persisted_end_user.name}" }
        end
      end

      describe_with_arturo_setting_disabled(:comment_email_ccs_allowed) do
        describe_with_arturo_setting_enabled(:ticket_followers_allowed) do
          it { unsaved_ticket.email_cc_names.must_equal "" }
        end

        describe_with_arturo_setting_disabled(:ticket_followers_allowed) do
          it { unsaved_ticket.email_cc_names.must_equal "" }
        end
      end
    end

    describe_with_arturo_setting_disabled(:follower_and_email_cc_collaborations) do
      describe_with_arturo_setting_enabled(:collaboration_enabled) do
        it { unsaved_ticket.email_cc_names.must_equal "" }
      end

      describe_with_arturo_setting_disabled(:collaboration_enabled) do
        it { unsaved_ticket.email_cc_names.must_equal "" }
      end
    end
  end

  describe '#only_follower_collaborations_changed?' do
    let(:persisted_collaborators) { [end_user, agent] }
    let(:ticket) do
      Ticket.new(account: account, description: 'New Ticket').tap do |ticket|
        ticket.set_collaborators = persisted_collaborators.map(&:id)
        ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
        ticket.save!
      end
    end

    describe 'with no changes' do
      it { refute ticket.only_follower_collaborations_changed? }
    end

    describe 'with changes' do
      before { ticket.additional_collaborators = [persisted_agent.id] }

      it { refute ticket.only_follower_collaborations_changed? }
    end

    describe 'with all new settings enabled' do
      before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

      describe 'with follower changes' do
        before { ticket.set_followers = [{ user_id: persisted_agent.id, action: :put }] }

        it { assert ticket.only_follower_collaborations_changed? }
      end

      describe 'when a follower is deleted' do
        before { ticket.set_followers = [{user_id: agent.id, action: :delete}] }

        it { assert ticket.only_follower_collaborations_changed? }
      end

      describe 'when a email_cc is deleted' do
        before { ticket.set_email_ccs = [{ user_id: end_user.id, action: :delete }] }

        it { refute ticket.only_follower_collaborations_changed? }
      end

      describe 'with email_cc and follower changes' do
        before do
          ticket.set_followers = [{ user_id: persisted_agent.id, action: :put }]
          ticket.set_email_ccs = [{ user_id: persisted_end_user.id, action: :put }]
        end

        it { refute ticket.only_follower_collaborations_changed? }
      end

      describe "when ticket's @collaborations_for_removal has not been initialized" do
        let(:ticket) { tickets(:minimum_1) }

        it "initializes @collaborations_for_removal" do
          ticket.instance_variable_get('@collaborations_for_removal').must_be_nil
          ticket.only_follower_collaborations_changed?
          ticket.instance_variable_get('@collaborations_for_removal').must_equal []
        end

        it { refute ticket.only_follower_collaborations_changed? }
      end
    end
  end

  describe "#persisted_email_cc_user_ids" do
    let(:persisted_collaborators) { [persisted_end_user] }

    before do
      CollaborationSettingsTestHelper.new(
        feature_arturo: true,
        feature_setting: true,
        followers_setting: false,
        email_ccs_setting: false
      ).apply
    end

    describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
      it { unsaved_ticket.persisted_email_cc_user_ids.must_equal [] }
    end

    describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
      it "calls the LegacyTypeTransformer.email_ccs" do
        Collaboration::LegacyTypeTransformer.expects(:email_ccs).returns([persisted_end_user]).at_least_once
        unsaved_ticket.persisted_email_cc_user_ids
      end

      it { unsaved_ticket.persisted_email_cc_user_ids.must_include persisted_end_user.id }
      it { unsaved_ticket.persisted_email_cc_user_ids.wont_include end_user.id }
    end
  end

  describe '#collaboration_changes' do
    let(:email_cc_change) do
      EmailCcChange.new.tap do |change|
        change.current_email_ccs = [end_user.email]
        change.previous_email_ccs = []
      end
    end

    let(:follower_change) do
      FollowerChange.new.tap do |change|
        change.current_followers = [agent.email]
        change.previous_followers = []
      end
    end

    before do
      CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
      ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
      ticket.audit.events << email_cc_change
      ticket.audit.events << follower_change
      ticket.save!
    end

    describe 'when email-CCs are disabled' do
      before { account.stubs(has_comment_email_ccs_allowed_enabled?: false) }
      it { assert_equal ['FollowerChange'], ticket.collaboration_changes.map(&:type) }
    end

    describe 'when followers are disabled' do
      before { account.stubs(has_ticket_followers_allowed_enabled?: false) }
      it { assert_equal ['EmailCcChange'], ticket.collaboration_changes.map(&:type) }
    end

    describe 'when both email-CCs and followers are disabled' do
      before { CollaborationSettingsTestHelper.disable_all_new_collaboration_settings }
      it { assert_equal [], ticket.collaboration_changes.map(&:type) }
    end

    describe 'when both email-CCs and followers are enabled' do
      before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }
      it { assert_equal ['EmailCcChange', 'FollowerChange'], ticket.collaboration_changes.map(&:type) }
    end
  end

  describe "#current_collaborator_ids" do
    let(:persisted_collaborators) { [end_user, agent] }
    let(:ticket) do
      Ticket.new(account: account, description: 'New Ticket').tap do |ticket|
        ticket.set_collaborators = persisted_collaborators.map(&:id)
        ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
        ticket.save!
      end
    end

    it "returns the IDs of the collaborations' users" do
      ticket.current_collaborator_ids.sort.must_equal persisted_collaborators.map(&:id).sort
    end

    describe "when there are no collaborations" do
      let(:persisted_collaborators) { [] }

      it { ticket.current_collaborator_ids.must_be_empty }
    end
  end

  describe "#email_cc_ids" do
    let(:persisted_collaborators) { [end_user, agent] }
    let(:ticket) do
      Ticket.new(account: account, description: 'New Ticket').tap do |ticket|
        ticket.set_email_ccs = persisted_collaborators.each_with_object([]) do |collaborator, normalized_params|
          normalized_params << { user_id: collaborator.id, action: :put }
        end
        ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
        ticket.save!
      end
    end

    describe "with email CCs settings enabled" do
      before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

      it "returns the IDs of the email CCs" do
        ticket.email_cc_ids.sort.must_equal persisted_collaborators.map(&:id).sort
      end

      describe "when there are no collaborations" do
        let(:persisted_collaborators) { [] }

        it { ticket.email_cc_ids.must_be_empty }
      end
    end

    describe "with email CCs settings disabled" do
      it { ticket.email_cc_ids.must_be_empty }
    end
  end

  describe "#follower_ids" do
    let(:persisted_collaborators) { [persisted_agent, agent] }
    let(:ticket) do
      Ticket.new(account: account, description: 'New Ticket').tap do |ticket|
        ticket.set_followers = persisted_collaborators.each_with_object([]) do |collaborator, normalized_params|
          normalized_params << { user_id: collaborator.id, action: :put }
        end
        ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
        ticket.save!
      end
    end

    describe "with followers settings enabled" do
      before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

      it "returns the IDs of the followers" do
        ticket.follower_ids.sort.must_equal persisted_collaborators.map(&:id).sort
      end

      describe "when there are no collaborations" do
        let(:persisted_collaborators) { [] }

        it { ticket.follower_ids.must_be_empty }
      end
    end

    describe "with followers settings disabled" do
      let(:persisted_collaborators) { [end_user, persisted_agent, agent] }

      it "returns the IDs of all collaborators" do
        ticket.follower_ids.sort.must_equal persisted_collaborators.map(&:id).sort
      end
    end
  end

  describe "collaboration associations" do
    before do
      @ticket = Ticket.new
      @ticket.account = accounts(:minimum)
      @ticket.account.stubs(:is_serviceable?).returns(true)
    end

    subject { @ticket }

    describe "#followers" do
      describe "with LEGACY_CC collaborations" do
        before do
          @ticket.collaborations.build(
            collaborator_type: CollaboratorType.LEGACY_CC,
            user_id: @ticket.account.agents.first.id,
            account_id: @ticket.account.id
          )
          @ticket.collaborations.build(
            collaborator_type: CollaboratorType.LEGACY_CC,
            user_id: @ticket.account.end_users.first.id,
            account_id: @ticket.account.id
          )
          @ticket.description = "blah"
          @ticket.requester = users(:minimum_end_user2)
          @ticket.will_be_saved_by(@ticket.requester, via_id: 4)
          @ticket.save
        end

        describe "when follower_and_email_cc_collaborations setting is off" do
          [
            CollaborationSettingsTestHelper.new(
              feature_arturo: false,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            ),
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            )
          ].each do |scenario|
            describe scenario.to_s do
              it "returns all collaborators" do
                scenario.apply
                ticket = Ticket.find(@ticket.id)
                ticket.stubs(in_flight_collaborators: [agent])
                ticket.stubs(collaborators: [agent, end_user])
                assert_equal ticket.collaborations.size, ticket.followers.size
              end
            end
          end

          describe "with include_uncommitted" do
            let(:ticket) { Ticket.find(@ticket.id) }

            before do
              ticket.stubs(in_flight_collaborators: [agent])
              ticket.stubs(collaborators: [agent, end_user])
            end

            subject { ticket.followers(include_uncommitted: uncommitted) }

            describe "set to false" do
              let(:uncommitted) { false }

              it "returns persisted and un-persisted collaborators" do
                assert_equal subject, [agent, end_user]
              end
            end

            describe "set to true" do
              let(:uncommitted) { true }

              it "returns persisted collaborators" do
                assert_equal subject, [agent]
              end
            end
          end
        end

        describe "when follower_and_email_cc_collaborations setting is on" do
          describe "when ticket_followers_allowed setting is off" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                followers_setting: false,
                email_ccs_setting: false
              ).apply
            end

            it "returns empty array" do
              ticket = Ticket.find(@ticket.id)
              assert_empty ticket.followers
            end
          end

          describe "when ticket_followers_allowed setting is on" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                followers_setting: true,
                email_ccs_setting: false
              ).apply
            end

            it "returns collaborators that are agents" do
              assert_equal 1, @ticket.followers.size
              assert(@ticket.followers.first.is_agent?)
            end
          end
        end
      end

      describe "with EMAIL_CC and FOLLOWER collaborations" do
        before do
          @agent = users(:minimum_agent)
          @admin = users(:minimum_admin)

          @ticket.collaborations.build(
            collaborator_type: CollaboratorType.FOLLOWER,
            user_id: @agent.id,
            account_id: @ticket.account.id
          )
          @ticket.collaborations.build(
            collaborator_type: CollaboratorType.EMAIL_CC,
            user_id: @admin.id,
            account_id: @ticket.account.id
          )
          @ticket.description = "blah"
          @ticket.requester = users(:minimum_end_user2)
          @ticket.will_be_saved_by(@ticket.requester, via_id: 4)
        end

        describe "when follower_and_email_cc_collaborations setting is off" do
          [
            CollaborationSettingsTestHelper.new(
              feature_arturo: false,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            ),
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            )
          ].each do |scenario|
            describe scenario.to_s do
              it "returns all collaborators" do
                scenario.apply
                @ticket.save
                ticket = Ticket.find(@ticket.id)
                assert_equal ticket.collaborations.size, ticket.followers.size
              end
            end
          end
        end

        describe "when follower_and_email_cc_collaborations setting is on" do
          describe "when ticket_followers_allowed setting is off" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                followers_setting: false,
                email_ccs_setting: false
              ).apply
              @ticket.save
            end

            it "returns an empty array" do
              ticket = Ticket.find(@ticket.id)
              assert_empty ticket.followers
            end
          end

          describe "when ticket_followers_allowed setting is on" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                followers_setting: true,
                email_ccs_setting: false
              ).apply
              @ticket.save
            end

            it "returns collaborators with CollaboratorType.FOLLOWER type" do
              assert_equal 1, @ticket.followers.size
              follower_collaborations = @ticket.collaborations.where(collaborator_type: CollaboratorType.FOLLOWER)
              assert_no_difference_between_arrays follower_collaborations.map(&:user_id), @ticket.followers.map(&:id)
            end

            describe "when there are FOLLOWER collaborations marked for deletion" do
              let(:new_follower_user) { users(:minimum_admin_not_owner) }

              before { @ticket.set_collaborators_with_type([new_follower_user.id], CollaboratorType.FOLLOWER) }

              it "does not include the removed collaborator" do
                @ticket.followers.must_equal [new_follower_user]
              end
            end
          end
        end
      end

      describe "with LEGACY_CC, EMAIL_CC and FOLLOWER collaborations with same user to check de-duplication" do
        before do
          @ticket = tickets(:minimum_1)
          agent_id = @ticket.account.agents.last.id
          types = [CollaboratorType.LEGACY_CC, CollaboratorType.FOLLOWER, CollaboratorType.EMAIL_CC]

          types.each do |collaborator_type|
            @ticket.collaborations.create(collaborator_type: collaborator_type, user_id: agent_id, account_id: @ticket.account.id)
          end
        end

        describe "when follower_and_email_cc_collaborations setting is off" do
          [
            CollaborationSettingsTestHelper.new(
              feature_arturo: false,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            ),
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            )
          ].each do |scenario|
            describe scenario.to_s do
              it "returns unique users" do
                scenario.apply
                ticket = Ticket.find(@ticket.id)
                assert_unique_objects ticket.followers
              end
            end
          end
        end

        describe "when follower_and_email_cc_collaborations setting is on" do
          describe "when ticket_followers_allowed setting is off" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                followers_setting: false,
                email_ccs_setting: false
              ).apply
            end

            it "returns unique users" do
              ticket = Ticket.find(@ticket.id)
              assert_unique_objects ticket.followers
            end
          end

          describe "when ticket_followers_allowed setting is on" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                followers_setting: true,
                email_ccs_setting: false
              ).apply
            end

            it "returns unique users" do
              ticket = Ticket.find(@ticket.id)
              assert_unique_objects ticket.followers
            end
          end
        end
      end

      describe "when the CCs/Followers master setting is enabled" do
        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: false,
            email_ccs_setting: false
          ).apply
        end

        it "passes the users_preloaded parameter value to the LegacyTypeTransformer" do
          Collaboration::LegacyTypeTransformer.expects(:followers).with(
            collaborations: @ticket.send(:in_flight_collaborations),
            settings: @ticket.follower_and_email_cc_settings,
            account_id: @ticket.account_id,
            users_preloaded: true
          )
          ticket.followers(users_preloaded: true)
        end
      end
    end

    describe "#email_ccs" do
      describe "with LEGACY_CC collaborations" do
        before do
          @ticket.collaborations.build(
            collaborator_type: CollaboratorType.LEGACY_CC,
            user_id: @ticket.account.agents.first.id,
            account_id: @ticket.account.id
          )
          @ticket.collaborations.build(
            collaborator_type: CollaboratorType.LEGACY_CC,
            user_id: @ticket.account.end_users.first.id,
            account_id: @ticket.account.id
          )
          @ticket.description = "blah"
          @ticket.requester = users(:minimum_end_user2)
          @ticket.will_be_saved_by(@ticket.requester, via_id: 4)
          @ticket.save
        end

        describe "when comment_email_ccs_allowed setting is off" do
          [
            CollaborationSettingsTestHelper.new(
              feature_arturo: false,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            ),
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            ),
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: true,
              followers_setting: false,
              email_ccs_setting: false
            )
          ].each do |scenario|
            describe scenario.to_s do
              it "returns an empty array" do
                ticket = Ticket.find(@ticket.id)
                scenario.apply
                assert_equal [], ticket.email_ccs
              end
            end
          end
        end

        describe "when comment_email_ccs_allowed setting is on" do
          before do
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: true,
              followers_setting: true,
              email_ccs_setting: true
            ).apply
          end

          it "returns collaborators that are not agents" do
            assert_equal 1, @ticket.email_ccs.size
            assert(@ticket.email_ccs.first.is_end_user?)
          end
        end
      end

      describe "with EMAIL_CC and FOLLOWER collaborations" do
        let(:email_cc_user) { users(:minimum_end_user) }

        before do
          @ticket = tickets(:minimum_1)
          @follower_collaboration_user_id = @ticket.account.agents.last.id
          @email_cc_collaboration_user_id = email_cc_user.id

          @ticket.collaborations.create(
            collaborator_type: CollaboratorType.FOLLOWER,
            user_id: @follower_collaboration_user_id,
            account_id: @ticket.account.id
          )
          @ticket.collaborations.create(
            collaborator_type: CollaboratorType.EMAIL_CC,
            user_id: @email_cc_collaboration_user_id,
            account_id: @ticket.account.id
          )

          @ticket.description = "blah"
          @ticket.requester = users(:minimum_end_user2)
          @ticket.will_be_saved_by(@ticket.requester, via_id: 4)
        end

        describe "when comment_email_ccs_allowed setting is off" do
          [
            CollaborationSettingsTestHelper.new(
              feature_arturo: false,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            ),
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            ),
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: true,
              followers_setting: false,
              email_ccs_setting: false
            )
          ].each do |scenario|
            describe scenario.to_s do
              before do
                scenario.apply
                @ticket.save
                @reloaded_ticket = Ticket.find(@ticket.id)
              end

              it "returns an empty array" do
                assert_empty @reloaded_ticket.email_ccs
              end
            end
          end
        end

        describe "when comment_email_ccs_allowed setting is on" do
          before do
            scenario = CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: true,
              followers_setting: false,
              email_ccs_setting: true
            )
            scenario.apply
            @ticket.save
            @reloaded_ticket = Ticket.find(@ticket.id)
          end

          it "returns collaborators with CollaboratorType.EMAIL_CC type" do
            assert_equal 1, @reloaded_ticket.email_ccs.size
            assert_equal @email_cc_collaboration_user_id, @reloaded_ticket.email_ccs.first.id
          end

          describe "when there are EMAIL_CC collaborations marked for deletion" do
            let(:new_email_cc_user) { users(:minimum_search_user) }

            before { @reloaded_ticket.set_collaborators_with_type([new_email_cc_user.id], CollaboratorType.EMAIL_CC) }

            it "does not include the removed collaborator" do
              @reloaded_ticket.email_ccs.must_equal [new_email_cc_user]
            end
          end

          describe "with users that should be filtered" do
            let(:new_email_cc_user) { users(:minimum_search_user) }
            let(:filtered_user) do
              user = users(:minimum_end_user2)
              user.email = 'noreply@z3nmail.com'
              user.identities.first.mark_as_machine
              user.save!
              user
            end
            let(:suspended_user) do
              user = users(:minimum_author)
              user.suspend!
              user
            end
            let(:example_user) do
              user = users(:minimum_admin)
              user.identities.each(&:mark_as_reserved_example)
              user.save!
              user
            end
            let(:cc_users) { [new_email_cc_user, filtered_user, suspended_user, example_user] }

            before { @reloaded_ticket.set_collaborators_with_type(cc_users.map(&:id), CollaboratorType.EMAIL_CC) }

            describe "when filter_for_sending is false" do
              it "includes all collaborators" do
                @reloaded_ticket.email_ccs.must_equal cc_users.sort_by(&:name)
              end
            end

            describe "when filter_for_sending is true" do
              it "does not include the users that should be suppressed" do
                @reloaded_ticket.email_ccs(filter_for_sending: true).must_equal [new_email_cc_user]
              end
            end
          end
        end
      end

      describe "with LEGACY_CC, EMAIL_CC and FOLLOWER collaborations with same user to check de-duplication" do
        before do
          @ticket = tickets(:minimum_1)
          agent_id = @ticket.account.agents.last.id
          types = [CollaboratorType.LEGACY_CC, CollaboratorType.FOLLOWER, CollaboratorType.EMAIL_CC]

          types.each do |collaborator_type|
            @ticket.collaborations.create(collaborator_type: collaborator_type, user_id: agent_id, account_id: @ticket.account.id)
          end
        end

        describe "when follower_and_email_cc_collaborations setting is off" do
          [
            CollaborationSettingsTestHelper.new(
              feature_arturo: false,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            ),
            CollaborationSettingsTestHelper.new(
              feature_arturo: true,
              feature_setting: false,
              followers_setting: false,
              email_ccs_setting: false
            )
          ].each do |scenario|
            describe scenario.to_s do
              it "returns unique users" do
                scenario.apply
                ticket = Ticket.find(@ticket.id)
                assert_unique_objects ticket.email_ccs
              end
            end
          end
        end

        describe "when follower_and_email_cc_collaborations setting is on" do
          describe "when ticket_followers_allowed setting is off" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                followers_setting: false,
                email_ccs_setting: false
              ).apply
            end

            it "returns unique users" do
              ticket = Ticket.find(@ticket.id)
              assert_unique_objects ticket.email_ccs
            end
          end

          describe "when ticket_followers_allowed setting is on" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                followers_setting: true,
                email_ccs_setting: false
              ).apply
            end

            it "returns unique users" do
              ticket = Ticket.find(@ticket.id)
              assert_unique_objects ticket.email_ccs
            end
          end
        end
      end

      describe "when the CCs/Followers master setting is enabled" do
        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: false,
            email_ccs_setting: false
          ).apply
        end

        it "passes the users_preloaded parameter value to the LegacyTypeTransformer" do
          Collaboration::LegacyTypeTransformer.expects(:email_ccs).with(
            collaborations: @ticket.send(:in_flight_collaborations),
            settings: @ticket.follower_and_email_cc_settings,
            account_id: @ticket.account_id,
            users_preloaded: true
          )
          ticket.email_ccs(users_preloaded: true)
        end
      end
    end

    describe "#collaborators" do
      describe "with LEGACY_CC, EMAIL_CC and FOLLOWER collaborations with same user to check de-duplication" do
        before do
          @ticket = tickets(:minimum_1)
          agent_id = @ticket.account.agents.last.id
          types = [CollaboratorType.LEGACY_CC, CollaboratorType.FOLLOWER, CollaboratorType.EMAIL_CC]

          types.each do |collaborator_type|
            @ticket.collaborations.create(collaborator_type: collaborator_type, user_id: agent_id, account_id: @ticket.account.id)
          end
        end

        it "returns unique users" do
          assert_unique_objects @ticket.collaborators
        end
      end
    end
  end

  describe '#is_email_cc?' do
    let(:user)               { users(:minimum_agent) }
    let(:ticket)             { tickets(:minimum_1) }
    let(:collaboration_type) { CollaboratorType.LEGACY_CC }

    let(:collaboration) do
      Collaboration.new(
        collaborator_type: collaboration_type,
        user: user
      )
    end

    before { ticket.collaborations << collaboration }

    describe 'when the account has `comment_email_ccs_allowed` (and dependent settings) enabled' do
      before do
        CollaborationSettingsTestHelper.new(
          feature_arturo: true,
          feature_setting: true,
          email_ccs_setting: true
        ).apply
      end

      describe 'and the collaboration type is `EMAIL_CC`' do
        let(:collaboration_type) { CollaboratorType.EMAIL_CC }

        describe 'and the user is a collaborator on the ticket' do
          describe 'and the user has public comment access' do
            let(:user) { users(:minimum_agent) }

            it 'returns true' do
              assert ticket.is_email_cc?(user)
            end
          end

          describe_with_arturo_disabled :email_ccs_light_agents_v2 do
            describe 'and the user does not have public comment access' do
              let(:user) { create_light_agent }

              it 'returns false' do
                refute ticket.is_email_cc?(user)
              end
            end
          end

          describe_with_arturo_enabled :email_ccs_light_agents_v2 do
            describe_with_arturo_setting_disabled :light_agent_email_ccs_allowed do
              describe 'and the user does not have public comment access' do
                let(:user) { create_light_agent }

                it 'returns false' do
                  refute ticket.is_email_cc?(user)
                end
              end
            end

            describe_with_arturo_setting_enabled :light_agent_email_ccs_allowed do
              describe 'and the user does not have public comment access' do
                let(:user) { create_light_agent }

                it 'returns true' do
                  assert ticket.is_email_cc?(user)
                end
              end
            end
          end
        end

        describe 'and the user is not a collaborator on the ticket' do
          it 'returns false' do
            refute ticket.is_email_cc?(users(:minimum_admin))
          end
        end
      end

      describe 'and the collaboration type is `LEGACY_CC`' do
        let(:collaboration_type) { CollaboratorType.LEGACY_CC }

        describe 'and the user is a collaborator on the ticket' do
          describe 'and the user is not an agent' do
            let(:user) { users(:minimum_end_user) }

            it 'returns true' do
              assert ticket.is_email_cc?(user)
            end
          end

          describe 'and the user is an agent' do
            it 'returns false' do
              refute ticket.is_email_cc?(user)
            end
          end
        end

        describe 'and the user is not a collaborator on the ticket' do
          it 'returns false' do
            refute ticket.is_email_cc?(users(:minimum_admin))
          end
        end
      end

      describe 'and the collaboration type is `FOLLOWER`' do
        let(:collaboration_type) { CollaboratorType.FOLLOWER }

        it 'returns false' do
          refute ticket.is_email_cc?(user)
        end
      end

      describe 'and user is both follower and CC' do
        let(:collaboration_type) { CollaboratorType.EMAIL_CC }
        let(:follower_type)      { CollaboratorType.FOLLOWER }

        let(:follower_collaboration) do
          Collaboration.new(collaborator_type: follower_type, user: user)
        end

        before { ticket.collaborations << follower_collaboration }

        it 'returns true' do
          assert ticket.is_email_cc?(user)
        end
      end
    end

    describe 'when the account has `follower_and_email_cc_collaborations` disabled' do
      before do
        CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
      end

      it 'returns false' do
        refute ticket.is_email_cc?(user)
      end
    end
  end

  describe "#is_follower?" do
    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      before do
        @user = users(:with_groups_agent1)
        collaboration = Object.new
        collaboration.stubs(:user_id).returns(@user.id)
        @ticket = tickets(:with_groups_1)
        @ticket.stubs(:collaborations).returns([collaboration])
      end

      it "does not depend on an agent's particular name" do
        @user.update_attributes(name: 'Henry Otheruser')
        assert @ticket.is_follower?(@user)
      end
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      before do
        @ticket = tickets(:minimum_1)
        @agent = users(:minimum_agent)
        setup_collaborations(@ticket, @agent)
      end

      describe_with_arturo_setting_disabled :ticket_followers_allowed do
        it { refute @ticket.is_follower?(@agent) }
      end

      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        it "is true for a user that has only a follower collaboration on the ticket" do
          assert @ticket.is_follower?(@agent)
        end

        it "is true for an agent that has both follower and email CC collaborations on the ticket" do
          assert @ticket.is_follower?(users(:minimum_admin))
        end

        it "is false for a user that only has an email CC collaboration on the ticket" do
          refute @ticket.is_follower?(users(:minimum_end_user))
        end

        it "is false for an end user that only has a legacy CC collaboration on the ticket" do
          refute @ticket.is_follower?(users(:minimum_search_user))
        end

        it "is true for an agent that only has a legacy CC collaboration on the ticket" do
          assert @ticket.is_follower?(users(:minimum_admin_not_owner))
        end

        it "is false for an end user that has a follower collaboration on the ticket (downgraded agent case)" do
          downgraded_agent = users(:minimum_agent)
          assert @ticket.is_follower?(downgraded_agent)
          downgraded_agent.roles = Role::END_USER.id
          downgraded_agent.save!
          refute @ticket.is_follower?(downgraded_agent)
        end
      end
    end
  end

  describe "#is_participant?" do
    before do
      @ticket = tickets(:minimum_1)
      @agent = users(:minimum_agent)
      setup_collaborations(@ticket, @agent, false)
    end

    it "is true for the requester of the ticket" do
      assert @ticket.is_participant?(users(:minimum_author))
    end

    it "is true for the assignee of the ticket" do
      assert @ticket.is_participant?(@agent)
    end

    it "is true for a legacy CC collaborator" do
      assert @ticket.is_participant?(users(:minimum_search_user))
    end

    it "is true for a follower collaborator" do
      assert @ticket.is_participant?(users(:minimum_admin))
    end

    it "is true for an email CC collaborator" do
      assert @ticket.is_participant?(users(:minimum_end_user))
    end

    it "is false for a user that is not requester, assignee, or collaborator" do
      refute @ticket.is_participant?(users(:minimum_end_user2))
    end
  end

  describe "#is_collaborator?" do
    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it "is true for a user that has a persisted collaboration on the ticket" do
        end_user = users(:minimum_end_user)

        @ticket = tickets(:minimum_1)
        build_collaborations_with_collaborator_type(@ticket, [end_user], CollaboratorType.LEGACY_CC)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!

        end_user.expects(:new_record?).never
        Collaboration.any_instance.expects(:new_record?).never
        assert @ticket.is_collaborator?(end_user)
      end

      describe "with existing collaborations" do
        before do
          @ticket = tickets(:minimum_1)
          @agent = users(:minimum_agent)
          setup_collaborations(@ticket, @agent)
        end

        it "is true for a user that has an unpersisted collaboration built on the ticket" do
          user = users(:minimum_end_user2)
          refute @ticket.is_collaborator?(user)
          @ticket.collaborations.build(ticket: @ticket, user: user, collaborator_type: CollaboratorType.LEGACY_CC)
          Collaboration.any_instance.expects(:new_record?).at_least(1).returns(true)
          assert @ticket.is_collaborator?(user)
        end

        it "is true for collaborations with any collaborator_type" do
          assert @ticket.is_collaborator?(@agent)
          assert @ticket.is_collaborator?(users(:minimum_end_user))
          assert @ticket.is_collaborator?(users(:minimum_search_user))
        end
      end
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe "when collaborator_type is nil" do
        it "is true for a user that has a persisted collaboration on the ticket" do
          end_user = users(:minimum_end_user)

          @ticket = tickets(:minimum_1)
          build_collaborations_with_collaborator_type(@ticket, [end_user], CollaboratorType.EMAIL_CC)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          end_user.expects(:new_record?).never
          Collaboration.any_instance.expects(:new_record?).never
          assert @ticket.is_collaborator?(end_user)
        end

        describe "with existing collaborations" do
          before do
            @ticket = tickets(:minimum_1)
            @agent = users(:minimum_agent)
            setup_collaborations(@ticket, @agent)
          end

          it "is true for a user that has an unpersisted collaboration built on the ticket" do
            user = users(:minimum_end_user2)
            refute @ticket.is_collaborator?(user)
            @ticket.collaborations.build(ticket: @ticket, user: user, collaborator_type: CollaboratorType.EMAIL_CC)
            Collaboration.any_instance.expects(:new_record?).at_least(1).returns(true)
            assert @ticket.is_collaborator?(user)
          end

          it "does not check the collaborator_type of collaborations" do
            Collaboration.any_instance.expects(:collaborator_type).never
            @ticket.is_collaborator?(users(:minimum_end_user))
            @ticket.is_collaborator?(users(:minimum_search_user))
          end
        end
      end

      describe "when collaborator_type is specified" do
        before do
          @ticket = tickets(:minimum_1)
          @agent = users(:minimum_agent)
          setup_collaborations(@ticket, @agent)
        end

        it "is true when a collaboration exists for the user with the given type" do
          assert @ticket.is_collaborator?(users(:minimum_end_user), CollaboratorType.EMAIL_CC)
        end

        it "is true when the user has multiple collaborations on the ticket, including one with the given type" do
          assert @ticket.is_collaborator?(users(:minimum_admin), CollaboratorType.EMAIL_CC)
          assert @ticket.is_collaborator?(users(:minimum_admin), CollaboratorType.FOLLOWER)
        end

        it "is false when the user has no collaborations on the ticket with the given type" do
          refute @ticket.is_collaborator?(users(:minimum_end_user), CollaboratorType.FOLLOWER)
        end
      end
    end
  end

  describe "#email_cc_addresses" do
    before do
      @ticket = tickets(:minimum_1)
      @agent = users(:minimum_agent)
      setup_collaborations(@ticket, @agent)
    end

    describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
      it "returns an empty list" do
        @ticket.email_cc_addresses.must_equal []
      end
    end

    describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
      it "uses in_flight_collaborations" do
        @ticket.expects(:in_flight_collaborations).returns([])
        @ticket.email_cc_addresses
      end

      it "returns the main email addresses of the ticket's email CC users if they have one" do
        @ticket.email_cc_addresses.sort.must_equal [
          "minimum_end_user@aghassipour.com",
          "minimum_admin@aghassipour.com",
          "minimum_search_user@aghassipour.com"
        ].sort
      end

      it "includes a user with multiple collaborations only once" do
        admin = users(:minimum_admin)
        assert @ticket.collaborations.count { |collaboration| collaboration.user_id == admin.id } > 1
        assert_equal 1, (@ticket.email_cc_addresses.count { |address| address == admin.email })
      end
    end
  end

  describe "#follower_addresses" do
    before do
      @ticket = tickets(:minimum_1)
      @agent = users(:minimum_agent)
      setup_collaborations(@ticket, @agent)
    end

    describe_with_arturo_setting_disabled :ticket_followers_allowed do
      it "returns an empty list" do
        @ticket.send(:follower_addresses).must_equal []
      end
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        it "uses in_flight_collaborations" do
          @ticket.expects(:in_flight_collaborations).returns([])
          @ticket.send(:follower_addresses)
        end

        it "returns the main email addresses of the ticket's follower users if they have one" do
          @ticket.send(:follower_addresses).sort.must_equal [
            "minimum_agent@aghassipour.com",
            "minimum_admin@aghassipour.com",
            "minimum_admin_not_owner@aghassipour.com"
          ].sort
        end

        it "includes a user with multiple collaborations only once" do
          admin = users(:minimum_admin)
          assert @ticket.collaborations.count { |collaboration| collaboration.user_id == admin.id } > 1
          assert_equal 1, (@ticket.send(:follower_addresses).count { |address| address == admin.email })
        end
      end
    end
  end

  describe "#cached_collaborators" do
    let(:email_cc) { users(:minimum_end_user) }
    let(:follower) { users(:minimum_agent) }
    let(:ticket) { tickets(:minimum_1) }
    let(:user_ids) { [] }

    let(:cached_collaborators) { ticket.send(:cached_collaborators, user_ids) }

    describe_with_arturo_enabled :email_ccs do
      describe "when user_ids is empty" do
        it { cached_collaborators.must_be_empty }
      end

      describe "when user_ids has an ID of a user" do
        let(:user_ids) { [email_cc.id] }

        it "queries for the ID" do
          assert_sql_queries 4 do
            cached_collaborators
          end
        end

        it { cached_collaborators.must_equal Hash[email_cc.id, email_cc] }

        describe "when the @email_cc_users_by_id contains the user" do
          before do
            ticket.instance_variable_set(:@email_cc_users_by_id, Hash[email_cc.id, email_cc])
          end

          describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
            it "does not query for the email CC ID" do
              assert_sql_queries 1 do
                cached_collaborators
              end
            end

            it { cached_collaborators.must_equal Hash[email_cc.id, email_cc] }
          end

          describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
            it "queries for the email CC ID" do
              assert_sql_queries 2 do
                cached_collaborators
              end
            end

            it { cached_collaborators.must_equal Hash[email_cc.id, email_cc] }
          end
        end

        describe "when the @follower_users_by_id contains the user" do
          let(:user_ids) { [follower.id] }

          before do
            ticket.instance_variable_set(:@follower_users_by_id, Hash[follower.id, follower])
          end

          describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
            it "does not query for the follower ID" do
              assert_sql_queries 1 do
                cached_collaborators
              end
            end

            it { cached_collaborators.must_equal Hash[follower.id, follower] }
          end

          describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
            it "queries for the follower ID" do
              assert_sql_queries 2 do
                cached_collaborators
              end
            end

            it { cached_collaborators.must_equal Hash[follower.id, follower] }
          end
        end
      end
    end

    describe_with_arturo_disabled :email_ccs do
      describe "when user_ids is empty" do
        it { cached_collaborators.must_be_empty }
      end

      describe "when user_ids has an ID of a user" do
        let(:user_ids) { [email_cc.id] }

        it "queries for the ID" do
          assert_sql_queries 4 do
            cached_collaborators
          end
        end

        it { cached_collaborators.must_equal Hash[email_cc.id, email_cc] }
      end
    end
  end

  describe "#update_current_collaborators" do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }
    let(:agent) { users(:minimum_agent) }
    let(:expected_current_collaborators) do
      "Admin Man <minimum_admin@aghassipour.com>, "\
        "Agent Minimum <minimum_agent@aghassipour.com>, Not owner man <minimum_admin_not_owner@aghassipour.com>, "\
        "minimum_end_user <minimum_end_user@aghassipour.com>, minimum_search_user <minimum_search_user@aghassipour.com>"
    end

    before { setup_collaborations(ticket, agent) }

    it "sets the current_collaborators attribute to the correct values and gets rid of duplicates" do
      ticket.send(:update_current_collaborators)
      ticket.current_collaborators.must_equal expected_current_collaborators
    end

    describe "when a collaboration's user is invalid" do
      let(:invalid_user) { users(:minimum_end_user) }

      before { invalid_user.update_attribute(:name, "a") }

      it "does not exclude the invalid user's info from the current_collaborators attribute" do
        ticket.send(:update_current_collaborators)
        ticket.current_collaborators.must_include invalid_user.email
      end
    end

    describe "with CCs/Followers settings enabled" do
      before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

      let(:expected_current_collaborators) do
        "Admin Man <minimum_admin@aghassipour.com>, Agent Minimum <minimum_agent@aghassipour.com>, Not owner man "\
        "<minimum_admin_not_owner@aghassipour.com>, minimum_end_user <minimum_end_user@aghassipour.com>, "\
        "minimum_search_user <minimum_search_user@aghassipour.com>"
      end

      it "sets the current_collaborators attribute to the correct values without duplicates" do
        ticket.send(:update_current_collaborators)
        ticket.current_collaborators.must_equal expected_current_collaborators
      end

      describe "when a collaboration's user is invalid" do
        let(:invalid_user) { users(:minimum_end_user) }

        before do
          invalid_user.identities.email.first.update_attribute(:value, "a")
        end

        it "excludes the invalid user's info from the current_collaborators attribute" do
          ticket.send(:update_current_collaborators)
          ticket.current_collaborators.wont_include "minimum_end_user"
        end
      end
    end
  end

  def add_comment_and_save(ticket)
    ticket.add_comment(body: 'new comment', is_public: true)
    ticket.will_be_saved_by(users(:minimum_author))
    ticket.save!
  end

  describe "#transform_collaborations" do
    it "is called when a ticket is saved" do
      Ticket.any_instance.expects(:transform_collaborations)
      ticket = tickets(:minimum_1)
      add_comment_and_save(ticket)
    end

    it "is called before #add_ticket_events_to_audit and #save_modified_collaborations when a ticket is saved" do
      before_save_sequence = sequence('before_save_sequence')

      ticket = tickets(:minimum_1)

      ticket.expects(:transform_collaborations).in_sequence(before_save_sequence)
      ticket.expects(:add_ticket_events_to_audit).in_sequence(before_save_sequence)
      ticket.expects(:save_modified_collaborations).in_sequence(before_save_sequence)

      add_comment_and_save(ticket)
    end

    describe_with_arturo_disabled :email_ccs_transform_collaborations do
      it "does not call Collaboration::LegacyTypeTransformer.transform" do
        Collaboration::LegacyTypeTransformer.expects(:transform).never
        tickets(:minimum_1).send(:transform_collaborations)
      end

      it "does not call remove_duplicate_collaborations" do
        Ticket.any_instance.expects(:remove_duplicate_collaborations).never
        tickets(:minimum_1).send(:transform_collaborations)
      end

      it "does not call mark_invalid_collaborations" do
        Ticket.any_instance.expects(:mark_invalid_collaborations).never
        tickets(:minimum_1).send(:transform_collaborations)
      end

      it "does not call limit_total_number_of_ccs" do
        Ticket.any_instance.expects(:limit_total_number_of_ccs).never
        tickets(:minimum_1).send(:transform_collaborations)
      end
    end

    describe_with_arturo_enabled :email_ccs_transform_collaborations do
      it "calls Collaboration::LegacyTypeTransformer.transform" do
        Collaboration::LegacyTypeTransformer.expects(:transform)
        tickets(:minimum_1).send(:transform_collaborations)
      end

      it "calls remove_duplicate_collaborations" do
        Ticket.any_instance.expects(:remove_duplicate_collaborations)
        tickets(:minimum_1).send(:transform_collaborations)
      end

      it "calls mark_invalid_collaborations" do
        Ticket.any_instance.expects(:mark_invalid_collaborations)
        tickets(:minimum_1).send(:transform_collaborations)
      end

      it "calls limit_total_number_of_ccs" do
        Ticket.any_instance.expects(:limit_total_number_of_ccs)
        tickets(:minimum_1).send(:transform_collaborations)
      end

      describe 'when ticket contains invalid collaborations' do
        let(:ticket)             { tickets(:minimum_1) }
        let(:collaboration_user) { users(:minimum_agent) }

        before do
          ticket.collaborations.create!(
            user: collaboration_user,
            collaborator_type: collaboration_type
          )
        end

        describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
          describe_with_arturo_setting_enabled :ticket_followers_allowed do
            before do
              ticket.will_be_saved_by(users(:minimum_author))
              ticket.save!
            end

            describe 'and collaboration type is `FOLLOWER`' do
              let(:collaboration_type) { CollaboratorType.FOLLOWER }

              describe 'and the user no longer exists' do
                let(:collaboration_user) { users(:minimum_agent) }

                before do
                  Collaboration.any_instance.stubs(user: nil)

                  ticket.save!
                end

                it 'removes them from collaborations' do
                  assert_empty ticket.reload.collaborations
                end
              end

              describe 'and an end-user exists as a follower' do
                let(:collaboration_user) { users(:minimum_end_user) }

                it 'removes them from collaborations' do
                  assert_empty ticket.reload.collaborations
                end
              end

              describe 'and an agent exists as a follower' do
                let(:collaboration_user) { users(:minimum_agent) }

                it 'does not remove them from collaborations' do
                  assert_equal(
                    [collaboration_user.id],
                    ticket.collaborations.map(&:user_id)
                  )
                end
              end
            end

            describe 'and collaboration type is `LEGACY_CC`' do
              let(:collaboration_type) { CollaboratorType.LEGACY_CC }

              describe 'and the collaborator is a end user' do
                let(:collaboration_user) { users(:minimum_end_user) }

                it 'does not remove them from collaborations' do
                  assert_equal(
                    [collaboration_user.id],
                    ticket.collaborations.map(&:user_id)
                  )
                end
              end

              describe 'and the collaborator is a agent' do
                let(:collaboration_user) { users(:minimum_agent) }

                it 'does not remove them from collaborations' do
                  assert_equal(
                    [collaboration_user.id],
                    ticket.collaborations.map(&:user_id)
                  )
                end
              end
            end
          end

          describe_with_arturo_setting_disabled :ticket_followers_allowed do
            describe 'and collaboration type is `FOLLOWER`' do
              let(:collaboration_type) { CollaboratorType.FOLLOWER }

              describe 'and an end-user exists as a follower' do
                let(:collaboration_user) { users(:minimum_end_user) }

                before do
                  ticket.will_be_saved_by(users(:minimum_author))
                  ticket.save!
                end

                it 'does not remove them from collaborations' do
                  assert_equal(
                    [collaboration_user.id],
                    ticket.collaborations.map(&:user_id)
                  )
                end
              end
            end
          end

          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            before do
              ticket.will_be_saved_by(users(:minimum_author))
              ticket.save!
            end

            describe 'and collaboration type is `EMAIL_CC`' do
              let(:collaboration_type) { CollaboratorType.EMAIL_CC }

              describe 'and the user no longer exists' do
                let(:collaboration_user) { users(:minimum_agent) }

                before do
                  Collaboration.any_instance.stubs(user: nil)

                  ticket.save!
                end

                it 'removes them from collaborations' do
                  assert_empty ticket.reload.collaborations
                end
              end

              describe 'and user without public comment access exists as CC' do
                let(:collaboration_user) { create_light_agent }

                it 'removes them from collaborations' do
                  assert_empty ticket.reload.collaborations
                end
              end

              describe 'and an agent exists as a CC' do
                let(:collaboration_user) { users(:minimum_agent) }

                it 'does not remove them from collaborations' do
                  assert_equal(
                    [collaboration_user.id],
                    ticket.collaborations.map(&:user_id)
                  )
                end
              end

              describe 'and an end user exists as a CC' do
                let(:collaboration_user) { users(:minimum_end_user) }

                it 'does not remove them from collaborations' do
                  assert_equal(
                    [collaboration_user.id],
                    ticket.collaborations.map(&:user_id)
                  )
                end
              end
            end

            describe 'and collaboration type is `LEGACY_CC`' do
              let(:collaboration_type) { CollaboratorType.LEGACY_CC }

              describe 'and the collaborator is a end user' do
                let(:collaboration_user) { users(:minimum_end_user) }

                it 'does not remove them from collaborations' do
                  assert_equal(
                    [collaboration_user.id],
                    ticket.collaborations.map(&:user_id)
                  )
                end
              end

              describe 'and the collaborator is a agent' do
                let(:collaboration_user) { users(:minimum_agent) }

                it 'does not remove them from collaborations' do
                  assert_equal(
                    [collaboration_user.id],
                    ticket.collaborations.map(&:user_id)
                  )
                end
              end
            end
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            describe 'and collaboration type is `EMAIL_CC`' do
              let(:collaboration_type) { CollaboratorType.EMAIL_CC }

              describe 'and user without public comment access exists as CC' do
                let(:collaboration_user) { create_light_agent }

                before do
                  ticket.will_be_saved_by(users(:minimum_author))
                  ticket.save!
                end

                it 'does not remove them from collaborations' do
                  assert_equal(
                    [collaboration_user.id],
                    ticket.collaborations.map(&:user_id)
                  )
                end
              end
            end
          end

          describe 'and collaboration does not have associated user' do
            let(:collaboration_type) { CollaboratorType.LEGACY_CC }
            let(:collaboration_user) { users(:minimum_agent) }

            before do
              ticket.will_be_saved_by(users(:minimum_author))
              ticket.save!

              Collaboration.any_instance.stubs(user: nil)
              ticket.save!
            end

            it 'removes it from collaborations' do
              assert_empty ticket.reload.collaborations
            end
          end
        end

        describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
          let(:collaboration_user) { users(:minimum_end_user) }
          let(:collaboration_type) { CollaboratorType.FOLLOWER }

          before do
            ticket.will_be_saved_by(users(:minimum_author))
            ticket.save!
          end

          it 'does not remove them from collaborations' do
            assert_equal(
              [collaboration_user.id],
              ticket.collaborations.map(&:user_id)
            )
          end
        end
      end

      describe 'when new collaborations are passed in' do
        describe 'when new email-CCs settings are enabled' do
          let(:ticket) { tickets(:minimum_1) }
          let(:user_1) { users(:minimum_agent) }

          before do
            CollaborationSettingsTestHelper.enable_all_new_collaboration_settings

            Ticket.any_instance.stubs(max_email_ccs_limit: 2)

            ticket.collaborations.create!(
              user: user_1,
              collaborator_type: CollaboratorType.EMAIL_CC
            )
          end

          describe "and adding new collaborations won't exceed the limit" do
            let(:user_2) { users(:minimum_end_user) }

            before do
              ticket.collaborations.build(
                user: user_2,
                collaborator_type: CollaboratorType.EMAIL_CC
              )

              ticket.will_be_saved_by(users(:minimum_author))
              ticket.save!
            end

            it 'adds all the collaborations to the ticket' do
              assert ticket.collaborations.map(&:user_id).include?(user_2.id)
            end
          end

          describe 'and there are max number of collaborations already on ticket' do
            let(:user_2) { users(:minimum_end_user) }

            before do
              Ticket.any_instance.stubs(max_email_ccs_limit: 1)

              ticket.collaborations.build(
                user: user_2,
                collaborator_type: CollaboratorType.EMAIL_CC
              )

              ticket.will_be_saved_by(users(:minimum_author))
              ticket.save!
            end

            it 'does not add any new collaborations to the ticket' do
              refute ticket.collaborations.map(&:user_id).include?(user_2.id)
            end
          end

          describe 'and adding all new collaborations exceeds the limit' do
            let(:user_2) { users(:minimum_end_user) }
            let(:user_3) { users(:minimum_end_user2) }

            before do
              Ticket.any_instance.stubs(max_email_ccs_limit: 2)

              ticket.collaborations.build(
                user: user_2,
                collaborator_type: CollaboratorType.EMAIL_CC
              )

              ticket.collaborations.build(
                user: user_3,
                collaborator_type: CollaboratorType.EMAIL_CC
              )

              ticket.will_be_saved_by(users(:minimum_author))
              ticket.save!
            end

            it 'only adds new collaborations until the limit is reached' do
              assert ticket.collaborations.map(&:user_id).include?(user_2.id)
            end

            it 'does not persist other collaboration after the limit' do
              refute ticket.collaborations.map(&:user_id).include?(user_3.id)
            end
          end
        end

        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          let(:user_2) { users(:minimum_end_user) }
          let(:user_3) { users(:minimum_end_user2) }

          before do
            Ticket.any_instance.stubs(max_email_ccs_limit: 1)

            ticket.collaborations.build(
              user: user_2,
              collaborator_type: CollaboratorType.EMAIL_CC
            )

            ticket.collaborations.build(
              user: user_3,
              collaborator_type: CollaboratorType.EMAIL_CC
            )

            ticket.will_be_saved_by(users(:minimum_author))
            ticket.save!
          end

          it 'adds all new collaborations' do
            assert_equal(
              [user_2, user_3].map(&:id),
              ticket.collaborations.map(&:user_id)
            )
          end
        end
      end
    end
  end

  describe '#invalid_email_ccs' do
    describe_with_arturo_enabled :email_ccs do
      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          let(:account) { accounts(:minimum) }
          let(:ticket) { tickets(:minimum_1) }
          let(:collaborator_type) { CollaboratorType.EMAIL_CC }
          let(:collaboration_user) { users(:minimum_end_user) }
          let(:user) { create_light_agent }

          def mark_invalid_collaborations
            @collaboration = ticket.collaborations.build(account: account, ticket: ticket, user: user, collaborator_type: collaborator_type)
            ticket.send(:mark_invalid_collaborations)
          end

          describe_with_arturo_setting_enabled :light_agent_email_ccs_allowed do
            it 'does not mark the light agent as an invalid email cc' do
              mark_invalid_collaborations
              ticket.instance_variable_get(:@collaborations_for_removal).must_be_empty
            end
          end

          describe_with_arturo_setting_disabled :light_agent_email_ccs_allowed do
            it 'marks the light agent as an invalid email cc' do
              mark_invalid_collaborations
              ticket.instance_variable_get(:@collaborations_for_removal).must_include @collaboration
            end
          end
        end
      end
    end
  end

  describe "#persist_new_email_cc_users" do
    it "is called when a ticket is saved" do
      Ticket.any_instance.expects(:persist_new_email_cc_users)
      ticket = tickets(:minimum_1)
      add_comment_and_save(ticket)
    end

    it "is called before #run_triggers_on_audit when a ticket is saved" do
      before_save_sequence = sequence('before_save_sequence')

      ticket = tickets(:minimum_1)

      ticket.expects(:persist_new_email_cc_users).in_sequence(before_save_sequence)
      ticket.expects(:run_triggers_on_audit).in_sequence(before_save_sequence)

      add_comment_and_save(ticket)
    end

    describe "when the ticket has collaborations with new users" do
      let(:email_cc_user) { User.new(account: account, name: "New Email CC") }
      let(:invalid_email_cc_user) { User.new(account: account) }
      let(:follower_user) { User.new(account: account, name: "New Follower") }
      let(:ticket) do
        t = tickets(:minimum_1)
        t.collaborations.build(ticket: t, user: email_cc_user, collaborator_type: CollaboratorType.EMAIL_CC)
        t.collaborations.build(ticket: t, user: invalid_email_cc_user, collaborator_type: CollaboratorType.EMAIL_CC)
        t.collaborations.build(ticket: t, user: follower_user, collaborator_type: CollaboratorType.FOLLOWER)
        t.collaborations
        t.will_be_saved_by(users(:minimum_agent))
        t
      end

      describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
        it "does not save the new email CC user" do
          ticket.send(:persist_new_email_cc_users)
          assert email_cc_user.new_record?
        end

        it "does not save the new follower user" do
          ticket.send(:persist_new_email_cc_users)
          assert follower_user.new_record?
        end
      end

      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        it "saves the new email CC user" do
          ticket.send(:persist_new_email_cc_users)
          refute email_cc_user.new_record?
        end

        it "does not save the new follower user" do
          ticket.send(:persist_new_email_cc_users)
          assert follower_user.new_record?
        end

        describe "with invalid users" do
          before do
            invalid_email_cc_user.expects(:save!).raises(ActiveRecord::RecordInvalid.new(invalid_email_cc_user))
          end

          it "does not save the new invalid email CC user" do
            ticket.send(:persist_new_email_cc_users)
            assert invalid_email_cc_user.new_record?
          end

          it "removes the new invalid email CC user from in_flight_collaborators" do
            ticket.send(:persist_new_email_cc_users)
            assert_empty (ticket.send(:in_flight_collaborations).select { |collaboration| invalid_email_cc_user == collaboration.user })
          end
        end
      end
    end
  end

  describe 'when saving a ticket with duplicate collaborations' do
    let(:ticket_collaborations) { ticket.collaborations }
    let(:persisted_timestamp)   { Date.new(2000, 1, 1) }
    let(:ticket)                { tickets(:minimum_1) }
    let(:type_legacy_cc)        { CollaboratorType.LEGACY_CC }
    let(:type_email_cc)         { CollaboratorType.EMAIL_CC }
    let(:type_follower)         { CollaboratorType.FOLLOWER }
    let(:user)                  { users(:minimum_admin) }

    describe_with_arturo_disabled :email_ccs_transform_collaborations do
      before do
        CollaborationSettingsTestHelper.disable_all_new_collaboration_settings

        [type_legacy_cc, type_email_cc, type_follower].each do |type|
          Collaboration.create(ticket: ticket, user: user, collaborator_type: type)
        end

        ticket.will_be_saved_by(user)
        ticket.save!
      end

      it 'preserves all duplicate collaborations' do
        assert_equal 3, ticket_collaborations.length
      end
    end

    describe_with_arturo_enabled :email_ccs_transform_collaborations do
      before do
        CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
      end

      describe 'and all the collaborations are of the same type' do
        describe 'and the duplicates are persisted' do
          before do
            Collaboration.
              create(ticket: ticket, user: user, collaborator_type: type_email_cc).
              update_column(:created_at, persisted_timestamp)

            Collaboration.create(ticket: ticket, user: user, collaborator_type: type_email_cc)

            ticket.will_be_saved_by(user)
            ticket.save!
          end

          it 'preserves only the earliest collaboration' do
            assert_equal([persisted_timestamp], ticket_collaborations.map(&:created_at))
          end
        end

        describe 'and the duplicates are in flight' do
          before do
            2.times do
              ticket.collaborations.build(user: user, ticket: ticket, collaborator_type: type_email_cc)
            end

            ticket.will_be_saved_by(user)
            ticket.save!
          end

          it 'preserves one collaboration' do
            assert_equal 1, ticket_collaborations.length
          end

          before_should 'log information about the removed duplicate' do
            Rails.logger.stubs(:info)

            Rails.logger.expects(:info).at_least_once.with do |message|
              message =~ /Removing duplicate non-persisted collaboration/
            end
          end
        end

        describe 'and there are persisted and in flight duplicates' do
          before do
            2.times do
              ticket.collaborations.build(user: user, ticket: ticket, collaborator_type: type_email_cc)
            end

            Collaboration.
              create(ticket: ticket, user: user, collaborator_type: type_email_cc).
              update_column(:created_at, persisted_timestamp)

            ticket.will_be_saved_by(user)
            ticket.save!
          end

          it 'preserves only the earliest collaboration' do
            assert_equal([persisted_timestamp], ticket_collaborations.map(&:created_at))
          end

          before_should 'log information about the removed duplicates' do
            Rails.logger.stubs(:info)

            Rails.logger.expects(:info).at_least_once.with do |message|
              message =~ /Removing duplicate non-persisted collaboration/
            end
          end
        end

        describe 'and the in flight collaborations are unpersisted users' do
          let(:unpersisted_user_1) { User.new(account: account, email: '1@email.com', name: '1') }
          let(:unpersisted_user_2) { User.new(account: account, email: '2@email.com', name: '2') }

          before do
            ticket.collaborations.build(user: unpersisted_user_1, ticket: ticket, collaborator_type: type_email_cc)
            ticket.collaborations.build(user: unpersisted_user_2, ticket: ticket, collaborator_type: type_email_cc)

            ticket.will_be_saved_by(user)
            ticket.save!
          end

          it 'does not mark unpersisted users as duplicates of each other' do
            assert_equal 2, ticket.collaborations.size
          end
        end

        describe "and multiple in-flight collaborations do not have an email identity" do
          let(:user_1) { User.new(account: account, name: '1') }
          let(:user_2) { User.new(account: account, name: '2') }

          before do
            ticket.collaborations.build(user: user_1, ticket: ticket, collaborator_type: type_email_cc)
            ticket.collaborations.build(user: user_2, ticket: ticket, collaborator_type: type_email_cc)
          end

          it 'does not delete collaborations' do
            previous_collaborations = ticket.collaborations
            ticket.send(:remove_duplicate_collaborations)
            (ticket.collaborations - previous_collaborations).must_be_empty
            ticket.collaborations.map(&:user).must_include user_2
          end
        end
      end

      describe 'and there is a mix of email CC and follower collaborations' do
        before do
          [type_email_cc, type_follower].each do |type|
            Collaboration.
              create(ticket: ticket, user: user, collaborator_type: type).
              update_column(:created_at, persisted_timestamp)
          end

          [type_email_cc, type_follower].each do |type|
            Collaboration.create(ticket: ticket, user: user, collaborator_type: type)
          end

          ticket.will_be_saved_by(user)
          ticket.save!
        end

        let(:email_cc_collaborations) do
          ticket_collaborations.select { |c| c.collaborator_type == type_email_cc }
        end

        let(:follower_collaborations) do
          ticket_collaborations.select { |c| c.collaborator_type == type_follower }
        end

        it 'preserves only the earliest CC collaboration for the user' do
          assert_equal([persisted_timestamp], email_cc_collaborations.map(&:created_at))
        end

        it 'preserves only the earliest follower collaboration for the user' do
          assert_equal([persisted_timestamp], follower_collaborations.map(&:created_at))
        end
      end
    end
  end

  describe "#save_modified_collaborations" do
    it "is called when a ticket is saved" do
      Ticket.any_instance.expects(:save_modified_collaborations)
      ticket = tickets(:minimum_1)
      add_comment_and_save(ticket)
    end

    describe "with existing collaborations" do
      before do
        @ticket = tickets(:minimum_1)
        @agent = users(:minimum_agent)
        setup_collaborations(@ticket, @agent)
      end

      describe_with_arturo_disabled :email_ccs_transform_collaborations do
        it "does not try to save collaborations" do
          Collaboration.any_instance.expects(:save!).never
          add_comment_and_save(@ticket)
        end
      end

      describe_with_arturo_enabled :email_ccs_transform_collaborations do
        it "calls save on the ticket's collaborations" do
          Collaboration.any_instance.expects(:save!).at_least_once
          add_comment_and_save(@ticket)
        end

        it "rescues StandardErrors" do
          Collaboration.any_instance.stubs(:save!).raises(StandardError)
          add_comment_and_save(@ticket)
        end
      end
    end
  end

  describe "collaborations when follower_and_email_cc_collaborations setting changes" do
    before do
      @ticket = tickets(:minimum_1)
      @agent = users(:minimum_agent)
      setup_collaborations(@ticket, @agent)
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      it "changes legacy CC collaborations to followers or email CCs" do
        assert (@ticket.collaborations.any? { |c| c.collaborator_type == CollaboratorType.LEGACY_CC })
        add_comment_and_save(@ticket)
        refute (@ticket.collaborations.any? { |c| c.collaborator_type == CollaboratorType.LEGACY_CC })
      end
    end
  end

  describe 'when the ticket is updated with `email_ccs`' do
    let(:account)         { accounts(:minimum) }
    let(:cc_user)         { users(:minimum_agent) }
    let(:ticket_with_ccs) { tickets(:minimum_1) }

    let(:updated_ticket) do
      Zendesk::Tickets::Initializer.new(
        account,
        agent,
        ticket: {
          email_ccs: [{
            user_id: cc_user.id,
            user_email: 'example@zendesk-test.com'
          }]
        },
        ticket_object: ticket_with_ccs
      ).ticket
    end

    describe 'and the `follower_and_email_cc_collaborations` setting is enabled' do
      before do
        CollaborationSettingsTestHelper.new(
          feature_arturo: true,
          feature_setting: true,
          email_ccs_setting: true
        ).apply
      end

      describe 'and the agent can make public comments' do
        let(:agent) { users(:minimum_admin) }

        it 'modifies ccs on the ticket' do
          assert_equal [cc_user], updated_ticket.email_ccs
        end
      end

      describe 'and the agent cannot make public comments' do
        let(:agent) { create_light_agent }

        before do
          Account.any_instance.stubs(has_light_agents?: true)
        end

        it 'does not add any ccs on ticket' do
          updated_ticket.expects(:set_email_ccs=).never
        end

        it 'does not modify the ccs list on ticket' do
          assert_empty updated_ticket.email_ccs
        end
      end
    end

    describe 'when `follower_and_email_cc_collaborations` is disabled' do
      let(:agent) { users(:minimum_admin) }

      before do
        CollaborationSettingsTestHelper.
          disable_all_new_collaboration_settings
      end

      it 'does not attempt to update the ccs on ticket' do
        ticket_with_ccs.expects(:set_email_ccs=).never
      end
    end
  end

  describe "collaboration placeholders" do
    describe 'when follower_and_email_cc_collaborations is disabled' do
      before { CollaborationSettingsTestHelper.disable_all_new_collaboration_settings }

      describe "and the emails are not included in the placeholder content" do
        before do
          @agent = users(:minimum_agent)

          @end_user = users(:minimum_end_user)
          @end_user.name = 'User Minuscule <user@minuscule.com>'
          @end_user.save

          @ticket = tickets(:minimum_1)
          @ticket.set_collaborators = [@agent.id, @end_user.id]
          @ticket.will_be_saved_by(@agent)
          @ticket.save!
        end

        describe '#cc_names' do
          # Two LEGACY_CCs collaborators: Agent and End-User
          it { assert_equal 'Agent Minimum, User Minuscule', @ticket.cc_names }
        end

        describe '#email_cc_names' do
          # Like `Ticket#email_ccs` (and the API), email_cc_names is not populated when the CCs feature is off
          it { assert_equal '', @ticket.email_cc_names }
        end

        describe '#follower_names' do
          # follower_names is empty when the CCs feature is off
          it { assert_equal '', @ticket.follower_names }
        end
      end
    end

    describe 'when the new collaboration is enabled' do
      before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

      before do
        @agent = users(:minimum_agent)

        @end_user = users(:minimum_end_user)
        @end_user.name = 'User Minuscule <user@minuscule.com>'
        @end_user.save

        @ticket = tickets(:minimum_1)
        build_collaborations_with_collaborator_type(@ticket, [@agent, @end_user], CollaboratorType.LEGACY_CC)
        @ticket.will_be_saved_by(@agent)
        @ticket.save!
      end

      describe "#cc_names" do
        it { assert_equal 'User Minuscule', @ticket.cc_names }
      end

      describe '#email_cc_names' do
        it { assert_equal 'User Minuscule', @ticket.email_cc_names }
      end

      describe '#follower_names' do
        it { assert_equal 'Agent Minimum', @ticket.follower_names }
      end
    end
  end

  describe "#notify_followers" do
    describe 'notifying followers when the ticket audit is not valid' do
      let(:ticket) { tickets(:minimum_1) }

      before do
        CollaborationSettingsTestHelper.enable_all_new_collaboration_settings

        # Destroy all triggers to try and avoid multiple notification events
        # being created for the user.
        ticket.account.triggers.destroy_all

        # This will initialize an audit on the ticket
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.add_comment(body: 'abc', is_public: true)
        ticket.stubs(followers: [users(:minimum_agent)])

        audit = ticket.instance_variable_get('@audit')
        audit.stubs(valid?: false)
      end

      it 'does not create a notification event' do
        assert_no_difference('FollowerNotification.count(:all)') do
          ticket.save!
        end
      end
    end

    describe "reloading collaborations" do
      let(:account) { accounts(:minimum) }
      let(:ticket) { tickets(:minimum_1) }
      let(:cc) { users(:minimum_end_user) }
      let(:agent) { users(:minimum_agent) }

      subject { ticket.notify_followers }

      before do
        build_collaborations_with_collaborator_type(ticket, [cc], CollaboratorType.LEGACY_CC)
        ticket.will_be_saved_by(agent)
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        it "reloads collaborations" do
          ticket.collaborations.expects(:reload)
          ticket.collaborators.expects(:reload).never
          subject
        end
      end

      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        describe_with_arturo_enabled :email_ccs_transform_collaborations do
          it "reloads collaborations" do
            ticket.collaborations.expects(:reload)
            subject
          end

          it "reloads collaborators" do
            ticket.collaborators.expects(:reload)
            subject
          end
        end

        describe_with_arturo_disabled :email_ccs_transform_collaborations do
          describe "when collaborators reload is successful" do
            it "reloads collaborators" do
              ticket.collaborations.expects(:reload).never
              ticket.collaborators.expects(:reload)
              subject
            end
          end

          describe "when collaborators reload raises" do
            before do
              ticket.collaborators.stubs(:reload).raises(ArgumentError.new)
            end

            it "does not raise" do
              subject
            end
          end
        end
      end
    end
  end

  describe "#mark_invalid_collaborations" do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }
    let(:collaborator_type) { CollaboratorType.LEGACY_CC }

    describe "when there is a persisted collaboration with an invalid user" do
      let(:invalid_user) { users(:minimum_end_user) }

      before do
        @collaboration = ticket.collaborations.build(account: account, ticket: ticket, user: invalid_user, collaborator_type: collaborator_type)
        ticket.will_be_saved_by(ticket.requester)
        ticket.save!

        UserEmailIdentity.any_instance.stubs(value: "foo")
      end

      it "adds the collaboration to the collaborations_for_removal instance variable" do
        ticket.send(:mark_invalid_collaborations)
        ticket.instance_variable_get(:@collaborations_for_removal).must_include @collaboration
      end
    end

    describe "when there is an unpersisted collaboration with an invalid user" do
      let(:invalid_user) { users(:minimum_end_user) }

      before do
        @collaboration = ticket.collaborations.build(account: account, ticket: ticket, user: invalid_user, collaborator_type: collaborator_type)
        UserEmailIdentity.any_instance.stubs(value: "foo")
      end

      it "removes the collaboration from the in-flight collaborations list on the ticket" do
        assert ticket.collaborations.include?(@collaboration)
        ticket.send(:mark_invalid_collaborations)
        refute ticket.collaborations.include?(@collaboration)
      end

      it "does not add the collaboration to the collaborations_for_removal instance variable" do
        ticket.send(:mark_invalid_collaborations)
        ticket.instance_variable_get(:@collaborations_for_removal).wont_include @collaboration
      end
    end

    describe "when there is a persisted collaboration with a deleted user" do
      let(:deleted_user) { users(:minimum_end_user) }

      before do
        @collaboration = ticket.collaborations.build(account: account, ticket: ticket, user: deleted_user, collaborator_type: collaborator_type)
        ticket.will_be_saved_by(ticket.requester)
        ticket.save!

        User.delete(deleted_user.id)
        Rails.cache.clear
      end

      it "adds the collaboration to the collaborations_for_removal instance variable" do
        ticket.send(:mark_invalid_collaborations)
        ticket.instance_variable_get(:@collaborations_for_removal).must_include @collaboration
      end
    end

    describe "when there is an unpersisted collaboration with a deleted user" do
      let(:deleted_user_id) { users(:minimum_end_user).id }

      before do
        User.delete(deleted_user_id)
        Rails.cache.clear

        @collaboration = ticket.collaborations.build(account: account, ticket: ticket, user_id: deleted_user_id, collaborator_type: collaborator_type)
      end

      it "removes the collaboration from the in-flight collaborations list on the ticket" do
        assert ticket.collaborations.include?(@collaboration)
        ticket.send(:mark_invalid_collaborations)
        refute ticket.collaborations.include?(@collaboration)
      end

      it "does not add the collaboration to the collaborations_for_removal instance variable" do
        ticket.send(:mark_invalid_collaborations)
        ticket.instance_variable_get(:@collaborations_for_removal).wont_include @collaboration
      end
    end

    describe "when a ticket with @collaborations_for_removal present is saved successfully " do
      before do
        ticket.set_collaborators = [agent.id, end_user.id]
        ticket.will_be_saved_by(agent)
        ticket.save!

        ticket.instance_variable_set(:@collaborations_for_removal, ticket.collaborations)
      end

      describe_with_arturo_enabled :email_ccs_clear_collaborations_for_removal do
        it "clears out the @collaborations_for_removal instance variable" do
          assert_equal 2, ticket.instance_variable_get(:@collaborations_for_removal).size

          ticket.save!

          assert_nil ticket.instance_variable_get(:@collaborations_for_removal)
        end
      end

      describe_with_arturo_disabled :email_ccs_clear_collaborations_for_removal do
        it "does not clear out the @collaborations_for_removal instance variable" do
          assert_equal 2, ticket.instance_variable_get(:@collaborations_for_removal).size

          ticket.save!

          assert_equal 2, ticket.instance_variable_get(:@collaborations_for_removal).size
        end
      end
    end
  end

  def assert_no_difference_between_arrays(array1, array2)
    result = Set.new(array1) ^ Set.new(array2)
    assert_equal 0, result.size
  end

  def assert_unique_objects(active_record_objects)
    id_count_map = active_record_objects.each_with_object({}) do |active_record_object, hsh|
      hsh[active_record_object.id] ||= 0
      hsh[active_record_object.id] += 1
    end
    counts_not_one = id_count_map.values.reject { |count| count == 1 }
    assert_empty counts_not_one
  end
end
