require_relative "../../support/test_helper"
require 'zendesk_stats'

SingleCov.not_covered!

describe 'TicketStats' do
  fixtures :users, :user_identities, :groups, :memberships, :organizations, :tickets, :accounts, :sequences, :subscriptions, :ticket_fields, :account_property_sets

  describe "With CSR stats" do
    before do
      @duration = 1.hour
      @time_stamp = Time.now.to_date - 10.days
      @account = accounts(:minimum)
      50.times do |i|
        v = i + 1
        @account.ticket_stats.create! agent_id: v, num: v, value: v - rand(v), ts: @time_stamp, duration: @duration, type: "csr"
      end
    end

    describe "lpf_csr, threshold of 10%" do
      it "gives me back 45 agents, sorted by rating" do
        # now we have 90 days limit for the duration in zendesk_stats gem
        @res = @account.ticket_stats.by_agent.lpf_csr(stat_name: "csr", start: 88.days.ago, threshold: 0.1)
        assert @res[:data]
        assert_equal 45, @res[:data].size

        0.upto(@res[:data].size - 1) do |i|
          if i > 0
            assert @res[:data][i].value <= @res[:data][i - 1].value
          end
        end

        assert_equal 5, @res[:csr_threshold]
      end
    end
  end

  describe "A ticket" do
    describe "being created" do
      before do
        @ts = 1.hour.ago
        Timecop.freeze @ts

        @end_user = users(:minimum_end_user)
        @ticket = accounts(:minimum).tickets.build requester: @end_user, description: "a ticket", via_id: 4
      end

      it "Stores a create and a user_comment event, and no audit event" do
        Zendesk::ClassicStats::TicketEvent.expects(:store).with do |h|
          ticket_id = Ticket.last.id
          if h[:type] == 'create'
            assert_equal({
              type: 'create', account_id: @ticket.account_id, shard_id: @ticket.account.shard_id,
              user_id: @end_user.id, ticket_id: ticket_id, ts: @ts.to_i,
              group_id: @ticket.account.groups.first.id, organization_id: @end_user.organization_id,
              via_id: 4, account_subdomain: @ticket.account.subdomain, ticket_status: @ticket.status,
              nice_id: @ticket.nice_id
            }, h)
          else
            assert_equal({
              type: 'user_comment', account_id: @ticket.account_id, shard_id: @ticket.account.shard_id,
              user_id: @end_user.id, ticket_id: ticket_id, is_public: true,
              ts: @ts.to_i, via_id: 4, comment_via_id: 4, ticket_status: @ticket.status,
              account_subdomain: @ticket.account.subdomain, nice_id: @ticket.nice_id
            }, h)
          end
          true
        end.twice
        @ticket.will_be_saved_by(users(:minimum_end_user))
        @ticket.save!
      end
    end

    describe "A ticket being created and solved at the same time" do
      before do
        @ts = 1.hour.ago
        Timecop.freeze @ts

        @end_user = users(:minimum_end_user)
        @agent = users(:minimum_agent)
        @ticket = accounts(:minimum).tickets.build requester: @end_user, assignee: @agent, description: "a ticket", via_id: 4
      end

      it "stores four events: create, comment, audit, solve " do
        @ticket.status_id = StatusType.SOLVED
        stored_solved_event = false
        stored_audit_event = false
        stored_create_event = false
        stored_comment_event = false
        Zendesk::ClassicStats::TicketEvent.expects(:store).at_least_once.with do |h|
          stored_solved_event ||= (h[:type] == "solve")
          stored_audit_event ||= (h[:type] == "audit")
          stored_create_event ||= (h[:type] == "create")
          stored_comment_event ||= (h[:type] == "user_comment")
          true
        end
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert stored_solved_event
        assert stored_audit_event
        assert stored_create_event
        assert stored_comment_event
      end
    end
    describe "A ticket being updated" do
      before do
        @ts = 1.hour.ago
        Timecop.freeze @ts

        @account = accounts(:minimum)
        @end_user = users(:minimum_end_user)
        @agent = users(:minimum_agent)
        @other_agent = users(:minimum_admin)
        @ticket = tickets(:minimum_2)
        @ticket.via_id = 4
      end

      it "Stores a user_comment event when an enduser comment is added, but no audit event" do
        @ticket.add_comment(body: 'foobar', is_public: true, via_id: 21)
        Zendesk::ClassicStats::TicketEvent.expects(:store).with(
          type: 'user_comment', user_id: @ticket.requester.id,
          ts: @ts.to_i, shard_id: @ticket.account.shard_id,
          is_public: true, ticket_id: @ticket.id, via_id: 4,
          account_id: @ticket.account_id, comment_via_id: 21,
          account_subdomain: @ticket.account.subdomain,
          ticket_status: @ticket.status,
          nice_id: @ticket.nice_id
        )

        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end

      it "Stores an agent_comment event, with an audit when an agent comment is added" do
        @ticket.assignee_id = @agent.id
        @ticket.will_be_saved_by(@agent)
        @ticket.save!

        @ticket.add_comment(body: 'foobar', is_public: true, via_id: 21)
        Zendesk::ClassicStats::TicketEvent.expects(:store).with(
          type: 'agent_comment', agent_id: @ticket.assignee.id,
          ts: @ts.to_i, shard_id: @ticket.account.shard_id,
          is_public: true, ticket_id: @ticket.id, via_id: 4,
          account_id: @ticket.account_id, comment_via_id: 21,
          account_subdomain: @ticket.account.subdomain,
          ticket_status: @ticket.status,
          nice_id: @ticket.nice_id
        )

        @ticket.will_be_saved_by(@agent)
        @audit = @ticket.audit
        Zendesk::ClassicStats::TicketEvent.expects(:store).with do |h|
          (h[:type] == 'audit' && h[:agent_id] == @audit.author.id && h[:ticket_id] == @ticket.id &&
           h[:ts] == @ts.to_i  && h[:via_id]   == 4 && h[:shard_id] == @ticket.account.shard_id &&
           h[:account_id] == @ticket.account_id) # We know everything but audit_id
        end
        @ticket.save!
      end

      it "Stores the comment author id in the audit event instead of the audit author id when they differ" do
        @ticket.assignee_id = @agent.id
        @ticket.will_be_saved_by(@agent)
        @ticket.save!

        @ticket.add_comment(body: 'foobar', is_public: true, via_id: 5, author_id: @other_agent.id)
        Zendesk::ClassicStats::TicketEvent.expects(:store).with(
          type: 'agent_comment', agent_id: @other_agent.id,
          ts: @ts.to_i, shard_id: @ticket.account.shard_id,
          account_id: @ticket.account_id, ticket_id: @ticket.id,
          via_id: 4, comment_via_id: 5, ticket_status: 'Open',
          is_public: true, account_subdomain: @ticket.account.subdomain,
          nice_id: @ticket.nice_id
        )

        @ticket.will_be_saved_by(@agent)
        @audit = @ticket.audit
        Zendesk::ClassicStats::TicketEvent.expects(:store).with do |h|
          (h[:type] == 'audit' && h[:agent_id] == @other_agent.id && h[:ticket_id] == @ticket.id &&
           h[:ts] == @ts.to_i  && h[:via_id]   == 4 && h[:shard_id] == @ticket.account.shard_id &&
           h[:account_id] == @ticket.account_id) # We know everything but audit_id
        end
        @ticket.save!
      end

      it "Should ignore private comments, but add an audit stat" do
        @ticket.assignee_id = @agent.id
        @ticket.will_be_saved_by(@agent)
        @ticket.save!

        @ticket.add_comment(body: 'foobar', is_public: false)

        @ticket.will_be_saved_by(@agent)
        @audit = @ticket.audit
        Zendesk::ClassicStats::TicketEvent.expects(:store).with do |h|
          (h[:type] == 'audit' && h[:agent_id] == @audit.author.id && h[:ticket_id] == @ticket.id &&
           h[:ts] == @ts.to_i  && h[:via_id]   == 4 && h[:shard_id] == @ticket.account.shard_id &&
           h[:account_id] == @ticket.account_id) # We know everything but audit_id
        end
        @ticket.save!
      end

      it "Stores an assigned event and audit when the ticket is re-assigned" do
        @ticket.assignee_id = @agent.id
        Zendesk::ClassicStats::TicketEvent.expects(:store).with(
          type: 'assign', agent_id: @ticket.assignee.id, group_id: @ticket.group_id,
          ts: @ts.to_i, shard_id: @ticket.account.shard_id,
          account_id: @ticket.account_id, ticket_id: @ticket.id, via_id: 4,
          account_subdomain: @ticket.account.subdomain,
          ticket_status: @ticket.status,
          nice_id: @ticket.nice_id
        )
        @ticket.will_be_saved_by(@agent)
        @audit = @ticket.audit
        Zendesk::ClassicStats::TicketEvent.expects(:store).with do |h|
          (h[:type] == 'audit' && h[:agent_id] == @audit.author.id && h[:ticket_id] == @ticket.id &&
           h[:ts] == @ts.to_i  && h[:via_id]   == 4 && h[:shard_id] == @ticket.account.shard_id &&
           h[:account_id] == @ticket.account_id) # We know everything but audit_id
        end
        @ticket.save!
      end

      it "Stores an assigned event and audit when the ticket's group changes (even if the assignee doesn't)" do
        @ticket.assignee_id = @agent.id
        @ticket.will_be_saved_by(@agent)
        @ticket.save!

        group = @account.groups.create! name: "Foo BAR GROUP"
        group.memberships.create! user: @ticket.assignee
        group.save!

        @ticket.group = group

        Zendesk::ClassicStats::TicketEvent.expects(:store).with(
          type: 'assign', agent_id: @ticket.assignee.id, group_id: group.id,
          ts: @ts.to_i, shard_id: @ticket.account.shard_id,
          account_id: @ticket.account_id, ticket_id: @ticket.id, via_id: 4,
          account_subdomain: @ticket.account.subdomain,
          ticket_status: @ticket.status,
          nice_id: @ticket.nice_id
        )
        @ticket.will_be_saved_by(@agent)
        @audit = @ticket.audit
        Zendesk::ClassicStats::TicketEvent.expects(:store).with do |h|
          (h[:type] == 'audit' && h[:agent_id] == @audit.author.id && h[:ticket_id] == @ticket.id &&
           h[:ts] == @ts.to_i  && h[:via_id]   == 4 && h[:shard_id] == @ticket.account.shard_id &&
           h[:account_id] == @ticket.account_id) # We know everything but audit_id
        end
        @ticket.save!
      end

      it "Stores a solve event when the ticket is solved" do
        @ticket.assignee_id = @agent.id
        @ticket.will_be_saved_by(@agent)
        @ticket.save!
        @ticket.status_id = StatusType.SOLVED
        Zendesk::ClassicStats::TicketEvent.expects(:store).with(
          type: 'solve', agent_id: @ticket.assignee.id, group_id: @ticket.group.id,
          ts: @ts.to_i, shard_id: @ticket.account.shard_id,
          account_id: @ticket.account_id, ticket_id: @ticket.id, via_id: 4,
          account_subdomain: @ticket.account.subdomain,
          ticket_status: @ticket.status,
          nice_id: @ticket.nice_id
        )
        @ticket.will_be_saved_by(@agent)
        @audit = @ticket.audit
        Zendesk::ClassicStats::TicketEvent.expects(:store).with do |h|
          (h[:type] == 'audit' && h[:agent_id] == @audit.author.id && h[:ticket_id] == @ticket.id &&
           h[:ts] == @ts.to_i  && h[:via_id]   == 4 && h[:shard_id] == @ticket.account.shard_id &&
           h[:account_id] == @ticket.account_id) # We know everything but audit_id
        end
        @ticket.save!
      end

      it "Stores a csr and no audit event when the ticket gets csr offered" do
        @ticket.satisfaction_score = SatisfactionType.GOOD
        Zendesk::ClassicStats::TicketEvent.expects(:store).with(
          type: 'csr', user_id: @ticket.requester.id, score: SatisfactionType.GOOD,
          ts: @ts.to_i, shard_id: @ticket.account.shard_id,
          account_id: @ticket.account_id, ticket_id: @ticket.id, via_id: 4,
          account_subdomain: @ticket.account.subdomain,
          ticket_status: @ticket.status,
          nice_id: @ticket.nice_id
        )
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end

      it "Tags events where the satisfaction probability is set" do
        Arturo.enable_feature!(:satisfaction_prediction)
        @ticket.satisfaction_probability = 0.9
        Zendesk::ClassicStats::TicketEvent.expects(:store).with do |h|
          (h[:type] == 'audit' && h[:satisfaction_probability_changed])
        end
        @ticket.will_be_saved_by(@agent)
        @ticket.save!
        Arturo.disable_feature!(:satisfaction_prediction)
      end

      it "Stores a close event when a ticket is closed" do
        @ticket.status_id = StatusType.CLOSED
        @ticket.will_be_saved_by(@agent)

        Zendesk::ClassicStats::TicketEvent.expects(:store).with(
          type: 'close', group_id: @ticket.group.id,
          ts: @ts.to_i, shard_id: @ticket.account.shard_id,
          account_id: @ticket.account_id, ticket_id: @ticket.id, via_id: 4,
          account_subdomain: @ticket.account.subdomain,
          ticket_status: @ticket.status,
          nice_id: @ticket.nice_id
        )

        @audit = @ticket.audit
        Zendesk::ClassicStats::TicketEvent.expects(:store).with do |h|
          (h[:type] == 'audit' && h[:agent_id] == @audit.author.id && h[:ticket_id] == @ticket.id &&
           h[:ts] == @ts.to_i  && h[:via_id]   == 4 && h[:shard_id] == @ticket.account.shard_id &&
           h[:account_id] == @ticket.account_id) # We know everything but audit_id
        end
        @ticket.save!
      end
    end
  end
end
