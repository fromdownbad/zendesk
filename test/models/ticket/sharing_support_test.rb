require_relative "../../support/test_helper"
require_relative "../../support/sharing_test_helper"

SingleCov.not_covered!

describe 'Ticket::Sharing' do
  include SharingTestHelper

  fixtures :accounts, :tickets, :users, :payments, :subscriptions, :remote_authentications, :groups, :memberships

  resque_inline false

  describe 'Given a ticket' do
    before do
      Resque.jobs.clear
      @ticket = tickets(:minimum_1)
    end

    describe 'When its status changes' do
      before do
        @ticket.status_id = StatusType.OPEN
        @ticket.will_be_saved_by(@ticket.assignee)
        @ticket.save!
      end

      it 'does not queue a resque job' do
        Sharing::TicketJob.expects(:enqueue).never
      end
    end
  end

  describe 'Given a shared ticket' do
    before do
      @ticket = tickets(:minimum_1)
      agreement = FactoryBot.create(:agreement, account: @ticket.account, status: :accepted)
      Timecop.freeze(10.minutes.ago) do # bypass the update enqueue delay
        @shared_ticket = @ticket.shared_tickets.create!(agreement: agreement)
      end

      assert @ticket.status_id != StatusType.OPEN
      assert !@ticket.shared_tickets.empty?
    end

    describe 'When its status changes' do
      before do
        @ticket.status_id = StatusType.OPEN
        @ticket.will_be_saved_by(@ticket.assignee)
        @ticket.save!
      end

      before_should 'enqueue a resque job ' do
        Sharing::TicketJob.expects(:enqueue).once.with(@shared_ticket.account_id, :update_partner, @shared_ticket.id)
      end
    end

    describe 'When its status changes via ticket sharing' do
      before do
        @ticket.status_id = StatusType.OPEN
        @ticket.will_be_saved_by(@ticket.assignee, via_id: ViaType.TICKET_SHARING)
        @ticket.save!
      end

      it 'does not enqueue a resque job' do
        Sharing::TicketJob.expects(:enqueue).never
      end
    end

    describe 'When its agreement is not active' do
      before do
        @shared_ticket.agreement.status = :declined
        @shared_ticket.agreement.save!
      end

      describe 'And its status changes' do
        before do
          @ticket.status_id = StatusType.OPEN
        end

        it 'does not enqueue a resque job' do
          Sharing::TicketJob.expects(:enqueue).never
          @ticket.will_be_saved_by(@ticket.assignee)
          @ticket.save!
        end
      end
    end

    describe 'When its agreement has an SSL Error' do
      before do
        @shared_ticket.agreement.status = :ssl_error
        @shared_ticket.agreement.save!
      end

      describe 'And its status changes' do
        before do
          @ticket.status_id = StatusType.OPEN
        end

        it 'does not enqueue a resque job' do
          Sharing::TicketJob.expects(:enqueue).never
          @ticket.will_be_saved_by(@ticket.assignee)
          @ticket.save!
        end
      end
    end

    describe 'When its agreement has been deleted' do
      before do
        @shared_ticket.agreement.destroy
        @shared_ticket.reload
        assert_nil(@shared_ticket.agreement)
      end

      describe 'And its status changes' do
        before do
          @ticket.status_id = StatusType.OPEN
        end

        it 'does not enqueue a resque job' do
          Sharing::TicketJob.expects(:enqueue).never
          @ticket.will_be_saved_by(@ticket.assignee)
          @ticket.save!
        end
      end
    end
  end

  describe 'Given a shared ticket' do
    before do
      account = accounts(:minimum)
      agreement = FactoryBot.create(:agreement, account: account)
      @ticket = tickets(:minimum_1)
      @ticket.shared_tickets.create!(agreement: agreement)
    end

    describe 'deleting that ticket' do
      it 'does not enqueue a partner update' do
        Sharing::TicketJob.expects(:enqueue).never
        @ticket.will_be_saved_by(@ticket.submitter)
        @ticket.soft_delete!
      end
    end
  end

  describe 'Given a ticket with two shared tickets' do
    before do
      @ticket = tickets(:minimum_1)
      @agreement1 = FactoryBot.create(:agreement, account: @ticket.account, status: :accepted)
      @agreement2 = FactoryBot.create(:agreement, account: @ticket.account, status: :accepted)
      @shared_ticket1 = @ticket.shared_tickets.create!(agreement: @agreement1)
      @shared_ticket2 = @ticket.shared_tickets.create!(agreement: @agreement2)
    end

    describe "an update from one" do
      it "enqueues a reshare only to the other" do
        ts_ticket = @shared_ticket1.send(:for_partner)
        ts_ticket.current_actor = new_ticket_sharing_actor('name' => 'Updater', 'role' => 'agent')
        ts_ticket.comments << new_ticket_sharing_comment('uuid' => 'new_comment_uuid',
                                                         'author' => ts_ticket.current_actor,
                                                         'body' => 'super awesome comment value')

        Sharing::TicketJob.expects(:enqueue).with(@ticket.account_id, :update_partner, @shared_ticket2.id).once
        @shared_ticket1.from_partner(ts_ticket)
      end
    end
  end

  describe 'Given an open shared ticket' do
    before do
      @ticket = tickets(:minimum_1)
      agreement = FactoryBot.create(:agreement, account: @ticket.account, status: :accepted)
      @shared_ticket = @ticket.shared_tickets.create!(agreement: agreement)

      # A priming save. When a ticket is first saved, some initial values are set.
      @ticket.status_id = StatusType.OPEN
      @ticket.will_be_saved_by(@ticket.assignee)
      @ticket.save!
      @ticket.reload
      assert_equal(StatusType.OPEN, @ticket.status_id)
    end

    describe 'When nothing changes' do
      it 'does not enqueue a partner update' do
        Sharing::TicketJob.expects(:enqueue).never
        @ticket.will_be_saved_by(@ticket.assignee)
        @ticket.save!
      end
    end

    describe 'When unshared fields change' do
      before do
        @ticket.assignee = users(:minimum_agent)
        @ticket.will_be_saved_by(@ticket.assignee)
        @ticket.group = @ticket.account.groups.first
        @ticket.save!
      end

      before_should 'not enqueue a partner update' do
        Sharing::TicketJob.expects(:enqueue).never
      end
    end

    describe 'when only the satisfaction probability changes' do
      before do
        @ticket.satisfaction_probability = 0.912819
        @ticket.will_be_saved_by(User.system)
        @ticket.save!
      end

      before_should 'not enqueue a partner update' do
        Sharing::TicketJob.expects(:enqueue).never
      end
    end

    describe 'when shared field and the satisfaction probability changes' do
      before do
        @ticket.status_id = StatusType.PENDING
        @ticket.satisfaction_probability = 0.912819
        @ticket.will_be_saved_by(User.system)
        @ticket.save!
      end

      before_should 'enqueue a resque job ' do
        Sharing::TicketJob.expects(:enqueue).once.with(@shared_ticket.account_id, :update_partner, @shared_ticket.id)
      end
    end
  end
end
