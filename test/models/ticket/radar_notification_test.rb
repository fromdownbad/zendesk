require_relative "../../support/test_helper"
require 'zendesk/radar_factory'

SingleCov.not_covered!

describe Ticket do
  fixtures :tickets, :users, :accounts

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @agent = users(:minimum_agent)
    @now = Timecop.freeze(Time.now)
    @notification = mock('notification')
  end

  describe "#notify_radar" do
    describe "sends notification" do
      it "on ticket update" do
        RadarFactory.expects(:create_radar_notification).with(@ticket.account, "ticket/#{@ticket.nice_id}").returns(@notification)
        expected_message = {updated_by: @agent.id, updated_at: @now.to_i}.to_json
        @notification.expects(:send).with('updated', expected_message)

        @ticket.ticket_type_id = TicketType.QUESTION
        @ticket.will_be_saved_by(@agent)
        @ticket.save!
      end

      it "when user is system user" do
        RadarFactory.expects(:create_radar_notification).with(@ticket.account, "ticket/#{@ticket.nice_id}").returns(@notification)
        expected_message = {updated_by: -1, updated_at: @now.to_i}.to_json
        @notification.expects(:send).with('updated', expected_message)

        @ticket.ticket_type_id = TicketType.QUESTION
        @ticket.will_be_saved_by(User.system)
        @ticket.save!
      end
    end

    describe "skips notification" do
      before do
        Audit.any_instance.stubs(:disable_triggers).returns(false)
      end

      it "on ticket delete" do
        RadarFactory.expects(:create_radar_notification).with(@ticket.account, "ticket/#{@ticket.nice_id}").never
        @ticket.will_be_saved_by(@agent)
        @ticket.soft_delete!
      end

      it "on empty update" do
        @ticket.will_be_saved_by(@agent)
        @ticket.save!

        RadarFactory.expects(:create_radar_notification).with(@ticket.account, "ticket/#{@ticket.nice_id}").never
        @ticket.will_be_saved_by(@agent)
        @ticket.save!
      end

      it "when missing current_user" do
        RadarFactory.expects(:create_radar_notification).with(@ticket.account, "ticket/#{@ticket.nice_id}").never
        @ticket.will_be_saved_by(@agent)
        # some weird fallout from the way precreated account conversion does its thing
        @ticket.stubs(:current_user).returns(nil)
        @ticket.save!
      end
    end
  end
end
