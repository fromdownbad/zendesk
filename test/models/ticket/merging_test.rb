require_relative "../../support/test_helper"
require_relative '../../support/collaboration_settings_test_helper'

SingleCov.covered! uncovered: 17

describe Ticket do
  fixtures :accounts, :ticket_fields, :custom_field_options, :tickets, :users, :ticket_forms, :groups, :organizations, :account_settings, :events

  describe "#add_ticket_merge_audit" do
    let(:ticket) { tickets(:minimum_1) }

    describe "for a source TicketMergeAudit" do
      before do
        ticket.add_ticket_merge_audit('source', target_id: 1, user: users(:minimum_agent))
        assert_equal TicketMergeAudit, ticket.audit.class
      end

      it "does not have a value" do
        assert_nil ticket.audit.value
      end

      it "has a target_id" do
        assert_equal 1, ticket.audit.target_id
      end

      it "sets the current_user" do
        assert_equal users(:minimum_agent), ticket.current_user
      end
    end

    describe "for a target TicketMergeAudit" do
      before do
        ticket.add_ticket_merge_audit('target', source_tickets: [tickets(:minimum_2), tickets(:minimum_3)], user: users(:minimum_agent))
      end

      it "sets the value as a string of the source nice_ids delimited by commas" do
        assert_equal "#{tickets(:minimum_2).nice_id},#{tickets(:minimum_3).nice_id}", ticket.audit.value
      end

      it "does not have a target_id" do
        assert_nil ticket.audit.target_id
      end

      it "sets the current_user" do
        assert_equal users(:minimum_agent), ticket.current_user
      end
    end
  end

  describe 'merging tickets' do
    let(:user) { users(:minimum_admin) }
    let(:target) { tickets(:minimum_1) }
    let(:source1) { tickets(:minimum_2) }
    let(:source2) { tickets(:minimum_3) }
    let(:source3) { tickets(:minimum_4) }

    describe '#add_merge_ticket_link' do
      it 'creates a new TicketLink of merge type for the ticket' do
        source1.add_merge_ticket_link(target)
        tl = source1.target_links.last
        assert_equal tl.link_type, TicketLink::MERGE
        assert_equal tl.target_id, target.id
      end
    end

    Zendesk::Types::ViaType.values.each do |via_voice_id|
      it "links target to via_reference_id for #{via_voice_id} tickets" do
        source1.stubs(:via_id).returns(via_voice_id)
        source1.expects(:update_voice_ticket).once
        source1.merge_into(target, user, "Abc", true)
      end
    end

    describe '#merge_collaborators' do
      let(:agent_with_alias) do
        agent = users(:minimum_admin)
        agent.name = 'Agent'
        agent.alias = 'Alias'
        agent.save
        agent
      end

      let(:agent_with_name) do
        agent = users(:minimum_admin_not_owner)
        agent.name = 'Agent'
        agent.save
        agent
      end

      let(:legacy_agent) do
        agent = users(:minimum_admin_not_verified)
        agent.name = 'Legacy'
        agent.save
        agent
      end

      describe_with_arturo_disabled :email_ccs do
        it 'should call set_collaborators' do
          target.expects(:force_collaboration=).with(true)
          target.expects(:set_collaborators=).with([legacy_agent.id])
          target.merge_collaborators(follower_ids: [legacy_agent.id], email_cc_ids: [], collaborator_ids: [])
        end
      end

      describe_with_arturo_enabled :email_ccs do
        before { source1.collaborations << Collaboration.new(collaborator_type: CollaboratorType.LEGACY_CC, user: legacy_agent) }

        describe 'legacy_ccs' do
          it 'should call set_collaborators if any source tickets had legacy collaborators' do
            target.expects(:force_collaboration=).twice.with(true)
            target.expects(:set_collaborators=).with([legacy_agent.id])
            target.expects(:set_followers=).with([{user_id: agent_with_name.id, action: :put}])
            target.merge_collaborators(collaborator_ids: [legacy_agent.id], follower_ids: [agent_with_name.id], email_cc_ids: [])
          end

          it 'should never call set_collaborators if no legacy_ccs are on either the source or target' do
            target.expects(:set_collaborators=).never
            target.merge_collaborators(follower_ids: [agent_with_name.id], email_cc_ids: [], collaborator_ids: [])
          end
        end

        describe 'followers' do
          describe 'and at least one source ticket has a follower' do
            before { source2.collaborations << Collaboration.new(collaborator_type: CollaboratorType.FOLLOWER, user: agent_with_alias) }

            it 'should call set_followers and set force_collaboration flag' do
              target.expects(:force_collaboration=).with(true)
              target.expects(:set_followers=).with([{user_id: agent_with_alias.id, action: :put}])
              target.merge_collaborators(follower_ids: [agent_with_alias.id], email_cc_ids: [], collaborator_ids: [])
            end
          end

          describe 'none of the sources or target have any followers' do
            it 'should not call set_followers' do
              target.expects(:set_followers).never
              target.merge_collaborators(follower_ids: [], email_cc_ids: [], collaborator_ids: [agent_with_alias.id])
            end
          end
        end

        describe 'email_ccs' do
          describe 'and at least one source ticket has an email_cc' do
            before { source2.collaborations << Collaboration.new(collaborator_type: CollaboratorType.EMAIL_CC, user: agent_with_name) }

            it 'should call set_email_ccs and set force_collaboration flag' do
              target.expects(:set_email_ccs=).with([{user_id: agent_with_name.id, action: :put}])
              target.expects(:force_collaboration=).with(true)
              target.merge_collaborators(follower_ids: [], email_cc_ids: [agent_with_name.id], collaborator_ids: [])
            end

            describe 'and new email-ccs settings are enabled' do
              let(:user)  { users(:minimum_end_user) }
              let(:limit) { 1 }

              before do
                CollaborationSettingsTestHelper.enable_all_new_collaboration_settings

                Ticket.any_instance.stubs(max_email_ccs_limit: limit)

                target.collaborations.create!(
                  collaborator_type: CollaboratorType.EMAIL_CC,
                  user: user
                )

                target.merge_collaborators(
                  follower_ids: [],
                  email_cc_ids: [agent_with_name.id],
                  collaborator_ids: []
                )

                target.will_be_saved_by(users(:minimum_admin))
                target.save!
              end

              describe 'and target ticket already has maximum number of CCs allowed' do
                let(:limit) { 1 }

                it 'does not add any new CCs' do
                  refute target.collaborations.map(&:user_id).include?(agent_with_name.id)
                end
              end

              describe "and adding CCs from source ticket won't exceed the limit" do
                let(:limit) { 2 }

                it 'add new CCs' do
                  assert target.collaborations.map(&:user_id).include?(agent_with_name.id)
                end
              end
            end
          end

          describe 'none of the sources or target have any email_ccs' do
            it 'should never call set_email_ccs' do
              target.expects(:set_email_ccs=).never
              target.merge_collaborators(follower_ids: [agent_with_name.id], email_cc_ids: [], collaborator_ids: [])
            end
          end
        end
      end
    end
  end
end
