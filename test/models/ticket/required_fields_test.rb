require_relative "../../support/test_helper"
SingleCov.covered!

describe Ticket do
  fixtures :accounts, :ticket_fields, :custom_field_options, :tickets, :users, :ticket_forms

  let(:account) { accounts(:minimum) }
  let(:admin) { users(:minimum_admin) }
  let(:ticket) { tickets(:minimum_2) }
  let(:ticket_form) { ticket_forms(:minimum_ticket_form) }
  let(:parent_field) { ticket_fields(:field_tagger_custom) }
  let(:system_parent_field) { ticket_fields(:minimum_field_priority) }
  let(:child_field) { ticket_fields(:field_textarea_custom) }
  let(:child_field2) { ticket_fields(:field_checkbox_custom) }
  let(:cfo1) { custom_field_options(:field_tagger_custom_option_1) }
  let(:cfo2) { custom_field_options(:field_tagger_custom_option_2) }
  let(:user) { users(:minimum_admin) }
  let(:condition) do
    condition = TicketFieldCondition.new.tap do |c|
      c.account = account
      c.ticket_form = ticket_form
      c.parent_field = parent_field
      c.child_field = child_field
      c.value = cfo2.value
      c.user_type = :agent
    end
    condition.save

    condition
  end

  let(:system_field_condition) do
    condition = TicketFieldCondition.new.tap do |c|
      c.account = account
      c.ticket_form = ticket_form
      c.parent_field = system_parent_field
      c.child_field = child_field2
      c.value = 2 # Low
      c.user_type = :agent
    end
    condition.save

    condition
  end

  before do
    Account.any_instance.stubs(:has_ticket_forms?).returns(true)
    ticket_form.account_id = account.id
    ticket_form.save!

    TicketFormField.create(
      ticket_form: ticket_form,
      account: account,
      ticket_field: parent_field,
      position: 1
    )
    TicketFormField.create(
      ticket_form: ticket_form,
      account: account,
      ticket_field: child_field,
      position: 2
    )
    TicketFormField.create(
      ticket_form: ticket_form,
      account: account,
      ticket_field: system_parent_field,
      position: 3
    )
    TicketFormField.create(
      ticket_form: ticket_form,
      account: account,
      ticket_field: child_field2,
      position: 2
    )

    # Reload form to refresh memoized collections:
    #   * @agent_ticket_field_conditions
    #   * @end_user_ticket_field_conditions
    ticket_form_id = ticket_form.id
    ticket_form = TicketForm.find(ticket_form_id)

    ticket.ticket_form = ticket_form
    system_field_condition
  end

  describe_with_arturo_enabled :native_conditional_fields_enabled do
    describe "agent required fields validation errors" do
      describe "when the current form has agent conditions" do
        before do
          assert ticket.ticket_form.agent_ticket_field_conditions.present?
          ticket.stubs(:agent_required_fields).returns([child_field])

          ticket.assignee = users(:minimum_admin)
          ticket.status_id = StatusType.OPEN
          ticket.will_be_saved_by(users(:minimum_admin))
        end

        it "adds errors when we update the ticket using #agent_required_fields" do
          refute ticket.save
          assert_includes ticket.errors[:base].join, "#{child_field.title}: needed"
        end
      end

      describe "when the current form doesn't have agent conditions" do
        before { ticket_form.ticket_field_conditions.for_agents.delete_all }

        it "doesn't add errors" do
          ticket.stubs(:agent_required_fields).returns([child_field])

          ticket.assignee = users(:minimum_admin)
          ticket.status_id = StatusType.OPEN
          ticket.will_be_saved_by(users(:minimum_admin))
          assert ticket.save
        end
      end

      describe "when agent_required_fields includes a nil field" do
        it "doesn't add errors and doesn't fail" do
          ticket.stubs(:agent_required_fields).returns([nil])

          ticket.assignee = users(:minimum_admin)
          ticket.status_id = StatusType.OPEN
          ticket.will_be_saved_by(users(:minimum_admin))
          assert ticket.save
        end
      end

      describe "when a new ticket is created using a form with required fields" do
        let(:new_ticket) { account.tickets.new(requester: admin, description: 'hello', ticket_form_id: ticket_form.id) }
        let(:via_id) { ViaType.WEB_FORM }

        before do
          new_ticket.stubs(:agent_required_fields).returns([child_field])
          new_ticket.will_be_saved_by(admin, via_id: via_id)
        end

        [ViaType.LINKED_PROBLEM, ViaType.BATCH].each do |via_type|
          describe "and current via is '#{ViaType.to_s(via_type)}'" do
            let(:via_id) { via_type }

            it "doesn't validate required fields" do
              assert new_ticket.save
            end
          end
        end

        [ViaType.WEB_FORM, ViaType.CLOSED_TICKET, ViaType.WEB_SERVICE, ViaType.MOBILE].each do |via_type|
          describe "and current via is '#{ViaType.to_s(via_type)}'" do
            let(:via_id) { via_type }

            it "validates required fields" do
              refute new_ticket.save
              assert_includes new_ticket.errors[:base].join, "#{child_field.title}: needed"
            end
          end
        end

        # Mail API can set tags that change tagger ticket fields.
        # That can cause undesired mail rejections if the change reveals an always required field.
        # We skip validations only for new mail tickets because we still want to reject status changes
        # through email API when the new status requires values on certain fields.
        describe "and current via is 'Mail'" do
          let(:via_id) { ViaType.MAIL }

          # We want to check globally required fields for new Mail tickets marked as solved.
          describe "and the new ticket is marked as 'Solved'" do
            before { new_ticket.status_id = StatusType.SOLVED }

            it "validates required fields" do
              refute new_ticket.save
              assert_includes new_ticket.errors[:base].join, "#{child_field.title}: is required when solving a ticket"
            end
          end

          [StatusType.NEW, StatusType.OPEN, StatusType.PENDING].each do |status_type|
            describe "and the new ticket is marked as '#{StatusType.to_s(status_type)}'" do
              before { new_ticket.status_id = status_type }

              it "doesn't validate required fields" do
                assert new_ticket.save
              end
            end
          end
        end
      end
    end
  end

  describe "#measure_agent_required_fields" do
    it "calls agent_required_fields" do
      ticket.expects(:agent_required_fields).returns([])
      ticket.measure_agent_required_fields { ticket.validate_agent_required_fields }
    end

    it "measures the agent_required_fields execution time" do
      ZendeskAPM.expects(:trace).with(
        'validate_agent_required_fields',
        service: TicketField::APM_SERVICE_NAME
      )
      ticket.measure_agent_required_fields { ticket.validate_agent_required_fields }
    end
  end

  describe "#agent_required_fields" do
    before { ticket.status_id = StatusType.SOLVED }

    describe_with_arturo_enabled :native_conditional_fields_enabled do
      describe "without a condition" do
        describe "and the child field is not globally required" do
          # The child field is not conditional but is also not globally required
          it "doesn't include the child field" do
            refute ticket.agent_required_fields.include?(child_field)
          end
        end

        describe "and the child field is globally required" do
          before { child_field.update_attribute(:is_required, true) }

          # The child field is not conditional so we respect global requirement
          it "includes the child field" do
            assert ticket.agent_required_fields.include?(child_field)
          end
        end

        describe "and the ticket is not solved" do
          before { ticket.status_id = StatusType.OPEN }

          describe "and the child field is globally required" do
            before { child_field.update_attribute(:is_required, true) }

            # Global requirement is only effective when solving the ticket.
            it "doesn't include the child field" do
              refute ticket.agent_required_fields.include?(child_field)
            end
          end
        end
      end

      describe "with a condition that is always required" do
        describe "using a custom field in the condition" do
          before { condition.update_attribute(:required_on_statuses, TicketFieldCondition::REQUIRED_ON_ALL_STATUSES) }

          # We have validations to stop ticket field deletions used in conditions
          # but we found edge cases with conditions pointing to deleted fields
          # and we want to be sure ticket save still works in that situation.
          describe "and the parent field was deleted" do
            before { TicketField.where(id: condition.parent_field_id).delete_all }

            it "doesn't include the child field" do
              refute ticket.agent_required_fields.include?(child_field)
            end
          end

          # Child field is not visible
          describe "and the parent field has no value" do
            it "doesn't include the child field" do
              refute ticket.agent_required_fields.include?(child_field)
            end
          end

          # Child field is not visible
          describe "and the parent field has a different option selected" do
            before do
              TicketFieldEntry.create!(ticket_field: parent_field, ticket: ticket, value: cfo1.value)
            end

            it "doesn't include the child field" do
              refute ticket.agent_required_fields.include?(child_field)
            end

            describe "and the child field is globally required" do
              before { child_field.update_attribute(:is_required, true) }

              # The child field is not visible and cannot be required
              it "doesn't include the child field" do
                refute ticket.agent_required_fields.include?(child_field)
              end
            end
          end

          # Child field is visible
          describe "and the option selected in the parent field is the same as the defined in the condition" do
            before do
              TicketFieldEntry.create!(ticket_field: parent_field, ticket: ticket, value: cfo2.value)
            end

            it "includes the child field" do
              assert ticket.agent_required_fields.include?(child_field)
            end
          end
        end

        describe "using a system field in the condition" do
          before { system_field_condition.update_attribute(:required_on_statuses, TicketFieldCondition::REQUIRED_ON_ALL_STATUSES) }

          describe "and the option selected in the system parent field is the same as the defined in the condition" do
            it "includes the child field" do
              assert ticket.agent_required_fields.include?(child_field2)
            end
          end
        end
      end

      describe "with a condition that is not required" do
        before { condition.update_attribute(:is_required, false) }

        describe "and the option selected in the parent field is the same as the defined in the condition" do
          before do
            TicketFieldEntry.create!(ticket_field: parent_field, ticket: ticket, value: cfo2.value)
          end

          it "doesn't include the child field" do
            refute ticket.agent_required_fields.include?(child_field)
          end

          describe "and the child field is globally required" do
            before { child_field.update_attribute(:is_required, true) }

            # Overrides global requirement
            it "doesn't include the child field" do
              refute ticket.agent_required_fields.include?(child_field)
            end
          end
        end
      end

      describe "with required_on_statuses" do
        before do
          TicketFieldCondition.any_instance.stubs(:required_on_statuses).returns(required_on_statuses)
          TicketFieldEntry.create!(ticket_field: parent_field, ticket: ticket, value: cfo2.value)
        end

        describe "with a condition that is required on some statuses" do
          let(:required_on_statuses) { "#{StatusType.NEW} #{StatusType.OPEN}" }

          describe "and the ticket is in one of the required statuses" do
            before { ticket.status_id = StatusType.NEW }

            it "includes the child field" do
              assert ticket.agent_required_fields.include?(child_field2)
            end
          end

          describe "and the ticket is not in one of the required statuses" do
            before { ticket.status_id = StatusType.SOLVED }

            it "doesn't include the child field" do
              refute ticket.agent_required_fields.include?(child_field2)
            end
          end

          describe "when solving a ticket and the field is globally required but solved is not in the list of required statuses in the condition" do
            before do
              child_field2.update_attribute(:is_required, true)
              ticket.status_id = StatusType.SOLVED
            end

            it "overrides global flag with the conditional requirements so it doesn't include the child field" do
              refute ticket.agent_required_fields.include?(child_field2)
            end
          end
        end

        describe "with a condition that is required on all statuses" do
          let(:required_on_statuses) { TicketFieldCondition::REQUIRED_ON_ALL_STATUSES }

          [StatusType.NEW, StatusType.OPEN, StatusType.HOLD, StatusType.SOLVED].each do |status_id|
            it "includes the field when the ticket is in the operable status_id: #{status_id}" do
              ticket.status_id = status_id
              assert ticket.agent_required_fields.include?(child_field2)
            end
          end

          [StatusType.DELETED, StatusType.ARCHIVED, StatusType.CLOSED].each do |status_id|
            it "doesn't include the field when the ticket is in the inoperable status_id #{status_id}" do
              ticket.status_id = status_id
              refute ticket.agent_required_fields.include?(child_field2)
            end
          end
        end
      end
    end

    describe_with_arturo_disabled :native_conditional_fields_enabled do
      describe "with a condition that is not required" do
        before { condition.update_attribute(:is_required, false) }

        describe "and the option selected in the parent field is the same as the defined in the condition" do
          before do
            TicketFieldEntry.create!(ticket_field: parent_field, ticket: ticket, value: cfo2.value)
          end

          describe "and the child field is globally required" do
            before { child_field.update_attribute(:is_required, true) }

            # Respects global requirement
            it "includes the child field" do
              assert ticket.agent_required_fields.include?(child_field)
            end
          end

          describe "and the child field is not globally required" do
            before { child_field.update_attribute(:is_required, false) }

            # Respects global requirement
            it "doesn't include the child field" do
              refute ticket.agent_required_fields.include?(child_field)
            end
          end
        end
      end
    end
  end

  describe "#end_user_required_fields" do
    describe_with_arturo_enabled :native_conditional_fields_enabled do
      describe "without a condition" do
        describe "and the child field is not globally required" do
          # The child field is not conditional but is also not globally required
          it "doesn't include the child field" do
            refute ticket.end_user_required_fields.include?(child_field)
          end
        end

        describe "and the child field is globally required" do
          before { child_field.update_attribute(:is_required_in_portal, true) }

          # The child field is not conditional so we respect global requirement
          it "includes the child field" do
            assert ticket.end_user_required_fields.include?(child_field)
          end
        end
      end

      describe "with an end user condition that is required" do
        describe "using a custom field in the condition" do
          before { condition.update_attributes(is_required: true, user_type: :end_user) }

          # Child field is not visible
          describe "and the parent field has no value" do
            it "doesn't include the child field" do
              refute ticket.end_user_required_fields.include?(child_field)
            end
          end

          # Child field is not visible
          describe "and the parent field has a different option selected" do
            before do
              TicketFieldEntry.create!(ticket_field: parent_field, ticket: ticket, value: cfo1.value)
            end

            it "doesn't include the child field" do
              refute ticket.end_user_required_fields.include?(child_field)
            end

            describe "and the child field is globally required" do
              before { child_field.update_attribute(:is_required_in_portal, true) }

              # The child field is not visible and cannot be required
              it "doesn't include the child field" do
                refute ticket.end_user_required_fields.include?(child_field)
              end
            end
          end

          # Child field is visible
          describe "and the option selected in the parent field is the same as the defined in the condition" do
            before do
              TicketFieldEntry.create!(ticket_field: parent_field, ticket: ticket, value: cfo2.value)
            end

            it "includes the child field" do
              assert ticket.end_user_required_fields.include?(child_field)
            end
          end
        end

        describe "using a system field in the condition" do
          before { system_field_condition.update_attribute(:required_on_statuses, TicketFieldCondition::REQUIRED_ON_ALL_STATUSES) }

          describe "and the option selected in the system parent field is the same as the defined in the condition" do
            it "includes the child field" do
              assert ticket.agent_required_fields.include?(child_field2)
            end
          end
        end
      end

      describe "with an end user condition that is not required" do
        before { condition.update_attributes(is_required: false, user_type: :end_user) }

        describe "and the option selected in the parent field is the same as the defined in the condition" do
          before do
            TicketFieldEntry.create!(ticket_field: parent_field, ticket: ticket, value: cfo2.value)
          end

          it "doesn't include the child field" do
            refute ticket.end_user_required_fields.include?(child_field)
          end

          describe "and the child field is globally required" do
            before { child_field.update_attribute(:is_required_in_portal, true) }

            # Overrides global requirement
            it "doesn't include the child field" do
              refute ticket.end_user_required_fields.include?(child_field)
            end
          end
        end
      end
    end

    describe_with_arturo_disabled :native_conditional_fields_enabled do
      describe "with an end user condition that is not required" do
        before { condition.update_attributes(is_required: false, user_type: :end_user) }

        describe "and the option selected in the parent field is the same as the defined in the condition" do
          before do
            TicketFieldEntry.create!(ticket_field: parent_field, ticket: ticket, value: cfo2.value)
          end

          describe "and the child field is globally required" do
            before { child_field.update_attribute(:is_required_in_portal, true) }

            # Respects global requirement
            it "includes the child field" do
              assert ticket.end_user_required_fields.include?(child_field)
            end
          end

          describe "and the child field is not globally required" do
            before { child_field.update_attribute(:is_required_in_portal, false) }

            # Respects global requirement
            it "doesn't include the child field" do
              refute ticket.end_user_required_fields.include?(child_field)
            end
          end
        end
      end
    end
  end
end
