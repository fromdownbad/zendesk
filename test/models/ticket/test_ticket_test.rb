require_relative "../../support/test_helper"
require 'zendesk/radar_factory'

SingleCov.not_covered!

describe 'Ticket' do
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @admin = users(:minimum_admin)
    @group = groups(:minimum_group)
    @test_requester = @account.users.new(Ticket::TEST_REQUESTER)
    Account.any_instance.stubs(:field_ticket_type).returns(FieldTicketType.new)
  end

  describe "#test_ticket?" do
    it "returns true for tickets requested by #{Ticket::TEST_REQUESTER[:email]}" do
      @ticket.stubs(:requester).returns(@test_requester)
      assert @ticket.test_ticket?
    end

    it "returns false for tickets not requested by #{Ticket::TEST_REQUESTER[:email]}" do
      refute @ticket.test_ticket?
    end
  end

  describe "#create_test_ticket!" do
    before do
      @ticket = Ticket.create_test_ticket!(@account, @admin)
    end

    should_change("the number of Users", by: 1) { User.count(:all) }
    should_change("the number of Tickets", by: 1) { Ticket.count(:all) }

    it "creates a test ticket and the test requester" do
      assert_equal Ticket::TEST_REQUESTER[:email], User.last.email
      assert Ticket.last.test_ticket?
    end

    it "automatically assign the test ticket to the given agent" do
      assert_equal @admin, Ticket.last.assignee
    end

    it "returns an instance of Ticket" do
      assert @ticket.instance_of? Ticket
    end

    it "returns the ticket that was just created" do
      assert_equal @ticket, Ticket.last
    end
  end

  describe '#generate_email_forward_pending_ticket' do
    before do
      @forward_from_email = 'hello@zobuzendesk.com'
      @ticket = Ticket.generate_email_forward_pending_ticket(@account, @admin)
    end

    it "returns a ticket mentioning the forward_from_email" do
      @ticket.subject.must_include I18n.t('txt.zero_states.email_forward_pending_ticket.subject')
      @ticket.description.must_include I18n.t('txt.zero_states.email_forward_pending_ticket.body_pending')
    end
  end

  describe "#generate_sample_ticket_without_notifications" do
    describe "with no template name" do
      it "defaults to meet_the_ticket" do
        ticket = Ticket.generate_sample_ticket_without_notifications(@account, @admin)
        template = Ticket.ticket_template_with_context(@admin, "meet_the_ticket_en")
        assert_equal ticket.subject, template[:subject]
      end
    end

    describe "with template name" do
      it "creates ticket using the right template" do
        ticket = Ticket.generate_sample_ticket_without_notifications(@account, @admin, "suite_trial_sample_ticket")
        template = Ticket.ticket_template_with_context(@admin, "suite_trial_sample_ticket")
        assert_equal ticket.subject, template[:subject]
      end

      it "logs a metric when a suite trial sample ticket is created" do
        Ticket.expects(:instrument_sample_ticket_creation).with(@account, "suite_trial_sample_ticket")
        Ticket.generate_sample_ticket_without_notifications(@account, @admin, "suite_trial_sample_ticket")
      end
    end

    describe "instrumentation for suite trial sample ticket" do
      let(:statsd_client) { stub_for_statsd }

      before do
        Zendesk::StatsD::Client.stubs(:new).with(namespace: ['suite_trial']).returns(statsd_client)
      end

      it "increments a counter when ticket is created" do
        statsd_client.expects(:increment).with("sample_ticket_created", instance_of(Hash)).once
        Ticket.instrument_sample_ticket_creation(@account, "suite_trial_sample_ticket")
      end

      it "does not log the metric when a suite trial sample ticket is not created" do
        statsd_client.expects(:increment).with("sample_ticket_created", instance_of(Hash)).never
        Ticket.instrument_sample_ticket_creation(@account, "meet_the_ticket")
      end
    end
  end

  describe "#add_automatic_reply" do
    before do
      Ticket.create_test_ticket!(@account, @admin)
    end

    it "handles when assignee gets set to nil" do
      test_ticket = Ticket.last
      test_ticket.assignee_id = nil
      test_ticket.will_be_saved_by(@admin, via_id: ViaType.GROUP_CHANGE)
      test_ticket.save!
    end

    it "adds an automatic comment after the first and the second admin's responses in a test ticket" do
      test_ticket = Ticket.last
      assert_equal 1, test_ticket.comments.count(:all)

      # add first admin's response
      test_ticket.add_comment(body: "Thanks for this information, please tell me more.", is_public: true)
      test_ticket.will_be_saved_by(@admin, via_id: ViaType.WEB_FORM)
      test_ticket.save!
      assert_equal 3, test_ticket.comments.count(:all)
      first_automatic_reply = test_ticket.comments.last
      assert_match(/When you clicked Submit/, first_automatic_reply.body)
      assert_equal Ticket::TEST_REQUESTER[:email], first_automatic_reply.author.email
      assert first_automatic_reply.via?(:SAMPLE_INTERACTIVE_TICKET)

      # add second admin's response
      test_ticket.add_comment(body: "Hope this resolves your issue, please let me know if you need anything else.", is_public: true)
      test_ticket.will_be_saved_by(@admin, via_id: ViaType.WEB_FORM)
      test_ticket.save!
      assert_equal 5, test_ticket.comments.count(:all)
      second_automatic_reply = test_ticket.comments.last
      assert_match(/But wait! What if your customer has another question/, second_automatic_reply.body)
      assert_equal Ticket::TEST_REQUESTER[:email], second_automatic_reply.author.email
      assert second_automatic_reply.via?(:SAMPLE_INTERACTIVE_TICKET)
    end

    it "adds nothing when ticket is shared" do
      stub_request(:any, %r{http://example.com/sharing/tickets})
      test_ticket = Ticket.last

      agreement = FactoryBot.create(
        :agreement,
        account: test_ticket.account,
        direction: :out, status: :accepted
      )
      shared_ticket = SharedTicket.new(
        ticket: test_ticket,
        agreement_id: agreement.id
      )
      shared_ticket.save!
      test_ticket.reload

      # add admin's response
      test_ticket.add_comment(body: "Thanks for this information, please tell me more.", is_public: true)
      test_ticket.will_be_saved_by(@admin, via_id: ViaType.WEB_FORM)
      test_ticket.save!
      # only contains the initial comment and the admin's response, no automatic reply
      assert_equal 2, test_ticket.comments.count(:all)
    end

    it "does nothing for non test tickets" do
      requester = users(:minimum_author)

      ticket = @account.tickets.build do |t|
        t.via_id         = ViaType.MAIL
        t.requester      = requester
        t.subject        = 'A non test ticket'
        t.description    = 'Some description'
      end

      ticket.will_be_saved_by(requester)
      ticket.save!
      assert_equal 1, ticket.comments.count(:all)

      # add admin's response
      ticket.add_comment(body: "Thanks for this information, please tell me more.", is_public: true)
      ticket.will_be_saved_by(@admin, via_id: ViaType.WEB_FORM)
      ticket.save!
      # no automatic reply added
      assert_equal 2, ticket.comments.count(:all)
    end
  end

  describe '#enable_triggers' do
    it 'activates specified triggers' do
      disabled_triggers = []
      trigger = @account.triggers.first
      trigger.update_attribute(:is_active, false)
      disabled_triggers << trigger

      Ticket.enable_triggers(disabled_triggers)

      assert(trigger.is_active)
    end
  end

  describe '#with_triggers_disabled' do
    before do
      @title_keys = %w[
        txt.default.triggers.notify_assignee_reopened.title
        txt.default.triggers.notify_group_assignment.title
      ]

      @account.triggers.update_all(is_active: false)

      @account.
        triggers.
        where(title: I18n.t(@title_keys.first)).
        first.
        update_attribute(:is_active, true)

      @block_called = false
    end

    it 'calls the block' do
      Ticket.with_triggers_disabled(@account, @title_keys) do
        @block_called = true
      end

      assert @block_called
    end

    it 'does not activate non-active triggers with a matching title' do
      Ticket.with_triggers_disabled(@account, @title_keys) do
        # method checks for a block
      end

      assert_equal(
        1,
        @account.
          triggers.
          where(title: @title_keys.map { |key| I18n.t(key) }).
          where(is_active: true).
          count
      )
    end

    it 're-enables triggers if block code raises error' do
      assert_raises(RuntimeError) do
        Ticket.with_triggers_disabled(@account, @title_keys) do
          raise 'ticket error'
        end
      end

      assert_equal(
        1,
        @account.
          triggers.
          where(title: @title_keys.map { |key| I18n.t(key) }).
          where(is_active: true).
          count
      )
    end
  end
end
