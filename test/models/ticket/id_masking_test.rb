require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Ticket::IdMasking do
  fixtures :tickets
  fixtures :accounts

  describe "A ticket" do
    before do
      @ticket = Ticket.last
      @account = accounts(:minimum)
    end

    describe "referenced by an encoded ID" do
      it "is located" do
        Ticket.find_by_encoded_id(@ticket.encoded_id).must_equal @ticket
      end

      it "is not located when the encoded id is tampered with" do
        Ticket.find_by_encoded_id(@ticket.encoded_id.succ).must_be_nil
      end

      it "is not located when the encoded id is invalid" do
        Ticket.find_by_encoded_id('75N0E7XN6P').must_be_nil
      end

      it "that is archived is found also after archiving" do
        archive_and_delete @ticket

        @account.tickets.find_by_encoded_id(@ticket.encoded_id).must_be_instance_of Ticket
        @account.tickets.find_by_encoded_id(@ticket.encoded_id).must_equal @ticket
      end
    end

    describe "a generated message id" do
      it "begins with the encoded id" do
        assert(@ticket.generate_message_id.starts_with?("<#{@ticket.encoded_id.delete('-')}"))
      end

      it "includes a randomly generated part" do
        assert_not_equal @ticket.generate_message_id, @ticket.generate_message_id
      end

      it "is a valid message id" do
        assert_match(/<.*@.*>/, @ticket.generate_message_id)
      end
    end

    describe "encoded id" do
      it "is at least 10 characters, excluding dashes" do
        @ticket.encoded_id.delete('-').size.must_be :>=, 10
      end

      it "has a dash in the middle" do
        @ticket.encoded_id.index('-').must_equal 6
      end
    end
  end

  describe "Id Masking" do
    let(:masked_id) { 12345 }

    describe "hashids" do
      it "is null if salt string is not configured" do
        Ticket::IdMasking.stubs(:hashids_salt).returns(nil)
        Ticket::IdMasking.send(:hashids).must_be_nil
      end

      it "is not null if salt string is configured" do
        Ticket::IdMasking.send(:hashids).wont_be_nil
      end
    end

    describe "v2 masking" do
      it "encodes a hashids version of the id" do
        Ticket::IdMasking.mask(masked_id).must_equal 'P6NX7E-0N57'
      end
    end

    describe "v2 unmasking" do
      it "decodes a v2 ID containing a dash" do
        Ticket::IdMasking.unmask('P6NX7E-0N57').must_equal masked_id
      end

      it "decodes a v2 ID not containing a dash" do
        Ticket::IdMasking.unmask('P6NX7E0N57').must_equal masked_id
      end

      it "decodes a v2 ID longer than 10 characters" do
        Ticket::IdMasking.unmask('037XX8EP79Y').must_equal 4398046511104
      end

      it "is case insensitive" do
        Ticket::IdMasking.unmask('p6nx7e0n57').must_equal masked_id
      end

      it "is nil when the id is invalid" do
        Ticket::IdMasking.unmask('75N0E7XN6P').must_be_nil
      end
    end
  end
end
