require_relative "../support/test_helper"

SingleCov.covered! uncovered: 5

describe UserIdentity do
  def create_user(options = {})
    account = accounts(:minimum)
    options = { account: account }.merge(options)
    FactoryBot.create(:user, options)
  end

  fixtures :accounts, :subscriptions, :users, :user_identities,
    :account_property_sets, :tokens, :role_settings, :channels_user_profiles

  it "does not allow the last identity to be destroyed" do
    user = create_user
    id1  = user.identities.first
    id2  = user.identities.create!(value: 'foo@example.com')
    assert_equal(2, user.identities.size)

    assert id1.destroy
    refute id2.reload.destroy
  end

  it "responds to humanize" do
    user     = create_user
    identity = user.identities.first

    assert_equal('Email', identity.humanize)
  end

  it "transfers itself to another user" do
    user = users(:minimum_agent)
    identity = user_identities(:minimum_end_user)

    assert_not_equal user, identity.user

    identity.transfer_to_user(user)

    assert_equal user, identity.user
  end

  describe 'validations' do
    subject { users(:minimum_author) }

    describe "uniqueness of value" do
      let(:uniqueness_query) do
        /SELECT COUNT\(\*\) FROM `user_identities` WHERE `user_identities`\.`type` IN \('UserEmailIdentity'\) AND \(account_id = \d+ AND value = '\w+@server\.com' AND type = 'UserEmailIdentity'( AND id <> \d+)?\)/
      end

      before do
        UserEmailIdentity.create!(user: subject, value: 'minimum_author@server.com')
      end

      describe "when creating an identity that already exists" do
        it "validates unsuccessfully" do
          identity = UserEmailIdentity.new(user: subject, value: 'minimum_author@server.com')
          assert_sql_queries(1, uniqueness_query) do
            refute identity.valid?
          end
          assert identity.errors.full_messages.join.include?(I18n.t('activerecord.errors.models.user_identity.attributes.value.taken', value: 'minimum_author@server.com'))
        end
      end

      describe "when updating an identity to a value that already exists" do
        it "validates unsuccessfully" do
          identity = UserEmailIdentity.create!(user: subject, value: 'minimum_cat@server.com')
          identity.value = 'minimum_author@server.com'
          assert_sql_queries(1, uniqueness_query) do
            refute identity.valid?
          end
          assert identity.errors.full_messages.join.include?(I18n.t('activerecord.errors.models.user_identity.attributes.value.taken', value: 'minimum_author@server.com'))
        end
      end

      describe "when updating an identity without changing the value" do
        it "does not validate uniqueness" do
          identity = UserEmailIdentity.create!(user: subject, value: 'minimum_cat@server.com')
          refute identity.is_verified
          identity.is_verified = true
          assert identity.is_verified
          assert_sql_queries(0, uniqueness_query) do
            assert identity.valid?
          end
        end
      end
    end

    describe "uniqueness of priority within user_id and type" do
      let(:uniqueness_query) do
        /SELECT  1 AS one FROM `user_identities` WHERE \(`user_identities`\.`priority` = BINARY \d( AND `user_identities`\.`id` != \d+)? AND `user_identities`\.`user_id` = \d+ AND `user_identities`\.`type` = 'UserEmailIdentity'\) LIMIT 1/
      end

      before do
        UserEmailIdentity.create!(user: subject, value: 'minimum_author@server.com')
      end

      describe "when creating an identity" do
        it "sets the lowest available priority and validates successfully" do
          identity = UserEmailIdentity.new(user: subject, value: 'minimum_cat@server.com')
          assert_equal 1, identity.priority
          assert_sql_queries(1, uniqueness_query) do
            assert identity.valid?
          end
          assert_equal subject.reload.identities.length + 1, identity.priority
        end
      end

      describe "when updating an identity to a priority that already exists" do
        it "validates unsuccessfully" do
          identity = UserEmailIdentity.create!(user: subject, value: 'minimum_cat@server.com')
          identity.priority = 1
          assert_sql_queries(1, uniqueness_query) do
            refute identity.valid?
          end
          assert_equal "Priority 1 has already been taken", identity.errors.full_messages.join
        end
      end

      describe "when updating an identity without changing the priority" do
        it "does not validate uniqueness" do
          identity = UserEmailIdentity.create!(user: subject, value: 'minimum_cat@server.com')
          refute identity.is_verified
          identity.is_verified = true
          assert identity.is_verified
          assert_sql_queries(0, uniqueness_query) do
            assert identity.valid?
          end
        end
      end
    end
  end

  describe 'scopes' do
    subject { users(:minimum_author) }

    describe 'any_channel' do
      before do
        UserAnyChannelIdentity.destroy_all
        @any_channel_identity = UserAnyChannelIdentity.create(user: subject, value: '12:34', account: subject.account, is_verified: false)
      end

      it 'returns AnyChannel identities' do
        assert_equal 1, subject.identities.any_channel.length
        assert_equal @any_channel_identity, subject.identities.any_channel.first
      end
    end

    describe 'unverified_email' do
      let(:verified_email_identity) do
        UserEmailIdentity.create(
          user: subject,
          value: "EnduserVerified@zendesk.com",
          account: subject.account,
          is_verified: true
        )
      end
      let(:unverified_email_identity) do
        UserEmailIdentity.create(
          user: subject,
          value: "EnduserUnverified@zendesk.com",
          account: subject.account,
          is_verified: false
        )
      end

      describe 'unverified email created before Sept 17, 2017' do
        before do
          UserEmailIdentity.destroy_all
          Timecop.freeze('2015-01-01') do
            unverified_email_identity
          end
          verified_email_identity
        end

        it 'returns unverified email identities' do
          Timecop.freeze('2017-09-20') do
            assert_empty subject.identities.unverified_emails
          end
        end
      end

      describe 'unverified email created after Sept 17, 2017' do
        before do
          UserEmailIdentity.destroy_all
          Timecop.freeze('2017-09-18') do
            unverified_email_identity
          end
          verified_email_identity
        end

        it 'returns unverified email identities' do
          Timecop.freeze('2017-09-20') do
            assert_equal 1, subject.identities.unverified_emails.length
            assert_equal unverified_email_identity, subject.identities.unverified_emails.first
          end
        end
      end

      describe 'when only verified email identity is present' do
        before do
          UserEmailIdentity.destroy_all
          verified_email_identity
        end

        it 'returns an empty object' do
          assert_empty subject.identities.unverified_emails
        end
      end
    end
  end

  describe "create_identity" do
    subject { users(:minimum_author) }

    describe "#set_lowest_priority" do
      describe "when is_verified is false" do
        let(:new_identity) { UserEmailIdentity.create(user: subject, value: "my.new.email@zendesk.com", account: subject.account, is_verified: false) }

        it "should not be the primary identity" do
          assert !new_identity.reload.primary?
        end
      end

      describe "when is_verified is true" do
        let(:new_identity) { UserEmailIdentity.new(user: subject, value: "my.new.email@zendesk.com", account: subject.account, is_verified: true) }

        describe "when @make_primary is false" do
          before do
            new_identity.save!
          end

          it "should not be the primary identity" do
            assert !new_identity.reload.primary?
          end
        end

        describe "when @make_primary is true" do
          before do
            new_identity.set_primary_when_verifying!
            new_identity.save!
          end

          it "should be the primary identity" do
            assert new_identity.reload.primary?
          end
        end
      end
    end
  end

  describe "#make_primary!" do
    subject { users(:minimum_end_user) }

    before do
      @identity = user_identities(:minimum_end_user)
      @new_identity = UserEmailIdentity.create!(user: subject, value: "my.new.email@zendesk.com")
    end

    describe "loaded identities" do
      before do
        @new_identity.user.identities.all
        @new_identity.make_primary!
      end

      it "updates all other identities of the type" do
        identity = @new_identity.user.identities.detect { |id| id == @identity }
        assert_equal 2, identity.priority
      end
    end

    it "does not load all other identities" do
      @new_identity.reload
      @new_identity.make_primary!

      refute @new_identity.user.identities.loaded?

      identity = @new_identity.user.identities.detect { |id| id == @identity }
      assert_equal 2, identity.priority
    end

    describe "when the user has a password reset token" do
      it "destroys the token" do
        assert_equal tokens(:minimum_token), subject.verification_tokens.first
        @new_identity.make_primary!
        assert_equal 0, subject.verification_tokens.count(:all)
      end
    end
  end

  describe "changed_to_primary?" do
    subject { users(:minimum_end_user) }

    let (:new_identity) do
      UserEmailIdentity.create!(user: subject, value: "my.new.email@zendesk.com")
    end

    describe "identity change to primary" do
      before do
        new_identity.priority = 1
      end

      it "returns true" do
        assert(new_identity.changed_to_primary?)
      end
    end

    describe "identity change from primary to secondary" do
      before do
        new_identity.make_primary!
        new_identity.priority = 2
      end

      it "returns false" do
        assert_equal false, new_identity.changed_to_primary?
      end
    end
  end

  it "still retrieves identities with an undeliverable_count > 50" do
    user = users(:minimum_end_user)
    assert_equal(8, user.identities.reload.size)

    UserEmailIdentity.create!(user: user, account: user.account, value: "bar@example.com")
    UserEmailIdentity.create!(undeliverable_count: 1, user: user, account: user.account, value: "baz@example.com")

    assert_equal(10, user.identities.reload.size)
    assert_equal user.email, user.identities.first.value

    user.identities.update_all(undeliverable_count: 50)
    assert_equal user.identities.first.value, user.reload.email
  end

  it "is fully destroyed when a user is deleted" do
    user = create_user
    user.current_user = users(:minimum_admin)
    user.delete!
    refute user.is_active?
    assert_equal 0, UserIdentity.where(user_id: user.id).count(:all)
  end

  # note that these tests must be run with "encoding: utf8" set in database.yml or they'll just pass.
  describe "UTF-8 encoding problems" do
    before do
      @account = accounts(:minimum)
    end

    it "does not crash when attempting to find a UTF-8 identity" do
      ident = @account.user_identities.find_by_value("исчезла игра")
      assert ident.nil?
    end

    it "does not crash when attempting to create a UTF-8 identity (it vail with validation error instead)" do
      identity = UserEmailIdentity.create account: @account, value: "исчезла игра@foo.com", user: users(:minimum_end_user)
      refute identity.save
      identity.errors.full_messages.join.must_include "Accented"
    end

    it "does not break with all this tomfoolery" do
      identity = UserIdentity.first
      assert_equal identity, UserIdentity.find_by_value(identity.value)
      # run twice to make sure rails isn't redefining find_by_value
      assert_equal identity, UserIdentity.find_by_value(identity.value)

      # make sure associations still work
      association = identity.account.user_identities
      UserIdentity.expects(:find_by_sql).with do |sql|
        sql.where_sql.must_include 'account_id'
        true
      end.returns([])
      association.find_by_value(identity.value)
    end
  end

  describe "with a deleted user row" do
    before do
      @user = create_user
      @user.current_user = users(:minimum_admin)
      @user.delete!
    end

    it "is not able to create or update an identity on a deleted user" do
      assert_raises(ActiveRecord::RecordInvalid) do
        UserEmailIdentity.create!(user: @user, account: @user.account, value: "foo@foo.com")
      end
    end
  end

  describe "#find_by_email" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
    end

    it "finds users on the account" do
      assert_not_nil @account.user_identities.find_by_email("minimum_end_user@aghassipour.com")
    end

    it "does not find users on other accounts" do
      @account = accounts(:with_groups)
      assert_nil @account.user_identities.find_by_email(@user.email)
    end

    it "returns nil when the email does not exist in the system" do
      assert_nil @account.user_identities.find_by_email("noop@example.com")
    end

    it "does not raise an error when someone enters silly characters" do
      email = "♥lumara♥"
      assert_nil @account.user_identities.find_by_email(email)
    end

    describe "when only searching for verified users" do
      it "finds a user with a verified email" do
        identity = UserEmailIdentity.create!(user: @user, value: "john@example.com", is_verified: true)
        assert_equal identity, @account.user_identities.where(is_verified: true).find_by_email('john@example.com')
      end

      it "does not find a user with an unverified email" do
        UserEmailIdentity.create!(user: @user, value: "john@example.com", is_verified: false)
        assert_nil @account.user_identities.where(is_verified: true).find_by_email('john@example.com')
      end
    end
  end

  describe "#find_by_twitter_screen_name" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
      profile = channels_user_profiles(:minimum_twitter_user_profile_1)
      profile.update_column(:external_id, @account.user_identities.twitter.first.value)
      @screen_name = "@#{profile.name}"
    end

    it "returns a user that exists in the system" do
      assert_not_nil @account.user_identities.find_by_twitter_screen_name(@screen_name)
    end

    it "does not find users on other accounts" do
      account = accounts(:with_groups)
      assert_nil account.user_identities.find_by_twitter_screen_name(@screen_name)
    end

    it "returns nil when presented with a email not matching an identity" do
      assert_nil @account.user_identities.find_by_twitter_screen_name("@abcdef0987654321")
    end

    it "does not raise an error when someone enters silly characters" do
      assert_nil @account.user_identities.find_by_twitter_screen_name("♥lumara♥")
    end
  end

  describe "#find_by_main_identity" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
    end

    it "uses find_by_email when the parameter is an email" do
      UserEmailIdentity.create!(user: @user, value: "admin@example.com", is_verified: true)
      assert_equal @user, @account.user_identities.find_by_main_identity("admin@example.com").user
    end

    it "uses find_by_twitter_screen_name when the parameter is a twitter screen name" do
      profile = channels_user_profiles(:minimum_twitter_user_profile_1)
      profile.update_column(:external_id, @account.user_identities.twitter.first.value)
      assert_not_nil @account.user_identities.find_by_main_identity("@#{profile.screen_name}")
    end
  end

  describe "#find_by_facebook_id" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
      @user.identities << UserFacebookIdentity.create!(value: '1234', user: @user)
    end

    it "finds users on the account" do
      assert_not_nil @account.user_identities.find_by_facebook_id('1234')
    end

    it "does not find users on other accounts" do
      account = accounts(:with_groups)
      assert_nil account.user_identities.find_by_facebook_id('1234')
    end

    it "returns nil when no user can be found" do
      assert_nil @account.user_identities.find_by_facebook_id('1337')
    end
  end

  describe "#verify" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
      @identity = UserEmailIdentity.create!(user: @user, value: "admin@example.com")
      refute @identity.is_verified?
    end

    it "sets is_verified? as true" do
      @identity.verify
      assert @identity.reload.is_verified?
    end
  end

  describe "#verify!" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
      @identity = UserEmailIdentity.create!(user: @user, value: "admin@example.com")
      refute @identity.is_verified?
      @identity.verification_tokens.create!
    end

    it "clears verification tokens and verify" do
      @identity.verify!
      assert @identity.reload.is_verified?
      assert_equal 0, @identity.verification_tokens.count(:all)
    end

    describe "when password is not set" do
      before do
        User.any_instance.stubs(:crypted_password).returns(nil)
      end

      it "verifies" do
        @identity.verify!
        assert @identity.reload.is_verified?
        assert_equal 1, @identity.verification_tokens.count(:all)
      end
    end
  end

  describe 'Given a foreign user' do
    before do
      account = accounts(:minimum)
      @user = User.new(name: 'Foreign user', account: account)
      @user.foreign = true
      assert @user.save, @user.errors.to_xml
    end

    it 'allows new foreign identities' do
      @user.identities << UserForeignIdentity.new(value: 'abc123')
      @user.save!
      @user.reload
      assert_equal(1, @user.identities.size)
    end

    it 'does not allow other identities' do
      new_identity = UserEmailIdentity.new(value: 'foo@example.com')
      @user.identities << new_identity
      refute new_identity.valid?

      @user.reload
      assert_equal(0, @user.identities.size)
    end
  end

  describe 'Trial Account User Identity Limit' do
    before do
      @user = create_user
      @identity = @user.identities.where(type: 'UserEmailIdentity').first
      @account = @identity.account
      @model = :user_identity
      @limit = 1000
      @error_msg = I18n.t("txt.error_message.models.#{@model}_limit_v3")
      FraudScoreJob.stubs(:enqueue_account)
    end

    describe '#validate_trial_user_identity_limit' do
      describe 'when user identity trial limit is enabled' do
        before do
          Account.any_instance.stubs(:whitelisted?).returns(false)
          TrialLimit.stubs(:has_trial_limit?).with(@account, @model).returns(true)
        end

        describe 'limit exceeded' do
          before do
            TrialLimit.stubs(:reached_trial_limit?).with(@account, @model, @limit).returns(true)
          end

          it 'enqueues a fraud score job' do
            FraudScoreJob.expects(:enqueue_account).with(@account, :user_identity_trial_limit_exceeded).once
            UserEmailIdentity.create(user: @user, account: @account, value: 'new-test-email@example.com')
          end

          it 'fails to create new email identity for existing user' do
            identity = UserEmailIdentity.new(user: @user, account: @account, value: 'new-test-email@example.com')

            refute identity.save
            identity.errors.full_messages.join.must_include @error_msg
          end

          it 'fails to update existing email identity' do
            refute @identity.update_attributes(value: 'update-test-email@example.com')
            @identity.errors.full_messages.join.must_include @error_msg
          end

          it 'does not fail when deleting identity' do
            TrialLimit.stubs(:reached_trial_limit?).with(@account, @model, @limit).returns(false)
            @id2 = UserEmailIdentity.create!(user: @user, account: @account, value: 'id2-test-email@example.com')
            TrialLimit.stubs(:reached_trial_limit?).with(@account, @model, @limit).returns(true)

            assert @id2.destroy
          end

          it 'fails to create new non-email identity' do
            identity = UserTwitterIdentity.new(user: @user, account: @account, value: 123)

            refute identity.save
          end

          it 'includes the expected user identity trial limit error' do
            identity = UserEmailIdentity.new(user: @user, account: @account, value: 'new-test-email@example.com')

            identity.save
            assert identity.errors.full_messages.join.include?(I18n.t('txt.error_message.models.user_identity_limit_v3'))
          end
        end

        describe 'below limit' do
          before do
            TrialLimit.stubs(:reached_trial_limit?).with(@account, @model, @limit).returns(false)
            FraudScoreJob.expects(:enqueue_account).never
          end

          it 'validates if new identity is created for existing user' do
            assert UserEmailIdentity.new(user: @user, account: @account, value: 'some-test-email@example.com')
          end

          it 'updates existing identity' do
            assert @identity.update_attributes(value: 'updated-test-email@example.com')
          end

          it 'does not include the user identity trial limit error' do
            identity = UserEmailIdentity.new(user: @user, account: @account, value: 'new-test-email@example.com')

            identity.save
            refute identity.errors.full_messages.join.include?(I18n.t('txt.error_message.models.user_identity_limit_v3'))
          end

          it 'does not disable account' do
            assert @account.is_serviceable
            UserEmailIdentity.create(user: @user, account: @account, value: 'new-test-email@example.com')
            assert @account.is_serviceable
          end

          it 'has is_abusive set to false' do
            refute @account.settings.is_abusive
            UserEmailIdentity.create(user: @user, account: @account, value: 'new-test-email@example.com')
            refute @account.settings.is_abusive
          end
        end
      end

      describe 'when user identity limit is disabled' do
        before do
          TrialLimit.stubs(:has_trial_limit?).with(@account, @model).returns(false)
        end

        it 'creates new email identity for existing user' do
          identity = UserEmailIdentity.new(user: @user, account: @account, value: 'some-test-email@example.com')
          assert identity.save
        end

        it 'updates existing email identity' do
          assert @identity.update_attributes(value: 'new-test-email@example.com')
        end

        it 'creates a new non-email identity' do
          identity = UserTwitterIdentity.new(user: @user, account: @account, value: 123)
          assert identity.save
        end

        it 'does not disable account' do
          assert @account.is_serviceable
          UserEmailIdentity.create(user: @user, account: @account, value: 'new-test-email@example.com')
          assert @account.is_serviceable
        end

        it 'has is_abusive set to false' do
          refute @account.settings.is_abusive
          UserEmailIdentity.create(user: @user, account: @account, value: 'new-test-email@example.com')
          refute @account.settings.is_abusive
        end
      end
    end

    describe '#update_trial_user_identity_count' do
      describe 'when user identity trial limit is enabled' do
        before do
          TrialLimit.stubs(:has_trial_limit?).with(@account, @model).returns(true)
        end

        describe_with_arturo_enabled :orca_classic_increase_model_count_on_update do
          it 'calls #increment_trial_limit_count with correct args' do
            TrialLimit.expects(:increment_trial_limit_count).with(@account, @model, 45.days.to_i).once
            @identity.send("update_trial_#{@model}_count".to_sym)
          end
        end
        describe_with_arturo_disabled :orca_classic_increase_model_count_on_update do
          it 'calls #increment_trial_limit_count with correct args' do
            TrialLimit.expects(:increment_trial_limit_count).never
            @identity.send("update_trial_#{@model}_count".to_sym)
          end
        end
      end

      describe 'when user identity limit is disabled' do
        before do
          TrialLimit.stubs(:has_trial_limit?).with(@account, @model).returns(false)
        end

        it 'does not call #increment_trial_limit_count' do
          TrialLimit.expects(:increment_trial_limit_count).never
          @identity.send("update_trial_#{@model}_count".to_sym)
        end
      end
    end
  end

  describe 'user identity limit validation' do
    before do
      @user = users(:minimum_end_user)
      @user.identities.delete_all
      @user.identities << UserEmailIdentity.new(user: @user, value: 'first@identity.com')
      @user.save
      @user.reload

      @identity_limit = 1
      Account.any_instance.stubs(:settings).returns(stub(
        max_identities: @identity_limit,
        help_center_state: :disabled,
        web_portal_state: false,
        ssl_required?: false,
        ssl_enabled?: false,
        has_user_tags?: false
      ))
    end

    describe_with_arturo_enabled :user_identity_limit do
      before { @identity = UserEmailIdentity.new(user: @user, value: 'aa@example.com') }

      describe "when an account is not whitelisted" do
        before { Account.any_instance.stubs(:whitelisted?).returns(false) }

        it 'invalidates the user identity by adding an error to it' do
          refute @identity.valid?, 'Identity should not be valid because the user identity limit has been reached.'
          refute @identity.save, 'Identity should not be saved because the user identity limit has been reached.'

          expected_error_msg = "The number of identities for this user has exceeded the limit of #{@identity.account.settings.max_identities}. An identity can be an email address, a phone number, a Twitter account, or a Facebook account."
          assert @identity.errors.full_messages.join.must_include(expected_error_msg)
        end

        it 'prevents the creation of the identity' do
          assert_no_difference -> { UserIdentity.count } do
            @identity.save
          end
        end
      end

      describe "when an account is whitelisted" do
        before { UserIdentity.any_instance.stubs(:whitelisted_from_user_limits?).returns(true) }

        it 'allows the saving of the identity' do
          assert @identity.valid?, 'Identity should be valid because the user identity limit is not being checked.'
          assert @identity.save, 'Identity should be valid because the user identity limit is not being checked.'
        end
      end
    end

    describe_with_arturo_disabled :user_identity_limit do
      before { @identity = UserEmailIdentity.new(user: @user, value: 'aa@example.com') }

      it 'allows the saving of the identity' do
        assert @identity.valid?, 'Identity should be valid because the user identity limit is not being checked.'
        assert @identity.save, 'Identity should be valid because the user identity limit is not being checked.'
      end
    end
  end

  describe '.social_identity_exists?' do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_end_user) }

    describe 'facebook identity' do
      let(:fb_identity) { { type: 'facebook', value: '1234' } }

      it 'returns false for new facebook identity' do
        refute account.user_identities.social_identity_exists?(fb_identity)
      end

      it 'returns true for existing facebook identity' do
        user.identities.facebook.create!(value: fb_identity[:value])
        assert account.user_identities.social_identity_exists?(fb_identity)
      end
    end

    describe 'twitter identity' do
      let(:profile) { channels_user_profiles(:minimum_twitter_user_profile_1) }
      let(:identity) { { type: 'twitter', value: profile.name } }

      it "returns false for new twitter identity" do
        refute account.user_identities.social_identity_exists?(identity)
      end

      it 'returns true for existing twitter identity' do
        user.identities.twitter.create!(value: profile.external_id.to_s)
        assert account.user_identities.social_identity_exists?(identity)
      end
    end
  end

  describe "scopes queries to account ID" do
    let(:identity) { user_identities(:minimum_end_user) }

    it "for user association" do
      assert_sql_queries(1, /`users`\.`account_id` IN \(#{identity.account_id}, -1\)/) do
        identity.user
      end
    end
  end
end
