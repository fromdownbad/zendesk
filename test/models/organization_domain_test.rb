require_relative '../support/test_helper'

SingleCov.covered!

describe OrganizationDomain do
  fixtures :organization_domains, :organizations

  let(:organization_domain) { organization_domains(:organization_domain1) }
  let(:account) { organization_domain.account }

  should_validate_presence_of :account
  should_validate_presence_of :organization
  should_validate_presence_of :value

  describe '.valid_values' do
    describe 'when there are valid domain values' do
      it 'returns the valid domain values' do
        values = OrganizationDomain.valid_values(['one.com', 'two.net'])
        assert_equal ['one.com', 'two.net'], values
      end
    end

    describe 'when there are invalid domain values' do
      it 'does not return the invalid domain values' do
        values = OrganizationDomain.valid_values(['one', 'user@two.net'])
        assert_equal [], values
      end
    end

    describe 'when there are both valid and invalid domain values' do
      it 'only returns the valid values' do
        values = OrganizationDomain.valid_values(['one.com', 'two.net', 'one', 'user@two.net'])
        assert_equal ['one.com', 'two.net'], values
      end
    end

    describe 'when passed an empty array' do
      it 'returns an empty array' do
        values = OrganizationDomain.valid_values([])
        assert_equal [], values
      end
    end
  end

  describe '#validate_value' do
    let(:organization) { organizations(:minimum_organization1) }
    before { organization_domain.save }

    describe 'for invalid domains' do
      let(:organization_domain) do
        OrganizationDomain.new(organization: organization, value: 'something')
      end

      it 'adds a validation error' do
        refute_valid organization_domain
        assert_includes organization_domain.errors, :domain_name
        assert_includes organization_domain.errors[:domain_name], "'something' is not a properly formatted domain address"
      end
    end

    describe 'for valid domains' do
      let(:organization_domain) do
        OrganizationDomain.new(organization: organization, value: 'zendesk.com')
      end

      it 'does not add any error' do
        assert_valid organization_domain
        refute_includes organization_domain.errors, :domain_name
      end
    end
  end

  # [EM-4614] Check if any test from Organizaton#domain_names has to be carried over
  describe '#domain_name' do
    it 'is an alias of #value' do
      organization_domain.expects(:value)
      organization_domain.domain_name
    end
  end

  describe 'when given a value with uppercase characters' do
    let(:organization) { organizations(:minimum_organization1) }
    let(:organization_domain) do
      OrganizationDomain.create!(organization: organization, value: 'ZeNdEsK.cOm')
    end

    it 'downcases the value before validating and saving' do
      assert_equal 'zendesk.com', organization_domain.value
    end
  end
end
