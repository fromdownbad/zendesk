require_relative "../../support/test_helper"

SingleCov.covered!

describe ExternalUserData do
  fixtures :external_user_datas

  describe "An ExternalUserData object" do
    before do
      @external_user_data = external_user_datas(:external_user_data_1)
    end

    it "reports state" do
      @external_user_data.data = nil
      refute @external_user_data.data?
      @external_user_data.data = {foo: :bar}
      assert @external_user_data.data?

      @external_user_data.stubs(:stale?).returns(false)
      refute @external_user_data.needs_sync?

      @external_user_data.stubs(:stale?).returns(true)
      assert @external_user_data.needs_sync?

      @external_user_data.stubs(:stale?).returns(false)
      @external_user_data.data = nil
      assert @external_user_data.needs_sync?

      @external_user_data.stubs(:stale?).returns(false)
      @external_user_data.data = {foo: :bar}
      @external_user_data.sync_errored!
      assert @external_user_data.needs_sync?
    end

    it "syncs data" do
      @external_user_data.data = nil
      @external_user_data.sync!(foo: :bar)
      assert @external_user_data.data?
      assert_equal ExternalUserData::SyncStatus::SYNC_OK, @external_user_data.sync_status
    end

    it "records error state" do
      refute @external_user_data.sync_errored?
      @external_user_data.sync_errored!
      assert @external_user_data.sync_errored?
    end
  end
end
