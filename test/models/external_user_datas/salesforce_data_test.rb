require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe SalesforceData do
  fixtures :users, :external_user_datas

  describe "#start_sync" do
    before do
      @salesforce_data = external_user_datas(:external_user_data_1)
    end

    it "reports properties" do
      assert_nil @salesforce_data.status
      assert_nil @salesforce_data.records

      @salesforce_data.sync!(records: ["a", "b"])

      assert_equal "ok", @salesforce_data.status
      assert_equal ["a", "b"], @salesforce_data.records

      @salesforce_data.sync_errored!
      assert_equal "errored", @salesforce_data.status

      @salesforce_data.sync_errored!(records: ["c", "d"])
      assert_equal "errored", @salesforce_data.status
      assert_equal ["c", "d"], @salesforce_data.records
    end

    it "schedules a data sync" do
      SalesforceSyncJob.expects(:enqueue)
      @salesforce_data.start_sync(users(:minimum_end_user))
      assert_equal ExternalUserData::SyncStatus::SYNC_PENDING, @salesforce_data.sync_status
    end
  end

  describe "#delete_cache" do
    let(:salesforce_data) { external_user_datas(:external_user_data_1) }
    it "deletes data" do
      assert_equal salesforce_data, salesforce_data.delete_cache
    end
  end
end
