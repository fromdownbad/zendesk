require_relative "../../support/test_helper"

SingleCov.covered!

describe Explore::Subscription do
  let(:account)      { accounts(:minimum) }
  let(:product_name) { Explore::Subscription::PRODUCT_NAME }
  let(:client)       { stub }
  let(:subject)      { Explore::Subscription.find_for_account(account) }

  let(:expected_product) do
    Zendesk::Accounts::Product.new(
      'state'            => Zendesk::Accounts::Product::SUBSCRIBED,
      'name'             => :explore,
      'trial_expires_at' => 10.days.ago,
      'plan_settings'    => {
        'plan_type' => 3,
        'max_agents' => 5
      }
    )
  end

  before do
    Zendesk::Accounts::Client.stubs(new: client)
    client.stubs(product: expected_product)
  end

  it 'inherits from AccountServiceSubscription' do
    assert_equal AccountServiceSubscription, Explore::Subscription.superclass
  end

  it 'has the expected product name' do
    assert_equal 'explore', Explore::Subscription::PRODUCT_NAME
  end

  it 'specifies the expected valid attributes' do
    expected_attrs = [:plan_type, :max_agents]
    assert_equal expected_attrs, Explore::Subscription::PLAN_SETTINGS
  end

  describe '#include_in_easy_agent_add?' do
    let(:original) { :subscribed? }
    let(:aliased)  { :include_in_easy_agent_add? }

    it 'equals #subscribed?' do
      assert_equal subject.method(original), subject.method(aliased)
    end
  end

  describe 'plan settings accessors' do
    describe '#plan_type' do
      it 'returns the plan_type stored in account service plan_settings' do
        expected_plan_type = expected_product.plan_settings['plan_type']
        assert_equal expected_plan_type, subject.plan_type
      end
    end

    describe '#plan_name' do
      let(:plan_type) { 1 }

      it 'returns the description of the plan_type in ZBC' do
        expected_plan_name = ZBC::Explore::PlanType[plan_type].try(:description)
        subject.stubs(plan_type: plan_type)
        assert_equal expected_plan_name, subject.plan_name
      end
    end

    describe '#max_agents' do
      it 'returns the max_agents stored in account service plan_settings' do
        expected_max_agents = expected_product.plan_settings['max_agents']
        assert_equal expected_max_agents, subject.max_agents
      end
    end
  end

  describe '#inspect' do
    let(:expected) do
      "#<Explore::Subscription:#{subject.object_id} " \
        "state=#{subject.state} " \
        "plan_type=#{subject.plan_type} " \
        "max_agents=#{subject.max_agents} " \
        "trial_expires_at=#{subject.trial_expires_at}>"
    end

    it 'displays the expected attributes' do
      assert_equal expected, subject.inspect
    end
  end
end
