require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Voice::VoiceAccount do
  fixtures :accounts, :users

  let(:described_class) { Voice::VoiceAccount }

  describe '.remote_data_for_account' do
    subject { described_class.remote_data_for_account(account) }

    describe 'when the account is voice_enabled' do
      let(:account) { accounts(:minimum) }

      let(:request_path) { %r{/api/v2/channels/voice/voice_account.json} }

      describe 'when VoiceAccount record exists' do
        let(:body)    { read_test_file('voice/voice_account.json') }
        let(:headers) { { content_type: 'application/json' } }

        before do
          account.stubs(:voice_enabled?).returns(true)

          stub_request(:get, request_path).
            to_return(body: body, headers: headers)
        end

        it 'returns a voice account object' do
          assert_equal subject.id,        1
          assert_equal subject.plan_type, 2
          assert_equal subject.plan_name, 'advanced'
        end

        it 'returns the serialized creation date' do
          assert_kind_of(DateTime, subject.created_at)
        end
      end

      describe 'when external call encounters any Zendesk::Voice::RESCUABLE_ERRORS' do
        before do
          account.stubs(:voice_enabled?).returns(true)
          stub_request(:get, request_path).to_raise(Faraday::TimeoutError)

          mock_statsd_obj = Minitest::Mock.new
          # let statsd.increment returns 641
          mock_statsd_obj.expect(:increment, 641, [String, Hash])

          Zendesk::StatsD::Client.stubs(:new).returns(mock_statsd_obj)
        end

        it 'returns nil' do
          assert_nil subject
          Voice::VoiceAccount::RemoteDataDelegator.expects(:new).never
        end
      end

      describe 'when a Kragle::ResourceNotFound error is raised' do
        before do
          stub_request(:get, request_path).
            to_raise(Kragle::ResourceNotFound)
        end

        it 'returns nil' do
          refute subject
        end
      end

      describe 'when a NetworkError is raised' do
        before do
          stub_request(:get, request_path).
            to_raise(Faraday::Error::ConnectionFailed)
        end

        it 'returns nil' do
          refute subject
        end
      end
    end

    describe 'when the account is not voice_enabled' do
      let(:account) { stub(voice_enabled?: false) }

      it 'returns nil' do
        refute subject
      end
    end
  end

  describe '#user_seats_enabled' do
    describe 'with lotus_feature_voice_staff_service_roles arturo on' do
      before do
        Arturo.enable_feature!(:lotus_feature_voice_staff_service_roles)
      end

      it 'returns true' do
        account = accounts(:minimum)
        voice_account = FactoryBot.create(:voice_account, account: account, plan_type: 3)

        assert voice_account.user_seats_enabled?
      end
    end

    describe 'with lotus_feature_voice_staff_service_roles arturo off' do
      it 'returns true for USER_SEATS_PLANS' do
        account = accounts(:minimum)
        voice_account = FactoryBot.create(:voice_account, account: account, plan_type: 2)

        assert voice_account.user_seats_enabled?
      end

      it 'returns false for non USER_SEATS_PLANS' do
        account = accounts(:minimum)
        voice_account = FactoryBot.create(:voice_account, account: account, plan_type: 3)

        refute voice_account.user_seats_enabled?
      end
    end
  end

  describe Voice::VoiceAccount::RemoteDataDelegator do
    let(:described_class) { Voice::VoiceAccount::RemoteDataDelegator }

    describe '#show_trial_or_legacy_voice_subscription?' do
      let(:delegator) { described_class.new(remote_data) }

      let(:remote_data) do
        stub(
          body: {
            'voice_account' => {
              billing_plan_type: billing_plan_type,
              trial:             trial,
              trial_expired:     trial_expired
            }
          }
        )
      end

      subject { delegator.show_trial_or_legacy_voice_subscription? }

      describe 'when its billing_plan_type is not present' do
        let(:billing_plan_type) { nil }
        let(:trial)             { anything }
        let(:trial_expired)     { anything }

        it 'returns true' do
          assert subject
        end
      end

      describe 'when its billing_plan_type is present' do
        let(:billing_plan_type) { stub }

        describe 'when it is an expired trial' do
          let(:trial_expired) { true }
          let(:trial)         { anything }

          it 'returns false' do
            refute subject
          end
        end

        describe 'when it is not an expired trial' do
          let(:trial_expired) { false }

          describe 'when it is a trial' do
            let(:trial) { true }

            it 'returns true' do
              assert subject
            end
          end

          describe 'when it not a trial' do
            let(:trial) { false }

            it 'returns false' do
              refute subject
            end
          end
        end
      end
    end
  end
end
