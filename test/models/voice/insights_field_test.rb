require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Voice::InsightsField do
  fixtures :accounts, :subscriptions, :ticket_fields

  describe '.filter_fields_for_account' do
    it 'filters VoiceInsights ticket fields from `fields` collection' do
      account = accounts(:minimum)
      id = account.ticket_fields.first.id

      FactoryBot.create(:voice_insights_field, account_id: account.id, ticket_field_id: id, label: 'test_field')
      result = Voice::InsightsField.filter_fields_for_account(account.ticket_fields, account.id)
      refute_includes result.map(&:id), id
    end
  end
end
