require_relative "../../../support/test_helper"

SingleCov.covered!

describe Voice::PartnerEditionAccount::AccountServiceIntegration do
  let(:account) { accounts(:minimum) }

  before do
    Zendesk::Accounts::Client.any_instance.
      stubs(:update_or_create_product!)
    Timecop.freeze
  end

  describe 'when a TPE account is created' do
    before do
      Zendesk::Accounts::Client.any_instance.
        expects(:update_or_create_product!).once.
        with('talk_partner', create_product_params, context: "voice_partner_edition_integration")
    end

    let(:create_product_params) do
      {
        product: {
          state:            :trial,
          trial_expires_at: 30.days.from_now.utc.iso8601,
          plan_settings: {
            plan_type:            1,
            max_agents:           10,
            has_talk_cti_partner: false
          }
        }
      }
    end

    it 'publishes the account details to pravda' do
      Voice::PartnerEditionAccount.find_or_create_by_account(account)
    end
  end

  describe 'when a TPE account is updated' do
    describe 'for purchase TPE' do
      before do
        FactoryBot.create(:tpe_subscription, account: account)
        Zendesk::Accounts::Client.any_instance.
          expects(:update_or_create_product!).once.
          with('talk_partner', update_product_params, context: "voice_partner_edition_integration")
      end

      let(:update_product_params) do
        {
          product: {
            trial_expires_at: 30.days.from_now.utc.iso8601,
            plan_settings: {
              plan_type:            1,
              max_agents:           2,
              has_talk_cti_partner: false
            }
          }
        }
      end

      it 'publishes the account details to pravda' do
        account.voice_partner_edition_account.subscribe!
      end
    end

    describe 'for TPE trial' do
      let(:tpe_account) do
        account.voice_partner_edition_account
      end

      let(:create_product_params) do
        {
          product: {
            state:            :trial,
            trial_expires_at: 30.days.from_now.utc.iso8601,
            plan_settings: {
              plan_type:            1,
              max_agents:           10,
              has_talk_cti_partner: false,
            }
          }
        }
      end

      before do
        Zendesk::Accounts::Client.any_instance.
          stubs(:update_or_create_product!).once.
          with('talk_partner', create_product_params, context: "voice_partner_edition_integration")

        Voice::PartnerEditionAccount.find_or_create_by_account(account)
      end

      describe 'and the trial expiry date has come to pass' do
        let(:update_product_params) do
          {
            product: {
              state:            :expired,
              trial_expires_at: 30.days.from_now.utc.iso8601,
              plan_settings: {
                plan_type:            1,
                max_agents:           10,
                has_talk_cti_partner: false
              }
            }
          }
        end

        it 'updates the account details in pravda' do
          # NOTE: There is a job that runs every two hours that will check for
          # records where the :trial attribute is true and whose
          # :trial_expires_at attribute value is in the past. Records that
          # match this criteria are soft-deleted and recreated with the
          # :active attribute set to false.
          Zendesk::Accounts::Client.any_instance.
            expects(:update_or_create_product!).once.
            with('talk_partner', update_product_params, context: "voice_partner_edition_integration")

          Timecop.freeze(31.days.from_now) do
            tpe_account.deactivate!
          end
        end
      end

      describe 'and it was updated due to plan_type change' do
        let(:update_product_params) do
          {
            product: {
              state:            :trial,
              trial_expires_at: 30.days.from_now.utc.iso8601,
              plan_settings: {
                plan_type:            0,
                max_agents:           10,
                has_talk_cti_partner: false
              }
            }
          }
        end

        it 'does not update the account details in pravda' do
          tpe_account.update_attribute(:plan_type, 0)
        end
      end

      describe 'and it was updated to subscribe' do
        let(:update_product_params) do
          {
            product: {
              trial_expires_at: 30.days.from_now.utc.iso8601,
              plan_settings: {
                plan_type:            1,
                max_agents:           10,
                has_talk_cti_partner: false
              }
            }
          }
        end

        it 'updates the account details in pravda' do
          Zendesk::Accounts::Client.any_instance.
            expects(:update_or_create_product!).once.
            with('talk_partner', update_product_params, context: "voice_partner_edition_integration")

          tpe_account.subscribe!
        end
      end
    end
  end
end
