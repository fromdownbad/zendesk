require_relative "../../support/test_helper"

SingleCov.covered!

describe Voice::RechargeSettings do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:gbp_account) do
    gbp_account = account
    gbp_account.subscription.update_attribute(:currency_type, CurrencyType.GBP)
    gbp_account
  end
  let(:eur_account) do
    eur_account = account
    eur_account.subscription.update_attribute(:currency_type, CurrencyType.EUR)
    eur_account
  end

  describe "creating voice recharge settings" do
    describe "for USD account" do
      subject { Voice::RechargeSettings.create(account: account) }

      it "has amount $50.00" do
        assert_equal 50.00, subject.amount
      end

      it "has minimum balance $5.00" do
        assert_equal 5.00, subject.minimum_balance
      end

      it "is enabled" do
        assert subject.enabled
      end
    end

    describe "for GBP account" do
      subject { Voice::RechargeSettings.create(account: gbp_account) }

      it "has amount 50.00" do
        assert_equal 50.00, subject.amount
      end

      it "has minimum balance 5.00" do
        assert_equal 5.00, subject.minimum_balance
      end

      it "is enabled" do
        assert subject.enabled
      end
    end

    describe "for EUR account" do
      subject { Voice::RechargeSettings.create(account: eur_account) }

      it "has amount 50.00" do
        assert_equal 50.00, subject.amount
      end

      it "has minimum balance 5.00" do
        assert_equal 5.00, subject.minimum_balance
      end

      it "is enabled" do
        assert subject.enabled
      end
    end
  end

  describe "updating voice recharge settings" do
    describe "when amount is nil" do
      subject { Voice::RechargeSettings.create(account: account) }

      before do
        subject.update_attributes(amount: nil)
      end

      it "stores amount error" do
        assert subject.errors[:amount].present?
      end
    end

    describe "when amount is not in the inclusion list" do
      subject { Voice::RechargeSettings.create(account: account) }

      before do
        subject.update_attributes(amount: 0.10)
      end

      it "stores amount error" do
        assert subject.errors[:amount].present?
      end
    end

    describe "when amount is in the inclusion list" do
      subject { Voice::RechargeSettings.create(account: account) }

      before do
        subject.update_attributes(amount: 100)
      end

      it "sets amount" do
        assert_equal 100.00, subject.amount
      end

      it "sets balance" do
        assert_equal 10.00, subject.minimum_balance
      end

      it "does not store amount error" do
        assert subject.errors[:amount].blank?
      end
    end
  end
end
