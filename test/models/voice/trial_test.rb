require_relative "../../support/test_helper"

SingleCov.covered!

describe Voice::Trial do
  describe '.product_payload' do
    let(:expected_payload) do
      {
        product: {
          state: Zendesk::Accounts::Product::NOT_STARTED,
          plan_settings: {
            plan_type: ZBC::Voice::PlanType::Professional.plan_type,
            suite: true
          }
        }
      }
    end

    it 'returns the correct product payload to send to Account Service' do
      assert_equal(expected_payload, Voice::Trial.product_payload)
    end
  end
end
