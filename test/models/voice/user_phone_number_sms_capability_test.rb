require_relative "../../support/test_helper"

SingleCov.covered!

describe Voice::UserPhoneNumberSmsCapability do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }

  describe '#attribute_type' do
    it 'is 2' do
      Voice::UserPhoneNumberSmsCapability.new.attribute_type.must_equal 2
    end
  end

  describe '#available?' do
    it 'when value is 1, returns true' do
      Voice::UserPhoneNumberSmsCapability.new(value: '1').available?.must_equal true
    end

    it 'when value NOT 1, returns false' do
      Voice::UserPhoneNumberSmsCapability.new(value: '0').available?.must_equal false
    end
  end
end
