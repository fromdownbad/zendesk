require_relative "../../support/test_helper"

SingleCov.covered!

describe Voice::PartnerEditionAccount do
  let(:account) { accounts(:minimum) }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
  end

  describe "#audit_attribute" do
    let(:account) { accounts(:minimum) }
    let(:voice_partner_edition_account) { FactoryBot.create(:voice_partner_edition_account, account_id: account.id) }

    before do
      assert_equal 0, voice_partner_edition_account.cia_events.size
    end

    it "audits action changes" do
      CIA.audit actor: users(:minimum_agent) do
        voice_partner_edition_account.update_attribute(:active, false)
      end

      cia_event = voice_partner_edition_account.reload.cia_events.first
      assert cia_event.present?
      assert_equal cia_event.action, "update"
      assert_equal cia_event.visible, false
    end

    it "audits plan_type changes" do
      CIA.audit actor: users(:minimum_agent) do
        voice_partner_edition_account.update_attribute(:plan_type, Voice::PartnerEditionAccount::PLAN_TYPE[:legacy])
      end

      cia_event = voice_partner_edition_account.reload.cia_events.first
      assert cia_event.present?
      assert_equal cia_event.action, "update"
      assert_equal cia_event.visible, false
    end

    it "audits trial_expires_at changes" do
      Timecop.travel(Time.now + 1.seconds)
      CIA.audit actor: users(:minimum_agent) do
        voice_partner_edition_account.update_attribute(:trial_expires_at, 30.days.from_now)
      end

      cia_event = voice_partner_edition_account.reload.cia_events.first
      assert cia_event.present?
      assert_equal cia_event.action, "update"
    end
  end

  describe "#legacy?" do
    it "returns true when plan is legacy" do
      FactoryBot.create(
        :voice_partner_edition_account,
        account_id: account.id,
        plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:legacy]
      )
      assert account.voice_partner_edition_account.legacy?
    end

    it "returns false when plan is regular" do
      FactoryBot.create(
        :voice_partner_edition_account,
        account_id: account.id
      )
      assert !account.voice_partner_edition_account.legacy?
    end
  end

  describe ".find_or_create_by_account" do
    describe "when account does not have a partner_edition_account" do
      it "creates an active regular partner edition account" do
        assert_difference 'Voice::PartnerEditionAccount.where(account_id: account.id, active: true, plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:regular]).count', 1 do
          ::Voice::PartnerEditionAccount.find_or_create_by_account(account)
        end
      end

      it "sets trial_expires_at to 30 days from now" do
        travel_to Time.now do
          voice_partner_edition_account = ::Voice::PartnerEditionAccount.find_or_create_by_account(account)
          assert_equal 30.days.from_now, voice_partner_edition_account.trial_expires_at
        end
      end

      it "sets trial flag to true" do
        voice_partner_edition_account = ::Voice::PartnerEditionAccount.find_or_create_by_account(account)
        assert(voice_partner_edition_account.trial)
      end
    end

    describe "when account has a partner_edition_account" do
      it "doesn't create a partner edition account" do
        FactoryBot.create(
          :voice_partner_edition_account,
          account_id: account.id
        )

        assert_difference 'Voice::PartnerEditionAccount.where(account_id: account.id).count', 0 do
          ::Voice::PartnerEditionAccount.find_or_create_by_account(account)
        end
      end

      it 'returns first account that has not been deleted' do
        FactoryBot.create(:voice_partner_edition_account, account: account).soft_delete!
        partner_edition_account = FactoryBot.create(:voice_partner_edition_account, account: account)

        assert_equal Voice::PartnerEditionAccount.find_or_create_by_account(account), partner_edition_account
      end

      it "doesn't update trial_expires_at for existing account" do
        voice_partner_account = FactoryBot.create(
          :voice_partner_edition_account,
          account_id: account.id,
          trial_expires_at: 20.days.from_now
        ).reload

        found_partner_account = ::Voice::PartnerEditionAccount.find_or_create_by_account(account)

        assert_equal found_partner_account.trial_expires_at, voice_partner_account.trial_expires_at
      end
    end
  end

  describe '#update_active_flag' do
    let(:partner_edition_account) { FactoryBot.create(:voice_partner_edition_account, account_id: account.id) }

    it 'does not set active to true when trial_expires_at changes to nil' do
      partner_edition_account.create_and_delete(active: false)

      partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)
      partner_edition_account.create_and_delete(trial_expires_at: nil)

      partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

      assert_equal false, partner_edition_account.active?
    end

    it 'does not set active to true when trial_expires_at does not change' do
      partner_edition_account.create_and_delete(trial_expires_at: nil, active: false)

      partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)
      partner_edition_account.create_and_delete(plan_type: 1)

      partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

      assert_equal false, partner_edition_account.active?
    end

    it 'sets active to true when trial_expires_at changes to new value' do
      partner_edition_account.create_and_delete(trial_expires_at: nil, active: false)

      partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)
      partner_edition_account.create_and_delete(trial_expires_at: 3.days.from_now)

      partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

      assert(partner_edition_account.active?)
    end
  end

  describe '#subscribe!' do
    let(:partner_edition_account) { FactoryBot.create(:voice_partner_edition_account, active: false, account: account, trial_expires_at: 3.days.from_now) }

    it 'sets trial to false, active to true and does not change trial_expires_at on new account' do
      partner_edition_account.subscribe!

      new_partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

      assert_not_nil new_partner_edition_account.trial_expires_at
      assert_equal false, new_partner_edition_account.trial?
      assert(new_partner_edition_account.active?)
      assert_equal 2, Voice::PartnerEditionAccount.with_deleted { Voice::PartnerEditionAccount.count }
    end
  end

  describe '#deactivate!' do
    let(:partner_edition_account) { FactoryBot.create(:voice_partner_edition_account, account: account) }

    it 'sets new account active flag to false' do
      partner_edition_account

      partner_edition_account.deactivate!

      new_partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

      assert_equal false, new_partner_edition_account.active?
      assert_equal 2, Voice::PartnerEditionAccount.with_deleted { Voice::PartnerEditionAccount.count }
    end
  end

  describe '#in_trial?' do
    let(:partner_edition_account) { FactoryBot.build(:voice_partner_edition_account) }

    it 'returns true when trial and active are true' do
      partner_edition_account.trial = true
      partner_edition_account.active = true

      assert(partner_edition_account.in_trial?)
    end

    it 'returns false when trial is false and active is true' do
      partner_edition_account.trial = false
      partner_edition_account.active = true

      assert_equal false, partner_edition_account.in_trial?
    end

    it 'returns false when trial is true and active is false' do
      partner_edition_account.trial = true
      partner_edition_account.active = false

      assert_equal false, partner_edition_account.in_trial?
    end

    it 'returns false when trial and active are false' do
      partner_edition_account.trial = false
      partner_edition_account.active = false

      assert_equal false, partner_edition_account.in_trial?
    end
  end

  describe '#updated_subscription!' do
    let(:partner_edition_account) { Voice::PartnerEditionAccount.find_or_create_by_account(account) }
    let(:partner_edition_subscription) { FactoryBot.build(:tpe_subscription) }

    it 'subscribes voice partner edition account when subscription is active' do
      Voice::PartnerEditionAccount.any_instance.expects(:subscribe!)
      partner_edition_subscription.active = true

      partner_edition_account.updated_subscription!(partner_edition_subscription)
    end

    it 'deactivates voice partner edition account when subscription is not active' do
      Voice::PartnerEditionAccount.any_instance.expects(:deactivate!)
      partner_edition_subscription.active = false

      partner_edition_account.updated_subscription!(partner_edition_subscription)
    end
  end

  describe '#create_and_delete' do
    let(:partner_edition_account) { FactoryBot.create(:voice_partner_edition_account, account: account) }

    before do
      Timecop.freeze(Time.now)
    end

    it 'returns true when new record is created successfully' do
      assert(partner_edition_account.create_and_delete(active: true))
    end

    it 'returns false when new record is not created successfully' do
      assert_equal false, partner_edition_account.create_and_delete(plan_type: -1)
    end

    it 'creates new record with updated fields' do
      partner_edition_account.create_and_delete(active: true)
      new_partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

      assert_not_equal partner_edition_account.id, new_partner_edition_account.id
      assert(new_partner_edition_account.active)
      assert_equal partner_edition_account.plan_type, new_partner_edition_account.plan_type
      assert_equal partner_edition_account.trial, new_partner_edition_account.trial
      assert_equal partner_edition_account.trial_expires_at.to_a, new_partner_edition_account.trial_expires_at.to_a
    end

    it 'marks old record as deleted' do
      partner_edition_account.create_and_delete(active: true)

      assert_equal Time.now, partner_edition_account.deleted_at
    end

    describe 'when account is not subscribed (trial: true)' do
      it 'sets active flag to true if trial_expires_at is in future' do
        partner_edition_account.update_attributes(active: false, trial: true)
        partner_edition_account.create_and_delete(trial_expires_at: 3.days.from_now)

        new_partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

        assert(new_partner_edition_account.active)
        assert(new_partner_edition_account.trial)
      end

      it 'does not set active flag to true if trial_expires_at is in past' do
        partner_edition_account.update_attributes(active: false, trial: true)
        partner_edition_account.create_and_delete(trial_expires_at: 3.days.ago)

        new_partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

        refute(new_partner_edition_account.active)
        assert(new_partner_edition_account.trial)
      end

      it 'does not set active flag to true if trial_expires_at is not updated' do
        partner_edition_account.update_attributes(active: false, trial: true)
        partner_edition_account.create_and_delete(plan_type: 0)

        new_partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

        refute(new_partner_edition_account.active)
        assert(new_partner_edition_account.trial)
      end
    end

    describe 'when account is subscribed (trial: false)' do
      it 'does not set active or trial flag to true if trial_expires_at is in future' do
        partner_edition_account.update_attributes(active: false, trial: false)
        partner_edition_account.create_and_delete(trial_expires_at: 3.days.from_now)

        new_partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

        refute(new_partner_edition_account.active)
        refute(new_partner_edition_account.trial)
      end

      it 'does not set active or trial flag to true if trial_expires_at is in past' do
        partner_edition_account.update_attributes(active: false, trial: false)
        partner_edition_account.create_and_delete(trial_expires_at: 3.days.ago)

        new_partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)

        refute(new_partner_edition_account.active)
        refute(new_partner_edition_account.trial)
      end
    end
  end
end
