require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Voice::Subscription do
  fixtures :accounts, :subscriptions, :role_settings

  let(:account) { accounts(:minimum) }
  let(:voice_subscription) do
    FactoryBot.create(:voice_subscription, account_id: account.id)
  end

  describe 'when created' do
    before { ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue) }

    it "belongs to an account" do
      assert voice_subscription.account.present?
    end

    it "has a plan type" do
      assert_equal 1, voice_subscription.plan_type
    end

    describe 'audit log' do
      let(:audit) { CIA::Event.last }

      before do
        Voice::UpdateVoiceJob.stubs(:enqueue)
        assert_nil CIA::Event.last
      end

      describe 'requirements' do
        let(:audited_attributes) do
          %w[
            account_id
            plan_type
            max_agents
            suspended
            is_prepaid
          ]
        end

        it 'is required for specific attributes' do
          assert_equal audited_attributes, voice_subscription.audited_attributes
        end
      end

      describe 'updating any of the auditable attributes' do
        before do
          CIA.audit actor: account.owner do
            voice_subscription.update_attributes!(max_agents: 1)
          end
        end

        it 'generates an audit log' do
          assert_equal 'update', audit.action
          assert_equal account.owner, audit.actor
          assert_equal voice_subscription, audit.source
        end
      end
    end

    describe '#is_active?' do
      before  { voice_subscription.stubs(:suspended?).returns(suspended) }
      subject { voice_subscription.is_active? }

      describe 'when suspended? is true' do
        let(:suspended) { true }

        it { refute subject }
      end

      describe 'when suspended? is false' do
        let(:suspended) { false }

        it { assert subject }
      end
    end

    describe '#include_in_easy_agent_add?' do
      let(:original) { :is_active? }
      let(:aliased)  { :include_in_easy_agent_add? }

      subject { voice_subscription }

      it 'equals #is_active?' do
        assert_equal subject.method(original), subject.method(aliased)
      end
    end

    describe '#suspend!' do
      it 'changes the suspended status to true and queues a job to update voice' do
        stub_request(:put, "https://minimum.zendesk-test.com/api/v2/channels/voice/internal/sub_accounts/subscription_updated.json")
        voice_subscription.suspend!
        assert(voice_subscription.suspended?)
      end
    end

    describe "#plan_name" do
      describe "when plan type is 'Basic'" do
        before do
          voice_subscription.update_column(
            :plan_type,
            ZBC::Voice::PlanType::Basic.plan_type
          )
        end

        it "is 'Basic'" do
          assert_equal 'Basic', voice_subscription.plan_name
        end
      end

      describe "when plan type is 'Advanced'" do
        before do
          voice_subscription.update_column(
            :plan_type,
            ZBC::Voice::PlanType::Advanced.plan_type
          )
        end

        it "is 'Advanced'" do
          assert_equal 'Advanced', voice_subscription.plan_name
        end
      end

      describe "when plan type is non-existent" do
        before do
          voice_subscription.update_column(:plan_type, -1)
        end

        it "is nil" do
          assert_nil voice_subscription.plan_name
        end
      end
    end

    describe '#original_voice_plan_type?' do
      describe "when plan type is pre 2017 repackage" do
        before do
          voice_subscription.update_column(
            :plan_type,
            ZBC::Voice::PlanType::Advanced.plan_type
          )
        end

        it "return true" do
          assert voice_subscription.original_voice_plan_type?
        end
      end

      describe "when plan type is post 2017 repackage" do
        before do
          voice_subscription.update_column(
            :plan_type,
            ZBC::Voice::PlanType::Team.plan_type
          )
        end

        it "return false" do
          refute voice_subscription.original_voice_plan_type?
        end
      end
    end

    describe '#notify_voice_of_creation' do
      describe_with_arturo_disabled :voice_disable_subscription_notifications do
        it 'notifies voice when it is created' do
          s = FactoryBot.build(:voice_subscription, account: account)
          s.unstub(:notify_voice_of_creation)
          Voice::CreateVoiceJob.expects(:enqueue).with(account.id, s.plan_type).once
          assert s.save
        end

        it 'does not notify voice when it is updated' do
          s = FactoryBot.build(:voice_subscription, account: account)
          s.unstub(:notify_voice_of_creation)
          Voice::CreateVoiceJob.expects(:enqueue).with(account.id, s.plan_type).once

          s.save
          Voice::UpdateVoiceJob.expects(:enqueue)
          assert s.update_attributes(max_agents: 10, plan_type: 1)
        end
      end

      describe_with_arturo_enabled :voice_disable_subscription_notifications do
        it 'does not notify voice when it is created' do
          s = FactoryBot.build(:voice_subscription, account: account)
          s.unstub(:notify_voice_of_creation)
          Voice::CreateVoiceJob.expects(:enqueue).with(account.id, s.plan_type).never
          assert s.save
        end
      end
    end

    describe '#allow_voice_trial' do
      it 'changes the voice_trial_enabled settings to true' do
        s = FactoryBot.build(:voice_subscription, account: account)

        account.settings.voice_trial_enabled = false
        account.settings.save!

        s.save!

        account.settings.reload
        assert account.settings.voice_trial_enabled?
      end
    end

    describe '#update_voice_seats' do
      before do
        UserSeat.any_instance.stubs(:mark_as_unavailable_on_voice)
        stub_request(:post, %r{/api/v2/channels/voice/availabilities/\d+/voice_seat_lost.json})
      end

      describe "without arturo voice_downgrade_agents_support" do
        before do
          Arturo.disable_feature!(:voice_downgrade_agents_support)
        end

        it "destroys seats for the least active agents when a created subscription doesn't cover existing seats" do
          user_1 = FactoryBot.create(:agent, account: account)
          user_2 = FactoryBot.create(:agent, account: account)

          body = {
            "recent_active_agents" => [user_1.id]
          }.to_json

          req_stub =
            stub_request(:get, %r{/api/v2/channels/voice/internal/sub_accounts/recent_active_agents.json}).
              to_return(body: body, headers: { content_type: 'application/json' })

          FactoryBot.create(:voice_user_seat, account: account, user: user_1)
          FactoryBot.create(:voice_user_seat, account: account, user: user_2)

          s = FactoryBot.build(:voice_subscription, account: account, max_agents: 1)
          s.unstub(:recent_active_agent_ids)
          s.save

          assert_requested(req_stub)
          assert_equal 1, account.user_seats.voice.count(:all)
          assert UserSeat.where(user_id: user_1.id).exists?
          refute UserSeat.where(user_id: user_2.id).exists?
        end
      end

      describe "with arturo voice_downgrade_agents_support" do
        before do
          Arturo.enable_feature!(:voice_downgrade_agents_support)
        end

        it "destroys seats order by user created_at" do
          user_1 = FactoryBot.create(:agent, account: account)
          user_2 = FactoryBot.create(:agent, created_at: (user_1.created_at + 2.minutes), account: account)

          FactoryBot.create(:voice_user_seat, account: account, user: user_1)
          FactoryBot.create(:voice_user_seat, account: account, user: user_2)

          s = FactoryBot.build(:voice_subscription, account: account, max_agents: 1)
          s.save

          assert_equal 1, account.user_seats.voice.count(:all)
          assert UserSeat.where(user_id: user_1.id).exists?
          refute UserSeat.where(user_id: user_2.id).exists?
        end
      end

      it "doesn't destroy seats for agents with no voice availability when a created subscribtion covers them" do
        voice_active_user = FactoryBot.create(:agent, account: account)
        voice_inactive_user_1 = FactoryBot.create(:agent, account: account)
        voice_inactive_user_2 = FactoryBot.create(:agent, account: account)

        FactoryBot.create(:voice_user_seat, account: account, user: voice_active_user)
        FactoryBot.create(:voice_user_seat, account: account, user: voice_inactive_user_1)
        FactoryBot.create(:voice_user_seat, account: account, user: voice_inactive_user_2)

        Voice::Subscription.any_instance.stubs(:recent_active_agent_ids).returns([voice_active_user.id])
        s = FactoryBot.build(:voice_subscription, account: account, max_agents: 2)
        s.unstub(:recent_active_agent_ids)
        s.save

        assert_equal 2, account.user_seats.voice.count(:all)
      end

      it "doesn't destroy seats when a created subscription covers existing seats" do
        agent_1 = FactoryBot.create(:agent, account: account)
        agent_2 = FactoryBot.create(:agent, account: account)

        FactoryBot.create(:voice_user_seat, account: account, user: agent_1)
        FactoryBot.create(:voice_user_seat, account: account, user: agent_2)
        FactoryBot.create(:voice_subscription, account: account, max_agents: 2)

        assert_equal 2, account.user_seats.voice.count(:all)
      end
    end

    describe '#notify_voice_of_update' do
      describe_with_arturo_disabled :voice_disable_subscription_notifications do
        it 'does not notify voice after create' do
          Voice::UpdateVoiceJob.expects(:enqueue).never
          FactoryBot.create(:voice_subscription, account: account, max_agents: 2)
        end

        it 'does not notify voice after an update rollback' do
          subscription = FactoryBot.create(:voice_subscription, account: account, max_agents: 2)
          subscription.plan_type = 2
          Voice::UpdateVoiceJob.expects(:enqueue).never
          with_rollback(subscription) { subscription.save }
        end

        it 'notifies voice after update' do
          subscription = FactoryBot.create(:voice_subscription, account: account, max_agents: 2)
          Voice::UpdateVoiceJob.
            expects(:enqueue).
            with(account.id, plan_type: 1, suspended: true)
          subscription.update_attributes(plan_type: 1, suspended: true)
        end

        it 'does not notify voice if none of the attributes were changed' do
          subscription = FactoryBot.create(:voice_subscription, account: account, max_agents: 2)
          Voice::UpdateVoiceJob.expects(:enqueue).never
          subscription.save!
        end
      end

      describe_with_arturo_enabled :voice_disable_subscription_notifications do
        it 'does not notify voice after update' do
          subscription = FactoryBot.create(:voice_subscription, account: account, max_agents: 2)
          Voice::UpdateVoiceJob.expects(:enqueue).never
          subscription.update_attributes(plan_type: 1, suspended: true)
        end
      end
    end
  end

  describe '#create_zuora_voice_usage_subscription' do
    describe 'when billing_check_for_voice_usage_subscription_during_sync Arturo is On' do
      before do
        Arturo.enable_feature!(:billing_check_voice_usage_subscription_during_sync)
      end

      it 'is not called when voice_subscription is created' do
        ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never
        FactoryBot.create(:voice_subscription, account: account, max_agents: 2)
      end
    end

    describe 'when billing_check_for_voice_usage_subscription_during_sync Arturo is Off' do
      before do
        Arturo.disable_feature!(:billing_check_voice_usage_subscription_during_sync)
      end

      it 'is called when voice_subscription is created' do
        ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).once
        FactoryBot.create(:voice_subscription, account: account, max_agents: 2)
      end
    end
  end
end
