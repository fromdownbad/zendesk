require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 6

describe Voice::FeatureMapper do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:feature_mapper) { Voice::FeatureMapper.new(account) }

  before do
    voice_accounts = mock
    voice_accounts.stubs(:exists?).returns(true)
    Voice::VoiceAccount.stubs(:where).returns(voice_accounts)
    Account.any_instance.stubs(:voice_enabled?).returns(true)
  end

  describe '#feature_enabled?' do
    def stub_random_voice_feature(value)
      body = {
        feature: {
          random_voice_feature: value
        }
      }.to_json
      @features_stub = stub_request(:get, %r{/api/v2/channels/voice/internal/features/check/random_voice_feature}).
        to_return(body: body, headers: { content_type: 'application/json' })
    end

    it 'returns true if the given feature is enabled' do
      stub_random_voice_feature true
      assert feature_mapper.feature_enabled?(:random_voice_feature)
    end

    it 'returns false if the given feature is disabled' do
      stub_random_voice_feature false
      refute feature_mapper.feature_enabled?(:random_voice_feature)
    end
  end
end
