require_relative "../support/test_helper"

SingleCov.covered!

describe 'CustomStatus' do
  fixtures :accounts, :custom_statuses

  let!(:custom_status) { custom_statuses(:minimum_solved) }

  let(:custom_status4) do # Second valid custom status from same account
    CustomStatus.new(
      account_id: custom_status.account_id,
      system_status_id: custom_status.system_status_id,
      agent_label: 'label4',
      end_user_label: 'label4'
    )
  end

  describe "uniqueness of system_status_id" do
    let(:account1) { accounts(:minimum) }
    let(:account2) { accounts(:support) }

    let(:custom_status2) do # invalid custom status (duplicate position)
      CustomStatus.new(
        account_id: custom_status.account_id,
        system_status_id: custom_status.system_status_id,
        agent_label: 'label2',
        end_user_label: 'label2'
      )
    end

    let(:custom_status3) do # valid custom status (duplicate position but different account)
      CustomStatus.new(
        account_id: account2.id,
        system_status_id: custom_status.system_status_id,
        agent_label: 'label3',
        end_user_label: 'label3'
      )
    end

    let(:custom_status5) do # invalid custom status (duplicate agent_label)
      CustomStatus.new(
        account_id: custom_status.account_id,
        system_status_id: custom_status.system_status_id,
        agent_label: custom_status.agent_label,
        end_user_label: 'label2'
      )
    end

    let(:custom_status6) do # Third valid custom status from same account
      CustomStatus.new(
        account_id: custom_status.account_id,
        system_status_id: custom_status.system_status_id,
        agent_label: 'label6',
        end_user_label: 'label6'
      )
    end

    describe 'CustomStatuses#default' do
      it 'should set original custom statuses to default false' do
        custom_status4.default = true
        assert custom_status4.save
        custom_status.reload
        refute custom_status.default
      end

      it 'should have only one win out if two are set to default' do
        custom_status6.default = true
        custom_status4.default = true
        assert custom_status6.save
        assert custom_status4.save
        refute custom_status.reload.default
        refute custom_status6.reload.default
        assert custom_status4.reload.default
      end

      describe 'invalid' do
        it 'to set default status to default false' do
          custom_status.default = false
          refute custom_status.valid?
        end
        it 'to set default status to inactive' do
          custom_status.active = false
          refute custom_status.valid?
        end
        it 'to set default status to soft deleted' do
          custom_status.soft_delete
          refute custom_status.valid?
        end
      end
    end

    it "should not allow the same agent_label" do
      refute custom_status5.valid?
      assert_equal "#{custom_status.agent_label} has already been taken", custom_status5.errors[:agent_label].first
    end

    it "should allow the same agent_label for different accounts" do
      custom_status5.account = account2
      assert custom_status5.valid?
    end

    it "should not allow 2 system_status_id's with the same position" do
      # Initial position is automatically assigned
      custom_status2.save!
      custom_status2.position = custom_status.position
      custom_status2.valid?
      assert_equal "System status error handling this request", custom_status2.errors.full_messages.first
    end

    it "should allow 2 system_status_id's with the same position from different accounts" do
      # Initial position is automatically assigned
      custom_status3.save!
      custom_status3.position = custom_status.position
      assert custom_status3.valid?
    end

    it "should allow 2 system_status_id's with the different positions from the same account" do
      assert custom_status4.valid?
    end
  end

  describe "soft deletion" do
    it "should set deleted_at" do
      custom_status4.soft_delete!
      assert custom_status4.deleted_at?
    end

    it "should set position to nil" do
      custom_status4.soft_delete!
      refute custom_status4.position?
    end
  end

  describe "validate_system_status_id_is_immutable" do
    it 'should not be valid' do
      custom_status.system_status_id = StatusType.PENDING
      refute custom_status.valid?
      assert_equal "can not be changed", custom_status.errors.messages[:system_status_id].first
    end
  end
end
