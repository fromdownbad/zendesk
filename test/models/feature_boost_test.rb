require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe FeatureBoost do
  fixtures :accounts, :subscriptions, :users, :payments, :credit_cards
  let(:account) { accounts(:minimum) }

  it "is unique for a account" do
    FeatureBoost.create!(account: account, valid_until: 30.days.from_now, boost_level: 4)
    refute_valid FeatureBoost.new(account: account, valid_until: 30.days.from_now, boost_level: 3)
    refute_valid FeatureBoost.new(account: account, valid_until: 20.days.from_now, boost_level: 4)
  end

  it "is inactivated when an account subscription plan type is changed" do
    FeatureBoost.create!(account: account, valid_until: 30.days.from_now, boost_level: 4)
    assert account.feature_boost.is_active, 'feature_boost should be active'
    account.subscription.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge)
    refute account.reload.feature_boost.is_active, 'feature_boost should be deactivated'
  end

  describe "boost expiry date" do
    let(:feature_boost) { FeatureBoost.new(account: account, boost_level: 4) }

    it "should not be boosted for more than 30 days" do
      feature_boost.valid_until = 40.days.from_now
      refute_valid feature_boost
      refute feature_boost.save
    end

    it "should be less than 30 days" do
      feature_boost.valid_until = 30.days.from_now
      assert_valid feature_boost
      assert feature_boost.save
    end

    describe_with_arturo_enabled :remote_support_promo_boost_extension_plan do
      describe "when account is on Team plan" do
        before do
          account.subscription.plan_type = ZBC::Zendesk::PlanType::Team.plan_type
          account.subscription.pricing_model_revision = ZBC::Zendesk::PricingModelRevision::PATAGONIA
          account.subscription.save!
        end

        it "allows boosting for more than 30 days" do
          feature_boost.valid_until = 40.days.from_now
          assert_valid feature_boost
          assert feature_boost.save
        end

        it "does not allow boosting for more than 6 months" do
          feature_boost.valid_until = 7.months.from_now
          refute_valid feature_boost
          refute feature_boost.save
        end
      end

      describe "when account is not on Team plan" do
        it "does not allow boosting for more than 30 days" do
          feature_boost.valid_until = 40.days.from_now
          refute_valid feature_boost
          refute feature_boost.save
        end
      end
    end

    describe "for a sandbox account" do
      before { account.expects(:is_sandbox?).returns(true) }

      it "can be more than 30 days" do
        feature_boost.valid_until = 40.days.from_now
        assert feature_boost.save
      end
    end
  end

  describe "#boost_plan_name" do
    describe "legacy pricing model subscription" do
      (1..4).each do |level|
        (0..5).each do |pricing_model|
          before do
            Subscription.any_instance.stubs(:pricing_model_revision).returns(pricing_model)
            @plan_names = plan_types.map(&:name)
          end
          it "should return patagonia plan names" do
            assert_equal @plan_names[level - 1], feature_boost(level, account).boost_plan_name.downcase
          end
        end
      end
    end

    describe "patagonia pricing model subscription" do
      (1..4).each do |level|
        before do
          Subscription.any_instance.stubs(:pricing_model_revision).
            returns(ZBC::Zendesk::PricingModelRevision::PATAGONIA)
          @plan_names = plan_types.map(&:name)
        end

        it "should return patagonia plan names" do
          assert_equal @plan_names[level - 1], feature_boost(level, account).boost_plan_name.downcase
        end
      end
    end
  end

  describe "Inactivating feature boost" do
    let(:account) { accounts(:trial) }

    let(:boost_level) { SubscriptionPlanType.ExtraLarge }

    before do
      @feature_boost = FeatureBoost.new(account: account, valid_until: 30.days.from_now, boost_level: boost_level)
      @feature_boost.save(validate: false)
    end

    it "succeeds for expired trials that don't have a credit card'" do
      account.subscription.update_attribute(:is_trial, false)
      assert_equal false, account.subscription.is_trial?
      assert_nil account.subscription.credit_card

      account.feature_boost.update_attributes! is_active: false
    end

    describe "when there are boosted addons present" do
      before do
        account.subscription_feature_addons.create!(name: 'test_feature_addon', boost_expires_at: 5.days.from_now)
      end

      it "calls destroy_all on boosted subscription feature addons" do
        assert_equal 1, account.subscription_feature_addons.boosted.count(:all)
        account.feature_boost.update_attributes! is_active: false
        assert_equal 0, account.subscription_feature_addons.boosted.count(:all)
      end

      describe "and the purchased plan is the same as the boost level" do
        before do
          account.subscription.update_attributes! plan_type: boost_level
        end

        it "doesn't call destroy_all on boosted subscription feature addons" do
          assert_equal 1, account.subscription_feature_addons.boosted.count(:all)
          account.feature_boost.update_attributes! is_active: false
          assert_equal 1, account.subscription_feature_addons.boosted.count(:all)
        end
      end
    end
  end

  describe "Updating feature boost valid_until date" do
    let(:account) { accounts(:trial) }

    let(:feature_boost) do
      FeatureBoost.new(
        account: account,
        valid_until: 30.days.from_now,
        boost_level: 4
      )
    end

    let(:addon_boost) { { name: 'test_feature_addon', boost_expires_at: 5.days.from_now } }

    let(:new_expiry_date) { 8.days.from_now.to_date }

    before do
      feature_boost.save(validate: false)
      account.subscription_feature_addons.create!(addon_boost)
      feature_boost.update_attributes! valid_until: new_expiry_date
    end

    it "also update any boosted addons expiry date" do
      expected = new_expiry_date
      actual   = account.subscription_feature_addons.boosted.first.boost_expires_at.to_date

      assert_equal expected, actual
    end
  end

  describe "#subscription_feature_service" do
    let(:feature_boost) do
      FeatureBoost.new(account: account, valid_until: 30.days.from_now, boost_level: 4)
    end

    describe "when a feature boost is commited" do
      it "calls the subscription feature" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account).returns(stub(execute!: true))
        feature_boost.save!
      end
    end
  end

  def feature_boost(level, account)
    FeatureBoost.new(account: account, boost_level: level)
  end

  def plan_types
    ZBC::Zendesk::PlanType.pricing_model_specific_types(
      ZBC::Zendesk::PricingModelRevision::PATAGONIA
    )
  end
end
