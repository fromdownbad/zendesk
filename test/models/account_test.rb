require_relative "../support/test_helper"
require_relative "../support/suite_test_helper"
require_relative "../support/multiproduct_test_helper"
require_relative "../support/billing_test_helper"
require_relative "../support/sharing_test_helper"
require_relative "../support/reply_parser_test_helper"
require 'zendesk/outgoing_mail/variables'
require 'active_support/cache/libmemcached_local_store'

SingleCov.covered! uncovered: 59

describe Account do
  fixtures :all

  extend ReplyParserTestHelper
  include MultiproductTestHelper
  include ZendeskBillingCore::ZuoraTestHelper
  include SharingTestHelper
  include SuiteTestHelper

  let(:account) { accounts(:minimum) }
  let(:multiproduct_account) { setup_shell_account }
  let(:support_trial_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_trial_product).with_indifferent_access) }
  let(:support_inactive_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_inactive_product).with_indifferent_access) }
  let(:chat_inactive_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:chat_inactive_product).with_indifferent_access) }

  before do
    Resque.stubs(:enqueue).with { true }.returns(true)
  end

  describe "kasket cache clearing" do
    # Test environment uses TestMemoryStore. We need to integrate with memcache.
    let(:cache) do
      ActiveSupport::Cache::LibmemcachedLocalStore.new(
        Zendesk::Configuration[:memcached_servers].values.sort,
        client: Rails.application.config.memcache_options
      )
    end

    before { Kasket.stubs(:cache).returns(cache) }

    describe "#clear_kasket_cache!" do
      it "clears every memcache server using the same client settings" do
        account.kasket_keys.each do |key|
          Kasket.cache.write(key, "foo")
        end

        account.clear_kasket_cache!

        account.kasket_keys.each do |key|
          Kasket.cache.read(key).must_be_nil
        end
      end
    end

    describe "#clear_reflections_kasket_cache!" do
      it "clears every memcache server using the same client settings" do
        keys = account.class.reflections.values.map do |r|
          r.klass.kasket_key_for([[:account_id, account.id]])
        end

        keys.each do |key|
          Kasket.cache.write(key, "foo")
        end

        account.clear_reflections_kasket_cache!

        keys.each do |key|
          Kasket.cache.read(key).must_be_nil
        end
      end
    end

    describe "#delete_keys_from_kasket_cache" do
      it "logs error when memcache blows up" do
        Memcached.any_instance.expects(:delete).at_least_once.raises(Memcached::ServerIsMarkedDead.new)
        Rails.logger.expects(:debug).at_least_once
        account.send(:delete_keys_from_kasket_cache, ['1', '2', '3'])
      end
    end
  end

  describe "when an account has an unverified owner" do
    it "allows account to be saved" do
      account.owner.is_verified = false
      account.save!
      account.name = "My shiny new account"
      assert(account.save!)
    end
  end

  describe "a shell account" do
    it "allows the account to be saved" do
      account = setup_shell_account
      account.name = "my shiny new account"
      account.save
      assert_empty account.errors.messages
    end
  end

  describe "#reload_associations_into_kasket!" do
    it "calls reload on account associations" do
      u = account.users.to_a.first
      original_name = u.name
      u.name = 'dirty'

      assert_equal 'dirty', account.users.first.name
      account.reload_associations_into_kasket!
      assert_equal original_name, account.users.first.name
    end
  end

  describe "soft_deletion" do
    before do
      account.subscription.stubs(:canceled_on).returns(90.days.ago)
    end

    it "creates a deletion audit" do
      audit = DataDeletionAudit.new(reason: 'canceled')
      DataDeletionAudit.expects(:build_for_cancellation).once.returns(audit)
      account.soft_delete!

      assert_includes account.data_deletion_audits, audit
    end

    it "rolls back on error" do
      DataDeletionAudit.stubs(:build_for_cancellation).raises
      assert_raises(RuntimeError) { account.soft_delete! }

      account.reload
      assert_nil account.deleted_at
    end

    it "checks against feature" do
      account.expects(:has_prevent_deletion_if_churned?).once.returns(true)
      assert_raises(Zendesk::DataDeletion::PermanentError) { account.soft_delete! }
    end

    it "nils the route's gam_domain" do
      account.route.update_attribute(:gam_domain, 'test')
      account.soft_delete!

      assert_nil account.route.gam_domain
    end
  end

  describe "scopes" do
    describe ".exclude_test_expired" do
      let(:scoped_account_ids) { Account.exclude_test_expired.map(&:id) }

      it "does not return accounts with subdomains containing with 'test' or 'expired'" do
        accounts(:trial).update_attribute(:subdomain, "abcdtest123")
        accounts(:minimum).update_attribute(:subdomain, "abcdexpired123")

        refute_includes scoped_account_ids, accounts(:trial).id
        refute_includes scoped_account_ids, accounts(:minimum).id
      end
    end

    describe ".waiting_hard_deletion" do
      it "includes accounts with a waiting hard deletion audit" do
        Account.with_deleted { Account.waiting_hard_deletion.to_a.must_equal [] }
        accounts(:trial).cancel!
        Account.with_deleted { Account.waiting_hard_deletion.to_a.must_equal [] }
        accounts(:trial).soft_delete!
        Account.with_deleted { Account.waiting_hard_deletion.to_a.must_equal [accounts(:trial)] }
        accounts(:trial).data_deletion_audits.waiting.first.enqueue!
        Account.with_deleted { Account.waiting_hard_deletion.to_a.must_equal [] }
      end
    end
  end

  describe '#time_zone=' do
    describe 'when a schedule exists' do
      before { account.schedules.create(name: 'Schedule', time_zone: 'London') }

      describe "and the 'multiple schedules' feature is enabled" do
        before do
          Account.any_instance.stubs(:has_multiple_schedules?).returns(true)

          account.time_zone = 'Jerusalem'

          account.save!
        end

        it 'does not update the associated schedule time zone' do
          assert_equal 'London', account.reload.schedule.time_zone
        end

        it 'updates the account time zone' do
          assert_equal 'Jerusalem', account.reload.time_zone
        end
      end

      describe "and the 'multiple schedules' feature is not enabled" do
        before do
          Account.any_instance.stubs(:has_multiple_schedules?).returns(false)

          account.time_zone = 'Jerusalem'

          account.save!
        end

        it 'updates the associated schedule time zone' do
          assert_equal 'Jerusalem', account.reload.schedule.time_zone
        end

        it 'updates the account time zone' do
          assert_equal 'Jerusalem', account.reload.time_zone
        end
      end
    end

    describe 'when a schedule does not exist' do
      before do
        account.schedules.destroy_all

        account.time_zone = 'Copenhagen'

        account.save!
      end

      it 'does not blow up' do
        assert_equal 'Copenhagen', account.reload.time_zone
      end
    end
  end

  describe '#schedule' do
    before do
      Account.any_instance.stubs(:has_multiple_schedules?).returns(true)
    end

    describe 'when the account has an active schedule' do
      before do
        account.schedules.create(name: 'A', time_zone: 'UTC').
          tap(&:soft_delete).
          update_column(:created_at, Time.utc(2010))

        account.schedules.create(name: 'B', time_zone: 'UTC').update_column(
          :created_at,
          Time.utc(2012)
        )
        account.schedules.create(name: 'C', time_zone: 'UTC').update_column(
          :created_at,
          Time.utc(2011)
        )
      end

      it 'returns the first active schedule by creation time' do
        assert_equal 'C', account.schedule.name
      end
    end

    describe 'when the account does not have an active schedule' do
      before do
        account.schedules.create(name: 'Test', time_zone: 'UTC').soft_delete
      end

      it 'returns nil' do
        assert_nil account.schedule
      end
    end
  end

  describe "#currency_type" do
    describe "a USD account" do
      let(:account) do
        create_account_from_hash(default_account_hash, 'USD').tap(&:save!)
      end

      it "has a USD trial subscription" do
        assert_equal CurrencyType.USD, account.subscription.currency_type
      end
    end

    describe "a non-USD EUR account" do
      let(:account) do
        create_account_from_hash(default_account_hash, 'EUR').tap(&:save!)
      end

      it "has a trial subscription with EUR as currency_type" do
        assert_equal CurrencyType.EUR, account.subscription.currency_type
      end
    end

    describe "a non-USD bad parameter account" do
      let(:account) do
        create_account_from_hash(default_account_hash, 'FOO').tap(&:save!)
      end

      it "has a trial subscription with USD as currency_type" do
        assert_equal CurrencyType.USD, account.subscription.currency_type
      end
    end
  end

  describe "#active_zuora_coupon" do
    describe "for non zuora managed accounts" do
      before do
        account.stubs(:has_zuora_managed_billing_enabled?).returns(false)
      end

      it "returns nil" do
        assert_nil account.active_zuora_coupon
      end
    end

    describe "for zuora managed accounts" do
      before do
        # Ensure the coupon is replicated in Billing
        zuora_coupons(:fixed).send(:create_billing_coupon)

        ZendeskBillingCore::Zuora::CouponRedemption.any_instance.stubs(:upload_coupon_discount).returns(true)
        Subscription.any_instance.stubs(:zuora_managed?).returns(true)
        account.stubs(:has_zuora_managed_billing_enabled?).returns(true)
      end

      describe "when there are no coupon redemptions" do
        before do
          account.zuora_coupon_redemptions.clear
        end

        it "does not return a coupon" do
          assert_nil account.active_zuora_coupon
        end
      end

      describe "when there is an active coupon redemption" do
        describe "and the coupon has not yet expired" do
          before do
            account.zuora_coupon_redemptions.create!(state: "active", account: account, zuora_coupon: zuora_coupons(:fixed))
            @active_coupon = account.active_zuora_coupon
          end

          it "returns the coupon for that redemption" do
            assert @active_coupon
            assert_equal zuora_coupons(:fixed), @active_coupon
          end
        end

        describe "and the coupon has expired" do
          before do
            redemptions = stub(active: [zuora_coupon_redemptions(:active_with_expired_coupon)])
            Account.any_instance.stubs(zuora_coupon_redemptions: redemptions)
            @active_coupon = account.active_zuora_coupon
          end

          it "returns the coupon for that redemption" do
            assert @active_coupon
            assert_equal zuora_coupons(:expired), @active_coupon
          end
        end
      end

      describe "when the coupon redemption is inactive" do
        before do
          account.zuora_coupon_redemptions.create!(state: "inactive", account: account, zuora_coupon: zuora_coupons(:fixed))
          refute account.zuora_coupon_redemptions.empty?
          @active_coupon = account.active_zuora_coupon
        end

        it "does not return a coupon" do
          assert_nil @active_coupon
        end
      end
    end
  end

  describe "#is_failing?" do
    describe "For zuora CC billing enabled account" do
      before do
        Account.any_instance.stubs(:has_zuora_managed_billing_enabled?).returns(true)
      end

      describe "With zuora subscription" do
        before do
          @zuora_subscription = mock
          Subscription.any_instance.stubs(:zuora_subscription).returns(@zuora_subscription)
        end

        describe "in dunning" do
          before do
            @zuora_subscription.stubs(:in_dunning?).returns(true)
          end
          it "returns true" do
            assert account.is_failing?
          end
        end

        describe "not in dunning" do
          before do
            @zuora_subscription.stubs(:in_dunning?).returns(false)
          end
          it "returns false" do
            assert_equal false, account.is_failing?
          end
        end
      end

      describe "for trial account" do
        it "returns false" do
          assert_equal false, accounts(:trial).is_failing?
        end
      end
    end
    it "does not raise when the credit_card record is new" do
      account.subscription.stubs(:credit_card).returns(stub(new_record?: true, created_at: nil))
      refute account.is_failing?
    end
  end

  describe "#set_defaults" do
    before do
      @account = Accounts::Classic.new
      @account.save
    end

    it "enables legacy collaboration" do
      assert @account.is_collaboration_enabled?
      refute @account.is_collaborators_addable_only_by_agents?
      refute @account.has_follower_and_email_cc_collaborations_enabled?
    end

    it "enables captcha for the account" do
      assert @account.settings.captcha_required
    end

    it "enables auto assigning of tickets upon solve" do
      assert @account.settings.assign_tickets_upon_solve?
    end

    it "enables changing the assignee from the agent back to the group" do
      assert @account.settings.change_assignee_to_general_group?
    end

    it "enables first comment private setting" do
      assert @account.is_first_comment_private_enabled?
    end

    it "disables native condtional fields migrated setting" do
      refute @account.settings.native_conditional_fields_migrated?
    end

    it "sets onboarding-capability to SUPPORT_SUITE_TRIAL_REFRESH" do
      assert_equal Account::OnboardingSupport::SUPPORT_SUITE_TRIAL_REFRESH, @account.settings.checklist_onboarding_version
    end

    it "sets benchmark_opted_out to false" do
      refute @account.settings.benchmark_opt_out
    end
  end

  describe "#reactivation_attributes" do
    before do
      account.cancel!
    end

    it "sets is_serviceable and is_active to true when the account has a credit card" do
      Subscription.any_instance.stubs(:credit_card).returns(CreditCard.new)

      attr = account.reactivation_attributes

      assert(attr[:is_active])
      assert(attr[:is_serviceable])
    end

    it "sets is_serviceable and is_active to true when the account is in it's trial window" do
      account.subscription.expects(:is_trial?).at_least_once.returns(false)

      attr = account.reactivation_attributes

      assert(attr[:is_active])
      assert(attr[:is_serviceable])
    end

    it "sets is_serviceable and is_active to true when the account is invoicing" do
      Subscription.any_instance.stubs(:invoicing?).returns(true)
      Subscription.any_instance.stubs(:crediting?).returns(false)

      attr = account.reactivation_attributes

      assert(attr[:is_active])
      assert(attr[:is_serviceable])
    end

    it "sets is_serviceable to false and is_active to true when the account is crediting, not in trial, and does not have a credit card on file" do
      account.subscription.credit_card = nil

      attr = account.reactivation_attributes

      assert(attr[:is_active])
      assert_equal false, attr[:is_serviceable]
    end
  end

  describe '#expire_trial!' do
    let(:account) { Account.new(subscription: subscription, multiproduct: multiproduct, name: 'test') }
    let(:subscription) { Subscription.new }
    let(:multiproduct) { false }

    before do
      account.subdomain = 'test123'
      account.stubs(:id).returns(123)
      account.stubs(:save)
      subscription.stubs(:save)
      Zendesk::Accounts::Client.any_instance.stubs(:update_product)
    end

    it 'sets is_trial to false' do
      account.expire_trial!
      assert_equal false, account.subscription.is_trial
    end

    it 'sets is_serviceable to false' do
      account.expire_trial!
      assert_equal false, account.is_serviceable
    end

    describe 'shell account' do
      let(:multiproduct) { true }

      it 'does not change is_serviceable field' do
        account.expire_trial!
        assert(account.is_serviceable)
      end
    end

    it 'updates the product to staff service' do
      Zendesk::Accounts::Client.any_instance.expects(:update_product)
      account.expire_trial!
    end
  end

  describe "#reactivate!" do
    before do
      Timecop.freeze Date.today
      account.cancel!
    end

    it "sets up the subscription correctly" do
      account.subscription.trial_expires_on = 65.days.ago
      account.reactivate!
      assert_equal 65.days.ago, account.subscription.trial_expires_on
    end

    describe "when multiproduct account" do
      before do
        @multiproduct_account = setup_shell_account
        @multiproduct_account.cancel!
      end

      it "does set is_serviceable and is_active" do
        @multiproduct_account.reactivate!
        assert(@multiproduct_account.reload.is_active)
        assert_equal false, @multiproduct_account.reload.is_serviceable
      end
    end

    describe "when multiproduct account with support" do
      before do
        @multiproduct_account = setup_shell_account_with_support
        @multiproduct_account.cancel!
      end

      it "sets up the subscription correctly" do
        @multiproduct_account.subscription.trial_expires_on = 65.days.ago
        @multiproduct_account.reactivate!
        assert_equal 65.days.ago, @multiproduct_account.subscription.trial_expires_on
      end
    end
  end

  describe "#end_users" do
    before do
      @users = account.end_users
    end

    it "includes end user" do
      assert @users.include?(users(:minimum_end_user))
    end

    it "does not include agents" do
      refute @users.include?(users(:minimum_agent))
    end
  end

  describe "#all_users" do
    before do
      @users = account.all_users
    end

    it "includes active users" do
      assert @users.include?(users(:minimum_end_user))
    end

    it "includes inactive users" do
      assert @users.include?(users(:inactive_user))
    end
  end

  describe "#all_end_users" do
    before do
      @users = account.all_end_users
    end

    it "includes active end users" do
      assert @users.include?(users(:minimum_end_user))
    end

    it "includes inactive end users" do
      assert @users.include?(users(:inactive_user))
    end

    it "does not include agents" do
      refute @users.include?(users(:minimum_agent))
    end
  end

  describe "#tickets_solved_this_week" do
    before do
      @agent = account.agents.first
      account.tickets.each(&:delete)
    end

    it "reflects the number of tickets solved within the last week" do
      Timecop.freeze(account.start_of_week.utc - 1.day)

      assert_equal 0, @agent.tickets_solved_this_week

      t = account.tickets.new(subject: "foo1", description: "bar1", requester: account.owner, assignee: @agent, status_id: StatusType.SOLVED)
      t.will_be_saved_by(account.owner)
      t.save!

      assert_equal 1, @agent.tickets_solved_this_week

      Timecop.travel(3.days)

      assert_equal 0, @agent.tickets_solved_this_week

      t = account.tickets.new(subject: "foo2", description: "bar2", requester: account.owner, assignee: @agent, status_id: StatusType.SOLVED)
      t.will_be_saved_by(account.owner)
      t.save!

      assert_equal 1, account.tickets_solved_this_week
    end

    it "reflects the number of solved tickets as would be reported by other mechanisms" do
      t1 = account.tickets.new(subject: "foo1", description: "bar1", requester: account.owner, assignee: @agent)
      t1.will_be_saved_by(account.owner)
      t1.save!

      t2 = account.tickets.new(subject: "foo2", description: "bar2", requester: account.owner, assignee: @agent)
      t2.will_be_saved_by(account.owner)
      t2.save!

      assert_equal 0, account.tickets_solved_this_week

      Zendesk::Tickets::Merger.new(user: @agent).merge(
        target: t1,
        sources: [t2],
        target_comment: "winner",
        source_comment: "losers",
        target_is_public: true,
        source_is_public: true
      )

      assert_equal StatusType.CLOSED, t2.reload.status_id

      t1.status_id = StatusType.SOLVED
      t1.will_be_saved_by(account.owner)
      t1.save!

      assert_equal 2, account.tickets_solved_this_week
    end
  end

  describe "An Account" do
    should_validate_presence_of :help_desk_size

    it "orders categories alphabetically" do
      Category.without_arsi.delete_all
      account.categories.create!(name: 'Zebra')
      account.categories.create!(name: 'Ape')

      assert_equal ['Ape', 'Zebra'], account.categories.map(&:name)
    end

    it "tokenizes whitelists properly" do
      assert_equal 6, account.domain_whitelist.tokenize.size
    end

    it "#destroy (case 1)" do
      Attachment.any_instance.stubs(stores: [:fs])
      HeaderLogo.any_instance.stubs(stores: [:fs])
      MobileLogo.any_instance.stubs(stores: [:fs])
      Logo.any_instance.stubs(stores: [:fs])
      Photo.any_instance.stubs(stores: [:fs])
      Favicon.any_instance.stubs(stores: [:fs])
      CachingObserver.stubs(:expire_fragments_on_save_or_destroy) # avoid lots of weird errors that we never saw in prod

      # in order for destroy through relations to work
      [Change, Comment, Create, External].each do |klass|
        klass.any_instance.stubs(new_record?: true)
      end

      without_foreign_key_checks do # http://www.pivotaltracker.com/story/show/3177721
        account.destroy
      end
      assert account_fully_deleted(account.id)
    end

    it "#destroy (case 2)" do
      CachingObserver.stubs(:expire_fragments_on_save_or_destroy) # avoid lots of weird errors that we never saw in prod
      Subscription.any_instance.stubs(:should_validate_presence_of_credit_card?).returns(false)
      with_groups_account = Account.find(accounts(:with_groups).id)
      with_groups_account.cancel!
      without_foreign_key_checks do # http://www.pivotaltracker.com/story/show/3177721
        with_groups_account.destroy
      end
      assert account_fully_deleted(with_groups_account.id)
    end

    it "validates the time_zone" do
      assert(account.valid?)

      account.time_zone = 'Copenhagen'
      assert(account.valid?)

      account.time_zone = 'Monkey'
      assert(!account.valid?)
      assert(account.errors[:time_zone].any?)

      account.time_zone = nil
      assert(!account.valid?)
      assert(account.errors[:time_zone].any?)
    end

    describe_with_arturo_disabled :switch_kyev_to_kyiv do
      it "does not switch account's time zone from Kyev to Kyiv" do
        account.time_zone = 'Kyev'
        account.valid?

        assert_equal 'Kyev', account.time_zone
      end
    end

    describe_with_arturo_enabled :switch_kyev_to_kyiv do
      it "switches account's time zone from Kyev to Kyiv" do
        account.time_zone = 'Kyev'
        account.valid?

        assert_equal 'Kyiv', account.time_zone
      end
    end

    it "legacy accounts fallback to onboarding-capability of SUPPORT_SUITE_TRIAL_REFRESH" do
      assert_equal Account::OnboardingSupport::SUPPORT_SUITE_TRIAL_REFRESH, account.settings.checklist_onboarding_version
    end
  end

  describe "#backup_email_address" do
    it "returns the expected default email address" do
      assert_equal(
        "support@#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}",
        account.backup_email_address
      )
    end
  end

  describe "#recipient_emails" do
    it "is all emails" do
      assert_equal [
        "support@minimum.zendesk-test.com", # backup
        "support@zdtestlocalhost.com",      # default
        "without_name@example.net",         # other
        "not_default@example.net"           # other
      ].sort, accounts(:minimum).recipient_emails.sort
    end
  end

  describe "#email_address_in_use_error" do
    it "returns an error if the email address is the same as the account's reply_address" do
      assert_equal(
        "support@zdtestlocalhost.com cannot be used; it is in use as a support address",
        account.email_address_in_use_error(account.reply_address)
      )
    end

    it "returns an error if the email address is found in the account's recipient addresses" do
      assert_equal(
        "without_name@example.net cannot be used; it is in use as a support address",
        account.email_address_in_use_error(recipient_addresses(:without_name).email)
      )
    end

    it "does not return an error if the email address is not in the account's recipient addresses" do
      assert_nil account.email_address_in_use_error("not_in_use@example.net")
    end
  end

  describe "#reply_address" do
    let(:recipient_address) { recipient_addresses(:default) }

    it "returns the default_recipient_address" do
      assert_equal account.default_recipient_address.email, account.reply_address
    end

    it "does not return a brand recipient address" do
      recipient_address.update_column(:brand_id, 123) # put on some other brand
      assert_equal account.backup_email_address, account.reply_address
    end

    it "returns the account backup_email_address if there is no default_recipient_address" do
      account.default_recipient_address.send(:delete)
      assert_equal account.backup_email_address, account.reload.reply_address
    end
  end

  describe "Account saving" do
    it "strips whitespace from subdomain and downcase the letters" do
      account.subdomain = "   Staugaard   "
      account.save!
      assert_equal("staugaard", account.subdomain)
    end

    it "strips tags from name" do
      account.name = "Foobar <script>alert(1);</script>"
      account.save!
      exp = RAILS4 ? "Foobar alert(1);" : "Foobar "
      assert_equal(exp, account.name)
    end

    it "includes value for duplicate subdomain error" do
      account.subdomain = "support"
      account.valid?
      assert_equal "support has already been taken", account.errors.messages[:subdomain].first
    end

    it "includes value for subdomain is too short error" do
      account.subdomain = "s"
      account.valid?
      assert_equal "s is invalid (three or more letters and numbers only, without spaces)", account.errors.messages[:subdomain].first
    end

    it "includes value for subdomain is too long error" do
      account.subdomain = 'support' * 30
      account.valid?
      assert_equal "#{'support' * 30} is too long (maximum is 63 characters)", account.errors.messages[:subdomain].first
    end
  end

  describe "Accounts with unverified owners" do
    before do
      @user = users(:minimum_admin_not_verified)
      account.owner = @user
    end

    it "is able to be cancelled" do
      assert account.cancel!
    end
  end

  describe "Liquid template validation" do
    it "determines if a valid liquid template" do
      account.html_mail_template = "Not valid value"
      refute account.save
      account.html_mail_template = "{{delimiter}} {{header}} {{content}} {{footer}}"
      assert account.save

      account.text_mail_template = "Not valid value"
      refute account.save
      account.text_mail_template = "{{delimiter}} {{header}} {{content}} {{footer}} is text!"
      assert account.save
    end

    describe_with_reply_parser "email template validation" do
      it "does not require delimiter to be present in the email template" do
        account.html_mail_template = "{{header}} {{content}} {{footer}}"
        assert account.save
      end
    end
  end

  describe "Account Property set" do
    it "delegates to the account_propert_set instance correctly" do
      account.signup_page_text = 'New Signup Page Text'
      account.html_mail_template = "New {{delimiter}} {{header}} {{content}} {{footer}}"
      account.text_mail_template = "New {{delimiter}} {{header}} {{content}} {{footer}} is text!"
      account.branding = Branding.new
      account.cc_email_template = 'New CC mail template'
      assert account.save
      account.reload

      assert_equal 'New Signup Page Text', account.signup_page_text
      assert_equal "New {{delimiter}} {{header}} {{content}} {{footer}}", account.html_mail_template
      assert_equal "New {{delimiter}} {{header}} {{content}} {{footer}} is text!", account.text_mail_template
      assert_equal Branding.new.to_h.values.sort, account.branding.to_h.values.sort
      assert_equal 'New CC mail template', account.cc_email_template
    end

    describe "when html_mail_template exceeds column size" do
      let(:default_html_mail_template) { "{{delimiter}} {{header}} {{content}} {{footer}}" }

      it "does not update html_mail_template" do
        assert_equal default_html_mail_template, account.html_mail_template
        account.update_attributes(html_mail_template: "a" * 66535)
        assert_equal default_html_mail_template, account.reload.html_mail_template
        refute account.errors.messages.key?(:html_mail_template)
        assert account.errors["base"].include?(I18n.t("txt.admin.controllers.settings.html_mail_template_too_long"))
      end
    end

    describe "when text_mail_template exceeds column size" do
      let(:default_text_mail_template) { "{{content}}\n\n{{footer}}" }

      it "does not update text_mail_template" do
        assert_equal default_text_mail_template, account.text_mail_template
        account.update_attributes(text_mail_template: "a" * 66535)
        assert_equal default_text_mail_template, account.reload.text_mail_template
        refute account.errors.messages.key?(:text_mail_template)
        assert account.errors["base"].include?(I18n.t("txt.admin.controllers.settings.text_mail_template_too_long"))
      end
    end

    describe '#cc_subject_template' do
      let(:default_template) { I18n.t("txt.default.cc_email_subject_v2") }

      it "returns the default template when nil" do
        account.cc_subject_template = nil
        assert_equal account.cc_subject_template, default_template
      end

      it "returns the default template when empty" do
        account.cc_subject_template = ""
        assert_equal account.cc_subject_template, default_template
      end

      it "does not return the default template when filled" do
        account.cc_subject_template = "sample title"
        assert_equal account.cc_subject_template, "sample title"
      end

      it "does not store the default template to not trigger a confusing audit to be created" do
        account.cc_subject_template = default_template
        account.cc_subject_template.must_equal default_template
        account.settings.cc_subject_template.must_equal ""
      end
    end

    describe '#follower_subject_template' do
      let(:default_template) { I18n.t("txt.default.follower_email_subject") }

      it "returns the default template when nil" do
        account.follower_subject_template = nil
        assert_equal account.follower_subject_template, default_template
      end

      it "returns the default template when empty" do
        account.follower_subject_template = ""
        assert_equal account.follower_subject_template, default_template
      end

      it "does not return the default template when filled" do
        account.follower_subject_template = "sample title"
        assert_equal account.follower_subject_template, "sample title"
      end

      it "does not store the default template to not trigger a confusing audit to be created" do
        account.follower_subject_template = default_template
        account.follower_subject_template.must_equal default_template
        account.settings.follower_subject_template.must_equal ""
      end
    end

    describe '#cc_email_template' do
      let(:default_template) { I18n.t("txt.default.cc_email_text.paragraph_v2") }

      it "returns the default template when nil" do
        account.cc_email_template = nil
        assert_equal account.cc_email_template, default_template
      end

      it "returns the default template when empty" do
        account.cc_email_template = ""
        assert_equal account.cc_email_template, default_template
      end

      it "does not return the default template when filled" do
        account.cc_email_template = "sample body"
        assert_equal account.cc_email_template, "sample body"
      end
    end

    describe '#follower_email_template' do
      let(:default_template) { I18n.t("txt.default.follower_email_text") }

      before do
        account.stubs(has_follower_and_email_cc_collaborations_enabled?: true)
      end

      it "returns the default template when nil" do
        account.follower_email_template = nil
        assert_equal account.follower_email_template, default_template
      end

      it "returns the default template when empty" do
        account.follower_email_template = ""
        assert_equal account.follower_email_template, default_template
      end

      it "does not return the default template when filled" do
        account.follower_email_template = "sample body"
        assert_equal account.follower_email_template, "sample body"
      end
    end

    describe '#text_mail_template' do
      describe 'when active record attribute is blank' do
        before do
          assert account.account_property_set.attributes["text_mail_template"].blank?
        end

        it 'defaults to TEXT_MAIL_TEMPLATE when value stored in account property set is blank' do
          assert_equal Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE, account.text_mail_template
        end

        it 'validates' do
          assert account.account_property_set.valid?
        end
      end

      describe 'when active record value is present and valid' do
        before do
          @new_template = Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE + " foo is text"
          account.text_mail_template = @new_template
        end

        it 'fetches the stored text_mail_template when one is present' do
          assert_equal @new_template, account.text_mail_template
        end

        it 'validates' do
          assert account.account_property_set.valid?
        end
      end
    end

    describe "introductory text HTML" do
      it "is corrected when changed" do
        account.introductory_text = nil
        account.save!

        account.introductory_text = 'Badly <DIV> formatted <a>html</a>'
        account.save!
        assert_equal "Badly <div> formatted <a>html</a>\n</div>", account.introductory_text
      end

      it "does not be corrected when unchanged" do
        refute account.account_property_set.introductory_text_changed?
        account.account_property_set.expects(:tidy_introductory_text).never
        account.save!
      end
    end
  end

  describe "#fetch_active_triggers" do
    it "caches results in Rails.cache" do
      a = account.fetch_active_triggers
      other = Account.find(account.id)
      other.expects(:triggers).never
      b = other.fetch_active_triggers
      a.must_equal b
      a.wont_be_same_as b
    end

    it "memoizes the cached triggers" do
      a = account.fetch_active_triggers
      b = account.fetch_active_triggers
      a.must_be_same_as b
    end

    it "expires the memoized value when the memcache key changes" do
      a = account.fetch_active_triggers
      # Make a change, CachingObserver expires the key
      a.last.destroy
      Timecop.travel(Time.now + 1.seconds)
      b = account.fetch_active_triggers
      b.length.must_equal a.length - 1
    end
  end

  describe '#fetch_active_non_update_triggers' do
    let(:non_update_trigger) do
      stub('non-update trigger', change_conditions?: false)
    end

    let(:other_trigger) { stub('update trigger', change_conditions?: true) }

    before do
      account.stubs(fetch_active_triggers: [non_update_trigger, other_trigger])
    end

    it 'returns triggers that do not have change conditions' do
      assert_equal(
        [non_update_trigger],
        account.fetch_active_non_update_triggers
      )
    end
  end

  describe '#fetch_active_prediction_triggers' do
    let(:prediction_trigger) do
      stub('prediction trigger', satisfaction_prediction_conditions?: true)
    end

    let(:other_trigger) do
      stub('non-prediction trigger', satisfaction_prediction_conditions?: false)
    end

    before do
      account.stubs(fetch_active_triggers: [prediction_trigger, other_trigger])
    end

    it 'returns triggers that have satisfaction prediction conditions' do
      assert_equal(
        [prediction_trigger],
        account.fetch_active_prediction_triggers
      )
    end
  end

  describe "#fetch_active_taggers" do
    it "caches results in Rails.cache" do
      a = account.fetch_active_taggers
      other = Account.find(account.id)
      other.expects(:active_taggers).never
      b = other.fetch_active_taggers
      a.must_equal b
      a.wont_be_same_as b
    end

    it "memoizes the cached triggers" do
      a = account.fetch_active_taggers
      b = account.fetch_active_taggers
      a.must_be_same_as b
    end

    it "expires the memoized value when the memcache key changes" do
      a = account.fetch_active_taggers
      # Make a change, CachingObserver expires the key
      a.last.destroy
      Timecop.travel(Time.now + 1.seconds)
      b = account.fetch_active_taggers
      b.length.must_equal a.length - 1
    end

    describe_with_arturo_enabled :active_taggers_skip_cache do
      it "does not cache results" do
        active_taggers = account.active_taggers
        account.reload
        a = account.fetch_active_taggers
        other = Account.find(account.id)
        other.expects(:active_taggers).returns(active_taggers)
        b = other.fetch_active_taggers
        a.must_equal b
        a.wont_be_same_as b
      end

      it "memoizes results" do
        a = account.fetch_active_taggers
        b = account.fetch_active_taggers
        a.must_be_same_as b
      end
    end
  end

  describe "#fetch_active_tagger" do
    let(:tagger) { account.active_taggers.first }

    it "caches result in Rails.cache" do
      a = account.fetch_active_tagger(tagger.id)
      other = Account.find(account.id)
      other.expects(:active_taggers).never
      b = other.fetch_active_tagger(tagger.id)
      a.must_equal b
      a.wont_be_same_as b
    end

    describe_with_and_without_arturo_enabled :active_taggers_skip_cache do
      it "returns nil when non-existent ID is passed" do
        assert_nil account.fetch_active_tagger(658743723)
      end
    end

    describe_with_arturo_enabled :active_taggers_skip_cache do
      it "does not cache result" do
        active_taggers = account.active_taggers
        a = account.fetch_active_tagger(tagger.id)
        other = Account.find(account.id)
        other.expects(:active_taggers).returns(active_taggers)
        b = other.fetch_active_tagger(tagger.id)
        a.must_equal b
        a.wont_be_same_as b
      end

      it "does not memoize result" do
        a = account.fetch_active_tagger(tagger.id)
        b = account.fetch_active_tagger(tagger.id)
        a.must_be_same_as b
      end
    end
  end

  describe "API rate limits" do
    describe "#api_rate_limit_boosted?" do
      describe "when there is a setting" do
        describe "and the setting is in the future" do
          before { account.settings.api_rate_limit_boost = 14.days.from_now.to_i }

          it "returns true" do
            assert account.api_rate_limit_boosted?
          end
        end

        describe "and the setting is in the past" do
          before { account.settings.api_rate_limit_boost = 14.days.ago.to_i }

          it "returns false" do
            refute account.api_rate_limit_boosted?
          end
        end
      end
    end

    describe "#api_time_rate_limit" do
      describe "when account has high volume addon" do
        before do
          account.subscription.stubs(has_high_volume_api?: true)
        end

        it "returns 30 mins" do
          assert_equal 30.minutes.to_i, account.api_time_rate_limit
        end
      end

      describe "when #api_rate_limit_boosted? is true" do
        before { account.stubs(:api_rate_limit_boosted?).returns(true) }

        it "returns 30.minutes" do
          assert_equal 30.minutes.to_i, account.api_time_rate_limit
        end
      end

      describe "#has_api_time_limit_setting? is true" do
        before do
          account.stubs(:api_rate_limit_boosted?).returns(false)
          account.stubs(:has_api_time_limit_setting?).returns(true)
          account.settings.api_time_limit = 30
        end

        it "returns 30 minutes" do
          assert_equal 30.minutes.to_i, account.api_time_rate_limit
        end
      end

      describe "when #api_rate_limit_boosted? and #has_api_time_limit_setting? are false " do
        before do
          account.stubs(:api_rate_limit_boosted?).returns(false)
          account.stubs(:has_api_time_limit_setting?).returns(false)
        end

        it "returns 10.minutes" do
          assert_equal 10.minutes.to_i, account.api_time_rate_limit
        end
      end
    end

    describe '#default_api_time_rate_limit' do
      describe 'for high volume addon accounts' do
        before do
          subscription_settings = { has_high_volume_api?: true }
          account.subscription.stubs(subscription_settings)
        end

        it 'is 30 mins' do
          assert_equal 30.minutes, account.default_api_time_rate_limit
        end
      end

      it 'is 10 mins' do
        assert_equal 10.minutes, account.default_api_time_rate_limit
      end
    end

    describe "#api_rate_time_limit_threshold" do
      describe "when #api_rate_limit_boosted? is true" do
        before { account.stubs(:api_rate_limit_boosted?).returns(true) }

        it "is 3" do
          assert_equal 3, account.api_time_rate_limit_threshold
        end
      end

      describe "when #api_rate_limit_boosted? is false" do
        before { account.stubs(:api_rate_limit_boosted?).returns(false) }

        it "is 0" do
          assert_equal 0, account.api_time_rate_limit_threshold
        end
      end
    end
  end

  describe "#boosted?" do
    before do
      account.create_feature_boost(valid_until: 20.days.from_now)
    end

    it "returns false if the account is on the Enterprise plan" do
      assert account.subscription.plan_type != SubscriptionPlanType.ExtraLarge
      assert account.boosted?
      account.subscription.update_attributes!(plan_type: SubscriptionPlanType.ExtraLarge)
      refute account.boosted?
    end

    it "returns false if the feature boost has been inactivated" do
      assert account.boosted?
      account.feature_boost.update_attribute(:is_active, false)
      refute account.reload.boosted?
    end
  end

  describe "mixed case in host_mapping" do
    before do
      account.stubs(:valid_cname_record?).returns(true)
      account.host_mapping = "Foo.bar.com"
      account.save!
    end

    it "normalizes on save" do
      assert_equal "foo.bar.com", account.host_mapping
    end
  end

  describe "#host_name" do
    describe "host_mapping validation" do
      it "allows the .travel top level domain" do
        account.stubs(:valid_cname_record?).returns(true)
        account.host_mapping = "something.example.travel"
        assert_valid account
      end

      it "sets be invalid in case host mapping is not available on the subscription" do
        account.host_mapping = "www.example.com"
        account.subscription.expects(:has_host_mapping?).returns(false)
        refute_valid account
      end

      it "does not result in an invalid record if the host_mapping is blank" do
        account.host_mapping = ""
        account.subscription.expects(:has_host_mapping?).never
        assert_valid account
      end

      it "does not load subscription in order to get an unmapped URL" do
        account.expects(:subscription).never
        account.host_mapping   = "hello.example.com"
        account.is_ssl_enabled = true
        account.host_name(mapped: false)
      end
    end
  end

  describe "has_forums2?" do
    it "delegates to the account's settings" do
      account.settings.forums2 = false
      assert_equal false, account.has_forums2?

      account.settings.forums2 = true
      assert(account.has_forums2?)
    end
  end

  describe "has_status_hold?" do
    describe "when enabled in Arturo" do
      before do
        Arturo.enable_feature!(:status_hold)
      end

      describe "and the subscription has the feature" do
        before { account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large) }

        it "returns true" do
          assert account.has_status_hold?
        end
      end

      describe "and the subscription does not have the feature" do
        before do
          account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Small)
          Zendesk::Features::SubscriptionFeatureService.new(account).execute!
          account.reload
        end

        it "returns false" do
          refute account.has_status_hold?
        end
      end
    end
  end

  describe "use_status_hold?" do
    describe "when the account has the capability" do
      before { account.stubs(:has_status_hold?).returns(true) }

      it "defers to the status field" do
        [true, false].each do |bool|
          account.stubs(:field_status).returns(stub(use_status_hold?: bool))
          assert_equal bool, account.use_status_hold?
        end
      end
    end

    describe "when the account does not have the capability" do
      before { account.stubs(:has_status_hold?).returns(false) }

      it "never asks the status field" do
        account.expects(:field_status).never
        refute account.use_status_hold?
      end
    end
  end

  describe "#ticket_forms_is_active?" do
    describe "when the account has the capability" do
      before { account.stubs(:has_ticket_forms?).returns(true) }

      describe "and has more than the default form" do
        before do
          limit_mock   = mock
          limit_mock.stubs(:count).returns(2)
          ticket_forms = stub
          ticket_forms.stubs(:limit).returns(limit_mock)
          account.stubs(:ticket_forms).returns(ticket_forms)
        end

        it "returns true" do
          assert(account.ticket_forms_is_active?)
        end
      end

      describe "and has only the default form" do
        before do
          limit_mock = mock
          limit_mock.stubs(:count).returns(1)
          ticket_forms = stub
          ticket_forms.stubs(:limit).returns(limit_mock)
          account.stubs(:ticket_forms).returns(ticket_forms)
        end

        it "returns false" do
          assert_equal false, account.ticket_forms_is_active?
        end
      end
    end

    describe "when the account does not have the capability" do
      before { account.stubs(:has_ticket_forms?).returns(false) }

      it "never asks for ticket forms" do
        account.expects(:ticket_forms).never
        refute account.ticket_forms_is_active?
      end
    end
  end

  describe "#available_languages" do
    it "does not return all the available languages on this account, but rather only English because the account does not have permission for extra languages" do
      account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Medium)
      account.update_attribute(:allowed_translation_locale_ids, [translation_locales(:spanish).id, translation_locales(:japanese).id])

      assert_equal ["English"], account.available_languages.collect(&:name)
    end

    it "returns all the available languages on this account, including allowed translations, sorted by name" do
      account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
      Zendesk::Features::SubscriptionFeatureService.new(account).execute!
      account.reload
      account.update_attribute(:allowed_translation_locale_ids, [translation_locales(:spanish).id, translation_locales(:japanese).id])

      assert_equal ["English", "Japanese", "spanish"],
        account.available_languages.collect(&:name)
    end

    it "does not allow any duplicates on the list" do
      assert_equal ["English"], account.available_languages.collect(&:name)
      account.update_attribute(:allowed_translation_locale_ids, [translation_locales(:english_by_zendesk).id])
      assert_equal ["English"], account.available_languages.collect(&:name)
    end
  end

  %w[macros views].each do |rule_type|
    describe "#shared_#{rule_type}" do
      before do
        assert account.send(rule_type).present?,
          "In this context, the account must have #{rule_type}"
      end

      it "includes #{rule_type} owned by the account" do
        account.send(rule_type).each do |rule_owned_by_group|
          assert account.send("shared_#{rule_type}").include?(rule_owned_by_group)
        end
      end

      describe "for an account supporting group rules" do
        before do
          account.subscription.stubs(:has_group_rules?).returns(true)

          assert account.groups.present?,
            "For this test, the account must have at least one group"
          assert account.groups.map(&rule_type.to_sym).flatten.present?,
            "For this test, at least one of the account's groups must have #{rule_type}"
        end

        it "includes #{rule_type} owned by any of the account's groups" do
          account.groups.map(&rule_type.to_sym).flatten.each do |rule_owned_by_group|
            assert account.send("shared_#{rule_type}").include?(rule_owned_by_group)
          end
        end
      end

      describe "for an account that does not support group rules" do
        before do
          account.subscription.stubs(:has_group_rules?).returns(false)

          assert account.groups.present?,
            "For this test, the account must have at least one group"
          assert account.groups.map(&rule_type.to_sym).flatten.present?,
            "For this test, at least one of the account's groups must have #{rule_type}"
        end

        it "does not include #{rule_type} owned by a Group" do
          account.send("shared_#{rule_type}").each { |rule| refute rule.owner.is_a?(Group) }
        end
      end
    end
  end

  describe "#touch" do
    before do
      account.update_attribute(:last_login, nil)
    end

    it "sets a last_login on the account if this has not yet been done, and not change updated_at" do
      updated_at = account.updated_at
      assert account.last_login.nil?
      account.update_last_login
      account.reload
      assert account.last_login.present?
      assert_equal updated_at, account.updated_at
    end

    it "does not update last_login if it's recent" do
      last_login = 1.minute.ago
      account.update_attribute(:last_login, last_login)
      assert_equal last_login.to_i, account.reload.last_login.to_i
      account.update_last_login
      assert_equal last_login.to_i, account.reload.last_login.to_i
    end

    it "updates last_login if it's not recent" do
      last_login = 1.hour.ago
      account.update_attribute(:last_login, last_login)
      assert_equal last_login.to_i, account.reload.last_login.to_i
      account.update_last_login
      assert last_login.to_i < account.reload.last_login.to_i
    end

    describe "readonly last_login record" do
      before { account.settings.last_login_record.stubs(:readonly?).returns(true) }

      it "does not update last_login if the record is readonly" do
        account.update_last_login
        assert_nil account.reload.last_login
      end
    end

    describe "migrating last_login to settings" do
      before do
        @ts = Time.now
      end

      it "reads off settings if settings is present" do
        ts = 1.hour.ago
        account.settings.last_login = ts.to_s
        assert_equal ts.to_i, account.last_login.to_i
      end

      it "stores the time to settings" do
        ts = Time.now + 5.hours
        Timecop.freeze(ts) do
          account.update_last_login
          assert_equal ts.to_i, account.last_login.to_i
        end
      end
    end
  end

  describe '#show_upsell_for_twitter_accounts?' do
    it 'does not exceed maximum' do
      MonitoredTwitterHandle.expects(:where).with(account_id: account.id).returns(stub(count: 9))
      assert_equal false, account.show_upsell_for_twitter_accounts?
    end

    it 'exceed maximum' do
      MonitoredTwitterHandle.expects(:where).with(account_id: account.id).returns(stub(count: 11))
      assert(account.show_upsell_for_twitter_accounts?)
    end
  end

  describe "#has_end_user_ui?" do
    it 'returns false if the web_portal is disabled and help_center is not enabled' do
      account.stubs(:web_portal_disabled?).returns(true)
      account.stubs(:help_center_enabled?).returns(false)
      assert_equal false, account.has_end_user_ui?
    end

    it 'returns true if help_center is enabled' do
      account.stubs(:help_center_enabled?).returns(true)
      assert(account.has_end_user_ui?)
    end

    it 'returns true if web_portal is not disabled' do
      account.stubs(:web_portal_disabled?).returns(false)
      assert(account.has_end_user_ui?)
    end
  end

  describe "source_allows_delayed_verification?" do
    [
      ["Android", Zendesk::Accounts::Source::ANDROID],
      ["Blackberry", Zendesk::Accounts::Source::BLACKBERRY],
      ["iPad", Zendesk::Accounts::Source::IPAD],
      ["iPhone", Zendesk::Accounts::Source::IPHONE]
    ].each do |label, source|
      it "is true for #{label}" do
        account.source = "#{source} bar"
        assert account.source_allows_delayed_verification?
      end
    end

    it "is false for other sources" do
      account.source = "zendesk.com"
      refute account.source_allows_delayed_verification?
    end
  end

  describe '#sign_in_requires_verification?' do
    it 'returns false if the account was created via Blackberry in the last 24 hours' do
      account.stubs(:source).returns(Zendesk::Accounts::Source::BLACKBERRY)
      account.stubs(:created_at).returns(12.hours.ago)
      refute account.sign_in_requires_verification?
    end

    it 'returns true if the account was created via Blackberry more than 24 hours ago' do
      account.stubs(:source).returns(Zendesk::Accounts::Source::BLACKBERRY)
      account.stubs(:created_at).returns(25.hours.ago)
      assert account.sign_in_requires_verification?
    end

    it 'returns true if the account was created via web' do
      account.stubs(:source).returns('Zendesk.com')
      assert account.sign_in_requires_verification?
    end
  end

  describe '#two_factor_last_update' do
    describe 'account setting - two_factor_all_agents is not present,' do
      before { assert_nil account.reload.settings.two_factor_all_agents }

      it 'returns account.created' do
        assert_equal account.created_at, account.two_factor_last_update
      end
    end

    describe 'account setting - two_factor_all_agents is present' do
      before do
        account.settings.two_factor_all_agents = true
        account.save!
      end

      it 'returns two_factor_all_agents.updated_at' do
        assert_equal account.settings.find_by_name('two_factor_all_agents')[:updated_at], account.two_factor_last_update
      end
    end
  end

  describe '#settings.twitter_search_stream?' do
    before do
      @account = accounts(:support)
    end

    it "proxies to the account's features" do
      assert_equal(false, @account.settings.twitter_search_stream?)
      @account.settings.enable(:twitter_search_stream)
      @account.save!
      assert(@account.settings.twitter_search_stream?)
    end
  end

  describe "kasket" do
    before { accounts(:minimum).update_column(:host_mapping, "xxxxx") }

    it "caches by id" do
      Rails.cache.clear
      assert_sql_queries 1 do
        2.times do
          Account.find_by_id(account.id)
        end
      end
    end

    it "caches by subdomain on account" do
      Rails.cache.clear
      assert_sql_queries 1 do
        2.times do
          Account.find_by_subdomain(account.subdomain)
        end
      end
    end

    it "caches by host_mapping on account" do
      Rails.cache.clear
      assert_sql_queries 1 do
        2.times do
          Account.find_by_host_mapping(account.host_mapping)
        end
      end
    end
  end

  describe "#has_public_forums?" do
    it "returns false if there are no accessible forums" do
      account.forums.stubs(:accessible_to).returns(stub(exists?: false))
      assert_equal false, account.has_public_forums?
    end

    it "returns true if forums are accessible" do
      account.forums.stubs(:accessible_to).returns(stub(exists?: true))
      assert(account.has_public_forums?)
    end
  end

  describe '#sharing_url' do
    it 'exists' do
      assert_equal('https://minimum.zendesk-test.com/sharing', account.sharing_url)
    end
  end

  describe '.ids_to_shard_id_map' do
    before do
      @accounts = Account.all
      @map = Account.ids_to_shard_id_map(@accounts.map(&:id))
    end

    it "returns a map of account.id => shard_id" do
      @accounts.each do |a|
        assert_equal a.shard_id, @map[a.id]
      end
    end
  end

  describe "#valid_cname_record?" do
    before do
      account.host_mapping = "foo.bar.com"
    end

    describe "when resolved to minimum.ssl.zendesk.com" do
      before { Zendesk::Net::CNAMEResolver.expects(:resolve?).with("foo.bar.com").returns(stub(name: "minimum.ssl.zendesk-test.com")) }

      it "does not cause validations to fail" do
        assert account.save
      end
    end

    describe "when resolved to minimum.zendesk.com" do
      before { Zendesk::Net::CNAMEResolver.expects(:resolve?).with("foo.bar.com").returns(stub(name: "minimum.zendesk-test.com")) }

      it "does not cause validations to fail" do
        assert account.save
      end
    end
  end

  describe '#sharing_agreements_with' do
    it 'returns [] if no sharing agreements' do
      assert_equal(account.sharing_agreements_with(:jira), [])
    end

    it 'returns [] if no matching sharing agreements' do
      other_id = Sharing::Agreement::SHARED_WITH_MAP[:jira] + 1 # non-jira
      account.stubs(:sharing_agreements).returns([FactoryBot.create(:agreement, shared_with_id: other_id)])
      assert_equal(account.sharing_agreements_with(:jira), [])
    end

    it 'returns matching sharing agreements' do
      jira_id = Sharing::Agreement::SHARED_WITH_MAP[:jira]
      account.stubs(:sharing_agreements).returns([FactoryBot.create(:agreement, shared_with_id: jira_id)])
      assert_not_nil account.sharing_agreements_with(:jira)
    end
  end

  describe ".lookup" do
    it 'ignores sandbox accounts' do
      account = FactoryBot.create(:account, name: 'Search string').becomes(Account)
      sandbox = FactoryBot.create(
        :account,
        name: 'Search string SANDBOX',
        sandbox_master_id: account.id
      )

      accounts = Account.lookup('Search string')

      assert accounts.include?(account)
      refute accounts.include?(sandbox)
    end
  end

  describe '.find_by_fqdn' do
    it 'finds an account by subdomain' do
      a = Account.find_by_fqdn("#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}")
      assert_equal account, a
    end

    it 'finds an account by hostmapping' do
      account.host_mapping = "foo.bar.com"
      account.save validate: false

      a = Account.find_by_fqdn("foo.bar.com")
      assert_equal account, a
    end
  end

  describe "#allows_collaborator?" do
    before do
      @address = 'hello@example.com'
    end

    it "includes most email addresses" do
      assert account.allows_collaborator?(@address)
    end

    it "does not allow recipient address as a follower" do
      refute account.allows_collaborator?(account.backup_email_address)
    end

    it "does not include addresses in the cc/follower blacklist" do
      account.cc_blacklist = @address
      refute account.allows_collaborator?(@address)
    end

    it "includes variations of non-zendesk recipient addresses" do
      address = recipient_addresses(:not_default).email
      assert account.allows_collaborator?(address.sub('@', '+id1234@')) # requester ticket id
      assert account.allows_collaborator?(address.sub('@', '+id1234+5678')) # agent ticket id
    end

    it "does not include variations of zendesk recipient addresses" do
      address = recipient_addresses(:backup).email
      refute account.allows_collaborator?(address.sub('@', '+id1234@')) # requester ticket id
      refute account.allows_collaborator?(address.sub('@', '+id1234+5678@')) # agent ticket id
    end
  end

  describe "#is_open?" do
    it "returns false when the account is not open" do
      account.is_open = false

      refute account.is_open?
    end

    it "returns true the account is open" do
      account.is_open = true

      assert account.is_open?
    end
  end

  describe '#render_forum_title' do
    before do
      account.forum_title = "{{dc.wombat_forums}}"
      @end_user = users(:minimum_end_user)
      @user_locale = translation_locales(:english_by_zendesk)
      @end_user.stubs(:translation_locale).returns(@user_locale)
      @current_locale = translation_locales(:brazilian_portuguese)
      I18n.locale = @current_locale
    end

    it 'renders dynamic content correctly via liquid' do
      Zendesk::Liquid::DcContext.expects(:render).with("{{dc.wombat_forums}}", account, @end_user, 'text/plain', @current_locale).returns("Wombat Forums")
      assert_equal "Wombat Forums", account.render_forum_title(@end_user)
    end
  end

  describe '#unrestricted_agents' do
    before do
      Account.any_instance.stubs(:has_light_agents?).returns(true)
      account.agents.where(restriction_id: 0).to_a

      @light_agent = users(:minimum_agent)
      @light_agent.assigned.stubs(:not_closed).returns(Ticket.where('id != id'))

      @light_agent.permission_set = @light_agent.account.permission_sets.create!(
        role_type: PermissionSet::Type::LIGHT_AGENT,
        name: "Test Light Agent Role"
      )
      @light_agent.save!

      assert @light_agent.is_light_agent?
    end

    it 'does not include light agents' do
      refute account.unrestricted_agents.map(&:id).include?(@light_agent.id)
    end
  end

  describe "#spam_prevention" do
    before do
      account.using_wp_spam_filter = true
      account.save!
    end

    describe "checking the settings" do
      it "returns :cm for content moderation" do
        account.settings.spam_prevention = true
        account.is_forum_moderation_enabled = true
        account.using_wp_spam_filter = false
        account.save!
        assert_equal :off, account.reload.spam_prevention
      end

      it "returns :sf for spam filter" do
        account.settings.spam_prevention = true
        account.is_forum_moderation_enabled = true
        account.using_wp_spam_filter = true
        account.save!
        assert_equal :off, account.reload.spam_prevention
      end

      it "returns :off if spam_prevention is off" do
        account.settings.spam_prevention = false
        account.save!
        assert_equal :off, account.reload.spam_prevention
      end
    end

    describe "setting spam_prevention" do
      it "sets the correct setting for :cm" do
        account.spam_prevention = :cm
        account.save!
        account.reload
        refute account.using_wp_spam_filter
        assert_equal :off, account.spam_prevention
      end

      it "sets the correct setting for :sf" do
        account.spam_prevention = :sf
        account.save!
        account.reload
        assert account.using_wp_spam_filter
        assert_equal :off, account.spam_prevention
      end
    end
  end

  describe "using_wp_spam_filter" do
    describe "setting" do
      before do
        account.using_wp_spam_filter = true
        account.settings.spam_prevention = true
        account.save!
      end

      it "toggles the account setting" do
        account.using_wp_spam_filter = "true"
        account.save!

        assert account.settings.spam_filter

        account.using_wp_spam_filter = "false"
        account.save!

        refute account.settings.spam_filter
      end

      it "accepts strings and bools" do
        account.using_wp_spam_filter = false
        account.save!

        refute account.using_wp_spam_filter?

        account.using_wp_spam_filter = "true"
        account.save!
        assert account.using_wp_spam_filter?
      end

      describe "getting setting status" do
        it "is accessible via using_wp_spam_filter" do
          account.using_wp_spam_filter = "true"
          assert account.using_wp_spam_filter

          account.using_wp_spam_filter = "false"
          refute account.using_wp_spam_filter
        end

        it "reads from the account setting" do
          account.settings.stubs(:spam_filter?).returns(true)

          assert account.using_wp_spam_filter
        end
      end
    end
  end

  describe "assignable_agents" do
    before do
      @admin = account.admins.first
      @light_agents = PermissionSet.create!(name: "Light agent", account: account, role_type: 1)
      @chat_agents = PermissionSet.create!(name: "Chat Agent", account: account, role_type: 2)
      @admin.update_attribute(:permission_set_id, @light_agents.id)
    end

    it "always includes admins, even if they have a light agent permission type" do
      assert account.assignable_agents.include?(@admin)
    end

    it "excludes light agents from being assigned" do
      agent = users(:minimum_agent)
      agent.update_attribute(:permission_set_id, @light_agents.id)
      refute_includes(account.assignable_agents, agent, "Shouldn't be included!")
    end

    it "excludes chat agents from being assigned" do
      agent = users(:minimum_agent)
      agent.update_attribute(:permission_set_id, @chat_agents.id)
      refute_includes(account.assignable_agents, agent, "Shouldn't be included!")
    end
  end

  describe "requester_agents" do
    before do
      @light_agents = PermissionSet.create!(name: "Light agent", account: account, role_type: 1)
      @chat_agents = PermissionSet.create!(name: "Chat Agent", account: account, role_type: 2)
    end
    it "allows light agents to be requesters" do
      agent = users(:minimum_agent)
      agent.update_attribute(:permission_set_id, @light_agents.id)
      assert_includes(account.requester_agents, agent, "Should be included!")
    end

    it "allows chat agents to be requesters" do
      agent = users(:minimum_agent)
      agent.update_attribute(:permission_set_id, @chat_agents.id)
      assert_includes(account.requester_agents, agent, "Should be included!")
    end
  end

  describe "contributors" do
    before do
      @light_agents = PermissionSet.create!(name: "Light agent", account: account, role_type: 1)
      @chat_agents = PermissionSet.create!(name: "Chat Agent", account: account, role_type: 2)
      @contributors = PermissionSet.create!(name: "contributor", account: account, role_type: 3)
    end

    let(:agent) { users(:minimum_agent) }

    it "returns contributors" do
      agent.update_attribute(:permission_set_id, @contributors.id)
      assert_includes(account.contributors, agent)
    end

    it "does not return light_agents" do
      agent.update_attribute(:permission_set_id, @light_agents.id)
      refute_includes(account.contributors, agent)
    end

    it "does not return chat_agents" do
      agent.update_attribute(:permission_set_id, @chat_agents.id)
      refute_includes(account.contributors, agent)
    end
  end

  describe "#find_user_by_email" do
    before do
      @user = users(:minimum_end_user)
    end

    it "finds in scope" do
      assert_equal @user, account.find_user_by_email("minimum_end_user@aghassipour.com")
      assert_equal @user, account.find_user_by_email("minimum_end_user@aghassipour.com", scope: account.users)
    end

    it "does not find out of scope" do
      agent = users(:minimum_agent)
      assert_nil account.find_user_by_email(agent.email, scope: account.end_users)
    end

    it "finds regardless of leading/trailing whitespace" do
      assert_equal @user, account.find_user_by_email(" minimum_end_user@aghassipour.com\t")
    end
  end

  describe "#find_or_create_user_by_email" do
    before do
      @user = users(:minimum_end_user)
    end

    it "finds" do
      assert_equal @user, account.find_or_create_user_by_email("minimum_end_user@aghassipour.com")
    end

    it "creates" do
      assert_difference "User.count(:all)", 1 do
        @user = account.find_or_create_user_by_email("foobar@baz.com", name: "Xxx", scope: account.end_users)
      end
      assert_equal "foobar@baz.com", @user.email
      assert_equal "Xxx", @user.name
    end
  end

  describe "#find_user_by_twitter_screen_name" do
    before do
      @user = users(:minimum_end_user)
      profile = channels_user_profiles(:minimum_twitter_user_profile_1)
      profile.update_column(:external_id, account.user_identities.twitter.first.value)
      @screen_name = "@#{profile.name}"
    end

    it "finds in scope" do
      assert_not_nil account.find_user_by_twitter_screen_name(@screen_name)
    end

    it "does not find out of scope" do
      assert_nil account.find_user_by_twitter_screen_name(@screen_name, scope: account.agents)
    end
  end

  describe "#find_user_by_main_identity" do
    before do
      @user = users(:minimum_end_user)
    end

    it "finds via twitter" do
      profile = channels_user_profiles(:minimum_twitter_user_profile_1)
      profile.update_column(:external_id, account.user_identities.twitter.first.value)
      assert_not_nil account.find_user_by_main_identity("@#{profile.name}")
    end

    it "finds in scope" do
      assert_equal @user, account.find_user_by_main_identity(@user.email)
      assert_equal @user, account.find_user_by_main_identity(@user.email, scope: account.users)
    end

    it "does not find out of scope" do
      account.find_user_by_main_identity(@user.email, scope: account.agents)
    end
  end

  describe "#find_user_by_facebook_id" do
    before do
      @user = users(:minimum_end_user)
      @user.identities << UserFacebookIdentity.create!(value: '1234', user: @user)
    end

    it "finds in scope" do
      assert_equal @user, account.find_user_by_facebook_id('1234')
      assert_equal @user, account.find_user_by_facebook_id('1234', scope: account.users)
    end

    it "does not find out of scope" do
      assert_nil account.find_user_by_facebook_id('1234', scope: account.agents)
    end
  end

  describe "#default_organization_for" do
    before do
      account.organizations.destroy_all
      account.organizations.create!(name: "some-org", domain_names: ["abc.com", "xyz.net", "user@email.com"])
    end

    describe "when the domain name passed in is blank" do
      [nil, ""].each do |domain_name|
        it "returns nil" do
          assert_nil account.default_organization_for(domain_name)
        end
      end
    end

    describe "when the account has no organization_domains with the domain name passed in" do
      it "returns nil" do
        assert_nil account.default_organization_for("some-other-domain.com")
      end

      describe "when the account has an organization_domain that is only a partial match for the domain name passed in" do
        it "returns nil" do
          domain_name = "prefix." + account.organization_domains.first.value

          assert_nil account.default_organization_for(domain_name)
        end
      end
    end

    describe "when the account has a single organization with an organization_domain that is a match for the domain name passed in" do
      it "returns the organization" do
        org_domain = account.organization_domains.first
        domain_name = org_domain.value
        expected_organization = org_domain.organization

        assert_equal expected_organization, account.default_organization_for(domain_name)
      end
    end

    describe "when the account has multiple organizations with organization_domains that match the domain name passed in" do
      let(:domain_name) { "non-unique-domain.com" }

      before do
        account.organizations.create!(name: "mmm-org", domain_names: [domain_name])
        account.organizations.create!(name: "aaa-org", domain_names: ["other-domain.com", domain_name])
        account.organizations.create!(name: "zzz-org", domain_names: ["acme@example.com", domain_name])
      end

      it "returns the organization with the first alphabetical name" do
        expected_organization = account.organizations.find_by_name("aaa-org")

        assert_equal expected_organization, account.default_organization_for(domain_name)
      end
    end

    describe "when the account has an organization_domain that matches the domain name passed in, but it is soft-deleted" do
      before do
        account.organizations.create!(name: "to-be-soft-deleted", domain_names: ["to-be-soft-deleted.com"]).destroy

        org_domains = OrganizationDomain.unscoped.where(account: account, value: "to-be-soft-deleted.com")

        assert_equal 1, org_domains.count
        assert org_domains.all?(&:deleted?)
      end

      it "returns nil" do
        assert_nil account.default_organization_for("to-be-soft-deleted.com")
      end
    end
  end

  describe "#organization_domains" do
    describe "when the account has soft-deleted organization_domains" do
      before do
        account.organization_domains.destroy_all
        account.organization_emails.destroy_all
        account.organizations.delete_all

        account.organizations.create(name: "org1", domain_names: ["org1.com"])
        # `destroy`-ing an organziation soft-deletes both the organization and its
        # organization_domains associations
        account.organizations.create(name: "org2", domain_names: ["org2.com"]).destroy
      end

      it "only returns the undeleted organization_domains" do
        assert_equal 2, OrganizationDomain.unscoped.where(account: account).count
        assert_equal 1, account.organization_domains.count
        assert_equal ["org1.com"], account.organization_domains.pluck(:value)
      end
    end
  end

  describe "#organization_emails" do
    describe "when the account has soft-deleted organizations with organization_emails" do
      before do
        account.organization_domains.destroy_all
        account.organization_emails.destroy_all
        account.organizations.delete_all

        account.organizations.create(name: "org1", domain_names: ["user@org1.com"])
        # `destroy`-ing an organziation soft-deletes both the organization and its
        # organization_emails associations
        account.organizations.create(name: "org2", domain_names: ["user@org2.com"]).destroy
      end

      it "only returns the undeleted organization_emails" do
        assert_equal 2, OrganizationEmail.unscoped.where(account: account).count
        assert_equal 1, account.organization_emails.count
        assert_equal ["user@org1.com"], account.organization_emails.pluck(:value)
      end
    end
  end

  describe "should render DC liquid placeholders" do
    it "#chat_welcome_message" do
      Zendesk::Liquid::DcContext.stubs(:render).returns("Yep, it render")
      assert_equal "Yep, it render", account.chat_welcome_message
    end

    it "returns the placeholder if it is requested" do
      account.stubs(:chat_welcome_message).returns("{{this.is.a.placeholder}}")
      assert_equal "{{this.is.a.placeholder}}", account.chat_welcome_message(true)
    end

    it "#ticket_forms_instructions" do
      Zendesk::Liquid::DcContext.stubs(:render).returns("Rendering wombats")
      assert_equal "Rendering wombats", account.ticket_forms_instructions
    end
  end

  describe "#translation_locale" do
    it "returns english_by_zendesk if locale_id is not set" do
      account.locale_id = 0
      account.save!

      assert_equal ENGLISH_BY_ZENDESK, account.translation_locale
    end

    it "returns english_by_zendesk if locale doesn't exist" do
      account.locale_id = 12345
      account.save!

      assert_equal ENGLISH_BY_ZENDESK, account.translation_locale
    end

    it "returns the locale if found" do
      @spanish = translation_locales(:spanish)
      account.locale_id = @spanish.id
      account.save!

      assert_equal @spanish, account.translation_locale
    end
  end

  describe "#has_verification_captcha_enabled?" do
    describe "Arturo bit enabled" do
      before { Arturo.enable_feature!(:verification_captcha) }

      describe "account setting enabled" do
        before do
          account.settings.verification_captcha_enabled = true
          account.save!
        end

        it "returns true" do
          assert(account.has_verification_captcha_enabled?)
        end
      end

      describe "account setting disabled" do
        before do
          account.settings.verification_captcha_enabled = false
          account.save!
        end

        it "returns false" do
          assert_equal false, account.has_verification_captcha_enabled?
        end
      end
    end

    describe "Arturo bit disabled" do
      before { Arturo.disable_feature!(:verification_captcha) }

      describe "account setting enabled" do
        before do
          account.settings.verification_captcha_enabled = true
          account.save!
        end

        it "returns false" do
          assert_equal false, account.has_verification_captcha_enabled?
        end
      end

      describe "account setting disabled" do
        before do
          account.settings.verification_captcha_enabled = false
          account.save!
        end

        it "returns false" do
          assert_equal false, account.has_verification_captcha_enabled?
        end
      end
    end
  end

  describe "#remote_authentication_available?" do
    it "is true if saml is available" do
      account.stubs(has_saml?: true)
      assert account.remote_authentication_available?
    end

    it "is true if jwt is available" do
      account.stubs(has_jwt?: true)
      assert account.remote_authentication_available?
    end

    it "is false otherwise" do
      account.stubs(has_saml?: false, has_jwt?: false)
      refute account.remote_authentication_available?
    end
  end

  describe "#system_user_custom_field_keys" do
    before do
      sf = account.custom_fields.for_user.build(key: "system::sf1", title: "txt.some_system_field", is_system: 1, type: 'Text')
      sf.type = 'Text'
      sf.save!
    end

    it "returns the keys correctly" do
      assert_includes account.system_user_custom_field_keys, "system::sf1"
    end
  end

  describe "#allows?" do
    describe "when blacklisting everything" do
      before do
        account.stubs(:domain_blacklist).returns("*")
      end

      describe "when the domain_whitelist is empty" do
        before do
          account.stubs(:domain_whitelist).returns("")
        end

        it "does not allow the email address" do
          refute account.allows?("tester@z3nmail.com")
          refute account.whitelists?("tester@z3nmail.com")
          assert account.blacklists?("tester@z3nmail.com")
        end
      end

      describe "when whitelisting a domain via the domain_whitelist" do
        before do
          account.stubs(:domain_whitelist).returns("z3nmail.com")
        end

        it "gives the whitelist precedence" do
          assert account.allows?("tester@z3nmail.com")
          assert account.whitelists?("tester@z3nmail.com")
          assert account.blacklists?("tester@z3nmail.com")
          refute account.allows?("admin@gmail.com")
          refute account.whitelists?("admin@gmail.com")
          assert account.blacklists?("admin@gmail.com")
        end
      end
    end

    describe "when the blacklist is empty" do
      before do
        account.stubs(:domain_blacklist).returns("")
      end

      describe "when the domain_whitelist is empty" do
        before do
          account.stubs(:domain_whitelist).returns("")
        end

        it "will allow an email address" do
          assert account.allows?("tester@z3nmail.com")
        end
      end
    end
  end

  describe "#whitelists?" do
    before do
      account.stubs(:domain_whitelist).returns("otherdomain.com")
      account.organizations.create!(name: "some-org", domain_names: ["tester.com"])
    end

    describe "when the address is in the domain_whitelist" do
      it "returns true" do
        assert account.whitelists?("someone@otherdomain.com")
      end
    end

    describe "when the address is not in the domain_whitelist" do
      describe "when the address is whitelisted by an OrganizationDomain or OrganizationEmail" do
        it "returns true" do
          assert account.whitelists?("test1@tester.com")
        end
      end

      describe "when the address is not whitelisted by an OrganizationDomain or OrganizationEmail" do
        it "returns false" do
          refute account.whitelists?("random@random.net")
        end
      end
    end
  end

  describe "#in_domain_whitelist?" do
    before do
      account.stubs(:domain_whitelist).returns("some.com domains.net here.edu some.subdomain.of.z3nmail.com user@z3nmail.com")
    end

    describe "when the address passed in is blank" do
      it "returns false" do
        refute account.in_domain_whitelist?("")
      end
    end

    describe "when the address format is not an email" do
      it "returns false" do
        refute account.in_domain_whitelist?("blah")
      end
    end

    describe "when an address is in the domain_whitelist" do
      it "returns true" do
        assert account.in_domain_whitelist?("someone@some.com")
      end
    end

    describe "when an address is not in the domain_whitelist" do
      it "returns false" do
        refute account.in_domain_whitelist?("random@rando.net")
      end
    end

    describe "when the address matches a full email address on the domain_whitelist" do
      it "returns true" do
        assert account.in_domain_whitelist?("user@z3nmail.com")
      end
    end

    describe "when the address contains of a subdomain of a domain in domain_whitelist" do
      it "returns true" do
        assert account.in_domain_whitelist?("user@other.some.com")
      end
    end

    describe "when a subdomain is domain_whitelisted but not the top level domain" do
      it "does not whitelist an address with a top level domain" do
        refute account.in_domain_whitelist?("tester@z3nmail.com")
        assert account.in_domain_whitelist?("tester@some.subdomain.of.z3nmail.com")
      end
    end
  end

  describe "#organization_whitelists?" do
    before do
      account.organizations.create!(name: "some-org", domain_names: ["tester.com", "test-user@email.com"])
      account.reload
    end

    describe "when the address passed in is blank" do
      it "returns false" do
        refute account.organization_whitelists?("")
      end
    end

    describe "when the address format is not an email" do
      it "returns false" do
        refute account.organization_whitelists?("blah")
      end

      it "doesn't query OrganizationEmail or OrganizationDomain records" do
        assert_sql_queries 0 do
          account.organization_whitelists?("blah")
        end
      end
    end

    describe "when the address domain is in the list of org allowlist domain exclusions" do
      describe "when the address does not match an OrganizationEmail record" do
        before do
          account.organizations.create!(name: "some-gmail-org", domain_names: ["gmail.com"])
        end

        it "returns false" do
          refute account.organization_whitelists?("test@gmail.com")
        end
      end

      describe "when the address does match an OrganizationEmail record" do
        before do
          account.organizations.create!(name: "some-gmail-org", domain_names: ["test@gmail.com"])
        end

        it "returns true" do
          assert account.organization_whitelists?("test@gmail.com")
        end
      end
    end

    describe "when the address domain is a subdomain of a domain in the list of org allowlist domain exclusions" do
      describe "when the address does match an OrganizationDomain record" do
        before do
          account.organizations.create!(name: "some-gmail-org", domain_names: ["other.gmail.com"])
        end

        it "returns true" do
          assert account.organization_whitelists?("test@other.gmail.com")
        end
      end

      describe "when the address does match an OrganizationEmail record" do
        before do
          account.organizations.create!(name: "some-gmail-org", domain_names: ["test@other.gmail.com"])
        end

        it "returns true" do
          assert account.organization_whitelists?("test@other.gmail.com")
        end
      end
    end

    describe "when the address does not match an OrganizationDomain record" do
      describe "when the address does not match an OrganizationEmail record" do
        let(:address) { "other@email.com" }

        it "returns false" do
          refute account.organization_whitelists?(address)
        end

        it "queries both OrganizationEmail and OrganizationDomain records" do
          assert_sql_queries 2 do
            account.organization_whitelists?(address)
          end
        end
      end

      describe "when the address does match an OrganizationEmail record" do
        let(:address) { "test-user@email.com" }

        it "returns true" do
          assert account.organization_whitelists?(address)
        end

        it "only queries the OrganizationEmail records" do
          assert_sql_queries 1 do
            account.organization_whitelists?(address)
          end
        end
      end
    end

    describe "when the address does match an OrganizationDomain record" do
      let(:address) { "person@tester.com" }

      it "returns true" do
        assert account.organization_whitelists?(address)
      end

      it "queries both OrganizationEmail and OrganizationDomain records" do
        assert_sql_queries 2 do
          account.organization_whitelists?(address)
        end
      end
    end

    describe "when an address contains a subdomain of a top level domain in an OrganizationDomain" do
      it "does whitelist the address' subdomain" do
        assert account.organization_whitelists?("person@some.tester.com")
      end
    end

    describe "when an OrganizationDomain has a subdomain but not the top level domain" do
      before do
        account.organizations.create!(name: "some-other-org", domain_names: ["some.subdomain.of.z3nmail.com"])
      end

      it "does not whitelist an address with a top level domain" do
        refute account.organization_whitelists?("tester@z3nmail.com")
        assert account.organization_whitelists?("tester@some.subdomain.of.z3nmail.com")
      end
    end

    describe "when an address ends with the domain of an OrganizationDomain" do
      it "does not whitelist the address" do
        refute account.organization_whitelists?("person@someothertester.com")
      end
    end

    describe "when an address has the same domain as an OrganizationEmail" do
      it "does not whitelist the address" do
        assert account.organization_whitelists?("test-user@email.com")
        refute account.organization_whitelists?("admin-user@email.com")
      end
    end

    describe "when an address contains a subdomain of an OrganizationEmail" do
      it "does not whitelist the address" do
        refute account.organization_whitelists?("test-user@some.other.email.com")
      end
    end

    describe "when an address ends with the string of an OrganizationEmail" do
      it "does not whitelist the address" do
        refute account.organization_whitelists?("another-test-user@email.com")
      end
    end

    describe "when the address only matches a soft-deleted OrganizationDomain or a soft-deleted OrganizationEmail" do
      before do
        # `destroy`-ing an organization soft deletes the organization and
        # soft deletes its organization_domains and organization_emails
        # associations
        account.organizations.create!(
          name: "org-to-soft-delete",
          domain_names: ["org-to-soft-delete.com", "user@org-to-soft-delete.com"]
        ).destroy

        org_domains = OrganizationDomain.unscoped.where(account: account, value: "org-to-soft-delete.com")
        org_emails = OrganizationEmail.unscoped.where(account: account, value: "user@org-to-soft-delete.com")

        assert_equal 1, org_domains.count
        assert org_domains.all?(&:deleted?)

        assert_equal 1, org_emails.count
        assert org_emails.all?(&:deleted?)
      end

      it "returns false" do
        refute account.organization_whitelists?("user@org-to-soft-delete.com")
      end
    end
  end

  describe "#conflicting_domain_list" do
    describe "when there are no OrganizationDomain or OrganizationEmail records" do
      describe "when the domain_blacklist is empty" do
        it "does not return any conflicting domains" do
          assert_equal "", account.conflicting_domain_list
        end
      end

      describe "when the domain_blacklist is not empty" do
        before { account.update_attributes(domain_blacklist: "bar.com foo@bar.com") }

        it "does not return any conflicting domains" do
          assert_equal "", account.conflicting_domain_list
        end
      end
    end

    describe "when there are conflicting OrganizationDomain records" do
      before do
        account.organizations.create!(name: "some-org", domain_names: ["z3nmail.com"])
      end

      it "returns conflicting domains from the domain_blacklist" do
        account.update_attributes(domain_blacklist: "z3nmail.com")

        assert_equal "z3nmail.com", account.conflicting_domain_list
      end

      it "returns conflicting emails from the domain_blacklist" do
        account.update_attributes(domain_blacklist: "tester@z3nmail.com")

        assert_equal "tester@z3nmail.com", account.conflicting_domain_list
      end
    end

    describe "when there are conflicting OrganizationEmail records" do
      before do
        account.organizations.create!(name: "some-org", domain_names: ["tester@z3nmail.com"])
      end

      it "returns conflicting emails from the domain_blacklist" do
        account.update_attributes(domain_blacklist: "tester@z3nmail.com")

        assert_equal "tester@z3nmail.com", account.conflicting_domain_list
      end

      it "does not return conflicting domains from the domain_blacklist" do
        account.update_attributes(domain_blacklist: "z3nmail.com")

        assert_equal "", account.conflicting_domain_list
      end

      describe "when there are OrganizationDomain records with the same domain as the OrganizationEmail record" do
        before do
          account.organizations.create!(name: "some-other-org", domain_names: ["z3nmail.com"])
        end

        it "does not return duplicate conflicting emails from the domain_blacklist" do
          account.update_attributes(domain_blacklist: "tester@z3nmail.com")

          assert_equal "tester@z3nmail.com", account.conflicting_domain_list
        end
      end
    end
  end

  describe '#validate_canceled_on_deletion' do
    before do
      account.stubs(:is_active?).returns(false)
      account.stubs(:is_serviceable?).returns(false)

      account.deleted_at = Time.now
    end

    it 'passes if account is canceled' do
      account.subscription.canceled_on = Time.now
      account.subscription.save!

      assert account.valid?
    end

    it 'fails if account is not canceled' do
      refute account.valid?
    end
  end

  describe '#validate_route_belongs_to_account' do
    it 'passes if route belongs to account' do
      route = account.routes.create!(subdomain: "newroute")
      account.route = route
      assert account.valid?
    end

    it 'fails if route does not belong to account' do
      route = accounts(:inactive).routes.create!(subdomain: "newroute")
      account.route = route
      refute account.valid?
    end
  end

  describe "#fallback_cdn_provider" do
    describe "with cdn_provider set to default" do
      it "selects cloudfront as fallback CDN" do
        assert_equal "cloudfront", account.fallback_cdn_provider
      end
    end

    describe "with cdn_provider set to disabled" do
      it "returns no fallback CDN" do
        account.settings.cdn_provider = "disabled"
        assert_nil account.fallback_cdn_provider
      end
    end

    describe "with cdn_provider set to edgecast" do
      it "selects cloudfront as fallback CDN" do
        account.settings.cdn_provider = "edgecast"
        assert_equal "cloudfront", account.fallback_cdn_provider
      end
    end

    describe "with cdn_provider set to cloudfront" do
      it "selects edgecast as fallback CDN" do
        account.settings.cdn_provider = "cloudfront"
        assert_equal "edgecast", account.fallback_cdn_provider
      end
    end
  end

  describe "set_conditional_logging" do
    it "sets conditional_logging_pattern to nil if called without any patterns" do
      account.settings.conditional_logging_pattern = "test"
      account.set_conditional_logging
      assert_nil account.settings.conditional_logging_pattern, "pattern should be nil"
    end

    it "encodes patterns correctly" do
      account.set_conditional_logging(
        request_method: /request_id/,
        path_info: /path_info/,
        http_user_agent: /user_agent/,
        query_string: /query_string/,
        user_id: /user_id/,
        status: /status/
      )
      assert_equal account.settings.conditional_logging_pattern, "[\"(?-mix:request_id)\",\"(?-mix:path_info)\",\"(?-mix:user_agent)\",\"(?-mix:query_string)\",\"(?-mix:user_id)\",\"(?-mix:status)\",null]"
    end

    it "raises an error if pattern results in JSON too large to save in the setting" do
      ten_char_pattern = "1234567890"
      assert_raise ArgumentError do
        account.set_conditional_logging(user_id: /#{ten_char_pattern * 300}/)
      end
    end

    it "raises an error if passed in non-regex items as patterns" do
      assert_raise ArgumentError do
        account.set_conditional_logging(user_id: 123454)
      end
    end
  end

  describe 'conditional rate limits' do
    before do
      account.texts.conditional_rate_limits = nil
      account.texts.save!
    end

    describe '#add_conditional_rate_limit' do
      it 'adds the rate limit' do
        account.add_conditional_rate_limit(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          request_method: 'POST'
        )

        account.conditional_rate_limits.map { |l| l['prop_key'] }.must_include 'limit_1'
      end

      it 'adds the rate limit with expire time' do
        account.add_conditional_rate_limit(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          request_method: 'POST',
          expires_on: '2020-01-21T02:58:43+00:00'
        )

        account.conditional_rate_limits.map { |l| l['expires_on'] }.must_include '2020-01-21T02:58:43+00:00'
      end
    end

    describe '#add_conditional_rate_limit!' do
      it 'adds the rate limit and saves the account' do
        account.expects(:save!)

        account.add_conditional_rate_limit!(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          request_method: 'POST'
        )
      end

      it 'adds the rate limit with an expiry date and saves the account' do
        account.expects(:save!)

        account.add_conditional_rate_limit!(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          request_method: 'POST',
          expires_on: '2020-01-21T02:58:43+00:00'
        )
      end
    end

    describe '#remove_conditional_rate_limit!' do
      before do
        account.add_conditional_rate_limit!(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          request_method: 'POST'
        )

        account.add_conditional_rate_limit!(
          key:      'limit_2',
          limit:    1,
          interval: 1,
          request_method: 'PUT'
        )
      end

      it 'removes only the specified rate limit' do
        account.remove_conditional_rate_limit!('limit_1')

        keys = account.reload.conditional_rate_limits.map { |l| l['prop_key'] }

        keys.wont_include 'limit_1'
        keys.must_include 'limit_2'
      end
    end

    describe '#clear_conditional_rate_limits!' do
      before do
        account.add_conditional_rate_limit!(
          key:      'limit_1',
          limit:    1,
          interval: 1,
          request_method: 'POST'
        )

        account.add_conditional_rate_limit!(
          key:      'limit_2',
          limit:    1,
          interval: 1,
          request_method: 'PUT'
        )
      end

      it 'removes all rate limits' do
        account.clear_conditional_rate_limits!

        account.reload.conditional_rate_limits.must_be_nil
      end
    end
  end

  describe "#restore" do
    let(:method_call_instrumentation) { stub }

    before do
      account.cancel!
      account.subscription.credit_card = CreditCard.new
      account.subscription.canceled_on = 130.days.ago
      account.subscription.save!
      account.soft_delete!
      account.deleted_at = 40.days.ago
      account.save!

      @audit = account.data_deletion_audits.first
      @audit.created_at = 40.days.ago
      @audit.save!

      Zendesk::MethodCallInstrumentation.stubs(:new).returns(method_call_instrumentation)
      method_call_instrumentation.stubs(:perform)
    end

    it 'performs method call instrumentation' do
      method_call_instrumentation.expects(:perform)
      account.restore!
    end

    it "throws exception if account is partially deleted" do
      @audit.fail!(StandardError.new("test"))
      assert_raises RuntimeError do
        account.restore!
      end
    end

    it "cancels waiting data deletion jobs" do
      account.restore!
      assert @audit.reload.canceled?
    end

    it "un soft-deletes the account" do
      account.restore!
      assert_nil account.reload.deleted_at
    end

    it "resets canceled_on date" do
      Timecop.freeze Date.today
      account.restore!
      assert_equal account.subscription.canceled_on, Date.today
    end

    describe "with a finished moved audit" do
      before do
        audit = account.data_deletion_audits.create(reason: 'moved')
        audit.finish!
      end

      it "un soft-deletes the account" do
        account.restore!
        assert_nil account.reload.deleted_at
      end

      it "does not cancel the audit" do
        account.restore!
        audit = account.data_deletion_audits.find(&:for_shard_move?)
        assert_equal audit.finished?, true
      end
    end
  end

  describe "#min_autocomplete_chars" do
    it "is memoized and cached" do
      Rails.cache.expects(:fetch).with("#{account.id}/min_autocomplete_chars", anything).returns(100).once
      val = account.min_autocomplete_chars
      assert_equal val, account.min_autocomplete_chars
    end
  end

  describe 'updating the IP settings of an account' do
    before do
      @multiproduct_account = setup_shell_account
      Arturo.enable_feature!(:notify_settings_change_to_zopim)
      mock_connection = mock
      mock_connection.expects(:notify_account_settings_change).returns(true)
      Zendesk::Accounts::Client.any_instance.stubs(:chat_product).returns(true)
      Zopim::InternalApiClient.expects(:new).with(@multiproduct_account.id).returns(mock_connection)
    end

    it 'calls Chat for updating the ip range' do
      @multiproduct_account.ip_ranges = '1.2.3.4'
      @multiproduct_account.save
    end

    it 'calls Chat for changing enable_agent_ip_restrictions' do
      @multiproduct_account.settings.enable_agent_ip_restrictions = true
      @multiproduct_account.save
    end

    it 'calls Chat for changing ip_restriction_enabled' do
      @multiproduct_account.settings.ip_restriction_enabled = true
      @multiproduct_account.save
    end
  end

  describe "cancelling an account" do
    it "not call set_cancellation_date if is_active or is_serviceable are unchanged" do
      account.expects(:set_cancellation_date).never
      account.subdomain = "newsubdomain"
      assert account.save
    end

    it "call set_cancellation_date if is_active and is_serviceable changes" do
      account.expects(:set_cancellation_date).once
      account.is_serviceable = false
      account.is_active = false
      assert account.save
    end

    it "call set_cancellation_date if is_active changes" do
      account.expects(:set_cancellation_date).once
      account.is_active = false
      assert account.save
    end

    it "call set_cancellation_date if is_serviceable changes" do
      account.expects(:set_cancellation_date).once
      account.is_serviceable = false
      assert account.save
    end

    it "set churned_on date if inactive and unserviceable" do
      account.is_serviceable = false
      account.is_active = false
      account.save
      account.reload
      assert account.subscription.churned_on.present?
    end

    it "not set churned_on date if active, not serviceable" do
      account.is_serviceable = false
      account.save
      account.reload
      assert account.subscription.churned_on.nil?
    end

    # setting an account inactive also sets it unserviceable.

    it "clear the churned_on date when fully reactivated" do
      account.is_active = false
      account.save
      account.reload
      assert account.subscription.churned_on.present?

      account.is_active = true
      account.is_serviceable = true
      account.save
      account.reload
      assert account.subscription.churned_on.nil?
    end

    it "clear the churned_on date when account becomes active" do
      account.is_active = false
      account.save
      account.reload
      assert account.subscription.churned_on.present?

      account.is_active = true
      account.save
      account.reload
      assert account.subscription.churned_on.nil?
    end
  end

  describe "#support_addresses_cache_key" do
    it "returns cache key for the current account" do
      account.send(:support_addresses_cache_key).must_equal "account/#{account.id}/ticket_sharing_partner_support_addresses"
    end
  end

  describe "#to_liquid" do
    def render(template)
      Liquid::Template.parse(template).render('account' => account)
    end

    it "renders {{account.incoming_phone_number_ID}}" do
      account.stubs(:phone_numbers).returns([stub(id: 33, number: "8675309", to_liquid: "8675309")])
      render("{{account.incoming_phone_number_33}}").must_equal "8675309"
    end

    it "renders {{account}}" do
      render("{{account}}").must_equal "Minimum account"
    end

    it "renders {{account.name}}" do
      render("{{account.name}}").must_equal "Minimum account"
    end
  end

  describe "#ticket_sharing_partner_support_addresses" do
    let(:key) { account.send(:support_addresses_cache_key) }

    before do
      account.stubs(:sharing_agreements).returns([FactoryBot.create(:agreement)])
    end

    describe "when cached addresses are available" do
      let(:addresses) { ["bar@example.com"] }

      before do
        Rails.cache.fetch(key) do
          addresses
        end
      end

      it "does not enqueue the ticket sharing support addresses job" do
        TicketSharingSupportAddressesJob.expects(:enqueue).never
        account.ticket_sharing_partner_support_addresses
      end

      it "returns cached addresses" do
        account.expects(:known_ticket_sharing_partner_identities).never
        account.ticket_sharing_partner_support_addresses.must_equal addresses
      end
    end

    describe "when addresses are not cached" do
      before do
        Rails.cache.delete(key)
      end

      it "enqueues the ticket sharing support addresses job even when refresh is not requested" do
        TicketSharingSupportAddressesJob.expects(:enqueue)
        account.ticket_sharing_partner_support_addresses
      end

      it "returns known sharing partner identities from database" do
        TicketSharingSupportAddressesJob.stubs(:enqueue).returns(nil)
        known_identities = account.user_email_identities.ticket_sharing_partners.pluck(:value)
        account.ticket_sharing_partner_support_addresses.must_equal known_identities
      end
    end

    it "refreshes the cache after 1 day" do
      addresses = account.ticket_sharing_partner_support_addresses
      Rails.cache.read(account.send(:support_addresses_cache_key)).wont_be_nil

      Timecop.travel(1.day) do
        Rails.cache.read(account.send(:support_addresses_cache_key)).must_be_nil
        account.ticket_sharing_partner_support_addresses.must_equal addresses
      end
    end

    it "returns early if there are no sharing agreements" do
      account.stubs(:sharing_agreements).returns([])
      Rails.cache.delete(key)
      TicketSharingSupportAddressesJob.expects(:enqueue).never
      account.ticket_sharing_partner_support_addresses.must_equal []
    end
  end

  describe "#refresh_ticket_sharing_partner_support_addresses" do
    let(:error_response) { TicketSharing::FakeHttpResponse.new(500, "testbody") }

    before do
      @agreement = create_valid_agreement(status: :accepted)
    end

    describe "when agreement's support addresses are successfully retrieved" do
      let(:old_support_addresses) { ["ticket_sharing_partner@example.com"] }
      let(:new_support_addresses) { ["deliverable@example.com"] }

      before do
        Sharing::Agreement.any_instance.stubs(:support_addresses).returns(new_support_addresses.dup)
      end

      it "caches all support addresses for all accounts we have ticket sharing with" do
        account.refresh_ticket_sharing_partner_support_addresses
        Rails.cache.read(account.send(:support_addresses_cache_key)).must_equal new_support_addresses
      end

      it "marks a user identity that is a support address of the remote account as a ticket sharing partner" do
        account.refresh_ticket_sharing_partner_support_addresses
        user_identity = user_identities(:deliverable_identity)
        assert_equal DeliverableStateType.TICKET_SHARING_PARTNER, user_identity.deliverable_state
      end

      it "marks a user identity that is no longer a support address of the remote account as deliverable" do
        account.refresh_ticket_sharing_partner_support_addresses
        user_identity = user_identities(:ticket_sharing_partner_identity)
        assert_equal DeliverableStateType.DELIVERABLE, user_identity.deliverable_state
      end

      it "does not attempt to update an identity whose deliverable state has not changed" do
        Sharing::Agreement.any_instance.stubs(:support_addresses).returns(old_support_addresses + new_support_addresses)
        UserEmailIdentity.any_instance.expects(:mark_deliverable_state).times(1)
        account.refresh_ticket_sharing_partner_support_addresses
      end
    end

    describe "when support_addresses returns an HTTP error response" do
      before do
        @agreement2 = create_valid_agreement(status: :accepted)
        Sharing::Agreement.any_instance.stubs(:support_addresses).returns(["support addresses 1"], error_response)
      end

      it "updates only user identities that are now ticket sharing partners" do
        account.expects(:mark_identities).with(["support addresses 1"], false)
        account.refresh_ticket_sharing_partner_support_addresses
      end

      it "caches only addresses and not error responses" do
        account.refresh_ticket_sharing_partner_support_addresses
        Rails.cache.read(account.send(:support_addresses_cache_key)).must_equal ["support addresses 1"]
      end
    end

    describe "when there is more than one accepted sharing agreement" do
      before do
        @agreement2 = create_valid_agreement(status: :accepted)
      end

      it "collects support addresses from both agreements" do
        Sharing::Agreement.any_instance.stubs(:support_addresses).returns(["support addresses 1"], ["support addresses 2"])
        account.refresh_ticket_sharing_partner_support_addresses
        Rails.cache.read(account.send(:support_addresses_cache_key)).must_equal ["support addresses 1", "support addresses 2"]
      end

      describe "when one agreement returns an HTTP error response" do
        it "only caches the support addresses of the account from the other agreement" do
          Sharing::Agreement.any_instance.stubs(:support_addresses).returns("support addresses 1", error_response)
          account.refresh_ticket_sharing_partner_support_addresses
          Rails.cache.read(account.send(:support_addresses_cache_key)).must_equal ["support addresses 1"]
        end
      end
    end

    describe "when the agreement's remote account has been deleted" do
      before do
        @agreement.stubs(:support_addresses).returns([])
      end

      it "caches an empty array" do
        account.refresh_ticket_sharing_partner_support_addresses
        Rails.cache.read(account.send(:support_addresses_cache_key)).must_equal []
      end
    end
  end

  describe '#disable_triggers' do
    it 'deactivates valid triggers' do
      trigger_title = 'Notify requester of received request'
      trigger = account.triggers.find_by_title(trigger_title)
      assert(trigger.is_active)

      account.disable_triggers(
        [
          'txt.default.triggers.notify_requester_received.title',
          'some.invalid.trigger.key'
        ]
      )

      trigger = account.triggers.find_by_title(trigger_title)

      assert_equal false, trigger.is_active
    end
  end

  describe "#has_google_apps?" do
    it "returns false when route has nil gam_domain" do
      account.role_settings.update_attribute(:agent_google_login, true)
      refute account.has_google_apps?
    end

    it "returns false when agent google login is disabled" do
      account.route.update_attribute(:gam_domain, 'foo.com')
      refute account.has_google_apps?
    end

    it "returns true when both conditions are true" do
      refute account.settings.has_google_apps?
      account.route.update_attribute(:gam_domain, 'foo.com')
      account.role_settings.update_attribute(:agent_google_login, true)

      assert account.has_google_apps?
    end
  end

  describe "#update_has_google_apps" do
    it "returns false when route has nil gam_domain" do
      account.role_settings.update_attribute(:agent_google_login, true)
      # after_update callback will invoke account.update_has_google_apps

      refute_nil account.settings.has_google_apps
      refute account.settings.has_google_apps
    end

    it "returns false when agent google login is disabled" do
      account.route.update_attribute(:gam_domain, 'foo.com')
      account.update_has_google_apps

      refute_nil account.settings.has_google_apps
      refute account.settings.has_google_apps
    end

    it "returns true when both conditions are true" do
      refute account.settings.has_google_apps?
      account.route.update_attribute(:gam_domain, 'foo.com')
      account.role_settings.update_attribute(:agent_google_login, true)
      # after_update callback will invoke account.update_has_google_apps
      account.reload
      assert account.settings.has_google_apps
    end
  end

  describe "#enable_google_login!" do
    it "enables agent and end-user google login but disables agent zendesk login" do
      assert account.role_settings.agent_zendesk_login
      refute account.role_settings.agent_google_login
      refute account.role_settings.end_user_google_login

      account.enable_google_login!
      refute account.role_settings.agent_zendesk_login
      assert account.role_settings.agent_google_login
      assert account.role_settings.end_user_google_login
    end
  end

  describe "#enable_office_365_login!" do
    it "enables agent and end-user office 365 login but disables agent zendesk login" do
      assert account.role_settings.agent_zendesk_login
      refute account.role_settings.agent_office_365_login
      refute account.role_settings.end_user_office_365_login

      account.enable_office_365_login!
      refute account.role_settings.agent_zendesk_login
      assert account.role_settings.agent_office_365_login
      assert account.role_settings.end_user_office_365_login
    end
  end

  describe "#enable_google_apps!" do
    it "enables google login, updates route and settings" do
      google_domain = 'googledomain.com'
      account.enable_google_apps!(google_domain)

      assert_equal google_domain, account.route.gam_domain
      assert_equal google_domain, account.settings.google_apps_domain
      assert account.settings.has_google_apps
      assert account.settings.switched_to_google_apps
    end
  end

  describe "#disable_google_apps!" do
    it "disables google login, updates route and settings" do
      account.disable_google_apps!

      assert account.role_settings.agent_zendesk_login
      refute account.role_settings.agent_google_login
      refute account.role_settings.end_user_google_login
      assert_nil account.route.gam_domain
      assert_nil account.settings.google_apps_domain
      refute account.settings.has_google_apps
      refute account.settings.has_google_apps_admin
      refute account.settings.switched_to_google_apps
    end
  end

  describe "#help_center_onboarding_state" do
    it "returns :enabled when help_center_state is :disabled" do
      account.stubs(:help_center_state).returns(:disabled)
      assert_equal :enabled, account.help_center_onboarding_state
    end

    it "returns :disabled when help_center_state is not :disabled" do
      account.stubs(:help_center_state).returns("foo")
      assert_equal :disabled, account.help_center_onboarding_state
    end
  end

  describe "#email_settings_path" do
    it "returns /agent/admin/email otherwise" do
      assert_equal "/agent/admin/email", account.email_settings_path
    end
  end

  describe "account product sync validation" do
    let(:account) { FactoryBot.create(:account) }

    it "enqueues an AccountProductValidationJob when saving account record" do
      account.expects(:instrument_product_sync_validation).at_least_once
      account.subdomain = 'subdomain2'
      account.save!
    end
  end

  describe "is_trial?" do
    it "returns true for a trial subscription" do
      account = accounts(:trial)
      assert account.subscription.is_trial?
      assert account.is_trial?
    end

    it "returns false for a paid subscription" do
      refute account.subscription.is_trial?
      refute account.is_trial?
    end

    describe 'missing subscriptions' do
      it 'returns false for multiproduct account' do
        account = accounts(:multiproduct)
        account.subscription = nil
        refute account.is_trial?
      end

      it 'throws an exception for non-multiproduct account' do
        account = accounts(:minimum)
        account.subscription.credit_card.delete
        account.reload
        account.subscription = nil
        assert_raises RuntimeError do
          account.is_trial?
        end
      end
    end
  end

  describe '#subscription' do
    describe 'subscription record present' do
      let(:account) { accounts(:minimum) }

      it 'returns a subscription record' do
        assert_instance_of Subscription, account.subscription
      end
    end

    describe 'subscription record missing' do
      let(:account) { Account.new }

      it 'returns a nil subscription record' do
        assert_instance_of NilSubscription, account.subscription
      end
    end
  end

  describe "#fallback_ticket_form" do
    before do
      @default_ticket_form = TicketForm.init_default_form(account)
      @default_ticket_form.save!
    end

    describe "when the ticket has a brand" do
      before do
        @brand_with_form = FactoryBot.create(:brand, account_id: account.id)
        @restricted_ticket_form = account.ticket_forms.create!(name: "form with restrictions", position: 2, default: false, in_all_brands: false)
        @restricted_ticket_form.ticket_form_brand_restrictions.create! do |tfbr|
          tfbr.account = account
          tfbr.brand   = @brand_with_form
        end
      end

      it "sets the account default ticket form if the brand has no active ticket forms" do
        brand2 = FactoryBot.create(:brand, account_id: account.id)
        @default_ticket_form.update_attributes(in_all_brands: false)
        @default_ticket_form.reload

        assert_equal [], brand2.ticket_forms
        assert_equal @default_ticket_form, account.fallback_ticket_form(brand2)
      end

      it "sets the first active ticket form belonging to the brand if the default ticket form is not available to the brand" do
        @default_ticket_form.update_attributes(in_all_brands: false)

        assert_equal false, @brand_with_form.ticket_forms.active.include?(@default_ticket_form)
        assert_equal @restricted_ticket_form, account.fallback_ticket_form(@brand_with_form)
      end

      it "sets the default ticket form if it is available to the brand even if it is not the first active ticket form belonging to the brand" do
        @default_ticket_form.update_attributes(in_all_brands: false)
        @default_ticket_form.update_attributes(position: 3)
        @default_ticket_form.ticket_form_brand_restrictions.create! do |tfbr|
          tfbr.account = account
          tfbr.brand   = @brand_with_form
        end

        assert_equal @restricted_ticket_form, @brand_with_form.ticket_forms.active.first
        assert_equal @default_ticket_form, account.fallback_ticket_form(@brand_with_form)
      end
    end

    it "sets the account default ticket form if brand is nil" do
      assert_equal @default_ticket_form, account.fallback_ticket_form(nil)
    end
  end

  describe "when help center is disabled" do
    it "disables required registration setting on save" do
      account.is_signup_required = true
      account.disable_help_center
      account.save!
      refute account.is_signup_required
    end
  end

  describe '#answer_bot_subscription' do
    subject { account.answer_bot_subscription }

    it 'queries the accounts service' do
      AnswerBot::Subscription.expects(:find_for_account).with(account)
      subject
    end
  end

  describe '#create_answer_bot_subscription' do
    subject { account.create_answer_bot_subscription }

    it 'creates the product using the accounts service' do
      AnswerBot::Subscription.expects(:create_for_account).with(account)
      subject
    end
  end

  describe '#outbound_subscription' do
    subject { account.outbound_subscription }

    it 'queries the accounts service' do
      Outbound::Subscription.expects(:find_for_account).with(account)
      subject
    end
  end

  describe '#create_outbound_subscription' do
    subject { account.create_outbound_subscription }

    it 'creates the product using the accounts service' do
      Outbound::Subscription.expects(:create_for_account).with(account)
      subject
    end
  end

  describe '#explore_subscription' do
    it 'queries the accounts service' do
      Explore::Subscription.expects(:find_for_account).with(account)
      account.explore_subscription
    end
  end

  describe '#create_explore_subscription' do
    describe 'with no attributes' do
      it 'creates an empty product using the accounts service' do
        Explore::Subscription.expects(:create_for_account).with(account, nil)
        account.create_explore_subscription
      end
    end

    describe 'with attributes' do
      let(:attributes) { { state: :trial, plan_type: 3, max_agents: 5 } }

      it 'creates the product with attributes using the accounts service' do
        Explore::Subscription.expects(:create_for_account).with(
          account,
          attributes
        )
        account.create_explore_subscription(attributes)
      end
    end
  end

  describe '#billing_multi_product_participant?' do
    let(:participation) do
      account.billing_multi_product_participant?
    end

    describe 'when account is not soba-participation eligible' do
      before { Billing::SobaParticipation.stubs(eligible?: false) }

      it { refute participation }
    end

    describe 'when account is soba-participation eligible' do
      before { Billing::SobaParticipation.stubs(eligible?: true) }

      it { assert participation }
    end
  end

  describe '#suite_allowed?' do
    before  { account.stubs(:has_active_suite?).returns(has_active_suite) }
    subject { account.suite_allowed? }

    describe 'when account has Suite product' do
      let(:has_active_suite) { true }

      it { assert subject }
    end

    describe 'when account does not have Suite product' do
      let(:has_active_suite) { false }

      describe 'when :billing_suite arturo is OFF' do
        before { Arturo.disable_feature!(:billing_suite) }
        it     { refute subject }
      end

      describe 'when :billing_suite arturo is ON' do
        before do
          Arturo.enable_feature!(:billing_suite)
          account.stubs(:no_chat_phase_one?).returns(no_chat_phase_one)
        end

        describe 'when account has Zopim Phase 1' do
          let(:no_chat_phase_one) { false }

          it { refute subject }
        end

        describe 'when account does not have Zopim Phase 1' do
          let(:no_chat_phase_one) { true }

          describe 'when :suite_purchase_override arturo is ON' do
            before { Arturo.enable_feature!(:suite_purchase_override) }
            it     { assert subject }
          end

          describe 'when :suite_purchase_override arturo is OFF' do
            before { Arturo.disable_feature!(:suite_purchase_override) }

            describe 'when :restrict_suite arturo is ON' do
              before { Arturo.enable_feature!(:restrict_suite) }
              it     { refute subject }
            end

            describe 'when :restrict_suite arturo is OFF' do
              let(:launch_date) { ENV['BILLING_SUITE_GA_DATETIME'] }
              let(:after_ga)    { Time.parse(launch_date) + 8.hours }
              let(:before_ga)   { Time.parse(launch_date) - 8.hours }
              let(:created_at)  { after_ga }

              before do
                Arturo.disable_feature!(:restrict_suite)
                account.stubs(:created_at).returns(created_at)
              end

              describe 'when environment variable is not present' do
                with_env BILLING_SUITE_GA_DATETIME: nil do
                  let(:fallback_date)        { launch_date }
                  let(:parsed_fallback_date) { fallback_date.to_time }

                  before do
                    Account.send(:remove_const, 'SUITE_GA_DATETIME_FALLBACK')
                    Account.const_set(
                      'SUITE_GA_DATETIME_FALLBACK',
                      fallback_date
                    )
                  end

                  it 'uses fallback constant and logs' do
                    Time.expects(:parse).
                      with(fallback_date).
                      at_least_once.
                      returns(parsed_fallback_date)

                    assert subject
                  end
                end
              end

              describe 'when account was created after Suite GA' do
                let(:created_at) { after_ga }

                it { assert subject }
              end

              describe 'when account was created before Suite GA' do
                let(:created_at) { before_ga }

                it { refute subject }
              end
            end
          end
        end
      end
    end
  end

  describe '#zuora_account_id' do
    subject { account.zuora_account_id }

    describe 'when billing_id attr is set' do
      before do
        account.stubs(:billing_id).returns('cafefeed')
      end

      it { assert_equal 'cafefeed', subject }
    end

    describe 'when billing_id attr is not set' do
      before do
        account.stubs(:billing_id).returns(nil)
      end

      describe 'and it has a zuora subscription' do
        let(:zuora_subscription) do
          stub(zuora_account_id: 'cafefeed')
        end

        before do
          account.subscription.stubs(:zuora_subscription).
            returns(zuora_subscription)
        end

        it { assert_equal 'cafefeed', subject }
      end

      describe 'and it has no zuora subscription' do
        before do
          account.subscription.stubs(:zuora_subscription).returns(nil)
        end

        it { assert_nil subject }
      end
    end
  end

  describe '#spp?' do
    describe_with_arturo_enabled :ocp_spp do
      describe 'when Account Services indicate an spp account' do
        before do
          Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns({"is_spp" => true})
        end

        it 'returns true' do
          assert(account.spp?)
        end
      end

      describe 'when Account Services do not indicate an spp account' do
        before do
          Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns({"is_spp" => false})
        end

        it 'returns false' do
          assert_equal(false, account.spp?)
        end
      end

      describe 'when a network error occurs' do
        before do
          Faraday::Connection.any_instance.stubs(:get).raises(Faraday::Error.new('big bad error'))
          Rails.logger.stubs(:error)
        end

        it 'logs the error and returns false' do
          assert_equal(false, account.spp?)
        end
      end
    end

    describe_with_arturo_disabled :ocp_spp do
      before do
        Zendesk::Accounts::Client.any_instance.expects(:account_from_account_service).never
      end

      it 'returns false' do
        assert_equal(false, account.spp?)
      end
    end
  end

  describe '#spp_suite?' do
    describe_with_arturo_enabled :ocp_spp do
      describe 'when Account Services indicate an spp account' do
        let(:support_spp_non_suite) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_trial_spp_non_suite).with_indifferent_access) }
        let(:support_spp_suite) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_trial_spp_suite).with_indifferent_access) }
        let(:support_not_spp) { support_trial_product }

        before do
          Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns({'is_spp' => true})
        end

        describe 'when Support skus is zendesk_suite' do
          before do
            Zendesk::Accounts::Client.any_instance.stubs(:product!).returns(support_spp_suite)
          end

          it 'returns true' do
            assert(account.spp_suite?)
          end
        end

        describe 'when Support skus is support' do
          before do
            Zendesk::Accounts::Client.any_instance.stubs(:product!).returns(support_spp_non_suite)
          end

          it 'returns false' do
            assert_equal(false, account.spp_suite?)
          end
        end

        describe 'when Support skus is empty' do
          before do
            Zendesk::Accounts::Client.any_instance.stubs(:product!).returns(support_not_spp)
          end

          it 'returns false' do
            assert_equal(false, account.spp_suite?)
          end
        end
      end

      describe 'when Account Services do not indicate an spp account' do
        before do
          Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns({'is_spp' => false})
        end

        it 'returns false' do
          assert_equal(false, account.spp_suite?)
        end
      end
    end

    describe_with_arturo_disabled :ocp_spp do
      it 'returns false' do
        Zendesk::Accounts::Client.any_instance.expects(:product).never
        assert_equal(false, account.spp_suite?)
      end
    end
  end

  describe '#products' do
    let(:expected_products) { [support_trial_product, chat_inactive_product] }

    before do
      Zendesk::Accounts::Client.any_instance.stubs(:products).returns(expected_products)
    end

    it 'returns products' do
      actual_products = multiproduct_account.products
      assert_equal expected_products, actual_products
    end

    describe 'caching behavior' do
      subject { multiproduct_account.products(use_cache: use_cache) }

      describe 'default' do
        subject { multiproduct_account.products }

        describe_with_arturo_enabled :ocp_account_products_use_cache do
          it 'retrieves products from account service using cache' do
            Zendesk::Accounts::Client.any_instance.expects(:products).with(use_cache: true)
            subject
          end
        end

        describe_with_arturo_disabled :ocp_account_products_use_cache do
          it 'retrieves products from account service without using cache' do
            Zendesk::Accounts::Client.any_instance.expects(:products).with(use_cache: false)
            subject
          end
        end
      end

      describe 'use cache' do
        let(:use_cache) { true }

        it 'retrieves products from account service using cache' do
          Zendesk::Accounts::Client.any_instance.expects(:products).with(use_cache: true)

          subject
        end
      end

      describe 'not to use cache' do
        let(:use_cache) { false }

        it 'retrieves products from account service without using cache' do
          Zendesk::Accounts::Client.any_instance.expects(:products).with(use_cache: false)

          subject
        end
      end
    end
  end

  describe '#active_products?' do
    it 'returns true when at least one active product' do
      products = [support_trial_product, chat_inactive_product]
      multiproduct_account.stubs(:products).returns(products)
      assert multiproduct_account.active_products?
    end

    it 'returns false when no active products' do
      products = [support_inactive_product, chat_inactive_product]
      multiproduct_account.stubs(:products).returns(products)
      refute multiproduct_account.active_products?
    end

    it 'returns false when there are no products' do
      products = []
      multiproduct_account.stubs(:products).returns(products)
      refute multiproduct_account.active_products?
    end
  end

  describe_with_arturo_disabled :render_custom_uri_as_links do
    it "returns an empty array by default" do
      assert_equal [], account.render_custom_uri_hyperlinks
    end

    it "returns an empty array even when a value is present in the database" do
      account.texts.render_custom_uri_hyperlinks = "custom1, custom2"
      account.save!

      account.reload

      assert_equal [], account.render_custom_uri_hyperlinks
    end
  end

  describe_with_arturo_enabled :render_custom_uri_as_links do
    it "returns the list of custom uri schemes added by the admin in settings" do
      account.texts.render_custom_uri_hyperlinks = "custom1, custom2"
      account.save!

      account.reload

      assert_equal ["custom1", "custom2"], account.render_custom_uri_hyperlinks
    end
  end

  describe '#support_suite_trial_refresh_capability' do
    let(:account) { FactoryBot.build(:account) }
    describe 'older accounts fallback' do
      # NOTE: intentionally not saving the account to simulate an account created before support_suite_trial_refresh_capability existed
      it 'has onboarding capability of SUPPORT_SUITE_TRIAL_REFRESH' do
        assert_equal Account::OnboardingSupport::SUPPORT_SUITE_TRIAL_REFRESH, account.settings.checklist_onboarding_version
      end
    end

    describe_with_arturo_disabled :support_suite_trial_refresh_phase2 do
      before do
        Arturo.disable_feature!(:support_suite_trial_refresh_phase2)
        account.save!
      end

      it 'sets onboarding capability to SUPPORT_SUITE_TRIAL_REFRESH if enable support_suite_trial_refresh' do
        assert_equal Account::OnboardingSupport::SUPPORT_SUITE_TRIAL_REFRESH, account.settings.checklist_onboarding_version
      end
    end

    describe_with_arturo_enabled :support_suite_trial_refresh_phase2 do
      before do
        Arturo.enable_feature!(:support_suite_trial_refresh_phase2)
        account.save!
      end

      it 'sets onboarding capability to SUPPORT_SUITE_TRIAL_REFRESH_phase2' do
        assert_equal Account::OnboardingSupport::SUPPORT_SUITE_TRIAL_REFRESH_PHASE2, account.settings.checklist_onboarding_version
      end
    end
  end

  def create_account_from_hash(values, currency_name = 'USD')
    @new_account = values.fetch(:class, Accounts::Classic).new do |a|
      a.name              = values[:name]
      a.subdomain         = values[:subdomain]
      a.time_zone         = values[:time_zone]
      a.help_desk_size    = values[:help_desk_size]
      a.trial_coupon_code = values[:trial_coupon_code]
    end

    @new_account.translation_locale = TranslationLocale.last

    owner_attributes = {name: 'Random Person', email: 'radnom.person@example.com', is_verified: false }
    @new_account.set_owner(owner_attributes)

    @new_account.set_address(country_id: 155, phone: '123456')
    @new_account.set_currency(currency_name)
    @new_account
  end

  def default_account_hash
    {
      name: 'NewTestAccount',
      subdomain: Token.generate(10),
      time_zone: 'Copenhagen',
      help_desk_size: "Small team"
    }
  end

  def account_fully_deleted(account_id)
    assert_raise(ActiveRecord::RecordNotFound) { Account.find(account_id) }

    [
      User, Event, Entry, Post, Forum, Group, Ticket, Logo, Attachment, Tagging,
      Payment, Sequence, Organization, UserIdentity
    ].each do |model|
      model_count = model.where(account_id: account_id).count(:all)
      assert_equal 0, model_count, "#{model.name} not fully deleted, #{model_count} stragglers"
    end

    assert_equal 0, Rule.where(owner_type: 'Account', owner_id: account_id).count(:all)

    true
  end
end
