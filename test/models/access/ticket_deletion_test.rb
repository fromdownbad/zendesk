require_relative "../../support/test_helper"
require_relative "../../support/satisfaction_test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::TicketDeletion' do
  fixtures :accounts, :account_property_sets, :users, :groups, :memberships, :user_identities, :subscriptions, :payments, :sequences, :tickets, :events, :ticket_fields, :organizations

  describe "Legacy users" do
    before do
      @account = accounts(:minimum)
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
      @user = users(:minimum_agent)
      @user.permission_set = nil
      @user.save!
    end

    it "is not is able to delete tickets" do
      refute @user.can?(:delete, Ticket)
      @user.account.tickets.all.each do |ticket|
        refute @user.can?(:delete, ticket)
      end
    end
  end

  describe "With permission sets" do
    before do
      @account = accounts(:minimum)
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
      @user = users(:minimum_agent)
      @user.permission_set = accounts(:minimum).permission_sets.create!(name: "sfds")
      @user.save!
    end

    describe "Deleting tickets" do
      describe "an admin" do
        before { @user = users(:minimum_admin) }
        it "is able to delete any ticket" do
          assert @user.can?(:delete, Ticket)
          @user.account.tickets.all.each do |ticket|
            assert @user.can?(:delete, ticket)
          end
        end
      end

      describe "an agent" do
        before do
          @user = users(:minimum_agent)
        end

        describe "with the ticket_deletion permission" do
          before { @user.permission_set.permissions.enable(:ticket_deletion) }

          describe "when settings#ticket_delete_for_agents is not set" do
            before { @user.account.settings.expects(:ticket_delete_for_agents?).never }

            it "is still is able to delete tickets" do
              assert @user.can?(:delete, Ticket)
              @user.account.tickets.all.each do |ticket|
                assert @user.can?(:delete, ticket)
              end
            end
          end

          describe "when settings#ticket_delete_for_agents is set" do
            before { @user.account.settings.expects(:ticket_delete_for_agents?).never }

            it "is able to delete tickets" do
              assert @user.can?(:delete, Ticket)
              @user.account.tickets.all.each do |ticket|
                assert @user.can?(:delete, ticket)
              end
            end
          end
        end

        describe "without the ticket_deletion permission" do
          before { @user.permission_set.permissions.disable(:ticket_deletion) }

          describe "when settings#ticket_delete_for_agents is not set" do
            before { @user.account.settings.expects(:ticket_delete_for_agents?).never }

            it "is not is able to delete tickets" do
              refute @user.can?(:delete, Ticket)
              @user.account.tickets.all.each do |ticket|
                refute @user.can?(:delete, ticket)
              end
            end
          end
          describe "when settings#ticket_delete_for_agents is set" do
            before { @user.account.settings.expects(:ticket_delete_for_agents?).never }
            it "is not is able to delete tickets" do
              refute @user.abilities(:reload).can?(:delete, Ticket)
              @user.account.tickets.all.each do |ticket|
                refute @user.can?(:delete, ticket)
              end
            end
          end
        end
      end

      describe "an end user" do
        before { @user = users(:minimum_end_user) }
        it "not is able to delete any ticket" do
          refute @user.can?(:delete, Ticket)
          @user.account.tickets.all.each do |ticket|
            refute @user.can?(:delete, ticket)
          end
        end
      end
    end
  end

  describe "Without Permission Sets" do
    describe "Deleting tickets" do
      describe "an admin" do
        before { @user = users(:minimum_admin) }
        it "is able to delete any ticket" do
          assert @user.can?(:delete, Ticket)
          @user.account.tickets.all.each do |ticket|
            assert @user.can?(:delete, ticket)
          end
        end
      end

      describe "an agent" do
        before { @user = users(:minimum_agent) }
        describe "when settings#ticket_delete_for_agents is not set" do
          before { @user.account.settings.expects(:ticket_delete_for_agents?).at_least_once.returns(false) }
          it "is not is able to delete tickets" do
            refute @user.can?(:delete, Ticket)
            @user.account.tickets.all.each do |ticket|
              refute @user.can?(:delete, ticket)
            end
          end
        end
        describe "when settings#ticket_delete_for_agents is set" do
          before { @user.account.settings.expects(:ticket_delete_for_agents?).at_least_once.returns(true) }
          it "is able to delete tickets" do
            assert @user.can?(:delete, Ticket)
            @user.account.tickets.all.each do |ticket|
              assert @user.can?(:delete, ticket)
            end
          end
        end
      end

      describe "an end user" do
        before { @user = users(:minimum_end_user) }
        it "not is able to delete any ticket" do
          refute @user.can?(:delete, Ticket)
          @user.account.tickets.all.each do |ticket|
            refute @user.can?(:delete, ticket)
          end
        end
      end
    end
  end
end
