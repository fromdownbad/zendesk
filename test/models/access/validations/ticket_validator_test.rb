require_relative "../../../support/test_helper"
require_relative "../../../support/collaboration_settings_test_helper"
require_relative "../../../support/agent_test_helper"

SingleCov.covered! uncovered: 1

describe 'Abilities::Validations::TicketValidator' do
  extend ArturoTestHelper

  include AgentTestHelper

  fixtures :tickets, :users, :groups, :memberships, :accounts, :subscriptions,
    :user_identities, :sequences, :ticket_fields, :account_property_sets,
    :account_settings

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:agent) do
    users(:minimum_agent).tap do |min_agent|
      min_agent.permission_set = min_agent.account.permission_sets.create!(name: "Ticket Validator Test")
    end
  end
  let(:end_user) { users(:minimum_end_user) }
  let(:end_user_email) { "#{end_user.name} <#{end_user.email}>" }
  let(:admin) { users(:minimum_admin) }
  let(:search_end_user) { users(:minimum_search_user) }

  let(:custom_agent_ticket_editing) { true }
  let(:custom_agent_comment_access) { 'private' }
  let(:custom_agent) { create_custom_role_agent(ticket_editing: custom_agent_ticket_editing, comment_access: custom_agent_comment_access) }

  let(:expected_users_name) { "Agent Minimum" }
  let(:expected_edit_error_response) do
    {
      error: "RecordInvalid",
      description: "Record validation errors",
      details: {
        base: [
          { description: "Ticket cannot be created/updated by #{expected_users_name}", error: "AccessDenied" }
        ]
      }
    }
  end
  let(:expected_add_public_comment_error_response) do
    {
      error: "RecordInvalid",
      description: "Record validation errors",
      details: {
        base: [
          { description: "You do not have permission to add public comments.", error: "AccessDenied" }
        ]
      }
    }
  end

  describe "Creating a ticket" do
    let(:ticket) do
      agent.account.tickets.build(description: "A new ticket").tap do |ticket|
        ticket.requester = end_user
        ticket.will_be_saved_by(agent)
      end
    end

    describe "when the agent saving the ticket is a light agent" do
      let(:agent) { create_light_agent }

      describe_with_and_without_arturo_enabled :email_restricted_agent_fix do
        it "is valid" do
          assert ticket.save
          assert_empty(ticket.errors)
        end
      end
    end

    describe "when the agent saving the ticket is a custom role agent who can't comment publicly" do
      let(:agent) { custom_agent }

      describe_with_and_without_arturo_enabled :email_restricted_agent_fix do
        it "is valid" do
          assert ticket.save
          assert_empty(ticket.errors)
        end
      end
    end
  end

  describe "Editing a ticket" do
    before do
      agent.account.stubs(:has_permission_sets?).returns(true)
      ticket.stubs(:changed?).returns(true)
      assert ticket.changed?
    end

    describe "when the agent may edit ticket" do
      before do
        agent.permission_set.permissions.enable(:ticket_editing)
        assert agent.can?(:edit, ticket)
      end

      it "validates" do
        ticket.will_be_saved_by(agent)
        assert ticket.save
        assert_empty(ticket.errors)
      end

      it "validates when comment is trusted and a new comment is present for FB tickets" do
        ticket.add_comment(body: "abce")
        ticket.will_be_saved_by(agent)
        assert ticket.save
        assert_empty(ticket.errors)
      end

      describe "but cannot comment publicly" do
        let(:expected_users_name) { custom_agent.name }

        describe_with_arturo_disabled :email_restricted_agent_fix do
          it "is valid when public comment is added (bug)" do
            ticket.add_comment(body: "abce")
            ticket.will_be_saved_by(custom_agent)

            assert ticket.save
            assert_empty(ticket.errors)
          end

          it "is valid when private comment is added" do
            ticket.add_comment(body: "abce", is_public: false)
            ticket.will_be_saved_by(custom_agent)

            assert ticket.save
            assert_empty(ticket.errors)
          end

          it "is valid when requester is changed" do
            ticket.requester = admin
            ticket.will_be_saved_by(custom_agent)

            assert ticket.save
            assert_empty(ticket.errors)
          end

          it "is valid when collaborators/CCs have changed (bug)" do
            # Ticket#only_follower_collaborations_changed? will always return false when ticket_followers_allowed is disabled
            ticket.stubs(changed: ["current_collaborators"], current_collaborators_was: end_user_email, only_follower_collaborations_changed?: false)
            ticket.will_be_saved_by(custom_agent)

            assert ticket.save
            assert_empty(ticket.errors)
          end
        end

        describe_with_arturo_enabled :email_restricted_agent_fix do
          it "is valid when private comment is added" do
            ticket.add_comment(body: "abce", is_public: false)
            ticket.will_be_saved_by(custom_agent)

            assert ticket.save
            assert_empty(ticket.errors)
          end

          it "is invalid if the comment is public" do
            ticket.add_comment(body: "abce", is_public: true)
            ticket.will_be_saved_by(custom_agent)

            refute ticket.save
            assert_equal expected_add_public_comment_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
          end

          it "is valid when the requester is changed" do
            ticket.stubs(:changed).returns(["requester_id"])
            ticket.requester = end_user
            ticket.will_be_saved_by(custom_agent)

            assert ticket.save
            assert_empty(ticket.errors)
          end

          describe_with_arturo_setting_disabled :email_log_ticket_attributes_during_edit_access_check do
            it "should throw record validation error on invalid update" do
              ticket.stubs(changed: ["current_collaborators"], current_collaborators_was: end_user_email, only_follower_collaborations_changed?: false)
              ticket.will_be_saved_by(custom_agent)

              refute ticket.save
              assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
            end
          end

          describe 'with all new CCs features disabled' do
            it "is invalid when collaborators/CCs have changed" do
              # Ticket#only_follower_collaborations_changed? will always return false when ticket_followers_allowed is disabled
              ticket.stubs(changed: ["current_collaborators"], current_collaborators_was: end_user_email, only_follower_collaborations_changed?: false)
              ticket.will_be_saved_by(custom_agent)

              refute ticket.save
              assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
            end
          end

          describe 'with all new CCs features enabled' do
            before do
              CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
              ticket.stubs(changed: ["current_collaborators"], current_collaborators_was: end_user_email)
            end

            it "is valid when followers have changed" do
              ticket.stubs(only_follower_collaborations_changed?: true)
              ticket.will_be_saved_by(custom_agent)

              assert ticket.save
              assert_empty(ticket.errors)
            end

            it "is invalid when email_ccs have changed" do
              ticket.stubs(only_follower_collaborations_changed?: false)
              ticket.will_be_saved_by(custom_agent)

              refute ticket.save
              assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
            end
          end
        end
      end
    end

    describe "when the agent may not edit the ticket and is adding a flagged comment" do
      before do
        agent.permission_set.permissions.disable(:ticket_editing)
        refute agent.abilities(true).can?(:edit, ticket)
        ticket.add_flags!([EventFlagType.REGISTERED_USER_LOGGED_OUT])
      end

      it "is valid when the only changed attribute is recipients_for_latest_mail or status" do
        ticket.recipients_for_latest_mail = 'five'
        ticket.status_id = StatusType.OPEN
        ticket.will_be_saved_by(agent)
        assert ticket.save
        assert_empty(ticket.errors)
      end

      it "is valid when the only changed attribute is assignee_id" do
        ticket.assignee = admin
        ticket.will_be_saved_by(agent)
        assert ticket.save
        assert_empty(ticket.errors)
      end

      describe "when any other attribute is changed" do
        before do
          ticket.subject = 'five'
          ticket.will_be_saved_by(agent)
        end

        it "is not valid" do
          refute ticket.save
          assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
        end
      end
    end

    describe "when the agent may not edit ticket and is a light agent" do
      let!(:light_agent) { create_light_agent }
      let(:expected_users_name) { light_agent.name }

      before do
        light_agent.permission_set.permissions.disable(:ticket_editing)
        refute light_agent.abilities(true).can?(:edit, ticket)
        refute light_agent.abilities(true).can?(:publicly, Comment)
        assert light_agent.is_light_agent?

        ticket.current_collaborators = "#{end_user.name} <#{end_user.email}>, #{admin.name} <#{admin.email}>"
        ticket.will_be_saved_by(admin)
        ticket.save
      end

      it "is valid if a private comment is added" do
        ticket.add_comment(body: "body", is_public: false)
        ticket.will_be_saved_by(light_agent)

        assert ticket.save
        assert_empty(ticket.errors)
      end

      it "is invalid if a public comment is added" do
        ticket.add_comment(body: "body", is_public: true)
        ticket.will_be_saved_by(light_agent)

        refute ticket.save
        assert_equal expected_add_public_comment_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
      end

      it "is invalid if the priority is changed" do
        ticket.priority_id = PriorityType.HIGH
        ticket.will_be_saved_by(light_agent)

        refute ticket.save
        assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
      end

      describe "is valid if the requester is changed and the light agent was the requester" do
        before do
          ticket.update_column(:requester_id, light_agent.id)
          ticket.will_be_saved_by(light_agent)
        end

        it "if the new requester is an end user" do
          ticket.stubs(:changed).returns(["requester_id", "organization_id"])
          ticket.requester = end_user
          assert ticket.save
          assert_empty(ticket.errors)
        end

        it "if the new requester is an agent" do
          ticket.stubs(:changed).returns(["requester_id", "group_id"])
          ticket.requester = admin
          assert ticket.save
          assert_empty(ticket.errors)
        end
      end

      it "is invalid if the requester is changed and the light agent is NOT the requester" do
        ticket.stubs(:changed).returns(["requester_id"])
        ticket.requester = end_user

        ticket.will_be_saved_by(light_agent)
        refute ticket.save
        assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
      end

      describe "with current collaborator changes" do
        it "is valid when collaborators order has changed" do
          ticket.stubs(:changed).returns(["current_collaborators"])
          ticket.current_collaborators = "#{admin.name} <#{admin.email}>, #{end_user.name} <#{end_user.email}>"

          ticket.will_be_saved_by(light_agent)
          assert_valid ticket
        end

        it "is valid when collaborators syntax is mixed" do
          ticket.stubs(:changed).returns(["current_collaborators"])
          ticket.current_collaborators = "#{end_user.name} <#{end_user.email}>, <#{admin.email}>"

          ticket.will_be_saved_by(light_agent)
          assert_valid ticket
        end

        it "is valid if there are no collaborators listed on the ticket" do
          ticket.stubs(:changed).returns(["current_collaborators"])
          ticket.stubs(:current_collaborators).returns(nil)
          ticket.stubs(:current_collaborators_was).returns(nil)

          ticket.will_be_saved_by(light_agent)
          assert_valid ticket
        end

        it "is valid if a collaborator listed on the ticket's name has changed" do
          ticket.stubs(:changed).returns(["current_collaborators"])
          ticket.current_collaborators = "#{admin.name} new <#{admin.email}>, #{end_user.name} <#{end_user.email}>"

          ticket.will_be_saved_by(light_agent)
          assert_valid ticket
        end
      end

      describe 'with all new CCs features enabled' do
        before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

        describe 'with email_ccs and followers' do
          before do
            ticket.collaborations.build(user_id: end_user.id, collaborator_type: CollaboratorType.EMAIL_CC)
            ticket.collaborations.build(user_id: admin.id, collaborator_type: CollaboratorType.FOLLOWER)
            ticket.will_be_saved_by(light_agent)
            ticket.save!
          end

          it "is valid when followers are added by the agent" do
            ticket.set_followers = [user_id: users(:minimum_admin_not_owner).id, action: :put]
            ticket.will_be_saved_by(light_agent)

            assert_valid ticket
          end

          it "is not valid when email_ccs are added by the agent" do
            ticket.set_email_ccs = [user_id: search_end_user.id, action: :put]
            ticket.will_be_saved_by(light_agent)

            refute ticket.save
            assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
          end

          it "is valid when followers are removed by the agent" do
            ticket.set_followers = [user_id: admin.id, action: :delete]
            ticket.will_be_saved_by(light_agent)

            assert_valid ticket
          end

          it "is not valid when email_ccs are removed by the agent" do
            ticket.set_email_ccs = [user_id: end_user.id, action: :delete]
            ticket.will_be_saved_by(light_agent)

            refute ticket.save
            assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
          end
        end

        describe 'with legacy_ccs' do
          before do
            ticket.collaborations.build(user_id: end_user.id, collaborator_type: CollaboratorType.LEGACY_CC)
            ticket.collaborations.build(user_id: admin.id, collaborator_type: CollaboratorType.LEGACY_CC)
            ticket.will_be_saved_by(light_agent)
            ticket.save!
          end

          it "is valid when followers are added by the agent" do
            ticket.set_followers = [user_id: users(:minimum_admin_not_owner).id, action: :put]
            ticket.will_be_saved_by(light_agent)

            assert_valid ticket
          end

          it "is not valid when email_ccs are added by the agent" do
            ticket.set_email_ccs = [user_id: search_end_user.id, action: :put]
            ticket.will_be_saved_by(light_agent)

            refute ticket.save
            assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
          end

          it "is valid when followers are removed by the agent" do
            ticket.set_followers = [user_id: admin.id, action: :delete]
            ticket.will_be_saved_by(light_agent)

            assert_valid ticket
          end

          it "is not valid when email_ccs are removed by the agent" do
            ticket.set_email_ccs = [user_id: end_user.id, action: :delete]
            ticket.will_be_saved_by(light_agent)

            refute ticket.save
            assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
          end

          it "is not valid when email_ccs are removed via set_collaborators" do
            ticket.set_collaborators = [admin.id]
            ticket.will_be_saved_by(light_agent)

            refute ticket.save
            assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
          end
        end
      end

      describe_with_arturo_setting_disabled :ticket_followers_allowed do
        it "is not valid when collaborators are added by the agent" do
          ticket.stubs(:changed).returns(["current_collaborators"])
          ticket.additional_collaborators = "#{search_end_user.name} <#{search_end_user.email}>"

          ticket.will_be_saved_by(light_agent)
          refute ticket.save
          assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
        end

        it "is not valid when collaborators removed by the agent" do
          ticket.stubs(:changed).returns(["current_collaborators"])
          ticket.set_collaborators = end_user.id.to_s

          ticket.will_be_saved_by(light_agent)
          refute ticket.save
          assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
        end
      end

      it "is not valid when any other attribute is changed" do
        ticket.stubs(:changed).returns(["status_id"])
        ticket.will_be_saved_by(light_agent)
        refute ticket.save
        assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
      end

      it "is not valid when agreement_id is present but also not present in an associated shared_ticket" do
        shared_ticket = stub
        ticket.stubs(:changed?).returns(false)
        ticket.agreement_id = "12345"
        ticket.stubs(:shared_tickets).returns([shared_ticket])
        shared_ticket.stubs(:agreement_id).returns(98765)
        ticket.will_be_saved_by(light_agent)
        refute ticket.save
        assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
      end
    end

    describe "when the agent may not edit ticket and is a custom role agent" do
      let(:custom_agent_ticket_editing) { false }
      let(:expected_users_name) { custom_agent.name }

      it "is not valid" do
        ticket.priority_id = PriorityType.HIGH
        ticket.will_be_saved_by(custom_agent)

        refute ticket.save
        assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
      end

      it "is valid when removed collaborators are invalid" do
        end_user.stubs(:is_active?).returns(false)
        admin.stubs(:is_active?).returns(false)

        ticket.stubs(:changed).returns(["current_collaborators"])
        ticket.set_collaborators = nil

        ticket.will_be_saved_by(custom_agent)
        assert ticket.save
        assert_empty(ticket.errors)
      end

      describe "and cannot comment publicly" do
        let(:custom_agent_ticket_editing) { false }
        let(:expected_users_name) { custom_agent.name }

        it "is valid when private comment is added" do
          ticket.add_comment(body: "abce", is_public: false)
          ticket.will_be_saved_by(custom_agent)

          assert ticket.save
          assert_empty(ticket.errors)
        end

        it "is valid when status_id has changed" do
          ticket.status_id = StatusType.PENDING
          ticket.will_be_saved_by(custom_agent)

          assert ticket.save
          assert_empty(ticket.errors)
        end

        it "is invalid if the comment is public" do
          ticket.add_comment(body: "abce", is_public: true)
          ticket.will_be_saved_by(custom_agent)

          refute ticket.save
          assert_equal expected_add_public_comment_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
        end

        it "is valid when the requester is changed" do
          ticket.stubs(:changed).returns(["requester_id"])
          ticket.requester = end_user
          ticket.will_be_saved_by(custom_agent)

          refute ticket.save
          assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
        end

        describe 'with all new CCs features disabled' do
          it "invalid when collaborators/CCs have changed" do
            # Ticket#only_follower_collaborations_changed? will always return false when ticket_followers_allowed is disabled
            ticket.stubs(changed: ["current_collaborators"], current_collaborators_was: end_user_email, only_follower_collaborations_changed?: false)
            ticket.will_be_saved_by(custom_agent)

            refute ticket.save
            assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
          end

          describe_with_arturo_disabled :email_restricted_agent_fix do
            it "is invalid when collaborators/CCs have changed" do
              # Ticket#only_follower_collaborations_changed? will always return false when ticket_followers_allowed is disabled
              ticket.stubs(changed: ["current_collaborators"], current_collaborators_was: end_user_email, only_follower_collaborations_changed?: false)
              ticket.will_be_saved_by(custom_agent)

              refute ticket.save
              assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
            end

            it "is invalid when status_id has changed (bug)" do
              ticket.stubs(changed: ["status_id"])
              ticket.will_be_saved_by(custom_agent)

              refute ticket.save
              assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
            end
          end
        end

        describe 'with all new CCs features enabled' do
          before do
            CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
            ticket.stubs(changed: ["current_collaborators"], current_collaborators_was: end_user_email)
          end

          it "is valid when followers have changed" do
            ticket.stubs(only_follower_collaborations_changed?: true)
            ticket.will_be_saved_by(custom_agent)

            assert ticket.save
            assert_empty(ticket.errors)
          end

          it "invalid when email_ccs have changed" do
            ticket.stubs(only_follower_collaborations_changed?: false)
            ticket.will_be_saved_by(custom_agent)

            refute ticket.save
            assert_equal expected_edit_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
          end
        end
      end
    end

    it "uses the ticket's application_logger for logging" do
      ticket.expects(:application_logger).returns(Rails.logger)
      ticket.valid?
    end
  end

  describe "when there is no ticket type" do
    before do
      ticket.will_be_saved_by(admin)
      ticket.ticket_type_id = nil
    end

    it "is not valid" do
      refute ticket.valid?
      assert_equal 2, ticket.errors.full_messages.length
      assert_equal "Type: cannot be blank", ticket.errors.full_messages[0]
    end
  end

  describe "#validates_ticket_type_id" do
    let(:ticket) { Ticket.new account: account, description: "Wombats" }

    before do
      account.stubs(:has_extended_ticket_types?).returns(true)
      ticket.will_be_saved_by(admin)
    end

    it "fails if ticket_type_id is not part of TicketType" do
      ticket.ticket_type_id = 5
      refute ticket.valid?
      assert_equal "Type: is invalid", ticket.errors.full_messages.first
    end

    it "is valid if ticket_type_id is part of TicketType" do
      ticket.ticket_type_id = TicketType.Problem
      assert ticket.valid?
    end
  end

  describe "Changing the assignee" do
    let(:assignee) { admin }

    before do
      agent.abilities.stubs(:can?).with(:edit, ticket).returns(true)

      ticket.assignee = assignee
      assert ticket.assignee_id_changed?
      assert ticket.assignee_id
      ticket.stubs(:assignee).with(anything).returns(assignee)
    end

    describe "when the assignee is able to edit the ticket" do
      before do
        assignee.abilities.stubs(:can?).with(:edit, ticket).returns(true)
        assignee.abilities.stubs(:can?).with(:be_assigned_to, Ticket).returns(true)
        assignee.abilities.stubs(:can?).with(:add_comment, ticket).returns(true)
        assignee.abilities.stubs(:can?).with(:publicly, Comment).returns(true)
      end

      it "is allowed" do
        ticket.will_be_saved_by(assignee)
        assert ticket.save, "Errors: #{ticket.errors.full_messages}"
      end
    end

    describe "when the assignee is not able to edit the ticket" do
      let(:expected_response) do
        {
          error: "RecordInvalid",
          description: "Record validation errors",
          details: {
            base: [
              { description: "Ticket cannot be created/updated by Admin Man", error: "AccessDenied" }
            ]
          }
        }
      end

      before do
        assignee.abilities.stubs(:can?).with(:edit, ticket).returns(false)
        assignee.abilities.stubs(:can?).with(:be_assigned_to, Ticket).returns(false)
        assignee.abilities.stubs(:can?).with(:add_comment, ticket).returns(false)
        assignee.abilities.stubs(:can?).with(:add, Comment).returns(false)
        assignee.abilities.stubs(:can?).with(:publicly, Comment).returns(true)
      end

      it "is not allowed" do
        ticket.will_be_saved_by(assignee)
        refute ticket.save
        assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(ticket)
      end
    end
  end

  describe "assigning the submitter" do
    describe "on existing tickets" do
      it "is not allowed" do
        ticket.submitter = users(:minimum_author)
        ticket.will_be_saved_by(admin)
        refute ticket.save
        assert_equal "Submitter attempted to change submitter_id from 57888 to 19662 but submitter is immutable", ticket.errors.full_messages.first
      end
    end
  end

  describe "Adding tags to a ticket" do
    let(:expected_add_or_edit_tags_error_response) do
      {
        error: "RecordInvalid",
        description: "Record validation errors",
        details: {
          base: [
            { description: "Sorry, you are not allowed to add or edit tags on this ticket.", error: "AccessDenied" }
          ]
        }
      }
    end

    before do
      agent.account.stubs(:has_permission_sets?).returns(true)
      assert agent.can?(:edit, ticket)
    end

    describe "setting tags" do
      before do
        ticket.attributes = { set_tags: "foo bar 1 2 3"}
        assert ticket.current_tags_changed?
      end

      describe "when the agent may edit ticket tags" do
        before do
          agent.permission_set.permissions.enable(:edit_ticket_tags)
          assert agent.can?(:edit_tags, ticket)
        end
        it "is valid" do
          ticket.will_be_saved_by(agent)
          assert ticket.save
          assert_empty(ticket.errors)
        end
      end

      describe "when the agent may not edit ticket tags" do
        before do
          agent.permission_set.permissions.disable(:edit_ticket_tags)
          refute agent.abilities(true).can?(:edit_tags, ticket)
        end

        it "is not valid if the agent is not the requester" do
          ticket.will_be_saved_by(agent)
          refute ticket.save
          assert_equal expected_add_or_edit_tags_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
        end

        it "is valid if the agent is the requester" do
          ticket.requester = agent
          ticket.will_be_saved_by(agent)
          assert ticket.save
        end
      end
    end

    describe "removing tags" do
      before do
        ticket.attributes = { remove_tags: "hello"}
        assert ticket.current_tags_changed?
      end

      describe "when the agent may edit ticket tags" do
        before do
          agent.permission_set.permissions.enable(:edit_ticket_tags)
          assert agent.abilities(true).can?(:edit_tags, ticket)
        end

        it "is valid" do
          ticket.will_be_saved_by(agent)
          assert ticket.save
          assert_empty(ticket.errors)
        end
      end

      describe "when the agent may not edit ticket tags" do
        before do
          agent.permission_set.permissions.disable(:edit_ticket_tags)
          refute agent.abilities(true).can?(:edit_tags, ticket)
        end

        it "is not valid" do
          ticket.will_be_saved_by(agent)
          refute ticket.save
          assert_equal expected_add_or_edit_tags_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
        end
      end
    end

    describe "adding tags" do
      before do
        ticket.attributes = { additional_tags: "hi"}
        assert ticket.current_tags_changed?
      end

      describe "when the agent may edit ticket tags" do
        before do
          agent.permission_set.permissions.enable(:edit_ticket_tags)
          assert agent.abilities(true).can?(:edit_tags, ticket)
        end

        it "is valid" do
          ticket.will_be_saved_by(agent)
          assert ticket.save
          assert_empty(ticket.errors)
        end
      end

      describe "when the agent may not edit ticket tags" do
        before do
          agent.permission_set.permissions.disable(:edit_ticket_tags)
          refute agent.abilities(true).can?(:edit_tags, ticket)
        end

        it "is not valid" do
          ticket.will_be_saved_by(agent)
          refute ticket.save
          assert_equal expected_add_or_edit_tags_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
        end
      end
    end

    describe "submitting the tags unchanged" do
      before do
        ticket.will_be_saved_by(agent)
        ticket.save!
        refute ticket.current_tags_changed?
        ticket.attributes = { set_tags: ticket.current_tags }
      end

      describe "when the agent may not edit ticket tags" do
        it "is valid" do
          ticket.will_be_saved_by(agent)
          assert ticket.save
          assert_empty(ticket.errors)
        end

        describe "with agent tagging causing tag changes" do
          before do
            ticket.requester_id = end_user.id
            User.any_instance.expects(:ticket_tags).twice.returns([], ["1234567890", "0987654321"])
          end

          it "is valid" do
            ticket.will_be_saved_by(agent)
            assert ticket.save, ticket.errors.to_xml
            assert_empty(ticket.errors)
            assert_equal "0987654321 1234567890 hello", ticket.current_tags
          end
        end
      end
    end
  end

  describe 'Comment Permissions:' do
    before do
      agent.account.stubs(:has_permission_sets?).returns(true)
    end

    describe 'Given a agent who can add comments to a ticket' do
      before do
        agent.permission_set.permissions.comment_access = 'public'
        assert agent.can?(:add, Comment)
      end

      describe 'Trying to add a comment to a ticket' do
        before { ticket.add_comment(body: 'hi') }

        it 'yields a valid ticket' do
          ticket.will_be_saved_by(agent)
          assert ticket.valid?, ticket.errors.to_xml
        end
      end
    end

    describe 'Given a agent who can add comments to a ticket that is shared and whose agreement requires private comments' do
      before do
        agent.permission_set.permissions.comment_access = 'public'
        assert agent.can?(:add, Comment)
      end

      describe 'Trying to add a comment to a ticket' do
        before do
          ticket.stubs(:allows_comment_visibility_toggle?).returns(false)
          ticket.add_comment(body: 'hi', is_public: true)
        end

        it 'yields a valid ticket' do
          ticket.will_be_saved_by(agent)
          ticket.save!
          refute ticket.comments.last.is_public?
        end
      end

      describe 'Comments from the sharing account should remain public' do
        before do
          ticket.stubs(:allows_comment_visibility_toggle?).returns(false)
          agent.stubs(:foreign?).returns(true)
          ticket.add_comment(body: 'hi', is_public: true)
        end

        it 'yields a valid ticket' do
          ticket.will_be_saved_by(agent)
          ticket.save!
          assert ticket.comments.last.is_public?
        end
      end
    end

    describe 'Given a agent who can not add public comments to a ticket' do
      before do
        agent.permission_set.permissions.comment_access = 'private'
        refute agent.can?(:publicly, Comment)
      end

      describe 'Trying to add a public comment to a ticket' do
        before { ticket.add_comment(body: 'hi', is_public: true) }

        it 'yields an invalid ticket' do
          ticket.will_be_saved_by(agent)
          refute ticket.valid?
          assert_equal expected_add_public_comment_error_response, Api::V2::ErrorsPresenter.new.present(ticket)
        end
      end

      describe 'Trying to add a blank comment (an empty html form)' do
        before { ticket.add_comment(body: '', is_public: true) }

        it 'yields a valid ticket' do
          ticket.will_be_saved_by(agent)
          assert ticket.valid?
        end
      end
    end

    describe 'Given a agent who can add public comments to a ticket' do
      before do
        agent.permission_set.permissions.comment_access = 'public'
        assert agent.can?(:add, Comment)
        assert agent.can?(:publicly, Comment)
      end

      describe 'Trying to add a public comment to a ticket' do
        before { ticket.add_comment(body: 'hi', is_public: true) }

        it 'yields a valid ticket' do
          ticket.will_be_saved_by(agent)
          assert ticket.valid?
        end
      end
    end

    describe 'Given a agent who can not edit but can comment on a tickets' do
      before do
        # Priming the ticket with a save.
        # On the first save ticket.changed? returns true because it sets
        # Up some initial values.
        ticket.will_be_saved_by(ticket.requester)
        ticket.save

        agent.permission_set.permissions.disable(:ticket_editing)
        agent.permission_set.permissions.comment_access = 'public'

        refute agent.can?(:edit, ticket)
        assert agent.can?(:publicly, Comment)
      end

      describe 'Trying to add a public comment to a ticket' do
        before { ticket.add_comment(body: 'hi', is_public: true) }

        it 'yields a valid ticket' do
          ticket.will_be_saved_by(agent)
          assert ticket.valid?, ticket.errors.to_xml
        end
      end
    end

    describe 'Given a group restricted agent' do
      before do
        agent.permission_set.permissions.ticket_access = 'within-groups'
        assert agent.agent_restriction?(:groups)
        assert agent.is_restricted_agent?
      end

      describe "Who is following" do
        before do
          ticket.set_collaborators = [agent.id]
          assert ticket.is_follower?(agent)
        end

        describe 'Trying to add a comment to a ticket' do
          before do
            ticket.add_comment(body: 'hi', is_public: true)
          end

          it 'yields a valid ticket' do
            ticket.will_be_saved_by(agent)
            assert ticket.valid?, ticket.errors.to_xml
          end
        end
      end
    end

    describe "Flagged comments" do
      before do
        ticket.add_comment(body: 'hi')
        ticket.will_be_saved_by(agent)
      end

      describe "when the author cannot edit the ticket" do
        before do
          agent.stubs(:can?).with(:edit, ticket).returns(false)
          agent.stubs(:can?).with(:add_comment, ticket).returns(false)
          agent.stubs(:can?).with(:add, Comment).returns(false)
        end

        it "is invalid" do
          refute ticket.valid?
        end

        it "is valid when ticket's comment has a flag that allows comment" do
          ticket.stubs(flags_allow_comment?: true)
          assert ticket.valid?
        end

        it "is not valid when ticket's comment has a flag that does not allow comment" do
          ticket.stubs(flags_allow_comment?: false)
          refute ticket.valid?
        end
      end
    end
  end

  describe 'Sharing management' do
    let(:agreement) { FactoryBot.create(:agreement, account: account) }

    before do
      agent.account.stubs(:has_permission_sets?).returns(true)
    end

    describe 'Given a agent who can edit a ticket' do
      before { assert agent.can?(:edit, ticket) }

      describe 'Trying to share that ticket' do
        before do
          ticket.will_be_saved_by(agent, via_id: ViaType.WEB_FORM)
          ticket.update_attributes(agreement_id: agreement.id)
        end

        it 'yields a valid ticket' do
          ticket.will_be_saved_by(agent)
          assert ticket.valid?, ticket.errors.to_xml
        end
      end
    end

    describe 'Given a agent who can not edit a ticket' do
      before do
        agent.permission_set.permissions.disable(:ticket_editing)
        refute agent.can?(:edit, ticket)
      end

      describe 'Trying to share that ticket' do
        before do
          ticket.will_be_saved_by(agent, via_id: ViaType.WEB_FORM)
          ticket.update_attributes(agreement_id: agreement.id)
        end

        it 'yields an invalid ticket' do
          ticket.will_be_saved_by(agent)
          refute ticket.valid?
        end
      end
    end
  end

  describe 'Given an agent who can create end users' do
    before { assert agent.can?(:create_new, User) }

    describe 'Trying to set the requester on a new ticket' do
      let(:ticket) do
        agent.account.tickets.build(description: "A new ticket").tap do |ticket|
          ticket.requester_data = "New User <new_user@example.com>"
          ticket.will_be_saved_by(agent)
          ticket.save
        end
      end

      it "sets the requester's name and email" do
        assert_equal('New User', ticket.requester.name)
        assert_equal('new_user@example.com', ticket.requester.email)
        assert ticket.valid?, ticket.errors.to_xml
      end
    end
  end

  describe 'Given an agent who can not create end users' do
    before do
      agent.account.stubs(:has_permission_sets?).returns(true)
      agent.permission_set.permissions.end_user_profile = 'readonly'
    end

    before { refute agent.can?(:create_new, User) }

    describe 'Trying to set the requester on a new ticket' do
      let(:ticket) do
        agent.account.tickets.build(description: "A new ticket").tap do |ticket|
          ticket.requester_data = "New User <new_user@example.com>"
          ticket.will_be_saved_by(agent)
          ticket.save
        end
      end

      it "cannot set the requester's name and email" do
        assert_match(/new user/, ticket.errors[:base].join)
      end
    end
  end

  describe "when the only change to a ticket is that the actor, an agent who can only comment privately, has been added as a collaborator" do
    let(:ticket) { tickets(:minimum_1) }
    let(:light_agent) { create_light_agent }
    let(:private_comment_custom_role_agent) { create_custom_role_agent(ticket_editing: true, comment_access: 'private') }

    before do
      # Add a couple of new collaborators to the stock :minimum_1 ticket,
      # which does not come with any current collaborators out of the box.
      # This should help prevent regressions related to flattened vs un-flattened
      # arrays of collaborators in the internals of some TicketValidator
      # private methods.
      ticket.set_collaborators = "some.user@example.com, other.user@example.com"
      ticket.will_be_saved_by(ticket.requester)
      ticket.save!
    end

    describe_with_arturo_enabled :email_restricted_agent_fix do
      describe "when the legacy CCs feature is enabled" do
        before do
          Account.any_instance.stubs(:is_collaboration_enabled?).returns(true)
          CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
        end

        describe "when the actor is a light agent" do
          before do
            ticket.additional_collaborators = [light_agent.id.to_s]
            ticket.will_be_saved_by(light_agent)
          end

          it "successfully validates the ticket" do
            assert_valid ticket
          end
        end

        describe "when the actor is a custom role agent who can only comment privately" do
          before do
            ticket.additional_collaborators = [private_comment_custom_role_agent.id.to_s]
            ticket.will_be_saved_by(private_comment_custom_role_agent)
          end

          it "successfully validates the ticket" do
            assert_valid ticket
          end
        end
      end

      describe "when the modern email CCs & followers feature is enabled" do
        before do
          Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
          CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
        end

        describe "when the actor is added as an email CC" do
          describe "when the actor is a light agent" do
            before do
              ticket.additional_collaborators_with_email_cc_type = [light_agent.id.to_s]
              ticket.will_be_saved_by(light_agent)
            end

            it "is unable to validate the ticket" do
              refute ticket.save
            end
          end

          describe "when the actor is a custom role agent who can only comment privately" do
            before do
              ticket.additional_collaborators_with_email_cc_type = [private_comment_custom_role_agent.id.to_s]
              ticket.will_be_saved_by(private_comment_custom_role_agent)
            end

            it "is unable to validate the ticket" do
              refute ticket.save
            end
          end
        end

        describe "when the actor is added as a follower" do
          describe "when the actor is a light agent" do
            before do
              ticket.additional_collaborators_with_follower_type = [light_agent.id.to_s]
              ticket.will_be_saved_by(light_agent)
            end

            it "successfully validates the ticket" do
              assert_valid ticket
            end
          end

          describe "when the actor is a custom role agent who can only comment privately" do
            before do
              ticket.additional_collaborators_with_follower_type = [private_comment_custom_role_agent.id.to_s]
              ticket.will_be_saved_by(private_comment_custom_role_agent)
            end

            it "successfully validates the ticket" do
              assert_valid ticket
            end
          end
        end
      end
    end

    # Note: With the arturo `email_restricted_agent_fix` disabled,
    # TicketValidator#can_edit_and_publicly_comment? returns `true` for light
    # agents and private comment-only custom role agents. This allows these
    # agents to update `ticket.current_collaborators`, which is a bug.
    describe_with_arturo_disabled :email_restricted_agent_fix do
      describe "when the legacy CCs feature is enabled" do
        before do
          Account.any_instance.stubs(:is_collaboration_enabled?).returns(true)
          CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
        end

        describe "when the actor is a light agent" do
          before do
            ticket.additional_collaborators = [light_agent.id.to_s]
            ticket.will_be_saved_by(light_agent)
          end

          it "successfully validates the ticket" do
            assert_valid ticket
          end
        end

        describe "when the actor is a custom role agent who can only comment privately" do
          before do
            ticket.additional_collaborators = [private_comment_custom_role_agent.id.to_s]
            ticket.will_be_saved_by(private_comment_custom_role_agent)
          end

          it "successfully validates the ticket" do
            assert_valid ticket
          end
        end
      end

      describe "when the modern email CCs & followers feature is enabled" do
        before do
          Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
          CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
        end

        describe "when the actor is added as an email CC" do
          describe "when the actor is a light agent" do
            before do
              ticket.additional_collaborators_with_email_cc_type = [light_agent.id.to_s]
              ticket.will_be_saved_by(light_agent)
            end

            it "successfully validates the ticket" do
              assert_valid ticket
            end
          end

          describe "when the actor is a custom role agent who can only comment privately" do
            before do
              ticket.additional_collaborators_with_email_cc_type = [private_comment_custom_role_agent.id.to_s]
              ticket.will_be_saved_by(private_comment_custom_role_agent)
            end

            it "successfully validates the ticket" do
              assert_valid ticket
            end
          end
        end

        describe "when the actor is added as a follower" do
          describe "when the actor is a light agent" do
            before do
              ticket.additional_collaborators_with_follower_type = [light_agent.id.to_s]
              ticket.will_be_saved_by(light_agent)
            end

            it "successfully validates the ticket" do
              assert_valid ticket
            end
          end

          describe "when the actor is a custom role agent who can only comment privately" do
            before do
              ticket.additional_collaborators_with_follower_type = [private_comment_custom_role_agent.id.to_s]
              ticket.will_be_saved_by(private_comment_custom_role_agent)
            end

            it "successfully validates the ticket" do
              assert_valid ticket
            end
          end
        end
      end
    end
  end
end
