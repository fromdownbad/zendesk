require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Report' do
  fixtures :accounts, :account_property_sets, :users, :groups, :memberships,
    :user_identities, :subscriptions, :payments, :sequences, :tickets, :events, :ticket_fields, :organizations

  describe "Report Abilities" do
    before do
      @permission_set = build_valid_permission_set
      @end_user = users(:with_groups_end_user)
      @restricted_agent = users(:with_groups_agent_groups_restricted)
      @agent = users(:with_groups_agent1)
      @admin = users(:with_groups_admin)
      User.any_instance.stubs(:is_light_agent?).returns(false)
    end

    describe "Viewing" do
      it("be denied for end users") { refute @end_user.can?(:view, Report) }
      it("be denied for restricted agents") { refute @restricted_agent.can?(:view, Report) }
      it("be allowed for non restricted agents") { assert @agent.can?(:view, Report) }
      it("be allowed for admins") { assert @admin.can?(:view, Report) }

      describe "for agents with permission sets" do
        before do
          @agent.account.stubs(:has_permission_sets?).returns(true)
          @agent.stubs(permission_set: @permission_set)
        end

        it("be allowed for agents with full access") do
          assert @agent.can?(:view, Report)
        end

        it("be allowed for agents with readonly access") do
          @permission_set.permissions.report_access = 'readonly'
          assert @agent.can?(:view, Report)
        end

        it("be denied for agents with no report access") do
          @permission_set.permissions.report_access = 'none'
          refute @agent.can?(:view, Report)
        end
      end
    end

    describe "Managing" do
      it("be denied for end users") { refute @end_user.can?(:manage, ::Report) }
      it("be denied for restricted agents") { refute @restricted_agent.can?(:manage, ::Report) }
      it("be denied for non restricted agents") { refute @agent.can?(:manage, ::Report) }
      it("be allowed for admins") { assert @admin.can?(:manage, ::Report) }

      describe "for agents with permission sets" do
        before do
          @agent.account.stubs(:has_permission_sets?).returns(true)
          @agent.stubs(permission_set: @permission_set)
        end

        it("be allowed for agents with full access") do
          assert @agent.can?(:manage, ::Report)
        end

        it("be denied for agents with readonly access") do
          @permission_set.permissions.report_access = 'readonly'
          refute @agent.can?(:manage, ::Report)
        end

        it("be denied for agents with no report access") do
          @permission_set.permissions.report_access = 'none'
          refute @agent.can?(:manage, ::Report)
        end
      end
    end
  end
end
