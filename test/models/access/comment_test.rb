require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Comment' do
  fixtures :users, :accounts, :subscriptions

  subject { @user }

  describe 'Old school permissions' do
    before do
      @user = users(:minimum_agent)
    end

    describe 'Given an agent allowed to add public comments' do
      before do
        @user.is_private_comments_only = false
      end

      should_be_able_to(:add, 'comments') { Comment }
      should_be_able_to(:publicly, 'comment') { Comment }
    end

    describe 'Given an agent only allowed to add private comments' do
      before do
        @user.is_private_comments_only = true
      end

      should_be_able_to(:add, 'comments') { Comment }
      should_not_be_able_to(:publicly, 'comment') { Comment }
    end
  end

  describe 'New hotness permissions' do
    before do
      @user = users(:minimum_agent)
      @user.permission_set = @user.account.permission_sets.create!(name: "Test")

      assert @user.account.stubs(:has_permission_sets?).returns(true)
    end

    describe 'Given an agent with no ability to comment' do
      before { @user.permission_set.permissions.comment_access = 'none' }

      should_not_be_able_to(:add, 'comments') { Comment }
      should_not_be_able_to(:publicly, 'comment') { Comment }
    end

    describe 'Given an agent with the ability to add private comments' do
      before { @user.permission_set.permissions.comment_access = 'private' }

      should_be_able_to(:add, 'comments') { Comment }
      should_not_be_able_to(:publicly, 'comment') { Comment }
    end

    describe 'Given an agent with the ability to add public comments' do
      before { @user.permission_set.permissions.comment_access = 'public' }

      should_be_able_to(:add, 'comments') { Comment }
      should_be_able_to(:publicly, 'comment') { Comment }
    end

    describe 'Given an agent' do
      it 'is able to mark a comment as private' do
        assert @user.can?(:make_comment_private, Comment)
      end

      it 'is able to edit their own public comment' do
        assert @user.can?(:edit, Comment.new(author: @user, is_public: true))
      end
    end

    describe 'Given a light agent' do
      before do
        @user = users(:minimum_agent)
        @user.account.stubs(:has_light_agents?).returns(true)
        @user.permission_set = @user.account.permission_sets.create!(name: I18n.t("txt.default.roles.light_agent.name"), role_type: PermissionSet::Type::LIGHT_AGENT)
        @user.permission_set.permissions.set(PermissionSet::LightAgent.configuration)
      end

      it 'not is able to mark a comment as private' do
        refute @user.can?(:make_comment_private, Comment)
      end
    end
  end

  describe 'Given an end user' do
    before do
      @user = users(:minimum_end_user)
    end

    it 'not is able to mark a comment as private' do
      refute @user.can?(:make_comment_private, Comment)
    end

    should_be_able_to(:add, 'comments') { Comment }
    should_be_able_to(:publicly, 'comment') { Comment }
  end

  describe 'Given an admin' do
    before do
      @user = users(:minimum_admin)
    end

    it 'is able to mark a comment as private' do
      assert @user.can?(:make_comment_private, Comment)
    end

    it 'is able to edit any public comment' do
      other_user_comment = Comment.new(author: users(:minimum_agent), is_public: true)
      assert @user.can?(:edit, other_user_comment)
    end

    should_be_able_to(:add, 'comments') { Comment }
    should_be_able_to(:publicly, 'comment') { Comment }
  end
end
