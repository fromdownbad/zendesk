require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Forum' do
  fixtures :accounts, :account_property_sets, :users, :groups, :memberships,
    :user_identities, :subscriptions, :payments, :sequences, :tickets, :events, :ticket_fields,
    :organizations, :forums, :entries, :posts

  def self.test_organization_access
    describe "forum access" do
      it "is able to access forums in user orgs" do
        @user.stubs(organization_memberships: stub(exists?: true))
        assert @user.can?(:view, @forum)
      end

      it "is able to access forums without org" do
        @forum.stubs(:organization_id).returns(nil)
        assert @user.can?(:view, @forum)
      end

      it "is not be able to access forums with other org" do
        @user.stubs(organization_memberships: stub(exists?: false))
        @forum.stubs(:organization_id).returns(111)
        refute @user.can?(:view, @forum)
      end
    end
  end

  describe "Forum abilities:" do
    before do
      @forum = forums(:solutions)
      @entry = entries(:ponies)
      @post = posts(:ponies_post_1)
      @user = users(:minimum_end_user)
      @admin = users(:minimum_admin)
      @agent = users(:minimum_agent)
      @permission_set = build_valid_permission_set
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
      @authorizor = Access::Authorization.any_instance
    end

    describe "forum moderation" do
      before do
        @moderator = users(:minimum_search_user)
        @moderator.is_moderator = true
      end

      describe "with permission sets" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
        end

        it "is true for admin" do
          assert @admin.can?(:moderate, Forum)
        end

        it "is true for agent" do
          assert @agent.can?(:moderate, Forum)
        end

        it "is true for moderator" do
          assert @moderator.can?(:moderate, Forum)
        end

        it "is false for end user" do
          refute @user.can?(:moderate, Forum)
        end
      end

      describe "without permission sets" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(false)
        end

        it "is true for admin" do
          assert @admin.can?(:moderate, Forum)
        end

        it "is true for agent" do
          assert @agent.can?(:moderate, Forum)
        end

        it "is true for moderator" do
          assert @moderator.can?(:moderate, Forum)
        end

        it "is false for end user" do
          refute @user.can?(:moderate, Forum)
        end
      end
    end

    [Forum, Entry, Post, Category].each do |klass|
      describe "Managing #{klass.name.pluralize}" do
        before do
          Subscription.any_instance.stubs(:has_categorized_forums?).returns(true)
        end

        describe "for anonymous users" do
          before { @user.stubs(:is_anonymous_user?).returns(true) }
          it("is denied") { refute @user.can?(:manage, klass) }
        end

        describe "for end users" do
          it("is denied") { refute @user.can?(:manage, klass) }
        end

        describe "for agents" do
          it("is denied") { refute @agent.can?(:manage, klass) }
        end

        describe "for admins" do
          it("is allowed") { assert @admin.can?(:manage, klass) }
        end

        describe "for agents with permission sets" do
          before { @agent.stubs(permission_set: @permission_set) }
          it("is allowed for full access") do
            assert @agent.can?(:manage, klass)
          end

          %w[edit-topics readonly].each do |forum_access|
            it("is denied for #{forum_access} access") do
              @permission_set.permissions.forum_access = forum_access
              refute @agent.can?(:manage, klass)
            end
          end
        end
      end
    end

    describe "Managing Categories in an account with no categories" do
      before { Subscription.any_instance.stubs(:has_categorized_forums?).returns(false) }

      it "is not is allowed for anyone" do
        refute @admin.can? :manage, Category
        refute @agent.can? :manage, Category
        refute @user.can? :manage, Category
      end

      describe "for agents with permission sets" do
        before { @agent.stubs(permission_set: @permission_set) }

        %w[full edit-topics readonly].each do |forum_access|
          it("not is allowed for #{forum_access} access") do
            @permission_set.permissions.forum_access = forum_access
            refute @agent.can?(:manage, Category)
          end
        end
      end
    end

    describe "Watching forums" do
      describe "for anonymous users" do
        before { @user.stubs(:is_anonymous_user?).returns(true) }
        it("is denied") { refute @user.can?(:watch, @forum) }
      end

      describe "for users without email addresses" do
        before { @user.expects(:has_email?).returns(false) }
        it("is denied") { refute @user.can?(:watch, @forum) }
      end

      describe "for Twitter" do
        before { @forum.account.expects(:subdomain).returns('twitter') }
        it("is denied") { refute @user.can?(:watch, @forum) }
      end

      describe "otherwise" do
        it "defers to can?(:view, forum) (true)" do
          @user.abilities.expects(:defer?).with(:view, @forum).returns(true)
          assert @user.can?(:watch, @forum)
        end
        it "defers to can?(:view, forum) (false)" do
          @user.abilities.expects(:defer?).with(:view, @forum).returns(false)
          refute @user.can?(:watch, @forum)
        end
      end

      describe "for agents with permission sets" do
        before { @agent.stubs(permission_set: @permission_set) }

        %w[edit-topics readonly].each do |forum_access|
          it("is allowed for #{forum_access} access") do
            @permission_set.permissions.forum_access = forum_access
            assert @agent.can?(:watch, @forum)
          end
        end
      end
    end

    describe "Editing forums" do
      describe "for admins" do
        it("is allowed") { assert @admin.can?(:edit, @forum) }
      end
      describe "for non-admins" do
        it("is denied") { refute @agent.can?(:edit, @forum) }
      end

      describe "for agents with permission sets" do
        before { @agent.stubs(permission_set: @permission_set) }

        %w[edit-topics readonly].each do |forum_access|
          it("is denied for #{forum_access} access") do
            @permission_set.permissions.forum_access = forum_access
            refute @agent.can?(:edit, @forum)
          end
        end
      end
    end

    describe "Adding topics to forums" do
      describe "for admins" do
        it("is allowed") { assert @admin.can?(:add_topic, @forum) }
      end
      describe "for non restricted agents" do
        it("is allowed") { assert @agent.can?(:add_topic, @forum) }
      end
      describe "for restricted agents" do
        before do
          @agent.stubs(:is_non_restricted_agent?).returns(false)
          @agent.stubs(:is_moderator?).returns(false)
        end

        describe "and the forum is locked" do
          before { @forum.expects(:is_locked?).returns(true) }
          it("is denied") { refute @agent.can?(:add_topic, @forum) }
        end

        describe "and the forum is not locked" do
          before { @forum.expects(:is_locked?).returns(false) }
          describe "and the agent can not see that forum" do
            before { @authorizor.expects(:defer?).with(:view, @forum).returns(false) }
            it("is denied") { refute @agent.can?(:add_topic, @forum) }
          end

          describe "and the agent can see that forum" do
            before { @authorizor.expects(:defer?).with(:view, @forum).returns(true) }
            it("is allowed") { assert @agent.can?(:add_topic, @forum) }
          end
        end
      end

      describe "for agents with permission sets that cannot view the forum" do
        before do
          @agent.stubs(permission_set: @permission_set)
          @authorizor.expects(:defer?).with(:view, @forum).returns(false)
        end

        %w[edit-topics readonly].each do |forum_access|
          describe "and #{forum_access} access" do
            before { @permission_set.permissions.forum_access = forum_access }
            it("is denied") { refute @agent.can?(:add_topic, @forum) }
          end
        end
      end

      describe "for agents with permission sets that can view the forum" do
        before do
          @agent.stubs(permission_set: @permission_set)
          @agent.stubs(:has_permission_set?).returns(true)
          @authorizor.expects(:defer?).with(:view, @forum).returns(true)
        end

        %w[full edit-topics].each do |forum_access|
          describe "and #{forum_access} access" do
            before { @permission_set.permissions.forum_access = forum_access }
            it("is allowed") { assert @agent.can?(:add_topic, @forum) }
          end
        end

        describe "and readonly access" do
          before { @permission_set.permissions.forum_access = 'readonly' }

          describe "and the forum is locked" do
            before { @forum.expects(:is_locked?).returns(true) }
            it("is denied") { refute @agent.can?(:add_topic, @forum) }
          end

          describe "and the forum is not locked" do
            before { @forum.expects(:is_locked?).returns(false) }
            it("is allowed") { assert @agent.can?(:add_topic, @forum) }
          end
        end
      end
    end

    describe "Viewing forums" do
      describe "for unrestricted Agents" do
        before { @agent.stubs(:agent_restriction?).returns(false) }

        it "is allowed" do
          assert @user.can?(:view, @forum)
        end
      end

      describe "for organization-restricted agents" do
        before do
          @user = @agent
          @user.stubs(:agent_restriction?).returns(true)
        end
        test_organization_access
      end

      describe "for end users/non-agents" do
        describe "for agent-restricted/internal forums" do
          before do
            @restriction = stub
            @restriction.expects(:agents_only?).returns(true)
            @forum.expects(:visibility_restriction).returns(@restriction)
          end
          it "is denied" do
            refute @user.can?(:view, @forum)
          end
        end

        describe "for public forums" do
          before do
            @restriction = stub
            @restriction.expects(:agents_only?).returns(false)
            @forum.expects(:visibility_restriction).returns(@restriction)
          end
          test_organization_access
        end
      end

      describe "for anonymous users" do
        before { @user.stubs(:is_anonymous_user?).returns(true) }

        describe "for public/unrestricted forums" do
          before do
            @restriction = stub
            @restriction.expects(:everybody?).returns(true)
            @forum.expects(:visibility_restriction).returns(@restriction)
          end

          it "is allowed" do
            assert @user.can?(:view, @forum)
          end
        end

        describe "for restricted-access forums" do
          before do
            @restriction = stub
            @restriction.expects(:everybody?).returns(false)
            @forum.expects(:visibility_restriction).returns(@restriction)
          end
          it "is denied" do
            refute @user.can?(:view, @forum)
          end
        end
      end

      describe "with user tags" do
        before do
          Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
        end

        describe "for public forums" do
          before do
            @restriction = stub
            @restriction.expects(:everybody?).returns(true)
            @forum.expects(:visibility_restriction).returns(@restriction)
          end

          describe "for anonymous users" do
            before do
              @user.stubs(:is_agent?).returns(false)
              @user.expects(:is_anonymous_user?).returns(true)
            end

            it "is denied" do
              refute @user.can?(:view, @forum)
            end
          end
        end

        describe "for non-agents" do
          before do
            @forum.stubs(:tag_array).returns(["bar", "baz", "foo"])
            @user.stubs(:is_agent?).returns(false)
            @user.stubs(:is_anonymous_user?).returns(false)
          end

          describe "when the end user's tags exactly match all of the forum's tags" do
            before { @user.expects(:all_tags).returns(["baz", "bar", "foo"]) }

            it "is allowed" do
              assert @user.can?(:view, @forum)
            end
          end

          describe "when the end user's tags match are a subset the forum's tags" do
            before { @user.expects(:all_tags).returns(["baz", "bar"]) }

            it "is denied" do
              refute @user.can?(:view, @forum)
            end
          end

          describe "when the end user's tags match are a superset the forum's tags" do
            before { @user.expects(:all_tags).returns(["baz", "bar", "foo", "wibble", "wabble"]) }

            it "is allowed" do
              assert @user.can?(:view, @forum)
            end
          end
        end

        describe "for agents" do
          before do
            @user.stubs(:is_agent?).returns(true)
            @user.stubs(:is_anonymous_user?).returns(false)
          end
          it "is irrelevant" do
            @user.expects(:all_tags).never
            @user.can?(:view, @forum)
          end
        end
      end

      describe "for agents with permission sets" do
        before do
          @user = @agent
          @user.stubs(permission_set: @permission_set)
        end

        %w[edit-topics readonly].each do |forum_access|
          describe "and #{forum_access} access" do
            before do
              @permission_set.permissions.disable(:forum_access_restricted_content)
              @permission_set.permissions.forum_access = forum_access
            end
            test_organization_access
          end
        end
      end
    end

    describe "Reading Categories" do
      describe "in an account with categories" do
        before { Subscription.any_instance.stubs(:has_categorized_forums?).returns(true) }

        it "is allowed for an Admin" do
          assert @admin.can? :read, Category
        end

        it "is allowed for an Agent" do
          assert @agent.can? :read, Category
        end

        it "is allowed for an End User" do
          assert @user.can? :read, Category
        end

        describe "for agents with permission sets" do
          before { @agent.stubs(permission_set: @permission_set) }

          %w[full edit-topics readonly].each do |forum_access|
            it("is allowed for #{forum_access} access") do
              @permission_set.permissions.forum_access = forum_access
              assert @agent.can?(:read, Category)
            end
          end
        end
      end

      describe "in an account with no categories" do
        before { Subscription.any_instance.stubs(:has_categorized_forums?).returns(false) }

        it "not is allowed for an Admin" do
          refute @admin.can? :read, Category
        end

        it "not is allowed for an Agent" do
          refute @agent.can? :read, Category
        end

        it "not is allowed for an End User" do
          refute @user.can? :read, Category
        end

        describe "for agents with permission sets" do
          before { @agent.stubs(permission_set: @permission_set) }

          %w[full edit-topics readonly].each do |forum_access|
            it("not is allowed for #{forum_access} access") do
              @permission_set.permissions.forum_access = forum_access
              refute @agent.can?(:read, Category)
            end
          end
        end
      end
    end

    describe "Editing entries" do
      describe "for anonymous users" do
        before { @user.expects(:is_anonymous_user?).returns(true) }
        it("is denied") { refute @user.can?(:edit, @entry) }
      end

      describe "for users that cannot view the forum" do
        before { @user.abilities.expects(:defer?).with(:view, @entry.forum).returns(false) }
        it("is denied") { refute @user.can?(:edit, @entry) }
      end

      describe "for users that may view the forum" do
        before { @user.abilities.expects(:defer?).with(:view, @entry.forum).returns(true) }
        describe "for moderators" do
          before { @user.expects(:is_moderator?).returns(true) }
          it("is allowed") { assert @user.can?(:edit, @entry) }
        end

        describe "for entries with nil submitter_ids" do
          before { @entry.expects(:submitter_id).returns(nil) }
          it("is allowed") { assert @user.can?(:edit, @entry) }
        end

        describe "when the entry's submitter is not the current user" do
          before { @entry.expects(:submitter_id).twice.returns(-1) }
          it("is denied") { refute @user.can?(:edit, @entry) }
        end

        describe "when the forum is locked" do
          before do
            @entry.submitter_id = @user.id
            @entry.forum.expects(:is_locked?).returns(true)
          end

          describe "when the agent is unrestricted" do
            before { @user.expects(:is_non_restricted_agent?).returns(true) }
            it("is allowed") { assert @user.can?(:edit, @entry) }
          end

          describe "should is denied for all other agents" do
            before do
              @user.expects(:is_non_restricted_agent?).returns(false)
              @user.expects(:is_moderator?).returns(false)
            end
            it("is denied") { refute @user.can?(:edit, @entry) }
          end
        end
      end

      describe "for agents with permission sets that cannot view the forum" do
        before do
          @agent.stubs(permission_set: @permission_set)
          @authorizor.expects(:defer?).with(:view, @entry.forum).returns(false)
        end

        %w[edit-topics readonly].each do |forum_access|
          describe "and #{forum_access} access" do
            before { @permission_set.permissions.forum_access = forum_access }
            it("is denied") { refute @agent.can?(:edit, @entry) }
          end
        end
      end

      describe "for agents with permission sets that can view the forum" do
        before do
          @agent.stubs(permission_set: @permission_set)
          @authorizor.expects(:defer?).with(:view, @entry.forum).returns(true)
        end

        %w[full edit-topics].each do |forum_access|
          describe "and #{forum_access} access" do
            before { @permission_set.permissions.forum_access = forum_access }
            it("is allowed") do
              assert @agent.can?(:edit, @entry)
            end

            describe "and the forum is locked" do
              before { @entry.forum.stubs(:is_locked?).returns(true) }
              it("is allowed") { assert @agent.can?(:edit, @entry) }
            end
          end
        end

        describe "and readonly access" do
          before { @permission_set.permissions.forum_access = 'readonly' }

          describe "and the submitter_id is nil" do
            before { @entry.expects(:submitter_id).returns(nil) }
            it("is allowed") { assert @agent.can?(:edit, @entry) }
          end

          describe "and the entry's submitter is the current user" do
            before { @entry.stubs(:submitter_id).returns(@agent.id) }
            it("is allowed") { assert @agent.can?(:edit, @entry) }
          end

          describe "and the entry's submitter is not the current user" do
            before { @entry.stubs(:submitter_id).returns(-1) }
            it("is denied") { refute @agent.can?(:edit, @entry) }
          end

          describe "when the forum is locked" do
            before do
              @entry.stubs(:submitter_id).returns(@agent.id)
              @entry.forum.expects(:is_locked?).returns(true)
            end
            it("is denied") { refute @agent.can?(:edit, @entry) }
          end
        end
      end
    end

    describe "Watching entries" do
      it "defers to forum authorizor" do
        @user.abilities.expects(:defer?).with(:watch, @entry.forum)
        @user.can?(:watch, @entry)
      end

      describe "for agents with permission sets" do
        before { @agent.stubs(permission_set: @permission_set) }

        %w[full edit-topics readonly].each do |forum_access|
          describe "and #{forum_access} access" do
            it("defers to forum authorizor") do
              @permission_set.permissions.forum_access = forum_access
              assert @user.can?(:watch, @entry)
            end
          end
        end
      end
    end

    describe "Replying to entries" do
      describe "for anonymous users" do
        before { @user.expects(:is_anonymous_user?).returns(true) }
        it("is denied") { refute @user.can?(:reply_to, @entry) }
      end

      describe "for locked entries" do
        before do
          @user.stubs(:is_anonymous_user?).returns(false)
          @entry.stubs(:is_locked?).returns(true)
        end
        it("is denied") { refute @user.can?(:reply_to, @entry) }

        # this is the current behavior. unclear if it's intentional
        it "is allowed for admins" do
          @user.stubs(:has_permission_set?).returns(false)
          @user.stubs(:is_admin?).returns(true)
          assert @user.can?(:reply_to, @entry)
        end

        # this is the current behavior. unclear if it's intentional
        it "is allowed with full forum access permissions" do
          @user.stubs(:has_permission_set?).returns(true)
          @user.permission_set = PermissionSet.new
          @user.permission_set.permissions.forum_access.must_equal 'full'

          assert @user.can?(:reply_to, @entry)
        end
      end

      describe "other cases" do
        before do
          @user.expects(:is_anonymous_user?).returns(false)
          @entry.expects(:is_locked?).returns(false)
        end
        it("is allowed") { assert @user.can?(:reply_to, @entry) }
      end

      describe "for agents with permission sets" do
        before { @agent.stubs(permission_set: @permission_set) }

        describe "for locked entries" do
          before { @entry.expects(:is_locked?).returns(true) }

          it("is allowed for agents with full access") { assert @agent.can?(:reply_to, @entry) }

          %w[edit-topics readonly].each do |forum_access|
            it "is denied for agents with #{forum_access} access" do
              @permission_set.permissions.forum_access = forum_access
              refute @agent.can?(:reply_to, @entry)
            end
          end
        end

        describe "for unlocked entries" do
          before { @entry.expects(:is_locked?).returns(false) }

          %w[full edit-topics readonly].each do |forum_access|
            it "is allowed for agents with #{forum_access} access" do
              @permission_set.permissions.forum_access = forum_access
              assert @agent.can?(:reply_to, @entry)
            end
          end
        end
      end
    end

    describe "Viewing posts" do
      it "defers to can_view_forum?" do
        @user.abilities.expects(:defer?).with(:view, @post.forum)
        @user.can?(:view, @post)
      end

      describe "for agents with permission sets" do
        before { @agent.stubs(permission_set: @permission_set) }

        %w[full edit-topics readonly].each do |forum_access|
          describe "and #{forum_access} access" do
            it("defers to can_view_forum?") do
              @user.abilities.expects(:defer?).with(:view, @post.forum)
              @permission_set.permissions.forum_access = forum_access
              @user.can?(:view, @post)
            end
          end
        end
      end
    end

    describe "Editing posts" do
      describe "for anonymous users" do
        before { @user.expects(:is_anonymous_user?).returns(true) }
        it("is denied") { refute @user.can?(:edit, @post) }
      end

      describe "for users that may not view the post" do
        before { @user.abilities.expects(:defer?).with(:view, @post).returns(false) }
        it("is denied") { refute @user.can?(:edit, @post) }
      end

      describe "for moderators" do
        before { @user.stubs(:is_moderator?).returns(true) }
        it("is allowed") { assert @user.can?(:edit, @post) }
      end

      describe "for posts belonging to locked entries" do
        before do
          @post.stubs(:user_id).returns(@user.id)
          @post.entry.expects(:is_locked?).returns(true)
        end
        it("is denied") { refute @user.can?(:edit, @post) }
      end

      describe "belonging to unlocked entries" do
        before { @post.entry.stubs(:is_locked?).returns(false) }
        describe "when the post has a nil user_id" do
          before { @post.stubs(:user_id).returns(nil) }
          it("is allowed") { assert @user.can?(:edit, @post) }
        end
        describe "when the post's user_id matches this user'd id" do
          before { @post.stubs(:user_id).returns(@user.id) }
          it("is allowed") { assert @user.can?(:edit, @post) }
        end
        describe "when the post's user_id does not match this user's id" do
          before { @post.stubs(:user_id).returns(-1) }
          it("is denied") { refute @user.can?(:edit, @post) }
        end
      end

      describe "for agents with permission sets that cannot view the forum" do
        before do
          @agent.stubs(permission_set: @permission_set)
          @authorizor.expects(:defer?).with(:view, @post.entry.forum).returns(false)
        end

        %w[edit-topics readonly].each do |forum_access|
          describe "and #{forum_access} access" do
            before { @permission_set.permissions.forum_access = forum_access }
            it("is denied") { refute @agent.can?(:edit, @post) }
          end
        end
      end

      describe "for agents with permission sets that can view the forum" do
        before do
          @agent.stubs(permission_set: @permission_set)
          @authorizor.expects(:defer?).with(:view, @post.entry.forum).returns(true)
        end

        %w[full edit-topics].each do |forum_access|
          describe "and #{forum_access} access" do
            before { @permission_set.permissions.forum_access = forum_access }
            it("is allowed") { assert @agent.can?(:edit, @post) }
          end
        end

        describe "and readonly access" do
          before { @permission_set.permissions.forum_access = 'readonly' }

          describe "and locked entries" do
            before { @post.entry.stubs(:is_locked?).returns(true) }
            it("is denied") { refute @agent.can?(:edit, @post) }
          end

          describe "and unlocked entries" do
            before { @post.entry.stubs(:is_locked?).returns(false) }

            describe "and the user_id is nil" do
              before { @post.stubs(:user_id).returns(nil) }
              it("is allowed") { assert @agent.can?(:edit, @post) }
            end

            describe "when the post's user_id matches this agents id" do
              before { @post.stubs(:user_id).returns(@agent.id) }
              it("is allowed") { assert @agent.can?(:edit, @post) }
            end

            describe "when the post's user_id doesn't match this agents id" do
              before { @post.stubs(:user_id).returns(-1) }
              it("is denied") { refute @agent.can?(:edit, @post) }
            end
          end
        end
      end
    end

    describe "Accessing restricted content" do
      describe "for admins" do
        it("is allowed") { assert @admin.can?(:access_restricted_content, Forum) }
      end

      describe "for non restricted agents" do
        before { @agent.stubs(:restriction_id).returns(RoleRestrictionType.None) }
        it("is allowed") { assert @agent.can?(:access_restricted_content, Forum) }
      end

      describe "for agents restricted to tickets assigned" do
        before { @agent.stubs(:restriction_id).returns(RoleRestrictionType.Assigned) }
        it("is allowed") { assert @agent.can?(:access_restricted_content, Forum) }
      end

      describe "for agents restricted to organization" do
        before { @agent.stubs(:restriction_id).returns(RoleRestrictionType.Organization) }
        it("is denied") { refute @agent.can?(:access_restricted_content, Forum) }
      end

      describe "for agents with permission sets" do
        before { @agent.stubs(permission_set: @permission_set) }

        describe "and full access" do
          before do
            @permission_set.permissions.forum_access = 'full'
            @permission_set.permissions.enable(:forum_access_restricted_content)
          end
          it("is allowed") { assert @agent.can?(:access_restricted_content, Forum) }
        end

        %w[edit-topics readonly].each do |forum_access|
          describe "and #{forum_access} access" do
            before { @permission_set.permissions.forum_access = forum_access }

            describe "and no access to organization restricted content" do
              before { @permission_set.permissions.disable(:forum_access_restricted_content) }

              it("is denied") { refute @agent.can?(:access_restricted_content, Forum) }
            end

            describe "and access to organization restricted content" do
              before { @permission_set.permissions.enable(:forum_access_restricted_content) }

              it("is allowed") { assert @agent.can?(:access_restricted_content, Forum) }
            end
          end
        end
      end
    end

    describe "Reordering topics" do
      describe "for admins" do
        it("is allowed") { assert @admin.can?(:reorder, Entry) }
      end

      describe "for non moderator agents" do
        before { @agent.stubs(:is_moderator?).returns(false) }
        it("is denied") { refute @agent.can?(:reorder, Entry) }
      end

      describe "for moderator agents" do
        before { @agent.stubs(:is_moderator?).returns(true) }
        it("is allowed") { assert @agent.can?(:reorder, Entry) }
      end

      describe "for agents with permission sets" do
        before { @agent.stubs(permission_set: @permission_set) }

        %w[full edit-topics].each do |forum_access|
          describe "and #{forum_access} access" do
            before { @permission_set.permissions.forum_access = forum_access }
            it("is allowed") { assert @agent.can?(:reorder, Entry) }
          end
        end

        describe "and readonly access" do
          before { @permission_set.permissions.forum_access = 'readonly' }
          it("is denied") { refute @agent.can?(:reorder, Entry) }
        end
      end
    end

    describe "Moderating topics" do
      describe "for admins" do
        it("is allowed") { assert @admin.can?(:moderate, Entry) }
      end

      describe "for non moderator agents" do
        before { @agent.stubs(:is_moderator?).returns(false) }
        it("is denied") { refute @agent.can?(:moderate, Entry) }
      end

      describe "for moderator agents" do
        before { @agent.stubs(:is_moderator?).returns(true) }
        it("is allowed") { assert @agent.can?(:moderate, Entry) }
      end

      describe "for agents with permission sets" do
        before { @agent.stubs(permission_set: @permission_set) }

        %w[full edit-topics].each do |forum_access|
          describe "and #{forum_access} access" do
            before { @permission_set.permissions.forum_access = forum_access }
            it("is allowed") { assert @agent.can?(:moderate, Entry) }
          end
        end

        describe "and readonly access" do
          before { @permission_set.permissions.forum_access = 'readonly' }
          it("is denied") { refute @agent.can?(:moderate, Entry) }
        end
      end
    end
  end
end
