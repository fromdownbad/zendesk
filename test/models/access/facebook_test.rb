require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Facebook' do
  fixtures :accounts, :account_property_sets, :users

  describe "Facebook Abilities" do
    before do
      @agent = users(:minimum_agent)
      @permission_set = PermissionSet.new
      @permission_set.permissions.enable(:manage_facebook)
      User.any_instance.stubs(:is_light_agent?).returns(false)
    end

    describe "Facebook availability" do
      describe "for end users" do
        it "is is not is allowed" do
          end_user = users(:minimum_end_user)
          refute end_user.can?(:manage, ::Facebook::Page)
        end
      end

      describe "for agents without permission sets" do
        it "is is not is allowed" do
          refute @agent.can?(:manage, ::Facebook::Page)
        end
      end

      describe "for agents with permission sets" do
        before do
          @agent.stubs(:permission_set).returns(@permission_set)
          @agent.account.expects(:has_permission_sets?).returns(true)
        end

        it "is allowed if they have the manage_facebook permission" do
          assert @agent.can?(:manage, ::Facebook::Page)
        end

        it "is is is not is allowed if they don't have the manage_facebook permission" do
          @permission_set.permissions.disable(:manage_facebook)
          refute @agent.can?(:manage, ::Facebook::Page)
        end
      end

      describe "for admins" do
        before do
          @admin = users(:minimum_admin)
        end

        it "is allowed" do
          assert @admin.can?(:manage, ::Facebook::Page)
        end
      end
    end
  end
end
