require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::AgentTicket' do
  fixtures :all

  describe "An agent" do
    before do
      @user = users(:minimum_agent)
      @user.stubs(:is_admin?).returns(false)
    end

    subject { @user }

    should_be_able_to(:view, 'unsaved tickets') { accounts(:minimum).tickets.new }

    should_be_able_to(:edit, 'unsaved tickets') { accounts(:minimum).tickets.new }

    describe "without agent restrictions" do
      before do
        @ticket = tickets(:minimum_1)
        @user.stubs(:agent_restriction?).with(:none).returns(true)
        @user.stubs(:agent_restriction?).with(:assigned).returns(false)
        @user.stubs(:agent_restriction?).with(:organization).returns(false)
        @user.stubs(:agent_restriction?).with(:groups).returns(false)
      end

      should_be_able_to(:view, 'a saved ticket') { @ticket }

      describe "without permission sets" do
        before { @user.expects(:permission_set).returns(nil) }

        should_be_able_to(:edit, 'a saved ticket') { @ticket }
        should_be_able_to(:edit, '(or create) a new ticket') { @user.account.tickets.new }
      end

      describe "with permission sets" do
        before do
          @account = @user.account
          @account.stubs(:has_permission_sets?).returns(true)
          @account.permission_sets.create!(name: "foo")
          @user.update_attributes! permission_set: @account.permission_sets.first
          assert @user.has_permission_set?
          assert @user.can?(:view, @ticket)
        end

        describe "with ticket editing allowed" do
          before { @user.permission_set.permissions.enable(:ticket_editing) }

          should_be_able_to(:edit, 'a saved ticket') { @ticket }
          should_be_able_to(:edit, '(or create) a new ticket') { @user.account.tickets.new }
        end

        describe "with ticket editing not allowed" do
          before { @user.permission_set.permissions.disable(:ticket_editing) }

          should_not_be_able_to(:edit, 'a saved ticket') { @ticket }
          should_be_able_to(:edit, '(or create) a new ticket') { @user.account.tickets.new }
        end
      end

      it "has unlimited access to the ticket form" do
        refute @user.can?(:only_create_ticket_comments, tickets(:minimum_1))
      end
    end

    describe "with assigned-ticket restrictions" do
      before do
        @user.stubs(:agent_restriction?).with(:none).returns(false)
        @user.stubs(:agent_restriction?).with(:assigned).returns(true)
        @user.stubs(:agent_restriction?).with(:organization).returns(false)
      end

      describe "reassigning to other agents" do
        before do
          @ticket = tickets(:minimum_1)
          @ticket.assignee = @user
          @ticket.will_be_saved_by(@user)
          @ticket.save!
          @ticket.assignee = users(:minimum_admin)
        end

        it "is allowed" do
          assert @user.can?(:edit, @ticket)
          @ticket.will_be_saved_by(@user)
          assert @ticket.save
        end
      end

      describe "when the ticket is assigned to the agent" do
        before { tickets(:minimum_1).stubs(:assignee_id).returns(@user.id) }
        it "is able to view the ticket if the ticket is assigned to the agent" do
          assert @user.can?(:view, tickets(:minimum_1))
          refute @user.can?(:only_create_ticket_comments, tickets(:minimum_1))
        end
        should_be_able_to(:edit, 'a ticket that is assigned to the agent') { tickets(:minimum_1) }
        it "has unlimited access to the ticket form" do
          refute @user.can?(:only_create_ticket_comments, tickets(:minimum_1))
        end
      end

      describe "when the ticket is unassigned" do
        before { tickets(:minimum_1).expects(:assignee_id).returns(nil) }

        should_not_be_able_to(:view, 'the ticket') { tickets(:minimum_1) }
        should_not_be_able_to(:edit, 'the ticket') { tickets(:minimum_1) }
      end

      describe "when the ticket is assigned to another agent" do
        before { tickets(:minimum_1).stubs(:assignee_id).returns(@user.id + 100) }

        should_not_be_able_to(:view, 'the ticket') { tickets(:minimum_1) }
        should_not_be_able_to(:edit, 'the ticket') { tickets(:minimum_1) }

        describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
          describe "and is collaborating on the ticket" do
            before { tickets(:minimum_1).expects(:is_collaborator?).with(@user).returns(true) }

            should_be_able_to(:view, 'the ticket') { tickets(:minimum_1) }
            should_be_able_to(:edit, 'the ticket') { tickets(:minimum_1) }

            it "has limited access to the ticket form" do
              assert @user.can?(:only_create_ticket_comments, tickets(:minimum_1))
            end
          end
        end

        describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
          describe_with_arturo_setting_enabled :ticket_followers_allowed do
            describe "and is collaborating on the ticket" do
              before { tickets(:minimum_1).expects(:is_collaborator?).with(@user).returns(true) }

              should_be_able_to(:view, 'the ticket') { tickets(:minimum_1) }
              should_be_able_to(:edit, 'the ticket') { tickets(:minimum_1) }

              it "has limited access to the ticket form" do
                assert @user.can?(:only_create_ticket_comments, tickets(:minimum_1))
              end
            end
          end
        end
      end

      describe "when the ticket is assigned to the current agent" do
        before { tickets(:minimum_1).stubs(:assignee_id).returns(@user.id) }

        describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
          describe "and the current agent is also collaborating on the ticket" do
            before { tickets(:minimum_1).expects(:is_collaborator?).with(@user).returns(true) }

            should_be_able_to(:view, 'the ticket') { tickets(:minimum_1) }
            should_be_able_to(:edit, 'the ticket') { tickets(:minimum_1) }

            it "has unlimited access to the ticket form" do
              refute @user.can?(:only_create_ticket_comments, tickets(:minimum_1))
            end
          end
        end

        describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
          describe_with_arturo_setting_enabled :ticket_followers_allowed do
            describe "and the current agent is also collaborating on the ticket" do
              before { tickets(:minimum_1).expects(:is_collaborator?).with(@user).returns(true) }

              should_be_able_to(:view, 'the ticket') { tickets(:minimum_1) }
              should_be_able_to(:edit, 'the ticket') { tickets(:minimum_1) }

              it "has unlimited access to the ticket form" do
                refute @user.can?(:only_create_ticket_comments, tickets(:minimum_1))
              end
            end
          end
        end
      end
    end

    describe "with groups restrictions" do
      before do
        @user.stubs(:agent_restriction?).with(:none).returns(false)
        @user.stubs(:agent_restriction?).with(:assigned).returns(false)
        @user.stubs(:agent_restriction?).with(:groups).returns(true)
        @user.stubs(:agent_restriction?).with(:organization).returns(false)
        @ticket = tickets(:minimum_1)
      end
      describe "when the ticket belongs to a group that the agent belongs to" do
        before do
          @ticket.expects(:group_id).returns(1234567890)
          @user.expects(:in_group?).with(1234567890).returns(true)
        end

        it "is able to view the ticket" do
          assert @user.can?(:view, @ticket)
        end

        it "is able to edit the ticket" do
          assert @user.can?(:edit, @ticket)
        end

        it "should not be able to create only comments" do
          refute @user.can?(:only_create_ticket_comments, @ticket)
        end
      end
      describe "when the ticket does not belong to a group that the agent belongs to" do
        before do
          @ticket.expects(:group_id).returns(1234567890)
          @user.stubs(:in_group?).with(1234567890).returns(false)
        end

        should_not_be_able_to(:view, '(and edit) the ticket') { tickets(:minimum_1) }
      end
    end

    describe "with organization restrictions" do
      before do
        @user.stubs(:agent_restriction?).with(:none).returns(false)
        @user.stubs(:agent_restriction?).with(:assigned).returns(false)
        @user.stubs(:agent_restriction?).with(:groups).returns(false)
        @user.stubs(:agent_restriction?).with(:organization).returns(true)
        @ticket = tickets(:minimum_1)
        @ticket.expects(:requester_id).returns(7777777777)
        @ticket.expects(:requester_id_was).returns(7777777777)
        @ticket.expects(:organization_id).returns(1111).at_least_once
      end

      describe "when the ticket belongs to an organization that the agent belongs to" do
        before { @user.expects(:has_organizations?).returns(true) }

        it "is able to view the ticket" do
          assert @user.can?(:view, @ticket)
        end

        it "is able to edit the ticket" do
          assert @user.can?(:edit, @ticket)
        end

        it "should not be able to create only comments" do
          refute @user.can?(:only_create_ticket_comments, @ticket)
        end
      end

      describe "when the ticket does not belong to an organization that the agent belongs to" do
        before { @user.expects(:has_organizations?).returns(false) }

        should_not_be_able_to(:view, '(and edit) the ticket') { tickets(:minimum_1) }
      end
    end
  end
end
