require_relative "../../support/test_helper"
require_relative "../../support/satisfaction_test_helper"
require_relative "../../support/agent_test_helper"
require_relative "../../support/collaboration_settings_test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Ticket' do
  fixtures :accounts, :account_property_sets, :users, :groups,
    :memberships, :user_identities, :subscriptions, :payments, :sequences,
    :tickets, :events, :ticket_fields, :organizations, :organization_memberships

  include SatisfactionTestHelper
  include ArturoTestHelper
  include AgentTestHelper

  describe "An account admin" do
    before { @user = users(:minimum_admin) }

    it "is allowed to view tickets on own account" do
      accounts(:minimum).tickets.all.each do |ticket|
        assert @user.can?(:view, ticket)
      end
    end

    it "is allowed to edit tickets on own account" do
      accounts(:minimum).tickets.all.each do |ticket|
        assert @user.can?(:edit, ticket)
      end
    end

    it 'is able to is assigned to tickets on own account' do
      assert @user.abilities.can?(:be_assigned_to, Ticket)
    end

    it "is allowed to mark tickets on own account as spam" do
      accounts(:minimum).tickets.all.each do |ticket|
        assert @user.can?(:mark_as_spam, ticket)
      end
    end

    it "is allowed to delete tickets on own account" do
      accounts(:minimum).tickets.all.each do |ticket|
        assert @user.can?(:delete, ticket)
      end
    end

    it "is not is allowed to view/edit/delete tickets on other accounts" do
      accounts(:with_groups).tickets.all.each do |ticket|
        refute @user.can?(:view, ticket)
        refute @user.can?(:edit, ticket)
        refute @user.can?(:delete, ticket)
        refute @user.can?(:mark_as_spam, ticket)
      end
    end
  end

  describe "The system user" do
    before { @user = User.system }

    it "is granted access to all tickets" do
      Ticket.all.each do |ticket|
        assert @user.can?(:view, ticket)
      end
    end

    it "is granted access to all forums" do
      Forum.all.each do |forum|
        assert @user.can?(:view, forum)
      end
    end

    it "is granted access to all posts" do
      Post.all.each do |post|
        assert @user.can?(:view, post)
      end
    end

    it "is not be able to be_assigned_to on tickets" do
      refute @user.abilities.can?(:be_assigned_to, Ticket)
    end
  end

  describe "Viewing" do
    let(:user)   { users(:minimum_end_user) }
    let(:agent)  { users(:minimum_agent) }
    let(:organization)  { organizations(:minimum_organization1) }
    let(:organization2) { organizations(:minimum_organization2) }

    describe "by a group restricted agent" do
      let(:ticket) { tickets(:minimum_2) }
      let(:private_ticket) do
        ticket.account.settings.first_comment_private_enabled = true
        ticket.account.settings.save!
        ticket_initializer = Zendesk::Tickets::Initializer.new(ticket.account, agent, requester_id: user.id, ticket: { submitter: agent, subject: "Private ", comment: { public: false, value: "private ticket about end user" } })
        private_ticket1 = ticket_initializer.ticket
        private_ticket1.will_be_saved_by(agent)
        private_ticket1.save!
        private_ticket1
      end

      before do
        Arturo.enable_feature!(:first_comment_private)
        Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
        agent.update_attribute(:restriction_id, RoleRestrictionType.ORGANIZATION)
      end

      it "shows private tickets to agent that user requested" do
        assert agent.can?(:view, private_ticket)
      end

      it "does not show private tickets to end_user that end_user requested" do
        assert private_ticket.is_private?
        refute user.can?(:view, private_ticket)
      end

      it "shows tickets if they belong to the ticket's organization" do
        agent.update_attribute(:organizations, [organization])
        assert agent.can?(:view, ticket)
      end

      it "is not show tickets if they don't belong to the ticket's organization" do
        agent.update_attribute(:organizations, [organization2])
        refute agent.can?(:view, ticket)
      end
    end

    describe "by an end user" do
      let(:ticket) { tickets(:minimum_1) }

      it "shows tickets that they requested" do
        ticket.requester = user
        ticket.will_be_saved_by(agent)
        ticket.save!
        assert user.can?(:view, ticket)
      end

      it "does not show tickets that they did is not request" do
        ticket.requester = agent
        ticket.will_be_saved_by(agent)
        ticket.save!
        refute user.can?(:view, ticket)
      end

      describe "that is a member of a shared organization" do
        before do
          agent.update_attribute(:organization, organization)
          ticket.requester = agent
          ticket.will_be_saved_by(agent)
          ticket.save!
        end

        it "shows tickets from accessible organizations" do
          user.abilities.expects(:defer?).with(:view, organization).returns(true)

          assert user.can?(:view, ticket)
        end

        it "does not show tickets from inaccessible organizations" do
          user.abilities.expects(:defer?).with(:view, organization).returns(false)

          refute user.can?(:view, ticket)
        end
      end
    end
  end

  describe "Editing" do
    let(:agent)  { users(:minimum_agent) }

    describe "by an end user" do
      let(:user)   { users(:minimum_end_user) }
      let(:ticket) { tickets(:minimum_1) }

      it "is possible on tickets that they've requested" do
        ticket.requester = user
        ticket.will_be_saved_by(agent)
        ticket.save!

        assert user.can?(:edit, ticket)
      end

      it "is possible on new tickets" do
        assert user.can?(:edit, Ticket.new)
      end

      it "is possible on tickets on which they are collaborating" do
        ticket.collaborations.build(ticket: ticket, user: user, collaborator_type: CollaboratorType.EMAIL_CC)
        ticket.requester = agent
        ticket.will_be_saved_by(agent)
        ticket.save!

        assert user.can?(:edit, ticket)
      end

      it "is not possible to assign tickets on own account" do
        refute user.abilities.can?(:be_assigned_to, Ticket)
      end

      it "is not possible on tickets they are not participating in (requester or collaborator)" do
        ticket.requester = agent
        ticket.will_be_saved_by(agent)
        ticket.save!

        refute user.can?(:edit, ticket)
      end

      describe "that replies via twitter" do
        before do
          ticket.stubs(comment: stub(via_facebook?: false, via_twitter?: true))
          user.stubs(has_twitter_identity?: true)
        end

        it "is possible" do
          assert user.can?(:edit, ticket)
        end
      end

      describe "that replies via facebook" do
        before do
          ticket.stubs(comment: stub(via_facebook?: true, via_twitter?: false))
          user.stubs(has_facebook_identity?: true)
        end

        it "is possible" do
          assert user.can?(:edit, ticket)
        end
      end

      describe "that is a member of a shared organization" do
        let(:organization1) { organizations(:minimum_organization1) }
        let(:organization2) { organizations(:minimum_organization2) }
        before { Account.any_instance.stubs(has_multiple_organizations_enabled?: true) }

        it "is possible on tickets from accessible organizations" do
          organization2.update_attribute(:is_shared_comments, true)
          user.update_attribute(:organizations, [organization1, organization2])
          agent.update_attribute(:organization, organization2)

          ticket.requester = agent
          ticket.will_be_saved_by(agent)
          ticket.save!

          assert_equal organization1, user.default_organization
          assert user.can?(:edit, ticket)
        end

        it "is not possible on tickets from inaccessible organizations" do
          ticket.requester = agent
          ticket.organization = organization1
          ticket.will_be_saved_by(agent)
          ticket.save!

          refute user.can?(:edit, ticket)
        end
      end
    end

    describe "An agent lacking the 'ticket_editing' permission" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
        agent.permission_set = agent.account.permission_sets.create!(name: "Test")
        agent.permission_set.permissions.disable(:ticket_editing)
      end

      it 'not is able to is assigned to tickets on own account' do
        refute agent.abilities.can?(:be_assigned_to, Ticket)
      end
    end
  end

  describe "Editing ticket properties" do
    before do
      @ticket = tickets(:minimum_1)
      @user = users(:minimum_agent)
    end
    it "is not is allowed for users that canis not edit the ticket" do
      @user.abilities.expects(:defer?).with(:edit, @ticket).returns(false)
      refute @user.can?(:edit_properties, @ticket)
    end

    it "is not is allowed for users that can only create comments on that ticket" do
      @user.abilities.expects(:defer?).with(:edit, @ticket).returns(true)
      @user.abilities.expects(:defer?).with(:only_create_ticket_comments, @ticket).returns(true)
      refute @user.can?(:edit_properties, @ticket)
    end

    it "is allowed for users that can edit the ticket and not only can create comments" do
      @user.abilities.expects(:defer?).with(:edit, @ticket).returns(true)
      @user.abilities.expects(:defer?).with(:only_create_ticket_comments, @ticket).returns(false)
      assert @user.can?(:edit_properties, @ticket)
    end

    it "is allowed for new tickets" do
      @ticket = Ticket.new
      assert @user.can?(:edit_properties, @ticket)
    end
  end

  describe 'Given an agent who can not edit tickets' do
    before do
      @agent = users(:minimum_agent)
      @agent.account.stubs(:has_permission_sets?).returns(true)

      @agent.permission_set = @agent.account.permission_sets.create!(name: "Test")

      @agent.permission_set.permissions.disable(:ticket_editing)
      refute @agent.permission_set.permissions.ticket_editing?
    end

    describe 'And a ticket that they are not assigned to' do
      before do
        end_user = users(:minimum_end_user)
        @ticket = @agent.account.tickets.build(requester: end_user, assignee: end_user, description: "hi")
        @ticket.will_be_saved_by(@ticket.requester)
        @ticket.save!
      end

      subject { @agent }

      should_not_be_able_to(:edit_properties, "of that ticket") { @ticket }

      describe 'If they are the requester' do
        before do
          @ticket.requester = @agent
        end

        should_be_able_to(:edit_properties, "of that ticket") { @ticket }
      end
    end
  end

  describe 'Ticket resolution' do
    describe 'an end-user' do
      before do
        @user = users(:minimum_end_user)
        @ticket = tickets(:minimum_1)
      end

      it "is not be able to mark as solved a ticket requested by an end-user" do
        assert_not_equal @user.organization_id, @ticket.organization_id
        refute @user.can?(:mark_solved, @ticket)
      end

      it 'is not be able to mark as solved their own solved ticket' do
        @ticket.status_id = StatusType.SOLVED
        refute @user.can?(:mark_solved, @ticket)
      end

      it 'is not be able to mark as solved their own assignee-less ticket' do
        @ticket.requester = @user
        @ticket.status_id = StatusType.PENDING
        @ticket.assignee = nil
        refute @user.can?(:mark_solved, @ticket)
      end

      it 'is not be able to mark as solved if a problem ticket' do
        @ticket.requester = @user
        @ticket.status_id = StatusType.OPEN
        @ticket.ticket_type_id = 3
        refute @user.can?(:mark_solved, @ticket)
      end

      it 'is able to mark their own pending ticket as solved' do
        @ticket.requester = @user
        @ticket.status_id = StatusType.PENDING
        assert @user.can?(:mark_solved, @ticket)
      end

      it 'is not be able to keep as solved a ticket requested by ais nother end-user' do
        assert_not_equal @user.organization_id, @ticket.organization_id
        refute @user.can?(:keep_solved, @ticket)
      end

      it 'is not be able to keep as solved their own unsolved ticket' do
        @ticket.requester = @user
        @ticket.status_id = StatusType.OPEN
        refute @user.can?(:keep_solved, @ticket)
      end

      it 'is able to keep as solved their own solved ticket' do
        @ticket.requester = @user
        @ticket.status_id = StatusType.SOLVED
        assert @user.can?(:keep_solved, @ticket)
      end
    end
  end

  describe 'Rating satisfaction with tickets' do
    [true, false].each do |agent_as_end_user_enabled|
      describe ":agent_as_end_user Arturo #{agent_as_end_user_enabled ? 'en' : 'dis'}abled" do
        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            let(:ticket) { tickets(:minimum_2) }
            let(:ever_solved) { true }
            let(:satisfaction_score) { SatisfactionType.UNOFFERED }
            let(:end_user) { users(:minimum_end_user) }
            let(:agent) { users(:minimum_agent) }
            let(:requester_id) { end_user.id }
            let(:via_reference_id) { ViaType.HELPCENTER }
            let(:submitter_id) { end_user.id }

            before do
              Arturo.set_feature!(:agent_as_end_user, agent_as_end_user_enabled)
              ticket.stubs(:ever_solved?).returns(ever_solved)
              ticket.stubs(:satisfaction_score).returns(satisfaction_score)
              ticket.update_column(:requester_id, requester_id)
              ticket.stubs(:via_id).returns(via_id)
              ticket.stubs(:via_reference_id).returns(via_reference_id)
              ticket.stubs(:submitter_id).returns(submitter_id)
            end

            describe_with_arturo_setting_disabled :customer_satisfaction do
              it "returns false for an end-user" do
                refute end_user.can?(:rate_satisfaction, ticket)
              end
            end

            describe_with_arturo_setting_enabled :customer_satisfaction do
              describe "for an agent" do
                let(:requester_id) { agent.id }

                it "returns false" do
                  refute agent.can?(:rate_satisfaction, ticket)
                end
              end

              describe "someone else's ticket" do
                let(:requester_id) { agent.id }

                it "returns false for an end-user" do
                  refute end_user.can?(:rate_satisfaction, ticket)
                end
              end

              describe "ticket that has never been solved or had satisfaction offered" do
                let(:ever_solved) { false }

                it "returns false for an end-user" do
                  refute end_user.can?(:rate_satisfaction, ticket)
                end
              end

              describe "ticket has been solved" do
                it "returns true for an end-user" do
                  assert end_user.can?(:rate_satisfaction, ticket)
                end
              end

              describe "ticket that has been marked offered" do
                let(:ever_solved) { false }
                let(:satisfaction_score) { SatisfactionType.OFFERED }

                it "returns true for an end-user" do
                  assert end_user.can?(:rate_satisfaction, ticket)
                end
              end

              describe "agent-as-end-user ticket" do
                let(:requester_id) { agent.id }
                let(:submitter_id) { agent.id }

                it "returns #{agent_as_end_user_enabled}" do
                  assert_equal agent_as_end_user_enabled, agent.can?(:rate_satisfaction, ticket)
                end
              end
            end
          end
        end
      end
    end
  end

  describe 'Viewing satisfaction event details' do
    before do
      @end_user = users(:minimum_end_user)
      @agent = users(:minimum_agent)
      @ticket = build_ticket_with_satisfaction_rating
      @other_users_ticket = tickets(:minimum_1)
    end

    describe 'an end-user' do
      it "only reacts is able to see details of their own ratings" do
        assert @end_user.can?(:view_satisfaction, @ticket)
        refute @end_user.can?(:view_satisfaction, @other_users_ticket)
      end
    end

    describe 'an agent' do
      it "is able details of other agents' ratings" do
        @ticket.assignee = users(:minimum_admin)
        assert @agent.can?(:view_satisfaction, @ticket)
      end

      it "is able to see details of their own ratings" do
        @ticket.assignee = @agent
        assert @agent.can?(:view_satisfaction, @ticket)
      end
    end

    describe 'an admin' do
      it "is able to see details of all agents' ratings regardless of privacy setting" do
        @admin = users(:minimum_admin)

        assert @admin.can?(:view_satisfaction, @ticket)
      end
    end
  end

  describe "Bulk merging tickets" do
    before do
      @end_user = users(:minimum_end_user)
      @agent = users(:minimum_agent)
      @admin = users(:minimum_admin)
      @permission_set = build_valid_permission_set
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
    end

    describe "in an account with ticket collaboration disabled and email CCs disabled" do
      before do
        Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
        Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
      end

      it "is denied for admins, agents and end users" do
        refute @admin.can?(:bulk_merge, Ticket)
        refute @agent.can?(:bulk_merge, Ticket)
        refute @end_user.can?(:bulk_merge, Ticket)
      end

      it "is denied for agents with permission set even if they have ticket merge access" do
        @agent.stubs(:permission_set).returns(@permission_set)
        @permission_set.permissions.enable(:ticket_merge)
        refute @agent.can?(:bulk_merge, Ticket)
      end
    end

    describe "in an account with ticket collaboration enabled" do
      before do
        Account.any_instance.stubs(:is_collaboration_enabled?).returns(true)
        Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
      end

      it "is allowed for admins and agents and denied for end users" do
        assert @admin.can?(:bulk_merge, Ticket)
        assert @agent.can?(:bulk_merge, Ticket)
        refute @end_user.can?(:bulk_merge, Ticket)
      end

      describe "for agents with permission set" do
        before { @agent.stubs(:permission_set).returns(@permission_set) }

        describe "and ticket merge access" do
          before { @permission_set.permissions.enable(:ticket_merge) }

          it "is allowed" do
            assert @agent.can?(:bulk_merge, Ticket)
          end
        end

        describe "and no ticket merge access" do
          before { @permission_set.permissions.disable(:ticket_merge) }

          it "is denied" do
            refute @agent.can?(:bulk_merge, Ticket)
          end
        end
      end
    end

    describe "in an account with email CCs enabled" do
      before do
        Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
        Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
      end

      it "is allowed for admins and agents and denied for end users" do
        assert @admin.can?(:bulk_merge, Ticket)
        assert @agent.can?(:bulk_merge, Ticket)
        refute @end_user.can?(:bulk_merge, Ticket)
      end

      describe "for agents with permission set" do
        before { @agent.stubs(:permission_set).returns(@permission_set) }

        describe "and ticket merge access" do
          before { @permission_set.permissions.enable(:ticket_merge) }

          it "is allowed" do
            assert @agent.can?(:bulk_merge, Ticket)
          end
        end

        describe "and no ticket merge access" do
          before { @permission_set.permissions.disable(:ticket_merge) }

          it "is denied" do
            refute @agent.can?(:bulk_merge, Ticket)
          end
        end
      end
    end
  end

  describe "Bulk editing tickets" do
    let(:end_user) { users(:minimum_end_user) }
    let(:agent) { users(:minimum_agent) }
    let(:admin) { users(:minimum_admin) }

    describe "on account without permission sets" do
      before { Account.any_instance.stubs(:has_permission_sets?).returns(false) }

      it "is allowed for admins and agents and denied for end users" do
        assert admin.can?(:ticket_bulk_edit, Ticket)
        assert agent.can?(:ticket_bulk_edit, Ticket)
        refute end_user.can?(:ticket_bulk_edit, Ticket)
      end
    end

    describe "on account with permission sets" do
      let(:permission_set) { build_valid_permission_set }

      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
        agent.stubs(:permission_set).returns(permission_set)
      end

      it "is allowed for admins and denied for end-users" do
        assert admin.can?(:ticket_bulk_edit, Ticket)
        refute end_user.can?(:ticket_bulk_edit, Ticket)
      end

      describe "for agents with the permission disabled" do
        before { permission_set.permissions.disable(:ticket_bulk_edit) }

        it "prevents bulk editing tickets" do
          refute agent.can?(:ticket_bulk_edit, Ticket)
        end
      end

      describe "for agents with the permission enabled" do
        before { permission_set.permissions.enable(:ticket_bulk_edit) }

        it "allows bulk editing tickets" do
          assert agent.can?(:ticket_bulk_edit, Ticket)
        end
      end
    end
  end

  describe "Editing tags in account with permission sets" do
    let(:account) { accounts(:minimum) }
    let(:end_user) { users(:minimum_end_user) }
    let(:agent) { users(:minimum_agent) }
    let(:admin) { users(:minimum_admin) }
    let(:ticket) { tickets(:minimum_1) }
    let(:new_ticket) { account.tickets.new }
    let(:permission_set) { build_valid_permission_set }

    before do
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
      agent.stubs(:permission_set).returns(permission_set)
    end

    it "is allowed for admins" do
      assert admin.can?(:edit_tags, ticket)
    end

    it "is denied for end-users" do
      refute end_user.can?(:edit_tags, ticket)
    end

    describe "for agents with edit_ticket_tags permission enabled" do
      before { permission_set.permissions.stubs(:edit_ticket_tags?).returns(true) }

      it "is allowed for existing tickets" do
        assert agent.can?(:edit_tags, ticket)
      end

      it "is allowed for new tickets" do
        assert agent.can?(:edit_tags, new_ticket)
      end

      it "is allowed for their own tickets" do
        ticket.stubs(:requester).returns(agent)
        assert agent.can?(:edit_tags, ticket)
      end
    end

    describe "for agents with edit_ticket_tags permission disabled" do
      before { permission_set.permissions.stubs(:edit_ticket_tags?).returns(false) }

      it "is denied for existing tickets" do
        refute agent.can?(:edit_tags, ticket)
      end

      it "is allowed for new tickets" do
        assert agent.can?(:edit_tags, new_ticket)
      end

      it "is allowed for their own tickets" do
        ticket.stubs(:requester).returns(agent)
        assert agent.can?(:edit_tags, ticket)
      end
    end
  end

  describe "Merging tickets" do
    before do
      @end_user = users(:minimum_end_user)
      @agent = users(:minimum_agent)
      @admin = users(:minimum_admin)
      @ticket = tickets(:minimum_1)
      @permission_set = build_valid_permission_set
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
    end

    it "is allowed for admins and agents and allowed tickets" do
      assert @admin.can?(:merge, @ticket)
      assert @agent.can?(:merge, @ticket)
    end

    it "is denied for end users" do
      refute @end_user.can?(:merge, @ticket)
    end

    it "is denied for tickets that the user can't view" do
      @admin.abilities.expects(:defer?).with(:view, @ticket).returns(false)
      refute @admin.can?(:merge, @ticket)
    end

    it "is denied for new tickets" do
      @ticket.stubs(:new_record?).returns(true)
      refute @admin.can?(:merge, @ticket)
    end

    it "is denied for shared tickets" do
      @ticket.stubs(:shared_tickets).returns([SharedTicket.new])
      refute @admin.can?(:merge, @ticket)
    end

    it "is denied for tickets that the user can view but can only add comments" do
      @admin.abilities.stubs(:defer?).with(:view, @ticket).returns(true)
      @admin.abilities.expects(:defer?).with(:only_create_ticket_comments, @ticket).returns(true)
      refute @admin.can?(:merge, @ticket)
    end

    describe "for agents with permission set" do
      before { @agent.stubs(:permission_set).returns(@permission_set) }

      describe "and ticket merge access" do
        before { @permission_set.permissions.enable(:ticket_merge) }

        it "is allowed when the ticket conditions are satisfied" do
          Access::Permissions::TicketPermission.any_instance.expects(:can_merge_ticket?).with(@ticket).returns(true)
          assert @agent.can?(:merge, @ticket)
        end

        it "is denied when the ticket conditions are not satisfied" do
          Access::Permissions::TicketPermission.any_instance.expects(:can_merge_ticket?).with(@ticket).returns(false)
          refute @agent.can?(:merge, @ticket)
        end
      end

      describe "and no ticket merge access" do
        before { @permission_set.permissions.disable(:ticket_merge) }

        it "is denied even when the ticket conditions are satisfied" do
          Access::Permissions::TicketPermission.any_instance.expects(:can_merge_ticket?).with(@ticket).returns(true)
          refute @agent.can?(:merge, @ticket)
        end

        it "is denied when the ticket conditions are not satisfied" do
          Access::Permissions::TicketPermission.any_instance.expects(:can_merge_ticket?).with(@ticket).returns(false)
          refute @agent.can?(:merge, @ticket)
        end
      end
    end
  end

  describe "#view_private_content?" do
    let(:ticket) { tickets(:minimum_1) }

    describe "agent_as_end_user Arturo is disabled" do
      let(:user) { users(:minimum_admin) }

      before do
        Arturo.disable_feature!(:agent_as_end_user)
      end

      it "returns the result of calling #view?" do
        Access::Policies::TicketPolicy.
          any_instance.
          expects(:view?).
          with(ticket).
          returns(true)
        assert user.can?(:view_private_content, ticket)
      end
    end

    describe "agent_as_end_user Arturo is enabled" do
      before do
        Arturo.enable_feature!(:agent_as_end_user)
      end

      describe "user is admin" do
        let(:user) { users(:minimum_admin) }

        it "is true" do
          Access::Policies::TicketPolicy.
            any_instance.
            expects(:view?).
            never

          assert user.can?(:view_private_content, ticket)
        end
      end

      describe "user is not an agent" do
        let(:user) { users(:minimum_end_user) }

        it "is false" do
          Access::Policies::TicketPolicy.
            any_instance.
            expects(:view?).
            never

          refute user.can?(:view_private_content, ticket)
        end
      end

      describe "ticket is 'agent as end user' for agent" do
        let(:user) { users(:minimum_agent) }

        it "is false" do
          Access::Policies::TicketPolicy.
            any_instance.
            expects(:view?).
            never

          ticket.expects(:agent_as_end_user_for?).with(user).returns(true)
          refute user.can?(:view_private_content, ticket)
        end
      end

      describe "ticket is not 'agent as end user' for agent" do
        let(:user) { users(:minimum_agent) }

        it "passes through to #view?" do
          Access::Policies::TicketPolicy.
            any_instance.
            expects(:view?).
            with(ticket).
            returns(true)

          ticket.expects(:agent_as_end_user_for?).with(user).returns(false)
          assert user.can?(:view_private_content, ticket)
        end
      end
    end
  end

  describe "#only_create_ticket_comments?" do
    before do
      Account.any_instance.stubs(:has_light_agents?).returns(true)
      @account = accounts(:minimum)

      @user = users(:minimum_agent)

      @group1 = groups(:minimum_group)
      @group2 = groups(:support_group).tap { |g| g.is_active = true }
      @group2.save!

      @org1 = organizations(:minimum_organization1)
      @org2 = organizations(:minimum_organization2)

      @ticket = tickets(:minimum_1)
    end

    it "is false if the agent is neither restricted nor a light agent" do
      refute @user.is_light_agent?
      refute @user.is_restricted_agent?
      refute @user.can?(:only_create_ticket_comments, @ticket)
    end

    describe "by light agents" do
      before do
        @user = users(:minimum_agent)
        @user.permission_set = @account.permission_sets.create!(name: I18n.t("txt.default.roles.light_agent.name"), role_type: PermissionSet::Type::LIGHT_AGENT)
        @user.permission_set.permissions.set(PermissionSet::LightAgent.configuration)
        @user.permission_set.permissions.set(ticket_access: "all")
        @user.assigned.not_closed.update_all(assignee_id: nil)
        @user.save!

        assert @user.is_light_agent?
        refute @ticket.is_collaborator?(@user)
        assert @user.can?(:view, @ticket)
        refute @user.can?(:edit, @ticket)
      end

      describe "an unrestricted light agent" do
        before { @user.permission_set.permissions.set(ticket_access: "all") }

        it "can only_create_ticket_comments?" do
          assert @user.permission_set.has_ticket_restriction?(:none)
          refute @user.is_restricted_agent?
          assert @user.can?(:only_create_ticket_comments, @ticket)
        end
      end

      describe "an assignment restricted light agent" do
        before do
          @user.permission_set.permissions.set(ticket_access: "assigned-only")
          assert @user.permission_set.has_ticket_restriction?(:assigned)
          assert @user.is_restricted_agent?
        end

        it "can not create comments for tickets they're not assigned to" do
          refute @user.can?(:only_create_ticket_comments, @ticket)
        end

        it "can not create comments for tickets they're assigned to" do
          # light agents aren't supposed to be assigned tickets
          @ticket.assignee_id = @user.id
          refute @user.can?(:only_create_ticket_comments, @ticket)
        end
      end

      describe "a group restricted light agent" do
        before do
          @user.permission_set.permissions.set(ticket_access: "within-groups")
          assert @user.permission_set.has_ticket_restriction?(:groups)
          assert @user.is_restricted_agent?
        end

        it "can edit if ticket is within agent group" do
          assert @user.can?(:only_create_ticket_comments, @ticket)
        end

        it "cannot edit if ticket is not within agent group" do
          @ticket.group = nil
          refute @user.can?(:only_create_ticket_comments, @ticket)
        end
      end

      describe "an organization restricted light agent" do
        before do
          @user.permission_set.permissions.set(ticket_access: "within-organization")
          assert @user.permission_set.has_ticket_restriction?(:organization)
          assert @user.is_restricted_agent?
        end

        it "can edit if ticket is within agent orgs" do
          @ticket.organization = @user.organization
          assert @user.can?(:only_create_ticket_comments, @ticket)
        end

        it "cannot edit if ticket is not within agent orgs" do
          refute @user.can?(:only_create_ticket_comments, @ticket)
        end
      end
    end

    describe "by restricted agents" do
      let(:access_level) { "all" }

      before do
        @user = users(:minimum_agent)
        @user.permission_set = @account.permission_sets.create!(name: "Test")
        @user.permission_set.permissions.set(ticket_access: access_level)
        @user.assigned.not_closed.update_all(assignee_id: nil)
        @user.save!
      end

      describe "an assignment restricted agent" do
        let(:access_level) { "assigned-only" }

        before do
          assert @user.permission_set.has_ticket_restriction?(:assigned)
          assert @user.is_restricted_agent?
        end

        describe "which is also a collaborator" do
          before do
            @ticket.stubs(:is_collaborator?).with(@user).returns(true)
          end

          it "is true for tickets they're not assigned to" do
            @ticket.assignee_id = users(:minimum_admin).id
            assert @user.can?(:only_create_ticket_comments, @ticket)
          end

          it "is false for tickets they're assigned to" do
            assert_equal @user.id, @ticket.assignee_id
            refute @user.can?(:only_create_ticket_comments, @ticket)
          end
        end

        describe "which is not a collaborator" do
          before do
            @ticket.stubs(:is_collaborator?).with(@user).returns(false)
          end

          it "is false even for tickets they're not assigned to" do
            @ticket.assignee_id = users(:minimum_admin).id
            refute @user.can?(:only_create_ticket_comments, @ticket)
          end
        end
      end

      describe "a group restricted agent" do
        let(:access_level) { "within-groups" }

        before do
          assert @user.permission_set.has_ticket_restriction?(:groups)
          assert @user.is_restricted_agent?
        end

        describe "which is also a collaborator" do
          before do
            @ticket.stubs(:is_collaborator?).with(@user).returns(true)
          end

          it "is false if ticket is within agent group" do
            assert @user.in_group?(@ticket.group)
            refute @user.can?(:only_create_ticket_comments, @ticket)
          end

          it "is true if ticket is not within agent group" do
            @user.stubs(:in_group?).with(@ticket.group).returns(false)
            assert @user.can?(:only_create_ticket_comments, @ticket)
          end
        end

        describe "which is not a collaborator" do
          before do
            @ticket.stubs(:is_collaborator?).with(@user).returns(false)
          end

          it "is false even if ticket is not within agent group" do
            @user.stubs(:in_group?).returns(false)
            refute @user.can?(:only_create_ticket_comments, @ticket)
          end
        end
      end

      describe "an organization restricted agent" do
        let(:access_level) { "within-organization" }

        before do
          assert @user.permission_set.has_ticket_restriction?(:organization)
          assert @user.is_restricted_agent?
        end

        describe "which is also a collaborator" do
          before do
            @ticket.stubs(:is_collaborator?).with(@user).returns(true)
          end

          it "is false if ticket is within agent orgs" do
            assert @user.has_organizations?(@ticket.organization_id)
            refute @user.can?(:only_create_ticket_comments, @ticket)
          end

          it "is true if ticket is not within agent orgs" do
            @user.stubs(:has_organizations?).with(@ticket.organization_id).returns(false)
            assert @user.can?(:only_create_ticket_comments, @ticket)
          end
        end

        describe "which is not a collaborator" do
          before do
            @ticket.stubs(:is_collaborator?).with(@user).returns(false)
          end

          it "is false even if ticket is not within agent orgs" do
            @user.stubs(:has_organizations?).returns(false)
            refute @user.can?(:only_create_ticket_comments, @ticket)
          end
        end
      end
    end
  end

  describe "Marking as spam" do
    let(:agent)  { users(:minimum_agent) }
    let(:ticket) { tickets(:minimum_1) }
    describe 'an agent' do
      it 'denies marking themselves as spam' do
        ticket.stubs(:requester_id).returns(agent.id)
        refute agent.can?(:mark_as_spam, ticket)
      end

      it 'denies an agent requester' do
        agent2 = users(:minimum_agent)
        ticket.stubs(:requester_id).returns(agent2.id)
        refute agent.can?(:mark_as_spam, ticket)
      end

      it 'denies an admin requester' do
        admin = users(:minimum_admin)
        ticket.stubs(:requester_id).returns(admin.id)
        refute agent.can?(:mark_as_spam, ticket)
      end

      it 'denies a nil requester' do
        ticket.stubs(:requester).returns(nil)
        refute agent.can?(:mark_as_spam, ticket)
      end

      it 'denies a shared ticket' do
        ticket.stubs(:shared_tickets).returns([SharedTicket.new])
        refute agent.can?(:mark_as_spam, ticket)
      end

      describe "can edit requester" do
        before do
          agent.stubs(:has_permission_set?).returns(true)
          agent.permission_set = PermissionSet.new
          agent.permission_set.permissions.end_user_profile = 'full'

          assert agent.can?(:edit, ticket.requester)
        end

        it "is able to mark as spam if agent has ability to delete ticket" do
          agent.stubs(:agent_restriction?).returns(false)
          assert agent.can?(:delete, ticket)
          assert agent.can?(:mark_as_spam, ticket)
        end

        it "denies if agent does not have abilty to delete ticket" do
          agent.permission_set.permissions.disable(:ticket_deletion)
          refute agent.can?(:delete, ticket)
          refute agent.can?(:mark_as_spam, ticket)
        end
      end

      describe "cannot edit requester" do
        before do
          agent.stubs(:has_permission_set?).returns(true)
          agent.permission_set = PermissionSet.new
          agent.permission_set.permissions.end_user_profile = 'readonly'

          refute agent.can?(:edit, ticket.requester)
        end

        it "denies whether agent has ability to delete ticket or not" do
          agent.permission_set.permissions.disable(:ticket_deletion)
          refute agent.can?(:delete, ticket)
          refute agent.can?(:mark_as_spam, ticket)
        end
      end
    end
  end

  describe "Adding collaborators" do
    let(:account)  { accounts(:minimum) }
    let(:ticket)   { Ticket.new(account: account) }
    let(:agent)    { users(:minimum_agent) }
    let(:end_user) { users(:minimum_end_user) }

    describe "with CCs/Followers feature disabled" do
      before do
        CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
      end

      describe "for an account with collaboration disabled" do
        before { Account.any_instance.stubs(is_collaboration_enabled?: false) }

        it "is disallowed for agents" do
          refute agent.can?(:add_collaborator, ticket)
        end

        it "is disallowed for end-users" do
          refute end_user.can?(:add_collaborator, ticket)
        end
      end

      describe "for an account with collaboration enabled" do
        before { Account.any_instance.stubs(is_collaboration_enabled?: true) }

        describe "only for agents" do
          before { Account.any_instance.stubs(is_collaborators_addable_only_by_agents?: true) }

          it "is allowed for agents" do
            assert agent.can?(:add_collaborator, ticket)
          end

          it "is disallowed for end-users" do
            refute end_user.can?(:add_collaborator, ticket)
          end
        end

        describe "also for end-users" do
          before { Account.any_instance.stubs(is_collaborators_addable_only_by_agents?: false) }

          it "is allowed for agents" do
            assert agent.can?(:add_collaborator, ticket)
          end

          it "is allowed for end-users" do
            assert end_user.can?(:add_collaborator, ticket)
          end
        end
      end
    end

    describe "with CCs/Followers feature enabled" do
      let(:can_add_follower) { false }
      let(:can_add_email_cc) { false }

      before do
        CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
        Access::Policies::TicketPolicy.any_instance.stubs(add_follower?: can_add_follower)
        Access::Policies::TicketPolicy.any_instance.stubs(add_email_cc?: can_add_email_cc)
      end

      describe "when add_follower? is false" do
        describe "when add_email_cc? is false" do
          it "is disallowed for agents" do
            refute agent.can?(:add_collaborator, ticket)
          end

          it "is disallowed for end-users" do
            refute end_user.can?(:add_collaborator, ticket)
          end
        end

        describe "when add_email_cc? is true" do
          let(:can_add_email_cc) { true }

          it "is allowed for agents" do
            assert agent.can?(:add_collaborator, ticket)
          end

          it "is allowed for end-users" do
            assert end_user.can?(:add_collaborator, ticket)
          end
        end
      end

      describe "when add_follower? is true" do
        let(:can_add_follower) { true }

        describe "when add_email_cc? is false" do
          it "is allowed for agents" do
            assert agent.can?(:add_collaborator, ticket)
          end

          it "is allowed for end-users" do
            assert end_user.can?(:add_collaborator, ticket)
          end
        end

        describe "when add_email_cc? is true" do
          let(:can_add_email_cc) { true }

          it "is allowed for agents" do
            assert agent.can?(:add_collaborator, ticket)
          end

          it "is allowed for end-users" do
            assert end_user.can?(:add_collaborator, ticket)
          end
        end
      end
    end
  end

  describe "Adding followers" do
    let(:account)  { accounts(:minimum) }
    let(:ticket)   { Ticket.new(account: account) }
    let(:agent)    { users(:minimum_agent) }
    let(:end_user) { users(:minimum_end_user) }

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe_with_arturo_setting_disabled :ticket_followers_allowed do
        it "is not allowed for agents or end users" do
          refute agent.can?(:add_follower, ticket)
          refute end_user.can?(:add_follower, ticket)
        end
      end

      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        it "is allowed for agents" do
          assert agent.can?(:add_follower, ticket)
        end

        it "is not allowed for end users" do
          refute end_user.can?(:add_follower, ticket)
        end
      end
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it "uses legacy check for adding collaborators" do
        Access::Policies::TicketPolicy.any_instance.expects(:add_collaborator?)
        end_user.can?(:add_follower, ticket)
      end
    end
  end

  describe "Adding email CCs" do
    let(:account)  { accounts(:minimum) }
    let(:ticket)   { Ticket.new(account: account) }
    let(:agent)    { users(:minimum_agent) }
    let(:end_user) { users(:minimum_end_user) }

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
        it "is not allowed for agents or end users" do
          refute agent.can?(:add_email_cc, ticket)
          refute end_user.can?(:add_email_cc, ticket)
        end
      end

      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        it "is allowed for agents" do
          assert agent.can?(:add_email_cc, ticket)
        end

        it "is allowed for end users" do
          assert end_user.can?(:add_email_cc, ticket)
        end

        describe "with collaborators_addable_only_by_agents enabled" do
          before do
            Account.any_instance.stubs(is_collaborators_addable_only_by_agents?: true)
          end

          it "is allowed for end users" do
            assert end_user.can?(:add_email_cc, ticket)
          end
        end
      end
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it "uses legacy check for adding collaborators" do
        Access::Policies::TicketPolicy.any_instance.expects(:add_collaborator?)
        end_user.can?(:add_email_cc, ticket)
      end
    end
  end

  describe "Viewing ticket satisfaction predictions" do
    before do
      @end_user = users(:minimum_end_user)
      @agent = users(:minimum_agent)
      @admin = users(:minimum_admin)
      @permission_set = build_valid_permission_set
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
    end

    describe "in an account with satisfaction prediction disabled" do
      before { Account.any_instance.stubs(:has_satisfaction_prediction_enabled?).returns(false) }
      it "is denied for admins, agents and end users" do
        refute @admin.can?(:view_satisfaction_prediction, Ticket)
        refute @agent.can?(:view_satisfaction_prediction, Ticket)
        refute @end_user.can?(:view_satisfaction_prediction, Ticket)
      end

      it "is denied for agents with permission set even if they have have permission to view satisfaction prediction" do
        @agent.stubs(:permission_set).returns(@permission_set)
        @permission_set.permissions.enable(:view_ticket_satisfaction_prediction)
        refute @agent.can?(:view_satisfaction_prediction, Ticket)
      end
    end

    describe "in an account with satisfaction prediction enabled" do
      before { Account.any_instance.stubs(:has_satisfaction_prediction_enabled?).returns(true) }
      it "is allowed for admins and agents and denied for end users" do
        assert @admin.can?(:view_satisfaction_prediction, Ticket)
        assert @agent.can?(:view_satisfaction_prediction, Ticket)
        refute @end_user.can?(:view_satisfaction_prediction, Ticket)
      end

      describe "for agents with permission set" do
        before { @agent.stubs(:permission_set).returns(@permission_set) }

        describe "and permission to view satisfaction prediction" do
          before { @permission_set.permissions.enable(:view_ticket_satisfaction_prediction) }
          it "is allowed" do
            assert @agent.can?(:view_satisfaction_prediction, Ticket)
          end
        end

        describe "and no permission to view satisfaction prediction" do
          before { @permission_set.permissions.disable(:view_ticket_satisfaction_prediction) }
          it "is denied" do
            refute @agent.can?(:view_satisfaction_prediction, Ticket)
          end
        end
      end
    end
  end

  describe "Viewing deleted tickets" do
    before do
      @end_user = users(:minimum_end_user)
      @agent = users(:minimum_agent)
      @admin = users(:minimum_admin)
      @permission_set = build_valid_permission_set
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
    end

    it "is allowed for admins" do
      assert @admin.can?(:view_deleted, Ticket)
    end

    it "is not allowed for restricted agents with ticket_delete_for_agents setting disabled" do
      @agent.stubs(:is_non_restricted_agent?).returns(false)
      @agent.account.settings.disable(:ticket_delete_for_agents)
      refute @agent.can?(:view_deleted, Ticket)
    end

    it "is not allowed for unrestricted agents without ticket_delete_for_agents setting disabled" do
      @agent.stubs(:is_non_restricted_agent?).returns(true)
      @agent.account.settings.disable(:ticket_delete_for_agents)
      refute @agent.can?(:view_deleted, Ticket)
    end

    it "is allowed for unrestricted agents with ticket_delete_for_agents setting enabled" do
      @agent.stubs(:is_non_restricted_agent?).returns(true)
      @agent.account.settings.enable(:ticket_delete_for_agents)
      assert @agent.can?(:view_deleted, Ticket)
    end

    it "is not allowed for restricted agents" do
      @agent.stubs(:is_non_restricted_agent?).returns(false)
      @agent.account.settings.enable(:ticket_delete_for_agents)
      refute @agent.can?(:view_deleted, Ticket)
    end

    it "is not allowed for end users" do
      refute @end_user.can?(:view_deleted, Ticket)
    end

    describe "for agents with permission set" do
      before { @agent.stubs(:permission_set).returns(@permission_set) }

      describe "and permission to view deleted tickets" do
        before { @permission_set.permissions.enable(:view_deleted_tickets) }
        it "is allowed" do
          assert @agent.can?(:view_deleted, Ticket)
        end

        describe "without permission to ticket_deletion" do
          before { @permission_set.permissions.disable(:ticket_deletion) }

          it "is denied" do
            refute @agent.can?(:view_deleted, Ticket)
          end
        end
      end

      describe "and no permission to view deleted tickets" do
        before { @permission_set.permissions.disable(:view_deleted_tickets) }
        it "is denied" do
          refute @agent.can?(:view_deleted, Ticket)
        end

        describe "without permission to ticket_deletion" do
          before { @permission_set.permissions.disable(:ticket_deletion) }

          it "is denied" do
            refute @agent.can?(:view_deleted, Ticket)
          end
        end
      end
    end
  end

  describe "#edit_attribute_values?" do
    describe 'administrator' do
      it 'is allowed to edit attribute values' do
        assert users(:minimum_admin).can?(:edit_attribute_values, Ticket)
      end
    end

    describe 'agent' do
      let(:agent) { users(:minimum_agent) }

      before do
        agent.account.settings.stubs(:edit_ticket_skills_permission).
          returns(Zendesk::Accounts::SettingsControllerSupport::
            MANAGE_TICKET_SKILLS_PERMISSION[skills_permission])
      end

      describe 'when the edit ticket skills permission is editable by admins' do
        let(:skills_permission) { :EDITABLE_BY_ADMINS }

        it 'is not allowed to edit ticket attribute values' do
          refute agent.can?(:edit_attribute_values, Ticket)
        end
      end

      describe 'when the edit ticket skills permission is visible to agents' do
        let(:skills_permission) { :VISIBLE_TO_AGENTS }

        it 'is not allowed to edit ticket attribute values' do
          refute agent.can?(:edit_attribute_values, Ticket)
        end
      end

      describe 'when the edit ticket skills permission is hidden for all' do
        let(:skills_permission) { :HIDDEN_FOR_ALLi }

        it 'is not allowed to edit ticket attribute values' do
          refute agent.can?(:edit_attribute_values, Ticket)
        end
      end

      describe 'when the edit ticket skills permission is editable by all' do
        let(:skills_permission) { :EDITABLE_BY_ALL }

        describe 'and the agent is not a light agent' do
          it 'is allowed to edit ticket attribute values' do
            assert agent.can?(:edit_attribute_values, Ticket)
          end
        end

        describe 'and the agent is a light agent' do
          let(:agent) { create_light_agent }

          before do
            Account.any_instance.stubs(has_light_agents?: true)
          end

          it 'is not allowed to edit ticket attribute values' do
            refute agent.can?(:edit_attribute_values, Ticket)
          end
        end
      end
    end
  end
end
