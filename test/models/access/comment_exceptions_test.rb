require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::CommentExceptions' do
  fixtures :users, :accounts, :subscriptions, :tickets, :organizations, :groups

  describe 'Old school permissions' do
    before do
      @user = users(:minimum_agent)
      @ticket = tickets(:minimum_1)
    end

    subject { @user }

    describe 'Given a group restricted agent' do
      before do
        @user.restriction_id = RoleRestrictionType.Groups
        assert @user.agent_restriction?(:groups)
        assert @user.is_restricted_agent?
      end

      describe "Who is not following" do
        before do
          @ticket.group_id = groups(:minimum_group).id
          @user.groups.clear
          refute @ticket.is_follower?(@user)
        end

        should_not_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end

      describe "Who is following" do
        before do
          @ticket.set_collaborators = [@user.id]
          assert @ticket.is_follower?(@user)
        end

        should_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end

      describe "Who is in the same group as the ticket" do
        before do
          @ticket.group_id = groups(:minimum_group).id
          assert @user.in_group?(@ticket.group)
        end

        should_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end
    end

    describe 'Given an assignee restricted agent' do
      before do
        @user.restriction_id = RoleRestrictionType.Assigned
        assert @user.agent_restriction?(:assigned)
      end

      describe 'Who is assigned to a ticket' do
        before do
          assert_equal(@ticket.assignee, @user)
        end

        should_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end

      describe 'Who is not assigned to a ticket' do
        before do
          @ticket.assignee = users(:minimum_admin)
          @ticket.will_be_saved_by(@user)
          @ticket.save!
          assert_not_equal(@ticket.assignee, @user)
        end

        should_not_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end
    end

    describe 'Given an organization restricted agent' do
      before do
        @organization = organizations(:minimum_organization1)
        @ticket.organization_id = @organization.id

        @user.restriction_id = RoleRestrictionType.Organization
        assert @user.agent_restriction?(:organization)
      end

      describe "Who is in the requester's organization" do
        before do
          @user.update_attribute(:organization, @organization)
          assert_equal(@ticket.organization, @user.organization)
        end

        should_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end

      describe "Who is not in the requester's organization" do
        before do
          assert_not_equal(@ticket.organization, @user.organization)
        end

        should_not_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end
    end

    describe 'Given an end user' do
      before do
        @user = users(:minimum_end_user)
      end

      describe "on tickets requested by them" do
        before do
          @ticket.requester = @user
        end

        should_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end

      describe "on tickets not requested by them" do
        should_not_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end
    end
  end

  describe 'New hotness permissions' do
    before do
      @ticket = tickets(:minimum_1)
      @user = users(:minimum_agent)
      @user.permission_set = @user.account.permission_sets.create!(name: "Test")
      assert @user.account.stubs(:has_permission_sets?).returns(true)
    end

    subject { @user }

    describe 'Given a group restricted agent' do
      before do
        @user.permission_set.permissions.ticket_access = 'within-groups'
        assert @user.agent_restriction?(:groups)
        assert @user.is_restricted_agent?
      end

      describe "Who is following" do
        before do
          @ticket.set_collaborators = [@user.id]
          assert @ticket.is_follower?(@user)
        end

        should_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end
    end

    describe 'Given an assignee restricted agent' do
      before do
        @user.permission_set.permissions.ticket_access = 'assigned-only'
        assert @user.agent_restriction?(:assigned)
      end

      describe 'Who is assigned to a ticket' do
        before do
          assert_equal(@ticket.assignee, @user)
        end

        should_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end
    end

    describe 'Given an end user' do
      before do
        @user = users(:minimum_end_user)
      end

      describe "on tickets requested by them" do
        before do
          @ticket.requester = @user
        end

        should_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end

      describe "on tickets not requested by them" do
        should_not_be_able_to(:add_comment, 'to the ticket') { @ticket }
      end
    end
  end
end
