require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Twitter' do
  fixtures :users, :accounts, :subscriptions

  before do
    User.any_instance.stubs(:is_light_agent?).returns(false)
  end

  describe 'Old school permissions' do
    it 'allows agents to see the twitter search stream' do
      user = users(:minimum_agent)
      assert user.can?(:view, Channels::Twitter::Search)
    end

    it 'allows agents to edit twitter searches' do
      user = users(:minimum_agent)
      assert user.can?(:edit, Channels::Twitter::Search)
    end
  end

  describe 'New hotness permissions' do
    before do
      @user = users(:minimum_agent)
      @user.permission_set = @user.account.permission_sets.create!(name: "Test")
      @user.account.expects(:has_permission_sets?).returns(true)
    end

    describe 'Given an agent without access to view twitter searches' do
      before do
        @user.permission_set.permissions.disable(:view_twitter_searches)
        refute @user.permission_set.permissions.view_twitter_searches?
      end

      it 'is not is able to see twitter stuff' do
        refute @user.can?(:view, Channels::Twitter::Search)
      end
    end

    describe 'Given an agent with access to view twitter searches' do
      before do
        @user.permission_set.permissions.enable(:view_twitter_searches)
        assert @user.permission_set.permissions.view_twitter_searches?
      end

      it 'is able to see twitter stuff' do
        assert @user.can?(:view, Channels::Twitter::Search)
      end
    end

    describe 'Given an agent with access to manage settings' do
      before do
        @user.permission_set.permissions.enable(:extensions_and_channel_management)
        assert @user.permission_set.permissions.extensions_and_channel_management?
      end

      it 'is able to manage twitter searches' do
        assert @user.can?(:edit, Channels::Twitter::Search)
      end
    end

    describe 'Given an agent without access to manage settings' do
      before do
        @user.permission_set.permissions.disable(:extensions_and_channel_management)
        refute @user.permission_set.permissions.extensions_and_channel_management?
      end

      it 'not is able to manage twitter searches' do
        refute @user.can?(:edit, Channels::Twitter::Search)
      end
    end
  end

  describe 'Given an end user' do
    before { @user = users(:minimum_end_user) }

    it 'is not is able to view twitter stuff' do
      refute @user.can?(:view, Channels::Twitter::Search)
    end
  end

  describe 'Given an admin' do
    before { @user = users(:support_admin) }

    it 'is able to view twitter stuff' do
      assert @user.can?(:view, Channels::Twitter::Search)
    end
  end
end
