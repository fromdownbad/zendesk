require_relative "../../support/test_helper"
require_relative "../../support/satisfaction_test_helper"

SingleCov.covered! uncovered: 8

describe Access::Authorization do
  include SatisfactionTestHelper

  fixtures :accounts, :account_property_sets, :users, :groups, :memberships, :user_identities, :subscriptions, :payments, :sequences, :tickets, :events, :ticket_fields, :organizations

  describe '#can?' do
    let(:admin) { users(:minimum_admin) }

    it 'returns false on a nil object' do
      refute Access::Authorization.new(admin).can?(:view, nil)
    end
  end

  describe 'Listing forum/entry subscribers' do
    before do
      @admin = users(:minimum_admin)
      @agent = users(:minimum_agent)
    end

    it 'is available to admins' do
      assert Access::Authorization.new(@admin).can?(:list_subscribers, Entry)
      assert Access::Authorization.new(@admin).can?(:list_subscribers, Forum)
    end

    it 'is available to non-restricted moderators' do
      @agent.stubs(:is_moderator?).returns(true)
      @agent.stubs(:is_non_restricted_agent).returns(true)
      assert Access::Authorization.new(@agent).can?(:list_subscribers, Entry)
      assert Access::Authorization.new(@agent).can?(:list_subscribers, Forum)
    end

    it 'does not be available to non-moderators' do
      @agent.stubs(:is_moderator?).returns(false)
      @agent.stubs(:is_non_restricted_agent?).returns(true)
      refute Access::Authorization.new(@agent).can?(:list_subscribers, Entry)
      refute Access::Authorization.new(@agent).can?(:list_subscribers, Forum)
    end

    it 'does not be available to restricted moderators' do
      @agent.stubs(:is_moderator?).returns(true)
      @agent.stubs(:is_non_restricted_agent?).returns(false)
      refute Access::Authorization.new(@agent).can?(:list_subscribers, Entry)
      refute Access::Authorization.new(@agent).can?(:list_subscribers, Forum)
    end
  end

  describe 'using liquid in comments' do
    it 'is allowed for agents' do
      assert Access::Authorization.new(users(:minimum_agent)).can?(:use_liquid_in, Comment)
    end

    it 'is disallowed for end-users' do
      refute Access::Authorization.new(users(:minimum_end_user)).can?(:use_liquid_in, Comment)
    end
  end

  describe "a lookup on an account w/out permission sets" do
    it "incurs no database queries" do
      @agent  = users(:minimum_agent)
      @ticket = tickets(:minimum_1)
      assert_sql_queries 0 do
        @agent.can?(:view, @ticket)
      end
    end
  end
end
