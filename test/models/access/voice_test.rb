require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Voice' do
  fixtures :accounts, :account_property_sets, :users

  describe "Voice Abilities" do
    before do
      @agent = users(:minimum_agent)
      @permission_set = PermissionSet.new
      @permission_set.permissions.enable(:voice_availability_access)
    end

    describe "Voice availability" do
      describe "for end users" do
        it "is not is allowed" do
          end_user = users(:minimum_end_user)
          refute end_user.can?(:accept, ::Voice::Call)
        end
      end

      describe "for agents without permission sets" do
        it "is allowed" do
          assert @agent.can?(:accept, ::Voice::Call)
        end
      end

      describe "for agents with permission sets" do
        before do
          @agent.stubs(:permission_set).returns(@permission_set)
          @agent.account.stubs(:has_permission_sets?).returns(true)
          @agent.account.stubs(:has_light_agents?).returns(true)
        end

        describe "user_seats feature disabled" do
          it "is allowed if they have the voice_availability_access permission" do
            assert @agent.can?(:accept, ::Voice::Call)
          end

          it "is not is allowed if they don't have the voice_availability_access permission" do
            @permission_set.permissions.disable(:voice_availability_access)
            refute @agent.can?(:accept, ::Voice::Call)
          end

          it "is not allowed if they don't have the ticket_editing permission" do
            @permission_set.permissions.disable(:ticket_editing)
            refute @agent.can?(:accept, ::Voice::Call)
          end
        end

        describe "user_seats feature disabled" do
          before { Voice::FeatureMapper.any_instance.stubs(:feature_enabled?).with(:user_seats).returns(true) }

          it "is allowed if they have the voice_availability_access permission" do
            assert @agent.can?(:accept, ::Voice::Call)
          end

          it "is allowed if they don't have the voice_availability_access permission" do
            @permission_set.permissions.disable(:voice_availability_access)
            assert @agent.can?(:accept, ::Voice::Call)
          end

          it "is not allowed if they don't have the ticket_editing permission" do
            @permission_set.permissions.disable(:ticket_editing)
            refute @agent.can?(:accept, ::Voice::Call)
          end
        end
      end
    end

    describe "API resources" do
      before do
        @resources = [Voice::PhoneNumber, Voice::Core::CallStat]
      end

      describe "end users" do
        it "is restricted from reading and writing" do
          end_user = users(:minimum_end_user)
          @resources.each do |resource|
            refute end_user.can?(:read, resource)
            refute end_user.can?(:write, resource)
          end
        end
      end

      describe "agent with permission set" do
        it "allows reading, reject writing" do
          account = accounts(:minimum)
          permission_set = PermissionSet.new(account: account, name: "things")
          permission_set.save!

          permission_set.users.create! do |user|
            user.name = "Foo"
            user.email = "foo@bar.org"
            user.roles = Role::AGENT.id
            user.account = account
          end

          @resources.each do |resource|
            assert @agent.can?(:read, resource)
            refute @agent.can?(:write, resource)
          end
        end
      end

      describe "agents" do
        it "is allowed to read" do
          @resources.each do |resource|
            assert @agent.can?(:read, resource)
          end
        end

        it "is restricted from writing" do
          @resources.each do |resource|
            refute @agent.can?(:write, resource)
          end
        end
      end

      describe "admins" do
        before do
          @admin = users(:minimum_admin)
        end

        it "is allowed to read" do
          @resources.each do |resource|
            assert @admin.can?(:read, resource)
          end
        end

        it "is allowed to write" do
          @resources.each do |resource|
            assert @admin.can?(:write, resource)
          end
        end
      end
    end
  end

  describe "Light Voice Abilities" do
    before do
      @agent = users(:minimum_agent)
      @permission_set = PermissionSet.new
      @permission_set.permissions.enable(:voice_availability_access)
    end

    describe "Voice availability" do
      describe "for end users" do
        it "is not is allowed" do
          end_user = users(:minimum_end_user)
          voice_ability = VoiceAbility.new(end_user)
          refute voice_ability.can?(:accept, ::Voice::Call)
        end
      end

      describe "for agents without permission sets" do
        it "is allowed" do
          @voice_ability = VoiceAbility.new(@agent)
          assert @voice_ability.can?(:accept, ::Voice::Call)
        end
      end

      describe "for agents with permission sets" do
        before do
          @agent.stubs(:permission_set).returns(@permission_set)
          @agent.account.stubs(:has_permission_sets?).returns(true)
        end

        it "is allowed if they have the voice_availability_access permission" do
          @voice_ability = VoiceAbility.new(@agent)
          assert @voice_ability.can?(:accept, ::Voice::Call)
        end

        it "is not is allowed if they don't have the voice_availability_access permission" do
          @permission_set.permissions.disable(:voice_availability_access)
          @voice_ability = VoiceAbility.new(@agent)
          refute @voice_ability.can?(:accept, ::Voice::Call)
        end
      end
    end
  end
end
