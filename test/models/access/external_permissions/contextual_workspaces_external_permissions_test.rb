require_relative '../../../support/test_helper'

SingleCov.covered!

describe "Access::ExternalPermissions::ContextualWorkspacesExternalPermissions" do
  fixtures :users

  describe "manage contextual workspaces" do
    describe "with permission" do
      before do
        Access::ExternalPermissions::ContextualWorkspacesExternalPermissions.any_instance.stubs(:allow?).
          with([Access::ExternalPermissions::ContextualWorkspacesExternalPermissions::CONTEXTUAL_WORKSPACES_MANAGE]).returns(true)
      end

      it 'is able to manage contextual workspaces' do
        assert users(:minimum_admin).can?(:manage, Workspace)
      end
    end

    describe "without permission" do
      before do
        Access::ExternalPermissions::ContextualWorkspacesExternalPermissions.any_instance.stubs(:allow?).
          with([Access::ExternalPermissions::ContextualWorkspacesExternalPermissions::CONTEXTUAL_WORKSPACES_MANAGE]).returns(false)
      end
      it 'is not able to manage contextual workspaces' do
        refute users(:minimum_agent).can?(:manage, Workspace)
      end
    end
  end
end
