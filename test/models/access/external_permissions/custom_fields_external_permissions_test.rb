require_relative '../../../support/test_helper'

SingleCov.covered!

describe "Access::ExternalPermissions::CustomFieldsExternalPermissions" do
  fixtures :users

  describe "manage user fields" do
    describe "with permission" do
      before do
        Access::ExternalPermissions::CustomFieldsExternalPermissions.any_instance.stubs(:allow?).
          with([Access::ExternalPermissions::CustomFieldsExternalPermissions::USER_FIELDS_MANAGE]).returns(true)
      end

      it 'is able to manage user fields' do
        assert users(:minimum_admin).can?(:manage_user_fields, CustomField::Field)
      end
    end

    describe "without permission" do
      before do
        Access::ExternalPermissions::CustomFieldsExternalPermissions.any_instance.stubs(:allow?).
          with([Access::ExternalPermissions::CustomFieldsExternalPermissions::USER_FIELDS_MANAGE]).returns(false)
      end
      it 'is not able to manage user fields' do
        refute users(:minimum_agent).can?(:manage_user_fields, CustomField::Field)
      end
    end
  end
end
