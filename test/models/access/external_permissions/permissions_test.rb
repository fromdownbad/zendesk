require_relative '../../../support/test_helper'

SingleCov.covered!

describe "Access::ExternalPermissions::Permissions" do
  fixtures :users

  let(:permissions_agent_client) { stub }
  let(:admin) { users(:minimum_admin) }
  let(:account) { { id: admin.account.id, shard_id: 1 } }
  let(:user) { { id: admin.id, role: admin.roles, permission_set_id: nil } }
  let(:resource_scopes) { ["tickets:read"] }
  let(:request) { { input: { account: account, user: user, resource_scopes: resource_scopes } } }
  let(:response) { { "result" => true } }

  let(:permissions) do
    Access::ExternalPermissions::Permissions.new(admin)
  end

  before(:all) do
    Zendesk::ExternalPermissions::PermissionsAgentClient.stubs(:new).returns(permissions_agent_client)
    permissions_agent_client.stubs(:request).with(account, user, resource_scopes).returns(request)
    permissions_agent_client.stubs(:allow!).with(request).returns(stub(body: response))
  end

  describe 'allow?' do
    it 'makes call and returns permission' do
      assert_equal permissions.allow?(resource_scopes), true
    end
  end
end
