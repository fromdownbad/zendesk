require_relative '../../../support/test_helper'

SingleCov.covered!

describe "Access::ExternalPermissions::External" do
  before do
    @policy = Access::ExternalPermissions::ExternalPolicy.new({"id" => "01ENTMMK5RMDD0BEFH5R4DSMN5", "name" => "TestRole", "subject" => {"id" => "10111", "type" => "custom_role"}, "statements" => [{"effect" => "allow", "resource" => "user_fields", "scopes" => ["manage"]}] })
  end

  describe 'policy' do
    it '#get' do
      assert_equal @policy.id, "01ENTMMK5RMDD0BEFH5R4DSMN5"
      statements = @policy.statements[0]
      assert_equal statements.effect, "allow"
      assert_equal statements.resource, "user_fields"
      assert_equal statements.scopes, ["manage"]
    end
  end
end
