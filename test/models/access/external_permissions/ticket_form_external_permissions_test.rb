require_relative '../../../support/test_helper'

SingleCov.covered!

describe "Access::ExternalPermissions::TicketFormExternalPermissions" do
  fixtures :users

  describe "manage ticket forms" do
    describe "with permission" do
      before do
        Access::ExternalPermissions::TicketFormExternalPermissions.any_instance.stubs(:allow?).
          with([Access::ExternalPermissions::TicketFormExternalPermissions::TICKET_FORMS_MANAGE]).returns(true)
      end

      it 'is able to manage ticket forms' do
        assert users(:minimum_admin).can?(:manage, TicketForm)
      end
    end

    describe "without permission" do
      before do
        Access::ExternalPermissions::TicketFormExternalPermissions.any_instance.stubs(:allow?).
          with([Access::ExternalPermissions::TicketFormExternalPermissions::TICKET_FORMS_MANAGE]).returns(false)
      end
      it 'is not able to manage ticket forms' do
        refute users(:minimum_agent).can?(:manage, TicketForm)
      end
    end
  end
end
