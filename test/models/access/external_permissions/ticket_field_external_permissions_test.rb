require_relative '../../../support/test_helper'

SingleCov.covered!

describe "Access::ExternalPermissions::TicketFieldExternalPermissions" do
  fixtures :users

  describe "manage ticket fields" do
    describe "with permission" do
      before do
        Access::ExternalPermissions::TicketFieldExternalPermissions.any_instance.stubs(:allow?).
          with([Access::ExternalPermissions::TicketFieldExternalPermissions::TICKET_FIELDS_MANAGE]).returns(true)
      end

      it 'is able to manage ticket fields' do
        assert users(:minimum_admin).can?(:manage, TicketField)
      end
    end

    describe "without permission" do
      before do
        Access::ExternalPermissions::TicketFieldExternalPermissions.any_instance.stubs(:allow?).
          with([Access::ExternalPermissions::TicketFieldExternalPermissions::TICKET_FIELDS_MANAGE]).returns(false)
      end

      it 'is not able to manage ticket fields' do
        refute users(:minimum_agent).can?(:manage, TicketField)
      end
    end
  end
end
