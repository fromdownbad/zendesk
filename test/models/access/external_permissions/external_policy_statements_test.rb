require_relative '../../../support/test_helper'

SingleCov.covered!

describe "Access::ExternalPermissions::ExternalPolicyStatements" do
  before do
    @statements = Access::ExternalPermissions::ExternalPolicyStatements.new({ "effect" => "allow", "resource" => "user_fields", "scopes" => ["manage"] })
  end

  describe 'statements' do
    it '#get' do
      assert_equal @statements.effect, "allow"
      assert_equal @statements.resource, "user_fields"
      assert_equal @statements.scopes, ["manage"]
    end
  end
end
