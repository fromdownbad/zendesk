require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Group' do
  fixtures :users, :groups, :organizations, :accounts, :subscriptions

  describe "Without permission sets" do
    before do
      Account.any_instance.stubs(:has_permission_sets?).returns(false)
    end

    describe "can?(:edit, group)" do
      before { @group = groups(:minimum_group) }
      describe "for admins" do
        before { @user = users(:minimum_admin) }
        it "is allowed" do
          assert @user.can?(:edit, @group)
        end
      end

      describe "for regular agents" do
        before { @user = users(:minimum_agent) }
        it "is denied" do
          refute @user.can?(:edit, @group)
        end
      end

      describe "for endusers" do
        before { @user = users(:minimum_end_user) }
        it "is denied" do
          refute @user.can?(:edit, @group)
        end
      end
    end
  end

  describe "With permission sets" do
    before do
      Account.any_instance.expects(:has_permission_sets?).returns(true).at_least_once
    end

    describe "can?(:edit, organization)" do
      before do
        @user = users(:minimum_agent)
        @user.permission_set = @user.account.permission_sets.create! name: "Foo"
      end
      it "defers to the agent's permission set (true) " do
        @user.permission_set.permissions.expects(:edit_organizations?).returns(true).at_least_once
        assert @user.abilities(:reload).can?(:edit, @user.account.organizations.first)
        assert @user.abilities(:reload).can?(:edit_notes, @user.account.organizations.first)
      end
      it "defers to the agent's permission set (false) " do
        @user.permission_set.permissions.expects(:edit_organizations?).returns(false).at_least_once
        refute @user.abilities(:reload).can?(:edit, @user.account.organizations.first)
        refute @user.abilities(:reload).can?(:edit_notes, @user.account.organizations.first)
      end
    end

    describe "can?(:edit, group)" do
      before do
        @user = users(:minimum_agent)
        @user.permission_set = @user.account.permission_sets.create! name: "Foo"
      end
      it "defers to the agent's permission set (true) " do
        @user.permission_set.permissions.expects(:edit_organizations?).returns(true).at_least_once
        assert @user.abilities(:reload).can?(:edit, @user.account.groups.first)
      end
      it "defers to the agent's permission set (false) " do
        @user.permission_set.permissions.expects(:edit_organizations?).returns(false).at_least_once
        refute @user.abilities(:reload).can?(:edit, @user.account.groups.first)
      end
    end
  end
end
