require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::PermissionSet' do
  fixtures :users, :groups, :organizations, :accounts, :subscriptions

  describe "PermissionSet Abilities" do
    before do
      @account = accounts(:minimum)
      @admin = users(:minimum_admin)
    end

    describe "custom permission set" do
      before { @custom = PermissionSet.create(account: @account, name: "things") }

      it "is editable and deletable" do
        assert @admin.can?(:edit, @custom)
        assert @admin.can?(:delete, @custom)
      end
    end

    describe "new custom permission set" do
      before { @custom = PermissionSet.new(account: @account, name: "things") }

      it "is editable but no deletable" do
        assert @admin.can?(:edit, @custom)
        refute @admin.can?(:delete, @custom)
      end
    end

    describe "light agent permission set" do
      before { @light_agent = PermissionSet.create_light_agent!(@account) }

      it "is editable but no deletable" do
        assert @admin.can?(:edit, @light_agent)
        refute @admin.can?(:delete, @light_agent)
      end
    end
  end
end
