require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Access::Chat' do
  fixtures :accounts, :account_property_sets, :users

  describe "Chat Abilities" do
    before do
      @agent = users(:minimum_agent)
      @permission_set = PermissionSet.new
      @permission_set.permissions.enable(:chat_availability_access)
    end

    describe "Chat availability" do
      describe "for end users" do
        it "is not is allowed" do
          end_user = users(:minimum_end_user)
          refute end_user.can?(:accept, Access::Chat::IncomingChats)
        end
      end

      describe "for agents without permission sets" do
        before { @agent.stubs(:permission_set).returns(@permission_set) }
        it "is allowed" do
          assert @agent.can?(:accept, Access::Chat::IncomingChats)
        end
      end

      describe "for agents with permission sets" do
        before do
          @agent.account.stubs(:has_permission_sets?).returns(true)
          @agent.stubs(:permission_set).returns(@permission_set)
        end

        it "is allowed if they have the chat_availability_access permission" do
          assert @agent.can?(:accept, Access::Chat::IncomingChats)
        end

        it "is not is allowed if they don't have the chat_availability_access permission" do
          @permission_set.permissions.disable(:chat_availability_access)
          refute @agent.abilities(:reload).can?(:accept, Access::Chat::IncomingChats)
        end

        it "is not is allowed if they are light agents" do
          @permission_set.permissions.disable(:ticket_editing)
          @permission_set.role_type = 1
          refute @agent.can?(:accept, ::Voice::Call)
        end
      end
    end
  end
end
