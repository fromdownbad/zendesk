require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Access::Settings' do
  fixtures :users, :accounts, :subscriptions, :rules, :targets

  describe 'Legacy permissions' do
    describe 'An end user' do
      subject { users(:minimum_end_user) }

      should_not_be_able_to(:edit, 'extension settings') { Access::Settings::Extensions }
      should_not_be_able_to(:edit, 'business rule settings') { Access::Settings::BusinessRules }
    end

    describe 'An agent' do
      subject { users(:support_agent) }

      should_not_be_able_to(:edit, 'extension settings') { Access::Settings::Extensions }
      should_not_be_able_to(:edit, 'business rule settings') { Access::Settings::BusinessRules }
    end

    describe 'Given an admin' do
      before { @user = users(:minimum_admin) }
      subject { @user }

      should_be_able_to(:edit, 'extension settings') { Access::Settings::Extensions }
      should_be_able_to(:edit, 'business rule settings') { Access::Settings::BusinessRules }

      it 'is able to edit Triggers and Automations' do
        assert @user.can?(:edit, rules(:trigger_notify_requester_of_received_request))
        assert @user.can?(:edit, rules(:automation_close_ticket))
        assert @user.can?(:edit, Trigger)
        assert @user.can?(:edit, Automation)
        assert @user.can?(:edit, Target)
      end
    end

    describe 'Given non-admins' do
      before do
        @agent = users(:with_groups_agent1)
        @end_user = users(:with_groups_end_user)
        @restricted_agent = users(:with_groups_agent_groups_restricted)
      end

      describe "Edit triggers/automations" do
        it("be denied for end users") do
          refute @end_user.can?(:edit, Trigger)
          refute @end_user.can?(:edit, Automation)
          refute @end_user.can?(:edit, Target)
        end

        it("be denied for restricted agents") do
          refute @restricted_agent.can?(:edit, Trigger)
          refute @restricted_agent.can?(:edit, Automation)
          refute @end_user.can?(:edit, Target)
        end

        it("be denied for non restricted agents") do
          refute @agent.can?(:edit, Trigger)
          refute @agent.can?(:edit, Automation)
          refute @end_user.can?(:edit, Target)
        end

        describe "for agents with permission sets" do
          before do
            @permission_set = PermissionSet.new
            @permission_set.permissions.set(
              macro_access: 'full',
              view_access: 'full',
              report_access: 'full'
            )
            @agent.stubs(permission_set: @permission_set)
          end

          it("be denied for agents with full access to views/macros") do
            refute @agent.can?(:edit, Trigger)
            refute @agent.can?(:edit, Automation)
            refute @end_user.can?(:edit, Target)
          end
        end
      end
    end
  end

  describe 'Enterprise permissions' do
    before do
      @user = users(:minimum_agent)
      @user.permission_set = @user.account.permission_sets.create!(name: "Test")

      @user.account.stubs(:has_permission_sets?).returns(true)
    end

    subject { @user }

    describe 'Given an agent without access to edit extension settings' do
      before do
        @user.permission_set.permissions.disable(:extensions_and_channel_management)
        refute @user.permission_set.permissions.extensions_and_channel_management?
      end

      should_not_be_able_to(:edit, 'extension settings') { Access::Settings::Extensions }

      it 'is not is able to edit Targets' do
        refute @user.can?(:edit, Target)
      end
    end

    describe 'Given an agent with access to edit extension settings' do
      before do
        @user.permission_set.permissions.enable(:extensions_and_channel_management)
        assert @user.permission_set.permissions.extensions_and_channel_management?
      end

      should_be_able_to(:edit, 'twitter stuff') { Access::Settings::Extensions }

      it 'is able to edit Targets' do
        assert @user.can?(:edit, Target)
      end
    end

    describe 'Given an agent without access to edit business rule settings' do
      before do
        @user.permission_set.permissions.disable(:business_rule_management)
        refute @user.permission_set.permissions.business_rule_management?
      end

      should_not_be_able_to(:edit, 'business rule settings') { Access::Settings::BusinessRules }

      it 'not is able to edit Triggers and Automations' do
        refute @user.can?(:edit, rules(:trigger_notify_requester_of_received_request))
        refute @user.can?(:edit, rules(:automation_close_ticket))
      end
    end

    describe 'Given an agent with access to edit business rules settings' do
      before do
        @user.permission_set.permissions.enable(:business_rule_management)
        assert @user.permission_set.permissions.business_rule_management?
      end

      should_be_able_to(:edit, 'business rule settings') { Access::Settings::BusinessRules }

      it 'is able to edit Triggers and Automations' do
        assert @user.can?(:edit, rules(:trigger_notify_requester_of_received_request))
        assert @user.can?(:edit, rules(:automation_close_ticket))
      end
    end
  end
end
