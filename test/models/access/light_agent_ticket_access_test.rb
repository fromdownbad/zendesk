require_relative "../../support/test_helper"
require_relative "../../support/collaboration_settings_test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'LightAgentTicketAccess' do
  fixtures :accounts, :tickets, :users

  describe 'A light agent' do
    before do
      CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
      @ticket = tickets(:minimum_1)
      @user = users(:minimum_agent)
      permission_set = PermissionSet.initialize_light_agent(accounts(:minimum))
      @user.stubs(:permission_set).returns(permission_set)
      @user.stubs(:has_permission_set?).returns(true)
    end

    it 'is able to edit and view new tickets' do
      t = Ticket.new
      assert @user.can?(:edit, t)
      assert @user.can?(:edit_properties, t)
    end

    describe "with a ticket belonging to another group" do
      before { @user.stubs(:in_group?).returns(false) }

      it 'is is not be able to edit that ticket' do
        refute @user.can?(:edit, @ticket)
      end

      it 'is not is able to view that ticket' do
        refute @user.can?(:view, @ticket)
      end

      it 'is able to edit a ticket and the properties if he is the requester' do
        @ticket.stubs(:requester_id).returns(@user.id)

        assert @user.can?(:edit, @ticket)
        assert @user.can?(:edit_properties, @ticket)
        refute @user.can?(:only_create_ticket_comments, @ticket)
      end

      describe "when collaborating on the ticket" do
        before { @ticket.stubs(:is_collaborator?).with(@user).returns(true) }

        it "is able to edit the ticket" do
          assert @user.can?(:edit, @ticket)
        end

        it "not is able to edit the ticket's properties" do
          refute @user.can?(:edit_properties, @ticket)
        end

        it "only reacts be able to leave ticket comments" do
          assert @user.can?(:only_create_ticket_comments, @ticket)
        end
      end

      describe "when can_assign_to_any_group is enabled" do
        before do
          @user.abilities.stubs(:defer?).with(:assign_to_any_group, @ticket).returns(true)
          @user.stubs(:in_group?).with(100).returns(true)
        end

        it "is able to view the ticket if the agent is in the previous group" do
          @ticket.stubs(:group_id_was).returns(100)
          assert @user.can?(:view, @ticket)
        end

        it "is not be able to view or edit the ticket if the agent is is not the previous group" do
          refute @user.can?(:view, @ticket)
        end
      end
    end

    describe "with a ticket belonging to one of the light agent's groups" do
      before { @user.stubs(:in_group?).returns(true) }

      it 'is is not be able to edit that ticket' do
        refute @user.can?(:edit, @ticket)
      end

      it 'is able to view that ticket' do
        assert @user.can?(:view, @ticket)
      end

      it 'is able to edit a ticket and the properties if he is the requester' do
        @ticket.stubs(:requester_id).returns(@user.id)

        assert @user.can?(:edit, @ticket)
        assert @user.can?(:edit_properties, @ticket)
        refute @user.can?(:only_create_ticket_comments, @ticket)
      end

      describe "when collaborating on the ticket" do
        before { @ticket.stubs(:is_collaborator?).with(@user).returns(true) }

        it "is able to edit the ticket" do
          assert @user.can?(:edit, @ticket)
        end

        it "not is able to edit the ticket's properties" do
          refute @user.can?(:edit_properties, @ticket)
        end

        it "not only reacts be able to leave ticket comments" do
          # This is kind of odd, but it be inconsequential.
          # The important thing is that the :edit_properties ability is negative
          # for (user, ticket) pairs in this state.
          refute @user.can?(:only_create_ticket_comments, @ticket)
        end
      end
    end
  end
end
