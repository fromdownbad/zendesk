require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::OrganizationMembership' do
  fixtures :accounts, :users, :organization_memberships

  [true, false].each do |permission_sets|
    describe "OrganizationMembership access with#{"out" unless permission_sets} permission sets" do
      let(:organization_membership) { organization_memberships(:minimum) }
      let(:admin_user) { users(:minimum_admin) }
      let(:agent_user) { users(:minimum_agent) }
      let(:end_user)   { users(:minimum_end_user) }

      before { Account.any_instance.stubs(:has_permission_sets?).returns(permission_sets) }

      describe "can?(:edit, organization_membership)" do
        it "is allowed for admins" do
          assert admin_user.can?(:edit, organization_membership)
        end

        it "is allowed for regular agents" do
          assert agent_user.can?(:edit, organization_membership)
        end

        it "is denied for end users" do
          refute end_user.can?(:edit, organization_membership)
        end
      end

      describe "can?(:view, organization_membership)" do
        it "is allowed for admins" do
          assert admin_user.can?(:view, organization_membership)
        end

        it "is allowed for regular agents" do
          assert agent_user.can?(:view, organization_membership)
        end

        describe "for end users" do
          it "is allowed for their organization_memisrships" do
            assert end_user.can?(:view, organization_membership)
          end

          it "is denied for other organization_memisrships" do
            refute end_user.can?(:view, organization_memberships(:minimum2))
          end
        end
      end
    end
  end
end
