require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Membership' do
  fixtures :accounts, :users, :memberships

  [true, false].each do |permission_sets|
    describe "Membership access with#{"out" unless permission_sets} permission sets" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(permission_sets)
        @membership = memberships(:minimum)
      end

      describe "can?(:edit, membership)" do
        describe "for admins" do
          before { @user = users(:minimum_admin) }

          it "is allowed" do
            assert @user.can?(:edit, @membership)
          end
        end

        describe "for regular agents" do
          before { @user = users(:minimum_agent) }

          it "is denied" do
            refute @user.can?(:edit, @membership)
          end
        end

        describe "for endusers" do
          before { @user = users(:minimum_end_user) }

          it "is denied" do
            refute @user.can?(:edit, @membership)
          end
        end
      end

      describe "can?(:view, membership)" do
        describe "for admins" do
          before { @user = users(:minimum_admin) }

          it "is allowed" do
            assert @user.can?(:view, @membership)
          end
        end

        describe "for regular agents" do
          before { @user = users(:minimum_agent) }

          it "is allowed" do
            assert @user.can?(:view, @membership)
          end
        end

        describe "for endusers" do
          before { @user = users(:minimum_end_user) }

          it "is denied" do
            refute @user.can?(:view, @membership)
          end
        end
      end
    end
  end
end
