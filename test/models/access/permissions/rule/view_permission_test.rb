require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Access::Permissions::Rule::ViewPermission do
  include TestSupport::Rule::Helper

  fixtures :all

  let(:user)    { users(:with_groups_agent1) }
  let(:account) { user.account }

  let(:associated_group)   { user.groups.first }
  let(:unassociated_group) { groups(:minimum_group) }

  let(:permission_set) { build_valid_permission_set }

  let(:owner) { account }

  let(:view) { create_view(owner: owner) }

  before do
    Rule.destroy_all

    Account.any_instance.stubs(has_permission_sets?: true)

    user.stubs(permission_set: permission_set)

    permission_set.permissions.view_access = view_access
  end

  describe '#edit?' do
    describe 'when the user has `full` access' do
      let(:view_access) { 'full' }

      it 'is allowed' do
        assert user.can?(:edit, view)
      end
    end

    describe 'when the user has `manage-group` access' do
      let(:view_access) { 'manage-group' }

      describe 'and the view is user owned' do
        let(:owner) { user }

        it 'is allowed' do
          assert user.can?(:edit, view)
        end
      end

      describe 'and the view is account owned' do
        let(:owner) { account }

        it 'is not allowed' do
          refute user.can?(:edit, view)
        end
      end

      describe 'and the view is group owned' do
        describe_with_arturo_enabled :multiple_group_views_reads do
          before(:each) do
            GroupView.destroy_all

            view.owner_type = 'Group'
          end

          describe 'by one group' do
            let(:view_group) do
              GroupView.new do |group_view|
                group_view.account_id = account.id
                group_view.group_id   = group.id
                group_view.view_id    = view.id
              end
            end

            describe 'on view creation' do
              let(:group) { associated_group }

              before do
                view.group_owner_ids = [group.id]
              end

              it 'is allowed' do
                assert user.can?(:edit, view)
              end

              describe 'and the group is not in view `group_owner_ids`' do
                before do
                  view.group_owner_ids = [unassociated_group.id]
                  view.owner = user.account
                end

                it 'is not allowed' do
                  refute user.can?(:edit, view)
                end

                describe 'and `owner` is set to group' do
                  before do
                    view.owner = group
                  end

                  it 'is not allowed' do
                    refute user.can?(:edit, view)
                  end
                end
              end

              describe 'and the group is only in one of the view `group_owner_ids`' do
                before do
                  view.group_owner_ids = [associated_group.id, unassociated_group.id]
                  view.owner = account
                end

                it 'is not allowed' do
                  refute user.can?(:edit, view)
                end

                describe 'and `owner` is set to group' do
                  before do
                    view.owner = group
                  end

                  it 'is not allowed' do
                    refute user.can?(:edit, view)
                  end
                end
              end

              describe 'and the owner is account' do
                before do
                  view.group_owner_ids = nil
                  view.owner = account
                end

                it 'is not allowed' do
                  refute user.can?(:edit, view)
                end

                describe 'but `owner` is changed to group' do
                  before do
                    view.owner = group
                  end

                  it 'is allowed' do
                    assert user.can?(:edit, view)
                  end
                end
              end
            end

            describe 'on view update' do
              before(:each) do
                view_group.save!
              end

              describe 'and the user is a member of the group' do
                let(:group) { associated_group }

                it 'is allowed' do
                  assert user.can?(:edit, view)
                end
              end

              describe 'and the user is not a member of the group' do
                let(:group) { unassociated_group }

                it 'is not allowed' do
                  refute user.can?(:edit, view)
                end
              end
            end
          end

          describe 'by multiple groups' do
            before do
              [associated_group, unassociated_group].each do |group|
                GroupView.create! do |group_view|
                  group_view.account_id = account.id
                  group_view.group_id = group.id
                  group_view.view_id = view.id
                end
              end
            end

            it 'is not allowed' do
              refute user.can?(:edit, view)
            end
          end
        end

        describe_with_arturo_disabled :multiple_group_views_reads do
          describe 'and the user is a member of the group' do
            let(:owner) { associated_group }

            it 'is allowed' do
              assert user.can?(:edit, view)
            end
          end

          describe 'and the user is not a member of the group' do
            let(:owner) { unassociated_group }

            it 'is not allowed' do
              refute user.can?(:edit, view)
            end
          end

          describe 'and it is owned by multiple groups' do
            before do
              [associated_group, unassociated_group].each do |group|
                GroupView.create! do |group_view|
                  group_view.account_id = account.id
                  group_view.group_id = group.id
                  group_view.view_id = view.id
                end
              end
            end

            it 'is not allowed' do
              refute user.can?(:edit, view)
            end
          end
        end
      end
    end

    describe 'when the user has `manage-personal` access' do
      let(:view_access) { 'manage-personal' }

      describe 'and the user owns the view' do
        let(:owner) { user }

        it 'is allowed' do
          assert user.can?(:edit, view)
        end
      end

      describe 'and the user does not own the view' do
        let(:owner) { account }

        it 'is not allowed' do
          refute user.can?(:edit, view)
        end
      end
    end

    %w[readonly playonly].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:view_access) { permission }

        describe 'and the user owns the view' do
          let(:owner) { user }

          it 'is not allowed' do
            refute user.can?(:edit, view)
          end
        end

        describe 'and the user does not own the view' do
          let(:owner) { account }

          it 'is not allowed' do
            refute user.can?(:edit, view)
          end
        end
      end
    end
  end

  describe '#view?' do
    %w[full manage-group manage-personal readonly].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:view_access) { permission }

        it 'is allowed' do
          assert user.can?(:view, View)
        end
      end
    end

    describe 'when the user has `playonly` access' do
      let(:view_access) { 'playonly' }

      it 'is not allowed' do
        refute user.can?(:view, View)
      end
    end
  end

  describe '#manage_personal?' do
    %w[full manage-group manage-personal].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:view_access) { permission }

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is allowed' do
            assert user.can?(:manage_personal, View)
          end
        end

        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, View)
          end
        end
      end
    end

    %w[readonly playonly].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:view_access) { permission }

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, View)
          end
        end

        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, View)
          end
        end
      end
    end
  end

  describe '#manage_group?' do
    %w[full manage-group].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:view_access) { permission }

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is allowed' do
            assert user.can?(:manage_group, View)
          end
        end

        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, View)
          end
        end
      end
    end

    %w[manage-personal readonly playonly].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:view_access) { permission }

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is not allowed' do
            refute user.can?(:manage_group, View)
          end
        end

        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, View)
          end
        end
      end
    end
  end

  describe '#manage_shared?' do
    describe 'when the user has `full` access' do
      let(:view_access) { 'full' }

      it 'is allowed' do
        assert user.can?(:manage_shared, View)
      end
    end

    %w[manage-group manage-personal readonly playonly].each do |permission|
      describe "when the user has less than `#{permission}` access" do
        let(:view_access) { permission }

        it 'is not allowed' do
          refute user.can?(:manage_shared, View)
        end
      end
    end
  end
end
