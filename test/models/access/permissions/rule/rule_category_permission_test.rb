require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Access::Permissions::Rule::RuleCategoryPermission do
  include TestSupport::Rule::Helper

  fixtures :all

  let(:agent) { users(:with_groups_agent1) }
  let(:admin) { users(:with_groups_admin) }

  let(:user)    { agent }
  let(:account) { user.account }

  let(:permission_set) { build_valid_permission_set }

  let(:trigger_category) { create_category }

  before do
    Account.any_instance.stubs(has_permission_sets?: true)

    user.stubs(permission_set: permission_set)
  end

  describe '#edit?' do
    before do
      permission_set.permissions.business_rule_management = rule_management
    end

    describe 'when the user can manage business rules' do
      let(:rule_management) { true }

      it 'is allowed' do
        assert user.can?(:edit, trigger_category)
      end
    end

    describe 'when the user cannot manage business rules' do
      let(:rule_management) { false }

      it 'is not allowed' do
        refute user.can?(:edit, trigger_category)
      end
    end
  end

  describe '#view?' do
    before do
      permission_set.permissions.business_rule_management = rule_management

      Account.any_instance.stubs(has_unlimited_triggers?: false)
    end

    describe 'when the user can manage business rules' do
      let(:rule_management) { true }

      it 'is allowed' do
        assert user.can?(:view, trigger_category)
      end
    end

    describe 'when the user cannot manage business rules' do
      let(:rule_management) { false }

      it 'is not allowed' do
        refute user.can?(:view, trigger_category)
      end
    end
  end
end
