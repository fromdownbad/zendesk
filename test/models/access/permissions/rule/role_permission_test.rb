require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Access::Permissions::Rule::RolePermission do
  fixtures :all

  let(:user) { users(:minimum_agent) }

  let(:permission) { nil }
  let(:group_edit) { false }

  let(:owner)      { nil }
  let(:owner_type) { nil }

  let(:rule) { stub('rule', owner: owner, owner_type: owner_type) }

  let(:described_class) { Access::Permissions::Rule::RolePermission }

  let(:role_permission_class) do
    Class.new(described_class) do
      def initialize(user, permission, group_edit)
        super(user)

        @permission = permission
        @group_edit = group_edit
      end

      protected

      attr_reader :permission

      private

      def group_edit?(*)
        @group_edit
      end
    end
  end

  let(:role_permission) do
    role_permission_class.new(user, permission, group_edit)
  end

  describe '#edit?' do
    describe 'when the permission is `full`' do
      let(:permission) { 'full' }

      it 'returns true' do
        assert role_permission.edit?(rule)
      end
    end

    describe 'when the permission is `manage-group`' do
      let(:permission) { 'manage-group' }

      describe 'and the user owns the rule' do
        let(:owner) { user }

        it 'returns true' do
          assert role_permission.edit?(rule)
        end
      end

      describe 'and the rule is not scoped to groups' do
        let(:owner_type) { 'Account' }

        it 'returns false' do
          refute role_permission.edit?(rule)
        end
      end

      describe 'and the rule is scoped to groups' do
        let(:owner_type) { 'Group' }

        describe 'and the user can edit the rule' do
          let(:group_edit) { true }

          it 'returns true' do
            assert role_permission.edit?(rule)
          end
        end

        describe 'and the user cannot edit the rule' do
          let(:group_edit) { false }

          it 'returns false' do
            refute role_permission.edit?(rule)
          end
        end
      end
    end

    describe 'when the permission is `manage-personal`' do
      let(:permission) { 'manage-personal' }

      describe 'and the user owns the rule' do
        let(:owner) { user }

        it 'returns true' do
          assert role_permission.edit?(rule)
        end
      end

      describe 'and the user does not own the rule' do
        let(:owner) { user.account }

        it 'returns false' do
          refute role_permission.edit?(rule)
        end
      end
    end

    describe 'when the permission is a different value' do
      let(:permission) { 'readonly' }

      it 'returns false' do
        refute role_permission.edit?(rule)
      end
    end
  end

  describe '#manage_personal?' do
    %w[full manage-group manage-personal].each do |permission|
      describe "when the permission is `#{permission}`" do
        let(:permission) { permission }

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is allowed' do
            assert role_permission.manage_personal?
          end
        end

        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute role_permission.manage_personal?
          end
        end
      end
    end

    describe 'when the permission is a different value' do
      let(:permission) { 'readonly' }

      before { Subscription.any_instance.stubs(has_personal_rules?: true) }

      it 'returns false' do
        refute role_permission.edit?(rule)
      end
    end
  end

  describe '#manage_group?' do
    %w[full manage-group].each do |permission|
      describe "when the permission is `#{permission}`" do
        let(:permission) { permission }

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is allowed' do
            assert role_permission.manage_group?
          end
        end

        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute role_permission.manage_group?
          end
        end
      end
    end

    describe 'when the permission is a different value' do
      let(:permission) { 'manage-personal' }

      before { Subscription.any_instance.stubs(has_group_rules?: true) }

      it 'returns false' do
        refute role_permission.manage_group?
      end
    end
  end

  describe '#manage_shared?' do
    describe 'when the permission is `full`' do
      let(:permission) { 'full' }

      it 'is allowed' do
        assert role_permission.manage_shared?
      end
    end

    describe 'when the permission is a different value' do
      let(:permission) { 'manage-group' }

      it 'returns false' do
        refute role_permission.manage_shared?
      end
    end
  end

  describe 'when `permission` is not defined' do
    let(:role_permission) { described_class.new(user) }

    it 'fails' do
      assert_raises(RuntimeError) { role_permission.edit?(rule) }
    end
  end

  describe 'when `group_edit?` is not defined' do
    let(:role_permission_class) do
      Class.new(described_class) do
        def permission
          'manage-group'
        end
      end
    end

    let(:role_permission) { role_permission_class.new(user) }

    let(:owner_type) { 'Group' }

    it 'fails' do
      assert_raises(RuntimeError) { role_permission.edit?(rule) }
    end
  end
end
