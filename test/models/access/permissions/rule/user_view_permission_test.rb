require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Access::Permissions::Rule::UserViewPermission do
  fixtures :all

  let(:user)  { users(:with_groups_agent1) }
  let(:group) { groups(:minimum_group) }

  let(:permission_set) { build_valid_permission_set }

  let(:available_user_lists) { 'none' }
  let(:user_view_access)     { 'readonly' }

  let(:user_view) { rules(:global_user_view) }

  before do
    Account.any_instance.stubs(has_permission_sets?: true)

    user.stubs(permission_set: permission_set)

    user_view.account = user.account

    permission_set.permissions.available_user_lists = available_user_lists
    permission_set.permissions.user_view_access     = user_view_access
  end

  describe '#edit?' do
    describe 'when the user has `full` access' do
      let(:user_view_access) { 'full' }

      it 'is allowed' do
        assert user.can?(:edit, user_view)
      end
    end

    describe 'when the user has `manage-group` access' do
      let(:user_view_access) { 'manage-group' }

      describe 'and the user owns the user_view' do
        before { user_view.owner = user }

        it 'is allowed' do
          assert user.can?(:edit, user_view)
        end
      end

      describe 'and the user view is not group wide' do
        before { user_view.owner = user.account }

        it 'is not allowed' do
          refute user.can?(:edit, user_view)
        end
      end

      describe 'and the user view is group wide' do
        describe 'and the user is a member of the group' do
          before { user_view.owner = user.groups.first }

          it 'is allowed' do
            assert user.can?(:edit, user_view)
          end
        end

        describe 'and the user is not a member of the group' do
          before { user_view.owner = group }

          it 'is not allowed' do
            refute user.can?(:edit, user_view)
          end
        end
      end
    end

    describe 'when the user has `manage-personal` access' do
      let(:user_view_access) { 'manage-personal' }

      describe 'and the user owns the user_view' do
        before { user_view.owner = user }

        it 'is allowed' do
          assert user.can?(:edit, user_view)
        end
      end

      describe 'and the user does not own the user_view' do
        before { user_view.owner = user.account }

        it 'is not allowed' do
          refute user.can?(:edit, user_view)
        end
      end
    end

    describe 'when the user has `readonly` access' do
      let(:user_view_access) { 'readonly' }

      describe 'and the user owns the user_view' do
        before { user_view.owner = user }

        it 'is not allowed' do
          refute user.can?(:edit, user_view)
        end
      end

      describe 'and the user does not own the user_view' do
        before { user_view.owner = user.account }

        it 'is not allowed' do
          refute user.can?(:edit, user_view)
        end
      end
    end
  end

  describe '#view?' do
    describe 'when the user has access to `all` of the available user lists' do
      let(:available_user_lists) { 'all' }

      it 'is allowed' do
        assert user.can?(:view, UserView)
      end
    end

    describe 'when the user has access to `none` of the available user lists' do
      let(:available_user_lists) { 'none' }

      it 'is not allowed' do
        refute user.can?(:view, UserView)
      end
    end
  end

  describe '#manage_personal?' do
    %w[full manage-group manage-personal].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:user_view_access) { permission }

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is allowed' do
            assert user.can?(:manage_personal, UserView)
          end
        end

        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, UserView)
          end
        end
      end
    end

    describe 'when the user has `readonly` access' do
      let(:user_view_access) { 'readonly' }

      describe 'and the account has personal rules' do
        before do
          Subscription.any_instance.stubs(has_personal_rules?: true)
        end

        it 'is not allowed' do
          refute user.can?(:manage_personal, UserView)
        end
      end

      describe 'and the account does not have personal rules' do
        before do
          Subscription.any_instance.stubs(has_personal_rules?: true)
        end

        it 'is not allowed' do
          refute user.can?(:manage_personal, UserView)
        end
      end
    end
  end

  describe '#manage_group?' do
    %w[full manage-group].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:user_view_access) { permission }

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is allowed' do
            assert user.can?(:manage_group, UserView)
          end
        end

        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end
      end
    end

    %w[manage-personal readonly].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:user_view_access) { permission }

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end

        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end
      end
    end
  end

  describe '#manage_shared?' do
    describe 'when the user has `full` access' do
      let(:user_view_access) { 'full' }

      it 'is allowed' do
        assert user.can?(:manage_shared, UserView)
      end
    end

    %w[manage-group manage-personal readonly].each do |permission|
      describe "when the user has less than `#{permission}` access" do
        let(:user_view_access) { permission }

        it 'is not allowed' do
          refute user.can?(:manage_shared, UserView)
        end
      end
    end
  end
end
