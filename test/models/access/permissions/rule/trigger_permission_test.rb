require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Access::Permissions::Rule::TriggerPermission do
  include TestSupport::Rule::Helper

  fixtures :all

  let(:agent) { users(:with_groups_agent1) }
  let(:admin) { users(:with_groups_admin) }

  let(:user)    { agent }
  let(:account) { user.account }

  let(:permission_set) { build_valid_permission_set }

  let(:trigger) do
    create_trigger(
      definition: Definition.new.tap do |definition|
        definition.conditions_all.push(
          DefinitionItem.new('status_id', 'is', 1)
        )
        definition.actions.push(
          DefinitionItem.new(
            'notification_user',
            'is',
            ['current_user', '', '']
          )
        )
      end
    )
  end

  before do
    Account.any_instance.stubs(has_permission_sets?: true)

    user.stubs(permission_set: permission_set)
  end

  describe '#edit?' do
    before do
      permission_set.permissions.business_rule_management = rule_management
    end

    describe 'when the user can manage business rules' do
      let(:rule_management) { true }

      it 'is allowed' do
        assert user.can?(:edit, trigger)
      end
    end

    describe 'when the user cannot manage business rules' do
      let(:rule_management) { false }

      it 'is not allowed' do
        refute user.can?(:edit, trigger)
      end
    end
  end

  describe '#view?' do
    before do
      permission_set.permissions.business_rule_management = rule_management

      Account.any_instance.stubs(has_unlimited_triggers?: false)
    end

    describe 'when the user can manage business rules' do
      let(:rule_management) { true }

      it 'is allowed' do
        assert user.can?(:view, trigger)
      end
    end

    describe 'when the user cannot manage business rules' do
      let(:rule_management) { false }

      describe 'and the trigger is active' do
        before { trigger.is_active = true }

        it 'is allowed' do
          assert user.can?(:view, trigger)
        end
      end

      describe 'and the trigger is inactive' do
        before { trigger.is_active = false }

        it 'is not allowed' do
          refute user.can?(:view, trigger)
        end
      end

      describe 'and no trigger is provided' do
        it 'is not allowed' do
          refute user.can?(:view, Trigger)
        end
      end
    end
  end
end
