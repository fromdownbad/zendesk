require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Access::Permissions::Rule::AutomationPermission do
  include TestSupport::Rule::Helper

  fixtures :all

  let(:user)    { users(:with_groups_agent1) }
  let(:account) { user.account }

  let(:permission_set) { build_valid_permission_set }

  let(:automation) { create_automation(active: active) }
  let(:active)     { true }

  before do
    Account.any_instance.stubs(has_permission_sets?: true)

    automation.account = account

    user.stubs(permission_set: permission_set)

    permission_set.permissions.business_rule_management = rule_management
  end

  describe '#edit?' do
    describe 'when the user can manage business rules' do
      let(:rule_management) { true }

      describe 'and the account has unlimited automations' do
        before do
          Account.any_instance.stubs(has_unlimited_automations?: true)
        end

        it 'is allowed' do
          assert user.can?(:edit, automation)
        end
      end

      describe 'and the account does not have unlimited automations' do
        before do
          Account.any_instance.stubs(has_unlimited_automations?: false)
        end

        it 'is not allowed' do
          refute user.can?(:edit, automation)
        end
      end
    end

    describe 'when the user cannot manage business rules' do
      let(:rule_management) { false }

      describe 'and the account has unlimited automations' do
        before do
          Account.any_instance.stubs(has_unlimited_automations?: true)
        end

        it 'is not allowed' do
          refute user.can?(:edit, automation)
        end
      end

      describe 'and the account does not have unlimited automations' do
        before do
          Account.any_instance.stubs(has_unlimited_automations?: false)
        end

        it 'is not allowed' do
          refute user.can?(:edit, automation)
        end
      end
    end
  end

  describe '#view?' do
    before do
      Account.any_instance.stubs(has_unlimited_automations?: true)
    end

    describe 'when the user can manage business rules' do
      let(:rule_management) { true }

      it 'is allowed' do
        assert user.can?(:view, automation)
      end
    end

    describe 'when the user cannot manage business rules' do
      let(:rule_management) { false }

      describe 'and the automation is active' do
        it 'is allowed' do
          assert user.can?(:view, automation)
        end
      end

      describe 'and the automation is inactive' do
        let(:active) { false }

        it 'is not allowed' do
          refute user.can?(:view, automation)
        end
      end

      describe 'and no automation is provided' do
        it 'is not allowed' do
          refute user.can?(:view, Automation)
        end
      end
    end
  end
end
