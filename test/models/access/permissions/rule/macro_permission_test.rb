require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Access::Permissions::Rule::MacroPermission do
  include TestSupport::Rule::Helper

  fixtures :all

  let(:user)    { users(:with_groups_agent1) }
  let(:account) { user.account }

  let(:associated_group)   { user.groups.first }
  let(:unassociated_group) { groups(:minimum_group) }

  let(:permission_set) { build_valid_permission_set }

  let(:owner) { account }

  let(:macro) { create_macro(owner: owner) }

  before do
    Rule.destroy_all

    Account.any_instance.stubs(has_permission_sets?: true)

    user.stubs(permission_set: permission_set)

    permission_set.permissions.macro_access = macro_access
  end

  describe '#edit?' do
    describe 'when the user has `full` access' do
      let(:macro_access) { 'full' }

      it 'is allowed' do
        assert user.can?(:edit, macro)
      end
    end

    describe 'when the user has `manage-group` access' do
      let(:macro_access) { 'manage-group' }

      describe 'and the user owns the macro' do
        let(:owner) { user }

        it 'is allowed' do
          assert user.can?(:edit, macro)
        end
      end

      describe 'and the rule is not group wide' do
        let(:owner) { account }

        it 'is not allowed' do
          refute user.can?(:edit, macro)
        end
      end

      describe 'and the rule is group wide' do
        before do
          GroupMacro.destroy_all

          macro.owner_type = 'Group'
        end

        describe 'and it is owned by a single group' do
          before do
            GroupMacro.create! do |group_macro|
              group_macro.account_id = account.id
              group_macro.group_id   = group.id
              group_macro.macro_id   = macro.id
            end
          end

          describe 'and the user is a member of the group' do
            let(:group) { associated_group }

            it 'is allowed' do
              assert user.can?(:edit, macro)
            end
          end

          describe 'and the user is not a member of the group' do
            let(:group) { unassociated_group }

            it 'is not allowed' do
              refute user.can?(:edit, macro)
            end
          end
        end

        describe 'and the group owner IDs are specified' do
          before { macro.group_owner_ids = [group.id] }

          describe 'and the user is a member of the group' do
            let(:group) { associated_group }

            it 'is allowed' do
              assert user.can?(:edit, macro)
            end
          end

          describe 'and the user is not a member of the group' do
            let(:group) { unassociated_group }

            it 'is not allowed' do
              refute user.can?(:edit, macro)
            end
          end
        end

        describe 'and it is owned by multiple groups' do
          before do
            [associated_group, unassociated_group].each do |group|
              GroupMacro.create! do |group_macro|
                group_macro.account_id = account.id
                group_macro.group_id   = group.id
                group_macro.macro_id   = macro.id
              end
            end
          end

          it 'is not allowed' do
            refute user.can?(:edit, macro)
          end
        end
      end
    end

    describe 'when the user has `manage-personal` access' do
      let(:macro_access) { 'manage-personal' }

      describe 'and the user owns the macro' do
        let(:owner) { user }

        it 'is allowed' do
          assert user.can?(:edit, macro)
        end
      end

      describe 'and the user does not own the macro' do
        let(:owner) { account }

        it 'is not allowed' do
          refute user.can?(:edit, macro)
        end
      end
    end

    describe 'when the user has `readonly` access' do
      let(:macro_access) { 'readonly' }

      describe 'and the user owns the macro' do
        let(:owner) { user }

        it 'is not allowed' do
          refute user.can?(:edit, macro)
        end
      end

      describe 'and the user does not own the macro' do
        let(:owner) { account }

        it 'is not allowed' do
          refute user.can?(:edit, macro)
        end
      end
    end
  end

  describe '#view?' do
    %w[full manage-group manage-personal].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:macro_access) { permission }

        it 'is allowed' do
          assert user.can?(:view, Macro)
        end
      end
    end

    describe 'when the user has `readonly` access' do
      let(:macro_access) { 'readonly' }

      it 'is not allowed' do
        refute user.can?(:view, Macro)
      end
    end
  end

  describe '#manage_personal?' do
    %w[full manage-group manage-personal].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:macro_access) { permission }

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is allowed' do
            assert user.can?(:manage_personal, Macro)
          end
        end

        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, Macro)
          end
        end
      end
    end

    describe 'when the user has `readonly` access' do
      let(:macro_access) { 'readonly' }

      describe 'and the account has personal rules' do
        before { Subscription.any_instance.stubs(has_personal_rules?: true) }

        it 'is not allowed' do
          refute user.can?(:manage_personal, Macro)
        end
      end

      describe 'and the account does not have personal rules' do
        before { Subscription.any_instance.stubs(has_personal_rules?: true) }

        it 'is not allowed' do
          refute user.can?(:manage_personal, Macro)
        end
      end
    end
  end

  describe '#manage_group?' do
    %w[full manage-group].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:macro_access) { permission }

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is allowed' do
            assert user.can?(:manage_group, Macro)
          end
        end

        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, Macro)
          end
        end
      end
    end

    %w[manage-personal readonly].each do |permission|
      describe "when the user has `#{permission}` access" do
        let(:macro_access) { permission }

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is not allowed' do
            refute user.can?(:manage_group, Macro)
          end
        end

        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, Macro)
          end
        end
      end
    end
  end

  describe '#manage_shared?' do
    describe 'when the user has `full` access' do
      let(:macro_access) { 'full' }

      it 'is allowed' do
        assert user.can?(:manage_shared, Macro)
      end
    end

    %w[manage-group manage-personal readonly].each do |permission|
      describe "when the user has less than `#{permission}` access" do
        let(:macro_access) { permission }

        it 'is not allowed' do
          refute user.can?(:manage_shared, Macro)
        end
      end
    end
  end
end
