require_relative "../../../support/test_helper"
require 'access/permissions/user_permission'

SingleCov.covered!

describe 'Access::Permissions::FullUserPermission' do
  let(:agent) { users(:minimum_agent) }
  let(:end_user) { users(:minimum_end_user) }

  let(:role_permission) do
    Access::Permissions::FullUserPermission.new(agent)
  end

  describe '#edit?' do
    describe 'for answer bot user' do
      before do
        end_user.stubs(:answer_bot?).returns(true)
      end

      it 'returns false' do
        refute role_permission.edit?(end_user)
      end
    end
  end
end
