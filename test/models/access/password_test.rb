require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Password' do
  fixtures :users, :accounts, :subscriptions

  subject { users(:minimum_end_user2) }
  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_end_user) }
  let(:abilities) { [:request_password_reset, :set_password, :change_password, :create_password] }

  def assert_can(actor:, subject:, only:, as:)
    only.each do |ability|
      assert actor.can?(ability, subject), "#{as} expected #{ability}"
    end

    (abilities - only).each do |ability|
      refute actor.can?(ability, subject), "#{as} did not expect #{ability}"
    end
  end

  def assert_can_only(actor:, myself: [], admin: [], agent: [], end_user: [])
    refute_equal actor.id, subject.id

    assert_can(actor: actor, subject: actor, only: myself, as: "as myself")

    subject.stubs(:roles).returns(Role::ADMIN.id)
    assert_can(actor: actor, subject: subject, only: admin, as: "for an admin")

    subject.stubs(:roles).returns(Role::AGENT.id)
    assert_can(actor: actor, subject: subject, only: agent, as: "for an agent")

    subject.stubs(:roles).returns(Role::END_USER.id)
    assert_can(actor: actor, subject: subject, only: end_user, as: "for an enduser")
  end

  describe "when admin can set passwords" do
    before { Account.any_instance.stubs(admins_can_set_user_passwords: true) }

    describe "when end-users can change their password" do
      before { Account.any_instance.stubs(is_end_user_password_change_visible?: true) }

      it "as the account owner" do
        assert_can_only(
          actor: users(:minimum_admin),
          myself: [:change_password, :set_password],
          admin: [:request_password_reset, :set_password],
          agent: [:request_password_reset, :set_password],
          end_user:  [:request_password_reset, :set_password]
        )
      end

      it "as an admin" do
        assert_can_only(
          actor: users(:minimum_admin_not_owner),
          myself: [:change_password, :set_password],
          admin: [:request_password_reset, :set_password],
          agent: [:request_password_reset, :set_password],
          end_user:  [:request_password_reset, :set_password]
        )
      end

      it "as an agent" do
        assert_can_only(
          actor: users(:minimum_agent),
          myself: [:change_password]
        )
      end

      it "as an end user" do
        assert_can_only(
          actor: users(:minimum_end_user),
          myself: [:request_password_reset, :change_password]
        )
      end
    end

    describe "when end-users cannot change their password" do
      before { Account.any_instance.stubs(is_end_user_password_change_visible?: false) }

      it "as the account owner" do
        assert_can_only(
          actor: users(:minimum_admin),
          myself: [:change_password, :set_password],
          admin: [:request_password_reset, :set_password],
          agent: [:request_password_reset, :set_password],
          end_user:  [:request_password_reset, :set_password]
        )
      end

      it "as an admin" do
        assert_can_only(
          actor: users(:minimum_admin_not_owner),
          myself: [:change_password, :set_password],
          admin: [:request_password_reset, :set_password],
          agent: [:request_password_reset, :set_password],
          end_user:  [:request_password_reset, :set_password]
        )
      end

      it "as an agent" do
        assert_can_only(
          actor: users(:minimum_agent),
          myself: [:change_password]
        )
      end

      it "authorizes as an end user" do
        assert_can_only(
          actor: users(:minimum_end_user),
          myself: []
        )
      end
    end

    describe "when users do not have a password" do
      before { User.any_instance.stubs(crypted_password: nil) }

      it "as the account owner" do
        assert_can_only(
          actor: users(:minimum_admin),
          myself: [:set_password],
          admin: [:set_password, :request_password_reset],
          agent: [:set_password, :request_password_reset],
          end_user:  [:set_password, :request_password_reset]
        )
      end

      it "as an admin" do
        assert_can_only(
          actor: users(:minimum_admin_not_owner),
          myself: [:set_password],
          admin: [:set_password, :request_password_reset],
          agent: [:set_password, :request_password_reset],
          end_user:  [:set_password, :request_password_reset]
        )
      end

      it "as an agent" do
        assert_can_only(
          actor: users(:minimum_agent),
          myself: []
        )
      end

      it "as an end user" do
        assert_can_only(
          actor: users(:minimum_end_user),
          myself: [:request_password_reset]
        )
      end
    end

    describe "with sso enabled" do
      before do
        account.role_settings.update_attributes!(
          end_user_zendesk_login: false,
          agent_zendesk_login: false,
          end_user_remote_login: true,
          agent_remote_login: true
        )
      end

      describe "when user has password" do
        it "as the account owner" do
          assert_can_only(
            actor: users(:minimum_admin),
            myself: [:change_password, :set_password, :request_password_reset],
            admin: [:request_password_reset, :set_password],
            agent: [:request_password_reset, :set_password],
            end_user:  [:request_password_reset, :set_password]
          )
        end

        it "as an admin" do
          assert_can_only(
            actor: users(:minimum_admin_not_owner),
            myself: [:change_password, :set_password, :request_password_reset],
            admin: [:request_password_reset],
            agent: [:request_password_reset, :set_password],
            end_user:  [:request_password_reset, :set_password]
          )
        end

        it "as an agent" do
          assert_can_only(
            actor: users(:minimum_agent),
            myself: [:change_password, :request_password_reset]
          )
        end

        it "as an end user" do
          assert_can_only(
            actor: users(:minimum_end_user),
            myself: [:request_password_reset]
          )
        end
      end

      describe "when users do not have a password" do
        before { User.any_instance.stubs(crypted_password: nil) }

        describe "and zendesk auth is disabled" do
          before do
            account.role_settings.update_attributes!(
              end_user_password_allowed: false,
              agent_password_allowed: false
            )
          end

          it "as the account owner" do
            assert_can_only(
              actor: users(:minimum_admin),
              myself: [:set_password],
              admin: [:create_password, :set_password],
              agent: [:create_password, :set_password],
              end_user:  [:create_password, :set_password]
            )
          end

          it "as an admin" do
            assert_can_only(
              actor: users(:minimum_admin_not_owner),
              myself: [:set_password],
              admin: [:create_password],
              agent: [:create_password, :set_password],
              end_user:  [:create_password, :set_password]
            )
          end

          it "as an agent" do
            assert_can_only(
              actor: users(:minimum_agent),
              myself: []
            )
          end

          it "as an end user" do
            assert_can_only(
              actor: users(:minimum_end_user),
              myself: []
            )
          end
        end

        describe "and zendesk auth is enabled" do
          before do
            account.role_settings.update_attributes!(
              end_user_password_allowed: true
            )
          end

          it "as the account owner" do
            assert_can_only(
              actor: users(:minimum_admin),
              myself: [:set_password, :request_password_reset],
              admin: [:create_password, :set_password, :request_password_reset],
              agent: [:create_password, :set_password, :request_password_reset],
              end_user:  [:create_password, :set_password, :request_password_reset]
            )
          end

          it "as an admin" do
            assert_can_only(
              actor: users(:minimum_admin_not_owner),
              myself: [:set_password, :request_password_reset],
              admin: [:create_password, :request_password_reset],
              agent: [:create_password, :set_password, :request_password_reset],
              end_user:  [:create_password, :set_password, :request_password_reset]
            )
          end

          it "as an agent" do
            assert_can_only(
              actor: users(:minimum_agent),
              myself: [:request_password_reset]
            )
          end

          it "as an end user" do
            assert_can_only(
              actor: users(:minimum_end_user),
              myself: [:request_password_reset]
            )
          end
        end
      end
    end
  end

  describe "when admins cannot set passwords" do
    before { Account.any_instance.stubs(admins_can_set_user_passwords: false) }

    describe "when end-users can change their password" do
      before { Account.any_instance.stubs(is_end_user_password_change_visible?: true) }

      it "as the account owner" do
        assert_can_only(
          actor: users(:minimum_admin),
          myself: [:change_password],
          admin: [:request_password_reset],
          agent: [:request_password_reset],
          end_user:  [:request_password_reset]
        )
      end

      it "as an admin" do
        assert_can_only(
          actor: users(:minimum_admin_not_owner),
          myself: [:change_password],
          admin: [:request_password_reset],
          agent: [:request_password_reset],
          end_user:  [:request_password_reset]
        )
      end

      it "as an agent" do
        assert_can_only(
          actor: users(:minimum_agent),
          myself: [:change_password],
          end_user: [:request_password_reset]
        )
      end

      it "as an end user" do
        assert_can_only(
          actor: users(:minimum_end_user),
          myself: [:change_password, :request_password_reset]
        )
      end

      describe "with permission set" do
        before { account.stubs(has_permission_sets?: true) }

        describe "with enterprise account" do
          before { Subscription.any_instance.stubs(plan_type: SubscriptionPlanType.ExtraLarge) }

          it "when an agent user cannot modify user profiles" do
            agent = users(:minimum_agent)
            agent.abilities.stubs(:defer?).with(:edit, subject).returns(false)
            agent.stubs(permission_set: PermissionSet.new)
            assert_can_only(
              actor: agent,
              myself: [:change_password],
              end_user: []
            )
          end

          # This doesn't work but it should. When the password policy is fixed restore this test
          should_eventually "when an agent user can modify user profiles" do
            agent = users(:minimum_agent)
            agent.abilities.expects(:defer?).with(:edit, subject).returns(true).once

            assert_can_only(
              actor: agent,
              myself: [:change_password],
              end_user: [:request_password_reset]
            )
          end
        end
      end
    end

    describe "when end-users cannot change their password" do
      before do
        Account.any_instance.stubs(is_end_user_password_change_visible?: false)
      end

      it "as the account owner" do
        assert_can_only(
          actor: users(:minimum_admin),
          myself: [:change_password],
          admin: [:request_password_reset],
          agent: [:request_password_reset],
          end_user:  [:request_password_reset]
        )
      end

      it "as an admin" do
        assert_can_only(
          actor: users(:minimum_admin_not_owner),
          myself: [:change_password],
          admin: [:request_password_reset],
          agent: [:request_password_reset],
          end_user:  [:request_password_reset]
        )
      end

      it "as an agent" do
        assert_can_only(
          actor: users(:minimum_agent),
          myself: [:change_password],
          end_user: [:request_password_reset]
        )
      end

      it "as an end user" do
        assert_can_only(
          actor: users(:minimum_end_user),
          myself: []
        )
      end

      describe "with permission sets" do
        before do
          account.stubs(has_permission_sets?: true)
          Subscription.any_instance.stubs(plan_type: SubscriptionPlanType.ExtraLarge)
        end

        it "when an agent user cannot modify user profiles" do
          agent = users(:minimum_agent)
          agent.stubs(permission_set: PermissionSet.new)
          agent.abilities.stubs(:defer?).with(:edit, subject).returns(false)

          assert_can_only(
            actor: agent,
            myself: [:change_password],
            end_user: []
          )
        end

        # This doesn't work but it should. When the password policy is fixed restore this test
        should_eventually "when an agent user can modify user profiles" do
          agent = users(:minimum_agent)
          agent.abilities.expects(:defer?).with(:edit, subject).returns(true).once

          assert_can_only(
            actor: agent,
            myself: [:change_password],
            end_user: [:request_password_reset]
          )
        end
      end
    end

    describe "with sso enabled" do
      before do
        account.role_settings.update_attributes!(
          end_user_zendesk_login: false,
          agent_zendesk_login: false,
          end_user_remote_login: true,
          agent_remote_login: true
        )
      end

      describe "when user has password" do
        it "as the account owner" do
          assert_can_only(
            actor: users(:minimum_admin),
            myself: [:change_password, :request_password_reset],
            admin: [:request_password_reset],
            agent: [:request_password_reset],
            end_user:  [:request_password_reset]
          )
        end

        it "as an admin" do
          assert_can_only(
            actor: users(:minimum_admin_not_owner),
            myself: [:change_password, :request_password_reset],
            admin: [:request_password_reset],
            agent: [:request_password_reset],
            end_user:  [:request_password_reset]
          )
        end

        it "as an agent" do
          assert_can_only(
            actor: users(:minimum_agent),
            myself: [:change_password, :request_password_reset],
            end_user: [:request_password_reset]
          )
        end

        it "as an end user" do
          assert_can_only(
            actor: users(:minimum_end_user),
            myself: [:request_password_reset]
          )
        end

        ## TODO add enterprise permission test here
      end

      describe "when users do not have a password" do
        before { User.any_instance.stubs(crypted_password: nil) }

        describe "and zendesk auth is disabled" do
          before do
            account.role_settings.update_attributes!(
              end_user_password_allowed: false,
              agent_password_allowed: false
            )
          end

          it "as the account owner" do
            assert_can_only(
              actor: users(:minimum_admin),
              myself: [],
              admin: [:create_password],
              agent: [:create_password],
              end_user:  [:create_password]
            )
          end

          it "as an admin" do
            assert_can_only(
              actor: users(:minimum_admin_not_owner),
              myself: [],
              admin: [:create_password],
              agent: [:create_password],
              end_user:  [:create_password]
            )
          end

          it "as an agent" do
            assert_can_only(
              actor: users(:minimum_agent),
              myself: [],
              end_user: [:create_password]
            )
          end

          it "as an end user" do
            assert_can_only(
              actor: users(:minimum_end_user),
              myself: []
            )
          end
        end

        describe "and zendesk auth is enabled" do
          before do
            account.role_settings.update_attributes!(
              end_user_password_allowed: true,
              agent_password_allowed: true
            )
          end

          it "as the account owner" do
            assert_can_only(
              actor: users(:minimum_admin),
              myself: [:request_password_reset],
              admin: [:create_password, :request_password_reset],
              agent: [:create_password, :request_password_reset],
              end_user:  [:create_password, :request_password_reset]
            )
          end

          it "as an admin" do
            assert_can_only(
              actor: users(:minimum_admin_not_owner),
              myself: [:request_password_reset],
              admin: [:create_password, :request_password_reset],
              agent: [:create_password, :request_password_reset],
              end_user:  [:create_password, :request_password_reset]
            )
          end

          it "as an agent" do
            assert_can_only(
              actor: users(:minimum_agent),
              myself: [:request_password_reset],
              admin: [],
              agent: [],
              end_user: [:create_password, :request_password_reset]
            )
          end

          it "as an end user" do
            assert_can_only(
              actor: users(:minimum_end_user),
              myself: [:request_password_reset]
            )
          end
        end

        ## TODO add enterprise permission test here
      end
    end
  end
end
