require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Attachment' do
  fixtures :all

  subject { @user }

  describe "The system user" do
    before do
      @user = User.system
      @user.expects(:is_system_user?).returns(true)
    end
    should_be_able_to(:view, 'all attachments') { ::Attachment.new }
  end

  describe "Admins" do
    before { @user = users(:minimum_admin) }

    should_be_able_to(:view, 'attachments on their own account') { ::Attachment.new(account: @user.account) }
    should_not_be_able_to(:view, 'attachments on other account') { ::Attachment.new(account: accounts(:with_groups)) }
    should_be_able_to(:delete, 'attachments that have token source') { attachments(:upload_token_attachment) }
    should_be_able_to(:delete, 'attachments from an entry') { attachments(:attachment_entry) }
    should_not_be_able_to(:delete, 'attachments from a post') { attachments(:attachment_post) }
    should_not_be_able_to(:delete, 'attachments from a comment') { attachments(:serialization_comment_attachment) }
    should_be_able_to(:create) { Attachment }
    should_be_able_to(:view, 'expirable attachments from UpdateRequesterTargetRulesJob') do
      ::ExpirableAttachment.new(created_via: CcsAndFollowers::UpdateRequesterTargetRulesJob.to_s)
    end

    describe "viewing ExpirableAttachments created via an export job" do
      let(:export_configuration) { Zendesk::Export::Configuration.new(@user.account) }
      let(:whitelisted_expectation) do
        export_configuration.expects(:whitelisted?).
          times(ImportExportJobPolicy::EXPORT_JOB_CLASS_NAMES.size).with(@user)
      end
      before { Zendesk::Export::Configuration.stubs(new: export_configuration) }

      describe "and the admin is whitelisted" do
        before { whitelisted_expectation.returns(true) }

        it "is allowed" do
          ImportExportJobPolicy::EXPORT_JOB_CLASS_NAMES.each do |export_job_class_name|
            assert @user.can?(:view, ::ExpirableAttachment.new(created_via: export_job_class_name))
          end
        end
      end

      describe "and the admin is not whitelisted" do
        before { whitelisted_expectation.returns(false) }

        it "is denied" do
          ImportExportJobPolicy::EXPORT_JOB_CLASS_NAMES.each do |export_job_class_name|
            refute @user.can?(:view, ::ExpirableAttachment.new(created_via: export_job_class_name))
          end
        end
      end
    end
  end

  describe "Agents" do
    before do
      @user = users(:minimum_agent)
      StoresSynchronizationJob.stubs(:enqueue)
    end

    should_be_able_to(:view, 'attachments on their own account') { ::Attachment.new(account: @user.account) }
    should_not_be_able_to(:view, 'attachments on other account') { ::Attachment.new(account: accounts(:with_groups)) }
    should_be_able_to(:delete, 'attachments that have token source') { attachments(:upload_token_attachment) }
    should_be_able_to(:delete, 'attachments from an entry') { attachments(:attachment_entry) }
    should_not_be_able_to(:delete, 'attachments from a post') { attachments(:attachment_post) }
    should_not_be_able_to(:delete, 'attachments from a comment') { attachments(:serialization_comment_attachment) }
    should_be_able_to(:create) { Attachment }

    should_not_be_able_to(:view, 'expirable attachments from UpdateRequesterTargetRulesJob') do
      ::ExpirableAttachment.new(created_via: CcsAndFollowers::UpdateRequesterTargetRulesJob.to_s)
    end

    describe "viewing Comment attachments" do
      describe "when the ticket has been deleted" do
        before { @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: nil, body: 'foo')) }
        it "is denied" do
          refute @user.can?(:view, @attachment)
        end
      end

      describe "when the source is not present" do
        before do
          @attachment = ::Attachment.new(account: @user.account)
        end
        it "is denied" do
          @attachment.stubs(:source_type).returns("Comment")
          @attachment.stubs(:source_id).returns(-1)
          refute @user.can?(:view, @attachment)
        end
      end

      describe "when the source is present but blank" do
        before do
          @ticket = tickets(:minimum_1)
          @attachment = ::Attachment.new(account: @user.account, source: @ticket.comments.first)
          @attachment.save!(validate: false)
          @attachment.reload
        end

        it "is allowed" do
          @attachment.source.stubs(:blank?).returns(true)
          assert @user.can?(:view, @attachment)
        end
      end

      describe "when secure downloads is disabled" do
        before do
          object = mock.tap do |settings|
            settings.stubs(:private_attachments?).returns false
            settings.stubs(:follower_and_email_cc_collaborations?).returns true
          end

          Account.any_instance.stubs(:settings).returns object
        end

        describe "when the agent is in a group that cannot see the ticket" do
          before do
            @user = users(:with_groups_agent_assigned_restricted)
            @ticket = tickets(:with_groups_1)
            @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: @ticket, body: 'foo'))
          end

          it { assert @user.can?(:view, @attachment) }
        end

        describe "when the agent is in a group that can see the ticket" do
          before do
            @ticket = tickets(:minimum_1)
            @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: @ticket, body: 'foo'))
          end

          it { assert @user.can?(:view, @attachment) }
        end
      end

      describe "when secure downloads is enabled" do
        before do
          object = mock.tap do |settings|
            settings.stubs(:private_attachments?).returns true
            settings.stubs(:follower_and_email_cc_collaborations?).returns true
          end

          Account.any_instance.stubs(:settings).returns object
        end

        describe "when the agent is in a group that cannot see the ticket" do
          before do
            @user = users(:with_groups_agent_assigned_restricted)
            @ticket = tickets(:with_groups_1)
            @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: @ticket, body: 'foo'))
          end

          it { refute @user.can?(:view, @attachment) }
        end

        describe "when the agent is in a group that can see the ticket" do
          before do
            @ticket = tickets(:minimum_1)
            @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: @ticket, body: 'foo'))
          end

          it { assert @user.can?(:view, @attachment) }
        end
      end

      describe "when the ticket has been archived" do
        before do
          @ticket = tickets(:minimum_1)
          @attachment = ::Attachment.new(account: @user.account, source: @ticket.comments.first)
          @attachment.save!(validate: false)

          archive_and_delete(@ticket)
          @attachment.reload
        end

        it "is allowed" do
          assert @user.can?(:view, @attachment)
        end
      end

      describe "when the ticket has been archived without physical deletion" do
        before do
          @ticket = tickets(:minimum_1)
          @attachment = ::Attachment.new(account: @user.account, source: @ticket.comments.first)
          @attachment.save!(validate: false)

          @ticket.archive!
          Ticket.where(id: @ticket.id).update_all(status_id: StatusType.ARCHIVED)
          @attachment.reload
        end

        it "is allowed" do
          assert @user.can?(:view, @attachment)
        end
      end
    end

    describe "viewing Entry attachments" do
      before do
        @entry       = ::Entry.new
        @entry.forum = ::Forum.new
        @attachment  = ::Attachment.new(source: @entry)
      end

      it "is allowed if the entry is public" do
        @entry.stubs(:is_public?).returns(true)
        assert @user.can?(:view, @attachment)
      end

      it "is allowed if the entry is private" do
        @entry.stubs(:is_public?).returns(false)
        assert @user.can?(:view, @attachment)
      end
    end
  end

  describe "Agents from enterprise plan" do
    before do
      @user = users(:minimum_agent)
      @permission_set = build_valid_permission_set
      @user.stubs(permission_set: @permission_set)
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
    end

    should_be_able_to(:view, 'attachments on their own account') { ::Attachment.new(account: @user.account) }
    should_not_be_able_to(:view, 'attachments on other account') { ::Attachment.new(account: accounts(:with_groups)) }
  end

  describe "Light agents" do
    before do
      create_light_agent
    end

    describe "viewing ExpirableAttachments" do
      it "is denied" do
        refute @light_agent.can?(:view, ::ExpirableAttachment.new(created_via: "UserXmlExportJob"))
        refute @light_agent.can?(:view, ::ExpirableAttachment.new(created_via: "XmlExportJob"))
        refute @light_agent.can?(:view, ::ExpirableAttachment.new(created_via: "CcsAndFollowers::UpdateRequesterTargetRulesJob"))
      end
    end
  end

  describe "End users" do
    before { @user = users(:minimum_end_user) }
    describe "viewing ExpirableAttachments" do
      it "is denied" do
        refute @user.can?(:view, ::ExpirableAttachment.new)
      end
    end

    describe "viewing Comment attachments" do
      describe "when the ticket has been deleted" do
        before { @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: nil, body: 'foo')) }
        it "is denied" do
          refute @user.can?(:view, @attachment)
        end
      end

      describe "when the user is the ticket's requester" do
        before { @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: Ticket.new(requester_id: @user.id), body: 'foo')) }
        it "is allowed" do
          assert @user.can?(:view, @attachment)
        end
      end

      describe "when the user is a collaborator on the ticket" do
        before do
          @ticket = tickets(:minimum_1)
          @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: @ticket, body: 'foo'))
          @ticket.expects(:is_collaborator?).with(@user).returns(true)
        end
        it "is allowed" do
          assert @user.can?(:view, @attachment)
        end
      end

      describe "when the user belongs to the ticket's organization" do
        before do
          @ticket = tickets(:minimum_1)
          @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: @ticket, body: 'foo'))
          @ticket.stubs(:organization).returns(@ticket.account.organizations.first)
          @user.abilities.expects(:defer?).with(:view, @ticket.account.organizations.first).returns(true)
        end
        it "is allowed" do
          assert @user.can?(:view, @attachment)
        end
      end

      describe "all other cases" do
        before do
          @ticket = tickets(:minimum_1)
          @attachment = ::Attachment.new(account: @user.account, source: Comment.new(ticket: @ticket, body: 'foo'))
        end

        it "defers to account.settings.private_attachments? (case: enabled)" do
          @user.account.settings.expects(:private_attachments?).returns(false)
          assert @user.can?(:view, @attachment)
        end
        it "defers to account.settings.private_attachments? (case: disabled)" do
          @user.account.settings.expects(:private_attachments?).returns(true)
          refute @user.can?(:view, @attachment)
        end
      end
    end

    describe "creating attachments" do
      describe "when the user is anonymous" do
        before { @user.stubs(is_anonymous_user?: true) }

        describe_with_arturo_enabled :sse_prevent_anonymous_uploads do
          describe "when the end users can attach files setting is not enabled" do
            before { @user.account.stubs(is_attaching_enabled?: false) }

            it { refute @user.can?(:create, Attachment) }
          end

          describe "when the end users can attach files setting is enabled" do
            before { @user.account.stubs(is_attaching_enabled?: true) }

            describe "when the private attachments setting is enabled" do
              before { @user.account.settings.stubs(private_attachments?: true) }

              it { refute @user.can?(:create, Attachment) }
            end

            describe "when the private attachments setting is not enabled" do
              before { @user.account.settings.stubs(private_attachments?: false) }

              it { assert @user.can?(:create, Attachment) }
            end
          end
        end

        describe_with_arturo_disabled :sse_prevent_anonymous_uploads do
          describe "when the end users can attach files setting is not enabled" do
            before { @user.account.stubs(is_attaching_enabled?: false) }

            it "returns false" do
              refute @user.can?(:create, Attachment)
            end
          end

          describe "when the end users can attach files setting is enabled" do
            before { @user.account.stubs(is_attaching_enabled?: true) }

            describe "when the private attachments setting is enabled" do
              before { @user.account.settings.stubs(private_attachments?: true) }

              it "returns false" do
                refute @user.can?(:create, Attachment)
              end
            end

            describe "when the private attachments setting is not enabled" do
              before { @user.account.settings.stubs(private_attachments?: false) }

              it "returns true" do
                assert @user.can?(:create, Attachment)
              end
            end
          end
        end
      end

      describe "when the user is not anonymous" do
        describe "when the end users can attach files setting is enabled" do
          before { @user.account.stubs(is_attaching_enabled?: true) }

          describe "when the private attachments setting is enabled" do
            before { @user.account.settings.stubs(private_attachments?: true) }

            it "returns true" do
              assert @user.can?(:create, Attachment)
            end
          end

          describe "when the private attachments setting is not enabled" do
            before { @user.account.settings.stubs(private_attachments?: false) }

            it "returns true" do
              assert @user.can?(:create, Attachment)
            end
          end
        end

        describe "when the end users can attach files setting is not enabled" do
          before { @user.account.stubs(is_attaching_enabled?: false) }

          it "returns false" do
            refute @user.can?(:create, Attachment)
          end
        end
      end
    end

    describe "viewing Post attachments" do
      before do
        @post = Post.new
        @attachment = Attachment.new(source: @post)
      end

      it "defers to can?(:view, post) (positive)" do
        @user.abilities.expects(:defer?).with(:view, @post).returns(true)
        assert @user.can?(:view, @attachment)
      end

      it "defers to can?(:view, post) (negative)" do
        @user.abilities.expects(:defer?).with(:view, @post).returns(false)
        refute @user.can?(:view, @attachment)
      end
    end

    describe "viewing Entry attachments" do
      before do
        @user  = users(:minimum_end_user)
        @forum = Forum.new
        @entry = Entry.new
        @entry.forum = @forum
        @attachment = Attachment.new(source: @entry)
      end

      it "is allowed if the entry is public" do
        @forum.expects(:visibility_restriction).returns(stub(agents_only?: false))
        assert @user.can?(:view, @attachment)
      end

      it "is denied if the entry is private" do
        @forum.expects(:visibility_restriction).returns(stub(agents_only?: true))
        refute @user.can?(:view, @attachment)
      end
    end

    describe "viewing UploadTokens" do
      before { @attachment = Attachment.new(source: source, account: Account.new) }

      describe "with target user" do
        let(:source) do
          token = UploadToken.new
          token.target = @user
          token
        end

        it "is allowed if user is not anonymous" do
          assert @user.can?(:view, @attachment)
        end

        describe "given the user is anonymous" do
          before do
            @attachment.account = Account.new
            @user.stubs(is_anonymous_user?: true)
          end

          it "is not allowed" do
            refute @user.can?(:view, @attachment)
          end

          it "is allowed if the upload is inline and private attachments are off" do
            @attachment.account.settings.stubs(private_attachments?: false)
            @attachment.stubs(inline?: true)
            assert @user.can?(:view, @attachment)
          end
        end
      end

      describe "with target user who is a system user" do
        let(:source) do
          token = UploadToken.new
          token.target = User.system
          token
        end

        describe_with_arturo_enabled :polaris do
          it "is allowed if user is not anonymous" do
            assert @user.can?(:view, @attachment)
          end

          describe "given the user is anonymous" do
            before do
              @attachment.account = Account.new
              @user.stubs(is_anonymous_user?: true)
            end

            it "is allowed" do
              assert @user.can?(:view, @attachment)
            end
          end
        end

        describe_with_arturo_disabled :polaris do
          it "is allowed if user is not anonymous" do
            assert @user.can?(:view, @attachment)
          end

          describe "given the user is anonymous" do
            before do
              @attachment.account = Account.new
              @user.stubs(is_anonymous_user?: true)
            end

            it "is not allowed" do
              refute @user.can?(:view, @attachment)
            end

            it "is allowed if the upload is inline and private attachments are off" do
              @attachment.account.settings.stubs(private_attachments?: false)
              @attachment.stubs(inline?: true)
              assert @user.can?(:view, @attachment)
            end
          end
        end
      end

      describe "with target ticket" do
        let(:ticket) { tickets(:minimum_1) }

        let(:source) do
          token = UploadToken.new
          token.target = ticket
          token
        end

        describe_with_arturo_enabled :polaris do
          it "is allowed if user is not anonymous" do
            assert @user.can?(:view, @attachment)
          end

          describe "given the user is anonymous" do
            before do
              @attachment.account = Account.new
              @user.stubs(is_anonymous_user?: true)
            end

            it "is allowed" do
              assert @user.can?(:view, @attachment)
            end
          end
        end

        describe_with_arturo_disabled :polaris do
          it "is allowed if user is not anonymous" do
            assert @user.can?(:view, @attachment)
          end

          describe "given the user is anonymous" do
            before do
              @attachment.account = Account.new
              @user.stubs(is_anonymous_user?: true)
            end

            it "is not allowed" do
              refute @user.can?(:view, @attachment)
            end

            it "is allowed if the upload is inline and private attachments are off" do
              @attachment.account.settings.stubs(private_attachments?: false)
              @attachment.stubs(inline?: true)
              assert @user.can?(:view, @attachment)
            end
          end
        end
      end

      describe "without target user" do
        let(:source) { UploadToken.new }

        it "is allowed if private_attachments is off" do
          @attachment.account.settings.stubs(private_attachments?: false)
          assert @user.can?(:view, @attachment)
        end

        it "is allowed with private_attachments turned off and user is anonymous" do
          @user.stubs(is_anonymous_user?: true)
          @attachment.account.settings.stubs(private_attachments?: false)
          assert @user.can?(:view, @attachment)
        end

        it "is allowed with private_attachments turned on and user is anonymous" do
          @user.stubs(is_anonymous_user?: true)
          @attachment.account.settings.stubs(private_attachments?: true)
          assert @user.can?(:view, @attachment)
        end

        describe_with_arturo_enabled :view_attachments_without_target_and_private_attachments_on do
          it "is allowed if private_attachments is off" do
            @attachment.account.settings.stubs(private_attachments?: false)
            assert @user.can?(:view, @attachment)
          end

          it "is allowed if private_attachments are on and user is not anonymous" do
            @user.stubs(is_anonymous_user?: false)
            @attachment.account.settings.stubs(private_attachments?: true)
            assert @user.can?(:view, @attachment)
          end

          it "is not allowed if private_attachments are on and user is anonymous" do
            @user.stubs(is_anonymous_user?: true)
            @attachment.account.settings.stubs(private_attachments?: true)
            refute @user.can?(:view, @attachment)
          end
        end
      end
    end

    describe "viewing anything else" do
      before { @attachment = Attachment.new(source: User.new) }
      it "always is allowed" do
        assert @user.can?(:view, @attachment)
      end
    end
  end

  private

  def create_light_agent
    Account.any_instance.stubs(:has_permission_sets?).returns(true)

    @light_agent = User.create! do |user|
      user.account = accounts(:minimum)
      user.name = "Prince Light"
      user.email = "prince@example.org"
      user.roles = Role::AGENT.id
    end

    @light_agent.permission_set = PermissionSet.create_light_agent!(accounts(:minimum))
    @light_agent.save!
  end
end
