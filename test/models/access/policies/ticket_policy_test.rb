require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 11

describe 'Access::Policies::TicketPolicy' do
  fixtures :all

  describe Access::Policies::TicketPolicy do
    subject { ticket_policy.edit_tags? }

    let(:user) { users(:minimum_end_user) }
    let(:ticket_policy) { ::Access::Policies::TicketPolicy.new(user) }

    describe '#edit_tags?' do
      describe 'when user is an end user' do
        it { refute subject }
      end

      describe 'when user is an agent' do
        let(:user) { users(:minimum_agent) }

        it { assert subject }
      end
    end

    describe '#edit?' do
      describe 'ticket is new' do
        it 'returns true' do
          ticket = Ticket.new
          assert ticket_policy.edit?(ticket)
        end
      end

      describe 'ticket is not new' do
        before do
          @ticket = tickets(:minimum_1)
        end

        describe 'user is agent' do
          before do
            user.expects(:is_agent?).returns(true)
          end

          it 'passes through to `user.can?`' do
            user.expects(:can?).with(:view, @ticket).returns('bogus')
            assert_equal 'bogus', ticket_policy.edit?(@ticket)
          end
        end

        describe 'user is foreign' do
          before do
            user.expects(:is_agent?).returns(false)
            user.expects(:foreign?).returns(true)
          end

          it 'returns true' do
            assert ticket_policy.edit?(@ticket)
          end
        end

        describe 'user is ticket requester' do
          before do
            user.expects(:is_agent?).returns(false)
            user.expects(:foreign?).returns(false)
            @ticket.requester_id = user.id
          end

          it 'returns true' do
            assert ticket_policy.edit?(@ticket)
          end
        end

        describe 'ticket has organization' do
          before do
            @ticket.organization = organizations(:minimum_organization1)
          end

          describe 'organization does not have shared comments' do
            before do
              @ticket.organization.shared_comments = false
            end

            it 'returns false' do
              refute ticket_policy.edit?(@ticket)
            end
          end

          describe 'organization does have shared comments' do
            before do
              @ticket.organization.shared_comments = true
            end

            describe 'user is not in organization' do
              it 'returns false' do
                user.organizations.delete_all
                refute ticket_policy.edit?(@ticket)
              end
            end

            describe 'user is in organization' do
              it 'returns true' do
                assert ticket_policy.edit?(@ticket)
              end
            end
          end
        end

        describe 'ticket does not have comment' do
          before do
            @ticket.comment = nil
          end

          it 'returns false' do
            refute ticket_policy.edit?(@ticket)
          end
        end

        describe 'ticket has a comment' do
          before do
            @comment = Comment.new
            @ticket.comment = @comment
          end

          describe 'ticket is via twitter' do
            before do
              @comment.via_id = ::Channels::Constants::ViaType.TWITTER
            end

            describe 'user has a twitter identity' do
              before do
                UserTwitterIdentity.any_instance.stubs(:twitter_user_profile).returns(true)
              end

              it 'returns true' do
                assert ticket_policy.edit?(@ticket)
              end
            end

            describe 'user does not have a twitter identity' do
              before do
                UserTwitterIdentity.any_instance.stubs(:twitter_user_profile).returns(false)
              end

              it 'returns false' do
                refute ticket_policy.edit?(@ticket)
              end
            end
          end

          describe 'ticket is via facebook' do
            before do
              @comment.via_id = ::Channels::Constants::ViaType.FACEBOOK_POST
            end

            describe 'user has a facebook identity' do
              it 'returns true' do
                assert ticket_policy.edit?(@ticket)
              end
            end

            describe 'user does not have a facebook identity' do
              before do
                user.identities.facebook.delete_all
              end

              it 'returns false' do
                refute ticket_policy.edit?(@ticket)
              end
            end
          end

          describe 'ticket is via AnyChannel' do
            before do
              @comment.via_id = ::Channels::Constants::ViaType.ANY_CHANNEL
            end

            describe 'user has an AnyChannel identity' do
              it 'returns true' do
                assert ticket_policy.edit?(@ticket)
              end
            end

            describe 'user does not have an AnyChannel identity' do
              before do
                user.identities.any_channel.delete_all
              end

              it 'returns false' do
                refute ticket_policy.edit?(@ticket)
              end
            end
          end
        end

        describe 'user is collaborator on ticket' do
          before { @ticket.collaborations.build(user: user) }

          it 'calls is_collaborator? on the ticket' do
            @ticket.expects(:is_collaborator?)
            ticket_policy.edit?(@ticket)
          end

          it { assert ticket_policy.edit?(@ticket) }
        end

        describe 'none of the above' do
          it { refute ticket_policy.edit?(@ticket) }
        end
      end
    end

    describe '#create_side_conversation?' do
      subject { ticket_policy.create_side_conversation? }

      describe 'when user is an end user' do
        it { refute subject }
      end

      describe 'when user is an agent' do
        let(:user) { users(:minimum_agent) }

        it { assert subject }
      end
    end
  end
end
