require_relative '../../../support/test_helper'

SingleCov.covered!

describe 'Access::Policies::VoiceCallPolicy' do
  fixtures :all

  describe '#view_dashboard' do
    let(:admin) { users(:minimum_admin) }
    let(:agent) { users(:minimum_agent) }

    describe 'lotus_feature_voice_staff_service_roles arturo enabled' do
      before do
        Arturo.enable_feature!(:lotus_feature_voice_staff_service_roles)
      end

      it 'admin is allowed' do
        assert admin.can?(:view_dashboard, ::Voice::Call)
      end

      it 'agent is not allowed' do
        assert !agent.can?(:view_dashboard, ::Voice::Call)
      end

      describe 'lotus_feature_voice_agent_view_dashboard arturo enabled' do
        before do
          Arturo.enable_feature!(:lotus_feature_voice_agent_view_dashboard)
        end

        it 'admin is allowed' do
          assert admin.can?(:view_dashboard, ::Voice::Call)
        end

        it 'agent is allowed' do
          assert agent.can?(:view_dashboard, ::Voice::Call)
        end
      end

      describe 'lotus_feature_voice_agent_view_dashboard arturo disabled' do
        before do
          Arturo.disable_feature!(:lotus_feature_voice_agent_view_dashboard)
        end

        it 'admin is allowed' do
          assert admin.can?(:view_dashboard, ::Voice::Call)
        end

        it 'agent is not allowed' do
          assert !agent.can?(:view_dashboard, ::Voice::Call)
        end
      end
    end

    describe 'lotus_feature_voice_staff_service_roles arturo disabled' do
      before do
        Arturo.disable_feature!(:lotus_feature_voice_staff_service_roles)
      end

      describe 'lotus_feature_voice_agent_view_dashboard arturo enabled' do
        before do
          Arturo.enable_feature!(:lotus_feature_voice_agent_view_dashboard)
        end

        it 'admin is allowed' do
          assert admin.can?(:view_dashboard, ::Voice::Call)
        end

        it 'agent is allowed' do
          assert agent.can?(:view_dashboard, ::Voice::Call)
        end
      end

      describe 'lotus_feature_voice_agent_view_dashboard arturo disabled' do
        before do
          Arturo.disable_feature!(:lotus_feature_voice_agent_view_dashboard)
        end

        it 'admin is allowed' do
          assert admin.can?(:view_dashboard, ::Voice::Call)
        end

        it 'agent is allowed' do
          assert agent.can?(:view_dashboard, ::Voice::Call)
        end
      end
    end
  end
end
