require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 17

describe Access::Policies::AttachmentPolicy do
  fixtures :accounts, :users, :attachments, :tickets
  let(:user) { users(:serialization_agent) }
  let(:attachment_policy) { ::Access::Policies::AttachmentPolicy.new(user) }
  let(:statsd_client) { mock('statsd_client') }

  describe '#create?' do
    let(:account) { accounts(:minimum) }

    describe 'as an agent' do
      let(:user) { users(:minimum_agent) }

      it { assert attachment_policy.create? }
    end

    describe_with_arturo_enabled :sse_prevent_anonymous_uploads do
      describe 'as an anonymous user' do
        before { user.stubs(:is_anonymous_user?).returns(true) }

        let(:user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account) }

        describe 'with an account that does not allow end users to upload attachments' do
          before { user.account.stubs(:is_attaching_enabled?).returns(false) }

          it { refute attachment_policy.create? }
        end

        describe 'with an account that allows anonymous requests' do
          before { user.account.stubs(:is_open?).returns(true) }

          describe 'with private attachments setting enabled' do
            before { user.account.settings.stubs(:private_attachments?).returns(true) }
            it { refute attachment_policy.create? }
          end

          describe 'with private attachments setting disabled' do
            before { user.account.settings.stubs(:private_attachments?).returns(false) }
            it { assert attachment_policy.create? }
          end
        end

        describe 'with an account that does not allow anonymous requests' do
          before { user.account.stubs(:is_open?).returns(false) }

          describe 'with private attachments setting enabled' do
            before { user.account.settings.stubs(:private_attachments?).returns(true) }
            it { refute attachment_policy.create? }
          end

          describe 'with private attachments setting disabled' do
            before { user.account.settings.stubs(:private_attachments?).returns(false) }
            it { refute attachment_policy.create? }
          end
        end
      end

      describe 'as an authenticated end user' do
        let(:user) { users(:minimum_end_user) }

        describe 'when end users cannot upload attachments' do
          before { user.account.stubs(:is_attaching_enabled?).returns(false) }

          it { refute attachment_policy.create? }
        end

        describe 'when end users can upload attachments' do
          before { user.account.stubs(:is_attaching_enabled?).returns(true) }

          it { assert attachment_policy.create? }
        end
      end
    end

    describe_with_arturo_disabled :sse_prevent_anonymous_uploads do
      describe 'as an anonymous user' do
        before { user.stubs(:is_anonymous_user?).returns(true) }

        let(:user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account) }

        describe 'with an account that does not allow end users to upload attachments' do
          before { user.account.stubs(:is_attaching_enabled?).returns(false) }

          it { refute attachment_policy.create? }
        end

        describe 'with private attachments setting enabled' do
          before { user.account.settings.stubs(:private_attachments?).returns(true) }
          it { refute attachment_policy.create? }
        end

        describe 'with private attachments setting disabled' do
          before { user.account.settings.stubs(:private_attachments?).returns(false) }
          it { assert attachment_policy.create? }
        end
      end

      describe 'as an end user' do
        let(:user) { users(:minimum_end_user) }

        describe 'with an account that does not allow end users to upload attachments' do
          before { user.account.stubs(:is_attaching_enabled?).returns(false) }

          it { refute attachment_policy.create? }
        end

        describe 'with private attachments setting enabled' do
          before { user.account.settings.stubs(:private_attachments?).returns(true) }
          it { assert attachment_policy.create? }
        end

        describe 'with private attachments setting disabled' do
          before { user.account.settings.stubs(:private_attachments?).returns(false) }
          it { assert attachment_policy.create? }
        end
      end
    end

    describe_with_arturo_enabled :sse_prevent_anonymous_uploads_log_only do
      describe 'as an anonymous user' do
        before { user.stubs(:is_anonymous_user?).returns(true) }

        let(:user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account) }

        describe 'with an account that does not allow end users to upload attachments' do
          before do
            user.account.stubs(:is_attaching_enabled?).returns(false)
            Rails.logger.stubs(:info)
          end

          it "does not send metrics" do
            Rails.logger.expects(:info).with(anything).never
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('agent_policy_change.upload', tags: anything).never

            attachment_policy.create?
          end
        end

        describe 'with an account that has private attachments enabled' do
          before do
            user.account.settings.stubs(:private_attachments?).returns(true)
            Rails.logger.stubs(:info)
          end

          it "sends metrics" do
            Rails.logger.expects(:info).with("[Agent attachment policy upload] outcome:cannot_upload, reason:private_attachments_enabled")
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('agent_policy_change.upload', tags: ['outcome:cannot_upload', 'reason:private_attachments_enabled']).once

            attachment_policy.create?
          end
        end

        describe 'with private attachments setting disabled' do
          before { user.account.settings.stubs(:private_attachments?).returns(false) }

          describe 'with an account that does not accept anonymous tickets' do
            before do
              user.account.stubs(:is_open?).returns(false)
              Rails.logger.stubs(:info)
            end

            it "sends metrics" do
              Rails.logger.expects(:info).with("[Agent attachment policy upload] outcome:cannot_upload, reason:anonymous_tickets_not_allowed")
              Zendesk::StatsD::Client.any_instance.expects(:increment).with('agent_policy_change.upload', tags: ['outcome:cannot_upload', 'reason:anonymous_tickets_not_allowed']).once

              attachment_policy.create?
            end
          end

          describe 'with an account that accepts anonymous tickets' do
            before do
              user.account.stubs(:is_open?).returns(true)
              Rails.logger.stubs(:info)
            end

            it "sends metrics" do
              Rails.logger.expects(:info).with("[Agent attachment policy upload] outcome:can_upload, reason:correct_settings")
              Zendesk::StatsD::Client.any_instance.expects(:increment).with('agent_policy_change.upload', tags: ['outcome:can_upload', 'reason:correct_settings']).once

              attachment_policy.create?
            end
          end
        end
      end
    end
  end

  describe '#view?' do
    before do
      Zendesk::StatsD::Client.stubs(:new).
        with(namespace: 'attachments').
        returns(statsd_client)
    end

    describe 'with a comment attachment' do
      let(:attachment) { attachments(:serialization_comment_attachment) }

      describe 'with an agent user' do
        describe_with_arturo_disabled :sse_agent_attachment_view_policy do
          it 'returns true' do
            assert attachment_policy.view?(attachment)
          end

          it 'does not send agent_policy_change metrics' do
            statsd_client.expects(:increment).with('agent_policy_change', anything).never

            attachment_policy.view?(attachment)
          end
        end

        describe_with_arturo_enabled :sse_agent_attachment_view_policy do
          let(:user) { users(:minimum_agent) }
          let(:ticket) { tickets(:minimum_1) }
          let(:attachment) { ::Attachment.new(account: user.account, source: Comment.new(ticket: ticket, body: 'foo')) }

          describe 'with private attachments setting enabled' do
            before do
              user.account.settings.private_attachments = true
              user.account.settings.save!
            end

            describe 'with a ticket that the agent can view' do
              it { assert attachment_policy.view?(attachment) }

              describe_with_arturo_enabled :sse_agent_attachment_view_policy_log_only do
                before do
                  Rails.logger.stubs(:info)
                  Rails.logger.expects(:info).with("[Agent attachment policy] outcome:can_view, reason:can_view_ticket, attachment_id: #{attachment.id}, ticket_id: #{ticket.id}")

                  statsd_client.expects(:increment).with('agent_policy_change', tags: ['outcome:can_view', 'reason:can_view_ticket'])
                end

                it 'sends agent_policy_change metrics' do
                  attachment_policy.view?(attachment)
                end
              end
            end

            describe 'with a ticket that the agent cannot view' do
              before do
                admin = users(:minimum_admin)
                ticket.update_column :assignee_id, admin.id

                user.update_column :restriction_id, RoleRestrictionType.ASSIGNED
              end

              it { refute attachment_policy.view?(attachment) }

              describe_with_arturo_enabled :sse_agent_attachment_view_policy_log_only do
                before do
                  Rails.logger.stubs(:info)
                  Rails.logger.expects(:info).with("[Agent attachment policy] outcome:cannot_view, reason:inadequate_permissions, attachment_id: #{attachment.id}, ticket_id: #{ticket.id}")

                  statsd_client.expects(:increment).with('agent_policy_change', tags: ['outcome:cannot_view', 'reason:inadequate_permissions'])
                end

                it 'sends agent_policy_change metrics' do
                  attachment_policy.view?(attachment)
                end
              end
            end
          end

          describe 'with private attachments setting disabled' do
            before do
              user.account.settings.private_attachments = false
              user.account.settings.save!
            end

            it { assert attachment_policy.view?(attachment) }

            describe_with_arturo_enabled :sse_agent_attachment_view_policy_log_only do
              describe 'with a ticket that the agent cannot view' do
                before do
                  admin = users(:minimum_admin)
                  ticket.update_column :assignee_id, admin.id

                  user.update_column :restriction_id, RoleRestrictionType.ASSIGNED

                  Rails.logger.stubs(:info)
                  Rails.logger.expects(:info).with("[Agent attachment policy] outcome:can_view, reason:private_attachments_not_enabled, attachment_id: #{attachment.id}, ticket_id: #{ticket.id}")
                  statsd_client.expects(:increment).with('agent_policy_change', tags: ['outcome:can_view', 'reason:private_attachments_not_enabled'])
                end

                it 'sends agent_policy_change metrics' do
                  attachment_policy.view?(attachment)
                end
              end
            end
          end
        end
      end

      describe 'with an unauthenticated user' do
        before do
          user.account.settings.private_attachments = true
          user.account.settings.save!
        end

        # User from a different account to the attachment
        let(:user) { users(:minimum_end_user) }

        it 'returns false' do
          refute attachment_policy.view?(attachment)
        end
      end

      describe 'with an archived ticket' do
        before do
          ticket = attachment.source.ticket
          ticket.archive!
          attachment.ticket_id = ticket.id
          attachment.save!
          attachment.reload
        end

        it 'determines view permission without accessing the archive' do
          ZendeskArchive.router.expects(:get_multi).never
          ZendeskArchive.router.expects(:get).never

          assert attachment_policy.view?(attachment)
        end
      end
    end
  end
end
