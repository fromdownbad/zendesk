require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Access::Policies::RulePolicy do
  include TestSupport::Rule::Helper

  fixtures :all

  let(:agent)            { users(:with_groups_agent1) }
  let(:admin)            { users(:with_groups_admin) }
  let(:end_user)         { users(:with_groups_end_user) }
  let(:restricted_agent) { users(:with_groups_agent_groups_restricted) }

  before do
    Subscription.any_instance.stubs(has_personal_rules?: true)
    Subscription.any_instance.stubs(has_group_rules?: true)

    User.any_instance.stubs(is_light_agent?: false)
  end

  describe 'when accessing triggers' do
    let(:trigger) { rules(:trigger_notify_all_agents_of_received_request) }

    describe 'and the user is not an admin' do
      let(:user) { agent }

      describe '#edit?' do
        it 'is not allowed' do
          refute user.can?(:edit, trigger)
        end
      end

      describe '#view?' do
        describe 'when the trigger is active' do
          it 'is allowed' do
            assert user.can?(:view, trigger)
          end
        end

        describe 'when the trigger is inactive' do
          before { trigger.is_active = false }

          it 'is not allowed' do
            refute user.can?(:view, trigger)
          end
        end

        describe 'when no trigger is provided' do
          let(:trigger) { nil }

          it 'is not allowed' do
            refute user.can?(:view, Trigger)
          end
        end
      end
    end

    describe 'and the user is an admin' do
      let(:user) { admin }

      describe '#edit?' do
        it 'is allowed' do
          assert user.can?(:edit, trigger)
        end
      end

      describe '#view?' do
        describe 'when the trigger is active' do
          it 'is allowed' do
            assert user.can?(:view, trigger)
          end
        end

        describe 'when the trigger is inactive' do
          before { trigger.is_active = false }

          it 'is allowed' do
            assert user.can?(:view, trigger)
          end
        end

        describe 'when no trigger is provided' do
          let(:trigger) { nil }

          it 'is allowed' do
            assert user.can?(:view, Trigger)
          end
        end
      end
    end
  end

  describe 'when accessing trigger categories' do
    let(:account) { user.account }
    let(:trigger_category) { create_category }

    describe 'and the user is not an admin' do
      let(:user) { agent }

      describe '#edit?' do
        it 'is not allowed' do
          refute user.can?(:edit, trigger_category)
        end
      end

      describe '#view?' do
        it 'is allowed' do
          assert user.can?(:view, trigger_category)
        end
      end
    end

    describe 'and the user is an admin' do
      let(:user) { admin }

      describe '#edit?' do
        it 'is allowed' do
          assert user.can?(:edit, trigger_category)
        end
      end

      describe '#view?' do
        it 'is allowed' do
          assert user.can?(:view, RuleCategory)
        end
      end
    end
  end

  describe 'when accessing automations' do
    let(:automation) do
      rules(:automation_close_ticket).tap do |automation|
        automation.account = accounts(:with_groups)
      end
    end

    describe 'and the account does not have unlimited automations' do
      before { Account.any_instance.stubs(has_unlimited_automations?: false) }

      describe 'and the user is not an admin' do
        let(:user) { agent }

        describe '#edit?' do
          it 'is not allowed' do
            refute user.can?(:edit, automation)
          end
        end

        describe '#view?' do
          describe 'when the automation is active' do
            it 'is allowed' do
              assert user.can?(:view, automation)
            end
          end

          describe 'when the automation is inactive' do
            before { automation.is_active = false }

            it 'is not allowed' do
              refute user.can?(:view, automation)
            end
          end

          describe 'when no automation is provided' do
            it 'is not allowed' do
              refute user.can?(:view, Automation)
            end
          end
        end
      end

      describe 'and the user is an admin' do
        let(:user) { admin }

        describe '#edit?' do
          it 'is not allowed' do
            refute user.can?(:edit, automation)
          end
        end

        describe '#view?' do
          it 'is allowed' do
            assert user.can?(:view, automation)
          end

          describe 'when the automation is inactive' do
            before { automation.is_active = false }

            it 'is not allowed' do
              refute user.can?(:view, automation)
            end
          end

          describe 'when no automation is provided' do
            it 'is not allowed' do
              refute user.can?(:view, Automation)
            end
          end
        end
      end
    end

    describe 'and the account has unlimited automations' do
      before { Account.any_instance.stubs(has_unlimited_automations?: true) }

      describe 'and the user is not an admin' do
        let(:user) { agent }

        describe '#edit?' do
          it 'is not allowed' do
            refute user.can?(:edit, automation)
          end
        end

        describe '#view?' do
          describe 'when the automation is active' do
            it 'is allowed' do
              assert user.can?(:view, automation)
            end
          end

          describe 'when the automation is inactive' do
            before { automation.is_active = false }

            it 'is not allowed' do
              refute user.can?(:view, automation)
            end
          end

          describe 'when no automation is provided' do
            it 'is not allowed' do
              refute user.can?(:view, Automation)
            end
          end
        end
      end

      describe 'and the user is an admin' do
        let(:user) { admin }

        describe '#edit?' do
          it 'is allowed' do
            assert user.can?(:edit, automation)
          end
        end

        describe '#view?' do
          it 'is allowed' do
            assert user.can?(:view, automation)
          end
        end
      end
    end
  end

  describe 'when accessing macros' do
    let(:macro) { rules(:active_macro_for_with_groups_group1) }

    describe 'and the user is an end user' do
      let(:user) { end_user }

      describe '#view?' do
        it 'is not allowed' do
          refute user.can?(:view, Macro)
        end
      end

      describe '#edit?' do
        describe 'and the user does not own the rule' do
          it 'is not allowed' do
            refute user.can?(:edit, macro)
          end
        end

        describe 'and the user owns the rule' do
          before { macro.stubs(owner: user) }

          it 'is not allowed' do
            refute user.can?(:edit, macro)
          end
        end
      end

      describe '#manage_personal?' do
        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, Macro)
          end
        end

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, Macro)
          end
        end
      end

      describe '#manage_group?' do
        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, Macro)
          end
        end

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is not allowed' do
            refute user.can?(:manage_group, Macro)
          end
        end
      end

      describe '#manage_shared?' do
        it 'is not allowed' do
          refute user.can?(:manage_shared, Macro)
        end
      end
    end

    describe 'and the user is an agent' do
      let(:user) { agent }

      describe '#view?' do
        it 'is allowed' do
          assert user.can?(:view, Macro)
        end
      end

      describe '#edit?' do
        describe 'and the user does not own the rule' do
          it 'is not allowed' do
            refute user.can?(:edit, macro)
          end
        end

        describe 'and the user owns the rule' do
          before { macro.stubs(owner: user) }

          it 'is allowed' do
            assert user.can?(:edit, macro)
          end
        end
      end

      describe '#manage_personal?' do
        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, Macro)
          end
        end

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is allowed' do
            assert user.can?(:manage_personal, Macro)
          end
        end
      end

      describe '#manage_group?' do
        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, Macro)
          end
        end

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is not allowed' do
            refute user.can?(:manage_group, Macro)
          end
        end
      end

      describe '#manage_shared?' do
        it 'is not allowed' do
          refute user.can?(:manage_shared, Macro)
        end
      end
    end

    describe 'and the user is an admin' do
      let(:user) { admin }

      describe '#view?' do
        it 'is allowed' do
          assert user.can?(:view, Macro)
        end
      end

      describe '#edit?' do
        describe 'and the user does not own the rule' do
          it 'is allowed' do
            assert user.can?(:edit, macro)
          end
        end

        describe 'and the user owns the rule' do
          before { macro.stubs(owner: user) }

          it 'is allowed' do
            assert user.can?(:edit, macro)
          end
        end
      end

      describe '#manage_personal?' do
        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, Macro)
          end
        end

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is allowed' do
            assert user.can?(:manage_personal, Macro)
          end
        end
      end

      describe '#manage_group?' do
        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, Macro)
          end
        end

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is allowed' do
            assert user.can?(:manage_group, Macro)
          end
        end
      end

      describe '#manage_shared?' do
        it 'is allowed' do
          assert user.can?(:manage_shared, Macro)
        end
      end
    end
  end

  describe 'when accessing views' do
    let(:view) { rules(:active_view_for_minimum_account) }

    before { view.account = accounts(:with_groups) }

    describe 'and the account does not have unlimited views' do
      let(:user) { admin }

      before { Account.any_instance.stubs(has_unlimited_views?: false) }

      describe '#view?' do
        it 'is allowed' do
          assert user.can?(:view, view)
        end
      end

      describe '#edit?' do
        it 'is not allowed' do
          refute user.can?(:edit, view)
        end
      end

      describe '#manage_personal?' do
        it 'is not allowed' do
          refute user.can?(:manage_personal, View)
        end
      end

      describe '#manage_group?' do
        it 'is not allowed' do
          refute user.can?(:manage_group, View)
        end
      end

      describe '#manage_shared?' do
        it 'is not allowed' do
          refute user.can?(:manage_shared, View)
        end
      end
    end

    describe 'and the account has unlimited views' do
      before { Account.any_instance.stubs(has_unlimited_views?: true) }

      describe 'and the user is an end user' do
        let(:user) { end_user }

        describe '#view?' do
          it 'is not allowed' do
            refute user.can?(:view, view)
          end
        end

        describe '#edit?' do
          describe 'and the user does not own the rule' do
            it 'is not allowed' do
              refute user.can?(:edit, view)
            end
          end

          describe 'and the user owns the rule' do
            before { view.stubs(owner: user) }

            it 'is not allowed' do
              refute user.can?(:edit, view)
            end
          end
        end

        describe '#manage_personal?' do
          describe 'and the account does not have personal rules' do
            before do
              Subscription.any_instance.stubs(has_personal_rules?: false)
            end

            it 'is not allowed' do
              refute user.can?(:manage_personal, View)
            end
          end

          describe 'and the account has personal rules' do
            before do
              Subscription.any_instance.stubs(has_personal_rules?: true)
            end

            it 'is not allowed' do
              refute user.can?(:manage_personal, View)
            end
          end
        end

        describe '#manage_group?' do
          describe 'and the account does not have group rules' do
            before { Subscription.any_instance.stubs(has_group_rules?: false) }

            it 'is not allowed' do
              refute user.can?(:manage_group, View)
            end
          end

          describe 'and the account has group rules' do
            before { Subscription.any_instance.stubs(has_group_rules?: true) }

            it 'is not allowed' do
              refute user.can?(:manage_group, View)
            end
          end
        end

        describe '#manage_shared?' do
          it 'is not allowed' do
            refute user.can?(:manage_shared, View)
          end
        end
      end

      describe 'and the user is an agent' do
        let(:user) { agent }

        describe '#view?' do
          it 'is allowed' do
            assert user.can?(:view, view)
          end
        end

        describe '#edit?' do
          describe 'and the user does not own the rule' do
            it 'is not allowed' do
              refute user.can?(:edit, view)
            end
          end

          describe 'and the user owns the rule' do
            before { view.stubs(owner: user) }

            it 'is allowed' do
              assert user.can?(:edit, view)
            end
          end
        end

        describe '#manage_personal?' do
          describe 'and the account does not have personal rules' do
            before do
              Subscription.any_instance.stubs(has_personal_rules?: false)
            end

            it 'is not allowed' do
              refute user.can?(:manage_personal, View)
            end
          end

          describe 'and the account has personal rules' do
            before do
              Subscription.any_instance.stubs(has_personal_rules?: true)
            end

            it 'is allowed' do
              assert user.can?(:manage_personal, View)
            end
          end
        end

        describe '#manage_group?' do
          describe 'and the account does not have group rules' do
            before { Subscription.any_instance.stubs(has_group_rules?: false) }

            it 'is not allowed' do
              refute user.can?(:manage_group, View)
            end
          end

          describe 'and the account has group rules' do
            before { Subscription.any_instance.stubs(has_group_rules?: true) }

            it 'is not allowed' do
              refute user.can?(:manage_group, View)
            end
          end
        end

        describe '#manage_shared?' do
          it 'is not allowed' do
            refute user.can?(:manage_shared, View)
          end
        end
      end

      describe 'and the user is an admin' do
        let(:user) { admin }

        describe '#view?' do
          it 'is allowed' do
            assert user.can?(:view, view)
          end
        end

        describe '#edit?' do
          describe 'and the user does not own the rule' do
            it 'is allowed' do
              assert user.can?(:edit, view)
            end
          end

          describe 'and the user owns the rule' do
            before { view.stubs(owner: user) }

            it 'is allowed' do
              assert user.can?(:edit, view)
            end
          end
        end

        describe '#manage_personal?' do
          describe 'and the account does not have personal rules' do
            before do
              Subscription.any_instance.stubs(has_personal_rules?: false)
            end

            it 'is not allowed' do
              refute user.can?(:manage_personal, View)
            end
          end

          describe 'and the account has personal rules' do
            before do
              Subscription.any_instance.stubs(has_personal_rules?: true)
            end

            it 'is allowed' do
              assert user.can?(:manage_personal, View)
            end
          end
        end

        describe '#manage_group?' do
          describe 'and the account does not have group rules' do
            before { Subscription.any_instance.stubs(has_group_rules?: false) }

            it 'is not allowed' do
              refute user.can?(:manage_group, View)
            end
          end

          describe 'and the account has group rules' do
            before { Subscription.any_instance.stubs(has_group_rules?: true) }

            it 'is allowed' do
              assert user.can?(:manage_group, View)
            end
          end
        end

        describe '#manage_shared?' do
          it 'is allowed' do
            assert user.can?(:manage_shared, View)
          end
        end
      end
    end
  end

  describe 'when accessing user views' do
    let(:user_view) { rules(:global_user_view) }

    before { user_view.account = accounts(:with_groups) }

    describe 'and the user is an end user' do
      let(:user) { end_user }

      describe '#view?' do
        it 'is not allowed' do
          refute user.can?(:view, UserView)
        end
      end

      describe '#edit?' do
        describe 'and the user does not own the rule' do
          it 'is not allowed' do
            refute user.can?(:edit, user_view)
          end
        end

        describe 'and the user owns the rule' do
          before { user_view.stubs(owner: user) }

          it 'is not allowed' do
            refute user.can?(:edit, user_view)
          end
        end
      end

      describe '#manage_personal?' do
        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, UserView)
          end
        end

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, UserView)
          end
        end
      end

      describe '#manage_group?' do
        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end
      end

      describe '#manage_shared?' do
        it 'is not allowed' do
          refute user.can?(:manage_shared, UserView)
        end
      end
    end

    describe 'and the user is a restricted agent' do
      let(:user) { restricted_agent }

      describe '#view?' do
        it 'is not allowed' do
          refute user.can?(:view, UserView)
        end
      end

      describe '#edit?' do
        describe 'and the user does not own the rule' do
          it 'is not allowed' do
            refute user.can?(:edit, user_view)
          end
        end

        describe 'and the user owns the rule' do
          before { user_view.stubs(owner: user) }

          it 'is not allowed' do
            refute user.can?(:edit, user_view)
          end
        end
      end

      describe '#manage_personal?' do
        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, UserView)
          end
        end

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, UserView)
          end
        end
      end

      describe '#manage_group?' do
        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end
      end

      describe '#manage_shared?' do
        it 'is not allowed' do
          refute user.can?(:manage_shared, UserView)
        end
      end
    end

    describe 'and the user is an agent' do
      let(:user) { agent }

      describe '#view?' do
        it 'is allowed' do
          assert user.can?(:view, UserView)
        end
      end

      describe '#edit?' do
        describe 'and the user does not own the rule' do
          it 'is not allowed' do
            refute user.can?(:edit, UserView)
          end
        end

        describe 'and the user owns the rule' do
          before { user_view.stubs(owner: user) }

          it 'is allowed' do
            assert user.can?(:edit, user_view)
          end
        end
      end

      describe '#manage_personal?' do
        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, UserView)
          end
        end

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is allowed' do
            assert user.can?(:manage_personal, UserView)
          end
        end
      end

      describe '#manage_group?' do
        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end
      end

      describe '#manage_shared?' do
        it 'is not allowed' do
          refute user.can?(:manage_shared, UserView)
        end
      end
    end

    describe 'and the user is an admin' do
      let(:user) { admin }

      describe '#view?' do
        it 'is allowed' do
          assert user.can?(:view, UserView)
        end
      end

      describe '#edit?' do
        describe 'and the user does not own the rule' do
          it 'is allowed' do
            assert user.can?(:edit, user_view)
          end
        end

        describe 'and the user owns the rule' do
          before { user_view.stubs(owner: user) }

          it 'is allowed' do
            assert user.can?(:edit, user_view)
          end
        end
      end

      describe '#manage_personal?' do
        describe 'and the account does not have personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: false)
          end

          it 'is not allowed' do
            refute user.can?(:manage_personal, UserView)
          end
        end

        describe 'and the account has personal rules' do
          before do
            Subscription.any_instance.stubs(has_personal_rules?: true)
          end

          it 'is allowed' do
            assert user.can?(:manage_personal, UserView)
          end
        end
      end

      describe '#manage_group?' do
        describe 'and the account does not have group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: false) }

          it 'is not allowed' do
            refute user.can?(:manage_group, UserView)
          end
        end

        describe 'and the account has group rules' do
          before { Subscription.any_instance.stubs(has_group_rules?: true) }

          it 'is allowed' do
            assert user.can?(:manage_group, UserView)
          end
        end
      end

      describe '#manage_shared?' do
        it 'is allowed' do
          assert user.can?(:manage_shared, UserView)
        end
      end
    end
  end
end
