require_relative '../../../support/test_helper'

SingleCov.covered!

describe Access::Policies::PostPolicy do
  fixtures :all

  let(:agent)     { users(:minimum_agent) }
  let(:admin)     { users(:minimum_admin) }
  let(:end_user)  { users(:minimum_end_user) }
  let(:post)      { posts(:sticky_reply) }

  before do
    User.any_instance.stubs(is_light_agent?: false)
  end

  describe 'when accessing posts' do
    describe '#manage?' do
      it 'is allowed for admins' do
        assert admin.can?(:manage, post)
      end

      it 'is not allowed for agents' do
        refute agent.can?(:manage, post)
      end

      it 'is not allowed for end users' do
        refute end_user.can?(:manage, post)
      end
    end

    describe '#view?' do
      let(:forum) { post.forum }

      describe 'when the associated forum has been deleted' do
        before do
          forum.soft_delete!
        end

        it 'is not allowed and does not raise an error' do
          end_user.stubs(is_anonymous_user?: true)
          refute end_user.can?(:view, post.reload)
        end
      end
    end

    describe '#edit?' do
      it 'is not allowed for anonymous users' do
        end_user.stubs(is_anonymous_user?: true)
        refute end_user.can?(:edit, post)
      end

      it 'is allowed for moderators' do
        end_user.stubs(is_moderator?: true)
        assert end_user.can?(:edit, post)
      end
    end
  end
end
