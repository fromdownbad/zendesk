require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Event' do
  fixtures :accounts, :account_property_sets, :users, :groups, :memberships, :user_identities, :subscriptions, :payments, :sequences, :tickets, :events, :ticket_fields, :organizations
  describe 'Viewing Events:' do
    subject { @user }

    describe 'an end-user' do
      before do
        @user = users(:minimum_end_user)
      end

      should_eventually "not be able to view a public comment on a ticket belonging to another end-user not in their organization" do
        refute @user.can?(:view, events(:create_comment_for_minimum_ticket_1))
      end

      should_eventually "be able to view a public comment on a ticket belonging to another end-user in their organization" do
        assert @user.can?(:view, _something_)
      end

      should_not_be_able_to(:view, 'a private comment on their own ticket') do
        evt = events(:create_comment_for_minimum_ticket_2)
        evt.stubs(:is_public?).returns(false)
        evt
      end

      should_be_able_to(:view, 'public comment on their own ticket') { events(:create_comment_for_minimum_ticket_2) }
    end

    describe 'an agent' do
      before do
        @user = users(:minimum_agent)
      end

      should_not_be_able_to(:view, 'a public comment on a ticket from another account') { events(:serialization_comment) }

      should_be_able_to(:view, "a private comment on a ticket assigned to another agent") do
        evt = events(:create_comment_for_minimum_ticket_2)
        evt.is_public = false
        evt.ticket.assignee = users(:minimum_admin)
        evt
      end

      describe "using permission sets" do
        before do
          @permission_set = PermissionSet.new
          @user.account.stubs(:has_permission_sets?).returns(true)
          @user.stubs(:permission_set).returns(@permission_set)
        end

        [Audit, Cc, Change, Create, External, OrganizationActivity, MacroReference, Notification].each do |type|
          should_be_able_to(:view, "a public #{type}") do
            type.new(account: @user.account, is_public: true, value_reference: "Hello")
          end
        end

        should_be_able_to(:view, "a FacebookComment") do
          FacebookComment.new(account: @user.account, is_public: true, value_reference: {})
        end
      end

      describe "not using permission sets" do
        before do
          assert_equal false, @user.has_permission_set?
        end

        [Audit, Cc, Change, Create, External, OrganizationActivity, MacroReference, Notification].each do |type|
          should_be_able_to(:view, "a public #{type}") do
            type.new(account: @user.account, is_public: true, value_reference: "Hello")
          end
        end

        should_be_able_to(:view, "a FacebookComment") do
          FacebookComment.new(account: @user.account, is_public: true, value_reference: {})
        end
      end
    end
  end
end
