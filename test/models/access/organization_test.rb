require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Organization' do
  fixtures :users, :organizations, :accounts, :subscriptions

  subject { user }

  describe "Legacy Permissions" do
    describe "An enduser assigned to an organization" do
      let(:organization1) { organizations(:minimum_organization1) }
      let(:organization2) { organizations(:minimum_organization2) }
      let(:organization3) { organizations(:minimum_organization3) }
      let(:user) { users(:minimum_end_user) }

      describe "with end_user_restriction?(:organization) = true" do
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
          user.update_attribute(:organizations, [organization1, organization2])
          user.expects(:end_user_restriction?).with(:organization).returns(true)
        end

        should_be_able_to(:view, 'organization 1') { organization1 }
        should_be_able_to(:view, 'organization 2') { organization2 }
        should_not_be_able_to(:view, 'organization 3') { organization3 }
      end

      describe "that is shared" do
        before do
          user.expects(:end_user_restriction?).with(:organization).returns(false)
          organization1.stubs(:is_shared?).returns(true)
        end

        should_be_able_to(:view, 'organization1') { organization1 }
        should_be_able_to(:watch, 'organization1') { organization1 }
      end

      describe "when the organization is not shared" do
        before { user.organization.update_attributes(is_shared: false) }

        describe "and the user does not have end_user_restriction?(:organization) = true" do
          before { user.expects(:end_user_restriction?).with(:organization).returns(false) }

          should_not_be_able_to(:view, 'the organization') { user.organization }
          should_not_be_able_to(:watch, 'the organization') { user.organization }
        end
      end

      describe "#view_all" do
        it "returns false" do
          refute user.can?(:view_all, Organization)
        end
      end

      describe "#view_all_owned" do
        describe "when end_user_restriction?(:organization) = true" do
          before do
            user.stubs(:end_user_restriction?).with(:organization).returns(true)
          end

          it "returns true" do
            assert user.can?(:view_all_owned, Organization)
          end
        end

        describe "when end_user_restriction?(:organization) = false" do
          before do
            user.stubs(:end_user_restriction?).with(:organization).returns(false)
          end

          it "returns false" do
            refute user.can?(:view_all_owned, Organization)
          end
        end
      end
    end

    describe "An enduser without any organization associations" do
      let(:user) { users(:minimum_author) }
      before { assert_empty(user.organizations) }

      it "is not be able to view other organizations" do
        organizations.each do |organization|
          refute user.can?(:view, organization)
        end
      end

      it "is not be able to watch other organizations" do
        organizations.each do |organization|
          refute user.can?(:watch, organization)
        end
      end
    end

    describe "Admins" do
      let(:user) { users(:minimum_admin) }

      it "is able to view all organizations" do
        organizations.each do |organization|
          assert user.can?(:view, organization)
        end
      end

      it "is able to edit all organizations" do
        organizations.each do |organization|
          assert user.can?(:edit, organization)
        end
      end
    end

    describe "Agents" do
      let(:user) { users(:minimum_agent) }

      it "is able to view all organizations" do
        organizations.each do |organization|
          assert user.can?(:view, organization)
        end
      end

      it "not is able to edit all organizations" do
        organizations.each do |organization|
          refute user.can?(:edit, organization)
        end
      end

      describe "#view_all" do
        describe "when agent_restriction(:organization) = true" do
          before do
            user.stubs(:agent_restriction?).with(:organization).returns(true)
          end

          it "returns false" do
            refute user.can?(:view_all, Organization)
          end
        end

        describe "when agent_restriction(:organization) = false" do
          before do
            user.stubs(:agent_restriction?).with(:organization).returns(false)
          end

          it "returns true" do
            assert user.can?(:view_all, Organization)
          end
        end
      end

      describe "#view_all_owned" do
        it "returns true" do
          assert user.can?(:view_all_owned, Organization)
        end
      end
    end

    describe "restricted" do
      let(:organization2) { organizations(:minimum_organization2) }
      let(:user)          { users(:minimum_agent) }

      before do
        user.update_attributes(
          organization: organization2,
          restriction_id: RoleRestrictionType.ORGANIZATION
        )
      end

      it "is not be able to edit is notes of an organization" do
        organizations.each do |organization|
          refute user.can?(:edit_notes, organization)
        end
      end

      it "is not be able to list organizations" do
        refute user.can?(:list, Organization)
      end

      it "is not be able to view other organizations" do
        refute user.can?(:view, organizations(:minimum_organization1))
      end

      it "is able to view their own organization" do
        assert user.can?(:view, organization2)
      end
    end

    describe "unrestricted" do
      let(:agent) { users(:minimum_agent) }

      it "is able to edit notes of an organization" do
        agent.stubs(:is_non_restricted_agent?).returns(true)

        organizations.each do |organization|
          assert agent.can?(:edit_notes, organization)
        end
      end
    end
  end

  describe "Enterprise Permissions" do
    let(:permission_set) { PermissionSet.new(account: user.account) }
    let(:user)           { users(:minimum_agent) }

    before do
      user.account.stubs(:has_permission_sets?).returns(true)
      user.permission_set = permission_set

      user.update_attribute(:organization, organizations(:minimum_organization1))
    end

    describe "if the user has the 'readonly' permission" do
      before do
        permission_set.permissions.set(end_user_profile: 'readonly')
      end

      it "is able to list organizations" do
        assert user.can? :list, Organization
      end

      it "is able to view any organization" do
        organizations.each do |organization|
          assert user.can?(:view, organization)
        end
      end
    end

    describe "if the user has the 'edit-within-org' permission" do
      before do
        permission_set.permissions.set(end_user_profile: 'edit-within-org')
      end

      it "is not be able to list organizations" do
        refute user.can? :list, Organization
      end

      it "only able to view own organization" do
        assert user.can?(:view, organizations(:minimum_organization1))
        refute user.can?(:view, organizations(:minimum_organization2))
      end
    end
  end
end
