require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::User' do
  fixtures :users, :accounts, :subscriptions, :organizations, :organization_memberships, :role_settings
  before do
    @end_user = users(:minimum_end_user)
    @agent    = users(:minimum_agent)
    @admin    = users(:minimum_admin)
    @non_owner = users(:minimum_admin_not_owner)
  end

  describe "alias" do
    describe "if the account doesn't have aliasing" do
      it "is not is allowed" do
        refute @agent.can? :set_agent_display_name, @agent
      end
    end

    describe "if the account has aliasing" do
      before do
        Account.any_instance.stubs(:has_agent_display_names?).returns(true)
      end

      describe "with another agent" do
        before do
          @other_agent = @end_user.dup
          @other_agent.stubs(is_agent?: true)
        end

        it "is not allow other agent to change agent display_name" do
          refute @other_agent.can? :set_agent_display_name, @agent
        end
      end

      describe "with an admin" do
        it "allows admin to change agent display_name" do
          assert @admin.can? :set_agent_display_name, @agent
        end
      end

      it "allows agent change own display_name" do
        assert @agent.can? :set_agent_display_name, @agent
      end

      it "allows admin change agent display_name" do
        assert @admin.can? :set_agent_display_name, @agent
      end

      it "is not allow end user to change agent display_name" do
        refute @end_user.can? :set_agent_display_name, @agent
      end

      it "is not allow agent to change end user" do
        refute @agent.can? :set_agent_display_name, @end_user
      end
    end
  end

  describe "#can_modify_user_tags?" do
    it "is not is allowed for end users" do
      refute @end_user.can?(:modify_user_tags, @end_user)
    end

    it "is not is allowed to modify own tags for agents" do
      refute @agent.can?(:modify_user_tags, @agent)
    end

    it "is allowed for end users by non-restricted agents" do
      @agent.stubs(:is_non_restricted_agent?).returns(true)
      assert @agent.can?(:modify_user_tags, @end_user)
    end

    it "is not is allowed for end users by restricted agents" do
      @agent.stubs(:is_non_restricted_agent?).returns(false)
      refute @agent.can?(:modify_user_tags, @end_user)
    end

    it "is always allowed for admins" do
      assert @admin.can?(:modify_user_tags, @end_user)
    end

    it "is not is allowed for other agents by an agent" do
      refute @agent.can?(:modify_user_tags, @agent.dup)
    end

    it "is not is allowed for owners by an admin" do
      refute @non_owner.can?(:modify_user_tags, @admin)
    end

    it "is allowed for owners by themselves" do
      assert @admin.can?(:modify_user_tags, @admin)
    end

    it "is not is allowed for foreign users" do
      @end_user.stubs(:foreign?).returns(true)
      refute @admin.can?(:modify_user_tags, @end_user)
    end
  end

  describe "Legacy Permissions" do
    describe "Viewing the autocomplete list of users" do
      describe "if the user is not an agent" do
        before do
          @agent.stubs(:is_agent?).returns(false)
        end

        it "is not is allowed" do
          refute @agent.can? :lookup, User
        end
      end

      describe "if the user is a restricted agent" do
        before do
          @agent.stubs(:is_restricted_agent?).returns(true)
        end

        it "is allowed" do
          assert @agent.can? :lookup, User
        end
      end

      describe "if the user is an unrestricted agent" do
        it "is allowed" do
          assert @agent.can? :lookup, User
        end
      end
    end

    describe "Viewing the list of users" do
      describe "if the user is not an agent" do
        before do
          @agent.stubs(:is_agent?).returns(false)
        end

        it "is not is allowed" do
          refute @agent.can? :list, User
        end
      end
      describe "if the user is a restricted agent" do
        before do
          @agent.stubs(:is_restricted_agent?).returns(true)
        end

        it "is not is allowed" do
          refute @agent.can? :list, User
        end
      end

      describe "if the user is an unrestricted agent" do
        it "is allowed" do
          assert @agent.can? :list, User
        end
      end
    end

    describe "Searching for users" do
      describe "if the user is not an agent" do
        before do
          @agent.stubs(:is_agent?).returns(false)
        end

        it "is not is allowed" do
          refute @agent.can?(:search, User)
        end
      end
      describe "if the user is a restricted agent" do
        before do
          @agent.stubs(:is_restricted_agent?).returns(true)
        end

        it "is allowed" do
          assert @agent.can?(:search, User)
        end
      end

      describe "if the user is an unrestricted agent" do
        it "is allowed" do
          assert @agent.can?(:search, User)
        end
      end
    end

    describe "Editing/Creating/Deleting non-end-users" do
      [:edit, :create, :delete].each do |action|
        describe "for action: #{action} on" do
          describe "Zendesk admins" do
            before do
              @end_user.stubs(:is_zendesk_admin?).returns(true)
              @end_user.stubs(:is_end_user?).returns(false)
              @end_user.stubs(:is_account_owner?).returns(false)
              @end_user.stubs(:foreign?).returns(false)
            end
            it "is allowed if the current user is a Zendesk admin" do
              @agent.expects(:is_zendesk_admin?).at_least_once.returns(true)
              @agent.expects(:is_admin?).at_least_once.returns(true)
              assert @agent.can?(action, @end_user)
            end
            it "is not is allowed if the current is not a Zendesk admin" do
              @agent.expects(:is_zendesk_admin?).at_least_once.returns(false)
              refute @agent.can?(action, @end_user)
            end
          end

          describe "other agents" do
            before do
              @end_user.stubs(:is_zendesk_admin?).returns(false)
              @end_user.stubs(:is_end_user?).returns(false)
              @end_user.stubs(:foreign?).returns(false)
              @end_user.stubs(:is_agent?).returns(true)
            end

            describe "when the current user is an admin" do
              before do
                @agent.stubs(:is_admin?).returns(true)
                @agent.stubs(:is_non_restricted_agent?).returns(true)
              end

              it "is allowed when the other user is not the account owner" do
                @end_user.expects(:is_account_owner?).returns(false)
                assert @agent.can?(action, @end_user)
              end

              it "is denied when the other user is the account owner" do
                @end_user.expects(:is_account_owner?).returns(true)
                refute @agent.can?(action, @end_user)
              end
            end

            describe "when the current user is not an admin" do
              before do
                @agent.stubs(:is_admin?).returns(false)
                @agent.stubs(:is_non_restricted_agent?).returns(true)
              end
              it "is denied" do
                refute @agent.can?(action, @end_user)
              end
            end
          end

          describe "foreign users" do
            before do
              @end_user.stubs(:is_end_user?).returns(false)
              @end_user.stubs(:is_zendesk_admin?).returns(false)
              @end_user.stubs(:foreign?).returns(true)
            end

            it "is denied" do
              refute @agent.can?(action, @end_user)
            end
          end
        end
      end
    end

    describe "Editing yourself" do
      before do
        @agent.stubs(:id).returns(1234567890)
      end

      it "is allowed" do
        assert @agent.can?(:edit, @agent)
      end

      describe "changing your permissions" do
        before do
          @agent_copy = @agent.dup
          @agent_copy.restriction_id = RoleRestrictionType.ORGANIZATION
          @agent.restriction_id = RoleRestrictionType.GROUPS
        end

        it "is not allowed" do
          refute @agent.can?(:edit, @agent_copy)
        end
      end
    end

    describe "Editing/Creating end-users" do
      before do
        @end_user.stubs(:is_zendesk_admin?).returns(false)
        @end_user.stubs(:is_end_user?).returns(true)
        @end_user.stubs(:foreign?).returns(false)
      end

      describe "when the current user is an end user" do
        before { @agent.stubs(:is_end_user?).returns(true) }
        it "is denied creating" do
          refute @agent.can?(:create, @end_user)
        end

        describe "when current user is a new record, and is the same as other_user" do
          it "is allowed creating" do
            user = accounts(:minimum).users.new
            user.roles = Role::END_USER.id

            assert user.can?(:create, user)
          end
        end

        describe "when current user is anonymous" do
          before do
            @agent.stubs(:is_end_user?).returns(true)
            @agent.stubs(:is_anonymous_user?).returns(true)
          end

          it "is allowed creating" do
            assert @agent.can?(:create, @end_user)
          end
        end
      end

      describe "when the current user is a non restricted agent" do
        before { @agent.stubs(:is_non_restricted_agent?).returns(true) }
        it "is allowed" do
          assert @agent.can?(:edit, @end_user)
          assert @agent.can?(:create, @end_user)
        end

        it 'is not is allowed to edit foreign users' do
          @end_user.stubs(:foreign?).returns(true)
          refute @agent.can?(:edit, @end_user)
        end

        it 'is allowed to delete foreign users' do
          @end_user.stubs(:foreign?).returns(true)
          assert @agent.can?(:delete, @end_user)
        end

        describe "agent changing an admin role" do
          before do
            @admin.role = 'End-user'
          end

          it 'is not allowed to change the user role' do
            refute @agent.can?(:edit, @admin)
          end
        end
      end

      describe "when the current user is a restricted agent" do
        before do
          @end_user = @end_user.dup # need a new record to test this properly
          @agent.stubs(:is_non_restricted_agent?).returns(false)
        end
        it "is denied editing" do
          refute @agent.can?(:edit, @end_user)
        end

        describe "for an organization-restricted agent" do
          describe_with_arturo_disabled :restrict_user_policy do
            before do
              @agent.expects(:agent_restriction?).with(:organization).returns(true).at_least_once
            end

            it "is allowed creating if the new user has no org and current has no org" do
              @agent.expects(organization_ids: [])
              @end_user.organization = nil
              @end_user.organization_memberships.clear
              assert @agent.can?(:create, @end_user)
            end

            it "is allowed creating if the new user has no org" do
              @agent.expects(organization_ids: [1234])
              @end_user.organization = nil
              @end_user.organization_memberships.clear
              assert @agent.can?(:create, @end_user)
            end

            it "is allowed creating if the new user is in the same org as this user" do
              @agent.expects(organization_ids: [@end_user.organization_id])
              assert @agent.can?(:create, @end_user)
            end

            it "is allowed creating if the new user is in the secondary org as this user" do
              @agent.expects(organization_ids: [1234, @end_user.organization_id])
              assert @agent.can?(:create, @end_user)
            end

            it "is not allowed creating if the new user is not in the same org as this user" do
              @end_user.expects(organization_ids_with_new: [2345, 3456])
              @agent.expects(organization_ids: [1234])
              refute @agent.can?(:create, @end_user)
            end

            it "is not is allowed creating if the new user has one organization that is not in this users orgs" do
              @end_user.expects(organization_ids_with_new: [@agent.organization_id, 1234])
              @agent.expects(organization_ids: [@agent.organization_id])
              refute @agent.can?(:create, @end_user)
            end
          end

          describe_with_arturo_enabled :restrict_user_policy do
            it "is not allowed to create new users" do
              refute @agent.can?(:create, @end_user)
            end
          end
        end

        describe "for assignee and group restricted agents" do
          before { @agent.stubs(:is_non_restricted_agent?).returns(false) }

          describe_with_arturo_enabled :restrict_user_policy do
            it "is not allowed to create new users" do
              refute @agent.can?(:create, @end_user)
            end
          end

          describe_with_arturo_disabled :restrict_user_policy do
            before { @agent.expects(:agent_restriction?).with(:organization).returns(false) }

            it "is allowed to create new users" do
              assert @agent.can?(:create, @end_user)
            end
          end
        end
      end
    end

    describe "Editing an Answer Bot user" do
      before do
        @end_user.stubs(:answer_bot?).returns(true)
      end

      it "is denied" do
        refute @agent.can?(:edit, @end_user)
      end
    end

    describe "Assuming" do
      describe "yourself" do
        before do
          @agent.stubs(:id).returns(1234567890)
          @end_user.stubs(:id).returns(1234567890)
          @agent.abilities.expects(:defer?).with(:edit, @end_user).never
        end
        it "is denied" do
          refute @agent.can?(:assume, @end_user)
        end
      end
      describe "other users" do
        before { @end_user.expects(:id).returns(1234567890) }
        [true, false].each do |restriction|
          it "defers to #can?(:edit) (#{restriction})" do
            @agent.abilities.expects(:defer?).with(:edit, @end_user).returns(restriction)
            assert_equal restriction, @agent.can?(:assume, @end_user)
          end
        end
      end
    end

    describe "Managing identities" do
      describe "when remote authentication is enabled" do
        before do
          @end_user.stubs(:login_allowed?).with(:remote).returns(true)
        end
        describe "and when the user is an agent" do
          before { @agent.stubs(:is_agent?).returns(true) }
          it "is allowed" do
            assert @agent.can?(:manage_identities_of, @end_user)
          end
        end

        describe "and when the user is not an agent" do
          before { @agent.stubs(:is_agent?).returns(false) }
          it "is denied" do
            refute @agent.can?(:manage_identities_of, @end_user)
          end
        end
      end

      describe "when remote authentication is not enabled" do
        before do
          @end_user.stubs(:login_allowed?).with(:remote).returns(false)
        end
        [true, false].each do |restriction|
          it "defers to #can?(:edit, user) (#{restriction})" do
            @agent.abilities.expects(:defer?).with(:edit, @end_user).returns(restriction)
            assert_equal restriction, @agent.can?(:manage_identities_of, @end_user)
          end
        end
      end

      describe "between different kinds of users" do
        before do
          @admin.stubs(:login_allowed?).with(:remote).returns(false)
        end

        it "is not allow privilege escalation" do
          assert @agent.can?(:manage_identities_of, @end_user)
          assert @admin.can?(:manage_identities_of, @end_user)
          assert @admin.can?(:manage_identities_of, @agent)
          refute   @agent.can?(:manage_identities_of, @admin)
          refute   @end_user.can?(:manage_identities_of, @admin)
          refute   @end_user.can?(:manage_identities_of, @agent)
        end
      end
    end

    describe "Managing external accounts" do
      before do
        account = mock
        @end_user.stubs(:account).returns(account)
      end

      describe "without permissions sets" do
        before do
          assert_equal false, @agent.has_permission_set?
        end

        describe "and when the user is an agent" do
          before { @agent.stubs(:is_agent?).returns(true) }

          [true, false].each do |restriction|
            it "defers to #can?(:edit, user) (#{restriction})" do
              @agent.abilities.expects(:defer?).with(:edit, @end_user).returns(restriction)
              assert_equal restriction, @agent.can?(:manage_external_accounts_of, @end_user)
            end
          end
        end

        describe "and when the user is not an agent" do
          before { @agent.stubs(:is_agent?).returns(false) }

          [true, false].each do |restriction|
            it "defers to #can?(:edit, user) (#{restriction})" do
              @agent.abilities.expects(:defer?).with(:edit, @end_user).returns(restriction)
              assert_equal restriction, @agent.can?(:manage_external_accounts_of, @end_user)
            end
          end
        end
      end

      describe "with permission sets" do
        before do
          @agent.stubs(:has_permission_set?).returns(true)
          @agent.permission_set = PermissionSet.new
          @agent.permission_set.permissions.end_user_profile = 'full'
          assert @agent.is_agent?
        end

        it "is not be able to manage external accounts" do
          assert_equal false, @agent.can?(:manage_external_accounts_of, @agent)
          assert_equal false, @agent.can?(:manage_external_accounts_of, @end_user)
        end
      end
    end

    describe "Editing notes" do
      describe "for non-restricted agents" do
        before do
          @agent.stubs(:is_agent?).returns(true)
          @agent.stubs(:is_non_restricted_agent?).returns(true)
        end
        it "is allowed" do
          assert @agent.can?(:edit_notes, @end_user)
        end
      end

      describe "for restricted agents" do
        before do
          @agent.stubs(:is_agent?).returns(true)
          @agent.stubs(:is_non_restricted_agent?).returns(false)
        end
        it "is not is allowed" do
          refute @agent.can?(:edit_notes, @end_user)
        end
      end

      describe "for non agents" do
        before do
          @agent.stubs(:is_agent?).returns(false)
        end
        it "is denied" do
          refute @agent.can?(:manually_verify_identities_of, @end_user)
        end
      end
    end

    describe "Editing organization memberships" do
      before do
        @organization_membership = organization_memberships(:minimum)
      end

      describe "for agents" do
        before do
          @agent.stubs(:is_agent?).returns(true)
        end

        it "is allowed" do
          assert @agent.can?(:edit, @organization_membership)
        end
      end

      describe "for non agents" do
        before do
          @agent.stubs(:is_agent?).returns(false)
        end

        it "is denied" do
          refute @agent.can?(:edit, @organization_membership)
        end
      end
    end

    describe "Legacy Identity verification" do
      before do
        @owner = users(:minimum_admin)
      end

      describe "for agents" do
        before do
          @agent.stubs(:is_agent?).returns(true)
        end
        it "is allowed" do
          assert @agent.can?(:manually_verify_identities_of, @end_user)
        end

        describe "looking at an owner" do
          before do
            assert @owner.is_account_owner?
          end

          it "is not is able to verify now" do
            refute @agent.can?(:verify_now, @owner)
          end

          it "is able to send verification e-mail" do
            assert @agent.can?(:send_verification_email, @owner)
          end
        end

        describe "looking at another user" do
          it "is able to verify now" do
            assert @agent.can?(:verify_now, @end_user)
          end

          it "is able to send verification e-mail" do
            assert @agent.can?(:send_verification_email, @end_user)
          end
        end
      end

      describe "for non agents" do
        before do
          @non_agent = users(:minimum_end_user)
          refute @non_agent.is_agent?
        end

        it "is denied" do
          refute @non_agent.can?(:manually_verify_identities_of, @end_user)
        end

        it "not is able to send verification e-mail" do
          refute @non_agent.can?(:send_verification_email, @end_user)
        end
      end

      describe "for owners" do
        before do
          assert @owner.is_account_owner?
        end

        it "is not is able to verify now" do
          refute @owner.can?(:verify_now, @owner)
        end

        it "is able to send verification e-mail" do
          assert @owner.can?(:send_verification_email, @owner)
        end
      end
    end

    describe 'Given an account in which agents can create end users' do
      before do
        @account = accounts(:minimum)
        @account.agents_can_create_users = true
      end

      describe 'Agents' do
        before do
          @agent.account = @account
          assert @agent.is_agent?
        end

        it 'is able to create end users' do
          assert @agent.can?(:create_new, User)
        end
      end

      describe 'Admins' do
        before do
          @agent = users(:minimum_admin)
          @agent.account = @account
        end

        it 'is able to create end users' do
          assert @agent.can?(:create_new, User)
        end
      end
    end

    describe 'Given an account in which agents can not create end users' do
      before do
        @account = accounts(:minimum)
        @account.agents_can_create_users = false
      end

      describe 'Agents' do
        before do
          @agent.account = @account
          assert @agent.is_agent?
        end

        it 'not is able to create end users' do
          refute @agent.can?(:create_new, User)
        end
      end

      describe 'Admins' do
        before do
          @agent = users(:minimum_admin)
          @agent.account = @account
        end

        it 'is able to create end users' do
          assert @agent.can?(:create_new, User)
        end
      end
    end
  end

  describe "Enterprise Permissions" do
    before do
      @permission_set = PermissionSet.new
      @agent.account.stubs(:has_permission_sets?).returns(true)
      @agent.stubs(:permission_set).returns(@permission_set)
    end

    describe "Editing user tags" do
      it "is allowed for end users by agents who can edit user profiles" do
        @agent.permission_set.permissions.end_user_profile = 'full'
        assert @agent.can?(:modify_user_tags, @end_user)
      end

      it "is not is allowed for end users by agents who cannot edit user profiles" do
        @agent.permission_set.permissions.end_user_profile = 'readonly'
        refute @agent.can?(:modify_user_tags, @end_user)
      end
    end

    describe "Viewing the autocomplete list of users" do
      describe "if the user is not an agent" do
        before do
          @agent.stubs(:is_agent?).returns(false)
        end

        it "is not is allowed" do
          refute @agent.can? :lookup, User
        end
      end

      describe "if the user is an agent" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
        end

        describe "if the user has the 'readonly' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'readonly')
          end

          it "is allowed" do
            assert @agent.can? :lookup, User
          end
        end

        describe "if the user has the 'edit' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'edit')
          end

          it "is allowed" do
            assert @agent.can? :lookup, User
          end
        end

        describe "if the user has the 'full' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'full')
          end

          it "is allowed" do
            assert @agent.can? :lookup, User
          end
        end
      end
    end

    describe "Viewing the list of users" do
      describe "if the user is not an agent" do
        before do
          @agent.stubs(:is_agent?).returns(false)
        end

        it "is not is allowed" do
          refute @agent.can?(:list, User)
        end
      end

      describe "if the user is an agent" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
        end

        describe "if the user has 'available_user_lists' permission set to 'none'" do
          before do
            @permission_set.permissions.set(available_user_lists: 'none')
          end

          it "is denied" do
            refute @agent.can?(:list, User)
          end
        end

        describe "if the user has 'available_user_lists' permission set to 'all'" do
          before do
            @permission_set.permissions.set(available_user_lists: 'all')
          end

          it "is allowed" do
            assert @agent.can?(:list, User)
          end
        end
      end
    end

    describe "Editing non-end-users" do
      describe "Zendesk admins" do
        before do
          @end_user.stubs(:is_zendesk_admin?).returns(true)
          @end_user.stubs(:is_end_user?).returns(false)
          @end_user.stubs(:is_account_owner?).returns(false)
          @end_user.stubs(:foreign?).returns(false)
        end
        it "is allowed if the current user is a Zendesk admin" do
          @agent.expects(:is_zendesk_admin?).at_least_once.returns(true)
          @agent.expects(:is_admin?).at_least_once.returns(true)
          assert @agent.can?(:edit, @end_user)
        end
        it "is not is allowed if the current is not a Zendesk admin" do
          @agent.expects(:is_zendesk_admin?).at_least_once.returns(false)
          refute @agent.can?(:edit, @end_user)
        end
      end

      describe "other agents" do
        before do
          @end_user.stubs(:is_zendesk_admin?).returns(false)
          @end_user.stubs(:is_end_user?).returns(false)
          @end_user.stubs(:foreign?).returns(false)
        end

        describe "#can_manage_people?" do
          it "allows the agent to manage people when can_manage_people_permission_check is enabled" do
            Account.any_instance.stubs(:has_can_manage_people_permission_check?).returns(true)
            assert @agent.can?(:manage_people, @end_user)
          end
          it "denies the agent to manage people when can_manage_people_permission_check disabled" do
            Account.any_instance.stubs(:has_can_manage_people_permission_check?).returns(false)
            refute @agent.can?(:manage_people, @end_user)
          end
          it "denies the agent to manage people when can_manage_people_permission_check enabled and edit org disabled" do
            Account.any_instance.stubs(:has_can_manage_people_permission_check?).returns(true)
            @agent.permission_set.permissions.disable(:edit_organizations)
            refute @agent.can?(:manage_people, @end_user)
          end
        end

        describe "when the current user is an admin" do
          before do
            @agent.stubs(:is_admin?).returns(true)
            @agent.stubs(:is_non_restricted_agent?).returns(true)
          end
          it "is allowed when the other user is not the account owner" do
            @end_user.expects(:is_account_owner?).returns(false)
            assert @agent.can?(:edit, @end_user)
          end
          it "is denied when the other user is the account owner" do
            @end_user.expects(:is_account_owner?).returns(true)
            refute @agent.can?(:edit, @end_user)
          end
        end

        describe "when the current user is not an admin" do
          before do
            @agent.stubs(:is_admin?).returns(false)
            @agent.stubs(:is_non_restricted_agent?).returns(true)
          end
          it "is denied" do
            refute @agent.can?(:edit, @end_user)
          end
        end
      end

      describe "foreign users" do
        before do
          @end_user.stubs(:is_end_user?).returns(false)
          @end_user.stubs(:is_zendesk_admin?).returns(false)
          @end_user.stubs(:foreign?).returns(true)
        end

        it "is denied" do
          refute @agent.can?(:edit, @end_user)
        end
      end
    end

    describe "Editing end-users" do
      describe "end users" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          @end_user.stubs(:is_zendesk_admin?).returns(false)
          @end_user.stubs(:is_end_user?).returns(true)
          @end_user.stubs(:foreign?).returns(false)
        end

        describe "when the agent has the 'readonly' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'readonly')
          end

          it "is not is allowed" do
            refute @agent.can? :edit, @end_user
          end
        end

        describe "when the agent has the 'edit-within-org' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'edit-within-org')
          end

          subject { @agent }

          should_be_able_to(:edit, 'end users in their organization') do
            @agent.organizations << @end_user.organization
            @end_user
          end

          should_not_be_able_to(:edit, "end users not in their organization") do
            refute_equal @end_user.organization, @agent.organization
            @end_user
          end
        end

        describe "when the agent has the 'edit' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'edit')
          end

          it "is allowed" do
            assert @agent.can? :edit, @end_user
          end

          it 'is not be able to edit foreign users' do
            @end_user.stubs(:foreign?).returns(true)
            refute @agent.can?(:edit, @end_user)
          end
        end

        describe "when the agent has the 'full' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'full')
          end

          it "is allowed" do
            assert @agent.can? :edit, @end_user
          end

          it 'is not be able to edit foreign users' do
            @end_user.stubs(:foreign?).returns(true)
            refute @agent.can?(:edit, @end_user)
          end

          it 'is able to delete foreign users' do
            @end_user.stubs(:foreign?).returns(true)
            refute @agent.can?(:delete, @end_user)
          end
        end
      end
    end

    describe "Editing yourself" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
        user.stubs(:id).returns(1234567890)
        other_user.stubs(:id).returns(1234567890)

        user.stubs(has_permission_set?: true)
        user.stubs(permission_set: @permission_set)
      end

      describe "end_user.can?(:edit, end_user)" do
        let(:user) { @end_user }
        let(:other_user) { @end_user }

        %w[readonly edit-within-org edit full].each do |permission|
          describe "for agents with the #{permission} permission" do
            before do
              @permission_set.permissions.set(end_user_profile: permission)
            end

            it "is allowed" do
              assert user.can?(:edit, other_user)
            end
          end
        end
      end

      describe "end_user.can?(:edit, agent)" do
        let(:user) { @end_user }
        let(:other_user) { @agent }

        %w[readonly edit-within-org edit full].each do |permission|
          describe "for agents with the #{permission} permission" do
            before do
              @permission_set.permissions.set(end_user_profile: permission)
            end

            it "is not is allowed" do
              refute user.can?(:edit, other_user)
            end
          end
        end
      end

      describe "end_user.can?(:edit, admin)" do
        let(:user) { @end_user }
        let(:other_user) { @admin }

        %w[readonly edit-within-org edit full].each do |permission|
          describe "for agents with the #{permission} permission" do
            before do
              @permission_set.permissions.set(end_user_profile: permission)
            end

            it "is not is allowed" do
              refute user.can?(:edit, other_user)
            end
          end
        end
      end

      describe "agent.can?(:edit, admin)" do
        let(:user) { @agent }
        let(:other_user) { @admin }

        %w[readonly edit-within-org edit full].each do |permission|
          describe "for agents with the #{permission} permission" do
            before do
              @permission_set.permissions.set(end_user_profile: permission)
            end

            it "is not is allowed" do
              refute user.can?(:edit, other_user)
            end
          end
        end
      end

      describe "agent.can?(:edit, agent)" do
        let(:user) { @agent }
        let(:other_user) { @agent }

        %w[readonly edit-within-org edit full].each do |permission|
          describe "for agents with the #{permission} permission" do
            before do
              @permission_set.permissions.set(end_user_profile: permission)
            end

            it "is allowed" do
              assert user.can?(:edit, other_user)
            end
          end
        end
      end

      describe "agent.can?(:edit, agent) while changing permissions" do
        let(:user) { @agent }
        let(:other_user) { @agent.dup }

        %w[readonly edit-within-org edit].each do |permission|
          describe "for agents with the #{permission} permission" do
            before do
              other_user = @agent.dup
              other_user.stubs(permission_set: @permission_set.dup)
              other_user.permission_set.permissions.set(end_user_profile: 'full')
              @permission_set.permissions.set(end_user_profile: permission)
            end

            it "is not allowed" do
              refute user.can?(:edit, other_user)
            end
          end
        end
      end

      describe "agent.can?(:edit, end_user)" do
        let(:user) { @agent }
        let(:other_user) { @end_user }

        %w[readonly edit-within-org edit full].each do |permission|
          describe "for user with the #{permission} permission" do
            before do
              @permission_set.permissions.set(end_user_profile: permission)
            end

            it "is not allowed" do
              refute user.can?(:edit, other_user)
            end
          end
        end
      end
    end

    describe "Assuming" do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
      end

      describe "yourself" do
        ['readonly', 'edit-within-org', 'edit', 'full'].each do |permission|
          describe "when the agent has the #{permission} permission" do
            before do
              @permission_set.permissions.set(end_user_profile: permission)
            end

            it "is denied" do
              refute @agent.can?(:assume, @agent)
            end
          end
        end
      end

      describe "other users" do
        ['readonly', 'edit-within-org'].each do |permission|
          describe "when the agent has the #{permission} permission" do
            before { @permission_set.permissions.set(end_user_profile: permission) }

            it "is not is allowed" do
              refute @agent.can?(:assume, @end_user)
            end
          end
        end

        ['edit', 'full'].each do |permission|
          describe "when the agent has the #{permission} permission" do
            before { @permission_set.permissions.set(end_user_profile: permission) }

            [true, false].each do |restriction|
              it "defers to #can?(:edit) (#{restriction})" do
                @agent.abilities.expects(:defer?).with(:edit, @end_user).returns(restriction)
                assert_equal restriction, @agent.can?(:assume, @end_user)
              end
            end
          end
        end

        describe "when in the same org" do
          describe "when the agent has edit-within-org permission" do
            before { @permission_set.permissions.set(end_user_profile: 'edit-within-org') }

            it "is allowed" do
              @agent.organizations << @end_user.organization
              assert @agent.can?(:assume, @end_user)
            end
          end
        end
      end
    end

    describe "Managing identities" do
      describe "and when the user is an agent" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          @agent.stubs(:is_agent?).returns(true)
        end

        describe "if the agent has the 'readonly' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'readonly')
          end

          it "is not is allowed" do
            refute @agent.can?(:manage_identities_of, @end_user)
          end

          it "is allowed on themselves" do
            assert @agent.can?(:manage_identities_of, @agent)
          end
        end

        describe "when the agent has the 'edit-within-org' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'edit-within-org')
          end

          it "is allowed for end users in their organization" do
            @agent.organizations << @end_user.organization
            assert @agent.can? :manage_identities_of, @end_user
          end

          it "is allowed when neither agent nor end user has an organization" do
            @agent.organization = nil
            @agent.save!
            @end_user.organization = nil
            @end_user.save!

            assert @agent.can? :manage_identities_of, @end_user
          end

          it "is not is allowed for end users not in their organization" do
            @end_user.organization_id = 1
            @agent.organization_id = 2
            refute @agent.can? :manage_identities_of, @end_user
          end
        end

        describe "if the agent has the 'edit' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'edit')
          end

          it "is allowed" do
            assert @agent.can?(:manage_identities_of, @end_user)
          end
        end

        describe "if the agent has the 'full' permission" do
          before do
            @permission_set.permissions.set(end_user_profile: 'full')
          end

          it "is allowed" do
            assert @agent.can?(:manage_identities_of, @end_user)
          end
        end

        describe "looking at an owner" do
          before do
            @owner = users(:minimum_admin)
            assert @owner.is_account_owner?
          end

          it "is not is able to verify now" do
            refute @agent.can?(:verify_now, @owner)
          end

          it "is able to send a verification e-mail" do
            assert @agent.can?(:send_verification_email, @owner)
          end
        end

        describe "looking at another user" do
          it "description" do
            assert @agent.can?(:verify_now, @end_user)
          end

          it "is able to send a verification e-mail" do
            assert @agent.can?(:send_verification_email, @end_user)
          end
        end
      end

      describe "and when the user is not an agent" do
        before { @agent.stubs(:is_agent?).returns(false) }
        it "is denied" do
          refute @agent.can?(:manage_identities_of, @end_user)
        end
      end

      describe "when the current user is the owner" do
        before do
          @owner = users(:minimum_admin)
          assert @owner.is_account_owner?
        end

        describe "looking at the owner" do
          it "is not is able to verify now" do
            refute @owner.can?(:verify_now, @owner)
          end

          it "is able to send a verification e-mail" do
            assert @owner.can?(:send_verification_email, @owner)
          end
        end
      end
    end

    describe "Editing notes" do
      describe "on users" do
        [true, false].each do |restriction|
          it "defers to #can?(:edit) (#{restriction})" do
            @agent.abilities.expects(:defer?).with(:edit, @end_user).returns(restriction)
            assert_equal restriction, @agent.can?(:edit_notes, @end_user)
          end
        end
      end
    end

    describe "Identity verification" do
      describe "for agents" do
        before do
          @agent.stubs(:is_agent?).returns(true)
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
        end

        it "is allowed" do
          assert @agent.can?(:manually_verify_identities_of, @end_user)
        end
      end

      describe "for non agents" do
        before do
          @agent.stubs(:is_agent?).returns(false)
        end
        it "is denied" do
          refute @agent.can?(:manually_verify_identities_of, @end_user)
        end
      end
    end

    describe "Editing organization memberships" do
      before do
        @organization_membership = organization_memberships(:minimum)
      end

      describe "if the user has the 'full' permission" do
        before do
          @agent.permission_set.permissions.enable(:edit_organizations)
          assert @agent.permission_set.permissions.edit_organizations?
        end

        describe "for agents" do
          before do
            @agent.stubs(:is_agent?).returns(true)
          end

          it "is allowed" do
            assert @agent.can?(:edit, @organization_membership)
          end
        end

        describe "for light agents" do
          before do
            @agent.stubs(:is_light_agent?).returns(true)
            @agent.stubs(:is_agent?).returns(true)
          end

          it "is denied" do
            refute @agent.can?(:edit, @organization_membership)
          end
        end

        describe "for non agents" do
          before do
            @agent.stubs(:is_agent?).returns(false)
          end

          it "is denied" do
            refute @agent.can?(:edit, @organization_membership)
          end
        end
      end

      describe "if the user has the 'readonly' permission" do
        before do
          @agent.permission_set.permissions.disable(:edit_organizations)
          refute @agent.permission_set.permissions.edit_organizations?
        end

        describe "for agents" do
          before do
            @agent.stubs(:is_agent?).returns(true)
          end

          it "is allowed" do
            assert @agent.can?(:edit, @organization_membership)
          end
        end

        describe "for light agents" do
          before do
            @agent.stubs(:is_light_agent?).returns(true)
            @agent.stubs(:is_agent?).returns(true)
          end

          it "is denied" do
            refute @agent.can?(:edit, @organization_membership)
          end
        end

        describe "for non agents" do
          before do
            @agent.stubs(:is_agent?).returns(false)
          end

          it "is denied" do
            refute @agent.can?(:edit, @organization_membership)
          end
        end
      end
    end

    describe 'Given an agent who can not edit end users' do
      before do
        @agent.permission_set.permissions.end_user_profile = 'readonly'
      end

      describe 'The agent' do
        it 'is not is able to create new users' do
          refute @agent.can?(:create_new, User)
        end
      end
    end

    describe 'Given an agent with organization access to end users' do
      before do
        @agent.permission_set.permissions.end_user_profile = 'edit-within-org'
      end

      describe 'The agent' do
        it 'is able to create new users' do
          assert @agent.can?(:create_new, User)
        end
      end
    end

    describe 'Given an agent with edit access to end users' do
      before do
        @agent.permission_set.permissions.end_user_profile = 'edit'
      end

      describe 'The agent' do
        it 'is able to create new users' do
          assert @agent.can?(:create_new, User)
        end
      end
    end

    describe 'Given an agent with full access to end users' do
      before do
        @agent.permission_set.permissions.end_user_profile = 'full'
      end

      describe 'The agent' do
        it 'is able to create new users' do
          assert @agent.can?(:create_new, User)
        end
      end
    end

    describe 'Given an admin' do
      before do
        @agent = users(:minimum_admin)
      end

      it 'is able to create new users' do
        assert @agent.can?(:create_new, User)
      end
    end
  end

  describe "#role_or_permission" do
    before do
      @enduser        = users(:minimum_end_user)
      @agent          = users(:minimum_agent)
      @admin          = users(:minimum_admin)
      @permission_set = PermissionSet.create(account: accounts(:minimum), name: "things")
    end

    it "gets the role or permission_set for an enterprise user" do
      @agent.permission_set = @permission_set

      assert_equal @permission_set.id, @agent.role_or_permission_set
      assert_equal 'end_user', @enduser.role_or_permission_set
      assert_equal 'admin', @admin.role_or_permission_set
    end

    it "gets the role for a non-enterprise user" do
      assert_equal 'agent', @agent.role_or_permission_set
      assert_equal 'end_user', @enduser.role_or_permission_set
      assert_equal 'admin', @admin.role_or_permission_set
    end

    it "sets the role or permission_sets for enterprise user " do
      @enduser.role_or_permission_set = @permission_set.id
      @agent.role_or_permission_set = 'admin'
      @admin.role_or_permission_set = 'end_user'

      assert_equal @permission_set.id, @enduser.role_or_permission_set
      assert_equal 'admin', @agent.role_or_permission_set
      assert_equal 'end_user', @admin.role_or_permission_set
    end
  end

  describe "#role_change_denied?" do
    describe "changing user roles" do
      describe "as an admin" do
        let(:admin) { stub(user: @admin).extend(Access::UserCommon) }
        let(:other_admin) { @admin.dup }

        before do
          other_admin.role = 'End-user'
        end

        it "is allowed to change the role of another admin" do
          refute admin.send(:role_change_denied?, other_admin)
        end
      end

      describe "as an agent" do
        let(:agent) { stub(user: @agent).extend(Access::UserCommon) }

        before { @admin.role = 'End-user' }

        it "is not allowed to change the role of admins" do
          assert agent.send(:role_change_denied?, @admin)
        end
      end

      describe "as an end-user" do
        let(:end_user) { stub(user: @end_user).extend(Access::UserCommon) }

        before { @admin.role = 'End-user' }

        it "is not allowed to change the role of admins" do
          assert end_user.send(:role_change_denied?, @admin)
        end
      end
    end
  end
end
