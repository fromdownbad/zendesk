require_relative "../../support/test_helper"

SingleCov.not_covered! # covers policies and permissions

describe 'Abilities::Cms' do
  fixtures :accounts, :account_property_sets, :users

  describe "Cms Abilities" do
    before do
      @agent = users(:minimum_agent)
      @permission_set = PermissionSet.new
      @permission_set.permissions.enable(:manage_dynamic_content)
      User.any_instance.stubs(:is_light_agent?).returns(false)
    end

    describe "Cms availability" do
      describe "for end users" do
        it "is is not is allowed" do
          end_user = users(:minimum_end_user)
          refute end_user.can?(:manage, ::Cms::Text)
          refute end_user.can?(:manage, ::Cms::Variant)
        end
      end

      describe "for agents without permission sets" do
        it "is is not is allowed" do
          refute @agent.can?(:manage, ::Cms::Text)
          refute @agent.can?(:manage, ::Cms::Variant)
        end
      end

      describe "for agents with permission sets" do
        before do
          @agent.stubs(:permission_set).returns(@permission_set)
          @agent.account.expects(:has_permission_sets?).returns(true)
        end

        it "is allowed if they have the manage_dynamic_content permission" do
          assert @agent.can?(:manage, ::Cms::Text)
          assert @agent.can?(:manage, ::Cms::Variant)
        end

        it "is is is not is allowed if they don't have the manage_dynamic_content permission" do
          @permission_set.permissions.disable(:manage_dynamic_content)
          refute @agent.can?(:manage, ::Cms::Text)
          refute @agent.can?(:manage, ::Cms::Variant)
        end
      end

      describe "for admins" do
        before do
          @admin = users(:minimum_admin)
        end

        it "is allowed" do
          assert @admin.can?(:manage, ::Cms::Text)
          assert @admin.can?(:manage, ::Cms::Variant)
        end
      end
    end
  end
end
