require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe FraudScore do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  should belong_to(:account)
  should serialize(:other_params)
  should validate_presence_of(:account_id)

  score_params_hash = {"free_mail_domain" => false}

  it 'allows multiple records with the same account_id' do
    FactoryBot.create(:fraud_score, account_id: account.id)
    FactoryBot.create(:fraud_score, account_id: account.id)
    assert_equal 2, FraudScore.where(account_id: account.id).count
  end

  describe '#notify_voice' do
    before do
      Zendesk::StatsD::Client.any_instance.stubs(:histogram)
    end

    describe_with_arturo_enabled :voice_block_risky do
      it 'notifies voice of talk_risky' do
        Voice::SendFraudScoreJob.expects(:enqueue).with(account.id, true).once
        FactoryBot.create(:fraud_score, account_id: account.id, actions: {talk_risky: true})
      end

      it 'logs the account creation delta' do
        Zendesk::StatsD::Client.any_instance.expects(:histogram).with(
          'account_creation_delta',
          kind_of(Float),
          tags: ['source_event:trial_signup']
        ).once
        FactoryBot.create(:fraud_score, account_id: account.id, actions: {talk_risky: true}, source_event: Fraud::SourceEvent::TRIAL_SIGNUP)
      end
    end

    describe_with_arturo_disabled :voice_block_risky do
      it 'does not notify voice of talk_risky' do
        Voice::SendFraudScoreJob.expects(:enqueue).with(account.id, true).never
        FactoryBot.create(:fraud_score, account_id: account.id, actions: {talk_risky: true})
      end

      it 'does not log the account creation delta' do
        Zendesk::StatsD::Client.any_instance.expects(:histogram).never
        FactoryBot.create(:fraud_score, account_id: account.id, actions: {talk_risky: true}, source_event: Fraud::SourceEvent::TRIAL_SIGNUP)
      end
    end
  end

  describe 'verified_fraud validation' do
    it 'should not accept non-boolean values' do
      [nil, ''].each do |verified_fraud_value|
        fraud_score = FraudScore.new(account_id: account.id, verified_fraud: verified_fraud_value)
        fraud_score.valid?
        assert fraud_score.errors[:verified_fraud].present?
      end
    end

    it 'should accept booleans and boolean-like values' do
      [true, false, '0', '1', 0, 1].each do |verified_fraud_value|
        fraud_score = FraudScore.new(account_id: account.id, verified_fraud: verified_fraud_value)
        fraud_score.valid?
        assert_empty fraud_score.errors[:verified_fraud]
      end
    end
  end

  describe 'is_whitelisted validation' do
    it 'should not accept non-boolean values' do
      [nil, ''].each do |is_whitelisted_value|
        fraud_score = FraudScore.new(account_id: account.id, is_whitelisted: is_whitelisted_value)
        fraud_score.valid?
        assert fraud_score.errors[:is_whitelisted].present?
      end
    end

    it 'should accept booleans and boolean-like values' do
      [true, false, '0', '1', 0, 1].each do |is_whitelisted_value|
        fraud_score = FraudScore.new(account_id: account.id, is_whitelisted: is_whitelisted_value)
        fraud_score.valid?
        assert_empty fraud_score.errors[:is_whitelisted]
      end
    end
  end

  describe '#auto_suspension?' do
    describe 'when source event is a trial signup' do
      describe 'and verified fraud' do
        before do
          @fraud_score = FraudScore.new(account_id: account.id, verified_fraud: true, source_event: Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'is an auto suspension' do
          assert @fraud_score.auto_suspension?
        end
      end

      describe 'and not verified fraud' do
        before do
          @fraud_score = FraudScore.new(account_id: account.id, verified_fraud: false, source_event: Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'is not an auto suspension' do
          refute @fraud_score.auto_suspension?
        end
      end
    end

    describe 'when source event is not a trial signup' do
      describe 'and verified fraud' do
        before do
          @fraud_score = FraudScore.new(account_id: account.id, verified_fraud: true, source_event: Fraud::SourceEvent::TAKEN_OVER_ACCOUNT)
        end

        it 'is not an auto suspension' do
          refute @fraud_score.auto_suspension?
        end
      end
    end
  end

  describe '#manual_suspension?' do
    it 'returns false if not manual suspension source event' do
      fraud_score = FraudScore.new(account_id: account.id, verified_fraud: true, source_event: Fraud::SourceEvent::TRIAL_SIGNUP)
      refute fraud_score.manual_suspension?
    end

    it 'returns true if manual suspension source event' do
      fraud_score = FraudScore.new(account_id: account.id, verified_fraud: true, source_event: Fraud::SourceEvent::MANUAL_SUSPENSION)
      assert fraud_score.manual_suspension?
    end
  end

  describe '#free_mail?' do
    it 'returns true if no free_mail_domain score params' do
      fraud_score = FraudScore.new(account_id: account.id, verified_fraud: true, source_event: Fraud::SourceEvent::TRIAL_SIGNUP)
      assert fraud_score.free_mail?
    end

    it 'returns true if no free_mail action' do
      fraud_score = FraudScore.new(account_id: account.id, verified_fraud: true, source_event: Fraud::SourceEvent::TRIAL_SIGNUP, score_params: {free_mail_domain: false})
      assert fraud_score.free_mail?
    end

    it 'returns true if free_mail action is true' do
      fraud_score = FraudScore.new(
        account_id: account.id, verified_fraud: true, source_event: Fraud::SourceEvent::TRIAL_SIGNUP,
        score_params: {'free_mail_domain' => false}, actions: {free_mail: true}
      )
      assert fraud_score.free_mail?
    end

    it 'returns false if free_mail action is false' do
      fraud_score = FraudScore.new(
        account_id: account.id, verified_fraud: true,
        source_event: Fraud::SourceEvent::TRIAL_SIGNUP,
        score_params: {'free_mail_domain' => false}, actions: {free_mail: false}
      )
      refute fraud_score.free_mail?
    end

    it 'returns true if free_mail_domain param is true' do
      fraud_score = FraudScore.new(
        account_id: account.id, verified_fraud: true, source_event: Fraud::SourceEvent::TRIAL_SIGNUP,
        score_params: {'free_mail_domain' => true}, actions: {free_mail: false}
      )
      assert fraud_score.free_mail?
    end
  end

  describe '#risk_score' do
    describe 'when insights_risk_score is missing from score_params' do
      it 'returns nil if no insights_risk_score' do
        fraud_score = FraudScore.new(account_id: account.id, source_event: Fraud::SourceEvent::SHELL_CREATED, score_params: {})
        assert_nil fraud_score.risk_score
      end
    end

    describe 'when insights_risk_score contains a score' do
      it 'returns the score' do
        fraud_score = FraudScore.new(account_id: account.id, source_event: Fraud::SourceEvent::SHELL_CREATED, score_params: {'insights_risk_score' => 67})
        assert_equal fraud_score.risk_score, 67
      end
    end
  end

  describe '#minfraud_id' do
    describe 'when minfraud_id contains an minfraud id' do
      it 'returns the score' do
        fraud_score = FraudScore.new(account_id: account.id, source_event: Fraud::SourceEvent::MANUAL_SUSPENSION, score_params: {'minfraud_id' => "c1bfca90-b37d-45dc-a63d-d1203bd26154"})
        assert_equal fraud_score.minfraud_id, "c1bfca90-b37d-45dc-a63d-d1203bd26154"
      end
    end

    describe 'when minfraud_id is missing from score_params id' do
      it 'returns the score' do
        fraud_score = FraudScore.new(account_id: account.id, source_event: Fraud::SourceEvent::MANUAL_SUSPENSION, score_params: {'minfraud_id' => ""})
        assert_equal fraud_score.minfraud_id, ""
      end
    end
  end

  describe 'other_params serialization' do
    describe 'attribute readers' do
      before do
        fraud_score_hash = {
            other_params: {subdomain: 'subdomain1', owner_email: 'test@example.com', score_params: score_params_hash, source_event: Fraud::SourceEvent::TRIAL_SIGNUP },
            service_response: {
              actions: {auto_suspend: true},
              reasons: {auto_suspend: ['Bangkok Essentials']}
            }
        }
        @model = FraudScore.new(fraud_score_hash)
      end

      it 'works for subdomain' do
        assert_equal 'subdomain1', @model.subdomain
      end

      it 'works for owner_email' do
        assert_equal 'test@example.com', @model.owner_email
      end

      it 'works for score_params' do
        assert_equal score_params_hash, @model.score_params
      end

      it 'works for source_event' do
        assert_equal :trial_signup, @model.source_event
      end

      it 'works for service response actions' do
        assert_kind_of Hash, @model.actions
        assert(@model.actions[:auto_suspend])
      end

      it 'works for service response reasons' do
        assert_kind_of Hash, @model.reasons
        assert_equal ['Bangkok Essentials'], @model.reasons[:auto_suspend]
      end
    end

    describe 'attribute setters' do
      before { @model = FraudScore.new }

      it 'works for subdomain' do
        @model.subdomain = 'subdomain2'
        assert_equal 'subdomain2', @model.subdomain
      end

      it 'works for owner_email' do
        @model.owner_email = 'test2@example.com'
        assert_equal 'test2@example.com', @model.owner_email
      end

      it 'works for source_event' do
        @model.source_event = Fraud::SourceEvent::TRIAL_SIGNUP
        assert_equal :trial_signup, @model.source_event
      end

      it 'works for score_params' do
        @model.score_params = score_params_hash
        assert_equal score_params_hash, @model.score_params
      end

      it 'works for service_response actions' do
        @model.actions = {'auto_suspend' => true}
        assert_equal({'auto_suspend' => true}, @model.actions)
      end

      it 'works for service_response reasons' do
        @model.reasons = {'auto_suspend' => ['Bangkok Essentials']}
        assert_equal({'auto_suspend' => ['Bangkok Essentials']}, @model.reasons)
      end
    end

    it 'are returned by factory as expected' do
      fraud_score = FactoryBot.create(:fraud_score, account_id: account.id)
      assert_equal 'test@test.com', fraud_score.owner_email
      assert_equal 'test', fraud_score.subdomain
      assert_equal score_params_hash, fraud_score.score_params
      assert_equal Fraud::SourceEvent::TRIAL_SIGNUP, fraud_score.source_event
    end
  end
end
