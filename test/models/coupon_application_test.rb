require_relative "../support/test_helper"
require_relative "../support/billing_test_helper"

SingleCov.covered! uncovered: 2

describe 'CouponApplication' do
  describe 'Belonging to a DiscountCoupon' do
    before do
      Timecop.freeze Time.local(1970, 1, 1, 0, 0, 0)
      @subscription = FactoryBot.build :subscription
      @coupon       = FactoryBot.build :active_discount_coupon
    end

    describe '#is_valid_on?' do
      it 'pass through to :coupon' do
        @coupon_application = CouponApplication.new(coupon: @coupon)
        @coupon.expects(:is_valid_on?).with(@subscription, @coupon_application).returns(true)

        assert @coupon_application.is_valid_on? @subscription
      end
    end

    describe 'exhaustion methods' do
      before do
        @coupon_application = CouponApplication.new(coupon: @coupon)
      end

      describe 'for a new record' do
        it 'calculates #exhaustion_date starting from now' do
          assert_equal Time.now + @coupon.length_days.days, @coupon_application.exhaustion_date
        end

        it 'returns !exhasted?' do
          refute @coupon_application.exhausted?
        end

        it 'returns exhausted for as_of dates past the coupon length' do
          assert @coupon_application.exhausted? Time.now + 10.days + @coupon.length_days.days
        end
      end

      describe 'for an application 10 days ago' do
        before do
          Timecop.travel(Time.now + 10.days)
        end

        it 'calculates #exhaustion_date from the time of application' do
          assert_equal (10.days.ago + @coupon_application.coupon.length_days.days).to_s, @coupon_application.exhaustion_date.to_s
        end

        it 'returns !exhasted?' do
          refute @coupon_application.exhausted?
        end

        it 'returns exhausted for as_of dates past the coupon length' do
          assert @coupon_application.exhausted? @coupon_application.exhaustion_date + 1.day
        end
      end

      describe 'when exhaustion_date is forced by a parameter to new' do
        before do
          @coupon_application = CouponApplication.new(exhaustion_date: '1916-06-05'.to_date)
        end

        it 'returns #exhaustion_date as set' do
          assert_equal Time.local(1916, 6, 5), @coupon_application.exhaustion_date.to_time
        end

        it 'returns exhasted?' do
          assert @coupon_application.exhausted?
        end

        it 'returns !exhausted? for as_of dates when the application was still active' do
          assert @coupon_application.exhausted?(@coupon_application.exhaustion_date + 1.day)
        end
      end
    end

    describe '#configure_discount_calculation' do
      before do
        @calcs = mock
        @coupon.maximum_monthly_discount = 68.99
        @coupon_application = CouponApplication.new(coupon: @coupon,
                                                    initial_full_monthly_price: 42)
      end

      it 'sets coupon inputs on a Discounts calculation helper object' do
        @calcs.expects(:coupon_discount_percentage=).with(20)
        @calcs.expects(:coupon_exhaustion_date=).with(365.days.from_now)
        @calcs.expects(:coupon_excluded_amount=).with(42 * 95 / 30)
        @calcs.expects(:coupon_maximum_monthly_discount=).with(68.99)

        @coupon_application.configure_discount_calculation(@calcs, 95)
      end
    end
  end

  describe 'Belonging to a TrialCoupon' do
    before do
      Timecop.freeze Time.local(1970, 1, 1, 0, 0, 0)
      @subscription = FactoryBot.build :subscription
      @coupon       = FactoryBot.build :active_trial_coupon
    end

    describe '#configure_discount_calculation' do
      before do
        @calcs = mock
        @coupon.maximum_monthly_discount = 8.99
        @coupon_application = CouponApplication.new(coupon: @coupon,
                                                    initial_full_monthly_price: 42)
      end

      it 'does not set any paramters on the Discount' do
        @calcs.expects(:coupon_discount_percentage=).never
        @calcs.expects(:coupon_exhaustion_date=).never
        @calcs.expects(:coupon_excluded_amount=).never
        @calcs.expects(:coupon_maximum_monthly_discount=).never

        @coupon_application.configure_discount_calculation(@calcs, 95)
      end
    end
  end
end
