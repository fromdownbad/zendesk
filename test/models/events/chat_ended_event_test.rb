require_relative "../../support/test_helper"

SingleCov.covered!

describe ChatEndedEvent do
  fixtures :accounts, :tickets

  should have_many(:chat_transcripts)
  should validate_presence_of :ticket
  should validate_presence_of :chat_started_event

  let(:ticket) { tickets(:minimum_1) }
  let(:chat_started_event) { ChatStartedEvent.new(ticket: ticket) }

  describe 'validation' do
    it 'allows proper value' do
      event = ChatEndedEvent.new(chat_started_event: chat_started_event, ticket: ticket, value: {
        'chat_id' => '123',
        'visitor_id' => '456'
      })
      assert_predicate event, :valid?
    end

    it 'checks for required chat_id' do
      event = ChatEndedEvent.new(chat_started_event: chat_started_event, ticket: ticket, value: {
        'visitor_id' => '456'
      })
      assert_predicate event, :invalid?
    end

    it 'checks for required visitor_id' do
      event = ChatEndedEvent.new(chat_started_event: chat_started_event, ticket: ticket, value: {
        'chat_id' => '123'
      })
      assert_predicate event, :invalid?
    end

    it 'does not allow for additional attributes' do
      event = ChatEndedEvent.new(chat_started_event: chat_started_event, ticket: ticket, value: {
        'chat_id' => '123',
        'visitor_id' => '456',
        'foo' => 'bar'
      })
      assert_predicate event, :invalid?
    end
  end

  describe 'ticket subject' do
    before do
      ticket.will_be_saved_by(ticket.requester)
      ticket.via_id = ViaType.CHAT
    end

    it 'uses ancestor implementation if served & returns new subject' do
      ChatEndedEvent.new(chat_started_event: chat_started_event, ticket: ticket, value: { chat_id: '123', visitor_id: '456', is_served: true }).apply!
      assert_equal ::I18n.t('txt.chat.new_conversation_title', visitor_name: ticket.requester.name), ticket.subject
    end

    it 'returns missed chat subject if not served' do
      ChatEndedEvent.new(chat_started_event: chat_started_event, ticket: ticket, value: { chat_id: '123', visitor_id: '456', is_served: false }).apply!
      assert_equal ::I18n.t('txt.chat.missed_conversation_title', visitor_name: ticket.requester.name), ticket.subject
    end
  end

  describe 'tags' do
    let(:event) do
      ChatEndedEvent.new(chat_started_event: chat_started_event, ticket: ticket, value: { chat_id: '123', visitor_id: '456', is_served: true, tags: %w[foo bar] }).tap(&:validate!)
    end

    before do
      ticket.will_be_saved_by(ticket.requester)
      ticket.via_id = ViaType.CHAT
      event.apply!
    end

    describe 'for a served chat' do
      let(:event) do
        ChatEndedEvent.new(chat_started_event: chat_started_event, account: ticket.account, ticket: ticket, value: { chat_id: '123', visitor_id: '123', is_served: true })
      end

      it 'applies ended tags' do
        assert_equal 'hello zopim_chat zopim_chat_ended', ticket.reload.current_tags
      end
    end

    describe 'for a missed chat' do
      let(:event) do
        ChatEndedEvent.new(chat_started_event: chat_started_event, account: ticket.account, ticket: ticket, value: { chat_id: '123', visitor_id: '123', is_served: false })
      end

      it 'applies missed tags' do
        assert_equal 'hello zopim_chat zopim_chat_ended zopim_chat_missed', ticket.reload.current_tags
      end
    end

    describe 'when started tags is subset of ended tags' do
      let(:chat_started_event) do
        ChatStartedEvent.new(ticket: ticket, value: { tags: ['foo'] })
      end

      it 'applies only the remaining tags' do
        # this works because we didn't actually apply the started tags earlier
        # it simulates a situation whereby a started tag was removed before chat ended
        assert_equal 'bar hello zopim_chat zopim_chat_ended', ticket.reload.current_tags
      end
    end

    describe 'when started tags are disjoint from ended tags' do
      let(:chat_started_event) do
        ChatStartedEvent.new(ticket: ticket, value: { tags: ['hello'] })
      end

      it 'removes disjointed tags' do
        # works because ticket has a 'hello' tag applied by default
        assert_equal 'bar foo zopim_chat zopim_chat_ended', ticket.reload.current_tags
      end
    end
  end
end
