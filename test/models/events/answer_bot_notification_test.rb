require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe AnswerBotNotification do
  fixtures :accounts, :events, :users, :tickets, :recipient_addresses, :brands,
    :user_identities, :organizations, :ticket_fields, :ticket_field_entries, :groups,
    :account_settings

  extend ArturoTestHelper

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
    Account.any_instance.stubs(:default_brand).returns(brands(:minimum))
  end
  should_validate_presence_of :recipients
  should_serialize :value

  let(:ticket) { tickets(:minimum_1) }
  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:enduser) { users(:minimum_end_user) }
  let(:deflection_config) do
    {recipient: 'requester_id', subject: 'email subject', body: 'email body'}
  end
  let(:deflection_trigger) do
    d = Definition.new
    d.actions.push(DefinitionItem.new("deflection", "is", deflection_config))
    d.conditions_all.push(DefinitionItem.new("status_id", "is", [StatusType.NEW]))
    Trigger.create!(account: account, title: 'deflection trigger', definition: d, owner: account)
  end
  let(:rule_id) { deflection_trigger.id }
  let(:ccs) { [users(:minimum_end_user2), users(:minimum_author)] }
  let(:notification_event) do
    { recipients: Array(ticket.requester.id), subject: 'test subject', body: 'test body', audit: ticket.audit, ccs: ccs }
  end
  let(:notification) do
    notification = AnswerBotNotification.new(notification_event)
    notification.via_id = ViaType.RULE
    notification.via_reference_id = rule_id
    notification
  end

  describe "sending notifications" do
    it "sets the ticket requester as the recipient" do
      assert(notification.recipients.include?(ticket.requester.id))
    end

    it "returns the notification in the events" do
      ticket.stubs(:requester).returns(enduser)
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << notification
      ticket.save!

      assert(ticket.events.pluck(:type).include?("AnswerBotNotification"))
    end

    describe "with ticket CCs" do
      before do
        ticket.stubs(:requester).returns(enduser)
        ticket.stubs(:email_ccs).returns(ccs)
        ticket.will_be_saved_by(User.system)
        ticket.audit.events << notification
        ticket.save!
      end

      it "adds ticket ccs to the notification event" do
        event_value = ticket.events.where(type: "AnswerBotNotification").last.value

        ccs.each do |user|
          assert_includes event_value, user.id
        end
      end
    end
  end
end
