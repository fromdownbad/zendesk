require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketMergeAudit do
  let(:ticket_merge_audit) { TicketMergeAudit.new(value: "1,2,3,4") }

  should allow_mass_assignment_of(:merge_type) if RAILS4
  should allow_mass_assignment_of(:target_id) if RAILS4

  describe "#source_ids" do
    describe "when the audit is from a target ticket" do
      before { ticket_merge_audit.merge_type = 'target' }

      it "returns an array of ticket ids from the value of the audit" do
        assert_equal [1, 2, 3, 4], ticket_merge_audit.source_ids
      end
    end

    describe "when the audit is from a source ticket" do
      before { ticket_merge_audit.merge_type = 'source' }

      it "returns nil" do
        assert_nil ticket_merge_audit.source_ids
      end
    end
  end
end
