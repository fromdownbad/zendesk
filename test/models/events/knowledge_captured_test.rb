require_relative "../../support/test_helper"

SingleCov.covered!

describe KnowledgeCaptured do
  fixtures :accounts, :tickets, :users

  let(:user) { users(:minimum_agent) }
  let(:ticket) { tickets(:minimum_1) }
  let(:article) { { id: 1, url: 'http://helpcenter.article/1.json', title: 'article title 1', html_url: 'http://helpcenter.article/1', locale: 'en-US' } }
  let(:template) { { id: 1, url: 'http://helpcenter.article/1.json', title: 'article title 1', html_url: 'http://helpcenter.article/1' } }

  it "creates a new event with an article" do
    kcs_capture_create = KnowledgeCaptured.new(article: article, template: template)

    ticket.will_be_saved_by(user)
    ticket.audit.events << kcs_capture_create
    ticket.save!

    assert(ticket.events.pluck(:type).include?("KnowledgeCaptured"))
  end

  it "cannot be created without an article" do
    kcs_capture_create = KnowledgeCaptured.new
    kcs_capture_create.template = template

    refute(kcs_capture_create.valid?)
  end

  it "cannot be created without a template" do
    kcs_capture_create = KnowledgeCaptured.new
    kcs_capture_create.article = article

    refute(kcs_capture_create.valid?)
  end

  it 'is invalid when the article do not have the {id, url, html_url, title, locale} structure' do
    kcs_capture_create = KnowledgeCaptured.new
    kcs_capture_create.article = { id: 1 }
    kcs_capture_create.template = template

    refute(kcs_capture_create.valid?)
  end

  it 'is invalid when the template do not have the {id, url, html_url, title} structure' do
    kcs_capture_create = KnowledgeCaptured.new
    kcs_capture_create.article = article
    kcs_capture_create.template = { id: 1 }

    refute(kcs_capture_create.valid?)
  end

  it 'body retrieves both article and its template details' do
    kcs_capture_create = KnowledgeCaptured.new
    kcs_capture_create.article = article
    kcs_capture_create.template = template

    ticket.will_be_saved_by(user)
    ticket.audit.events << kcs_capture_create
    ticket.save!

    assert_equal({ article: article, template: template }, kcs_capture_create.body)
    assert(kcs_capture_create.body[:article].is_a?(Hash))
    assert(kcs_capture_create.body[:template].is_a?(Hash))
  end
end
