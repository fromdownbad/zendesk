require_relative "../../support/test_helper"

SingleCov.covered!

describe AutomaticAnswerSend do
  fixtures :accounts, :tickets, :users

  resque_inline false
  let(:suggested_articles) do
    [
      { id: 1, url: 'http://helpcenter.article/1.json', title: 'article title 1', html_url: 'http://helpcenter.article/1' },
      { id: 2, url: 'http://helpcenter.article/2.json', title: 'article title 2', html_url: 'http://helpcenter.article/2' },
      { id: 3, url: 'http://helpcenter.article/3.json', title: 'article title 3', html_url: 'http://helpcenter.article/3' }
    ]
  end

  let(:ticket) { tickets(:minimum_1) }
  let(:agent) { users(:minimum_agent) }
  let(:user) { users(:minimum_end_user) }

  let(:automatic_answer_send) do
    send_event = AutomaticAnswerSend.new
    send_event.suggested_articles = suggested_articles
    send_event
  end

  let(:automatic_answer_reject) do
    event = AutomaticAnswerReject.new
    event.reviewer_id = ticket.requester_id
    event.reviewer_name = ticket.requester.name
    event.irrelevant = true
    event.article = suggested_articles.first.slice(:id, :title, :html_url, :url)
    event
  end

  let(:automatic_answer_viewed) do
    event = AutomaticAnswerViewed.new
    event.viewer_id = ticket.requester_id
    event.article = suggested_articles.first.slice(:id, :title, :html_url, :url)
    event
  end

  let(:aa_response) do
    suggested_articles.map { |article| { article: article, reviews: { enduser: nil, agent: [] }, viewed: false } }
  end

  let(:latest_automatic_answer_reject) { automatic_answer_reject.dup }

  it 'build a send event with a list of suggested artices ids' do
    auto_answer_send_event = AutomaticAnswerSend.new
    auto_answer_send_event.suggested_articles = suggested_articles
    assert_equal(suggested_articles, auto_answer_send_event.suggested_articles)
    assert(auto_answer_send_event.suggested_articles)
  end

  it 'is invalid when provided without a list of articles' do
    auto_answer_send_event = AutomaticAnswerSend.new
    refute(auto_answer_send_event.valid?)
  end

  it 'is invalid when any of the suggested articles do not have the {id, url} structure' do
    suggested_articles << { id: 4, url: nil }
    auto_answer_send_event = AutomaticAnswerSend.new
    auto_answer_send_event.suggested_articles = suggested_articles

    refute(auto_answer_send_event.valid?)
  end

  describe '#add_reject_events_to_response' do
    before do
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << automatic_answer_send
      ticket.save!

      ticket.will_be_saved_by(User.system)
      ticket.audit.events << automatic_answer_reject
      ticket.save!
    end

    it 'returns an empty array or nil if there are no reviews' do
      response_with_reviews = automatic_answer_send.send :add_reject_events_to_response, aa_response, []

      assert_equal(0, response_with_reviews.first[:reviews][:agent].length)
      assert_nil(response_with_reviews.first[:reviews][:enduser])
      assert_equal(0, response_with_reviews.second[:reviews][:agent].length)
      assert_nil(response_with_reviews.second[:reviews][:enduser])
      assert_equal(0, response_with_reviews.third[:reviews][:agent].length)
      assert_nil(response_with_reviews.third[:reviews][:enduser])
    end

    it 'includes the reject events when the send.body is called' do
      assert(automatic_answer_send.body.is_a?(Array))
      assert(!automatic_answer_send.body.first[:reviews][:enduser].empty?)
      assert_equal(ticket.requester_id, automatic_answer_send.body.first[:reviews][:enduser][:user_id])
    end

    it 'includes only latest reject event per article by a reviewer when the send.body is called' do
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << latest_automatic_answer_reject
      ticket.save!

      assert(automatic_answer_send.body.is_a?(Array))

      assert_equal(latest_automatic_answer_reject.reviewer_id, automatic_answer_send.body.first[:reviews][:enduser][:user_id])
    end

    it 'includes the reject event by an agent' do
      latest_automatic_answer_reject.reviewer_id = agent.id
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << latest_automatic_answer_reject
      ticket.save!

      assert(automatic_answer_send.body.is_a?(Array))

      assert_equal(latest_automatic_answer_reject.reviewer_id, automatic_answer_send.body.first[:reviews][:agent].last[:user_id])
    end

    it 'includes the reject event by more than one person' do
      latest_automatic_answer_reject.reviewer_id = agent.id

      ticket.will_be_saved_by(User.system)
      ticket.audit.events << latest_automatic_answer_reject
      ticket.save!

      assert(automatic_answer_send.body.is_a?(Array))
      assert(!automatic_answer_send.body.first[:reviews][:enduser].empty?)
      assert_equal(1, automatic_answer_send.body.first[:reviews][:agent].count)
    end

    it "does not include the reject event with false if the false event is later than true" do
      latest_automatic_answer_reject.irrelevant = false

      ticket.will_be_saved_by(User.system)
      ticket.audit.events << latest_automatic_answer_reject
      ticket.save!

      assert(automatic_answer_send.body.is_a?(Array))
      assert_nil(automatic_answer_send.body.first[:reviews][:enduser])
    end

    it 'only includes one last reject event by reviewer and one last reject event by agent for the same article' do
      # Review 1 by ticket requester
      aa_reject = automatic_answer_reject.dup
      aa_reject.reviewer_id = ticket.requester_id
      aa_reject.reason = 1
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject
      ticket.save!

      # Review 1 by an agent
      aa_reject_by_agent = automatic_answer_reject.dup
      aa_reject_by_agent.reviewer_id = agent.id
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject_by_agent
      ticket.save!

      # Review 2 by another agent
      aa_reject_event_agent_2 = automatic_answer_reject.dup
      aa_reject_event_agent_2.reviewer_id = 2
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject_event_agent_2
      ticket.save!

      assert_equal(2, automatic_answer_send.body.first[:reviews][:agent].count)
      assert_equal(ticket.requester_id, automatic_answer_send.body.first[:reviews][:enduser][:user_id])
      assert_equal(TicketDeflectionArticle::REASON_STRINGS[aa_reject.reason], automatic_answer_send.body.first[:reviews][:enduser][:reason])

      agent_ids = automatic_answer_send.body.first[:reviews][:agent].map { |review| review[:user_id] }
      assert(agent_ids.include?(2))
    end

    it 'only includes one last reject event by reviewer and one last reject event by agent for many articles' do
      # Review 1 by ticket requester for article 1
      aa_reject = automatic_answer_reject.dup
      aa_reject.reviewer_id = ticket.requester_id
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject
      ticket.save!

      # Review 1 by an agent for article 1
      aa_reject_by_agent = automatic_answer_reject.dup
      aa_reject_by_agent.reviewer_id = agent.id
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject_by_agent
      ticket.save!

      # Review 2 by another agent for article 1
      aa_reject_event_agent_2 = automatic_answer_reject.dup
      aa_reject_event_agent_2.reviewer_id = 2
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject_event_agent_2
      ticket.save!

      # Review 1 by an agent for article 2
      aa_reject_by_agent_for_article_2 = automatic_answer_reject.dup
      aa_reject_by_agent_for_article_2.article = suggested_articles.second.slice(:html_url, :id, :url, :title)
      aa_reject_by_agent_for_article_2.reviewer_id = agent.id
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject_by_agent_for_article_2
      ticket.save!

      response = automatic_answer_send.body
      agent_ids = automatic_answer_send.body.first[:reviews][:agent].map { |review| review[:user_id] }
      assert(agent_ids.include?(2))
      assert(agent_ids.include?(agent.id))
      assert_equal(ticket.requester_id, response.first[:reviews][:enduser][:user_id])
    end

    describe 'when an article in the reject event does not match a suggested article in the send event' do
      let(:other_article_id) { 123 }
      let(:send_event) do
        send_event = AutomaticAnswerSend.new
        send_event.suggested_articles = suggested_articles
        send_event
      end
      let(:reject_event) do
        reject_event = AutomaticAnswerReject.new
        reject_event.reviewer_id = ticket.requester_id
        reject_event.irrelevant = true
        reject_event.article = suggested_articles.first.slice(:id, :title, :html_url, :url)
        reject_event.value[:article][:id] = other_article_id
        reject_event
      end

      before do
        ticket.audits.delete_all

        ticket.will_be_saved_by(User.system)
        ticket.audit.events << send_event
        ticket.audit.events << reject_event
        ticket.save!
      end

      it 'does not include the review in the response body' do
        response_body = send_event.body
        reviews = response_body.any? { |article| article[:reviews][:enduser] }

        refute reviews
      end
    end
  end

  describe "#find_last_reject_review" do
    it "returns id of the last event reviewer" do
      last_review = automatic_answer_send.send(:find_last_reject_review, [automatic_answer_reject])
      assert_equal automatic_answer_reject.reviewer_id, last_review[:user_id]
    end

    it "returns does not return reviews if the last review has a false irrelevant value" do
      latest_automatic_answer_reject.irrelevant = false
      last_review = automatic_answer_send.send(:find_last_reject_review, [automatic_answer_reject, latest_automatic_answer_reject])
      assert_nil(last_review)
    end

    it "returns nil if there are no reviews" do
      assert_nil(automatic_answer_send.send(:find_last_reject_review, []))
    end
  end

  describe "reject_info_for_article" do
    before do
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << automatic_answer_send
      ticket.save!

      ticket.will_be_saved_by(User.system)
      ticket.audit.events << automatic_answer_reject
      ticket.save!
    end

    it 'returns both requester and agent reviews' do
      ticket.will_be_saved_by(User.system)
      latest_automatic_answer_reject.reviewer_id = agent.id
      ticket.audit.events << latest_automatic_answer_reject
      ticket.save!

      reviews = automatic_answer_send.send(:reject_info_for_article, [latest_automatic_answer_reject, automatic_answer_reject])
      assert_equal(ticket.requester_id, reviews[:enduser][:user_id])
      assert_equal([{ user_id: agent.id, reason: '' }], reviews[:agent])
    end
  end

  describe "#add_viewed_events_to_response" do
    it 'returns true when the article is viewed by the ticket requester' do
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << automatic_answer_viewed
      ticket.save!

      automatic_answer_send.send(:add_viewed_events_to_response, aa_response, [automatic_answer_viewed])
    end

    it 'returns false when the article is not viewed by the ticket requester' do
      automatic_answer_send.send(:add_viewed_events_to_response, aa_response, [])
    end

    describe 'when an article in the viewed event does not match a suggested article in the send event' do
      let(:other_article_id) { 123 }
      let(:send_event) do
        send_event = AutomaticAnswerSend.new
        send_event.suggested_articles = suggested_articles
        send_event
      end
      let(:viewed_event) do
        viewed_event = AutomaticAnswerViewed.new
        viewed_event.viewer_id = ticket.requester_id
        viewed_event.article = suggested_articles.first.slice(:id, :title, :html_url, :url)
        viewed_event.value[:article][:id] = other_article_id
        viewed_event
      end

      before do
        ticket.audits.delete_all

        ticket.will_be_saved_by(User.system)
        ticket.audit.events << send_event
        ticket.audit.events << viewed_event
        ticket.save!
      end

      it 'does not include the viewed event in the response body' do
        response_body = send_event.body
        viewed_articles = response_body.any? { |article| article[:viewed] }

        refute viewed_articles
      end
    end
  end

  describe "#consolidate_agent_rejections" do
    it 'returns latest rejection value when there are multiple reviews by the same agent' do
      # first reject event by agent
      aa_reject_by_agent = automatic_answer_reject.dup
      aa_reject_by_agent.reviewer_id = agent.id
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject_by_agent
      ticket.save!

     # second reject event by agent, which is an undo for the first reject event
      second_aa_reject_by_agent = automatic_answer_reject.dup
      second_aa_reject_by_agent.reviewer_id = agent.id
      second_aa_reject_by_agent.irrelevant = false
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << second_aa_reject_by_agent
      ticket.save!

      result = automatic_answer_send.send(:consolidate_agent_rejections, [aa_reject_by_agent, second_aa_reject_by_agent])
      assert_equal([], result)
    end

    it 'returns all reject events by multiple agents' do
      # first reject event by agent
      aa_reject_by_agent = automatic_answer_reject.dup
      aa_reject_by_agent.reviewer_id = agent.id
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject_by_agent
      ticket.save!

      # second reject event by another agent
      aa_reject_by_second_agent = automatic_answer_reject.dup
      aa_reject_by_second_agent.reviewer_id = 10002
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_reject_by_second_agent
      ticket.save!

      result = automatic_answer_send.send(:consolidate_agent_rejections, [aa_reject_by_agent, aa_reject_by_second_agent])
      assert_equal([{ user_id: aa_reject_by_agent.reviewer_id, reason: '' }, { user_id: aa_reject_by_second_agent.reviewer_id, reason: ''}], result)
    end

    it 'returns empty array when there are no reviews' do
      result = automatic_answer_send.send(:consolidate_agent_rejections, [])
      assert_equal([], result)
    end
  end
end
