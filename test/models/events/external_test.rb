require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe External do
  fixtures :targets, :events
  should_validate_presence_of :value

  describe "aliased attributes" do
    let(:external) { External.new }

    it "alias :value_previous to :success" do
      external.expects(:value_previous)
      external.success
    end

    it "alias :value_reference to :body" do
      external.expects(:value_reference)
      external.body
    end

    it "alias :value to :resource" do
      external.expects(:value)
      external.resource
    end
  end

  describe "target job" do
    let(:target) { targets(:email_valid) }
    let(:audit) { events(:create_audit_for_minimum_ticket_1) }
    let(:external) { External.new(via_id: ViaType.RULE, via_reference_id: 0, resource: target.id, body: "Send and email to target") }

    describe "when the target is valid and active" do
      before { Resque.expects(:enqueue).with(TargetJob, anything, anything, anything) }

      it "enqueues a TargetJob" do
        audit.events << external
      end
    end

    describe "when the target" do
      before { Resque.expects(:enqueue).with(TargetJob, anything, anything, anything).never }

      describe "is not valid" do
        before { EmailTarget.any_instance.stubs(valid?: false, is_active: true) }

        it "does not enqueue a TargetJob" do
          audit.events << external
        end
      end

      describe "is not active" do
        before { EmailTarget.any_instance.stubs(valid?: true, is_active: false) }

        it "does not enqueue a TargetJob" do
          audit.events << external
        end
      end
    end
  end

  describe "trigger cleanup job" do
    let(:target) { targets(:email_valid) }
    let(:audit) { events(:create_audit_for_minimum_ticket_1) }
    let(:external) { External.new(via_id: ViaType.RULE, via_reference_id: 0, resource: target.id, body: "Send and email to target") }

    describe "when the target does not exist" do
      before do
        target.delete
        Resque.expects(:enqueue).with(TriggerCleanupJob, anything, anything, anything, anything)
      end

      it "enqueues a TriggerCleanupJob" do
        audit.events << external
      end
    end

    describe "when the target exists" do
      before do
        Resque.stubs(:enqueue)
        Resque.expects(:enqueue).with(TriggerCleanupJob, anything, anything, anything, anything).never
      end

      it "does not enqueue a TriggerCleanupJob" do
        audit.events << external
      end
    end
  end

  describe "#body" do
    let(:external) { External.new(value_reference: value_reference) }

    describe "when #requires_deserialization? returns true" do
      before { external.stubs(requires_deserialization?: true) }

      describe "and the value is present" do
        let(:value_reference) { "--- \n- - this\n  - that\n" }

        it "returns a YAML deserialized value" do
          assert_equal [["this", "that"]], external.body
        end
      end

      describe "and the value is nil" do
        let(:value_reference) { nil }

        it "returns value_reference" do
          assert_nil external.body
        end
      end
    end

    describe "when #requires_deserialization? returns false" do
      let(:value_reference) { "--- \n- - this\n  - that\n" }
      before { external.stubs(requires_deserialization?: false) }

      it "returns value_reference" do
        assert_equal value_reference, external.body
      end
    end
  end

  describe "#requires_deserialization?" do
    let(:target) { targets(:url_v2) }
    let(:external) { External.new(account: target.account, value: target.id) }

    describe "when the target is a UrlTargetV2" do
      describe "when the target uses parameters" do
        before { target.stubs(uses_parameters?: true) }

        it "returns true" do
          assert external.requires_deserialization?
        end
      end

      describe "when the target does not use parameters" do
        before { UrlTargetV2.any_instance.stubs(uses_parameters?: false) }

        it "returns false" do
          refute external.requires_deserialization?
        end
      end
    end

    describe "when the target is not a UrlTargetV2" do
      before { external.stubs(target: UrlTarget.new) }

      it "returns false" do
        refute external.requires_deserialization?
      end
    end
  end
end
