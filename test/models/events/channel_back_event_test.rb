require_relative "../../support/test_helper"
require_relative "../../support/attachment_test_helper"

SingleCov.covered!

describe ChannelBackEvent do
  fixtures :events, :accounts, :tickets

  before do
    @comment = events(:create_comment_for_minimum_ticket_1)
    @audit   = @comment.audit
  end

  describe "#initiate_channels_conversion" do
    it "is called when a new event is created" do
      event = ChannelBackEvent.new
      event.expects(:initiate_channels_conversion)
      @audit.events << event
    end

    it "calls ReplyInitiator#reply with reply_params" do
      event = ChannelBackEvent.new(source_id: 4, account: accounts(:minimum))

      params = stub('params')
      event.expects(:reply_params).returns(params)
      reply_initiator = stub('reply_initiator')

      ::Channels::Converter::ReplyInitiator.expects(:new).with(params).returns(reply_initiator)
      reply_initiator.expects(:reply)
      event.initiate_channels_conversion
    end
  end

  describe "#reply_params" do
    before do
      @account = accounts(:minimum)
      @event = ChannelBackEvent.new(
        source_id: 4,
        account: @account,
        ticket: @comment.ticket,
        value: { text: 'test text' }
      )
      comment = stub('comment',
        id: 123,
        attachments: [
          stub('attachment', url: 'one', thumbnail?: nil),
          stub('thumbnail', url: 'one', thumbnail?: true)
        ])
      @event.stubs(:comment).returns(comment)
    end

    it "returns a hash with the correct keys" do
      params = @event.reply_params
      assert_equal @event.account_id, params[:account_id]
      assert_equal @event.comment.id, params[:comment_id]
      assert_equal @event.source_id, params[:source_id]
      assert_equal @event.value[:text], params[:text]
      assert_equal @event.ticket_id, params[:ticket_id]
      assert_nil params[:ticket_url] # this might be wrong
      assert_equal [], params[:recipient_ids]
      assert_equal ['one'], params[:attachments]
      refute params[:new_thread]
    end

    it "gets identities from UserAnyChannelIdentities" do
      user = @event.ticket.requester
      user.identities << UserAnyChannelIdentity.new(
        user: user,
        value: '123:abc',
        priority: 1,
        is_verified: true
      ).tap do |uac|
        uac.account = @event.account
      end
      user.identities << UserAnyChannelIdentity.new(
        user: user,
        value: '234:def',
        priority: 1,
        is_verified: true
      ).tap do |uac|
        uac.account = @event.account
      end
      user.save!
      @event.ticket.update_attribute(:via_id, ViaType.ANY_CHANNEL)

      params = @event.reply_params
      assert_equal ['123:abc', '234:def'], params[:recipient_ids]
    end

    it "only sets recipient_ids for AnyChannel tickets" do
      user = @event.ticket.requester
      user.identities << UserAnyChannelIdentity.new(
        user: user,
        value: '123:abc',
        priority: 1,
        is_verified: true
      ).tap do |uac|
        uac.account = @event.account
      end
      @event.ticket.update_attribute(:via_id, ViaType.TWITTER_DM)

      params = @event.reply_params
      assert_empty params[:recipient_ids]
    end
  end

  describe "#channels_prefix" do
    before do
      @event = ChannelBackEvent.new(source_id: 4)
    end

    describe "if ticket is via facebook" do
      before do
        ticket = stub('ticket', via_twitter?: false, via_facebook?: true)
        @event.stubs(:ticket).returns(ticket)

        @event.expects(:facebook_test).with(3, "test", false)
      end

      it "calls facebook_test" do
        @event.channels_prefix(:test, 3, "test", false)
      end
    end

    describe "if ticket is via twitter" do
      before do
        ticket = stub('ticket', via_twitter?: true, via_facebook?: false)
        @event.stubs(:ticket).returns(ticket)

        @event.expects(:twitter_test)
      end

      it "calls twitter_test" do
        @event.channels_prefix(:test)
      end
    end

    describe "if ticket is not via twitter, facebook, or any_channel" do
      before do
        ticket = stub('ticket', via_twitter?: false, via_facebook?: false, via_any_channel?: false)
        @event.stubs(:ticket).returns(ticket)
      end

      it "raises error" do
        assert_raise(RuntimeError) { @event.channels_prefix(:test) }
      end
    end
  end

  # TODO: Generalize test for facebook / twitter
  describe "#to_s" do
    describe "when ticket is via twitter" do
      before do
        @event = ChannelBackEvent.new(
          value: {
            source: {
              name: "Source Name",
              screen_name: "source"
            },
            requester: {
              name: "Requester Name",
              screen_name: "requester",
            }
          },
          source_id: 4
        )

        ticket = stub('ticket', via_twitter?: true, via_facebook?: false)
        @event.stubs(:ticket).returns(ticket)
      end

      it "returns the correct values for Tweets" do
        assert_equal "To Requester Name (<a href=\"http://www.twitter.com/requester\" target=\"blank\">@requester</a>) from Source Name (<a href=\"http://www.twitter.com/source\" target=\"blank\">@source</a>)", @event.to_s
      end
    end

    describe "when ticket is via facebook" do
      before do
        @event = ChannelBackEvent.new(
          value: {
            external_id: 12345678,
            source: {
              external_id: 1234,
              name: 'Facebook Page'
            }
          }
        )

        @ticket = stub('ticket', via_twitter?: false, via_facebook?: true)

        @event.stubs(:ticket).returns(@ticket)
      end

      describe "and ticket is via facebook post" do
        before do
          @ticket.expects(:via?).with(:facebook_post).returns(true)
        end

        it "returns the correct values for Facebook" do
          assert_equal "Commented on <a href='/channels/resources?external_id=12345678&resource_type=facebook_post' target='_blank'>Wall Post</a> as Facebook Page <a href=\"https://www.facebook.com/1234\" target=\"blank\">Facebook Page</a>", @event.to_s
        end
      end

      describe "and ticket is via facebook message" do
        before do
          @ticket.expects(:via?).with(:facebook_post).returns(false)
        end

        it "returns the correct values for Facebook" do
          assert_equal "Replied to <a href='/channels/resources?external_id=12345678&resource_type=facebook_message' target='_blank'>Private Message</a> as Facebook Page <a href=\"https://www.facebook.com/1234\" target=\"blank\">Facebook Page</a>", @event.to_s
        end
      end
    end

    describe "when ticket is via any_channel" do
      before do
        @event = ChannelBackEvent.new(
          value: {
            text: 'This is the Channelback text',
          }
        )

        @ticket = stub('ticket', via_twitter?: false, via_facebook?: false, via_any_channel?: true)

        @event.stubs(:ticket).returns(@ticket)
      end

      it "returns the correct values for AnyChannel" do
        assert_equal "Comment sent to integration service", @event.to_s
      end
    end
  end

  describe "#ticket_url" do
    before do
      @event = ChannelBackEvent.new(source_id: 4)
    end

    describe "ticket is via twitter" do
      before do
        ticket = stub('ticket', via_twitter?: true, twicket_url: 'http://www.hello.com/twickets/1')
        @event.stubs(:ticket).returns(ticket)
      end

      describe "#add_link? returns true" do
        before do
          @event.expects(:add_link?).returns(true)
        end

        it "returns twicket url" do
          assert_equal 'http://www.hello.com/twickets/1', @event.ticket_url
        end
      end

      describe "#add_link? returns false" do
        before do
          @event.expects(:add_link?).returns(false)
        end

        it "returns nil" do
          assert_nil @event.ticket_url
        end
      end
    end

    describe "ticket is not via twitter" do
      before do
        ticket = stub('ticket', via_twitter?: false)
        @event.stubs(:ticket).returns(ticket)
      end

      it "returns nil" do
        assert_nil @event.ticket_url
      end
    end
  end

  describe "#add_link?" do
    before do
      @account = accounts(:minimum)
      @event = ChannelBackEvent.new(source_id: 4, account: @account, ticket: @comment.ticket)
    end

    describe "has_twitter_link_capability is false" do
      before do
        @account.stubs(:has_end_user_ui?).returns(false)
      end

      it "returns false" do
        refute @event.add_link?
      end
    end

    describe "has_twitter_link_capability is true" do
      before do
        @account.stubs(:has_end_user_ui?).returns(true)
      end

      describe "no_short_url_tweet_append account setting is false" do
        before do
          @account.settings.no_short_url_tweet_append = false
          @account.save!
        end

        describe "url_shortener is available" do
          before do
            @url_shortener = stub('url shortener')
            @account.stubs(:url_shortener).returns(@url_shortener)
          end

          describe "url_shortener add_url_by_default is true" do
            before do
              @url_shortener.expects(:add_url_by_default).returns(true)
            end

            it "returns true" do
              assert @event.add_link?
            end
          end

          describe "url_shortener add_url_by_default is false" do
            before do
              @url_shortener.expects(:add_url_by_default).returns(false)
            end

            it "returns true if append_ticket_url is 1" do
              @event.append_ticket_url = "1"
              assert @event.add_link?
            end

            it "returns true if append_ticket_url is true" do
              @event.append_ticket_url = true
              assert @event.add_link?
            end

            it "returns false if append_ticket_url is 0" do
              @event.append_ticket_url = "0"
              refute @event.add_link?
            end
          end
        end

        describe "url_shortener is not available" do
          before do
            @account.stubs(:url_shortener).returns(nil)
          end

          describe "if Arturo twitter_add_link_default_false is enabled" do
            before do
              Arturo.enable_feature! :twitter_add_link_default_false
            end

            it "returns false" do
              refute @event.add_link?
            end
          end

          describe "if Arturo twitter_add_link_default_false is disabled" do
            before do
              Arturo.disable_feature! :twitter_add_link_default_false
            end

            it "returns true" do
              assert @event.add_link?
            end
          end

          describe 'help_center_enabled' do
            before do
              @account.settings.help_center_state = "enabled"
              @account.save!
            end

            it 'returns false if the brand help center state is not enabled' do
              HelpCenter.
                stubs(:find_by_brand_id).
                with(@event.ticket.brand.id).
                returns(HelpCenter.new(id: 42, state: 'archived'))

              refute @event.add_link?
            end

            it 'returns true if the brand help center state is enabled' do
              HelpCenter.
                stubs(:find_by_brand_id).
                with(@event.ticket.brand.id).
                returns(HelpCenter.new(id: 42, state: 'enabled'))

              assert @event.add_link?
            end
          end
        end
      end

      describe "no_short_url_tweet_append account setting is true" do
        before do
          # @account.settings.stubs(:no_short_url_tweet_append?).returns(true)
          settings = stub('settings', no_short_url_tweet_append?: true)
          @account.stubs(:settings).returns(settings)
        end

        it "returns false" do
          refute @event.add_link?
        end
      end
    end
  end

  describe "#new_thread" do
    {true => 't', false => 'f'}.each do |assigned_value, fetched_value|
      describe "when #{assigned_value}" do
        before do
          @event = ChannelBackEvent.new(source_id: 4, new_thread: assigned_value)
        end

        it "should be #{fetched_value}" do
          assert_equal fetched_value, @event.new_thread
        end
      end
    end
  end
end
