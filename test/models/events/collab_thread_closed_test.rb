require_relative "../../support/test_helper"

SingleCov.covered!

describe CollabThreadClosed do
  fixtures :accounts, :tickets, :users

  let(:user) { users(:minimum_1) }
  let(:ticket) do
    ticket = tickets(:minimum_1)
    ticket.will_be_saved_by(User.system)
    ticket
  end
  let(:event) { CollabThreadClosed.new }

  let(:subject) { 'Can anyone in IT help with this?' }
  let(:thread_id) { 'abc123' }
  let(:recipient_count) { 2 }

  it 'creates an event when provided the right parameters' do
    event = CollabThreadClosed.new
    event.subject = subject
    event.thread_id = thread_id
    event.recipient_count = recipient_count

    ticket.audit.events << event
    ticket.save!

    saved_event = ticket.reload.audits.last.events.to_a.find { |e| e.type == 'CollabThreadClosed' }

    assert_equal subject, saved_event.subject
    assert_equal thread_id, saved_event.thread_id
    assert_equal recipient_count, saved_event.recipient_count
  end

  it 'sets the event to private' do
    refute event.is_public?
  end

  it 'sets the correct type' do
    assert_equal 'CollabThreadClosed', event.type
  end

  it 'is invalid when there is no subject' do
    event = CollabThreadClosed.new
    refute event.valid?
    assert event.errors.messages[:subject].present?
  end

  it 'is invalid when there is no thread_id' do
    event = CollabThreadClosed.new
    refute event.valid?
    assert event.errors.messages[:thread_id].present?
  end
end
