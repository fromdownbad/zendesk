require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe AutomaticAnswerViewed do
  fixtures :tickets
  let(:article) { { id: 1, url: 'https://support.zendesk.com/hc/articles/1.json', html_url: 'https://support.zendesk.com/hc/articles/1', title: "article title" } }

  before do
    @ticket = tickets(:minimum_1)
    @ticket.will_be_saved_by(User.system)
  end

  it 'create an viewed event when provided with an article ' do
    viewed_event = AutomaticAnswerViewed.new
    viewed_event.article = article
    viewed_event.viewer_id = 1

    @ticket.audit.events << viewed_event

    assert_equal(article[:id], viewed_event.article[:id])
    assert_equal(article[:url], viewed_event.article[:url])
    assert_equal(article[:html_url], viewed_event.article[:html_url])
    assert_equal(article[:title], viewed_event.article[:title])

    assert(viewed_event.valid?)
  end

  it 'does not create an viewed event when provided without an viewer id' do
    viewed_event = AutomaticAnswerViewed.new
    viewed_event.article = article
    @ticket.audit.events << viewed_event

    assert_equal(article[:id], viewed_event.article[:id])
    assert_equal(article[:url], viewed_event.article[:url])
    assert_equal(article[:html_url], viewed_event.article[:html_url])
    assert_equal(article[:title], viewed_event.article[:title])

    refute viewed_event.valid?
  end

  it 'does not create a viewed event when provided with an article without required fields' do
    viewed_event = AutomaticAnswerViewed.new
    viewed_event.article = { id: 1, url: 'https://support.zendesk.com/hc/articles/1.json' }
    @ticket.audit.events << viewed_event

    refute viewed_event.valid?
  end
end
