require_relative "../../support/test_helper"

SingleCov.covered!

describe CollabThreadEvent do
  fixtures :accounts, :tickets, :users

  let(:user) { users(:minimum_1) }
  let(:ticket) do
    ticket = tickets(:minimum_1)
    ticket.will_be_saved_by(User.system)
    ticket
  end
  let(:event) { CollabThreadClosed.new }

  let(:subject) { 'Can anyone in IT help with this?' }
  let(:thread_id) { 'abc123' }

  class MyTestEvent < CollabThreadEvent; end

  it 'creates an event when provided the right parameters' do
    event = MyTestEvent.new
    event.subject = subject
    event.thread_id = thread_id
    event.identifier = "abcde"

    assert_equal subject, event.subject
    assert_equal thread_id, event.thread_id
    assert_equal "abcde", event.identifier
  end

  it 'sets the event to private' do
    refute event.is_public?
  end

  it 'is invalid when there is no subject' do
    event = CollabThreadClosed.new
    refute event.valid?
    assert event.errors.messages[:subject].present?
  end

  it 'is invalid when there is no thread_id' do
    event = CollabThreadClosed.new
    refute event.valid?
    assert event.errors.messages[:thread_id].present?
  end
end
