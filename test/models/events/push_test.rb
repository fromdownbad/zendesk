require_relative "../../support/test_helper"

SingleCov.covered!

describe Push do
  fixtures :accounts, :tickets, :events
  should_validate_presence_of :value_reference

  it 'has an HTML-safe #to_s' do
    assert subject.to_s.html_safe?
  end

  describe "#read_only?" do
    it "does not be read-only if a new record" do
      push = Push.new
      push.stubs(:new_record?).returns(true)
      refute push.readonly?
    end

    it "is read-only if not a new record" do
      push = Push.new
      push.stubs(:new_record?).returns(false)
      assert push.readonly?
    end
  end

  describe "#resource" do
    it "returns #value when #resource is called" do
      push = Push.new
      push.expects(:value).returns(123)
      assert_equal 123, push.resource
    end
  end

  describe "#resource_attributes=" do
    it "is cached in rendered form as the value on create" do
      push = Push.new
      attrs = ActiveSupport::OrderedHash.new
      attrs[:location] = 'http://example.com'
      attrs[:name] = 'Ex Ample'
      push.resource_attributes = attrs
      push.valid?
      view = "<li><strong>Location</strong>: <a href='http://example.com' target='_blank'>http://example.com</a></li><li><strong>name</strong>: Ex Ample</li>"

      assert_equal view, push.value
    end
  end

  describe "#value_reference" do
    it "is html_safe" do
      subject.value_reference = '<script>foo<script>'
      assert subject.value_reference.html_safe?
    end
  end

  describe "#to_s" do
    it "is the value" do
      subject.value = '<script>foo<script>'
      assert_equal '<script>foo<script>', subject.to_s
    end

    it "is html_safe" do
      subject.value = '<script>foo<script>'
      assert subject.to_s.html_safe?
    end
  end

  describe "#remove_javascript_from_value_and_value_reference" do
    before do
      @account = accounts(:minimum)
      @ticket = tickets(:minimum_1)
    end

    # customers put html in their data an expet it to show
    # but we do not want to allow e.g. agents to insert javascript that could give them admin cookies
    it "escapes evilness but keep styling related things" do
      code = '<script>evilAttack();</script><em onhover="evilAttack()" style="color:red">Hi</em><a href="bar">x</a><ul><li>Foo</li></ul>'
      @event = Push.new(
        value_reference: code,
        via_id: ViaType.RESOURCE_PUSH,
        value: code,
        account: @account,
        ticket: @ticket
      )
      @ticket.audits.last.events << @event
      @event.reload

      escaped = '<em style="color: red;">Hi</em><a href="bar">x</a><ul><li>Foo</li></ul>'
      assert_equal escaped, @event.value
      assert_equal escaped, @event.value_reference
    end

    it "dos nothing on frozen records" do
      code = "<script>a</script>"
      @event = Push.new(
        value_reference: code,
        via_id: ViaType.RESOURCE_PUSH,
        value: code,
        account: @account,
        ticket: @ticket
      )
      @event.stubs(:frozen?).returns true # e.g. object destroyed
      @ticket.audits.last.events << @event

      assert_equal code, @event.value
      assert_equal code, @event.value_reference
    end
  end
end
