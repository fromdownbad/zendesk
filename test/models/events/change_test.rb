require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 18

describe Change do
  should_validate_presence_of :value_reference

  it "is read-only if not a new record" do
    change = Change.new
    change.stubs(:new_record?).returns(false)

    assert change.readonly?
  end
end
