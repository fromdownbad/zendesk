require_relative "../../support/test_helper"
require_relative "../../support/rule"

SingleCov.covered! uncovered: 25

describe Audit do
  include ArturoTestHelper
  include CustomFieldsTestHelper
  include TestSupport::Rule::Helper

  should_validate_presence_of :ticket, :author, :via_id

  fixtures :all

  describe "#via_twitter?" do
    before do
      @audit = Audit.new
    end

    it "returns true if audit is via twitter" do
      @audit.via_id = ViaType.TWITTER
      assert @audit.via_twitter?
      @audit.via_id = ViaType.TWITTER_DM
      assert @audit.via_twitter?
      @audit.via_id = ViaType.TWITTER_FAVORITE
      assert @audit.via_twitter?
    end

    it "returns false if audit is not via twitter" do
      @audit.via_id = ViaType.WEB_FORM
      refute @audit.via_twitter?
    end
  end

  describe "#metadata" do
    describe "on a legacy client string" do
      before do
        @audit = Audit.new(value_previous: "Message ID: a\nClient: b\nIP address: c\nLocation: d")
      end

      it "returns a hash from the client string" do
        assert_equal "a", @audit.metadata[:system][:message_id]
        assert_equal "b", @audit.metadata[:system][:client]
        assert_equal "c", @audit.metadata[:system][:ip_address]
        assert_equal "d", @audit.metadata[:system][:location]
      end

      it "returns the original client as client" do
        assert_equal "Message ID: a\nClient: b\nIP address: c\nLocation: d", @audit.client
      end

      it "is frozen" do
        assert_raise(RuntimeError) { @audit.metadata[:foo] = :bar }
      end
    end

    describe "scrub_metadata!" do
      let(:account) { accounts(:minimum) }
      let(:ticket)  { tickets(:minimum_1) }
      let(:via_id)           { ViaType.RULE }
      let(:user)             { users(:minimum_agent) }

      before do
        @metadata = {
          system: {
            message_id: "a",
            client: "b",
            ip_address: "c",
            location: "d"
          },
          custom: { foo: "bar"}
        }
        @json = @metadata.to_json
        @audit = Audit.create(value_previous: @json, account: account, ticket: ticket, via_id: via_id, author_id: user.id)
      end

      describe 'with delete_ticket_metadata_pii account setting' do
        before do
          @audit.account.settings.delete_ticket_metadata_pii = true
          @audit.account.save!
          @audit.mark_for_scrubbing = true
        end

        describe 'with Arturo' do
          before do
            Arturo.enable_feature!(:ticket_metadata_deletion_at_close)
          end

          it 'scrubs value' do
            @audit.value = 'my@email.address'
            @audit.via_id = ViaType.MAIL
            assert_equal JSON.parse(@json), @audit.metadata
            @audit.scrub_metadata!
            assert_nil @audit.value
          end

          it 'does not scrubs value for non-MAIL via_id' do
            @audit.value = 'nonPIIdata'
            @audit.via_id = ViaType.WEB_FORM
            assert_equal JSON.parse(@json), @audit.metadata
            @audit.scrub_metadata!
            assert_equal 'nonPIIdata', @audit.value
          end

          it 'scrubs all metadata' do
            assert_equal JSON.parse(@json), @audit.metadata
            @audit.scrub_metadata!
            result = { 'system' => { 'message_id' => '', 'client' => '', 'ip_address' => '', 'location' => ''}, 'custom' => { 'foo' => '' } }
            assert_equal result, @audit.metadata
          end
        end

        it 'scrubs no data without arturo' do
          Arturo.disable_feature!(:ticket_metadata_deletion_at_close)
          assert_equal JSON.parse(@json), @audit.metadata
          @audit.scrub_metadata!
          assert_equal JSON.parse(@json), @audit.reload.metadata
        end

        describe 'for allowed keys' do
          before do
            Account.any_instance.stubs(:has_delete_ticket_metadata_pii_enabled?).returns(true)
            @expected = { 'system' => { 'message_id' => '', 'client' => '', 'ip_address' => '', 'location' => ''}, 'custom' => { 'foo' => '' }, 'nested' => {} }
            @metadata[:nested] = {}
          end

          it 'at root or deeper level, values are not scrubbed' do
            Audit::METADATA_KEY_ALLOWLIST.each do |key|
              @metadata[key] = @expected[key] = 'value'
              @metadata[:nested][key] = @expected['nested'][key] = 'value'
            end

            audit = Audit.create(value_previous: @metadata.to_json, account: account, ticket: ticket, via_id: via_id, author_id: user.id)
            audit.mark_for_scrubbing = true
            audit.scrub_metadata!
            assert_equal @expected, audit.reload.metadata
            assert_equal 'value', audit.reload.metadata[Audit::METADATA_KEY_ALLOWLIST.first]
            assert_equal 'value', audit.reload.metadata['nested'][Audit::METADATA_KEY_ALLOWLIST.first]
          end

          it 'with a hash for a value, no values in the hash are scrubbed' do
            @metadata[Audit::METADATA_KEY_ALLOWLIST.first.to_sym] = { "asdf" => "qwer" }
            @expected[Audit::METADATA_KEY_ALLOWLIST.first] = { "asdf" => "qwer" }

            audit = Audit.create(value_previous: @metadata.to_json, account: account, ticket: ticket, via_id: via_id, author_id: user.id)
            audit.mark_for_scrubbing = true
            audit.scrub_metadata!
            assert_equal @expected, audit.reload.metadata
          end
        end
      end

      describe 'without delete_ticket_metadata_pii account setting' do
        before do
          @audit.account.settings.delete_ticket_metadata_pii = false
          @audit.account.save!
          @audit.mark_for_scrubbing = true
        end

        it 'scrubs no data with Arturo' do
          Arturo.enable_feature!(:ticket_metadata_deletion_at_close)
          assert_equal JSON.parse(@json), @audit.metadata
          @audit.scrub_metadata!
          assert_equal JSON.parse(@json), @audit.reload.metadata
        end

        it 'scrubs no data without Arturo' do
          Arturo.disable_feature!(:ticket_metadata_deletion_at_close)
          assert_equal JSON.parse(@json), @audit.metadata
          @audit.scrub_metadata!
          assert_equal JSON.parse(@json), @audit.reload.metadata
        end
      end

      describe 'reporting' do
        before do
          Account.any_instance.stubs(:has_delete_ticket_metadata_pii_enabled?).returns(true)
          @audit.mark_for_scrubbing = true
        end

        describe 'successful deletions' do
          it 'Notifies datadog' do
            Zendesk::StatsD::Client.any_instance.expects(:increment).with("scrub_metadata.success", tags: ["account_id:#{account.id}"])
            @audit.scrub_metadata!
          end
        end

        describe 'unsuccessful deletions due to ticket being archived' do
          before { Ticket.any_instance.stubs(:archived?).returns(true) }

          it 'Notifies datadog and logs' do
            Rails.logger.expects(:error).with("Failed to scrub_metadata! as ticket is archived: account_id: #{account.id}, ticket_id: #{@audit.ticket.id}, audit_id: #{@audit.id}")
            Zendesk::StatsD::Client.any_instance.expects(:increment).with("scrub_metadata.failure.archived", tags: ["account_id:#{account.id}", "ticket_id:#{@audit.ticket.id}", "audit_id:#{@audit.id}"])
            @audit.scrub_metadata!
            assert_equal JSON.parse(@json), @audit.reload.metadata
          end
        end

        describe 'unsuccessful deletions for other reasons' do
          before { Audit.any_instance.stubs(:scrub_metadata_hash!).raises(StandardError) }

          it 'Notifies datadog and logs' do
            Rails.logger.expects(:error).with("Failed to scrub_metadata! for account #{account.id}: StandardError, ticket_id: #{@audit.ticket.id}, audit_id: #{@audit.id}")
            Zendesk::StatsD::Client.any_instance.expects(:increment).with("scrub_metadata.failure.unknown", tags: ["account_id:#{account.id}", "ticket_id:#{@audit.ticket.id}", "audit_id:#{@audit.id}"])
            @audit.scrub_metadata!
          end
        end
      end
    end

    describe "on a json string" do
      before do
        json = {
          system: {
            message_id: "a",
            client: "b",
            ip_address: "c",
            location: "d"
          },
          custom: { foo: "bar"}
        }.to_json

        @audit = Audit.new(value_previous: json)
      end

      it "returns a hash from the json string" do
        assert_equal "a", @audit.metadata[:system][:message_id]
        assert_equal "b", @audit.metadata[:system][:client]
        assert_equal "c", @audit.metadata[:system][:ip_address]
        assert_equal "d", @audit.metadata[:system][:location]
        assert_equal "bar", @audit.metadata[:custom][:foo]
      end

      it "returns legacy client string as client" do
        assert_equal "Message ID: a\nClient: b\nIP address: c\nLocation: d", @audit.client
      end

      it "is frozen" do
        assert_raise(RuntimeError) { @audit.metadata[:foo] = :bar }
      end
    end
  end

  describe "#set_metadata" do
    it "changes existing json" do
      default_metadata = {"custom" => {}, "system" => {}}

      audit = Audit.new(value_previous: {foo: "bar"}.to_json)
      assert_equal(default_metadata.merge("foo" => "bar"), audit.metadata)
      audit.set_metadata :baz, "boom"

      assert_equal(default_metadata.merge("foo" => "bar", "baz" => "boom"), audit.metadata)
      assert_equal(default_metadata.merge("foo" => "bar", "baz" => "boom"), JSON.load(audit.value_previous))
    end
  end

  describe "#set_custom_metadata!" do
    before { @audit = Audit.new(value_previous: {custom: {}}.to_json) }

    it "sets the custom metadata" do
      z1_request_url = 'https://support.zendesk.com/hc/requests/12345'
      @audit.expects(:save!).once
      @audit.set_custom_metadata!("z1_request_url" => z1_request_url)
      assert_includes @audit.metadata[:custom].keys, "z1_request_url"
      assert_equal z1_request_url, @audit.metadata[:custom][:z1_request_url]
    end

    describe "when metadata contains an invalid key" do
      it "does not set the value for the invalid key in the custom metadata" do
        @audit.expects(:save!).once
        @audit.set_custom_metadata!("invalid_key" => "invalid_value")
        assert_includes @audit.metadata[:custom].keys, "invalid_key"
        assert_equal "", @audit.metadata[:custom]['invalid_key']
      end
    end
  end

  describe "#delete_eml_file" do
    let(:raw_email_identifier) { "" }

    it "does not set raw_email_identifier in metadata to nil if raw email identifier is blank" do
      audit = Audit.new(account: accounts(:minimum), value_previous: { system: { raw_email_identifier: raw_email_identifier } }.to_json)
      audit.raw_email.expects(:delete_eml_file!).never
      audit.delete_eml_file
      assert_includes audit.metadata[:system].keys, "raw_email_identifier"
      assert_equal raw_email_identifier, audit.metadata[:system]['raw_email_identifier']
    end
  end

  describe "#delete_json_files" do
    let(:json_email_identifier) { "" }

    it "does not set json_email_identifier in metadata to nil if raw json identifier is blank" do
      audit = Audit.new(account: accounts(:minimum), value_previous: { system: { json_email_identifier: json_email_identifier } }.to_json)
      audit.raw_email.expects(:delete_json_files!).never
      audit.delete_json_files
      assert_includes audit.metadata[:system].keys, "json_email_identifier"
      assert_equal json_email_identifier, audit.metadata[:system]['json_email_identifier']
    end
  end

  describe "#delete_eml_file!" do
    let(:account) { accounts(:minimum) }

    it "sets raw_email_identifier in metadata to nil if raw email exists" do
      audit = Audit.new(
        account: account,
        value_previous: {
          system: {
            raw_email_identifier: "872305/5e93f40360118562924909a4767a0c427be8b6f7-4897.eml"
          }
        }.to_json
      )
      audit.raw_email.expects(:delete_eml_file!).once
      audit.expects(:save!).once
      audit.delete_eml_file!
      assert_nil audit.metadata[:system][:raw_email_identifier]
    end

    it "does not set raw_email_identifier in metadata to nil if raw email does not exist" do
      audit = Audit.new(
        account: account,
        value_previous: {
          system: {}
        }.to_json
      )
      audit.raw_email.expects(:delete_eml_file!).never
      audit.expects(:save!).once
      audit.delete_eml_file!

      refute_includes audit.metadata[:system].keys, "raw_email_identifier"
    end
  end

  describe "#disable_radar_notification?" do
    let(:account) { accounts(:minimum) }
    let(:user)    { account.owner }

    before do
      @ticket = Ticket.new(account: account, description: "testing always_notify_radar")
      @audit = Audit.new(account: account, ticket: @ticket)
    end

    describe "when triggers are disabled" do
      before do
        @audit.disable_triggers = true
      end

      [CommentPrivacyChange, KnowledgeLinked, KnowledgeCaptured, KnowledgeFlagged].each do |klass|
        it "return false if the first event is a #{klass}" do
          @audit.events << klass.new(audit: @audit)

          refute @audit.disable_radar_notification?
        end
      end

      Audit::CHAT_CHANNELS.each do |chat_channel|
        it "return false if ticket is a #{ViaType.to_s(chat_channel)} ticket" do
          @ticket = Ticket.new(account: account, description: "testing always_notify_radar", via_id: chat_channel)
          @audit = Audit.new(account: account, ticket: @ticket)
          @audit.disable_triggers = true

          refute @audit.disable_radar_notification?
        end
      end

      it "return true for other cases" do
        @audit.events << Change.new(audit: @audit)

        assert @audit.disable_radar_notification?
      end
    end

    describe "when triggers are enabled" do
      before do
        @audit.disable_triggers = false
      end

      [CommentPrivacyChange, KnowledgeLinked, KnowledgeCaptured, KnowledgeFlagged].each do |klass|
        it "return false if the first event is a #{klass}" do
          @audit.events << klass.new(audit: @audit)

          refute @audit.disable_radar_notification?
        end
      end

      Audit::CHAT_CHANNELS.each do |chat_channel|
        it "return false if ticket is a #{ViaType.to_s(chat_channel)} ticket" do
          @ticket = Ticket.new(account: account, description: "testing always_notify_radar", via_id: chat_channel)
          @audit = Audit.new(account: account, ticket: @ticket)
          @audit.disable_triggers = false

          refute @audit.disable_radar_notification?
        end
      end

      it "return false for other cases" do
        @audit.events << Change.new(audit: @audit)

        refute @audit.disable_radar_notification?
      end
    end
  end

  describe "metadata validation" do
    before do
      @audit = Audit.new(via_id: ViaType.WEB_SERVICE)
      @audit.ticket = Ticket.new
      @audit.account_id = 1
      @audit.author = User.new
    end

    it "does not allow a string larger than 4 kilobytes" do
      @audit.set_metadata :custom, "xs" => "x" * 4100
      refute @audit.valid?
      assert_equal ["metadata", "is invalid"], @audit.errors.first.map(&:to_s)
    end

    it "allows a string less than 2 kilobytes" do
      @audit.set_metadata :custom, "xs" => "x" * 4000
      assert @audit.valid?
    end
  end

  describe "trigger loop" do
    before do
      @account = accounts(:minimum)
      @account.triggers.delete_all
      @user = @account.owner
    end

    describe "triggers reacting on triggers" do
      before do
        # this one depends on trigger B
        create_trigger!("A",
          conditions_all: [
            ["status_id", "is", 1]
          ], actions: [
            ["ticket_type_id", 1],
            ["current_tags", "a"]
          ])

        create_trigger!("B",
          conditions_all: [
            ["status_id", "is", 0]
          ],
          actions: [
            ["status_id", 1],
            ["current_tags", "b"]
          ])
      end

      it "reacts to changes made by triggers" do
        # this one depends on the combined change set of trigger A and B
        create_trigger!("C",
          conditions_all: [
            ["ticket_type_id", "value", 1],
            ["status_id",      "value", 1]
          ],
          actions: [
            ["current_tags", "c"]
          ])

        ticket = @account.tickets.new(requester: @user, description: 'foo', status_id: 0, ticket_type_id: 0)
        ticket.will_be_saved_by(@user)
        ticket.save!

        assert_equal ["a", "b", "c"], ticket.tag_array.sort
      end

      it "only react on the changes made by the trigger (not the user)" do
        # this one depends on the change set of trigger A but not the not the change set of B
        create_trigger!("C",
          conditions_all: [
            ["ticket_type_id", "value", 1],
            ["status_id",      "not_changed"]
          ],
          actions: [
            ["current_tags", "c"]
          ])

        ticket = @account.tickets.new(requester: @user, description: 'foo', status_id: 0, ticket_type_id: 0)
        ticket.will_be_saved_by(@user)
        ticket.save!

        assert_equal ["a", "b"], ticket.tag_array.sort
      end
    end
  end

  describe '#execute' do
    let(:account) { accounts(:minimum) }
    let(:ticket)  { tickets(:minimum_1) }

    let(:ticket_sharing_triggers) { [new_trigger(title: 'ticket_sharing')] }
    let(:prediction_triggers)     { [new_trigger(title: 'prediction')] }

    let(:active_triggers) do
      [new_trigger(title: 'active 1'), new_trigger(title: 'active 2')]
    end

    let(:audit) { Audit.new(account: account, ticket: ticket) }

    before do
      account.triggers.delete_all

      account.stubs(
        fetch_active_non_update_triggers: ticket_sharing_triggers,
        fetch_active_prediction_triggers: prediction_triggers,
        fetch_active_triggers:            active_triggers
      )

      ticket.status_id = StatusType.PENDING

      audit.events << Change.new(audit: audit)
    end

    describe 'when triggers are disabled' do
      before do
        audit.disable_triggers = true

        audit.execute
      end

      before_should 'not perform trigger execution' do
        Zendesk::Rules::Trigger::Execution.expects(:execute).never
      end
    end

    describe 'when the audit has no events' do
      before do
        audit.events.delete_all

        audit.execute
      end

      before_should 'not perform trigger execution' do
        Zendesk::Rules::Trigger::Execution.expects(:execute).never
      end
    end

    describe 'when invoked' do
      before { audit.execute }

      before_should 'execute the `default` triggers' do
        Zendesk::Rules::Trigger::Execution.
          expects(:execute).
          with(
            account:        account,
            events:         audit.events,
            execution_type: :default,
            ticket:         ticket
          )
      end
    end

    describe 'when `ticket_sharing_create` is true' do
      before do
        audit.stubs(ticket_sharing_create: true)

        audit.execute
      end

      before_should 'execute the `ticket_sharing_create` triggers' do
        Zendesk::Rules::Trigger::Execution.
          expects(:execute).
          with(
            account:        account,
            events:         audit.events,
            execution_type: :ticket_sharing_create,
            ticket:         ticket
          )
      end
    end

    describe 'when updated via satisfaction prediction' do
      before do
        audit.stubs(via_id: ViaType.SATISFACTION_PREDICTION)
      end

      describe 'and the account has satisfaction prediction' do
        before do
          account.stubs(has_satisfaction_prediction?: true)

          audit.execute
        end

        before_should 'execute the `satisfaction_prediction` triggers' do
          Zendesk::Rules::Trigger::Execution.
            expects(:execute).
            with(
              account:        account,
              events:         audit.events,
              execution_type: :satisfaction_prediction,
              ticket:         ticket
            )
        end
      end

      describe 'and the account does not have satisfaction prediction' do
        before do
          account.stubs(has_satisfaction_prediction?: false)

          audit.execute
        end

        before_should 'execute the `default` triggers' do
          Zendesk::Rules::Trigger::Execution.
            expects(:execute).
            with(
              account:        account,
              events:         audit.events,
              execution_type: :default,
              ticket:         ticket
            )
        end
      end
    end
  end

  describe "#suspension_type_id" do
    before do
      @audit = events(:untrusted_audit_for_minimum_ticket_1)
    end

    it "persists" do
      @audit.suspension_type_id = SuspensionType.OTHER_USER_UPDATE
      @audit.save!
      reloaded = Audit.find(@audit.id)

      assert_equal reloaded.suspension_type_id, SuspensionType.OTHER_USER_UPDATE
    end
  end

  describe "#flagged_reasons" do
    before do
      @audit = events(:untrusted_audit_for_minimum_ticket_1)
    end

    it "returns a flagged reason key if the flag type is untrusted" do
      # array passed into #list is exclusions from returned list
      EventFlagType.list([EventFlagType.DEFAULT_TRUSTED, EventFlagType.DEFAULT_UNTRUSTED]).each do |flag|
        next if EventFlagType.trusted?(flag.last)

        @audit.flag!([flag.last])
        @audit.save!
        @audit.flagged_reasons.must_include flag.first

        @audit.unflag!(flag.last)
      end
    end

    it "returns a generic reason key if the flag type is unsupported" do
      @audit.flag!([EventFlagType.DEFAULT_UNTRUSTED])
      @audit.save!

      @audit.flagged_reasons.must_include 'default'
    end

    it "returns a corresponding flagged reason key if a suspension type was used instead of the flag" do
      @audit.stubs(:suspension_type_id).returns(SuspensionType.OTHER_USER_UPDATE)
      @audit.flagged_reasons.must_include "other_user_update"
    end
  end

  describe "#flagged?" do
    before do
      @audit = events(:untrusted_audit_for_minimum_ticket_1)
    end

    it "is false when the audit has no flags" do
      refute @audit.flagged?
    end

    describe "when the audit has been flagged" do
      before do
        @audit.flag!([EventFlagType.DEFAULT_UNTRUSTED])
        @audit.save!
      end

      it "is true with no specified flag type" do
        assert @audit.flagged?
      end

      it "is false when specified flag type is not one of the flags on the audit" do
        refute @audit.flagged?(EventFlagType.OTHER_USER_UPDATE)
      end
    end
  end

  describe "on commit" do
    it "logs its id" do
      @log    = StringIO.new
      @logger = Logger.new(@log)
      @logger.stubs(:append_attributes)
      Rails.stubs(:logger).returns(@logger)
      @audit = events(:untrusted_audit_for_minimum_ticket_1)
      @audit.save!
      assert_match /^W, \[[^\]]+\]\s+WARN -- : Committed audit \d+$/, @log.string
    end
  end

  describe "#trusted?" do
    it "is true with no data" do
      assert(Audit.new.trusted?)
    end

    it "is true with data" do
      audit = Audit.new(value_previous: {foo: :bar}.to_json)
      assert(audit.trusted?)
    end

    it "is false with false for :trusted" do
      audit = Audit.new(value_previous: {trusted: false}.to_json)
      assert_equal false, audit.trusted?
    end

    it "is true with true for :trusted" do
      audit = Audit.new(value_previous: {trusted: true}.to_json)
      assert(audit.trusted?)
    end
  end

  describe "flagged comments" do
    describe "untrusted!" do
      before do
        @audit = Audit.new
        @audit.account = Account.new
      end

      it "starts out trusted" do
        assert @audit.trusted?
      end

      it "makes the audit untrusted" do
        @audit.untrusted!
        refute @audit.trusted?
        refute @audit.metadata["trusted"]
        assert_includes @audit.value_previous, '"trusted":false'
      end
    end

    describe "#flag!" do
      let (:flag_type) { EventFlagType.DEFAULT_UNTRUSTED }
      let (:default_untrusted_options) { HashWithIndifferentAccess.new(testkey: "testvalue") }
      let (:flags_options) { HashWithIndifferentAccess.new(flag_type => default_untrusted_options) }

      before do
        @audit = events(:create_audit_for_minimum_ticket_1)
      end

      it "saves flags to the audit" do
        refute @audit.metadata.include?("flags")
        @audit.flag!([flag_type])
        assert @audit.metadata["flags"].include?(flag_type)
      end

      it "saves flag options to the audit" do
        refute @audit.metadata.include?("flags_options")
        @audit.flag!([flag_type], flags_options)
        assert @audit.metadata["flags_options"][flag_type.to_s].include?("testkey")
      end
    end

    describe "#flags" do
      before do
        @audit = events(:create_audit_for_minimum_ticket_1)
      end

      it "is empty when no flags have been saved in the metadata" do
        assert_empty(@audit.flags)
      end

      describe "when flags have been saved in the metadata" do
        let (:flags) { [0, 1] }

        let (:flags_options) do
          # saving to metadata does a .to_json so the keys get converted to strings
          {"0" => HashWithIndifferentAccess.new(trusted: false), "1" => HashWithIndifferentAccess.new(trusted: true)}
        end

        let (:audit_flags) { AuditFlags.new(flags) }

        before do
          @audit.set_metadata("flags", flags)
          @audit.set_metadata("flags_options", flags_options)
        end

        it "loads the flags from metadata" do
          @audit.flags.must_equal audit_flags
        end

        it "loads the flags options from metadata" do
          @audit.flags.options.must_equal flags_options
        end
      end
    end
  end

  describe "#locate_client" do
    describe "when GeoLocation is failing" do
      before { Zendesk::GeoLocation.expects(:locate_ticket_audit!).raises("Hello") }
      it "does not raise further" do
        ZendeskExceptions::Logger.expects(:record).once
        Audit.new.send(:locate_client)
      end
    end
  end

  describe '#grow' do
    let(:account)  { accounts(:minimum) }
    let(:end_user) { users(:minimum_end_user) }
    let(:events)   { ticket.audit.events }
    let(:follower) { users(:minimum_agent) }
    let(:user)     { account.owner }

    let(:ticket) do
      account.tickets.new(
        requester: user,
        description: 'grow wombat',
        status_id: 0
      )
    end

    before { ticket.will_be_saved_by(user) }

    describe 'multibrand' do
      before { ticket.brand_id = account.default_brand.id }

      describe 'when multibrand feature is enabled' do
        before do
          Account.any_instance.stubs(has_multibrand?: true)

          ticket.audit.grow
        end

        it "adds the brand event to the audit's events" do
          assert(events.map(&:value_reference).include?('brand_id'))
        end
      end

      describe 'when multibrand feature is disabled' do
        before { Account.any_instance.stubs(has_multibrand?: false) }

        it "does not add the brand event to the audit's events" do
          refute events.map(&:value_reference).include?('brand_id')
        end
      end
    end

    describe "original_recipient_address" do
      before { ticket.original_recipient_address = account.admins.first.email }

      describe "for a new ticket" do
        describe_with_and_without_arturo_enabled :original_recipient_address_audit do
          it "does not add the original_recipient_address event to the audit's events" do
            ticket.audit.grow
            refute events.map(&:value_reference).include?("original_recipient_address")
          end
        end
      end

      describe "for an existing ticket" do
        before { ticket.stubs(new_record?: false) }

        it "adds the original_recipient_address event to the audit's events" do
          ticket.audit.grow
          assert events.map(&:value_reference).include?("original_recipient_address")
        end

        describe_with_arturo_disabled :original_recipient_address_audit do
          it "does not add the original_recipient_address event to the audit's events" do
            ticket.audit.grow
            refute events.map(&:value_reference).include?("original_recipient_address")
          end
        end
      end
    end

    describe 'when the ticket has external changes' do
      let(:checkbox) do
        {
          class: 'Checkbox',
          key:   'field_checkbox',
          info:  'alexander',
          title: 'checkbox field',
          value: 'selected'
        }
      end

      let(:decimal) do
        {
          class: 'Decimal',
          key:   'field_decimal',
          info:  'hamilton',
          title: 'decimal field',
          value: '1776.1789',
        }
      end

      before do
        @account = account

        [
          create_external_change_event(checkbox),
          create_external_change_event(decimal)
        ].each { |change| ticket.add_external_change(change) }

        ticket.audit.grow
      end

      it 'includes the external change events' do
        [checkbox, decimal].each do |change|
          assert_includes events.map(&:value), change[:value]
        end
      end
    end

    describe 'when the ticket has collaboration changes' do
      let(:email_cc_change) do
        EmailCcChange.new.tap do |change|
          change.current_email_ccs = [end_user.email]
          change.previous_email_ccs = []
        end
      end

      let(:follower_change) do
        FollowerChange.new.tap do |change|
          change.current_followers = [follower.email]
          change.previous_followers = []
        end
      end
      let(:current_collaborators_change) { Create.new(value_reference: 'current_collaborators', value: "howdy") }

      before do
        ticket.stubs(collaboration_changes: [email_cc_change, follower_change])
        ticket.current_collaborators = [current_collaborators_change.value]
      end

      describe "when follower_and_email_cc_collaborations is enabled" do
        before do
          Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: true)

          ticket.audit.grow
        end

        it 'includes only the new collaboration changes events' do
          assert events.any? do |event|
            event.class.name == email_cc_change.class.name &&
              event.current_email_ccs == email_cc_change.email &&
              event.previous_email_ccs == email_cc_change.previous_email_ccs
          end
          assert events.any? do |event|
            event.class.name == follower_change.class.name &&
              event.current_followers == follower_change.email &&
              event.previous_followers == follower_change.previous_email_ccs
          end

          assert events.none? { |event| event.value_reference == current_collaborators_change.value_reference }
        end
      end

      describe "when follower_and_email_cc_collaborations is disabled" do
        before do
          Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: false)

          ticket.audit.grow
        end

        it 'includes the current_collaborators change event' do
          assert events.any? do |event|
            event.class.name == current_collaborators_change.class.name &&
            event.value_reference == current_collaborators_change.value_reference
            event.value == current_collaborators_change.value
          end
        end
      end
    end

    describe "created_at for audits and events" do
      it "child events inherit parent audit's created_at" do
        create_timestamp = Time.parse('2011-11-11 11:11:11Z')
        ticket.fields = { ticket_fields(:field_text_custom).id.to_s => 'Have a nice day' }
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.audit.created_at = create_timestamp
        ticket.save!

        ticket.audits.last.events.select { |e| e.type == 'Create' }.each do |event|
          assert_equal event.created_at, create_timestamp
        end

        change_timestamp = Time.parse('2012-12-12 12:12:12Z')
        ticket.fields = { ticket_fields(:field_text_custom).id.to_s => 'Change event' }
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.audit.created_at = change_timestamp
        ticket.save!
        ticket.audits.last.events.select { |e| e.type == 'Change' }.each do |event|
          assert_equal event.created_at, change_timestamp
        end
      end
    end
  end

  describe "#redactions?" do
    describe "when audit contains a TicketFieldRedactEvent" do
      let (:audit) { Audit.new.tap { |audit| audit.events = [TicketFieldRedactEvent.new] } }

      it "returns true" do
        assert audit.redactions?
      end
    end

    describe "when audit contains a CommentRedactionEvent" do
      let (:audit) { Audit.new.tap { |audit| audit.events = [CommentRedactionEvent.new] } }

      it "returns true" do
        assert audit.redactions?
      end
    end

    describe "when audit does not contain a redaction event" do
      let (:audit) { Audit.new.tap { |audit| audit.events = [Change.new] } }

      it "returns false" do
        refute audit.redactions?
      end
    end
  end

  describe '#initialize_defaults' do
    let(:recipients)       { 'foo@example.com bar@example.com' }
    let(:recipients_array) { recipients.split(" ") }
    let(:via_id)           { ViaType.RULE }
    let(:account)          { accounts(:minimum) }
    let(:user)             { users(:minimum_agent) }

    def save_ticket(email_cc_addresses, recipients_for_latest_mail)
      @ticket = Ticket.new(account: account, description: "testing initialize_defaults")
      @ticket.stubs(:email_cc_addresses).returns(email_cc_addresses)
      @ticket.recipients_for_latest_mail = recipients_for_latest_mail
      @ticket.will_be_saved_by(user, via_id: via_id)

      if via_id == ViaType.CLOSED_TICKET
        closed_ticket = account.closed_tickets.first

        @ticket.via_followup_source_id = closed_ticket.nice_id
        @ticket.set_followup_source(user)
      end

      @ticket.save!
    end

    it 'memoizes `is_public` to false' do
      audit = Audit.create

      refute audit.is_public
    end

    describe 'when audit is set as public before saving' do
      let(:audit) do
        Audit.new(
          account_id: account.id,
          ticket: tickets(:minimum_1),
          author_id: user.id,
          via_id: via_id
        )
      end

      before do
        audit.is_public = true
        audit.save!
      end

      it 'does not change the `is_public` value' do
        assert audit.is_public
      end
    end

    describe "when via_id is ViaType.MAIL" do
      let(:via_id) { ViaType.MAIL }

      describe_with_and_without_arturo_enabled :email_ccs do
        it "sets recipients to its ticket's recipients_for_latest_mail" do
          save_ticket([], recipients)
          @ticket.audits.last.recipients.must_equal recipients
        end
      end

      it "calls truncate_recipients if recipients are present" do
        Audit.any_instance.expects(:truncate_recipients)
        save_ticket([], recipients)
      end
    end

    describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
      [ViaType.WEB_SERVICE, ViaType.WEB_FORM, ViaType.CLOSED_TICKET].each do |via_id|
        describe "when via_id is #{ViaType.to_s(via_id)}" do
          let(:via_id) { via_id }

          before do
            save_ticket(recipients_array, recipients)
          end

          it 'does not set recipients' do
            @ticket.audits.last.recipients.must_be_nil
          end
        end
      end
    end

    describe 'when via type is something else' do
      let(:via_id) { ViaType.RULE }

      before do
        save_ticket(recipients_array, recipients)
      end

      it 'does not set recipients' do
        @ticket.audits.last.recipients.must_be_nil
      end
    end

    describe_with_arturo_disabled :email_ccs do
      it "does not call truncate_recipients even if recipients are present" do
        Audit.any_instance.expects(:truncate_recipients).never
        save_ticket(recipients_array, "")
      end
    end
  end

  describe "#truncate_recipients" do
    let(:new_recipients) { "foo@example.com bar@example.com" }
    let(:limit) { Audit::DEFAULT_MAX_RECIPIENTS_SIZE }

    before do
      @audit = Audit.new
      Zendesk::Extensions::TruncateAttributesToLimit.stubs(:limit).returns(limit)
    end

    describe "when new_recipients is shorter than the limit" do
      it "does not truncate the given recipients string if its length is less than the Zendesk::Extensions::TruncateAttributesToLimit limit" do
        @audit.send(:truncate_recipients, new_recipients).must_equal new_recipients
      end
    end

    describe "when new_recipients is the same length as the limit" do
      let(:limit) { 31 }

      it "does not truncate the given recipients string if its length is exactly the Zendesk::Extensions::TruncateAttributesToLimit limit" do
        @audit.send(:truncate_recipients, new_recipients).must_equal new_recipients
      end
    end

    describe "when new_recipients is longer than the limit" do
      let(:limit) { 30 }

      it "truncates and adds an ellipsis-style entry to the list" do
        @audit.send(:truncate_recipients, new_recipients).must_equal "foo@example.com ..."
      end

      describe "and the cutoff point lines up with a space delimiter in the list" do
        let(:new_recipients) { "foo@example.com bar@example.com blah@example.com" }
        let(:limit) { 31 }

        it "truncates the entry just before the cutoff to accommodate the ellipsis" do
          @audit.send(:truncate_recipients, new_recipients).must_equal "foo@example.com ..."
        end
      end

      describe "and the cutoff point is within the first entry" do
        let(:limit) { 10 }

        it "truncates and returns just the ellipsis-style entry" do
          @audit.send(:truncate_recipients, new_recipients).must_equal "..."
        end
      end
    end

    describe "when an exception is raised" do
      it "records the exception and returns new_recipients" do
        Zendesk::Extensions::TruncateAttributesToLimit.unstub(:limit)
        Zendesk::Extensions::TruncateAttributesToLimit.stubs(:limit).raises(StandardError, "stubbed exception")
        ZendeskExceptions::Logger.expects(:record)
        @audit.send(:truncate_recipients, new_recipients).must_equal new_recipients
      end
    end
  end

  describe "#recipients_list" do
    let(:ticket) { tickets(:minimum_1) }
    let(:via_id) { ViaType.MAIL }
    let(:audit) { Audit.new { |a| a.account = accounts(:minimum); a.ticket = ticket; a.recipients = "test1 test2"; a.via_id = via_id } }
    let(:recipients_list) { audit.recipients_list }

    describe "when via type is MAIL" do
      it "returns space-delimited recipients list as an Array" do
        assert_equal 2, recipients_list.length
        recipients_list.must_equal ["test1", "test2"]
      end

      describe "when Audit has no recipients" do
        let(:audit) { Audit.new { |a| a.ticket = ticket; a.via_id = ticket.via_id } }

        it { recipients_list.must_equal [] }
      end
    end

    describe "when via type is WEB_FORM" do
      let(:via_id) { ViaType.WEB_FORM }

      it { recipients_list.must_equal [] }
    end
  end

  describe "#active_recipients_ids" do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }
    let(:recipients) { "minimum_end_user@aghassipour.com minimum_search_user@aghassipour.com" }
    let(:via_id) { ViaType.MAIL }
    let(:audit) { Audit.new { |a| a.account = account; a.ticket = ticket; a.recipients = recipients; a.via_id = via_id } }
    let(:active_recipients_ids) { audit.active_recipients_ids }
    let(:minimum_end_user_id) { users(:minimum_end_user).id }
    let(:minimum_search_user) { users(:minimum_search_user) }

    describe "when via type is MAIL" do
      describe "when all recipients are active Users" do
        it "returns an Array of the User IDs" do
          assert_equal 2, active_recipients_ids.length
          active_recipients_ids.must_include minimum_end_user_id
          active_recipients_ids.must_include users(:minimum_search_user).id
        end
      end

      describe "when one recipient is the author of the event" do
        before do
          audit.author = users(:minimum_end_user)
          audit.save!
        end

        it "does not include the author's ID" do
          assert_equal 1, active_recipients_ids.length
          active_recipients_ids.wont_include minimum_end_user_id
          active_recipients_ids.must_include users(:minimum_search_user).id
        end
      end

      describe "when one recipient is an inactive User" do
        let(:recipients) { "minimum_end_user@aghassipour.com inactive_user@zendesk.com" }

        it "returns an Array of the active User IDs" do
          assert_equal 1, active_recipients_ids.length
          active_recipients_ids.must_include minimum_end_user_id
        end
      end

      describe "when one recipient is a support (encoded ID) address for the account" do
        before do
          audit.author = users(:minimum_end_user)
          audit.save!
        end

        let(:recipients) { "minimum_end_user@aghassipour.com support+id92pd0w-qg02@minimum.zendesk-test.com" }

        it "does not include the support address" do
          assert_equal 0, audit.recipients_ids_or_identifiers.length
          audit.recipients_ids_or_identifiers.must_equal []
        end
      end

      describe "when Audit has no recipients" do
        let(:audit) { Audit.new { |a| a.account = account; a.ticket = ticket; a.via_id = ticket.via_id } }

        it { active_recipients_ids.must_be_empty }
      end
    end

    describe "when via type is WEB_FORM" do
      let(:via_id) { ViaType.WEB_FORM }

      let(:recipient_notification_cc) { minimum_search_user }

      before do
        Audit.any_instance.stubs(recipients_notification_ccs: [recipient_notification_cc])
      end

      describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
        it { active_recipients_ids.must_equal [minimum_search_user.id] }
      end

      describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
        let(:recipient_notification_cc) { minimum_search_user.id }

        it { active_recipients_ids.must_equal [minimum_search_user.id] }
      end
    end

    describe "when via type is FACEBOOK_POST" do
      let(:via_id) { ViaType.FACEBOOK_POST }

      it { active_recipients_ids.must_be_empty }
    end
  end

  describe "#recipients_ids_or_identifiers" do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }
    let(:recipients) { "minimum_end_user@aghassipour.com minimum_search_user@aghassipour.com" }
    let(:via_id) { ViaType.MAIL }
    let(:audit) { Audit.new { |a| a.account = account; a.ticket = ticket; a.recipients = recipients; a.via_id = via_id } }
    let(:recipients_ids_or_identifiers) { audit.recipients_ids_or_identifiers }
    let(:minimum_end_user_id) { users(:minimum_end_user).id }

    describe "when via type is MAIL" do
      describe "when all recipients are active Users" do
        it "returns an Array of the User IDs" do
          assert_equal 2, recipients_ids_or_identifiers.length
          recipients_ids_or_identifiers.must_include minimum_end_user_id
          recipients_ids_or_identifiers.must_include users(:minimum_search_user).id
        end
      end

      describe "when one recipient is the author of the event" do
        before do
          audit.author = users(:minimum_end_user)
          audit.save!
        end

        it "does not include the author's ID" do
          assert_equal 1, recipients_ids_or_identifiers.length
          recipients_ids_or_identifiers.wont_include minimum_end_user_id
          recipients_ids_or_identifiers.must_include users(:minimum_search_user).id
        end
      end

      describe "when one recipient is an inactive User" do
        let(:recipients) { "minimum_end_user@aghassipour.com inactive_user@zendesk.com" }

        it "returns an Array of the active User IDs and inactive email addresses" do
          assert_equal 2, recipients_ids_or_identifiers.length
          recipients_ids_or_identifiers.must_include minimum_end_user_id
          recipients_ids_or_identifiers.must_include "inactive_user@zendesk.com"
        end
      end

      describe "when one recipient is a support address for the account" do
        let(:recipients) { "minimum_end_user@aghassipour.com not_default@example.net" }

        it "does not include the support address" do
          assert_equal 1, recipients_ids_or_identifiers.length
          recipients_ids_or_identifiers.must_equal [minimum_end_user_id]
        end
      end
    end

    describe "when via type is WEB_FORM" do
      let(:via_id) { ViaType.WEB_FORM }

      let(:recipients_notification_ccs) { [] }

      before do
        Audit.any_instance.stubs(recipients_notification_ccs: recipients_notification_ccs)
      end

      it "does not use the recipients_list" do
        Audit.any_instance.expects(:recipients_list).never
        recipients_ids_or_identifiers
      end

      describe "when there are no notification CCs" do
        it { recipients_ids_or_identifiers.must_be_empty }
      end

      describe "when there are notification CCs" do
        let(:ccd_user) { users(:minimum_search_user) }
        let(:recipients_notification_ccs) { [ccd_user] }

        it "includes the CC'd user's ID" do
          recipients_ids_or_identifiers.must_include ccd_user.id
        end

        describe "when the CC'd user has been soft deleted" do
          before do
            ccd_user.current_user = users(:minimum_admin)
            ccd_user.delete!
          end

          it "includes the CC'd user's name and not the ID" do
            recipients_ids_or_identifiers.must_include ccd_user.name
          end
        end
      end
    end

    describe "when via type is FACEBOOK_POST" do
      let(:via_id) { ViaType.FACEBOOK_POST }

      it { recipients_ids_or_identifiers.must_be_empty }
    end
  end

  describe '#recipients_support_address' do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }

    before do
      @audit = Audit.new { |a| a.account = account; a.ticket = ticket; a.recipients = recipients; a.via_id = via_id }
      @recipients_support_address = @audit.recipients_support_address
    end

    describe 'when via type is mail' do
      let(:via_id) { ViaType.MAIL }

      describe 'when recipients list is absent' do
        let(:recipients) { "" }

        it 'returns nil' do
          refute @recipients_support_address
        end
      end

      describe 'when recipients list is present' do
        let(:recipients) { "support@minimum.zendesk-test.com" }

        it 'returns the first email mapped to a support address' do
          assert_equal "support@minimum.zendesk-test.com", @recipients_support_address
        end
      end
    end

    describe 'when via type is not mail' do
      let(:via_id) { ViaType.WEB_FORM }
      let(:recipients) { "support@minimum.zendesk-test.com" }

      it 'returns nil' do
        refute @recipients_support_address
      end
    end
  end

  describe "#recipients_notification_to" do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }
    let(:recipients) { "minimum_end_user@aghassipour.com minimum_search_user@aghassipour.com" }
    let(:via_id) { ViaType.WEB_FORM }
    let(:audit) do
      Audit.create do |a|
        a.account = account
        a.author = ticket.requester
        a.ticket = ticket
        a.recipients = recipients
        a.via_id = via_id
      end
    end
    let(:recipients_notification_to) { audit.recipients_notification_to }
    let(:minimum_end_user_id) { users(:minimum_end_user).id }
    let(:ccd_user) { users(:minimum_search_user) }
    let(:notification_to_recipient_ids) { [minimum_end_user_id] }
    let(:notification_ccs) { [ccd_user] }
    let(:create_notification) { true }

    before do
      if create_notification
        @notification = NotificationWithCcs.new(
          to_recipient_ids: notification_to_recipient_ids,
          subject: "subject",
          body: "body",
          audit: audit,
          ccs: notification_ccs
        )
        @notification.save!
      end
    end

    describe "when via type is MAIL" do
      let(:via_id) { ViaType.MAIL }

      it { refute recipients_notification_to }
    end

    describe "when notification_with_ccs is nil" do
      let(:create_notification) { false }

      it { refute recipients_notification_to }
    end

    describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      describe "when notification_with_ccs has one recipient" do
        let(:notification_ccs) { [] }

        it { recipients_notification_to.must_equal users(:minimum_end_user).id }
      end

      describe "when notification_with_ccs has two recipients" do
        it { recipients_notification_to.must_equal users(:minimum_end_user).id }
      end

      it "accesses the notification through the association" do
        audit.expects(notifications_with_ccs: [@notification]).at_least_once
        recipients_notification_to
      end
    end

    describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
      describe "when notification_with_ccs has one recipient" do
        let(:notification_ccs) { [] }

        it { recipients_notification_to.must_equal users(:minimum_end_user) }
      end

      describe "when notification_with_ccs has two recipients" do
        it { recipients_notification_to.must_equal users(:minimum_end_user) }
      end

      it "does not access the notification through the association" do
        audit.expects(:notifications_with_ccs).never
        recipients_notification_to
      end
    end
  end

  describe "#recipients_notification_ccs" do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }
    let(:recipients) { "minimum_end_user@aghassipour.com minimum_search_user@aghassipour.com" }
    let(:via_id) { ViaType.WEB_FORM }
    let(:audit) do
      Audit.create do |a|
        a.account = account;
        a.author = ticket.requester;
        a.ticket = ticket;
        a.recipients = recipients;
        a.via_id = via_id
      end
    end
    let(:recipients_notification_ccs) { audit.recipients_notification_ccs }
    let(:minimum_end_user_id) { users(:minimum_end_user).id }
    let(:ccd_user) { users(:minimum_search_user) }
    let(:notification_to_recipient_ids) { [minimum_end_user_id] }
    let(:notification_ccs) { [ccd_user] }
    let(:create_notification) { true }

    before do
      if create_notification
        notification = NotificationWithCcs.new(
          to_recipient_ids: notification_to_recipient_ids,
          subject: "subject",
          body: "body",
          audit: audit,
          ccs: notification_ccs
        )
        notification.save!
      end
    end

    describe "when via type is MAIL" do
      let(:via_id) { ViaType.MAIL }

      it { recipients_notification_ccs.must_equal [] }
    end

    describe "when notification_with_ccs is nil" do
      let(:create_notification) { false }

      it { recipients_notification_ccs.must_equal [] }
    end

    describe "when notification_with_ccs has one recipient" do
      let(:notification_ccs) { [] }

      it { recipients_notification_ccs.must_equal [] }
    end

    describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      describe "when notification_with_ccs has two recipients" do
        it { recipients_notification_ccs.must_equal [ccd_user.id] }
      end
    end

    describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
      describe "when notification_with_ccs has two recipients" do
        it { recipients_notification_ccs.must_equal [ccd_user] }
      end
    end
  end

  describe "#notifications_suppressed_for=" do
    let(:recipients) do
      [
        users(:minimum_agent).id,
        users(:with_groups_agent1).id,
        users(:with_groups_agent2).id
      ]
    end
    let(:audit) do
      Audit.new do |a|
        a.account = accounts(:minimum);
        a.ticket = tickets(:minimum_1);
        a.recipients = "user_id_1, user_id_2";
        a.via_id = ViaType.MAIL
      end
    end

    before do
      @notifications_suppressed_for_count_limit = Audit::NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT
      Audit.send(:remove_const, :NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT)
      Audit.const_set(:NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT, stubbed_count_limit)
    end

    describe "when recipients is shorter than the stubbed limit #{Audit::NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT}" do
      let(:stubbed_count_limit) { recipients.size + 1 }

      it "does not truncate the recipients" do
        audit.notifications_suppressed_for = recipients
        assert_equal recipients, audit.metadata['notifications_suppressed_for']
      end
    end

    describe "when recipients is equal to the stubbed limit #{Audit::NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT}" do
      let(:stubbed_count_limit) { recipients.size }

      it "does not truncate the recipients" do
        audit.notifications_suppressed_for = recipients
        assert_equal recipients, audit.metadata['notifications_suppressed_for']
      end
    end

    describe "when recipients is longer than the stubbed limit #{Audit::NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT}" do
      let(:stubbed_count_limit) { recipients.size - 1 }

      it "truncates the recipients to the limit" do
        audit.notifications_suppressed_for = recipients
        assert_equal stubbed_count_limit, audit.metadata['notifications_suppressed_for'].size
      end
    end

    after do
      Audit.send(:remove_const, :NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT)
      Audit.const_set(:NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT, @notifications_suppressed_for_count_limit)
    end
  end

  private

  def create_trigger!(name, options)
    options[:conditions_all] ||= []
    options[:conditions_any] ||= []
    options[:actions] ||= options[:action] if options[:action]

    definition = Definition.new

    options[:conditions_all].each do |condition|
      definition.conditions_all << DefinitionItem.new(*condition)
    end

    options[:conditions_any].each do |condition|
      definition.conditions_any << DefinitionItem.new(*condition)
    end

    options[:actions].each do |action|
      definition.actions << DefinitionItem.new(action[0], nil, action[1])
    end

    trigger = @account.triggers.create!(title: name, definition: definition)
    Timecop.travel(Time.now + 1.seconds)
    trigger
  end

  def create_external_change_event(field)
    UserCustomFieldEvent.build_from_values(
      nil,
      create_user_custom_field!(
        field[:key],
        field[:title],
        field[:class]
      ).tap do |custom_field|
        custom_field.stubs(
          value: field[:value],
          info_for_external_change_event: {info: field[:info]}
        )
      end
    )
  end
end
