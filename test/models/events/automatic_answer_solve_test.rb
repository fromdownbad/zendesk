require_relative "../../support/test_helper"

SingleCov.covered!

describe AutomaticAnswerSolve do
  fixtures :tickets
  let(:solved_article) { { id: 1, url: 'https://support.zendesk.com/hc/articles/1.json' } }

  before do
    @ticket = tickets(:minimum_1)
    @ticket.will_be_saved_by(User.system)
  end

  it 'create a solve event when provided with a ticket solved article ' do
    auto_answer_solve_event = AutomaticAnswerSolve.new
    auto_answer_solve_event.solved_article = solved_article
    @ticket.audit.events << auto_answer_solve_event

    assert_equal(solved_article[:id], auto_answer_solve_event.solved_article[:id])
    assert(auto_answer_solve_event.valid?)
  end

  it 'does not create a solve event when provided with an invalid solved article' do
    auto_answer_solve_event = AutomaticAnswerSolve.new
    @ticket.audit.events << auto_answer_solve_event

    refute auto_answer_solve_event.valid?
  end

  it 'does not create a solve event when provided with an solved article without url' do
    auto_answer_solve_event = AutomaticAnswerSolve.new
    auto_answer_solve_event.solved_article = { id: 1, url: nil }
    @ticket.audit.events << auto_answer_solve_event

    refute auto_answer_solve_event.valid?
  end
end
