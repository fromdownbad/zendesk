require_relative "../../support/test_helper"
require_relative "../../support/collaboration_settings_test_helper"

SingleCov.covered!

describe Notification do
  fixtures :accounts, :events, :users, :tickets, :recipient_addresses, :brands,
    :user_identities, :organizations, :ticket_fields, :ticket_field_entries, :groups,
    :account_settings

  extend ArturoTestHelper

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    Account.any_instance.stubs(:default_brand).returns(brands(:minimum))
  end
  should_validate_presence_of :recipients
  should_serialize :value

  describe "sending notifications" do
    before do
      @end_user = users(:minimum_end_user)
      @agent = users(:minimum_agent)
      @notification = subject.notifications.create(via_id: ViaType.RULE,
                                                   via_reference_id: -1, recipients: [@end_user, @agent].map(&:id),
                                                   account: accounts(:minimum), subject: "New Comment", body: "There was a new comment...")
    end

    describe "untrusted audit" do
      subject { events(:untrusted_audit_for_minimum_ticket_1) }

      it "sends both agent and end-user notifications" do
        assert_includes @notification.send(:valid_to_recipients), @end_user
        assert_includes @notification.send(:valid_to_recipients), @agent
      end
    end

    describe "trusted audit" do
      subject { events(:create_audit_for_minimum_ticket_1) }

      it "sends both agent and end-user notifications" do
        assert_includes @notification.send(:valid_to_recipients), @end_user
        assert_includes @notification.send(:valid_to_recipients), @agent
      end
    end
  end

  describe 'when a notification is created' do
    let(:notification) do
      Notification.new(
        account:    accounts(:minimum),
        ticket:     tickets(:minimum_1),
        recipients: [users(:minimum_agent), users(:minimum_end_user)].map(&:id),
        audit:      events(:create_audit_for_minimum_ticket_1),
        subject:    'The rain in Spain',
        body:       'Stays mainly in the plain'
      )
    end

    before_should 'call `enqueue_ticket_update` in the after commit' do
      notification.expects(:enqueue_ticket_update).once

      notification.save
    end
  end

  describe '#enqueue_ticket_update' do
    let(:agent)           { users(:minimum_agent) }
    let(:agent2)          { users(:minimum_admin) }
    let(:suspended_agent) { users(:minimum_admin_not_owner) }

    let(:end_user)        { users(:minimum_end_user) }
    let(:end_user2)       { users(:minimum_author) }
    let(:suspended_user)  { users(:minimum_search_user) }
    let(:email_less_user) { users(:minimum_end_user2) }

    let(:account) { accounts(:minimum) }
    let(:ticket)  { tickets(:minimum_1) }
    let(:audit)   { events(:create_audit_for_minimum_ticket_1) }

    let(:recipients) do
      [
        agent,
        agent2,
        suspended_agent,
        end_user,
        end_user2,
        suspended_user,
        email_less_user
      ].map(&:id)
    end

    let(:notification) do
      Notification.new(
        account:    account,
        ticket:     ticket,
        audit:      audit,
        subject:    'The rain in Spain',
        body:       'Stays mainly in the plain',
        recipients: recipients
      )
    end

    let(:expected_recipients) do
      account.
        users.
        where(id: [agent.id, agent2.id, end_user.id, end_user2.id]).
        select('id, account_id, name, roles').
        includes(:identities, :settings).
        to_a.
        sort { |a, b| recipients.find_index(a) <=> recipients.find_index(b) }
    end

    before do
      suspended_user.suspend!
      suspended_agent.suspend!
    end

    describe 'when the notification have been delivered' do
      describe ':agent_as_end_user Arturo is disabled' do
        let(:invocations) do
          [
            [
              notification,
              ticket,
              [expected_recipients[0], expected_recipients[3]].to_set,
              false
            ],
            [
              notification,
              ticket,
              [expected_recipients[1], expected_recipients[2]].to_set,
              true
            ]
          ]
        end

        before do
          Arturo.disable_feature!(:agent_as_end_user)
          notification.send(:enqueue_ticket_update)
        end

        before_should 'deliver separate notifications to agents and end users' do
          EventsMailer.
            expects(:deliver_rule).
            times(2).
            with do |notification, ticket, recipients, for_agent|
              invocations.shift == [
                notification,
                ticket,
                recipients.to_set,
                for_agent
              ]
            end
        end
      end

      describe ':agent_as_end_user Arturo is enabled' do
        let(:via_id) { ViaType.WEB_SERVICE }
        let(:via_reference_id) { ViaType.HELPCENTER }
        let(:expected_end_user_recipients) { [expected_recipients[0], expected_recipients[3], expected_recipients[1]] }
        let(:expected_agent_recipients) { [expected_recipients[2]] }

        let(:invocations) do
          [
            [notification, ticket, expected_end_user_recipients.to_set, false],
            [notification, ticket, expected_agent_recipients.to_set, true]
          ]
        end

        before do
          Arturo.enable_feature!(:agent_as_end_user)
          ticket.update_column(:via_id, via_id)
          ticket.update_column(:via_reference_id, via_reference_id)
        end

        describe 'via_id does not match agent-as-end-user requirements' do
          let(:via_id) { ViaType.RULE }
          let(:expected_end_user_recipients) { [expected_recipients[0], expected_recipients[3]] }
          let(:expected_agent_recipients) { [expected_recipients[1], expected_recipients[2]] }

          it 'treats submitter as agent' do
            EventsMailer.
              expects(:deliver_rule).
              times(2).
              with do |notification, ticket, recipients, for_agent|
                invocations.shift == [
                  notification,
                  ticket,
                  recipients.to_set,
                  for_agent
                ]
              end

            notification.send(:enqueue_ticket_update)
          end
        end

        describe 'via_reference_id does not match agent-as-end-user requirements' do
          let(:via_reference_id) { nil }
          let(:expected_end_user_recipients) { [expected_recipients[0], expected_recipients[3]] }
          let(:expected_agent_recipients) { [expected_recipients[1], expected_recipients[2]] }

          it 'treats submitter as agent' do
            EventsMailer.
              expects(:deliver_rule).
              times(2).
              with do |notification, ticket, recipients, for_agent|
                invocations.shift == [
                  notification,
                  ticket,
                  recipients.to_set,
                  for_agent
                ]
              end

            notification.send(:enqueue_ticket_update)
          end
        end

        describe 'via_id and via_reference_id match agent-as-end-user requirements' do
          [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |test_via_id|
            describe "via_id is #{test_via_id}" do
              let(:via_id) { test_via_id }

              describe 'submitter is agent' do
                it 'treats submitter as end user' do
                  EventsMailer.
                    expects(:deliver_rule).
                    times(2).
                    with do |notification, ticket, recipients, for_agent|
                      invocations.shift == [
                        notification,
                        ticket,
                        recipients.to_set,
                        for_agent
                      ]
                    end

                  notification.send(:enqueue_ticket_update)
                end
              end

              describe 'submitter is admin' do
                let(:expected_end_user_recipients) { [expected_recipients[0], expected_recipients[3]] }
                let(:expected_agent_recipients) { [expected_recipients[1], expected_recipients[2]] }

                before do
                  ticket.update_column(:submitter_id, expected_recipients[2].id)
                end

                it 'treats submitter as agent' do
                  EventsMailer.
                    expects(:deliver_rule).
                    times(2).
                    with do |notification, ticket, recipients, for_agent|
                      invocations.shift == [
                        notification,
                        ticket,
                        recipients.to_set,
                        for_agent
                      ]
                    end

                  notification.send(:enqueue_ticket_update)
                end
              end
            end
          end
        end
      end
    end

    describe 'when the ticket does not exist' do
      let(:ticket) { nil }

      before { notification.send(:enqueue_ticket_update) }

      before_should 'not deliver a notification' do
        EventsMailer.expects(:deliver_rule).never
      end
    end

    describe 'when delivery count exceeds the `collaborators_maximum` account setting' do
      before { account.update_attributes(settings: {collaborators_maximum: 0}) }

      describe 'and delivery count does not exceed COLLABORATORS_NOTIFICATION_LIMIT' do
        it 'does not add a `TranslatableError` event to audit events' do
          EventsMailer.expects(:deliver_rule).twice
          notification.send(:enqueue_ticket_update)
          refute_equal 'TranslatableError', audit.events.last.type
        end
      end

      describe 'and delivery count exceeds COLLABORATORS_NOTIFICATION_LIMIT' do
        before do
          @collaborators_notification_limit = Notification::COLLABORATORS_NOTIFICATION_LIMIT
          Notification.send(:remove_const, :COLLABORATORS_NOTIFICATION_LIMIT)
          Notification.const_set(:COLLABORATORS_NOTIFICATION_LIMIT, 0)
          notification.send(:enqueue_ticket_update)
        end

        after do
          Notification.send(:remove_const, :COLLABORATORS_NOTIFICATION_LIMIT)
          Notification.const_set(:COLLABORATORS_NOTIFICATION_LIMIT, @collaborators_notification_limit)
        end

        before_should 'not deliver a notification' do
          EventsMailer.expects(:deliver_rule).never
        end

        it 'adds a `TranslatableError` event to audit events' do
          assert_equal 'TranslatableError', audit.events.last.type
        end
      end
    end

    describe 'when the number of recipients exceeds the delivery chunk limit' do
      before do
        Notification.send(:remove_const, :LEGACY_DELIVERY_CHUNK_LIMIT)
        Notification.const_set(:LEGACY_DELIVERY_CHUNK_LIMIT, 1)

        notification.send(:enqueue_ticket_update)
      end

      after do
        Notification.send(:remove_const, :LEGACY_DELIVERY_CHUNK_LIMIT)
        Notification.const_set(:LEGACY_DELIVERY_CHUNK_LIMIT, 25)
      end

      before_should 'deliver one notification to each chunk' do
        EventsMailer.expects(:deliver_rule).times(4)
      end
    end

    describe 'when there are no recipients' do
      let(:recipients) { [] }

      before { notification.send(:enqueue_ticket_update) }

      before_should 'not deliver a notification' do
        EventsMailer.expects(:deliver_rule).never
      end
    end
  end

  describe "aliased attributes" do
    before { @notification = Notification.new }

    it "alias :value_previous to :subject" do
      @notification.expects :value_previous
      @notification.subject
    end

    it "alias :value_reference to :body" do
      @notification.expects :value_reference
      @notification.body
    end

    it "alias :value to :recipients" do
      @notification.expects :value
      @notification.recipients
    end
  end

  describe "setting defaults" do
    let(:recipients) { [users(:minimum_agent).id] }

    before do
      audit = subject

      @notification = Notification.create(
        recipients: recipients,
        audit: audit,
        subject: 'This is the notification title',
        body: 'This is the notification body'
      )
    end

    describe "with a comment audit" do
      subject do
        events(:create_audit_for_minimum_ticket_1).tap do |audit|
          audit.comment.author_id = users(:minimum_admin).id
        end
      end

      it "grabs the author_id of the comment" do
        assert_equal subject.comment.author, @notification.author
      end
    end

    describe "without a comment audit" do
      subject do
        events(:create_audit_for_minimum_ticket_1).tap do |audit|
          audit.comment.delete
        end
      end

      it "grabs the author_id of the audit" do
        assert_equal subject.author, @notification.author
      end
    end

    describe "if the notification is sent to the comment end user" do
      let(:recipients) { [users(:minimum_end_user).id] }

      subject do
        events(:create_audit_for_minimum_ticket_1).tap do |audit|
          audit.comment.author_id = users(:minimum_end_user).id
        end
      end

      it "grabs the author_id of the audit" do
        assert_equal subject.author, @notification.author
      end
    end
  end

  it "delivers multiple notification" do
    audit = events(:create_audit_for_minimum_ticket_1)
    recipients = [users(:minimum_agent).id]
    3.times do |i|
      notification = Notification.new(recipients: recipients, subject: "This is the notification title for #{i}", body: "This is the notification body for #{i}")
      audit.events << notification
    end

    audit.reload

    assert_equal 3, ActionMailer::Base.deliveries.size
  end

  it "does not send notification to user whose secondary identity is a recipient" do
    # Create a valid ticket with collaborators
    ticket = tickets(:minimum_1)
    ticket.set_collaborators = "deliverable@example.com" # secondary identity for minimum_end_user
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
    ticket.collaborators.reload
    ticket.add_comment(body: ticket.description, is_public: true)

    # Create audit and notification
    audit = events(:create_audit_for_minimum_ticket_1)
    audit.recipients = "deliverable@example.com"
    audit.save
    recipients = [users(:minimum_agent).id,]
    notification = Notification.new(recipients: recipients, subject: "This is the notification title", body: "This is the notification body")
    audit.events << notification

    # Should only send to the agent
    assert_equal 1, ActionMailer::Base.deliveries.size
  end

  describe "#message_id" do
    describe 'when outbound email has a ticket id' do
      before do
        EventsMailer.any_instance.stubs(:ticket_message_id).returns("original message id")
        @audit = events(:create_audit_for_minimum_ticket_1)
        @notification = Notification.new(
          recipients: [users(:minimum_agent).id, users(:minimum_end_user).id],
          audit: @audit,
          subject: 'This is the notification title',
          body: 'This is the notification body'
        )
        @audit.events << @notification
      end

      it "saves message ID in OutboundEmail" do
        @notification.message_id = "sample message id"
        _(@notification.send(:outbound_email)).wont_be_nil
        _(@notification.send(:outbound_email).message_id).must_equal "sample message id"
      end

      it "finds message ID used in EventsMailer" do
        _(@notification.message_id).must_equal "original message id"
      end
    end

    describe 'when outbound email is missing a ticket id' do
      let(:statsd_client) { @notification.send(:statsd_client) }
      let(:account) { accounts(:minimum) }
      let(:ticket) { tickets(:minimum_1) }
      let(:encoded_id) { ticket.encoded_id.sub('-', '') }
      let(:ticket_message_id) { "<#{encoded_id}_5cddc4b43dc66_3c93fc7c66caf5c7150_sprut@zendesk-test.com>" }
      let(:logger) { stub(info: stub) }

      before do
        EventsMailer.any_instance.stubs(:ticket_message_id).returns(ticket_message_id)
        @audit = events(:create_audit_for_minimum_ticket_1)
        @notification = Notification.new(
          recipients: [users(:minimum_agent), users(:minimum_end_user)].map(&:id),
          audit: @audit,
          subject: 'This is the notification title',
          body: 'This is the notification body'
        )
        # Explicitly create an outbound email without a ticket to simuate the
        # race condition that can occur when a customer misuses the ticket API
        # to quickly create and delete tickets.
        @outbound_email_without_ticket_id = OutboundEmail.new(
          account: account,
          ticket: nil,
          notification: @notification
        )
        Zendesk::StatsD::Client.stubs(:new).returns(stub_for_statsd)
      end

      it 'does not call #save on the outbound email message' do
        OutboundEmail.expects(:new).with(anything).returns(@outbound_email_without_ticket_id).twice
        statsd_client.expects(:increment).with('outbound_email_missing_ticket_id').once
        OutboundEmail.any_instance.expects(:save).never
        Notification.any_instance.stubs(logger: logger)
        logger.expects(:info).with(regexp_matches(/Outbound ticket id is nil for account_id/)).once
        @audit.events << @notification
      end
    end
  end

  describe '#logger' do
    it 'is Rails.logger' do
      assert_same Rails.logger, Notification.send(:logger)
    end
  end

  describe '#to_s' do
    let(:notification) do
      Notification.new(
        account:    accounts(:minimum),
        ticket:     tickets(:minimum_1),
        recipients: [users(:minimum_agent), users(:minimum_end_user)].map(&:id),
        audit:      events(:create_audit_for_minimum_ticket_1),
        subject:    'The rain in Spain',
        body:       'Stays mainly in the plain'
      )
    end

    it 'returns the stringified recipient names' do
      assert_equal 'Agent Minimum, minimum_end_user', notification.to_s
    end
  end
end
