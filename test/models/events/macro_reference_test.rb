require_relative '../../support/test_helper'

SingleCov.covered!

describe MacroReference do
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }
  let(:macro)   { rules(:macro_incident_escalation) }

  let!(:reference_value) { macro.id.to_s }

  let(:macro_reference) do
    MacroReference.create!(
      account:         account,
      ticket:          ticket,
      audit:           ticket.audits.last,
      author:          account.owner,
      value:           reference_value,
      value_previous:  'value_previous',
      value_reference: 'value_reference'
    )
  end

  it 'aliases `macro_id` to `value`' do
    assert_equal macro_reference.value, macro_reference.macro_id
  end

  describe '#macro' do
    it 'is not deleted' do
      refute macro_reference.macro.deleted?
    end

    it 'returns the macro title' do
      assert_equal macro.title, macro_reference.macro.title
    end

    describe 'when the macro is soft deleted' do
      before { macro.soft_delete }

      it 'is deleted' do
        assert macro_reference.macro.deleted?
      end

      it 'returns the title for a deleted macro' do
        assert_equal(
          I18n.t(
            'txt.api.v2.rule_source.soft_deleted_reference',
            title: macro.title
          ),
          macro_reference.macro.title
        )
      end
    end

    describe 'when the macro is hard deleted' do
      before { macro.destroy }

      it 'is deleted' do
        assert macro_reference.macro.deleted?
      end

      it 'returns the title for an unknown macro' do
        assert_equal 'Unknown rule (deleted)', macro_reference.macro.title
      end
    end
  end
end
