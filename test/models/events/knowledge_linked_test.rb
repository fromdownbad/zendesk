require_relative "../../support/test_helper"

SingleCov.covered!

describe KnowledgeLinked do
  fixtures :accounts, :tickets, :users

  let(:user) { users(:minimum_agent) }
  let(:ticket) { tickets(:minimum_1) }
  let(:article) { { id: 1, url: 'http://helpcenter.article/1.json', title: 'article title 1', html_url: 'http://helpcenter.article/1', locale: 'en-US' } }

  it "creates a new event with an article" do
    kcs_capture_link = KnowledgeLinked.new
    kcs_capture_link.article = article
    ticket.will_be_saved_by(user)
    ticket.audit.events << kcs_capture_link
    ticket.save!

    assert(ticket.events.pluck(:type).include?("KnowledgeLinked"))
    assert_equal(article[:id].to_s, kcs_capture_link.article_id.to_s)
  end

  it "cannot be created without an article" do
    kcs_capture_link = KnowledgeLinked.new

    refute(kcs_capture_link.valid?)
    assert_nil(kcs_capture_link.article_id)
  end

  it "with a nil article sets article_id to nil" do
    kcs_capture_link = KnowledgeLinked.new
    kcs_capture_link.article = nil
    refute(kcs_capture_link.article_id)
  end

  it 'is invalid when the article do not have the {id, url, html_url, url, locale} structure' do
    kcs_capture_link = KnowledgeLinked.new
    kcs_capture_link.article = { id: 1 }

    refute(kcs_capture_link.valid?)
  end
end
