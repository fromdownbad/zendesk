require_relative "../../support/test_helper"

SingleCov.covered!

describe SmsNotification do
  fixtures :accounts, :events, :users, :tickets, :recipient_addresses, :brands,
    :user_identities, :organizations, :ticket_fields, :ticket_field_entries, :groups,
    :account_settings

  should_validate_presence_of :recipients
  should_serialize :value

  describe "aliased attributes" do
    before { @notification = SmsNotification.new }

    it "alias :value_previous to :phone_number_id" do
      @notification.expects :value_previous
      @notification.phone_number_id
    end

    it "alias :value_reference to :body" do
      @notification.expects :value_reference
      @notification.body
    end

    it "alias :value to :recipients" do
      @notification.expects :value
      @notification.recipients
    end
  end

  describe '#after_commit' do
    before { SmsNotification.any_instance.stubs(:sms_enabled?).returns(true) }

    it "does not enqueue if account does not have sms enabled" do
      audit = events(:create_audit_for_minimum_ticket_1)

      notification = SmsNotification.new(
        recipients: [users(:minimum_agent).id],
        audit: audit,
        body: 'something'
      )
      notification.stubs(:sms_enabled?).returns(false)

      notification.via_reference_id = 5
      notification.phone_number_id = '42'
      notification.stubs(:id).returns 999
      Sms::NotificationJob.expects(:enqueue).never
      audit.events << notification
    end

    it "doesn't enqueue if notification has no body" do
      audit = events(:create_audit_for_minimum_ticket_1)

      notification = SmsNotification.new(
        recipients: [users(:minimum_agent).id],
        audit: audit,
        body: ''
      )

      notification.via_reference_id = 5
      notification.phone_number_id = '42'
      notification.stubs(:id).returns 999
      Sms::NotificationJob.expects(:enqueue).never
      audit.events << notification
    end

    it "doesn't enqueue if notification has body but Liquid turns it to be blank" do
      audit = events(:create_audit_for_minimum_ticket_1)

      notification = SmsNotification.new(
        recipients: [users(:minimum_agent).id],
        audit: audit,
        body: 'not empty'
      )

      Zendesk::Liquid::TicketContext.expects(:render).returns(nil)

      notification.via_reference_id = 5
      notification.phone_number_id = '42'
      notification.stubs(:id).returns 999
      Sms::NotificationJob.expects(:enqueue).never
      audit.events << notification
    end

    it "enqueues one Sms::NotificationJob" do
      audit = events(:create_audit_for_minimum_ticket_1)

      notification = SmsNotification.new(
        recipients: [users(:minimum_agent).id],
        audit: audit,
        body: 'This is the notification body {{ticket.id}}'
      )
      notification.via_reference_id = 5
      notification.phone_number_id = '42'

      # Need to stub ID so that we can use it in the expectation below
      notification.stubs(:id).returns 999

      Sms::NotificationJob.expects(:enqueue).with(audit.account_id,
        phone_number_id: '42',
        user_id: users(:minimum_agent).id,
        body: "This is the notification body #{audit.ticket.nice_id}",
        trigger_id: 5,
        ticket_id: audit.ticket_id,
        event_id: 999)

      audit.events << notification
    end
  end
end
