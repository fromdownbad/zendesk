require_relative '../../support/test_helper'

SingleCov.covered!

describe ScheduleAssignment do
  fixtures :all

  let(:account) { accounts(:minimum) }

  let(:previous_schedule) do
    account.schedules.create!(name: 'schedule_1', time_zone: account.time_zone)
  end
  let(:new_schedule) do
    account.schedules.create!(name: 'schedule_2', time_zone: account.time_zone)
  end

  let(:schedule_assignment) do
    ScheduleAssignment.new(
      value_previous: previous_schedule.try(:id),
      value: new_schedule.try(:id)
    )
  end

  before { Account.any_instance.stubs(has_multiple_schedules?: true) }

  describe '#previous_schedule_id' do
    it "returns the `value_previous` value" do
      assert_equal(
        schedule_assignment.value_previous,
        schedule_assignment.previous_schedule_id
      )
    end
  end

  describe '#new_schedule_id' do
    it "returns the `value` value" do
      assert_equal(
        schedule_assignment.value,
        schedule_assignment.new_schedule_id
      )
    end
  end

  describe '#previous_schedule' do
    describe 'when there is not a previous schedule' do
      let(:previous_schedule) { nil }

      it 'returns nil' do
        assert_nil schedule_assignment.previous_schedule
      end
    end

    describe 'when there is a previous schedule' do
      it 'returns the previous schedule' do
        assert_equal previous_schedule, schedule_assignment.previous_schedule
      end
    end
  end

  describe '#new_schedule' do
    describe 'when there is not a new schedule' do
      let(:new_schedule) { nil }

      it 'returns nil' do
        assert_nil schedule_assignment.new_schedule
      end
    end

    describe 'when there is a new schedule' do
      it 'returns the new schedule' do
        assert_equal new_schedule, schedule_assignment.new_schedule
      end
    end
  end
end
