require_relative "../../support/test_helper"

SingleCov.covered!

describe OrganizationActivity do
  fixtures :all

  describe 'when an `OrganizationActivity` event is created' do
    let(:account) { accounts(:minimum) }
    let(:ticket)  { tickets(:minimum_1) }
    let(:audit)   { events(:create_audit_for_minimum_ticket_1) }

    let(:agent)    { users(:minimum_agent) }
    let(:end_user) { users(:minimum_end_user) }

    let(:notification) do
      OrganizationActivity.new(
        account: account,
        ticket:  ticket,
        audit:   audit,
        value:   [agent.id, end_user.id]
      )
    end

    let(:expected_end_user) do
      account.
        users.
        where(id: end_user.id).
        select('id, account_id, name, roles').
        includes(:identities, :settings).
        to_a
    end

    let(:expected_agent) do
      account.
        users.
        where(id: agent.id).
        select('id, account_id, name, roles').
        includes(:identities, :settings).
        to_a
    end

    let(:invocations) do
      [
        [ticket, expected_end_user, notification, notification.id],
        [ticket, expected_agent, notification, notification.id]
      ]
    end

    before { notification.send(:enqueue_ticket_update) }

    before_should 'deliver seperate notifications to agents and end users' do
      EventsMailer.
        expects(:deliver_organization_activity_notification).
        twice.
        with do |ticket, recipient, notification, id|
          invocations.shift == [ticket, recipient, notification, id]
        end
    end

    describe 'when the ticket does not exist' do
      let(:ticket) { nil }

      before { notification.send(:enqueue_ticket_update) }

      before_should 'not deliver a notification' do
        EventsMailer.expects(:deliver_organization_activity_notification).never
      end
    end
  end

  describe '#body' do
    let(:account) { accounts(:minimum) }

    let(:notification) do
      OrganizationActivity.new(
        account: account,
        ticket:  tickets(:minimum_1),
        audit:   events(:create_audit_for_minimum_ticket_1),
        value:   [42, 13]
      )
    end

    it 'returns the organization activity email template' do
      assert_equal(
        account.organization_activity_email_template,
        notification.body
      )
    end
  end
end
