require_relative "../../support/test_helper"

SingleCov.covered!

describe AuditFlags do
  fixtures :events

  before do
    @audit_flags = AuditFlags.new
  end

  describe "#trusted?" do
    it "is trusted with trusted flag" do
      @audit_flags.add(EventFlagType.DEFAULT_TRUSTED)
      assert @audit_flags.trusted?
    end

    it "is not trusted with untrusted flag" do
      @audit_flags.add(EventFlagType.DEFAULT_UNTRUSTED)
      refute @audit_flags.trusted?
    end

    it "is trusted when empty" do
      assert @audit_flags.trusted?
    end
  end

  describe "#allowed_to_comment?" do
    it "is false when empty" do
      refute @audit_flags.allowed_to_comment?
    end

    it "is false when no flags are allowed to comment" do
      @audit_flags.add(EventFlagType.ATTACHMENT_TOO_BIG)
      refute @audit_flags.allowed_to_comment?
    end

    it "is true when the only flag is allowed to comment" do
      @audit_flags.add(EventFlagType.OTHER_USER_UPDATE)
      assert @audit_flags.allowed_to_comment?
    end

    it "is true when one of multiple flags is allowed to comment" do
      @audit_flags.add(EventFlagType.OTHER_USER_UPDATE)
      @audit_flags.add(EventFlagType.ATTACHMENT_TOO_BIG)
      assert @audit_flags.allowed_to_comment?
    end
  end

  describe "#untrusted_flags" do
    before do
      @audit_flags.add(EventFlagType.DEFAULT_TRUSTED)
      @audit_flags.add(EventFlagType.DEFAULT_UNTRUSTED)
    end

    it "returns untrusted flags only" do
      flags = @audit_flags.untrusted_flags
      assert flags.include?(EventFlagType.DEFAULT_UNTRUSTED)
      refute flags.include?(EventFlagType.DEFAULT_TRUSTED)
    end
  end

  describe "#options" do
    it "is stored as a hash" do
      @audit_flags.options.must_equal ({})
    end
  end

  describe "#merge_flags" do
    before do
      @audit = events(:create_audit_for_minimum_ticket_1)
    end

    describe "with untrusted reason" do
      before do
        @audit.flags.merge_flags([EventFlagType.DEFAULT_UNTRUSTED], @audit.flags.options)
        @audit.save_flags!
      end

      it "makes the audit untrusted and flagged" do
        assert @audit.flagged?
        refute @audit.trusted?
      end

      it "adds trusted field with value false to flag options" do
        untrusted_flag_options = @audit.flags.options[EventFlagType.DEFAULT_UNTRUSTED]
        assert untrusted_flag_options.key?("trusted")
        assert_equal false, untrusted_flag_options["trusted"]
      end
    end

    describe "with trusted reason" do
      before do
        @audit.flags.merge_flags([EventFlagType.DEFAULT_TRUSTED], @audit.flags.options)
        @audit.save_flags!
      end

      it "makes the audit flagged but still trusted" do
        assert @audit.flagged?
        assert @audit.trusted?
      end

      it "is no longer flagged after unflag! is used" do
        @audit.unflag!(EventFlagType.DEFAULT_TRUSTED)
        refute @audit.flagged?
        assert @audit.trusted?
      end

      it "adds trusted field with value true to flag options" do
        untrusted_flag_options = @audit.flags.options[EventFlagType.DEFAULT_TRUSTED]
        assert untrusted_flag_options.key?("trusted")
        assert(untrusted_flag_options["trusted"])
      end
    end
  end
end
