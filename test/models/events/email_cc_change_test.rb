require_relative "../../support/test_helper"
require_relative "../../support/collaboration_settings_test_helper"

SingleCov.covered!

describe EmailCcChange do
  fixtures :accounts, :tickets, :users

  let(:ticket) { tickets(:minimum_1) }
  let(:user) { users(:minimum_agent) }

  before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

  def save_new_event(current_email_ccs: nil, previous_email_ccs: nil)
    @event = EmailCcChange.new.tap do |change|
      change.current_email_ccs = current_email_ccs
      change.previous_email_ccs = previous_email_ccs
    end
    ticket.will_be_saved_by(user)
    ticket.audit.events << @event
    ticket.set_nice_id
    ticket.save!
  end

  it "is valid" do
    event = EmailCcChange.new
    assert event.valid?
  end

  it "is read_only if not a new record" do
    event = EmailCcChange.new
    event.stubs(:new_record?).returns(false)
    assert event.readonly?
  end

  it "has a value_reference of `current_email_ccs`" do
    save_new_event
    @event.value_reference.must_equal "current_email_ccs"
  end

  describe "#current_email_ccs" do
    it "is an empty Array by default" do
      save_new_event
      @event.current_email_ccs.must_equal []
    end

    it "accepts Array values" do
      save_new_event(current_email_ccs: [])
      save_new_event(current_email_ccs: ["foo@example.com"])
    end

    it "does not accept String values" do
      assert_raise(ActiveRecord::SerializationTypeMismatch) do
        save_new_event(current_email_ccs: "foo bar")
      end
    end
  end

  describe "#previous_email_ccs" do
    it "is an empty Array by default" do
      save_new_event
      @event.previous_email_ccs.must_equal []
    end

    it "accepts Array values" do
      save_new_event(previous_email_ccs: [])
      save_new_event(previous_email_ccs: ["foo@example.com"])
    end

    it "does not accept String values" do
      assert_raise(ActiveRecord::SerializationTypeMismatch) do
        save_new_event(previous_email_ccs: "foo bar")
      end
    end
  end
end
