require_relative "../../support/test_helper"

SingleCov.covered!

describe 'TicketUnshareEvent' do
  fixtures :tickets, :users, :accounts, :account_property_sets

  resque_inline false

  describe 'Given a ticket unshare event With a soft deleted agreement' do
    before do
      account = accounts(:minimum)
      agreement = FactoryBot.create(:agreement, account: account, name: 'foo')
      agreement.soft_delete!

      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)
      @event = TicketUnshareEvent.new(agreement_id: agreement.id, account: account)

      @ticket.audit.events << @event
    end

    it 'provides a proper recipient_name' do
      assert_equal('foo', @event.recipient_name)
    end
  end

  describe "after create" do
    before do
      account = accounts(:minimum)
      agreement = FactoryBot.create(:agreement, account: account, name: 'foo')

      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@ticket.requester)

      if shared_ticket
        @ticket.shared_tickets.create!(agreement_id: agreement.id)
        @ticket.save!

        @ticket.will_be_saved_by(@ticket.requester)
      end

      @event = TicketUnshareEvent.new(
        agreement_id: agreement.id,
        account: account,
        audit: @ticket.audit
      )
    end

    describe "with a shared_ticket" do
      let(:shared_ticket) { true }

      before { @event.save! }

      it "is removed" do
        assert_equal [], @ticket.reload.shared_tickets
      end
    end

    describe "without a shared_ticket" do
      let(:shared_ticket) { false }

      it "is successful" do
        @event.save!
      end
    end
  end
end
