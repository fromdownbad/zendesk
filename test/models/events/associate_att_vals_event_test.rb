require_relative "../../support/test_helper"

SingleCov.covered!

describe AssociateAttValsEvent do
  fixtures :accounts, :tickets, :users

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:ticket) { tickets(:minimum_1) }

  it "creates a new event with att vals" do
    att_vals_event = AssociateAttValsEvent.new
    att_vals_event.attribute_values = [{id: '1', name: 'att val 1'}]

    ticket.will_be_saved_by(user)
    ticket.audit.events << att_vals_event
    ticket.save!

    assert(ticket.events.pluck(:type).include?("AssociateAttValsEvent"))
  end

  # See https://zendesk.atlassian.net/browse/TR-435
  it "can be created with attribute_values = []" do
    att_vals_event = AssociateAttValsEvent.new
    att_vals_event.attribute_values = []
    assert(att_vals_event.valid?)
  end
end
