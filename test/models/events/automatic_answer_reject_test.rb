require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe AutomaticAnswerReject do
  fixtures :tickets
  let(:article) { { id: 1, url: 'https://support.zendesk.com/hc/articles/1.json', html_url: 'https://support.zendesk.com/hc/articles/1', title: "article title" } }

  before do
    @ticket = tickets(:minimum_1)
    @ticket.will_be_saved_by(User.system)
  end

  it 'create an reject event when provided with an article ' do
    auto_answer_reject_event = AutomaticAnswerReject.new
    auto_answer_reject_event.article = article
    auto_answer_reject_event.reviewer_id = 1
    auto_answer_reject_event.reviewer_name = 'John Doe'

    @ticket.audit.events << auto_answer_reject_event

    assert_equal(article[:id], auto_answer_reject_event.article[:id])
    assert_equal(article[:url], auto_answer_reject_event.article[:url])
    assert_equal(article[:html_url], auto_answer_reject_event.article[:html_url])
    assert_equal(article[:title], auto_answer_reject_event.article[:title])

    assert(auto_answer_reject_event.valid?)
  end

  it 'does not create an reject event when provided without an reviewer id' do
    auto_answer_reject_event = AutomaticAnswerReject.new
    auto_answer_reject_event.article = article
    auto_answer_reject_event.irrelevant = true
    @ticket.audit.events << auto_answer_reject_event

    assert_equal(article[:id], auto_answer_reject_event.article[:id])
    assert_equal(article[:url], auto_answer_reject_event.article[:url])
    assert_equal(article[:html_url], auto_answer_reject_event.article[:html_url])
    assert_equal(article[:title], auto_answer_reject_event.article[:title])

    refute auto_answer_reject_event.valid?
  end

  it 'does not create a solve event when provided with an article without required fields' do
    auto_answer_reject_event = AutomaticAnswerReject.new
    auto_answer_reject_event.article = { id: 1, url: 'https://support.zendesk.com/hc/articles/1.json' }
    @ticket.audit.events << auto_answer_reject_event

    refute auto_answer_reject_event.valid?
  end

  it 'creates a reject event with irrelevant set to false' do
    auto_answer_reject_event = AutomaticAnswerReject.new
    auto_answer_reject_event.irrelevant = true

    assert auto_answer_reject_event.irrelevant
  end

  it 'creates a reject event with irrelevant set to false by default' do
    auto_answer_reject_event = AutomaticAnswerReject.new
    auto_answer_reject_event.send :set_type_and_is_public
    refute auto_answer_reject_event.irrelevant
  end

  it 'creates a reject event with an empty reason by default' do
    auto_answer_reject_event = AutomaticAnswerReject.new
    assert_equal '', auto_answer_reject_event.reason
  end

  it 'creates a reject event with correct reason by default' do
    auto_answer_reject_event = AutomaticAnswerReject.new
    auto_answer_reject_event.reason = 'reject_reason'
    assert_equal 'reject_reason', auto_answer_reject_event.reason
  end
end
