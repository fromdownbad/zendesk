require_relative "../../support/test_helper"

SingleCov.covered!

describe Error do
  should_validate_presence_of :value

  describe "#to_s" do
    before { @error = Error.new }

    it "prints 'Error: 123456' when a value is given" do
      @error.value = 123456
      assert_equal "Error: 123456", @error.to_s
    end

    it "prints 'Error: blank' when no value is given" do
      assert_equal "Error: blank", @error.to_s
    end
  end
end
