require_relative "../../support/test_helper"
require_relative '../../support/collaboration_settings_test_helper'

SingleCov.covered!

describe Cc do
  fixtures :accounts, :events, :tickets

  describe 'when a `Cc` event is created' do
    let(:account) { accounts(:minimum) }
    let(:ticket)  { tickets(:minimum_1) }
    let(:audit)   { events(:create_audit_for_minimum_ticket_1) }

    let(:agent)    { users(:minimum_agent) }
    let(:end_user) { users(:minimum_end_user) }

    let(:notification) do
      Cc.new.tap do |change|
        change.account = account
        change.ticket = ticket
        change.audit = audit
        change.value = [agent.id, end_user.id]
      end
    end

    let(:expected_end_user) do
      account.
        users.
        where(id: end_user.id).
        select('id, account_id, name, roles').
        includes(:identities, :settings).
        to_a
    end

    let(:expected_agent) do
      account.
        users.
        where(id: agent.id).
        select('id, account_id, name, roles').
        includes(:identities, :settings).
        to_a
    end

    let(:invocations) do
      [
        [ticket, expected_end_user, false, notification, notification.id],
        [ticket, expected_agent, true, notification, notification.id]
      ]
    end

    before do
      account.stubs(is_collaboration_enabled?: true)
      ticket.stubs(current_collaborators: stub(present?: true)) if ticket

      notification.send(:enqueue_ticket_update)
    end

    before_should 'deliver seperate notifications to agents and end users' do
      EventsMailer.
        expects(:deliver_follower_notification).
        twice.
        with do |notification, ticket, recipient, for_agent, id|
          invocations.shift == [notification, ticket, recipient, for_agent, id]
        end
    end

    describe 'when the ticket does not exist' do
      let(:ticket) { nil }

      before { notification.send(:enqueue_ticket_update) }

      before_should 'not deliver a notification' do
        EventsMailer.expects(:deliver_follower_notification).never
      end
    end
  end

  describe '#body' do
    let(:audit) { events(:create_audit_for_minimum_ticket_1) }

    let(:notification) do
      Cc.new.tap do |change|
        change.account = accounts(:minimum)
        change.ticket = tickets(:minimum_1)
        change.audit = audit
        change.value = [42, 13]
      end
    end

    it 'returns the audit comment' do
      assert_equal audit.comment, notification.body
    end
  end

  it "is read-only if not a new record" do
    cc = Cc.new
    cc.stubs(:new_record?).returns(false)

    assert cc.readonly?
  end

  describe "with collaboration enabled and CCs/Followers settings disabled" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.account.stubs(:is_collaboration_enabled?).returns(true)
      CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
    end

    it "records the list of ids that were cc'ed in the value field" do
      cc = Cc.create(value: ids = [1, 2, 3, 4, 5], audit: @ticket.audits.first)
      assert ids, cc.value.inspect
    end
  end

  describe "with collaboration disabled and CCs/Followers settings enabled" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.account.stubs(:is_collaboration_enabled?).returns(false)
      CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
    end

    it "records the list of ids that were cc'ed in the value field" do
      cc = Cc.create(value: ids = [1, 2, 3, 4, 5], audit: @ticket.audits.first)
      assert ids, cc.value.inspect
    end
  end
end
