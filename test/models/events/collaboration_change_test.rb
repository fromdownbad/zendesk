require_relative "../../support/test_helper"
require_relative "../../support/collaboration_settings_test_helper"

SingleCov.covered!

describe CollaborationChange do
  fixtures :accounts, :users, :tickets

  let(:account) { accounts(:minimum) }
  let(:end_user) { users(:minimum_end_user) }
  let(:agent) { users(:minimum_agent) }
  let(:admin) { users(:minimum_admin) }
  let(:ticket) do
    ticket = Ticket.new(account: account, description: 'New Ticket')
    ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
    ticket.save!
    ticket
  end

  before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

  describe '.last_event_addresses' do
    let(:events) { ticket.events }

    describe 'when there are no change events' do
      it { assert_equal [], CollaborationChange.last_event_addresses(events) }
    end

    describe 'when the ticket has multiple change events' do
      let(:email_cc_change) do
        EmailCcChange.new.tap do |change|
          change.current_email_ccs = [end_user.email]
          change.previous_email_ccs = []
        end
      end

      let(:follower_change) do
        FollowerChange.new.tap do |change|
          change.current_followers = [agent.email]
          change.previous_followers = []
        end
      end

      before do
        ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
        ticket.audit.events << email_cc_change
        ticket.audit.events << follower_change
        ticket.save!
      end

      it { assert_equal [end_user.email], EmailCcChange.last_event_addresses(events) }
      it { assert_equal [agent.email], FollowerChange.last_event_addresses(events) }
    end
  end

  describe '.current_addresses' do
    before do
      [
        { collaborator_type: CollaboratorType.LEGACY_CC, user: agent },
        { collaborator_type: CollaboratorType.FOLLOWER, user: admin },
        { collaborator_type: CollaboratorType.FOLLOWER, user: agent },
        { collaborator_type: CollaboratorType.FOLLOWER, user: agent }, # A dup
        { collaborator_type: CollaboratorType.EMAIL_CC, user: end_user }
      ].each { |item| ticket.collaborations << Collaboration.new(item) }
    end

    def current_addresses(filter_by)
      CollaborationChange.current_addresses(account, ticket.collaborations, filter_by)
    end

    it 'returns a "truncated" array of primary email addresses and removes the duplicates (if any)' do
      assert_equal [agent.email], current_addresses(:legacy_cc?)
      assert_equal [end_user.email], current_addresses(:email_cc?)
      assert_equal [agent.email, admin.email].sort, current_addresses(:follower?)
    end

    describe 'having in-flight collaborations with in-flight users' do
      let(:inflight_user) do
        User.new(
          name: 'Email CC',
          email: 'in.flight@enduser.com',
          account: account,
          roles: RoleType.ENDUSER
        )
      end

      let!(:inflight_collaboration) do
        ticket.collaborations.build(
          collaborator_type: CollaboratorType.EMAIL_CC,
          user: inflight_user
        )
      end

      it 'still includes the in-flight collaboration' do
        # At this point the user and collaboration are saved.
        collaborations = ticket.collaborations
        inflight_user.save!

        # The in-flight collaboration is still detected, even when the
        # ticket still has the last collaboration as `user_id => nil`
        assert_equal [end_user.email, inflight_user.email].sort,
          CollaborationChange.current_addresses(account, collaborations, :email_cc?)
      end
    end
  end

  describe '.create?' do
    let(:primary_email) { end_user.email }
    let(:work_email) { end_user.identities.email.first.value }
    let(:home_email) { end_user.identities.email.second.value }
    let(:truncation) do
      # Means "someone else" in the context of previous and current list. As "..."
      Zendesk::Events::AuditEvents::ArrayValueTruncation::TRUNCATED_VALUE_INDICATOR
    end

    before do
      # NOTE: this user has 5 emails: primary == work, work != home addresses
      assert_equal primary_email, work_email
      refute_equal work_email, home_email
    end

    describe "when it shouldn't create a Change Event" do
      # No changes
      it { refute CollaborationChange.create?(account, [], []) }

      # Same address before and after.
      it { refute CollaborationChange.create?(account, [primary_email], [primary_email]) }

      # Same user but different address.
      it { refute CollaborationChange.create?(account, [work_email], [home_email]) }

      # User exists, but added with multiple addresses.
      it { refute CollaborationChange.create?(account, [work_email], [work_email, home_email]) }

      # Same user got added multiple times with same addresses.
      it { refute CollaborationChange.create?(account, [work_email], [work_email, work_email]) }
    end

    describe 'when it should create a Change Event' do
      # User got removed.
      it { assert CollaborationChange.create?(account, [primary_email], []) }

      # User got added.
      it { assert CollaborationChange.create?(account, [], [primary_email]) }

      # Previously absent user got added multiple times via different addresses.
      it { assert CollaborationChange.create?(account, [], [work_email, primary_email]) }

      # Same user got added multiple times with different addresses.
      it { assert CollaborationChange.create?(account, [], [work_email, work_email]) }

      # When the actual users are the same, but one of the lists is truncated.
      it { assert CollaborationChange.create?(account, [work_email, home_email], [work_email, home_email, truncation]) }

      # When both lists have been truncated.
      it { assert CollaborationChange.create?(account, [work_email, home_email, truncation], [work_email, home_email, truncation]) }
    end

    describe 'when the list got truncated' do
      let(:previous_addresses) { [] }
      let(:current_addresses) { [] }

      before { CollaborationChange.stubs(truncated?: true) }
      it { assert CollaborationChange.create?(account, previous_addresses, current_addresses) }
    end
  end

  describe '.truncate' do
    let(:array_of_addresses) { [] }

    it 'calls truncate_value from ArrayValueTruncation' do
      Zendesk::Events::AuditEvents::ArrayValueTruncation.expects(:truncate_value).with([], EmailCcChange).once
      EmailCcChange.truncate(array_of_addresses)

      Zendesk::Events::AuditEvents::ArrayValueTruncation.expects(:truncate_value).with([], FollowerChange).once
      FollowerChange.truncate(array_of_addresses)
    end
  end

  describe '.truncated?' do
    subject { EmailCcChange.truncated?(array_of_addresses) }

    describe 'when the last address ends with truncation indicator' do
      let(:array_of_addresses) { ['ceo@example.com', 'boss@example.com', '...'] }
      it { assert subject }
    end

    describe "when the last address doesn't with truncation indicator" do
      let(:array_of_addresses) { ['ceo@zendesk.com', 'boss@example.com'] }
      it { refute subject }
    end
  end

  describe '.primary_email_addresses_from_users' do
    subject { EmailCcChange.primary_email_addresses_from_users(account, user_ids) }

    let(:user_ids) { [end_user.id, agent.id] }
    it { assert_equal [agent.email, end_user.email], subject }
  end

  describe '.user_ids_from_addresses' do
    subject { EmailCcChange.user_ids_from_addresses(account, array_of_addresses) }

    let(:array_of_addresses) { [agent.email, end_user.email] }
    it { assert_equal [agent.id, end_user.id], subject }

    describe "when the user email identity is not verified" do
      before do
        end_user.identities.email.create!(
          value: 'steve.jobs@apple.com',
          is_verified: false
        )
      end

      let(:array_of_addresses) { ['steve.jobs@apple.com'] }
      it { assert_equal [end_user.id], subject }
    end
  end
end
