require_relative "../../support/test_helper"
require_relative "../../support/voice_comment_test_helper"

SingleCov.covered! uncovered: 9

describe 'VoiceComment' do
  include TimeDateHelper
  extend VoiceCommentTestHelper
  fixtures :accounts, :account_property_sets, :attachments, :users, :subscriptions, :groups, :memberships, :tickets
  let(:transcription_text) { "foo: hi\nbar: bye\n" }

  def self.should_i18n_lookup(method, key)
    describe "#method" do
      before { ::I18n.expects(:t).with(key).returns(key.reverse) }
      it "performs an I18n lookup with #{key}" do
        assert_equal key.reverse, @voice_comment.send(method)
      end
    end
  end

  let(:to_number) { '+14155557777' }

  describe "Given a VoiceComment" do
    before do
      @recording_sid = "RE5d4706b2a7ce9c6085119352bc3f96c"
      @recording_url = "https://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/#{@recording_sid}"
      @call = mock('Call')
      @call_id = 7890
      @agent = users(:minimum_agent)
      @now = 'June 04, 2014 05:10:27 AM'
      @ticket = tickets(:minimum_1)
      @call.stubs(:voice_comment_data).returns(recording_url: @recording_url,
                                               recording_duration: "7",
                                               transcription_text: transcription_text,
                                               transcription_status: "completed",
                                               call_duration: 126,
                                               answered_by_id: @agent.id,
                                               answered_by_name: @agent.name,
                                               call_id: @call_id,
                                               from: '+14155556666',
                                               to: to_number,
                                               started_at: @now,
                                               outbound: false,
                                               recorded: true,
                                               line_type: 'phone')
      @call.stubs(:account_id).returns(accounts(:minimum).id)
      @ticket.add_voice_comment(@call, author_id: accounts(:minimum).agents.last.id,
                                       body: "Call recording")
      @ticket.will_be_saved_by(accounts(:minimum).agents.last)
      @ticket.save!
      @voice_comment = @ticket.reload.voice_comments.last
      @external_recording_url = "#{accounts(:minimum).url}/v2/recordings/#{@call_id}/#{@recording_sid}"
    end

    should_i18n_lookup("header_label", "event.voice.details.header")
    should_i18n_lookup("call_from_label", "event.voice.details.from")
    should_i18n_lookup("call_to_label", "event.voice.details.to")
    should_i18n_lookup("time_of_call_label", "event.voice.details.time")
    should_i18n_lookup("location_label", "event.voice.details.location")
    should_i18n_lookup("answered_by_label", "event.voice.details.answeredBy")
    should_i18n_lookup("call_duration_label", "event.voice.details.length")
    should_i18n_lookup("recording_url_label", "event.voice.details.listen")
    should_i18n_lookup("transcription_label", "event.voice.transcription.header")

    describe "#location" do
      before { @voice_comment.data[:location] = "Løgstør" }
      it "returns the data value" do
        assert_equal "Løgstør", @voice_comment.location
      end
    end

    describe "#html_body" do
      before { @voice_comment[:value] = nil }
      it "is based in body" do
        refute_nil @voice_comment.html_body
      end
    end

    describe "#render_location?" do
      describe "when the comment has location data" do
        before { @voice_comment.data[:location] = "Løgstør" }
        it "returns true" do
          assert @voice_comment.render_location?
        end
      end

      describe "when the comment has no location data" do
        before { @voice_comment.data[:location] = nil }
        it "returns false" do
          refute @voice_comment.render_location?
        end
      end
    end

    describe "render_transcription?" do
      describe "when the call is incoming" do
        before do
          @voice_comment.via_id = ViaType.PHONE_CALL_INBOUND
        end

        describe "and there is a voice transcription value" do
          before { @voice_comment.data[:transcription_text] = "hello" }
          it "returns false" do
            assert_equal false, @voice_comment.render_transcription?
          end
        end
      end

      describe "when the call is not incoming" do
        before do
          @voice_comment.via_id = ViaType.VOICEMAIL
        end

        describe "and there is a voice transcription value" do
          before { @voice_comment.data[:transcription_text] = "hello" }
          it "returns true" do
            assert(@voice_comment.render_transcription?)
          end

          describe "and the recorded field is false" do
            before { @voice_comment.data[:recorded] = false }
            it "returns true" do
              assert(@voice_comment.render_transcription?)
            end
          end
        end

        describe "and there is no transcription data" do
          before { @voice_comment.data[:transcription_text] = nil }
          it "returns false" do
            assert_equal(false, @voice_comment.render_transcription?)
          end
        end
      end
    end

    describe "#answered_by_name" do
      before { @voice_comment.data[:answered_by_name] = "Admin Extraordinaire" }
      it "returns the data value" do
        assert_equal "Admin Extraordinaire", @voice_comment.answered_by_name
      end
    end

    describe "#transcription" do
      describe "when transcription is completed" do
        before { @voice_comment.stubs(:transcription_completed?).returns(true) }
        it "returns the transcript" do
          assert_equal @voice_comment.data[:transcription_text], @voice_comment.transcription
        end
      end

      describe "when transcription is not completed" do
        before { @voice_comment.stubs(:transcription_completed?).returns(false) }
        it "returns the missing transcription text" do
          ::I18n.expects(:t).with("event.voice.transcription.not_available").returns("hello")
          assert_equal "hello", @voice_comment.transcription
        end
      end
    end

    describe "#answered_by" do
      describe "when no matching user is found" do
        before { @voice_comment.account.users.expects(:find_by_id).with(@agent.id).returns(nil) }
        it "returns the unknown agent text" do
          ::I18n.expects(:t).with("event.voice.details.unknown_agent").returns("hello")
          assert_equal "hello", @voice_comment.answered_by
        end
      end

      describe "when a user is found" do
        before { @voice_comment.account.users.expects(:find_by_id).with(@agent.id).returns(@agent) }
        it "returns the clean_name of the agent" do
          @agent.expects(:clean_name).returns("Hank")
          assert_equal "Hank", @voice_comment.answered_by
        end
      end
    end

    describe "#via" do
      it "returns true for appropriate via ids" do
        vias = ViaType.list.map(&:last)
        vias.each do |via|
          comment = VoiceComment.new(via_id: via)
          case via
          when ViaType.PHONE_CALL_INBOUND
            assert comment.via_voice?
            assert comment.via_phone?
          when ViaType.PHONE_CALL_OUTBOUND
            assert comment.via_voice?
            assert comment.via_phone?
          when ViaType.VOICEMAIL
            assert comment.via_voice?
            refute comment.via_phone?
          when ViaType.API_PHONE_CALL_INBOUND
            assert comment.via_voice?
          when ViaType.API_PHONE_CALL_OUTBOUND
            assert comment.via_voice?
          when ViaType.API_VOICEMAIL
            assert comment.via_voice?
          else
            refute comment.via_voice?
            refute comment.via_phone?
          end
        end
      end
    end

    describe "#recording_url" do
      describe "when external" do
        it "returns the externally visible URL" do
          assert_equal @external_recording_url, @voice_comment.recording_url
        end
      end

      describe "when :recording_url is nil" do
        it "does not crash" do
          @voice_comment.stubs(:data).returns({})
          @voice_comment.recording.sid
        end

        it "lookups :recording_url from call" do
          @voice_comment.stubs(:call).returns(@call)
          @call.stubs(:recording_url).returns(@recording_url)
          assert_equal @external_recording_url, @voice_comment.recording_url
        end
      end
    end

    describe "#internal_recording_url" do
      describe "when not external" do
        it "returns the Twilio URL" do
          expected = "#{@recording_url}.#{VoiceComment::FORMAT}"
          assert_equal expected, @voice_comment.internal_recording_url
        end
      end
    end

    it "has default format" do
      assert_equal "mp3", VoiceComment::FORMAT
    end

    describe "#player_recording_urls" do
      before do
        @expected = ["#{@recording_url}.mp3", "#{@recording_url}.wav"]
      end

      it "returns supported list of urls" do
        assert_equal @expected, @voice_comment.player_recording_urls
      end

      it "lookups :recording_url from call" do
        @voice_comment.stubs(:call).returns(@call)
        @call.stubs(:recording_url).returns(@recording_url)

        assert_equal @expected, @voice_comment.player_recording_urls
      end
    end

    describe "#recording_duration" do
      it "returns data[:recording_duration]" do
        assert_equal "7", @voice_comment.recording_duration
      end

      it "lookups :recording_duration from call if blank" do
        @voice_comment.stubs(:data).returns(call_id: @call_id)
        @call.stubs(:recording_duration).returns("8")
        @voice_comment.stubs(:call).returns(@call)
        assert_equal "8", @voice_comment.recording_duration
      end
    end

    describe "#call_duration" do
      it "returns data[:call_duration]" do
        assert_equal 126, @voice_comment.call_duration
      end

      it "formats correctly" do
        assert_equal "2 minutes, 6 seconds", format_time_interval(@voice_comment.call_duration)
      end
    end

    describe "#data" do
      it "returns an empty hash when value_previous is not a hash" do
        @voice_comment.expects(:value_previous).returns("foo")
        expected = {}
        assert_equal expected, @voice_comment.data
      end

      it "returns a hash including the clean agent's name" do
        @voice_comment.data[:answered_by_id] = 123

        assert_equal @agent.clean_name, @voice_comment.data[:answered_by_name]
      end
    end

    describe "#to_s" do
      describe "with body passed in" do
        it "returns first line of body" do
          assert_equal "minimum 1 ticket", @voice_comment.to_s
        end
      end

      describe "without body passed in" do
        before do
          @ticket.add_voice_comment(@call, author_id: accounts(:minimum).agents.last.id)
          @ticket.will_be_saved_by(accounts(:minimum).agents.last)
          @ticket.save!
          @voice_comment = @ticket.reload.voice_comments.last
        end

        it "returns first line of subject" do
          assert_equal "minimum 1 ticket", @voice_comment.to_s
        end
      end
    end

    describe "#display_subject" do
      it "returns the first line" do
        assert_equal "minimum 1 ticket", @voice_comment.display_subject
      end

      it "calls ticket_subject to get subject unless via_voice?" do
        @voice_comment.stubs(:ticket_subject).returns("stubbed_subject")
        assert_equal @voice_comment.via_voice?, false
        assert_equal "stubbed_subject", @voice_comment.display_subject
      end
    end

    describe "#formatted_from" do
      it "returns rendered phone number" do
        assert_match(/\+1 \(415\) 555-6666/, @voice_comment.formatted_from)
      end
    end

    describe "#formatted_to" do
      describe "for a PSTN call" do
        it "returns rendered to number" do
          assert_match(/\+1 \(415\) 555-7777/, @voice_comment.formatted_to)
        end
      end

      describe "for a digital call" do
        before do
          @call.stubs(:voice_comment_data).returns(recording_url: @recording_url,
                                                   recording_duration: "7",
                                                   transcription_text: transcription_text,
                                                   transcription_status: "completed",
                                                   call_duration: 126,
                                                   answered_by_id: @agent.id,
                                                   answered_by_name: @agent.name,
                                                   call_id: @call_id,
                                                   from: '+14155556666',
                                                   to: "Digital line",
                                                   started_at: @now,
                                                   outbound: false,
                                                   recorded: true,
                                                   line_type: 'digital')
          @ticket.add_voice_comment(@call, author_id: accounts(:minimum).agents.last.id,
                                           body: "Call recording")
          @ticket.will_be_saved_by(accounts(:minimum).agents.last)
          @ticket.save!
          @voice_comment = @ticket.reload.voice_comments.last
        end

        it "returns digital line nickname" do
          assert_match(/Digital line/, @voice_comment.formatted_to)
        end
      end
    end

    describe "#body" do
      describe "with an archived ticket" do
        before do
          @account = accounts(:minimum)
          @ticket = @account.tickets.build(requester: users(:minimum_end_user),
                                           assignee: users(:minimum_agent),
                                           subject: "Archivable Ticket",
                                           description: "This is a description",
                                           external_id: 5,
                                           group: users(:minimum_agent).groups.first)

          @ticket.add_voice_api_comment(value: "Hello from voice.",
                                        is_public: true,
                                        from: "+14155558888",
                                        to: "+14155559999",
                                        call_duration: 2,
                                        started_at: Time.now.to_s)

          @ticket.will_be_saved_by(@ticket.assignee)
          @ticket.save!
        end

        it "returns non-nil value" do
          archive_and_delete @ticket
          ticket = Ticket.find_by_id(@ticket.id)

          assert ticket.latest_comment.body
        end
      end

      describe "with body passed in" do
        it "returns the complete body" do
          assert_equal "minimum 1 ticket\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: #{@agent.name}\nLength of phone call: 2 minutes, 6 seconds\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n\nTranscription:\n\nfoo: hi\nbar: bye\n\n", @voice_comment.body
        end

        describe "when the call is inbound" do
          before do
            @voice_comment.via_id = ViaType.PHONE_CALL_INBOUND
          end

          describe "when the recording consent is set to always" do
            before do
              @voice_comment.data[:recording_consent_action] = nil
              @voice_comment.data[:recorded] = true
            end

            it "returns the complete body without recording consent information" do
              assert_equal "Inbound call from +1 (415) 555-6666\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: Agent Minimum\nLength of phone call: 2 minutes, 6 seconds\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n", @voice_comment.body
            end
          end

          describe "when the recording consent is set to never" do
            before do
              @voice_comment.data[:recording_consent_action] = nil
              @voice_comment.data[:recorded] = false
              @voice_comment.data[:recording_url] = nil
            end

            it "returns the complete body without recording consent information" do
              assert_equal "Inbound call from +1 (415) 555-6666\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: Agent Minimum\nLength of phone call: 2 minutes, 6 seconds\n", @voice_comment.body
            end
          end

          describe "when the recording consent is set to opt_in" do
            describe "when caller opted in" do
              before do
                @voice_comment.data[:recording_consent_action] = 'caller_opted_in'
                @voice_comment.data[:recorded] = true
              end

              it "returns the complete body with correct recording consent information" do
                assert_equal "Inbound call from +1 (415) 555-6666\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: Agent Minimum\nLength of phone call: 2 minutes, 6 seconds\nCall recording consent: Caller pressed 3 to opt in\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n", @voice_comment.body
              end
            end

            describe "when caller did not opt in" do
              before do
                @voice_comment.data[:recording_consent_action] = 'caller_did_not_opt_in'
                @voice_comment.data[:recorded] = false
                @voice_comment.data[:recording_url] = nil
              end

              it "returns the complete body with correct recording consent information" do
                assert_equal "Inbound call from +1 (415) 555-6666\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: Agent Minimum\nLength of phone call: 2 minutes, 6 seconds\nCall recording consent: Caller did not opt in\n", @voice_comment.body
              end
            end
          end

          describe "when the recording consent is set to opt_out" do
            describe "when caller opted out" do
              before do
                @voice_comment.data[:recording_consent_action] = 'caller_opted_out'
                @voice_comment.data[:recorded] = false
                @voice_comment.data[:recording_url] = nil
              end

              it "returns the complete body with correct recording consent information" do
                assert_equal "Inbound call from +1 (415) 555-6666\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: Agent Minimum\nLength of phone call: 2 minutes, 6 seconds\nCall recording consent: Caller pressed 3 to opt out\n", @voice_comment.body
              end
            end

            describe "when caller did not opt out" do
              before do
                @voice_comment.data[:recording_consent_action] = 'caller_did_not_opt_out'
                @voice_comment.data[:recorded] = true
              end

              it "returns the complete body with correct recording consent information" do
                assert_equal "Inbound call from +1 (415) 555-6666\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: Agent Minimum\nLength of phone call: 2 minutes, 6 seconds\nCall recording consent: Caller did not opt out\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n", @voice_comment.body
              end
            end
          end
        end

        describe "when answered by name is nil" do
          before { @voice_comment.data[:answered_by_name] = nil }

          it "does not include answered by in the body" do
            assert_equal "minimum 1 ticket\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nLength of phone call: 2 minutes, 6 seconds\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n\nTranscription:\n\nfoo: hi\nbar: bye\n\n", @voice_comment.body
          end
        end
      end

      describe "without body passed in" do
        before do
          @ticket.add_voice_comment(@call, author_id: accounts(:minimum).agents.last.id)
          @ticket.will_be_saved_by(accounts(:minimum).agents.last)
          @ticket.save!
          @voice_comment = @ticket.reload.voice_comments.last
        end

        it "returns the entire body with subject in the first line" do
          assert_equal "minimum 1 ticket\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: #{@agent.name}\nLength of phone call: 2 minutes, 6 seconds\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n\nTranscription:\n\nfoo: hi\nbar: bye\n\n", @voice_comment.body
        end
      end

      describe "when comment is a merge comment" do
        it "returns a body with merge content" do
          value_text = "Request #10 \"Phone Call from: Caller Unknown\" was closed and merged into this request. Last comment in request #10: Inbound call from Unknown"
          @voice_comment.stubs(value: value_text, value_previous: @voice_comment.value_previous.merge(merged: true))

          assert_equal "Request #10 \"Phone Call from: Caller Unknown\" was closed and merged into this request. Last comment in request #10: Inbound call from Unknown\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: #{@agent.name}\nLength of phone call: 2 minutes, 6 seconds\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n\nTranscription:\n\nfoo: hi\nbar: bye\n\n", @voice_comment.body
        end
      end

      describe "when comment is a not a merge comment" do
        it "returns a body with without the merge content" do
          assert_equal "minimum 1 ticket\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 AM\nAnswered by: #{@agent.name}\nLength of phone call: 2 minutes, 6 seconds\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n\nTranscription:\n\nfoo: hi\nbar: bye\n\n", @voice_comment.body
        end
      end

      describe "when via_id is passed" do
        before do
          @ticket = Ticket.find(@ticket.id)
          @ticket.via_id = via_id
          @ticket.add_voice_api_comment(value: "TPE inbound",
                                        is_public: true,
                                        from: "+14155558888",
                                        to: "+14155559999",
                                        answered_by_id: users(:minimum_agent).id,
                                        call_duration: 2,
                                        started_at: Time.now.to_s)
          @ticket.will_be_saved_by(accounts(:minimum).agents.last)
          @ticket.save!
          @voice_comment = @ticket.reload.voice_comments.last
        end

        describe "when is an api inbound call" do
          let(:via_id) { ViaType.API_PHONE_CALL_INBOUND }

          it_includes_answered_by
        end

        describe "when is an inbound call" do
          let(:via_id) { ViaType.PHONE_CALL_INBOUND }

          it_includes_answered_by
          it_does_not_include_answered_by_when_name_is_nil
        end

        describe "when is an api outbound call" do
          let(:via_id) { ViaType.API_PHONE_CALL_OUTBOUND }

          it_includes_called_by
          it_does_not_include_answered_by_when_name_is_nil
        end

        describe "when is an outbound call" do
          let(:via_id) { ViaType.PHONE_CALL_OUTBOUND }

          it_includes_called_by
          it_does_not_include_called_by_when_name_is_nil
        end

        describe "when is a voicemail" do
          let(:via_id) { ViaType.VOICEMAIL }

          it_includes_answered_by
        end

        describe "when is an api voicemail" do
          let(:via_id) { ViaType.API_VOICEMAIL }

          it_includes_answered_by
        end

        describe "when is a web service" do
          let(:via_id) { ViaType.WEB_SERVICE }

          it_includes_answered_by
        end
      end
    end

    describe "#redact!" do
      it "redacts text from voicemail transcription" do
        assert @voice_comment.redact!("bar")
        assert_equal "foo: hi\n▇▇▇: bye\n", @voice_comment.transcription
      end

      describe 'when transcription text contatins phone number' do
        let(:transcription_text) { "Its 4pm. My phone number is +1 (415) 555-6666." }

        it "redacts phone number from voicemail transcription" do
          assert @voice_comment.redact!("+1 (415) 555-6666")
          assert_equal "Its 4pm. My phone number is ▇▇ ▇▇▇▇▇ ▇▇▇▇▇▇▇▇.", @voice_comment.transcription
        end
      end

      it "redacts from phone_number in full" do
        assert @voice_comment.redact!("+1 (415) 555-6666")
        assert_equal "▇▇▇▇▇▇▇▇▇▇▇▇", @voice_comment.from
      end

      describe 'when phone number has an extension' do
        let(:to_number) { '+14155557777x123' }

        it 'redacts to phone_number (with extension) in full' do
          assert @voice_comment.redact!('+1 415 555 7777 x 123')
          assert_equal "▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇", @voice_comment.to
        end
      end

      it "redacts from phone_number partially" do
        assert @voice_comment.redact!("555-6666")
        assert_equal "+1415▇▇▇▇▇▇▇", @voice_comment.from
      end

      it "redacts to phone_number partially" do
        assert @voice_comment.redact!('+1 (415) 555-7777')
        assert_equal "▇▇▇▇▇▇▇▇▇▇▇▇", @voice_comment.to
      end

      it "redacts answered_by_name" do
        assert @voice_comment.redact!('Agent Minimum')
        assert_equal "▇▇▇▇▇ ▇▇▇▇▇▇▇", @voice_comment.answered_by_name
      end

      it "redacts answered_by_name partially" do
        assert @voice_comment.redact!('Agent M')
        assert_equal "▇▇▇▇▇ ▇inimum", @voice_comment.answered_by_name
      end

      it "redacts answered_by_name partially in 2 steps" do
        assert @voice_comment.redact!('Agent')
        assert @voice_comment.redact!('Minimum')
        assert_equal "▇▇▇▇▇ ▇▇▇▇▇▇▇", @voice_comment.answered_by_name
      end
    end

    describe '#answered_by_name' do
      describe "when answered_by_name contains newline character" do
        it "returns clean name" do
          @voice_comment.data[:answered_by_name] = "Agent Minimum"

          assert_equal "Agent Minimum", @voice_comment.answered_by_name
        end
      end

      describe "when answered_by_name was redacted" do
        it "returns the redacted text" do
          @voice_comment.data[:answered_by_name] = "▇▇▇▇▇ ▇▇▇▇▇▇▇"

          assert_equal "▇▇▇▇▇ ▇▇▇▇▇▇▇", @voice_comment.answered_by_name
        end
      end
    end

    describe "#process_as_markdown?" do
      before do
        @voice_comment = @ticket.reload.voice_comments.last
        @voice_comment.account.settings.stubs(markdown_ticket_comments?: true)
      end

      describe "when via is Web form" do
        it "returns true" do
          assert @voice_comment.process_as_markdown?
        end
      end

      vias = VoiceComment::VOICE_VIA_TYPES
      vias.each do |via_id|
        describe "when via_id is #{via_id}" do
          before { @voice_comment.via_id = via_id }

          it "returns true" do
            assert @voice_comment.process_as_markdown?
          end
        end
      end
    end
  end

  describe VoiceComment::Recording do
    describe 'with Twilio URL' do
      let(:account_sid) { 'AC1d43679a7bced3f4aaa6c42aa8ef5a3d' }
      let(:sid) { 'RE05b294bfb2cc9b57b4732ee6364f1a91' }
      let(:recording) { VoiceComment::Recording.new("https://api.twilio.com/2010-04-01/Accounts/#{account_sid}/Recordings/#{sid}") }

      it 'has an account_sid and sid' do
        assert_equal 'AC1d43679a7bced3f4aaa6c42aa8ef5a3d', recording.account_sid
        assert_equal 'RE05b294bfb2cc9b57b4732ee6364f1a91', recording.sid
      end
    end

    describe 'with non-Twilio URL' do
      let(:recording) { VoiceComment::Recording.new('https://192.168.17.130/agc/get_recording.php?filename=GC_CHIL_7876053440_20131003160315_CL112D.mp3') }

      it 'does not have an account_sid or sid' do
        assert_nil recording.account_sid
        assert_nil recording.sid
      end
    end
  end
end
