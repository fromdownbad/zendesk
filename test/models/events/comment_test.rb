require_relative "../../support/test_helper"
require_relative "../../support/attachment_test_helper"
require_relative "../../support/arturo_test_helper"
require 'premailer'

SingleCov.covered! uncovered: 20

describe Comment do
  extend ArturoTestHelper

  fixtures :accounts,
    :tokens,
    :tickets,
    :events,
    :users,
    :translation_locales,
    :sequences,
    :signatures,
    :event_decorations,
    :attachments,
    :ticket_metric_sets,
    :suspended_tickets

  before do
    @comment = Comment.new(body: 'New comment')
    @comment.account = accounts(:minimum)
    @comment.author  = users(:minimum_author)
  end

  subject { @comment }

  it 'does not have an HTML-safe to_s' do
    refute @comment.to_s.html_safe?
  end

  describe "validations" do
    it "is valid" do
      assert @comment.valid?
    end

    describe_with_arturo_disabled :validate_author_belongs_to_account_on_create do
      describe "with author that doesn't belong to the account" do
        before { @comment.author = users(:multiproduct_light_agent) }

        it "is valid" do
          assert @comment.valid?
        end
      end
    end

    describe_with_arturo_enabled :validate_author_belongs_to_account_on_create do
      describe "with author that doesn't belong to the account" do
        before { @comment.author = users(:multiproduct_light_agent) }

        it "is not valid" do
          refute @comment.valid?
        end
      end

      describe "with system user as author" do
        before { @comment.author = User.system }

        it "is valid" do
          assert @comment.valid?
        end
      end
    end
  end

  describe "attachments" do
    before do
      @comment.ticket = tickets(:minimum_1)
      @comment.audit  = events(:create_audit_for_minimum_ticket_1)
    end

    describe "single upload" do
      before do
        @upload = tokens(:minimum_upload_token)
        @comment.uploads = @upload.value
        @comment.create_attachments
      end

      it "adds attachments" do
        assert_equal 2, @comment.attachments.size
        assert_equal @upload.attachments, @comment.attachments
      end

      describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
        before do
          @comment.add_attachment_linked_domain_events
        end

        it 'publishes an AttachmentLinkedToComment domain event' do
          domain_event_publisher = FakeEscKafkaMessage.new
          @comment.ticket.domain_event_publisher = domain_event_publisher
          @comment.ticket.will_be_saved_by(users(:minimum_agent))
          @comment.ticket.save!

          events = domain_event_publisher.events.map do |event|
            proto = event.fetch(:value)
            ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(proto)
          end

          assert_equal 2, events.count { |event| event.event == :attachment_linked_to_comment }
        end
      end
    end

    describe "with a private comment" do
      before do
        @comment.is_public = false
        @upload = tokens(:minimum_upload_token)
        @comment.uploads = [@upload.value]
        @comment.create_attachments
      end

      it "makes attachment private" do
        assert @comment.attachments.none?(&:is_public?)
      end
    end

    describe "multiple uploads" do
      before do
        @uploads = [tokens(:minimum_upload_token), tokens(:other_minimum_upload_token)]
        @attachments = @uploads.map(&:attachments).tap(&:flatten!)
        @comment.uploads = @uploads.map(&:value)
        @comment.create_attachments
      end

      it "adds all attachments" do
        assert_equal 3, @comment.attachments.size
        assert_equal @attachments, @comment.attachments
      end

      describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
        before do
          @comment.add_attachment_linked_domain_events
        end

        it 'publishes multiple AttachmentLinkedToComment domain events' do
          domain_event_publisher = FakeEscKafkaMessage.new
          @comment.ticket.domain_event_publisher = domain_event_publisher
          @comment.ticket.will_be_saved_by(users(:minimum_agent))
          @comment.ticket.save!

          events = domain_event_publisher.events.map do |event|
            proto = event.fetch(:value)
            ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(proto)
          end

          assert_equal 3, events.select { |event| event.event == :attachment_linked_to_comment }.size
        end
      end
    end

    it "fails with missing upload" do
      @comment.uploads = ["something-invalid"]
      @comment.body = "waffle"
      assert_raise ActiveRecord::RecordInvalid do
        refute @comment.save! # stop save
        assert_equal ["is invalid"], @comment.errors[:uploads]
      end
    end

    describe "with attachments added directly (without an upload token)" do
      describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
        let(:ticket)  { Ticket.new(account: accounts(:minimum), description: 'ahhh') }
        let(:comment) { ticket.comment }

        before do
          comment.attachments = [attachments(:upload_token_attachment)]
        end

        it 'publishes an AttachmentLinkedToComment domain event' do
          domain_event_publisher = FakeEscKafkaMessage.new
          ticket.domain_event_publisher = domain_event_publisher
          ticket.will_be_saved_by(users(:minimum_agent))
          ticket.save!

          events = domain_event_publisher.events.map do |event|
            proto = event.fetch(:value)
            ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(proto)
          end

          assert_equal 1, comment.attachments.size
          assert_equal 1, events.count { |event| event.event == :attachment_linked_to_comment }
          attachment_event = events.find { |event| event.event == :attachment_linked_to_comment }
          refute_equal 0, attachment_event.attachment_linked_to_comment.attachment.id.value
        end
      end
    end

    describe "with attachments added using #build (without an upload token)" do
      describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
        let(:ticket)  { Ticket.new(account: accounts(:minimum), description: 'ahhh') }
        let(:comment) { ticket.comment }

        before do
          attributes = {
            is_public: false,
            account: comment.account,
            author: comment.author,
            content_type: 'content_type',
            filename: 'some/file.jpg'
          }
          comment.attachments.build(attributes)
          comment.attachments.first.size = 1
        end

        it 'publishes an AttachmentLinkedToComment domain event' do
          domain_event_publisher = FakeEscKafkaMessage.new
          ticket.domain_event_publisher = domain_event_publisher
          ticket.will_be_saved_by(users(:minimum_agent))
          ticket.save!

          events = domain_event_publisher.events.map do |event|
            proto = event.fetch(:value)
            ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(proto)
          end

          assert_equal 1, comment.attachments.size
          assert_equal 1, events.count { |event| event.event == :attachment_linked_to_comment }
          attachment_event = events.find { |event| event.event == :attachment_linked_to_comment }
          refute_equal 0, attachment_event.attachment_linked_to_comment.attachment.id.value
        end
      end
    end
  end

  describe "Updating a comment" do
    before { @comment = tickets(:minimum_1).comments.first }

    it "is disallowed when no changes exist" do
      assert_raise(ActiveRecord::ReadOnlyRecord) { @comment.save! }
    end

    describe "when changes exist to attributes other then :is_public" do
      before { @comment.html_body = "sfshgfsdjkdfsdf" }

      it "is disallowed" do
        assert_raise(ActiveRecord::ReadOnlyRecord) { @comment.save! }
      end

      describe "and a change exists on is_public" do
        before { @comment.is_public = !@comment.is_public }

        it "is disallowed" do
          assert_raise(ActiveRecord::ReadOnlyRecord) { @comment.save! }
        end
      end
    end

    describe "when :is_public is the only changed attribute" do
      describe "and the comment is becoming private" do
        before do
          @comment.is_public = false
          @comment.ticket.ticket_metric_set.replies = 1
          @comment.ticket.will_be_saved_by(users(:minimum_agent))
        end

        it "is allowed" do
          @comment.save!
          refute @comment.is_public?
          event = @comment.ticket.reload.audits.last.events.last
          assert_equal CommentPrivacyChange, event.class
          assert_equal @comment.id, event.event_id.to_i
          assert_equal (@comment.is_public? ? 1 : 0), event.value.to_i
        end

        describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
          it "publishes a CommentMadePrivate domain event" do
            domain_event_publisher = FakeEscKafkaMessage.new
            @comment.ticket.domain_event_publisher = domain_event_publisher
            @comment.save!

            assert_equal 1, domain_event_publisher.events.length
            proto = domain_event_publisher.events.first.fetch(:value)
            event = ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(proto)
            assert_equal :comment_made_private, event.event
          end
        end

        it "decrements ticket replies count" do
          @comment.save!
          assert_equal @comment.ticket.ticket_metric_set.replies, 0
        end

        it "sends a radar notification" do
          RadarFactory.expects(:create_radar_notification).returns(stub(updated: true))
          @comment.save!
        end

        describe "ticket privacy" do
          before do
            @ticket = tickets(:minimum_2)
            @public_comment = @ticket.comments.first
            @user = users(:minimum_agent)
          end

          it "changes ticket to be a private ticket if the ticket has no public comments anymore" do
            # Assert this comment is the only comment on the public ticket
            assert(@ticket.is_public)
            assert_equal 1, @ticket.comments.count

            # Change the only comment on the ticket to private
            @public_comment.is_public = false
            @public_comment.ticket.ticket_metric_set.replies = 1
            @public_comment.ticket.will_be_saved_by(@user)
            @public_comment.save!

            @public_comment.reload
            @ticket.reload

            # Assert the comment changes to private and the ticket changes to private
            assert_equal false, @public_comment.is_public
            assert_equal false, @ticket.is_public

            event = @public_comment.ticket.reload.audits.last.events.last
            assert_equal Change, event.class
            assert_equal "is_public", event.value_reference
            assert_equal @ticket.id, event.ticket_id
            assert_equal 0, event.value.to_i
          end

          it "does not change ticket to be a private ticket if the ticket has other public comments" do
            @ticket.add_comment(is_public: true, body: "Hi again public wombat")
            @ticket.will_be_saved_by(@user)
            @ticket.save!

            assert(@ticket.public_comments.count > 1)
            assert(@ticket.is_public)

            # Change the first comment to be private
            @public_comment.is_public = false
            @public_comment.ticket.ticket_metric_set.replies = 1
            @public_comment.ticket.will_be_saved_by(users(:minimum_agent))
            @public_comment.save!

            event = @public_comment.ticket.reload.audits.last.events.last
            assert_equal CommentPrivacyChange, event.class
            assert_equal @public_comment.id, event.value_reference.to_i
            assert_equal false, @public_comment.is_public
            assert(@ticket.is_public)
          end
        end
      end

      describe "and the comment is becoming public" do
        before do
          Comment.any_instance.stubs(:create_privacy_change_event)
          @comment.update_attributes! is_public: false
          @comment.is_public = true
        end

        it "is disallowed" do
          assert_raise(ActiveRecord::ReadOnlyRecord) { @comment.save! }
        end
      end
    end

    describe "when the ticket is closed" do
      before do
        @comment.ticket.status_id = StatusType.CLOSED
        @comment.ticket.will_be_saved_by(users(:minimum_agent))
        @comment.ticket.save!
        assert @comment.ticket.status_id == StatusType.CLOSED
        assert @comment.is_public?
      end

      describe "and the :is_public attr is changed from public to private" do
        before do
          @comment.is_public = false
          @comment.ticket.will_be_saved_by(users(:minimum_agent))
        end

        it "will fail" do
          assert_raise(ActiveRecord::ReadOnlyRecord) { @comment.save }
          @comment.reload
          assert @comment.is_public?
        end
      end
    end
  end

  describe "#format" do
    let(:comment) { Comment.new }

    it "allows setting the format to rich" do
      refute_equal "rich", comment.format
      comment.format = "rich"
      assert_equal "rich", comment.format
    end

    it "only ever return rich or nil" do
      comment.value_previous = "horse"
      assert_nil comment.format
    end
  end

  describe "#suppress_unicode_replacement" do
    let(:ticket) { tickets(:minimum_1) }
    let(:comment) { ticket.comments.last }

    describe "on comments containing control characters" do
      before do
        ticket.will_be_saved_by(users(:minimum_agent))
        ticket.add_comment(body: "a\bc\x1Dd\x0Fe\x1Cf\x1Eg\x1Fh", html_body: "<p>a\bc\x1Dd\x0Fe\x1Cf\x1Eg\x1Fh</p>")
      end

      describe_with_arturo_enabled :suppress_unicode_replacement do
        it "strips control characters" do
          ticket.save!

          assert_equal "acdefgh", comment.body
          refute_includes comment.html_body, "�"
          refute_includes comment.html_body, "\b"
          refute_includes comment.html_body, "\x1C"
          refute_includes comment.html_body, "\x1D"
          refute_includes comment.html_body, "\x1E"
          refute_includes comment.html_body, "\x1F"
          refute_includes comment.html_body, "\x0F"
        end
      end

      describe_with_arturo_disabled :suppress_unicode_replacement do
        it "replaces control characters with unicode replacement" do
          ticket.save!

          assert_equal "a\bc\u001Dd\u000Fe\u001Cf\u001Eg\u001Fh", comment.body
          assert_includes comment.html_body, "<p dir=\"auto\">a\bc\u001Dd\u000Fe\u001Cf\u001Eg\u001Fh</p>"
        end
      end
    end
  end

  describe "#sanitize_body" do
    let(:ticket) { tickets(:minimum_1) }
    let(:comment) { ticket.comments.last }

    before do
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.add_comment(body: "Hi! #123", html_body: "<style>p { color: #FFFFFF; }</style><p>Hi! #123</p>")
    end

    it "stores filtered markup with ticket linking" do
      ticket.save!

      assert comment.rich?
      assert_equal "<p style=\"color: #FFFFFF\">Hi! <a href=\"/tickets/123\" target=\"_blank\" rel=\"ticket\">#123</a></p>", comment.send(:value)
    end

    describe "error handling" do
      let(:original_plain_body) { "Hello Walter" }
      let(:html_body) { "<div>Hello Walter</div>" }

      before { ticket.comment.html_body = html_body }

      describe "when ZendeskCommentMarkup.filter_input raises an error" do
        let(:error) { StandardError.new("something bad happened") }

        before { ZendeskCommentMarkup.stubs(:filter_input).raises(error) }

        describe "without plain text to fall back on" do
          before { ticket.comment.original_plain_body = nil }

          it { assert_raises(StandardError) { ticket.save! } }

          it "does not record the HTML sanitization error" do
            ZendeskExceptions::Logger.expects(:record).never
            ticket.save! rescue StandardError
          end
        end

        describe "with plain text to fall back on" do
          before { ticket.comment.original_plain_body = original_plain_body }

          it "falls back to plain text" do
            ticket.save!
            comment.body.must_equal original_plain_body
          end

          it "records HTML sanitization error" do
            ZendeskExceptions::Logger.expects(:record).with do |exception, params|
              assert_equal(error, exception)
              assert_equal(Comment, params[:location].class)
              assert_equal('Error sanitizing HTML comment value', params[:message])
            end.once
            ticket.save!
          end
        end
      end
    end
  end

  describe "truncation" do
    let(:ticket) do
      ticket = tickets(:minimum_1)
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket
    end

    let(:limit) { Zendesk::MtcMpqMigration::Content.max_content_size }

    describe "plaintext" do
      let(:comment) do
        comment = ticket.add_comment(body: 'a' * limit * 2)
        ticket.save!
        comment
      end

      it "truncates the text" do
        assert_equal limit, comment.send(:value).bytesize
        assert_equal ('a' * limit), comment.send(:value)
      end

      it "records a truncation audit flag" do
        assert comment.audit.flags.include?(EventFlagType.EXCEEDED_64K_SIZE)
      end

      it "remains valid after truncation (because we're messing with before_validation)" do
        assert comment.valid?
      end
    end

    describe "html" do
      let(:ellip) { "&hellip;" }
      let(:bytesize_of_element_tags) { "<div></div>".bytesize }
      let(:desired_result) { "<div>#{'a' * (limit - ellip.bytesize - bytesize_of_element_tags) + ellip}</div>" }
      let(:html_comment_value) { "<div>#{'a' * limit}</div>" }
      let!(:comment) do
        comment = ticket.add_comment(html_body: html_comment_value)
        ticket.save!
        comment
      end

      it "truncates the HTML, retaining legal element structure when content is too large to be stored in comment" do
        # Bytesize comparison is often easier to debug than comparing strings when things are not working
        assert_equal desired_result.bytesize, comment.send(:value).bytesize
        assert_equal desired_result, comment.send(:value)
      end

      it "records a truncation audit flag" do
        assert comment.audit.flags.include?(EventFlagType.EXCEEDED_64K_SIZE)
      end

      it "remains valid after truncation (because we're messing with before_validation)" do
        assert comment.valid?
      end
    end
  end

  describe "#redaction_event" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @comment = @ticket.add_comment(body: "hello")
      @comment.build_redaction_event
      @ticket.save!
      @ticket.reload
      @comment.reload
    end

    it "autosaves" do
      assert @comment.redaction_event
    end

    it "inherits the new comment's audit" do
      audit = @comment.audit
      assert audit.events.include?(@comment.redaction_event)
    end

    it "is correctly associated with the comment" do
      assert_equal @comment,     @comment.redaction_event.comment
      assert_equal 'comment_id', @comment.redaction_event.value_reference
    end

    it "associates with the ticket" do
      assert(@ticket.redactions?)
    end
  end

  describe "#redact!" do
    let(:comment) { events(:create_comment_for_minimum_ticket_1) }

    before do
      assert_equal "minimum ticket 1", comment.body
    end

    it "is able to update a comment" do
      assert comment.redact!("ticket")
      assert_equal "minimum ▇▇▇▇▇▇ 1", comment.reload.body
    end

    it "does not be able to redact non-existent text" do
      refute comment.redact!("ticketxx")
      assert_equal "minimum ticket 1", comment.reload.body
    end

    it "redacts upper and lower case versions" do
      assert comment.redact!("TiCKet")
      assert_equal "minimum ▇▇▇▇▇▇ 1", comment.reload.body
    end

    it "redacts correct size" do
      assert comment.redact!("1")
      assert_equal "minimum ticket ▇", comment.reload.body
    end

    it "redacts with spaces" do
      assert comment.redact!("ticket 1")
      assert_equal "minimum ▇▇▇▇▇▇ ▇", comment.reload.body
    end

    it "does not redact individual words" do
      refute comment.redact!("minimum 1")
      assert_equal "minimum ticket 1", comment.reload.body
    end

    it "does not redact via regex" do
      refute comment.redact!(".*")
      assert_equal "minimum ticket 1", comment.reload.body
    end

    it "keeps the comment size within the limit after redaction" do
      limit = comment.send(:max_bytesize)
      comment.update_column(:value, "a#{'0' * (limit - 1)}")
      comment.redact!("a")
      assert comment.body.size <= limit
    end

    it "redacts raw_email_identifier" do
      identifier = "872305/5e93f40360118562924909a4767a0c427be8b6f7-4897.eml"
      comment.audit.set_metadata(:system, raw_email_identifier: identifier)
      comment.audit.save!
      assert_equal identifier, comment.audit.reload.metadata[:system][:raw_email_identifier]
      comment.audit.raw_email.expects(:delete_eml_file!).once
      comment.audit.raw_email.expects(:redact_json_files!).once
      assert comment.redact!("ticket 1")
      assert_nil comment.reload.audit.metadata[:system][:raw_email_identifier]
    end

    describe "when comment body contains HTML escaped characters" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @comment = @ticket.add_comment(body: "<3 & Soul", format: 'rich')
        @ticket.save!
      end

      it "redacts left angle bracket" do
        assert @comment.redact!("<")
        assert_equal "▇3 & Soul", @comment.reload.body
      end

      it "redacts ampersand with space" do
        assert @comment.redact!("& ")
        assert_equal "<3 ▇ Soul", @comment.reload.body
      end

      it "redacts left angle bracket and ampersand" do
        assert @comment.redact!("<3 &")
        assert_equal "▇▇ ▇ Soul", @comment.reload.body
      end

      it "keeps the comment size within the limit after redaction" do
        limit = comment.send(:max_bytesize)
        comment.update_column(:value, "a#{'0' * (limit - 1)}")
        comment.redact!("a")
        assert comment.body.size <= limit
      end
    end
  end

  describe "#omniredact!" do
    describe_with_arturo_enabled :agent_workspace_omni_redaction do
      let(:comment) { events(:create_comment_for_minimum_ticket_1) }
      let(:user) { users(:minimum_agent) }
      let(:ticket) { tickets(:minimum_1) }

      before do
        @audit = ticket.will_be_saved_by(user)
      end

      it "is able to update a comment" do
        assert comment.omniredact!("minimum <redact>ticket</redact> 1", user, ticket.audit)
        assert_equal "minimum ▇▇▇▇▇▇ 1", comment.body
      end

      it "does not be able to redact non-existent text" do
        assert comment.omniredact!("minimum ticket 1<redact></redact>", user, ticket.audit)
        assert_equal "minimum ticket 1", comment.body
      end

      it "redacts correct size" do
        assert comment.omniredact!("minimum ticket <redact>1</redact>", user, ticket.audit)
        assert_equal "minimum ticket ▇", comment.reload.body
      end

      it "redacts with spaces" do
        assert comment.omniredact!("minimum <redact>ticket 1</redact>", user, ticket.audit)
        assert_equal "minimum ▇▇▇▇▇▇▇▇", comment.reload.body
      end

      it "does not redact via regex" do
        refute comment.omniredact!(".*", user, ticket.audit)
        assert_equal "minimum ticket 1", comment.reload.body
      end

      it "keeps the comment size within the limit after redaction" do
        limit = comment.send(:max_bytesize)
        comment.update_column(:value, "a#{'0' * (limit - 1)}")
        comment.omniredact!("a", user, ticket.audit)
        assert comment.body.size <= limit
      end

      it "redacts raw_email_identifier" do
        identifier = "872305/5e93f40360118562924909a4767a0c427be8b6f7-4897.eml"
        comment.audit.set_metadata(:system, raw_email_identifier: identifier)
        comment.audit.save!
        assert_equal identifier, comment.audit.reload.metadata[:system][:raw_email_identifier]
        comment.audit.raw_email.expects(:delete_eml_file!).once
        comment.audit.raw_email.expects(:redact_json_files!).twice
        assert comment.omniredact!("minimum <redact>ti</redact>ck<redact>et 1 </redact>", user, ticket.audit)
        assert_nil comment.reload.audit.metadata[:system][:raw_email_identifier]
      end

      describe "when comment body contains HTML escaped characters" do
        before do
          @ticket = tickets(:minimum_1)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @comment = @ticket.add_comment(body: "<3 & Soul", format: 'rich')
          @ticket.save!

          @ticket.will_be_saved_by(users(:minimum_agent))
          @audit = @ticket.audit
        end

        it "redacts left angle bracket" do
          assert @comment.omniredact!("<redact><</redact>3 & Soul", user, @audit)
          assert_equal "▇3 & Soul", @comment.reload.body
        end

        it "redacts ampersand with space" do
          assert @comment.omniredact!("<3<redact> &</redact> Soul", user, @audit)
          assert_equal "<3▇▇ Soul", @comment.reload.body
        end

        it "redacts left angle bracket and ampersand" do
          assert @comment.omniredact!("<redact><3 &</redact> Soul", user, @audit)
          assert_equal "▇▇▇▇ Soul", @comment.reload.body
        end

        it "keeps the comment size within the limit after redaction" do
          limit = comment.send(:max_bytesize)
          value = "a#{'0' * (limit - 1)}"
          comment.update_column(:value, value)
          comment.omniredact!("<redact>#{value}</redact>", user, @audit)
          assert comment.body.size <= limit
        end
      end
    end
  end

  describe "#add_redacted_attachment!" do
    let(:comment) { events(:create_comment_for_minimum_ticket_1) }
    let(:attachment) { create_attachment("#{Rails.root}/test/files/normal_1.jpg") }

    it "is able to add an attachment" do
      assert_empty(comment.attachments)
      comment.add_redacted_attachment!(attachment)
      assert comment.attachments.length == 1
    end

    it "does not add a domain event to the comment's ticket" do
      comment.add_redacted_attachment!(attachment)
      assert_empty(comment.ticket.domain_events)
    end
  end

  describe "Saving a comment" do
    before do
      @ticket = tickets(:minimum_1)
      @comment_string = "foo bar\n\n\"apples bananas\""
    end

    it "does not truncate the comment's value" do
      @ticket.add_comment(body: @comment_string, is_public: true)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.save!
      assert_equal @comment_string, @ticket.reload.comments.last.body
    end

    it 'evaluates comment.updated_at_with_time liquid with the time (hour and minute)' do
      @ticket.add_comment(body: 'The time is: {{ticket.latest_comment.created_at_with_time}}', is_public: true)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.save!
      assert_equal 'The time is: January 1, 2007, 05:32', @ticket.reload.comments.last.body
    end

    it "strips whitespace" do
      @ticket.add_comment(body: "  Hello There  ", is_public: true)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket.save!
      assert_equal "Hello There", @ticket.reload.comments.last.body
    end

    it 'does not raise an exception when given a nil value' do
      @ticket.add_comment(body: nil)
      @ticket.will_be_saved_by(users(:minimum_author))
      @ticket.save!
    end

    describe 'liquid' do
      it 'evaluates liquid placeholders if saved by someone who may use liquid in comments' do
        @ticket.add_comment(body: 'Hello, {{ticket.requester}}', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal 'Hello, Author Minimum', @ticket.reload.comments.last.body
      end

      it 'does not evaluate liquid placeholders if saved by someone who may not use liquid in comments' do
        @ticket.add_comment(body: 'Hello, {{ticket.requester}}', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_author))
        @ticket.save!
        assert_equal 'Hello, {{ticket.requester}}', @ticket.reload.comments.last.body
      end

      it "defaults to original value if passed an invalid liquid template" do
        @ticket.add_comment(body: 'Hello, {%ticket.requester}}', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal 'Hello, {%ticket.requester}}', @ticket.reload.comments.last.body
      end

      describe 'evaluate liquid in requesters language' do
        before do
          @spanish = translation_locales(:spanish)
          @user = users(:minimum_author)
          @user.translation_locale = @spanish
          @ticket.requester = @user
        end

        it 'liquids should be in spanish' do
          @ticket.add_comment(body: 'La prioridad es {{ticket.priority}}', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!
          assert_equal 'La prioridad es Normal', @ticket.reload.comments.last.body
        end
      end

      describe 'Mime type of liquid' do
        before do
          @ticket.account.cms_texts.create!(
            name: "multiline",
            fallback_attributes: {
              is_fallback: true,
              nested: true,
              account: @ticket.account,
              value: "this\r\nis\r\n\r\nmultiline",
              translation_locale_id: 1
            }
          )
        end

        describe_with_arturo_disabled :simple_format_dc_in_rich_comments do
          it 'evaluates placeholder as plain text in html_body if the comment is a rich text comment' do
            @ticket.add_comment(body: 'Multiline: {{dc.multiline}}', is_public: true, format: 'rich')
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!

            assert_equal(
              "<div class=\"zd-comment\" dir=\"auto\">Multiline: this\nis\n\nmultiline</div>",
              @ticket.reload.comments.last.html_body(for_agent: true)
            )
          end

          it 'evaluates the placeholder as plain text if the comment is not a rich text comment' do
            @ticket.add_comment(body: 'Multiline: {{dc.multiline}}', is_public: true, format: nil)
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!

            assert_equal(
              "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Multiline: this<br>\nis</p>\n<p dir=\"auto\">multiline</p></div>",
              @ticket.reload.comments.last.html_body(for_agent: true)
            )
          end
        end

        describe_with_arturo_enabled :simple_format_dc_in_rich_comments do
          it 'evaluates placeholder as HTML in html_body if the comment is a rich text comment' do
            @ticket.add_comment(body: 'Multiline: {{dc.multiline}}', is_public: true, format: 'rich')
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!

            assert_equal(
              "<div class=\"zd-comment\" dir=\"auto\">Multiline: <p dir=\"auto\">this<br>is</p><p dir=\"auto\">multiline</p></div>",
              @ticket.reload.comments.last.html_body(for_agent: true)
            )
          end

          it 'evaluates the placeholder as plain text if the comment is not a rich text comment' do
            @ticket.add_comment(body: 'Multiline: {{dc.multiline}}', is_public: true, format: nil)
            @ticket.will_be_saved_by(users(:minimum_agent))
            @ticket.save!

            assert_equal(
              "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Multiline: this<br>\nis</p>\n<p dir=\"auto\">multiline</p></div>",
              @ticket.reload.comments.last.html_body(for_agent: true)
            )
          end
        end
      end

      describe 'permitted special characters in liquid' do
        let(:comment) do
          <<-LIQUID
            {% case ticket.requester.language %}
            {% when 'Français' %}
            French special character matched
            {% else %}
            special characters not matched
            {% endcase %}
          LIQUID
        end

        before do
          user = users(:minimum_author)
          user.stubs(:should_clear_locale?).returns false
          @ticket.requester = user
          user.translation_locale = translation_locales(:french)
        end

        it 'interprets special characters' do
          @ticket.add_comment(body: comment, is_public: true, format: 'rich')
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          assert_match(
            "French special character matched",
            @ticket.reload.comments.last.html_body(for_agent: true)
          )
        end
      end
    end

    describe "#force_privacy" do
      # Note: force_privacy will be called twice:
      # Once from the Comment model, and once via evaluate_privacy in the Ticket model when ticket.save!
      # Is called.
      before do
        @ticket.add_comment(body: 'public wombat ticket comment', is_public: true, via_id: ViaType.WEB_SERVICE)
      end

      it 'saves the comment as private if authors permissions are restricted' do
        Comment.any_instance.expects(:author_permission_restricted?).at_most(2).returns(true)
        @ticket.will_be_saved_by(users(:minimum_author))
        @ticket.save!

        assert_equal 'public wombat ticket comment', @ticket.reload.comments.last.body
        assert_equal false, @ticket.comments.last.is_public
      end

      it 'saves the comment as private if the ticket has privacy restrictions' do
        Comment.any_instance.expects(:private_ticket_restricted?).at_most(2).returns(true)
        @ticket.will_be_saved_by(users(:minimum_author))
        @ticket.save!

        assert_equal 'public wombat ticket comment', @ticket.reload.comments.last.body
        assert_equal false, @ticket.comments.last.is_public
      end

      it 'saves the comment as private if the ticket is a merge comment on a private ticket' do
        Comment.any_instance.expects(:private_merge_ticket?).at_most(2).returns(true)
        @ticket.will_be_saved_by(users(:minimum_author))
        @ticket.save!

        assert_equal 'public wombat ticket comment', @ticket.reload.comments.last.body
        assert_equal false, @ticket.comments.last.is_public
      end

      it 'does not change the comment privacy if the authors permissions are not restricted and the comments is_public attribute is explicitly set' do
        Comment.any_instance.expects(:author_permission_restricted?).at_most(2).returns(false)
        Comment.any_instance.stubs(:is_public).returns(true)
        @ticket.will_be_saved_by(users(:minimum_author))
        @ticket.save!

        assert_equal 'public wombat ticket comment', @ticket.reload.comments.last.body
        assert(@ticket.comments.last.is_public)
      end
    end

    describe 'an agent signature' do
      let(:limit) { Zendesk::MtcMpqMigration::Content.max_content_size }

      describe "adds signatures on correct types" do
        before do
          Brand.any_instance.stubs(:signature_template).returns("Agent White signing off..")
          @ticket.update_column(:requester_id, users(:minimum_end_user).id)
          @ticket.add_comment(body: 'Hello friend', is_public: true, via_id: ViaType.WEB_FORM)
          @ticket.will_be_saved_by(users(:minimum_agent))
        end

        [ViaType.WEB_FORM, ViaType.WEB_SERVICE, ViaType.CLOSED_TICKET, ViaType.LINKED_PROBLEM, ViaType.MOBILE].each do |type|
          it "adds agent signatures to comment if ticket is via #{ViaType[type].name}" do
            @ticket.via_id = type
            @ticket.save!
            assert_equal "Hello friend\n\nAgent White signing off..", @ticket.reload.comments.last.body
          end
        end
      end

      describe 'with blank rich content' do
        before do
          Brand.any_instance.stubs(:signature_template).returns("{{agent.signature}}")
          users(:minimum_agent).create_signature(html_value: '')
        end

        it 'adds nothing to a plain comment' do
          @ticket.add_comment(body: 'Hello friend', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          assert_equal "Hello friend", @ticket.reload.comments.last.body
        end

        it 'adds nothing to a rich comment' do
          @ticket.add_comment(html_body: '<p>Hello friend</p>', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello friend</p></div>", @ticket.reload.comments.last.html_body
        end
      end

      describe 'with rich content' do
        before do
          Brand.any_instance.stubs(:signature_template).returns("# signature\n{{agent.signature}}")
          users(:minimum_agent).create_signature(html_value: '<strong>Thanks</strong>')
        end

        it 'adds the plain version of the signature to a plain comment' do
          @ticket.add_comment(body: 'Hello friend', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          assert_equal "Hello friend\n\n# signature\n**Thanks**", @ticket.reload.comments.last.body
        end

        it 'adds the rich version of the signature to a rich comment' do
          @ticket.add_comment(html_body: '<p>Hello friend</p>', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello friend</p>\n\n<div class=\"signature\"><h1 dir=\"auto\">signature</h1>\n\n<p dir=\"auto\"><strong>Thanks</strong></p></div></div>", @ticket.reload.comments.last.html_body
        end

        it 'correctly interprets liquid conditions in the signature' do
          Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
          Brand.any_instance.stubs(:signature_template).returns("{{agent.signature}} {% case agent.tags %} {% when 'wombat' %}Wombat! {% when 'lemur' %}Lemur! {% else %}Else! {% endcase %}")

          agent = users(:minimum_agent)
          agent.update_attribute(:current_tags, 'wombat')
          agent.create_signature(html_value: '<strong>Bye!</strong>')

          @ticket.add_comment(html_body: '<p>Hello friend<p>', is_public: true)
          @ticket.will_be_saved_by(agent)
          @ticket.save!

          assert_equal "Hello friend\n\n**Bye!** Wombat!", @ticket.reload.comments.last.body
          assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello friend</p><p dir=\"auto\">\n\n</p><div class=\"signature\"><p dir=\"auto\"><strong>Bye!</strong> Wombat!</p></div></div>", @ticket.reload.comments.last.html_body
        end

        it 'correctly returns a plain text body before saving' do
          @ticket.add_comment(html_body: 'Hello <strong>friend</strong>', is_public: true)

          assert_equal 'Hello **friend**', @ticket.comment.body
        end
      end

      describe 'with blank rich content' do
        before do
          Brand.any_instance.stubs(:signature_template).returns("{{agent.signature}}")
          users(:minimum_agent).create_signature(value: '')
        end

        it 'adds nothing to a plain comment' do
          @ticket.add_comment(body: 'Hello friend', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          assert_equal "Hello friend", @ticket.reload.comments.last.body
        end

        it 'adds nothing to a rich comment' do
          @ticket.add_comment(html_body: '<p>Hello friend</p>', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello friend</p></div>", @ticket.reload.comments.last.html_body
        end
      end

      describe 'with plain content' do
        before do
          Brand.any_instance.stubs(:signature_template).returns("# signature\n{{agent.signature}}")
          users(:minimum_agent).create_signature(value: '**Thanks**')
        end

        it 'adds the plain version of the signature to to a plain comment' do
          @ticket.add_comment(body: 'Hello friend', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          assert_equal "Hello friend\n\n# signature\n**Thanks**", @ticket.reload.comments.last.body
        end

        it 'adds a markdown processed version to a rich comment' do
          @ticket.add_comment(html_body: '<p>Hello friend</p>', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!

          assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello friend</p>\n\n<div class=\"signature\"><h1 dir=\"auto\">signature</h1>\n\n<p dir=\"auto\"><strong>Thanks</strong></p></div></div>", @ticket.reload.comments.last.html_body
        end
      end

      it 'adds agent signatures to plain comment' do
        Brand.any_instance.stubs(:signature_template).returns("Agent White signing off..")
        @ticket.add_comment(body: 'Hello friend', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "Hello friend\n\nAgent White signing off..", @ticket.reload.comments.last.body
        assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello friend</p>\n<p dir=\"auto\">Agent White signing off..</p></div>", @ticket.reload.comments.last.html_body
      end

      it 'truncates plain comment over max with enough space for agent signature' do
        sig = "Agent White signing off.."
        Brand.any_instance.stubs(:signature_template).returns(sig)
        # create a plain comment with body one byte larger than max bytesize
        @ticket.add_comment(body: "a" * (limit + 1), is_public: true, via_id: ViaType.WEB_FORM)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "#{'a' * (limit - 100 - sig.size)}\n\n#{sig}", @ticket.reload.comments.last.body
      end

      it 'truncates plain comment when appended agent signature goes over max bytesize' do
        sig = "Agent White signing off.."
        Brand.any_instance.stubs(:signature_template).returns(sig)
        # create a plain comment not long enough to trigger the limit, but close
        @ticket.add_comment(body: "a" * (limit - sig.size), is_public: true, via_id: ViaType.WEB_FORM)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "#{'a' * (limit - 100 - sig.size)}\n\n#{sig}", @ticket.reload.comments.last.body
      end

      it 'adds agent signatures to html comment' do
        Brand.any_instance.stubs(:signature_template).returns("Agent White signing off..")
        @ticket.add_comment(html_body: '<p>Hello friend</p>', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "Hello friend\n\nAgent White signing off..", @ticket.reload.comments.last.body
        assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello friend</p>\n\n<div class=\"signature\"><p dir=\"auto\">Agent White signing off..</p></div></div>", @ticket.reload.comments.last.html_body
      end

      it 'runs signatures through Markdown rendering' do
        @ticket.add_comment(html_body: '<p>Hello friend</p>', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))

        Brand.any_instance.stubs(:signature_template).returns("# sincerely, agent")
        @ticket.save!

        assert_equal "Hello friend\n\n# sincerely, agent", @ticket.reload.comments.last.body
        assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello friend</p>\n\n<div class=\"signature\"><h1 dir=\"auto\">sincerely, agent</h1></div></div>", @ticket.reload.comments.last.html_body
      end

      describe 'channels tickets' do
        before do
          Brand.any_instance.stubs(:signature_template).returns("Agent White signing off..")
          @ticket.update_column(:requester_id, users(:minimum_end_user).id)
          @ticket.add_comment(body: 'Hello friend', is_public: true, via_id: ViaType.WEB_FORM)
          @ticket.will_be_saved_by(users(:minimum_agent))
        end

        [ViaType.TWITTER_FAVORITE, ViaType.TWITTER_DM, ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE].each do |type|
          it "does not add agent signatures to comment if ticket is via #{type}" do
            @ticket.expects(:create_channel_back_event)
            @ticket.via_id = type
            @ticket.save!
            assert_equal "Hello friend", @ticket.reload.comments.last.body
          end
        end

        [ViaType.TWITTER_FAVORITE, ViaType.TWITTER_DM, ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE].each do |type|
          it "does add agent signatures to comment if ticket is via #{type} and no channeling back" do
            @ticket.comment.stubs(:should_create_channel_back_event?).returns(false)
            @ticket.via_id = type
            @ticket.save!
            assert_equal "Hello friend\n\nAgent White signing off..", @ticket.reload.comments.last.body
          end
        end
      end

      it 'evaluates {{agent.name}} as the comment submitter\'s name' do
        Brand.any_instance.stubs(:signature_template).returns("{{agent.name}} signing off")
        @ticket.add_comment(body: 'Hi wombat', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "Hi wombat\n\nAgent Minimum signing off", @ticket.reload.comments.last.body
      end

      it 'evaluates {{current_user.name}} as the comment submitter\'s name' do
        Brand.any_instance.stubs(:signature_template).returns("{{current_user.name}} signing off")
        @ticket.add_comment(body: 'Hi wombat', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "Hi wombat\n\nAgent Minimum signing off", @ticket.reload.comments.last.body
      end

      it 'does not evaluate {{current_user.name}} as the ticket requester\'s name' do
        Brand.any_instance.stubs(:signature_template).returns("{{current_user.name}} signing off")
        @ticket.add_comment(body: 'Hi wombat', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_not_equal "Hi wombat\n\nAuthor Minimum signing off", @ticket.reload.comments.last.body
      end

      it 'evaluates {{account.name}} as the current account\'s name' do
        Brand.any_instance.stubs(:signature_template).returns("I am from {{account.name}}")
        @ticket.add_comment(body: 'Hi wombat', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "Hi wombat\n\nI am from Minimum account", @ticket.reload.comments.last.body
      end

      it 'evaluates dynamic content in agent signatures' do
        Brand.any_instance.stubs(:signature_template).returns("{{dc.agent_sig}}")
        Zendesk::Liquid::SignatureContext.any_instance.stubs(:render).returns("Agent White signing off..")
        @ticket.add_comment(body: 'Hi wombat', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "Hi wombat\n\nAgent White signing off..", @ticket.reload.comments.last.body
      end

      it 'does not add signature if ticket brand is nil' do
        @ticket.stubs(:brand).returns(nil)
        @ticket.add_comment(body: 'Hi wombat', is_public: true)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
        assert_equal "Hi wombat", @ticket.reload.comments.last.body
      end
    end

    describe 'privacy handling' do
      let(:new_ticket) { Ticket.new(account: accounts(:minimum), subject: "blah", requester_id: users(:minimum_author), description: "descraption") }
      let(:existing_ticket) do
        new_ticket.tap do |t|
          agent = users(:minimum_agent).tap do |a|
            a.stubs(:can?).with(:use_liquid_in, Event).returns(true)
            a.stubs(:can?).with(:add_comment, t).returns(true)
            a.stubs(:can?).with(:add, Comment).returns(true)
            a.stubs(:can?).with(:edit, t).returns(true)
            a.stubs(:can?).with(:publicly, Comment).returns(true)
          end

          t.add_comment(comment)
          t.will_be_saved_by(agent)
          t.save!
        end
      end

      let(:comment_author) do
        users(:minimum_admin).tap do |a|
          a.stubs(:can?).with(:use_liquid_in, Event).returns(true)
          a.stubs(:can?).with(:add_comment, ticket).returns(true)
          a.stubs(:can?).with(:edit, ticket).returns(true)
        end
      end

      let(:public_author) do
        comment_author.tap do |a|
          a.stubs(:can?).with(:publicly, Comment).returns(true)
          a.stubs(:can?).with(:add, Comment).returns(true)
        end
      end
      let(:private_author) do
        comment_author.tap do |a|
          a.stubs(:can?).with(:publicly, Comment).returns(false)
          a.stubs(:can?).with(:add, Comment).returns(true)
        end
      end

      let(:public_comment) { { body: "#{is_public ? 'public' : 'private'} comment", is_public: true } }
      let(:private_comment) { { body: "#{is_public ? 'public' : 'private'} comment", is_public: false } }

      # Explanation of each entry of the text matrix
      # [
      #   ticket: "Is the ticket new or existing?",
      #   comment: "Is the existing comment public or private?",
      #   author: "Can the author of the comment do so publicly?",
      #   is_public: "Is the comment public?",
      #   made_private: "Are we changing the only public comment on a ticket to be private?"
      #   expectation: {
      #     comment: "The comment should be public",
      #     ticket: "The ticket should be public"
      #   }
      # ]
      privacy_tests = [
        { ticket: :new_ticket,      comment: nil,              author: :public_author,  is_public: true,  made_private: false, expectation:  { comment: true,  ticket: true  } },
        { ticket: :new_ticket,      comment: nil,              author: :public_author,  is_public: false, made_private: false, expectation:  { comment: false, ticket: false } },
        { ticket: :new_ticket,      comment: nil,              author: :private_author, is_public: true,  made_private: false, expectation:  { comment: false, ticket: false } },
        { ticket: :new_ticket,      comment: nil,              author: :private_author, is_public: false, made_private: false, expectation:  { comment: false, ticket: false } },

        # Make an existing public comment private, you cannot make an existing private comment public
        { ticket: :existing_ticket, comment: :public_comment,  author: :public_author,  is_public: true,  made_private: true,  expectation:  { comment: false, ticket: false  } },
        { ticket: :existing_ticket, comment: :public_comment,  author: :private_author, is_public: true,  made_private: true,  expectation:  { comment: false, ticket: false  } },

        { ticket: :existing_ticket, comment: :public_comment,  author: :public_author,  is_public: true,  made_private: false, expectation:  { comment: true,  ticket: true  } },
        { ticket: :existing_ticket, comment: :public_comment,  author: :public_author,  is_public: false, made_private: false, expectation:  { comment: false, ticket: true  } },
        { ticket: :existing_ticket, comment: :public_comment,  author: :private_author, is_public: true,  made_private: false, expectation:  { comment: nil,   ticket: true  } },
        { ticket: :existing_ticket, comment: :public_comment,  author: :private_author, is_public: false, made_private: false, expectation:  { comment: false, ticket: true  } },
        { ticket: :existing_ticket, comment: :private_comment, author: :public_author,  is_public: true,  made_private: false, expectation:  { comment: true,  ticket: true  } },
        { ticket: :existing_ticket, comment: :private_comment, author: :public_author,  is_public: false, made_private: false, expectation:  { comment: false, ticket: false } },
        { ticket: :existing_ticket, comment: :private_comment, author: :private_author, is_public: true,  made_private: false, expectation:  { comment: nil,   ticket: false } },
        { ticket: :existing_ticket, comment: :private_comment, author: :private_author, is_public: false, made_private: false, expectation:  { comment: false, ticket: false } }
      ]
      before do
        ticket.will_be_saved_by(author)
        Zendesk::CommentPublicity.stubs(:last_public_comment_made_private?).returns(made_private)
        ticket.add_comment(body: "Saving a #{is_public ? 'public' : 'private'} comment", is_public: is_public)
      end

      privacy_tests.each do |config|
        expectation = config[:expectation]

        describe "with #{config.inspect}" do
          let(:ticket) { send(config[:ticket]) }
          let(:comment) { send(config[:comment]) unless config[:comment].nil? }
          let(:author) { send(config[:author]) }
          let(:is_public) { config[:is_public] }
          let(:made_private) { config[:made_private] }

          if expectation[:comment].nil?
            # Comments left by restricted agents are not valid
            it 'raises an error' do
              assert_raise ActiveRecord::RecordInvalid do
                ticket.save!
              end
            end
          elsif config[:made_private]
            # If we're modifying an existing comment privacy, then we're modifying comment directly via create_privacy_change_event
            before do
              change_comment = ticket.comments.last
              change_comment.ticket.will_be_saved_by(users(:minimum_agent))
              change_comment.is_public = false
              change_comment.save!
              change_comment.reload
              ticket.reload
            end

            it "last public comment made private: the comment is #{expectation[:comment] ? 'public' : 'private'}" do
              assert_equal  expectation[:comment], ticket.comments.last.is_public
            end
            it "last public comment made private: the ticket is #{expectation[:ticket] ? 'public' : 'private'}" do
              assert_equal expectation[:ticket], ticket.is_public
            end
          else
            before do
              ticket.save!
              ticket.reload
            end

            it "the comment is #{expectation[:comment] ? 'public' : 'private'}" do
              assert_equal ticket.comments.last.is_public, expectation[:comment]
            end

            it "the ticket is #{expectation[:ticket] ? 'public' : 'private'}" do
              assert_equal ticket.is_public, expectation[:ticket]
            end
          end
        end
      end
    end

    describe "with via type 'linked problem'" do
      before do
        Brand.any_instance.stubs(:signature_template).returns("{{agent.signature}}")
        User.any_instance.stubs(:signature).returns(Signature.new(value: "Rodney Ruxin"))
        @ticket.add_comment(body: 'Hello friend', is_public: true, via_id: ViaType.LINKED_PROBLEM)
        @ticket.will_be_saved_by(users(:minimum_agent))
        @ticket.save!
      end

      it "adds signature of the author if present" do
        @ticket.comments.last.body.must_include "Rodney Ruxin"
      end
    end

    describe "comment redaction" do
      before { Account.any_instance.stubs(has_credit_card_sanitization_enabled?: true) }

      describe "adding a comment to an existing ticket" do
        before do
          @ticket.add_comment(html_body: '<p>straight cc: 378282246310005</p>', is_public: true)
          @ticket.will_be_saved_by(users(:minimum_agent))
          @ticket.save!
          @ticket.reload
        end

        it "description is not written to" do
          assert_includes @ticket.tag_array, "system_credit_card_redaction"
          assert_equal "minimum 1 ticket", @ticket.reload.description
        end
      end

      describe "when the comment is the first comment on a ticket" do
        it "description is written to and redacted" do
          account = accounts(:minimum)
          author = users(:minimum_agent)
          new_ticket = Zendesk::Tickets::Initializer.new(account,
            author,
            requester_id: author.id,
            ticket: {
              comment: "cc: 378282246310005 :cc"
            }).ticket
          new_ticket.will_be_saved_by(author)
          new_ticket.save!
          new_ticket.reload

          assert_equal "cc: 378282▇▇▇▇▇0005 :cc", new_ticket.reload.description
        end
      end
    end
  end

  describe "#is_email_only?" do
    [:twitter, :twitter_dm, :twitter_favorite, :facebook_post, :facebook_message].each do |channel|
      before do
        @comment = Comment.new
        @comment.stubs(:channel_back_requested?).returns(false)
        @comment.is_public = true
        @comment.via_id = ViaType.WEB_FORM
      end

      describe "for #{channel}" do
        it "is true if all conditions pass" do
          assert @comment.is_email_only?
        end

        it "is false if via #{channel}" do
          @comment.via_id = ViaType.fuzzy_find(channel)
          assert @comment.via_id
          refute @comment.is_email_only?
        end

        it "is false if channeling back" do
          @comment.stubs(:channel_back_requested?).returns(true)
          refute @comment.is_email_only?
        end

        it "is false if is private" do
          @comment.is_public = false
          refute @comment.is_email_only?
        end
      end
    end
  end

  describe "#validate_not_empty" do
    it "adds error 'Please provide a comment/description with your attachment or screencast' if value is blank and there are attachments" do
      @comment.stubs(:uploads).returns(:test_attachment)
      @comment.body = ""
      @comment.send :validate_not_empty

      assert_equal ['Please provide a comment/description with your attachment or screencast'], @comment.errors["base"]
    end
  end

  describe "#validate_under_limit" do
    let(:ticket) { tickets(:minimum_1) }

    before do
      ticket.account.settings.stubs(:comments_per_ticket_limit).returns(2)
      comment_text = "foo bar"
      ticket.add_comment(body: comment_text, is_public: true)
      ticket.will_be_saved_by(users(:minimum_agent))
    end

    it "allows creating a comment when under limit" do
      ticket.comments.stubs(:count).returns(1)
      assert ticket.save!
    end

    it "disallows creating a comment when over limit" do
      ticket.comments.stubs(:count).returns(2)
      assert_raise ActiveRecord::RecordInvalid do
        ticket.save!
      end
    end
  end

  describe "#first?" do
    describe "adding a comment to an existing ticket" do
      let(:ticket) { tickets(:minimum_1) }

      before do
        ticket.add_comment(body: 'foo bar foo', is_public: true)
        ticket.will_be_saved_by(users(:minimum_agent))
        ticket.save!
        ticket.reload
      end

      it "returns value as false" do
        comment = ticket.comments.last
        refute comment.first?
      end
    end

    describe "when the comment is the first comment on a ticket" do
      let(:account) { accounts(:minimum) }
      let(:author) { users(:minimum_agent) }

      it "returns value as true" do
        new_ticket = Zendesk::Tickets::Initializer.new(account,
          author,
          requester_id: author.id,
          ticket: {
            comment: "apple pineapple",
          }).ticket
        new_ticket.will_be_saved_by(author)
        new_ticket.save!
        new_ticket.reload

        comment = new_ticket.comments.last
        assert comment.first?
      end
    end
  end

  describe "#trusted? and #flagged?" do
    before do
      @audit = Audit.new
      @audit.account = accounts(:minimum)
    end

    # [2015.11.02 - abeckwith] this might go away if we refactor untrusted! to use flag!
    describe "with untrusted!" do
      before do
        @audit.untrusted!
      end

      it "is untrusted when untrusted! is used with new comment" do
        comment = Comment.new(audit: @audit)
        comment.expects(:new_record?).returns false
        refute comment.trusted?
      end

      it "is untrusted when untrusted! is used with new ticket'" do
        ticket = Ticket.new
        ticket.audit = @audit
        refute Comment.new(ticket: ticket).trusted?
      end
    end

    describe "#flagged?" do
      describe "with untrusted flag type" do
        before do
          @audit.flag!([EventFlagType.DEFAULT_UNTRUSTED])
        end

        it "is flagged and untrusted when flag! is used with new comment" do
          comment = Comment.new(audit: @audit)
          comment.stubs(:new_record?).returns false
          assert comment.flagged?
          refute comment.trusted?
        end

        it "is flagged and untrusted when flag! is used with new ticket" do
          ticket = Ticket.new
          ticket.audit = @audit
          comment = Comment.new(ticket: ticket)
          assert comment.flagged?
          refute comment.trusted?
        end
      end

      describe "with trusted flag type" do
        before do
          @audit.flag!([EventFlagType.DEFAULT_TRUSTED])
        end

        it "is flagged and trusted when flag! is used with new comment" do
          comment = Comment.new(audit: @audit)
          comment.stubs(:new_record?).returns false
          assert comment.flagged?
          assert comment.trusted?
        end

        it "is flagged and trusted when flag! is used with new ticket" do
          ticket = Ticket.new
          ticket.audit = @audit
          comment = Comment.new(ticket: ticket)
          assert comment.flagged?
          assert comment.trusted?
        end
      end
    end
  end

  describe "#current_audit" do
    let(:ticket) { tickets(:minimum_1) }

    before do
      comment_string = "foo bar\n\n\"apples bananas\""
      ticket.add_comment(body: comment_string, is_public: true)
      ticket.will_be_saved_by(users(:minimum_agent))
    end

    describe "when the comment is new" do
      before { assert ticket.comment.new_record? }

      it "returns the ticket's audit" do
        assert_equal ticket.audit, ticket.comment.current_audit
      end
    end

    describe "when the comment is persisted" do
      before do
        @comment = ticket.comment
        ticket.save!
        refute @comment.reload.new_record?
      end

      it "returns its own audit if the comment is persisted" do
        assert_equal @comment.audit, @comment.current_audit
      end
    end
  end

  describe "#to_s" do
    it "returns its value if it is not nil" do
      @comment.stubs(:value).returns("Test")
      assert_equal "Test", @comment.to_s
    end

    it "returns 'Empty comment' if its value is nil" do
      @comment.stubs(:value)
      assert_equal "Empty comment", @comment.to_s
    end
  end

  describe "#body" do
    describe "memoization" do
      before do
        @comment = tickets(:minimum_1).comments.last
      end

      it "does not evaluate after initial pass" do
        comment_str = @comment.body
        @comment.expects(:value).never
        10.times { assert_equal comment_str, @comment.body }
      end

      it "re-evalutes after reloading the object" do
        @comment.expects(:value).twice.returns("foo") # once for blank check
        @comment.reload
        10.times { assert_equal "foo", @comment.body }
      end

      it "re-evaluates after setting value" do
        @comment.expects(:value).twice.returns("foo")
        @comment.send(:value=, "doesn't matter because expects intercepts anyway")
        10.times { assert_equal "foo", @comment.body }
      end

      it "does not escape * and _" do
        @comment.stubs(:format).returns("rich")
        @comment.stubs(:value).returns("https://example.com/foo_bar/foo*bar")
        assert_equal "https://example.com/foo_bar/foo*bar", @comment.body
      end

      it "does not escape < and >" do
        @comment.stubs(:format).returns("rich")
        @comment.stubs(:value).returns("<div>></div><<>> < >")
        assert_equal ">\n<<>> < >", @comment.body
      end

      it "strips html style tags" do
        @comment.stubs(:format).returns("rich")
        @comment.stubs(:value).returns("<style type=\"text/css\">\n<!--\ntable td\n\t{border-collapse: bla}; -->\n</style>Test reply")
        assert_equal "Test reply", @comment.body
      end

      it "returns the plain_body value if html_stripper fails" do
        @comment.stubs(:format).returns("rich")
        @comment.stubs(:value).returns("<style type=\"text/css\">\n<!--\ntable td\n\t{border-collapse: bla}; -->\n</style>Test reply")
        ZendeskCommentMarkup::StripperFilter.stubs(:to_html).raises(SystemStackError.new)
        assert_equal @comment.plain_body, @comment.body
      end

      it "preserves new lines in pre blocks" do
        @comment.stubs(:format).returns("rich")
        @comment.stubs(:value).returns("Here we go:<br><pre style=\"white-space: pre-wrap\">one<br>two<br>three<br>four<br></pre>")
        assert_equal "Here we go:\n\n    one\n    two\n    three\n    four", @comment.body
      end
    end
  end

  describe '#plain_body' do
    it "correctly renders complex html" do
      rich_text_comment = events(:serialization_comment_with_line_spaces)
      expected_plain_text = "This comment is going\nto have\na lot\nof\nline spaces.\nThis is to test that\n\nline breaks\nare correctly\n\nrendered\nas\n\nnew lines"

      rich_text_comment.plain_body.must_equal expected_plain_text
    end

    it "correctly renders text without any markdown" do
      plain_text_comment = events(:serialization_comment)

      plain_text_comment.plain_body.must_equal "Wibble"
    end

    it "correctly strips out emoji" do
      emoji_comment = events(:serialization_comment_with_emoji)
      expected_plain_text = "Some text with an emoji &nbsp;"

      emoji_comment.plain_body.must_equal expected_plain_text
    end

    it "correctly strips out both rich text and emoji" do
      emoji_and_rich_text_comment = events(:serialization_comment_with_newlines_and_emoji)
      expected_plain_text = "Comment\n\nThis comment has rich text , a link and emoji &nbsp;\n\nIt also\nhas new\nlines"

      emoji_and_rich_text_comment.plain_body.must_equal expected_plain_text
    end

    # See https://github.com/rgrove/sanitize/issues/170
    it "does not replace non-breaking spaces with a normal space character" do
      emoji_and_rich_text_comment = events(:serialization_comment_with_non_breaking_spaces)
      expected_plain_text = "Comment\n\nThis comment&nbsp;has rich text with non-breaking spaces&nbsp;in&nbsp;it"
      emoji_and_rich_text_comment.plain_body.must_equal expected_plain_text
    end

    it "strips html comments in style tags" do
      text_with_comments = events(:serialization_comment_with_html_comments)
      expected_text = "Body text"

      text_with_comments.plain_body.must_equal expected_text
    end

    it "calls Nokogiri::HTML5.fragment" do
      rich_text_comment = events(:serialization_comment_with_line_spaces)
      mock_document_fragment = Nokogiri::HTML5.fragment('')
      Nokogiri::HTML5.expects(:fragment).once.returns(mock_document_fragment)
      rich_text_comment.plain_body
    end

    describe "when html exceeds Nokogumbo::DEFAULT_MAX_TREE_DEPTH" do
      it "returns the expected html fragment" do
        rich_text_comment = events(:serialization_comment_with_line_spaces)
        nested_html_content = nest_html_content('<div>Abc<div/>', Nokogumbo::DEFAULT_MAX_TREE_DEPTH + 1)
        rich_text_comment.update_column('value', nested_html_content)
        assert_equal "Abc", rich_text_comment.plain_body
      end
    end

    describe "when a SystemStackError exception is raised" do
      before { Nokogiri::HTML5.stubs(:fragment).raises(SystemStackError).once }

      it "returns an empty string" do
        Rails.logger.expects(:warn).once.with(regexp_matches(/Plain body could not be parsed for comment/))
        rich_text_comment = events(:serialization_comment_with_line_spaces)
        assert_equal '', rich_text_comment.plain_body
      end
    end
  end

  describe '#temp_plain_body=' do
    let(:comment) { tickets(:minimum_1).comments.first }
    let(:temp_value) { "New plain body" }

    before do
      assert_equal "<div class=\"zd-comment\" dir=\"auto\">minimum <span>ticket 1</span></div>", comment.html_body
      comment.temp_plain_body = temp_value
    end

    it "temporarily sets new plain value" do
      assert_equal "<div class=\"zd-comment\" dir=\"auto\">#{temp_value}</div>", comment.html_body
    end

    it "cannot be saved" do
      assert_raise(ActiveRecord::ReadOnlyRecord) { comment.save! }
    end
  end

  describe "#html_body" do
    it "does not truncate long links by default" do
      @comment.stubs(:value).returns("Test with http://www.example.com/really/really/really/really/really/really/really/really/long link")
      assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Test with <a href=\"http://www.example.com/really/really/really/really/really/really/really/really/long\" target=\"_blank\" rel=\"nofollow noreferrer\">http://www.example.com/really/really/really/really/really/really/really/really/long</a> link</p></div>", @comment.html_body
    end

    it "truncates long links according to account setting" do
      @comment.account.settings.link_display_truncation_length = 20
      @comment.stubs(:value).returns("Test with http://www.example.com/really/really/really/really/really/really/really/really/long link")
      assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Test with <a href=\"http://www.example.com/really/really/really/really/really/really/really/really/long\" target=\"_blank\" rel=\"nofollow noreferrer\">http://www.exampl...</a> link</p></div>", @comment.html_body
    end

    it "correctly escape when presenter is simple format" do
      @comment.stubs(:value).returns("< http://www.test.com <script>alert(\"wowza\")</script> <strong>yowza</strong>\r\n'http://test.com'\r\n")
      simple_format = @comment.html_body(formatter: :simple_format)
      assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">&lt; <a href=\"http://www.test.com\" target=\"_blank\" rel=\"nofollow noreferrer\">http://www.test.com</a> &lt;script&gt;alert(\"wowza\")&lt;/script&gt; &lt;strong&gt;yowza&lt;/strong&gt;\n<br>'<a href=\"http://test.com\" target=\"_blank\" rel=\"nofollow noreferrer\">http://test.com</a>'</p></div>", simple_format
      assert simple_format.html_safe?
    end

    it "does not emojify if not enabled in account" do
      @comment.account.stubs(:has_emoji_autocompletion_enabled?).returns(false)
      @comment.stubs(:value).returns("\"http://www.test.com\" <script>alert(\"wowza\")</script> <strong>yowza</strong>\r\n'http://test.com'\r\n")
      @comment.expects(:emojify!).never
      @comment.html_body
    end

    it "emojifys if enabled in account" do
      @comment.account.stubs(:has_emoji_autocompletion_enabled?).returns(true)
      @comment.stubs(:value).returns("\"http://www.test.com\" <script>alert(\"wowza\")</script> <strong>yowza</strong>\r\n'http://test.com'\r\n")
      @comment.expects(:emojify!).once
      @comment.html_body
    end

    it "strips trailing whitespace on a plain comment" do
      @comment.stubs(:value).returns("Hello   there   \n \u00a0 ")
      assert_equal("<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello   there</p></div>", @comment.html_body)
    end

    it "strips trailing whitespace on a rich comment" do
      @comment.stubs(:value).returns("Hello   there   \n \u00a0 ")
      @comment.stubs(:rich?).returns(true)
      assert_equal("<div class=\"zd-comment\" dir=\"auto\">Hello   there</div>", @comment.html_body)
    end

    it "adds zd-comment-pre-styled so we do not add our styles to pre-styled incoming mail" do
      @comment.stubs(:value).returns("test")
      @comment.stubs(:rich?).returns(true)
      @comment.via_id = ViaType.MAIL
      @comment.html_body.must_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\">test</div>"
    end

    describe "with rich_content_in_emails enabled" do
      before { @comment.account.settings.stubs(rich_content_in_emails?: true) }

      describe "inline images from HTML emails" do
        let(:comment_value) { "<p>blah <img src=\"https://#{@host_name}/attachments/token/xyz/?name=foo.png\"> blah</p>" }

        before do
          @host_name = Zendesk::Routes::UrlGenerator.new(@comment.account.route, @comment.account).host_name_without_port
          @comment.stubs(value: comment_value)
          @comment.stubs(:rich?).returns(true)
          @comment.via_id = ViaType.MAIL
        end

        it "removes the base URL leaving a relative URL" do
          @comment.html_body(relative_attachment_urls: true).must_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><p dir=\"auto\">blah <img src=\"/attachments/token/xyz/?name=foo.png\"> blah</p></div>"
        end

        it "does not remove the base URL" do
          @comment.html_body(relative_attachment_urls: false).must_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><p dir=\"auto\">blah <img src=\"https://#{@host_name}/attachments/token/xyz/?name=foo.png\"> blah</p></div>"
        end

        describe 'with deeply nested comments' do
          let(:comment_value) { nest_html_content('<div>Abc<div/>', Nokogumbo::DEFAULT_MAX_TREE_DEPTH * 10) }

          it 'returns an empty string' do
            assert_equal('', @comment.html_body)
          end
        end
      end
    end

    describe "when account has change its route's host" do
      before do
        @comment.stubs(:rich?).returns(true)
        @comment.stubs(:attachment_tokens_map).returns(['xyz'])
      end

      describe "subdomain change" do
        before do
          @host_name = Zendesk::Routes::UrlGenerator.new(@comment.account.route, @comment.account).host_name_without_port
          @comment.stubs(value: "<p>blah <img src=\"https://minimumold.zendesk-test.com/attachments/token/xyz/?name=foo.png\"> blah</p>")
        end

        describe_with_arturo_enabled :inline_images_rebuild_path_v2 do
          it "rebuilds the attachment url with the new host name" do
            @comment.html_body.must_include @host_name
          end
        end

        describe_with_arturo_disabled :inline_images_rebuild_path_v2 do
          it "does not rebuilds the attachment url with the new host name" do
            @comment.html_body.wont_include @host_name
          end
        end
      end

      describe "host_mapping change" do
        before do
          @comment.account.route.update_attribute(:host_mapping, "what.ever.test")
          @host_name = Zendesk::Routes::UrlGenerator.new(@comment.account.route, @comment.account).host_name_without_port
          @comment.stubs(value: "<p>blah <img src=\"https://olddomain.thatwedont.know/attachments/token/xyz/?name=foo.png\"> blah</p>")
        end

        describe_with_arturo_enabled :inline_images_rebuild_path_v2 do
          it "rebuilds the attachment url with current host" do
            @comment.html_body.must_include @host_name
          end

          it "does not modify non-attachment imgs" do
            @comment.stubs(value: "<p>blah <img src=\"https://something.com/bar/?name=foo.png\"> blah</p>")
            @comment.html_body.wont_include @host_name
          end
        end

        describe_with_arturo_disabled :inline_images_rebuild_path_v2 do
          it "does not rebuild the attachment with current host" do
            @comment.html_body.wont_include @host_name
          end
        end
      end
    end

    describe "autolinking" do
      before do
        @comment.stubs(process_as_markdown?: true)
        @comment.stubs(:value).returns("blah #1 blah\r\n")
      end

      describe "when passing in for_agent as true" do
        it "renders a lotus path" do
          assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">blah <a href=\"/agent/tickets/1\" target=\"_blank\" rel=\"ticket\">#1</a> blah</p></div>", @comment.html_body(for_agent: true)
        end
      end

      describe "when passing in for_agent as false" do
        describe "with help center not enabled" do
          before do
            @comment.account.stubs(help_center_enabled?: false)
          end

          it "does not render a requests path" do
            assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">blah #1 blah</p></div>", @comment.html_body(for_agent: false)
          end
        end

        describe "and the account is help center enabled" do
          before { @comment.account.stubs(help_center_enabled?: true) }

          it "renders a help center request path" do
            assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">blah <a href=\"/hc/requests/1\" target=\"_blank\" rel=\"ticket\">#1</a> blah</p></div>", @comment.html_body(for_agent: false)
          end
        end

        describe "and the account has help center disabled" do
          before do
            @comment.account.stubs(help_center_enabled?: false)
          end

          it "does not auto link ticket ids" do
            assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">blah #1 blah</p></div>", @comment.html_body
          end
        end
      end

      describe "when not passing in for_agent" do
        describe "and help center not enabled" do
          before do
            @comment.account.stubs(help_center_enabled?: false)
          end

          it "does not render a requests path" do
            assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">blah #1 blah</p></div>", @comment.html_body
          end
        end

        describe "and the account is help center enabled" do
          before { @comment.account.stubs(help_center_enabled?: true) }

          it "renders a help center request path" do
            assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">blah <a href=\"/hc/requests/1\" target=\"_blank\" rel=\"ticket\">#1</a> blah</p></div>", @comment.html_body
          end
        end
      end
    end

    describe "block element direction" do
      before do
        @comment.stubs(process_as_markdown?: true)

        @headers = {
          p:  'Test paragraph',
          h1: '# Heading 1',
          h2: '## Heading 2',
          h3: '### Heading 3',
          h4: '#### Heading 4',
          h5: '##### Heading 5',
          h6: '###### Heading 6'
        }
      end

      describe "rtl auto directionality" do
        it "adds dir=\"auto\" to headers" do
          @headers.each do |element, text|
            @comment.body = text
            display_text = text.tr('#', '').strip
            assert_equal "<div class=\"zd-comment\" dir=\"auto\"><#{element} dir=\"auto\">#{display_text}</#{element}></div>", @comment.html_body
          end
        end

        it "adds dir=\"auto\" to ol elements" do
          @comment.body = "1. List item"
          assert_equal "<div class=\"zd-comment\" dir=\"auto\"><ol type=\"1\" dir=\"auto\">\n<li>List item</li>\n</ol></div>", @comment.html_body
        end

        it "adds dir=\"auto\" to ul elements" do
          @comment.body = "* List item"
          assert_equal "<div class=\"zd-comment\" dir=\"auto\"><ul type=\"disc\" dir=\"auto\">\n<li type=\"disc\">List item</li>\n</ul></div>", @comment.html_body
        end
      end
    end

    describe "with markdown via mobile" do
      before do
        @comment.account.settings.stubs(markdown_ticket_comments?: true)
        @comment.author.stubs(is_agent?: true)
        @comment.via_id = ViaType.MOBILE
      end

      it "correctly renders links as href" do
        @comment.stubs(:value).returns("This is a comment. [Click me](http://www.zendesk.com)")

        assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">This is a comment. <a href=\"http://www.zendesk.com\" rel=\"noreferrer\">Click me</a></p></div>", @comment.html_body
      end

      it "correctly renders emphasis" do
        @comment.stubs(:value).returns("Hello *there*")

        assert_equal "<div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello <em>there</em></p></div>", @comment.html_body
      end
    end
  end

  describe "#html_body with css" do
    let(:css) { ZendeskText::MarkdownStyle::DEFAULT_CSS }

    before do
      @comment.stubs(process_as_markdown?: true)
    end

    describe "on a rich comment" do
      before do
        @comment.stubs(:value).returns("<p>hello</p>")
        @comment.stubs(:rich?).returns(true)
      end

      describe_with_arturo_disabled :email_simplified_threading do
        it "inlines the css styles" do
          assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\"><p style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\" dir=\"auto\">hello</p></div>", @comment.html_body(css: css)
        end
      end

      describe_with_arturo_enabled :email_simplified_threading do
        let(:css) { ZendeskText::MarkdownStyle::DEFAULT_CSS_V2 }

        it "inlines the css styles" do
          assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\"><p style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\" dir=\"auto\">hello</p></div>", @comment.html_body(css: css)
        end
      end
    end

    describe "on a markdown comment" do
      before do
        @comment.stubs(:value).returns("hello")
        @comment.stubs(:rich?).returns(false)
      end

      describe_with_arturo_disabled :email_simplified_threading do
        it "inlines the css styles" do
          assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\"><p dir=\"auto\" style=\"color: #2b2e2f; font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif; font-size: 14px; line-height: 22px; margin: 15px 0\">hello</p></div>", @comment.html_body(css: css)
        end
      end

      describe_with_arturo_enabled :email_simplified_threading do
        let(:css) { ZendeskText::MarkdownStyle::DEFAULT_CSS_V2 }

        it "inlines the css styles" do
          assert_equal "<div class=\"zd-comment\" dir=\"auto\" style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\"><p dir=\"auto\" style=\"color: #2b2e2f; line-height: 22px; margin: 15px 0\">hello</p></div>", @comment.html_body(css: css)
        end
      end
    end

    describe "on a pre-styled comment" do
      let(:css) { ZendeskText::MarkdownStyle::PRESTYLED_CSS }

      before do
        @comment.stubs(:value).returns("Hello")
        @comment.stubs(pre_styled?: true)
        @comment.account.settings.stubs(rich_content_in_emails?: true)
      end

      describe_with_arturo_disabled :email_simplified_threading do
        it "includes zd-comment-pre-styled class and pre-styled CSS inline" do
          assert_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><p dir=\"auto\" style=\"font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Tahoma', Verdana, sans-serif\">Hello</p></div>", @comment.html_body(css: css)
        end
      end

      describe_with_arturo_enabled :email_simplified_threading do
        let(:css) { nil }

        it "includes zd-comment-pre-styled class and no pre-styled CSS inline" do
          assert_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><p dir=\"auto\">Hello</p></div>", @comment.html_body(css: css)
        end
      end
    end
  end

  describe "#temp_html_body=" do
    let(:comment) { tickets(:minimum_1).comments.first }
    let(:temp_value) { "<div>New HTML body</div>" }

    before do
      assert_equal "<div class=\"zd-comment\" dir=\"auto\">minimum <span>ticket 1</span></div>", comment.html_body
      comment.temp_html_body = temp_value
    end

    it "temporarily sets new html value" do
      assert_equal "<div class=\"zd-comment\" dir=\"auto\">#{temp_value}</div>", comment.html_body
    end

    it "cannot be saved" do
      assert_raise(ActiveRecord::ReadOnlyRecord) { comment.save! }
    end
  end

  describe "#to_liquid" do
    it "returns a Hash" do
      assert_kind_of Hash, @comment.to_liquid
    end

    it "returns basic comment properties" do
      @comment = events(:create_comment_for_minimum_ticket_1)
      result = @comment.to_liquid

      assert_equal @comment.id, result['id']
      assert_equal @comment.body, result['value']
      assert_equal @comment.is_public, result['is_public']
    end

    describe "liquid_comment_original_recipients Arturo is enabled" do
      before do
        Arturo.enable_feature!(:liquid_comment_original_recipients)
      end

      describe "Comment audit has recipients" do
        it "returns array of recipients" do
          @comment.audit = Audit.new
          @comment.audit.recipients = 'test@example.com test2@example.com'
          assert_equal ['test@example.com', 'test2@example.com'], @comment.to_liquid['original_recipients']
        end
      end

      describe "Comment audit does not have recipients" do
        it "does not return recipients" do
          @comment.audit = Audit.new
          @comment.audit.recipients = nil
          refute @comment.to_liquid.key?('original_recipients')
        end
      end

      describe "Comment does not have an audit" do
        it "does not return recipients" do
          @comment.audit = nil
          refute @comment.to_liquid.key?('original_recipients')
        end
      end
    end

    describe "liquid_comment_original_recipients Arturo is disabled" do
      before do
        Arturo.disable_feature!(:liquid_comment_original_recipients)
      end

      describe "Comment audit has recipients" do
        it "does not return recipients" do
          @comment.audit = Audit.new
          @comment.audit.recipients = 'test@example.com test2@example.com'
          refute @comment.to_liquid.key?('original_recipients')
        end
      end
    end

    describe "when value is HTML" do
      before do
        @comment.html_body = 'minimum ticket 1<span color="red">RED<span>'
      end

      it 'returns :value as plain text' do
        assert_equal 'minimum ticket 1RED', @comment.to_liquid['value']
      end
    end
  end

  describe "#intermediate_value" do
    describe "when not set" do
      it "returns the body" do
        @comment.body = "booody"
        assert_equal @comment.body, @comment.intermediate_value
      end
    end

    describe "when set" do
      it "returns intermediate value" do
        original_body = @comment.body.to_s
        @comment.set_liquidized_body('new test body')
        assert_equal original_body, @comment.intermediate_value
        assert_equal 'new test body', @comment.body
      end
    end
  end

  describe "#set_liquidized_body" do
    it "sets body and saves previous value" do
      # Note: this test is essentially identical to a test of #intermediate_value, but we're duplicating the logic
      # for clarity and to assure that we cover this function even if the other is deleted.
      original_body = @comment.body.to_s
      @comment.set_liquidized_body('new test body')
      assert_equal original_body, @comment.intermediate_value
      assert_equal 'new test body', @comment.body
    end
  end

  describe "#latest_by_created_at_for_tickets" do
    fixtures :tickets, :events
    it "returns the latest comment for each ticket" do
      comments = Comment.send(:latest_by_created_at_for_tickets, Ticket.all.map(&:id)).values
      assert_equal comments.map(&:id).sort, comments.map(&:id).sort.uniq

      comments.each do |comment|
        # Order by same order as Comment#latest_by_created_at_for_tickets
        assert_equal comment, Comment.where(ticket_id: comment.ticket_id).order("ticket_id DESC, created_at DESC, id DESC").first
      end
    end
  end

  describe "#channel_back_requested?" do
    it "returns true or false if channel_back is set to true or false as a String" do
      @comment = Comment.new(channel_back: "true")
      assert @comment.channel_back_requested?

      @comment.channel_back = "false"
      refute @comment.channel_back_requested?
    end

    it "returns true if channel_back is set to 1 or 0 as an Integer" do
      @comment = Comment.new(channel_back: 1)
      assert @comment.channel_back_requested?

      @comment.channel_back = 0
      refute @comment.channel_back_requested?
    end

    it "returns true if channel_back is set to 'dm'" do
      comment = Comment.new
      comment.channel_back = 'dm'
      assert comment.channel_back_requested?
    end

    it "returns false if channel_back is set to false" do
      comment = Comment.new
      comment.channel_back = 'false'
      refute comment.channel_back_requested?
      comment.channel_back = false
      refute comment.channel_back_requested?
    end
  end

  describe "#supports_channel_back?" do
    before do
      @comment = events(:create_comment_for_minimum_ticket_1)
    end

    [
      ViaType.TWITTER,
      ViaType.TWITTER_DM,
      ViaType.TWITTER_FAVORITE,
      ViaType.FACEBOOK_POST,
      ViaType.FACEBOOK_MESSAGE,
      ViaType.ANY_CHANNEL
    ].each do |via_id|
      describe "when the ticket via_id is #{via_id} and the comment is not a conversion and this is a public reply" do
        before do
          @comment.ticket.update_column(:via_id, via_id)
          @comment.via_id = ViaType.WEB_FORM
          @comment.is_public = 'true'
          @comment.author = users(:minimum_agent)

          assert @comment.ticket_via_channel?
          refute @comment.comment_via_channel?
          assert @comment.public_reply?
        end

        it "returns true" do
          assert @comment.supports_channel_back?
        end

        describe "when the ticket is not via channel" do
          before do
            @comment.ticket.update_column(:via_id, ViaType.WEB_FORM)
            @comment.reload
            refute @comment.ticket_via_channel?
          end

          it "returns false" do
            refute @comment.supports_channel_back?
          end
        end

        describe "when comment is from a conversion" do
          before do
            @comment.via_id = via_id
            assert @comment.comment_via_channel?
          end

          it "returns false" do
            refute @comment.supports_channel_back?
          end
        end

        describe "when this is not a public reply" do
          before do
            @comment.is_public = false
            refute @comment.public_reply?
          end

          it "returns false" do
            refute @comment.supports_channel_back?
          end
        end

        describe "when this is a requester reply" do
          before do
            @comment.author = users(:minimum_end_user)
            refute @comment.public_reply?
          end

          it "returns false" do
            refute @comment.supports_channel_back?
          end
        end

        describe 'when the latest comment has allow_channelback flag' do
          [false, true, 0, nil, 'false', ''].each do |value|
            describe "when the allow_channelback flag value is #{value}" do
              # Only disable channelback if value is boolean false
              expected_value = value != false

              before do
                event_decoration = event_decorations(:minimum)
                decoration_data = event_decoration.data
                decoration_data.any_channel = { allow_channelback: value }
                event_decoration.data = decoration_data
                @comment.event_decoration = event_decoration
                @comment.ticket.expects(:latest_public_comment).returns(@comment)
              end

              it "return #{expected_value}" do
                assert_equal expected_value, @comment.supports_channel_back?
              end
            end
          end
        end
      end
    end
  end

  describe "#should_create_channel_back_event?" do
    [
      { supports_channel_back: true, channel_back_requested: true, expected: true },
      { supports_channel_back: true, channel_back_requested: false, expected: false },
      { supports_channel_back: false, channel_back_requested: true, expected: false },
      { supports_channel_back: false, channel_back_requested: false, expected: false }
    ].each do |test_case|
      describe "when channel back is #{test_case[:supports_channel_back] ? 'supported' : 'not supported'} and #{test_case[:channel_back_requested] ? 'is' : 'is not'} requested" do
        before do
          @comment = events(:create_comment_for_minimum_ticket_1)
          @comment.stubs(:supports_channel_back?).returns(test_case[:supports_channel_back])
          @comment.channel_back = test_case[:channel_back_requested]
        end

        it "returns the right value" do
          assert_equal test_case[:expected], @comment.should_create_channel_back_event?
        end
      end
    end
  end

  describe "#ticket_via_twitter?" do
    [true, false].each do |mention|
      [true, false].each do |dm|
        describe "ticket_via_tweet? returns #{mention}, ticket_via_twitter_dm? returns #{dm}" do
          before do
            @comment = events(:create_comment_for_minimum_ticket_1)
            @comment.expects(:ticket_via_tweet?).at_most_once.returns(mention)
            @comment.expects(:ticket_via_twitter_dm?).at_most_once.returns(dm)
          end

          it "returns the OR of the two values" do
            assert_equal (mention || dm), @comment.ticket_via_twitter?
          end
        end
      end
    end
  end

  describe "#ticket_via_tweet?" do
    describe "supported via_id" do
      [ViaType.TWITTER, ViaType.TWITTER_FAVORITE].each do |via_id|
        describe "via_id is #{via_id}" do
          before do
            @comment = events(:create_comment_for_minimum_ticket_1)
            @comment.audit.ticket.via_id = via_id
          end

          it "returns true" do
            assert @comment.ticket_via_tweet?
          end
        end
      end
    end

    describe "unsupported via_id" do
      before do
        @comment = events(:create_comment_for_minimum_ticket_1)
        @comment.audit.ticket.via_id = ViaType.WEB_FORM
      end

      [123, nil].each do |channel_source_id|
        describe "channel_source_id #{channel_source_id}" do
          before do
            @comment.channel_source_id = channel_source_id
          end

          it "returns false" do
            refute @comment.ticket_via_tweet?
          end
        end
      end
    end

    describe "no audit" do
      it "returns false" do
        refute @comment.ticket_via_tweet?
      end
    end
  end

  describe "#ticket_via_twitter_dm?" do
    before do
      @comment = events(:create_comment_for_minimum_ticket_1)
    end

    describe "via twitter_dm" do
      before do
        @comment.audit.ticket.via_id = ViaType.TWITTER_DM
      end

      it "returns true" do
        assert @comment.ticket_via_twitter_dm?
      end
    end

    describe "via something other than twitter_dm" do
      before do
        @comment.audit.ticket.via_id = ViaType.WEB_FORM
      end

      it "returns false" do
        refute @comment.ticket_via_twitter_dm?
      end
    end

    describe "no audit" do
      it "returns false" do
        refute @comment.ticket_via_twitter_dm?
      end
    end
  end

  describe "#ticket_via_facebook?" do
    before do
      @comment = events(:create_comment_for_minimum_ticket_1)
    end

    describe "when the ticket is via facebook message" do
      before do
        @comment.audit.ticket.via_id = ViaType.FACEBOOK_MESSAGE
      end

      it "returns true" do
        assert @comment.ticket_via_facebook?
      end
    end

    describe "when the ticket is via facebook post" do
      before do
        @comment.audit.ticket.via_id = ViaType.FACEBOOK_POST
      end

      it "returns true" do
        assert @comment.ticket_via_facebook?
      end
    end

    describe "when the ticket is not via facebook" do
      before do
        @comment.audit.ticket.via_id = ViaType.WEB_FORM
      end

      it "returns false" do
        refute @comment.ticket_via_facebook?
      end
    end

    describe "no audit" do
      it "returns false" do
        refute @comment.ticket_via_facebook?
      end
    end
  end

  describe "#ticket_via_any_channel?" do
    before do
      @comment = events(:create_comment_for_minimum_ticket_1)
    end

    describe "when the ticket is via any_channel" do
      before do
        @comment.audit.ticket.via_id = ViaType.ANY_CHANNEL
      end

      it "returns true" do
        assert @comment.ticket_via_any_channel?
      end
    end

    describe "when the ticket is not via any_channel" do
      before do
        @comment.audit.ticket.via_id = ViaType.WEB_FORM
      end

      it "returns false" do
        refute @comment.ticket_via_any_channel?
      end
    end

    describe "no audit" do
      it "returns false" do
        refute @comment.ticket_via_any_channel?
      end
    end
  end

  describe "#validate_public_reply" do
    # We are gathering data before fixing the string (hence anychannel default back to twitter)
    string_map = {
      'twitter' => 'twitter',
      'facebook message' => 'facebook',
      'any channel' => 'twitter'
    }

    {
      ViaType.TWITTER => 'twitter',
      ViaType.FACEBOOK_MESSAGE => 'facebook message',
      ViaType.ANY_CHANNEL => 'any channel'
    }.each do |via_id, channel|
      describe "when attempting invalid reply on #{channel}" do
        before do
          @comment.is_public = true
          @comment.channel_back = false
          @comment.audit = Audit.new
          @comment.ticket = @comment.audit.ticket = tickets(:minimum_1)
          @comment.ticket.requester.identities.email.delete_all
          @comment.audit.ticket.via_id = via_id
          @comment.author = users(:minimum_agent)
          @comment.body = 'reply'
          assert @comment.supports_channel_back?
        end

        it "adds an error message" do
          refute @comment.valid?

          assert_equal [I18n.t("api.errors.#{string_map[channel]}.invalid_reply")], @comment.errors[:base]
        end

        it "adds log message" do
          expected_error_message = "Attempt to make a non-channelback non-email public reply for ticket #{@comment.ticket.id}" \
                                  " in account #{@comment.account.id} for channel #{channel}"
          Rails.logger.expects(:warn).with(expected_error_message)
          refute @comment.valid?
        end

        it "increments the invalid_public_reply statsd metric" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(:invalid_public_reply, tags: ["channel:#{channel}"])

          refute @comment.valid?
        end

        describe "when channel back not supported" do
          before do
            @comment.audit.ticket.via_id = ViaType.WEB_FORM
            refute @comment.supports_channel_back?
          end

          it "is valid" do
            assert @comment.valid?
          end
        end

        describe "when channel back was requested" do
          before do
            @comment.channel_back = true
            assert @comment.channel_back_requested?
          end

          it "is valid" do
            assert @comment.valid?
          end
        end

        describe "when email only is allowed" do
          before do
            @comment.ticket = tickets(:minimum_2)
            assert @comment.ticket.requester.identities.email.exists?
          end

          it "is valid" do
            assert @comment.valid?
          end
        end
      end
    end
  end

  describe "#public_reply?" do
    before { @comment.body = 'comment' }

    describe "when the comment is not public" do
      before { @comment.is_public = false }

      it "returns false" do
        refute @comment.public_reply?
      end
    end

    describe "when the comment is public" do
      before { @comment.is_public = true }

      describe "and it is a reply" do
        before { @comment.author.stubs(:is_agent?).returns(true) }

        it "returns true" do
          assert @comment.public_reply?
        end
      end

      describe "and it is not a reply" do
        before { @comment.author.stubs(:is_agent?).returns(false) }

        it "returns false" do
          refute @comment.public_reply?
        end
      end
    end
  end

  describe "#reply?" do
    [true, false].each do |agent_as_end_user_arturo|
      describe ":agent_as_end_user arturo is #{agent_as_end_user_arturo}" do
        before do
          Arturo.set_feature!(:agent_as_end_user, agent_as_end_user_arturo)
        end

        describe "when the comment does not have a value or attachment" do
          before do
            @comment.body = nil
            @comment.attachments = []
          end

          it "returns false" do
            refute @comment.reply?
          end
        end

        describe "when the comment has a value" do
          before { @comment.body = 'comment' }

          describe "and the author is not an agent or foreign agent" do
            before do
              @comment.author.stubs(:is_agent?).returns(false)
              @comment.author.stubs(:foreign_agent?).returns(false)
            end

            it "returns false" do
              refute @comment.reply?
            end
          end

          describe "and the author is an agent" do
            before { @comment.author.stubs(:is_agent?).returns(true) }

            it "returns true" do
              assert @comment.reply?
            end
          end

          describe "and the author is a foreign_agent" do
            before { @comment.author.stubs(:foreign_agent?).returns(true) }

            it "returns true" do
              assert @comment.reply?
            end
          end
        end

        describe "when the comment has an attachment" do
          let(:attachment) do
            create_attachment("#{Rails.root}/test/files/normal_1.jpg")
          end

          before { @comment.attachments = [attachment] }

          describe "and the author is an agent" do
            before { @comment.author.stubs(:is_agent?).returns(true) }

            it "returns true" do
              assert @comment.reply?
            end
          end

          describe "and the author is a foreign_agent" do
            before { @comment.author.stubs(:foreign_agent?).returns(true) }

            it "returns true" do
              assert @comment.reply?
            end
          end
        end

        describe "when the author is submitter and agent-as-end-user" do
          before do
            @comment.author.roles = Role::AGENT.id
            @comment.body = 'comment'
            @comment.ticket = tickets(:minimum_1)
            @comment.ticket.via_id = ViaType.WEB_SERVICE
            @comment.ticket.via_reference_id = ViaType.HELPCENTER
            @comment.ticket.submitter_id = @comment.author.id
          end

          it "returns #{!agent_as_end_user_arturo}" do
            assert_equal !agent_as_end_user_arturo, @comment.reply?
          end
        end
      end
    end
  end

  describe "#from_end_user?" do
    before do
      @comment.ticket = tickets(:minimum_1)
    end

    [true, false].each do |arturo_enabled|
      describe "agent_as_end_user Arturo is #{arturo_enabled ? 'en' : 'dis'}abled" do
        before do
          Arturo.set_feature!(:agent_as_end_user, arturo_enabled)
        end

        describe "when the comment is public" do
          before { @comment.is_public = true }

          describe "and the author is a foreign agent" do
            before do
              @comment.author.roles         = Role::END_USER.id
              @comment.author.foreign_agent = true
            end

            it "returns false" do
              refute @comment.from_end_user?
            end
          end

          describe "and the author is an end user" do
            before do
              @comment.author.roles         = Role::END_USER.id
              @comment.author.foreign_agent = false
            end

            it "returns true" do
              assert @comment.from_end_user?
            end
          end

          describe "and the comment is via an inbound phone call" do
            before do
              @comment.author.roles = Role::AGENT.id
              @comment.via_id       = ViaType.PHONE_CALL_INBOUND
            end

            it "returns true" do
              assert @comment.from_end_user?
            end
          end
        end
      end

      describe "when the comment is not public" do
        before { @comment.is_public = false }

        it "returns false" do
          refute @comment.from_end_user?
        end
      end
    end

    describe 'agent_as_end_user Arturo is enabled' do
      let(:agent_as_end_user) { true }

      before do
        Arturo.enable_feature!(:agent_as_end_user)
        @comment.is_public = true
        @comment.author.roles = Role::AGENT.id
        Ticket.any_instance.expects(:agent_as_end_user_for?).returns(agent_as_end_user)
      end

      describe 'ticket#agent_as_end_user_for? returns true' do
        it 'returns true' do
          assert @comment.from_end_user?
        end
      end

      describe 'ticket#agent_as_end_user_for? returns false' do
        let(:agent_as_end_user) { false }

        it 'returns false' do
          refute @comment.from_end_user?
        end
      end
    end

    describe 'agent_as_end_user Arturo is disabled' do
      before do
        Arturo.disable_feature!(:agent_as_end_user)
        @comment.is_public = true
        @comment.author.roles = Role::AGENT.id
      end

      it 'does not call Ticket#agent_as_end_user_for' do
        Ticket.any_instance.expects(:agent_as_end_user_for?).never
        @comment.from_end_user?
      end
    end
  end

  describe "#is_private?" do
    before { @comment.body = 'comment' }

    describe "when the comment is not public" do
      before { @comment.is_public = false }

      it "returns true" do
        assert @comment.is_private?
      end
    end

    describe "when the comment is public" do
      before { @comment.is_public = true }

      it "returns false" do
        refute @comment.is_private?
      end
    end
  end

  describe "#phone_call_inbound?" do
    describe "when the comment is via an inbound phone call" do
      before { @comment.via_id = ViaType.PHONE_CALL_INBOUND }

      it "returns true" do
        assert @comment.phone_call_inbound?
      end
    end

    describe "when the comment is not via an inbound phone call" do
      before { @comment.via_id = ViaType.WEB_FORM }

      it "returns false" do
        refute @comment.phone_call_inbound?
      end
    end
  end

  describe "#phone_call_outbound?" do
    describe "when the comment is via an outbound phone call" do
      before { @comment.via_id = ViaType.PHONE_CALL_OUTBOUND }

      it "returns true" do
        assert @comment.phone_call_outbound?
      end
    end

    describe "when the comment is not via an outbound phone call" do
      before { @comment.via_id = ViaType.WEB_FORM }

      it "returns false" do
        refute @comment.phone_call_outbound?
      end
    end
  end

  describe "#create_post_on_corresponding_entry" do
    def run_after_create_callbacks
      @comment.send(:run_callbacks, :create)
    end

    describe "that is public and belongs to a Ticket which belongs to an Entry" do
      before do
        @comment.is_public = true
        @comment.build_ticket
        @comment.ticket.build_entry
        @comment.audit = Audit.new
      end

      it "creates a Post on the Entry with a matching author" do
        expected_user = users(:minimum_author)

        @comment.ticket.entry.posts.expects(:create!).with(has_entry(user: expected_user))

        run_after_create_callbacks
      end

      it "creates a Post on the Entry with a matching body" do
        expected_body = "That is fantastic"
        @comment.body = expected_body

        @comment.ticket.entry.posts.expects(:create!).with(has_entry(body: expected_body.present))

        run_after_create_callbacks
      end

      it "creates a Post on the Entry with matching attachments" do
        attachments = [
          attachments(:attachment_entry),
          attachments(:attachment_entry_thumb)
        ]

        @comment.attachments = attachments
        @comment.ticket.entry.posts.expects(:create!)

        RestUpload.expects(:new).with(anything, 'a.png', 'image/png').once
        Attachment.expects(:create!).once

        run_after_create_callbacks
      end
    end

    describe "that is not public and belongs to a Ticket which belongs to an Entry" do
      before do
        @comment.is_public = false
        @comment.build_ticket
        @comment.ticket.build_entry
        @comment.audit = Audit.new
      end

      it "does not attempt to create a Post" do
        @comment.ticket.entry.expects(:posts).never
        run_after_create_callbacks
      end
    end

    describe "that belongs to a Ticket which does not belong to an Entry" do
      before do
        @comment.build_ticket
        @comment.audit = Audit.new
      end

      it "does not attempt to create a Post" do
        @comment.ticket.build_entry
        @comment.ticket.entry.expects(:posts).never
        run_after_create_callbacks
      end
    end
  end

  describe "#process_as_markdown?" do
    before { @comment.account.settings.stubs(markdown_ticket_comments?: true) }

    [ViaType.WEB_FORM, ViaType.WEB_SERVICE, ViaType.LINKED_PROBLEM, ViaType.CLOSED_TICKET, ViaType.TICKET_SHARING, ViaType.TOPIC, ViaType.RULE, ViaType.MERGE, ViaType.HELPCENTER, ViaType.MOBILE].each do |via_id|
      describe "when via_id is #{via_id}" do
        before { @comment.via_id = via_id }

        describe "when author not present" do
          before { @comment.author = nil }

          it "returns false" do
            refute @comment.process_as_markdown?
          end
        end

        describe "when author is an agent" do
          before { @comment.author.stubs(is_agent?: true) }

          it "returns true" do
            assert @comment.process_as_markdown?
          end
        end

        describe "when author is a foreign agent" do
          before { @comment.author.stubs(is_agent?: false, foreign_agent?: true) }

          it "returns true" do
            assert @comment.process_as_markdown?
          end
        end

        describe "when the account has rich text enabled" do
          before do
            @comment.account.settings.stubs(rich_text_comments?: true, markdown_ticket_comments?: false)
            @comment.author.stubs(is_agent?: true)
          end

          it "returns false for rich comments" do
            @comment.stubs(format: "rich")
            assert !@comment.process_as_markdown?
          end

          it "returns true for non-rich comments" do
            @comment.stubs(format: "")
            assert @comment.process_as_markdown?
          end
        end
      end
    end

    describe "when the via is not valid" do
      before do
        @comment.stubs(via?: false)
      end

      it "returns false" do
        refute @comment.process_as_markdown?
      end
    end
  end

  describe "#not_in_twitter_maintenance_mode" do
    before do
      @comment.stubs(ticket_via_twitter?: true)
    end

    describe "channel_back" do
      before do
        @comment.stubs(channel_back_requested?: true)
      end

      it "is valid" do
        assert @comment.valid?(:create)
      end
    end

    describe "twitter_maintenance enabled" do
      before do
        @comment.stubs(channel_back_requested?: false)
        Arturo.enable_feature!(:twitter_maintenance)
      end

      it "is valid" do
        assert @comment.valid?(:create)
      end
    end

    describe "channel_back and twitter_maintenance enabled" do
      before do
        @comment.stubs(channel_back_requested?: true)
        Arturo.enable_feature!(:twitter_maintenance)
      end

      it "does not be valid" do
        refute @comment.valid?(:create)
        assert_equal ["Twitter integration is in maintenance mode at this time.  Please try again later."], @comment.errors[:base]
      end
    end
  end

  describe "#strip_invalid_images" do
    def add_comment(comment)
      ticket = tickets(:minimum_1)
      comment = ticket.add_comment(
        html_body: comment,
        author: ticket.requester,
        via_id: 1,
        via_reference_id: 1
      )
      ticket.will_be_saved_by(users(:minimum_agent))
      comment.save!
      comment.send(:value).gsub!(%r{/token/.+?/}, '/token/TOKEN/')
      comment
    end

    it "does not remove normal" do
      account = tickets(:minimum_1).account
      content = %(HELLO <img src="#{account.url}/attachments/token/xxxx.jpg"> THERE)
      comment = add_comment(content)
      comment.html_body(base_url: account.url).must_equal "<div class=\"zd-comment\" dir=\"auto\">#{content}</div>"
    end

    it "removes base64 since this should be handled on client side" do
      comment = add_comment('HELLO <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEAAAAALAAAAAABAAEAAAI="> THERE')
      comment.html_body.must_equal "<div class=\"zd-comment\" dir=\"auto\">HELLO  THERE</div>"
    end

    it "ignores other attributes" do
      comment = add_comment('HELLO <img width="1" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEAAAAALAAAAAABAAEAAAI=" heigth="1"> THERE')
      comment.html_body.must_equal "<div class=\"zd-comment\" dir=\"auto\">HELLO  THERE</div>"
    end

    it "removes webkit-fake-url attachments" do
      comment = add_comment('HELLO <img src="webkit-fake-url://e303f4ea-3e1d-4c51-a83d-52707aeeb397/image.tiff"> THERE')
      comment.html_body.must_equal "<div class=\"zd-comment\" dir=\"auto\">HELLO  THERE</div>"
      comment.attachments.size.must_equal 0
    end
  end

  describe "#screencasts" do
    before do
      @ticket = tickets(:minimum_1)
      @comment = @ticket.comments.first

      screencasts = [
        {position: 1, url: 'http://test.com/abc123'},
        {position: 2, url: 'http://test.com/abc567'}
      ]

      @comment.audit.metadata[:system][:screencasts] = screencasts
    end

    it "returns screencasts using Comment#screencasts" do
      assert_equal(2, @comment.screencasts.size)
      assert_equal(2, @comment.screencasts.last.position)
      assert_equal('http://test.com/abc567', @comment.screencasts.last.url)
    end
  end

  describe "#author_permission_restricted" do
    it 'returns true if author is a light agent' do
      @comment.author.expects(:is_light_agent?).returns(true)

      assert(@comment.send(:author_permission_restricted?))
    end

    it 'returns true if author does not have public comment permissions' do
      @comment.author.expects(:can?).with(:publicly, Comment).returns(false)

      assert(@comment.send(:author_permission_restricted?))
    end

    it 'returns false if author is nil' do
      @comment.stubs(:author).returns(nil)

      assert_equal false, @comment.send(:author_permission_restricted?)
    end

    it 'returns false if author has public comment permission' do
      @comment.author.expects(:can?).with(:publicly, Comment).returns(true)

      assert_equal false, @comment.send(:author_permission_restricted?)
    end
  end

  describe "#signature_added?" do
    let (:comment) { tickets(:minimum_1).comments.first }

    describe "when the signature should not be added" do
      before { comment.stubs(:should_add_signature?).returns(false) }

      it "returns false" do
        refute comment.signature_added?
      end
    end

    describe "when the signature should be added" do
      before { comment.stubs(:should_add_signature?).returns(true) }

      describe "when the comment author doesn't have a signature" do
        before { comment.author.stubs(:signature).returns(nil) }

        it "returns false" do
          refute comment.signature_added?
        end
      end

      describe "when the comment author has a blank signature" do
        before { comment.author.set_signature("", rich: false) }

        it "returns false" do
          refute comment.signature_added?
        end
      end

      describe "when the comment author has a signature" do
        before { comment.author.set_signature("some signature", rich: false) }

        it "returns true" do
          assert comment.signature_added?
        end
      end
    end
  end

  describe "#private_ticket_restricted?" do
    before do
      @private_ticket = tickets(:minimum_1)
      @private_ticket.stubs(:is_public).returns(false)
      @comment.stubs(:ticket).returns(@private_ticket)
      @comment.is_public = nil
    end

    it 'returns true if ticket is private, the update is made via email or email/api, and comments is_public attribute is nil' do
      @comment.stubs(:via_id).returns(ViaType.MAIL)
      assert(@comment.send(:private_ticket_restricted?))
    end

    it 'returns false if ticket is private and update is not made via email/api' do
      @comment.expects(:via_id).returns(ViaType.WEB_FORM)
      assert_equal false, @comment.send(:private_ticket_restricted?)
    end

    it 'returns false if ticket is public and update is made via email/api' do
      @private_ticket.expects(:is_public).returns(true)
      assert_equal false, @comment.send(:private_ticket_restricted?)
    end

    it 'returns false if is_public attribute is specifically set on the comment' do
      @comment.is_public = true
      assert_equal false, @comment.send(:private_ticket_restricted?)
    end
  end

  describe "#private_merge_ticket?" do
    before do
      @private_ticket = tickets(:minimum_1)
      @private_ticket.stubs(:is_public).returns(false)
      @comment.stubs(:ticket).returns(@private_ticket)
      @comment.stubs(:audit).returns(Audit.new)
      @comment.is_public = nil
    end

    it 'returns true if ticket is private and this comment is made by a merge' do
      @comment.audit.stubs(:via_id).returns(ViaType.MERGE)
      assert(@comment.send(:private_merge_ticket?))
    end

    it 'returns false if ticket is private and this comment is not made by a merge' do
      @comment.audit.expects(:via_id).returns(ViaType.WEB_FORM)
      assert_equal false, @comment.send(:private_merge_ticket?)
    end

    it 'returns false if ticket is public and this comment is made by a merge' do
      @private_ticket.expects(:is_public).returns(true)
      @comment.audit.stubs(:via_id).returns(ViaType.MERGE)
      assert_equal false, @comment.send(:private_merge_ticket?)
    end

    it 'returns false if no audit is present' do
      @comment.stubs(:audit).returns(nil)
      assert_equal false, @comment.send(:private_merge_ticket?)
    end
  end

  describe "#legacy_markdown?" do
    before do
      Arturo.disable_feature! :legacy_markdown_sunset
      @comment.account.settings.enable(:rich_text_comments)
      @comment.account.settings.disable(:markdown_ticket_comments)
    end

    it 'Flag disabled with rich text' do
      assert @comment.legacy_markdown?
    end

    it 'Flag enabled with rich text' do
      Arturo.enable_feature! :legacy_markdown_sunset
      refute @comment.legacy_markdown?
    end

    it 'Flag enabled with markdown' do
      Arturo.enable_feature! :legacy_markdown_sunset
      @comment.account.settings.disable(:rich_text_comments)
      @comment.account.settings.enable(:markdown_ticket_comments)
      refute @comment.legacy_markdown?
    end
  end

  describe "#attachment_tokens_map" do
    describe "with comment attachment" do
      before do
        @attachment = attachments(:inline)
        @comment.attachments << @attachment
      end

      it "returns the tokens map" do
        assert_equal [@attachment.token], @comment.send(:attachment_tokens_map)
      end
    end

    describe "with upload token attachement" do
      let(:upload_token) { UploadToken.create(source: @comment.account, target: @comment.author) }
      let(:body) { File.read("#{Rails.root}/test/files/small.png") }

      it "returns attachment joined from upload token sources" do
        attachment = Attachment.create!(
          uploaded_data: RestUpload.new(body, 'small.png', 'image/png'),
          account: @comment.account,
          is_public: true,
          source: upload_token
        ) do |a|
          a.inline = true
        end

        assert @comment.send(:attachment_tokens_map).include? attachment.token
      end
    end

    describe "when author id exists in other accounts" do
      let(:foreign_account) { accounts(:support) }
      let(:body) { File.read("#{Rails.root}/test/files/small.png") }

      before do
        @comment = tickets(:minimum_1).comments.last
        @upload_token = UploadToken.create(source: @comment.account, target: @comment.author)
        @foreign_attachment = Attachment.create!(
          uploaded_data: RestUpload.new(body, 'small.png', 'image/png'),
          account: accounts(:support),
          is_public: true
        )
        @foreign_attachment.update_column(:source_id, @upload_token.id)
      end

      it "should not pull any foreign attachments" do
        refute @comment.send(:attachment_tokens_map).include? @foreign_attachment.token
      end
    end
  end

  private

  def nest_html_content(html_content, nesting)
    "#{'<div>' * nesting}#{html_content}#{'</div>' * nesting}"
  end
end
