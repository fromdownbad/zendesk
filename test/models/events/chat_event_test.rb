require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe ChatEvent do
  describe "Chat events" do
    should_validate_presence_of :value
  end
end
