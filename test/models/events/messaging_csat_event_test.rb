require_relative "../../support/test_helper"

SingleCov.covered!

describe MessagingCsatEvent do
  fixtures :accounts, :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:agent) { ticket.submitter }

  it 'creates messaging csat event' do
    ticket.will_be_saved_by(agent)
    ticket.audit.events << MessagingCsatEvent.new.tap do |event|
      event.chat_started_id = 1
      event.audit = ticket.audit
      event.via_id = 2
      event.via_reference_id = 3
    end
    ticket.save!

    ticket.reload
    csat_event = ticket.events.where(type: 'MessagingCsatEvent').last

    assert_equal 1, csat_event.chat_started_id
    assert_equal 2, csat_event.via_id
    assert_equal 3, csat_event.via_reference_id
  end
end
