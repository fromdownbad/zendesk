require_relative "../../support/test_helper"
require_relative "../../support/rules_test_helper"

SingleCov.covered! uncovered: 4

describe 'UserCustomFieldEvent' do
  include RulesTestHelper
  include CustomFieldsTestHelper
  fixtures :all

  describe "User custom field events" do
    before do
      @account = accounts(:minimum)
      @ticket = tickets(:minimum_1)
      @requester = User.find(@ticket.requester.id)

      create_user_custom_field!("field_decimal", "decimal field", "Decimal")
      new_rule(DefinitionItem.new("requester.custom_fields.field_decimal", nil, -10.0.to_s)).apply_actions(@ticket)
      @ticket.save!

      cfv = @requester.custom_field_values.value_for_key("field_decimal")
      @ref_hash = {
        object_id: cfv.id,
        object_type: "CustomField::Value",
        info: { cf_field_id: cfv.field.id, owner_id: cfv.owner_id }
      }

      @event_to_s = generate_event(@ref_hash).to_s
    end

    it "renders the same even if CustomField::Value no longer exists" do
      @requester.custom_field_values.value_for_key("field_decimal").delete
      assert_equal @event_to_s, generate_event(@ref_hash).to_s
    end

    it "fails gracefully if CustomField::Field no longer exists" do
      cfv = @requester.custom_field_values.value_for_key("field_decimal")
      field = cfv.field
      cfv.delete
      field.delete
      assert_equal "", generate_event(@ref_hash).to_s
    end
  end

  def generate_event(ref_hash)
    UserCustomFieldEvent.create!(
      account: @account,
      ticket: @ticket,
      audit: @ticket.audits.last,
      author: @account.owner,
      value: -10.0,
      value_previous: 123.45,
      value_reference: ref_hash
    )
  end
end
