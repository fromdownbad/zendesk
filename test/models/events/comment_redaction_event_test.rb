require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe CommentRedactionEvent do
  describe "associations" do
    describe "FacebookComment" do
      let(:facebook_comment) { FacebookComment.new }

      it "builds a comment_redaction_event" do
        facebook_comment.build_redaction_event
      end
    end

    describe "Comment" do
      let(:comment) { Comment.new }

      it "builds a comment_redaction" do
        comment.build_redaction_event
      end
    end

    describe "VoiceComment" do
      let(:voice_comment) { VoiceComment.new }

      it "builds a comment_redaction" do
        voice_comment.build_redaction_event
      end
    end
  end
end
