require_relative "../../support/test_helper"

SingleCov.covered!

describe SlaTargetChange do
  fixtures :accounts, :tickets, :events, :users
  should_serialize :value

  describe "aliased attributes" do
    before { @subject = SlaTargetChange.new }

    it "alias :value_previous to :initial_target" do
      @subject.expects :value_previous
      @subject.initial_target
    end

    it "alias :value_reference to :metric_id" do
      @subject.expects :value_reference
      @subject.metric_id
    end
  end

  describe ".audit_changes" do
    let(:ticket) { tickets(:minimum_1) }

    before do
      ticket.will_be_saved_by(users(:minimum_agent))

      initial_metrics = {
        Zendesk::Sla::TicketMetric::FirstReplyTime.id => stub(target: 30, business_hours?: false),
        Zendesk::Sla::TicketMetric::RequesterWaitTime.id => stub(target: 60, business_hours?: false)
      }

      new_metrics = {
        Zendesk::Sla::TicketMetric::FirstReplyTime.id => stub(target: 40, business_hours?: false),
      }

      old_policy = stub('stub old policy', title: 'stub old policy', id: 1)
      new_policy = stub('stub new policy', title: 'stub new policy', id: 2)
      SlaTargetChange.audit_changes(ticket, initial_metrics, new_metrics, old_policy, new_policy)
    end

    it "creates an audit for the FirstReplyTime change" do
      @subject = ticket.events.find do |audit|
        audit.type == 'SlaTargetChange' &&
        Zendesk::Sla::TicketMetric::FirstReplyTime.id.to_s == audit.metric_id &&
        audit.initial_target[:minutes] == 30 &&
        audit.initial_target[:in_business_hours] == false &&
        audit.final_target[:minutes] == 40 &&
        audit.final_target[:in_business_hours] == false &&
        audit.current_sla_policy == 'stub new policy'
      end

      assert @subject.present?
    end

    it "creates an audit for the RequesterWaitTime change" do
      @subject = ticket.events.find do |audit|
        audit.type == 'SlaTargetChange' &&
        Zendesk::Sla::TicketMetric::RequesterWaitTime.id.to_s == audit.metric_id &&
        audit.initial_target[:minutes] == 60 &&
        audit.initial_target[:in_business_hours] == false &&
        audit.final_target.nil? &&
        audit.current_sla_policy == 'stub new policy'
      end

      assert @subject.present?
    end
  end

  describe "#target_info=" do
    before do
      @subject = SlaTargetChange.new
      @subject.target_info = {
        metric_id: 1,
        initial_target: 2,
        final_target: 3,
        current_sla_policy: Hashie::Mash.new(title: 'test policy')
      }
    end

    it "sets metric id" do
      assert_equal '1', @subject.metric_id
    end

    it "sets initial_target" do
      assert_equal 2, @subject.initial_target
    end

    it "sets final_target" do
      assert_equal 3, @subject.final_target
    end

    it "sets current_sla_policy" do
      assert_equal 'test policy', @subject.current_sla_policy
    end
  end
end
