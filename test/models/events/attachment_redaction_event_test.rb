require_relative "../../support/test_helper"

SingleCov.covered!

describe AttachmentRedactionEvent do
  fixtures :events, :accounts, :tickets, :attachments

  describe "#comment_id" do
    let(:attachment) { attachments(:serialization_comment_attachment) }
    let(:event) { AttachmentRedactionEvent.create!(audit: events(:serialization_comment).audit, attachment_id: attachment.id) }

    it "returns" do
      assert_equal Attachment.find(event.attachment_id).source_id, event.comment_id
    end

    it "is nil if attachment is missing" do
      event.attachment_id = nil
      assert_nil event.comment_id
    end

    it "is nil if attachment is unfound" do
      event.attachment_id = 123
      assert_nil event.comment_id
    end

    it "is nil if attachment is not for a comment" do
      attachment.update_attribute(:source_type, "User")
      assert_nil event.comment_id
    end
  end
end
