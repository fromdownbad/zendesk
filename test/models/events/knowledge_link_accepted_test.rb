require_relative "../../support/test_helper"

SingleCov.covered!

describe KnowledgeLinkAccepted do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:article) do
    {
      id: 1,
      url: 'http://helpcenter.article/1.json',
      title: 'article title 1',
      html_url: 'http://helpcenter.article/1',
      locale: 'en-us'
    }
  end

  before do
    ticket.will_be_saved_by(User.system)
  end

  it "creates a new event with an article" do
    event = KnowledgeLinkAccepted.new
    event.article = article

    ticket.audit.events << event
    ticket.save!

    assert(ticket.events.pluck(:type).include?("KnowledgeLinkAccepted"))
  end

  it "cannot be created without an article" do
    event = KnowledgeLinkAccepted.new

    refute(event.valid?)
  end

  it 'is invalid when the article does not have the {id, url, html_url, url} structure' do
    event = KnowledgeLinkAccepted.new
    event.article = { id: 1 }

    refute(event.valid?)
  end
end
