require_relative "../../support/test_helper"

SingleCov.covered!

describe 'CommentPrivacyChange' do
  fixtures :tickets, :events, :users

  it "is read-only if not a new record" do
    change = CommentPrivacyChange.new
    change.stubs(:new_record?).returns(false)
    assert change.readonly?
  end

  describe "creating an EventChange event" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent))
    end
    it "does not accept event_ids from other tickets" do
      @event = CommentPrivacyChange.new(event_id: 1234567890,
                                        ticket: @ticket,
                                        audit: @ticket.audit)
      refute @event.save
      assert @event.errors[:event_id].any?
    end

    it "accepts event ids from this ticket" do
      @event = CommentPrivacyChange.new(event_id: @ticket.comments.first.id,
                                        audit: @ticket.audit,
                                        ticket: @ticket)
      assert @event.save
    end

    it "expects :event_id, :value and :value_previous as input" do
      @event = CommentPrivacyChange.new(event_id: @ticket.comments.first.id,
                                        value: false,
                                        value_previous: true,
                                        audit: @ticket.audit,
                                        ticket: @ticket)
      assert @event.save
    end
  end

  describe "#changed_to" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent))

      @event1 = CommentPrivacyChange.new(event_id: @ticket.comments.first.id,
                                         value: false,
                                         value_previous: true,
                                         audit: @ticket.audit,
                                         ticket: @ticket)

      @event2 = CommentPrivacyChange.new(event_id: @ticket.comments.first.id,
                                         value: true,
                                         value_previous: true,
                                         audit: @ticket.audit,
                                         ticket: @ticket)
    end

    it "responds with 'private' or 'public' as correct" do
      assert_equal 'private', @event1.changed_to
      @event1.save!
      assert_equal 'private', @event1.changed_to
      assert_equal 'private', @event1.reload.changed_to

      assert_equal 'public', @event2.changed_to
      @event2.save!
      assert_equal 'public', @event2.changed_to
      assert_equal 'public', @event2.reload.changed_to
    end
  end

  describe "#to_s" do
    before do
      @ticket = tickets(:minimum_1)
      @event = CommentPrivacyChange.new(event_id: @ticket.comments.first.id,
                                        value: false,
                                        value_previous: true,
                                        audit: @ticket.audit,
                                        ticket: @ticket)
    end
    it "renders a nicely formatted string" do
      assert_match /Comment .* made private/, @event.to_s
    end
  end
end
