require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe TranslatableError do
  fixtures :tickets, :users, :accounts

  describe "#resolve_error" do
    it "translates normal error" do
      result = TranslatableError.new(ticket: Ticket.first).send(:resolve_error, "assignee", "unauthorized")
      assert_equal "Assignee: unauthorized", result
    end

    it "translates error with unknown type" do
      result = TranslatableError.new.send(:resolve_error, :type, :invalid)
      assert_equal "Type: is invalid", result
    end
  end

  describe "#to_s" do
    it "returns the correct string" do
      disallowed_users = "<austin1@zendesk.com>, <austin2@zendesk.com>, <austin3@zendesk.com>"
      event_options = {
        key: "ticket_fields.current_collaborators.new_users_not_allowed",
        removed_ccs: disallowed_users
      }
      error = TranslatableError.new(ticket: Ticket.first, value: event_options)
      assert_equal "This user does not have permission to create new user records. The following email addresses have been removed from CC: austin1@zendesk.com, austin2@zendesk.com, austin3@zendesk.com", error.to_s
    end
  end
end
