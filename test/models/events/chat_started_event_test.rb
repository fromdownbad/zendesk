require_relative "../../support/test_helper"
require_relative "../../support/attachment_test_helper"

SingleCov.covered!

describe ChatStartedEvent do
  fixtures :accounts, :tickets, :events, :tokens, :attachments

  should have_many(:chat_transcripts)
  should validate_presence_of :ticket

  let(:ticket) { tickets(:minimum_1) }
  let(:agent) { ticket.submitter }

  describe 'validation' do
    it 'allows proper value' do
      event = ChatStartedEvent.new(ticket: ticket, value: {
        'chat_id' => '123',
        'visitor_id' => '456'
      })
      assert event.valid?
    end

    it 'checks for required chat_id' do
      event = ChatStartedEvent.new(ticket: ticket, value: {
        'visitor_id' => '456'
      })
      refute event.valid?
    end

    it 'checks for required visitor_id' do
      event = ChatStartedEvent.new(ticket: ticket, value: {
        'chat_id' => '123'
      })
      refute event.valid?
    end

    it 'allows for optional chat_start_url' do
      event = ChatStartedEvent.new(ticket: ticket, value: {
        'chat_id' => '123',
        'visitor_id' => '456',
        'chat_start_url' => 'https://example.com'
      })
      assert event.valid?
    end

    it 'does not allow for additional attributes' do
      event = ChatStartedEvent.new(ticket: ticket, value: {
        'chat_id' => '123',
        'visitor_id' => '456',
        'foo' => 'bar'
      })
      refute event.valid?
    end
  end

  describe 'uploads' do
    let(:upload) do
      # force load association here, because after the attachments are added, they would have changed ownership
      tokens(:minimum_upload_token).tap { |u| u.attachments.to_a }
    end
    let(:transcripts) do
      ChatTranscript::ChatTranscriptsBuilder.build(account: accounts(:minimum), chat_started_event: event, ticket: ticket) do |builder|
        builder.add_chat_history [{
          type: 'ChatFileAttachment',
          actor_id: '123',
          actor_type: 'agent',
          actor_name: 'mr smith',
          timestamp: Time.zone.now.to_i,
          support_upload_token: upload.value
        }.with_indifferent_access]
      end
    end
    let(:parser) do
      Zendesk::ChatTranscripts::ChatAttachmentsParser.new(transcripts.flat_map(&:chat_history))
    end
    let(:event) do
      ChatStartedEvent.new(account: accounts(:minimum), ticket: ticket, value: { chat_id: '123', visitor_id: '123' })
    end

    before do
      event.audit = events(:create_audit_for_minimum_ticket_1)
      event.add_attachments(parser.attachments(event.account))

      ticket.will_be_saved_by(agent)
      event.apply!
    end

    it 'adds attachments' do
      assert_equal 2, event.attachments.size
      assert_equal upload.attachments, event.attachments
      assert event.attachments.none?(&:is_public?)

      event.attachments.map(&:reload).each do |attachment|
        assert agent.can? :view, attachment
        assert ticket.requester.can? :view, attachment
      end
    end

    describe 'after ticket archiving' do
      before { ticket.archive! }

      it 'attachments can still be viewed' do
        event.attachments.map(&:reload).each do |attachment|
          assert agent.can? :view, attachment # .reload
          assert ticket.requester.can? :view, attachment
        end
      end
    end
  end

  describe 'tags' do
    before do
      ticket.will_be_saved_by(agent)
      ticket.via_id = ViaType.CHAT
      event.apply!
    end

    describe 'without user tags' do
      let(:event) do
        ChatStartedEvent.new(account: accounts(:minimum), ticket: ticket, value: { chat_id: '123', visitor_id: '123' })
      end

      it 'applies system tags' do
        assert_equal 'hello zopim_chat', ticket.reload.current_tags
      end
    end

    describe 'with user tags' do
      let(:event) do
        ChatStartedEvent.new(ticket: ticket, value: {
          'chat_id' => '123',
          'visitor_id' => '456',
          'tags' => %w[foo zopim_chat_ended zopim_chat_missed]
        })
      end

      it 'strips system tags' do
        assert_equal 'foo hello zopim_chat', ticket.reload.current_tags
      end
    end
  end

  describe 'ticket subject' do
    let(:ticket) do
      Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_end_user), {}).ticket
    end

    describe 'for chat tickets' do
      before { ticket.via_id = ViaType.CHAT }

      it 'uses requester name as visitor with default chat subject' do
        ChatStartedEvent.new(ticket: ticket, value: { chat_id: '123', visitor_id: '456' }).apply!
        assert_equal ::I18n.t('txt.chat.new_conversation_title', visitor_name: ticket.requester.name), ticket.subject
      end
    end

    describe 'for social messaging tickets' do
      before { ticket.via_id = ViaType.WHATSAPP }

      it 'uses requester name as visitor with default conversation subject' do
        ChatStartedEvent.new(ticket: ticket, value: { chat_id: '123', visitor_id: '456' }).apply!
        assert_equal ::I18n.t('txt.chat.new_conversation_title', visitor_name: ticket.requester.name), ticket.subject
      end
    end
  end
end
