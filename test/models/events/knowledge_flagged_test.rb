require_relative "../../support/test_helper"

SingleCov.covered!

describe KnowledgeFlagged do
  fixtures :accounts, :tickets, :users

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:ticket) { tickets(:minimum_1) }
  let(:article) { { id: 1, url: 'http://helpcenter.article/1.json', title: 'article title 1', html_url: 'http://helpcenter.article/1', locale: 'en-us' } }

  it "creates a new event with an article" do
    kcs_capture_flag = KnowledgeFlagged.new
    kcs_capture_flag.article = article

    ticket.will_be_saved_by(user)
    ticket.audit.events << kcs_capture_flag
    ticket.save!

    assert(ticket.events.pluck(:type).include?("KnowledgeFlagged"))
  end

  it "cannot be created without an article" do
    kcs_capture_flag = KnowledgeFlagged.new

    refute(kcs_capture_flag.valid?)
  end

  it 'is invalid when the article does not have the {id, url, html_url, url} structure' do
    kcs_capture_flag = KnowledgeFlagged.new
    kcs_capture_flag.article = { id: 1 }

    refute(kcs_capture_flag.valid?)
  end
end
