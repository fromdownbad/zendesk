require_relative "../../support/test_helper"
require 'events/ticket_notifier'

SingleCov.covered! uncovered: 3

# NOTE: this spec was brought back from the deleted spec/ (fast-rspec) suite.
# That's why it uses Stubs extensively.

describe TicketNotifier do
  let(:audit) { stub_everything }
  let(:ticket) { stub_everything }

  subject { TicketNotifier.new(ticket, audit) }

  before do
    ticket.stubs(current_user: stub(machine?: false), account: stub_everything)
    subject.stubs(filtered_cc_ids: stub(blank?: false, slice: true))
    subject.stubs(filtered_organization_subscriber_ids: stub(blank?: false))
  end

  describe ".notify" do
    describe "without an audit comment" do
      it "doesn't notify ccs or organization subscribers" do
        TicketNotifier.any_instance.expects(:notify_organization_subscribers).never
        TicketNotifier.any_instance.expects(:notify_followers).never
        TicketNotifier.notify(ticket, audit)
      end
    end

    describe "given an audit with comment" do
      before do
        audit.stubs(comment: stub_everything)
        TicketNotifier.any_instance.stubs(:notify_organization_subscribers)
        TicketNotifier.any_instance.stubs(:notify_followers)
      end

      it "notifies legacy ccs and followers" do
        TicketNotifier.any_instance.expects(:notify_followers).once
        TicketNotifier.notify(ticket, audit)
      end

      it "notifies organization subscribers" do
        TicketNotifier.any_instance.expects(:notify_organization_subscribers).once
        TicketNotifier.notify(ticket, audit)
      end
    end
  end

  describe "#notify_followers" do
    let(:direct_recipient_id) { 1 }
    let(:direct_recipient_user) { stub(id: direct_recipient_id, email: 'direct_recipient@example.com', is_agent?: false, suspended?: false, email_is_deliverable?: true) }
    let(:follower_id) { 2 }
    let(:follower) { stub(id: follower_id, email: 'follower@example.com', is_agent?: false, suspended?: false, email_is_deliverable?: true) }

    describe "when collaboration is enabled and followers are present" do
      before { subject.stubs(notification_recipients_present?: true) }

      describe "with follower_and_email_cc_collaborations setting disabled" do
        before do
          ticket.stubs(
            id: 1,
            current_user: stub(machine?: false),
            account: stub(
              id: 1,
              has_follower_and_email_cc_collaborations_enabled?: false,
              has_comment_email_ccs_allowed_enabled?: false
            ),
            followers: [follower]
          )

          audit.stubs(
            id: 1,
            comment: stub(is_public?: true, author_id: 3),
            notifications: []
          )

          Zendesk::InboundMail::RecipientsHelper.stubs(:latest_email_user_ids).returns([])
        end

        it "creates a legacy cc event" do
          Cc.expects(:create).with(recipients: [follower_id], audit: audit).once
          subject.send(:notify_followers)
        end
      end

      describe "with ticket_followers_allowed setting enabled" do
        before do
          ticket.stubs(
            id: 1,
            current_user: stub(machine?: false),
            account: stub(
              id: 1,
              has_follower_and_email_cc_collaborations_enabled?: true,
              has_ticket_followers_allowed_enabled?: true,
              has_comment_email_ccs_allowed_enabled?: false
            ),
            followers: [follower]
          )

          audit.stubs(
            id: 1,
            comment: stub(is_public?: true, author_id: 3),
            notifications: []
          )

          Zendesk::InboundMail::RecipientsHelper.stubs(:latest_email_user_ids).returns([])
        end

        it "creates a follower notification event" do
          FollowerNotification.expects(:create).with(recipients: [follower_id], audit: audit).once
          subject.send(:notify_followers)
        end
      end

      describe "given more than 25 filtered follower ids" do
        before { subject.stubs(filtered_follower_ids: 1.upto(26).to_a) }

        it "takes all the ids" do
          subject.expects(:create_follower_event).with(1.upto(26).to_a, anything).once
          subject.send(:notify_followers)
        end
      end
    end

    describe "when either collaboration is disabled or no followers are present" do
      before { subject.stubs(notification_recipients_present?: false) }

      it "does not create a cc event" do
        subject.expects(:create_follower_event).never
        subject.send(:notify_followers)
      end
    end
  end

  describe "#notify_organization_subscribers" do
    describe "given a public comment and an organization" do
      before { subject.stubs(public_comment_and_organization?: true) }

      it "creates an organization update event" do
        subject.expects(:create_organization_activity_event).with(anything, anything).once
        subject.send(:notify_organization_subscribers)
      end
    end

    describe "when comment isn't public or there isn't an organization" do
      before { subject.stubs(public_comment_and_organization?: false) }

      it "does not create an organization activity event" do
        subject.expects(:create_organization_activity_event).never
        subject.send(:notify_organization_subscribers)
      end
    end
  end

  describe '#public_comment_and_organization?' do
    before { audit.stubs(comment: stub_everything) }

    describe "given a public comment and organization" do
      before do
        ticket.stubs(organization: stub)
        audit.stubs(comment: stub(is_public?: true))
      end

      it { assert subject.send(:public_comment_and_organization?) }
    end

    describe "when the ticket doesn't belong to an organization" do
      before { ticket.stubs(organization: nil) }

      it "does not create an organization activity event" do
        subject.expects(:create_organization_activity_event).never
        subject.send(:notify_organization_subscribers)
      end
    end

    describe "when the audit comment is not public" do
      before { audit.stubs(comment: stub(is_public?: false)) }

      it "does not create an organization activity event" do
        subject.expects(:create_organization_activity_event).never
        subject.send(:notify_organization_subscribers)
      end
    end
  end

  describe '#notification_recipients_present?' do
    describe 'with the follower_and_email_cc_collaborations setting enabled' do
      describe 'with the ticket_followers_allowed setting enabled' do
        describe 'when followers are present' do
          before do
            ticket.stubs(
              account: stub(
                has_follower_and_email_cc_collaborations_enabled?: true,
                has_ticket_followers_allowed_enabled?: true
              ),
              followers: stub(present?: true)
            )
          end

          it { assert subject.send(:notification_recipients_present?) }
        end

        describe 'when there are no followers present' do
          before do
            ticket.stubs(
              account: stub(
                has_follower_and_email_cc_collaborations_enabled?: true,
                has_ticket_followers_allowed_enabled?: true
              ),
              followers: stub(present?: false)
            )
          end

          it { refute subject.send(:notification_recipients_present?) }
        end
      end

      describe 'with the ticket_followers_allowed setting disabled' do
        before do
          ticket.stubs(
            account: stub(
              has_follower_and_email_cc_collaborations_enabled?: true,
              has_ticket_followers_allowed_enabled?: false
            )
          )
        end

        it { refute subject.send(:notification_recipients_present?) }
      end
    end

    describe 'with the follower_and_email_cc_collaborations setting disabled' do
      describe 'when collaboration is enabled' do
        describe 'when current_collaborators contains collaborators' do
          before { ticket.stubs(current_collaborators: stub(present?: true)) }

          before do
            ticket.stubs(
              account: stub(
                has_follower_and_email_cc_collaborations_enabled?: false,
                is_collaboration_enabled?: true
              ),
              current_collaborators: stub(
                present?: true
              )
            )
          end

          it { assert subject.send(:notification_recipients_present?) }
        end

        describe 'when current_collaborators does not contain collaborators' do
          before do
            ticket.stubs(
              account: stub(
                has_follower_and_email_cc_collaborations_enabled?: false,
                is_collaboration_enabled?: true
              ),
              current_collaborators: stub(
                present?: false
              )
            )
          end

          it { refute subject.send(:notification_recipients_present?) }
        end
      end

      describe 'when collaboration is disabled' do
        before do
          ticket.stubs(
            account: stub(
              has_follower_and_email_cc_collaborations_enabled?: false,
              is_collaboration_enabled?: false
            )
          )
        end

        it { refute subject.send(:notification_recipients_present?) }
      end
    end
  end

  describe "#organization_subscribers" do
    let(:notifier) { TicketNotifier.new(ticket, audit) }

    describe "when the ticket has no organization" do
      before { ticket.stubs(organization: nil) }

      it "returns an empty array" do
        assert_equal [], subject.send(:organization_subscribers)
      end
    end

    describe "when the ticket has an organization" do
      let(:user_with_own_tickets) { stub(email: "own_tickets@user.com") }
      let(:user_with_org_tickets) { stub(email: "org_tickets@user.com") }
      let(:watchers) { stub(end_users: [user_with_own_tickets, user_with_org_tickets]) }
      let(:organization) { stub(watchers: watchers) }

      before { ticket.stubs(organization: organization) }

      describe "when the organization is shared" do
        before { organization.stubs(is_shared: true) }

        it "returns all subscribers to the organization" do
          assert_equal [user_with_own_tickets, user_with_org_tickets], subject.send(:organization_subscribers)
        end
      end

      describe "when the organization is not shared" do
        before { organization.stubs(is_shared: false) }

        it "only returns subscribers who have access to organization tickets" do
          user_with_own_tickets.expects(:end_user_restriction?).with(:organization).returns(false)
          user_with_org_tickets.expects(:end_user_restriction?).with(:organization).returns(true)

          assert_equal [user_with_org_tickets], subject.send(:organization_subscribers)
        end
      end
    end
  end

  describe "#filtered_users" do
    let(:account) do
      stub(
        user_identities: stub(:email),
        has_follower_and_email_cc_collaborations_enabled?: follower_and_email_cc_collaborations_enabled
      )
    end

    let(:author) { stub(id: 1, email: 'author@user.com', is_agent?: true, suspended?: false, email_is_deliverable?: true, account: account) }
    let(:trigger_user) { stub(id: 2, email: 'trigger@user.com', is_agent?: true, suspended?: false, email_is_deliverable?: true, account: account) }
    let(:cc_user) { stub(id: 3, email: 'cc@user.com', is_agent?: true, suspended?: false, email_is_deliverable?: true, account: account) }
    let(:enduser) { stub(id: 4, email: 'end@user.com', is_agent?: false, suspended?: false, email_is_deliverable?: true, account: account) }
    let(:twitter_user) { stub(id: 5, email: nil, is_agent?: false, suspended?: false, email_is_deliverable?: true, account: account) }
    let(:suspended_user) { stub(id: 6, email: 'suspended@user.com', is_agent?: false, suspended?: true, email_is_deliverable?: true, account: account) }
    let(:undeliverable_user) { stub(id: 7, email: 'undeliverable@user.com', is_agent?: false, suspended?: false, email_is_deliverable?: false, account: account) }
    let(:direct_recipient_user) { stub(id: 8, email: 'direct_recipient@user.com', is_agent?: true, suspended?: false, email_is_deliverable?: true, account: account) }
    let(:submitter) { stub(id: 9, email: 'submitter@user.com', is_agent?: true, suspended?: false, email_is_deliverable?: true, account: account) }
    let(:follower_and_email_cc_collaborations_enabled) { false }
    let(:via_mail) { true }

    before do
      Arturo.stubs(:feature_enabled_for?).with(:agent_as_end_user, anything).returns(false)

      @all_users = [
        author,
        enduser,
        cc_user,
        trigger_user,
        twitter_user,
        suspended_user,
        undeliverable_user,
        direct_recipient_user,
        submitter
      ]

      subject.stubs(organization_subscribers: @all_users)
      subject.stubs(followers: @all_users)

      Zendesk::InboundMail::RecipientsHelper.stubs(:latest_email_user_ids).with(ticket, [cc_user.email, direct_recipient_user.email]).returns([3, 8])

      ticket.stubs(account: account)
    end

    [true, false].each do |comment_visibility|
      describe "when the audit comment is #{comment_visibility ? 'public' : 'private'}" do
        let(:comment) { stub(author_id: author.id, is_public?: comment_visibility) }
        let(:audit) do
          stub(
            comment: comment,
            recipients: "#{cc_user.email.upcase} #{direct_recipient_user.email}",
            notifications: [stub(recipients: trigger_user.id)],
            via?: via_mail
          )
        end

        before do
          subject.stubs(audit: audit)
          @filtered_users = subject.send(:filtered_users)
        end

        it "removes the comment author" do
          refute @filtered_users.include?(author)
        end

        it "removes the suspended users" do
          refute @filtered_users.include?(suspended_user)
        end

        it "removes a user whose email identity is undeliverable" do
          refute @filtered_users.include?(undeliverable_user)
        end

        describe "when the users have been emailed directly from the email" do
          it { refute @filtered_users.include?(direct_recipient_user) }

          describe "when the follower_and_email_cc_collaborations setting is enabled" do
            let(:follower_and_email_cc_collaborations_enabled) { true }

            describe "when the audit's via type is mail" do
              it { refute @filtered_users.include?(direct_recipient_user) }
            end

            describe "when the audit's via type is not mail" do
              let(:via_mail) { false }

              it { assert @filtered_users.include?(direct_recipient_user) }
            end
          end
        end

        it "removes users that already received a trigger notification for this audit" do
          refute @filtered_users.include?(trigger_user)
        end

        describe "when user email is given" do
          # NOTE: CC's email is in uppercase from the Audit's recipient list.
          it "does case insensitive filtering" do
            refute @filtered_users.include?(cc_user)
          end
        end
      end
    end

    describe "when the audit comment is public" do
      let(:comment) { stub(author_id: author.id, is_public?: true) }
      let(:audit) { stub(comment: comment, recipients: "#{cc_user.email.upcase} #{direct_recipient_user.email}", notifications: [stub(recipients: trigger_user.id)]) }

      before do
        subject.stubs(audit: audit)
        @filtered_users = subject.send(:filtered_users)
      end

      it "does not remove end users" do
        assert @filtered_users.include?(enduser)
      end

      describe "when user doesn't have email identity" do
        before do
          audit.stubs(recipients: twitter_user.email.to_s)
          @filtered_users = subject.send(:filtered_users)
        end

        it "does not remove the user and avoids throwing an error" do
          assert @filtered_users.include?(twitter_user)
        end
      end
    end

    describe "when the audit comment is private" do
      let(:comment) { stub(author_id: author.id, is_public?: false) }
      let(:audit) { stub(comment: comment, recipients: "#{cc_user.email.upcase} #{direct_recipient_user.email}", notifications: [stub(recipients: trigger_user.id)]) }

      before do
        subject.stubs(audit: audit)
      end

      describe "when the ticket is not from HelpCenter" do
        before do
          @filtered_users = subject.send(:filtered_users)
        end

        it "removes endusers" do
          refute @filtered_users.include?(enduser)
        end

        describe "when user doesn't have email identity" do
          before do
            audit.stubs(recipients: twitter_user.email.to_s)
            @filtered_users = subject.send(:filtered_users)
          end

          it "removes the user and avoids throwing an error" do
            refute @filtered_users.include?(twitter_user)
          end
        end
      end

      describe "when the agent_as_end_user Arturo is disabled" do
        before do
          Arturo.stubs(:feature_enabled_for?).with(:agent_as_end_user, anything).returns(false)
        end

        describe "when users cannot see private content" do
          before do
            @all_users.each { |user| user.stubs(:can?).with(:view_private_content, ticket).returns(false) }
          end

          it "does not remove users" do
            refute subject.send(:filtered_users).empty?
          end
        end
      end

      describe "when the agent_as_end_user Arturo is enabled" do
        before do
          Arturo.stubs(:feature_enabled_for?).with(:agent_as_end_user, anything).returns(true)
        end

        describe "when users cannot see private content" do
          before do
            @all_users.each { |user| user.stubs(:can?).with(:view_private_content, ticket).returns(false) }
          end

          it "removes users" do
            assert_empty(subject.send(:filtered_users))
          end
        end

        describe "when users can see private content" do
          before do
            @all_users.each { |user| user.stubs(:can?).with(:view_private_content, ticket).returns(true) }
          end

          it "does not remove users" do
            refute subject.send(:filtered_users).empty?
          end
        end
      end
    end
  end
end
