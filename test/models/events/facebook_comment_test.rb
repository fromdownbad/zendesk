require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe FacebookComment do
  fixtures :accounts, :users, :subscriptions, :sequences, :tickets, :facebook_pages

  before do
    @account   = accounts(:minimum)
    @agent     = users(:minimum_agent)
    @page      = facebook_pages(:minimum_facebook_page_1)
    @ticket    = tickets(:minimum_1)
    @ticket.comment = FacebookComment.new(
      body: "hang tight bro", is_public: true, channel_back: "1",
      ticket: @ticket, author: @agent, via_id: 0,
      via_reference_id: 3, account: @account,
      value_reference: {via_zendesk: true}
    )
    @ticket.will_be_saved_by(@agent)
    @ticket.save!
    @comment = @ticket.comments.last
    Brand.any_instance.stubs(:signature_template).returns("sincerely, The Dude")
  end

  it "does not include agent signature" do
    assert_equal "hang tight bro", @comment.body
  end

  it "includes agent signature if channel back is false" do
    @ticket.comment = FacebookComment.new(
      body: "hang tight bro", is_public: true, channel_back: "0", ticket: @ticket,
      author: @agent, via_id: 0, via_reference_id: 3, account: @account, value_reference: {via_zendesk: true}
    )
    @ticket.will_be_saved_by(@agent)
    @ticket.save
    assert_equal "hang tight bro\n\nsincerely, The Dude", @ticket.comments.last.body
  end

  describe "credit card redaction" do
    before do
      Account.any_instance.stubs(has_credit_card_sanitization_enabled?: true)

      @ticket.comment = FacebookComment.new(
        body: 'Hello, my credit card number is 4111 1111 1111 1111',
        is_public: true, channel_back: "1",
        ticket: @ticket, author: @agent, via_id: 0,
        via_reference_id: 3, account: @account,
        value_reference: {via_zendesk: true}
      )
      Koala::Facebook::API.any_instance.stubs(:put_comment).returns("id" => "32423e2d")
      @ticket.will_be_saved_by(@agent)
      @ticket.save!
      @comment = @ticket.comments.last
    end

    it "redacts the credit card number" do
      assert_equal "Hello, my credit card number is 4111 11▇▇ ▇▇▇▇ 1111", @comment.body
    end
  end

  it "returns via_zendesk" do
    assert_equal @comment.via_zendesk, @comment.data[:via_zendesk]
    @comment.data[:via_zendesk] = !@comment.data[:via_zendesk]
    assert_equal @comment.via_zendesk, @comment.data[:via_zendesk]
  end

  describe "#for_partner" do
    before do
      @for_partner = @comment.for_partner
    end

    it "returns a ticketsharing actor" do
      assert_equal TicketSharing::Actor, @for_partner.class
      assert_equal 'user', @for_partner.role
    end

    it "uses requester from metadata" do
      @comment.data[:requester] = {name: "Hurley"}
      assert_equal "Hurley", @comment.for_partner.name
    end

    it "uses author if metadata not present" do
      assert_equal @agent.safe_name(false), @for_partner.name
    end
  end

  describe "shared_attachments" do
    before do
      FacebookComment.any_instance.stubs(:data).returns(
        attachments: [
          { 'id' => 'a', 'mime_type' => "image/jpeg", 'name' => "abc.jpg", "size" => "262443"},
          { 'id' => 'b', 'mime_type' => "application/pdf", 'name' => "abc.pdf", "size" => "23423"}
        ]
      )
    end

    it "creates shared attachments" do
      assert_equal 2, @ticket.comments.last.shared_attachments.size
      assert_equal "abc.jpg", @ticket.comments.last.shared_attachments[0].filename
      assert_equal "abc.pdf", @ticket.comments.last.shared_attachments[1].filename
      assert_equal "image/jpeg", @ticket.comments.last.shared_attachments[0].content_type
      assert_equal "application/pdf", @ticket.comments.last.shared_attachments[1].content_type
    end
  end

  describe "#to_s" do
    it "reads content from value_reference" do
      @comment.value_reference = {content: "xxx"}
      assert_equal "xxx", @comment.to_s.to_s
    end
  end

  describe "value_reference" do
    describe "not too long value reference" do
      before do
        @expected_value_reference = {long_data: 'a' * (FacebookComment::MAX_VALUE_REFERENCE - 30) }
        @ticket.comment = FacebookComment.new(
          body: "hang tight bro", is_public: true, channel_back: "1",
          ticket: @ticket, author: @agent, via_id: 0,
          via_reference_id: 3, account: @account,
          value_reference: @expected_value_reference
        )
        Koala::Facebook::API.any_instance.stubs(:put_comment).returns("id" => "32423e2d")
        @ticket.will_be_saved_by(@agent)
        @ticket.save!
        @comment = @ticket.comments.last
      end

      it "does not replace with {}" do
        assert_equal @expected_value_reference, @comment.value_reference
      end
    end

    describe "very long value_reference" do
      before do
        @ticket.comment = FacebookComment.new(
          body: "hang tight bro", is_public: true, channel_back: "1",
          ticket: @ticket, author: @agent, via_id: 0,
          via_reference_id: 3, account: @account,
          value_reference: {long_data: 'a' * (FacebookComment::MAX_VALUE_REFERENCE + 1) }
        )
        Koala::Facebook::API.any_instance.stubs(:put_comment).returns("id" => "32423e2d")
        @ticket.will_be_saved_by(@agent)
        @ticket.save!
        @comment = @ticket.comments.last
      end

      it "replaces with {}" do
        expected = {}
        assert_equal expected, @comment.value_reference
      end
    end
  end

  describe "#data" do
    describe "value_reference is String (i.e. invalid YAML)" do
      before do
        @comment.value_reference = 'testing'
      end

      it "returns {}" do
        expected = {}
        assert_equal expected, @comment.data
      end
    end

    describe "value_reference is not String" do
      before do
        @expected_value_reference = { 'a' => 'b' }
        @comment.value_reference = @expected_value_reference
      end

      it "returns value_reference" do
        assert_equal @expected_value_reference, @comment.data
      end
    end
  end

  describe "#data=" do
    before do
      @value_reference = 'value reference'
      @comment.data = @value_reference
    end

    it "sets value_reference" do
      assert_equal @value_reference, @comment.value_reference
    end
  end
end
