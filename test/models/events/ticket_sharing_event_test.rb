require_relative "../../support/test_helper"

SingleCov.covered!

describe 'TicketSharingEvent' do
  fixtures :accounts, :tickets, :users

  # Really, we need these too?
  fixtures :subscriptions, :payments, :groups, :memberships,
    :account_property_sets

  resque_inline false

  it 'should create a shared ticket after create' do
    ticket = tickets(:minimum_1)
    user = users(:minimum_agent)

    agreement = FactoryBot.create(:agreement, direction: :out, status: :accepted)

    assert_difference 'SharedTicket.count(:all)', +1 do
      ticket.will_be_saved_by(user)
      ticket.audit.events << TicketSharingEvent.new(agreement_id: agreement.id)
      ticket.save!
    end
  end

  it 'should provide a recipient name' do
    account = accounts(:minimum)
    agreement = FactoryBot.create(:agreement, account: account, name: 'Partner name')
    event = TicketSharingEvent.new(account: account, agreement_id: agreement.id)

    assert_equal('Partner name', event.recipient_name)
  end

  it 'recipient name should provide a reasonable default' do
    agreement = FactoryBot.create(:agreement, name: 'Partner name')
    event = TicketSharingEvent.new(agreement_id: agreement.id)

    assert_equal String, event.recipient_name.class
  end

  it 'should serialize recipients properly' do
    ticket = tickets(:minimum_1)
    user = users(:minimum_agent)
    agreement = FactoryBot.create(:agreement, direction: :out, status: :accepted,
                                              account: ticket.account)

    ticket.will_be_saved_by(user)

    event = TicketSharingEvent.new(ticket: ticket,
                                   agreement_id: agreement.id,
                                   audit: ticket.audit)

    event.save!
    event.reload
    assert_equal(agreement.id, event.agreement_id)
  end

  it 'should not raise an error in case of a trigger that shares a ticket' do
    ticket = tickets(:minimum_1)
    user = users(:minimum_agent)
    agreement = FactoryBot.create(:agreement, direction: :out, status: :accepted)
    ticket.will_be_saved_by(user)
    ticket.audit.events << TicketSharingEvent.new(agreement_id: agreement.id)
    ticket.audit.events << TicketSharingEvent.new(agreement_id: agreement.id)
    ticket.set_nice_id
    assert ticket.save!
  end
end
