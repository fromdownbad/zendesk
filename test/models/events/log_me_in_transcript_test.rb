require_relative "../../support/test_helper"

SingleCov.covered!

describe 'LogMeInTranscript' do
  before do
    @test_pickup_time = (Time.now - 1.minute).to_s(:db)
    @params = simple_payload(@test_pickup_time)
  end

  describe "#truncate_body" do
    before do
      params = JSON.parse(read_test_file('log_me_in_transcript_test_data_long.json'))

      @lmit = LogMeInTranscript.build_from_params(params)
      @lmit.stubs(:inherit_defaults_from_audit).returns(true)
      @lmit.account = accounts(:minimum)
      @lmit.author = users(:minimum_author)

      @old_bytesize = @lmit.body.bytesize
      @lmit.save!
    end

    it "truncates body when it is too long on create" do
      new_bytesize = @lmit.body.bytesize

      assert new_bytesize < @old_bytesize
      assert new_bytesize <= @lmit.send(:max_content_size)
    end

    it "still includes all the information after truncation" do
      body = @lmit.body

      LogMeInTranscript::OTHER_KEYS.each do |key|
        assert body.include?(key[1])
      end

      LogMeInTranscript::TIME_TRACKING_KEYS.each do |key|
        assert body.include?(key[1])
      end

      LogMeInTranscript::TIMESTAMP_KEYS.each do |key|
        assert body.include?(key[1])
      end

      LogMeInTranscript::CUSTOM_FIELD_KEYS.each do |key|
        assert body.include?(key[1])
      end

      LogMeInTranscript::SURVEY_KEYS.each do |key|
        assert body.include?(key[1])
      end
    end
  end

  it '#collect_time_tracking_keys' do
    result = LogMeInTranscript.collect_time_tracking_keys(@params)
    assert_match(/Work time \(seconds\):.*120$/, result)
  end

  describe '#collect_timestamps' do
    it 'collects session timestamps in proper format' do
      result = LogMeInTranscript.collect_timestamps(@params)
      assert_match(/Pickup time \(UTC\):.*#{Regexp.escape(DateTime.parse(@test_pickup_time).strftime("%A, %B %d, %Y at %I:%M %p"))}/, result)
    end

    describe 'with invalid timestamps' do
      before do
        @bad_pickup_time = '2008-10 09:09:09'
        @bad_timestamps = {
          "PickupTime" => @bad_pickup_time,
          "ClosingTime" => Time.now.to_s(:db),
          "LastActionTime" => Time.now.to_s(:db)
        }
      end

      it 'raises ArgumentError, log an exception and include the original timestamp' do
        ZendeskExceptions::Logger.expects(:record)
        result = LogMeInTranscript.collect_timestamps(@bad_timestamps)
        assert_match(/#{Regexp.escape(@bad_pickup_time)}/, result)
      end
    end
  end

  describe '#collect_custom_fields' do
    it 'collects present custom fields' do
      result = LogMeInTranscript.collect_custom_fields(@params)
      assert_match(/CField4: four$/, result)
    end

    it 'does not collect empty fields' do
      result = LogMeInTranscript.collect_custom_fields(@params)
      assert_no_match(/CField5/, result)
    end
  end

  describe '#collect_survey_fields' do
    it 'collects present survey fields' do
      result = LogMeInTranscript.collect_survey_fields(@params)
      assert_match(/TSurvey7: seven$/, result)
    end

    it 'does not collect blank survey fields' do
      result = LogMeInTranscript.collect_survey_fields(@params)
      assert_no_match(/TSurvey6/, result)
    end
  end

  it '#collect_other_fields' do
    result = LogMeInTranscript.collect_other_fields(@params)
    assert_match(/Technician email:.*tech@example.com$/, result)
  end

  it '#build_from_params' do
    event = LogMeInTranscript.build_from_params(@params)

    assert_match(/^3:59 PM Swamy:.*Hi there$/, event.body)
    assert_match(/^Work time \(seconds\):.*120$/, event.body)
    assert_match(/^Technician email:.*tech@example.com$/, event.body)
    assert_match(/^CField3: three$/, event.body)
    assert_match(/^TSurvey8: eight$/, event.body)
  end

  it '#build_comment_from_params' do
    comment_text = LogMeInTranscript.build_comment_from_params(@params)
    assert_match(/^3:59 PM Swamy:.*Hi there$/, comment_text)
    assert_match(/^Work time \(seconds\):.*120$/, comment_text)
    assert_match(/^Technician email:.*tech@example.com$/, comment_text)
  end

  it '#wrap_chat_logs' do
    wrapped_chat_text = LogMeInTranscript.wrap_chat_log(@params["ChatLog"])
    assert_match(/^Chat transcript begins/, wrapped_chat_text)
  end

  describe "#chat_transcript" do
    let(:body) { LogMeInTranscript.build_body_from_params(@params) }

    describe "value_reference is present" do
      before do
        @log_me_in_transcript = LogMeInTranscript.new(value_reference: body)
      end

      it "returns the full transcript" do
        assert_equal @log_me_in_transcript.chat_transcript, body
      end
    end

    describe "value is present and value_reference is nil" do
      before do
        @log_me_in_transcript = LogMeInTranscript.new(body: body)
      end

      it "returns the full transcript" do
        assert_equal @log_me_in_transcript.chat_transcript, body
      end
    end
  end

  private

  def simple_payload(pickup_time = (Time.now - 1.minute).to_s(:db))
    @params ||= {
      "PickupTime" => pickup_time,
      "ClosingTime" => Time.now.to_s(:db),
      "LastActionTime" => Time.now.to_s(:db),
      "WaitingTime" => "20",
      "WorkTime" => "120",
      "TechName" => "Joe Technician",
      "SessionID" => "12345",
      "TechEmail" => "tech@example.com",
      "Note" => "Note to self: insert pun here",
      "Platform" => "MAC OS X Snow Leopard",
      "CField0" => "zero",
      "CField1" => "one",
      "CField2" => "two",
      "CField3" => "three",
      "CField4" => "four",
      "CField5" => "",
      "CField6" => "six",
      "CField7" => "seven",
      "TSurvey0" => "zero",
      "TSurvey1" => "one",
      "TSurvey2" => "two",
      "TSurvey3" => "three",
      "TSurvey4" => "four",
      "TSurvey5" => "five",
      "TSurvey6" => "",
      "TSurvey7" => "seven",
      "TSurvey8" => "eight",
      "TSurvey9" => "nine",
      "ChatLog" => "3:59 PM Connecting to: control.app02-15.logmeinrescue.com (216.52.233.237:443)\n3:59 PM Connected to Applet (RSA 1024 bits, AES256-SHA 256 bits)\n3:59 PM Swamy: Hi Swamyu\n3:59 PM Swamy: Hi there\n4:00 PM The technician ended the session.\n"
    }
  end
end
