require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 9

describe Event do
  describe '#to_s' do
    before do
      @value = '<strong>Event triggered!</strong>'
      @event = Event.create! do |event|
        event.account_id = 1
        event.parent_id  = Event.first.id
        event.author_id  = 1
        event.value      = @value
      end
    end

    it 'declares the value not HTML-safe' do
      assert_equal @value, @event.to_s
      refute @event.to_s.html_safe?
    end
  end
end
