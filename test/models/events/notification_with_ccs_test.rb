require_relative '../../support/test_helper'

SingleCov.covered!

describe NotificationWithCcs do
  fixtures :all

  describe 'when a `NotificationWithCcs` event is created' do
    let(:account) { accounts(:minimum) }
    let(:ticket)  { tickets(:minimum_1) }
    let(:audit)   { events(:create_audit_for_minimum_ticket_1) }

    let(:ccs) { [users(:minimum_end_user2), users(:minimum_author)] }

    let(:to_recipient_ids) do
      [users(:minimum_agent).id, users(:minimum_end_user).id]
    end

    let(:notification) do
      NotificationWithCcs.new(
        account:          account,
        ticket:           ticket,
        subject:          'The rain in Spain',
        body:             'Stays mainly in the plain',
        audit:            audit,
        to_recipient_ids: to_recipient_ids,
        ccs:              ccs,
        via_id:           ViaType.RULE_REVISION,
        via_reference_id: 10007
      )
    end

    let(:expected_recipients) do
      account.
        users.
        where(id: to_recipient_ids).
        select('id, account_id, name, roles').
        includes(:identities, :settings).
        to_a
    end

    describe 'when no `ccs` are provided' do
      let(:ccs) { nil }

      it 'defaults the `ccs` to an empty array' do
        assert_empty notification.ccs
      end
    end

    describe 'when no `to_recipient_ids` are provided' do
      let(:to_recipient_ids) { nil }

      it 'defaults the `to_recipient_ids` to an empty array' do
        assert_empty notification.to_recipient_ids
      end
    end

    describe 'when the notification has been delivered' do
      before { notification.send(:enqueue_ticket_update) }

      before_should 'delivers one notification to agents and end users' do
        EventsMailer.
          expects(:deliver_rule).
          with(
            notification,
            ticket,
            includes(*expected_recipients),
            false,
            ccs
          ).
          once
      end
    end
  end
end
