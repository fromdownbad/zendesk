require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe VoiceApiComment do
  fixtures :tickets, :accounts, :users
  let(:data) do
    {
      from: "+14045555404",
      to: "+18005555800",
      recording_url: recording_url,
      call_duration: 50,
      answered_by_id: user.id,
      transcription_text: "a transcription",
      started_at: "2013-06-24 15:31:44 +0000",
      location: "location",
      call_id: '1',
      recorded: true
    }
  end

  let(:user)    { users(:minimum_agent) }
  let(:account) { accounts(:minimum) }
  let(:comment) { VoiceApiComment.new(data: data, ticket: ticket) }
  let(:ticket)  { tickets(:minimum_1) }
  let(:recording_url) { "/voice/calls/12345/voice_recordings" }

  before do
    VoiceApiComment.any_instance.stubs(:account).returns(account)
  end

  it "sets author" do
    comment = VoiceApiComment.new(data: data, author_id: users(:minimum_end_user).id, account: account)
    assert_equal comment.author, users(:minimum_end_user)
  end

  describe "#absolute_recording_url" do
    it "builds an absolute url from a relative url" do
      assert_equal "#{account.url(ssl: true)}#{data[:recording_url]}", comment.absolute_recording_url
    end

    it "does not format url, when it's already absolute" do
      data[:recording_url] = "#{account.url}/voice/calls/12345/voice_recordings"
      assert_equal data[:recording_url], comment.absolute_recording_url
    end

    it "returns an empty string if there is no recording" do
      data[:recording_url] = nil
      assert_equal "", comment.absolute_recording_url
    end

    it "returns an url provided by customer when call doesn't exist" do
      data[:call_id] = nil
      data[:recording_url] = 'https://mysite.com/recording.mp3'
      assert_equal data[:recording_url], comment.absolute_recording_url
    end
  end

  describe "#recording_url" do
    it "returns the recording URL" do
      assert_equal data[:recording_url], comment.recording_url
    end
  end

  describe "#player_recording_urls" do
    it "returns the recording URL" do
      data[:recording_url] = "#{data[:recording_url]}.mp3"
      assert_equal [data[:recording_url]], comment.player_recording_urls
    end

    it "adds 'mp3' and 'wav' to the recording URL without an extension" do
      assert_equal ["#{data[:recording_url]}.mp3", "#{data[:recording_url]}.wav"], comment.player_recording_urls
    end
  end

  describe "#transcription_visible" do
    it "returns true" do
      assert(comment.transcription_visible)
    end
  end

  describe "a persisted record" do
    it "has a value attribute" do
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.audit.events << comment
      ticket.save!

      assert_not_nil comment.body
      assert_equal "completed", comment.data[:transcription_status]
      assert_equal 50, comment.data[:call_duration]
    end
  end

  describe "#new" do
    describe "with standard 'data' param" do
      it "is valid" do
        assert(comment.valid?)
      end
    end

    describe "with 'location' omitted" do
      it "is valid" do
        data.delete(:location)
        assert(comment.valid?)
      end
    end

    describe "with 'public' omitted" do
      it "does not change the prev value" do
        data.delete(:public)
        assert_nil comment.is_public_changed?
      end
    end

    describe "with 'public' as symbol provided" do
      it "changes the prev value" do
        data[:public] = true
        assert true, comment.is_public_changed?
      end
    end

    describe "with 'public' as string provided" do
      it "changes the prev value" do
        data.merge("public" => true)
        assert true, comment.is_public_changed?
      end
    end

    describe "with 'transcription_text' omitted" do
      it "is valid" do
        data.delete(:transcription_text)
        assert(comment.valid?)
      end
    end

    describe "with blank 'data' param" do
      let(:data) { {} }
      it "is invalid" do
        assert_equal false, comment.valid?
        assert_not_nil comment.errors[:data]
      end
    end

    describe "with invalid 'data' param" do
      let(:data) { {blah: 5} }

      it "is invalid" do
        assert_equal false, comment.valid?
        assert_not_nil comment.errors[:to]
        assert_not_nil comment.errors[:call_duration]
        assert_not_nil comment.errors[:started_at]
      end
    end

    describe "'to' phone number" do
      describe 'WITH arturo :phone_number_formatting_disabled' do
        before { Arturo.enable_feature! :voice_phone_number_formatting_disabled }

        describe "with non-standard format" do
          it "is valid" do
            data[:to] = "\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87\xE2\x96\x87"
            assert(comment.valid?)
          end
        end

        describe "that is too long" do
          it "is valid" do
            data[:to] = "+123123456789012345"
            assert(comment.valid?)
            assert_not_nil comment.errors[:to]
          end
        end
      end
    end

    describe "recording_url" do
      describe "that is too long" do
        it "is invalid" do
          # A url of length > 255: "http://01234...789.mp3"
          data[:recording_url] = "http://" + (("0".."9").to_a * 250).join + ".mp3"
          assert_equal false, comment.valid?
          assert_not_nil comment.errors[:recording_url]
        end
      end
    end

    describe "location" do
      describe "that is too long" do
        it "is invalid" do
          # long string > 255: "01234...789"
          data[:location] = (("0".."9").to_a * 26).join
          assert_equal false, comment.valid?
          assert_not_nil comment.errors[:location]
        end
      end
    end

    describe "with invalid started_at" do
      it "is invalid" do
        data[:started_at] = "test"
        assert_equal false, comment.valid?
        assert_not_nil comment.errors[:started_at]
      end
    end

    describe "answered_by_id" do
      describe "when is not an integer" do
        it "is invalid" do
          data[:answered_by_id] = "test"

          assert_equal false, comment.valid?
          assert_not_nil comment.errors[:answered_by_id]
        end
      end
    end

    describe "call_duration" do
      describe "when is not an integer" do
        it "is invalid" do
          data[:call_duration] = "test"
          assert_equal false, comment.valid?
          assert_not_nil comment.errors[:call_duration]
        end
      end
    end
  end

  describe "#render_transcription?" do
    describe "with a blank transcription_text" do
      it "returns 'false'" do
        data[:transcription_text] = ""
        assert_equal false, comment.render_transcription?
      end
    end

    describe "with a valid transcription_text" do
      it "returns 'true'" do
        data[:transcription_text] = "some text"
        assert(comment.render_transcription?)
      end
    end
  end

  describe "#recording_duration" do
    describe "with a blank call duration" do
      it "returns 0" do
        data[:call_duration] = ""
        assert_equal 0, comment.recording_duration
      end
    end
  end

  describe "#call_duration" do
    describe "with a blank call duration" do
      it "returns 0" do
        data[:call_duration] = ""
        assert_equal 0, comment.recording_duration
      end
    end
  end

  describe "#answered_by" do
    describe "with a valid agent user_id" do
      it "returns the agent's 'clean name'" do
        data[:answered_by_id] = 2
        comment.stubs(:agent).returns(user)
        assert_equal user.clean_name, comment.answered_by
      end
    end

    describe "with an invalid agent user_id" do
      it "returns 0" do
        data[:answered_by_id] = ""
        comment.stubs(:account).returns(accounts(:minimum))
        assert_equal ::I18n.t("event.voice.details.unknown_agent"), comment.answered_by
      end
    end
  end

  describe "with secured recording URLs" do
    describe "with a custom domain" do
      let(:recording_url) { "https://domain.com/recording.mp3" }

      describe "#recording_url" do
        it "returns the recording URL" do
          assert_equal "https://domain.com/recording.mp3", comment.recording_url
        end
      end
    end

    describe "with twilio" do
      let(:recording_url) { "https://api.twilio.com/recording.mp3" }

      describe "#recording_url" do
        it "returns the recording URL" do
          assert_equal "https://api.twilio.com/recording.mp3", comment.recording_url
        end
      end
    end
  end
end
