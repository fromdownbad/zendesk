require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe TwitterEvent do
  fixtures :accounts, :users, :subscriptions, :groups, :memberships, :tickets, :events, :translation_locales

  describe "Tweet events" do
    should_validate_presence_of :requester_user_id
    should_validate_presence_of :sender_mth_id

    describe Tweet do
      before { @tweet = Tweet.new }
      should_serialize :value

      it "alias :value_previous to :from" do
        @tweet.expects :value_previous
        @tweet.from
      end

      it "alias :value_reference to :body" do
        @tweet.expects :value_reference
        @tweet.body
      end

      it "alias :value to :recipients" do
        @tweet.expects :value
        @tweet.recipients
      end
    end
  end
end
