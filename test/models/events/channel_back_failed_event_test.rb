require_relative "../../support/test_helper"
require_relative "../../support/attachment_test_helper"

SingleCov.covered!

describe ChannelBackFailedEvent do
  describe '#value' do
    it 'returns the value' do
      value = { description: 'testing' }
      cbe = ChannelBackFailedEvent.new(value: value)
      assert_equal 'testing', cbe.value[:description]
    end
  end
end
