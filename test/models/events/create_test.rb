require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Create do
  should_validate_presence_of :value_reference

  it "is read-only if not a new record" do
    create = Create.new
    create.stubs(:new_record?).returns(false)
    assert create.readonly?
  end
end
