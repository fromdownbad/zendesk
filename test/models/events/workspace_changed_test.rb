require_relative "../../support/test_helper"

SingleCov.covered!

describe WorkspaceChanged do
  fixtures :accounts, :tickets, :users

  let(:ticket) { tickets(:minimum_1) }

  it "creates an event with type 'WorkspaceChanged'" do
    ws_event = WorkspaceChanged.new
    ws_event.workspace_attributes = {
      action: :ADD,
      id: 1,
      title: 'workspace 1'
    }

    ticket.will_be_saved_by(User.system)
    ticket.audit.events << ws_event
    ticket.save!

    assert_equal 1, ticket.events.where(type: 'WorkspaceChanged', account_id: ticket.account_id).count
  end

  describe 'add_new_workspace_audit' do
    it "adds a new workspace changed audit with an action :ADD" do
      ws_event = WorkspaceChanged.new
      ws = OpenStruct.new(workspace: { id: 1, title: 'title' })
      ws_event.add_new_workspace_audit!(ws)

      assert ws_event.workspace_attributes
      assert_equal ws.workspace[:id], ws_event.workspace_id
      assert_equal ws.workspace[:title], ws_event.workspace_title
      assert_equal :ADD, ws_event.action
    end
  end

  describe 'add_change_workspace_audit' do
    it "adds a new workspace changed audit with an action :CHANGE" do
      ws_event = WorkspaceChanged.new
      workspace_attributes = OpenStruct.new(workspace: { id: 1, title: 'refunds workspace'}, previous_workspace: { id: 2, title: 'billing workspace'})
      ws_event.add_change_workspace_audit!(workspace_attributes)

      assert ws_event.workspace_attributes
      assert ws_event.previous_workspace_attributes

      assert_equal :CHANGE, ws_event.action
    end
  end

  describe 'add_delete_workspace_audit' do
    it "adds a new workspace changed audit with an action :DELETE" do
      ws_event = WorkspaceChanged.new
      workspace_attributes = OpenStruct.new(workspace: { id: 1, title: 'refunds workspace'})
      ws_event.add_delete_workspace_audit!(workspace_attributes)

      assert ws_event.workspace_attributes
      assert_equal workspace_attributes.workspace[:id], ws_event.workspace_id
      assert_equal workspace_attributes.workspace[:title], ws_event.workspace_title
      assert_equal :DELETE, ws_event.action
    end
  end
end
