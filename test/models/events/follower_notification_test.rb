require_relative "../../support/test_helper"
require_relative '../../support/collaboration_settings_test_helper'

SingleCov.covered!

describe FollowerNotification do
  fixtures :accounts, :events, :tickets

  describe 'when a `FollowerNotification` is created' do
    let(:account) { accounts(:minimum) }
    let(:ticket)  { tickets(:minimum_1) }
    let(:audit)   { events(:create_audit_for_minimum_ticket_1) }

    let(:agent) { users(:minimum_agent) }
    let(:admin) { users(:minimum_admin) }

    let(:notification) do
      FollowerNotification.new.tap do |change|
        change.account = account
        change.ticket = ticket
        change.audit = audit
        change.value = [agent.id, admin.id, users(:minimum_end_user).id]
      end
    end

    let(:expected_recipients) do
      account.
        users.
        where(id: [agent.id, admin.id]).
        select('id, account_id, name, roles').
        includes(:identities, :settings).
        to_a
    end

    describe ':agent_as_end_user Arturo is disabled' do
      before do
        Arturo.disable_feature!(:agent_as_end_user)
        notification.send(:enqueue_ticket_update)
      end

      before_should 'deliver a notification to agents only' do
        EventsMailer.
          expects(:deliver_follower_notification).
          with(
            ticket,
            expected_recipients,
            true,
            notification,
            notification.id
          ).
          once
      end

      describe 'when the ticket does not exist' do
        let(:ticket) { nil }

        before { notification.send(:enqueue_ticket_update) }

        before_should 'not deliver a notification' do
          EventsMailer.expects(:deliver_follower_notification).never
        end
      end
    end

    describe ':agent_as_end_user Arturo is enabled' do
      let(:via_id) { ViaType.WEB_SERVICE }
      let(:via_reference_id) { ViaType.HELPCENTER }

      before do
        Arturo.enable_feature!(:agent_as_end_user)
        ticket.update_column(:via_id, via_id)
        ticket.update_column(:via_reference_id, via_reference_id)

        EventsMailer.
          expects(:deliver_follower_notification).
          with(
            ticket,
            expected_recipients,
            true,
            notification,
            notification.id
          ).
          once
      end

      describe 'via_id does not match agent-as-end-user requirements' do
        let(:via_id) { ViaType.RULE }

        it 'delivers notification to all agents' do
          notification.send(:enqueue_ticket_update)
        end
      end

      describe 'via_reference_id does not match agent-as-end-user requirements' do
        let(:via_reference_id) { nil }

        it 'delivers notification to all agents' do
          notification.send(:enqueue_ticket_update)
        end
      end

      describe 'via_id and via_reference_id match agent-as-end-user requirements' do
        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |test_via_id|
          describe "via_id is #{test_via_id}" do
            let(:via_id) { test_via_id }
            describe 'submitter is an agent' do
              let(:expected_recipients) do
                account.
                  users.
                  where(id: [admin.id]).
                  select('id, account_id, name, roles').
                  includes(:identities, :settings).
                  to_a
              end

              it 'skips delivery to submitting agent' do
                notification.send(:enqueue_ticket_update)
              end
            end

            describe 'submitter is an admin' do
              let(:expected_recipients) do
                account.
                  users.
                  where(id: [agent.id, admin.id]).
                  select('id, account_id, name, roles').
                  includes(:identities, :settings).
                  to_a
              end

              before do
                ticket.update_column(:submitter_id, admin.id)
              end

              it 'delivers to admin' do
                notification.send(:enqueue_ticket_update)
              end
            end
          end
        end
      end
    end
  end

  describe '#body' do
    let(:audit) { events(:create_audit_for_minimum_ticket_1) }

    let(:notification) do
      FollowerNotification.new.tap do |change|
        change.account = accounts(:minimum)
        change.ticket = tickets(:minimum_1)
        change.audit = audit
        change.value = [42, 13]
      end
    end

    it 'returns the audit comment' do
      assert_equal audit.comment, notification.body
    end
  end

  it "is read-only if not a new record" do
    follower_notification = FollowerNotification.new
    follower_notification.stubs(:new_record?).returns(false)

    assert follower_notification.readonly?
  end

  describe "with collaboration disabled and CCs/Followers settings enabled" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.account.stubs(:is_collaboration_enabled?).returns(false)
      CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
    end

    it "records the list of ids that were sent a follower notification in the value field" do
      follower_notification = FollowerNotification.create(value: ids = [1, 2, 3, 4, 5], audit: @ticket.audits.first)
      assert ids, follower_notification.value.inspect
    end
  end
end
