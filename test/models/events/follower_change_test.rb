require_relative "../../support/test_helper"
require_relative "../../support/collaboration_settings_test_helper"

SingleCov.covered!

describe FollowerChange do
  fixtures :accounts, :tickets, :users

  let(:ticket) { tickets(:minimum_1) }
  let(:user) { users(:minimum_agent) }

  before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

  def save_new_event(current_followers: nil, previous_followers: nil)
    @event = FollowerChange.new.tap do |change|
      change.current_followers = current_followers
      change.previous_followers = previous_followers
    end
    ticket.will_be_saved_by(user)
    ticket.audit.events << @event
    ticket.set_nice_id
    ticket.save!
  end

  it "is valid" do
    event = FollowerChange.new
    assert event.valid?
  end

  it "is read_only if not a new record" do
    event = FollowerChange.new
    event.stubs(:new_record?).returns(false)
    assert event.readonly?
  end

  it "has a value_reference of `current_followers`" do
    save_new_event
    @event.value_reference.must_equal "current_followers"
  end

  describe "#current_followers" do
    it "is an empty Array by default" do
      save_new_event
      @event.current_followers.must_equal []
    end

    it "accepts Array values" do
      save_new_event(current_followers: [])
      save_new_event(current_followers: ["foo@example.com"])
    end

    it "does not accept String values" do
      assert_raise(ActiveRecord::SerializationTypeMismatch) do
        save_new_event(current_followers: "foo bar")
      end
    end
  end

  describe "#previous_followers" do
    it "is an empty Array by default" do
      save_new_event
      @event.previous_followers.must_equal []
    end

    it "accepts Array values" do
      save_new_event(previous_followers: [])
      save_new_event(previous_followers: ["foo@example.com"])
    end

    it "does not accept String values" do
      assert_raise(ActiveRecord::SerializationTypeMismatch) do
        save_new_event(previous_followers: "foo bar")
      end
    end
  end
end
