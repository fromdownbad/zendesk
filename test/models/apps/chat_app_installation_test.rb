require_relative "../../support/test_helper"

SingleCov.covered!

describe 'ChatAppInstallation' do
  before do
    Rails.env.stubs(:production?).returns(true)
  end

  describe '#handle_disable_installation' do
    before do
      @mock_app_market_client = MiniTest::Mock.new

      @mock_app_market_client.expect(:fetch_installations, [{
        "id" => 1,
        "enabled" => true,
        "app_id" => 30460
      }])
    end

    it 'fetches installations and update the relevant installation' do
      installation = Apps::ChatAppInstallation.new(app_market_client: @mock_app_market_client)
      response = Faraday::Response.new(status: 200, body: { 'enabled' => false })
      @mock_app_market_client.expect(:update_installation, response, [1, enabled: false])
      installation.handle_disable_installation
      @mock_app_market_client.verify
    end

    it 'raises StandardError if AppMarketClient cannot update installation' do
      installation = Apps::ChatAppInstallation.new(app_market_client: @mock_app_market_client)
      response = Faraday::Response.new(status: 200, body: { 'enabled' => true })
      @mock_app_market_client.expect(:update_installation, response, [1, enabled: false])
      assert_raises StandardError do
        installation.handle_disable_installation
      end
    end
    it 'assumes no installation on ZendeskAPI::Error::RecordNotFound' do
      app_market_client = Zendesk::AppMarketClient.new("test")
      app_market_client.stubs(:fetch_installations).raises(ZendeskAPI::Error::RecordNotFound.new('RecordNotFound'))
      installation = Apps::ChatAppInstallation.new(app_market_client: app_market_client)
      refute installation.handle_disable_installation
    end
  end
end
