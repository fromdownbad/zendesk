require_relative "../support/test_helper"
require_relative "../support/billing_test_helper"

SingleCov.covered! uncovered: 6

describe Coupon do
  before { Timecop.freeze(Time.now.beginning_of_day + 12.hours) }

  describe '#is_valid_on?' do
    it 'always return false' do
      refute Coupon.new.is_valid_on?(nil, nil)
    end
  end

  describe 'Effectiveness methods' do
    before do
      @coupon = FactoryBot.create(:trial_coupon)
      assert Time.now < @coupon.effective
    end

    describe 'before the effective window' do
      it 'returns false from in_effective_window?' do
        refute @coupon.in_effective_window?
      end

      it 'returns true from before_effective_window?' do
        assert @coupon.before_effective_window?
      end

      it 'returns false from after_effective_window?' do
        refute @coupon.after_effective_window?
      end

      it 'returns false from active?' do
        refute @coupon.active?
      end

      it 'returns true from editable?' do
        assert @coupon.editable?
      end
    end

    describe 'just after the effective date' do
      before do
        Timecop.travel @coupon.effective + 1.day
      end

      it 'returns true from in_effective_window?' do
        assert @coupon.in_effective_window?
      end

      it 'returns false from before_effective_window?' do
        refute @coupon.before_effective_window?
      end

      it 'returns false from after_effective_window?' do
        refute @coupon.after_effective_window?
      end

      it 'returns true from active?' do
        assert @coupon.active?
      end

      it 'returns false from editable?' do
        refute @coupon.editable?
      end
    end

    describe 'just after the expiry date' do
      before do
        Timecop.travel @coupon.expiry + 1.day
      end

      it 'returns false from in_effective_window?' do
        refute @coupon.in_effective_window?
      end

      it 'returns false from before_effective_window?' do
        refute @coupon.before_effective_window?
      end

      it 'returns true from after_effective_window?' do
        assert @coupon.after_effective_window?
      end

      it 'returns false from active?' do
        refute @coupon.active?
      end

      it 'returns false from editable?' do
        refute @coupon.editable?
      end
    end
  end

  describe 'validations:' do
    before do
      @trial_coupon = FactoryBot.build :trial_coupon
      assert @trial_coupon.valid?
    end

    it 'disallows destroying a coupon after its effective date' do
      @trial_coupon.effective = 1.day.ago
      @trial_coupon.save!

      refute @trial_coupon.destroy
    end

    it 'disallows changing #type after coupon is saved' do
      @trial_coupon.save!
      @trial_coupon.type = DiscountCoupon.to_s

      assert_raise ActiveRecord::RecordInvalid do
        @trial_coupon.save!
      end
    end

    describe 'after the effective date on a saved coupon' do
      before do
        @trial_coupon.effective = 1.day.ago
        @trial_coupon.save!
      end

      it 'makes all attributes other than #forced_inactive read only' do
        { coupon_code: 'Magazine',
          expiry: @trial_coupon.expiry + 1.day,
          effective: @trial_coupon.effective - 1,
          length_days: 1000}.each_pair do |attribute, value|
          assert @trial_coupon.valid?
          old = @trial_coupon.send attribute
          @trial_coupon.send "#{attribute}=", value
          refute @trial_coupon.valid?
          @trial_coupon.send "#{attribute}=", old
        end
      end

      it 'allows changes to :forced_inactive' do
        @trial_coupon.forced_inactive = !@trial_coupon.forced_inactive
        assert @trial_coupon.valid?
      end
    end

    it 'allows changes to all attributes prior to effective date on a saved coupon' do
      @trial_coupon.save!

      @trial_coupon.coupon_code = "shhhh"
      @trial_coupon.forced_inactive = true
      @trial_coupon.expiry = @trial_coupon.expiry + 1.day
      @trial_coupon.effective = @trial_coupon.effective - 1.day
      @trial_coupon.length_days = 666
      @trial_coupon.terms_and_conditions = "bonkers"

      assert @trial_coupon.valid?
    end
  end

  describe '#active' do
    before do
      @trial_coupon = FactoryBot.build(:trial_coupon)
    end

    it 'is false prior to the effective date' do
      refute @trial_coupon.active?
      @trial_coupon.forced_inactive = true
      refute @trial_coupon.active?
    end

    it 'is true on the effective date unless #forced_inactive' do
      @trial_coupon.effective = Time.zone.now

      assert @trial_coupon.active?

      @trial_coupon.forced_inactive = true
      refute @trial_coupon.active?

      @trial_coupon.forced_inactive = false
      assert @trial_coupon.active?
    end

    it 'is true after the effective date unless !#forced_inactive' do
      @trial_coupon.effective = 1.day.ago

      assert @trial_coupon.active?

      @trial_coupon.forced_inactive = true
      refute @trial_coupon.active?

      @trial_coupon.forced_inactive = false
      assert @trial_coupon.active?
    end
  end
end
