require_relative "../../support/test_helper"
SingleCov.covered!

describe TicketForm::TicketFieldConditions do
  fixtures :accounts, :account_settings, :ticket_fields, :custom_field_options

  let(:account) { accounts(:minimum) }
  let(:field_tagger_custom) { ticket_fields(:field_tagger_custom) }
  let(:field_tagger_custom_2) { FactoryBot.create(:field_tagger, account: account) }
  let(:field_tagger_custom_3) { FactoryBot.create(:field_tagger, account: account) }
  let(:field_checkbox) { ticket_fields(:field_checkbox_custom) }
  let(:field_text_custom) { ticket_fields(:field_text_custom) }
  let(:cfo1) { custom_field_options(:field_tagger_custom_option_1) }
  let(:cfo2) { custom_field_options(:field_tagger_custom_option_2) }
  let(:ticket_form) do
    params = {
      ticket_form: {
        name: "Wombat",
        display_name: "Wombats are everywhere",
        position: 2,
        default: false,
        active: true,
        end_user_visible: true,
        in_all_brands: true,
        ticket_field_ids: [
          field_tagger_custom.id,
          field_tagger_custom_2.id,
          field_checkbox.id,
          field_tagger_custom_3.id,
          field_text_custom.id
        ]
      }
    }

    form = Zendesk::TicketForms::Initializer.new(account, params).ticket_form
    form.save!

    form
  end
  let(:conditions) do
    [
      {
        parent_field_id: field_tagger_custom.id,
        value: cfo1.value,
        child_fields: [
          id: field_tagger_custom_2.id,
          is_required: false
        ]
      },
      {
        parent_field_id: field_checkbox.id,
        value: true,
        child_fields: [
          id: field_tagger_custom_3.id,
          is_required: false
        ]
      }
    ]
  end
  let(:ticket_form_with_conditions) do
    ticket_form.agent_conditions = conditions
    ticket_form.end_user_conditions = conditions
    ticket_form.save!

    ticket_form
  end

  before do
    Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(true)
  end

  describe "#agent_conditions=" do
    it "prepares and sets a new set of agent conditions as nested attributes" do
      first = ticket_form_with_conditions.agent_ticket_field_conditions.first
      second = ticket_form_with_conditions.agent_ticket_field_conditions.second

      # the first existing condition will be deleted
      # the second existing condition will be updated (is_required = true)
      # a new condition will be added
      new_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo2.value,
          child_fields: [
            id: field_tagger_custom_2.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_tagger_custom_3.id,
            is_required: true
          ]
        }
      ]

      expected = [
        {
          id: nil, # new condition
          account: account,
          user_type: :agent,
          parent_field: field_tagger_custom,
          value: cfo2.value,
          child_field: field_tagger_custom_2,
          is_required: false
        },
        {
          id: second.id, # correctly converts true to '1' and finds existing condition
          account: account,
          user_type: :agent,
          parent_field: field_checkbox,
          value: true,
          child_field: field_tagger_custom_3,
          is_required: true # new value
        },
        {
          id: first.id, # deleted condition
          _destroy: true
        }
      ]

      ticket_form_with_conditions.expects(:ticket_field_conditions_attributes=).with(expected)
      ticket_form_with_conditions.agent_conditions = new_conditions
    end

    it "correctly passes the incoming required_on_statuses to the condition" do
      expected = [
        {
          id: nil,
          account: account,
          user_type: :agent,
          parent_field: field_tagger_custom,
          value: cfo1.value,
          child_field: field_tagger_custom_2,
          is_required: nil,
          shaped_required_on_statuses: {
            type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
          }
        }
      ]

      ticket_form.expects(:ticket_field_conditions_attributes=).with(expected)
      ticket_form.agent_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_tagger_custom_2.id,
            required_on_statuses: {
              type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
            }
          ]
        }
      ]
    end

    it "doesn't try to parse required_on_statuses or raise errors if not received" do
      expected = [
        {
          id: nil,
          account: account,
          user_type: :agent,
          parent_field: field_tagger_custom,
          value: cfo1.value,
          child_field: field_tagger_custom_2,
          is_required: false
        }
      ]

      ticket_form.expects(:ticket_field_conditions_attributes=).with(expected)
      ticket_form.agent_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_tagger_custom_2.id,
            is_required: false
          ]
        }
      ]
    end
  end

  describe "#end_user_conditions=" do
    it "prepares and sets a new set of end-user conditions as nested attributes" do
      first = ticket_form_with_conditions.end_user_ticket_field_conditions.first
      second = ticket_form_with_conditions.end_user_ticket_field_conditions.second

      # the first existing condition will be deleted
      # the second existing condition will be updated (is_required = true)
      # a new condition will be added
      new_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo2.value,
          child_fields: [
            id: field_tagger_custom_2.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_tagger_custom_3.id,
            is_required: true
          ]
        }
      ]

      expected = [
        {
          id: nil, # new condition
          account: account,
          user_type: :end_user,
          parent_field: field_tagger_custom,
          value: cfo2.value,
          child_field: field_tagger_custom_2,
          is_required: false
        },
        {
          id: second.id, # correctly converts true to '1' and finds existing condition
          account: account,
          user_type: :end_user,
          parent_field: field_checkbox,
          value: true,
          child_field: field_tagger_custom_3,
          is_required: true # new value
        },
        {
          id: first.id, # deleted condition
          _destroy: true
        }
      ]

      ticket_form_with_conditions.expects(:ticket_field_conditions_attributes=).with(expected)
      ticket_form_with_conditions.end_user_conditions = new_conditions
    end
  end

  describe "circular conditions detection" do
    it "does not include circular condition error messages when parent/child fields are not in the form" do
      # Remove ticket fields from form
      ticket_form.ticket_field_ids = []
      assert ticket_form.save

      # Try to save conditions that use fields that are not in the form
      ticket_form.agent_conditions = [
        parent_field_id: field_tagger_custom.id,
        value: cfo1.value,
        child_fields: [
          id: field_tagger_custom_2.id,
          is_required: false
        ]
      ]

      refute ticket_form.save
      # Only includes error messages related to invalid parent/child fields
      assert_equal 2, ticket_form.errors.count
      assert_equal ["is invalid"], ticket_form.errors.messages[:'ticket_field_conditions.parent_field']
      assert_equal ["is invalid"], ticket_form.errors.messages[:'ticket_field_conditions.child_field']
      assert_equal false, ticket_form.errors.messages.value?([I18n.t('txt.admin.model.ticket_form.circular_conditions', field_1: '', field_2: '')])
    end

    describe "on agent conditions" do
      it "detects circular dependencies with 1 condition" do
        ticket_form.agent_conditions = [
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_tagger_custom.id,
            is_required: false
          ]
        ]

        refute ticket_form.valid?
        error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
          field_1: field_tagger_custom.title,
          field_2: field_tagger_custom.title)
        assert_equal [error_message], ticket_form.errors[:base]
      end

      it "detects circular dependencies with 2 conditions" do
        ticket_form.agent_conditions = [
          {
            parent_field_id: field_tagger_custom.id,
            value: cfo1.value,
            child_fields: [
              id: field_checkbox.id,
              is_required: false
            ]
          },
          {
            parent_field_id: field_checkbox.id,
            value: true,
            child_fields: [
              id: field_tagger_custom.id,
              is_required: false
            ]
          }
        ]
        refute ticket_form.valid?
        error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
          field_1: field_tagger_custom.title,
          field_2: field_checkbox.title)
        assert_equal [error_message], ticket_form.errors[:base]
      end
    end

    it "detects circular dependencies with 3 conditions" do
      ticket_form.agent_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_checkbox.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_text_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_text_custom.id,
          value: 'hi',
          child_fields: [
            id: field_tagger_custom.id,
            is_required: false
          ]
        }
      ]

      refute ticket_form.valid?
      error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
        field_1: field_text_custom.title,
        field_2: field_tagger_custom.title)
      assert_equal [error_message], ticket_form.errors[:base]
    end

    it "detects circular dependencies with 3 shuffled conditions" do
      ticket_form.agent_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_text_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_tagger_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_text_custom.id,
          value: 'hi',
          child_fields: [
            id: field_checkbox.id,
            is_required: false
          ]
        }
      ]

      refute ticket_form.valid?
      error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
        field_1: field_checkbox.title,
        field_2: field_tagger_custom.title)
      assert_equal [error_message], ticket_form.errors[:base]
    end

    it "detects circular dependencies with 5 shuffled conditions" do
      ticket_form.agent_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_text_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_tagger_custom_2.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_tagger_custom_2.id,
          value: field_tagger_custom_2.custom_field_options.first.value,
          child_fields: [
            id: field_tagger_custom_3.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_tagger_custom_3.id,
          value: field_tagger_custom_3.custom_field_options.first.value,
          child_fields: [
            id: field_tagger_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_text_custom.id,
          value: 'hi',
          child_fields: [
            id: field_checkbox.id,
            is_required: false
          ]
        }
      ]

      refute ticket_form.valid?
      error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
        field_1: field_checkbox.title,
        field_2: field_tagger_custom.title)
      assert_equal [error_message], ticket_form.errors[:base]
    end

    it "allows conditions without circular dependencies" do
      ticket_form.agent_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_checkbox.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_text_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_text_custom.id,
          value: 'hi',
          child_fields: [
            id: field_tagger_custom_2.id,
            is_required: false
          ]
        }
      ]

      assert ticket_form.valid?
    end

    describe "on end-user conditions" do
      it "detects circular dependencies with 1 condition" do
        ticket_form.end_user_conditions = [
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_tagger_custom.id,
            is_required: false
          ]
        ]

        refute ticket_form.valid?
        error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
          field_1: field_tagger_custom.title,
          field_2: field_tagger_custom.title)
        assert_equal [error_message], ticket_form.errors[:base]
      end

      it "detects circular dependencies with 2 conditions" do
        ticket_form.end_user_conditions = [
          {
            parent_field_id: field_tagger_custom.id,
            value: cfo1.value,
            child_fields: [
              id: field_checkbox.id,
              is_required: false
            ]
          },
          {
            parent_field_id: field_checkbox.id,
            value: true,
            child_fields: [
              id: field_tagger_custom.id,
              is_required: false
            ]
          }
        ]
        refute ticket_form.valid?
        error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
          field_1: field_tagger_custom.title,
          field_2: field_checkbox.title)
        assert_equal [error_message], ticket_form.errors[:base]
      end
    end

    it "detects circular dependencies with 3 conditions" do
      ticket_form.end_user_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_checkbox.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_text_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_text_custom.id,
          value: 'hi',
          child_fields: [
            id: field_tagger_custom.id,
            is_required: false
          ]
        }
      ]

      refute ticket_form.valid?
      error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
        field_1: field_text_custom.title,
        field_2: field_tagger_custom.title)
      assert_equal [error_message], ticket_form.errors[:base]
    end

    it "detects circular dependencies with 3 shuffled conditions" do
      ticket_form.end_user_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_text_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_tagger_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_text_custom.id,
          value: 'hi',
          child_fields: [
            id: field_checkbox.id,
            is_required: false
          ]
        }
      ]

      refute ticket_form.valid?
      error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
        field_1: field_checkbox.title,
        field_2: field_tagger_custom.title)
      assert_equal [error_message], ticket_form.errors[:base]
    end

    it "detects circular dependencies with 5 shuffled conditions" do
      ticket_form.end_user_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_text_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_tagger_custom_2.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_tagger_custom_2.id,
          value: field_tagger_custom_2.custom_field_options.first.value,
          child_fields: [
            id: field_tagger_custom_3.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_tagger_custom_3.id,
          value: field_tagger_custom_3.custom_field_options.first.value,
          child_fields: [
            id: field_tagger_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_text_custom.id,
          value: 'hi',
          child_fields: [
            id: field_checkbox.id,
            is_required: false
          ]
        }
      ]

      refute ticket_form.valid?
      error_message = I18n.t('txt.admin.model.ticket_form.circular_conditions',
        field_1: field_checkbox.title,
        field_2: field_tagger_custom.title)
      assert_equal [error_message], ticket_form.errors[:base]
    end

    it "allows conditions without circular dependencies" do
      ticket_form.end_user_conditions = [
        {
          parent_field_id: field_tagger_custom.id,
          value: cfo1.value,
          child_fields: [
            id: field_checkbox.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_checkbox.id,
          value: true,
          child_fields: [
            id: field_text_custom.id,
            is_required: false
          ]
        },
        {
          parent_field_id: field_text_custom.id,
          value: 'hi',
          child_fields: [
            id: field_tagger_custom_2.id,
            is_required: false
          ]
        }
      ]

      assert ticket_form.valid?
    end
  end

  it "does not show conditions with missing/deleted parent ticket fields" do
    # Skip no conditions validation to be able to destroy the field
    # and reproduce the missing field situation.
    ticket_form_with_conditions
    TicketField.any_instance.stubs(:validate_no_conditions_for_destroy)
    TicketField.destroy(field_tagger_custom.id)
    ticket_form_with_conditions.reload

    # Conditions without deleted field
    expected_agent_conditions = [
      parent_field_id: field_checkbox.id,
      value: true,
      child_fields: [
        id: field_tagger_custom_3.id,
        is_required: false,
        required_on_statuses: {
          type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES
        }
      ]
    ]

    expected_end_user_conditions = [
      parent_field_id: field_checkbox.id,
      value: true,
      child_fields: [
        id: field_tagger_custom_3.id,
        is_required: false
      ]
    ]

    assert_equal expected_agent_conditions, ticket_form_with_conditions.agent_conditions
    assert_equal expected_end_user_conditions, ticket_form_with_conditions.end_user_conditions
  end

  it "destroys conditions on soft deletion" do
    assert ticket_form_with_conditions.ticket_field_conditions.present?
    ticket_form_with_conditions.soft_delete!
    ticket_form_with_conditions.reload
    assert ticket_form_with_conditions.deleted?
    assert_empty(ticket_form_with_conditions.ticket_field_conditions)
  end
end
