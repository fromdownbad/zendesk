require_relative "../support/test_helper"

SingleCov.covered!

describe JiraIssue do
  should_validate_presence_of :issue_id, :account

  describe ".find_from_params" do
    it "does not raise when the issue_id key is a symbol" do
      JiraIssue.find_from_params(issue_id: "issue-43")
    end

    it "does not raise when the issue_id key is a string" do
      JiraIssue.find_from_params("issue_id" => "issue-43")
    end

    it "calls find_by_issue_id with the specified issue_id value" do
      params = {issue_id: "foo-87"}
      JiraIssue.expects(:find_by_issue_id).with(params[:issue_id])

      JiraIssue.find_from_params(params)
    end
  end
end
