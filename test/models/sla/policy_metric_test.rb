require_relative '../../support/test_helper'
require_relative '../../support/business_hours_test_helper'
require_relative '../../support/ticket_metric_helper'

SingleCov.covered!

describe Sla::PolicyMetric do
  include BusinessHoursTestHelper
  include TicketMetricHelper

  fixtures :accounts

  let(:account)  { accounts(:minimum) }
  let(:schedule) do
    account.schedules.create(name: 'Schedule', time_zone: account.time_zone)
  end

  let(:policy) { new_sla_policy }

  should_validate_presence_of :account,
    :policy,
    :priority_id

  before do
    account.stubs(:business_hours_active?).returns(true)

    set_time_zone(schedule, 'UTC')

    add_schedule_intervals(
      schedule,
      [:mon, '09:00', '17:00'],
      [:tue, '09:00', '15:00'],
      [:wed, '10:00', '16:30'],
      [:thu, '09:00', '17:00'],
      [:fri, '04:00', '14:30']
    )
  end

  describe 'scopes' do
    before do
      @policy_metric_1 = new_sla_policy_metric(
        policy:      policy,
        priority_id: PriorityType.LOW,
        metric_id:   Zendesk::Sla::TicketMetric::FirstReplyTime.id,
        target:      10
      )
      @policy_metric_2 = new_sla_policy_metric(
        policy:      policy,
        priority_id: PriorityType.LOW,
        metric_id:   Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
        target:      10
      )
      @policy_metric_3 = new_sla_policy_metric(
        policy:      policy,
        priority_id: PriorityType.NORMAL,
        metric_id:   Zendesk::Sla::TicketMetric::FirstReplyTime.id,
        target:      10
      )
      @policy_metric_4 = new_sla_policy_metric(
        policy:      policy,
        priority_id: PriorityType.NORMAL,
        metric_id:   Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
        target:      10
      )
    end

    describe 'with_priority' do
      let(:scoped_policy_metrics) do
        policy.policy_metrics.with_priority(PriorityType.LOW)
      end

      it 'returns metrics for the provided priority' do
        assert_equal(
          [@policy_metric_1, @policy_metric_2],
          scoped_policy_metrics.sort_by(&:metric_id)
        )
      end
    end
  end

  describe 'validations' do
    let(:policy_metric) { Sla::PolicyMetric.new }

    describe 'business hours' do
      [true, false].each do |boolean|
        describe "when set to #{boolean}" do
          before do
            policy_metric.tap { |pm| pm.business_hours = boolean }.valid?
          end

          it 'is valid' do
            refute policy_metric.errors.include?(:business_hours)
          end
        end
      end

      describe 'when set to nil' do
        before do
          refute policy_metric.tap { |pm| pm.business_hours = nil }.valid?
        end

        it 'is invalid' do
          assert policy_metric.errors.include?(:business_hours)
        end
      end
    end

    describe 'priority' do
      describe 'when set to' do
        before do
          policy_metric.tap { |pm| pm.priority_id = priority_id }.valid?
        end

        describe 'nil' do
          let(:priority_id) { nil }

          it 'is invalid' do
            assert policy_metric.errors.include?(:priority_id)
          end
        end

        describe 'a string' do
          let(:priority_id) { 'test' }

          it 'is invalid' do
            assert policy_metric.errors.include?(:priority_id)
          end
        end

        describe 'an invalid integer' do
          let(:priority_id) { -1 }

          it 'is invalid' do
            assert policy_metric.errors.include?(:priority_id)
          end
        end

        describe 'an invalid priority' do
          let(:priority_id) { PriorityType.find('-') }

          it 'is invalid' do
            assert policy_metric.errors.include?(:priority_id)
          end
        end

        describe 'a valid priority' do
          let(:priority_id) { PriorityType.NORMAL }

          it 'is valid' do
            refute policy_metric.errors.include?(:priority_id)
          end
        end
      end
    end

    describe 'metric' do
      describe 'when set to' do
        before do
          policy_metric.tap { |pm| pm.metric_id = metric_id }.valid?
        end

        describe 'nil' do
          let(:metric_id) { nil }

          it 'is invalid' do
            assert policy_metric.errors.include?(:metric_id)
          end
        end

        describe 'a string' do
          let(:metric_id) { 'test' }

          it 'is invalid' do
            assert policy_metric.errors.include?(:metric_id)
          end
        end

        describe 'an invalid integer' do
          let(:metric_id) { -1 }

          it 'is invalid' do
            assert policy_metric.errors.include?(:metric_id)
          end
        end

        describe 'a valid integer' do
          let(:metric_id) { Zendesk::Sla::TicketMetric::RequesterWaitTime.id }

          it 'is valid' do
            refute policy_metric.errors.include?(:metric_id)
          end
        end
      end
    end

    describe 'target validation' do
      describe 'when set to' do
        before { policy_metric.tap { |pm| pm.target = target }.valid? }

        describe 'nil' do
          let(:target) { nil }

          it 'is invalid' do
            assert policy_metric.errors.include?(:target)
          end
        end

        describe 'a float' do
          let(:target) { 1.23 }

          it 'is invalid' do
            assert policy_metric.errors.include?(:target)
          end
        end

        describe 'a negative integer' do
          let(:target) { -1 }

          it 'is invalid' do
            assert policy_metric.errors.include?(:target)
          end
        end

        describe 'zero' do
          let(:target) { 0 }

          it 'is invalid' do
            assert policy_metric.errors.include?(:target)
          end
        end

        describe 'a positive integer' do
          let(:target) { 1 }

          it 'is valid' do
            refute policy_metric.errors.include?(:target)
          end
        end
      end
    end
  end

  describe '#metric' do
    let(:policy_metric) do
      Sla::PolicyMetric.new do |pm|
        pm.metric_id = Zendesk::Sla::TicketMetric::RequesterWaitTime.id
      end
    end

    it 'returns the associated ticket metric class' do
      assert_equal(
        Zendesk::Sla::TicketMetric::RequesterWaitTime,
        policy_metric.metric
      )
    end
  end

  describe '#priority' do
    let(:policy_metric) do
      Sla::PolicyMetric.new do |pm|
        pm.priority_id = PriorityType.LOW
      end
    end

    it 'returns the associated ticket metric name' do
      assert_equal PriorityType[PriorityType.LOW], policy_metric.priority
    end
  end

  describe '#to_audit' do
    let(:policy_metric) do
      Sla::PolicyMetric.new do |pm|
        pm.target         = 50
        pm.business_hours = true
      end
    end

    it 'returns audit-formatted data' do
      assert_equal(
        {
          minutes:           policy_metric.target,
          in_business_hours: policy_metric.business_hours?
        },
        policy_metric.to_audit
      )
    end
  end

  describe '#equivalent?' do
    let(:policy_metric_1) do
      Sla::PolicyMetric.new do |pm|
        pm.sla_policy_id  = 1
        pm.metric_id      = 1
        pm.target         = 120
        pm.business_hours = true
      end
    end
    let(:policy_metric_2) do
      Sla::PolicyMetric.new do |pm|
        pm.sla_policy_id  = policy_metric_1.sla_policy_id
        pm.metric_id      = policy_metric_1.metric_id
        pm.target         = policy_metric_1.target
        pm.business_hours = policy_metric_1.business_hours
      end
    end

    describe 'when the policy metrics are equivalent' do
      it 'returns true' do
        assert policy_metric_1.equivalent?(policy_metric_2)
      end
    end

    describe 'when the associated policy is not the same' do
      before { policy_metric_2.sla_policy_id = 2 }

      it 'returns false' do
        refute policy_metric_1.equivalent?(policy_metric_2)
      end
    end

    describe 'when the metrics are not the same' do
      before { policy_metric_2.metric_id = 2 }

      it 'returns false' do
        refute policy_metric_1.equivalent?(policy_metric_2)
      end
    end

    describe 'when the targets are not the same' do
      before { policy_metric_2.target = 60 }

      it 'returns false' do
        refute policy_metric_1.equivalent?(policy_metric_2)
      end
    end

    describe 'when the `business_hours` property is not the same' do
      before { policy_metric_2.business_hours = false }

      it 'returns false' do
        refute policy_metric_1.equivalent?(policy_metric_2)
      end
    end
  end
end
