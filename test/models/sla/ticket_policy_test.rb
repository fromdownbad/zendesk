require_relative '../../support/test_helper'
require_relative '../../support/ticket_metric_helper'

SingleCov.covered!

describe Sla::TicketPolicy do
  include TicketMetricHelper

  fixtures :accounts, :tickets

  let(:ticket)  { tickets(:minimum_1) }
  let(:account) { ticket.account }
  let(:policy)  { new_sla_policy }

  let(:ticket_policy) { ticket.sla_ticket_policy }

  should_validate_presence_of :account,
    :ticket,
    :policy

  before do
    new_sla_policy_metric(
      policy:      policy,
      priority_id: ticket.priority_id,
      metric_id:   Zendesk::Sla::TicketMetric::FirstReplyTime.id,
      target:      10
    )

    new_sla_policy_metric(
      policy:      policy,
      priority_id: ticket.priority_id,
      metric_id:   Zendesk::Sla::TicketMetric::AgentWorkTime.id,
      target:      10
    )

    ticket.create_sla_ticket_policy(account: account, policy: policy)
  end

  describe '.upsert_sql' do
    it 'returns upserting SQL' do
      assert_match(/ON DUPLICATE KEY UPDATE/, Sla::TicketPolicy.upsert_sql)
    end
  end

  describe '#null_policy?' do
    describe 'when a policy is assigned' do
      it 'returns false' do
        refute ticket_policy.null_policy?
      end
    end

    describe 'when the null policy is assigned' do
      before do
        ticket.sla_ticket_policy.destroy

        ticket.build_sla_ticket_policy(account: account).tap do |ticket_policy|
          ticket_policy.sla_policy_id = Zendesk::Sla::NullPolicy.id

          ticket_policy.save!
        end
      end

      it 'returns true' do
        assert ticket_policy.null_policy?
      end
    end
  end

  describe '#policy' do
    describe 'when a policy is assigned' do
      it 'returns that policy' do
        assert_equal policy, ticket_policy.policy
      end
    end

    describe 'when the null policy is assigned' do
      before do
        ticket.sla_ticket_policy.destroy

        ticket.build_sla_ticket_policy(account: account).tap do |ticket_policy|
          ticket_policy.sla_policy_id = Zendesk::Sla::NullPolicy.id

          ticket_policy.save!
        end
      end

      it 'returns the null policy' do
        assert_equal Zendesk::Sla::NullPolicy, ticket.sla_ticket_policy.policy
      end
    end
  end

  describe '#policy_metrics' do
    it 'returns the priority metrics with the associated ticket priority' do
      assert_equal(
        policy.policy_metrics.with_priority(ticket.priority_id).to_a,
        ticket_policy.policy_metrics.to_a
      )
    end
  end

  describe '#current_statuses' do
    let(:policy_1) { new_sla_policy }
    let(:policy_2) { new_sla_policy(position: 2, tags: 'match_new_sla') }

    let(:end_user) { users(:minimum_end_user) }
    let(:agent)    { users(:minimum_agent) }

    before do
      ticket.events.delete_all
      ticket.metric_events.delete_all

      Account.any_instance.stubs(has_service_level_agreements?: true)

      ticket.update_column(:created_at, Time.utc(2015))

      Account.any_instance.stubs(use_status_hold?: true)

      (1..6).each do |metric_id|
        new_sla_policy_metric(
          policy:         policy_1,
          metric_id:      metric_id,
          target:         7,
          priority_id:    PriorityType.NORMAL
        )
        new_sla_policy_metric(
          policy:         policy_1,
          metric_id:      metric_id,
          target:         20,
          priority_id:    PriorityType.HIGH
        )
        new_sla_policy_metric(
          policy:         policy_2,
          metric_id:      metric_id,
          target:         17,
          priority_id:    PriorityType.HIGH
        )
      end

      measure_metrics(
        :agent_work_time,
        :pausable_update_time,
        :periodic_update_time,
        :reply_time,
        :requester_wait_time
      )

      tags_updated(after: 0.minutes, to: 'match_sla')
      status_updated(after: 0.minutes, to: StatusType.OPEN)
      comments_added([{after: 0.minutes, by: end_user}])
      priority_updated(after: 6.minutes, to: PriorityType.HIGH)
      status_updated(after: 10.minutes, to: StatusType.PENDING)
      status_updated(after: 20.minutes, to: StatusType.HOLD)
      tags_updated(after: 25.minutes, to: 'match_new_sla')
      status_updated(after: 29.minutes, to: StatusType.OPEN)
      comments_added([{after: 40.minutes, by: agent}])
      status_updated(after: 60.minutes, to: StatusType.SOLVED)
      status_updated(after: 70.minutes, to: StatusType.OPEN)
      comments_added([{after: 70.minutes, by: end_user}])
      status_updated(after: 80.minutes, to: StatusType.SOLVED)
    end

    it 'returns statuses for the assigned policy' do
      assert(
        ticket_policy.current_statuses.all? do |status|
          status.policy_metric.policy == policy_2
        end
      )
    end

    it 'returns statuses for the current priority' do
      assert(
        ticket_policy.current_statuses.all? do |status|
          status.policy_metric.priority_id == ticket.priority_id
        end
      )
    end

    it 'returns statuses for all tracked metrics' do
      assert_equal(
        (2..6).to_a,
        ticket_policy.current_statuses.map do |status|
          status.policy_metric.metric_id
        end.sort
      )
    end

    describe 'when include_first_reply_time is true' do
      describe 'and a first_reply_time metric exists' do
        it 'returns statuses for all tracked metrics including first_reply_time' do
          assert(
            ticket_policy.current_statuses.all? do |status|
              status.policy_metric.policy == policy_2
            end
          )
        end
      end

      describe 'and a first reply time metric does not exist' do
        before do
          Zendesk::Sla::MetricStatus.any_instance.stubs(first_policy_metric: nil)
        end

        it 'returns statuses for all other tracked metrics' do
          assert_equal(
            (2..6).to_a,
            ticket_policy.current_statuses(true).map do |status|
              status.policy_metric.metric_id
            end.sort
          )
        end
      end
    end
  end

  describe '#cache_statuses' do
    before do
      ticket.metric_events.agent_work_time.activate.create(
        account: ticket.account,
        time:    ticket.created_at
      )
      TicketMetric::ApplySla.create(
        account:     ticket.account,
        ticket:      ticket.reload,
        metric:      'agent_work_time',
        time:        ticket.created_at
      )

      ticket.metric_events.reply_time.activate.create(
        account:     ticket.account,
        instance_id: 1,
        time:        ticket.created_at
      )
      TicketMetric::ApplySla.create(
        account:     ticket.account,
        ticket:      ticket.reload,
        metric:      'reply_time',
        instance_id: 1,
        time:        ticket.created_at
      )

      ticket_policy.cache_statuses
    end

    it 'builds a status for each relevant policy metric using events' do
      assert_equal(
        ticket.metric_events.apply_sla.count,
        ticket_policy.statuses.map(&:policy_metric).count
      )
    end

    it 'persists the statuses' do
      assert ticket_policy.statuses.all?(&:persisted?)
    end
  end
end
