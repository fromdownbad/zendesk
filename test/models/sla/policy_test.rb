require_relative '../../support/test_helper'
require_relative '../../support/ticket_metric_helper'

SingleCov.covered!

describe Sla::Policy do
  include TicketMetricHelper

  fixtures :accounts,
    :tickets,
    :users

  let(:account)         { accounts(:minimum) }
  let(:policy)          { new_sla_policy }
  let(:inactive_policy) { new_sla_policy.tap(&:soft_delete) }

  before { account.sla_policies.destroy_all }

  describe 'when deleted' do
    describe 'and the `service_level_agreements` feature is enabled' do
      before do
        policy.account.stubs(has_service_level_agreements?: true)

        CIA.audit(actor: account.owner) { policy.soft_delete }
      end

      it 'is audited' do
        assert policy.cia_events.where(action: 'destroy').any?
      end
    end

    describe 'and the `service_level_agreements` feature is not enabled' do
      before do
        policy.account.stubs(has_service_level_agreements?: false)

        CIA.audit(actor: account.owner) { policy.soft_delete }
      end

      it 'is not audited' do
        assert policy.cia_events.where(action: 'destroy').none?
      end
    end
  end

  describe 'default scope' do
    before do
      new_sla_policy(position: 2).soft_delete
      new_sla_policy(position: 1)

      account.reload
    end

    it 'does not exclude deleted policies' do
      assert account.sla_policies.any?(&:deleted?)
    end

    it 'sorts by position' do
      assert_equal [1, 2], account.sla_policies.map(&:position)
    end

    describe 'and there are policies with same position' do
      before do
        new_sla_policy(position: 2)
        new_sla_policy(position: 1)
        new_sla_policy(position: 1)
      end

      it 'sorts policies with same position by `id`' do
        assert_equal(
          account.sla_policies.unscoped.where(position: 1).pluck(:id).sort,
          account.sla_policies.where(position: 1).pluck(:id)
        )
      end
    end
  end

  describe '.active' do
    it 'excludes deleted policies' do
      assert account.sla_policies.active.none?(&:deleted?)
    end
  end

  describe '.ordered' do
    before do
      new_sla_policy(position: 2)
      new_sla_policy(position: 1)
    end

    it 'returns policies ordered by position' do
      assert_equal [1, 2], account.sla_policies.ordered.map(&:position)
    end
  end

  describe '.update_position' do
    let(:policies) { (1..3).map { |i| new_sla_policy(position: i) } }

    before do
      @sql_queries = sql_queries do
        account.sla_policies.update_position(policies.map(&:id).reverse)
      end
    end

    it 'updates position based on the provided IDs' do
      assert_equal(
        policies.map(&:id).reverse,
        account.sla_policies.ordered.pluck(:id)
      )
    end

    it 'does not generate N + 1 queries' do
      assert_sql_queries 1, /UPDATE `sla_policies`/
    end
  end

  describe 'validation' do
    describe 'position' do
      describe 'presence' do
        describe 'when not present' do
          it 'is invalid' do
            assert_raises ActiveRecord::RecordInvalid do
              new_sla_policy(position: nil)
            end
          end
        end

        describe 'when present' do
          it 'is valid' do
            assert new_sla_policy(position: 1).valid?
          end
        end
      end

      describe 'numericality' do
        describe 'when not an integer' do
          it 'is invalid' do
            assert_raises ActiveRecord::RecordInvalid do
              new_sla_policy(position: 'foo')
            end
          end
        end

        describe 'when an integer' do
          it 'is valid' do
            assert new_sla_policy(position: 1).valid?
          end
        end
      end
    end

    describe 'filter' do
      describe 'when invalid' do
        it 'is invalid' do
          assert_raises ActiveRecord::RecordInvalid do
            new_sla_policy(
              conditions_all: [
                {
                  field:    'status_id',
                  operator: 'less_than',
                  value:    StatusType.NEW
                }
              ]
            )
          end
        end
      end

      describe 'when too large' do
        it 'is invalid' do
          assert_raises ActiveRecord::RecordInvalid do
            new_sla_policy(
              conditions_all: [
                {
                  field:    'current_tags',
                  operator: 'includes',
                  value:    '.' * 70_000
                }
              ]
            )
          end
        end
      end

      describe 'when valid' do
        it 'is valid' do
          assert(
            new_sla_policy(
              conditions_all: [
                {
                  field:    'current_tags',
                  operator: 'includes',
                  value:    'blah'
                }
              ]
            ).valid?
          )
        end
      end
    end

    describe 'policy metrics' do
      describe 'when an associated policy metric is invalid' do
        before do
          policy.policy_metrics.build do |pm|
            pm.account        = account
            pm.priority_id    = PriorityType.NORMAL
            pm.metric_id      = Zendesk::Sla::TicketMetric::RequesterWaitTime.id
            pm.target         = -100
            pm.business_hours = false
          end
        end

        it 'is invalid' do
          refute policy.valid?
        end
      end

      describe 'when there are duplicate associated policy metrics' do
        before do
          2.times do
            policy.policy_metrics.build do |pm|
              pm.account        = account
              pm.priority_id    = PriorityType.NORMAL
              pm.metric_id      =
                Zendesk::Sla::TicketMetric::RequesterWaitTime.id
              pm.target         = 100
              pm.business_hours = false
            end
          end
        end

        it 'is invalid' do
          refute policy.valid?
        end
      end

      describe 'when there are no associated policy metrics' do
        before { policy.policy_metrics.destroy_all }

        it 'is valid' do
          assert policy.valid?
        end
      end
    end

    describe 'limit' do
      describe 'and the number of account policies is under the limit' do
        describe 'and a policy is created' do
          it 'is valid' do
            assert new_sla_policy(position: 1).valid?
          end
        end
      end

      describe 'and the number of account policies is beyond the limit' do
        let!(:original_limit) { Sla::Policy::SLA_POLICIES_LIMIT }

        before do
          new_sla_policy(position: 3)

          Sla::Policy.const_set(:SLA_POLICIES_LIMIT, 1)
        end

        after do
          Sla::Policy.const_set(:SLA_POLICIES_LIMIT, original_limit)
        end

        describe 'and a policy is created' do
          it 'is invalid' do
            assert_raises Sla::Policy::SlaPolicyLimitExceededError do
              new_sla_policy(position: 4)
            end
          end
        end

        describe 'and a policy is updated from inactive to active' do
          it 'is invalid' do
            assert_raises Sla::Policy::SlaPolicyLimitExceededError do
              inactive_policy.update_attributes!(deleted_at: nil)
            end
          end
        end
      end
    end
  end

  describe '#title' do
    describe 'when changed' do
      before do
        CIA.audit(actor: account.owner) do
          policy.update_attributes!(title: 'New title')
        end
      end

      it 'is audited' do
        assert policy.cia_attribute_changes.where(attribute_name: :title).any?
      end
    end
  end

  describe '#description' do
    describe 'when changed' do
      before do
        CIA.audit(actor: account.owner) do
          policy.update_attributes!(description: 'New description')
        end
      end

      it 'is audited' do
        assert(
          policy.cia_attribute_changes.where(attribute_name: :description).any?
        )
      end
    end
  end

  describe '#position' do
    describe 'when changed' do
      before do
        CIA.audit(actor: account.owner) do
          policy.update_attributes!(position: 2)
        end
      end

      it 'is audited' do
        assert(
          policy.cia_attribute_changes.where(attribute_name: :position).any?
        )
      end
    end
  end

  describe '#match?' do
    let(:ticket) { tickets(:minimum_1) }

    let(:policy) do
      new_sla_policy(
        conditions_all: [
          {field: 'current_tags', operator: 'includes', value: 'match'}
        ]
      )
    end

    describe 'when a ticket matches the filter' do
      before { ticket.current_tags = 'match' }

      it 'returns true' do
        assert policy.match?(ticket)
      end
    end

    describe 'when the ticket does not match the filter' do
      before { ticket.current_tags = 'no_match' }

      it 'returns false' do
        refute policy.match?(ticket)
      end
    end
  end

  describe '#definition' do
    it 'returns the filter' do
      assert_equal policy.filter, policy.definition
    end
  end

  describe '#priority_metric_map' do
    before do
      @normal_rwt = new_sla_policy_metric(
        policy:    policy,
        priority_id: PriorityType.NORMAL,
        metric_id: Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
        target:    100
      )
      @normal_frt = new_sla_policy_metric(
        policy:      policy,
        priority_id: PriorityType.NORMAL,
        metric_id:   Zendesk::Sla::TicketMetric::FirstReplyTime.id,
        target:      100
      )

      @urgent_awt = new_sla_policy_metric(
        policy:      policy,
        priority_id: PriorityType.URGENT,
        metric_id:   Zendesk::Sla::TicketMetric::AgentWorkTime.id,
        target:      100
      )
    end

    it 'returns the mapped metrics' do
      assert_equal(
        {
          Zendesk::Sla::TicketMetric::RequesterWaitTime.id => @normal_rwt,
          Zendesk::Sla::TicketMetric::FirstReplyTime.id    => @normal_frt
        },
        policy.priority_metric_map(PriorityType.NORMAL)
      )
    end
  end

  describe '#active?' do
    describe 'when the policy has been deleted' do
      before { policy.soft_delete }

      it 'returns false' do
        refute policy.active?
      end
    end

    describe 'when the policy has not been deleted' do
      it 'returns true' do
        assert policy.active?
      end
    end
  end

  describe '#rule_type' do
    it 'returns the rule type' do
      assert_equal 'sla/policy', policy.rule_type
    end
  end
end
