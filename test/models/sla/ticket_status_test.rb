require_relative '../../support/test_helper'
require_relative '../../support/ticket_metric_helper'

SingleCov.covered!

describe Sla::TicketStatus do
  include TicketMetricHelper

  fixtures :accounts,
    :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }
  let(:policy)  { new_sla_policy }

  let(:ticket_policy) do
    ticket.create_sla_ticket_policy(
      ticket:  ticket,
      account: account,
      policy:  policy
    )
  end

  let(:policy_metric) do
    new_sla_policy_metric(
      policy:      policy,
      priority_id: ticket.priority_id,
      metric_id:   Zendesk::Sla::TicketMetric::FirstReplyTime.id,
      target:      20
    )
  end

  let(:ticket_status) do
    ticket_policy.statuses.create(
      account:       account,
      ticket_policy: ticket_policy,
      policy_metric: policy_metric
    )
  end

  describe 'when sorting' do
    let(:status_1) do
      ticket_policy.statuses.build(
        {
          ticket_policy: ticket_policy,
          policy_metric: policy_metric
        }.merge(status_1_attributes)
      )
    end
    let(:status_2) do
      ticket_policy.statuses.build(
        {
          ticket_policy: ticket_policy,
          policy_metric: policy_metric
        }.merge(status_2_attributes)
      )
    end

    let(:statuses) { [status_1, status_2] }

    describe 'when both metrics have an active breach' do
      let(:status_1_attributes) { {breach_at: Time.new(2015)} }
      let(:status_2_attributes) { {breach_at: Time.new(2014)} }

      it 'sorts the metric with the earlier breach time first' do
        assert_equal [status_2, status_1], statuses.sort
      end
    end

    describe 'when one metric has an active breach' do
      let(:status_1_attributes) { {fulfilled_at: Time.new(2014)} }
      let(:status_2_attributes) { {breach_at: Time.new(2015)} }

      it 'sorts that metric first' do
        assert_equal [status_2, status_1], statuses.sort
      end
    end

    describe 'when neither metric has an active breach' do
      let(:status_1_attributes) { {fulfilled_at: Time.new(2015)} }
      let(:status_2_attributes) { {fulfilled_at: Time.new(2014)} }

      it 'does not change the sort order' do
        assert_equal [status_1, status_2], statuses.sort
      end
    end

    describe 'when the paused metric is more breached than the active metric' do
      let(:status_1_attributes) { {paused: true,  breach_at: Time.new(2014)} }
      let(:status_2_attributes) { {paused: false, breach_at: Time.new(2015)} }

      it 'sorts the active metric first' do
        assert_equal [status_2, status_1], statuses.sort
      end
    end
  end

  describe '#ticket' do
    it 'delegates to the ticket policy' do
      assert_equal ticket_policy.ticket, ticket_status.ticket
    end

    describe 'when the ticket policy has been deleted in the meantime' do
      before do
        ticket_policy.statuses.create(
          account:       account,
          ticket_policy: ticket_policy,
          policy_metric: policy_metric
        )

        @ticket_status = ticket.sla_ticket_policy.statuses.first

        ticket.sla_ticket_policy.destroy
      end

      it 'does not blow up' do
        assert @ticket_status.ticket_policy.present?
      end
    end
  end

  describe '#name' do
    it 'returns the name of the associated metric' do
      assert_equal policy_metric.metric.name, ticket_status.name
    end
  end

  describe '#policy_metric' do
    it 'returns the policy metric' do
      assert_equal policy_metric, ticket_status.policy_metric
    end

    describe 'when the policy metric has been soft-deleted' do
      before do
        policy_metric.soft_delete

        ticket_status.reload
      end

      it 'returns the policy metric' do
        assert_equal policy_metric, ticket_status.policy_metric
      end
    end
  end

  describe '#measured?' do
    it 'returns true' do
      assert ticket_status.measured?
    end
  end

  describe '#breached?' do
    before { Timecop.freeze }

    describe 'when the `breach_at` time is in the past' do
      before { ticket_status.breach_at = Time.now - 1.minute }

      it 'returns true' do
        assert ticket_status.breached?
      end
    end

    describe 'when the `breach_at` time is in the future' do
      before { ticket_status.breach_at = Time.now + 1.minute }

      it 'returns false' do
        refute ticket_status.breached?
      end
    end

    describe 'when there is not a `breach_at` time' do
      before { ticket_status.breach_at = nil }

      it 'returns false' do
        refute ticket_status.breached?
      end
    end
  end

  describe '#fulfilled?' do
    describe 'when there is a fulfillment time' do
      before { ticket_status.fulfilled_at = Time.now }

      it 'returns true' do
        assert ticket_status.fulfilled?
      end
    end

    describe 'when there is not a fulfillment time' do
      before { ticket_status.fulfilled_at = nil }

      it 'returns false' do
        refute ticket_status.fulfilled?
      end
    end
  end

  describe '#stage' do
    it 'returns the stage of the metric' do
      assert_equal(
        Zendesk::Sla::Stage.for(ticket_status).name,
        ticket_status.stage.name
      )
    end
  end
end
