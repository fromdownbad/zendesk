require_relative "../support/test_helper"

SingleCov.covered! uncovered: 12

describe Address do
  fixtures :accounts, :subscriptions
  should_validate_presence_of :account, :name, :phone

  it "defaults its country to NULL" do
    assert_nil Address.new.country
  end

  it 'normalizes its website URL before save' do
    normalized_url = 'http://foobar.com'
    a = Address.new
    UrlNormalizer.stubs(:normalize).returns(normalized_url)
    a.save
    assert_equal normalized_url, a.website_url
  end

  it "validates phone number even if its 'UNKNOWN' or '-' during trial subscription " do
    a = Address.new(name: 'test')
    subscription = subscriptions(:trial)
    a.account = subscription.account
    a.phone = 'UNKNOWN'
    assert a.valid?
    a.phone = '-'
    assert a.valid?

    # subscription = subscriptions # make it a normal subscription and not a trial. Change to !valid? when the constraints have been put in.
    a.phone = 'UNKNOWN'
    assert a.valid?
    a.phone = '-'
    assert a.valid?
  end

  describe "#country=" do
    before do
      @address = Address.new
    end

    describe "with a String argument" do
      describe "that is a valid country code" do
        before do
          @country_code = "DK"
          @address.country = @country_code
          @address.account = accounts(:minimum)
          @address.name    = "Mr. Viking"
          @address.phone   = "123456"
          @address.save!
          @address.reload
        end

        it "sets to the country with a matching code" do
          assert_equal Country.find_by_code(@country_code), @address.country
        end
      end

      describe "that is an invalid country code" do
        before do
          @country_code = "XX"
          @address.country = @country_code
        end

        it "sets the country to nil" do
          assert_nil @address.country
        end
      end

      describe "that is an empty country code" do
        before do
          @address.country = ""
        end

        it "sets the country to nil" do
          assert_nil @address.country
        end
      end
    end
  end
end
