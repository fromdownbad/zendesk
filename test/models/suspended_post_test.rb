require_relative "../support/test_helper"

SingleCov.covered!

describe 'SuspendedPost' do
  fixtures :posts, :users, :entries, :accounts

  describe "Given an account and an entry with a post" do
    let(:account) { accounts(:minimum) }
    let(:entry) { entries(:sticky) }
    let(:user) do
      users(:minimum_end_user).tap do |user|
        user.stubs(whitelisted_from_moderation?: false)
      end
    end

    it "has an account without suspended posts" do
      Post.with_deleted do
        posts = account.posts.
          where('posts.deleted_at IS NOT NULL').
          where(flag_type_id: PostFlagType::SUSPENDED.id)

        assert_empty(posts)
      end
    end

    describe "when entry has no suspended posts" do
      it "has some visible posts" do
        assert entry.posts.any?
      end

      describe ".for_account" do
        it "returns empty" do
          assert_empty(SuspendedPost.for_account(account))
        end
      end

      describe ".authored_by" do
        it "returns empty" do
          assert_empty(SuspendedPost.authored_by(account.users))
        end
      end
    end

    describe "when account has moderated posts" do
      before do
        # Turn on moderation for account
        account.settings.moderated_posts = true
        account.settings.save!

        # The following will create a suspended post.
        @post = entry.posts.create!(body: "test", user: user)
      end

      it "has suspended post" do
        assert @post.suspended?
      end

      describe ".for_account" do
        it "returns post" do
          assert SuspendedPost.for_account(account) == [@post]
        end
      end

      describe ".authored_by" do
        it "returns post" do
          assert SuspendedPost.authored_by(account.users) == [@post]
        end
      end

      describe "multi-tenancy check" do
        let (:other_account) { accounts(:support) }

        let(:other_user) do
          users(:support_report_user).tap do |user|
            user.stubs(whitelisted_from_moderation?: false)
          end
        end

        let (:other_forum) do
          Forum.create(
            account: other_account,
            name: "bar",
            display_type_id: ForumDisplayType::QUESTIONS.id,
            visibility_restriction_id: VisibilityRestriction::EVERYBODY.id
          )
        end

        let (:other_entry) do
          attrs = { title: "other", text: "test", account: other_account }
          other_forum.entries.create!(attrs) { |e| e.current_user = other_user }
        end

        before do
          # Turn on moderation for other_account
          other_account.settings.moderated_posts = true
          other_account.settings.save!

          # The following will create a suspended post.
          @other_post = entry.posts.create!(body: "test", user: other_user)
        end

        it "has suspended post" do
          assert @other_post.suspended?
        end

        describe ".for_account" do
          it "does not return post belonging to a different account" do
            refute SuspendedPost.for_account(other_account).include?(@post)
          end
        end

        describe ".submitted_by" do
          it "does not return post authored by users on other account" do
            refute SuspendedPost.authored_by(other_account.users).include?(@post)
          end
        end
      end
    end
  end
end
