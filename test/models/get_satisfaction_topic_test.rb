require_relative "../support/test_helper"

SingleCov.covered!

describe GetSatisfactionTopic do
  describe "A GetSatisfaction Topic" do
    should_validate_presence_of :topic_id, :account
  end

  describe ".find_from_params" do
    it "does not raise when the topic_id key is a symbol" do
      GetSatisfactionTopic.find_from_params(topic_id: "123")
    end

    it "does not raise when the topic_id key is a string" do
      GetSatisfactionTopic.find_from_params("topic_id" => "topic_id_tag")
    end

    it "calls find_by_issue_id with the specified issue_id value" do
      params = {topic_id: "topic_id_tag"}
      GetSatisfactionTopic.expects(:find_by_topic_id).with(params[:topic_id])

      GetSatisfactionTopic.find_from_params(params)
    end
  end
end
