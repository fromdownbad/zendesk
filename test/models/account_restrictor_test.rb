require_relative "../support/test_helper"

SingleCov.covered!

describe 'AccountRestrictor' do
  fixtures :accounts

  describe ".deny?" do
    before do
      @account = accounts(:minimum)
      @wrong_account = accounts(:multiproduct)

      @record_class = stub("record_class", is_sharded?: true)
      @record = stub("record", class: @record_class)
      @record.stubs(:read_attribute).with(:account_id).returns(42)

      @restrictor = AccountRestrictor

      Zendesk.stubs(:current_account).returns(@account)
    end

    it "returns true if the record is on the wrong account" do
      @record.stubs(:read_attribute).with(:account_id).returns(@wrong_account.id)

      assert @restrictor.deny?(@record)
    end

    it "returns true if the record's account cannot be found" do
      @record.stubs(:read_attribute).with(:account_id).returns(9999)

      assert @restrictor.deny?(@record)
    end

    it "returns false if the record is on the right account" do
      @record.stubs(:read_attribute).with(:account_id).returns(@account.id)

      refute @restrictor.deny?(@record)
    end

    it "returns false if the record does not have an attribute #account_id" do
      @record.stubs(:read_attribute).with(:account_id).returns(nil)

      refute @restrictor.deny?(@record)
    end

    it "returns false if Zendesk.current_account is nil" do
      Zendesk.stubs(:current_account).returns(nil)
      @record.stubs(:read_attribute).with(:account_id).returns(@wrong_account.id)

      refute @restrictor.deny?(@record)
    end

    it "returns false if the account is the system account" do
      @record.stubs(:read_attribute).with(:account_id).returns(-1) # system account id

      refute @restrictor.deny?(@record)
    end

    describe "record_class is in AccountRestrictor::SKIPPED_MODELS" do
      AccountRestrictor::SKIPPED_MODELS.each do |model|
        it "returns false if the record is a #{model} and does not call right_account?" do
          @record = stub("record", class: model)

          @record.expects(:right_account?).never
          refute @restrictor.deny?(@record)
        end
      end
    end

    describe "when updating the sandbox" do
      before do
        @sandbox_for_this_account  = stub(id: 101)
        @sandbox_for_other_account = stub(id: 102)
      end

      it "returns true when the sandbox is not associated with the current account" do
        Zendesk.current_account.
          expects(:is_sandbox_master_of?).
          with(@sandbox_for_other_account.id).
          returns(false)

        @record.stubs(:read_attribute).with(:account_id).returns(@sandbox_for_other_account.id)
        assert @restrictor.deny?(@record)
      end

      it "returns false when the sandbox record is associated with the current account" do
        Zendesk.current_account.
          expects(:is_sandbox_master_of?).
          with(@sandbox_for_this_account.id).
          returns(true)

        @record.stubs(:read_attribute).with(:account_id).returns(@sandbox_for_this_account.id)
        refute @restrictor.deny?(@record)
      end
    end
  end
end
