require_relative "../../support/test_helper"
require_relative "../../support/pre_account_creation_helper"

SingleCov.covered! uncovered: 7

describe Accounts::Precreation do
  include PreAccountCreationHelper
  fixtures :all

  describe "Creating an Account in production" do
    before do
      stub_request(:get, %r{https://minfraud.maxmind.com/app/ccv2r}).to_return(body: '{"risk_score":1}')
      Rails.env.stubs(:production?).returns(true)
    end

    it "trigger callbacks for the created account" do
      account = FactoryBot.build(:account)
      account.set_owner(name: 'Random Person', email: 'random.person@example.com')
      account.set_address(country_code: 'DK', phone: '123456')

      pre_account_creation_callbacks.each do |method|
        account.expects(method)
      end

      account.save!
    end
  end

  describe "support product creation" do
    let(:account) do
      account = FactoryBot.build(:account)
      account.set_owner(name: 'Random Person', email: 'random.person@example.com')
      account.set_address(country_code: 'DK', phone: '123456')
      account
    end

    describe 'with arturo enabled :orca_classic_replace_fraud_report_job_with_fraud_score_job' do
      before do
        Arturo.enable_feature!(:orca_classic_replace_fraud_report_job_with_fraud_score_job)
      end

      it 'executes the fraud score job' do
        FraudScoreJob.expects(:account_creation_enqueue).with(account, Fraud::SourceEvent::TRIAL_SIGNUP).once
        account.save!
      end
    end

    describe 'when synchronous support product creation succeeds' do
      it 'does not enqueue support activation job' do
        Zendesk::TrialActivation.expects(:create_support_product).once
        SupportProductCreationJob.expects(:enqueue).never
        account.save!
      end
    end

    describe 'when synchronous support product creation fails' do
      it 'enqueues support activation job' do
        Zendesk::TrialActivation.expects(:create_support_product).once.
          raises(StandardError)
        SupportProductCreationJob.expects(:enqueue).once
        account.save!
      end
    end
  end

  describe '#create_guide_plan' do
    describe 'on a precreated account' do
      it 'starts a guide trial' do
        Guide::CreateTrialJob.expects(:enqueue).once

        initialize_account.save!
      end

      describe "with arturo on guide_disable_auto_trial" do
        before do
          Arturo.enable_feature!(:guide_disable_auto_trial)
        end

        it 'doesnt start a guide trial' do
          Guide::CreateTrialJob.expects(:enqueue).never

          initialize_account.save!
        end
      end
    end

    describe 'on a non-precreated account' do
      before do
        PreAccountCreation.stubs(:precreation_supported?).returns(false)
      end

      it 'starts a guide trial' do
        Guide::CreateTrialJob.expects(:enqueue).once

        initialize_account.save!
      end
    end
  end

  describe '#generate_owner_welcome_email' do
    let(:subdomain) { 'userchoice' }

    it "sends mail to owner for email verification" do
      Zendesk::StatsD::Client.any_instance.expects(:increment).at_least(0)
      Zendesk::StatsD::Client.any_instance.expects(:increment).
        with("generate_owner_welcome_email", any_parameters)

      Rails.logger.expects(:info).at_least(0)
      Rails.logger.expects(:info).
        with(regexp_matches(/^About to generate owner welcome email, checking subdomains: .*#{subdomain}/))

      initialize_account(
        account: {
          subdomain: subdomain
        }
      ).save!
    end
  end
end
