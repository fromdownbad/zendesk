require_relative '../../support/test_helper'
require_relative '../../support/classic_test_helper'
require_relative '../../support/pre_account_creation_helper'

SingleCov.covered!

describe 'Accounts::Classic' do
  include ClassicTestHelper
  include PreAccountCreationHelper
  fixtures :all

  before do
    Resque.stubs(:enqueue).returns(true)
    @new_account = create_account_from_hash(default_account_hash)
  end

  describe "A new account with verified owner" do
    it "does send verification email" do
      AccountsMailer.expects(:deliver_welcome_account_owner).once
      @new_account = create_account_from_hash(default_account_hash, true, true)
      @new_account.save!
    end
  end

  describe "#set_settings_defaults" do
    describe "when has_end_user_ui? is true" do
      before do
        @new_account.expects(:has_end_user_ui?).returns(true)
        @new_account.send(:set_settings_defaults)
      end

      it "enables no_short_url_tweet_append enablesd" do
        assert @new_account.settings.no_short_url_tweet_append?
      end
    end

    describe "when has_end_user_ui? is false" do
      before do
        @new_account.expects(:has_end_user_ui?).returns(false)
        @new_account.send(:set_settings_defaults)
      end

      it "enables no_short_url_tweet_append enablesd" do
        refute @new_account.settings.no_short_url_tweet_append?
      end
    end
  end

  describe "#setup_remote_jwt_authentications" do
    subject { remote_authentications(:minimum_remote_authentication) }
    let(:account) { accounts(:minimum).becomes(Accounts::Classic) }

    describe "with jwt enabled" do
      before do
        account.stubs(has_jwt?: true)
      end

      describe "with a jwt" do
        before do
          subject.auth_mode = RemoteAuthentication::JWT
          subject.save!
        end

        describe "setup" do
          before { account.send(:setup_remote_jwt_authentications) }
          should_not_change("remote auth count") do
            account.remote_authentications.count(:all)
          end
        end
      end

      describe "without a jwt" do
        before do
          assert_equal 0, account.remote_authentications.active.jwt.count(:all)
        end

        describe "setup" do
          before { account.send(:setup_remote_jwt_authentications) }
          should_change("remote auth count", by: 1) do
            account.remote_authentications.count(:all)
          end
        end
      end
    end

    describe "with jwt disabled" do
      before do
        account.stubs(has_jwt?: false)
        assert_equal 0, account.remote_authentications.active.jwt.count(:all)
      end

      describe "setup" do
        before { account.send(:setup_remote_jwt_authentications) }
        should_not_change("remote auth count") do
          account.remote_authentications.count(:all)
        end
      end
    end
  end

  describe "#setup_remote_saml_authentications" do
    subject { remote_authentications(:minimum_remote_authentication) }
    let(:account) { accounts(:minimum).becomes(Accounts::Classic) }

    describe "with saml enabled" do
      before do
        account.stubs(has_saml?: true)
      end

      describe "and exists" do
        before do
          subject.auth_mode = RemoteAuthentication::SAML
          subject.save!
        end

        describe "setup" do
          before { account.send(:setup_remote_saml_authentications) }
          should_not_change("remote auth count") do
            account.remote_authentications.count(:all)
          end
        end
      end

      describe "and does not exist" do
        before do
          assert_equal 0, account.remote_authentications.active.saml.count(:all)
        end

        describe "setup" do
          before { account.send(:setup_remote_saml_authentications) }
          should_change("remote auth count", by: 1) do
            account.remote_authentications.count(:all)
          end
        end
      end
    end

    describe "with saml disabled" do
      before do
        account.stubs(has_saml?: false)
        assert_equal 0, account.remote_authentications.active.saml.count(:all)
      end

      describe "setup" do
        before { account.send(:setup_remote_saml_authentications) }
        should_not_change("remote auth count") do
          account.remote_authentications.count(:all)
        end
      end
    end
  end

  describe "#setup_remote_authentications" do
    let(:account) { Accounts::Base.new }

    it "sets up remote authentications" do
      account.expects(:setup_remote_jwt_authentications).once
      account.expects(:setup_remote_saml_authentications).once

      account.send(:setup_remote_authentications)
    end
  end

  describe "magento" do
    before do
      @new_account = accounts(:minimum).becomes(Accounts::Magento)
      @new_account.source = Zendesk::Accounts::Source::MAGENTO
      @new_account.send(:set_defaults)
      @new_account.send(:set_settings_defaults)
    end

    describe "#add_integrations" do
      before do
        @new_account.remote_authentications.clear
      end

      describe "with remote_login_url" do
        before do
          @new_account.remote_login_url = "https://test.com"
          @new_account.send(:setup_remote_authentications)
        end

        it "creates a jwt remote auth" do
          assert @new_account.remote_authentications.jwt.active.exists?
        end
      end

      describe "without" do
        before do
          @new_account.send(:setup_remote_authentications)
          @new_account.send(:enable_api_token_access)
        end

        it "creates an api token" do
          assert_equal 1, @new_account.api_tokens.count(:all)
          assert @new_account.settings.api_token_access?
        end

        it "not creates a jwt remote auth" do
          refute @new_account.remote_authentications.jwt.active.exists?
          assert @new_account.remote_authentications.jwt.exists?
        end
      end
    end
  end

  describe_with_and_without_arturo_enabled :email_ccs_new_accounts do
    describe "A new account" do
      before do
        @new_account.trial_extras = [TrialExtra.new(key: 'hello', value: 'goodbye')]
      end

      it "calls all convert callbacks after precreation binding" do
        pre_account_creation_callbacks.each do |method|
          @new_account.expects(method)
        end

        @new_account.save!
      end

      # This looks stupid (and is stupid). But short of rewriting all of these tests
      # the quickest path to getting some reasonable amount of performance is to
      # create a single account and test everything that depends on #save in a
      # single context.
      it "sets up correctly" do
        @new_account.save!

        # should "have one default group called support" do
        assert_equal 1, @new_account.groups.count(:all)
        assert_equal "Support", @new_account.groups.first.name
        assert @new_account.groups.first.default?

        # should "contain one user whose default group is the Support group" do
        assert_equal 1, @new_account.groups.first.users.count(:all)
        assert @new_account.groups.first.users.first.memberships.first.default?

        # should 'add itself to the per shard account_ids table' do
        assert_equal 1, ActiveRecord::Base.connection.select_value("select count(*) from account_ids where id = #{@new_account.id}").to_i

        # should "have some default associated models created" do
        assert_equal 20, @new_account.loop_threshold
        assert_equal 6, @new_account.reports.length
        assert_equal 2, @new_account.users.count(:all)
        assert_equal 7, @new_account.ticket_fields.length
        assert_equal '', @new_account.domain_whitelist

        # create the first route
        assert_equal 1, @new_account.routes.count(:all)
        route = @new_account.route
        assert_equal @new_account.subdomain, route.subdomain
        assert_equal_with_nil @new_account.host_mapping, route.host_mapping # TODO: both nil !?

        # create the first brand
        assert_equal 1, @new_account.brands.count(:all)
        brand = @new_account.brands.first
        assert_equal @new_account.name,          brand.name
        assert_equal @new_account.default_brand, brand
        assert brand.active
        assert_equal brand, @new_account.tickets.first.brand

        # should "load the default rules for a new account" do
        path = @new_account.has_email_ccs_new_accounts? ? "db/defaults/ccs_followers_rules.yml" : "db/defaults/rules.yml"
        rule_defs = YAML.load(File.open(File.expand_path(path, Rails.root)))
        assert_equal rule_defs.length, @new_account.rules.length

        @new_account.rules.each do |rule|
          assert_equal @new_account, rule.owner.becomes(Accounts::Classic)
        end

        # should "assert some other stuff that probably shouldn't be asserted in this test (unnecessary) - SJT" do
        assert @new_account.nice_id_sequence
        assert @new_account.invoice_no_sequence
        assert @new_account.account_property_set
        assert @new_account.branding
        assert @new_account.subscription
        assert @new_account.subscription.plan_type
        assert @new_account.signup_page_text
        assert @new_account.signup_email_text
        assert @new_account.verify_email_text
        assert_equal @new_account.signature_template, Brand::DEFAULT_SIGNATURE_TEMPLATE
        assert @new_account.owner
        assert @new_account.role_settings
        assert @new_account.reply_address == @new_account.backup_email_address
        assert @new_account.field_subject
        assert @new_account.field_description
        assert @new_account.field_status
        assert @new_account.field_priority
        assert @new_account.field_group
        assert @new_account.field_ticket_type
        assert @new_account.field_assignee
        assert @new_account.is_active?
        assert @new_account.is_open?
        assert_equal @new_account.name, @new_account.owner.organization.name
        refute @new_account.owner.is_verified?

        # Account settings
        refute_account_setting @new_account, :home_page_search?
        refute_account_setting @new_account, :api_token_access?
        assert_account_setting @new_account, :email_template_photos?
        refute_account_setting @new_account, :email_template_selection?
        refute_account_setting @new_account, :api_password_access?
        assert_account_setting @new_account, :ticket_tagging?
        refute_account_setting @new_account, :ticket_auto_tagging?
        assert_account_setting @new_account, :ticket_show_empty_views?
        assert_account_setting @new_account, :twitter_search_stream?
        assert_account_setting @new_account, :show_introductory_text?
        assert_account_setting @new_account, :only_extended_metrics?
        assert_account_setting @new_account, :modern_email_template?
        assert_account_setting @new_account, :hide_entry_tags_enabled?
        assert_account_setting @new_account, :security_headers?
        assert_account_setting @new_account, :enable_agent_ip_restrictions?
        refute_account_setting @new_account, :enable_ip_mobile_access?
        refute_account_setting @new_account, :moderated_posts?
        refute_account_setting @new_account, :moderated_entries?
        assert_account_setting @new_account, :saml_issuer_account_url
        assert_account_setting @new_account, :rich_content_in_emails

        refute @new_account.settings.last_login_record.new_record?
        assert_equal ImportExportJobPolicy::VIEW_JOBS, @new_account.settings.export_accessible_types

        # should "create the default permission sets" do
        assert_equal @new_account.permission_sets.count(:all), 0

        assert_equal 2, @new_account.remote_authentications.count(:all)
        assert_equal 1, @new_account.remote_authentications.jwt.count(:all)
        assert_equal 1, @new_account.remote_authentications.saml.count(:all)
        assert_equal 0, @new_account.remote_authentications.active.count(:all)

        assert_equal 1, @new_account.trial_extras.count(:all)

        # should create backup recipient address
        assert_equal 1, @new_account.recipient_addresses.count(:all)
        ra = @new_account.recipient_addresses.first
        assert_equal @new_account.backup_email_address, ra.email
        assert_equal @new_account.name, ra.name
        assert(ra.default)
      end

      describe "sets correct subscription features" do
        before { @new_account.save! }

        it "creates zendesk feature bits" do
          subscription = @new_account.subscription
          actual = subscription.features.map(&:name).sort
          expected = Zendesk::Features::Catalogs::Catalog.features_for_plan(
            subscription.plan_type, :trial
          ).map(&:name).map(&:to_s)
          assert_equal expected, actual
        end
      end
    end
  end

  describe "custom statuses" do
    it "creates custom statuses" do
      @new_account.save!
      assert_equal 5, @new_account.custom_statuses.count(:all)
    end

    describe_with_arturo_disabled :create_default_custom_statuses_on_new_accounts do
      it "doesn't create custom statuses" do
        @new_account.save!
        assert_equal 0, @new_account.custom_statuses.count(:all)
      end
    end
  end

  describe "accept_wildcard_emails" do
    it "is disabled on create" do
      @new_account.save!
      assert_equal false, @new_account.settings.accept_wildcard_emails
    end
  end

  describe "the default export configuration when a new account is created" do
    it "sets the export_whitelisted_domain to the domain part of the owner's email address" do
      @new_account.set_owner(
        name: "Some One",
        email: "someone@example.net",
        is_verified: false
      )
      @new_account.send(:set_settings_defaults)

      assert_equal "example.net", @new_account.settings.export_whitelisted_domain
    end

    it "sets the export_whitelisted_domain to blank if owner's email is not given" do
      @new_account.set_owner(
        name: "Some One",
        email: nil,
        is_verified: false
      )
      @new_account.send(:set_settings_defaults)

      assert_equal "", @new_account.settings.export_whitelisted_domain
    end
  end

  describe "The web portal state when a new account is created" do
    it "sets the web portal state to disabled" do
      @new_account.save!
      assert_equal :disabled, @new_account.web_portal_state
    end
  end

  describe "creating a sandbox" do
    let(:account) { accounts(:minimum) }
    let(:sandbox) { account.reset_sandbox }

    before do
      account.settings.export_accessible_types = ImportExportJobPolicy::VIEW_JOBS
      account.settings.save!
    end

    it "copies serialized account settings" do
      assert_equal ImportExportJobPolicy::VIEW_JOBS, sandbox.settings.export_accessible_types
    end

    it "copies account subscription features" do
      actual   = sandbox.subscription.features.map { |f| { f.name => f.value } }
      expected = account.subscription.features.map { |f| { f.name => f.value } }
      assert_equal expected, actual
    end
  end

  describe "copying text properties when creating a sandbox" do
    let(:account) { accounts(:minimum) }

    before do
      account.texts.signup_source = "google.com"
      account.texts.domain_blacklist = "foo.bar,hoo.hah"
      account.texts.save!
      assert account.reset_sandbox
    end

    it "copies text properties to the sandbox" do
      assert_equal "foo.bar,hoo.hah", account.reload.sandbox.texts.domain_blacklist
    end

    it "uses the defaults for text properties that are not copied to the sandbox" do
      sandbox = account.reload.sandbox
      refute_equal account.texts.signup_source, sandbox.texts.signup_source
      assert_equal AccountText.default(:signup_source), sandbox.texts.signup_source

      refute_equal account.reply_address, sandbox.reply_address
      assert_equal sandbox.backup_email_address, sandbox.reply_address
    end
  end

  describe "web_portal_state when creating a sandbox" do
    let(:account) { accounts(:minimum) }

    it "does not alter it when it's restricted" do
      account.settings.web_portal_state = 'restricted'
      account.save!
      account.reset_sandbox
      sandbox = account.reload.sandbox

      assert_equal :restricted, sandbox.sandbox_master.web_portal_state
      assert_equal :restricted, sandbox.web_portal_state
    end

    it "does not alter it when it's disabled" do
      account.settings.web_portal_state = 'disabled'
      account.save!
      account.reset_sandbox
      sandbox = account.reload.sandbox

      assert_equal :disabled, sandbox.sandbox_master.web_portal_state
      assert_equal :disabled, sandbox.web_portal_state
    end
  end

  describe "help_center_state when creating a sandbox" do
    let(:account) { accounts(:minimum) }

    it "sets it to disabled when it's restricted" do
      account.settings.help_center_state = 'restricted'
      account.save!
      account.reset_sandbox
      sandbox = account.reload.sandbox

      assert_equal :restricted, sandbox.sandbox_master.help_center_state
      assert_equal :disabled, sandbox.help_center_state
    end
  end

  describe "A newly created sandbox" do
    before do
      @account = accounts(:minimum)
      @account.settings.screencasts_for_tickets = true
      @account.save!
      Subscription.any_instance.stubs(:has_ticket_forms?).returns(true)
      @new_sandbox1 = create_account_from_hash(default_account_hash.merge(class: Accounts::Sandbox))
      @new_sandbox1.sandbox_master_id = @account.id
      @new_sandbox1.save!
      assert_equal @account.sandbox.id, @new_sandbox1.id
    end

    it "does not send a welcome verification email" do
      AccountsMailer.expects(:deliver_welcome_account_owner).never
      @new_sandbox1.send(:generate_owner_welcome_email)
    end

    it "deprecates all old account sandboxes" do
      @new_sandbox2 = create_account_from_hash(default_account_hash.merge(class: Accounts::Sandbox))
      @new_sandbox2.sandbox_master_id = @account.id
      @new_sandbox2.save!
      @account.reload
      assert_equal @account.sandbox.id, @new_sandbox2.id

      @new_sandbox1.reload
      refute @new_sandbox1.is_active?
    end

    it "creates a default ticket form" do
      assert_equal 1, @new_sandbox1.ticket_forms.count(:all)
    end

    it "does not have screencasts_for_tickets enabled" do
      assert @new_sandbox1.sandbox_master.settings.screencasts_for_tickets?
      refute @new_sandbox1.settings.screencasts_for_tickets?
    end
  end

  describe "account_ids" do
    it 'raises and clean up its account_ids table entry when owner is not set' do
      account_id_count = ActiveRecord::Base.connection.select_value("select count(*) from account_ids")

      # should 'raise and call Account#delete_bogus_account_ids_entry when owner is not set' do
      Account.expects(:delete_bogus_account_ids_entry)

      assert_raise RuntimeError do
        @new_account = create_account_from_hash(default_account_hash, false)
        @new_account.save
      end

      assert_equal account_id_count, ActiveRecord::Base.connection.select_value("select count(*) from account_ids")
    end

    describe "#validate_trial_coupon_code" do
      it 'rejects bad coupon codes' do
        @coupon_account = Accounts::Classic.new(trial_coupon_code: 'lovely_warm_hug_not_over_long')
        assert_equal false, @coupon_account.valid?
        assert @coupon_account.errors[:trial_coupon_code].any?
      end

      it 'rejects a code for a discount coupon code' do
        @coupon = DiscountCoupon.create do |c|
          c.name = 'TEST'
          c.coupon_code = 'luudes'
          c.forced_inactive = false
          c.expiry = 1.year.from_now
          c.effective = 1.month.ago
          c.length_days = 365
          c.discount_percentage = 50
          c.minimum_incremental_revenue = 100
        end

        @coupon_account = Accounts::Classic.new(trial_coupon_code: @coupon.coupon_code)

        assert_equal false, @coupon_account.valid?
        assert @coupon_account.errors[:trial_coupon_code].any?
      end

      it 'rejects a code for a trial coupon code that is disabled' do
        @coupon = DiscountCoupon.create do |c|
          c.name = 'TEST'
          c.coupon_code = 'luudes'
          c.forced_inactive = false
          c.expiry = 1.year.from_now
          c.effective = 1.month.ago
          c.length_days = 365
          c.discount_percentage = 50
          c.minimum_incremental_revenue = 100
          c.forced_inactive = true
        end

        @coupon_account = Accounts::Classic.new(trial_coupon_code: @coupon.coupon_code)

        assert_equal false, @coupon_account.valid?
        assert @coupon_account.errors[:trial_coupon_code].any?
      end

      it 'accepts a code for a trial coupon, creating a CouponApplication as a side effect' do
        @coupon = TrialCoupon.create do |c|
          c.name = 'TEST'
          c.coupon_code = 'ovaltine'
          c.forced_inactive = false
          c.expiry = 1.year.from_now
          c.effective = 1.month.ago
          c.length_days = 365
          c.trial_plan_type = 1
          c.trial_max_agents = 3
        end

        create_account_from_hash default_account_hash.merge!(trial_coupon_code: @coupon.coupon_code)
        assert @new_account.valid?
      end
    end

    describe 'defaults' do
      before do
        create_account_from_hash default_account_hash
      end

      describe_with_and_without_arturo_enabled :email_ccs_default_rules_content do
        describe_with_and_without_arturo_setting_enabled :follower_and_email_cc_collaborations do
          it "creates some stuff using i18n" do
            I18n.stubs(:t).returns("foo".freeze)

            keys = %w[
              txt.default.forums.title
              txt.default.fields.subject.title
              txt.default.registration_message_v2
              txt.default.new_welcome_email_text
              txt.default.cc_email_subject_v2
              txt.default.cc_email_text.paragraph_v2
              txt.default.follower_email_subject
              txt.default.follower_email_text
              txt.default.verify_email_text
              txt.default.chat_welcome_message
              txt.default.automations.close.title
              txt.default.macros.close_and_redirect.title
              txt.default.groups.support.name
              txt.default.reports.backlog.title
              txt.default.organization_activity_email_text
              txt.default.ticket_forms_instructions
              txt.default.customer_lists.last_30_days_title
            ]

            requester_received_body_key = "txt.default.triggers.notify_requester_received.body_v4"
            requester_received_subject_key = "txt.default.triggers.notify_requester_received.subject_v3"
            requester_received_title_key = "txt.default.triggers.notify_requester_received.title"

            if @new_account.has_email_ccs_default_rules_content? && @new_account.has_follower_and_email_cc_collaborations_enabled?
              keys.concat(
                %w[
                  txt.default.triggers.notify_requester_and_ccs_of_received_request.title
                  txt.default.triggers.notify_requester_and_ccs_of_received_request.subject_v2
                  txt.default.triggers.notify_requester_and_ccs_of_update.title
                  txt.default.triggers.notify_requester_and_ccs_of_update.subject
                  txt.default.triggers.notify_requester_and_ccs_of_update.body_v4
                  txt.default.triggers.notify_requester_proactive_ticket.title
                  txt.default.triggers.notify_requester_proactive_ticket.description
                  txt.default.triggers.notify_requester_proactive_ticket.subject
                  txt.default.triggers.notify_requester_proactive_ticket.body
                ]
              )

              requester_received_body_key = "txt.default.triggers.notify_requester_and_ccs_of_received_request.body_v4"
              requester_received_subject_key = "txt.default.triggers.notify_requester_and_ccs_of_received_request.subject_v2"
              requester_received_title_key = "txt.default.triggers.notify_requester_and_ccs_of_received_request.title"
            else
              keys.concat(
                %w[
                  txt.default.triggers.notify_requester_received.title
                  txt.default.triggers.notify_requester_received.subject_v3
                  txt.default.triggers.notify_requester_update.title
                  txt.default.triggers.notify_requester_update.subject
                ]
              )

              keys << "txt.default.triggers.notify_requester_update.body_#{@new_account.has_email_ccs_default_rules_content? ? 'v4' : 'v3'}"
            end

            keys.each do |key|
              I18n.expects(:t).at_least(1).with(key, anything).returns(key.freeze)
            end

            I18n.expects(:t).with(requester_received_body_key).returns("foo<br />foo".freeze)

            @new_account.save!

            assert_equal "txt.default.forums.title", @new_account.forum_title

            assert @new_account.ticket_fields.find_by_title("txt.default.fields.subject.title")

            trigger = @new_account.triggers.find_by_title(requester_received_title_key)
            assert trigger
            assert_equal requester_received_subject_key, trigger.definition.actions.first.value[1] # notify_requester_received.subject
            assert_equal "foo\nfoo", trigger.definition.actions.first.value[2] # notify_requester_received.body

            automation = @new_account.automations.find_by_title("txt.default.automations.close.title")
            assert automation

            macro = @new_account.macros.find_by_title("txt.default.macros.close_and_redirect.title")
            assert macro
            # assert_equal "foo", macro.definition.actions.last.value

            view = @new_account.views.find_by_title("{{zd.your_unsolved_tickets}}")
            assert view

            group = @new_account.groups.find_by_name("txt.default.groups.support.name")
            assert group

            customer_list = @new_account.user_views.find_by_title("txt.default.customer_lists.last_30_days_title")
            assert customer_list

            staff = @new_account.permission_sets.find_by_name("txt.default.roles.staff.name")
            team_leader = @new_account.permission_sets.find_by_name("txt.default.roles.staff.name")
            advisor = @new_account.permission_sets.find_by_name("txt.default.roles.staff.name")
            refute staff
            refute team_leader
            refute advisor

            report = @new_account.reports.find_by_title("txt.default.reports.backlog.title")
            assert report

            assert_equal "txt.default.cc_email_subject_v2", @new_account.cc_subject_template
            assert_equal "txt.default.cc_email_text.paragraph_v2", @new_account.cc_email_template
            assert_equal "txt.default.follower_email_subject", @new_account.follower_subject_template
            assert_equal "txt.default.follower_email_text", @new_account.follower_email_template

            assert_equal "txt.default.new_welcome_email_text", @new_account.signup_email_text
            assert_equal "{{txt.email.delimiter}}", @new_account.mail_delimiter
            assert_equal "txt.default.chat_welcome_message", @new_account.chat_welcome_message
            assert_equal "txt.default.organization_activity_email_text\n\n{{ticket.comments_formatted}}", @new_account.organization_activity_email_template
            assert_equal "txt.default.ticket_forms_instructions", @new_account.ticket_forms_instructions
          end
        end
      end
    end

    describe_with_and_without_arturo_enabled :email_ccs_new_accounts do
      describe "with help center onboarding" do
        before do
          create_account_from_hash default_account_hash
        end

        it "uses the correct version of keys" do
          I18n.stubs(:t).returns("foo")
          %w[
            txt.default.fields.subject.title
            txt.default.registration_message_v2
            txt.default.new_welcome_email_text
            txt.default.cc_email_text.paragraph_v2
            txt.default.follower_email_text
            txt.default.verify_email_text
            txt.default.chat_welcome_message
            txt.default.automations.close.title
            txt.default.macros.close_and_redirect.title
            txt.default.groups.support.name
            txt.default.reports.backlog.title
            txt.default.organization_activity_email_text
            txt.default.ticket_forms_instructions
          ].each do |key|
            I18n.expects(:t).with(key, anything).returns(key)
          end

          unless @new_account.has_email_ccs_new_accounts?
            %w[
              txt.default.triggers.notify_requester_received.title
              txt.default.triggers.notify_requester_received.subject_v3
              txt.default.triggers.notify_requester_received.body_v4
              txt.default.triggers.notify_requester_update.body_v4
            ].each do |key|
              I18n.expects(:t).with(key, anything).returns(key)
            end
          end

          @new_account.save
        end
      end
    end

    describe "#add_subscription" do
      describe "sandbox" do
        before do
          @new_account.stubs(:sandbox_master).returns(accounts(:minimum))
          @new_account.set_currency('GBP')
          @new_account.send(:add_subscription)
        end

        before_should "create a non-trial sandbox" do
          @new_account = @new_account.becomes(Accounts::Sandbox)
          @new_account.expects(:create_subscription).with(
            account: @new_account,
            currency: 'GBP',
            is_trial: false,
            manual_discount: 100,
            manual_discount_expires_on: 5.years.from_now.to_date,
            base_agents: 25,
            plan_type: SubscriptionPlanType.Large,
            pricing_model_revision: ZBC::Zendesk::PricingModelRevision::NEW_FORUMS
          )
        end
      end

      describe "not-sandbox" do
        before do
          @new_account.stubs(trial_coupon_code: "test")
          @new_account.set_currency('JPY')
          @new_account.send(:add_subscription)
        end

        before_should "create a non-trial sandbox" do
          @new_account.expects(:create_subscription).with(
            trial_coupon_code: "test",
            account: @new_account,
            currency: 'JPY',
            is_trial: false
          )
        end
      end
    end

    describe "verification link" do
      before do
        create_account_from_hash(default_account_hash)
        @new_account.subdomain = "snow"

        @new_account.save!
      end

      it "returns a verification link for the owner of the account" do
        "#{@new_account.url(mapped: false)}/verification/email".must_include "https://snow.zendesk-test.com/verification/email"
      end
    end

    describe "google login link" do
      before do
        @new_account = create_account_from_hash(default_account_hash.merge(
          class: Accounts::GoogleAppMarket,
          is_google_login_enabled: true
        ))

        @new_account.subdomain = "snow"
        @new_account.save!
      end

      it "returns a link to login via google id" do
        assert_match "https://snow.zendesk-test.com/login?profile=google&return_to=https%3A%2F%2Fsnow.zendesk-test.com%2Flogin",
          @new_account.send(:verification_link)
      end
    end

    describe "#force_downcase_subdomain" do
      before do
        multicase_subdomain_hash = default_account_hash
        multicase_subdomain_hash[:subdomain] = "WibbLe"
        @new_account = create_account_from_hash(multicase_subdomain_hash)
        @new_account.save
        @new_account.reload
      end

      it "creates an account with a downcased subdomain" do
        assert_equal "wibble", @new_account.subdomain
      end
    end

    describe "#set_created_from_ip_address" do
      before do
        @new_account.set_created_from_ip_address("234.128.78.21")
      end

      it "captures the remote ip from which the account was created" do
        assert_equal "234.128.78.21", @new_account.settings.created_from_ip_address
      end
    end

    describe "#set_creation_channel" do
      before do
        @new_account.set_creation_channel("website1234")
      end

      it "sets the creation channel setting" do
        assert_equal "website1234", @new_account.settings.creation_channel
      end
    end
  end

  describe "#choose_shard!" do
    describe "account creation shard database table is not populated" do
      let (:shards) { anything }

      before do
        AccountCreationShard.stubs(:where).returns(shards)
        shards.stubs(:reload).returns(shards)
        shards.stubs(:pluck).returns([])
      end

      it "sends out rollbar" do
        ZendeskExceptions::Logger.expects(:record).once
        @new_account.choose_shard!(:us)
      end
    end

    describe "account creation shard database table is populated" do
      let (:shards) { anything }

      before do
        AccountCreationShard.stubs(:where).returns(shards)
        shards.stubs(:reload).returns(shards)
        shards.stubs(:pluck).returns([2])
      end

      it "use shards information from the database" do
        @new_account.choose_shard!(:us)
        assert_equal 2, @new_account.shard_id
      end
    end
  end

  describe "#sanitized_name" do
    let(:account) { accounts(:minimum).becomes(Accounts::Classic) }

    before do
      account.stubs(:name).returns("bad | brand")
    end

    it "removes pipes" do
      assert_equal account.send(:sanitized_name), "bad brand"
    end
  end
end
