require_relative "../../../support/test_helper"

SingleCov.covered!

describe Accounts::Fabric::AccountCreator do
  fixtures :accounts

  let(:endpoint) { 'endpoint' }
  let(:account_creation_endpoint) { "https://zendeskaccounts.#{Zendesk::Configuration['host']}/#{endpoint}" }
  let(:account) { accounts(:minimum) }
  let(:account_uri) { "https://#{account.subdomain}.#{Zendesk::Configuration['host']}" }
  let(:accept_tos_endpoint) { "#{account_uri}/api/private/mobile_sdk/tos/accept" }

  def stub_account_creation_request(body: nil, status: 201)
    stub_request(:post, account_creation_endpoint).
      to_return(
        status: status,
        headers: { 'Content-Type' => 'application/json' },
        body: body.to_json
      )
  end

  def stub_accept_tos_request(status: 200)
    stub_request(:post, accept_tos_endpoint).
      to_return(status: status)
  end

  before do
    stub_accept_tos_request
    stub_account_creation_request(body: {
      'account' => {
        'subdomain' => account.subdomain
      }
    })
  end

  let(:creator) do
    Accounts::Fabric::AccountCreator.new(
      {},
      subdomain: 'subdomain',
      account_creation_endpoint: 'endpoint',
      accept_tos_endpoint: 'endpoint',
      accept_tos: true
    )
  end

  it 'creates an account with success' do
    creator.create

    assert_equal account, creator.account
  end

  it 'returns nil when it fails to create an account' do
    stub_account_creation_request(status: 404)

    creator.create

    assert_nil creator.account
  end

  describe 'when tos acceptance fails' do
    it 'creates the account successfuly' do
      stub_accept_tos_request(status: 404)

      creator.create

      assert_equal account, creator.account
    end
  end
end
