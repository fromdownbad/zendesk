require_relative "../../../support/test_helper"

SingleCov.covered!

# Minitest thinks this file should be a Generator test case
class ActiveSupport::TestCase
  register_spec_type(/Accounts::Fabric::SubdomainGenerator/, self)
end

describe Accounts::Fabric::SubdomainGenerator do
  let(:email) { 'email@domain.com' }

  it 'generates a valid subdomain' do
    subdomain = Accounts::Fabric::SubdomainGenerator.new(email).subdomain

    assert %r{fabric-emaildomaincom} =~ subdomain, "Subdomain '#{subdomain}' does not matches with the format"
  end

  it 'restricts the subdomain size in 63 chars' do
    email = "#{'e' * 63}@domain.com"

    subdomain = Accounts::Fabric::SubdomainGenerator.new(email).subdomain

    assert_equal DNS_CHARACTER_LIMIT, subdomain.size
  end

  it 'prepends `z3n-` when the :mobile_sdk_fabric_integration_test is enabled' do
    Arturo.enable_feature! :mobile_sdk_fabric_integration_test

    subdomain = Accounts::Fabric::SubdomainGenerator.new(email).subdomain

    assert_match %r{\Az3n-}, subdomain
  end

  it 'does not prepend `z3n-` when the :mobile_sdk_fabric_integration_test is disabled' do
    Arturo.disable_feature! :mobile_sdk_fabric_integration_test

    subdomain = Accounts::Fabric::SubdomainGenerator.new(email).subdomain

    refute_match %r{\Az3n-}, subdomain
  end
end
