require_relative "../../../support/test_helper"

SingleCov.covered!

describe Accounts::Fabric::MobileSdkAppCreator do
  fixtures :accounts

  let(:endpoint) { 'api/v2/mobile_sdk_apps' }
  let(:account) { accounts(:minimum) }
  let(:account_endpoint) { "https://#{account.subdomain}.#{Zendesk::Configuration['host']}/#{endpoint}" }
  let(:mobile_sdk_app) do
    {
      'id' => 'id',
      'identifier' => 'identifier',
      'client_identifier' => 'client_identifier',
      'signup_source' => 'signup_source'
    }
  end

  def stub_mobile_sdk_app_creation_request(body: nil, status: 201)
    stub_request(:post, account_endpoint).
      to_return(
        status: status,
        headers: { 'Content-Type' => 'application/json' },
        body: body.to_json
      )
  end

  it 'creates an mobile_sdk_app with success' do
    stub_mobile_sdk_app_creation_request(body: {
      'mobile_sdk_app' => {
        'id' => 'id',
        'identifier' => 'identifier',
        'client_identifier' => 'client_identifier',
        'signup_source' => 'signup_source'
      }
    })

    creator = Accounts::Fabric::MobileSdkAppCreator.new('mobile_app', account)

    assert_equal mobile_sdk_app, creator.create
  end

  it 'returns nil when it fails to create an mobile_sdk_app' do
    stub_mobile_sdk_app_creation_request(status: 404)

    creator = Accounts::Fabric::MobileSdkAppCreator.new('mobile_app', account)

    assert_nil creator.create
  end
end
