require_relative "../../support/test_helper"
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered! uncovered: 63

describe Accounts::Base do
  include MultiproductTestHelper

  fixtures :accounts

  let(:shell_account) { setup_shell_account }
  let(:account) { Accounts::Base.find(shell_account.id) }

  describe "in Support accounts" do
    let(:account) { setup_shell_account_with_support }

    describe "account creation" do
      it "adds default properties" do
        Accounts::Base.any_instance.expects(:add_properties)
        account
      end

      describe_with_arturo_enabled :email_ccs_new_accounts do
        it "enables CCs/Followers and disables its rollback" do
          assert account.settings.follower_and_email_cc_collaborations
          assert account.settings.ccs_followers_no_rollback
        end
      end

      describe_with_arturo_disabled :email_ccs_new_accounts do
        it "does not enable CCs/Followers or disable its rollback" do
          refute account.settings.follower_and_email_cc_collaborations
          refute account.settings.ccs_followers_no_rollback
        end
      end

      describe_with_arturo_enabled :trigger_categories_new_accounts do
        it "enables the `trigger_categories_api` account setting" do
          assert account.settings.trigger_categories_api
        end
      end

      describe_with_arturo_disabled :trigger_categories_new_accounts do
        it "does not enable the `trigger_categories_api` account setting" do
          refute account.settings.trigger_categories_api
        end
      end
    end

    describe "existing accounts" do
      let(:account) { accounts(:minimum) }

      it "does not enable CCs/Followers or disable its rollback" do
        refute account.settings.follower_and_email_cc_collaborations
        refute account.settings.ccs_followers_no_rollback
      end
    end
  end

  describe '#add_default_brand' do
    before do
      assert account.default_brand.nil?
      assert_equal account.brands.length, 0
    end

    it 'adds a default brand' do
      assert account.send(:add_default_brand)
      account.reload
      assert account.default_brand.present?
      assert_equal account.brands.length, 1
    end

    it 'does nothing if the account has a default brand' do
      assert account.send(:add_default_brand)
      account.reload
      assert account.default_brand.present?
      assert account.send(:add_default_brand)
      account.reload
      assert account.default_brand.present?
      assert_equal account.brands.length, 1
    end
  end

  describe "#load_rules_from_yaml" do
    describe_with_arturo_disabled :email_ccs_default_rules_content do
      describe_with_and_without_arturo_setting_enabled :follower_and_email_cc_collaborations do
        it "loads the default rules.yml by default" do
          file = File.open(Accounts::Base::DEFAULT_RULES_PATH)
          File.expects(:open).with(Accounts::Base::DEFAULT_RULES_PATH).returns(file)
          account.load_rules_from_yaml
        end
      end
    end

    describe_with_arturo_enabled :email_ccs_default_rules_content do
      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        it "loads the default rules.yml by default" do
          file = File.open(Accounts::Base::DEFAULT_RULES_PATH)
          File.expects(:open).with(Accounts::Base::DEFAULT_RULES_PATH).returns(file)
          account.load_rules_from_yaml
        end
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        it "loads the CCs/Followers rules.yml by default" do
          file = File.open(Accounts::Base::DEFAULT_CCS_FOLLOWERS_RULES_PATH)
          File.expects(:open).with(Accounts::Base::DEFAULT_CCS_FOLLOWERS_RULES_PATH).returns(file)
          account.load_rules_from_yaml
        end
      end
    end

    describe "plan has_groups=true, unlimited_triggers=false" do
      before do
        account.stubs(:has_groups?).returns(true)
        account.stubs(:has_unlimited_triggers?).returns(false)
      end

      it "skips groups related rules" do
        account.load_rules_from_yaml
        notification_group = Trigger.where(account_id: account.id, title: "Notify group of assignment")
        assert_equal notification_group.length, 0
      end
    end

    describe "plan has_groups=true, unlimited_triggers=true" do
      before do
        account.stubs(:has_groups?).returns(true)
        account.stubs(:has_unlimited_triggers?).returns(true)
      end

      it "creates groups related rules" do
        account.load_rules_from_yaml
        notification_group = Trigger.where(account_id: account.id, title: "Notify group of assignment")
        assert_equal notification_group.length, 1
      end
    end
  end

  describe "#load_rule_categories_from_yaml" do
    describe_with_arturo_setting_disabled :trigger_categories_api do
      it 'does not load rule_categories.yml' do
        File.expects(:open).with('db/defaults/rule_categories.yml').never
        account.load_rule_categories_from_yaml
      end

      it 'does not create any rule categories' do
        RuleCategory.expects(:create!).never
        account.load_rule_categories_from_yaml
      end
    end

    describe_with_arturo_setting_enabled :trigger_categories_api do
      it 'loads rule_categories.yml by default' do
        file = File.open('db/defaults/rule_categories.yml')
        File.expects(:open).with('db/defaults/rule_categories.yml').returns(file)
        account.load_rule_categories_from_yaml
      end

      it 'creates a rule_category for each entry in the yaml file' do
        default_category_params = {
          'name' => 'Notifications',
          'rule_type' => 'Trigger',
          'position' => '1',
          account: account
        }

        RuleCategory.expects(:create!).with(default_category_params)
        account.load_rule_categories_from_yaml
      end
    end
  end

  describe "#version" do
    let(:request_received_key) { "txt.default.triggers.notify_requester_received.body_VERSION" }
    let(:other_key) { "txt.default.triggers.notify_requester_update.body_VERSION" }

    describe_with_arturo_disabled :email_ccs_default_rules_content do
      it "uses v4 notification template body for the request received trigger" do
        account.send(:version, request_received_key).must_equal "v4"
      end

      it "uses v3 notification template body for other triggers" do
        account.send(:version, other_key).must_equal "v3"
      end
    end

    describe_with_arturo_enabled :email_ccs_default_rules_content do
      it "uses v4 notification template body for the request received trigger" do
        account.send(:version, request_received_key).must_equal "v4"
      end

      it "uses v4 notification template body for other triggers" do
        account.send(:version, other_key).must_equal "v4"
      end
    end
  end
end
