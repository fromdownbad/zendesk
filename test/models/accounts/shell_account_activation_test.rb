require_relative "../../support/test_helper"
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered!

class SomeError < StandardError; end

describe Accounts::ShellAccountActivation do
  include MultiproductTestHelper

  let(:shell_account) { setup_shell_account }
  let(:account) { Accounts::ShellAccountActivation.find(shell_account.id) }
  let(:owner) { account.owner }

  describe '#activate_accounts_db_records_in_transaction' do
    it 'only inserts or updates accounts db tables' do
      shell_account
      queries = sql_queries(/INSERT|UPDATE/) do
        account.activate_accounts_db_records_in_transaction
      end
      tables_changed = (tables_inserted(queries) + tables_updated(queries)).uniq.sort

      assert_equal tables_changed, ["accounts", "role_settings", "subscriptions"]
    end

    it 'is idempotent and skips if already called once successfully' do
      account.activate_accounts_db_records_in_transaction

      assert_sql_queries(0, /INSERT|UPDATE/) do
        account.activate_accounts_db_records_in_transaction
      end
    end

    it 'rolls back all changes if error raised within transaction' do
      account.stubs(:add_address).raises(SomeError)
      begin
        account.activate_accounts_db_records_in_transaction
      rescue SomeError
      end

      assert_nil account.reload.subscription
    end
  end

  describe '#activate_shard_db_records_in_transaction' do
    let(:support_product) do
      Zendesk::SupportAccounts::Product.retrieve(account)
    end

    before do
      account.activate_accounts_db_records_in_transaction
    end

    it 'only changes shard db tables, with exception of `updated_at` timestamp of accounts db tables' do
      queries = sql_queries(/INSERT|UPDATE/) do
        account.activate_shard_db_records_in_transaction(owner)
      end
      tables_updated = tables_updated(queries) - ['accounts', 'subscriptions'] # There are a few DB writes on accounts and subscriptions that only change `updated_at`
      tables_changed = (tables_inserted(queries) + tables_updated).uniq.sort

      expected_tables_changed = ["account_ids", "account_property_sets", "account_settings", "account_texts", "brands", "custom_statuses", "esc_kafka_messages",
                                 "groups", "memberships", "organization_memberships", "organizations", "permission_set_permissions", "permission_sets", "recipient_addresses",
                                 "reports", "rule_revisions", "rules", "sequences", "subscription_features", "ticket_fields", "trigger_snapshots", "users"]
      # These are all shard db tables
      assert_equal expected_tables_changed, tables_changed

      update_queries = queries.select { |query| /UPDATE `?(accounts|subscriptions)`?/.match(query) }
      update_queries_excluding_timestamp_touch = update_queries.reject do |query|
        /UPDATE `(accounts|subscriptions)` SET (`(accounts|subscriptions)`\.)?`updated_at` = '\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}' WHERE/.match(query)
      end

      assert_empty update_queries_excluding_timestamp_touch
    end

    it 'is idempotent and skips if already called once successfully' do
      account.activate_shard_db_records_in_transaction(owner)

      assert_sql_queries(0, /INSERT|UPDATE/) do
        account.activate_shard_db_records_in_transaction(owner)
      end
    end

    describe 'brand creation fails' do
      before do
        account.stubs(:add_default_brand).raises(SomeError)
      end

      it 'rolls back some changes but not the account settings' do
        begin
          account.activate_shard_db_records_in_transaction(owner)
        rescue SomeError
        end

        assert_empty account.reload.rules
        refute_empty account.reload.settings
      end
    end

    it 'changes support product state from not_started to trial' do
      assert_equal :not_started, support_product.send(:state)
      account.activate_shard_db_records_in_transaction(owner)
      assert_equal :trial, support_product.send(:state)
    end

    describe 'export_whitelisted_domain account setting' do
      it 'is set to the default' do
        account.activate_shard_db_records_in_transaction(owner)
        assert_equal account.settings.export_whitelisted_domain, 'example.com'
      end
    end

    describe 'ssl_required account setting' do
      it 'is set to true' do
        account.activate_shard_db_records_in_transaction(owner)
        assert account.settings.ssl_required
      end
    end

    describe '#trial_extras' do
      it 'settings.original_remote_ip defaults to empty' do
        assert_equal "", account.settings.created_from_ip_address
      end

      it 'settings.suite_trial default to empty' do
        account.activate_shard_db_records_in_transaction(owner)
        assert_nil account.settings.suite_trial
      end

      describe 'when trial_extras is set' do
        let(:original_remote_ip) { "172.17.0.1" }

        before do
          account.trial_extras << TrialExtra.from_hash("suite_trial" => true, "original_remote_ip" => original_remote_ip)
          account.activate_shard_db_records_in_transaction(owner)
        end

        it 'settings.original_remote_ip is set correctly' do
          assert_equal original_remote_ip, account.settings.created_from_ip_address
        end

        it 'settings.suite_trial is true' do
          assert account.settings.suite_trial
        end
      end

      describe 'when trial_extras is NOT set' do
        before do
          account.trial_extras << TrialExtra.from_hash("suite_trial" => false)
          account.activate_shard_db_records_in_transaction(owner)
        end

        it 'settings.original_remote_ip is not set' do
          assert_equal "", account.settings.created_from_ip_address
        end

        it 'settings.suite_trial is false' do
          refute account.settings.suite_trial
        end
      end
    end
  end

  describe '#user_roles_initialization' do
    let(:user_roles_initialization) { account.user_roles_initialization(activated_by_user) }

    describe 'when Support activation is requested by account owner' do
      let(:activated_by_user) { owner }

      describe 'and account owner is a Support admin' do
        it 'sets owner as Support admin' do
          user_roles_initialization
        end
      end

      describe 'when permissions_billing_admin arturo is enabled' do
        before do
          Arturo.enable_feature!(:permissions_billing_admin)
        end

        it 'creates a billing_admin permission set and policy' do
          Zendesk::ExternalPermissions::PermissionsServiceClient.any_instance.expects(:create_default_policy!)
          user_roles_initialization
          assert account.permission_sets.where(role_type: ::PermissionSet::Type::BILLING_ADMIN).count == 1
        end
      end

      describe 'and account owner is not Support admin' do
        # The initial owner is always set as Support admin. It means ownership was transferred at least once in this scenario
        # So we need to create intial owner as admin
        let(:initial_owner) { FactoryBot.create(:verified_admin, account: shell_account) }

        before do
          initial_owner
          owner.update_columns(roles: Role::AGENT.id)
        end

        it 'downgrades old owner' do
          user_roles_initialization
          assert initial_owner.reload.is_contributor?
        end

        it 'sets owner as Support admin' do
          user_roles_initialization
          assert owner.reload.is_admin?
        end

        describe 'when Support activation is requested by Chat admin who is not account owner' do
          let(:chat_admin) { FactoryBot.create(:contributor, account: shell_account) }
          let(:activated_by_user) { chat_admin }

          before do
            Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).with(chat_admin.id).returns(chat: 'admin')
            # At this point, account should always have Subscription
            # But this test setup makes it really hard to add a valid
            # subscription, so stubed NilSubscription to return max_agents
            NilSubscription.any_instance.stubs(:max_agents).returns(5)
          end

          it 'downgrades old owner' do
            user_roles_initialization
            assert initial_owner.reload.is_contributor?
          end

          it 'upgrades activator to admin' do
            user_roles_initialization
            assert chat_admin.reload.is_admin?
          end
        end
      end
    end
  end

  it 'does not introduce new create callbacks on Classic Account' do
    custom_callbacks = Accounts::ShellAccountActivation._create_callbacks.collect(&:filter).reject { |cb| cb.to_s.starts_with?("autosave_associated_records_") }
    assert_equal custom_callbacks.count, 22
    custom_callbacks_without_anonymous_block = custom_callbacks.reject { |cb| cb.instance_of?(Integer) }
    # There is one anomymous callback defined by CIA
    assert_equal custom_callbacks_without_anonymous_block, [:enable_office_365_login!, :enable_google_login!, :add_rules_and_reports, :add_custom_fields, :add_address, :add_owner_to_group, :add_owner, :add_group, :add_custom_statuses, :add_default_brand, :create_account_property_set, :add_feature_bits, :add_subscription, :add_sequences, :save_settings_and_texts, :create_role_settings, :store_id_on_shard, :global_uid_before_create, :default_to_12_hour_clock_in_countries_where_it_is_dominant, :add_properties, :force_downcase_subdomain]
  end
end
