require_relative "../../support/test_helper"
require_relative "../../support/pre_account_creation_helper"

# splitting tests into 2 files so they can run in parallel
SingleCov.covered! uncovered: 10, file: "app/models/accounts/precreation.rb"

describe Accounts::Precreation do
  include PreAccountCreationHelper
  fixtures :all

  describe '.add_trial_extras' do
    let(:minimum) do
      accounts(:minimum).tap do |acc|
        acc.owner.email = 'noreply@zendesk.com'
        acc.owner.identities.first.destroy
        acc.owner.identities.reload
        acc.owner.identities.first.make_primary!
        acc.owner.identities.first.verify!
        TrialExtra.where(account_id: acc.id).destroy_all
      end
    end

    let(:precreated_account) do
      get_precreated_account(
        name: :precreation_one,
        account_id: minimum.id,
        target_locale_id: 1
      )
    end

    def new_account_with_precreation(params = {})
      precreated_account
      initialize_account(params)
      @account_initializer.account
    end

    def new_account_with_no_precreation(params = {})
      PreAccountCreation.stubs(:precreation_supported?).returns(false)
      PreAccountCreation.destroy_all
      initialize_account(params)
      @account_initializer.account
    end

    before do
      Zendesk::RedisStore.redis_client = FakeRedis::Redis.new
      @redis_client = Zendesk::RedisStore.client
      Accounts::Base.any_instance.stubs(:redis_client).returns(@redis_client)
      Accounts::Base.any_instance.stubs(:created_via_google?).returns(false)
      Accounts::Base.any_instance.stubs(:created_via_office_365?).returns(false)
      Accounts::Base.any_instance.stubs(:set_google_apps_creds)
    end

    after do
      TrialExtra.destroy_all
      @redis_client.flushall
    end

    it 'saves trial extras to Redis (not MySQL)' do
      # Validate that precreated accounts don't build the trial_extras active-record collection
      # NOTE: previously we asserted this by expecting the trial_extras method would not be executed but when implementing
      # A/B test for trial length we need to execute it during account initialization via trial_extras_alternative_length?
      TrialExtra.expects(:build_hash).never

      # Validate that we're correctly storing trial extras in Redis
      @redis_client.expects(:setex).with("account_#{precreated_account.account_id}_trial_extras", 90.days.seconds, is_a(String)).returns(true)

      # Stub jobs
      TrialExtrasJob.expects(:work).returns(true)
      AccountsPrecreationJob.stubs(:work)

      # new_account_with_precreation will use the account-initializer which triggers add_trial_extras
      new_account_with_precreation.save!
    end

    it 'writes trial extras to db if account is not precreated' do
      acc = new_account_with_no_precreation(trial_extras: {test_key: "test_val"})
      @redis_client.expects(:setex).with(regexp_matches(/trial_extras/), anything, anything).never
      TrialExtrasJob.expects(:enqueue).never

      acc.save!
      assert_equal acc.trial_extras.where(key: "test_key").first.value, "test_val"
      assert_equal acc.send(:redis_trial_extras), {}
    end

    it 'falls back to writing to db and tracks to statsd' do
      statsd_stub = Zendesk::StatsD::Client.new(namespace: "test")
      Zendesk::StatsD::Client.stubs(:new).returns(statsd_stub)
      statsd_stub.expects(:increment).at_least_once
      @redis_client.expects(:setex).with(regexp_matches(/trial_extras/), anything, anything).raises(StandardError)
      statsd_stub.expects(:increment).with('count', has_entries(tags: includes('status:failure', 'precreated:true', regexp_matches(/account_id/))))

      acc = new_account_with_precreation(trial_extras: {this_is_a_key: "this is a value"})
      acc.save!
      assert_equal acc.trial_extras.where(key: "this_is_a_key").first.value, "this is a value"
      assert_equal acc.send(:redis_trial_extras), {}
    end

    it 'tracks success to statsd' do
      statsd_stub = Zendesk::StatsD::Client.new(namespace: "test")
      Zendesk::StatsD::Client.stubs(:new).returns(statsd_stub)
      statsd_stub.expects(:increment).at_least_once
      statsd_stub.expects(:increment).with('count', has_entries(tags: includes('status:success', 'precreated:true', regexp_matches(/account_id/))))

      new_account_with_precreation.save!
    end

    it 'scopes trial extras by account' do
      CachingObserver.stubs(:expire_fragments_on_save_or_destroy)
      account1 = new_account_with_precreation(trial_extras: { key1: 'val1' })

      account1.save!

      assert_equal 'val1', account1.send(:redis_trial_extras)['key1']
      assert_equal 'val1', account1.trial_extras.where(key: 'key1').first.value
      assert_nil   account1.send(:redis_trial_extras)['key2']
      assert_nil   account1.trial_extras.where(key: 'key2').first
    end

    it 'allows adding to redis trial extras' do
      account1 = new_account_with_precreation(trial_extras: { key1: 'val1' })
      account1.add_trial_extras(key2: 'val2')

      assert_equal 'val1', account1.send(:redis_trial_extras)['key1']
      assert_equal 'val2', account1.send(:redis_trial_extras)['key2']

      account1.save!

      assert_equal 'val1', account1.trial_extras.where(key: 'key1').first.value
      assert_equal 'val2', account1.trial_extras.where(key: 'key2').first.value
    end

    it 'works with empty trial extras' do
      account1 = new_account_with_precreation(trial_extras: {})
      @redis_client.del(TrialExtra.redis_key(account1.id)) # fast account creation key gets auto-added during initalization
      account1.add_trial_extras({})
      account1.save!

      assert_equal @redis_client.get(TrialExtra.redis_key(account1.id)), "{}"
      assert_equal account1.send(:redis_trial_extras), {}
      assert_equal account1.trial_extras, []
    end
  end

  describe "#add_google_domain" do
    before do
      @account = FactoryBot.build(:account)
      @account.save!
    end

    it "does nothing when route.gam_domain already exists" do
      @account.route.update_attribute(:gam_domain, 'foo.com')
      @account.settings.google_apps_domain = 'bar.com'
      @account.settings.save!
      @account.send(:add_google_domain)

      assert_equal 'foo.com', @account.route.gam_domain
    end

    it "updates route.gam_domain from account settings if not nil" do
      @account.settings.google_apps_domain = 'bar.com'
      @account.settings.save!
      @account.send(:add_google_domain)

      assert_equal 'bar.com', @account.route.gam_domain
    end

    it "updates route.gam_domain from redis if not nil" do
      Zendesk::RedisStore.redis_client = FakeRedis::Redis.new
      redis_client = Zendesk::RedisStore.client
      redis_client.set(@account.google_apps_domain_redis_key, 'baz.com')
      @account.send(:add_google_domain)

      assert_equal 'baz.com', @account.route.gam_domain
    end
  end

  describe '#setup_chat_to_support_xsell' do
    let(:chat_app_id) { 55 }

    [true, false].each do |is_precreated|
      describe "called on a #{is_precreated ? '' : 'non-'}precreated account" do
        it "created by chat xsell : installs Zendesk Chat app" do
          ZopimIntegration.expects(:app_market_id).returns(chat_app_id)
          ZendeskAPI::AppInstallation.expects(:create!).with(
            anything,
            app_id: chat_app_id,
            settings: {
              name: ZopimIntegration.app_market_name
            }
          )
          initialize_account(
            trial_extras: {
              xsell_source: "zendesk_chat"
            }
          )
          @account = @account_initializer.send(
            is_precreated ? :find_or_precreate_account : :new_account
          )
          @account.save!
        end

        it "created not by chat xsell : does not install Zendesk Chat app" do
          ZendeskAPI::AppInstallation.expects(:create!).never
          initialize_account
          @account = @account_initializer.send(
            is_precreated ? :find_or_precreate_account : :new_account
          )
          @account.save!
        end
      end
    end
  end

  describe '#sync_entitlements_and_roles' do
    describe 'on a precreated account' do
      it 'syncs roles and entitlements' do
        Omnichannel::AccountUsersEntitlementsSyncJob.expects(:enqueue).once

        initialize_account.save!
      end
    end

    describe 'on a non-precreated account' do
      before do
        PreAccountCreation.stubs(:precreation_supported?).returns(false)
      end

      it 'does not sync roles and entitlements' do
        Omnichannel::AccountUsersEntitlementsSyncJob.expects(:enqueue).never

        initialize_account.save!
      end
    end
  end

  describe '#start_suite_trial' do
    let(:suite) { true }
    let(:account) do
      account = FactoryBot.build(:account)
      account.save!
      account.settings.suite_trial = suite
      account.settings.save!

      account
    end

    describe 'as a support trial' do
      let(:suite) { false }

      it 'should not enqueue a suite trial job' do
        SuiteTrialJob.expects(:enqueue).never
        account.send(:start_suite_trial)
      end
    end

    describe 'as a suite trial' do
      it 'should enqueue a suite trial job' do
        SuiteTrialJob.expects(:enqueue)
        account.send(:start_suite_trial)
      end

      describe 'with a SPP account' do
        before do
          account.stubs(:spp?).returns(true)
        end

        it 'should not enqure a suite trial job' do
          SuiteTrialJob.expects(:enqueue).never
          account.send(:start_suite_trial)
        end
      end
    end
  end
end
