require_relative "../support/test_helper"

SingleCov.covered!

describe Alert do
  fixtures :users

  subject do
    Alert.create { |a| a.value = "HI!" }
  end

  before do
    @user = users(:minimum_admin)
    @account = @user.account
    @account.stubs(:shard_id).returns(3)
    @account.stubs(:id).returns(12)
    @account.stubs(:subscription).returns(Object.new.tap do |s|
      s.stubs(:plan_type).returns(1)
      s.stubs(:account_type).returns("trial")
    end)
  end

  describe ".for" do
    describe "when there are no alerts" do
      it "return empty array" do
        assert_equal [], Alert.for(@user)
      end
    end

    describe "when there are alerts" do
      before do
        @a, @b, @c, @d = [['classic'], ['lotus'], ['classic', 'lotus'], 'all'].map do |interfaces|
          Alert.create! do |a|
            a.value = 'foo'
            a.is_active = true
            a.interfaces = interfaces
          end
        end
      end

      it "allow segmenting by interface" do
        assert_equal [], Alert.for(users(:minimum_end_user))
        assert_equal [@a, @b, @c, @d], Alert.for(@user)
        assert_equal [@b, @c, @d], Alert.for(@user, interface: 'lotus')
        assert_equal [@a, @c, @d], Alert.for(@user, interface: 'classic')
      end

      describe "when there are custom alerts" do
        before do
          @custom = Alert.create! do |a|
            a.interfaces = "boogaloo"
            a.is_active = true
            a.custom = true
            a.plans = "all"
            a.roles = "all"
            a.pods = "all"
            a.account_types = "all"
            a.shards = "all"
            a.link_url = "https://www.google.com/"
            a.value = "The message."
          end
        end

        describe "that are dismissed" do
          before do
            AlertDismissal.create!(user: @user, alert: @custom, account: @account)
          end

          it "not be retrieved" do
            assert_equal [@d], Alert.for(@user, custom: [:boogaloo], interface: "not used")
          end
        end

        describe "that are not dismissed" do
          it "be retrieved" do
            assert_equal [@d, @custom], Alert.for(@user, custom: ["boogaloo"], interface: "not used")
          end
        end
      end
    end
  end

  describe "#custom_interface_matches?" do
    describe "when there is no interface specfied" do
      subject { @alert = Alert.new }
      describe "and there is a custom interface provided" do
        it "be false" do
          refute subject.send(:custom_interface_matches?, "hello")
        end
      end

      describe "and there is no custom interface provided" do
        it "be false" do
          refute subject.send(:custom_interface_matches?, nil)
        end
      end
    end

    describe "when there is an interface specified" do
      subject { @alert = Alert.new { |a| a.interfaces = "hello" } }

      describe "and there is a custom interface provided" do
        describe "and they have an intersection" do
          it "be true" do
            assert subject.send(:custom_interface_matches?, ["hello", "there"])
          end
        end

        describe "and they do not have an intersection" do
          it "be false" do
            refute subject.send(:custom_interface_matches?, ["mjallo", "there"])
          end
        end
      end

      describe "and there is no custom interface provided" do
        it "be false" do
          refute subject.send(:custom_interface_matches?, nil)
        end
      end
    end
  end

  describe "#show_for?" do
    describe "an inactive Alert targeting all" do
      before do
        subject.is_active = false
      end

      it 'return false' do
        refute subject.show_for?(@user)
      end
    end

    describe "an active alert" do
      before do
        subject.is_active = true
      end

      describe "targeting all" do
        it 'return true' do
          assert subject.show_for?(@user)
        end
      end

      describe "targeting the account's shard" do
        before do
          subject.shards = [1, 2, 3]
        end
        it 'return true' do
          assert subject.show_for?(@user)
        end
      end

      describe "not targeting the account's shard" do
        before do
          subject.shards = [1, 2, 4]
        end
        it 'return false' do
          refute subject.show_for?(@user)
        end
      end

      describe "targeting pod" do
        before do
          subject.pods = [1, 2, 3]
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(2)
        end
        it 'return true' do
          assert subject.show_for?(@user)
        end
      end

      describe "not targeting pod" do
        before do
          subject.pods = [1, 3]
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(2)
        end
        it 'return false' do
          refute subject.show_for?(@user)
        end
      end

      describe "targeting the user's plan" do
        before do
          subject.plans = [1]
        end
        it 'return true' do
          assert subject.show_for?(@user)
        end
      end

      describe "not targeting the user's plan" do
        before do
          subject.plans = [2]
        end
        it 'return false' do
          refute subject.show_for?(@user)
        end
      end

      describe "targeting the user's role" do
        before do
          subject.roles = ['Admin']
        end
        it 'return true' do
          assert subject.show_for?(users(:minimum_admin_not_owner))
        end
      end

      describe "targeting owners" do
        before do
          subject.roles = ['Owner']
        end
        it 'return true' do
          assert subject.show_for?(@user)
        end
      end

      describe "not targeting the user's role" do
        before do
          subject.roles = ['End-user']
        end
        it 'return false' do
          refute subject.show_for?(@user)
        end
      end

      describe "targeting the user's account type" do
        before do
          subject.account_types = ['trial']
        end
        it 'return true' do
          assert subject.show_for?(@user)
        end
      end

      describe "not targeting the user's account type" do
        before do
          subject.roles = ['customer']
        end
        it 'return false' do
          refute subject.show_for?(@user)
        end
      end

      describe "dismissed by user" do
        before do
          AlertDismissal.create user: @user, account: @account, alert: subject
        end
        it 'return false' do
          refute subject.show_for?(@user)
        end
      end
    end
  end
end
