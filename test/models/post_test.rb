require_relative "../support/test_helper"

SingleCov.covered! uncovered: 8

describe Post do
  fixtures :posts, :entries, :users, :user_identities, :forums, :tokens, :attachments, :accounts

  describe "A Post" do
    should have_db_column(:sanitize_on_display).of_type(:boolean)
    should_validate_presence_of :body, :entry_id, :user_id

    it "does not serialize sanitize_on_display" do
      assert subject.send(:serialization_options)[:except].include?(:sanitize_on_display)
    end
  end

  describe 'A post for rss' do
    before do
      @post = posts(:fish_post_1)
    end

    it 'provides an rss title' do
      expected = "\"#{@post.entry.title}\" in #{@post.forum.title} - comment added by #{@post.user.name}"
      assert_equal(expected, @post.rss_title)
    end

    it 'provides rss guid parts' do
      assert_equal([@post.entry.forum_id, @post.entry.id, @post.id], @post.rss_guid_parts)
    end

    it 'provides a creator' do
      assert_equal(@post.user, @post.creator)
    end
  end

  describe "Creating a new Post" do
    before do
      @post = Post.create!(
        body: "Post #{Time.now.to_i}",
        entry_id: entries(:ponies).id,
        user_id: users(:minimum_end_user).id
      )
    end

    should_change("updated_at on its Entry") { entries(:ponies).reload.updated_at }

    it "sets updater on its Entry to the Post's user" do
      assert_equal @post.user, @post.entry.reload.updater
    end

    it 'strips script' do
      @post.update_attribute(:body, "Post<script>alert('baddie');</script>body")
      assert_equal "Postbody", @post.body
    end

    it 'allows html' do
      body = '<strong>Hi!</strong>'
      @post.update_attribute(:body, body)
      assert_equal body, @post.reload.body
    end

    it 'sanitizes for agents' do
      agent_post = Post.create!(
        body: "Post<script>alert('itsok');</script>body",
        entry_id: entries(:ponies).id,
        user_id: users(:minimum_agent).id
      )
      assert_equal "Postbody", agent_post.body
    end
  end

  describe "Saving a post" do
    before do
      @agent = users(:minimum_agent)
      @user  = users(:minimum_end_user)
      @html_text = '<strong>htmlok</strong>'
      @script_text = "bad<script>alert('baddie');</script>text"
      @post = posts(:fish_post_1)
    end

    describe 'existing record' do
      it 'sanitizes body and title' do
        @post.update_attribute(:body, @script_text)
        assert_equal "badtext", @post.reload.body
      end
      it 'allows html' do
        @post.update_attribute(:body, @html_text)
        assert_equal @html_text, @post.reload.body
      end
    end
  end

  describe "Creating a new Post with a set uploads attribute" do
    before do
      @token = tokens(:minimum_upload_token)
      @expected_attachment_ids = @token.attachments.pluck(:id)
      @post = Post.create!(
        body: "Post #{Time.now.to_i}",
        entry_id: entries(:ponies).id,
        user_id: users(:minimum_end_user).id,
        uploads: @token.value
      )
    end

    it "adds Attachments from the token specified by uploads to the Post" do
      assert_same_elements @expected_attachment_ids, @post.attachments.pluck(:id)
    end
  end

  describe "An informative Post" do
    before do
      @post  = posts(:questionable_answer)
      @entry = @post.entry
    end

    it "marks entry as unanswered" do
      assert_equal EntryFlagType::UNKNOWN, @entry.reload.flag
      @post.informative = true
      @post.save!
      assert_equal EntryFlagType::ANSWERED, @entry.reload.flag
      @post.soft_delete
      assert_equal EntryFlagType::UNKNOWN, @entry.reload.flag
    end
  end

  describe "#htmlify" do
    before do
      @post = posts(:questionable_answer)
      @post.body = "abc\r\n123"
    end

    it "calls htmlify if has is_mobile" do
      @post.expects(:htmlify)
      @post.is_mobile = true
      @post.save!
    end

    it "turns carriage returns into <br/>" do
      @post.htmlify
      assert_equal "abc<br/>123", @post.body
    end
  end

  describe "Updating an existing Post" do
    before do
      @post = posts(:sticky_reply)
      @post.update_attributes(body: "Updated Post #{Time.now.to_i}")
    end

    should_change("updated_at on its Entry") { entries(:sticky).reload.updated_at }

    it "sets updater on its Entry to the Post's user" do
      assert_equal @post.user, @post.entry.reload.updater
    end
  end

  describe "#to_xml" do
    before do
      @post = posts(:sticky_reply)
    end

    it "does not contain is_informative" do
      assert @post.to_xml.index('is_informative').nil?
    end
  end

  describe "with post moderation" do
    let(:account) { accounts(:minimum) }
    let(:entry) { account.entries.first }
    let(:user) do
      users(:minimum_end_user).tap do |user|
        user.stubs(whitelisted_from_moderation?: false)
      end
    end

    before do
      Entry.reset_counters(entry.id, :posts)

      account.settings.moderated_posts = true
      account.settings.save!
    end

    describe "on creation" do
      let(:post) { entry.posts.create!(body: "test", user: user) }

      describe "with an end user" do
        before { post }

        before_should "not call store_event" do
          ForumObserver.expects(:store_event).never
        end

        it "does not subscribe the submitter" do
          refute entry.subscribed?(user)
        end

        it "is deleted" do
          assert post.deleted?
        end

        it "is suspended?" do
          assert post.suspended?
        end

        it 'resets the entries cached counter' do
          entry.reload
          assert_equal entry.posts.count(:all), entry.posts_count
        end
      end

      describe "with a throttled end user" do
        let(:user) do
          users(:minimum_end_user).tap do |user|
            user.stubs(throttled_posting?: true)
          end
        end

        it "is invalid" do
          assert_raise(ActiveRecord::RecordInvalid) { post }
        end
      end

      describe "with a whitelisted end user" do
        let(:user) do
          users(:minimum_end_user).tap do |user|
            user.stubs(whitelisted_from_moderation?: true)
          end
        end

        before { post }

        before_should "call store_event" do
          ForumObserver.expects(:store_event).at_least_once
        end

        it "subscribes the submitter" do
          assert entry.subscribed?(user)
        end

        it "does not be deleted" do
          refute post.deleted?
        end

        it "does not be suspended?" do
          refute post.suspended?
        end

        it 'resets the entries cached counter' do
          entry.reload
          assert_equal entry.posts.count(:all), entry.posts_count
        end
      end

      describe "with an agent" do
        let(:user) { users(:minimum_agent) }

        before { post }

        before_should "call store_event" do
          ForumObserver.expects(:store_event).at_least_once
        end

        it "subscribes the submitter" do
          assert entry.subscribed?(user)
        end

        it "does not be deleted" do
          refute post.deleted?
        end

        it "does not be suspended?" do
          refute post.suspended?
        end

        it 'resets the entries cached counter' do
          entry.reload
          assert_equal entry.posts.count(:all), entry.posts_count
        end
      end
    end

    describe "on suspend" do
      let(:post) { posts(:ponies_post_1) }

      before { post.soft_delete! }

      it "is deleted" do
        assert post.deleted?
      end

      it "does not be suspended?" do
        refute post.suspended?
      end

      it "is findable" do
        Post.with_deleted do
          assert_equal post, Post.find(post.id)
        end
      end

      it 'resets the entries cached counter' do
        entry.reload
        assert_equal entry.posts.count(:all), entry.posts_count
      end
    end

    describe "on update" do
      let(:post) { posts(:ponies_post_1) }

      before do
        post.body = "new body"
        post.save!
      end

      it "does not be deleted" do
        refute post.deleted?
      end

      it "does not be suspended?" do
        refute post.suspended?
      end

      it 'resets the entries cached counter' do
        entry.reload
        assert_equal entry.posts.count(:all), entry.posts_count
      end
    end

    describe "on unsuspend" do
      let(:post) { entry.posts.create!(body: "test", user: user) }

      before do
        post.unsuspend!
      end

      before_should "call store_event" do
        ForumObserver.expects(:store_event).with(instance_of(Post)).once
        ForumObserver.expects(:store_event).with(instance_of(Watching)).once
      end

      it "subscribes the submitter" do
        assert entry.subscribed?(user)
      end

      it "does not be deleted" do
        refute post.deleted?
      end

      it "does not be suspended" do
        refute post.suspended?
      end

      it 'resets the entries cached counter' do
        entry.reload
        assert_equal entry.posts.count(:all), entry.posts_count
      end
    end

    describe "on deletion" do
      let(:entry) { entries(:ponies) }
      let(:post) { entry.posts.create!(body: "test", user: user) }

      before do
        post.soft_delete!
      end

      it "is deleted" do
        assert post.deleted?
      end

      it "does not be suspended" do
        refute post.suspended?
      end

      it 'resets the entries cached counter' do
        entry.reload
        assert_equal entry.posts.count(:all), entry.posts_count
      end
    end
  end

  it "tests should edit own post" do
    users(:minimum_end_user).can?(:edit, posts(:sticky_rebuttal))
  end

  it "tests should edit post as admin" do
    users(:minimum_admin).can?(:edit, posts(:sticky_rebuttal))
  end

  it "tests should not edit post in own entry" do
    refute users(:minimum_end_user).can?(:edit, posts(:sticky_reply))
  end

  it "tests non moderator cannot edit foreign posts" do
    assert users(:minimum_agent).can?(:edit, posts(:sticky_reply))
    users(:minimum_agent).update_attribute(:is_moderator, false)
    refute users(:minimum_agent).can?(:edit, posts(:sticky_reply))
  end

  it "tests access rights" do
    assert users(:minimum_end_user).can?(:view, posts(:sticky_reply))
    assert users(:minimum_agent).can?(:view, posts(:sticky_reply))

    users(:minimum_end_user).stubs(organization_memberships: stub(exists?: false))
    refute users(:minimum_end_user).can?(:view, posts(:fish_post_1))
  end

  it "uses Zendesk::ForumSanitizer" do
    assert Post.include?(Zendesk::ForumSanitizer)
  end
end
