require_relative "../../support/job_helper"
require_relative "../../support/attachment_test_helper"

SingleCov.covered! uncovered: 3

describe 'UserXmlExportJob' do
  fixtures :accounts, :users

  before do
    @minimum_admin = users(:minimum_admin)
  end

  describe "class methods" do
    before do
      @account = @minimum_admin.account
      @subscription = Subscription.new
    end

    describe "#perform" do
      before { Subscription.any_instance.stubs(:has_user_xml_export?).returns(true) }

      it "creates an expirable_attachment and send an email" do
        stub_request(:put, %r{\.amazonaws.com/data/expirable_attachments/.*xml.zip})
        assert_difference("ExpirableAttachment.count") do
          assert_emails_sent { UserXmlExportJob.perform(@minimum_admin.account_id, @minimum_admin.id) }
        end
      end

      describe "job is not accessible" do
        before do
          Zendesk::Export::Configuration.any_instance.
            expects(:type_accessible?).
            with(UserXmlExportJob).
            returns(false)
        end

        it "does not create an expirable_attachment" do
          refute_difference("ExpirableAttachment.count(:all)") do
            UserXmlExportJob.perform(@minimum_admin.account_id, @minimum_admin.id)
          end
        end
      end
    end

    describe "#identifier" do
      it "returns a identifier consisting of the account id use by resque-throttle" do
        assert_equal "account_id:#{@minimum_admin.account.id}", UserXmlExportJob.identifier(@minimum_admin.account_id, @minimum_admin.id)
      end
    end

    describe "#available_for?" do
      it "returns true if available for this account" do
        Subscription.any_instance.stubs(:has_user_xml_export?).returns(true)
        assert UserXmlExportJob.available_for?(@minimum_admin.account)
      end

      it "returns false if not available for this account" do
        Subscription.any_instance.stubs(:has_user_xml_export?).returns(false)
        refute UserXmlExportJob.available_for?(@minimum_admin.account)
      end
    end

    describe "#latest" do
      it "returns the latest expirable attachment that corresponds to a user_xml_export_job" do
        attachment = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", @account, created_via: UserXmlExportJob.name)
        assert_equal attachment, UserXmlExportJob.latest(@account)
      end
    end
  end
end
