require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 3

describe GooddataUserReprovisioningJob do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }

  describe '.work' do
    before do
      Zendesk::StatsD::Client.any_instance.stubs(:increment)
      Zendesk::StatsD::Client.any_instance.stubs(:histogram)
      Zendesk::Gooddata::UserProvisioning.stubs(:reprovision_user)
    end

    describe 'when an integration exists for the account' do
      before do
        account.create_gooddata_integration(
          status: 'complete',
          project_id: 'project_id'
        )
      end

      it 'logs the run' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')

        GooddataUserReprovisioningJob.work(account.id, user.id)
      end

      it 'reprovisions the user' do
        Zendesk::Gooddata::UserProvisioning.expects(:reprovision_user).with(user)

        GooddataUserReprovisioningJob.work(account.id, user.id)
      end

      it 'logs the running time' do
        Zendesk::StatsD::Client.any_instance.expects('histogram').
          with('execution_time', kind_of(Float))

        GooddataUserReprovisioningJob.work(account.id, user.id)
      end

      describe 'and reprovision fails' do
        before do
          Zendesk::Gooddata::UserProvisioning.expects(:reprovision_user).raises(RuntimeError)
        end

        it 'logs the failure' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed')

          assert_raises RuntimeError do
            GooddataUserReprovisioningJob.work(account.id, user.id)
          end
        end
      end
    end

    describe 'when an integration does not exist for the account' do
      it 'logs the run' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')

        GooddataUserReprovisioningJob.work(account.id, user.id)
      end

      it 'does not reprovision user' do
        Zendesk::Gooddata::UserProvisioning.expects(:reprovision_user).never

        GooddataUserReprovisioningJob.work(account.id, user.id)
      end
    end

    describe 'when zendesk user does not exist' do
      let(:user) { stub('user', id: 999) }

      it 'log the run' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')
        GooddataUserReprovisioningJob.work(account.id, user.id)
      end

      it 'does not reprovision user' do
        Zendesk::Gooddata::UserProvisioning.expects(:reprovision_user).never

        GooddataUserReprovisioningJob.work(account.id, user.id)
      end
    end
  end

  describe '.args_to_log' do
    let(:account_id) { 1 }
    let(:user_id) { 2 }

    it 'logs the account and user id' do
      expected = {
        account_id: account_id,
        zendesk_user_id: user_id,
        audit_id: nil
      }

      assert_equal expected, GooddataUserReprovisioningJob.args_to_log(account_id, user_id)
    end
  end
end
