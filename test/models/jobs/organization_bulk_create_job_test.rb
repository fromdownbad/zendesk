require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe OrganizationBulkCreateJob do
  include CustomFieldsTestHelper
  fixtures :accounts, :users
  include DomainEventsHelper

  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    Organization.any_instance.stubs(domain_event_publisher: domain_event_publisher)
    Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
    # don't use these fixtures
    CustomField::Field.without_arsi.delete_all
    CustomField::Value.without_arsi.delete_all
    @account = accounts(:minimum)
    @organizations = [{name: "Test Organization"}, {name: "Test Organization2"}]
    @job = OrganizationBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: @account.id,
      user_id: @account.owner_id,
      organizations: @organizations)
  end

  describe "With current organization having suitable permissions" do
    before do
      Organization.instance_variable_set(:@reserved_global_uids, [])
      Organization.expects(:generate_many_uids).with(2).returns([1234, 1567])
    end

    it "creates the organizations using reserved global uids" do
      @job.perform
      organization1 = @account.organizations.find_by_name("Test Organization")
      organization2 = @account.organizations.find_by_name("Test Organization2")

      assert organization1
      assert organization2
      assert_equal 1234, organization1.id
      assert_equal 1567, organization2.id
    end

    describe "when the organizations hashes contain valid domain_names params" do
      before do
        @organizations[0][:domain_names] = "northuldra.gov arendelle.gov"
        @organizations[1][:domain_names] = "elsa@northuldra.gov anna@arendelle.gov"
        @job = OrganizationBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037920",
          account_id: @account.id,
          user_id: @account.owner_id,
          organizations: @organizations)
      end

      it "adds organization_domains and organization_emails associations to the new organizations" do
        @job.perform
        organization1 = @account.organizations.find_by_name("Test Organization")
        organization2 = @account.organizations.find_by_name("Test Organization2")

        assert_equal 2, organization1.send(:organization_domains).size
        assert_empty organization1.send(:organization_emails)

        assert_empty organization2.send(:organization_domains)
        assert_equal 2, organization2.send(:organization_emails).size
      end
    end
  end

  describe "With current organization having custom fields" do
    before do
      # create custom fields
      create_organization_custom_field!("field_checkbox", "title", "Checkbox")
      create_organization_custom_field!("field_text", "title", "Text")
      # update organizations
      @organizations[0][:organization_fields] = {"field_checkbox" => true}
      @organizations[1][:organization_fields] = {"field_text" => "Go Giants!"}
      @job.options[:organizations] = @organizations
    end

    it "creates the organizations" do
      @job.perform
      org1 = @account.organizations.find_by_name("Test Organization")
      assert org1
      assert_equal({"field_checkbox" => true, "field_text" => nil}, org1.custom_field_values.as_json(account: @account))

      org2 = @account.organizations.find_by_name("Test Organization2")
      assert org2
      assert_equal({"field_checkbox" => false, "field_text" => "Go Giants!"}, org2.custom_field_values.as_json(account: @account))
    end

    describe "organization tags changed domain events" do
      before do
        @organizations[0][:tags] = %w[test]
        @organizations[1][:tags] = %w[test2]
      end

      it "emits a tags changed event" do
        @job.perform
        org1 = @account.organizations.find_by_name("Test Organization")
        assert org1

        events = decoded_organization_events(domain_event_publisher.events, :tags_changed)
        assert_equal 2, events.size
        assert_equal %w[test test2], events.map { |e| e.tags_changed.tags_added.tags }.flatten
      end
    end
  end
end
