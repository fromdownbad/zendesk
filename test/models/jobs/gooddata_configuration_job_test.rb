require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 3

describe GooddataConfigurationJob do
  fixtures :accounts, :users

  let(:account)       { accounts(:minimum) }
  let(:user)          { users(:minimum_agent) }

  describe ".work" do
    let(:integration) { stub('gooddata integration') }
    let(:integration_provisioning) { stub('integration provisioning') }

    before do
      Zendesk::Gooddata::IntegrationProvisioning.stubs(:new).
        with(account).
        returns(integration_provisioning)

      Zendesk::StatsD::Client.any_instance.stubs(:histogram)
      Zendesk::StatsD::Client.any_instance.stubs(:increment)
    end

    describe "when a GoodData integration exists" do
      describe "and it is v2" do
        before do
          integration_provisioning.stubs(:gooddata_integration).returns(integration)
          GooddataConfigurationJob.work(account.id)
        end

        before_should "configure the integration" do
          integration_provisioning.expects(:set_project_api_domains)
          integration_provisioning.expects(:set_project_time_zone)
          integration_provisioning.expects(:set_project_title)
        end
      end
    end

    describe "when a GoodData integration doesn't exist" do
      before do
        integration_provisioning.stubs(:gooddata_integration)
        GooddataConfigurationJob.work(account.id)
      end

      before_should "not try to configure the integration" do
        integration_provisioning.expects(:set_project_api_domains).never
        integration_provisioning.expects(:set_project_time_zone).never
        integration_provisioning.expects(:set_project_title).never
      end
    end

    describe "when the job fails" do
      before do
        integration_provisioning.stubs(:gooddata_integration).returns(integration)
        integration_provisioning.expects(:set_project_api_domains).raises(RuntimeError)
      end

      it "logs the failure" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed')

        assert_raises RuntimeError do
          GooddataConfigurationJob.work(account.id)
        end
      end
    end
  end

  describe ".args_to_log" do
    let(:account_id) { 1 }

    before { GooddataConfigurationJob.stubs(:audit_id).with(3).returns(4) }

    it "logs the account and user IDs" do
      assert_equal(
        {
          account_id: account_id,
          audit_id: 4
        },
        GooddataConfigurationJob.args_to_log(account_id, 3)
      )
    end
  end
end
