require_relative "../../support/job_helper"
require_relative "../../support/attachment_test_helper"

SingleCov.covered! uncovered: 1

describe 'ReportJob' do
  fixtures :accounts, :users

  before do
    @minimum_admin = users(:minimum_admin)
    @account = @minimum_admin.account
    params = { sets: { "1" => { conditions: {}, legend: 'Working tickets', state: 'working' } } }

    @report = Report.create!(title: "report title", author: @minimum_admin,
                             sets: params[:sets], account: @account, relative_interval_in_days: 30)
  end

  describe "class methods" do
    before do
      @account = @minimum_admin.account
      @subscription = Subscription.new
    end

    describe "#perform" do
      it "writes the results into the report and send an email" do
        @report.reload
        assert @report.result.blank?
        assert_nil @report.last_run_at
        assert_emails_sent { ReportJob.perform(@account.id, @minimum_admin.id, @report.id) }
        assert @report.reload.result.present?
        assert @report.reload.last_run_at
      end

      it "raises an exception if the requester is not found" do
        assert_raises(ActiveRecord::RecordNotFound) { ReportJob.perform(@account.id, -20, @report.id) }
      end

      it "raises an exception if the report is not found" do
        assert_raises(ActiveRecord::RecordNotFound) { ReportJob.perform(@account.id, @minimum_admin.id, -20) }
      end
    end

    describe "#identifier" do
      it "returns a identifier consisting of the account id and report id use by resque-throttle" do
        assert_equal "account_id:#{@account.id}-report_id:#{@report.id}",
          ReportJob.identifier(@minimum_admin.account.id, @minimum_admin.id, @report.id)
      end
    end

    describe "#available_for?" do
      it "returns true" do
        assert ReportJob.available_for?(@minimum_admin.account)
      end
    end

    describe "#latest" do
      it "returns nil" do
        assert_nil ReportJob.latest(@account)
      end
    end
  end
end
