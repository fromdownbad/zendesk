require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 2

describe PushNotificationSdkDeregistrationJob do
  fixtures :accounts, :mobile_sdk_apps, :users

  describe "#work" do
    let(:account) { accounts(:minimum_sdk) }
    let(:mobile_sdk_app) { mobile_sdk_apps(:minimum_sdk) }
    let(:urban_airship_app_key) { "urban_airship_app_key" }
    let(:urban_airship_master_secret) { "urban_airship_master_secret" }

    before do
      mobile_sdk_app.settings.push_notifications_type = MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP
      mobile_sdk_app.settings.urban_airship_key = urban_airship_app_key
      mobile_sdk_app.settings.urban_airship_master_secret = urban_airship_master_secret
      mobile_sdk_app.save!

      PushNotifications::SdkAuthenticatedDeviceIdentifier.any_instance.stubs(:register).returns(true)
      @device = PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(
        token: "foo",
        device_type: "iphone",
        account: account,
        user: users(:minimum_end_user),
        mobile_sdk_app: mobile_sdk_app
      )
    end

    it "loads the correct Urban Airship credentials" do
      PushNotifications::UrbanAirshipClient.expects(:new).with(
        urban_airship_app_key,
        urban_airship_master_secret,
        statsd_tags: ["name:sdk_deregistration"]
      ).returns(
        mock(unregister: [stub])
      )

      PushNotificationSdkDeregistrationJob.work(
        'account_id' => account.id,
        'mobile_sdk_app_id' => mobile_sdk_app.id,
        'identifier_id' => @device.id
      )
    end

    it "deregisters the device" do
      PushNotifications::UrbanAirshipClient.any_instance.expects(:unregister).with(
        @device.token,
        @device.device_type
      ).returns(stub)

      PushNotificationSdkDeregistrationJob.work(
        'account_id' => account.id,
        'mobile_sdk_app_id' => mobile_sdk_app.id,
        'identifier_id' => @device.id
      )
    end

    # This is to test a fix for an issue in Production https://zendesk.atlassian.net/browse/MSS-927
    it "rescues & logs when a device is already de-registered (ActiveRecord::RecordNotFound)" do
      @device.destroy

      Rails.logger.expects(:info).with("Could not find the device id #{@device.id} for account: #{account.id}")

      PushNotificationSdkDeregistrationJob.work(
        'account_id' => account.id,
        'mobile_sdk_app_id' => mobile_sdk_app.id,
        'identifier_id' => @device.id
      )
    end
  end
end
