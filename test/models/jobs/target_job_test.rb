require_relative "../../support/job_helper"
require 'faraday'

SingleCov.covered!

describe "TargetJob" do
  fixtures :targets, :events, :accounts, :tickets, :users,
    :account_property_sets
  include PerformJob

  describe "class methods" do
    describe "#perform" do
      before do
        @target = targets(:email_valid)
        audit = events(:create_audit_for_minimum_ticket_1)
        @external = External.new(via_id: ViaType.RULE, via_reference_id: 0, resource: @target.id, body: "Send and email to target")
        audit.events << @external
      end

      describe "indicate failure by raising exception rather than logging" do
        it "raises if the external is not valid (value is blank)" do
          TargetJob.any_instance.expects(:external_valid?).returns(false)
          External.expects(:find).with(anything).returns(@external)
          TargetJob.expects(:resque_warn).with(regexp_matches(
            /Account #{@external.account_id}: Error in TargetJob processing external #{@external.id}: Invalid external/
          ))
          perform_job(TargetJob, @external.account_id, @external.id)
        end

        it "raises if the target is not valid or active" do
          TargetJob.any_instance.expects(:target_valid?).returns(false)
          External.expects(:find).with(anything).returns(@external)
          TargetJob.expects(:resque_warn).with(regexp_matches(
            /Account #{@external.account_id}: Error in TargetJob processing external #{@external.id}: Invalid target/
          ))
          perform_job(TargetJob, @external.account_id, @external.id)
        end

        describe "errors" do
          before do
            EmailTarget.any_instance.expects(:send_message).raises(ArgumentError.new)
          end

          it "logs the error with the resque logger" do
            TargetJob.expects(:resque_warn)
            perform_job(TargetJob, @external.account_id, @external.id)
          end

          it "increases the target failure count if an exception is encountered while trying to send the target a message" do
            original_failures = @target.failures
            perform_job(TargetJob, @external.account_id, @external.id)
            assert_equal @target.reload.failures, original_failures + 1
          end

          describe "over the limit of failures" do
            before do
              @target.update_attribute(:failures, @target.account.settings.max_target_failures)
            end

            it "deactivates the target" do
              original_failures = @target.failures
              perform_job(TargetJob, @external.account_id, @external.id)
              refute @target.reload.is_active
              assert_equal @target.reload.failures, original_failures + 1
            end

            it "thes number of disabled emails sent" do
              ActionMailer::Base.perform_deliveries = true
              perform_job(TargetJob, @external.account_id, @external.id)
              assert_equal "Target 'Valid Email' disabled", ActionMailer::Base.deliveries.last.subject
            end
          end
        end

        describe "deadlocks" do
          before do
            message = "Mysql2::Error: Lock wait timeout exceeded; try restarting transaction"
            EmailTarget.any_instance.expects(:send_message).raises(ActiveRecord::StatementInvalid.new(message))
          end

          it "does not try to increase the failure count" do
            original_failures = @target.failures
            perform_job(TargetJob, @external.account_id, @external.id)
            assert_equal original_failures, @target.reload.failures
          end
        end
      end

      describe "success" do
        before do
          ActionMailer::Base.perform_deliveries = true
          perform_job(TargetJob, @external.account_id, @external.id)
        end

        should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }

        it "sends the email as defined in the target" do
          email = ActionMailer::Base.deliveries.last

          assert_equal [@target.email], email.to
          assert_equal @target.subject, email.subject
          email.joined_bodies.must_include "This email is a service from minimum"
        end

        it "increases the number of sent messages in target" do
          @target.reload
          assert_equal 2, @target.sent
        end
      end

      describe "email targets" do
        it "does not be rendered in the job" do
          job = TargetJob.new(@external)
          assert_nil job.message
          job.stubs(:target).returns(targets(:email_valid))
          assert_nil job.message
        end
      end

      describe "target job is failed due to different errors" do
        before do
          @ticket = tickets(:minimum_1)
          @target = targets(:url_v2)

          @external = External.new(
            via_id: @ticket.audits.last.via_id,
            author: @ticket.account.owner,
            via_reference_id: 0,
            resource: @target.id,
            body: "HTTP URL v2 target",
            account: @ticket.account,
            ticket: @ticket,
            audit: @ticket.audits.last,
            value: @target.id
          )
          @external.save(validate: false)

          @obj_hash = {
            account_id: @external.account_id,
            target_id: @target.id,
            target_type: @target.type,
            total_sent: @target.sent,
            total_failures: @target.current_failures,
            ticket_id: @external.ticket.id,
            event_id: @external.id,
          }

          @statsd_client = stub_for_statsd
          TargetJob.any_instance.stubs(:statsd_client).returns(@statsd_client)
          TargetJob.logger.expects(:append_attributes).with(
            target_type: @target.type
          )
        end

        it "should faraday http client timeout causes job failure" do
          # setup expectations
          e = Faraday::TimeoutError.new
          @statsd_client.expects(:increment).with("count.failure", tags: %w[source:url_target_v2 reason:request_timeout])
          UrlTargetV2.any_instance.expects(:send_message_with_retry).raises(e)
          TargetJob.expects(:resque_log).with(["executing target", @obj_hash])
          TargetJob.expects(:resque_log).with(["timeout", @obj_hash])
          UrlTargetV2.any_instance.expects(:message_failed).with(e, all_of(instance_of(External), responds_with(:id, @external.id)))
          TargetJob.expects(:resque_warn).with(regexp_matches(
            /Account #{@external.account_id}: Error in TargetJob processing external #{@external.id}: Timeout occurred. Request exceeded 10 seconds./
          ))

          # invoke the method to trigger the timeout exception
          TargetJob.work(@target.account_id, @external.id)
        end

        it "should generic http client timeout causes job failure" do
          # setup expectations
          e = Timeout::Error.new
          @statsd_client.expects(:increment).with("count.failure", tags: %w[source:url_target_v2 reason:request_timeout])
          UrlTargetV2.any_instance.expects(:send_message_with_retry).raises(e)
          TargetJob.expects(:resque_log).with(["executing target", @obj_hash])
          TargetJob.expects(:resque_log).with(["timeout", @obj_hash])
          UrlTargetV2.any_instance.expects(:message_failed).with(e, all_of(instance_of(External), responds_with(:id, @external.id)))

          TargetJob.expects(:resque_warn).with(regexp_matches(
            /Account #{@external.account_id}: Error in TargetJob processing external #{@external.id}: Timeout occurred. Request exceeded 10 seconds./
          ))

          # invoke the method to trigger the timeout exception
          TargetJob.work(@target.account_id, @external.id)
        end

        it "should failed response error causes job failure" do
          # setup expectations
          e = ::HTTPResponseStatusError.new "Failed response"
          @statsd_client.expects(:increment).with("count.failure", tags: %w[source:url_target_v2 reason:failed_response])
          UrlTargetV2.any_instance.expects(:send_message_with_retry).raises(e)
          TargetJob.expects(:resque_log).with(["executing target", @obj_hash])
          TargetJob.expects(:resque_log).with(["HTTP call failed", @obj_hash.merge(error_message: e.message)])
          UrlTargetV2.any_instance.expects(:message_failed).with(e, all_of(instance_of(External), responds_with(:id, @external.id)))

          # invoke the method to trigger the timeout exception
          TargetJob.work(@target.account_id, @external.id)
        end

        it "should non lock wait timeout invalid statement error causes job failure" do
          # setup expectations
          e = ActiveRecord::StatementInvalid.new "invalid statement"
          @statsd_client.expects(:increment).with("count.failure", tags: %w[source:url_target_v2 reason:invalid_statement])
          UrlTargetV2.any_instance.expects(:send_message_with_retry).raises(e)
          TargetJob.expects(:resque_log).with(["executing target", @obj_hash])
          TargetJob.expects(:resque_log).with(["exception", @obj_hash.merge(error_class: e.class.name, error_message: e.message)])
          UrlTargetV2.any_instance.expects(:message_failed).with(e, all_of(instance_of(External), responds_with(:id, @external.id)))
          TargetJob.expects(:resque_warn).with(regexp_matches(
            /Account #{@external.account_id}: Error in TargetJob processing external #{@external.id}: invalid statement/
          ))

          # invoke the method to trigger the timeout exception
          TargetJob.work(@target.account_id, @external.id)
        end

        it "should lock wait timeout invalid statement error causes job failure" do
          # setup expectations
          e = ActiveRecord::StatementInvalid.new "lock wait timeout invalid statement"
          @statsd_client.expects(:increment).with("count.failure", tags: %w[source:url_target_v2 reason:invalid_statement])
          UrlTargetV2.any_instance.expects(:send_message_with_retry).raises(e)
          TargetJob.expects(:resque_log).with(["executing target", @obj_hash])
          TargetJob.expects(:resque_log).with(["exception", @obj_hash.merge(error_class: e.class.name, error_message: e.message)]).never
          TargetJob.expects(:resque_warn).with(regexp_matches(
            /Account #{@external.account_id}: Error in TargetJob processing external #{@external.id}: lock wait timeout invalid statement/
          ))

          # invoke the method to trigger the timeout exception
          TargetJob.work(@target.account_id, @external.id)
        end

        it "should runtime exception causes job failure" do
          # setup expectations
          e = RuntimeError.new "ouch"
          @statsd_client.expects(:increment).with("count.failure", tags: %w[source:url_target_v2 reason:generic_error])
          UrlTargetV2.any_instance.expects(:send_message_with_retry).raises(e)
          TargetJob.expects(:resque_log).with(["executing target", @obj_hash])
          TargetJob.expects(:resque_log).with(["exception", @obj_hash.merge(error_class: e.class.name, error_message: e.message)])
          UrlTargetV2.any_instance.expects(:message_failed).with(e, all_of(instance_of(External), responds_with(:id, @external.id)))

          TargetJob.expects(:resque_warn).with(regexp_matches(
            /Account #{@external.account_id}: Error in TargetJob processing external #{@external.id}: ouch/
          ))

          # invoke the method to trigger the timeout exception
          TargetJob.work(@target.account_id, @external.id)
        end
      end

      describe "#send_message_to_target" do
        let(:target) { MiniTest::Mock.new }
        let(:msg) { "minimum" }
        let(:external) { nil }
        let(:retries) { 1 }
        let(:job) { TargetJob.new(external, retries) }

        before do
          TargetJob.any_instance.stubs(:target).returns(target)
          TargetJob.any_instance.stubs(:message).returns(msg)
          TargetJob.any_instance.stubs(:external).returns(external)
          TargetJob.any_instance.stubs(:retries).returns(retries)
        end

        it "should call send_message_with_retry when retriable" do
          target.expect(:retriable?, true)

          target.expect(:send_message_with_retry, true, [msg, external, retries])
          job.send_message_to_target
        end

        it "should call send_message when not retriable" do
          target.expect(:retriable?, false)

          target.expect(:send_message, true, [msg, external])
          job.send_message_to_target
        end
      end
    end
  end
end
