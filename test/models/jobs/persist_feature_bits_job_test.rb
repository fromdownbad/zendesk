require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe PersistFeatureBitsJob do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:subscription_feature_service) { stub(execute!: true) }

  describe '#work' do
    it "calls the subscription feature service" do
      Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account).returns(subscription_feature_service)
      PersistFeatureBitsJob.work(account.id)
    end
  end
end
