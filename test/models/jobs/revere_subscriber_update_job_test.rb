require_relative "../../support/job_helper"

SingleCov.covered!

describe RevereSubscriberUpdateJob do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:user) { account.admins.first }
  let(:api_client) { stub }

  before do
    RevereSubscriberUpdateJob.stubs(:revere_api_client).returns(api_client)
  end

  it 'subscribes user' do
    user.settings.revere_subscription = true
    user.settings.save!
    puts user.settings.inspect
    api_client.expects(:update_subscription).with(user, true)

    RevereSubscriberUpdateJob.work(account.id, user.id)
  end

  it 'unsubscribes user' do
    user.settings.revere_subscription = false
    user.settings.save!
    api_client.expects(:update_subscription).with(user, false)

    RevereSubscriberUpdateJob.work(account.id, user.id)
  end

  it 'leaves a user unsubscribed' do
    # a user that has never subscribed defaults to false (no revere_subscription user_setting)
    api_client.expects(:update_subscription).with(user, false)

    RevereSubscriberUpdateJob.work(account.id, user.id)
  end
end
