require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 7

describe 'RuleTicketCountJob' do
  fixtures :accounts, :users, :rules

  before { stub_occam_count(0) }

  describe '#perform' do
    let(:rule) { rules(:view_my_working_tickets) }
    let(:user) { users(:minimum_agent) }
    let(:cached_count) do
      CachedRuleTicketCount.new(rule: rule, user: user)
    end
    let(:job) { RuleTicketCountJob.new(rule, user, cached_count, true) }

    it 'executes with no errors' do
      job.perform
    end

    describe 'rule is an automation' do
      let(:rule) { rules(:automation_close_ticket) }

      it 'executes with no errors' do
        job.perform
      end
    end

    describe 'cached value' do
      describe 'on occam client connection error' do
        let(:error_name) { "occam/client/client_connection_error" }
        let(:network_error) { Faraday::ConnectionFailed.new(502, "Bad request") }

        before do
          Zendesk::Rules::RuleExecuter.
            stubs(:count_tickets).
            raises(Occam::Client::ClientConnectionError.new(network_error))
        end

        it 'raises an error and returns the cache value as is' do
          job.perform
          assert cached_count.zero?
          assert_nil cached_count.updated_at
        end

        it 'instruments the exception' do
          Rails.logger.expects(:warn).
            with("Connection to Occam failed with: #{error_name}")
          Rails.application.config.statsd.client.expects(:increment).
            with("zendesk.views.soft_errors", tags: ["error:#{error_name}", 'degraded:true', 'affects:count'])
          job.perform
        end
      end

      describe 'on occam successful request' do
        before do
          Zendesk::Rules::RuleExecuter.stubs(:count_tickets).returns(1)
        end

        it 'raises an error and returns the cache value as is' do
          job.perform
          assert_equal 1, cached_count.value
        end
      end
    end

    describe_with_arturo_enabled :occam_count_job_timeout do
      it 'sets query timeout to COUNT_JOB_TIMEOUT' do
        rule.
          expects(:count_tickets).
          with(
            user,
            timeout: RuleTicketCountJob::COUNT_JOB_TIMEOUT,
            caller: 'rule-ticket-count-job'
          )

        job.perform
      end
    end

    describe_with_arturo_disabled :occam_count_job_timeout do
      it 'sets query timeout to DEFAULT_COUNT_TIMEOUT' do
        rule.
          expects(:count_tickets).
          with(
            user,
            timeout: RuleTicketCountJob::DEFAULT_COUNT_TIMEOUT,
            caller: 'rule-ticket-count-job'
          )

        job.perform
      end
    end
  end
end
