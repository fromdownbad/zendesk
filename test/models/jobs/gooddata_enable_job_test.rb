require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 9

describe GooddataEnableJob do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:admin)   { users(:minimum_admin) }

  describe ".work" do
    let(:integration_provisioning) { stub('integration provisioning') }

    before do
      Zendesk::Gooddata::IntegrationProvisioning.stubs(:new).
        with(account, admin: admin).
        returns(integration_provisioning)

      integration_provisioning.stubs(:re_enable)

      GooddataEnableJob.any_instance.stubs(:sleep).returns(true)

      Zendesk::StatsD::Client.any_instance.stubs(:increment)
      Zendesk::StatsD::Client.any_instance.stubs(:histogram)
    end

    describe "when the process goes smoothly" do
      before do
        GooddataEnableJob.work(account.id, admin.id)
      end

      before_should "perform the integration provisioning process" do
        integration_provisioning.expects(:re_enable)
      end

      before_should "count the run" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).
          with('count')
      end

      before_should "record the running time" do
        Zendesk::StatsD::Client.any_instance.expects('histogram').
          with('execution_time', kind_of(Float))
      end
    end

    describe "when an unknown error occurs" do
      before do
        integration_provisioning.expects(:re_enable).raises(RuntimeError)
      end

      it "logs the error to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed')

        GooddataEnableJob.work(account.id, admin.id)
      end
    end
    [Account, User].each do |model|
      describe "when the #{model} cannot be found" do
        before do
          model.stubs(:find).raises(ActiveRecord::RecordNotFound)

          GooddataEnableJob.work(account.id, admin.id)
        end

        before_should "record the error" do
          ZendeskExceptions::Logger.expects(:record).with do |exception, params|
            assert_equal(ActiveRecord::RecordNotFound, exception.class)
            assert_equal(GooddataEnableJob, params[:location])
            assert_includes(params[:message], "account_id: #{account.id}, user_id: #{admin.id} could not be found")
          end
        end
      end
    end
  end

  describe ".args_to_log" do
    let(:account_id) { 1 }
    let(:user_id)    { 2 }

    it "logs the account and user IDs" do
      assert_equal(
        {account_id: account_id, user_id: user_id},
        GooddataEnableJob.args_to_log(account_id, user_id, 'foo', 'bar')
      )
    end
  end
end
