require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 2

describe OrganizationUnsetJob do
  fixtures :accounts, :users, :organizations, :organization_memberships

  describe "#perform" do
    let(:account)        { accounts(:minimum) }
    let(:organization)   { organizations(:minimum_organization1) }
    let(:unset_organization_memberships) do
      [organization_memberships(:minimum),
       organization_memberships(:minimum2)]
    end

    let(:unset_user_ids) { unset_organization_memberships.map(&:user_id).sort }

    it "initializes and execute an OrganizationAssociationRemoval with correct arguments" do
      OrganizationAssociationRemoval.expects(:new).
        with(account.id, organization.id, unset_user_ids).
        returns(stub(call: true))

      OrganizationUnsetJob.perform(account.id, organization.id)
    end
  end
end
