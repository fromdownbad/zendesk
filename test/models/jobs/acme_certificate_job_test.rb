require_relative "../../support/job_helper"
require_relative "../../support/test_helper"
require_relative "../../support/certificate_test_helper"

require 'acme/client'
require 'acmev2/client'

SingleCov.covered! uncovered: 29

describe 'AcmeCertificateJob' do
  include CertificateTestHelper

  fixtures :accounts, :routes
  let(:account) { accounts(:minimum) }
  let(:status) { AcmeCertificateJobStatus.create!(account_id: account.id) }
  let(:activate_certificate) { true }
  let(:job) { AcmeCertificateJob.new(account.id, status.id, activate_certificate, SecureRandom.hex) }
  let(:statsd) { stub_for_statsd }
  let(:acme_certificate) { AcmeCertificate.new(account: account, client: stub) }

  def generate_cert(state, auto, sni)
    cert = account.certificates.build
    cert.state = state
    cert.autoprovisioned = auto
    cert.sni_enabled = sni

    csr = new_csr(account.host_mapping, account.brand_host_mappings)
    self_sign_cert(cert, csr)

    cert.save!
    cert
  end

  let(:revoked_le_cert) do
    generate_cert 'revoked', true, true
  end

  let(:active_cert) do
    generate_cert 'active', true, true
  end

  def create_brand(_name, route)
    b = account.brands.create
    b.name = 'bar'
    b.route = route
    b.save!
    b
  end

  describe 'with lets_encrypt_acme_v2 arturo off' do
    before do
      Arturo.disable_feature!(:lets_encrypt_acme_v2)
      account.update_attribute(:host_mapping, 'example.com')
      account.settings.automatic_certificate_provisioning = true
      disable_lets_encrypt_observers { account.settings.save! }

      AcmeCertificateJob.any_instance.stubs(statsd_client: statsd)
    end

    describe "#work" do
      before do
        job.stubs(client: mock('client'))
        job.stubs(:pending_authorizations).returns([])
        job.stubs(:acme_certificate).returns(acme_certificate)
        Certificate.any_instance.stubs(crt_chain: 'crt_chain')
      end

      describe 'without hostmapping' do
        before do
          account.update_attribute(:host_mapping, nil)
          statsd.expects(:increment).with('count.failed', tags: ['error:no_host_mappings'])
          job.work
        end

        it "fails" do
          assert_equal 'failed', job.job_status.status
        end

        it 'sets message' do
          assert_equal "Account doesn't have any host mappings. Aborting", job.job_status.message
        end
      end

      describe "automatic renewal of certificate" do
        describe "autoprovisioned certificate needing renewal" do
          before do
            # independent of automatic_certificate_provisioning setting
            account.settings.automatic_certificate_provisioning = false
            account.settings.save!
            create_autoprovisioned_certificate!(state: 'active', not_after: 3.days.ago)
          end

          it "renews" do
            acme_certificate.expects(:request_certificate!)
            acme_certificate.expects(:install_certificate!)
            job.work
            assert_equal 'completed', job.job_status.status
          end
        end

        describe "certificate does not need renewal" do
          before do
            create_autoprovisioned_certificate!(state: 'active', not_after: 20.days.from_now)
            statsd.expects(:increment).with('count.failed', tags: ['error:no_renewal_needed'])
          end

          it "renews" do
            job.work
            assert_equal 'completed', job.job_status.status
          end
        end
      end

      describe "when an account has a revoked Let's Encrypt certificate" do
        describe "that covers all brands" do
          before do
            revoked_le_cert # side-effect only
            statsd.expects(:increment).with('count.success', tags: ['success:with_notification', 'activated:true'])
          end

          it "activates the certificate" do
            acme_certificate.expects(:install_certificate!)
            job.work
          end
        end
      end

      describe "adding a domain to an existing certificate" do
        before do
          active_cert # side-effect only

          disable_lets_encrypt_observers do
            route = account.routes.create!(subdomain: 'bar', host_mapping: 'bar.com')
            create_brand 'bar', route
          end

          acme_certificate.stubs(certificate: stub(account: account))

          statsd.expects(:increment).with('count.success', tags: ['success:without_notification', 'activated:true'])
        end

        it 'creates a new cert' do
          acme_certificate.expects(:request_certificate!)
          acme_certificate.expects(:install_certificate!)
          job.work
        end
      end

      describe "when an account doesn't have an autoprovisioned certificate" do
        it "requests, installs, and notifies a certificate" do
          SecurityMailer.expects(:deliver_lets_encrypt_enabled).with(acme_certificate.certificate)

          acme_certificate.expects(:request_certificate!)
          acme_certificate.expects(:install_certificate!)

          job.work
        end

        describe "with a customer provided certificate" do
          let!(:existing_certificate) { create_certificate!(state: 'active', create_ip: false, account: account) }

          it "requests, installs, and notifies a certificate" do
            SecurityMailer.expects(:deliver_lets_encrypt_enabled).with(acme_certificate.certificate)

            acme_certificate.expects(:request_certificate!)
            acme_certificate.expects(:install_certificate!)

            job.work
          end
        end
      end

      describe "Test mode: when the certificate should not be activated" do
        before do
          revoked_le_cert # side-effect only
          job.stubs(:activate_certificate).returns(false)
          statsd.expects(:increment).with('count.success', tags: ['success:without_notification', 'activated:false'])
        end

        it "requests a certificate, doesn't revoke the existing certificate, doesn't notify the customer" do
          job.expects(:activate_existing_certificate!).never
          job.expects(:notify_customer_of_activation!).never
          job.work
        end
      end
    end

    describe "concurrency" do
      it "aborts the current job if a job is already running" do
        QueueAudit.create!(account_id: account.id, job_klass: 'AcmeCertificateJob', enqueued_at: Time.now, enqueued_id: 1, enqueue_count: 1, payload: 'payload', timeout_at: Time.now + 1.hour)
        assert_raises(Zendesk::AcmeJob::JobAlreadyRunning) { AcmeCertificateJob.enqueue(account.id, status.id, true) }
      end
    end

    describe "rate limited" do
      before do
        Prop.throttle!(:lets_encrypt_certificates, account.id, increment: 10)

        AcmeReporter.any_instance.expects(:report_error).once
        statsd.expects(:increment).with('count.failed', tags: ['error:rate_limited'])
        AcmeCertificateJob.enqueue(account.id, status.id, true)
      end

      it "aborts the current job if the account is rate limited" do
        status.reload
        assert_equal 'failed', status.status
      end
    end

    describe "#job_status" do
      it "associates with the job_status" do
        assert_equal status, job.job_status
      end

      it "sets the enqueued_id on the job status" do
        assert_equal job.audit_id, job.job_status.enqueued_id
      end

      it "blows up if the enqueued_id is already set" do
        status.update_attributes!(enqueued_id: 'foo')
        assert_raises(RuntimeError, "AcmeCertificateJob enqueued with incorrect job_status_id") { job }
      end
    end

    describe "failure handling" do
      before do
        QueueAudit.without_arsi.destroy_all
        disable_lets_encrypt_observers { account.update_attribute(:host_mapping, 'kintnerc.net') }
        AcmeV2::Client.any_instance.stubs(:new_account).raises(Net::OpenTimeout)
        AcmeReporter.any_instance.expects(:report_error).once.returns(mock(id: 1))
      end

      it "notifies of failures and marks the job as complete" do
        AcmeCertificateJob.enqueue(account.id, status.id, true)

        assert_equal 1, QueueAudit.count
        refute_nil QueueAudit.last.completed_at
      end
    end
  end

  describe 'with the lets_encrypt_acme_v2 arturo on' do
    before do
      Arturo.enable_feature!(:lets_encrypt_acme_v2)
      account.update_attribute(:host_mapping, 'example.com')
      account.settings.automatic_certificate_provisioning = true
      disable_lets_encrypt_observers { account.settings.save! }

      AcmeCertificateJob.any_instance.stubs(statsd_client: statsd)
    end

    describe "#work" do
      before do
        job.stubs(client: mock('client'))
        job.stubs(:pending_authorizations).returns([])
        job.stubs(:acme_certificate).returns(acme_certificate)
        Certificate.any_instance.stubs(crt_chain: 'crt_chain')
      end

      describe 'without hostmapping' do
        before do
          account.update_attribute(:host_mapping, nil)
          statsd.expects(:increment).with('count.failed', tags: ['error:no_host_mappings'])
          job.work
        end

        it "fails" do
          assert_equal 'failed', job.job_status.status
        end

        it 'sets message' do
          assert_equal "Account doesn't have any host mappings. Aborting", job.job_status.message
        end
      end

      describe "automatic renewal of certificate" do
        describe "autoprovisioned certificate needing renewal" do
          before do
            # independent of automatic_certificate_provisioning setting
            account.settings.automatic_certificate_provisioning = false
            account.settings.save!
            create_autoprovisioned_certificate!(state: 'active', not_after: 3.days.ago)
            job.stubs(:register_and_authorize_order!).returns(true)
            job.stubs(:check_pending_authorizations!).returns(true)
          end

          it "renews" do
            acme_certificate.expects(:request_certificate!)
            acme_certificate.expects(:install_certificate!)
            job.work
            assert_equal 'completed', job.job_status.status
          end
        end

        describe "certificate does not need renewal" do
          before do
            create_autoprovisioned_certificate!(state: 'active', not_after: 20.days.from_now)
            statsd.expects(:increment).with('count.failed', tags: ['error:no_renewal_needed'])
          end

          it "renews" do
            job.work
            assert_equal 'completed', job.job_status.status
          end
        end
      end

      describe "when an account has a revoked Let's Encrypt certificate" do
        describe "that covers all brands" do
          before do
            revoked_le_cert # side-effect only
            statsd.expects(:increment).with('count.success', tags: ['success:with_notification', 'activated:true'])
          end

          it "activates the certificate" do
            acme_certificate.expects(:install_certificate!)
            job.work
          end
        end
      end

      describe "adding a domain to an existing certificate" do
        before do
          active_cert # side-effect only

          disable_lets_encrypt_observers do
            route = account.routes.create!(subdomain: 'bar', host_mapping: 'bar.com')
            create_brand 'bar', route
          end

          acme_certificate.stubs(certificate: stub(account: account))

          statsd.expects(:increment).with('count.success', tags: ['success:without_notification', 'activated:true'])

          job.stubs(:register_and_authorize_order!).returns(true)
          job.stubs(:check_pending_authorizations!).returns(true)
        end

        it 'creates a new cert' do
          acme_certificate.expects(:request_certificate!)
          acme_certificate.expects(:install_certificate!)
          job.work
        end
      end

      describe "when an account doesn't have an autoprovisioned certificate" do
        before do
          job.stubs(:register_and_authorize_order!).returns(true)
          job.stubs(:check_pending_authorizations!).returns(true)
        end

        it "requests, installs, and notifies a certificate" do
          SecurityMailer.expects(:deliver_lets_encrypt_enabled).with(acme_certificate.certificate)

          acme_certificate.expects(:request_certificate!)
          acme_certificate.expects(:install_certificate!)

          job.work
        end

        describe "with a customer provided certificate" do
          let!(:existing_certificate) { create_certificate!(state: 'active', create_ip: false, account: account) }

          it "requests, installs, and notifies a certificate" do
            SecurityMailer.expects(:deliver_lets_encrypt_enabled).with(acme_certificate.certificate)

            acme_certificate.expects(:request_certificate!)
            acme_certificate.expects(:install_certificate!)

            job.work
          end
        end
      end

      describe "Test mode: when the certificate should not be activated" do
        before do
          revoked_le_cert # side-effect only
          job.stubs(:activate_certificate).returns(false)
          statsd.expects(:increment).with('count.success', tags: ['success:without_notification', 'activated:false'])
        end

        it "requests a certificate, doesn't revoke the existing certificate, doesn't notify the customer" do
          job.expects(:activate_existing_certificate!).never
          job.expects(:notify_customer_of_activation!).never
          job.work
        end
      end
    end

    describe "concurrency" do
      it "aborts the current job if a job is already running" do
        QueueAudit.create!(account_id: account.id, job_klass: 'AcmeCertificateJob', enqueued_at: Time.now, enqueued_id: 1, enqueue_count: 1, payload: 'payload', timeout_at: Time.now + 1.hour)
        assert_raises(Zendesk::AcmeJob::JobAlreadyRunning) { AcmeCertificateJob.enqueue(account.id, status.id, true) }
      end
    end

    describe "rate limited" do
      before do
        Prop.throttle!(:lets_encrypt_certificates, account.id, increment: 10)

        AcmeReporter.any_instance.expects(:report_error).once
        statsd.expects(:increment).with('count.failed', tags: ['error:rate_limited'])
        AcmeCertificateJob.enqueue(account.id, status.id, true)
      end

      it "aborts the current job if the account is rate limited" do
        status.reload
        assert_equal 'failed', status.status
      end
    end

    describe "#job_status" do
      it "associates with the job_status" do
        assert_equal status, job.job_status
      end

      it "sets the enqueued_id on the job status" do
        assert_equal job.audit_id, job.job_status.enqueued_id
      end

      it "blows up if the enqueued_id is already set" do
        status.update_attributes!(enqueued_id: 'foo')
        assert_raises(RuntimeError, "AcmeCertificateJob enqueued with incorrect job_status_id") { job }
      end
    end

    describe "failure handling" do
      describe 'new account' do
        before do
          QueueAudit.without_arsi.destroy_all
          disable_lets_encrypt_observers { account.update_attribute(:host_mapping, 'kintnerc.net') }
          AcmeV2::Client.any_instance.stubs(:new_account).raises(Net::OpenTimeout)
          AcmeReporter.any_instance.expects(:report_error).once.returns(mock(id: 1))
        end

        it "notifies of failures and marks the job as complete" do
          AcmeCertificateJob.enqueue(account.id, status.id, true)

          assert_equal 1, QueueAudit.count
          refute_nil QueueAudit.last.completed_at
        end
      end
    end
  end
end
