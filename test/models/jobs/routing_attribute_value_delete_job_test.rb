require_relative "../../support/test_helper"
require_relative "../../support/routing"

SingleCov.covered!

describe RoutingAttributeValueDeleteJob do
  include TestSupport::Routing::Helper

  let(:account) { accounts(:support) }
  let(:attribute_ticket_map) { new_attribute_ticket_map(account: account, attribute_id: 1, attribute_value_id: 2) }
  let(:job) do
    RoutingAttributeValueDeleteJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      attribute_id: attribute_ticket_map.attribute_id,
      id: attribute_ticket_map.attribute_value_id)
  end
  let(:deco_client) { Zendesk::Deco::Client.new(account) }

  before do
    Rails.logger.stubs(:info)
    deco_client.stubs(:delete).returns(stub('response', status: 204))
    RoutingAttributeValueDeleteJob.any_instance.stubs(:deco).returns(deco_client)
  end

  describe '#work' do
    it "logs info of attribute value to be deleted" do
      Rails.logger.expects(:info).with("Deleting attribute value:"\
        "#{attribute_ticket_map.attribute_value_id} for account ID:#{account.id}").at_least_once

      job.perform
    end

    describe 'when Deco client call is successful' do
      before { job.perform }

      it 'completes job gracefuly' do
        assert_equal "completed", job.status.status

        assert_equal(
          "Deleted attribute value. Id: #{attribute_ticket_map.attribute_value_id}"\
          " AttributeId: #{attribute_ticket_map.attribute_id}.",
          job.status.message
        )
      end
    end

    describe 'when there is a network error' do
      before(:each) do
        deco_client.stubs(:delete).raises(Faraday::TimeoutError.new('Deco timeout'))

        job.perform
      end

      before_should "record exception message" do
        ZendeskExceptions::Logger.expects(:record).with do |_, params|
          assert_equal("Exception while running #{job.name} for account #{account.id}: Deco timeout", params[:message])
          assert_equal(RoutingAttributeValueDeleteJob::FINGERPRINT, params[:fingerprint])
        end.once
      end

      it 'completes job gracefuly' do
        assert_equal "completed", job.status.status
        assert_equal(
          "Failed to delete attribute value. Id: #{attribute_ticket_map.attribute_value_id}"\
          " AttributeId: #{attribute_ticket_map.attribute_id}."\
          " After 3 retries.",
          job.status.message
        )
      end
    end

    describe 'when Deco client raises a ResourceNotFound error' do
      before(:each) do
        deco_client.stubs(:delete).raises(Kragle::ResourceNotFound.new)

        job.perform
      end

      it 'completes job gracefuly' do
        assert_equal "completed", job.status.status
        assert_equal(
          "Could not find attribute value. Id: #{attribute_ticket_map.attribute_value_id}"\
          " AttributeId: #{attribute_ticket_map.attribute_id}.",
          job.status.message
        )
      end
    end

    describe 'when Deco client raises a BadRequest error' do
      before(:each) do
        deco_client.stubs(:delete).raises(Kragle::BadRequest.new("Bad Deco request"))

        job.perform
      end

      before_should "record exception message" do
        ZendeskExceptions::Logger.expects(:record).with do |_, params|
          assert_equal("Exception while running #{job.name} for account #{account.id}: Bad Deco request", params[:message])
          assert_equal(RoutingAttributeValueDeleteJob::FINGERPRINT, params[:fingerprint])
        end.once
      end

      it 'completes job gracefuly' do
        assert_equal "completed", job.status.status
        assert_equal(
          "Failed to delete attribute value. Id: #{attribute_ticket_map.attribute_value_id}"\
          " AttributeId: #{attribute_ticket_map.attribute_id}.",
          job.status.message
        )
      end
    end
  end
end
