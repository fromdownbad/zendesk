require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 4

describe OrganizationReassignJob do
  fixtures :accounts, :users, :organizations, :organization_domains

  def create_user_email_identity_for(user, local_part)
    UserEmailIdentity.create!(
      account: user.account,
      value: "#{local_part}@newdomain.com",
      is_verified: true,
      user: user
    )
  end

  describe "#perform" do
    let(:account)      { accounts(:minimum) }
    let(:organization) { account.organizations.last } # minimum_organization3:
    let(:organization_domain) { organization_domains(:organization_domain1) }
    let(:reassign_users_hash) do
      {
        "bar" => users(:minimum_search_user), # no organization
        "foo" => users(:minimum_end_user),    # minimum_organization1
        "tar" => users(:minimum_author),      # no organization
        "too" => users(:minimum_end_user2),   # minimum_organization1
      }
    end
    let(:batch1) { reassign_users_hash.values.map(&:id).slice(0, 2) }
    let(:batch2) { reassign_users_hash.values.map(&:id).slice(2, 4) }

    before do
      OrganizationReassignJob.batch_read_size  = 2
      OrganizationReassignJob.batch_write_size = 2

      reassign_users_hash.each do |local_part, user|
        create_user_email_identity_for(user, local_part)
      end

      Account.stubs(find: account)
    end

    describe_with_arturo_disabled :org_reassign_synchronize_on_domain do
      describe "adding a new domain to the organization" do
        it "initializes and call an OrganizationAssociationAddition instance with each batch" do
          OrganizationAssociationAddition.expects(:new).
            with(account.id, organization.id, batch1).
            returns(stub(call: organization))

          OrganizationAssociationAddition.expects(:new).
            with(account.id, organization.id, batch2).
            returns(stub(call: organization))

          OrganizationReassignJob.expects(:lock?).never
          OrganizationReassignJob.expects(:unlock).never

          OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])
        end
      end

      describe "with unverified identities" do
        before do
          unverified_identity = UserEmailIdentity.find_by_value("too@newdomain.com")
          unverified_identity.update_column(:is_verified, false)

          OrganizationReassignJob.batch_read_size  = 4
          OrganizationReassignJob.batch_write_size = 4

          @verified_batch = reassign_users_hash.values.select(&:is_verified?).map(&:id)
        end

        it "does not pulls unverified users" do
          OrganizationAssociationAddition.expects(:new).
            with(account.id, organization.id, @verified_batch).
            returns(stub(call: organization))

          OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])
        end
      end

      describe "When OrganizationAssociationAddition#call does not return the organization" do
        before do
          OrganizationAssociationAddition.expects(:new).
            with(account.id, organization.id, batch1).
            returns(stub(call: nil))
        end

        it "initializes and call an OrganizationAssociationRemoval instance to cleanup the current batch and exit" do
          OrganizationAssociationRemoval.expects(:new).
            with(account.id, organization.id, batch1).
            returns(stub(call: nil))

          OrganizationAssociationAddition.expects(:new).
            with(account.id, organization.id, batch2).never
          OrganizationAssociationRemoval.expects(:new).
            with(account.id, organization.id, batch2).never

          OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])
        end
      end

      describe_with_arturo_enabled :org_reassign_domain_mapping_kill_switch do
        it "raises an exception and does not call OrganizationAssociationRemoval" do
          OrganizationAssociationAddition.expects(:new).never
          OrganizationAssociationRemoval.expects(:new).never
          assert_raises(OrganizationReassignJob::KillSwitchFlipped) do
            OrganizationReassignJob.perform(account.id, organization.id, [], ["newdomain.com"])
          end
        end
      end

      describe_with_arturo_enabled :org_reassign_synchronize_on_domain do
        describe "when lock is obtained" do
          it "reassigns organizations and unlocks" do
            OrganizationAssociationAddition.expects(:new).
              with(account.id, organization.id, batch1).
              returns(stub(call: organization))

            OrganizationAssociationAddition.expects(:new).
              with(account.id, organization.id, batch2).
              returns(stub(call: organization))

            OrganizationReassignJob.expects(:lock?).
              with(account.id, "newdomain.com", organization.id, OrganizationAssociationAddition).
              returns(true)

            OrganizationReassignJob.expects(:unlock).
              with(account.id, "newdomain.com")

            OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])

            assert_equal(OrganizationReassignJob.send(:single_execution_key, 1, 'yahoo.com'), "OrganizationReassignJob/1/05c3ae107b3d826b7d00ff4a81e06c9fbcfbb6c0")
          end

          describe "when reassignment throws error" do
            it "kills the job and stops the iterations" do
              class Footwo
                def call
                  raise 'problem'
                end
              end

              OrganizationAssociationAddition.expects(:new).
                with(account.id, organization.id, batch1).
                returns(Footwo.new)

              OrganizationAssociationAddition.expects(:new).
                with(account.id, organization.id, batch2).
                returns(stub(call: organization)).never

              OrganizationReassignJob.expects(:lock?).
                with(account.id, "newdomain.com", organization.id, OrganizationAssociationAddition).
                returns(true)

              OrganizationReassignJob.expects(:unlock).
                with(account.id, "newdomain.com")

              assert_raises RuntimeError do
                OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])
              end
            end
          end

          describe "when unlock throws" do
            it "kills the job and stops the iterations" do
              class Foofootwo
                def set(alpha, beta, gamma)
                  @redis_client ||= Zendesk::RedisStore.client
                  @redis_client.set(alpha, beta, gamma)
                end

                def watch(gamma)
                  raise "foo #{gamma}"
                end
              end

              @redis_client = Foofootwo.new
              OrganizationReassignJob.stubs(:redis_client).returns(@redis_client)

              OrganizationAssociationAddition.expects(:new).
                with(account.id, organization.id, batch1).
                returns(stub(call: organization))

              OrganizationAssociationAddition.expects(:new).
                with(account.id, organization.id, batch2).
                returns(stub(call: organization))

              assert_raises RuntimeError do
                OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])
              end
            end
          end

          describe "when unlock throws a base error" do
            it "still iterates" do
              class Foofoo
                def set(alpha, beta, gamma)
                  @redis_client ||= Zendesk::RedisStore.client
                  @redis_client.set(alpha, beta, gamma)
                end

                def watch(gamma)
                  raise Redis::BaseError, "foo #{gamma}"
                end
              end

              @redis_client = Foofoo.new
              OrganizationReassignJob.stubs(:redis_client).returns(@redis_client)

              OrganizationAssociationAddition.expects(:new).
                with(account.id, organization.id, batch1).
                returns(stub(call: organization))

              OrganizationAssociationAddition.expects(:new).
                with(account.id, organization.id, batch2).
                returns(stub(call: organization))

              OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])
            end
          end
        end

        describe "when lock is not obtained when returns false" do
          it "ensures unlock is not called and skipped over" do
            OrganizationAssociationAddition.expects(:new).
              with(account.id, organization.id, batch1).never

            OrganizationAssociationAddition.expects(:new).
              with(account.id, organization.id, batch2).never

            OrganizationReassignJob.expects(:lock?).
              with(account.id, "newdomain.com", organization.id, OrganizationAssociationAddition).
              returns(false)

            OrganizationReassignJob.expects(:unlock).never

            OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])
          end
        end

        describe "when lock is not obtained due to exception" do
          it "does not perform the job" do
            class Foo
              def set(alpha, beta, gamma)
                raise "#{alpha}#{gamma}#{beta}"
              end
            end

            @redis_client = Foo.new
            OrganizationReassignJob.stubs(:redis_client).returns(@redis_client)

            OrganizationReassignJob.expects(:unlock).never
            OrganizationAssociationAddition.expects(:new).
              with(account.id, organization.id, batch1).never
            OrganizationAssociationAddition.expects(:new).
              with(account.id, organization.id, batch2).never
            assert_raises RuntimeError do
              OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])
            end
          end
        end
      end
    end

    describe "removing an existing domain" do
      before { organization_domain.update_attributes(organization_id: organization.id, value: "newdomain.com") }

      it "initializes and call an OrganizationAssociationRemoval instance with each batch" do
        OrganizationAssociationRemoval.expects(:new).
          with(account.id, organization.id, batch1).
          returns(stub(call: organization))

        OrganizationAssociationRemoval.expects(:new).
          with(account.id, organization.id, batch2).
          returns(stub(call: organization))

        OrganizationReassignJob.perform(account.id, organization.id, [], ["newdomain.com"])
      end

      describe "OrganizationAssociationRemoval#call does not return the organization" do
        before do
          OrganizationAssociationRemoval.expects(:new).
            with(account.id, organization.id, batch1).
            returns(stub(call: nil))
        end

        it "breaks" do
          OrganizationAssociationRemoval.expects(:new).
            with(account.id, organization.id, batch2).never

          OrganizationReassignJob.perform(account.id, organization.id, [], ["newdomain.com"])
        end
      end

      it "finds the batch of users by domain column" do
        OrganizationAssociationAddition.expects(:new).
          with(account.id, organization.id, batch1).
          returns(stub(call: organization))

        OrganizationAssociationAddition.expects(:new).
          with(account.id, organization.id, batch2).
          returns(stub(call: organization))

        OrganizationReassignJob.perform(account.id, organization.id, ["newdomain.com"], [])
      end

      describe_with_arturo_enabled :org_reassign_domain_mapping_kill_switch do
        it "raises an exception and does not call OrganizationAssociationRemoval" do
          OrganizationAssociationRemoval.expects(:new).never
          assert_raises(OrganizationReassignJob::KillSwitchFlipped) do
            OrganizationReassignJob.perform(account.id, organization.id, [], ["newdomain.com"])
          end
        end
      end
    end
  end
end
