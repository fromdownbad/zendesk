require_relative "../../support/job_helper"

SingleCov.covered!

describe 'TriggerCleanupJob' do
  fixtures :accounts, :targets, :rules

  let(:account) { accounts(:minimum) }
  let(:target_id) { targets(:twitter_valid).id }
  let(:trigger) { rules(:trigger_notify_requester_of_received_request) }

  before do
    item = DefinitionItem.new('notification_target', nil, target_id.to_s)

    trigger.definition.actions.push(item)
    trigger.save!

    targets(:twitter_valid).delete

    refute account.targets.find_by_id(target_id)
  end

  describe "#perform" do
    it "cleanup deleted target" do
      TriggerCleanupJob.perform(account.id, trigger.id, target_id)

      refute trigger.reload.definition.actions.map(&:source).include?('notification_target')
    end

    it "doesn't raise for an unknown trigger id" do
      TriggerCleanupJob.perform(account.id, nil, target_id)
      TriggerCleanupJob.perform(account.id, "abc", target_id)
      TriggerCleanupJob.perform(account.id, 123, target_id)
    end
  end
end
