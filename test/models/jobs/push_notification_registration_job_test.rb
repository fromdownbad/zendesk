require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 2

describe PushNotificationRegistrationJob do
  fixtures :all

  describe "#work" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_agent) }

    let(:device) do
      PushNotifications::DeviceIdentifier.any_instance.stubs(:register).returns(true)

      PushNotifications::DeviceIdentifier.new do |device|
        device.token = "foo"
        device.device_type = "iphone"
        device.account = account
        device.mobile_app_id = mobile_apps(:com_zendesk_agent).id
        device.save!
      end
    end

    it "does not un/register device for com.zendesk.agent" do
      args = {
        account_id: account.id,
        operation: 'register',
        device_type: 'iphone',
        mobile_app_identifier: 'com.zendesk.agent',
        identifier_id: device.id,
        use_api_v3: true
      }
      refute PushNotificationRegistrationJob.enqueue(args)
    end

    it "un/register device for any other app except com.zendesk.agent" do
      config = Zendesk::Configuration.dig!(:urban_airship, :"com.zendesk.inbox")
      PushNotifications::UrbanAirshipClient.expects(:new).with(config['app_key'], config['master_secret']).returns(
        mock(register: [stub])
      )

      args = {
        account_id: account.id,
        operation: :register,
        device_type: 'iphone',
        mobile_app_identifier: 'com.zendesk.inbox',
        identifier_id: device.id,
        use_api_v3: true
      }
      assert PushNotificationRegistrationJob.enqueue(args)
    end

    it "registers the device with the right Urban Airship client" do
      config = Zendesk::Configuration.dig!(:urban_airship, :"com.zendesk.agent")
      PushNotifications::UrbanAirshipClient.expects(:new).with(config['app_key'], config['master_secret']).returns(
        mock(register: [stub])
      )

      Arturo.disable_feature!(:sns_push_notifications)

      PushNotificationRegistrationJob.work(
        account_id: account.id,
        operation: :register,
        mobile_app_identifier: 'com.zendesk.agent',
        identifier_id: device.id,
        use_api_v3: true
      )
    end

    it "registers the device with the Amazon SNS Client" do
      Zendesk::PushNotifications::Adapters::SNS.expects(:new).returns(
        mock(register: [stub])
      )

      Arturo.enable_feature!(:sns_push_notifications)

      PushNotificationRegistrationJob.work(
        account_id: account.id,
        operation: :register,
        mobile_app_identifier: 'com.zendesk.agent',
        identifier_id: device.id,
        device_type: device.device_type,
        token: device.token
      )
    end

    it "unregisters the device with the Amazon SNS Client" do
      Zendesk::PushNotifications::Adapters::SNS.expects(:new).returns(
        mock(unregister: [stub])
      )

      Arturo.enable_feature!(:sns_push_notifications)

      PushNotificationRegistrationJob.work(
        account_id: account.id,
        operation: :unregister,
        mobile_app_identifier: 'com.zendesk.agent',
        token: 'foo',
        device_type: 'iphone',
        identifier_id: device.id,
        sns_target_arn: 'sns_target_arn'
      )
    end
  end
end
