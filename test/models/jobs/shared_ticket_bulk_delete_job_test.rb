require_relative "../../support/job_helper"

SingleCov.covered!

describe SharedTicketBulkDeleteJob do
  fixtures :accounts

  describe "#perform" do
    let(:account) { accounts(:minimum) }
    let(:agreement) { FactoryBot.create(:agreement, account: account) }
    let(:limit) { 10 }

    before do
      @shared_tickets = []

      [*1..limit].each do |seed|
        @shared_tickets << SharedTicket.create!(ticket: FactoryBot.create(:ticket, account: account), agreement: agreement, uuid: "asdf#{seed}")
      end

      SharedTicketBulkDeleteJob.perform(account.id, agreement.id)

      @shared_tickets.map(&:reload)
    end

    it "deletes all shared tickets passed in" do
      @shared_tickets.each do |shared_ticket|
        assert shared_ticket.deleted_at
      end
    end
  end

  describe "#find_agreement_with_deleted" do
    let(:agreement) { FactoryBot.create(:agreement) }

    before do
      stub_request(:put, %r{/sharing/agreements/[0-9a-f]{40}})
      agreement.soft_delete!
    end

    it "returns the soft-deleted agreement" do
      assert SharedTicketBulkDeleteJob.find_agreement_with_deleted(agreement.id)
    end
  end
end
