require_relative '../../support/test_helper'
require_relative "../../support/satisfaction_test_helper"

SingleCov.covered!

describe TwoFactorCsvJob do
  fixtures :all

  describe '#perform' do
    let(:account) { accounts(:minimum) }
    let(:user)    { users(:minimum_admin) }
    let(:user_2)  { users(:minimum_agent) }
    let(:otp)     { {phone_based: true, confirmed: true, phone_number: '123-123-1234'} }

    before do
      stub_request(:put, %r{/data/expirable_attachments/.*.zip})

      user.create_otp_setting!(otp)
      assert user.otp_configured?

      user_2.name = "=AND(2>1)"
      user_2.save!
      user_2.create_otp_setting!(otp)
      assert user.otp_configured?
    end

    it 'sends an email' do
      assert_emails_sent { TwoFactorCsvJob.perform(account.id, user.id) }
    end

    it 'creates an expirable attachment' do
      assert_difference('ExpirableAttachment.count(:all)') do
        TwoFactorCsvJob.perform(account.id, user.id)
      end
    end

    it 'is enqueued in the medium priority queue' do
      assert_equal :medium, Resque.queue_from_class(TwoFactorCsvJob)
    end

    it 'is throttled to run every 10 minutes' do
      assert_equal 10.minutes, TwoFactorCsvJob.settings[:can_run_every]
    end

    describe 'attachment' do
      before do
        TwoFactorCsvJob.perform(account.id, user.id)
        @attachment = ExpirableAttachment.last

        Zip::InputStream.open(@attachment.public_filename) do |io|
          io.get_next_entry
          @csv_contents = io.read.split("\n")
        end
      end

      it 'has the correct content-type' do
        assert_equal 'application/zip', @attachment.content_type
      end

      it 'has the correct filename' do
        assert_equal "two-factor-#{Time.now.strftime('%Y-%m-%d')}-csv.zip", @attachment.filename
      end

      it 'has the right headers in the csv' do
        headers = %("Agent name","Agent email","Two factor enabled?")
        assert_equal headers, @csv_contents[0]
      end

      it 'has the right content in the csv' do
        user_row = @csv_contents.select { |content| content.include? user.email }

        expected = [
          user.name,
          user.email,
          'Yes'
        ].map { |attr| "\"#{attr}\"" }.join(",")

        assert_equal expected, user_row[0]
      end

      it 'has three rows => header + content' do
        assert_equal account.agents.count(:all) + 1, @csv_contents.count
      end

      it "escapes CSV macros" do
        user_2_row = @csv_contents.select { |content| content.include? user_2.email }

        expected = [
          "'=AND(2>1)",
          user_2.email,
          'Yes'
        ].map { |attr| "\"#{attr}\"" }.join(",")

        assert_equal expected, user_2_row[0]
      end
    end

    describe 'chat phase 4 multiproduct account' do
      let(:account) { accounts(:multiproduct) }
      let(:user) { users(:multiproduct_owner) }

      before do
        TwoFactorCsvJob.perform(account.id, user.id)
        @attachment = ExpirableAttachment.last

        Zip::InputStream.open(@attachment.public_filename) do |io|
          io.get_next_entry
          @csv_contents = io.read.split("\n")
        end
      end

      it 'has the right content in the csv' do
        user_row = @csv_contents.select { |content| content.include? user.email }

        expected = [
          user.name,
          user.email,
          'Yes'
        ].map { |attr| "\"#{attr}\"" }.join(",")

        assert_equal expected, user_row[0]
      end

      it 'has the correct number of rows => header + content' do
        assert_equal account.agents.count(:all) + 1, @csv_contents.count
      end
    end
  end
end
