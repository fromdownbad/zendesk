require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 5

describe TagBulkUpdateJob do
  fixtures :accounts, :users, :tickets, :taggings

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_admin) }

  let(:job_options) do
    {
      account_id: account.id,
      user_id: user.id,
      tag_name: tag_name
    }
  end

  describe "with tags with some tickets" do
    let(:tag_name) { "hello" } # fixtures have two tickets with this tag.
    let(:ticket_1) { tickets(:minimum_1) }
    let(:ticket_2) { tickets(:minimum_2) }

    it 'removes tags from tickets' do
      assert ticket_1.current_tags.include?("hello")
      assert ticket_2.current_tags.include?("hello")

      job = TagBulkUpdateJob.perform_now(job_options)
      assert job.status['results']["ticket_count"] == 2
      assert job.status['results']["tag_name"] == "hello"

      assert (ticket_1.reload.current_tags.blank? || !ticket_1.current_tags.include?("hello"))
      assert (ticket_2.reload.current_tags.blank? || !ticket_2.current_tags.include?("hello"))
    end
  end

  describe "with tags with no tickets" do
    let(:tag_name) { "mxyzptlk" }

    it 'removes tags from no tickets' do
      job = TagBulkUpdateJob.perform_now(job_options)
      assert job.status['results']["ticket_count"] == 0
      assert job.status['results']["tag_name"] == "mxyzptlk"
    end
  end
end
