require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe GAMEmailForwardingJob do
  fixtures :accounts, :brands

  let(:account) { accounts(:minimum) }

  before do
    brand = brands(:minimum)
    account.default_brand = brand
    account.save!

    @statsd_stub = stub_for_statsd
    GAMEmailForwardingJob.stubs(statsd_client: @statsd_stub)
    GAMEmailForwardingJob.stubs(:sleep)
    @user = account.owner
    @access_token = 'access_token'
    @customer_id = "abc0123"
    @domain = 'zobuzendesk.com'
    @username = 'haabaato'
    @forward_from_email = "user@#{@domain}"
    @forward_to_email = "support@zobuzendesk.#{Zendesk::Configuration.fetch(:host)}"
    @url = "https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{@domain}/#{@username}/filter"
  end

  it "raises an error when API returns an error" do
    body = <<~XML
      <?xml version="1.0" encoding="UTF-8"?>
      <AppsForYourDomainErrors>
        <error errorCode="1303" invalidInput="random@email.com" reason="EntityNameNotValid" />
      </AppsForYourDomainErrors>
    XML
    stub_request(:post, @url).to_return(status: 400, body: body)

    assert_raises RuntimeError do
      GAMEmailForwardingJob.enqueue(account.id, @user.id, @access_token,
        @domain, @username, @forward_from_email, @forward_to_email)
      @statsd_stub.expects(:increment).with('count', tags: ["status:error, response:#{body}"])
    end
  end

  describe "when email forwarding is successful" do
    before do
      body = <<~XML
        <?xml version='1.0' encoding='UTF-8'?>
        <entry xmlns='http://www.w3.org/2005/Atom' xmlns:apps='http://schemas.google.com/apps/2006'>
          <id>https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{@domain}/#{@username}/filter/0</id>
          <updated>2016-04-27T22:27:53.369Z</updated>
          <link rel='self' type='application/atom+xml' href='https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{@domain}/#{@username}//filter/0'/>
          <link rel='edit' type='application/atom+xml' href='https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{@domain}/#{@username}//filter/0'/>
          <apps:property name='forwardTo' value='#{@forward_to_email}'/>
          <apps:property name='to' value='hello@zobuzendesk.com'/>
        </entry>
      XML
      stub_request(:post, @url).to_return(status: 200, body: body)
    end

    describe "with a valid recipient address" do
      before do
        account.recipient_addresses.create(
          email: @forward_from_email,
          default: false,
          name: 'Test Recipient Address',
          brand: account.default_brand
        )
      end

      it "emails the forwarded email" do
        EmailForwardingMailer.expects(:deliver_forwarding_complete_notice).with(@user, @forward_from_email)
        @statsd_stub.expects(:increment).with('count', tags: ["status:success"])

        GAMEmailForwardingJob.enqueue(account.id, @user.id, @access_token,
          @domain, @username, @forward_from_email, @forward_to_email)
      end

      it "sets the recipient address as default" do
        GAMEmailForwardingJob.enqueue(account.id, @user.id, @access_token,
          @domain, @username, @forward_from_email, @forward_to_email)

        recipient_address = account.recipient_addresses.find_by_email(@forward_from_email)
        assert recipient_address.default
      end

      it "replies to ticket to notify email forwarding is complete and solves it" do
        ticket = Ticket.generate_email_forward_pending_ticket(account, @user)
        Ticket.any_instance.expects(:solve).with(ticket.assignee)

        GAMEmailForwardingJob.enqueue(account.id, @user.id, @access_token,
          @domain, @username, @forward_from_email, @forward_to_email)
      end
    end
  end
end
