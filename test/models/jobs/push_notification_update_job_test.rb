require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe PushNotificationUpdateJob do
  fixtures :all

  describe "#work" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_agent) }

    let(:device) do
      PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register).returns(true)

      PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: "foo",
        device_type: "iphone",
        user: user,
        mobile_app: mobile_apps(:com_zendesk_agent)
      )
    end

    it "does not enqueue for com.zendesk.agent" do
      args = {
        account_id: account.id,
        token: device.token,
        device_type: device.device_type,
        mobile_app_identifier: 'com.zendesk.agent',
        badge: 2
      }
      refute PushNotificationUpdateJob.enqueue(args)
    end

    it "enqueue for any other app except com.zendesk.agent" do
      config = Zendesk::Configuration.dig!(:urban_airship, :'com.zendesk.inbox')
      PushNotifications::UrbanAirshipClient.expects(:new).with(config['app_key'], config['master_secret']).returns(
        mock(update: [stub])
      )
      args = {
        account_id: account.id,
        token: device.token,
        device_type: device.device_type,
        mobile_app_identifier: 'com.zendesk.inbox',
        badge: 2
      }
      assert PushNotificationUpdateJob.enqueue(args)
    end

    it "calls update in the correct Urban Airship client" do
      config = Zendesk::Configuration.dig!(:urban_airship, :"com.zendesk.agent")
      PushNotifications::UrbanAirshipClient.expects(:new).with(config['app_key'], config['master_secret']).returns(
        mock(update: [stub])
      )

      PushNotificationUpdateJob.work(
        account_id: account.id,
        token: device.token,
        device_type: device.device_type,
        mobile_app_identifier: 'com.zendesk.agent',
        badge: 2
      )
    end
  end
end
