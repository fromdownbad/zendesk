require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 8

describe Durable do
  fixtures :accounts
  include PerformJob

  class FakeDurableJob
    extend ZendeskJob::Resque::BaseJob
    extend Durable

    def self.queue
      :foo
    end

    def self.work(account_id, message, enqueued_id)
      worked.push([account_id, message, CIA.current_transaction[:actor], enqueued_id])
    end

    def self.args_to_log(*args)
      { arguments: args }
    end

    def self.worked
      @worked ||= []
    end
  end

  before do
    @account = accounts(:minimum)
    FakeDurableJob.worked.clear
  end

  describe "enqueue_and_get_audit" do
    it "returns the durable audit record" do
      audit = FakeDurableJob.enqueue_and_get_audit(@account.id, 'hello')
      audit.must_be_kind_of Resque::Durable::QueueAudit
      audit.persisted?.must_equal true
    end
  end

  describe "on audit failure" do
    before do
      @old_audit_retry_times = Durable::Extensions.send(:remove_const, :AUDIT_RETRY_TIMES)
      Durable::Extensions.const_set(:AUDIT_RETRY_TIMES, 0)
    end
    after do
      Durable::Extensions.send(:remove_const, :AUDIT_RETRY_TIMES)
      Durable::Extensions.const_set(:AUDIT_RETRY_TIMES, @old_audit_retry_times)
    end

    it "notifys audit_failed" do
      FakeDurableJob.expects(:notify).with do |message, exception|
        assert_includes message, "Durable job: unable to audit job FakeDurableJob with args [#{@account.id}, \"hello\""
        assert_equal ArgumentError, exception.class
      end
      FakeDurableJob.enqueue(@account.id, 'hello')
      payload = FakeDurableJob.worked.last
      Resque::Durable::QueueAudit.find_by_enqueued_id(payload.last)
    end
  end

  it "adds the account id to audits" do
    FakeDurableJob.enqueue(@account.id, 'hello')
    payload = FakeDurableJob.worked.last
    audit   = Resque::Durable::QueueAudit.find_by_enqueued_id(payload.last)
    assert_equal @account.id, audit.account_id
  end

  it "records audit exceptions instead of throwing them in production" do
    assert_raise(ActiveRecord::RecordInvalid) { FakeDurableJob.enqueue(@account.id, 'hello' * 5000) }
    Rails.env.stubs(:test?).returns(false)
    Resque.stubs(:inline?).returns(false)
    ZendeskExceptions::Logger.expects(:record)
    FakeDurableJob.enqueue(@account.id, 'hello' * 5000)
  end

  it "records Redis enqueue exceptions instead of throwing them in production" do
    Resque.stubs(:enqueue).raises(Errno::ECONNREFUSED.new("Unable to connect to Redis on etc..."))

    assert_raise(Errno::ECONNREFUSED) { FakeDurableJob.enqueue(@account.id, 'hello') }

    Rails.env.stubs(:test?).returns(false)
    ZendeskExceptions::Logger.expects(:record).with do |exception, params|
      assert_equal(Errno::ECONNREFUSED, exception.class)
      assert_equal(FakeDurableJob, params[:location])
      assert_includes(params[:message], "Durable job: unable to enqueue job FakeDurableJob with args [#{@account.id}, \"hello\"")
    end
    FakeDurableJob.enqueue(@account.id, 'hello')

    Resque.stubs(:enqueue).raises(ArgumentError.new)
    assert_raise(ArgumentError) { FakeDurableJob.enqueue(@account.id, 'hello') }
  end

  describe "environment" do
    let(:captured_environment) do
      [
        'ZendeskJob::Resque::Environment',
        {"cia" => {"actor" => ['Serialized-User', users(:minimum_agent).id]}, "enqueued_at" => Time.now.to_f}
      ]
    end
    let(:audit) do
      QueueAudit.new(
        job_klass: "FakeDurableJob",
        enqueued_id: "abc",
        payload: [@account.id, "hello", captured_environment],
        account_id: @account.id
      )
    end

    before do
      Resque::Durable::GUID.stubs(:generate).returns "NEW-UUID"
      Timecop.freeze
    end

    it "enqueues with audit and environment" do
      CIA.audit actor: users(:minimum_agent) do
        Resque.expects(:enqueue).with(FakeDurableJob, @account.id, "hello", captured_environment, "NEW-UUID")
        FakeDurableJob.enqueue(@account.id, 'hello')
      end
    end

    it "executes with restored/given environment" do
      audit.save!
      perform_job(FakeDurableJob, @account.id, "hello", captured_environment, audit.enqueued_id)
      assert_equal users(:minimum_agent), FakeDurableJob.worked.last[-2]
    end

    it "stores environment on failure" do
      CIA.audit actor: users(:minimum_agent) do
        Rails.env.stubs(:test?).returns(false)
        FakeDurableJob.enqueue(@account.id, 'hello')
        assert_equal [@account.id, "hello", captured_environment], QueueAudit.last.payload
      end
    end

    it "restores environment from previous failure" do
      CIA.audit actor: users(:minimum_admin) do
        Resque.expects(:enqueue).with(FakeDurableJob, @account.id, "hello", captured_environment, audit.enqueued_id)
        audit.enqueue
      end
    end
  end
end
