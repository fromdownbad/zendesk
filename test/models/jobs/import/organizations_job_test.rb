require_relative "../../../support/job_helper"

SingleCov.covered!

describe 'OrganizationsJob' do
  fixtures :accounts,
    :account_property_sets,
    :users,
    :user_identities,
    :organizations

  include CustomFieldsTestHelper

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_admin) }
  let(:update_records) { true }

  before do
    ActionMailer::Base.perform_deliveries = true
    @account = accounts(:minimum)
    @user = users(:minimum_admin)
    create_organization_custom_field!("text_field", "Text Field", "Text")
    create_organization_custom_field!("checkbox_field", "Checkbox Field", "Checkbox")
    stub_request(:put, %r{\.amazonaws.com/data/expirable_attachments/.*.csv})
  end

  describe "with non existent organization" do
    before do
      @token = token(@user, new_organizations_data)
      OrganizationsJob.perform(@user.account_id, @user.id, _update_records = true, @token)
    end

    should_change("the number of Organizations", by: 3) { Organization.count(:all) }

    # One for the upload token and one to download the file
    should_change("the number of Attachments", by: 1) { Attachment.count(:all) }
    should_change("the number of ExpirableAttachments", by: 1) { ExpirableAttachment.count(:all) }

    should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }

    it "creates an expirable attachment with the correct information" do
      attachment = ExpirableAttachment.last
      assert_equal 1.week.from_now.to_date, attachment.expire_at
      assert_equal 'text/csv', attachment.content_type
      assert_equal "OrganizationsJob", attachment.created_via
      result = File.open(attachment.public_filename).read
      assert result.include?('3 organizations created')
      assert result.include?('Hans')
      assert result.include?('Frank')
      assert result.include?('Niels')
    end

    it "sends an email with the correct information" do
      assert_equal [@user.email], ActionMailer::Base.deliveries.last.to
      assert_equal "Your organization import is ready", ActionMailer::Base.deliveries.last.subject
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(ExpirableAttachment.last.url(token: true))
    end
  end

  describe "variety in tags format" do
    before do
      @token = token(@user, existent_organization_data2)
      @user.account.settings.enable(:has_user_tags)
      @user.account.save!
      OrganizationsJob.perform(@user.account_id, @user.id, _update_records = true, @token)
      @organization = organizations(:minimum_organization1).reload
    end

    it "updates the organization with the correct data" do
      assert_equal 'bar foo', @organization.current_tags
    end
  end

  describe "with existent organization and update_records enabled" do
    before do
      @token = token(@user, existent_organization_data)
      @user.account.settings.enable(:has_user_tags)
      @user.account.save!
      OrganizationsJob.perform(@user.account_id, @user.id, _update_records = true, @token)
      @organization = organizations(:minimum_organization1).reload
    end

    # One for the upload token and one to download the file
    should_not_change("the number of Organizations") { Organization.count(:all) }

    # One for the upload token and one to download the file
    should_change("the number of Attachments", by: 1) { Attachment.count(:all) }
    should_change("the number of ExpirableAttachments", by: 1) { ExpirableAttachment.count(:all) }

    should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }

    it "creates an attachment with the correct information" do
      attachment = ExpirableAttachment.last
      assert_equal 1.week.from_now.to_date, attachment.expire_at
      assert_equal 'text/csv', attachment.content_type
      assert_equal "OrganizationsJob", attachment.created_via
      result = File.open(attachment.public_filename).read
      assert result.include?('1 organization updated')
      assert result.include?('minimum organization')
    end

    it "sends an email with the correct information" do
      assert_equal [@user.email], ActionMailer::Base.deliveries.last.to
      assert_equal "Your organization import is ready", ActionMailer::Base.deliveries.last.subject
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(ExpirableAttachment.last.url(token: true))
    end

    it "updates the organization with the correct data" do
      assert_equal 'minimum organization', @organization.name
      assert_equal 'New notes', @organization.notes
      assert_equal 'New details', @organization.details
      assert_equal 'bar foo', @organization.current_tags
      assert(@organization.is_shared)
      assert(@organization.is_shared_comments)
      assert_equal "1", @organization.custom_field_values.value_for_key("checkbox_field").value
      assert_equal "hihi", @organization.custom_field_values.value_for_key("text_field").value
    end
  end

  describe "with existent organization and update_records disabled" do
    before do
      @token = token(@user, existent_organization_data)
      OrganizationsJob.perform(@user.account_id, @user.id, _update_records = false, @token)
      @organization = organizations(:minimum_organization1).reload
    end

    # One for the upload token and one to download the file
    should_not_change("the number of Organizations") { Organization.count(:all) }

    # One for the upload token and one to download the file
    should_change("the number of Attachments", by: 1) { Attachment.count(:all) }
    should_change("the number of ExpirableAttachments", by: 1) { ExpirableAttachment.count(:all) }

    should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }

    it "creates a 1 week expirable attachment with the correct information" do
      attachment = ExpirableAttachment.last
      assert_equal 1.week.from_now.to_date, attachment.expire_at
      assert_equal 'text/csv', attachment.content_type
      assert_equal "OrganizationsJob", attachment.created_via
      result = File.open(attachment.public_filename).read
      assert result.include?('1 organization unchanged')
      assert result.include?('minimum organization')
    end

    it "sends an email with the correct information" do
      assert_equal [@user.email], ActionMailer::Base.deliveries.last.to
      assert_equal "Your organization import is ready", ActionMailer::Base.deliveries.last.subject
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(ExpirableAttachment.last.url(token: true))
    end

    it "does not update the organization" do
      assert_equal 'minimum organization', @organization.name
      assert_not_equal 'New notes', @organization.notes
      assert_not_equal 'New details', @organization.details
      assert_not_equal false, @organization.is_shared
      assert_not_equal true, @organization.is_shared_comments
    end
  end

  describe "with external_ids specified and existent organization matching an external_id" do
    before do
      @token = token(@user, existent_organization_with_external_id_data)
    end

    describe "with update_records enabled" do
      before do
        OrganizationsJob.perform(@user.account_id, @user.id, _update_records = true, @token)
        @organization = organizations(:minimum_organization3).reload
      end

      it "updates the organization" do
        assert_equal 'New name', @organization.name
        assert_equal 'New notes', @organization.notes
        assert_equal 'New details', @organization.details
      end

      should_not_change("the number of Organizations") { Organization.count(:all) }
    end

    describe "with update_records disabled" do
      before do
        OrganizationsJob.perform(@user.account_id, @user.id, _update_records = false, @token)
        @organization = organizations(:minimum_organization3).reload
      end

      it "does not update the organization" do
        assert_equal 'organization 3', @organization.name
        assert_not_equal 'New notes', @organization.notes
        assert_not_equal 'New details', @organization.details
      end

      should_not_change("the number of Organizations") { Organization.count(:all) }
    end
  end

  describe "with invalid headers" do
    before do
      @token = token(@user, invalid_headers_data)
      OrganizationsJob.perform(@user.account_id, @user.id, _update_records = false, @token)
    end

    # Only one for the upload token, the one for download should not be created
    should_change("the number of Attachments", by: 1) { Attachment.count(:all) }
    should_not_change("the number of ExpirableAttachments") { ExpirableAttachment.count(:all) }

    should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }

    should_not_change("the number of Organizations") { Organization.count(:all) }

    it "sends an email communicating the error" do
      assert_equal [@user.email], ActionMailer::Base.deliveries.last.to
      assert_equal "Organization import error", ActionMailer::Base.deliveries.last.subject
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?("Missing mandatory field in header: name")
    end
  end

  describe "instrument" do
    let(:statsd_client) { stub_for_statsd }
    let(:source) { 'source' }
    let(:organizations_count) { CSV.parse(new_organizations_data, headers: true).count }

    before do
      OrganizationsJob.stubs(:statsd_client).returns(statsd_client)
      statsd_client.expects(:increment).with("organization_bulk_import", tags: ["source:#{source}", "organizations_count:#{organizations_count}"]).once
    end

    it 'reports metrics to datadog' do
      @token = token(@user, new_organizations_data)
      OrganizationsJob.perform(@user.account_id, @user.id, _update_records = true, @token, source)
    end
  end

  describe '#process_row' do
    let(:csv_data) do
      <<~DATA
        name,default,domains
        "Both","default1.com","domain1.com"
      DATA
    end

    it "runs #process_row_organization_domains_and_emails to extract the data" do
      OrganizationsJob.expects(:process_row_organization_domains_and_emails).once
      OrganizationsJob.expects(:process_row_default_domains).never

      token = token(user, csv_data)
      OrganizationsJob.perform(account.id, user.id, update_records, token)
    end
  end

  describe "having a CSV with both DEFAULT and DOMAINS columns" do
    let(:csv_data) do
      <<~DATA
        name,domains,default
        "Domain","domains.com","default.com"
        "Email","email@domains.com","email@default.com"
        "Both","email@domains.org domains.org","email@default.com default.org",
      DATA
    end

    let(:domains_org) { account.organizations.where(name: 'Domain').first }
    let(:emails_org) { account.organizations.where(name: 'Email').first }
    let(:both_org) { account.organizations.where(name: 'Both').first }

    before do
      token = token(user, csv_data)
      OrganizationsJob.perform(account.id, user.id, update_records, token)
    end

    # It creates new OrganizationDomains and OrganizationEmails records
    should_change('the number of OrganizationDomain', by: 2) { OrganizationDomain.count(:all) }
    should_change('the number of OrganizationEmail', by: 2) { OrganizationEmail.count(:all) }

    # It creates new Organizations
    should_change('the number of Organization', by: 3) { Organization.count(:all) }

    it 'still writes to the Organizations.default attribute' do
      assert_equal ["domains.com"], domains_org.domain_names
      assert_equal ["email@domains.com"], emails_org.domain_names
      assert_equal ["email@domains.org", "domains.org"].sort, both_org.domain_names.sort
    end

    it 'extracts data from the DOMAINS column only' do
      assert_equal ["domains.com"],       domains_org.send(:organization_domains).map(&:value)
      assert_equal ["email@domains.com"], emails_org.send(:organization_emails).map(&:value)
      assert_equal ["email@domains.org"], both_org.send(:organization_emails).map(&:value)
      assert_equal ["domains.org"],       both_org.send(:organization_domains).map(&:value)
    end

    describe 'testing duplicated calls' do
      it 'logs which column has been used and the data extracted' do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(regexp_matches(/OrganizationsJob\: extracting DOMAINS for \'#{account.subdomain}\' account\:.*/)).times(3)
        Rails.logger.expects(:info).with(regexp_matches(/OrganizationsJob\: extracting DEFAULT for \'#{account.subdomain}\' account\:.*/)).never

        token = token(user, csv_data)
        OrganizationsJob.perform(account.id, user.id, update_records, token)
      end

      it 'calls domain_names= only once per row' do
        Organization.any_instance.expects(:domain_names=).with('domains.com').once
        Organization.any_instance.expects(:domain_names=).with('email@domains.com').once
        Organization.any_instance.expects(:domain_names=).with('email@domains.org domains.org').once

        token = token(user, csv_data)
        OrganizationsJob.perform(account.id, user.id, update_records, token)
      end
    end
  end

  describe "having a CSV with only DOMAINS column" do
    let(:csv_data) do
      <<~DATA
        name,domains
        "Domain","domains.com"
        "Email","email@domains.com"
        "Both","email@domains.org domains.org"
      DATA
    end

    let(:domains_org) { account.organizations.where(name: 'Domain').first }
    let(:emails_org) { account.organizations.where(name: 'Email').first }
    let(:both_org) { account.organizations.where(name: 'Both').first }

    before do
      token = token(user, csv_data)
      OrganizationsJob.perform(account.id, user.id, update_records, token)
    end

    # It creates new OrganizationDomains and OrganizationEmails records
    should_change('the number of OrganizationDomain', by: 2) { OrganizationDomain.count(:all) }
    should_change('the number of OrganizationEmail', by: 2) { OrganizationEmail.count(:all) }

    # It creates new Organizations
    should_change('the number of Organization', by: 3) { Organization.count(:all) }

    it 'still writes to the Organizations.default attribute' do
      assert_equal ["domains.com"], domains_org.domain_names
      assert_equal ["email@domains.com"], emails_org.domain_names
      assert_equal ["email@domains.org", "domains.org"].sort, both_org.domain_names.sort
    end

    it 'extracts data from the DOMAINS column' do
      assert_equal ["domains.com"],       domains_org.send(:organization_domains).map(&:value)
      assert_equal ["email@domains.com"], emails_org.send(:organization_emails).map(&:value)
      assert_equal ["email@domains.org"], both_org.send(:organization_emails).map(&:value)
      assert_equal ["domains.org"],       both_org.send(:organization_domains).map(&:value)
    end

    describe 'testing duplicated calls' do
      it 'logs which column has been used and the data extracted' do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(regexp_matches(/OrganizationsJob\: extracting DOMAINS for \'#{account.subdomain}\' account\:.*/)).times(3)
        Rails.logger.expects(:info).with(regexp_matches(/OrganizationsJob\: extracting DEFAULT for \'#{account.subdomain}\' account\:.*/)).never

        token = token(user, csv_data)
        OrganizationsJob.perform(account.id, user.id, update_records, token)
      end

      it 'calls domain_names= only once per row' do
        Organization.any_instance.expects(:domain_names=).with('domains.com').once
        Organization.any_instance.expects(:domain_names=).with('email@domains.com').once
        Organization.any_instance.expects(:domain_names=).with('email@domains.org domains.org').once

        token = token(user, csv_data)
        OrganizationsJob.perform(account.id, user.id, update_records, token)
      end
    end
  end

  describe "having a CSV with only DEFAULT column" do
    let(:csv_data) do
      <<~DATA
        name,default
        "Domain","default.com"
        "Email","email@default.com"
        "Both","email@default.org default.org"
      DATA
    end

    let(:domains_org) { account.organizations.where(name: 'Domain').first }
    let(:emails_org) { account.organizations.where(name: 'Email').first }
    let(:both_org) { account.organizations.where(name: 'Both').first }

    before do
      token = token(user, csv_data)
      OrganizationsJob.perform(account.id, user.id, update_records, token)
    end

    # It creates new OrganizationDomains and OrganizationEmails records
    should_change('the number of OrganizationDomain', by: 2) { OrganizationDomain.count(:all) }
    should_change('the number of OrganizationEmail', by: 2) { OrganizationEmail.count(:all) }

    # It creates new Organizations
    should_change('the number of Organization', by: 3) { Organization.count(:all) }

    it 'still writes to the Organizations.domain_names attribute' do
      assert_equal ["default.com"], domains_org.domain_names
      assert_equal ["email@default.com"], emails_org.domain_names
      assert_equal ["email@default.org", "default.org"].sort, both_org.domain_names.sort
    end

    it 'extracts data from the DEFAULT column' do
      assert_equal ["default.com"],       domains_org.send(:organization_domains).map(&:value)
      assert_equal ["email@default.com"], emails_org.send(:organization_emails).map(&:value)
      assert_equal ["email@default.org"], both_org.send(:organization_emails).map(&:value)
      assert_equal ["default.org"],       both_org.send(:organization_domains).map(&:value)
    end

    describe 'testing duplicated calls' do
      it 'logs which column has been used and the data extracted' do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(regexp_matches(/OrganizationsJob\: extracting DEFAULT for \'#{account.subdomain}\' account\:.*/)).times(3)
        Rails.logger.expects(:info).with(regexp_matches(/OrganizationsJob\: extracting DOMAINS for \'#{account.subdomain}\' account\:.*/)).never

        token = token(user, csv_data)
        OrganizationsJob.perform(account.id, user.id, update_records, token)
      end

      it 'calls domain_names= only once per row' do
        Organization.any_instance.expects(:domain_names=).with('default.com').once
        Organization.any_instance.expects(:domain_names=).with('email@default.com').once
        Organization.any_instance.expects(:domain_names=).with('email@default.org default.org').once

        token = token(user, csv_data)
        OrganizationsJob.perform(account.id, user.id, update_records, token)
      end
    end
  end

  private

  def new_organizations_data
    <<~DATA
      name,group,domains
      Hans,group1,domain1.com
      Frank,group2,domain2.com
      Niels,group3,domain3.com
    DATA
  end

  def existent_organization_data
    <<~DATA
      name,notes,details,shared,shared_comments,tags,custom_fields.checkbox_field,custom_fields.text_field
      "minimum organization","New notes","New details",true,true,"foo, bar",true,"hihi"
    DATA
  end

  def existent_organization_data2
    <<~DATA
      name,notes,details,shared,shared_comments,tags
      "minimum organization","New notes","New details",true,true,"foo,bar"
    DATA
  end

  def existent_organization_with_external_id_data
    <<~DATA
      name,external_id,notes,details
      "New name","organization-3","New notes","New details"
    DATA
  end

  def invalid_headers_data
    <<~DATA
      notes,details,shared,shared_comments,tags
      "New notes","New details",true,true,"foo, bar"
    DATA
  end

  def token(current_user, csv_data)
    account = current_user.account

    token = account.upload_tokens.create(source: current_user)
    token.add_raw_attachment(csv_data, 'import').save
    token.value
  end
end
