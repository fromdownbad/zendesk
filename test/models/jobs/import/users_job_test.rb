require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

describe UsersJob do
  fixtures :all
  include CustomFieldsTestHelper

  def perform_external_id(token, report_external_id = false)
    CIA.audit actor: admin do
      UsersJob.perform(account_id, admin.id, update_records, token, _password_email_change_csv_import = true, 'source', true, report_external_id)
    end
  end

  def perform(token)
    CIA.audit actor: admin do
      UsersJob.perform(account_id, admin.id, update_records, token)
    end
  end

  before do
    @account = accounts(:minimum)
    Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true)
    create_user_custom_field!("text_field", "Text Field", "Text")
    create_user_custom_field!("checkbox_field", "Checkbox Field", "Checkbox")
    create_user_custom_field!("custom_integer", "Integer Field", "Integer")
  end

  let(:organization1)       { organizations(:minimum_organization1) }
  let(:update_records)      { true }
  let(:dont_update_records) { false }
  let(:admin)               { users(:minimum_admin) }
  let(:agent)               { users(:minimum_agent) }
  let(:update_user)         { users(:minimum_end_user) }
  let(:account_id)          { admin.account_id }
  let(:brand1)              { FactoryBot.create(:brand, account_id: account_id, subdomain: "wombats", name: "Wombats Brand") }
  let(:phone)               { '' }
  let(:headers_array) do
    %w[ name email password phone notes external_id
        details role organization restriction tags
        custom_fields.checkbox_field custom_fields.text_field custom_fields.custom_integer brand ]
  end
  let(:new_user_array) do
    [
      "John Doe",               # name
      "john@company.com",       # email
      "123456",                 # password
      phone,                    # phone
      "Some notes about John",  # notes
      "11111",                  # external_id
      "Some details about John", # details
      "Agent",                  # role
      organization1.name,       # organization
      "Assigned",               # restrictions
      "foo, bar",               # tags
      true,                     # custom_checkbox_field
      "hihi",                   # custom_text_field
      "1",                      # custom_custom_integer
      brand1.subdomain          # subdomain of the brand to use for welome mail
    ]
  end

  let(:new_end_user_array) do
    [
      "John Doe",               # name
      "john@company.com",       # email
      "123456",                 # password
      phone,                    # phone
      "Some notes about John",  # notes
      "11111",                  # external_id
      "Some details about John", # details
      "End User",               # role
      organization1.name,       # organization
      "Assigned",               # restrictions
      "foo, bar",               # tags
      true,                     # custom_checkbox_field
      "hihi",                   # custom_text_field
      "1",                      # custom_custom_integer
      brand1.subdomain          # subdomain of the brand to use for welome mail
    ]
  end

  let(:user_with_shared_number_flag_as_true) do
    "name,email,phone,external_id,shared_phone_number\n" \
    "\"Carmelia Donald\",cd-variation-1@example.com,12125550000,CDVAR1,true\n" \
  end

  let(:user_with_shared_number_flag_as_false) do
    "name,email,phone,external_id,shared_phone_number\n" \
    "\"Michaele Helvey\",mh-variation-2@example.com,12125550000,MHVAR2,false\n" \
  end

  let(:multiple_users_with_shared_number_flag_as_false) do
    "name,email,phone,shared_phone_number,external_id\n" \
    "\"Delphia Ruiz\",dr-variation-3@example.com,12125550000,false,DRVAR3\n" \
    "\"Andera Kimbrel\",ak-variation-3@example.com,12125550000,false,AKVAR3\n"
  end

  let(:multiple_users_with_shared_number_flag_as_true) do
    "name,email,phone,shared_phone_number,external_id\n" \
    "\"Delphia Ruiz\",dr-variation-3@example.com,12125550000,true,DRVAR3\n" \
    "\"Andera Kimbrel\",ak-variation-3@example.com,12125550000,true,AKVAR3\n"
  end

  let(:multiple_users_with_no_shared_number_flag_set) do
    "name,email,phone,external_id\n" \
    "\"Delphia Ruiz\",dr-variation-3@example.com,12125550000,DRVAR3\n" \
    "\"Andera Kimbrel\",ak-variation-3@example.com,12125550000,AKVAR3\n"
  end

  let(:multiple_phone_numbers_csv) do
    "name,email,phone,external_id\n" \
    "\"John Smith\",jsmith@example.com,+12125550000,9999\n" \
    "\"John Smith\",jsmith@example.com,,9999\n" \
    "\"John Smith\",jsmith@example.org,+12125550001x123,9999\n"
  end
  let(:email_phone_csv) do
    "name,email,external_id,details,notes,phone\n" \
    "\"John Smith\",jsmith@example.com,,,,173666132\n"
  end

  before do
    ActionMailer::Base.perform_deliveries = true
    stub_request(:put, %r{\.amazonaws.com/data/expirable_attachments/.*.csv})
  end

  it "imports data with poorly formatted column names (trailing whitespace, mixed capitalization)" do
    token = generate_token(admin, new_user_data(unformatted_headers))
    perform(token)

    user = User.last
    assert_equal 'John Doe', user.name
    assert_equal 'john@company.com', user.email
  end

  it "imports data with multiple phone numbers" do
    token = generate_token(admin, multiple_phone_numbers_csv)
    perform(token)

    user = User.last
    assert_equal 2, user.identities.email.count(:all)
    assert_equal 2, user.identities.phone.count(:all)
  end

  it "imports data twice with multiple phone numbers" do
    token = generate_token(admin, multiple_phone_numbers_csv)
    perform(token)

    token = generate_token(admin, multiple_phone_numbers_csv)
    perform(token)

    user = User.last
    assert_equal 2, user.identities.email.count(:all)
    assert_equal 2, user.identities.phone.count(:all)
  end

  it "logs session info and other useful data via the Zendesk::Resque::BaseJob perform" do
    UsersJob.stubs(:deliver_job_complete) # don't really care about mail for this test
    Zendesk::Gooddata::UserProvisioning.stubs(:create_for_user)
    UserEmailIdentity.any_instance.stubs(:deliver_verification_email)
    token = generate_token(admin, new_user_data(unformatted_headers))
    perform(token)
  end

  describe "with email and phone, in an account with HC" do
    it "delivers verification email" do
      @account.enable_help_center!
      Account.any_instance.stubs(:is_welcome_email_when_agent_register_enabled?).returns(true)
      Users::PhoneNumber.any_instance.stubs(:valid_and_unique?).returns(true)
      UsersMailer.expects(:deliver_verify)
      token = generate_token(admin, email_phone_csv)
      perform(token)
    end
  end

  describe "with non existent user and reporting external id" do
    before do
      admin.account.settings.has_user_tags = true
      admin.account.settings.lookup(:has_user_tags).save!
      Account.any_instance.stubs(:admins_can_set_user_passwords).returns(true)
      token = generate_token(admin, new_user_data)
      perform_external_id(token, false)
    end

    should_create :user

    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    # One for the upload token and one to download the file
    should_change("the number of ExpirableAttachments", by: 1) do
      ExpirableAttachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 2) do
      ActionMailer::Base.deliveries.size
    end

    it "creates an attachment with the correct information" do
      attachment = ExpirableAttachment.last
      assert_equal 'text/csv', attachment.content_type
      assert_equal 'UsersJob', attachment.created_via
      result = File.open(attachment.public_filename).read
      assert result.include?('1 user created')
      assert result.include?('john@company.com')
      assert !result.include?('11111')
    end

    it "sends an email with the correct information" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert_equal(
        "Your user import is ready", ActionMailer::Base.deliveries.last.subject
      )
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        ExpirableAttachment.last.url(token: true)
      )
    end

    it "creates a user with the correct data" do
      user = User.last
      assert_equal 'John Doe', user.name
      assert_equal 'john@company.com', user.email
      assert       user.phone.blank?
      assert_equal 'Some notes about John', user.notes
      assert_equal '11111', user.external_id
      assert_equal 'Some details about John', user.details
      assert_equal 'bar foo', user.current_tags
      assert_equal organization1, user.organization
      assert(user.is_agent?)
      assert_equal RoleRestrictionType.ASSIGNED, user.restriction_id
      assert_equal 'hihi', user.custom_field_values.value_for_key("text_field").value
      assert_equal '1', user.custom_field_values.value_for_key("checkbox_field").value
      assert_equal 1, user.custom_field_values.value_for_key("custom_integer").value.to_i
    end

    it "does not send a verification email to the created user" do
      UsersMailer.expects(:deliver_verify).never
    end
  end

  describe "with non existent user and reporting external id" do
    before do
      admin.account.settings.has_user_tags = true
      admin.account.settings.lookup(:has_user_tags).save!
      Account.any_instance.stubs(:admins_can_set_user_passwords).returns(true)
      token = generate_token(admin, new_user_data)
      perform_external_id(token, true)
    end

    should_create :user

    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    # One for the upload token and one to download the file
    should_change("the number of ExpirableAttachments", by: 1) do
      ExpirableAttachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 2) do
      ActionMailer::Base.deliveries.size
    end

    it "creates an attachment with the correct information" do
      attachment = ExpirableAttachment.last
      assert_equal 'text/csv', attachment.content_type
      assert_equal 'UsersJob', attachment.created_via
      result = File.open(attachment.public_filename).read
      assert result.include?('1 user created')
      assert result.include?('john@company.com')
      assert result.include?('11111')
    end

    it "sends an email with the correct information" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert_equal(
        "Your user import is ready", ActionMailer::Base.deliveries.last.subject
      )
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        ExpirableAttachment.last.url(token: true)
      )
    end

    it "creates a user with the correct data" do
      user = User.last
      assert_equal 'John Doe', user.name
      assert_equal 'john@company.com', user.email
      assert       user.phone.blank?
      assert_equal 'Some notes about John', user.notes
      assert_equal '11111', user.external_id
      assert_equal 'Some details about John', user.details
      assert_equal 'bar foo', user.current_tags
      assert_equal organization1, user.organization
      assert(user.is_agent?)
      assert_equal RoleRestrictionType.ASSIGNED, user.restriction_id
      assert_equal 'hihi', user.custom_field_values.value_for_key("text_field").value
      assert_equal '1', user.custom_field_values.value_for_key("checkbox_field").value
      assert_equal 1, user.custom_field_values.value_for_key("custom_integer").value.to_i
    end

    it "does not send a verification email to the created user" do
      UsersMailer.expects(:deliver_verify).never
    end
  end

  describe "with shared_phone_number attribute" do
    describe "with end user phone number validation enabled" do
      before do
        Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
        Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(false)
      end

      it "should not create identity when shared_phone_number is true" do
        token = generate_token(admin, user_with_shared_number_flag_as_true)
        perform(token)

        user = User.last
        assert_equal 0, user.identities.phone.count(:all)
        assert_equal user.phone, "12125550000"
      end

      it "should create identity and not set shared phone number when shared_phone_number is false" do
        token = generate_token(admin, user_with_shared_number_flag_as_false)
        perform(token)

        user = User.last
        assert_equal 1, user.identities.phone.count(:all)
        assert_nil user.phone
        assert_equal user.phone_number, "+12125550000"
      end

      it "when shared_phone_number is false, should create identity only when phone number is unique" do
        token = generate_token(admin, multiple_users_with_shared_number_flag_as_false)
        perform(token)

        user_with_identity = User.find_by_name("Delphia Ruiz")
        user_with_number = User.find_by_name("Andera Kimbrel")

        assert_equal 1, user_with_identity.identities.phone.count(:all)
        assert_nil user_with_identity.phone
        assert_equal user_with_identity.phone_number, "+12125550000"
        assert_equal 0, user_with_number.identities.phone.count(:all)
        assert_equal user_with_number.phone, "12125550000"
        assert_equal user_with_number.phone_number, "12125550000"
      end

      it "when shared_phone_number is true, should not create identity whether phone number is unique or not" do
        token = generate_token(admin, multiple_users_with_shared_number_flag_as_true)
        perform(token)

        user_with_number1 = User.find_by_name("Delphia Ruiz")
        user_with_number2 = User.find_by_name("Andera Kimbrel")

        assert_equal user_with_number1.phone, "12125550000"
        assert_equal user_with_number2.phone, "12125550000"
        assert_equal 0, user_with_number1.identities.phone.count(:all)
        assert_equal 0, user_with_number2.identities.phone.count(:all)
      end

      it "when shared_phone_number is absent, should create identity only when phone number is unique" do
        token = generate_token(admin, multiple_users_with_no_shared_number_flag_set)
        perform(token)

        user_with_identity = User.find_by_name("Delphia Ruiz")
        user_with_number = User.find_by_name("Andera Kimbrel")

        assert_equal 1, user_with_identity.identities.phone.count(:all)
        assert_nil user_with_identity.phone
        assert_equal user_with_identity.phone_number, "+12125550000"
        assert_equal 0, user_with_number.identities.phone.count(:all)
        assert_equal user_with_number.phone, "12125550000"
        assert_equal user_with_number.phone_number, "12125550000"
      end
    end

    describe "with end user phone number validation disabled" do
      before do
        Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(false)
        Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(false)
      end
      it "should not create identity when shared_phone_number is true" do
        token = generate_token(admin, user_with_shared_number_flag_as_true)
        perform(token)

        user = User.last
        assert_equal 0, user.identities.phone.count(:all)
        assert_equal user.phone, "12125550000"
      end

      it "should create identity when shared_phone_number is false" do
        token = generate_token(admin, user_with_shared_number_flag_as_false)
        perform(token)

        user = User.last
        assert_equal 1, user.identities.phone.count(:all)
      end

      it "when shared_phone_number is false, should create identity only when phone number is unique" do
        token = generate_token(admin, multiple_users_with_shared_number_flag_as_false)
        perform(token)

        user_with_identity = User.find_by_name("Delphia Ruiz")
        user_with_number = User.find_by_name("Andera Kimbrel")

        assert_equal 1, user_with_identity.identities.phone.count(:all)
        assert_equal 0, user_with_number.identities.phone.count(:all)
        assert_equal user_with_number.phone, "12125550000"
      end

      it "when shared_phone_number is true, should not create identity whether phone number is unique or not" do
        token = generate_token(admin, multiple_users_with_shared_number_flag_as_true)
        perform(token)

        user_with_number1 = User.find_by_name("Delphia Ruiz")
        user_with_number2 = User.find_by_name("Andera Kimbrel")

        assert_equal user_with_number1.phone, "12125550000"
        assert_equal user_with_number2.phone, "12125550000"
        assert_equal 0, user_with_number1.identities.phone.count(:all)
        assert_equal 0, user_with_number2.identities.phone.count(:all)
      end

      it "when shared_phone_number is absent, should create identity only when phone number is unique" do
        token = generate_token(admin, multiple_users_with_no_shared_number_flag_set)
        perform(token)

        user_with_identity = User.find_by_name("Delphia Ruiz")
        user_with_number = User.find_by_name("Andera Kimbrel")

        assert_equal 1, user_with_identity.identities.phone.count(:all)
        assert_equal 0, user_with_number.identities.phone.count(:all)
        assert_equal user_with_number.phone, "12125550000"
      end
    end
  end

  describe "with Enterprise account and setting roles" do
    let(:permission_sets) { admin.account.permission_sets }
    let(:import_csv) do
      @token = generate_token(admin, enterprise_user_data)
      perform(@token)
    end

    before do
      Account.any_instance.stubs(has_permission_sets?: true)
      Account.any_instance.stubs(uses_12_hour_clock: true)
      Account.any_instance.stubs(is_ssl_enabled?: true)
      Account.any_instance.stubs(admins_can_set_user_passwords: true)
      Account.any_instance.stubs(web_portal_disabled?: false)
      Account.any_instance.stubs(help_center_enabled?: false)

      PermissionSet.create(account: admin.account, name: "Pure Awesome")
      PermissionSet.create_light_agent!(admin.account)
    end

    it "creates a user with a Light Agent, Enterprise role" do
      import_csv
      assert_equal(
        permission_sets.find_by_name("Light Agent").id,
        User.find_by_name("Heavenly Agent").permission_set_id
      )
    end

    it "creates end-users that are uploaded" do
      import_csv
      user = User.find_by_name("Mr. EndUser")
      assert_nil user.permission_set_id
      assert(user.is_end_user?)
    end

    it "creates a user with the correct custom Enterprise role" do
      import_csv
      assert_equal(
        permission_sets.find_by_name("Pure Awesome").id,
        User.find_by_name("Custom Agent").permission_set_id
      )
    end

    it "falls back on Light Agent if adding an agent and custom Enterprise role does not exist" do
      import_csv
      assert_equal(
        permission_sets.find_by_name("Light Agent").id,
        User.find_by_name("Heavenly Agent").permission_set_id
      )
    end

    it "fallbacks to Light agent restriction even if their light agent role is in another language" do
      import_csv
      light_agent_permission_set = permission_sets.find_by_name("Light Agent")
      light_agent_permission_set.update_attribute(:name, "Agent light")

      perform(@token)

      refute permission_sets.find_by_name("Light Agent")

      assert_equal(
        permission_sets.find_by_name("Agent light").id,
        User.find_by_name("Heavenly Agent").permission_set_id
      )
    end

    describe "with a blank restricted field" do
      it "creates an admin without a permission set" do
        import_csv
        assert_nil User.find_by_name("Admin").permission_set_id
      end

      describe "without a light agent permission set" do
        before { admin.account.light_agent_permission_set.destroy }

        it "creates the agent without a light agent permission set" do
          import_csv
          assert_nil User.find_by_name("Agent").permission_set_id
        end
      end
    end
  end

  describe "with Professional account and light_agents add-on" do
    let(:permission_sets) { admin.account.permission_sets }

    before do
      Account.any_instance.stubs(has_permission_sets?: false)
      Account.any_instance.stubs(has_light_agents?: true)
      Account.any_instance.stubs(uses_12_hour_clock: true)
      Account.any_instance.stubs(is_ssl_enabled?: true)
      Account.any_instance.stubs(admins_can_set_user_passwords: true)
      Account.any_instance.stubs(web_portal_disabled?: false)
      Account.any_instance.stubs(help_center_enabled?: false)

      PermissionSet.create_light_agent!(admin.account)

      perform(generate_token(admin, enterprise_user_data))
    end

    it "creates a user with the Light Agent role" do
      assert_equal(
        permission_sets.find_by_name("Light Agent").id,
        User.find_by_name("Heavenly Agent").permission_set_id
      )
    end

    it "creates end-users that are uploaded" do
      user = User.find_by_name("Mr. EndUser")
      assert_nil user.permission_set_id
      assert(user.is_end_user?)
    end

    it "does not create a custom permission set but creates an enduser" do
      custom_agent = User.find_by_name("Custom Agent")
      assert_nil custom_agent.permission_set_id
      assert(custom_agent.is_agent?)
    end
  end

  describe "adding a second user identity" do
    before do
      update_user.identities.each { |identity| identity.update_attribute(:is_verified, false) }
      assert(!update_user.is_verified?)

      data = add_multiple_user_identities_data(update_user)
      admin.account.settings.has_user_tags = true
      admin.account.settings.lookup(:has_user_tags).save!
      token = generate_token(admin, data)
      perform(token)
    end

    it "sends an email with the correct information" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      attachment = ExpirableAttachment.last
      result = File.open(attachment.public_filename).read

      assert result.include?(
        "2 users updated"
      ), 'should not error for name field'
      refute result.include?(
        "Name:  is too short"
      ), 'should not error for name field'
      assert_equal(
        "Your user import is ready", ActionMailer::Base.deliveries.last.subject
      )
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        ExpirableAttachment.last.url(token: true)
      )
    end
  end

  describe "with existent user and update_records enabled" do
    before do
      update_user.identities.each { |identity| identity.update_attribute(:is_verified, false) }
      assert(!update_user.is_verified?)

      # we should remove trailing space in the email before finding
      data = existent_user_data(update_user)
      data.gsub!(update_user.email, "  #{update_user.email}  ")

      admin.account.settings.has_user_tags = true
      admin.account.settings.lookup(:has_user_tags).save!
      token = generate_token(admin, data)
      perform(token)
    end

    should_not_change("the number of Users") { User.count(:all) }

    # One for the upload token and one to download the file
    should_change("the number of ExpirableAttachments", by: 1) do
      ExpirableAttachment.count(:all)
    end

    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 1) do
      ActionMailer::Base.deliveries.size
    end

    it "creates an attachment with the correct information" do
      attachment = ExpirableAttachment.last
      assert_equal 'text/csv', attachment.content_type
      assert_equal 'UsersJob', attachment.created_via
      result = File.open(attachment.public_filename).read
      assert result.include?('1 user updated')
      assert result.include?(update_user.email)
    end

    it "sends an email with the correct information" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        ExpirableAttachment.last.url(token: true)
      )
    end

    it "updates the user with the correct data" do
      assert_equal 'minimum_end_user@aghassipour.com', update_user.reload.email
      assert_equal 'John Doe', update_user.name
      assert       update_user.phone.blank?
      assert_equal 'Some notes about John', update_user.notes
      assert_equal '11111', update_user.external_id
      assert_equal 'Some details about John', update_user.details
      assert update_user.current_tags.include?('bar')
      assert update_user.current_tags.include?('foo')
      assert_equal organization1, update_user.organization
      assert(update_user.is_agent?)
      assert_equal RoleRestrictionType.ASSIGNED, update_user.restriction_id
    end

    it "does not verify the user" do
      assert(!update_user.reload.is_verified)
    end

    it "appends data when updating notes,details, or tags fields" do
      assert_equal 'Some notes about John', update_user.reload.notes
      assert_equal 'Some details about John', update_user.details
      assert update_user.current_tags.include?('bar')
      assert update_user.current_tags.include?('foo')

      new_token = generate_token(admin, update_user_data(update_user))
      perform(new_token)

      new_user = update_user.reload

      assert_equal 'Some notes about JohnSome more notes about John', new_user.notes
      assert_equal(
        'Some details about John',
        new_user.details,
        "Importing an empty column to an appended attribute should do nothing"
      )
      assert update_user.current_tags.include?('bar')
      assert update_user.current_tags.include?('foo')
      assert update_user.current_tags.include?('fun')
      assert update_user.current_tags.include?('wombat')
    end
  end

  describe "with existent user and update_records enabled and email blank ok" do
    before do
      update_user.identities.each { |identity| identity.update_attribute(:is_verified, false) }
      update_user.send("external_id=", '11111')
      update_user.save!
      update_user.reload
      assert(!update_user.is_verified?)
      # we should remove trailing space in the email before finding
      data = existent_user_data(update_user)
      data.gsub!("minimum_end_user@aghassipour.com", "")
      admin.account.settings.has_user_tags = true
      admin.account.settings.lookup(:has_user_tags).save!
      token = generate_token(admin, data)
      perform(token)
    end

    should_not_change("the number of Users") { User.count(:all) }

    # One for the upload token and one to download the file
    should_change("the number of ExpirableAttachments", by: 1) do
      ExpirableAttachment.count(:all)
    end

    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 1) do
      ActionMailer::Base.deliveries.size
    end

    it "creates an attachment with the correct information" do
      attachment = ExpirableAttachment.last
      assert_equal 'text/csv', attachment.content_type
      assert_equal 'UsersJob', attachment.created_via
      result = File.open(attachment.public_filename).read
      assert result.include?('1 user updated')
      assert result.include?(update_user.email)
    end

    it "sends an email with the correct information" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        ExpirableAttachment.last.url(token: true)
      )
    end

    it "updates the user with the correct data" do
      assert_equal 'minimum_end_user@aghassipour.com', update_user.reload.email
      assert_equal 'John Doe', update_user.name
      assert       update_user.phone.blank?
      assert_equal 'Some notes about John', update_user.notes
      assert_equal '11111', update_user.external_id
      assert_equal 'Some details about John', update_user.details
      assert update_user.current_tags.include?('bar')
      assert update_user.current_tags.include?('foo')
      assert_equal organization1, update_user.organization
      assert(update_user.is_agent?)
      assert_equal RoleRestrictionType.ASSIGNED, update_user.restriction_id
    end

    it "does not verify the user" do
      assert(!update_user.reload.is_verified)
    end

    it "appends data when updating notes,details, or tags fields" do
      assert_equal 'Some notes about John', update_user.reload.notes
      assert_equal 'Some details about John', update_user.details
      assert update_user.current_tags.include?('bar')
      assert update_user.current_tags.include?('foo')

      new_token = generate_token(admin, update_user_data(update_user))
      perform(new_token)

      new_user = update_user.reload

      assert_equal 'Some notes about JohnSome more notes about John', new_user.notes
      assert_equal(
        'Some details about John',
        new_user.details,
        "Importing an empty column to an appended attribute should do nothing"
      )
      assert update_user.current_tags.include?('bar')
      assert update_user.current_tags.include?('foo')
      assert update_user.current_tags.include?('fun')
      assert update_user.current_tags.include?('wombat')
    end
  end
  describe "if the password column is not present" do
    it "does not crash" do
      update_user.identities.each do |identity|
        identity.update_attribute(:is_verified, false)
      end
      assert(!update_user.is_verified?)

      admin.account.settings.has_user_tags = true
      admin.account.settings.lookup(:has_user_tags).save!
      token = generate_token(admin, user_data_without_password(update_user))

      perform(token)
    end
  end

  describe "if the password column is present" do
    describe "for an existing user" do
      let(:token) { generate_token(admin, existent_user_data(update_user)) }

      before do
        update_user.identities.each do |identity|
          identity.update_attribute(:is_verified, false)
        end
        assert(!update_user.is_verified?)

        admin.account.settings.has_user_tags = true
        admin.account.settings.lookup(:has_user_tags).save!
      end

      describe "and admins_can_set_user_passwords" do
        before do
          Account.any_instance.stubs(:admins_can_set_user_passwords).returns(true)
        end

        describe "when the user's password is not blank" do
          it "sets the existing user's password" do
            User.any_instance.expects(:password=)
            perform(token)
          end

          it "doesn't send the email for password change if it is requested that way" do
            SecurityMailer.expects(:deliver_profile_crypted_password_changed).never
            UsersJob.perform(account_id, admin.id, update_records, token, false)
          end

          it "sends the email for the password change if it is requested" do
            SecurityMailer.expects(:deliver_profile_crypted_password_changed)
            UsersJob.perform(account_id, admin.id, update_records, token, true)
          end
        end

        it "does not set the existing user's password if blank" do
          token = generate_token(admin, existent_user_data(update_user).gsub(/123456/, ''))
          User.any_instance.expects(:password=).never
          perform(token)
        end
      end

      describe "and !admins_can_set_user_passwords" do
        before do
          Account.any_instance.stubs(:admins_can_set_user_passwords).returns(false)
        end

        it "does not set the existing user's password" do
          User.any_instance.expects(:password=).never
          perform(token)
        end
      end
    end

    describe "with non existent user" do
      let(:token) { generate_token(admin, new_user_data) }

      before do
        admin.account.settings.has_user_tags = true
        admin.account.settings.lookup(:has_user_tags).save!
      end

      describe "and admins_can_set_user_passwords" do
        before do
          Account.any_instance.stubs(:admins_can_set_user_passwords).returns(true)
        end

        it "sets the new user's password" do
          User.any_instance.expects(:password=)
          perform(token)
        end
      end

      describe "and !admins_can_set_user_passwords" do
        before do
          Account.any_instance.stubs(:admins_can_set_user_passwords).returns(false)
        end

        it "does not set the new user's password" do
          User.any_instance.expects(:password=).never
          perform(token)
        end
      end
    end
  end

  describe "if the role column is present" do
    describe "when creating user" do
      describe 'without the name field' do
        before do
          admin.account.settings.has_user_tags = true
          admin.account.settings.lookup(:has_user_tags).save!
          Account.any_instance.stubs(:admins_can_set_user_passwords).returns(true)
          token = generate_token(admin, new_user_data.sub("John Doe", ""))
          perform(token)
        end

        it "it generates an error" do
          attachment = ExpirableAttachment.last
          result = File.open(attachment.public_filename).read

          assert result.include?(
            "1 user failed"
          ), 'should not create a user record'
          assert result.include?(
            "Name:  is too short"
          ), 'should error for name field'
        end
      end

      describe 'with a valid role' do
        it "is case insensitive" do
          token = generate_token(admin, new_agent_data("aGeNt", "agent@example.com", ""))
          perform(token)
          assert_equal "Agent", User.last.role
        end
      end

      describe 'with an invalid role' do
        it 'defaults to end-user' do
          token = generate_token(admin, new_agent_data("invalid", "agent@example.com", ""))
          perform(token)

          assert_equal "End-user", User.last.role
        end
      end
    end

    describe "when updating user" do
      describe 'with a valid role' do
        before do
          Zendesk::SupportUsers::User.any_instance.stubs(:perform_before_save_actions!)
          Zendesk::SupportUsers::User.any_instance.stubs(:perform_after_save_actions!)
          UsersJob.stubs(:deliver_job_complete)
        end

        describe 'when allow_agent_downgrade is on' do
          let(:allow_agent_downgrade) { true }

          it 'updates user role' do
            token = generate_token(admin, update_admin_data(agent, 'ADMIN'))
            perform(token, allow_agent_downgrade)

            assert_equal 'Admin', agent.reload.role
          end

          describe 'user is downgrading from agent to end user' do
            it 'downgrades agent' do
              token = generate_token(admin, update_admin_data(agent, 'END-USER'))
              perform(token, allow_agent_downgrade)

              assert_equal 'End-user', agent.reload.role
            end
          end
        end

        describe 'when allow_agent_downgrade is off' do
          describe 'user is downgrading from agent to end user' do
            it 'ignores downgrade' do
              token = generate_token(admin, update_admin_data(agent, 'END-USER'))
              perform(token)

              assert_equal 'Agent', agent.reload.role
            end
          end

          describe 'user is not downgrading' do
            it 'updates user roles' do
              token = generate_token(admin, update_admin_data(agent, 'ADMIN'))
              perform(token)

              assert_equal 'Admin', agent.reload.role
            end
          end
        end

        let(:source) { 'source' }

        def perform(token, allow_agent_downgrade = false)
          CIA.audit actor: admin do
            UsersJob.perform(account_id, admin.id, update_records, token, _password_email_change_csv_import = true, source, allow_agent_downgrade)
          end
        end
      end

      describe 'with an invalid role' do
        it 'ignores the new value' do
          token = generate_token(admin, update_admin_data(agent, "Administrator"))
          perform(token)

          assert_equal "Agent", agent.reload.role
        end
      end
    end
  end

  describe "if the phone column is present" do
    describe 'when phone number validation account setting is off' do
      before do
        Account.any_instance.stubs(is_end_user_phone_number_validation_enabled?: false)
      end

      describe 'when creating user' do
        describe 'with a valid and unique phone number' do
          it 'it creates the identity' do
            token = generate_token(admin, new_agent_data("agent", "agent@example.com", "+15554398790"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 1, identities.count
            assert_equal "+15554398790", identities.first.value
          end
        end

        describe 'with a valid and but non unique phone number' do
          it 'it saves the attribute only' do
            UserPhoneNumberIdentity.create!(user: agent, value: "+15554398790")
            token = generate_token(admin, new_agent_data("agent", "agent2@example.com", "+15554398790"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 0, identities.count
            assert_equal "+15554398790", user.phone
          end
        end

        describe 'with invalid phone number' do
          it 'it saves the attribute only' do
            token = generate_token(admin, new_agent_data("invalid", "agent@example.com", "invalid_phone_number"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 0, identities.count
            assert_equal "invalid_phone_number", user.phone
          end
        end
      end

      describe 'when updating user' do
        describe 'with a valid and unique phone number' do
          it 'it updates the identity' do
            UserPhoneNumberIdentity.create!(user: agent, value: "+15554398790")
            token = generate_token(admin, new_agent_data("agent", "agent@example.com", "+15554398791"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 1, identities.count
            assert_equal "+15554398791", identities.first.value
          end
        end

        describe 'with a valid and but non unique phone number' do
          it 'it updates the attribute only' do
            UserPhoneNumberIdentity.create!(user: agent, value: "+15554398790")
            token = generate_token(admin, new_agent_data("agent", "agent@example.com", "+15554398790"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 0, identities.count
            assert_equal "+15554398790", user.phone
          end
        end

        describe 'with invalid phone number' do
          it 'it updates the attribute only' do
            token = generate_token(admin, new_agent_data("invalid", "agent@example.com", "invalid_phone_number"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 0, identities.count
            assert_equal "invalid_phone_number", user.phone
          end
        end
      end
    end

    describe 'when phone number validation account setting is on' do
      before do
        Account.any_instance.stubs(is_end_user_phone_number_validation_enabled?: true)
      end

      describe 'when creating user' do
        describe 'with a valid and unique phone number' do
          it 'it creates the identity' do
            token = generate_token(admin, new_agent_data("agent", "agent@example.com", "+15554398731"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 1, identities.count
            assert_equal "+15554398731", identities.first.value
          end
        end

        describe 'with a valid and but non unique phone number' do
          it 'it saves the attribute only' do
            UserPhoneNumberIdentity.create!(user: agent, value: "+15554398790")
            token = generate_token(admin, new_agent_data("agent", "agent2@example.com", "+15554398790"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 0, identities.count
            assert_equal "+15554398790", user.phone
          end
        end

        describe 'with invalid phone number' do
          it 'it saves the attribute only' do
            token = generate_token(admin, new_agent_data("invalid", "agent@example.com", "invalid_phone_number"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 0, identities.count
            assert_nil user.phone
          end
        end
      end

      describe 'when updating user' do
        describe 'with a valid and unique phone number' do
          it 'it updates the identity' do
            UserPhoneNumberIdentity.create!(user: agent, value: "+15554398790")
            token = generate_token(admin, new_agent_data("agent", "agent@example.com", "+15554398791"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 1, identities.count
            assert_equal "+15554398791", identities.first.value
          end
        end

        describe 'with a valid and but non unique phone number' do
          it 'it updates the attribute only' do
            UserPhoneNumberIdentity.create!(user: agent, value: "+15554398790")
            token = generate_token(admin, new_agent_data("agent", "agent@example.com", "+15554398790"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 0, identities.count
            assert_equal "+15554398790", user.phone
          end
        end

        describe 'with invalid phone number' do
          it 'it updates the attribute only' do
            token = generate_token(admin, new_agent_data("invalid", "agent@example.com", "invalid_phone_number"))
            perform(token)

            user       = User.last
            identities = user.identities.phone
            assert_equal 0, identities.count
            assert_nil user.phone
          end
        end
      end
    end
  end

  describe "with existent user and update_records disabled" do
    before do
      admin.account.settings.has_user_tags = true
      admin.account.settings.lookup(:has_user_tags).save!
      token = generate_token(admin, existent_user_data(update_user))
      UsersJob.perform(account_id, admin.id, dont_update_records, token)
    end

    should_not_change("the number of Users") { User.count(:all) }

    # One for the upload token and one to download the file
    should_change("the number of ExpirableAttachments", by: 1) do
      ExpirableAttachment.count(:all)
    end

    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 1) do
      ActionMailer::Base.deliveries.size
    end

    it "creates an attachment with the correct information" do
      attachment = ExpirableAttachment.last
      assert_equal 'text/csv', attachment.content_type
      assert_equal 'UsersJob', attachment.created_via
      result = File.open(attachment.public_filename).read
      assert result.include?('1 user unchanged')
      assert result.include?(update_user.email)
    end

    it "sends an email with the correct information" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert_equal(
        "Your user import is ready", ActionMailer::Base.deliveries.last.subject
      )
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        ExpirableAttachment.last.url(token: true)
      )
    end

    it "does not update the user" do
      assert_equal 'minimum_end_user@aghassipour.com', update_user.reload.email
      assert_not_equal 'John Doe', update_user.name
      assert_not_equal '+15554398790', update_user.phone
      assert_not_equal 'Some notes about John', update_user.notes
      assert_not_equal '11111', update_user.external_id
      assert_not_equal 'Some details about John', update_user.details
      assert_not_equal 'bar foo', update_user.current_tags
      assert_not_equal true, update_user.is_agent?
      assert_not_equal RoleRestrictionType.ASSIGNED, update_user.restriction_id
    end
  end

  describe "with invalid user" do
    before do
      token = generate_token(admin, failed_user_data)
      UsersJob.perform(account_id, admin.id, dont_update_records, token)
    end

    should_not_change("the number of Users") { User.count(:all) }

    # One for the upload token and one to download the file
    should_change("the number of ExpirableAttachments", by: 1) do
      ExpirableAttachment.count(:all)
    end

    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 1) do
      ActionMailer::Base.deliveries.size
    end

    it "creates an attachment with the correct information" do
      attachment = ExpirableAttachment.last
      assert_equal 'text/csv', attachment.content_type
      assert_equal 'UsersJob', attachment.created_via
      result = File.open(attachment.public_filename).read
      assert result.include?('1 user failed')
      assert result.include?('John Doe')
      assert result.include?('Error message')
    end

    it "sends an email with the correct information" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert_equal(
        "Your user import is ready", ActionMailer::Base.deliveries.last.subject
      )
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        ExpirableAttachment.last.url(token: true)
      )
    end
  end

  describe "with malformed csv" do
    let (:csv_data) { "\"name\",\"email,\"password\"\n" }
    let (:token) { generate_token(admin, csv_data) }

    before do
      UsersJob.perform(account_id, admin.id, dont_update_records, token)
    end

    # Only one for the upload token, the one for download should not be created
    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 1) do
      ActionMailer::Base.deliveries.size
    end

    should_not_change("the number of Users") { User.count(:all) }

    it "sends an email communicating the error" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert_equal "User import error", ActionMailer::Base.deliveries.last.subject
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        "Read more about the required format of the file"
      )
    end
  end

  describe 'with invalid byte sequence' do
    let (:token) { generate_token(admin, csv_data) }
    let(:csv_data) { multiple_phone_numbers_csv + "\xe2" }

    before do
      UsersJob.perform(account_id, admin.id, dont_update_records, token)
    end

    # Only one for the upload token, the one for download should not be created
    should_change('the number of Attachments', by: 1) do
      Attachment.count(:all)
    end

    should_change('the number of mail deliveries', by: 1) do
      ActionMailer::Base.deliveries.size
    end

    should_not_change('the number of Users') { User.count(:all) }

    it 'sends an email communicating the error' do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert_equal I18n.t('txt.email.import_job.utf_error.title', error_message: 'Unknown error: invalid byte sequence in UTF-8.'), ActionMailer::Base.deliveries.last.subject
      assert_includes ActionMailer::Base.deliveries.last.joined_bodies, 'UTF-8'
    end
  end

  describe "with invalid headers" do
    before do
      now = Time.now
      Timecop.freeze now
      token = generate_token(admin, invalid_headers_with_data)
      UsersJob.perform(account_id, admin.id, dont_update_records, token)
    end

    # Only one for the upload token, the one for download should not be created
    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 1) do
      ActionMailer::Base.deliveries.size
    end

    should_not_change("the number of Users") { User.count(:all) }

    it "sends an email communicating the error" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert_equal "User import error", ActionMailer::Base.deliveries.last.subject
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        "Missing mandatory field in header: email"
      )
    end
  end

  describe "with unknown error while building data" do
    before do
      UsersJob.expects(:build_data).raises(
        RuntimeError.new('Unknown error')
      )
      token = generate_token(admin, new_user_data)
      assert_raises(RuntimeError) do
        UsersJob.perform(account_id, admin.id, dont_update_records, token)
      end
    end

    # Only one for the upload token, the one for download should not be created
    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 1) do
      ActionMailer::Base.deliveries.size
    end

    should_not_change("the number of Users") { User.count(:all) }

    it "sends an email communicating the error" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert_equal "User import error", ActionMailer::Base.deliveries.last.subject
      assert_match(
        /For some reason the data to import was not ready in our servers/,
        ActionMailer::Base.deliveries.last.joined_bodies
      )
    end
  end

  describe "with unknown error while processing data" do
    before do
      UsersJob.expects(:ensure_record).raises(
        RuntimeError.new('Unknown error')
      )
      token = generate_token(admin, new_user_data)
      assert_raises(RuntimeError) do
        UsersJob.perform(account_id, admin.id, dont_update_records, token)
      end
    end

    # Only one for the upload token, the one for download should not be created
    should_change("the number of Attachments", by: 1) do
      Attachment.count(:all)
    end

    should_change("the number of mail deliveries", by: 1) do
      ActionMailer::Base.deliveries.size
    end

    should_not_change("the number of Users") { User.count(:all) }

    it "sends an email communicating the error" do
      assert_equal [admin.email], ActionMailer::Base.deliveries.last.to
      assert_equal "User import error", ActionMailer::Base.deliveries.last.subject
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(
        "Failed to process row"
      )
    end
  end

  describe "multiple organizations" do
    let(:organization2) { organizations(:minimum_organization2) }
    let(:organization3) { organizations(:minimum_organization3) }
    let(:headers)       { headers_array.dup }
    let(:new_user)      { new_user_array.dup }
    let(:existing_user) { existing_user_array(update_user) }
    let(:email_index)   { headers_array.index("email") }
    let(:organization_index) { headers_array.index("organization") }
    let(:organization_names) { [organization1, organization2].map(&:name).join("|") }

    let(:new_data) do
      CSV.generate(force_quotes: true) do |csv|
        csv << headers
        csv << new_user
      end
    end

    let(:update_data) do
      CSV.generate(force_quotes: true) do |csv|
        csv << headers
        csv << existing_user
      end
    end

    describe "are not enabled for the account" do
      before do
        Account.any_instance.stubs(has_multiple_organizations_enabled?: false)
      end

      describe "creating a new user" do
        let(:token) { generate_token(admin, new_data) }

        it "Splits the organization column into separate names on the pipe character (|)" do
          new_user[organization_index] = "One, name|Second, name|Third name"
          User.any_instance.
            expects(:organization_names=).
            with(["One, name", "Second, name", "Third name"])

          perform(token)
        end

        it "assigns the organization association to the user" do
          User.any_instance.
            expects(:organization_names=).
            with([organization1.name])

          perform(token)
        end

        it "fails if multiple organization names are provided" do
          new_user[organization_index] = organization_names
          assert_difference("User.count(:all)", 0) do
            perform(token)
          end

          attachment = ExpirableAttachment.last
          assert_equal 'text/csv', attachment.content_type
          assert_equal 'UsersJob', attachment.created_via
          result = File.open(attachment.public_filename).read
          assert result.include?("1 user failed")
          assert result.include?("Multiple organizations were provided. Only one or none is accepted.")
        end
      end

      describe "updating an existing user" do
        let(:token) { generate_token(admin, update_data) }

        it "updates the organization association if a single name is provided" do
          existing_user[organization_index] = organization2.name
          User.any_instance.
            expects(:organization_names=).
            with([organization2.name])

          perform(token)
        end

        it "fails if multiple organization names are provided" do
          assert_equal [organization1.id], update_user.organizations.map(&:id)

          existing_user[organization_index] = organization_names

          perform(token)

          assert_equal [organization1.id], update_user.organizations.map(&:id)

          attachment = ExpirableAttachment.last
          assert_equal 'text/csv', attachment.content_type
          assert_equal 'UsersJob', attachment.created_via
          result = File.open(attachment.public_filename).read
          assert result.include?("1 user failed")
          assert result.include?("Multiple organizations were provided. Only one or none is accepted.")
        end
      end
    end

    describe "are enabled for the account" do
      before do
        Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
        Account.any_instance.stubs(:admins_can_set_user_passwords).returns(true)
      end

      describe "creating a new user" do
        let(:token) { generate_token(admin, new_data) }

        it "assigns an organization association for a single name" do
          User.any_instance.
            expects(:organization_names=).
            with([organization1.name])

          perform(token)
        end

        it "recognizes 'organization' or 'organizations' header" do
          headers[organization_index] = "organizations"
          User.any_instance.
            expects(:organization_names=).
            with([organization1.name])

          perform(token)
        end

        it "assigns an organization for each name provided" do
          new_user[organization_index] = organization_names
          User.any_instance.
            expects(:organization_names=).
            with([organization1.name, organization2.name])

          perform(token)
        end

        it "fails if a non-existent organization name is provided" do
          new_user[organization_index] = ["Non Praesto", organization1.name].join("|")

          assert_difference("User.count(:all)", 0) do
            perform(token)
          end

          attachment = ExpirableAttachment.last
          assert_equal 'text/csv', attachment.content_type
          assert_equal 'UsersJob', attachment.created_via
          result = File.open(attachment.public_filename).read
          assert result.include?("1 user failed")
          assert result.include?("organization for name: 'Non Praesto' does not exist")
        end
      end

      describe "updating an existing user" do
        let(:token) { generate_token(admin, update_data) }

        before do
          update_user.identities.each { |identity| identity.update_attribute(:is_verified, false) }
          assert(!update_user.is_verified?)
        end

        it "does not create new users" do
          assert_difference("User.count(:all)", 0) do
            perform(token)
          end
        end

        it "unsets existing organizations if field is blank" do
          existing_user[organization_index] = ""
          User.any_instance.
            expects(:organization_names=).
            with([])

          perform(token)
        end

        it "adds or keep all existing organization names provided" do
          existing_user[organization_index] = organization_names
          User.any_instance.
            expects(:organization_names=).
            with([organization1.name, organization2.name])

          perform(token)
        end

        it "fails if a non-existent organization name is provided" do
          existing_user[organization_index] = ["Non Praesto", organization1.name].join("|")

          perform(token)

          attachment = ExpirableAttachment.last
          assert_equal 'text/csv', attachment.content_type
          assert_equal 'UsersJob', attachment.created_via
          result = File.open(attachment.public_filename).read
          assert result.include?("1 user failed")
          assert result.include?("organization for name: 'Non Praesto' does not exist")
        end
      end
    end
  end

  describe "brands" do
    let(:headers)       { headers_array.dup }
    let(:new_user)      { new_user_array.dup }
    let(:new_end_user)  { new_end_user_array.dup }
    let(:existing_user) { existing_user_array(update_user) }
    let(:brand_index)   { headers_array.index("brand") }
    let(:brand2)        { FactoryBot.create(:brand, account_id: account_id, subdomain: "purplewombat", name: "Purple Wombat Brand", active: false) }

    let(:new_data) do
      CSV.generate(force_quotes: true) do |csv|
        csv << headers
        csv << new_user
      end
    end

    let(:new_end_user_data) do
      CSV.generate(force_quotes: true) do |csv|
        csv << headers
        csv << new_end_user
      end
    end

    let(:update_data) do
      CSV.generate(force_quotes: true) do |csv|
        csv << headers
        csv << existing_user
      end
    end

    describe "creating a new user" do
      let(:token) { generate_token(admin, new_data) }

      it "does not assign brand_id if no brand is found for the given subdomain" do
        new_user[brand_index] = "giraffesubdomain"
        User.any_instance.expects(:active_brand_id=).never

        perform(token)
      end

      it "does not assign brand_id if the subdomain given belongs to an inactive brand" do
        new_user[brand_index] = brand2.subdomain
        User.any_instance.expects(:active_brand_id=).never

        perform(token)
      end

      it "does not assign the brand_id if the brand found does not have an enabled help center" do
        User.any_instance.expects(:active_brand_id=).never

        perform(token)
      end

      it "assigns the brand_id if the brand exists and has an enabled help center" do
        hc = HelpCenter.new(id: 42, state: 'enabled')
        HelpCenter.stubs(:find_by_brand_id).returns(hc)
        User.any_instance.expects(:active_brand_id=).with(brand1.id)

        perform(token)
      end

      it "sends a welcome mail to user using the requested brand" do
        Account.any_instance.stubs(:admins_can_set_user_passwords).returns(false)
        hc = HelpCenter.new(id: 42, state: 'enabled')
        HelpCenter.stubs(:find_by_brand_id).returns(hc)

        perform(token)
        mail = ActionMailer::Base.deliveries.first
        assert_equal "Welcome to Zendesk Support for Wombats Brand", mail.subject
        mail.joined_bodies.must_include "You have been invited to join Zendesk Support for Wombats Brand"
        mail.joined_bodies.must_include "//wombats.zendesk-test.com/verification/email/"
      end

      describe 'when importing end user with direct line' do
        before do
          Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(false)
        end

        let(:token) { generate_token(update_user, new_end_user_data) }
        let(:phone) { '(442) 123-4567' }

        it 'sends a welcome mail to user using the requested brand' do
          Account.any_instance.stubs(:help_center_enabled?).returns(true)
          hc = HelpCenter.new(id: 42, state: 'enabled')
          HelpCenter.stubs(:find_by_brand_id).returns(hc)
          perform(token)
          mail = ActionMailer::Base.deliveries.first
          assert_equal "Welcome to Wombats Brand", mail.subject
          mail.joined_bodies.must_include "Welcome to Wombats Brand"
          mail.joined_bodies.must_include "//wombats.zendesk-test.com/verification/email/"
        end
      end
    end

    describe "updating an existing user" do
      let(:token) { generate_token(admin, update_data) }

      before do
        update_user.identities.each { |identity| identity.update_attribute(:is_verified, false) }
        assert(!update_user.is_verified?)
      end

      it "does not create new users" do
        assert_difference("User.count(:all)", 0) do
          perform(token)
        end
      end

      it "does not assign brand_id if no brand is found for the given subdomain" do
        new_user[brand_index] = "giraffesubdomain"
        User.any_instance.expects(:active_brand_id=).never

        perform(token)
      end

      it "does not assign brand_id if the subdomain given belongs to an inactive brand" do
        new_user[brand_index] = brand2.subdomain
        User.any_instance.expects(:active_brand_id=).never

        perform(token)
      end

      it "does not assign the brand_id if the brand found does not have an enabled help center" do
        User.any_instance.expects(:active_brand_id=).never

        perform(token)
      end

      it "assigns the brand_id if the brand exists and has an enabled help center" do
        hc = HelpCenter.new(id: 42, state: 'enabled')
        HelpCenter.stubs(:find_by_brand_id).returns(hc)
        User.any_instance.expects(:active_brand_id=).with(brand1.id)

        perform(token)
      end
    end

    describe "in an account with HC, when user is deleted and still has an identity" do
      let(:inactive_user) { users(:inactive_user) }
      let(:inactive_user_data) do
        CSV.generate(force_quotes: true) do |csv|
          csv << headers
          csv << existing_user_array(inactive_user)
        end
      end
      let(:token) { generate_token(admin, inactive_user_data.gsub('Agent', '')) }

      before do
        Account.any_instance.stubs(:admins_can_set_user_passwords).returns(false)
        @account.enable_help_center!
      end

      it "reset its email identity sends a welcome mail" do
        perform(token)
        mail = ActionMailer::Base.deliveries.first
        assert_equal "Welcome to minimum", mail.subject
        mail.joined_bodies.must_include "Welcome to minimum"
      end
    end
  end

  describe "instrument" do
    let(:statsd_client) { stub_for_statsd }
    let(:source) { 'source' }
    let(:users_count) { CSV.parse(new_user_data, headers: true).count }

    before do
      UsersJob.stubs(:statsd_client).returns(statsd_client)
      statsd_client.expects(:increment).with("user_bulk_import", tags: ["source:#{source}", "users_count:#{users_count}"]).once
    end

    it 'reports metrics to datadog' do
      token = generate_token(admin, new_user_data)
      perform(token)
    end

    def perform(token)
      CIA.audit actor: admin do
        UsersJob.perform(account_id, admin.id, update_records, token, _password_email_change_csv_import = true, source)
      end
    end
  end

  def existing_user_array(user)
    new_user_array.tap { |array| array[1] = user.email }
  end

  def header_string(headers = headers_array)
    CSV.generate_line(headers, force_quotes: true)
  end

  def data_string(data = new_user_array)
    CSV.generate_line(data, force_quotes: true)
  end

  def invalid_headers_with_data
    header_string(
      headers_array.reject { |header| header == "email" }
    ) + data_string(
      new_user_array.reject  { |field| field == "john@company.com" }
    )
  end

  def unformatted_headers
    header_string(
      headers_array.tap do |headers|
        headers[0] = "name "
        headers[2] = "Password "
      end
    )
  end

  def new_user_data(headers = header_string)
    headers + data_string
  end

  def existent_user_data(user)
    header_string + data_string(existing_user_array(user))
  end

  def add_multiple_user_identities_data(user)
    header_string + data_string(existing_user_array(user)) + data_string(existing_user_array(user)).sub('John Doe', '').sub(user.email, "another_#{user.email.strip}")
  end

  def new_agent_data(role, email, phone)
    header_string + data_string(
      ["John Agent", email, "", phone, "", "", "", role, "", "", ""]
    )
  end

  def update_admin_data(admin, role)
    header_string + data_string(
      ["John Admin", admin.email, "", "", "", "", "", role, "", "", ""]
    )
  end

  def update_user_data(user)
    header_string(%w[name Email notes details tags]) + data_string(
      ["John Doe", user.email, "Some more notes about John", "", "wombat, fun"]
    )
  end

  def user_data_without_password(user)
    header_string(
      headers_array.reject { |header| header == "password" }
    ) + data_string(
      existing_user_array(user).reject { |field| field == "123456" }
    )
  end

  def failed_user_data
    header_string + data_string(
      ["John Doe", "", "123456", "", "Some notes about John", "11111",
       "Some details about John", "Agent", "foo", "Assigned", "foo, bar"]
    )
  end

  def enterprise_user_data
    header_string + data_string(
      ["Heavenly Agent", "j@company.com", "123456", "+15554398790", "Some notes about J",
       "11111", "Some details about Heavenly Agent", "Agent", "", "Light Agent", "foo"]
    ) + data_string(
      ["Mr. EndUser", "mruser@company.com", "123456", "+15557798390", "Some notes about Mr. User",
       "44444", "Some details about Mr. EndUser", "End-User", "", "", "foo"]
    ) + data_string(
      ["Custom Agent", "madlib@company.com", "123456", "+15554398390", "Some notes about Madlib",
       "22222", "Some details about Custom Agent", "Agent", "", "Pure Awesome", "foo"]
    ) + data_string(
      ["MF DOOM", "mfdoom@company.com", "123456", "+15554393790", "Some notes about MF",
       "33333", "Some details about MF", "Agent", "", "WOOOOOOOOOOO", "foo"]
    ) + data_string(
      ["Admin", "admin@company.com", "123456", "+15557798391", "Some notes about admin",
       "55555", "Some details about admin", "Admin", "", '', "foo"]
    ) + data_string(
      ["Agent", "agent@company.com", "123456", "+15557798391", "Some notes about agent",
       "66666", "Some details about agent", "Agent", "", '', "foo"]
    )
  end

  def generate_token(current_user, csv_data)
    account = current_user.account
    token = account.upload_tokens.create(source: current_user)
    attach = token.add_raw_attachment(csv_data, 'import')
    attach.save
    token.value
  end
end
