require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 3

describe GooddataIntegrationsJob do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:admin)   { users(:minimum_admin) }

  describe ".work" do
    let(:integration_provisioning) { stub('integration provisioning') }

    before do
      Zendesk::Gooddata::IntegrationProvisioning.stubs(:new).
        with(account, admin: admin).
        returns(integration_provisioning)

      integration_provisioning.stubs(:perform)

      GooddataIntegrationsJob.any_instance.stubs(:sleep).returns(true)

      Zendesk::StatsD::Client.any_instance.stubs(:increment)
      Zendesk::StatsD::Client.any_instance.stubs(:histogram)
    end

    describe "when the process goes smoothly" do
      before do
        GooddataIntegrationsJob.work(account.id, admin.id)
      end

      before_should "perform the integration provisioning process" do
        integration_provisioning.expects(:perform)
      end

      before_should "count the run" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).
          with('count')
      end

      before_should "record the running time" do
        Zendesk::StatsD::Client.any_instance.expects('histogram').
          with('execution_time', kind_of(Float))
      end
    end

    [Account, User].each do |model|
      describe "when the #{model} cannot be found" do
        before do
          model.stubs(:find).raises(ActiveRecord::RecordNotFound)

          GooddataIntegrationsJob.work(account.id, admin.id)
        end

        before_should "record the error" do
          ZendeskExceptions::Logger.expects(:record).with do |exception, params|
            assert_equal(ActiveRecord::RecordNotFound, exception.class)
            assert_equal(GooddataIntegrationsJob, params[:location])
            assert_includes(params[:message], "account_id: #{account.id}, user_id: #{admin.id} could not be found")
          end
        end
      end
    end

    describe "when the GoodData API receives too many requests" do
      before { GooddataIntegrationsJob.work(account.id, admin.id) }

      before_should "sleep for a period of time" do
        integration_provisioning.stubs(:perform).
          raises(GoodData::Error::TooManyRequests).
          then.returns(true)

        GooddataIntegrationsJob.any_instance.expects(:sleep).with(60)
      end

      before_should "count the run each time" do
        integration_provisioning.stubs(:perform).
          raises(GoodData::Error::TooManyRequests).
          then.returns(true)

        Zendesk::StatsD::Client.any_instance.expects(:increment).
          with('count').twice
      end

      before_should "not record the running time" do
        integration_provisioning.stubs(:perform).
          raises(GoodData::Error::TooManyRequests).
          then.returns(true)

        Zendesk::StatsD::Client.any_instance.expects('histogram').
          with('execution_time', kind_of(Float))
      end

      before_should "try again" do
        GooddataIntegrationsJob.any_instance.stubs(:sleep).returns(true)

        integration_provisioning.expects(:perform).twice.
          raises(GoodData::Error::TooManyRequests).
          then.returns(true)
      end
    end

    describe "when the GoodData API is temporarily unavailable" do
      before { GooddataIntegrationsJob.work(account.id, admin.id) }

      before_should "sleep for a period of time" do
        integration_provisioning.stubs(:perform).
          raises(GoodData::Error::ServiceUnavailable).
          then.returns(true)

        GooddataIntegrationsJob.any_instance.expects(:sleep)
      end

      before_should "count the run each time" do
        integration_provisioning.stubs(:perform).
          raises(GoodData::Error::ServiceUnavailable).
          then.returns(true)

        Zendesk::StatsD::Client.any_instance.expects(:increment).
          with('count').twice
      end

      before_should "not record the running time" do
        integration_provisioning.stubs(:perform).
          raises(GoodData::Error::ServiceUnavailable).
          then.returns(true)

        Zendesk::StatsD::Client.any_instance.expects('histogram').
          with('execution_time', kind_of(Float))
      end

      before_should "try again" do
        GooddataIntegrationsJob.any_instance.stubs(:sleep).returns(true)

        integration_provisioning.expects(:perform).twice.
          raises(GoodData::Error::ServiceUnavailable).
          then.returns(true)
      end
    end

    describe "when another GoodData API error occurs" do
      before do
        integration_provisioning.stubs(:perform).raises(GoodData::Error)

        GooddataIntegrationsJob.work(account.id, admin.id)
      end

      before_should "record the error" do
        ZendeskExceptions::Logger.expects(:record).with do |exception, params|
          assert_equal(GoodData::Error, exception.class)
          assert_equal(GooddataIntegrationsJob, params[:location])
          assert_includes(params[:message], "GoodData API error occurred")
        end
      end
    end

    describe "when an unknown error occurs" do
      before do
        integration_provisioning.stubs(:perform).raises(StandardError)

        GooddataIntegrationsJob.work(account.id, admin.id)
      end

      before_should "record the error" do
        ZendeskExceptions::Logger.expects(:record).with do |exception, params|
          assert_equal(StandardError, exception.class)
          assert_equal(GooddataIntegrationsJob, params[:location])
          assert_includes(params[:message], "Unknown error occurred")
        end
      end

      before_should "logs the failure to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed')
      end
    end
  end

  describe ".args_to_log" do
    let(:account_id) { 1 }
    let(:user_id)    { 2 }

    it "logs the account and user IDs" do
      assert_equal(
        {account_id: account_id, user_id: user_id},
        GooddataIntegrationsJob.args_to_log(account_id, user_id, 'foo', 'bar')
      )
    end
  end
end
