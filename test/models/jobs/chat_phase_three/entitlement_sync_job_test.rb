require_relative '../../../support/job_helper'
require_relative '../../../support/chat_phase_3_helper'
require_relative '../../../support/suite_test_helper'

SingleCov.covered!

describe ChatPhaseThree::EntitlementSyncJob do
  include ChatPhase3Helper
  include SuiteTestHelper

  fixtures :all

  let(:account) { accounts(:multiproduct) }
  let(:user) { users(:multiproduct_support_agent) }
  let(:is_chat_enabled) { true }
  let(:is_chat_admin) { false }
  let(:zopim_identity) do
    OpenStruct.new(
      user_id: user.id,
      is_administrator?: is_chat_admin,
      is_enabled?: is_chat_enabled
    )
  end
  let(:subject) { ChatPhaseThree::EntitlementSyncJob }

  before do
    create_phase_3_zopim_subscription(account)
    User.any_instance.stubs(:zopim_identity).returns(zopim_identity)
  end

  describe '#work' do
    describe 'when the staff service sync returns successfully' do
      describe 'for a chat agent' do
        let(:expected_chat_role) { Zendesk::Entitlement::ROLES[:chat][:agent] }

        it 'calls the staff service to update the user chat role to agent' do
          Zendesk::StaffClient.any_instance.expects(:update_entitlements!).
            with(user.id, { chat: expected_chat_role }, false).once
          subject.work(account.id, user.id)
        end
      end

      describe 'for end user' do
        let(:user) { users(:multiproduct_end_user) }

        it 'calls the staff service to update the user chat role to agent' do
          Zendesk::StaffClient.any_instance.expects(:update_entitlements!).
            with(user.id, { chat: nil }, true).once
          subject.work(account.id, user.id)
        end
      end

      describe 'for a chat admin' do
        let(:is_chat_admin) { true }
        let(:expected_chat_role) { Zendesk::Entitlement::ROLES[:chat][:admin] }

        it 'calls the staff service to update the user chat role to admin' do
          Zendesk::StaffClient.any_instance.expects(:update_entitlements!).
            with(user.id, { chat: expected_chat_role }, false).once
          subject.work(account.id, user.id)
        end
      end

      describe 'when the chat identity is disabled' do
        let(:is_chat_enabled) { false }

        it 'calls the staff service to update the user chat role to nil' do
          Zendesk::StaffClient.any_instance.expects(:update_entitlements!).
            with(user.id, { chat: nil}, false).once
          subject.work(account.id, user.id)
        end
      end

      describe 'when the chat identity is not found' do
        before do
          User.any_instance.stubs(:zopim_identity).raises(ActiveRecord::RecordNotFound.new)
        end

        it 'calls the staff service to update the user chat role to nil' do
          Zendesk::StaffClient.any_instance.expects(:update_entitlements!).
            with(user.id, { chat: nil}, false).once
          subject.work(account.id, user.id)
        end
      end
    end
  end

  describe '#fatal_exceptions' do
    it 'lists exceptions that should not be retried' do
      assert_equal subject.fatal_exceptions, ChatPhaseThree::EntitlementSyncJob::NON_RETRYABLE_ERRORS
    end
  end

  describe '#try_again_callback' do
    it 'increments a metric on retry' do
      subject.statsd_client.expects(:increment).with(:retries).twice
      subject.try_again(StandardError.new, account.id, user.id, {})
      subject.try_again(StandardError.new, account.id, user.id, {})
    end
  end

  describe '#give_up_callback' do
    it 'increments a metric on giving up' do
      subject.statsd_client.expects(:increment).with(:failures).once
      subject.give_up(StandardError.new, account.id, user.id)
    end
  end
end
