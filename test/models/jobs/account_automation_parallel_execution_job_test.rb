require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe AccountAutomationParallelExecutionJob do
  fixtures :rules, :tickets

  let(:automation) { rules(:automation_close_ticket) }
  let(:ticket)     { tickets(:minimum_4) }
  let(:dehydrated_tickets) { [ticket] }
  let(:audit_id) { 1 }

  before do
    Arturo.enable_feature!(:parallel_automations_background_heartbeat)

    Resque::Durable::BackgroundHeartbeat.any_instance.stubs(:heartbeat!)
  end

  describe "#perform" do
    it "catches and logs exceptions" do
      failure_message = "Failed to execute automation #{automation.id}, account #{ticket.account.subdomain}, dehydrated_tickets: #{dehydrated_tickets}"

      Zendesk::Rules::DehydratedTicket.stubs(:new).raises(StandardError, "InvalidTicket")
      AccountAutomationParallelExecutionJob.expects(:resque_log).with("#{failure_message}: InvalidTicket")

      AccountAutomationParallelExecutionJob.perform(ticket.account_id, automation.id, dehydrated_tickets, audit_id)
    end
  end
end
