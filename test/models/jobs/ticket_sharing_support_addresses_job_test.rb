require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 2

describe TicketSharingSupportAddressesJob do
  fixtures :accounts

  describe ".work" do
    let(:account) { accounts(:minimum) }

    it "refreshes the ticket sharing partner support addresses for the account" do
      Account.any_instance.expects(:refresh_ticket_sharing_partner_support_addresses)
      TicketSharingSupportAddressesJob.work(account.id)
    end
  end

  describe ".args_to_log" do
    let(:account_id) { 1 }

    it "logs the account ID" do
      assert_equal(
        {
          account_id: account_id
        },
        TicketSharingSupportAddressesJob.args_to_log(account_id)
      )
    end
  end
end
