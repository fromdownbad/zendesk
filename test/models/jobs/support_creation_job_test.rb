require_relative "../../support/job_helper"

SingleCov.covered!

describe SupportCreationJob do
  fixtures :accounts

  describe '#work' do
    let(:account) { accounts(:shell_account_without_support) }

    it 'activates support trial' do
      Zendesk::TrialActivation.expects(:activate_support_trial!).with(instance_of(Accounts::ShellAccountActivation), instance_of(User))
      SupportCreationJob.perform_now(account_id: account.id, user_id: account.owner_id)
    end

    describe 'when retry limit is reached and job gave up' do
      let(:statsd_client) { stub_for_statsd }
      before do
        SupportCreationJob.stubs(:statsd_client).returns(statsd_client)
      end

      it 'increments a metric on giving up' do
        statsd_client.expects(:increment).with(:permanent_failure, tags: ["account:#{account.id}"]).once
        SupportCreationJob.give_up(Kragle::ResponseError.new, account_id: account.id, user_id: account.owner_id)
      end
    end
  end
end
