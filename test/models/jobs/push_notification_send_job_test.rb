require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe PushNotificationSendJob do
  fixtures :accounts, :tickets, :users, :devices, :mobile_apps, :device_identifiers

  describe "#work" do
    let(:account) { accounts(:minimum) }
    let(:device) { device_identifiers(:first_party) }
    let(:unregistered_device) { device_identifiers(:unregistered) }
    let(:user) { users(:minimum_agent) }
    let(:ticket) { tickets(:minimum_1) }
    let(:args) do
      {
        account_id: account.id,
        device_ids: [device.id],
        mobile_app_identifier: 'com.zendesk.agent',
        payload: {user_id: user.id, msg: 'foo', ticket_id: ticket.id}
      }
    end

    it "does not send a notification for com.zendesk.agent" do
      refute PushNotificationSendJob.enqueue(args)
    end

    it "send a notification for any other app except com.zendesk.agent" do
      config = Zendesk::Configuration.dig!(:urban_airship, :'com.zendesk.inbox')
      PushNotifications::UrbanAirshipClient.expects(:new).with(config['app_key'], config['master_secret']).returns(
        mock(push: [mock(success?: true)])
      )

      args[:mobile_app_identifier] = 'com.zendesk.inbox'
      assert PushNotificationSendJob.enqueue(args)
    end

    it "sends using Google Cloud Messaging when needed" do
      PushNotifications::GCMClient.expects(:new).with('api_key' => 'this-is-the-inbox-api-server-key').returns(
        mock(push: [mock(success?: true)])
      )

      params = {
        account_id: account.id,
        device_ids: [device.id],
        mobile_app_identifier: 'com.zendesk.inbox',
        gateway: 'gcm',
        payload: {user_id: user.id, msg: 'foo', ticket_id: ticket.id}
      }

      PushNotificationSendJob.work(params)
    end

    it "does not send a notification using Amazon SNS if `token_type` is `urban_airship_channel_id`" do
      device.update_attribute(:device_type, 'iphone')
      device.update_attribute(:token_type, 'urban_airship_channel_id')

      Zendesk::PushNotifications::Payloads::APNS.expects(:new).with(
        alert: 'foo',
        badge: 1,
        extra_payload: {ticket_id: ticket.id, user_id: user.id, enable_collapse_key: false}
      ).never

      Zendesk::PushNotifications::Adapters::SNS.expects(:new).never

      Arturo.enable_feature!(:sns_push_notifications)
      PushNotificationSendJob.work(args)
    end

    it "sends a notification using Amazon SNS to registered device" do
      device.update_attribute(:device_type, 'iphone')

      Zendesk::PushNotifications::Payloads::APNS.expects(:new).with(
        alert: 'foo',
        badge: 1,
        extra_payload: {ticket_id: ticket.id, user_id: user.id, enable_collapse_key: true}
      ).returns(
        mock(payload: [stub])
      )

      Zendesk::PushNotifications::Adapters::SNS.expects(:new).returns(
        mock(register: [stub], push: [stub])
      )

      Arturo.enable_feature!(:sns_push_notifications)
      PushNotificationSendJob.work(args)
    end

    it "registers and sends a notification using Amazon SNS" do
      params = {
        account_id: account.id,
        device_ids: [unregistered_device.id],
        mobile_app_identifier: 'com.zendesk.agent',
        payload: {user_id: user.id, msg: 'foo', ticket_id: ticket.id}
      }

      Zendesk::PushNotifications::Payloads::APNS.expects(:new).with(
        alert: 'foo',
        badge: 1,
        extra_payload: {ticket_id: ticket.id, user_id: user.id, enable_collapse_key: true}
      ).returns(
        mock(payload: [stub])
      )

      Zendesk::PushNotifications::Adapters::SNS.expects(:new).returns(
        mock(register: [stub], push: [stub])
      )

      Arturo.enable_feature!(:sns_push_notifications)
      PushNotificationSendJob.work(params)
    end

    it "send a notification with collapse_key enabled" do
      config = Zendesk::Configuration.dig!(:urban_airship, :"com.zendesk.agent")
      PushNotifications::UrbanAirshipClient.expects(:new).with(config['app_key'], config['master_secret']).returns(
        mock(push: [mock(success?: true)])
      )
      PushNotificationSendJob.work(args)
    end

    it "notifies statsd if sending fails" do
      config = Zendesk::Configuration.dig!(:urban_airship, :"com.zendesk.agent")
      PushNotifications::UrbanAirshipClient.expects(:new).with(config['app_key'], config['master_secret']).returns(
        mock(push: [mock(success?: false)])
      )
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('device_push.failed', tags: ['urban_airship'])
      PushNotificationSendJob.work(args)
    end
  end
end
