require_relative '../../support/job_helper'

SingleCov.covered!

describe SurveyPersistenceJob do
  let(:mock_survey_type) { 'foo_survey_type' }
  let(:mock_redis_key) { 'mock_redis_key' }
  let(:mock_redis_payload) do
    {
      type: mock_survey_type,
      question_names: %w[
        someQuestion
        aQuestionThatHappensToHaveNoResponse
        someMoreQuestion
      ],
      answer: {
        "someQuestion" => ["single choice"],
        "someMoreQuestion" => ["mult 0", "mult 1"],
      },
      user_email: 'submitter@example.com',
      owner_email: 'owner@example.com',
      account_id: 'account-id',
      subdomain: 'foosubdomain'
    }
  end
  let(:keys_always_present_in_eloqua_payload) do
    %w[elqformName elqSiteID Form_Name Responder_Email_Address accountID Subdomain].map &:to_sym
  end
  let (:expected_eloqua_payload_excerpt) do
    {
      "Question_01" => "someQuestion",
      "Answer_01" => "single choice",
      "Question_02" => "aQuestionThatHappensToHaveNoResponse",
      "Answer_02" => "",
      "Question_03" => "someMoreQuestion",
      "Answer_03" => "mult 0,mult 1",
    }
  end

  describe '.build_eloqua_payload' do
    def assert_equal_excerpt(expected, output)
      assert_equal expected, output.except(*keys_always_present_in_eloqua_payload)
    end

    describe 'when no strings need truncating' do
      before do
        @result = SurveyPersistenceJob.send(:build_eloqua_payload, mock_redis_payload)
      end

      it 'output contains the always-present keys' do
        keys_always_present_in_eloqua_payload.each do |key|
          assert_includes(@result, key)
        end
      end

      it 'output contains fields in survey config, including -Comment fields, in order, and no extra fields' do
        assert_equal_excerpt expected_eloqua_payload_excerpt, @result
      end
    end

    it 'truncates question string' do
      question_name = 'x' * (SurveyPersistenceJob::QUESTION_CHAR_LIMIT + 1)

      input = mock_redis_payload.tap do |p|
        p[:question_names].push(question_name)
      end
      expected = expected_eloqua_payload_excerpt.merge(
        'Question_04' => question_name.truncate(SurveyPersistenceJob::QUESTION_CHAR_LIMIT),
        'Answer_04' => ''
      )
      assert_equal_excerpt expected, SurveyPersistenceJob.send(:build_eloqua_payload, input)
    end

    it 'truncates answer string' do
      single_answer = 'asdf'
      answer = [single_answer] * (SurveyPersistenceJob::ANSWER_CHAR_LIMIT.to_f / single_answer.length + 1).ceil
      answer_str = answer.join(',')
      assert answer_str.length > SurveyPersistenceJob::ANSWER_CHAR_LIMIT

      input = mock_redis_payload.tap do |p|
        p[:answer].merge!(
          'someMoreQuestion' => answer
        )
      end
      expected = expected_eloqua_payload_excerpt.merge(
        'Answer_03' => answer_str.truncate(SurveyPersistenceJob::ANSWER_CHAR_LIMIT)
      )
      assert_equal_excerpt expected, SurveyPersistenceJob.send(:build_eloqua_payload, input)
    end

    it 'adds redeem_training_offer in the question_09 fields if needed' do
      input = mock_redis_payload.merge(training_offer_save: true)

      expected = expected_eloqua_payload_excerpt.merge(
        'Question_09' => 'Training_Offer_Save',
        'Answer_09' => 'true'
      )
      assert_equal_excerpt expected, SurveyPersistenceJob.send(:build_eloqua_payload, input)
    end

    it 'adds cancelled in the question_09 fields if needed' do
      input = mock_redis_payload.merge(canceled: false)

      expected = expected_eloqua_payload_excerpt.merge(
        'Question_10' => 'Canceled',
        'Answer_10' => 'false'
      )
      assert_equal_excerpt expected, SurveyPersistenceJob.send(:build_eloqua_payload, input)
    end
  end

  describe '.perform' do
    before do
      Zendesk::RedisStore.client.stubs(:get).with(mock_redis_key).
        returns(mock_redis_payload.to_json)
      @req_stub = stub_request(:post, SurveyPersistenceJob::ELOQUA_FORM_URI)
    end

    it 'is successful if calling eloqua returns an "empty" body' do
      @req_stub.to_return(status: 200, body: "\r\n")
      SurveyPersistenceJob.perform(redis_key: mock_redis_key)
      assert_requested(@req_stub)
    end

    it 'is successful if calling eloqua returns a confirmation page' do
      @req_stub.to_return(status: 200, body: "<script type='text/javascript'>document.location.href = '#{SurveyPersistenceJob::ELOQUA_FORM_CONFIRMATION_URI}';</script>")
      SurveyPersistenceJob.perform(redis_key: mock_redis_key)
      assert_requested(@req_stub)
    end

    it 'raises an exception if calling to eloqua was unsuccessful' do
      @req_stub.to_return(status: 200, body: '<html><p>some error details here...</p></html>')
      assert_raises RuntimeError do
        SurveyPersistenceJob.perform(redis_key: mock_redis_key)
      end
    end

    it 'does not call eloqua if payload in redis is not a valid json' do
      Zendesk::RedisStore.client.stubs(:get).with(mock_redis_key).
        returns(nil)
      assert_not_requested @req_stub
      SurveyPersistenceJob.perform(redis_key: mock_redis_key)
    end
  end

  describe '.args_to_log' do
    it 'returns a non-nil value' do
      assert_not_nil SurveyPersistenceJob.args_to_log(redis_key: 'foo')
    end
  end
end
