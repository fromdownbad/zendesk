require_relative '../../support/job_helper'
require_relative '../../support/test_helper'
require_relative '../../support/rule'

SingleCov.covered! uncovered: 1

describe 'FraudScoreJob' do
  include TestSupport::Rule::Helper

  fixtures :accounts

  let(:account) { accounts(:trial) }
  let(:auto_suspend) { false }
  let(:takeover) { false }
  let(:risky) { false }
  let(:support_risky) { false }
  let(:talk_risky) { false }
  let(:whitelist) { false }
  let(:account_update) { false }
  let(:source_event) { Fraud::SourceEvent::TRIAL_SIGNUP }
  let(:support_risky_status) { false }
  let(:pravda_response_body) { { account: { is_abusive: true } } }
  let(:afs_request_body) do
    {
      source_event: source_event,
      data: {
        account_id: account.id,
        subdomain: account.subdomain,
        owner_email: account.owner.email,
        time_zone: account.time_zone,
        account_name: account.name,
        owner_name: account.owner.name,
        raw_response: nil,
        source_event: source_event,
        created_at: account.created_at,
        zd_pod: ENV['ZENDESK_POD'],
        is_trial: account.account_is_trial?,
        created_from_ip_address: account.settings.created_from_ip_address,
        account_update: account_update,
        support_risky_status: support_risky_status,
        trial_extras: account.trial_extras.to_hash,
        minfraud_id: ''
      },
      timestamp: Time.now,
      vendor_review_prohibited: false
    }
  end

  let(:afs_response_body) do
    {
      account_fraud_service_release: "1.0.0",
      score: 0,
      actions: {
        auto_suspend: auto_suspend,
        takeover: takeover,
        risky: risky,
        support_risky: support_risky,
        talk_risky: talk_risky,
        whitelist: whitelist,
      }
    }
  end

  let(:afs_signal_response_body) do
    {
      account_fraud_service_release: "1.0.0",
      score: 0,
      actions: {
        auto_suspend: auto_suspend,
        takeover: takeover,
        risky: risky,
        support_risky: support_risky,
        talk_risky: talk_risky,
        whitelist: whitelist,
      }
    }
  end

  let(:afs_url) { "http://pod-1.account-fraud-service.service.consul:5000/api/services/private/activity" }

  before { Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns(false) }

  describe 'when the FraudScoreJob is enqueued' do
    before do
      Timecop.freeze
      stub_request(:post, afs_url).with(
        body: afs_request_body.to_json
      ).to_return(
        status: 200,
        body: afs_response_body.to_json,
        headers: {
          "Content-Type" => "application/json"
        }
      )
    end

    let(:last_fraud_score) { account.fraud_scores.last }

    def self.it_creates_fraud_scores
      it 'creates a fraud score with a trial payload' do
        assert_equal 1, account.fraud_scores.count
      end
    end

    def self.it_sets_service_response_in_the_fraud_score
      it 'sets service_response in the fraud score' do
        assert_kind_of Hash, last_fraud_score.actions
        assert_equal auto_suspend, last_fraud_score.actions[:auto_suspend]
      end
    end

    describe 'and the auto_suspend_via_afs arturo is disabled' do
      before do
        Arturo.disable_feature! :auto_suspend_via_afs
      end

      describe 'and the response has auto_suspend = false' do
        let(:auto_suspend) { false }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'keeps the account in a serviceable state' do
          assert account.reload.is_serviceable?
          refute account.abusive?
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end

      describe 'and the response has auto_suspend = true' do
        let(:auto_suspend) { true }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'keeps the account in a serviceable state' do
          assert account.reload.is_serviceable?
          refute account.reload.abusive?
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end
    end

    describe 'and the auto_suspend_via_afs arturo is enabled' do
      before do
        Arturo.enable_feature! :auto_suspend_via_afs
      end

      describe 'and the response has auto_suspend = true' do
        let(:auto_suspend) { true }

        before do
          stub_abusive_response(body: pravda_response_body)
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'suspends the account' do
          refute account.reload.is_serviceable?
          assert account.abusive?
          assert_equal account.settings.risk_assessment, "level" => "suspended", "product" => "support", "reason" => "auto-suspend"
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end

      describe 'and the response has auto_suspend = false' do
        let(:auto_suspend) { false }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'keeps the account in a serviceable state' do
          assert account.reload.is_serviceable?
          refute account.abusive?
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end
    end

    describe 'and the takeover_via_afs arturo is disabled' do
      before do
        Arturo.disable_feature! :takeover_via_afs
      end

      describe 'and the response has takeover = true' do
        let(:takeover) { true }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'does not take over the account' do
          assert_equal account.owner.email, account.reload.owner.email
          refute account.abusive?
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end

      describe 'and the response has takeover = false' do
        let(:takeover) { false }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'does not takes over the account' do
          assert_equal account.owner.email, account.reload.owner.email
          refute account.abusive?
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end
    end

    describe_with_arturo_enabled :support_risky_via_afs do
      describe 'and the response has support_risky = true' do
        let(:support_risky) { true }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'flags account as support risky' do
          account.settings.reload
          assert_equal account.settings.risk_assessment, "level" => "risky", "product" => "support", "reason" => "support risky"
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end

      describe 'and the response has support_risky = false' do
        let(:support_risky) { false }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'does not flag account as support risky' do
          account.settings.reload
          refute_equal account.settings.risk_assessment, "level" => "risky", "product" => "support", "reason" => "support risky"
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end
    end

    describe_with_arturo_enabled :whitelist_via_afs do
      before do
        account.settings.whitelisted_from_fraud_restrictions = false
        account.settings.save!
      end
      describe 'and the response has whitelist = true' do
        let(:whitelist) { true }
        let(:pravda_response_body) { { account: { is_abusive: false } } }

        before do
          stub_abusive_response(body: pravda_response_body)
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'whitelists the account' do
          account.settings.reload
          assert account.settings.whitelisted_from_fraud_restrictions
          refute account.abusive?
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end

      describe 'and the response has whitelist = false' do
        let(:whitelist) { false }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'does not whitelist the account' do
          account.settings.reload
          refute account.settings.whitelisted_from_fraud_restrictions
          refute account.abusive?
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end
    end

    describe_with_arturo_enabled :talk_risky_via_afs do
      describe 'and the response has talk_risky = true' do
        let(:talk_risky) { true }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'flags account as talk risky' do
          account.settings.reload
          assert_equal account.settings.risk_assessment, "level" => "risky", "product" => "talk", "reason" => "talk risky"
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end

      describe 'and the response has talk_risky = false' do
        let(:talk_risky) { false }

        before do
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end

        it 'does not flag account as talk risky' do
          account.settings.reload
          refute_equal account.settings.risk_assessment, "level" => "risky", "product" => "talk", "reason" => "talk risky"
        end

        it_creates_fraud_scores
        it_sets_service_response_in_the_fraud_score
      end
    end

    describe 'when the Account_fraud_service Arturo is enabled' do
      before do
        FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
      end
      it_creates_fraud_scores
      it_sets_service_response_in_the_fraud_score
    end

    describe_with_arturo_disabled :account_fraud_service_enabled do
      describe 'when the Account_fraud_service Arturo is disabled' do
        it 'does not allow fraud score to be created' do
          FraudScoreJob.expects(:create_fraud_score).never
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
          assert_equal account.fraud_scores.length, 0
        end
      end
    end

    describe 'logging trial_enqueue_timestamp' do
      before { Zendesk::StatsD::Client.any_instance.stubs(:histogram) }
      describe 'when params contains trial_enqueue_timestamp' do
        it 'tracks the enqueue time delta' do
          Zendesk::StatsD::Client.any_instance.expects(:histogram).with('trial_enqueue_delta', kind_of(Float), tags: ['source_event:trial_signup']).once
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP, nil, nil, 'trial_enqueue_timestamp' => Time.now.utc.to_s)
        end
      end

      describe 'when params does not contain trial_enqueue_timestamp' do
        it 'does not track the enqueue time delta' do
          Zendesk::StatsD::Client.any_instance.expects(:histogram).never
          FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        end
      end
    end
  end # End FSJ enqueued block

  describe 'when account_fraud_service response is empty' do
    before do
      Timecop.freeze
      stub_request(:post, afs_url).with(
        body: afs_request_body.to_json
      ).to_return(
        status: 200,
        body: "",
        headers: {
          "Content-Type" => "application/json"
        }
      )
      @job = FraudScoreJob.new(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
    end

    it 'return without calling create_fraud_score' do
      FraudScoreJob.any_instance.expects(:create_fraud_score).never
      @job.work
    end
  end

  describe "#work" do
    describe "when an exception occurs" do
      before do
        Timecop.freeze
        @job = FraudScoreJob.new(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
        @exception = StandardError.new("KABOOM")
        @job.stubs(:generate_fraud_score).raises(@exception)
      end

      it "logs the exception to the Rails logs" do
        Rails.logger.expects(:error).with { |m| m.starts_with?("FRAUD_SCORE_JOB_EXCEPTION: #{@exception.class.name}") }
        proc { @job.work }.must_raise(StandardError)
      end

      it "propagates the exception to Resque" do
        e = proc { @job.work }.must_raise(StandardError)
        e.must_equal(@exception)
        assert_equal e.message, 'KABOOM'
      end
    end
  end

  describe '#account_payload' do
    fixtures :account_settings

    let(:job) do
      FraudScoreJob.new(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
    end
    let(:request_payload) { job.send(:account_payload) }

    describe "when created_from_ip_address setting is set" do
      it "contains created_from_ip_address setting" do
        account.settings.created_from_ip_address = '1.1.1.1'
        account.save!
        assert_equal account.settings.created_from_ip_address, request_payload[:data][:created_from_ip_address]
      end
    end
  end

  describe 'calls #enqueue_account with correct parameters' do
    it 'enqueues the correct account values' do
      FraudScoreJob.expects(:enqueue).with(account.id, account.name, account.subdomain, account.owner.email,
        account.owner.name, account.time_zone, source_event, nil, nil, nil)
      FraudScoreJob.enqueue_account(account, source_event, nil, nil)
    end
  end

  describe 'calls #enqueue_account_in with correct parameters' do
    it 'enqueues the correct account values' do
      FraudScoreJob.expects(:enqueue_in).with(60.seconds, account.id, account.name, account.subdomain, account.owner.email,
        account.owner.name, account.time_zone, source_event, nil, nil, nil)
      FraudScoreJob.enqueue_account_in(account, source_event, 60.seconds, nil, nil)
    end
  end

  describe 'calls #account_creation_enqueue' do
    before do
      Arturo.enable_feature!(:fraud_score_job_maxmind)
      Timecop.freeze
    end

    it 'with a TRIAL_SIGNUP source_event' do
      FraudScoreJob.expects(:enqueue).with(account.id, account.name, account.subdomain, account.owner.email,
        account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP, '', 'null', trial_enqueue_timestamp: Time.now.utc)
      FraudScoreJob.account_creation_enqueue(account, Fraud::SourceEvent::TRIAL_SIGNUP)
    end

    it 'with a SHELL_CREATED source_event' do
      FraudScoreJob.expects(:enqueue_in).with(60.seconds, account.id, account.name, account.subdomain, account.owner.email,
        account.owner.name, account.time_zone, Fraud::SourceEvent::SHELL_CREATED, nil, nil, nil)
      FraudScoreJob.account_creation_enqueue(account, Fraud::SourceEvent::SHELL_CREATED, 60.seconds)
    end
  end

  describe 'when the account_fraud_service_enabled arturo is disabled' do
    let(:source_event) { Fraud::SourceEvent::TRIAL_SIGNUP }
    before do
      Timecop.freeze
      stub_request(:post, afs_url).with(
        body: afs_request_body.to_json
      ).to_return(
        status: 200,
        body: afs_response_body.to_json,
        headers: {
          "Content-Type" => "application/json"
        }
      )
      Arturo.disable_feature! :account_fraud_service_enabled
      FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
    end

    it 'does not create a fraud score' do
      FraudScoreJob.enqueue_account(account, Fraud::SourceEvent::TRIAL_SIGNUP)
      assert_equal 0, account.reload.fraud_scores.count
    end
  end

  describe 'guard against paid account actions' do
    let(:auto_suspend) { true }

    before do
      Arturo.enable_feature! :auto_suspend_via_afs
      Timecop.freeze
    end

    describe 'account is not a trialer' do
      before do
        Account.any_instance.stubs(:is_trial?).returns(false)
        Timecop.freeze
        stub_request(:post, afs_url).with(
          body: afs_request_body.to_json
        ).to_return(
          status: 200,
          body: afs_response_body.to_json,
          headers: {
            "Content-Type" => "application/json"
          }
        )

        FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
      end

      it 'does not take action' do
        assert account.reload.is_serviceable?
        refute account.abusive?
      end
    end

    describe 'account is a trialer' do
      let(:pravda_response_body) { { account: { is_abusive: false } } }

      before do
        stub_abusive_response(body: pravda_response_body)
        Account.any_instance.stubs(:is_trial?).returns(true)
        Timecop.freeze
        stub_request(:post, afs_url).with(
          body: afs_request_body.to_json
        ).to_return(
          status: 200,
          body: afs_response_body.to_json,
          headers: {
            "Content-Type" => "application/json"
          }
        )

        FraudScoreJob.work(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
      end

      it 'does take action' do
        refute account.reload.is_serviceable?
        assert account.abusive?
      end
    end
  end

  describe 'when no params are passed into FSJ' do
    it 'params instance variable is set to an empty hash' do
      fsj = FraudScoreJob.new(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, Fraud::SourceEvent::TRIAL_SIGNUP)
      assert_equal fsj.instance_variable_get(:@params), {}
    end
  end

  def stub_abusive_response(body: {}, status: 200)
    stub_request(:patch, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}").
      to_return(
        status:  status,
        body:    body.to_json,
        headers: { content_type: 'application/json; charset=utf-8' }
      )
  end
end
