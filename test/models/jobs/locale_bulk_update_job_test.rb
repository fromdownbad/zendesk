require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe "LocaleBulkUpdateJob" do
  fixtures :accounts, :users, :tickets, :translation_locales

  let(:english) { translation_locales(:english_by_zendesk) }
  let(:portuguese) { translation_locales(:brazilian_portuguese) }
  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }

  describe ".work" do
    before do
      user.locale_id = english.id
      user.save!

      @ticket1 = tickets(:minimum_1)
      @ticket1.requester = user
      @ticket1.will_be_saved_by(user)
      @ticket1.save!

      @ticket2 = tickets(:minimum_2)
      @ticket2.requester = user
      @ticket2.will_be_saved_by(user)
      @ticket2.save!

      user.tickets.not_closed.update_all(locale_id: portuguese.id)
    end

    it "does not raise when it can't find account" do
      Rails.logger.expects(:error).times(4)

      # Invalid account id should not raise
      LocaleBulkUpdateJob.work(nil, user.id)
      LocaleBulkUpdateJob.work('thisisnotavalidIDever', user.id)

      # Invalid user id should not raise
      LocaleBulkUpdateJob.work(account.id, nil)
      LocaleBulkUpdateJob.work(account.id, 'thisisNotAValidUserID')
    end

    it 'skips if not active / serviceable' do
      account.update_attribute(:is_serviceable, false)

      LocaleBulkUpdateJob.expects(:update_ticket_locales).never
      LocaleBulkUpdateJob.work(account.id, user.id)

      account.update_attribute(:is_serviceable, true)
    end

    it "updates the locales of all tickets provided" do
      LocaleBulkUpdateJob.work(account.id, user.id)
      assert user.tickets.not_closed.pluck(:locale_id).all? { |locale_id| locale_id == user.locale_id }
    end

    describe "updates the locales of all tickets in batches" do
      before { Users::Localization::MAX_UPDATE_ALL_TICKETS = 1 }
      after  { Users::Localization::MAX_UPDATE_ALL_TICKETS = 500 }

      it "calls tickets.update_all for each ticket" do
        assert(user.tickets.not_closed.length >= 1)
        ActiveRecord::Relation.any_instance.expects(:update_all).times(user.tickets.not_closed.length)
        LocaleBulkUpdateJob.work(account.id, user.id)
      end

      it "updates all of the tickets" do
        LocaleBulkUpdateJob.work(account.id, user.id)

        assert_empty user.tickets.not_closed.where(locale_id: portuguese.id)
      end
    end

    describe "short_circuit_locale_bulk_update_job enabled" do
      before do
        Arturo.enable_feature!(:short_circuit_locale_bulk_update_job)
      end

      it "short circuits .work and returns early" do
        account.expects(:on_shard).never

        assert_nil LocaleBulkUpdateJob.work(account.id, user.id)
      end
    end
  end
end
