require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 5

describe 'SolveIncidentsJob' do
  fixtures :accounts, :subscriptions, :users, :tickets, :groups, :memberships, :addresses, :account_property_sets, :ticket_fields, :attachments, :facebook_pages

  before do
    @account = accounts(:minimum)

    @ticket                = tickets(:minimum_3)
    @ticket.assignee       = users(:minimum_agent)
    @ticket.status_id      = StatusType.OPEN
    @ticket.ticket_type_id = TicketType.PROBLEM
    @ticket.account.stubs(:has_extended_ticket_types?).returns(true)
    @ticket.will_be_saved_by(@ticket.assignee)
    @ticket.save!
    @ticket.reload
    @ticket.status_id = StatusType.SOLVED

    @user     = @ticket.assignee
    @incident = tickets(:minimum_4)

    @incident.linked_id = @ticket.id
    @incident.status_id = StatusType.OPEN
    @incident.will_be_saved_by(@incident.assignee)
    @incident.save!
  end

  describe "#find_problem" do
    it "returns a problem from the database when passed an integer" do
      tickets = stub
      tickets.expects(:find_by_id).with(19)
      account = stub(tickets: tickets)

      SolveIncidentsJob.find_problem(account, problem: 19)
    end

    it "returns the instance object when passed a Ticket instance" do
      assert_equal @ticket, SolveIncidentsJob.find_problem(nil, problem: @ticket)
    end
  end

  describe "#find_user" do
    it "returns the system user when passed user_id -1" do
      assert_equal User.system, SolveIncidentsJob.find_user(nil, -1)
    end

    it "returns the an account user when passed a non -1 id" do
      assert_equal @user, SolveIncidentsJob.find_user(@account, @user.id)
    end
  end

  describe "solving a ticket with more incidents than limit" do
    describe "when the current user is not the system user" do
      it "enqueues a job" do
        @ticket.expects(:incident_count_mandates_background_processing?).returns(true)
        SolveIncidentsJob.expects(:enqueue).with(@account.id, @user.id, problem: @ticket.id)
        @ticket.will_be_saved_by(@user)
        @ticket.save!
      end
    end

    describe "when the current user is the system user" do
      before do
        @user = User.system
      end

      it "executes work directly" do
        SolveIncidentsJob.expects(:enqueue).never
        SolveIncidentsJob.expects(:work).once
        @ticket.will_be_saved_by(@user)
        @ticket.save!
      end
    end
  end

  describe "#solve_linked_incidents" do
    it "iterates all working incidents and mark them as solved" do
      SolveIncidentsJob.solve_linked_incidents(@account, @user, @ticket, { })
      @ticket.incidents.each do |incident|
        assert incident.solved?
        assert_equal incident.audits.last.via_id, ViaType.LINKED_PROBLEM
        assert_equal incident.audits.last.via_reference_id, @ticket.id
      end
    end

    describe "when there's required-for-agents custom fields" do
      before do
        field = ticket_fields(:field_text_custom)
        field.is_required = true
        field.save!
      end

      it "allows linked tickets to be solved even if they're blank" do
        SolveIncidentsJob.solve_linked_incidents(@account, @user, @ticket, { })
        @ticket.incidents.each do |incident|
          assert incident.solved?
        end
      end
    end
  end

  describe "solving the ticket with a comment" do
    before do
      @opts = { value: "{{ticket.id}} foobar", is_public: true, format: "rich" }
      @ticket.add_comment(body: "{{ticket.id}} foobar", is_public: true, account: @account, ticket: @ticket, format: "rich")
      Attachment.any_instance.stubs(current_data: 101010101010)
    end

    it "enqueues the job with a prerendered comment" do
      SolveIncidentsJob.expects(:enqueue).with(@account.id, @user.id, { problem: @ticket.id }.merge(@opts))
      @ticket.expects(:incident_count_mandates_background_processing?).returns(true)
      @ticket.will_be_saved_by(@user)
      @ticket.save!
      assert_equal @ticket.comments.last.body, "#{@ticket.nice_id} foobar"
    end

    it "adds a clone of the comments to the solved incidents" do
      stub_request(:put, %r{\.amazonaws.com/data/attachments/.*})
      @opts[:attachment_ids] = [attachments(:comment_attachment).id]
      @ticket.comment.attachments << attachments(:comment_attachment)
      @ticket.will_be_saved_by(@user)
      @ticket.save!
      results = SolveIncidentsJob.work(@account.id, @user.id, { problem: @ticket }.merge(@opts))

      @ticket.incidents.each do |incident|
        assert_equal "#{incident.nice_id} foobar", incident.comments.last.body
        assert_equal @user, incident.comments.last.author
        assert(incident.comments.last.is_public)
        assert_equal [attachments(:comment_attachment).filename], incident.comments.last.attachments.map(&:filename)
      end

      results.each do |result|
        assert_equal "Solved", result[:status]
        assert result[:success]
      end
    end

    it "always receives attachment ids" do
      @opts[:attachment_ids] = [attachments(:serialization_comment_attachment).id]

      SolveIncidentsJob.expects(:enqueue).with(@account.id, @user.id, { problem: @ticket.id }.merge(@opts))
      @ticket.expects(:incident_count_mandates_background_processing?).returns(true)

      @ticket.comment.attachments << attachments(:serialization_comment_attachment)
      @ticket.will_be_saved_by(@user)
      @ticket.save!
    end
  end

  describe "solving the ticket with a rich text comment" do
    before do
      @opts = { value: "foobar", is_public: true, format: "rich" }
      @ticket.add_comment(body: "foobar", is_public: true, format: "rich", account: @account, ticket: @ticket)
    end

    it "uses text format from the comment made on the problem ticket (provided in options)" do
      @ticket.will_be_saved_by(@user)
      @ticket.save!
      SolveIncidentsJob.work(@account.id, @user.id, { problem: @ticket }.merge(@opts))

      @ticket.incidents.each do |incident|
        assert_equal "rich", incident.comments.last.format
      end
    end
  end
end
