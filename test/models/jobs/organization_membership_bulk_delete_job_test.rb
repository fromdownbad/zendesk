require_relative "../../support/job_helper"

SingleCov.covered!

describe OrganizationMembershipBulkDeleteJob do
  fixtures :accounts, :organization_memberships

  let(:account) { accounts(:minimum) }
  let(:organization_membership1) { organization_memberships(:minimum) }
  let(:organization_membership2) { organization_memberships(:minimum2) }
  let(:job) do
    OrganizationMembershipBulkDeleteJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      ids: [organization_membership1.id, organization_membership2.id])
  end

  describe "#perform" do
    describe "as an agent" do
      describe "with valid ids" do
        before do
          job.perform
        end

        it "deletes organization memberships" do
          assert_nil account.organization_memberships.find_by_id(organization_membership1.id)
          assert_nil account.organization_memberships.find_by_id(organization_membership2.id)
        end
      end

      describe "with invalid ids" do
        before do
          job.options[:ids] = [1, 2]
          job.perform
        end

        it "returns error" do
          assert_includes job.status["results"], "id" => 1, "error" => "OrganizationMembershipNotFound"
          assert_includes job.status["results"], "id" => 2, "error" => "OrganizationMembershipNotFound"
        end
      end
    end
  end
end
