require_relative "../../support/job_helper"

SingleCov.covered!

describe FetchGoogleProfileImageJob do
  fixtures :accounts, :users
  include PerformJob

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_end_user)
    @url     = "http://hello.com/me.png"
  end

  describe "#perform" do
    before do
      stub_request(:get, @url).to_return(body: FixtureHelper::Files.read(file), headers: { content_type: "image/png" })
    end

    describe 'with a non-default file' do
      let(:file) { 'small.png' }

      it "sets the remote image as photo on the user" do
        assert @user.photo.blank?
        perform_job(FetchGoogleProfileImageJob, @account.id, @user.id, @url)
        assert @user.reload.photo.present?
      end
    end

    describe 'with a default file' do
      let(:file) { 'google_default.jpg' }

      it "does not set the remote image as photo on the user" do
        assert @user.photo.blank?
        perform_job(FetchGoogleProfileImageJob, @account.id, @user.id, @url)
        assert @user.reload.photo.blank?
      end
    end
  end
end
