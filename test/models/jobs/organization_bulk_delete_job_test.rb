require_relative "../../support/job_helper"
require_relative "../../support/test_helper"

SingleCov.covered!

describe OrganizationBulkDeleteJob do
  fixtures :accounts, :organizations

  let(:account) { accounts(:minimum) }
  let(:organization1) { organizations(:minimum_organization1) }
  let(:organization2) { organizations(:minimum_organization2) }
  let(:job) do
    OrganizationBulkDeleteJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      ids: [organization1.id, organization2.id],
      key: :id)
  end

  describe "#perform" do
    describe "as an admin" do
      describe "with valid ids" do
        before do
          job.perform
        end

        it "deletes organizations" do
          assert_nil account.organizations.find_by_id(organization1.id)
          assert_nil account.organizations.find_by_id(organization2.id)
        end
      end

      describe "with valid external_ids" do
        before do
          @external_ids = ['external_1', 'external_2']
          @external_ids.each do |external_id|
            FactoryBot.create(:organization, account: account, external_id: external_id, name: external_id)
          end
          job = OrganizationBulkDeleteJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
            account_id: account.id,
            user_id: account.owner_id,
            ids: @external_ids,
            key: :external_id)
          job.perform
        end

        it "deletes organizations" do
          assert_nil account.organizations.find_by_external_id(@external_ids[0])
          assert_nil account.organizations.find_by_external_id(@external_ids[1])
        end
      end

      describe "with invalid inputs" do
        before do
          job = OrganizationBulkDeleteJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
            account_id: account.id,
            user_id: account.owner_id,
            ids: [1, 2],
            key: :id)
          job.perform
        end

        it "returns error" do
          assert_includes job.status["results"], "id" => 1, "error" => "OrganizationNotFound"
          assert_includes job.status["results"], "id" => 2, "error" => "OrganizationNotFound"
        end
      end
    end
  end
end
