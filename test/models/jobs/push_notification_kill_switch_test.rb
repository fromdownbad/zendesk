require_relative "../../support/job_helper"

SingleCov.covered!

class PushNotificationKillSwitchTester
  extend PushNotificationKillSwitch
end

describe PushNotificationKillSwitch do
  fixtures :accounts, :tickets, :users, :devices, :mobile_apps

  let(:account) { accounts(:minimum) }
  let(:device) { devices(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:ticket) { tickets(:minimum_1) }
  let(:args) do
    {
      account_id: account.id,
      device_ids: [device.id],
      mobile_app_identifier: 'com.zendesk.agent',
      payload: {user_id: user.id, msg: 'foo', ticket_id: ticket.id}
    }
  end

  describe ".before_enqueue" do
    it "returns false when identifier com.zendesk.agent" do
      refute PushNotificationKillSwitchTester.before_enqueue(args, nil)
    end
  end
end
