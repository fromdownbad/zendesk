require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe BaseEntitlementSyncJob do
    describe '#work' do
      let(:job) { BaseEntitlementSyncJob.new(account_id: 123, user_id: 456) }
      let(:account) { Account.new }

      before do
        job.stubs(:account).returns(account)
        account.stubs(:products).returns([])
      end

      it 'raises an error' do
        assert_raises('Undefined Product') { job.work }
      end
    end
  end
end
