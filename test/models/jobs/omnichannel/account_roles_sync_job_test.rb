require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe AccountRolesSyncJob do
    describe '#work' do
      let(:account) { accounts(:minimum) }
      let(:role_sync_enabled) { true }
      let(:job) { AccountRolesSyncJob.new(account_id: account.id) }
      let(:role) { stub }

      before do
        PermissionSet.create_light_agent!(account)
        PermissionSet.create_chat_agent!(account)
        Zendesk::SupportAccounts::RoleFactory.stubs(:retrieve).returns(role)
        Zendesk::SupportAccounts::Account.any_instance.stubs(:role_sync_enabled?).returns(role_sync_enabled)
      end

      describe 'when role sync is enabled for account' do
        let(:role_sync_enabled) { true }

        it 'syncs each role for the account' do
          # Plus the two times for agent and admin roles
          role.expects(:sync!).times(account.permission_sets.count + 2)
          job.work
        end
      end

      describe 'when role sync is not enabled for account' do
        let(:role_sync_enabled) { false }

        it 'does nothing' do
          role.expects(:sync!).never
          job.work
        end
      end
    end
  end
end
