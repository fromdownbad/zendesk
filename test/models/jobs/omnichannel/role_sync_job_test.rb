require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe RoleSyncJob do
    describe '#work' do
      let(:account) { accounts(:minimum) }
      let(:permission_set) { PermissionSet.create(account: account, name: 'New Role', description: 'role description') }
      let(:role_name) { 'custom_role_name' }
      let(:permissions_changed) { ['forum_access', 'available_user_lists'] }
      let(:context) { 'test' }
      let(:role) { stub }

      let(:job) do
        RoleSyncJob.new(
          account_id: account.id, permission_set_id: permission_set.id, role_name: role_name, context: context, permissions_changed: permissions_changed
        )
      end

      before do
        Zendesk::SupportAccounts::RoleFactory.stubs(:retrieve).returns(role)
        Zendesk::SupportAccounts::Account.any_instance.stubs(:role_sync_enabled?).returns(role_sync_enabled)
      end

      describe 'when role sync is enabled for account' do
        let(:role_sync_enabled) { true }

        it 'sends a request to Pravda' do
          role.expects(:sync!).with(context: context, permissions_changed: permissions_changed, enabled: nil)
          job.work
        end

        describe 'enabled specified' do
          let(:job) do
            RoleSyncJob.new(
              account_id: account.id, permission_set_id: permission_set.id, role_name: role_name, context: context, permissions_changed: permissions_changed, enabled: true
            )
          end

          it 'sends a request to Pravda' do
            role.expects(:sync!).with(context: context, permissions_changed: permissions_changed, enabled: true)
            job.work
          end
        end
      end

      describe 'when role sync is not enabled for account' do
        let(:role_sync_enabled) { false }

        it 'does nothing' do
          role.expects(:sync!).never
          job.work
        end
      end
    end
  end
end
