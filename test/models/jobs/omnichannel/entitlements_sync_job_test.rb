require_relative '../../../support/job_helper'

SingleCov.covered!

module Omnichannel
  describe EntitlementsSyncJob do
    describe '#work' do
      let(:job) do
        EntitlementsSyncJob.new(
          account_id: account.id,
          user_id: user.id,
          products: products_to_sync
        )
      end
      let(:account) { accounts(:multiproduct) }
      let(:products) do
        [
          stub(name: :support),
          stub(name: :guide),
          stub(name: :explore),
          stub(name: :talk)
        ]
      end
      let(:user) { users(:multiproduct_support_agent) }
      let(:products_to_sync) { ['support', 'guide', 'explore', 'talk'] }

      let(:staff_client) { stub }
      let(:statsd_client) { stub_for_statsd }

      before do
        job.stubs(:account).returns(account)
        account.stubs(:products).returns(products)
        job.stubs(:staff_client).returns(staff_client)
        staff_client.stubs(:update_full_entitlements!)
        EntitlementsSyncJob.stubs(:statsd_client).returns(statsd_client)
      end

      describe 'when products to sync is empty' do
        let(:products_to_sync) { [] }

        it 'does nothing' do
          staff_client.expects(:update_full_entitlements!).never
          job.work
        end
      end

      describe 'when products is not empty' do
        let(:payload) do
          {
            'support' => {
              'name' => 'agent',
              'is_active' => true
            },
            'guide' => {
              'name' => 'agent',
              'is_active' => true
            },
            'explore' => {
              'name' => 'viewer',
              'is_active' => true
            },
            'talk' => {
              'name' => 'agent',
              'is_active' => false
            }
          }
        end

        it 'sends a request to staff service with right entitlements' do
          staff_client.expects(:update_full_entitlements!).with(user.id, payload, is_end_user: false)
          job.work
        end

        describe 'when user is end user' do
          let(:user) { users(:minimum_end_user2) }
          let(:existing_entitlements) do
            {
              'explore' => 'agent',
              'support' => 'agent',
              'guide' => 'agent',
              'talk' => 'agent'
            }
          end

          let(:payload) do
            {
              'support' => nil,
              'guide' => nil,
              'explore' => nil,
              'talk' => nil
            }
          end

          before do
            staff_client.stubs(:get_entitlements!).returns(existing_entitlements)
          end

          it 'sends a request to staff service to delete existing entitlements' do
            staff_client.expects(:update_full_entitlements!).with(user.id, payload, is_end_user: true)
            job.work
          end
        end

        describe 'when user does not have valid role for a product' do
          let(:products_to_sync) { ['support', 'explore'] }
          let(:support_entitlement) do
            stub(
              no_available_role?: false,
              payload: { 'name' => 'agent', 'is_active' => true }
            )
          end
          let(:explore_entitlement) do
            stub(
              no_available_role?: true,
              payload: { 'is_active' => false }
            )
          end
          let(:existing_entitlements) { { 'explore' => 'agent' } }
          let(:payload) do
            {
              'support' => {
                'name' => 'agent',
                'is_active' => true
              },
              'explore' => {
                'is_active' => false
              }
            }
          end

          before do
            staff_client.stubs(:get_entitlements!).returns(existing_entitlements)
            Zendesk::SupportUsers::Entitlement.stubs(:for).with(user, 'support').returns(support_entitlement)
            Zendesk::SupportUsers::Entitlement.stubs(:for).with(user, 'explore').returns(explore_entitlement)
          end

          it 'sends a request to staff service to disable current entitlement' do
            staff_client.expects(:update_full_entitlements!).with(user.id, payload, is_end_user: false)
            job.work
          end

          describe 'and there is no existing entitlement' do
            let(:existing_entitlements) { { 'explore' => nil } }
            let(:payload) do
              {
                'support' => {
                  'name' => 'agent',
                  'is_active' => true
                }
              }
            end

            it 'ignores the product' do
              staff_client.expects(:update_full_entitlements!).with(user.id, payload, is_end_user: false)
              job.work
            end
          end
        end

        describe 'when user does not have valid role in any product' do
          let(:existing_entitlements) { {} }
          before do
            user.roles = Role::END_USER.id
            staff_client.stubs(:get_entitlements!).returns(existing_entitlements)
          end

          it 'does nothing' do
            staff_client.expects(:update_full_entitlements!).never
            job.work
          end
        end

        describe 'when user is deleted' do
          before do
            staff_client.stubs(:update_full_entitlements!).raises(Kragle::Gone.new)
          end

          it 'increments a metric on skip' do
            statsd_client.expects(:increment).with(:user_deleted).once
            job.work
          end
        end

        describe 'when a product is not active' do
          let(:payload) do
            {
              'support' => {
                'name' => 'agent',
                'is_active' => true
              },
              'guide' => {
                'name' => 'agent',
                'is_active' => true
              },
              'explore' => {
                'name' => 'viewer',
                'is_active' => true
              }
            }
          end

          let(:products) do
            [
              stub(name: :support),
              stub(name: :guide),
              stub(name: :explore)
            ]
          end

          it 'sends a metric to dd about inactive product' do
            statsd_client.expects(:increment).with(:product_inactive).once
            job.work
          end

          it 'sends a request to staff service with right entitlements' do
            staff_client.expects(:update_full_entitlements!).with(user.id, payload, is_end_user: false)
            job.work
          end
        end

        describe 'when retry limit is reached and job gave up' do
          it 'increments a metric on giving up' do
            statsd_client.expects(:increment).with(:permanent_failure).once
            EntitlementsSyncJob.give_up(Kragle::ResponseError.new, account_id: account.id, user_id: user.id)
          end
        end
      end
    end
  end
end
