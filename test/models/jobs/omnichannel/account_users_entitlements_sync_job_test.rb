require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe AccountUsersEntitlementsSyncJob do
    let(:account) { accounts(:multiproduct) }
    let(:job) { AccountUsersEntitlementsSyncJob.new(account_id: account.id) }

    describe '#work' do
      before do
        Zendesk::SupportAccounts::Account.any_instance.stubs(:entitlements_sync_enabled?).returns(entitlements_sync_enabled)
      end

      describe 'when entitlements sync is enabled' do
        let(:entitlements_sync_enabled) { true }
        let(:entitlements_sync_enabled) { true }

        describe 'for shell account' do
          it 'sends a request to Metropolis for each user' do
            Zendesk::SupportUsers::EntitlementSynchronizer.expects(:perform).times(account.users.count)
            job.work
          end
        end

        describe 'for non-shell account' do
          let(:account) { accounts(:minimum) }

          it 'sends a request to Metropolis for each agent' do
            Zendesk::SupportUsers::EntitlementSynchronizer.expects(:perform).times(account.agents.count)
            job.work
          end
        end

        describe 'errors' do
          before do
            Zendesk::SupportUsers::EntitlementSynchronizer.stubs(:perform).raises(Kragle::UnprocessableEntity.new(error))
          end

          describe 'product is not in account service' do
            let(:error) { 'Entitlements Product Missing Error' }

            it 'raises not ready error' do
              assert_raises Zendesk::ProductNotReady do
                job.work
              end
            end
          end

          describe 'product is not ready in account service' do
            let(:error) { 'Entitlements Product Not Enabled Error' }

            it 'raises not ready error' do
              assert_raises Zendesk::ProductNotReady do
                job.work
              end
            end
          end

          describe 'other error' do
            let(:error) { 'Invalid params' }

            it 'bubbles up the error' do
              assert_raises Kragle::UnprocessableEntity do
                job.work
              end
            end
          end
        end

        describe 'when retry limit is reached and job gave up' do
          let(:statsd_client) { stub_for_statsd }

          before do
            AccountUsersEntitlementsSyncJob.stubs(:statsd_client).returns(statsd_client)
          end

          it 'increments a metric on giving up' do
            statsd_client.expects(:increment).with(:permanent_failure).once
            AccountUsersEntitlementsSyncJob.give_up(Kragle::ResponseError.new, account_id: account.id)
          end
        end
      end

      describe 'when entitlements sync is not enabled for account' do
        let(:entitlements_sync_enabled) { false }

        it 'does nothing' do
          Zendesk::SupportUsers::EntitlementSynchronizer.expects(:perform).never
          job.work
        end
      end
    end
  end
end
