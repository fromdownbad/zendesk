require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe AccountProductPlanSettingsSyncJob do
    let(:job) { Omnichannel::AccountProductPlanSettingsSyncJob.new(account_id: account.id) }
    let(:account) { accounts(:minimum) }
    let(:account_client) { stub }
    let(:plan_settings) do
      { boost_expires_at: Time.now, light_agents: false }
    end

    before do
      Zendesk::Accounts::Client.stubs(:new).returns(account_client)
      Zendesk::SupportAccounts::Product.any_instance.stubs(:plan_settings).returns(plan_settings)
    end

    describe '#work' do
      it 'syncs product to account service' do
        account_client.expects(:update_product!).with('support', { product: { plan_settings: plan_settings } }, context: 'account_product_plan_settings_sync_job')
        job.work
      end

      describe 'no support product' do
        before do
          Zendesk::SupportAccounts::Product.stubs(:retrieve).returns(nil)
        end

        it 'does not sync product to account service' do
          account_client.expects(:update_product!).never
          job.work
        end
      end
    end
  end
end
