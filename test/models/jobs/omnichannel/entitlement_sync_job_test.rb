require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe EntitlementSyncJob do
    describe '#work' do
      let(:job) do
        EntitlementSyncJob.new(
          account_id: account.id,
          user_id: user.id
        )
      end
      let(:account) { accounts(:multiproduct) }
      let(:user) { users(:multiproduct_support_agent) }

      before do
        Zendesk::SupportAccounts::Account.any_instance.stubs(:entitlements_sync_enabled?).returns(entitlements_sync_enabled)
      end

      describe 'when entitlements sync is enabled for account' do
        let(:entitlements_sync_enabled) { true }

        it 'sends a request to Metropolis' do
          Zendesk::SupportUsers::EntitlementSynchronizer.expects(:perform).with(user)
          job.work
        end

        describe 'when retry limit is reached and job gave up' do
          let(:statsd_client) { stub_for_statsd }
          before do
            EntitlementSyncJob.stubs(:statsd_client).returns(statsd_client)
          end

          it 'increments a metric on giving up' do
            statsd_client.expects(:increment).with(:permanent_failure).once
            EntitlementSyncJob.give_up(Kragle::ResponseError.new, account_id: account.id, user_id: user.id)
          end
        end

        describe 'errors' do
          before do
            Zendesk::SupportUsers::EntitlementSynchronizer.stubs(:perform).raises(Kragle::UnprocessableEntity.new(error))
          end

          describe 'product is not in account service' do
            let(:error) { 'Entitlements Product Missing Error' }

            it 'raises not ready error' do
              assert_raises Zendesk::ProductNotReady do
                job.work
              end
            end
          end

          describe 'product is not ready in account service' do
            let(:error) { 'Entitlements Product Not Enabled Error' }

            it 'raises not ready error' do
              assert_raises Zendesk::ProductNotReady do
                job.work
              end
            end
          end

          describe 'other error' do
            let(:error) { 'Invalid params' }

            it 'bubbles up the error' do
              assert_raises Kragle::UnprocessableEntity do
                job.work
              end
            end
          end
        end
      end

      describe 'when entitlements sync is not enabled for account' do
        let(:entitlements_sync_enabled) { false }

        it 'does nothing' do
          Zendesk::SupportUsers::EntitlementSynchronizer.expects(:perform).never
          job.work
        end
      end
    end
  end
end
