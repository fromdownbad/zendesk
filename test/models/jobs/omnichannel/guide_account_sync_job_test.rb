require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe GuideAccountSyncJob do
    describe '#work' do
      let(:job) { GuideAccountSyncJob.new(account_id: account_id) }
      let(:account_id) { 123 }
      let(:account) { Account.new }
      let(:user1) { User.new }
      let(:user2) { User.new }
      let(:sync_job1) { stub }
      let(:sync_job2) { stub }
      let(:products) { [Zendesk::Accounts::Product.new(name: Zendesk::Accounts::Client::GUIDE_PRODUCT, state: Zendesk::Accounts::Product::SUBSCRIBED)] }

      before do
        job.stubs(:account).returns(account)
        account.stubs(:agents).returns([user1, user2])
        account.stubs(:products).with(use_cache: false).returns(products)
        user1.stubs(:id).returns(1)
        user2.stubs(:id).returns(2)
      end

      describe_with_arturo_enabled :ocp_guide_account_sync do
        it 'syncs guide entitlements for account agents' do
          Omnichannel::GuideEntitlementSyncJob.expects(:new).with(account_id: 123, user_id: 1).returns(sync_job1)
          Omnichannel::GuideEntitlementSyncJob.expects(:new).with(account_id: 123, user_id: 2).returns(sync_job2)
          sync_job1.expects(:work)
          sync_job2.expects(:work)
          job.work
        end

        describe 'without guide product' do
          let(:products) { [] }

          it 'does not sync anything' do
            Omnichannel::GuideEntitlementSyncJob.expects(:new).never
            job.work
          end
        end
      end

      describe_with_arturo_disabled :ocp_guide_account_sync do
        it 'does not sync anything' do
          Omnichannel::GuideEntitlementSyncJob.expects(:new).never
          job.work
        end
      end
    end
  end
end
