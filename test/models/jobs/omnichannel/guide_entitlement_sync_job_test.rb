require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe GuideEntitlementSyncJob do
    describe '#work' do
      let(:job) do
        GuideEntitlementSyncJob.new(
          account_id: account_id,
          user_id: user_id
        )
      end
      let(:account) { accounts(:minimum) }
      let(:account_id) { 39834980934 }
      let(:user) { users(:minimum_agent) }
      let(:user_id) { 89348723243 }
      let(:payload) { { 'guide' => entitlement } }
      let(:role) { 'agent' }
      let(:entitlement) { { 'name' => role, 'is_active' => true } }
      let(:statsd_client) { stub_for_statsd }
      let(:products) { [Zendesk::Accounts::Product.new(name: Zendesk::Accounts::Client::GUIDE_PRODUCT, state: Zendesk::Accounts::Product::SUBSCRIBED)] }

      before do
        job.stubs(:account).returns(account)
        account.stubs(:id).returns(account_id)
        account.stubs(:products).returns(products)
        user.stubs(:id).returns(user_id)
        User.stubs(:find).returns(user)
        user.roles = Role::AGENT.id
        user.is_moderator = false
        GuideEntitlementSyncJob.stubs(:statsd_client).returns(statsd_client)
      end

      it 'sends a request to Metropolis' do
        Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).with(user_id, payload, is_end_user: false)
        job.work
      end

      describe 'user is deleted' do
        before do
          Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).raises(Kragle::Gone.new)
        end

        it 'increments a metric on skip' do
          statsd_client.expects(:increment).with(:user_deleted).once
          job.work
        end
      end

      describe 'Guide is inactive' do
        let(:products) { [] }

        it 'increments a metric on skip' do
          statsd_client.expects(:increment).with(:product_inactive).once
          job.work
        end
      end

      describe 'when retry limit is reached and job gave up' do
        it 'increments a metric on giving up' do
          statsd_client.expects(:increment).with(:permanent_failure).once
          GuideEntitlementSyncJob.give_up(Kragle::ResponseError.new, account_id: account_id, user_id: user_id)
        end
      end
    end
  end
end
