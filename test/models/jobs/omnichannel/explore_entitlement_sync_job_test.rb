require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe ExploreEntitlementSyncJob do
    describe '#work' do
      let(:job) do
        ExploreEntitlementSyncJob.new(
          account_id: account_id,
          user_id: user_id
        )
      end
      let(:account) { accounts(:multiproduct) }
      let(:account_id) { 39834980123 }
      let(:user) { users(:multiproduct_support_agent) }
      let(:user_id) { 89348723896 }
      let(:role) { 'viewer' }
      let(:statsd_client) { stub_for_statsd }
      let(:products) { [Zendesk::Accounts::Product.new(name: Zendesk::Accounts::Client::EXPLORE_PRODUCT, state: Zendesk::Accounts::Product::SUBSCRIBED)] }

      before do
        job.stubs(:account).returns(account)
        account.stubs(:id).returns(account_id)
        account.stubs(:products).returns(products)
        user.stubs(:id).returns(user_id)
        User.stubs(:find).returns(user)
        user.roles = Role::AGENT.id
        ExploreEntitlementSyncJob.stubs(:statsd_client).returns(statsd_client)
      end

      describe 'when agent has an active explore entitlement' do
        let(:payload) { { 'explore' => { 'name' => role, 'is_active' => true } } }
        it 'sends a request to Metropolis to set active entitlement' do
          Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).with(user_id, payload, is_end_user: false)
          job.work
        end
      end

      describe 'when user does not have an explore entitlement' do
        let(:role) { nil }
        let(:payload) { { 'explore' => nil } }
        let(:existing_entitlement) { 'agent' }

        before do
          user.roles = Role::END_USER.id
          Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).returns('explore' => existing_entitlement)
        end

        it 'sends a request to Metropolis to disable current entitlement' do
          Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).with(user_id, payload, is_end_user: true)
          job.work
        end

        describe 'no existing entitlement' do
          let(:existing_entitlement) { nil }

          it 'ignores the user' do
            Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).never
            job.work
          end
        end
      end

      describe 'user is deleted' do
        before do
          Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).raises(Kragle::Gone.new)
        end

        it 'increments a metric on skip' do
          statsd_client.expects(:increment).with(:user_deleted).once
          job.work
        end
      end

      describe 'Explore is inactive' do
        let(:products) { [] }

        it 'increments a metric on skip' do
          statsd_client.expects(:increment).with(:product_inactive).once
          job.work
        end
      end

      describe 'when retry limit is reached and job gave up' do
        it 'increments a metric on giving up' do
          statsd_client.expects(:increment).with(:permanent_failure).once
          ExploreEntitlementSyncJob.give_up(Kragle::ResponseError.new, account_id: account_id, user_id: user_id)
        end
      end
    end
  end
end
