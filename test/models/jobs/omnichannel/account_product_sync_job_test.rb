require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe AccountProductSyncJob do
    let(:job) { Omnichannel::AccountProductSyncJob.new(account_id: account.id) }
    let(:account) { accounts(:minimum) }
    let(:account_client) { stub }
    let(:product_params) { { states: 'subscribed' } }
    let(:statsd_client) { stub_for_statsd }

    before do
      Zendesk::Accounts::Client.stubs(:new).returns(account_client)
      Zendesk::SupportAccounts::Product.any_instance.stubs(:to_h).returns(product_params)
      job.stubs(:statsd_client).returns(statsd_client)
    end

    describe '#work' do
      it 'syncs product to account service' do
        account_client.expects(:update_product!).with('support', { product: product_params }, context: 'account_product_sync_job', include_deleted_account: true)
        job.work
      end

      describe 'no support product' do
        before do
          Zendesk::SupportAccounts::Product.stubs(:retrieve).returns(nil)
        end

        it 'does not sync product to account service' do
          account_client.expects(:update_product!).never
          job.work
        end
      end

      describe 'errors' do
        before do
          account_client.
            stubs(:update_product!).
            raises(Kragle::ResourceNotFound.new(error))
        end

        describe 'product is not ready in account service' do
          let(:error) { 'Could not find product support for account' }

          it 'raises not ready error' do
            assert_raises Zendesk::ProductNotReady do
              job.work
            end
          end
        end
      end
    end
  end
end
