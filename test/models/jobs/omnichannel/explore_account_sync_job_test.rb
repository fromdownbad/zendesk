require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe ExploreAccountSyncJob do
    fixtures :users, :accounts, :permission_sets

    describe '#work' do
      let(:job) { ExploreAccountSyncJob.new(account_id: account.id) }
      let(:account) { accounts(:multiproduct) }
      let(:sync_job) { stub }
      let(:products) { [Zendesk::Accounts::Product.new(name: Zendesk::Accounts::Client::EXPLORE_PRODUCT, state: Zendesk::Accounts::Product::SUBSCRIBED)] }

      before do
        Zendesk::Accounts::Client.any_instance.stubs(:products).returns(products)
      end

      it 'syncs explore entitlements for account agents' do
        Omnichannel::ExploreEntitlementSyncJob.stubs(:new).returns(sync_job).times(account.agents.count)
        sync_job.stubs(:work)
        job.work
      end

      describe 'without explore product' do
        let(:products) { [] }

        it 'does not sync anything' do
          Omnichannel::ExploreEntitlementSyncJob.expects(:new).never
          job.work
        end
      end
    end
  end
end
