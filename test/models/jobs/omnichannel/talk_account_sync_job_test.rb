require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe TalkAccountSyncJob do
    describe '#work' do
      let(:job) { TalkAccountSyncJob.new(account_id: account_id) }
      let(:account_id) { 123 }
      let(:account) { Account.new }
      let(:user1) { User.new }
      let(:user2) { User.new }
      let(:sync_job1) { stub }
      let(:sync_job2) { stub }
      let(:talk_product) do
        stub(
          name: Zendesk::Accounts::Client::TALK_PRODUCT.to_sym,
          state: Zendesk::Accounts::Product::SUBSCRIBED,
          plan_settings: {
            'plan_type' => plan_type
          }
        )
      end
      let(:plan_type) { 2 }
      let(:products) { [talk_product] }

      before do
        job.stubs(:account).returns(account)
        account.stubs(:agents).returns([user1, user2])
        account.stubs(:products).returns(products)
        user1.stubs(:id).returns(1)
        user2.stubs(:id).returns(2)
      end

      describe_with_arturo_enabled :ocp_talk_account_sync do
        it 'syncs talk entitlements for account agents' do
          Omnichannel::TalkEntitlementSyncJob.expects(:new).with(account_id: 123, user_id: 1).returns(sync_job1)
          Omnichannel::TalkEntitlementSyncJob.expects(:new).with(account_id: 123, user_id: 2).returns(sync_job2)
          sync_job1.expects(:work)
          sync_job2.expects(:work)
          job.work
        end

        describe 'without talk product' do
          let(:products) { [] }

          it 'does not sync anything' do
            Omnichannel::TalkEntitlementSyncJob.expects(:new).never
            job.work
          end
        end

        describe_with_arturo_enabled :central_admin_staff_mgmt_roles_tab do
          describe 'talk lite/legacy accounts' do
            let(:plan_type) { 3 }
            it 'syncs talk entitlements for account agents' do
              Omnichannel::TalkEntitlementSyncJob.expects(:new).with(account_id: 123, user_id: 1).returns(sync_job1)
              Omnichannel::TalkEntitlementSyncJob.expects(:new).with(account_id: 123, user_id: 2).returns(sync_job2)
              sync_job1.expects(:work)
              sync_job2.expects(:work)
              job.work
            end
          end

          describe 'not talk lite/legacy accounts' do
            it 'does not sync anything' do
              Omnichannel::TalkEntitlementSyncJob.expects(:new).never
              job.work
            end
          end
        end
      end

      describe_with_arturo_disabled :ocp_talk_account_sync do
        it 'does not sync anything' do
          Omnichannel::TalkEntitlementSyncJob.expects(:new).never
          job.work
        end
      end
    end
  end
end
