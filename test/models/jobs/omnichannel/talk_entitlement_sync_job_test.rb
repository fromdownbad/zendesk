require_relative "../../../support/job_helper"

SingleCov.covered!

module Omnichannel
  describe TalkEntitlementSyncJob do
    describe '#work' do
      let(:job) do
        TalkEntitlementSyncJob.new(
          account_id: account_id,
          user_id: user_id
        )
      end
      let(:account) { accounts(:with_paid_voice_and_zopim_subscription) }
      let(:account_id) { 39834980123 }
      let(:user) { users(:with_paid_voice_and_zopim_subscription_owner) }
      let(:user_id) { 89348723896 }
      let(:role) { 'agent' }
      let(:statsd_client) { stub_for_statsd }
      let(:products) { [Zendesk::Accounts::Product.new(name: Zendesk::Accounts::Client::TALK_PRODUCT, state: Zendesk::Accounts::Product::SUBSCRIBED)] }
      let!(:user_seat) { user.user_seats.create(seat_type: 'voice', account: account) }

      before do
        job.stubs(:account).returns(account)
        account.stubs(:id).returns(account_id)
        account.stubs(:products).returns(products)
        user.stubs(:id).returns(user_id)
        User.stubs(:find).returns(user)
        user.roles = Role::AGENT.id
        TalkEntitlementSyncJob.stubs(:statsd_client).returns(statsd_client)
      end

      describe 'when user has an active talk entitlement' do
        let(:payload) { { 'talk' => { 'name' => role, 'is_active' => true } } }
        it 'sends a request to Metropolis to set active entitlement' do
          Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).with(user_id, payload, is_end_user: false)
          job.work
        end
      end

      describe 'when user does not have an talk entitlement' do
        let(:role) { nil }
        let(:entitlement) { { 'name' => role } }
        let(:payload) { { 'talk' => nil } }
        let(:existing_entitlement) { 'agent' }

        before do
          Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).returns('talk' => existing_entitlement)
          user.stubs(:is_end_user?).returns(true)
        end

        it 'sends a request to Metropolis to disable current entitlement' do
          Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).with(user_id, payload, is_end_user: true)
          job.work
        end

        describe 'no existing entitlement' do
          let(:existing_entitlement) { nil }

          it 'ignores the user' do
            Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).never
            job.work
          end
        end
      end

      describe 'user is deleted' do
        before do
          Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).raises(Kragle::Gone.new)
        end

        it 'increments a metric on skip' do
          statsd_client.expects(:increment).with(:user_deleted).once
          job.work
        end
      end

      describe 'Talk is inactive' do
        let(:products) { [] }

        it 'increments a metric on skip' do
          statsd_client.expects(:increment).with(:product_inactive).once
          job.work
        end
      end

      describe 'when retry limit is reached and job gave up' do
        it 'increments a metric on giving up' do
          statsd_client.expects(:increment).with(:permanent_failure).once
          TalkEntitlementSyncJob.give_up(Kragle::ResponseError.new, account_id: account_id, user_id: user_id)
        end
      end
    end
  end
end
