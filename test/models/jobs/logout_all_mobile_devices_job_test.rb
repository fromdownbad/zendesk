require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe LogoutAllMobileDevicesJob do
  fixtures :accounts, :users, :tokens

  describe "#perform" do
    before do
      @account = accounts(:minimum)
      @admin = users(:minimum_admin)

      device_token = @admin.tokens.create!(account_id: @account.id, client_id: 1, user_id: @admin.id)
      @admin.mobile_devices.create!(account: @account, token: device_token.id)
      @admin.mobile_devices.create!(account: @account, token: 'guid-from-device').tap { |d| d.create_oauth_token(account_id: @account.id, client_id: 1) }.save!

      assert_equal 2, @account.mobile_devices.count
      assert_equal 2, @account.tokens.count
    end

    it "deletes all mobile oauth tokens but does not delete mobile devices" do
      LogoutAllMobileDevicesJob.perform(@account.id)

      assert_equal 0, @account.tokens.count
      assert_equal 2, @account.mobile_devices.count
    end
  end
end
