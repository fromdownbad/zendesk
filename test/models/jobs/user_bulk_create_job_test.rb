require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 3

describe UserBulkCreateJob do
  fixtures :accounts, :users, :organizations
  include DomainEventsHelper
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }
  let(:span) { stub(set_tag: {}, finish: {}) }

  before do
    ZendeskAPM.stubs(:trace).returns(span)
    span.stubs(:resource=)
    @account = accounts(:minimum)
    @organization1 = organizations(:minimum_organization1)
    @organization2 = organizations(:minimum_organization2)
    @users = [
      {name: "Test User", email: "test1@example.com", roles: "Admin", organization_id: @organization1.id},
      {name: "Test User2", email: "test2@example.com", roles: "Agent", organization_id: @organization2.id}
    ]
    @job = UserBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: @account.id,
      user_id: @account.owner_id,
      users: @users)
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  describe "With current user having suitable permissions" do
    before do
      User.instance_variable_set(:@reserved_global_uids, [])
      User.expects(:generate_many_uids).with(2).returns([1234, 1567])
    end

    describe 'instrumentation' do
      it 'sets correct APM facet tags' do
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :create)
        span.expects(:set_tag).with('zendesk.status', 'Created')
        @job.perform
      end
    end

    it "creates the users using reserved global uids" do
      @job.perform
      user1 = @account.users.find_by_name("Test User")
      user2 = @account.users.find_by_name("Test User2")

      assert user1
      assert user2
      assert_equal 1234, user1.id
      assert_equal 1567, user2.id
    end

    it "returns any available email or external_id in the results" do
      @job.options[:users][1] = { name: "Test User2", external_id: "ABC123", roles: "Agent"}

      results = @job.perform.last[:results]

      user1 = @account.users.find_by_name("Test User")
      user2 = @account.users.find_by_name("Test User2")

      assert_equal user1.email, results[0][:email]
      assert_nil results[1][:email]
      assert_equal user2.external_id, results[1][:external_id]
    end

    it "creates users on brand where request originated when active_brand_id is not provided" do
      @job = UserBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
        account_id: @account.id,
        user_id: @account.owner_id,
        users: @users,
        request_brand_id: 1234)

      User.any_instance.expects(:active_brand_id=).with(1234).twice

      @job.perform
    end

    it "emits user_created domain events" do
      @job.perform
      assert_equal 2, decode_user_events(domain_event_publisher.events, :user_organization_added).size
      assert_equal 2, decode_user_events(domain_event_publisher.events, :user_created).size
    end
  end

  describe "With current user not having suitable permissions" do
    before do
      @job.stubs(:current_user).returns(User.new)
    end

    it "does not create users and throw error response" do
      @job.perform
      assert_nil @account.users.find_by_name("Test User")
      assert_nil @account.users.find_by_name("Test User2")
    end

    it "does not emit user domain events" do
      @job.perform
      assert_equal 0, decode_user_events(domain_event_publisher.events).size
    end
  end

  describe "With current user able to create a subset of users" do
    before do
      @job.options[:users][1] = {name: "Test User2", email: "test2example.com", roles: "Agent"}
    end

    it "should properly convey the error and success statuses" do
      results = @job.perform
      assert_equal "Created", results.last[:results].first[:status]
      assert_equal "UserCreateError", results.last[:results].last[:error]
    end

    it "emits user_created domain events" do
      @job.perform
      assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
    end

    it 'sets correct APM facet tags' do
      span.expects(:set_tag).with('zendesk.id', any_parameters)
      span.expects(:set_tag).with('zendesk.action', :create)
      span.expects(:set_tag).with('zendesk.status', 'Failed')
      span.expects(:set_tag).with('zendesk.error', 'UserCreateError')
      span.expects(:set_tag).with('zendesk.details', any_parameters)
      @job.perform
    end
  end

  describe "With invalid organizations" do
    before do
      @job.options[:users][1] = {name: "Test User2", email: "test2example.com", roles: "Agent", organization_id: 123}
    end

    it "should properly convey the error and success statuses" do
      results = @job.perform
      assert_equal "Created", results.last[:results].first[:status]
      assert_equal "OrganizationNotFound", results.last[:results].last[:error]
      assert_equal "Failed", results.last[:results].last[:status]
    end

    it "emits correct domain events" do
      @job.perform
      assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
      assert_equal 1, decode_user_events(domain_event_publisher.events, :user_organization_added).size
    end

    it 'sets correct APM facet tags' do
      span.expects(:set_tag).with('zendesk.id', any_parameters)
      span.expects(:set_tag).with('zendesk.action', :create).never
      span.expects(:set_tag).with('zendesk.status', 'Failed')
      span.expects(:set_tag).with('zendesk.error', 'OrganizationNotFound')
      span.expects(:set_tag).with('zendesk.details', any_parameters).never
      @job.perform
    end
  end

  describe_with_arturo_enabled :bulk_job_errors_attribute do
    describe "when user creation fails" do
      before do
        @job.options[:users][1] = {name: "Test User2", email: "test2example.com", roles: "Agent"}
      end

      it "should return error instead of error" do
        results = @job.perform
        assert_equal "UserCreateError", results.last[:results].last[:errors]
      end

      it "should return which user" do
        results = @job.perform
        assert_includes results.last[:results].last[:details], 'Test User2'
      end

      it "emits user_created domain events" do
        @job.perform
        assert_equal 3, decode_user_events(domain_event_publisher.events).size
      end

      it 'sets correct APM facet tags' do
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :create)
        span.expects(:set_tag).with('zendesk.status', 'Failed')
        span.expects(:set_tag).with('zendesk.error', 'UserCreateError')
        span.expects(:set_tag).with('zendesk.details', any_parameters)
        @job.perform
      end
    end
  end
end
