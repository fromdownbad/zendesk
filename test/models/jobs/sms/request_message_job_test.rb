require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

module Sms
  describe RequestMessageJob do
    fixtures :all

    describe '#perform' do
      let(:account) { accounts(:minimum) }
      let(:ticket)  { tickets(:minimum_1) }
      let(:agent)   { users(:minimum_agent) }

      it 'sends an API request to the SMS app in reply to a ticket' do
        request = stub_request(
          :post,
          "https://#{account.subdomain}.zendesk-test.com/api/v2/channels/sms/internal/tickets/#{ticket.id}/reply"
        ).with(
          body: {
            message: {
              agent_id: agent.id,
              body: 'Message body'
            }
          }.to_json
        )
        RequestMessageJob.perform(account.id, ticket.id, agent.id, 'Message body')
        assert_requested(request)
      end
    end
  end
end
