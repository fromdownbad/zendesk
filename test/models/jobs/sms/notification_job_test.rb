require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

module Sms
  describe NotificationJob do
    fixtures :all

    describe '#perform' do
      let(:account) { accounts(:minimum) }

      it 'sends an API request to the SMS app to send an outbound message' do
        audit = events(:create_audit_for_minimum_ticket_1)
        user = users(:minimum_agent)

        request = stub_request(
          :post,
          "https://#{account.subdomain}.zendesk-test.com/api/v2/channels/sms/internal/triggers/5/messages"
        ).with(
          body: {
            message: {
              phone_number_id: -1,
              user_id: user.id,
              body: "This is the notification body",
              trigger_id: 5,
              ticket_id: audit.ticket_id,
              event_id: audit.id
            }
          }.to_json
        )

        NotificationJob.perform(account.id,
          phone_number_id: -1,
          user_id: user.id,
          body: "This is the notification body",
          trigger_id: 5,
          ticket_id: audit.ticket_id,
          event_id: audit.id)

        assert_requested(request)
      end
    end
  end
end
