require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 3

describe GooddataUserSyncJob do
  fixtures :accounts, :users

  let(:account)       { accounts(:minimum) }
  let(:user)          { users(:minimum_agent) }
  let(:gooddata_user) do
    GooddataUser.create! do |u|
      u.account = accounts(:minimum)
      u.user = users(:minimum_agent)
      u.gooddata_project_id = 'gooddata_project_id'
      u.gooddata_user_id = 'gooddata_user_id'
    end
  end

  describe ".work" do
    let(:user_provisioning) { stub('user provisioning') }

    before do
      Zendesk::Gooddata::UserProvisioning.stubs(:new).
        with(account).
        returns(user_provisioning)

      user_provisioning.stubs(:sync_gooddata_user)

      Zendesk::StatsD::Client.any_instance.stubs(:increment)
      Zendesk::StatsD::Client.any_instance.stubs(:histogram)
    end

    describe "when an integration exists for the account" do
      before do
        account.create_gooddata_integration(
          status: 'complete',
          project_id: 'project_id'
        )

        GooddataUserSyncJob.work(account.id, gooddata_user.id)
      end

      before_should "log the run" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')
      end

      before_should "sync the GoodData users" do
        user_provisioning.expects(:sync_gooddata_user).with(gooddata_user)
      end

      before_should "log the running time" do
        Zendesk::StatsD::Client.any_instance.expects('histogram').
          with('execution_time', kind_of(Float))
      end

      describe "and sync fails" do
        before do
          user_provisioning.expects(:sync_gooddata_user).raises(RuntimeError)
        end

        it "logs the failure" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed')

          assert_raises RuntimeError do
            GooddataUserSyncJob.work(account.id, gooddata_user.id)
          end
        end
      end
    end

    describe "when an integration does not exist for the account" do
      before do
        GooddataUserSyncJob.work(account.id, gooddata_user.id)
      end

      before_should "log the run" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')
      end

      before_should "not sync any users" do
        user_provisioning.expects(:sync_gooddata_user).
          with(gooddata_user).never
      end

      before_should "not log the running time" do
        Zendesk::StatsD::Client.any_instance.expects('histogram').
          with('execution_time', kind_of(Float)).never
      end
    end

    describe "when there are no users to sync" do
      before do
        destroyed_gooddata_user_id = gooddata_user.destroy.id

        GooddataUserSyncJob.work(account.id, destroyed_gooddata_user_id)
      end

      before_should "log the run" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')
      end

      before_should "not log the running time" do
        Zendesk::StatsD::Client.any_instance.expects('histogram').
          with('execution_time', kind_of(Float)).never
      end
    end
  end

  describe ".args_to_log" do
    let(:account_id)       { 1 }
    let(:gooddata_user_id) { 2 }

    before { GooddataUserSyncJob.stubs(:audit_id).with(3).returns(4) }

    it "logs the account and user IDs" do
      assert_equal(
        {
          account_id: account_id,
          gooddata_user_id: gooddata_user_id,
          audit_id: 4
        },
        GooddataUserSyncJob.args_to_log(account_id, gooddata_user_id, 3)
      )
    end
  end
end
