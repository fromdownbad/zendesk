require_relative "../../support/job_helper"
require 'salesforce_integration'

SingleCov.covered! uncovered: 4

describe 'SalesforceTicketSyncJob' do
  fixtures :accounts, :tickets

  describe "#perform" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.salesforce_ticket_data && @ticket.salesforce_ticket_data.delete
      @ticket.salesforce_ticket_data = SalesforceTicketData.create(ticket: @ticket, account: @ticket.account, sync_status: ExternalUserData::SyncStatus::SYNC_PENDING)
      @salesforce_integration = mock
      @salesforce_integration.stubs(:fetch_ticket_info).returns(records: [:foo])
      Account.any_instance.stubs(:salesforce_integration).returns(@salesforce_integration)
    end

    it "syncs data" do
      SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id)

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: [:foo]
      assert_equal ExternalUserData::SyncStatus::SYNC_OK, @ticket.salesforce_ticket_data.sync_status
    end

    it "handles login errors" do
      @salesforce_integration.stubs(:fetch_ticket_info).raises(Salesforce::Integration::LoginFailed)
      SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id)

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: []
      assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @ticket.salesforce_ticket_data.sync_status
    end

    it "handles connection errors" do
      @salesforce_integration.stubs(:fetch_ticket_info).raises(Faraday::Error::ConnectionFailed.new("end of file reached"))
      SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id)

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: []
      assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @ticket.salesforce_ticket_data.sync_status
    end

    it "handles timeout errors" do
      @salesforce_integration.stubs(:fetch_ticket_info).raises(Errno::ETIMEDOUT)
      SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id)

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: []
      assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @ticket.salesforce_ticket_data.sync_status
    end

    it "handles all client errors" do
      exception = Faraday::Error::ClientError.new('Server too busy')
      exception.stubs(:response).returns('Client Error')
      @salesforce_integration.stubs(:fetch_ticket_info).raises(exception)
      SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id)

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: []
      assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @ticket.salesforce_ticket_data.sync_status
    end

    it "handles 'RuntimeError' errors" do
      @salesforce_integration.stubs(:fetch_ticket_info).raises(Salesforce::Integration::RuntimeError)
      SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id)

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: []
      assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @ticket.salesforce_ticket_data.sync_status
    end

    it "handles 'TotalRequests Limit exceeded' errors" do
      @salesforce_integration.stubs(:fetch_ticket_info).raises(Exception.new("TotalRequests Limit exceeded"))
      SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id)

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: []
      assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @ticket.salesforce_ticket_data.sync_status
    end

    it "handles 'The REST API is not enabled for this Organization' errors" do
      @salesforce_integration.stubs(:fetch_ticket_info).raises(Exception.new("The REST API is not enabled for this Organization"))
      SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id)

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: []
      assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @ticket.salesforce_ticket_data.sync_status
    end

    it "handles 'Refresh token error' errors" do
      @salesforce_integration.stubs(:fetch_ticket_info).raises(Salesforce::NextGeneration::RefreshTokenError)
      SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id)

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: []
      assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @ticket.salesforce_ticket_data.sync_status
    end

    it "raises unknown errors" do
      @salesforce_integration.stubs(:fetch_ticket_info).raises(Exception.new)
      assert_raises(Exception) { SalesforceTicketSyncJob.perform(@ticket.account_id, @ticket.nice_id) }

      @ticket.salesforce_ticket_data.reload
      assert_equal @ticket.salesforce_ticket_data.data, records: []
      assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @ticket.salesforce_ticket_data.sync_status
    end
  end
end
