require_relative "../../support/job_helper"

SingleCov.covered!

describe SetUserLocaleJob do
  fixtures :accounts, :users, :translation_locales

  describe "#work" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_agent) }
    let(:spanish_locale) { translation_locales(:spanish) }
    let(:non_localized_agent_locale) { TranslationLocale.where(localized_agent: false).first }

    before do
      Account.any_instance.stubs(:available_languages).returns(TranslationLocale.all)
    end

    it "sets the user locale when a compatible locale is found in the settler" do
      args = {
        account_id: account.id,
        user_id: user.id,
        language: 'es-ES'
      }

      assert_equal ENGLISH_BY_ZENDESK, user.translation_locale
      SetUserLocaleJob.work(args)
      assert_equal spanish_locale, user.reload.translation_locale
    end

    it "defaults to account's locale when no compatible locale is found" do
      Account.any_instance.stubs(:translation_locale).returns(non_localized_agent_locale)

      args = {
        account_id: account.id,
        user_id: user.id,
        language: 'ru-RU'
      }

      assert_equal ENGLISH_BY_ZENDESK, user.translation_locale
      SetUserLocaleJob.work(args)
      assert_equal non_localized_agent_locale, user.reload.translation_locale
    end

    it "does not try to update if the user's locale is already set to the provided one" do
      args = {
        account_id: account.id,
        user_id: user.id,
        language: 'en-US'
      }

      assert_equal ENGLISH_BY_ZENDESK, user.translation_locale
      User.any_instance.expects(:update_attribute).with(:translation_locale, ENGLISH_BY_ZENDESK).never
      SetUserLocaleJob.work(args)
    end
  end
end
