require_relative "../../support/job_helper"

SingleCov.covered!

describe VariantsBulkCreateJob do
  fixtures :accounts

  let(:fbattrs) { {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: 1 } }
  let(:account) { accounts(:minimum) }
  let(:text) { Cms::Text.create!(name: "I haz a reference", fallback_attributes: fbattrs, account: account) }
  let(:job_params) { { account_id: account.id, user_id: account.owner_id, item_id: text.id } }

  describe "with valid parameters" do
    let(:params) do
      {variants: [
        { content: "Content A", locale_id: 2, active: true, is_fallback: false},
        { content: "Content B", locale_id: 4, active: true, is_fallback: false}
      ]}
    end

    before do
      @job = VariantsBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919", job_params.merge(params))
      @job.perform
    end

    should_change("the item cms variant count", by: 2) { text.variants.count(:all) }
    should_change("the dc translations count", by: 3) { DC::Translation.count(:all) }
  end

  describe "with invalid variants" do
    let(:params) do
      {variants: [
        { content: "Content A", locale_id: 1, active: true, is_fallback: false},
        { content: "Content B", locale_id: 1, active: true, is_fallback: false}
      ]}
    end

    before do
      @job = VariantsBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919", job_params.merge(params))
      @result = @job.perform
    end

    should_not_change("the the number of variants") { text.variants.count(:all) }

    it "returns error status" do
      errors = @result.last[:results].first
      assert_equal "Failed Creating variant ", errors[:status]
      assert_equal "Translation locale 1 has already been taken", errors[:errors]
    end
  end
end
