require_relative "../../support/job_helper"

SingleCov.covered!

describe GsuiteDeleteDomainAliasJob do
  before do
    @access_token = 'access_token'
    @domain = 'zobuzendesk.com'
    @customer_id = "abc0123"
    @client_stub = stub(delete_domain_alias: nil)
    Zendesk::Google::GAMClient.stubs(:new).returns(@client_stub)
  end

  it "calls the client's delete domain alias method" do
    domain_alias = "test-domain.com"
    @client_stub.expects(:delete_domain_alias).with(customer_id: @customer_id, domain_alias: domain_alias)
    GsuiteDeleteDomainAliasJob.enqueue(@access_token, @domain, @customer_id, domain_alias)
  end
end
