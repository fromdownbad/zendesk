require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe TrialExtrasJob do
  fixtures :accounts, :addresses
  include PerformJob

  before do
    @account = accounts(:minimum)
    @redis_client = TrialExtrasJob.redis_client
    @redis_client.set("account_#{@account.id}_trial_extras", "{\"test_field\":\"test_val\"}")
    @statsd_stub = stub_for_statsd
    TrialExtrasJob.stubs(statsd_client: @statsd_stub)
  end

  it "saves trial extras from redis to database" do
    TrialExtrasJob.enqueue(@account.id)

    assert_not_nil TrialExtra.where(account_id: @account.id, key: "test_field", value: "test_val").first
  end

  it 'tracks success to datadog' do
    @statsd_stub.expects(:increment).with('count', tags: ["status:success", "account_id:#{@account.id}"])

    TrialExtrasJob.enqueue(@account.id)
  end

  it 'tracks failure to datadog' do
    Account.any_instance.stubs(:save!).raises(StandardError)
    @statsd_stub.expects(:increment).with('count', tags: ["status:failure", "account_id:#{@account.id}"])

    TrialExtrasJob.enqueue(@account.id)
  end

  it 'succeeds if trial extras is empty' do
    @redis_client.stubs(:get).returns("{}")
    @statsd_stub.expects(:increment).with('count', tags: ["status:success", "account_id:#{@account.id}"])

    TrialExtrasJob.enqueue(@account.id)
  end

  it 'logs an error if redis trial extras are not set' do
    @redis_client.stubs(:get).returns(nil)
    @statsd_stub.expects(:increment).with('count', tags: ['status:failure', 'reason:trial_extras_missing', "account_id:#{@account.id}"])
    @account.expects(:save!).never

    TrialExtrasJob.enqueue(@account.id)
  end
end
