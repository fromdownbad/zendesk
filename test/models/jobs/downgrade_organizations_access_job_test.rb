require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'DowngradeOrganizationsAccessJob' do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:end_user) { users(:minimum_end_user) }

  describe "#perform" do
    it "changes restriction_id from 'organization' to 'assigned' for agents and to 'requested' for end users" do
      agent.update_attribute(:restriction_id, RoleRestrictionType.ORGANIZATION)
      end_user.update_attribute(:restriction_id, RoleRestrictionType.ORGANIZATION)

      DowngradeOrganizationsAccessJob.perform(account.id)

      assert_equal RoleRestrictionType.ASSIGNED, agent.reload.restriction_id
      assert_equal RoleRestrictionType.REQUESTED, end_user.reload.restriction_id
    end

    it "doesn't change restriction_id if it's different than 'organization'" do
      agent.update_attribute(:restriction_id, RoleRestrictionType.NONE)

      DowngradeOrganizationsAccessJob.perform(account.id)

      assert_equal RoleRestrictionType.NONE, agent.reload.restriction_id
    end
  end
end
