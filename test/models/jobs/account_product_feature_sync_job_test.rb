require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 1

describe AccountProductFeatureSyncJob do
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:enabled) { true }

  describe '::work' do
    before do
      Subscription.any_instance.expects(:has_feature_name?).returns(enabled)
      AccountProductFeatureSyncJob.work(account.id, 'feature_name', 'setting_name')
    end

    describe 'when the feature is on' do
      let(:enabled) { true }

      it 'enables it in the account service' do
        assert_requested(
          :patch,
          "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products/support?include_deleted_account=false",
          body: '{"product":{"plan_settings":{"setting_name":true}}}'
        )
      end
    end

    describe 'when the feature is off' do
      let(:enabled) { false }

      it 'disables it in the account service' do
        assert_requested(
          :patch,
          "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products/support?include_deleted_account=false",
          body: '{"product":{"plan_settings":{"setting_name":false}}}'
        )
      end
    end
  end
end
