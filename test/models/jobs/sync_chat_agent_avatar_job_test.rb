require_relative "../../support/job_helper"

SingleCov.covered!

describe SyncChatAgentAvatarJob do
  fixtures :accounts, :users

  describe "#work" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_agent) }

    describe "when account is found" do
      describe "and user is found" do
        it "calls the Zopim internal API to synchronize the user avatar" do
          Zopim::InternalApiClient.any_instance.expects(:update_agent_setting_avatar).with(user.id, user.photo_url)

          SyncChatAgentAvatarJob.work(account.id, user.id)
        end
      end

      describe "and user is not found" do
        it "does calls the Zopim internal API" do
          Zopim::InternalApiClient.any_instance.expects(:update_agent_setting_avatar).never

          SyncChatAgentAvatarJob.work(account.id, 2)
        end
      end
    end

    describe "when account is not found" do
      it "raises ActiveRecord::RecordNotFound" do
        assert_raises ActiveRecord::RecordNotFound do
          SyncChatAgentAvatarJob.work(0, 1)
        end
      end
    end
  end
end
