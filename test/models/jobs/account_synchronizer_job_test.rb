require_relative '../../support/job_helper'

SingleCov.covered!

describe AccountSynchronizerJob do
  include PerformJob

  let(:described_class) { AccountSynchronizerJob }

  let(:payload) { stub }

  it 'delegates to the AccountSynchronizer service' do
    AccountSynchronizer.expects(:call).with(payload)
    perform_job(described_class, payload)
  end

  describe '.work' do
    subject { described_class.work(payload) }

    it 'delegates to the AccountSynchronizer service' do
      AccountSynchronizer.expects(:call).with(payload)
      subject
    end
  end

  describe '.args_to_log' do
    subject { described_class.args_to_log(payload) }

    let(:expected) do
      { payload: payload }
    end

    it { assert_equal expected, subject }
  end
end
