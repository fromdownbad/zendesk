require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'UserViewCsvJob' do
  fixtures :accounts, :users, :rules, :cf_fields, :translation_locales, :organizations

  before do
    Arturo.enable_feature!(:user_views_negative_operators_enabled)
  end

  def self.should_paginate_across_all_users_that_satisfy_the_user_view
    it 'paginates across all users that satisfy the user view' do
      UserViewCsvJob.stubs(:per_page).returns(1)

      UserViewCsvJob.perform(account.id, user.id, user_view.id)

      attachment = ExpirableAttachment.last
      assert_match PermalinkFu.escape(user_view.title), attachment.filename
      attachment.content_type.must_include "application/zip"

      Zip::InputStream.open(attachment.public_filename) do |io|
        entry = io.get_next_entry
        contents = io.read.split("\n")

        assert_match PermalinkFu.escape(user_view.title), entry.name
        assert_equal %("User ID","Name","Created","Date"), contents[0]
        expected_users.each_with_index do |user, index|
          date1_value = nil
          if (date1 = user.custom_field_values.value_for_key('date1'))
            date1_value = Time.parse(date1.value).in_time_zone.to_s(:csv)
          end
          assert_equal %("#{user.id}","#{user.name}","#{user.created_at.in_time_zone.to_s(:csv)}","#{date1_value}"), contents[index + 1]
        end
      end
    end
  end

  describe '#perform' do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_admin) }
    let(:user_view) { rules(:minimum_custom_fields_view) }
    let(:expected_users) { Zendesk::UserViews::Executer.users_for_view(user_view, user) }

    before do
      stub_request(:put, %r{\.amazonaws.com/data/expirable_attachments/.*.zip})
    end

    it 'creates an expirable_attachment and send an email containing a csv file of all the users that satisfy the user view' do
      account.settings.has_user_tags = true
      account.settings.save!
      user_view.output.columns += ["current_tags", "organization_id"]
      user = expected_users.last
      user.tags = "abcd, 123"
      user.save!
      user_view.save!
      assert_difference('ExpirableAttachment.count(:all)') do
        assert_emails_sent { UserViewCsvJob.perform(account.id, user.id, user_view.id) }

        attachment = ExpirableAttachment.last
        assert_match PermalinkFu.escape(user_view.title), attachment.filename
        attachment.content_type.must_include "application/zip"

        Zip::InputStream.open(attachment.public_filename) do |io|
          entry = io.get_next_entry
          contents = io.read.split("\n")
          assert_match PermalinkFu.escape(user_view.title), entry.name
          assert_equal %("User ID","Name","Created","Date","Tags","Organization"), contents[0]
          expected_users.each_with_index do |u, index|
            date1_value = nil
            if (date1 = u.custom_field_values.value_for_key('date1'))
              date1_value = Time.parse(date1.value).in_time_zone.to_s(:csv)
            end
            assert_equal %("#{u.id}","#{u.name}","#{u.created_at.in_time_zone.to_s(:csv)}","#{date1_value}","#{u.tags.join(", ")}","#{u.organization.try(:name)}"), contents[index + 1]
          end
        end

        email = ActionMailer::Base.deliveries.last
        assert_equal [user.email], email.to
        assert_match user_view.title, email.subject
        assert_match user_view.title, email.joined_bodies
        assert_includes email.joined_bodies, attachment.url(token: true, mapped: false)
      end
    end

    describe 'without user views sampling enabled' do
      before do
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(false)
      end

      should_paginate_across_all_users_that_satisfy_the_user_view
    end

    describe 'with user views sampling enabled' do
      before do
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(true)
        account.settings.user_count = account.users.count(:all)
        account.save
      end

      # Naive way of getting all un-sampled users.
      let(:expected_users) do
        original_value = account.has_user_views_sampling_enabled?
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(false)
        result = Zendesk::UserViews::Executer.users_for_view(user_view, user)
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(original_value)
        result
      end

      it 'expects all users' do
        assert !expected_users.empty?
      end

      should_paginate_across_all_users_that_satisfy_the_user_view
    end

    it 'is in the medium priority queue' do
      assert_equal :medium, Resque.queue_from_class(UserViewCsvJob)
    end

    it 'is throttled to run every 10 minutes' do
      assert_equal 10.minutes, UserViewCsvJob.settings[:can_run_every]
    end

    it 'limits export to 100,000 rows' do
      assert_equal 100_000, UserViewCsvJob.limit
    end

    it 'caps number of rows if CSV export exceeds limit' do
      UserViewCsvJob.stubs(:per_page).returns(2)
      UserViewCsvJob.stubs(:limit).returns(1)

      UserViewCsvJob.perform(account.id, user.id, user_view.id)

      attachment = ExpirableAttachment.last
      assert_match PermalinkFu.escape(user_view.title), attachment.filename
      attachment.content_type.must_include "application/zip"

      Zip::InputStream.open(attachment.public_filename) do |io|
        io.get_next_entry
        contents = io.read.split("\n")
        assert_equal 2, contents.length
        user = expected_users.first
        assert_equal %("User ID","Name","Created","Date"), contents[0]
        assert_equal %("#{user.id}","#{user.name}","#{user.created_at.in_time_zone.to_s(:csv)}",""), contents[1]
      end
    end

    it 'caps number of rows if CSV export limit exceeds first page' do
      UserViewCsvJob.stubs(:per_page).returns(2)
      UserViewCsvJob.stubs(:limit).returns(3)

      UserViewCsvJob.perform(account.id, user.id, user_view.id)

      attachment = ExpirableAttachment.last
      assert_match PermalinkFu.escape(user_view.title), attachment.filename
      attachment.content_type.must_include "application/zip"

      Zip::InputStream.open(attachment.public_filename) do |io|
        io.get_next_entry
        contents = io.read.split("\n")

        assert_equal 4, contents.length
        user = expected_users.first
        assert_equal %("User ID","Name","Created","Date"), contents[0]
        assert_equal %("#{user.id}","#{user.name}","#{user.created_at.in_time_zone.to_s(:csv)}",""), contents[1]
      end
    end

    it 'caps number of rows if CSV export limit is contained within first page' do
      UserViewCsvJob.stubs(:per_page).returns(10)
      UserViewCsvJob.stubs(:limit).returns(3)

      UserViewCsvJob.perform(account.id, user.id, user_view.id)

      attachment = ExpirableAttachment.last
      assert_match PermalinkFu.escape(user_view.title), attachment.filename
      attachment.content_type.must_include "application/zip"

      Zip::InputStream.open(attachment.public_filename) do |io|
        io.get_next_entry
        contents = io.read.split("\n")

        assert_equal 4, contents.length
        user = expected_users.first
        assert_equal %("User ID","Name","Created","Date"), contents[0]
        assert_equal %("#{user.id}","#{user.name}","#{user.created_at.in_time_zone.to_s(:csv)}",""), contents[1]
      end
    end

    it 'uses locale of user' do
      portuguese = translation_locales(:brazilian_portuguese)

      user.translation_locale = portuguese
      user.save!

      assert_emails_sent { UserViewCsvJob.perform(account.id, user.id, user_view.id) }

      email = ActionMailer::Base.deliveries.last
      assert_equal "pt-BR_subject #{user_view.title}", email.subject
      email.joined_bodies.must_include "pt-BR_message_1"
      email.joined_bodies.must_include "pt-BR_message_2"
    end

    it 'escapes macro strings' do
      macro_characters = ["=", "+", "-", "@"]
      users = []
      macro_characters.each do |char|
        u = expected_users.first.dup
        u.name = "#{char}AND(2>1)"
        users << u if u.save!
      end

      UserViewCsvJob.perform(account.id, user.id, user_view.id)

      attachment = ExpirableAttachment.last

      Zip::InputStream.open(attachment.public_filename) do |io|
        io.get_next_entry
        contents = io.read.split("\n")

        users.each_with_index do |u, i|
          exported_user_name = contents[i + expected_users.count].split(',').second
          assert_equal %("'#{u.name}"), exported_user_name
        end
      end
    end
  end
end
