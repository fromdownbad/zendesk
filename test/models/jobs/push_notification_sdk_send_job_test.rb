require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 9

describe PushNotificationSdkSendJob do
  fixtures :accounts, :tickets, :users, :device_identifiers, :mobile_sdk_apps

  describe "#work" do
    let(:account) { accounts(:minimum_sdk) }
    let(:mobile_sdk_app) { mobile_sdk_apps(:minimum_sdk) }
    let(:device) { device_identifiers(:sdk_authenticated) }
    let(:user) { users(:minimum_agent) }
    let(:ticket) { tickets(:minimum_1) }
    let(:payload) do
      { "user_id" => user.id, "msg" => "foo", "ticket_id" => ticket.id }
    end

    let(:args) do
      {
        "account_id" => account.id,
        "device_ids" => [device.id],
        "mobile_sdk_app_id" => mobile_sdk_app.id,
        "payload" => payload
      }
    end

    describe "sdk app has push notifications enabled" do
      before do
        mobile_sdk_app.settings.push_notifications_enabled = true
        mobile_sdk_app.save!
      end

      describe "Custom callback based push notifications" do
        let(:customer_callback) { "https://client.service.com/zendesk/push" }

        before do
          mobile_sdk_app.settings.push_notifications_type = MobileSdkApp::PUSH_NOTIF_TYPE_WEBHOOK
          mobile_sdk_app.settings.push_notifications_callback = customer_callback
          mobile_sdk_app.save!
        end

        it "builds a client with the proper configuration" do
          PushNotifications::WebhookClient.expects(:new).with(
            customer_callback
          ).returns(
            mock(push: mock(success?: true))
          )

          PushNotificationSdkSendJob.work(args)
        end

        it "sends the push notification" do
          PushNotifications::WebhookClient.any_instance.expects(:push).with([device.id], payload).returns(
            mock(success?: true)
          )

          PushNotificationSdkSendJob.work(args)
        end
      end

      describe "Urban Airship push notifications" do
        let(:urban_airship_app_key) { "urban_airship_app_key" }
        let(:urban_airship_app_secret) { "urban_airship_app_secret" }

        before do
          mobile_sdk_app.settings.push_notifications_type = MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP
          mobile_sdk_app.settings.urban_airship_key = urban_airship_app_key
          mobile_sdk_app.settings.urban_airship_master_secret = urban_airship_app_secret
          mobile_sdk_app.save!
        end

        it "builds a client with the proper configuration" do
          PushNotifications::UrbanAirshipClient.expects(:new).with(
            urban_airship_app_key, urban_airship_app_secret,
            statsd_tags: ["name:sdk_send"]
          ).returns(
            mock(push: [mock(success?: true)])
          )

          PushNotificationSdkSendJob.work(args)
        end

        it "send the push notification" do
          PushNotifications::UrbanAirshipClient.any_instance.expects(:push).with(
            [device.id],
            payload,
            enable_collapse_key: true
          ).returns([mock(success?: true)])

          PushNotificationSdkSendJob.work(args)
        end
      end
    end

    describe "sdk app has push notifications disabled" do
      before do
        mobile_sdk_app.settings.push_notifications_enabled = false
        mobile_sdk_app.save!
      end

      it "does not send a notification" do
        PushNotifications::WebhookClient.expects(:new).never

        PushNotificationSdkSendJob.work(args)
      end
    end
  end
end
