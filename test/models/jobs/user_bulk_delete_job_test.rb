require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe UserBulkDeleteJob do
  fixtures :accounts, :users
  include DomainEventsHelper
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
    @account = accounts(:minimum)
    @user1 = users(:minimum_author)
    @user2 = users(:minimum_author_suspend)
    @job = UserBulkDeleteJob.new("status job uuid", account_id: @account.id, user_id: @account.owner_id, user_ids: [@user1.id, @user2.id])
  end

  describe "with current user not having suitable permissions" do
    before do
      @agent = users(:minimum_agent)
      @job.stubs(:current_user).returns(@agent)
    end

    it "does not delete users and throw error response" do
      results = @job.perform.last[:results]
      expected_results = [I18n.t('txt.job.user_bulk_delete_job.problem_encountered', locale: @agent.translation_locale)]
      assert_equal expected_results, results
      assert @account.users.find(@user1.id)
      assert @account.users.find(@user2.id)
    end

    it "does not emits user_identity_removed events" do
      @job.perform
      assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
    end

    it "does not emit user_soft_deleted domain events" do
      @job.perform
      assert_equal 0, decode_user_events(domain_event_publisher.events, :user_soft_deleted).size
    end
  end

  describe "when there are unforseen exceptions" do
    it "logs and reraise" do
      Rails.logger.expects(:error).with("Failed to complete job due to #{Exception}")
      User.any_instance.expects(:delete!).raises(Exception)
      assert_raises Exception do
        @job.perform
      end
    end
  end

  describe "With current user able to delete a subset of users" do
    it "shoulds properly convey the mixed success status" do
      assert @user1.tickets.any?
      assert_empty(@user2.tickets)

      results = @job.perform.last[:results]
      expected_results = [I18n.t('txt.job.user_bulk_delete_job.failed_for_users', count: 1, locale: @account.owner.translation_locale)]
      assert_equal expected_results, results

      assert @account.users.find(@user1.id)
      assert_nil @account.users.find_by_id(@user2.id)
    end

    it "emits correct user_soft_deleted domain events" do
      @job.perform
      assert_equal 1, decode_user_events(domain_event_publisher.events, :user_soft_deleted).size
    end
  end

  describe "With current user having suitable permissions" do
    before do
      @user1.tickets.update_all(status_id: StatusType.CLOSED)
    end

    it "deletes the users (and leave their content by default)" do
      assert @user1.posts.any?
      @job.perform
      assert @user1.reload.posts.any?
      assert_nil @account.users.find_by_id(@user1.id)
      assert_nil @account.users.find_by_id(@user2.id)
    end

    it "emits correct user_soft_deleted domain events" do
      @job.perform
      assert_equal 2, decode_user_events(domain_event_publisher.events, :user_soft_deleted).size
    end

    describe "with :remove_content => true" do
      before do
        @job.options[:remove_content] = true
        assert @user1.posts.any?
        @job.perform
      end

      it "deletes the users and their content" do
        assert_empty(@user1.reload.posts)
        assert_nil @account.users.find_by_id(@user1.id)
        assert_nil @account.users.find_by_id(@user2.id)
      end

      it "does not emits user_identity_removed events" do
        assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
      end

      it "emits correct user_soft_deleted domain events" do
        assert_equal 2, decode_user_events(domain_event_publisher.events, :user_soft_deleted).size
      end
    end
  end
end
