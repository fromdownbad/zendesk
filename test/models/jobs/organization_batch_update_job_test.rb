require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe OrganizationBatchUpdateJob do
  fixtures :accounts, :users, :organizations
  include DomainEventsHelper

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:organization1) { organizations(:minimum_organization1) }
  let(:organization2) { organizations(:minimum_organization2) }
  let(:organization3) { organizations(:minimum_organization3) }
  let(:updated_details) { "Updated details" }
  let(:job) do
    OrganizationBatchUpdateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      organizations: [
        { id: organization1.id, details: updated_details, tags: %w[test1] },
        { id: organization2.id, details: updated_details, tags: %w[test2] }
      ])
  end
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    Organization.any_instance.stubs(domain_event_publisher: domain_event_publisher)
    Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
  end

  describe "#perform" do
    describe "as a non restricted agent" do
      before do
        job.options[:user_id] = agent.id
        job.options[:organizations] = [
          { id: organization1.id, details: updated_details, notes: "Updated notes" },
          { id: organization2.id, details: updated_details, notes: "Updated notes" },
          { id: organization3.id, details: updated_details }
        ]
        job.perform
      end

      it "only updates notes if notes are present" do
        assert_equal organization1.details, organization1.reload.details
        assert_equal "Updated notes", organization1.reload.notes
        assert_nil organization2.reload.details
        assert_equal "Updated notes", organization2.reload.notes
        assert_nil organization3.reload.details
        assert_equal "Sample notes", organization3.reload.notes
      end
    end

    describe "as an admin" do
      describe "when referencing with id" do
        before do
          @success_results = [
            {"action" => "update", "status" => "Updated", "success" => true, "id" => organization1.id},
            {"action" => "update", "status" => "Updated", "success" => true, "id" => organization2.id}
          ]

          job.perform
        end

        it "updates organizations" do
          assert_equal @success_results, job.status["results"]
          assert_equal updated_details, organization1.reload.details
          assert_equal updated_details, organization2.reload.details
        end

        describe "organization tags changed domain events" do
          it "emits tags changed events" do
            events = decoded_organization_events(domain_event_publisher.events, :tags_changed)
            assert_equal 2, events.size
            assert_equal %w[test1 test2], events.map { |e| e.tags_changed.tags_added.tags }.flatten
            assert_equal %w[beta.test premium], events.map { |e| e.tags_changed.tags_removed.tags }.flatten
          end
        end
      end

      describe "when referencing with external_id" do
        before do
          @organization_ex1 = FactoryBot.create(:organization, account: account, external_id: 'external_1', name: 'external_1')
          @organization_ex2 = FactoryBot.create(:organization, account: account, external_id: 'external_2', name: 'external_2')

          @organizations =
            job.options[:organizations] = [
              { external_id: @organization_ex1.external_id, details: updated_details },
              { external_id: @organization_ex2.external_id, details: updated_details }
            ]
          job.perform
        end

        it "updates organizations" do
          assert_equal updated_details, @organization_ex1.reload.details
          assert_equal updated_details, @organization_ex2.reload.details
        end
      end

      describe "with invalid params" do
        before do
          job.options[:organizations] = [
            { id: 1, details: updated_details, notes: "Updated notes" },
            { external_id: 2, details: updated_details, notes: "Updated notes" }
          ]
          job.perform
        end

        it "returns error" do
          assert_includes job.status["results"], "index" => 0, "error" => "OrganizationNotFound", "id" => 1
          assert_includes job.status["results"], "index" => 1, "error" => "OrganizationNotFound", "external_id" => 2
        end
      end
    end
  end
end
