require_relative "../../support/test_helper"

ENV['RSPAMD_API_ENDPOINT'] = 'http://127.0.0.1:9001'
ENV['RSPAMD_SPAM_RESOURCE'] = '/learnspam'
ENV['RSPAMD_HAM_RESOURCE'] = '/learnham'
ENV['RSPAMD_PASSWORD'] = 'GoodPassword'
SingleCov.covered! uncovered: 4

describe 'RspamdFeedbackJob' do
  fixtures :users, :accounts
  let(:user) { users(:minimum_agent) }
  let(:recipient) { 'test@support.zendesk.com' }

  describe "Rspamd feedback" do
    before do
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []

      stub_request(:post, 'http://127.0.0.1:9001/learnham').
        to_return(status: 200, body: 'learnt ham', headers: {})
      stub_request(:post, 'http://127.0.0.1:9001/learnspam').
        to_return(status: 200, body: 'learnt spam', headers: {})
    end

    it "does nothing when there's no identifier" do
      Zendesk::InboundMail::RspamdTrain.expects(:call).never
      RspamdFeedbackJob.report_ham(user: user, identifier: nil, recipient: recipient)
    end

    it "does nothing when the analysis headers are missing" do
      create_remote_file("1/no_analysis_header.json",
        content_type: "application/json",
        content: '{"headers": {} }')

      Zendesk::InboundMail::RspamdTrain.expects(:call).never
      RspamdFeedbackJob.report_ham(user: user, identifier: '1/no_analysis_header.eml', recipient: recipient)
      RspamdFeedbackJob.report_spam(user: user, identifier: '1/no_analysis_header.eml', recipient: recipient)
    end

    describe "reporting spam" do
      it "does report to Rspamd when Rspamd agrees it's spam" do
        create_remote_file("1/known_spam.json",
          content_type: "application/json",
          content: '{"headers": {"X-Rspamd-Status": ["Yes, 10.0"]}}')

        Zendesk::StatsD::Client.any_instance.expects(:increment).with('starting', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.starting', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('finished', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.finished', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('client.enqueue', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.client.enqueue', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('agree.spam')
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('spam.bayes.peruser.success')
        Zendesk::InboundMail::RspamdTrain.new(raw_email: anything, classification: 'spam', recipient: recipient, classifier: 'peruser', backend: 'bayes')
        RspamdFeedbackJob.report_spam(user: user, identifier: '1/known_spam.eml', recipient: recipient, classifier: 'peruser', backend: 'bayes')
      end

      it "does report to Spam when Rspamd does not agree it's spam" do
        create_remote_file("1/known_spam.json",
          content_type: "application/json",
          content: '{"headers": {"X-Rspamd-Status": ["No, 4.0"]}}')

        Zendesk::StatsD::Client.any_instance.expects(:increment).with('starting', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.starting', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('finished', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.finished', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('client.enqueue', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.client.enqueue', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('agree.spam').never
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('spam.bayes.peruser.success')
        Zendesk::InboundMail::RspamdTrain.new(raw_email: anything, classification: 'spam', recipient: recipient, classifier: 'peruser', backend: 'bayes')
        RspamdFeedbackJob.report_spam(user: user, identifier: '1/known_spam.eml', recipient: recipient, classifier: 'peruser', backend: 'bayes')
      end
    end

    describe "reporting ham" do
      it "does report to Rspamd when Rspamd agrees it's ham" do
        create_remote_file("1/known_ham.json",
          content_type: "application/json",
          content: '{"headers": {"X-Rspamd-Status": ["No, 4.0"]}}')

        Zendesk::StatsD::Client.any_instance.expects(:increment).with('starting', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.starting', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('finished', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.finished', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('client.enqueue', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.client.enqueue', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('ham.bayes.peruser.success')
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('agree.ham')
        Zendesk::InboundMail::RspamdTrain.new(raw_email: anything, classification: 'ham', recipient: recipient, classifier: 'peruser', backend: 'bayes')
        RspamdFeedbackJob.report_ham(user: user, identifier: '1/known_ham.eml', recipient: recipient, classifier: 'peruser', backend: 'bayes')
      end

      it "does report to Ham when Rspamd does not agree it's ham" do
        create_remote_file("1/known_ham.json",
          content_type: "application/json",
          content: '{"headers": {"X-Rspamd-Status": ["Yes, 7.0"]}}')

        Zendesk::StatsD::Client.any_instance.expects(:increment).with('starting', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.starting', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('finished', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.finished', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('client.enqueue', tags:  ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('resque.client.enqueue', tags: ['job:RspamdFeedbackJob', 'queue:medium'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('agree.ham').never
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('ham.bayes.peruser.success')
        Zendesk::InboundMail::RspamdTrain.new(raw_email: anything, classification: 'ham', recipient: recipient, classifier: 'peruser', backend: 'bayes')
        RspamdFeedbackJob.report_ham(user: user, identifier: '1/known_ham.eml', recipient: recipient, classifier: 'peruser', backend: 'bayes')
      end
    end
  end

  def create_remote_file(identifier, params)
    default_params = {
      configuration: Zendesk::Mail.raw_remote_files.name
    }

    RemoteFiles::File.new(identifier, default_params.merge(params)).store_once!
  end
end
