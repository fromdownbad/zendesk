require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 3

describe GooddataUserCreateJob do
  fixtures :accounts, :users

  let(:account)       { accounts(:minimum) }
  let(:user)          { users(:minimum_agent) }

  describe ".work" do
    let(:user_provisioning) { stub('user provisioning') }

    before do
      Zendesk::Gooddata::UserProvisioning.stubs(:new).
        with(account).
        returns(user_provisioning)

      user_provisioning.stubs(:create_gooddata_user)

      Zendesk::StatsD::Client.any_instance.stubs(:histogram)
      Zendesk::StatsD::Client.any_instance.stubs(:increment)
    end

    describe "when a GoodData does not user exist" do
      before do
        GooddataUserCreateJob.work(account.id, user.id)
      end

      before_should "provisions a GoodData user" do
        user_provisioning.expects(:create_gooddata_user).with(user)
      end

      before_should "log the run" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')
      end

      before_should "log the running time" do
        Zendesk::StatsD::Client.any_instance.expects(:histogram).
          with('execution_time', kind_of(Float))
      end

      describe "and user creation fails" do
        before do
          user_provisioning.expects(:create_gooddata_user).raises(RuntimeError)
        end

        it "logs the failure" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed')

          assert_raises RuntimeError do
            GooddataUserCreateJob.work(account.id, user.id)
          end
        end
      end
    end

    describe "when a GoodData user exists" do
      before do
        GooddataUser.create! do |u|
          u.account = accounts(:minimum)
          u.user = users(:minimum_agent)
          u.gooddata_project_id = 'gooddata_project_id'
          u.gooddata_user_id = 'gooddata_user_id'
        end
        GooddataUserCreateJob.work(account.id, user.id)
      end

      before_should "does not try to provision the user" do
        user_provisioning.expects(:create_gooddata_user).never
      end
    end
  end

  describe ".args_to_log" do
    let(:account_id) { 1 }
    let(:user_id) { 2 }

    before { GooddataUserCreateJob.stubs(:audit_id).with(3).returns(4) }

    it "logs the account and user IDs" do
      assert_equal(
        {
          account_id: account_id,
          user_id: user_id,
          audit_id: 4
        },
        GooddataUserCreateJob.args_to_log(account_id, user_id, 3)
      )
    end
  end
end
