require_relative "../../support/job_helper"

SingleCov.covered!

describe CrmZendeskUserBulkCreateJob do
  fixtures :accounts, :users

  before do
    @account = accounts(:support)
    @users = [{name: "Test User", email: "test1@example.com", roles: "Admin"}, {name: "Test User2", email: "test2@example.com", roles: "Agent"}]
    @job = CrmZendeskUserBulkCreateJob.new("42f92a34-1ac6-3dd9-21ae-929b37927225",
      account_id: @account.id,
      user_id: @account.owner_id,
      users: @users)
  end

  describe "with current user having suitable permissions" do
    it "creates a user that doesn't match any of the params" do
      @job.perform

      assert @account.users.find_by_name("Test User")
      assert @account.users.find_by_name("Test User2")
    end

    it "updates a user that matches via the params" do
      user = users(:support_agent)

      @job.options[:users] = [{'name': 'A new name', 'email': user.email}]
      @job.perform

      assert_equal 'A new name', user.reload.name
    end
  end

  describe "with current user not having suitable permissions" do
    before do
      @job.stubs(:current_user).returns(User.new)
    end

    it "does not create or update users" do
      @job.perform

      assert_nil @account.users.find_by_name("Test User")
      assert_nil @account.users.find_by_name("Test User2")
    end
  end

  describe "with organizations" do
    before do
      @organization = @account.organizations.create!(name: "Org1", domain_names: ["org.com"])
    end

    it "returns failed jobs with invalid org" do
      @job.options[:users].map! { |u| u.merge(organization_id: 123) }
      @job.perform

      assert @job.status["results"].all? do |result|
        result["error"] == "OrganizationNotFound"
      end
    end

    it "creates users when a valid org is passed" do
      @job.options[:users].map! { |u| u.merge(organization_id: @organization.id) }
      @job.perform
      new_users = @account.users.where('name LIKE ?', 'Test User%')

      assert new_users.all? do |user|
        user.organization == @organization
      end
      assert_equal 2, new_users.count
    end
  end

  it "creates user with request_brand_id if no active_brand_id is provided" do
    @job.options[:request_brand_id] = 1234

    User.any_instance.expects(:active_brand_id=).with(1234).times(@job.options[:users].count)

    @job.perform
  end
end
