require_relative '../../support/job_helper'

SingleCov.covered!

describe 'ResetEndUsersLocaleJob' do
  fixtures :accounts, :translation_locales

  let(:account) { accounts(:minimum) }
  let(:spanish) { translation_locales(:spanish) }
  let(:japanese) { translation_locales(:japanese) }

  describe '#perform' do
    before do
      account.locale_id = spanish.id
      account.save!
      account.users.agents.update_all(locale_id: ENGLISH_BY_ZENDESK.id)
      account.users.end_users.update_all(locale_id: japanese.id)
    end

    it "does not raise when it can't find account" do
      ResetEndUsersLocaleJob.work(nil)
    end

    it 'skips if not active / serviceable' do
      ResetEndUsersLocaleJob.expects(:update_end_users_locale_to_match_account).never
      ResetEndUsersLocaleJob.work(accounts(:inactive).id)
    end

    it 'updates the locale of all end users to match the account' do
      ResetEndUsersLocaleJob.work(account.id)

      end_user_count = account.users.end_users.count(:all)
      with_locale_count = account.users.end_users.where(locale_id: nil).count(:all)

      with_locale_count.must_equal end_user_count
    end

    it 'sets the updated time of all end users' do
      update_time_before = account.users.end_users.where(locale_id: japanese.id).first.updated_at

      ResetEndUsersLocaleJob.work(account.id)

      update_time_before.wont_equal account.users.end_users.where(locale_id: nil).first.updated_at
    end

    it 'does not update agents on the account' do
      ResetEndUsersLocaleJob.work(account.id)

      account.users.agents.where(locale_id: account.locale_id).count(:all).must_equal 0
    end

    it 'updates the locale_id value of users when locale is removed from account' do
      assert_equal account.users.end_users.first.locale_id, japanese.id

      ResetEndUsersLocaleJob.work(account.id)

      assert_nil account.users.end_users.first.locale_id
    end
  end
end
