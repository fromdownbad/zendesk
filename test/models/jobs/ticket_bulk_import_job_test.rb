require_relative "../../support/job_helper"

SingleCov.covered!

describe TicketBulkImportJob do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1).attributes.symbolize_keys.slice(*Api::V2::TicketsController::TICKET_PARAMETER.keys) }
  let(:job) do
    TicketBulkImportJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      tickets: [ticket, ticket])
  end

  describe "#perform" do
    describe "with valid attributes" do
      before do
        job.perform
      end

      it "imports tickets" do
        assert account.tickets.find_by_nice_id(job.status["results"][0]["id"]).presence
        assert account.tickets.find_by_nice_id(job.status["results"][1]["id"]).presence
      end
    end

    describe "with invalid attributes" do
      before do
        Ticket.any_instance.stubs(:validate!).raises(ActiveRecord::RecordInvalid, Ticket.new)
        job.perform
      end

      it "fails gracefully" do
        assert_equal "failed", job.status.status
        assert_includes job.status["results"], "index" => 0, "error" => "TicketImportFailed", "details" => "Could not save ticket"
        assert_includes job.status["results"], "index" => 1, "error" => "TicketImportFailed", "details" => "Could not save ticket"
      end
    end

    describe "with spam detector caught tickets" do
      before do
        Ticket.any_instance.stubs(:validate!).raises(Fraud::SpamDetector::SpamTicketFoundException, Ticket.new)
        job.perform
      end

      it "fails gracefully" do
        assert_equal "failed", job.status.status
        assert_includes job.status["results"], "index" => 0, "error" => "TicketImportFailed", "details" => "Could not save ticket"
        assert_includes job.status["results"], "index" => 1, "error" => "TicketImportFailed", "details" => "Could not save ticket"
      end
    end

    describe "all tasks failed" do
      before do
        Ticket.any_instance.stubs(:save).returns(false)
        job.perform
      end
      it "should return error and set status failed" do
        assert_equal "failed", job.status.status
        assert_includes job.status["results"], "index" => 0, "error" => "TicketImportFailed", "details" => "Could not save ticket"
        assert_includes job.status["results"], "index" => 1, "error" => "TicketImportFailed", "details" => "Could not save ticket"
      end
    end
  end
end
