require_relative '../../support/test_helper'
require_relative "../../support/satisfaction_test_helper"

SingleCov.covered!

describe CsatCsvJob do
  include SatisfactionTestHelper
  fixtures :all

  describe '#perform' do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_admin) }
    let(:reason_code_enabled) { false }
    let(:reason) { FactoryBot.create(:satisfaction_reason, account: account) }

    before do
      Account.any_instance.stubs(:csat_reason_code_enabled?).returns(reason_code_enabled)

      Timecop.freeze(3.minute.ago) do
        build_ticket_with_satisfaction_rating(satisfaction_comment: "=AND(2>1)")
      end

      Timecop.freeze(2.minute.ago) do
        build_ticket_with_satisfaction_rating(
          satisfaction_score: SatisfactionType.BAD,
          satisfaction_reason_code: reason.reason_code
        )
      end

      Timecop.freeze(1.minute.ago) do
        build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
        build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.OFFERED)
      end

      stub_request(:put, %r{\.amazonaws.com/data/expirable_attachments/.*-csv.zip})
    end

    it 'sends an email' do
      assert_emails_sent { CsatCsvJob.perform(account.id, user.id) }
    end

    it 'creates an expirable attachment' do
      assert_difference('ExpirableAttachment.count') do
        CsatCsvJob.perform(account.id, user.id)
      end
    end

    describe 'attachment' do
      let(:rating) { account.satisfaction_ratings.order(:created_at).last }
      let(:csv_rating) { account.satisfaction_ratings.order(:created_at).first }
      let(:filters) { {} }

      before do
        CsatCsvJob.perform(account.id, user.id, filters)
        @attachment = ExpirableAttachment.last
        Zip::InputStream.open(@attachment.public_filename) do |io|
          io.get_next_entry
          @csv_contents = io.read.split("\n")
        end
      end

      it 'has the correct content-type' do
        assert_equal 'application/zip', @attachment.content_type
      end

      it 'has the correct filename' do
        assert_equal "satisfaction-ratings-#{Time.now.strftime('%Y-%m-%d')}-csv.zip", @attachment.filename
      end

      it 'has four rows => header + content' do
        assert_equal 4, @csv_contents.count
      end

      it 'does not contain an "offered" record' do
        assert_no_match /offered/, @csv_contents[1]
      end

      describe 'when csat_reason_code is not enabled' do
        it 'has the right headers in the csv' do
          headers = %("Requester","User Id","Email","Ticket Id","Brand","Group","Assignee","Satisfaction","Comment","Survey Date")
          assert_equal headers, @csv_contents[0]
        end

        it 'has the right content in the csv' do
          content = Time.use_zone(account.time_zone) do
            [
              rating.enduser.name,
              rating.enduser.id,
              rating.enduser.email,
              rating.ticket.nice_id,
              rating.ticket.brand.name,
              rating.group.name,
              rating.agent.name,
              Api::V2::Tickets::AttributeMappings::SATISFACTION_MAP[rating.score.to_s],
              rating.comment.value,
              rating.created_at.iso8601
            ].map { |attr| "\"#{attr}\"" }.join(",")
          end

          assert_equal content, @csv_contents[1]
        end

        it 'escapes CSV macros' do
          content = Time.use_zone(account.time_zone) do
            [
              csv_rating.enduser.name,
              csv_rating.enduser.id,
              csv_rating.enduser.email,
              csv_rating.ticket.nice_id,
              csv_rating.ticket.brand.name,
              csv_rating.group.name,
              csv_rating.agent.name,
              Api::V2::Tickets::AttributeMappings::SATISFACTION_MAP[csv_rating.score.to_s],
              csv_rating.comment.value.prepend("'"), # This is the CSV macro value that should be escaped
              csv_rating.created_at.iso8601
            ].map { |attr| "\"#{attr}\"" }.join(",")
          end

          assert_equal content, @csv_contents[3]
        end

        it 'sorts by most recent first' do
          first_date  = Time.parse(CSV.parse_line(@csv_contents[1])[9])
          second_date = Time.parse(CSV.parse_line(@csv_contents[2])[9])

          assert first_date > second_date
        end
      end

      describe 'when csat_reason_code is enabled' do
        let(:reason_code_enabled) { true }
        let(:rating) { account.satisfaction_ratings.order(:created_at)[1] }

        it 'has the right headers in the csv' do
          headers = %("Requester","User Id","Email","Ticket Id","Brand","Group","Assignee","Satisfaction","Reason","Comment","Survey Date")
          assert_equal headers, @csv_contents[0]
        end

        it 'has the right content in the csv' do
          content = Time.use_zone(account.time_zone) do
            [
              rating.enduser.name,
              rating.enduser.id,
              rating.enduser.email,
              rating.ticket.nice_id,
              rating.ticket.brand.name,
              rating.group.name,
              rating.agent.name,
              Api::V2::Tickets::AttributeMappings::SATISFACTION_MAP[rating.score.to_s],
              rating.reason.translated_value,
              rating.comment.value,
              rating.created_at.iso8601
            ].map { |attr| "\"#{attr}\"" }.join(",")
          end

          assert_equal content, @csv_contents[2]
        end

        it 'sorts by most recent first' do
          first_date  = Time.parse(CSV.parse_line(@csv_contents[1])[10])
          second_date = Time.parse(CSV.parse_line(@csv_contents[2])[10])

          assert first_date > second_date
        end

        describe 'with reason_code filter' do
          describe 'when ratings match filter' do
            let(:filters) { { 'reason_code' => [reason.reason_code] } }

            it 'includes the ratings' do
              assert_equal 2, @csv_contents.count
            end
          end

          describe 'when ratings do not match filter' do
            let(:filters) { { 'reason_code' => [Satisfaction::Reason::OTHER] } }

            it 'excludes the ratings' do
              assert_equal 1, @csv_contents.count
            end
          end
        end
      end

      describe 'with score filter' do
        describe 'when ratings match filter' do
          let(:filters) { { 'score' => 'good' } }

          it 'includes the ratings' do
            assert_equal 3, @csv_contents.count
          end
        end

        describe 'when ratings do not match filter' do
          let(:filters) { { 'score' => 'good_without_comment' } }

          it 'excludes the ratings' do
            assert_equal 1, @csv_contents.count
          end
        end
      end

      describe 'with start time filter' do
        describe 'when ratings match filter' do
          let(:filters) { { 'start_time' => 1.hour.ago } }

          it 'includes the ratings' do
            assert_equal 4, @csv_contents.count
          end
        end

        describe 'when ratings do not match filter' do
          let(:filters) { { 'start_time' => 1.hour.from_now } }

          it 'excludes the ratings' do
            assert_equal 1, @csv_contents.count
          end
        end
      end

      describe 'with end time filter' do
        describe 'when ratings match filter' do
          let(:filters) { { 'end_time' => 1.hour.from_now } }

          it 'includes the ratings' do
            assert_equal 4, @csv_contents.count
          end
        end

        describe 'when ratings do not match filter' do
          let(:filters) { { 'end_time' => 1.hour.ago } }

          it 'excludes the ratings' do
            assert_equal 1, @csv_contents.count
          end
        end
      end
    end

    describe 'with inconsistent ratings' do
      before do
        ticket = build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
        ticket.satisfaction_rating.update_column(:ticket_id, nil)

        ticket = build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
        ticket.satisfaction_rating.update_column(:comment_event_id, nil)

        ticket = build_ticket_with_satisfaction_rating(satisfaction_score: SatisfactionType.GOOD)
        ticket.update_column(:group_id, nil)
        ticket.update_column(:requester_id, 0)
        ticket.update_column(:submitter_id, 0)
        ticket.update_column(:assignee_id, 0)
        ticket.update_column(:brand_id, 0)
      end

      it 'does not break' do
        CsatCsvJob.perform(account.id, user.id)
      end
    end

    describe 'resque' do
      it 'is enqueued in the medium priority queue' do
        assert_equal :medium, Resque.queue_from_class(CsatCsvJob)
      end

      it 'is throttled to run every 10 minutes' do
        assert_equal 10.minutes, CsatCsvJob.settings[:can_run_every]
      end

      it 'specifies a scoped throttle key' do
        expected_key = "CsatCsvJob:account_id:#{account.id}:requester_id:#{user.id}"
        assert_equal expected_key, CsatCsvJob.key(account.id, user.id)
      end
    end
  end
end
