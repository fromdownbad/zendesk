require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 3

describe GooddataIntegrationsDestroyJob do
  fixtures :accounts

  let(:account)                  { accounts(:minimum) }
  let(:integration_provisioning) { stub('integration_provisioning') }
  let(:gooddata_integration) do
    account.create_gooddata_integration(
      project_id: 'my_project'
    )
  end

  describe ".work" do
    before do
      Zendesk::Gooddata::IntegrationProvisioning.stubs(:new).
        with(account, gooddata_integration: gooddata_integration).
        returns(integration_provisioning)
    end

    describe "when an integration exists on the account" do
      describe "doing the destroy" do
        describe "with an existing integration" do
          before do
            GooddataIntegrationsDestroyJob.work(account.id, gooddata_integration.id)
          end

          before_should "destroy the integration" do
            integration_provisioning.expects(:destroy_gooddata_integration)
          end
        end

        describe "with a soft deleted integration" do
          before do
            gooddata_integration.soft_delete
            GooddataIntegrationsDestroyJob.work(account.id, gooddata_integration.id)
          end

          before_should "destroy the integration" do
            integration_provisioning.expects(:destroy_gooddata_integration)
          end
        end
      end

      describe "logging stats" do
        before do
          integration_provisioning.stubs(:destroy_gooddata_integration)
          GooddataIntegrationsDestroyJob.work(account.id, gooddata_integration.id)
        end

        before_should "log the run" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')
        end

        before_should "log the running time" do
          Zendesk::StatsD::Client.any_instance.expects('histogram').
            with('execution_time', kind_of(Float))
        end
      end
    end

    describe "when an error occurs" do
      before do
        integration_provisioning.expects(:destroy_gooddata_integration).raises(RuntimeError)
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')
      end

      it "logs the failure occurence to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed')

        assert_raises RuntimeError do
          GooddataIntegrationsDestroyJob.work(account.id, gooddata_integration.id)
        end
      end
    end
  end

  describe ".args_to_log" do
    before_should "include account and project ID" do
      GooddataIntegrationsDestroyJob.expects(:audit_id).with(3).returns(4)
    end

    before do
      expected = { account_id: account.id, gooddata_integration_id: gooddata_integration.id, audit_id: 4 }
      assert_equal expected, GooddataIntegrationsDestroyJob.args_to_log(account.id, gooddata_integration.id, 3)
    end
  end
end
