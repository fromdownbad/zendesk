require_relative "../../support/job_helper"

SingleCov.covered!

describe UserBatchUpdateJob do
  include DomainEventsHelper
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }
  let(:span) { stub(set_tag: {}, finish: {}) }

  before do
    @account = FactoryBot.create(:account)
    @user1 = FactoryBot.create(:user, account: @account, name: 'User 1', external_id: 1, role: :end_user, tags: "u_tag1, utag_2", alias: "user1")
    @user2 = FactoryBot.create(:user, account: @account, name: 'User 2', external_id: 2, role: :end_user, tags: "u_tag3, utag_4", alias: "user2")
    @organization1 = FactoryBot.create(:organization, account: @account, name: 'Organization 1')
    @organization2 = FactoryBot.create(:organization, account: @account, name: 'Organization 2')

    @success_results = [
      { "id" => @user1.id, "action" => "update", "status" => "Updated", "success" => true },
      { "id" => @user2.id, "action" => "update", "status" => "Updated", "success" => true },
    ]
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  describe "updating users" do
    before do
      ZendeskAPM.stubs(:trace).returns(span)
      @job = UserBatchUpdateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
        account_id: @account.id,
        user_id: @account.owner_id,
        users: [
          { id: @user1.id, name: "New Name 1", organization_id: @organization1.id },
          { id: @user2.id, name: "New Name 2", organization_id: @organization2.id }
        ])
    end

    describe 'instrumentation' do
      it 'sets correct APM facet tags' do
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :update)
        span.expects(:set_tag).with('zendesk.status', 'Updated')
        @job.perform
      end
    end

    describe "with current user having suitable permissions" do
      it "updates the users" do
        @job.perform
        assert_equal @success_results, @job.status["results"]
        assert_equal @organization1.id, @user1.reload.organization.id
        assert_equal 'New Name 1', @user1.name
        assert_equal @organization2.id, @user2.reload.organization.id
        assert_equal 'New Name 2', @user2.name
      end

      it "emit correct user domain events" do
        @job.perform
        assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_created).size
        assert_equal 2, decode_user_events(domain_event_publisher.events, :user_organization_added).size
        assert_equal 2, decode_user_events(domain_event_publisher.events, :user_name_changed).size
      end

      describe "when adding or editing tags" do
        before do
          @account.settings.has_user_tags = true
          @account.save!
          @job.options[:users] = [
            { id: @user1.id, tags: 'moose, goose' },
            { id: @user2.id, tags: 'abc, def' }
          ]
        end

        it "updates tags" do
          @job.perform
          assert_equal @success_results, @job.status["results"]
          assert_equal ["goose", "moose"], @user1.reload.tags.sort
          assert_equal ["abc", "def"], @user2.reload.tags.sort
        end
      end

      describe "when referencing by external id" do
        before do
          @job.options[:users] = [
            { external_id: @user1.external_id, name: "New Name 1", organization_id: @organization1.id },
            { external_id: @user2.external_id, name: "New Name 2", organization_id: @organization2.id }
          ]
        end

        it "updates the users" do
          @job.perform
          assert_equal @success_results, @job.status["results"]
          assert_equal @organization1.id, @user1.reload.organization.id
          assert_equal 'New Name 1', @user1.name
          assert_equal @organization2.id, @user2.reload.organization.id
          assert_equal 'New Name 2', @user2.name
        end

        it "emit correct user domain events" do
          @job.perform
          assert_equal 2, decode_user_events(domain_event_publisher.events, :user_organization_added).size
          assert_equal 2, decode_user_events(domain_event_publisher.events, :user_name_changed).size
        end
      end

      describe "when referencing by external id in a different case" do
        let(:user3) { FactoryBot.create(:user, account: @account, name: 'User 3', external_id: 'external_id_1', role: :end_user) }
        let(:user4) { FactoryBot.create(:user, account: @account, name: 'User 4', external_id: 'EXTERNAL_ID_2', role: :end_user) }

        before do
          @job.options[:users] = [
            { external_id: user3.external_id.swapcase, name: "New Name 3" },
            { external_id: user4.external_id.swapcase, name: "New Name 4" }
          ]
        end

        it "updates the users despite different case" do
          success_results = [
            { "id" => user3.id, "action" => "update", "status" => "Updated", "success" => true },
            { "id" => user4.id, "action" => "update", "status" => "Updated", "success" => true },
          ]
          @job.perform
          assert_equal success_results, @job.status["results"]
          assert_equal 'New Name 3', user3.reload.name
          assert_equal 'New Name 4', user4.reload.name
          assert_equal 'EXTERNAL_ID_1', user3.external_id
          assert_equal 'external_id_2', user4.external_id
        end
      end

      describe "when id or external_id not present" do
        before do
          @job.options[:users] = [{ name: "New Name 1" }, { name: "New Name 2" }]
        end

        it "sets correct APM facet tags" do
          span.expects(:set_tag).with('zendesk.id', any_parameters).never
          span.expects(:set_tag).with('zendesk.external_id', any_parameters).never
          span.expects(:set_tag).with('zendesk.action', :update)
          span.expects(:set_tag).with('zendesk.status', 'Failed')
          span.expects(:set_tag).with('zendesk.error', 'id or external_id required')
          span.expects(:set_tag).with('zendesk.details', any_parameters).never
          @job.perform
        end
      end

      describe "with empty name" do
        let(:user5) { FactoryBot.create(:user, account: @account, name: 'User 3', external_id: 'external_id_3', role: :end_user) }
        before do
          @job.options[:users] = [{ external_id: user5.external_id, name: "" }]
        end

        it "sets correct APM facet tags" do
          span.expects(:set_tag).with('zendesk.id', any_parameters).never
          span.expects(:set_tag).with('zendesk.external_id', user5.external_id)
          span.expects(:set_tag).with('zendesk.action', :update)
          span.expects(:set_tag).with('zendesk.status', 'Failed')
          span.expects(:set_tag).with('zendesk.error', 'UserUpdateError')
          span.expects(:set_tag).with('zendesk.details', any_parameters)
          @job.perform
        end
      end

      describe "when passing in a default group id" do
        before do
          @user3 = FactoryBot.create(:user, id: 45000, account: @account, name: 'User 3', role: :agent)
          @user4 = FactoryBot.create(:user, id: 45001, account: @account, name: 'User 3', role: :agent)
          @group1 = FactoryBot.create(:group, id: 45000, account: @account, name: 'Group 1')
          @group2 = FactoryBot.create(:group, id: 45001, account: @account, name: 'Group 2')

          @user3.groups << [@group1, @group2]
          @user4.groups << [@group1, @group2]

          @user3.save!
          @user4.save!

          @job.options[:users] = [
            { id: @user3.id, default_group_id: @group1.id },
            { id: @user4.id, default_group_id: @group1.id }
          ]
        end

        it "updates the default groups" do
          @job.perform
          assert_equal @group1.id, @user3.reload.default_group_id
          assert_equal @group1.id, @user4.reload.default_group_id
        end

        describe "with an invalid organization id" do
          before do
            @job.options[:users].map! { |u| u.merge(organization_id: 123345667) }
            @job.options[:users] << { id: @user1.id, organization_id: @organization1.id }
          end

          it "returns errors" do
            @job.perform

            assert_includes @job.status["results"], "index" => 0, "id" => @user3.id, "error" => "OrganizationNotFound"
            assert_includes @job.status["results"], "index" => 1, "id" => @user4.id, "error" => "OrganizationNotFound"
            assert_includes @job.status["results"], "action" => "update", "status" => "Updated", "success" => true, "id" => @user1.id
          end

          it 'sets correct APM facet tags' do
            span.expects(:set_tag).with('zendesk.id', any_parameters)
            span.expects(:set_tag).with('zendesk.action', :update)
            span.expects(:set_tag).with('zendesk.status', 'Failed')
            span.expects(:set_tag).with('zendesk.error', 'OrganizationNotFound')
            span.expects(:set_tag).with('zendesk.details', any_parameters).never
            @job.perform
          end
        end
      end

      describe "with user domain events" do
        describe "when adding or removing tags" do
          before do
            @account.settings.has_user_tags = true
            @account.save!
            @job.options[:users] = [
              { id: @user1.id, tags: 'tag1, tag2' },
              { id: @user2.id, tags: 'tag3, tag4' }
            ]
            @job.perform
          end

          it "emit correct tags_changed events" do
            user_tags_changed_events = decode_user_events(domain_event_publisher.events, :tags_changed)
            assert_equal 2, user_tags_changed_events.length, 2
            assert_equal ["u_tag1", "utag_2"], user_tags_changed_events[0].tags_changed.tags_removed.tags
            assert_equal ["tag1", "tag2"], user_tags_changed_events[0].tags_changed.tags_added.tags
            assert_equal ["u_tag3", "utag_4"], user_tags_changed_events[1].tags_changed.tags_removed.tags
            assert_equal ["tag3", "tag4"], user_tags_changed_events[1].tags_changed.tags_added.tags
          end
        end

        describe "when update users' alias" do
          before do
            @job.options[:users] = [
              { id: @user1.id, alias: "alias 1" },
              { id: @user2.id, alias: "alias 2" }
            ]
            @job.perform
          end

          it "emit correct user_alias_changed events" do
            user_alias_changed_events = decode_user_events(domain_event_publisher.events, :user_alias_changed)
            assert_equal 2, user_alias_changed_events.length
            assert_equal "alias 1", user_alias_changed_events[0].user_alias_changed.current.value
            assert_equal "user1", user_alias_changed_events[0].user_alias_changed.previous.value
            assert_equal "alias 2", user_alias_changed_events[1].user_alias_changed.current.value
            assert_equal "user2", user_alias_changed_events[1].user_alias_changed.previous.value
          end
        end

        describe "when update users' inferred data" do
          before do
            @job.options[:users] = [
              { id: @user1.id, notes: "my notes 1", name: "user 1 name", details: "my details 1", external_id: "user1" },
              { id: @user2.id, notes: "my notes 2", name: "user 2 name", details: "my details 2", external_id: "user2" },
            ]
            @job.perform
          end

          it "emit correct inferred events" do
            assert_equal 2, decode_user_events(domain_event_publisher.events, :user_name_changed).size
            assert_equal 2, decode_user_events(domain_event_publisher.events, :user_notes_changed).size
            assert_equal 2, decode_user_events(domain_event_publisher.events, :user_details_changed).size
            assert_equal 2, decode_user_events(domain_event_publisher.events, :external_id_changed).size
          end
        end
      end
    end

    describe "with current user not having suitable permissions" do
      before do
        @job.stubs(:current_user).returns(User.new)
      end

      it "does not update users and returns errors" do
        @job.perform
        assert_nil @user1.reload.organization
        assert_nil @user2.reload.organization
        assert_includes @job.status["results"], "index" => 0, "id" => @user1.id, "error" => "PermissionDenied"
        assert_includes @job.status["results"], "index" => 1, "id" => @user2.id, "error" => "PermissionDenied"
      end

      it "does not emits user domain events" do
        @job.perform
        assert_equal 0, decode_user_events(domain_event_publisher.events).size
      end

      it 'sets correct APM facet tags' do
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :update)
        span.expects(:set_tag).with('zendesk.status', 'Failed')
        span.expects(:set_tag).with('zendesk.error', 'PermissionDenied')
        span.expects(:set_tag).with('zendesk.details', any_parameters).never
        @job.perform
      end
    end
  end
end
