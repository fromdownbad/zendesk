require_relative "../../support/job_helper"
require_relative "../../support/attachment_test_helper"

SingleCov.covered! uncovered: 17

describe ViewCsvJob do
  fixtures :tickets

  def stub_s3_upload
    stub_request(:put, %r{\.amazonaws.com/data/expirable_attachments/.*csv.zip})
  end

  fixtures :accounts, :account_property_sets, :users, :user_identities, :organizations, :subscriptions, :rules, :ticket_fields, :translation_locales

  let(:user) { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }
  let(:view) { account.views.first }

  before do
    Zendesk::Rules::RuleExecuter.
      any_instance.
      stubs(:find_tickets_with_occam_client).
      returns([tickets(:minimum_1)])
  end

  describe ".perform" do
    it "creates an expirable_attachment and send an email containing a csv file of all the tickets that satisfy the view" do
      stub_s3_upload
      assert_difference("ExpirableAttachment.count") do
        assert_emails_sent { ViewCsvJob.perform(account.id, user.id, view.id) }
        attachment = ExpirableAttachment.last
        assert_includes attachment.filename, PermalinkFu.escape(view.title)
        assert_includes attachment.content_type, "application/zip"
      end
    end

    it "raises an exception if the requester or rule was not found" do
      assert_raise(ActiveRecord::RecordNotFound) { ViewCsvJob.perform(account.id, nil, view.id) }
      assert_raise(ActiveRecord::RecordNotFound) { ViewCsvJob.perform(account.id, user.id, nil) }
    end

    it "includes status as a column when include_status is true" do
      refute view.output.columns.include?('Status')

      csv_file = Tempfile.new(["name", '.csv'])
      ViewCsvJob.send(:build_csv_file, csv_file, user, view, default_columns: [:status])
      content = CSV.read(csv_file)
      columns = content.shift
      assert_includes columns, 'Status'
    end

    describe "email localization" do
      before do
        user.translation_locale = translation_locales(:brazilian_portuguese)
        user.save!

        stub_s3_upload
      end

      it "uses locale of requester" do
        assert_emails_sent { ViewCsvJob.perform(account.id, user.id, view.id) }
        email = ActionMailer::Base.deliveries.last
        assert_equal "pt-BR_subject #{view.title}", email.subject
        assert_includes email.joined_bodies, 'pt-BR_message_1'
        assert_includes email.joined_bodies, 'pt-BR_message_2'
      end

      it "translate views" do
        view.update_attribute(:title, '{{zd.your_unsolved_tickets}}')
        translated_title = Zendesk::DynamicContent::SystemContent.cache(user.translation_locale)['your_unsolved_tickets']

        assert_emails_sent { ViewCsvJob.perform(account.id, user.id, view.id) }

        email = ActionMailer::Base.deliveries.last
        assert_equal "pt-BR_subject #{translated_title}", email.subject
        assert_includes email.joined_bodies, 'pt-BR_message_1'
        assert_includes email.joined_bodies, 'pt-BR_message_2'
      end
    end

    it 'escapes macro strings' do
      macro_characters = ["=", "+", "-", "@"]
      tickets = []

      macro_characters.each do |char|
        t = tickets(:minimum_1).dup
        t.subject = "#{char}AND(2>1)"
        t.nice_id = nil # Clear the nice_id so we don't overlap
        t.will_be_saved_by(user)
        tickets << t if t.save(validate: false)
      end

      Zendesk::Rules::RuleExecuter.
        any_instance.
        stubs(:find_tickets_with_occam_client).
        returns(tickets)

      stub_s3_upload
      ViewCsvJob.perform(account.id, user.id, view.id)

      attachment = ExpirableAttachment.last

      Zip::InputStream.open(attachment.public_filename) do |io|
        io.get_next_entry
        contents = io.read.split("\n")

        tickets.each_with_index do |t, i|
          exported_ticket_subject = contents[i + 1].split(',').third
          assert_equal %("'#{t.subject}"), exported_ticket_subject
        end
      end
    end

    it 'renders liquid in ticket values' do
      t = tickets(:minimum_1)
      t.subject = "Liquid goes here {{ticket.requester}}"
      t.will_be_saved_by(user)
      t.save

      view.stubs(:find_tickets).returns([t])

      stub_s3_upload
      ViewCsvJob.perform(account.id, user.id, view.id)

      attachment = ExpirableAttachment.last

      Zip::InputStream.open(attachment.public_filename) do |io|
        io.get_next_entry
        contents = io.read.split("\n")

        assert_equal %("Liquid goes here #{t.requester}"), contents[-1].split(',').third
      end
    end

    it 'renders dynamic content in ticket values' do
      t = tickets(:minimum_1)
      t.subject = "DC goes here {{dc.title}}"
      t.will_be_saved_by(user)
      t.save

      Cms::Text.create do |cms|
        cms.name = "title"
        cms.fallback_attributes = {
          is_fallback: true,
          nested: true,
          translation_locale_id: ENGLISH_BY_ZENDESK.id,
          value: "title"
        }
        cms.account = account
      end

      view.stubs(:find_tickets).returns([t])

      stub_s3_upload
      ViewCsvJob.perform(account.id, user.id, view.id)

      attachment = ExpirableAttachment.last

      Zip::InputStream.open(attachment.public_filename) do |io|
        io.get_next_entry
        contents = io.read.split("\n")

        assert_equal %("DC goes here title"), contents[-1].split(',').third
      end
    end
  end

  describe ".identifier" do
    it "returns a identifier consisting of the account id use by resque-throttle" do
      assert_equal "#{account.id}-#{user.id}-#{view.id}", ViewCsvJob.identifier(account.id, user.id, view.id, foo: :bar)
    end
  end

  describe ".available_for?" do
    it "depends on the subscription" do
      user.account.subscription.expects(:has_view_csv_export?).returns(true)
      assert ViewCsvJob.available_for?(user.account)
    end
  end

  describe ".latest" do
    it "returns the latest expirable attachment that corresponds to a view_csv_job" do
      attachment = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", account, created_via: ViewCsvJob.name)
      assert_equal attachment, ViewCsvJob.latest(account)
    end
  end
end
