require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 3

describe 'RecoverFalsePositiveSuspendedTicketJob' do
  fixtures :all
  describe 'work' do
    before :all do
      @susp_ticket = suspended_tickets(:minimum_unknown_author)
      @susp_ticket.cause = SuspensionType.MALICIOUS_PATTERN_DETECTED
      @susp_ticket.save!
      @job = RecoverFalsePositiveSuspendedTicketJob.new(shard_ids: [1], start_time: @susp_ticket.created_at - 60, options: {skip_spam_check: false})
    end

    it "will recover an errantly suspended ticket" do
      @job.options[:run_id] = "0001"
      SuspendedTicket.any_instance.expects(:recover).once
      @job.work
    end

    it "will not run spam detection if skip_spam_check option is true" do
      @job.options[:skip_spam_check] = true
      Fraud::SpamDetector.any_instance.expects(:suspend_ticket?).never
      SuspendedTicket.any_instance.expects(:recover).once
      @job.work
    end

    it "won't recover tickets which were not suspended by ATSD" do
      @susp_ticket.cause = SuspensionType.SPAM
      @susp_ticket.save!
      SuspendedTicket.any_instance.expects(:recover).never
      @job.work
    end

    it "won't recover tickets on a different shard" do
      skip "Switching shards is not currently working"
      @susp_ticket.account.update_attribute(:shard_id, 2)
      SuspendedTicket.any_instance.expects(:recover).never
      @job.work
    end

    it "will raise an exception when no start_time is given" do
      @job.start_time = nil
      assert_raise RecoverFalsePositiveSuspendedTicketJob::RecoverFalsePositiveSuspendedTicketJobException do
        @job.work
      end
    end

    it "will raise an exception when no shard_ids are given" do
      @job.shard_ids = []
      assert_raise RecoverFalsePositiveSuspendedTicketJob::RecoverFalsePositiveSuspendedTicketJobException do
        @job.work
      end
    end

    it "will not recover tickets from earlier than provided start_time" do
      @job.start_time = 1.days.ago
      SuspendedTicket.any_instance.expects(:recover).never
      @job.work
    end

    it "will not recover tickets from later than provided end_time" do
      @job.options[:end_time] = @susp_ticket.created_at - 60
      SuspendedTicket.any_instance.expects(:recover).never
      @job.work
    end

    it "will recover tickets from within a bounded start/end range" do
      @job.options[:end_time] = @susp_ticket.created_at + 60
      @job.start_time = @susp_ticket.created_at - 60
      SuspendedTicket.any_instance.expects(:recover).once
      @job.work
    end

    it "will recover tickets with the specified via type" do
      @susp_ticket.via_id = ViaType.MAIL
      @job.options[:via_id] = ViaType.MAIL
      SuspendedTicket.any_instance.expects(:recover).once
      @job.work
    end

    it "will not recover tickets which don't match the specified via type" do
      @susp_ticket.via_id = ViaType.MAIL
      @job.options[:via_id] = ViaType.WEB_WIDGET
      SuspendedTicket.any_instance.expects(:recover).never
      @job.work
    end
  end
end
