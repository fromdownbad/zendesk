require_relative "../../support/job_helper"

SingleCov.covered!

describe UpdateAppsFeatureJob do
  let(:account) { accounts(:minimum) }
  let(:app_market_client) { mock }

  before do
    app_market_client.stubs(:feature_change)
    Zendesk::AppMarketClient.stubs(:new).returns(app_market_client)
  end

  describe '#work' do
    subject { UpdateAppsFeatureJob.work(account.id, feature, old_value, new_value) }
    let(:feature) { 'private_apps' }
    let(:old_value) { false }
    let(:new_value) { true }

    describe 'when account id is invalid' do
      it 'does not raise AccountNotFound exception, but logs it' do
        ZendeskExceptions::Logger.expects(:record).never
        Rails.logger.expects(:error).twice

        UpdateAppsFeatureJob.work(nil, feature, old_value, new_value)
        UpdateAppsFeatureJob.work('thisisnotavalidIDever', feature, old_value, new_value)
      end
    end

    describe 'when the job runs successfully' do
      it 'initializes AppMarketClient with account subdomain' do
        Zendesk::AppMarketClient.expects(:new).with(account.subdomain).returns(app_market_client)
        subject
      end

      it 'sends feature change to API' do
        app_market_client.expects(:feature_change).with(feature, old_value, new_value)
        subject
      end
    end

    describe 'when an error occurs' do
      let(:rails_logger) { mock }
      let(:error) { ZendeskAPI::Error::NetworkError.new({}) }
      let(:params) do
        {
          account_id: account.id,
          feature: feature,
          old_value: old_value,
          new_value: new_value,
        }
      end
      let(:error_message) do
        JSON.dump(
          subdomain: account.subdomain,
          params: params,
          error_class: error.class.name,
          error_message: error.message
        )
      end

      before do
        rails_logger.stubs(:error)
        Rails.stubs(:logger).returns(rails_logger)
        error.stubs(:backtrace).returns(['backtract first line', 'backtrace second line'])
        app_market_client.stubs(:feature_change).raises(error)
      end

      describe 'on a non final attempt' do
        before do
          UpdateAppsFeatureJob.stubs(:retry_limit).returns(10000)
        end

        it 'raises but does not log the error' do
          rails_logger.expects(:error).never

          assert_raises ZendeskAPI::Error::NetworkError do
            subject
          end
        end

        it 'raises but does not report the error' do
          ZendeskExceptions::Logger.expects(:record).never

          assert_raises ZendeskAPI::Error::NetworkError do
            subject
          end
        end
      end

      describe 'on the final retry' do
        before do
          UpdateAppsFeatureJob.stubs(:retry_limit).returns(0)
        end

        it 'raises and logs the error' do
          rails_logger.expects(:error).with(
            "Failed to update apps feature. Message #{error_message}.\nBacktrace: #{error.backtrace.join("\n")}"
          )

          assert_raises ZendeskAPI::Error::NetworkError do
            subject
          end
        end

        it 'raises and reports the error' do
          ZendeskExceptions::Logger.expects(:record).with do |exception, params|
            assert_equal(error, exception)
            assert_includes(error_message, params[:message])
          end

          assert_raises ZendeskAPI::Error::NetworkError do
            subject
          end
        end
      end
    end
  end
end
