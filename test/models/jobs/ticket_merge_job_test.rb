require_relative "../../support/job_helper"

SingleCov.covered!

describe TicketMergeJob do
  fixtures :accounts, :users, :tickets

  def should_not_call_merge
    Users::Merge.expects(:new).never
    Users::Merge.any_instance.expects(:merge!).never
  end

  let(:user) { users(:minimum_agent) }
  let(:sources) { [tickets(:minimum_1)] }
  let(:source_ids) { sources.map(&:nice_id) }
  let(:source) { sources.first }
  let(:target) { tickets(:minimum_3) }

  before do
    @ticket_merger = stub('Tickets::Merger')
    Zendesk::Tickets::Merger.expects(:new).with(user: user).returns(@ticket_merger)

    @job = TicketMergeJob.new(
      'uuid',
      account_id: user.account_id,
      user_id: user.id,
      target: target.nice_id,
      sources: source_ids,
      target_comment: 'target_comment',
      source_comment: 'source_comment',
      target_is_public: true,
      source_is_public: false,
      target_href: 'target_href',
      source_href: 'source_href_1'
    )
  end

  describe "successful merge" do
    before do
      @ticket_merger.expects(:merge).with(
        target: target,
        sources: sources,
        target_comment: 'target_comment',
        source_comment: 'source_comment',
        target_is_public: true,
        source_is_public: false
      )
    end

    it "merges" do
      results = @job.perform

      assert_equal('completed', results[2]['status'])
      assert_not_nil(results[2]['message'])
    end

    it "updates the job status with success message" do
      results = @job.perform

      message = I18n.t('txt.controllers.tickets.merge_controller.ticket_has_been_merged_into_ticket', id_of_ticket_merged: source.nice_id, ticket_title_merged: "<a class='title' href='source_href_1'>#{source.title}</a>", id_main_ticket: target.nice_id, main_ticket_title: "<a class='title' href='target_href'>#{target.title}</a>")
      assert_equal(message, results[3][:results].first[:message])
      assert(results[3][:results].first[:success])
    end
  end

  describe "successful bulk merge" do
    let(:sources) { [tickets(:minimum_1), tickets(:minimum_2)] }

    before do
      @ticket_merger.expects(:merge).with(
        target: target,
        sources: sources,
        target_comment: 'target_comment',
        source_comment: 'source_comment',
        target_is_public: true,
        source_is_public: false
      )
    end

    it "merges" do
      results = @job.perform
      assert_equal('completed', results[2]['status'])
      assert_not_nil(results[2]['message'])
    end

    it "updates the job status with success message" do
      results = @job.perform

      message = I18n.t('txt.controllers.tickets.merge_controller.tickets_have_been_merged_into', tickets_joined_by_number_symbol: source_ids.join(", #{I18n.t('txt.controllers.tickets.merge_controller.tickets_have_been_merged_into_number_symbol')}"), ticket_id: "#{target.nice_id} <a class='title' href=target_href'>#{target.title}</a>")
      assert_equal(message, results[3][:results].first[:message])
      assert(results[3][:results].first[:success])
    end
  end

  describe "merge with source in invalid source" do
    before do
      @ticket_merger.expects(:merge).raises(Zendesk::Tickets::Merger::SourceInvalid.new(source))
      source.errors.add(:base, "Some error")
    end

    it "updates the job status with error message" do
      ZendeskExceptions::Logger.expects(:record).with do |exception, params|
        params[:location] == source && params[:message] && exception.is_a?(Zendesk::Tickets::Merger::SourceInvalid)
      end

      results = @job.perform

      message = "Sorry, ticket ##{source.nice_id} <a class=\"title\" href=\"source_href_1\">#{source.title}</a> could not be merged, as:<ul><li>Some error</li></ul>"
      assert_equal(message, results[3][:results].first[:message])
      refute(results[3][:results].first[:success])
    end
  end

  describe "merge with source in invalid target" do
    before do
      @ticket_merger.expects(:merge).raises(Zendesk::Tickets::Merger::TargetInvalid.new(target, user))
      target.errors.add(:base, "Some error")
    end

    it "updates the job status with error message" do
      ZendeskExceptions::Logger.expects(:record).with do |exception, params|
        params[:location] == target && params[:message] && exception.is_a?(Zendesk::Tickets::Merger::TargetInvalid)
      end

      results = @job.perform

      message = "Sorry, the target ##{target.nice_id} <a class=\"title\" href=\"target_href\">#{target.title}</a> could not be saved as:<ul><li>Some error</li></ul>"
      assert_equal(message, results[3][:results].first[:message])
      refute(results[3][:results].first[:success])
    end
  end

  describe "merge with unexpected error" do
    before do
      @ticket_merger.expects(:merge).raises(Zendesk::Tickets::Merger::MergeFailed)
    end

    it "updates the job status with error message" do
      ZendeskExceptions::Logger.expects(:record).with do |exception, params|
        params[:location] == target && params[:message] && exception.is_a?(Zendesk::Tickets::Merger::MergeFailed)
      end

      results = @job.perform

      message = I18n.t('txt.controllers.tickets.merge_controller.problem_while_merging')
      assert_equal(message, results[3][:results].first[:message])
      refute(results[3][:results].first[:success])
    end
  end
end
