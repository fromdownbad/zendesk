require_relative "../../support/job_helper"

SingleCov.covered!

describe 'ReEncryptExternalEmailCredentialJob' do
  fixtures :accounts, :account_settings, :users, :recipient_addresses, :brands

  let(:username) { "user@gmail.z3nmail.com" }
  let(:account) { accounts(:minimum) }
  let(:unencrypted_token) { 'secret_oauth_token' }

  describe ".work" do
    subject { ReEncryptExternalEmailCredentialJob }

    it 'works an instance of the job' do
      subject.any_instance.expects(:work)
      subject.work(account.id)
    end
  end

  describe ".args_to_log" do
    let(:account_id) { 1 }

    it "logs the account ID" do
      assert_equal(
        {
          account_id: account_id
        },
        ReEncryptExternalEmailCredentialJob.args_to_log(account_id)
      )
    end
  end

  describe '#work' do
    subject { ReEncryptExternalEmailCredentialJob.new(account.id) }

    describe 'when the encryption key has not changed' do
      before do
        account.account_property_set.set_defaults
        create_external_email_credential.tap do |external_email_credential|
          external_email_credential.encrypted_value = unencrypted_token
          external_email_credential.encryption_key_name = 'EXTERNAL_EMAIL_CREDENTIAL_TEST_KEY_A'
          external_email_credential.save!
        end
      end

      it 'does not re-encrypt' do
        encrypted_value_before_job = external_email_credential.encrypted_value

        subject.work

        encrypted_value_after_job = external_email_credential.encrypted_value
        assert_equal encrypted_value_before_job, encrypted_value_after_job
      end
    end

    describe 'when the encryption key has changed' do
      before do
        account.account_property_set.set_defaults
        create_external_email_credential.tap do |external_email_credential|
          external_email_credential.encrypted_value = unencrypted_token
          external_email_credential.encryption_key_name = 'EXTERNAL_EMAIL_CREDENTIAL_TEST_KEY_B'
          external_email_credential.save!
        end
      end

      it 're-encrypts the value' do
        encrypted_value_before_re_encrypt = external_email_credential.encrypted_value

        subject.work

        encrypted_value_after_re_encrypt = external_email_credential.encrypted_value
        refute_equal encrypted_value_before_re_encrypt, encrypted_value_after_re_encrypt
      end

      it 'does not change the decrypted value' do
        decrypted_value_before_re_encrypt = external_email_credential.decrypted_refresh_token

        subject.work

        decrypted_value_after_re_encrypt = external_email_credential.decrypted_refresh_token
        assert_equal decrypted_value_before_re_encrypt, decrypted_value_after_re_encrypt
      end

      it 're-encrypts with the new key' do
        encryption_key_name_before_re_encrypt = external_email_credential.encryption_key_name

        subject.work

        encryption_key_name_after_re_encrypt = external_email_credential.encryption_key_name
        refute_equal encryption_key_name_before_re_encrypt, encryption_key_name_after_re_encrypt
      end
    end
  end

  private

  def create_external_email_credential
    account.external_email_credentials.delete_all

    ExternalEmailCredential.new(
      account: account,
      username: username,
      current_user: users(:minimum_agent)
    )
  end

  def external_email_credential
    account.external_email_credentials.where(username: username).first
  end
end
