require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'GroupDeleteJob' do
  fixtures :accounts, :users, :groups

  before do
    ActionMailer::Base.deliveries = []
    @user = users(:minimum_admin)
    @group_id = 1
  end

  describe "#perform" do
    let(:group) { groups(:minimum_group) }

    it "sets the group id to nil for all the organizations belonging to the user's account" do
      @user.account.organizations.each { |org| org.update_attribute(:group_id, 1) }
      @user.reload
      @user.account.organizations.each { |org| assert_equal 1, org.group_id }

      GroupDeleteJob.perform(@user.account_id, @user.id, @group_id)

      @user.reload
      @user.account.organizations.each { |org| assert_nil org.group_id }
    end

    it "sets the assignee_id nil for all non-closed tickets belonging to the user's account" do
      GroupDeleteJob.perform(@user.account_id, @user.id, group.id)

      @user.reload
      @user.account.tickets.not_closed.where(group_id: group.id).find_each do |ticket|
        assert_nil ticket.assignee_id
      end
    end

    it "sets the group_id to nil if more than one group exists for the account" do
      assert_equal 1, @user.reload.account.groups.length
      group = Group.new(is_active: 1, name: "test group")
      group.account = @user.account
      group.save!
      assert_equal 2, @user.reload.account.groups.length

      GroupDeleteJob.perform(@user.account_id, @user.id, group.id)

      @user.reload
      @user.account.tickets.not_closed.where(group_id: group.id).find_each do |ticket|
        assert_nil ticket.assignee_id
        assert_nil ticket.group_id
        assert_equal 2, ticket.audits.first.events.length
        assert_equal ViaType.GROUP_DELETION, ticket.audits.first.events[0].via_id
      end
    end

    it "sets the group_id to the only group_id if only one group exists for the account" do
      GroupDeleteJob.perform(@user.account_id, @user.id, group.id)

      assert_equal 1, @user.account.groups.length

      @user.reload
      @user.account.tickets.not_closed.where(group_id: group.id).find_each do |ticket|
        assert_nil ticket.assignee_id
        assert_equal @user.account.groups.first.id, ticket.group_id
      end
    end
  end
end
