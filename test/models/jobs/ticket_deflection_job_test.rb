require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe TicketDeflectionJob do
  fixtures :tickets, :accounts

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:rule_id) { 1928 }
  let(:statsd_client_stub) { stub_for_statsd }
  let(:deflector) { stub('deflector') }

  before { AnswerBot::Deflector.stubs(:new).with(account).returns(deflector) }
  describe 'when the automatic answers setting is enabled for the account' do
    before do
      Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
    end

    it 'deflects via Deflector' do
      deflector.expects(:deflect).with(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: ViaType.MAIL)

      TicketDeflectionJob.perform(account.id, ticket.id, rule_id)
    end
  end

  describe 'when automatic answers setting is disabled for the account' do
    before do
      Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(false)
    end

    it 'does not attempt deflection' do
      deflector.expects(:deflect).never

      TicketDeflectionJob.perform(account.id, ticket.id, rule_id)
    end
  end

  describe 'logging' do
    before do
      Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)

      TicketDeflectionJob.any_instance.stubs(:statsd_client).returns(statsd_client_stub)
      deflector.stubs(:deflect).with(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: ViaType.MAIL)
    end

    it 'logs the execution time of the job' do
      statsd_client_stub.expects(:histogram).with('job_execution_time', kind_of(Float))

      TicketDeflectionJob.perform(account.id, ticket.id, rule_id)
    end

    it 'when the job perform is successful, logs a resque message' do
      TicketDeflectionJob.expects(:resque_log).with("Successfully performed TicketDeflectionJob for account #{account.id} ticket #{ticket.id}")
      statsd_client_stub.expects(:histogram)

      TicketDeflectionJob.perform(account.id, ticket.id, rule_id)
    end

    it 'when the job perform fails, logs a resque error' do
      deflector.stubs(:deflect).with(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: ViaType.MAIL).raises(StandardError)
      TicketDeflectionJob.expects(:resque_error).with("Could not perform TicketDeflectionJob for account #{account.id} ticket #{ticket.id}", anything)

      TicketDeflectionJob.perform(account.id, ticket.id, rule_id)
    end

    it 'when a statsd exception is raised, logs a resque error' do
      statsd_client_stub.expects(:histogram).raises(StandardError)
      TicketDeflectionJob.expects(:resque_error).with('Could not log execution time of TicketDeflectionJob to statsd')

      TicketDeflectionJob.perform(account.id, ticket.id, rule_id)
    end
  end
end
