require_relative "../../support/job_helper"

SingleCov.covered!

describe 'ScreenrDowngradingJob' do
  fixtures :accounts, :users

  describe "Given a valid account, provided with Screenr, and a user" do
    before do
      @account = accounts(:minimum)
      @user    = users(:minimum_agent)
      screenr_tenant = @account.build_screenr_tenant(
        domain: "test",
        host: "test.screenrtest.com",
        upgrade_url: "https://screenr-stage.chargify.test/h/90248/subscriptions/new?reference=support80",
        upgraded_account: false
      )
      screenr_tenant.save!
    end

    describe "name" do
      before do
        @job = ScreenrDowngradingJob.new(
          'uuid',
          account_id: @account.id,
          user_id: @user.id,
          domain: "supertest.com"
        )
      end

      it "returns right name" do
        assert_equal "ScreenrDowngradingJob account:#{@account.id} user:#{@user.id} domain:supertest.com", @job.name
      end
    end

    describe "when all goes fine" do
      before do
        @job = ScreenrDowngradingJob.new(
          'uuid',
          account_id: @account.id,
          user_id: @user.id,
          domain: "supertest.com"
        )

        stub_request(:post, "https://api.example.com/tenants/downgrade").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(body: '{"Success":true}')
      end

      it "returns api_status => 'downgraded' " do
        @job.perform
        assert_equal "downgraded", @job.status["results"]["api_status"]
      end
    end

    describe "when something goes wrong" do
      before do
        @job = ScreenrDowngradingJob.new(
          'uuid',
          account_id: @account.id,
          user_id: @user.id,
          domain: "supertest.com"
        )

        stub_request(:post, "https://api.example.com/tenants/downgrade").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":400,"Success":false}'
          )
      end

      it "returns api_status validation_failed" do
        @job.perform
        assert_equal "validation_failed",
          @job.status["results"]["api_status"]
      end
    end
  end
end
