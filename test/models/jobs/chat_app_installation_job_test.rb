require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe ChatAppInstallationJob do
  fixtures :all

  let(:account) { accounts(:minimum) }

  before do
    # Prevent create_zopim_integration! from running the job
    ::ChatAppInstallationJob.stubs(:enqueue)
    ZopimIntegration.any_instance.stubs(:ensure_not_phase_four)
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)
  end

  it "installs the chat app" do
    ZopimIntegration.any_instance.expects(:install_app).once
    account.create_zopim_integration!(
      external_zopim_id: 1,
      zopim_key: 'zopim-key'
    )
    ChatAppInstallationJob.perform(account.id)
  end

  it "silently succeeds if an exception is raised" do
    ZopimIntegration.any_instance.stubs(:install_app).raises(StandardError)
    account.create_zopim_integration!(
      external_zopim_id: 1,
      zopim_key: 'zopim-key'
    )
    ChatAppInstallationJob.perform(account.id)
  end

  it "silently fails if zopim integration is not present" do
    ZopimIntegration.any_instance.expects(:install_app).never
    assert account.zopim_integration.blank?
    ChatAppInstallationJob.perform(account.id)
  end
end
