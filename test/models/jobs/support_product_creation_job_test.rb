require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe SupportProductCreationJob do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:job) { SupportProductCreationJob.new(account_id: account.id) }

  describe '#work' do
    describe 'when product creation succeeds' do
      it 'creates support product' do
        Zendesk::TrialActivation.expects(:create_support_product).with(account)
        job.work
      end
    end
  end
end
