require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe OrganizationBulkUpdateJob do
  fixtures :accounts, :users, :organizations
  include DomainEventsHelper

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:organization1) { organizations(:minimum_organization1) }
  let(:organization2) { organizations(:minimum_organization2) }
  let(:updated_details) { "Updated details" }
  let(:tags) { [] }
  let(:job) do
    OrganizationBulkUpdateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      ids: [organization1.id, organization2.id],
      organization: {
        details: updated_details,
        tags: tags
      },
      key: :id)
  end
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    Organization.any_instance.stubs(domain_event_publisher: domain_event_publisher)
    Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
  end

  describe "#perform" do
    describe "as a non restricted agent" do
      describe "with valid params" do
        before do
          job.options[:user_id] = agent.id
          job.options[:organization][:notes] = "Updated notes"
          job.perform
        end

        it "only updates notes" do
          assert_equal organization1.details, organization1.reload.details
          assert_equal "Updated notes", organization1.reload.notes
          assert_nil organization2.reload.details
          assert_equal "Updated notes", organization2.reload.notes
        end
      end

      describe "with valid params and no notes param" do
        before do
          job.options[:user_id] = agent.id
          job.perform
        end

        it "updates nothing" do
          assert_equal organization1.details, organization1.reload.details
          assert_equal "Example of notes for organization", organization1.reload.notes
          assert_nil organization2.reload.details
          assert_nil organization2.reload.notes
        end
      end
    end

    describe "as an admin" do
      describe "with ids" do
        before do
          job.perform
        end

        it "updates organizations" do
          assert_equal updated_details, organization1.reload.details
          assert_equal updated_details, organization2.reload.details
        end
      end

      describe "with external_ids" do
        before do
          @organization_ex1 = FactoryBot.create(:organization, account: account, external_id: 'external_1', name: 'external_1')
          @organization_ex2 = FactoryBot.create(:organization, account: account, external_id: 'external_2', name: 'external_2')
          job.options[:ids] = ['external_1', 'external_2']
          job.options[:key] = :external_id
          job.perform
        end

        it "updates organizations" do
          assert_equal updated_details, @organization_ex1.reload.details
          assert_equal updated_details, @organization_ex2.reload.details
        end
      end

      describe "with tags" do
        let(:tags) { %w[test] }

        before do
          job.perform
        end

        it "emits a tags changed event" do
          events = decoded_organization_events(domain_event_publisher.events, :tags_changed)
          assert_equal 2, events.size
          assert_equal %w[test test], events.map { |e| e.tags_changed.tags_added.tags }.flatten
        end
      end

      describe "with invalid params" do
        before do
          job.options[:ids] = [1, 2]
          job.perform
        end

        it "returns error" do
          assert_includes job.status["results"], "error" => "OrganizationNotFound", "id" => 1
          assert_includes job.status["results"], "error" => "OrganizationNotFound", "id" => 2
        end
      end
    end
  end
end
