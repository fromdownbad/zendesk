require_relative "../../support/job_helper"

SingleCov.covered!

describe "DowncaseSubdomainsJob" do
  fixtures :accounts, :routes

  describe ".work" do
    it "downcases subdomains on account and route records" do
      uppercase_subdomain = "FOOBAR"
      account = accounts(:minimum)
      account.update_column(:subdomain, uppercase_subdomain)
      route = routes(:minimum)
      route.update_column(:subdomain, uppercase_subdomain)

      DowncaseSubdomainsJob.new.work(account.id)
      lowercase_subdomain = "foobar"
      assert_equal lowercase_subdomain, account.reload.subdomain
      assert_equal lowercase_subdomain, route.reload.subdomain
    end
  end
end
