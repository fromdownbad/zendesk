require_relative "../../support/job_helper"

SingleCov.covered!

describe UserBulkUpdateJob do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:admin_user) { users(:minimum_admin) }

  before do
    ZendeskBillingCore::Zuora::Jobs::AddVoiceJob.stubs(:enqueue)
    @permission_set = build_valid_permission_set
    User.any_instance.stubs(:permission_set).returns(@permission_set)

    stub_request(:post, %r{/voice_seat_lost.json})
    FactoryBot.create(:voice_subscription, account: account, plan_type: 0, max_agents: 1)
  end

  it 'creates voice user seats' do
    UserSeat.expects(:can_create_new_voice_seats?).returns(true).times(3)
    Zendesk::SupportUsers::Entitlement::Talk.any_instance.expects(:sync).times(2)

    results = UserBulkUpdateJob.perform_now(
      account_id: account.id,
      user_ids: [user.id, admin_user.id],
      voice_seat: 'enable'
    ).status['results']

    results.must_be_empty
    user.user_seats.voice.wont_be_empty
    admin_user.user_seats.voice.wont_be_empty
  end

  it 'destroys voice user seats' do
    user.user_seats.voice.where(account_id: account.id).create
    Zendesk::SupportUsers::Entitlement::Talk.any_instance.expects(:sync)

    results = UserBulkUpdateJob.perform_now(
      account_id: account.id,
      user_ids: [user.id],
      voice_seat: 'disable'
    ).status['results']

    results.must_be_empty
    user.user_seats.voice.must_be_empty
  end

  it 'returns the error message when voice seats cannot be created' do
    UserSeat.expects(:can_create_new_voice_seats?).returns(false)

    results = UserBulkUpdateJob.perform_now(
      account_id: account.id,
      user_ids: [user.id],
      voice_seat: 'enable'
    ).status['results']

    results.wont_be_empty
  end

  it 'does not fail when there is no seat to destroy' do
    Zendesk::SupportUsers::Entitlement::Talk.any_instance.expects(:sync).never
    results = UserBulkUpdateJob.perform_now(
      account_id: account.id,
      user_ids: [user.id],
      voice_seat: 'disable'
    ).status['results']

    results.must_be_empty
  end
end
