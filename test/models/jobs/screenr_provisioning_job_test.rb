require_relative "../../support/job_helper"

SingleCov.covered!

describe 'ScreenrProvisioningJob' do
  fixtures :accounts, :users

  describe "Given a valid account and user " do
    before do
      @account = accounts(:minimum)
      @user    = users(:minimum_agent)
    end

    describe "and a valid domain name" do
      before do
        @domain = "minimum"
        @job = ScreenrProvisioningJob.new(
          'uuid',
          account_id: @account.id,
          user_id: @user.id,
          domain: @domain,
          quiet: true
        )

        stub_request(:post, "https://api.example.com/tenants").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"Domain":"minimum",
                       "Host":"minimum.sfssdev.com",
                       "UpgradedAccount":false,
                       "Recorders":{"TicketsRecorderId":"TICKETS-ID",
                                    "ForumsRecorderId":"RECORDERS-ID"}}'
          )
      end

      it "creates a new Screenr::Tenant" do
        assert_difference 'Screenr::Tenant.count(:all)' do
          @job.perform
        end
      end

      it "associates account to Screenr::Tenant" do
        @job.perform
        assert_not_nil @account.screenr_tenant
      end

      it "saves domain coming from API" do
        @job.perform
        assert_equal "minimum", @account.screenr_tenant.domain
      end
    end

    describe "and a not available domain" do
      before do
        @domain = "minimum"
        @job = ScreenrProvisioningJob.new(
          'uuid',
          account_id: @account.id,
          user_id: @user.id,
          domain: @domain,
          quiet: true
        )
        stub_request(:post, "https://api.example.com/tenants").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":409,
                      "Message":"Domain is already registered.",
                      "AvailableDomains":["minimum1","minimum2","minimum3","minimum4","minimum5"]}'
          )
      end

      it "reports a domain in use" do
        @job.perform
        assert_equal "domain_in_use",
          @job.status["results"]["api_status"]
      end

      it "gives a list of alternative domains" do
        @job.perform
        assert_equal ["minimum1", "minimum2", "minimum3", "minimum4", "minimum5"],
          @job.status["results"]["alternative_domains"]
      end
    end

    describe "and a not valid domain" do
      before do
        @domain = "søren"
        @job = ScreenrProvisioningJob.new(
          'uuid',
          account_id: @account.id,
          user_id: @user.id,
          domain: @domain,
          quiet: true
        )
        stub_request(:post, "https://api.example.com/tenants").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":400,"Message":[{"Key":"Domain","Value":"Domain must start with a letter or number and contain only letters, numbers and/or dashes"}]}'
          )
      end

      it "reports a validation failed" do
        @job.perform
        assert_equal "validation_failed",
          @job.status["results"]["api_status"]
      end

      it "reports error message" do
        @job.perform
        assert_equal "Domain must start with a letter or number and contain only letters, numbers and/or dashes",
          @job.status["results"]["message"]
      end

      it "reports error_on" do
        @job.perform
        assert_equal "domain",
          @job.status["results"]["error_on"]
      end
    end
  end
end
