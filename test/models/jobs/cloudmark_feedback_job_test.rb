require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'CloudmarkFeedbackJob' do
  fixtures :users, :accounts

  describe "Cloudmark feedback" do
    before do
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
      @user = users(:minimum_agent)
      @account = @user.account
    end

    it "dos nothing when there's no identifier" do
      CloudmarkFeedbackMailer.expects(:deliver_header_feedback).never
      CloudmarkFeedbackJob.report_ham(@user, '')
    end

    it "dos nothing when the analysis headers are missing" do
      create_remote_file("1/no_analysis_header.json",
        content_type: "application/json",
        content: '{"headers": {} }')

      CloudmarkFeedbackMailer.expects(:deliver_header_feedback).never
      CloudmarkFeedbackJob.report_ham(@user, '1/no_analysis_header.eml')
    end

    describe "reporting spam" do
      before do
        create_remote_file("1/spam.json",
          content_type: "application/json",
          content: '{"headers": {"X-CMAE-Score": ["0"], "X-CMAE-Analysis": ["some analysis"]} }')
      end

      it "does not report to Cloudmark when Cloudmark agrees it's spam" do
        create_remote_file("1/known_spam.json",
          content_type: "application/json",
          content: '{"headers": {"X-CMAE-Score": ["100"], "X-CMAE-Analysis": ["some analysis"]} }')

        CloudmarkFeedbackMailer.expects(:deliver_header_feedback).never
        CloudmarkFeedbackJob.report_spam(@user, '1/known_spam.eml')
      end

      it "mails header feedback to Cloudmark" do
        assert_difference('CloudmarkFeedbackMailer.deliveries.size') do
          CloudmarkFeedbackJob.report_spam(@user, '1/spam.eml')
        end
        mail = CloudmarkFeedbackMailer.deliveries.last

        assert_equal ['zendesk-spam-headers@feedback.cloudmark.com'], mail.to
      end

      it "provides the correct params to the mailer" do
        delivery_params = {
          'classification'  => 'spam',
          'account_id'      => @account.id,
          'user_id'         => @user.id,
          'identifier'      => '1/spam.eml',
          'analysis_header' => 'some analysis'
        }

        CloudmarkFeedbackMailer.expects(:deliver_header_feedback).with(delivery_params)
        CloudmarkFeedbackJob.report_spam(@user, '1/spam.eml')
      end
    end

    describe "reporting ham" do
      before do
        create_remote_file("1/ham.json",
          content_type: "application/json",
          content: '{"headers": {"X-CMAE-Score": ["100"], "X-CMAE-Analysis": ["some analysis"]} }')
      end

      it "does not report to Cloudmark when Cloudmark agrees it's ham" do
        create_remote_file("1/known_ham.json",
          content_type: "application/json",
          content: '{"headers": {"X-CMAE-Score": ["90"], "X-CMAE-Analysis": ["some analysis"]} }')

        CloudmarkFeedbackMailer.expects(:deliver_header_feedback).never
        CloudmarkFeedbackJob.report_ham(@user, '1/known_ham.eml')
      end

      it "mails header feedback to Cloudmark" do
        assert_difference('CloudmarkFeedbackMailer.deliveries.size') do
          CloudmarkFeedbackJob.report_ham(@user, '1/ham.eml')
        end
        mail = CloudmarkFeedbackMailer.deliveries.last

        assert_equal ['zendesk-legit-headers@feedback.cloudmark.com'], mail.to
      end

      it "provides the correct params to the mailer" do
        delivery_params = {
          'classification'  => 'legit',
          'account_id'      => @account.id,
          'user_id'         => @user.id,
          'identifier'      => '1/ham.eml',
          'analysis_header' => 'some analysis'
        }
        CloudmarkFeedbackMailer.expects(:deliver_header_feedback).with(delivery_params)
        CloudmarkFeedbackJob.report_ham(@user, '1/ham.eml')
      end
    end
  end

  def create_remote_file(identifier, params)
    default_params = {
      configuration: Zendesk::Mail.raw_remote_files.name
    }

    RemoteFiles::File.new(identifier, default_params.merge(params)).store_once!
  end
end
