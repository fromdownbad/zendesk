require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe RemoveRegularOrganizationMembershipsJob do
  fixtures :accounts, :organizations, :users, :organization_memberships

  describe "#perform" do
    let(:account) { accounts(:minimum) }
    let(:user1)   { users(:minimum_end_user) }
    let(:user2)   { users(:minimum_end_user2) }
    let(:organization1) { organizations(:minimum_organization1) }
    let(:organization2) { organizations(:minimum_organization2) }
    let(:organization3) { organizations(:minimum_organization3) }

    before do
      Account.any_instance.stubs(has_multiple_organizations_enabled?: true)

      user1.organization_memberships.create!(
        account: account,
        organization: organization2,
        default: nil
      )
      user2.update_attribute(:organization, organization3)

      assert_equal 2, account.organization_memberships.where(default: nil).count(:all)
      assert_equal 2, account.organization_memberships.where(default: true).count(:all)
    end

    it "removes all regular organization_memberships from the account" do
      RemoveRegularOrganizationMembershipsJob.perform(account.id)
      assert_equal 0, account.organization_memberships.where(default: nil).count(:all)
    end

    it "does not remove any default organization_memberships from the account" do
      RemoveRegularOrganizationMembershipsJob.perform(account.id)
      assert_equal 2, account.organization_memberships.where(default: true).count(:all)
    end

    it "unsets the organization_id on any tickets associated with the user's non-default organizations" do
      # user 1
      User.any_instance.expects(:move_tickets_to_organization).
        with(organization1.id, nil).never # default_organization
      User.any_instance.expects(:move_tickets_to_organization).
        with(organization2.id, organization1.id).once # regular

      # user 2
      User.any_instance.expects(:move_tickets_to_organization).
        with(organization3.id, nil).never # default_organization
      User.any_instance.expects(:move_tickets_to_organization).
        with(organization1.id, organization3.id).once # regular

      RemoveRegularOrganizationMembershipsJob.perform(account.id)
    end
  end
end
