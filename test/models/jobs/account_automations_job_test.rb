require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 4

describe AccountAutomationsJob do
  fixtures :accounts, :rules, :tickets

  let(:automation) { rules(:automation_close_ticket) }
  let(:account_id) { automation.account_id }
  let(:time_for_conditions) { Time.now }
  let(:dry_run) { false }

  before do
    # Scope the tests to this single automation
    Automation.where('id != ?', automation.id).without_arsi.delete_all
    Arturo.enable_feature!(:automations_background_heartbeat)
    Resque.inline = true

    stub_occam_find([])

    # heartbeat! writes to MySQL in a separate thread. Classic runs with
    # transactional_fixtures, so the other thread's MySQL connection will not
    # see the QueueAudit created in the main thread.
    Resque::Durable::BackgroundHeartbeat.any_instance.stubs(:heartbeat!)
  end

  after do
    Resque.inline = false
  end

  it 'tracks valid automations' do
    Zendesk::StatsD::Client.any_instance.stubs(:increment).at_least_once.with do |key, hash|
      @statsd_invoked = true if key == 'automations.executed' && hash[:tags] == ['valid:true']
      true
    end
    AccountAutomationsJob.expects(:resque_log).at_least_once.with do |message|
      @log_invoked = true if message =~ /Invalid automation/
      true
    end

    stub_occam_count(1)
    automation.count_tickets.must_be :>, 0, 'automation fixture has no pending tickets'

    stub_occam_count(0)

    AccountAutomationsJob.enqueue(account_id, time_for_conditions, dry_run)
    automation.count_tickets.must_equal 0, 'automation did not apply to tickets'

    @log_invoked.must_be_nil 'not expected: resque_log(/Invalid automation/)'
    @statsd_invoked.must_equal true, 'not yet invoked: statsd.increment("automations.executed")'
  end

  it 'tracks invalid automations' do
    Automation.any_instance.stubs(:valid?).returns(false)

    Zendesk::StatsD::Client.any_instance.stubs(:increment).at_least_once.with do |key, hash|
      @statsd_invoked = true if key == 'automations.executed' && hash[:tags] == ['valid:false']
      true
    end
    AccountAutomationsJob.expects(:resque_log).at_least_once.with do |message|
      @log_invoked = true if message =~ /Invalid automation/
      true
    end

    stub_occam_count(1)
    automation.count_tickets.must_be :>, 0, 'automation fixture has no pending tickets'

    stub_occam_count(0)

    AccountAutomationsJob.enqueue(account_id, time_for_conditions, dry_run)
    automation.count_tickets.must_equal 0, 'automation did not apply to tickets'

    @log_invoked.must_equal true, 'not yet invoked: resque_log(/Invalid automation/)'
    @statsd_invoked.must_equal true, 'not yet invoked: statsd.increment("automations.executed")'
  end

  describe 'with parallel_automations arturo flag' do
    before { Arturo.enable_feature!(:parallel_automations) }
    after { Arturo.disable_feature!(:parallel_automations) }

    it 'processes tickets end-to-end' do
      ZendeskExceptions::Logger.expects(:record).never

      stub_occam_count(1)
      automation.count_tickets.must_be :>, 0, 'automation fixture has no pending tickets'

      stub_occam_count(0)

      AccountAutomationsJob.enqueue(account_id, time_for_conditions, dry_run)
      automation.count_tickets.must_equal 0, 'automation did not apply to tickets'
    end

    describe 'and the parallel_automations_limit account setting has been adjusted' do
      let(:limit)         { 3 }
      let(:tickets_count) { 8 }

      before do
        account = automation.account
        account.settings.parallel_automations_limit = limit
        account.save!

        user = account.users.last
        account.tickets.clear

        tickets_to_serve_from_occam = []

        tickets_count.times do
          ticket = account.tickets.build(
            subject: 'foo',
            description: 'foo',
            assignee: user
          )
          ticket.will_be_saved_by(user)
          ticket.save!

          ticket.solve(user)

          tickets_to_serve_from_occam << ticket
        end

        # Mock occam to return tickets based on the limit set on account
        stub_occam_find(tickets_to_serve_from_occam.map(&:id)[0...limit])
        stub_occam_count(tickets_count)
      end

      it 'processes tickets up to the limit' do
        automation.count_tickets.must_equal tickets_count
        AccountAutomationsJob.enqueue(account_id, time_for_conditions, dry_run)
        stub_occam_count(tickets_count - limit)

        automation.count_tickets.must_equal tickets_count - limit
      end
    end
  end

  describe 'without parallel_automations arturo flag' do
    before { Arturo.disable_feature!(:parallel_automations) }

    it 'processes tickets end-to-end' do
      ZendeskExceptions::Logger.expects(:record).never
      stub_occam_count(1)
      automation.count_tickets.must_be :>, 0, 'automation fixture has no pending tickets'
      stub_occam_count(0)

      AccountAutomationsJob.enqueue(account_id, time_for_conditions, dry_run)
      automation.count_tickets.must_equal 0, 'automation did not apply to tickets'
    end

    describe 'with find_tickets on replica' do
      before { Arturo.enable_feature!(:automations_find_tickets_on_slave) }
      after { Arturo.disable_feature!(:automations_find_tickets_on_slave) }

      it 'processes tickets end-to-end' do
        ZendeskExceptions::Logger.expects(:record).never
        stub_occam_count(1)
        automation.count_tickets.must_be :>, 0, 'automation fixture has no pending tickets'
        stub_occam_count(0)

        AccountAutomationsJob.enqueue(account_id, time_for_conditions, dry_run)
        automation.count_tickets.must_equal 0, 'automation did not apply to tickets'
      end
    end
  end

  describe 'when executer throws an error' do
    before do
      AccountAutomationsJob.
        stubs(:execute_automation).
        raises(StandardError.new('Automations error'))

      AccountAutomationsJob.stubs(:heartbeat)
    end

    it 'records the exception' do
      ZendeskExceptions::Logger.expects(:record)

      AccountAutomationsJob.work(account_id, time_for_conditions, dry_run, 1)
    end
  end
end
