require_relative '../../support/job_helper'

SingleCov.covered!

describe 'RestoreDefaultContentJob' do
  fixtures :accounts, :permission_sets, :translation_locales, :groups, :rules, :ticket_fields, :tickets

  let(:account) { accounts(:minimum) }
  let(:spanish) { translation_locales(:spanish) }
  let(:restore_default_content) { RestoreDefaultContentJob.new(account, account.translation_locale.id, spanish.id) }

  describe ".work" do
    it "creates a new restore_default_content_job instance and call translate_content" do
      RestoreDefaultContentJob.any_instance.expects(:translate_content).once
      RestoreDefaultContentJob.work(account.id, account.translation_locale.id, spanish.id)
    end

    it "does not throw any errors while translating" do
      RestoreDefaultContentJob.work(account.id, account.translation_locale.id, spanish.id)
    end

    it "skips if not active / serviceable" do
      RestoreDefaultContentJob.any_instance.expects(:translate_content).never
      RestoreDefaultContentJob.work(accounts(:inactive).id, account.translation_locale.id, spanish.id)
    end

    describe "when the account's locale changes" do
      let(:old_locale) { account.translation_locale.id }
      let(:new_locale) { spanish.id }

      subject { RestoreDefaultContentJob.work(account.id, old_locale, new_locale) }

      describe_with_arturo_enabled :email_ccs_default_rules_content do
        describe_with_and_without_arturo_enabled :email_ccs do
          it "includes the v2 CCs/Followers triggers and fixed original triggers" do
            RestoreDefaultContentJob.stubs(:resque_log)
            RestoreDefaultContentJob.expects(:resque_log).with("Including COLLABORATION_TRIGGERS_NAMES_V2 and FIXED_TRIGGERS_NAMES and NOTIFY_REQUESTER_PROACTIVE_TRIGGER_NAME for translation").once
            subject
          end
        end
      end

      describe_with_arturo_disabled :email_ccs_default_rules_content do
        describe_with_arturo_enabled :email_ccs do
          it "includes the collaboration triggers" do
            RestoreDefaultContentJob.stubs(:resque_log)
            RestoreDefaultContentJob.expects(:resque_log).with("Including COLLABORATION_TRIGGERS_NAMES for translation").once
            subject
          end
        end
      end

      it "does not throw any errors while translating" do
        refute_equal old_locale, new_locale
        subject
      end
    end
  end

  it ".args_to_log" do
    RestoreDefaultContentJob.args_to_log(1, 1, 1)
  end

  describe "#initialize" do
    it "prioritizes the old locale" do
      RestoreDefaultContentJob.any_instance.expects(old_locale_first_in_array: account.translation_locale).once
      RestoreDefaultContentJob.new(account, account.translation_locale.id, spanish.id)
    end
  end

  it "does not raise when it can't find account" do
    RestoreDefaultContentJob.work(nil, account.translation_locale.id, spanish.id)
  end

  describe "#translate_content" do
    it "calls each translate method for each content" do
      restore_default_content.expects(:translate_roles).once
      restore_default_content.expects(:translate_views).once
      restore_default_content.expects(:translate_user_views).once
      restore_default_content.expects(:translate_macros).once
      restore_default_content.expects(:translate_triggers).once
      restore_default_content.expects(:translate_automations).once
      restore_default_content.expects(:translate_reports).once
      restore_default_content.expects(:translate_ticket_fields).once
      restore_default_content.expects(:translate_ticket_forms).once
      restore_default_content.expects(:translate_account_defaults).once
      restore_default_content.expects(:translate_groups).once
      restore_default_content.expects(:translate_sample_tickets).once

      restore_default_content.translate_content
    end

    it "raises and rescues exeception" do
      restore_default_content.expects(:translate_roles).raises(StandardError.new('Oh noes'))
      restore_default_content.class.expects(resque_log: 'Failed to translate default content for roles')
      Zendesk::StatsD::Client.any_instance.expects(:increment).with(
        'errors', tags: ['exception_class:standard_error', 'content_type:roles']
      ).once

      # Tests always raises, but we check the exception is rescued and we log
      # send metrics
      assert_raises(StandardError) { restore_default_content.translate_content }
    end
  end

  describe "#old_locale_first_in_array" do
    it "prioritizes old locale" do
      assert_equal ENGLISH_BY_ZENDESK, restore_default_content.all_locales.first
      restore_default_content.send(:old_locale_first_in_array, spanish)
      assert_equal spanish, restore_default_content.all_locales.first
    end
  end

  describe "#prioritize_locale" do
    it "does not modify the array if the locale is in index 0" do
      assert_equal ENGLISH_BY_ZENDESK, restore_default_content.all_locales.first
      restore_default_content.all_locales.expects(:unshift).never
      restore_default_content.send(:prioritize_locale, 0)
    end

    it "modifys the array to prioritize a locale" do
      assert_equal ENGLISH_BY_ZENDESK, restore_default_content.all_locales.first
      restore_default_content.send :prioritize_locale, restore_default_content.all_locales.index(spanish)
      assert_equal spanish, restore_default_content.all_locales.first
    end
  end

  describe "content translation" do
    describe "#translate_views" do
      let(:views) { account.views }
      it "calls match and translate for each key" do
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.your_unsolved.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.group_unsolved.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.new_in_groups.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.recently_solved.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.all_unsolved.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.unassigned.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.recently_updated.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.pending.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.current_tasks.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.default.views.overdue_tasks.title', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: views, main_key: 'txt.models.account.customer_satisfaction_support.default_view_name', multi: true)

        restore_default_content.send(:translate_views)
      end

      describe "multiple object matching" do
        before do
          @spanish_title = I18n.t("txt.default.views.your_unsolved.title", locale: spanish)
          base_view = rules(:view_your_unsolved_tickets)
          spanish_view = base_view.dup
          spanish_view.update_attribute(:title, @spanish_title)
          spanish_view.reload
          @views = [base_view, spanish_view]
          account.views = @views
          account.save!(validate: false)
        end

        it "translates all possible views for an account" do
          restore_default_content.send(:translate_views)

          @views.each do |view|
            view.reload.title.must_equal @spanish_title
          end
        end
      end
    end

    describe '#translate_user_views' do
      let(:user_views) { account.user_views }
      it "calls match and translate for each key" do
        restore_default_content.expects(:match_and_translate).with(objects: user_views, main_key: 'txt.default.customer_lists.last_30_days_title', multi: true)

        restore_default_content.send(:translate_user_views)
      end
    end

    describe "#translate_macros" do
      let(:macros) { account.macros }
      it "calls match and translate for each key" do
        restore_default_content.expects(:match_and_translate).with(objects: macros, main_key: 'txt.default.macros.close_and_redirect.title', action_keys: 'txt.default.macros.close_and_redirect.value', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: macros, main_key: 'txt.default.macros.downgrade_and_inform.title', action_keys: 'txt.default.macros.downgrade_and_inform.value', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: macros, main_key: 'txt.default.macros.not_responding.title', action_keys: 'txt.default.macros.not_responding.value', multi: true)
        restore_default_content.expects(:match_and_translate).with(objects: macros, main_key: 'txt.default.macros.take_it.title', action_keys: 'txt.default.macros.take_it.value', multi: true)

        restore_default_content.send(:translate_macros)
      end
    end

    describe "#translate_automations" do
      let(:automations) { account.automations }

      it "calls match and translate for each key" do
        restore_default_content.expects(:match_and_translate).with(
          objects: automations,
          main_key: 'txt.default.automations.close.title',
          action_keys: { subject: 'txt.default.automations.close.subject' },
          multi: true
        )

        restore_default_content.expects(:match_and_translate).with(
          objects: automations,
          main_key: 'txt.default.automations.pending_24_hours.title',
          action_keys: {
            subject: 'txt.default.automations.pending_24_hours.subject',
            body: 'txt.default.automations.pending_24_hours.body_v2'
          },
          multi: true
        )

        restore_default_content.expects(:match_and_translate).with(
          objects: automations,
          main_key: 'txt.default.automations.pending_5_days.title',
          action_keys: {
            subject: 'txt.default.automations.pending_5_days.subject',
            body: 'txt.default.automations.pending_5_days.body_v2'
          },
          multi: true
        )

        restore_default_content.expects(:match_and_translate).with(
          objects: automations,
          main_key: 'txt.customer_satisfaction_rating.automation_v2.title',
          action_keys: {
            subject: 'txt.customer_satisfaction_rating.automation_v2.subject',
            body: 'txt.customer_satisfaction_rating.automation_v2.body'
          },
          multi: true
        )

        restore_default_content.expects(:match_and_translate).with(
          objects: automations,
          main_key: 'txt.admin.models.account.facebook_support.close_facebook_message_ticket_title',
          action_keys: {
            subject: 'txt.admin.models.account.facebook_support.close_facebook_message_ticket_subject'
          },
          multi: true
        )

        restore_default_content.expects(:match_and_translate).with(
          objects: automations,
          main_key: 'txt.admin.models.twitter.close_twitter_ticket_after_status_solved_title',
          action_keys: {
            subject: 'txt.admin.models.twitter.close_twitter_ticket_after_status_solved_subject'
          },
          multi: true
        )

        restore_default_content.send(:translate_automations)
      end
    end

    describe "#translate_triggers" do
      let(:triggers) { account.triggers }

      [true, false].each do |help_center_enabled|
        describe "with help center #{help_center_enabled ? 'enabled' : 'disabled'}" do
          before do
            restore_default_content.account.stubs(:help_center_enabled?).returns(help_center_enabled)

            # Stub it first since the request received and request updated triggers are
            # tested separately.
            restore_default_content.stubs(:match_and_translate)
          end

          describe_with_and_without_arturo_enabled :email_ccs_default_rules_content do
            describe_with_and_without_arturo_enabled :email_ccs do
              it "calls match and translate for each key" do
                restore_default_content.expects(:match_and_translate).with(
                  objects: triggers,
                  main_key: 'txt.default.triggers.notify_requester_solved.title',
                  action_keys: {
                    subject: 'txt.default.triggers.notify_requester_solved.subject',
                    body: 'txt.default.triggers.notify_requester_solved.',
                    versions: ["body_v3"],
                    subject_versions: []
                  },
                  multi: true
                )

                restore_default_content.expects(:match_and_translate).with(
                  objects: triggers,
                  main_key: 'txt.default.triggers.notify_assignee_update.title',
                  action_keys: {
                    subject: 'txt.default.triggers.notify_assignee_update.subject',
                    body: 'txt.default.triggers.notify_assignee_update.',
                    versions: ["body_v2"],
                    subject_versions: []
                  },
                  multi: true
                )

                restore_default_content.expects(:match_and_translate).with(
                  objects: triggers,
                  main_key: 'txt.default.triggers.notify_assignee_assignment.title',
                  action_keys: {
                    subject: 'txt.default.triggers.notify_assignee_assignment.subject',
                    body: 'txt.default.triggers.notify_assignee_assignment.',
                    versions: ["body_v2"],
                    subject_versions: []
                  },
                  multi: true
                )

                restore_default_content.expects(:match_and_translate).with(
                  objects: triggers,
                  main_key: 'txt.default.triggers.notify_assignee_reopened.title',
                  action_keys: {
                    subject: 'txt.default.triggers.notify_assignee_reopened.subject',
                    body: 'txt.default.triggers.notify_assignee_reopened.',
                    versions: ["body_v2"],
                    subject_versions: []
                  },
                  multi: true
                )

                restore_default_content.expects(:match_and_translate).with(
                  objects: triggers,
                  main_key: 'txt.default.triggers.notify_group_assignment.title',
                  action_keys: {
                    subject: 'txt.default.triggers.notify_group_assignment.subject',
                    body: 'txt.default.triggers.notify_group_assignment.',
                    versions: ["body_v2"],
                    subject_versions: []
                  },
                  multi: true
                )

                restore_default_content.expects(:match_and_translate).with(
                  objects: triggers,
                  main_key: 'txt.default.triggers.notify_all_received.title',
                  action_keys: {
                    subject: 'txt.default.triggers.notify_all_received.subject',
                    body: 'txt.default.triggers.notify_all_received.',
                    versions: ["body_v2"],
                    subject_versions: []
                  },
                  multi: true
                )

                restore_default_content.expects(:match_and_translate).with(
                  objects: triggers,
                  main_key: 'txt.default.triggers.auto_assign.title',
                  action_keys: {
                    subject: 'txt.default.triggers.auto_assign.subject'
                  },
                  multi: true
                )

                restore_default_content.send(:translate_triggers)
              end
            end

            describe "when user has changed one of the trigger values" do
              before do
                @trigger = account.rules.find_by_title('Notify requester of received request')
                @trigger.definition.actions[0].value = "Overwritten by user"
                @trigger.save!
              end

              it "does not lead to errors" do
                restore_default_content.send(:translate_triggers)
              end
            end
          end

          describe_with_arturo_enabled :email_ccs_default_rules_content do
            it "includes the COLLABORATION_TRIGGERS_NAMES_V2 when calling match and translate" do
              restore_default_content.expects(:match_and_translate).with(
                objects: triggers,
                main_key: 'txt.default.triggers.notify_requester_and_ccs_of_received_request.title',
                action_keys: {
                  subject: 'txt.default.triggers.notify_requester_and_ccs_of_received_request.',
                  body: 'txt.default.triggers.notify_requester_and_ccs_of_received_request.',
                  versions: ["body_v4", "body_v3", "body_v4"],
                  subject_versions: ["subject_v2", "subject", "subject_v2"]
                },
                multi: true
              )

              restore_default_content.expects(:match_and_translate).with(
                objects: triggers,
                main_key: 'txt.default.triggers.notify_requester_and_ccs_of_update.title',
                action_keys: {
                  subject: 'txt.default.triggers.notify_requester_and_ccs_of_update.subject',
                  body: 'txt.default.triggers.notify_requester_and_ccs_of_update.',
                  versions: ["body_v4", "body_v2"],
                  subject_versions: []
                },
                multi: true
              )

              restore_default_content.send(:translate_triggers)
            end

            it "includes the NOTIFY_REQUESTER_PROACTIVE_TRIGGER_NAME when calling match_and_translate" do
              restore_default_content.expects(:match_and_translate).with(
                objects: triggers,
                main_key: 'txt.default.triggers.notify_requester_proactive_ticket.title',
                action_keys: {
                  subject: 'txt.default.triggers.notify_requester_proactive_ticket.',
                  body: 'txt.default.triggers.notify_requester_proactive_ticket.',
                  description: 'txt.default.triggers.notify_requester_proactive_ticket.description',
                  versions: ["body"],
                  subject_versions: ["subject"]
                },
                multi: true
              )

              restore_default_content.send(:translate_triggers)
            end

            it "includes the FIXED_TRIGGERS_NAMES when calling match_and_translate" do
              restore_default_content.expects(:match_and_translate).with(
                objects: triggers,
                main_key: 'txt.default.triggers.notify_requester_received.title',
                action_keys: {
                  subject: 'txt.default.triggers.notify_requester_received.',
                  body: 'txt.default.triggers.notify_requester_received.',
                  versions: ["body_v4", "body_v3", "body_v4"],
                  subject_versions: ["subject_v3", "subject_v2", "subject_v3"]
                },
                multi: true
              )

              restore_default_content.expects(:match_and_translate).with(
                objects: triggers,
                main_key: 'txt.default.triggers.notify_requester_update.title',
                action_keys: {
                  subject: 'txt.default.triggers.notify_requester_update.subject',
                  body: 'txt.default.triggers.notify_requester_update.',
                  versions: ["body_v4", "body_v2"],
                  subject_versions: []
                },
                multi: true
              )

              restore_default_content.send(:translate_triggers)
            end
          end

          describe_with_arturo_disabled :email_ccs_default_rules_content do
            it "includes the original request received/updated triggers when calling match_and_translate" do
              restore_default_content.expects(:match_and_translate).with(
                objects: triggers,
                main_key: 'txt.default.triggers.notify_requester_received.title',
                action_keys: {
                  subject: 'txt.default.triggers.notify_requester_received.',
                  body: 'txt.default.triggers.notify_requester_received.',
                  versions: ["body_v3", "body_v4"],
                  subject_versions: ["subject_v3", "subject_v2"]
                },
                multi: true
              )

              restore_default_content.expects(:match_and_translate).with(
                objects: triggers,
                main_key: 'txt.default.triggers.notify_requester_update.title',
                action_keys: {
                  subject: 'txt.default.triggers.notify_requester_update.subject',
                  body: 'txt.default.triggers.notify_requester_update.',
                  versions: ["body_v2", "body_v3"],
                  subject_versions: []
                },
                multi: true
              )

              restore_default_content.send(:translate_triggers)
            end

            describe_with_arturo_enabled :email_ccs do
              it "includes the COLLABORATION_TRIGGERS_NAMES when calling match and translate" do
                restore_default_content.expects(:match_and_translate).with(
                  objects: triggers,
                  main_key: 'txt.default.triggers.notify_requester_and_ccs_of_received_request.title',
                  action_keys: {
                    subject: 'txt.default.triggers.notify_requester_and_ccs_of_received_request.subject',
                    body: 'txt.default.triggers.notify_requester_and_ccs_of_received_request.',
                    versions: ['body'],
                    subject_versions: []
                  },
                  multi: true
                )

                restore_default_content.expects(:match_and_translate).with(
                  objects: triggers,
                  main_key: 'txt.default.triggers.notify_requester_and_ccs_of_update.title',
                  action_keys: {
                    subject: 'txt.default.triggers.notify_requester_and_ccs_of_update.subject',
                    body: 'txt.default.triggers.notify_requester_and_ccs_of_update.',
                    versions: ['body'],
                    subject_versions: []
                  },
                  multi: true
                )

                restore_default_content.send(:translate_triggers)
              end
            end
          end
        end
      end
    end

    describe "#translate_reports" do
      before { create_reports(account) }

      it "calls match and translate for each key" do
        reports = account.reports
        restore_default_content.expects(:match_and_translate).with(objects: reports, main_key: 'txt.default.reports.backlog.title', action_keys: ["backlog", "created", "solved"])
        restore_default_content.expects(:match_and_translate).with(objects: reports, main_key: 'txt.default.reports.priorities.title', action_keys: ["high_urgent", "low_normal"])
        restore_default_content.expects(:match_and_translate).with(objects: reports, main_key: 'txt.default.reports.incidents.title', action_keys: ["new", "resolved", "working"])
        restore_default_content.expects(:match_and_translate).with(objects: reports, main_key: 'txt.default.reports.high_priority.title', action_keys: ["backlog", "created", "resolved"])
        restore_default_content.expects(:match_and_translate).with(objects: reports, main_key: 'txt.default.reports.resolution.title', action_keys: ["solved_less_than_2_hours", "solved_less_than_8_hours", "solved_less_than_24_hours"])
        restore_default_content.expects(:match_and_translate).with(objects: reports, main_key: 'txt.default.reports.unsolved.title', action_keys: ["web", "email", "chat", "twitter", "phone", "feedback_tab", "forum", "facebook"])

        restore_default_content.send(:translate_reports)
      end
    end

    describe "#translate_ticket_fields" do
      it "calls match and translate for each key" do
        ticket_fields = account.ticket_fields
        restore_default_content.expects(:match_and_translate).with(objects: ticket_fields, main_key: 'txt.default.fields.subject.title', action_keys: 'txt.default.fields.subject.description')
        restore_default_content.expects(:match_and_translate).with(objects: ticket_fields, main_key: 'txt.default.fields.description.title', action_keys: 'txt.default.fields.description.description')
        restore_default_content.expects(:match_and_translate).with(objects: ticket_fields, main_key: 'txt.default.fields.status.title', action_keys: 'txt.default.fields.status.description')
        restore_default_content.expects(:match_and_translate).with(objects: ticket_fields, main_key: 'txt.default.fields.type.title', action_keys: 'txt.default.fields.type.description')
        restore_default_content.expects(:match_and_translate).with(objects: ticket_fields, main_key: 'txt.default.fields.priority.title', action_keys: 'txt.default.fields.priority.description')
        restore_default_content.expects(:match_and_translate).with(objects: ticket_fields, main_key: 'txt.default.fields.group.title', action_keys: 'txt.default.fields.group.description')
        restore_default_content.expects(:match_and_translate).with(objects: ticket_fields, main_key: 'txt.default.fields.assignee.title', action_keys: 'txt.default.fields.assignee.description')

        restore_default_content.send(:translate_ticket_fields)
      end
    end

    describe "#translate_roles" do
      before { create_permission_sets(account) }

      it "calls match and translate for each key" do
        roles = account.permission_sets

        restore_default_content.expects(:match_and_translate).with(objects: roles, main_key: 'txt.default.roles.staff.name', action_keys: 'txt.default.roles.staff.description')
        restore_default_content.expects(:match_and_translate).with(objects: roles, main_key: 'txt.default.roles.team_leader.name', action_keys: 'txt.default.roles.team_leader.description')
        restore_default_content.expects(:match_and_translate).with(objects: roles, main_key: 'txt.default.roles.advisor.name', action_keys: 'txt.default.roles.advisor.description')
        restore_default_content.expects(:match_and_translate).with(objects: roles, main_key: 'txt.default.roles.light_agent.name', action_keys: 'txt.default.roles.light_agent.description')

        restore_default_content.send(:translate_roles)
      end
    end

    describe "#translate_ticket_forms" do
      it "calls match and translate for each key" do
        set_ticket_forms(account)
        ticket_forms = account.ticket_forms

        restore_default_content.expects(:match_and_translate).with(objects: ticket_forms, main_key: 'txt.admin.model.ticket_form.default_form_name')

        restore_default_content.send(:translate_ticket_forms)
      end
    end

    describe "#translate_groups" do
      it "calls match and translate for each key" do
        groups = account.groups

        restore_default_content.expects(:match_and_translate).with(objects: groups, main_key: 'txt.default.groups.support.name')

        restore_default_content.send(:translate_groups)
      end
    end

    describe "#translate_account_defaults" do
      before do
        account.ticket_forms_instructions = I18n.t('txt.default.ticket_forms_instructions')
      end

      it "does not translate if no match is found" do
        restore_default_content.expects(:match).with(account.cc_subject_template, 'default_cc_subject_template', {})
        restore_default_content.expects(:match).with(account.cc_email_template, 'default_cc_email_template', {})
        restore_default_content.expects(:match).with(account.follower_subject_template, 'default_follower_subject_template', {})
        restore_default_content.expects(:match).with(account.follower_email_template, 'default_follower_email_template', {})

        restore_default_content.expects(:match).with(account.organization_activity_email_template, 'default_organization_activity_email_template', {}).returns(true)
        restore_default_content.expects(:match).with(account.verify_email_text, 'txt.default.verify_email_text', {}).returns(true)
        restore_default_content.expects(:match).with(account.ticket_forms_instructions, 'txt.default.ticket_forms_instructions', {}).returns(true)

        restore_default_content.expects(:default_cc_subject_template).with(spanish).never
        restore_default_content.expects(:default_cc_email_template).with(spanish).never
        restore_default_content.expects(:default_follower_subject_template).with(spanish).never
        restore_default_content.expects(:default_follower_email_template).with(spanish).never

        restore_default_content.send(:translate_account_defaults)
        account.reload

        assert_equal "{{ticket.title}}", account.cc_subject_template
        assert_equal "{{ticket.title}}", account.follower_subject_template
        assert_equal "All CC's on this request: {{ticket.cc_names}}", account.cc_email_template
        assert_equal "Custom follower template for ({{ticket.id}})\r\n{{ticket.follower_reply_type_message}}\r\n{{ticket.comments_formatted}}", account.follower_email_template
      end

      it "calls the correct methods to translate" do
        account.signup_email_text = I18n.t('txt.default.new_welcome_email_text', account_name: account.name)

        restore_default_content.expects(:match).with(account.cc_subject_template, 'default_cc_subject_template', {}).returns(true)
        restore_default_content.expects(:match).with(account.cc_email_template, 'default_cc_email_template', {}).returns(true)
        restore_default_content.expects(:match).with(account.follower_subject_template, 'default_follower_subject_template', {}).returns(true)
        restore_default_content.expects(:match).with(account.follower_email_template, 'default_follower_email_template', {}).returns(true)

        restore_default_content.expects(:match).with(account.organization_activity_email_template, 'default_organization_activity_email_template', {}).returns(true)
        restore_default_content.expects(:match).with(account.verify_email_text, 'txt.default.verify_email_text', {}).returns(true)
        restore_default_content.expects(:match).with(account.ticket_forms_instructions, 'txt.default.ticket_forms_instructions', {}).returns(true)

        restore_default_content.expects(:default_organization_activity_email_template).with(spanish).returns("Spanish activity template")
        restore_default_content.expects(:match).with(account.signup_email_text, 'txt.default.new_welcome_email_text', account_name: 'Minimum account').returns(true)

        restore_default_content.expects(:default_cc_subject_template).with(spanish).returns("Spanish cc subject template")
        restore_default_content.expects(:default_cc_email_template).with(spanish).returns("Spanish cc email template")
        restore_default_content.expects(:default_follower_subject_template).with(spanish).returns("Spanish follower subject template")
        restore_default_content.expects(:default_follower_email_template).with(spanish).returns("Spanish follower email template")

        restore_default_content.expects(:translate).with('txt.default.new_welcome_email_text', account_name: "Minimum account", locale: spanish).returns("Spanish")
        restore_default_content.expects(:translate).with('txt.default.verify_email_text').returns("Spanish")
        restore_default_content.expects(:translate).with('txt.default.ticket_forms_instructions').returns("Spanish")

        restore_default_content.send(:translate_account_defaults)
        account.reload

        assert_equal "Spanish cc subject template", account.cc_subject_template
        assert_equal "Spanish cc email template", account.cc_email_template
        assert_equal "Spanish follower subject template", account.follower_subject_template
        assert_equal "Spanish follower email template", account.follower_email_template

        assert_equal "Spanish activity template", account.organization_activity_email_template
        assert_nil account.signup_page_text
        assert_equal "Spanish", account.signup_email_text
        assert_equal "Spanish", account.verify_email_text
        assert_nil account.chat_welcome_message
        assert_equal "Spanish", account.ticket_forms_instructions
      end

      it "does not translate if ticket_forms_instructions property is nil" do
        account.ticket_forms_instructions = nil
        restore_default_content.stubs(:match).returns(false)

        restore_default_content.send(:translate_account_defaults)
        account.reload

        assert_nil account.ticket_forms_instructions
      end

      it "does not translate if account owner if not verified" do
        account.owner.is_verified = false
        account.save!

        # Should not raise "ActiveRecord::RecordInvalid: Validation failed: Before you make changes to your settings, the owner of this account must verify their email address."
        restore_default_content.send(:translate_account_defaults)
      end
    end
  end

  describe "#translate_sample_tickets" do
    let(:ticket) { tickets(:minimum_1) }

    it "does not translate if no sample ticket is found" do
      ticket.expects(:save!).never

      restore_default_content.send(:translate_sample_tickets)
    end

    it "sets the subject, description, and requester name and saves" do
      account.tickets.stubs(:where).returns([ticket])
      ticket_template = { 'subject' => 'Subject', 'description' => 'Description' }
      Ticket.expects(:ticket_template_with_context).with(account.owner, 'meet_the_ticket_en').returns(ticket_template)

      ticket.requester.expects(:save!)
      ticket.expects(:save!)

      restore_default_content.send(:translate_sample_tickets)

      assert_equal ticket_template['subject'], ticket.subject
      assert_equal ticket_template['description'], ticket.description
      assert_equal I18n.t('txt.zero_states.sample_ticket.sample_customer'), ticket.requester.name
    end

    describe "account user identities trial limit" do
      let(:account) { accounts(:trial) }

      it "skips if over limit" do
        Arturo.enable_feature!(:user_identity_limit_for_trialers)
        key = TrialLimit.send(:trial_limit_key, account, :user_identity)
        limit = account.trial_account_user_identity_limit
        TrialLimit.redis_client.set(key, limit + 1)

        account.tickets.stubs(:where).returns([ticket])

        restore_default_content.send(:translate_sample_tickets)

        assert_equal 'minimum 1 ticket', ticket.subject
      end
    end
  end

  describe "#match_and_translate" do
    let(:objects) { [] }
    let(:main_key) { "main.key" }
    let(:view) { account.views.first }

    it "sets the title, set the object content and update the object if an object is match" do
      restore_default_content.expects(:match).with(objects, main_key).returns(view)
      restore_default_content.expects(:set_title_or_name).with(view, main_key)
      restore_default_content.expects(:set_object_content).with(view, main_key, nil)
      restore_default_content.expects(:update_object).with(view)
      restore_default_content.send(:match_and_translate, objects: objects, main_key: main_key)
    end

    it "does not set anything if not match is found" do
      restore_default_content.expects(:match).returns(nil)
      restore_default_content.expects(:set_title_or_name).never
      restore_default_content.expects(:set_object_content).never
      restore_default_content.expects(:update_object).never
      restore_default_content.send(:match_and_translate, objects: objects, main_key: main_key)
    end
  end

  describe "#set_title_or_name" do
    let(:key) { "some.key" }
    before { restore_default_content.expects(:translate).with(key).returns("A new name/title") }

    it "sets name for role" do
      create_permission_sets(account)
      role = account.permission_sets.first

      restore_default_content.send(:set_title_or_name, role, key)
      assert_equal "A new name/title", role.name
    end

    it "sets name for group" do
      group = account.groups.first
      restore_default_content.send(:set_title_or_name, group, key)
      assert_equal "A new name/title", group.name
    end

    it "sets display name and name for ticket form" do
      set_ticket_forms(account)
      ticket_form = account.ticket_forms.first

      restore_default_content.send(:set_title_or_name, ticket_form, key)
      assert_equal "A new name/title", ticket_form.name
      assert_equal "A new name/title", ticket_form.display_name
    end

    it "sets title for rules" do
      rule = account.rules.first
      restore_default_content.send(:set_title_or_name, rule, key)
      assert_equal "A new name/title", rule.title
    end
  end

  describe "#set_object_content" do
    let(:main_key) { "some.key" }
    let(:action_keys) { ["action.key.a", "action.key.b"] }

    it "translates comment if macro" do
      macro = account.macros.first
      restore_default_content.expects(:match_translate_comment).with(macro, action_keys)
      restore_default_content.send(:set_object_content, macro, main_key, action_keys)
    end

    describe "when object is a trigger" do
      let(:trigger) { account.triggers.first }

      it "translates notification" do
        restore_default_content.expects(:match_translate_notification).with(trigger, action_keys)
        restore_default_content.send(:set_object_content, trigger, main_key, action_keys)
      end

      describe "#set_trigger_description" do
        let(:key) { "some.key" }
        describe "#when there is a description present" do
          let(:action_keys) { { subject: "txt.subject.key", body: "txt.body.key.", description: "some.key" } }
          before { restore_default_content.expects(:translate).with(key).returns("A new name/description") }

          it "sets description for rules" do
            restore_default_content.send(:set_trigger_description, trigger, action_keys)
            assert_equal "A new name/description", trigger.description
          end
        end

        describe "when there is no description present" do
          let(:action_keys) { { subject: "txt.subject.key", body: "txt.body.key." } }

          it "does not set description for rules" do
            restore_default_content.send(:set_trigger_description, trigger, action_keys)
            refute_equal "A new name/description", trigger.description
          end
        end
      end
    end

    it "translates notification if automation" do
      automation = account.automations.first
      restore_default_content.expects(:match_translate_notification).with(automation, action_keys)
      restore_default_content.send(:set_object_content, automation, main_key, action_keys)
    end

    it "translates legend if report" do
      create_reports(account)
      report = account.reports.first

      restore_default_content.expects(:match_translate_legends).with(report, main_key, action_keys)
      restore_default_content.send(:set_object_content, report, main_key, action_keys)
    end

    it "translates properties if ticket field" do
      ticket_field = account.ticket_fields.first
      restore_default_content.expects(:match_translate_properties).with(ticket_field, main_key, action_keys)
      restore_default_content.send(:set_object_content, ticket_field, main_key, action_keys)
    end

    it "translates description if permission set" do
      create_permission_sets(account)
      role = account.permission_sets.first
      restore_default_content.expects(:match_translate_description).with(role, action_keys)
      restore_default_content.send(:set_object_content, role, main_key, action_keys)
    end

    it "doesn't fail on views" do
      view = account.views.first
      restore_default_content.send(:set_object_content, view, "", "").must_be_nil
    end

    it "doesn't fail on customer lists" do
      user_view = account.user_views.first
      restore_default_content.send(:set_object_content, user_view, "", "").must_be_nil
    end

    it "doesn't fail on ticket forms" do
      form = TicketForm.new
      restore_default_content.send(:set_object_content, form, "", "").must_be_nil
    end

    it "doesn't fail on strings" do
      restore_default_content.send(:set_object_content, "Support", "", "").must_be_nil
    end
  end

  describe "#update_object" do
    let(:object) { account.views.first }

    it "saves without validation" do
      object.expects(:save).with(validate: false)
      restore_default_content.send(:update_object, object)
    end
  end

  describe "#is_read_only?" do
    it "returns true if object is a permission set and a light agent" do
      create_permission_sets(account)
      role = account.permission_sets.first
      role.expects(:is_light_agent?).returns(true)
      assert restore_default_content.send(:is_read_only?, role)
    end

    it "returns false if object is not a permission set" do
      create_permission_sets(account)
      role = account.permission_sets.first
      role.expects(:is_light_agent?).returns(false)
      refute restore_default_content.send(:is_read_only?, role)
    end

    it "returns false if object is a permission set but not a light agent" do
      view = account.views.first
      refute restore_default_content.send(:is_read_only?, view)
    end

    it "returns true if object is a system ticket field" do
      assert restore_default_content.send(:is_read_only?, account.field_subject)
    end
  end

  describe "#match_translate_description" do
    let(:object) { accounts(:multiproduct).permission_sets.first }
    let(:description_key) { "txt.description.key" }

    it "does not set the description if the description is not present" do
      object.update_attribute(:description, nil)
      restore_default_content.expects(:translate).never
      restore_default_content.send(:match_translate_description, object, description_key)
    end

    it "does not set the description if there is no match" do
      object.update_attribute(:description, 'some description')
      restore_default_content.expects(:match).returns(nil)
      restore_default_content.expects(:translate).never
      restore_default_content.send(:match_translate_description, object, description_key)
    end

    it "sets the description if there is a match" do
      object.update_attribute(:description, 'some description')
      restore_default_content.expects(:match).returns("match")
      restore_default_content.expects(:translate).with(description_key).returns("a translated description")
      restore_default_content.send(:match_translate_description, object, description_key)
      assert_equal "a translated description", object.description
    end
  end

  describe "#match_translate_comment" do
    let(:macro) { account.macros.first }
    let(:action_key) { "key.action.key" }

    it "translates comment_value if value is present" do
      restore_default_content.expects(:match).returns(true)
      restore_default_content.expects(:translate).with(action_key).returns("translated macro comment")
      restore_default_content.send(:match_translate_comment, macro, action_key)

      comment = macro.definition.actions.find { |action| action.source == "comment_value" }
      assert_equal "translated macro comment", comment.value[0]
    end

    it "does not translate comment if value is not present" do
      comment = macro.definition.actions.find { |action| action.source == "comment_value" }
      comment.value = nil

      restore_default_content.expects(:translate).never
      restore_default_content.send(:match_translate_comment, macro, action_key)
    end
  end

  describe "#match_translate_notification" do
    let(:automation) { account.triggers.first }
    let(:action_keys) { { subject: "txt.subject.key", body: "txt.body.key" } }

    it "translates the body and subject of a notification if found" do
      restore_default_content.stubs(:match).returns(true, true)
      restore_default_content.expects(:translate).with("txt.subject.key").returns("This is a translated subject")
      restore_default_content.expects(:translate).with("txt.body.key").returns("This is a translated body")
      restore_default_content.send(:match_translate_notification, automation, action_keys)

      notification = automation.definition.actions.find do |action|
        ["notification_user", "notification_group"].include?(action.source)
      end

      assert_equal "This is a translated subject", notification.value[1]
      assert_equal "This is a translated body", notification.value[2]
    end

    it "does not translate the body and subject of a notification if not found" do
      automation.definition.actions.find do |action|
        ["notification_user", "notification_group"].include?(action.source)
      end

      automation.definition.actions.delete_if { |action| ["notification_user", "notification_group"].include?(action.source) }

      restore_default_content.expects(:translate).never
      restore_default_content.send(:match_translate_notification, automation, action_keys)
    end

    it "does not translate the subject if we don't find a match" do
      restore_default_content.stubs(:match).returns(nil, true)
      restore_default_content.expects(:translate).with("txt.subject.key").never
      restore_default_content.expects(:translate).with("txt.body.key").returns("This is a translated body")

      restore_default_content.send(:match_translate_notification, automation, action_keys)

      notification = automation.definition.actions.find do |action|
        ["notification_user", "notification_group"].include?(action.source)
      end
      assert_equal "[{{ticket.account}}] {{current_user.name}} has reopened \"{{ticket.title}}\"", notification.value[1]
      assert_equal "This is a translated body", notification.value[2]
    end

    it "does not transalte the body if we don't find a match" do
      restore_default_content.stubs(:match).returns(true, nil)
      restore_default_content.expects(:translate).with("txt.subject.key").returns("This is a translated subject")
      restore_default_content.expects(:translate).with("txt.body.key").never

      restore_default_content.send(:match_translate_notification, automation, action_keys)

      notification = automation.definition.actions.find do |action|
        ["notification_user", "notification_group"].include?(action.source)
      end
      assert_equal "This is a translated subject", notification.value[1]
      assert_equal "Please follow this link to review the status of this ticket and add updates {{ticket.link}}\n\n{{ticket.comments_formatted}}", notification.value[2]
    end

    describe "when action_keys includes a :versions property" do
      let(:action_keys) { { subject: "txt.subject.key", body: "txt.body.key.", versions: ['version1', 'version2'] } }

      it 'attempts to see if either version is a match' do
        notification = automation.definition.actions.first
        restore_default_content.expects(:match_trigger_versions).with(notification, action_keys)
        restore_default_content.send(:match_translate_notification, automation, action_keys)
      end

      it 'finds the appropriate version and translate body when there is a match' do
        restore_default_content.stubs(:match).returns(true)
        restore_default_content.stubs(:match_trigger_versions).returns(true)
        restore_default_content.expects(:notification_body).with(action_keys[:versions]).returns('version1')
        restore_default_content.expects(:translate).with('txt.subject.key')
        restore_default_content.expects(:translate).with('txt.body.key.version1')
        restore_default_content.send(:match_translate_notification, automation, action_keys)
      end

      it 'does not translate body when no versions match' do
        restore_default_content.stubs(:match).returns(false)
        restore_default_content.expects(:match_trigger_versions).returns(false)
        restore_default_content.expects(:notification_body).never
        restore_default_content.expects(:translate).never
        restore_default_content.send(:match_translate_notification, automation, action_keys)
      end
    end

    describe "when action_keys includes a :subject_versions property" do
      let(:action_keys) { { subject: "txt.subject.key.", body: "txt.body.key.version1", subject_versions: ['version1', 'version2'] } }

      it 'attempts to see if either subject_versions is a match' do
        notification = automation.definition.actions.first
        restore_default_content.expects(:match_trigger_subject_versions?).with(notification, action_keys)
        restore_default_content.send(:match_translate_notification, automation, action_keys)
      end

      it 'finds the appropriate subject_versions and translate subject when there is a match' do
        restore_default_content.stubs(:match).returns(true)
        restore_default_content.stubs(:match_trigger_subject_versions?).returns(true)
        restore_default_content.stubs(:match_trigger_versions).returns(true)
        restore_default_content.expects(:notification_body).with(action_keys[:subject_versions]).returns('version1')
        restore_default_content.expects(:translate).with('txt.subject.key.version1')
        restore_default_content.expects(:translate).with('txt.body.key.version1')
        restore_default_content.send(:match_translate_notification, automation, action_keys)
      end

      it 'does not translate body when no versions match' do
        restore_default_content.stubs(:match).returns(false)
        restore_default_content.expects(:match_trigger_subject_versions?).returns(false)
        restore_default_content.expects(:notification_body).never
        restore_default_content.expects(:translate).never
        restore_default_content.send(:match_translate_notification, automation, action_keys)
      end
    end
  end

  describe "#match_trigger_versions" do
    let(:notification) { account.triggers.first.definition.actions.first }
    let(:action_keys) { { subject: "txt.subject.key", body: "txt.body.key.", versions: ['version1', 'version2'] } }

    it "returns a matched object if a match is found" do
      restore_default_content.expects(:match).with(notification.value[2], "txt.body.key.version1").returns(false)
      restore_default_content.expects(:match).with(notification.value[2], "txt.body.key.version2").returns(true)
      assert_equal "version2", restore_default_content.send(:match_trigger_versions, notification, action_keys)
    end

    it "returns nil if no match is found" do
      restore_default_content.expects(:match).with(notification.value[2], "txt.body.key.version1").returns(false)
      restore_default_content.expects(:match).with(notification.value[2], "txt.body.key.version2").returns(false)
      assert_nil restore_default_content.send(:match_trigger_versions, notification, action_keys)
    end
  end

  describe "#match_translate_properties" do
    let(:ticket_field) { account.ticket_fields.first }
    let(:title_key) { "title.key" }
    let(:description_key) { "description.key" }

    before { restore_default_content.expects(:translate).with(title_key).returns("This is a translated title") }

    it "translates title in portal but not translate description if not present" do
      ticket_field.description = nil
      restore_default_content.expects(:match).with(description_key).never
      restore_default_content.expects(:translate).with(description_key).never
      restore_default_content.send(:match_translate_properties, ticket_field, title_key, description_key)

      assert_equal "This is a translated title", ticket_field.title_in_portal
    end

    it "translates title in portal but not translate description if present but not match is found" do
      restore_default_content.expects(:match).returns(nil)
      restore_default_content.expects(:translate).with(description_key).never
      restore_default_content.send(:match_translate_properties, ticket_field, title_key, description_key)
      assert_equal "This is a translated title", ticket_field.title_in_portal
    end

    it "translates title in portal but and translate ticket field description if present and match is found" do
      restore_default_content.expects(:match).returns("a match")
      restore_default_content.expects(:translate).with(description_key).returns("This is a translated description")
      restore_default_content.send(:match_translate_properties, ticket_field, title_key, description_key)
      assert_equal "This is a translated title", ticket_field.title_in_portal
      assert_equal "This is a translated description", ticket_field.description
    end
  end

  describe "#match_translate_legends" do
    before { create_reports(account) }

    let(:report) { account.reports.first }
    let(:action_keys) { ["backlog", "created", "solved"] }
    let(:title_key) { "txt.default.reports.backlog.title" }

    it "does not translate definition if a match is not found" do
      restore_default_content.expects(:match).returns(nil).at_least_once
      restore_default_content.expects(:translate).never
      restore_default_content.send(:match_translate_legends, report, title_key, action_keys)
    end

    it "translates definition if a match is found" do
      restore_default_content.expects(:match).with("Backlog", "txt.default.reports.backlog.backlog").returns(true)
      restore_default_content.expects(:match).with("New tickets", "txt.default.reports.backlog.created").returns(true)
      restore_default_content.expects(:match).with("New tickets", "txt.default.reports.backlog.backlog").returns(false)
      restore_default_content.expects(:match).with("Solved tickets", "txt.default.reports.backlog.solved").returns(true)
      restore_default_content.expects(:match).with("Solved tickets", "txt.default.reports.backlog.backlog").returns(false)
      restore_default_content.expects(:match).with("Solved tickets", "txt.default.reports.backlog.created").returns(false)

      restore_default_content.expects(:translate).with("txt.default.reports.backlog.backlog").returns("Spanish backlog")
      restore_default_content.expects(:translate).with("txt.default.reports.backlog.created").returns("Spanish created")
      restore_default_content.expects(:translate).with("txt.default.reports.backlog.solved").returns("Spanish solved")

      restore_default_content.send(:match_translate_legends, report, title_key, action_keys)

      assert_equal "Spanish backlog", report[:definition][0][:legend][:name]
      assert_equal "Spanish created", report[:definition][1][:legend][:name]
      assert_equal "Spanish solved", report[:definition][2][:legend][:name]
    end
  end

  describe "#notification_body" do
    it "returns the value if it is not an array" do
      restore_default_content.account.expects(:help_center_enabled?).never
      assert_equal "TEST", restore_default_content.send(:notification_body, "TEST")
    end

    it "returns the first value of the array if help center is not enable" do
      restore_default_content.account.expects(:help_center_enabled?).returns(false)
      assert_equal "first", restore_default_content.send(:notification_body, ["first", "second"])
    end

    it "returns the second value of the array if help center is enable" do
      restore_default_content.account.expects(:help_center_enabled?).returns(true)
      assert_equal "second", restore_default_content.send(:notification_body, ["first", "second"])
    end
  end

  describe "#default_cc_email_template" do
    it "returns v2" do
      I18n.expects(:t).with("txt.default.cc_email_text.paragraph_v2", locale: ENGLISH_BY_ZENDESK)
      restore_default_content.send(:default_cc_email_template, ENGLISH_BY_ZENDESK)
    end
  end

  describe "#default_follower_email_template" do
    it "returns the default text" do
      I18n.expects(:t).with("txt.default.follower_email_text", locale: ENGLISH_BY_ZENDESK)
      restore_default_content.send(:default_follower_email_template, ENGLISH_BY_ZENDESK)
    end
  end

  describe "#default_organization_activity_email_template" do
    it "returns the translated email template with the placeholder concatenated at the end" do
      restore_default_content.expects(:translate).with('txt.default.organization_activity_email_text', locale: spanish).returns("Translated in spanish")
      assert_equal "Translated in spanish\n\n{{ticket.comments_formatted}}", restore_default_content.send(:default_organization_activity_email_template, spanish)
    end
  end

  describe "#match" do
    before { restore_default_content.instance_variable_set :@all_locales, all_locales }

    let(:japanese) { translation_locales(:japanese) }
    let(:all_locales) { [japanese, spanish, ENGLISH_BY_ZENDESK] }
    let(:object) { Object.new }

    it "returns nil if no matched locales present" do
      restore_default_content.instance_variable_set :@all_locales, nil
      assert_nil restore_default_content.send(:match, object, "some.key")
    end

    it 'raises exception if can not find compare method' do
      assert_raises(RestoreDefaultContentJob::UndefinedCompareMethodError) do
        restore_default_content.send(:match, nil, "some.key")
      end
    end

    it "compares string" do
      restore_default_content.stubs(:choose_method).returns(:compare_string)
      refute restore_default_content.send(:match, "some string", "some.key")
      I18n.stubs(:t).with("some.key", anything).returns("some other string")
      assert restore_default_content.send(:match, "some other string", "some.key")
    end

    it "returns the stack with the matched locales in order" do
      restore_default_content.stubs(:choose_method).returns(:compare_string)
      restore_default_content.stubs(:compare_according_to_function).returns(false, true)
      restore_default_content.send(:match, object, "some.key")
      # Order is extremely important!!!
      assert_equal [spanish, japanese, ENGLISH_BY_ZENDESK], all_locales
    end

    it "returns the stack without modification if no match is found" do
      restore_default_content.stubs(:choose_method).returns(:compare_string)
      restore_default_content.stubs(:compare_according_to_function).returns(false, false, false)
      restore_default_content.send(:match, object, "some.key")
      assert_equal [japanese, spanish, ENGLISH_BY_ZENDESK], all_locales
    end

    it "returns the stack without modification if nil objects are returned" do
      restore_default_content.stubs(:choose_method).returns(:compare_string)
      restore_default_content.stubs(:compare_according_to_function).returns(nil, nil, nil)
      restore_default_content.send(:match, object, "some.key")
      assert_equal [japanese, spanish, ENGLISH_BY_ZENDESK], all_locales
    end

    it "returns true if function returns a boolean" do
      restore_default_content.stubs(:choose_method).returns(:compare_string)
      restore_default_content.stubs(:compare_according_to_function).returns(true)
      assert restore_default_content.send(:match, object, "some.key")
      assert_equal [japanese, spanish, ENGLISH_BY_ZENDESK], all_locales
    end

    it "returns true if function returns a boolean with several responses" do
      restore_default_content.stubs(:choose_method).returns(:compare_string)
      restore_default_content.stubs(:compare_according_to_function).returns(false, nil, object)
      assert_equal object, restore_default_content.send(:match, object, "some.key")
      assert_equal [ENGLISH_BY_ZENDESK, japanese, spanish], all_locales
    end
  end

  describe "#choose_method" do
    before do
      @account = accounts(:minimum)
    end

    it "returns compare_function if key is default_cc_subject_template" do
      key = "default_cc_subject_template"
      assert_equal :compare_function, restore_default_content.send(:choose_method, nil, key)
    end

    it "returns compare_function if key is default_cc_email_template" do
      key = "default_cc_email_template"
      assert_equal :compare_function, restore_default_content.send(:choose_method, nil, key)
    end

    it "returns compare_function if key is default_follower_subject_template" do
      key = "default_follower_subject_template"
      assert_equal :compare_function, restore_default_content.send(:choose_method, nil, key)
    end

    it "returns compare_function if key is default_follower_email_template" do
      key = "default_follower_email_template"
      assert_equal :compare_function, restore_default_content.send(:choose_method, nil, key)
    end

    it "returns compare_function if key is default_organization_activity_email_template" do
      key = "default_organization_activity_email_template"
      assert_equal :compare_function, restore_default_content.send(:choose_method, nil, key)
    end

    it "returns compare_title if object responds to title" do
      @object = Rule.new
      Rule.any_instance.stubs(:respond_to?).with(:title).returns(true)
      assert_equal :compare_title, restore_default_content.send(:choose_method, @object, nil)
    end

    it "returns compare_title if object is a rule" do
      @object = Rule.new
      assert_equal :compare_title, restore_default_content.send(:choose_method, [@object], nil)
    end

    it "returns compare_title if object is a report" do
      @object = Report.new
      assert_equal :compare_title, restore_default_content.send(:choose_method, [@object], nil)
    end

    it "returns compare_title if object is a ticket field" do
      @object = TicketField.new
      assert_equal :compare_title, restore_default_content.send(:choose_method, [@object], nil)
    end

    it "returns compare_name if object responds to name" do
      @object = Role.new
      Role.any_instance.stubs(:respond_to?).with(:title).returns(false)
      Role.any_instance.stubs(:respond_to?).with(:name).returns(true)
      assert_equal :compare_name, restore_default_content.send(:choose_method, @object, nil)
    end

    it "returns compare_string if object is a string" do
      @object = "this is a test string"
      assert_equal :compare_string, restore_default_content.send(:choose_method, @object, nil)
    end
  end

  describe "#object_has_title?" do
    it "returns false if object is not an array" do
      refute restore_default_content.send(:object_has_title?, "NOT AN ARRAY!")
    end

    it "returns true if object is a Rule" do
      assert restore_default_content.send(:object_has_title?, account.rules)
    end

    it "returns true if object is a Report" do
      create_reports(account)
      assert restore_default_content.send(:object_has_title?, account.reports)
    end

    it "returns true if object is a TicketField" do
      assert restore_default_content.send(:object_has_title?, account.ticket_fields)
    end
  end

  describe "#compare_according_to_function" do
    before do
      @account = accounts(:minimum)
    end

    it "calls function default_cc_subject_template" do
      restore_default_content.stubs(:default_cc_subject_template).returns("a bunch of strings")
      assert restore_default_content.send(:compare_according_to_function, "a bunch of strings", "default_cc_subject_template", :compare_function, locale: ENGLISH_BY_ZENDESK)
    end

    it "calls function default_cc_email_template" do
      restore_default_content.stubs(:default_cc_email_template).returns("a bunch of strings")
      assert restore_default_content.send(:compare_according_to_function, "a bunch of strings", "default_cc_email_template", :compare_function, locale: ENGLISH_BY_ZENDESK)
    end

    it "calls function default_follower_subject_template" do
      restore_default_content.stubs(:default_follower_subject_template).returns("a bunch of strings")
      assert restore_default_content.send(:compare_according_to_function, "a bunch of strings", "default_follower_subject_template", :compare_function, locale: ENGLISH_BY_ZENDESK)
    end

    it "calls function default_follower_email_template" do
      restore_default_content.stubs(:default_follower_email_template).returns("a bunch of strings")
      assert restore_default_content.send(:compare_according_to_function, "a bunch of strings", "default_follower_email_template", :compare_function, locale: ENGLISH_BY_ZENDESK)
    end

    it "calls function default_organization_activity_email_template" do
      restore_default_content.stubs(:default_organization_activity_email_template).returns("a bunch of strings")
      assert restore_default_content.send(:compare_according_to_function, "a bunch of strings", "default_organization_activity_email_template", :compare_function, locale: ENGLISH_BY_ZENDESK)
    end

    it "calls function find_by_name if the type of function is find_by_title" do
      object = stub("Fake AR")
      object.expects(:where).with(title: 'translation missing: en-US-x-1.a.key').returns(["A returned object"])
      assert_equal "A returned object", restore_default_content.send(:compare_according_to_function, object, "a.key", :compare_title, locale: ENGLISH_BY_ZENDESK)
    end

    it "calls function find_by_name if the type of function is find_by_name" do
      object = stub("Fake AR")
      object.expects(:where).with(name: 'translation missing: en-US-x-1.a.key').returns(["A returned object"])
      assert_equal "A returned object", restore_default_content.send(:compare_according_to_function, object, "a.key", :compare_name, locale: ENGLISH_BY_ZENDESK)
    end

    it "returns true when the object being matched includes newline characters" do
      object = "Here is a string\nwith a line break"
      I18n.expects(:t).with("some.key", locale: ENGLISH_BY_ZENDESK).returns("Here is a string<br />with a line break")
      assert restore_default_content.send(:compare_according_to_function, object, "some.key", :compare_string, locale: ENGLISH_BY_ZENDESK)
    end
  end

  describe "#translate" do
    let(:key) { 'txt.random.key' }

    it "translates with the new locale if no locale is passed" do
      I18n.expects(:t).with(key, locale: spanish)
      restore_default_content.send(:translate, key)
    end

    it "translates with the locale passed" do
      I18n.expects(:t).with(key, locale: ENGLISH_BY_ZENDESK)
      restore_default_content.send(:translate, key, locale: ENGLISH_BY_ZENDESK)
    end
  end

  def set_ticket_forms(account) # rubocop:disable Naming/AccessorMethodName
    unless account.ticket_forms.present?
      params = {
        ticket_form: {
          name: I18n.t('txt.admin.model.ticket_form.default_form_name'),
          display_name: I18n.t('txt.admin.model.ticket_form.default_form_name'),
          default: true,
          end_user_visible: true,
          position: 1,
          ticket_field_ids: account.ticket_fields.active.map(&:id)
        }
      }
      Zendesk::TicketForms::Initializer.new(account, params)

      account.save!
    end
  end

  def create_permission_sets(account)
    unless account.permission_sets.present?
      permission_sets = PermissionSet.build_default_permission_sets(account)
      permission_sets.push(PermissionSet.create_light_agent!(account))
      permission_sets.each(&:save!)
      account.reload
    end
  end

  def create_reports(account)
    unless account.reports.present?
      account.becomes(Accounts::Classic).send(:load_reports_from_yaml)
      account.reload
    end
  end
end
