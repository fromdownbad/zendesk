require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 2

describe AccountsAfterConversionJob do
  extend Api::V2::TestHelper
  fixtures :accounts

  describe '.work' do
    let(:minimum_account) { accounts(:minimum) }

    before do
      @statsd_stub = stub_for_statsd
      AccountsAfterConversionJob.stubs(statsd_client: @statsd_stub)
    end

    it 'calls run_after_convertion_callbacks' do
      @statsd_stub.expects(:increment).with('count', tags: ['result:success', 'language:english'])

      base_account = Accounts::Base.find(minimum_account.id)
      Accounts::Base.stubs(:find).with(minimum_account.id).returns(base_account)
      base_account.expects(:run_after_convertion_callbacks)
      AccountsAfterConversionJob.work(minimum_account.id)
    end

    it 'does not call run_after_convertion_callbacks when account is invalid' do
      @statsd_stub.expects(:increment).with('count', tags: ['result:failure'])
      ZendeskExceptions::Logger.expects(:record)

      assert_raise ActiveRecord::RecordNotFound do
        AccountsAfterConversionJob.work(Account.last.id + 1)
      end
    end
  end
end
