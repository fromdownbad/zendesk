require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 14

describe FetchProfileImageJob do
  fixtures :accounts, :users, :photos, :organizations
  include PerformJob

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_end_user)
    @url     = "http://hello.com/me.png"
  end

  describe "#perform" do
    describe "with good data" do
      before do
        stub_request(:get, @url).to_return(body: FixtureHelper::Files.read("small.png"), headers: { content_type: "image/png" })
      end

      it "sets the remote image as photo on the user" do
        assert @user.photo.blank?
        perform_job(FetchProfileImageJob, @account.id, @user.id, @url)
        assert @user.reload.photo.present?
      end

      it "sets the original_url too" do
        assert @user.photo.blank?
        perform_job(FetchProfileImageJob, @account.id, @user.id, @url)
        assert @user.reload.photo.original_url
      end
    end

    it "terminates early when receiving a bad status" do
      stub_request(:get, @url).to_return(status: 403, body: "foo", headers: {})
      User.any_instance.expects(:set_photo).never
      perform_job(FetchProfileImageJob, @account.id, @user.id, @url)
    end

    it "terminates early when receiving bad data" do
      stub_request(:get, @url).to_return(status: 200, body: "", headers: { content_type: "image/png" })
      User.any_instance.expects(:set_photo).never
      perform_job(FetchProfileImageJob, @account.id, @user.id, @url)
    end

    describe "fetching image from site with broken SSL" do
      before do
        Zendesk::Net::MultiSSL.stubs(:get).with(@url, raise_errors: false).returns(nil)
      end

      it "logs profile image fetch error" do
        assert_raises FetchProfileImageJob::RetrievalError do
          FetchProfileImageJob.set_remote_photo(@user, @url)
        end
      end
    end
  end

  describe "#file_name" do
    it "renames file based on user info and content type" do
      # test some known mime-types
      assert_equal "profile_image_#{@user.id}_#{@account.id}.jpg", FetchProfileImageJob.send(:file_name, @user, "/tmp/blahstuff", "image/jpeg")
      assert_equal "profile_image_#{@user.id}_#{@account.id}.png", FetchProfileImageJob.send(:file_name, @user, "/tmp/blahstuff", "image/png")
      assert_equal "profile_image_#{@user.id}_#{@account.id}.gif", FetchProfileImageJob.send(:file_name, @user, "/tmp/blahstuff", "image/gif")
      assert_equal "profile_image_#{@user.id}_#{@account.id}.bmp", FetchProfileImageJob.send(:file_name, @user, "/tmp/blahstuff", "image/bmp")
      # test some (currently) unknown/unhandled types
      assert_equal "profile_image_#{@user.id}_#{@account.id}.foo", FetchProfileImageJob.send(:file_name, @user, "/tmp/blahstuff.foo", "garbage")
      assert_equal "profile_image_#{@user.id}_#{@account.id}.bar", FetchProfileImageJob.send(:file_name, @user, "/tmp/blahstuff.bar", "image/jpg")
    end
  end
end
