require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'DowngradeAgentGroupsAccessJob' do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }

  describe "#perform" do
    it "changes restriction_id for agents from groups to assigned" do
      agent.update_attribute(:restriction_id, RoleRestrictionType.GROUPS)

      DowngradeAgentGroupsAccessJob.perform(account.id)

      assert_equal RoleRestrictionType.ASSIGNED, agent.reload.restriction_id
    end

    it "doesn't change restriction_id if not previously set to groups" do
      agent.update_attribute(:restriction_id, RoleRestrictionType.NONE)

      DowngradeAgentGroupsAccessJob.perform(account.id)

      assert_equal RoleRestrictionType.NONE, agent.reload.restriction_id
    end
  end
end
