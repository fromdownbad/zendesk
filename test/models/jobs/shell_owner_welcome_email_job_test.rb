require_relative '../../support/job_helper'

SingleCov.covered!

describe ShellOwnerWelcomeEmailJob do
  fixtures :accounts, :users

  let(:account) { accounts(:shell_account_without_support) }
  let(:user) { users(:shell_owner_without_support) }
  let(:mailer) { AccountsMailer.stubs(:deliver_welcome_account_owner) }
  let(:job) { ShellOwnerWelcomeEmailJob.new(account_id: account.id, user_id: user.id) }

  def stub_readiness_check(state:)
    Zendesk::AccountReadiness.any_instance.stubs(:readiness_check).returns(state)
  end

  before do
    stub_readiness_check(state: 'complete')
  end

  describe '#work' do
    it "sends the email to the owner" do
      mailer.with(user,
        account_url: account.url(mapped: false),
        priority_mail: "Shell account owner welcome email").once
      job.work
    end

    it "fails if the account is not found" do
      mailer.never
      assert_raises ActiveRecord::RecordNotFound do
        ShellOwnerWelcomeEmailJob.new(account_id: 9999999, user_id: user.id).work
      end
    end

    it "fails if the user is not found" do
      mailer.never
      assert_raises ActiveRecord::RecordNotFound do
        ShellOwnerWelcomeEmailJob.new(account_id: 9999999).work
      end
    end

    describe 'when account is not ready' do
      before { stub_readiness_check(state: 'pending') }
      it 'raises error' do
        mailer.never
        assert_raises ShellOwnerWelcomeEmailJob::AccountNotReady do
          job.work
        end
      end
    end
  end
end
