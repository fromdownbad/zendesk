require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 2

describe TicketBrandReassignJob do
  fixtures :accounts, :brands, :tickets

  describe "#perform" do
    let(:account)        { accounts(:minimum) }
    let(:brand)          { FactoryBot.create(:brand, account_id: account.id) }
    let(:ticket)         { tickets(:minimum_1) }

    before do
      ticket.brand = brand
      ticket.will_be_saved_by(account.owner)
      ticket.save
    end

    it "reassigns the ticket to the default brand" do
      assert_equal brand, ticket.brand

      TicketBrandReassignJob.perform(account.id, brand.id)

      assert_equal account.default_brand, ticket.reload.brand
    end
  end
end
