require_relative "../../support/job_helper"

SingleCov.covered!

describe ProductSyncValidationJob do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  describe '.job_enqueue_delay' do
    after { ENV.delete('PRODUCT_SYNC_VALIDATION_JOB_DELAY_SECONDS') }

    describe 'when env value override is provided' do
      it 'returns the delay specified by env var' do
        ENV['PRODUCT_SYNC_VALIDATION_JOB_DELAY_SECONDS'] = '20'
        assert_equal 20.seconds, ProductSyncValidationJob.job_enqueue_delay
      end
    end

    describe 'when env value is not provided' do
      it 'returns the default job delay' do
        expected = ProductSyncValidationJob::DEFAULT_JOB_DELAY_SECONDS.seconds
        assert_equal expected, ProductSyncValidationJob.job_enqueue_delay
      end
    end
  end

  describe '#work' do
    it 'debounces product sync validation when no product changes are expected' do
      Zendesk::Accounts::ProductSyncValidator.expects(:validate).with(account, instrument_results: true).once
      3.times { ProductSyncValidationJob.enqueue(account.id) }
    end

    it 'performs validation when product changes are expected' do
      Zendesk::Accounts::ProductSyncValidator.expects(:validate).with(account, instrument_results: true).twice
      3.times { ProductSyncValidationJob.enqueue(account.id) }
      account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
      ProductSyncValidationJob.enqueue(account.id)
    end

    it 'works when account is locked' do
      account.lock!
      Rails.cache.clear
      Zendesk::Accounts::ProductSyncValidator.expects(:validate).with(account, instrument_results: true).once
      ProductSyncValidationJob.enqueue(account.id)
    end

    it 'silently succeeds without validating when account shard is not found' do
      account.update_attribute(:shard_id, 99999)
      Zendesk::Accounts::ProductSyncValidator.expects(:validate).never
      ProductSyncValidationJob.enqueue(account.id)
    end
  end
end
