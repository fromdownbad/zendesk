require_relative "../../../support/job_helper"
require 'zopim_reseller_api'

SingleCov.covered! uncovered: 8

describe Zopim::AgentSyncJob do
  fixtures :all

  describe "synchronizing" do
    let(:zopim_subscription) { stub(zopim_account_id: 123456) }

    let(:user) { account.owner }

    let(:account) { accounts(:minimum) }

    let(:reseller_client) { stub }

    before do
      Zopim.config.stubs(reseller_api_credentials: {})
      Zopim::Reseller.stubs(client: reseller_client)
      Account.any_instance.stubs(zopim_subscription: zopim_subscription)
      User.any_instance.stubs(zopim_identity: zopim_agent)
      SecureRandom.stubs(hex: 'random-password')
    end

    describe "zopim agent missing or destroyed" do
      let(:zopim_agent) { nil }

      it "does not call the reseller" do
        payload = anything
        reseller_client.expects(:update_account_agent!).never
        Zopim::AgentSyncJob.perform(account.id, user.id, payload)
      end
    end

    describe "an existing zopim agent record" do
      let(:zopim_agent) do
        Zopim::Agent.new.tap do |record|
          record.account_id            = account.id
          record.user_id               = user.id
          record.zopim_subscription_id = 123456
          record.zopim_agent_id        = 654321
          record.stubs(:update_column)
        end
      end

      it "requests the reseller API to update the agent record" do
        payload = anything
        reseller_client.expects(:update_account_agent!).
          with(
            id: 123456,
            agent_id: 654321,
            data: payload
          )
        Zopim::AgentSyncJob.perform(account.id, user.id, payload)
      end
    end
  end
end
