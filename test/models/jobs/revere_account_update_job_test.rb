require_relative "../../support/job_helper"

SingleCov.covered!

describe RevereAccountUpdateJob do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let!(:user) { account.admins.first }
  let!(:user2) { account.admins.second }
  let(:api_client) { stub }

  before do
    RevereAccountUpdateJob.stubs(:revere_api_client).returns(api_client)
  end

  # this job is really just for updating the pod/sudomain, not subscribing/unsubscribing users
  # but that can't be tested on this side. so we just verify it makes the expected api calls.

  describe "#work" do
    it 'subscribe user' do
      user.settings.revere_subscription = true
      user.settings.save!
      api_client.expects(:update_subscription).with(user, true)

      RevereAccountUpdateJob.work(account.id)
    end

    it 'ignores account with no current subscribers' do
      user.settings.revere_subscription = false
      user.settings.save!
      api_client.expects(:update_subscription).never

      RevereAccountUpdateJob.work(account.id)
    end

    it 'ignores account with no one ever subscribed' do
      # no revere_subscription user_setting at all
      api_client.expects(:update_subscription).never
      RevereAccountUpdateJob.work(account.id)
    end
  end
end
