require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe TerminateAllSessionsJob do
  fixtures :accounts, :users, :tokens, :mobile_apps

  describe "#perform" do
    before do
      @account = accounts(:minimum)
      @admin = users(:minimum_admin)

      FactoryBot.create(:session_record, account: @account, user: @admin)
      tokens(:minimum_token)
      @admin.tokens.create!(account_id: @account.id, client_id: 1)

      PushNotifications::UrbanAirshipClient.any_instance.stubs(:register)
      PushNotifications::UrbanAirshipClient.any_instance.stubs(:unregister)
      PushNotificationRegistrationJob.stubs(:enqueue).returns(true)
      PushNotifications::FirstPartyDeviceIdentifier.create!(
        user: @admin,
        token: "HELLO",
        device_type: "iPhone",
        mobile_app: mobile_apps(:com_zendesk_agent)
      )

      device_token = @admin.tokens.create!(account_id: @account.id, client_id: 1)
      @admin.mobile_devices.create!(account: @account, token: device_token.id)
    end

    describe 'when delete_token is true' do
      it "logouts all users and tokens" do
        associations = [:verification_tokens, :shared_sessions, :tokens, :device_identifiers, :mobile_devices]
        associations.each { |a| refute_equal 0, @account.send(a).count(:all), "#{a} must not be 0" }

        TerminateAllSessionsJob.perform(@account.id, true)

        associations.each { |a| assert_equal 0, @account.send(a).count(:all), "#{a} must be 0" }
      end
    end

    describe 'when delete_token is false' do
      it "logouts all users but keeps tokens" do
        associations = [:verification_tokens, :shared_sessions, :tokens, :device_identifiers, :mobile_devices]
        associations.each { |a| refute_equal 0, @account.send(a).count(:all), "#{a} must not be 0" }

        TerminateAllSessionsJob.perform(@account.id, false)

        assert_equal 0, @account.shared_sessions.count(:all)

        assert_not_equal 0, @account.verification_tokens.count(:all)
        assert_not_equal 0, @account.tokens.count(:all)
        assert_not_equal 0, @account.verification_tokens.count(:all)
        assert_not_equal 0, @account.device_identifiers.count(:all)
        assert_not_equal 0, @account.mobile_devices.count(:all)
      end
    end
  end
end
