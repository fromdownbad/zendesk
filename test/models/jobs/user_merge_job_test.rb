require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe UserMergeJob do
  fixtures :accounts, :users, :role_settings
  include DomainEventsHelper

  let(:domain_event_publisher) { FakeEscKafkaMessage.new }
  let(:role_setting) { role_settings(:minimum) }

  before do
    Account.any_instance.stubs(role_settings: role_setting)
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  def should_not_call_merge
    Users::Merge.expects(:new).never
    Users::Merge.any_instance.expects(:merge!).never
  end

  def should_not_emit_user_merged
    assert_equal decode_user_events(domain_event_publisher.events, :user_merged).size, 0
  end

  let(:user) { users(:minimum_end_user) }
  let(:user_2) { users(:minimum_end_user2) }

  it "merges 2 users" do
    merge = stub("UserMerge")
    Users::Merge.expects(:new).with(winner: user, loser: user_2).returns(merge)
    merge.expects(:merge!)
    UserMergeJob.work(user.account_id, user.id, user_2.id)
  end

  it "does not merge the same users" do
    should_not_call_merge
    UserMergeJob.work(user.account_id, user.id, user.id)
    should_not_emit_user_merged
  end

  it "does not merge a missing user" do
    should_not_call_merge
    UserMergeJob.work(user.account_id, user.id, 0)
    should_not_emit_user_merged
  end

  describe "user merge locks" do
    it "clears the user redis keys" do
      Users::Merge.expects(:unlock_users).with(user.id, user_2.id)
      UserMergeJob.work(user.account_id, user.id, user_2.id)
    end

    it "clears the user redis keys even on error" do
      Users::Merge.expects(:unlock_users).with(user.id, user_2.id)
      UserMergeJob.stubs(:merge).raises(StandardError)
      assert_raises StandardError do
        UserMergeJob.work(user.account_id, user.id, user_2.id)
      end
    end
  end

  describe "when merge 2 users" do
    it "publish user_merged event" do
      Users::Merge.expects(:validate_loser).with(user_2).returns(:loser_valid)
      UserMergeJob.work(user.account_id, user.id, user_2.id)
      user_merged_events = decode_user_events(domain_event_publisher.events, :user_merged)
      assert_equal user_merged_events.size, 1
      assert_equal user_merged_events.first.user_merged.winner.id.value, user.id
    end
  end
end
