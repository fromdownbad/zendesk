require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'RollupBacklogJob' do
  before do
    @account_id = 1
    @time_stamp = Time.now.utc
    @duration = 1.hour.to_i
  end
  describe "when working" do
    it "invokes rollup" do
      Zendesk::Stats::TicketBacklog.expects(:rollup).with(@account_id, @duration, @time_stamp)
      RollupBacklogJob.work(@account_id, @duration, @time_stamp)
    end
    it "does not crash while logging" do
      assert RollupBacklogJob.args_to_log(@account_id, @duration, @time_stamp)
    end
  end
end
