require_relative "../../support/job_helper"

SingleCov.covered!

describe RecordCounterJob do
  fixtures :accounts, :users, :organizations

  let(:account) { accounts(:minimum) }
  let(:record_counter_class) { 'Zendesk::RecordCounter::Tickets' }
  let(:organization) { organizations(:minimum_organization1) }
  let(:current_user) { users(:minimum_admin) }
  let(:options) do
    {
      organization_id: organization.id,
      current_user_id: current_user.id
    }
  end
  let(:job) do
    RecordCounterJob.new(
      account_id: account.id,
      record_counter_class: record_counter_class,
      options: options
    )
  end
  let(:cache_key) { job.record_counter.cache_key }
  let(:cache_value) { job.record_counter.cache_value }

  describe "#work" do
    it "writes the correct cache key" do
      assert_nil Rails.cache.read(cache_key)

      job.perform

      assert_equal cache_value, Rails.cache.read(cache_key)
    end
  end

  describe "#single_execution_key" do
    it "returns the correct cache key" do
      single_execution_key = RecordCounterJob.single_execution_key(
        account_id: account.id,
        record_counter_class: record_counter_class,
        options: options
      )

      assert_equal single_execution_key, cache_key
    end
  end

  describe "#single_execution_key_expiration" do
    it "returns the correct expiration" do
      assert_equal 1.minute, RecordCounterJob.single_execution_key_expiration
    end
  end
end
