require_relative "../../support/job_helper"
require_relative "../../support/salesforce_test_helper"
require 'salesforce_integration'

SingleCov.covered! uncovered: 1

describe 'SalesforceSyncJob' do
  include SalesforceTestHelper

  fixtures :accounts, :users

  describe "#perform" do
    before do
      @user = users(:minimum_end_user)
      @user.salesforce_data = SalesforceData.create(user: @user, sync_status: ExternalUserData::SyncStatus::SYNC_PENDING)
    end

    it "syncs data" do
      salesforce_integration = mock
      salesforce_integration.expects(:fetch_info_for).returns(records: [:foo])
      Account.any_instance.stubs(:salesforce_integration).returns(salesforce_integration)
      SalesforceSyncJob.perform(@user.account_id, @user.id)

      @user.salesforce_data.reload
      assert_equal @user.salesforce_data.data, records: [:foo]
      assert_equal ExternalUserData::SyncStatus::SYNC_OK, @user.salesforce_data.sync_status
    end

    it_syncs_errored(raises: Salesforce::Integration::LoginFailed, error: :login_failed)
    it_syncs_errored(raises: Salesforce::NextGeneration::RefreshTokenError, error: :refresh_token_error)
    it_syncs_errored(raises: fault_error, error: :soap_fault)
    it_syncs_errored(raises: HTTPClient::ConnectTimeoutError, error: :connect_timeout)
    it_syncs_errored(raises: HTTPClient::ReceiveTimeoutError, error: :receive_timeout)
    it_syncs_errored(raises: Resolv::ResolvError, error: :sf_subdomain_resolv_failed)
    it_syncs_errored(raises: socket_dns_error, error: :sf_subdomain_resolv_failed)
    it_syncs_errored(raises: RuntimeError, error: :error, bubbles: true)
  end
end
