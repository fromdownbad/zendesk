require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 1

describe CreateFeatureOnLocalPodJob do
  describe '.work' do
    it 'executes the subscription feature services for each account' do
      Zendesk::Features::SubscriptionFeatureService.any_instance.
        expects(:execute!).at_least(1)

      CreateFeatureOnLocalPodJob.work(1)
    end
  end
end
