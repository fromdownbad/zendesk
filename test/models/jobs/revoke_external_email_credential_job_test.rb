require_relative "../../support/job_helper"

SingleCov.covered!

describe 'RevokeExternalEmailCredentialJob' do
  fixtures :accounts, :account_settings, :users, :recipient_addresses, :brands

  let(:unencrypted_token) { 'secret_oauth_token' }
  let(:credential) do
    ExternalEmailCredential.new(
      account: accounts(:minimum),
      username: 'foo@bar.com',
      current_user: users(:minimum_agent)
    ).tap do |external_email_credential|
      external_email_credential.encrypted_value = unencrypted_token
      external_email_credential.save!
    end
  end

  describe ".work" do
    subject { RevokeExternalEmailCredentialJob }

    it 'works an instance of the job' do
      subject.any_instance.expects(:work)
      subject.work(credential.encrypted_value, credential.encryption_key_name, credential.encryption_cipher_name)
    end
  end

  describe '#work' do
    subject { RevokeExternalEmailCredentialJob.new(credential.encrypted_value, credential.encryption_key_name, credential.encryption_cipher_name) }

    it 'makes a request to Google oauth2 revoke endpoint with the unencrypted credential' do
      expected_uri = "https://accounts.google.com/o/oauth2/revoke?token=#{unencrypted_token}"

      stub_get = stub_request(:get, expected_uri).
        to_return(status: 200, body: { message: 'OK' }.to_json, headers: {'Content-Type' => 'application/json'})

      subject.work
      assert_requested(stub_get)
    end

    it 'logs that it has successfully revoked the token' do
      Rails.logger.stubs(:info)
      Rails.logger.expects(:info).with("[RevokeExternalEmailCredentialJob] Successfully revoked token.").once
      subject.work
    end

    describe 'when the response does not have a HTTP success code' do
      before do
        body = { error: 'error', description: 'Invalid value' }.to_json

        stub_request(:get, %r{https://accounts.google.com/o/oauth2/revoke}).
          to_return(status: 500, body: body, headers: {'Content-Type' => 'application/json'})
      end

      it 'raises' do
        assert_raises(RevokeExternalEmailCredentialJob::RevokeExternalEmailCredentialError, "[RevokeExternalEmailCredentialJob] Failed. error - Invalid value") do
          subject.work
        end
      end

      describe 'and the error description says the token is revoked' do
        before do
          body = { error: 'invalid_token', error_description: 'Token expired or revoked' }.to_json

          stub_request(:get, %r{https://accounts.google.com/o/oauth2/revoke}).
            to_return(status: 400, body: body, headers: {'Content-Type' => 'application/json'})
        end

        it 'logs' do
          Rails.logger.stubs(:info)
          Rails.logger.expects(:info).with("[RevokeExternalEmailCredentialJob] Token already revoked.").once
          subject.work
        end

        it 'is successful' do
          assert subject.work
        end
      end

      describe 'and the error is invalid_token' do
        before do
          body = { error: 'invalid_token' }.to_json

          stub_request(:get, %r{https://accounts.google.com/o/oauth2/revoke}).
            to_return(status: 500, body: body, headers: {'Content-Type' => 'application/json'})
        end

        it 'logs' do
          Rails.logger.stubs(:info)
          Rails.logger.expects(:info).with("[RevokeExternalEmailCredentialJob] Token already invalidated.").once
          subject.work
        end

        it 'is successful' do
          assert subject.work
        end
      end
    end
  end
end
