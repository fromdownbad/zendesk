require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'AddAllAgentsToDefaultGroupJob' do
  fixtures :accounts, :users

  let(:account) { accounts(:with_groups) }
  let(:default_group) { groups(:with_groups_group1) }
  let(:old_group) { groups(:with_groups_group2) }
  let(:agent1) { users(:with_groups_agent1) }
  let(:agent2) { users(:with_groups_agent2) }

  describe "#perform" do
    setup do
      agent1.current_user = agent1 # required by agent1.remove_group_memberships
      agent1.add_group_memberships [old_group.id]
      agent1.default_group_id = old_group.id
      agent1.remove_group_memberships [default_group.id]
      agent1.save!

      refute agent1.reload.in_group?(default_group.id)
    end

    it "makes the agent default group match the account default group" do
      AddAllAgentsToDefaultGroupJob.perform(account.id)

      assert_equal(agent1.reload.default_group_id, default_group.id)
      assert_equal(agent2.reload.default_group_id, default_group.id)
    end

    it "leaves agents in their old groups" do
      AddAllAgentsToDefaultGroupJob.perform(account.id)

      assert agent1.reload.in_group?(old_group.id)
    end
  end
end
