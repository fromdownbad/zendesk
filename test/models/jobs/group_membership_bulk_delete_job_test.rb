require_relative "../../support/job_helper"

SingleCov.covered!

describe GroupMembershipBulkDeleteJob do
  fixtures :accounts, :memberships

  let(:account) { accounts(:minimum) }
  let(:group_membership1) { memberships(:minimum) }
  let(:group_membership2) { memberships(:minimum_admin) }
  let(:job) do
    GroupMembershipBulkDeleteJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      ids: [group_membership1.id, group_membership2.id])
  end

  describe "#perform" do
    describe "with valid ids" do
      before do
        UnassignTicketsJob.expects(:enqueue).twice
        job.perform
      end

      it "deletes group memberships" do
        assert_nil account.memberships.find_by_id(group_membership1.id)
        assert_nil account.memberships.find_by_id(group_membership2.id)
      end
    end

    describe "with invalid ids" do
      before do
        job.options[:ids] = [1, 2]
        job.perform
      end

      it "returns error" do
        assert_includes job.status["results"], "id" => 1, "error" => "GroupMembershipNotFound"
        assert_includes job.status["results"], "id" => 2, "error" => "GroupMembershipNotFound"
      end
    end
  end
end
