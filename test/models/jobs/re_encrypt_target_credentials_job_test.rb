require_relative "../../support/job_helper"
require 'zendesk/encryptable'

SingleCov.covered!

describe 'ReEncryptTargetCredentialsJob' do
  let(:account) { accounts(:minimum) }
  let(:title) { 'my target' }
  let(:unencrypted_credential) { 'plain_text_password' }
  let(:old_encryption_key_name) { 'TARGET_CREDENTIALS_ENCRYPTION_TEST_KEY_B' }
  let(:new_encryption_key_name) { ENV.fetch('TARGET_CREDENTIALS_ENCRYPTION_KEY') }
  let(:cipher) { ENV.fetch('TARGET_CREDENTIALS_ENCRYPTION_CIPHER_NAME') }

  describe ".work" do
    subject { ReEncryptTargetCredentialsJob }

    it 'works an instance of the job' do
      subject.any_instance.expects(:work)
      subject.work(account.id)
    end
  end

  describe ".args_to_log" do
    let(:account_id) { 1 }

    it "logs the account ID" do
      assert_equal({ account_id: account_id }, ReEncryptExternalEmailCredentialJob.args_to_log(account_id))
    end
  end

  describe '#work' do
    subject { ReEncryptTargetCredentialsJob.new(account.id) }

    describe 'when a password is present' do
      before do
        target = create_target_with(old_encryption_key_name)
        target.encrypted_password = unencrypted_credential
        target.encrypt!
        target.save!
      end

      it 'should re-encrypt the password' do
        password_before_re_encrypt = target.encrypted_password

        subject.work

        password_after_re_encrypt = target.encrypted_password

        refute_equal password_before_re_encrypt, password_after_re_encrypt
      end

      it 'should set the encryption key name to the new key' do
        subject.work

        assert target.encryption_key_name, new_encryption_key_name
      end

      it 'should encrypt the password with a new encryption key' do
        subject.work

        assert target.decrypted_value_for(:encrypted_password), unencrypted_credential
      end
    end

    describe 'when a token is present' do
      before do
        target = create_target_with(old_encryption_key_name)
        target.encrypted_token = unencrypted_credential
        target.encrypt!
        target.save!
      end

      it 'should re-encrypt the token ' do
        token_before_re_encrypt = target.encrypted_token

        subject.work

        token_after_re_encrypt = target.encrypted_token

        refute_equal token_before_re_encrypt, token_after_re_encrypt
      end

      it 'should set the encryption key name to the new key' do
        subject.work

        assert target.encryption_key_name, new_encryption_key_name
      end

      it 'should encrypt the token with a new encryption key' do
        subject.work

        assert target.decrypted_value_for(:encrypted_token), unencrypted_credential
      end
    end

    describe 'when a secret value is present' do
      before do
        target = create_target_with(old_encryption_key_name)
        target.encrypted_secret_value = unencrypted_credential
        target.encrypt!
        target.save!
      end

      it 'should re-encrypt the encrypted secret value' do
        secret_before_re_encrypt = target.encrypted_secret_value

        subject.work

        secret_value_after_re_encrypt = target.encrypted_secret_value

        refute_equal secret_before_re_encrypt, secret_value_after_re_encrypt
      end

      it 'should set the encryption key name to the new key' do
        subject.work

        assert target.encryption_key_name, new_encryption_key_name
      end

      it 'should encrypt the secret value with new encryption key' do
        subject.work

        assert target.decrypted_value_for(:encrypted_secret_value), unencrypted_credential
      end
    end

    describe 'when no encryptable value is present' do
      before { Target.create(title: title, account: account) }

      it 'does not run encryption on target' do
        subject.work

        # using read_attribute to get stored value, since encryptable gem overrides method
        refute target.read_attribute(:encryption_key_name)
        refute target.encrypted_password
        refute target.encrypted_secret_value
        refute target.encrypted_token
      end
    end

    describe 'when encrypted value is already encrypted with current key' do
      before do
        target = create_target_with(new_encryption_key_name)
        target.encrypted_secret_value = unencrypted_credential
        target.encrypt!
        target.save!
      end

      it 'does not run encryption on target' do
        secret_value_before_re_encrypt = target.encrypted_secret_value
        key_before_re_encrypt = target.encryption_key_name

        subject.work

        secret_value_after_re_encrypt = target.encrypted_secret_value
        key_after_re_encrypt = target.encryption_key_name

        assert secret_value_before_re_encrypt, secret_value_after_re_encrypt
        assert key_before_re_encrypt, key_after_re_encrypt
      end
    end

    describe 'when target is an app requirement' do
      before do
        target = create_target_with(old_encryption_key_name)
        target.encrypted_secret_value = unencrypted_credential
        target.encrypt!
        target.save!
        FactoryBot.create(:resource_collection_resource, account_id: target.account_id, resource: target)
      end

      it 'should re-encrypt the encrypted value' do
        secret_before_re_encrypt = target.encrypted_secret_value

        subject.work

        secret_value_after_re_encrypt = target.encrypted_secret_value

        refute_equal secret_before_re_encrypt, secret_value_after_re_encrypt
      end

      after do
        ResourceCollectionResource.where(resource_id: target.id).last.destroy
      end
    end
  end

  private

  def create_target_with(encryption_key_name)
    target = Target.create(account: account, title: title)
    target.encryption_key_name = encryption_key_name
    target.encryption_cipher_name = cipher
    target
  end

  def target
    account.targets.where(title: title).first
  end
end
