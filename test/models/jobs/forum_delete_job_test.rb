require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe ForumDeleteJob do
  fixtures :accounts, :users, :forums, :entries, :posts, :users

  describe "#work" do
    let(:forum) { forums(:solutions) }
    let(:account) { forum.account }
    let!(:entry_count) { forum.entries.count(:all) }

    before do
      ForumDeleteJob.work(account.id, account.owner_id, forum.id)
    end

    it "deletes forum and entries" do
      assert_nil Forum.where(id: forum.id).first
      assert_equal forum, Forum.with_deleted { Forum.find(forum.id) }

      assert_equal 0, Entry.where(forum_id: forum.id).count(:all)
      assert_equal entry_count, Entry.with_deleted { Entry.where(forum_id: forum.id).count(:all) }
    end
  end
end
