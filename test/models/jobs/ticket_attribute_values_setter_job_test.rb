require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe TicketAttributeValuesSetterJob do
  fixtures :accounts, :tickets, :events

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:attribute_value_ids) { ['1', '2'] }
  let(:audit) { ticket.audits.last }
  let(:options) do
    {
      'ticket_id' => ticket.id,
      'attribute_value_ids' => attribute_value_ids,
      'audit_id' => audit.id
      }
  end

  describe '::work' do
    it 'creates instance and calls #save_attributes' do
      setter_stub = stub('TicketAttributeValuesSetterJob')
      setter_stub.expects(:save_attribute_values)
      TicketAttributeValuesSetterJob.expects(:new).with(account, options).returns(setter_stub)
      TicketAttributeValuesSetterJob.work(account.id, options)
    end
  end

  describe '#save_attribute_values' do
    let (:deco_client) { stub('deco client') }
    let (:att_val_fixtures) do
      YAML.load_file(
        "#{Rails.root}/test/files/deco/attributes_and_values.yml"
      )['attributes']
    end
    let (:mock_response) { stub('response', body: {'items' => att_val_fixtures}) }

    it 'saves attribute values to Deco and amends audit' do
      Zendesk::Deco::Client.expects(:new).
        with(account).
        returns(deco_client)
      deco_client.expects(:post).
        with("types/10/instances/#{ticket.id}/bulk_instance_values",
          attribute_value_ids: attribute_value_ids).
        returns(mock_response)

      job = TicketAttributeValuesSetterJob.new(account, options)
      job.save_attribute_values

      audit = ticket.reload.audits.find do |item|
        item.events.find do |event|
          event.is_a? AssociateAttValsEvent
        end
      end
      assert audit
      event = audit.events.find { |item| item.is_a? AssociateAttValsEvent }
      expected = att_val_fixtures.map do |att|
        att['values'].map do |att_val|
          { id: att_val['id'], name: att_val['name'], attribute_id: att_val['attribute_id']}
        end
      end.flatten
      assert_equal expected, event.attribute_values
    end

    describe 'exception raised' do
      it 'logs exception to #error' do
        exception = StandardError.new
        account.stubs(:tickets).raises(exception)
        TicketAttributeValuesSetterJob.
          expects(:resque_error).
          with("Failed to save attribute value IDs for ticket #{ticket.id} on account #{account.id}: #{exception}")

        job = TicketAttributeValuesSetterJob.new(account, options)
        job.save_attribute_values
      end
    end
  end
end
