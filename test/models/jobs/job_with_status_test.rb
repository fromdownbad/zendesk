require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 14

describe JobWithStatus do
  fixtures :accounts, :users

  class JobWithStatusTestJob < JobWithStatus
    attr_accessor :state
    def work
      self.state = CIA.current_transaction
    end
  end

  class JobWithStatusTestJobWithUpdates < JobWithStatus
    priority :medium
    def work
      [0..9].each do |i|
        at(i, 9, "Tick")
      end
      completed(results: "Done")
    end
  end

  class JobWithStatusTestRaiseDatabaseException < JobWithStatus
    def work
      raise ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, 'test'
    end
  end

  let(:account) { accounts(:minimum) }

  it "sets system user" do
    job = JobWithStatusTestJob.new('12345', account_id: account.id)
    job.perform
    expected = {actor: User.system}
    assert_equal expected, job.state
  end

  it "sets captured user" do
    captured = CIA.audit(actor: users(:minimum_agent)) do
      JobWithStatus.capture_environment
    end
    job = JobWithStatusTestJob.new('12345', captured.merge(account_id: account.id))
    job.perform
    expected = {actor: users(:minimum_agent)}
    assert_equal expected, job.state
  end

  it "shards with the current_account shard_id on #perform" do
    account.expects(:on_shard).once
    Account.stubs(:find).with(account.id).returns(account)
    job = JobWithStatusTestJob.new('12345', account_id: account.id)
    job.perform
  end

  it "does not use resque logger, instead use rails logger" do
    job = JobWithStatusTestJob.new('abcd', {})
    assert_equal job.class.logger, Rails.logger
  end

  it "does not have raw SQL error message" do
    job = JobWithStatusTestRaiseDatabaseException.new('12345', account_id: account.id)
    assert_raise ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation do
      job.safe_perform!
    end
    assert_equal "failed", job.status.status
    assert_equal "The task failed because of a database error.", job.status.message
  end

  it "deletes job options except account_id" do
    options = JobWithStatus.options(account, users(:minimum_agent), options: "hello")
    uuid = Resque::Plugins::Status::Hash.get(JobWithStatusTestJobWithUpdates.enqueue(options))['uuid']
    expected_options = {'account_id' => account.id}.inspect
    assert_equal expected_options, Resque::Plugins::Status::Hash.get(uuid)['options'].inspect
  end
end
