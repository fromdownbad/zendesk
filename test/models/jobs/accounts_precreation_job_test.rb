require_relative '../../support/job_helper'

SingleCov.covered! uncovered: 1

describe AccountsPrecreationJob do
  include PerformJob
  extend Api::V2::TestHelper
  fixtures :translation_locales

  describe '.enqueue' do
    let(:classic_params) do
      {
        owner: {
          name: 'Precreated account owner',
          email: 'noreply@zendesk.com',
        },

        address: {
          phone: '123-456-7890'
        },

        account: {
          name:           'z3n Precreated Account',
          source:         'classic',
          subdomain:      subdomain,
          language:       'en-US',
          help_desk_size: '1-9'
        }
      }
    end

    let(:subdomain) { 'z3nprecreated-1234567890' }
    let(:shards) { anything }

    before do
      @statsd_stub = stub_for_statsd
      AccountsPrecreationJob.stubs(statsd_client: @statsd_stub)
      AccountCreationShard.stubs(:where).returns(shards)
      shards.stubs(:reload).returns(shards)
      shards.stubs(:pluck).returns([1])
    end

    it 'creates a classic account' do
      @statsd_stub.expects(:increment).with('count', tags: ['language:english', 'retry:false', 'type:success'])

      AccountsPrecreationJob.work(classic_params, '127.0.0.1', 'us')
      account = Account.find_by_subdomain(subdomain)
      pre_acc = account.pre_account_creation

      assert_not_nil account
      assert_not_nil pre_acc

      assert_equal 'classic', pre_acc.source
      assert_nil pre_acc.bound_at
      assert_equal PreAccountCreation::ProvisionStatus::INIT, pre_acc.status

      assert_equal Accounts::Classic, pre_acc.account.class
      assert_equal DefaultLocaleChooser.choose('en-US').id, account.locale_id

      expected_owner_info = classic_params[:owner]
      assert_equal expected_owner_info[:name], account.owner.name
      assert_equal expected_owner_info[:email], account.owner.email

      expected_account_info = classic_params[:account]
      assert_equal expected_account_info[:name], account.name
      assert_equal expected_account_info[:help_desk_size], account.help_desk_size
      assert_equal false, account.multiproduct

      expected_address_info = classic_params[:address]
      assert_equal expected_address_info[:phone], account.address.phone

      refute account.is_serviceable
    end

    it 'does not enqueue LocaleBulkUpdateJob job' do
      Arturo.disable_feature!(:skip_locale_bulk_update_job)
      LocaleBulkUpdateJob.expects(:enqueue).never
      AccountsPrecreationJob.work(classic_params, '127.0.0.1', 'us')
    end

    it 'logs error to statsd when pre-create account throws an error' do
      Accounts::Classic.any_instance.stubs(:save!).raises('Can not create an account')
      @statsd_stub.expects(:increment).with('count', tags: ['language:english', 'retry:false', 'type:failure'])
      ZendeskExceptions::Logger.expects(:record)

      error = assert_raises RuntimeError do
        AccountsPrecreationJob.work(classic_params, '127.0.0.1', 'us')
      end

      assert_equal 'Can not create an account', error.message
    end

    it 'logs retry status to statsd' do
      AccountsPrecreationJob.stubs(:retry_attempt).returns(1)
      Accounts::Classic.any_instance.stubs(:save!).raises('Can not create an account')
      @statsd_stub.expects(:increment).with('count', tags: ['language:english', 'retry:true', 'type:failure'])
      ZendeskExceptions::Logger.expects(:record).at_least(1)

      assert_raises RuntimeError do
        AccountsPrecreationJob.work(classic_params, '127.0.0.1', 'us')
      end
    end
  end
end
