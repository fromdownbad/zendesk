require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 2

describe 'UnassignTicketsJob' do
  before do
    @account = Account.new
    Account.stubs(:find).returns(@account)
    Ticket.any_instance.stubs(:account).returns(@account)
  end

  describe "#perform" do
    before do
      Timecop.freeze
      @requester_id = Time.now.to_i
      system = User.system
      User.stubs(:find).with(-1).returns(system)
      @current_user = User.new(account: @account)
      User.stubs(:find).with(@requester_id).returns(@current_user)
    end

    it "loads the user that requested the job from the database" do
      User.expects(:find).with(@requester_id).returns(User.new(account: Account.new))
      UnassignTicketsJob.perform(@account.id, Time.now.to_i, Time.now.to_i, @requester_id, ViaType.GROUP_CHANGE)
    end

    it "raises an exception if the requester_id is a system user" do
      user = User.new(account: Account.new)
      user.expects(:is_system_user?).returns(true)
      User.expects(:find).with(@requester_id).returns(user)
      assert_raises(RuntimeError) { UnassignTicketsJob.perform(@account.id, Time.now.to_i, Time.now.to_i, @requester_id, ViaType.GROUP_CHANGE) }
    end

    it "calls update_attributes with the appropriate arguments for each ticket loaded" do
      mock_ticket = Ticket.new do |ticket|
        ticket.expects(:assignee=).with(nil)
        ticket.expects(:will_be_saved_by).with(User.find(@requester_id), via_id: ViaType.GROUP_CHANGE)
        ticket.expects(:save!)
      end
      ActiveRecord::Relation.any_instance.stubs(:find_each).yields(mock_ticket)
      UnassignTicketsJob.perform(@account.id, Time.now.to_i, Time.now.to_i, Time.now.to_i, ViaType.GROUP_CHANGE)
    end

    describe "with auto_assign_upon_solve enabled" do
      before do
        @current_user.account.settings.assign_tickets_upon_solve = true
        @current_user.id = @requester_id

        @other_agent = User.new
        @other_agent.stubs(:id).returns(1234)

        @mock_group = Group.new do |group|
          group.users = group_users
        end
        User.any_instance.stubs(:groups).returns([requester_group])

        @mock_ticket = Ticket.new do |ticket|
          ticket.group = @mock_group
          ticket.status_id = ticket_status_id
          ticket.expects(:assignee=).with(assignee)
          ticket.expects(:will_be_saved_by).with(User.find(@requester_id), via_id: ViaType.GROUP_CHANGE)
          ticket.expects(:save!)
        end
        ActiveRecord::Relation.any_instance.stubs(:find_each).yields(@mock_ticket)
      end

      describe "ticket is open" do
        let(:requester_group) { @mock_group }
        let(:ticket_status_id) { StatusType.OPEN }
        let(:assignee) { nil }
        let(:group_users) { [] }

        it "only reassigns tickets if they're solved" do
          UnassignTicketsJob.perform(@account.id, Time.now.to_i, Time.now.to_i, Time.now.to_i, ViaType.GROUP_CHANGE)
        end
      end

      describe "requester is in the ticket group" do
        let(:requester_group) { @mock_group }
        let(:ticket_status_id) { StatusType.SOLVED }
        let(:assignee) { @current_user }
        let(:group_users) { [] }

        it "assigns the ticket to the requester" do
          UnassignTicketsJob.perform(@account.id, Time.now.to_i, Time.now.to_i, @current_user.id, ViaType.GROUP_CHANGE)
        end
      end

      describe "requester is NOT in the ticket group" do
        let(:requester_group) { nil }
        let(:ticket_status_id) { StatusType.SOLVED }

        describe "ticket group has another agent" do
          let(:assignee) { @other_agent }
          let(:group_users) { [@other_agent] }

          before do
            @mock_ticket.stubs(:group).returns(:foo)
            Ticket.any_instance.stubs(:assignable_agents_in_group).returns(group_users)
          end

          it "assigns the ticket to the first agent in the group" do
            UnassignTicketsJob.perform(@account.id, Time.now.to_i, Time.now.to_i, @current_user.id, ViaType.GROUP_CHANGE)
          end
        end

        describe "ticket group is empty" do
          before do
            @current_user.stubs(:default_group_id).returns(4321)
            @mock_ticket.expects(:group_id=).with(4321)
          end
          let(:assignee) { @current_user }
          let(:group_users) { [] }

          it "assigns the ticket to the requester if ticket group is empty" do
            UnassignTicketsJob.perform(@account.id, Time.now.to_i, Time.now.to_i, @current_user.id, ViaType.GROUP_CHANGE)
          end
        end
      end
    end
  end
end
