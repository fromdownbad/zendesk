require_relative "../../support/job_helper"

SingleCov.covered!

describe BillingRelatedUserDataJob do
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)
    @job = BillingRelatedUserDataJob.new("79a03f83-8dd9-4af1-85bc-787a62037919", account_id: @account.id)
  end

  describe "results" do
    before do
      Timecop.freeze
      ExpirableAttachment.any_instance.expects(:authenticated_s3_url).returns("https://foo")
      stub_request(:put, %r{\.amazonaws.com/data/expirable_attachments/.*_minimum_user_export.csv})
      @results = @job.perform
    end

    it "includes a link to an aws attachment" do
      assert_equal @results.last[:results][:s3url], "https://foo"
    end

    describe "the attachment" do
      before do
        token = @results.last[:results][:token]
        @attachment = ExpirableAttachment.find_by_account_id_and_token!(@account.id, token)
      end

      it "is for the current account" do
        assert_equal @account.id, @attachment.account_id
      end

      it "has an author id of the account owner" do
        assert_equal @account.owner.id, @attachment.author_id
      end
    end
  end

  describe "#row_data" do
    before do
      @result = []
      @job.send(:users).map do |user|
        @result << @job.send(:row_data, user)
      end
      @headers = BillingRelatedUserDataJob::HEADERS
    end

    it "has 1 row for every agent/admin" do
      assert_equal @account.users.size - @account.end_users.size, @result.size
    end

    it "has the correct values (based on headers)" do
      @headers.each do |header|
        scope = User.find(@result.last.first)
        scope = scope.account if header.to_s == "subdomain"
        assert_equal_with_nil scope.send(header), @result.detect { |m| m.first == @result.last.first }[@headers.index(header)]
      end
    end
  end
end
