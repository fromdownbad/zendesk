require_relative "../../../support/job_helper"

SingleCov.covered!

describe Voice::UsageSubscriptionCorrectorJob do
  let(:account) { accounts(:minimum) }

  describe '.perform' do
    subject { Voice::UsageSubscriptionCorrectorJob.perform(account.id) }

    let(:statsd_client) { stub_for_statsd }

    before do
      Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
      statsd_client.stubs(:increment)
      statsd_client.stubs(:timing)
    end

    describe "when the billing_voice_unuploaded_usage_cleanup_job_monitoring is ON" do
      let(:zuora_client)     { mock("ZendeskBillingCore::Zuora::Client") }
      let(:zuora_account_id) { "zuora-account-id" }

      let(:subscription)       { ZBC::Zuora::PlanAdapter::LegacyVoicePlanAdapter.new(zuora_subscription) }
      let(:zuora_subscription) { stub }
      let(:subscription_kind)  { nil }

      let(:current_termed_usage_subscription) do
        OpenStruct.new(
          id:                   101,
          name:                 "ZD-S00111111",
          term_start_date:      3.months.ago,
          term_end_date:        9.months.from_now,
          rate_plans:           [stub]
        )
      end

      let(:expired_termed_usage_subscription) do
        OpenStruct.new(
          id:                   101,
          name:                 "ZD-S00111111",
          term_start_date:      9.months.ago,
          term_end_date:        3.months.ago,
          rate_plans:           [stub]
        )
      end

      let(:future_termed_usage_subscription) do
        OpenStruct.new(
          id:                   101,
          name:                 "ZD-S00111111",
          term_start_date:      3.months.from_now,
          term_end_date:        9.months.from_now,
          rate_plans:           [stub]
        )
      end

      let(:primary_subscription_with_usage_rate_plan) do
        OpenStruct.new(
          id:                   101,
          name:                 "ZD-S00111111",
          term_start_date:      3.months.ago,
          term_end_date:        9.months.from_now,
          rate_plans:           [stub, stub, stub]
        )
      end

      let(:evergreen_usage_subscription) do
        OpenStruct.new(
          id:                   101,
          name:                 "ZD-S00111111",
          term_start_date:      3.months.ago,
          rate_plans:           [stub]
        )
      end

      before do
        Arturo.enable_feature! :billing_voice_unuploaded_usage_cleanup_job_monitoring

        account.subscription.create_zuora_subscription!(
          zuora_account_id:       zuora_account_id,
          plan_type:              account.subscription.plan_type,
          pricing_model_revision: account.subscription.pricing_model_revision,
          max_agents:             account.subscription.max_agents
        )

        zuora_subscription.stubs(:get_custom_field).with(:subscription_kind__c).returns(subscription_kind)

        ZendeskBillingCore::Zuora::Client.stubs(:new).with(zuora_account_id).returns(zuora_client)
        zuora_client.stubs(:get_arrears_voice_subscription!).returns(subscription)
      end

      describe "when the billing_voice_unuploaded_usage_cleanup_job is ON" do
        before do
          Arturo.enable_feature! :billing_voice_unuploaded_usage_cleanup_job
        end

        describe "when an account does NOT have a subscription with usage rate plan" do
          let(:subscription) { nil }

          before do
            ZBC::Zuora::Jobs::AddVoiceJob.stubs(:enqueue).with(account.id)
            zuora_client.expects(:get_arrears_voice_subscription!).once.returns(subscription)
            statsd_client.expects(:timing).once
          end

          it "calls ZBC::Zuora::Jobs::AddVoiceJob" do
            ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).with(account.id)

            subject
          end

          it "increments the metrics for adding a new usage subscription" do
            statsd_client.expects(:increment).with("add_usage_subscription")

            subject
          end
        end

        describe "when an account has a subscription with usage rate plan" do
          before do
            zuora_client.expects(:get_arrears_voice_subscription!).once.returns(subscription)
            statsd_client.expects(:timing).once
          end

          describe "and the kind field is NOT Usage" do
            let(:subscription_kind) { nil }

            let(:update_params) do
              { type: "Subscription", objects: [{ id: subscription.id, subscription_kind__c: "Usage" }] }
            end

            describe "and usage is the only rate plan on the subscription" do
              describe "and subscription starts in the future" do
                let(:zuora_subscription) { future_termed_usage_subscription }

                before do
                  IronBank::Update.stubs(:call).with(update_params)
                end

                it "updates the subscription to have usage kind" do
                  IronBank::Update.expects(:call).with(update_params)

                  subject
                end

                it "increments the metrics for adding kind to a usage subscription" do
                  statsd_client.expects(:increment).with("update_kind_to_usage")

                  subject
                end
              end

              describe "and subscription is currently active" do
                let(:zuora_subscription) { current_termed_usage_subscription }

                before do
                  IronBank::Update.stubs(:call).with(update_params)
                end

                it "updates the subscription to have usage kind" do
                  IronBank::Update.expects(:call).with(update_params)

                  subject
                end

                it "increments the metrics for adding kind to a usage subscription" do
                  statsd_client.expects(:increment).with("update_kind_to_usage")

                  subject
                end
              end

              describe "and subscription is expired" do
                let(:zuora_subscription) { expired_termed_usage_subscription }

                before do
                  ZBC::Zuora::Jobs::AddVoiceJob.stubs(:enqueue).with(account.id)
                end

                it "calls ZBC::Zuora::Jobs::AddVoiceJob" do
                  ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).with(account.id)

                  subject
                end

                it "increments the metrics for adding a new usage subscription" do
                  statsd_client.expects(:increment).with("add_usage_subscription")

                  subject
                end
              end

              describe "and subscription is evergreen" do
                let(:zuora_subscription) { evergreen_usage_subscription }

                before do
                  IronBank::Update.stubs(:call).with(update_params)
                end

                it "updates the subscription to have usage kind" do
                  IronBank::Update.expects(:call).with(update_params)

                  subject
                end

                it "increments the metrics for adding kind to a usage subscription" do
                  statsd_client.expects(:increment).with("update_kind_to_usage")

                  subject
                end
              end
            end

            # Usage rate plan is incorrectly placed on a subscription with other rate plans
            describe "and usage is NOT the only rate plan on the subscription" do
              let(:zuora_subscription) { primary_subscription_with_usage_rate_plan }

              it "does NOT update the subscription to have usage kind" do
                IronBank::Update.expects(:call).never

                subject
              end

              it "does NOT call ZBC::Zuora::Jobs::AddVoiceJob" do
                ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never

                subject
              end

              it "does NOT increment the metrics for adding kind to a usage subscription" do
                statsd_client.expects(:increment).never

                subject
              end
            end
          end

          # Usage subscription is already correctly configured, so should not do anything
          describe "and the kind field is set to Usage" do
            let(:subscription_kind) { "Usage" }

            describe "and subscription is currently active" do
              let(:zuora_subscription) { current_termed_usage_subscription }

              it "does NOT update the subscription to have usage kind" do
                IronBank::Update.expects(:call).never

                subject
              end

              it "does NOT call ZBC::Zuora::Jobs::AddVoiceJob" do
                ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never

                subject
              end

              it "does NOT increment the metrics for adding kind to a usage subscription" do
                statsd_client.expects(:increment).never

                subject
              end
            end

            describe "and subscription is expired" do
              let(:zuora_subscription) { expired_termed_usage_subscription }

              before do
                ZBC::Zuora::Jobs::AddVoiceJob.stubs(:enqueue).with(account.id)
              end

              it "calls ZBC::Zuora::Jobs::AddVoiceJob" do
                ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).with(account.id)

                subject
              end

              it "increments the metrics for adding a new usage subscription" do
                statsd_client.expects(:increment).with("add_usage_subscription")

                subject
              end
            end

            describe "and subscription starts in the future" do
              let(:zuora_subscription) { future_termed_usage_subscription }

              it "does NOT update the subscription to have usage kind" do
                IronBank::Update.expects(:call).never

                subject
              end

              it "does NOT call ZBC::Zuora::Jobs::AddVoiceJob" do
                ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never

                subject
              end

              it "does NOT increment the metrics for adding kind to a usage subscription" do
                statsd_client.expects(:increment).never

                subject
              end
            end

            describe "and subscription is evergreen" do
              let(:zuora_subscription) { evergreen_usage_subscription }

              it "does NOT update the subscription to have usage kind" do
                IronBank::Update.expects(:call).never

                subject
              end

              it "does NOT call ZBC::Zuora::Jobs::AddVoiceJob" do
                ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never

                subject
              end

              it "does NOT increment the metrics for adding kind to a usage subscription" do
                statsd_client.expects(:increment).never

                subject
              end
            end
          end
        end

        describe "error-handling" do
          describe "when there is a failure correcting" do
            it "increments the correct_subscription_error" do
              zuora_client.stubs(:get_arrears_voice_subscription!).raises(StandardError)

              statsd_client.expects(:increment).with("correct_subscription_error")

              subject
            end
          end

          describe "when there is a failure updating usage kind" do
            let(:zuora_subscription) { future_termed_usage_subscription } # To trigger a usage kind update attempt.

            it "increments the update_kind_to_usage_error counter" do
              IronBank::Update.stubs(:call).raises(StandardError)

              statsd_client.expects(:increment).with("update_kind_to_usage_error")

              subject
            end
          end

          describe "when there is a failure adding a usage subscription" do
            let(:zuora_subscription) { expired_termed_usage_subscription } # To trigger a subscription addition attempt.

            it "increments the add_usage_subscription_error counter" do
              ZBC::Zuora::Jobs::AddVoiceJob.stubs(:enqueue).raises(StandardError)

              statsd_client.expects(:increment).with("add_usage_subscription_error")

              subject
            end
          end
        end
      end

      describe "when the billing_voice_unuploaded_usage_cleanup_job is OFF" do
        before do
          Arturo.disable_feature! :billing_voice_unuploaded_usage_cleanup_job
          zuora_client.expects(:get_arrears_voice_subscription!).once.returns(subscription)
          statsd_client.expects(:timing).once
        end

        describe "when an account does NOT have a subscription with usage rate plan" do
          let(:subscription) { nil }

          it "does NOT call ZBC::Zuora::Jobs::AddVoiceJob" do
            ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never

            subject
          end

          it "increments the metrics for adding a new usage subscription" do
            statsd_client.expects(:increment).with("add_usage_subscription")

            subject
          end
        end

        describe "when an account has a subscription with usage rate plan" do
          describe "and the kind field is NOT Usage" do
            let(:subscription_kind) { nil }

            describe "and usage is the only rate plan on the subscription" do
              let(:zuora_subscription) { current_termed_usage_subscription }

              it "does NOT update the subscription to have usage kind" do
                IronBank::Update.expects(:call).never

                subject
              end

              it "does NOT call ZBC::Zuora::Jobs::AddVoiceJob" do
                ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never

                subject
              end

              it "increments the metrics for adding kind to a usage subscription" do
                statsd_client.expects(:increment).with("update_kind_to_usage")

                subject
              end
            end

            # Usage rate plan is incorrectly placed on a subscription with other rate plans
            describe "and usage is NOT the only rate plan on the subscription" do
              let(:zuora_subscription) { primary_subscription_with_usage_rate_plan }

              it "does NOT update the subscription to have usage kind" do
                IronBank::Update.expects(:call).never

                subject
              end

              it "does NOT call ZBC::Zuora::Jobs::AddVoiceJob" do
                ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never

                subject
              end

              it "does NOT increment the metrics for adding kind to a usage subscription" do
                statsd_client.expects(:increment).never

                subject
              end
            end
          end

          # Usage subscription is already correctly configured, so should not do anything
          describe "and the kind field is set to Usage" do
            let(:zuora_subscription) { current_termed_usage_subscription }
            let(:subscription_kind)  { "Usage" }

            it "does NOT update the subscription to have usage kind" do
              IronBank::Update.expects(:call).never

              subject
            end

            it "does NOT call ZBC::Zuora::Jobs::AddVoiceJob" do
              ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never

              subject
            end

            it "does NOT increment the metrics for adding kind to a usage subscription" do
              statsd_client.expects(:increment).never

              subject
            end
          end
        end
      end
    end

    describe "when the billing_voice_unuploaded_usage_cleanup_job_monitoring is OFF" do
      before { Arturo.disable_feature! :billing_voice_unuploaded_usage_cleanup_job_monitoring }

      it "does nothing" do
        ZBC::Zuora::Jobs::AddVoiceJob.expects(:enqueue).never
        IronBank::Update.expects(:call).never
        statsd_client.expects(:increment).never

        subject
      end
    end
  end

  describe '.args_to_log' do
    it "works" do
      assert_equal({ account_id: account.id }, Voice::UsageSubscriptionCorrectorJob.args_to_log(account.id))
    end
  end
end
