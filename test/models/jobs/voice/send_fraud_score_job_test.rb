require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

module Voice
  describe SendFraudScoreJob do
    describe '#perform' do
      let(:account) { accounts(:minimum) }

      it 'sends API request to notify voice of fraud score change' do
        Zendesk::Voice::InternalApiClient.
          any_instance.
          expects(:voice_fraud_score).
          with(account, true)

        SendFraudScoreJob.perform(account.id, true)
      end

      it 'does not bother with fraud score change for inactive accounts' do
        Account.any_instance.stubs(:is_active?).returns(false)

        Zendesk::Voice::InternalApiClient.expects(:new).never

        SendFraudScoreJob.perform(account.id, true)
      end

      it 'does not bother with fraud score change when risky is false' do
        Zendesk::Voice::InternalApiClient.expects(:new).never

        SendFraudScoreJob.perform(account.id, false)
      end
    end
  end
end
