require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

module Voice
  describe CreateVoiceJob do
    describe '#perform' do
      let(:account) { accounts(:minimum) }
      let(:subscription) { FactoryBot.build(:voice_subscription, account: account) }
      let!(:stubbed_req) do
        stub_request(:put, 'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/sub_accounts/subscription_created.json')
      end

      it 'sends an API request to voice to create the voice sub account' do
        CreateVoiceJob.perform(account.id, subscription.plan_type)
        assert_requested(stubbed_req)
      end
    end
  end
end
