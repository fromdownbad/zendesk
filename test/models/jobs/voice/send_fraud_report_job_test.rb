require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

module Voice
  describe SendFraudReportJob do
    describe '#perform' do
      let(:account) { accounts(:minimum) }
      let(:data) { { fraud_score: 90 } }

      it 'sends API request to notify voice of fraud report change' do
        Zendesk::Voice::InternalApiClient.
          any_instance.
          expects(:voice_fraud_report).
          with(account, data)
        SendFraudReportJob.perform(account.id, data[:fraud_score])
      end

      it 'does not bother with inactive accounts' do
        Account.any_instance.stubs(:is_active?).returns(false)
        Zendesk::Voice::InternalApiClient.expects(:new).never
        SendFraudReportJob.perform(account.id, data[:fraud_score])
      end
    end
  end
end
