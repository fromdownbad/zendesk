require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

describe Voice::NotifyVoiceSeatIsDestroyed do
  let!(:account) { accounts(:minimum) }
  let!(:agent) { users(:minimum_agent) }

  describe '#perform' do
    let!(:request) { stub_request(:post, %r{/api/v2/channels/voice/availabilities/#{agent.id}/voice_seat_lost.json}) }

    it 'notifies voice that the voice seat was lost by the user' do
      Voice::NotifyVoiceSeatIsDestroyed.perform(account.id, agent.id)
      assert_requested(request)
    end

    it 'handles a network error' do
      request.to_return(status: 502)
      ZendeskExceptions::Logger.expects(:record)

      Voice::NotifyVoiceSeatIsDestroyed.perform(account.id, agent.id)
      assert_requested(request, times: 3)
    end

    it 'handles a not found error' do
      request.to_return(status: 404)
      Voice::NotifyVoiceSeatIsDestroyed.perform(account.id, agent.id)
    end
  end
end
