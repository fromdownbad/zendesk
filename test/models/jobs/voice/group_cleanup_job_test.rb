require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

module Voice
  describe "GroupCleanupJob" do
    describe "#perform" do
      let(:account) { accounts(:minimum) }
      let(:group)   { FactoryBot.create(:group, account: account) }

      it "sends an API request to voice with the correct group" do
        Zendesk::Voice::InternalApiClient.any_instance.expects(:remove_from_routing).with(group.id)
        GroupCleanupJob.perform(account.id, group.id)
      end
    end
  end
end
