require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

module Voice
  describe UpdateVoiceJob do
    describe '#perform' do
      let(:account) { accounts(:minimum) }
      let(:subscription) { FactoryBot.build(:voice_subscription, account: account) }
      let(:data) { { plan_type: subscription.plan_type } }

      it 'sends API request to notify voice of subscription change' do
        Zendesk::Voice::InternalApiClient.
          any_instance.
          expects(:voice_subscription_updated).
          with(data)
        UpdateVoiceJob.perform(account.id, data)
      end
    end
  end
end
