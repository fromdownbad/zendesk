require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

module Voice
  describe ActivateVoiceJob do
    describe '#perform' do
      let(:account) { accounts(:minimum) }
      let!(:stubbed_req) do
        stub_request(:put, 'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/sub_accounts/activate.json')
      end

      it 'sends an API request to voice to activate the voice sub account' do
        ActivateVoiceJob.perform(account.id)
        assert_requested(stubbed_req)
      end
    end
  end
end
