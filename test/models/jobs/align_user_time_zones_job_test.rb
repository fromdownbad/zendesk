require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'AlignUserTimeZonesJob' do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
  end

  describe "#perform" do
    before do
      assert(@account.update(time_zone: 'Zagreb'))
      assert_equal 0, @account.users.where(time_zone: @account.time_zone).count(:all)
    end

    it "changes time zones of users to that of the account" do
      AlignUserTimeZonesJob.perform(@account.id)
      assert_equal @account.users.count(:all), @account.users.where('time_zone IS NULL OR time_zone = ?', @account.time_zone).count(:all)
    end
  end
end
