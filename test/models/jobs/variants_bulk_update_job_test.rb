require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe VariantsBulkUpdateJob do
  fixtures :accounts

  let(:fbattrs) { {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: 1 } }
  let(:account) { accounts(:minimum) }
  let(:text) { Cms::Text.create!(name: "I haz a reference", fallback_attributes: fbattrs, account: account) }
  let(:job_params) { { account_id: account.id, user_id: account.owner_id, item_id: text.id } }

  before do
    @variant = text.variants.last
    @second_variant = text.variants.create!(value: "Content B", translation_locale_id: 4, active: true, is_fallback: false)
    assert_equal 2, text.variants.count(:all)
  end

  describe "with a valid id" do
    let(:params) do
      {variants: [
        { content: "New content!",       id: @variant.id},
        { content: "New content for B!", id: @second_variant.id}
      ]}
    end

    before do
      @job = VariantsBulkUpdateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919", job_params.merge(params))
      @results = @job.perform
    end

    it "updates content of item" do
      assert_equal @variant.reload.value,        "New content!"
      assert_equal @second_variant.reload.value, "New content for B!"
    end

    it "updates content of dc content translation" do
      snippet = DC::Snippet.find_by_identifier(text.identifier)
      assert_equal snippet.default_value, "New content!"
    end
  end

  describe "with an invalid id" do
    let(:params) do
      {variants: [{ content: "New content!", id: 12345}]}
    end

    before do
      @job = VariantsBulkUpdateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919", job_params.merge(params))
      @results = @job.perform
    end

    it "rescues error and add it to the results" do
      assert_equal @results.last[:results].first[:errors], "Coudn't find variant with 12345"
    end
  end
end
