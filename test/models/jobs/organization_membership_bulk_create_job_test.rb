require_relative "../../support/job_helper"
require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe OrganizationMembershipBulkCreateJob do
  fixtures :accounts, :users, :organizations

  let(:organization) { organizations(:minimum_organization1) }
  let(:user1) { users(:minimum_agent) }
  let(:user2) { users(:minimum_admin) }
  let(:account) { accounts(:minimum) }
  let(:organization_memberships) do
    [
      { user_id: user1.id, organization_id: organization.id },
      { user_id: user2.id, organization_id: organization.id }
    ]
  end
  let(:job) do
    OrganizationMembershipBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      organization_memberships: organization_memberships)
  end

  describe "#perform" do
    describe "for agent" do
      describe "with no prior organization memberships" do
        describe "with valid arguments" do
          it "should create the organization memberships" do
            job.perform
            assert_equal "completed", job.status.status
            assert_includes user1.organization_memberships.map(&:organization_id), organization.id
            assert_includes user2.organization_memberships.map(&:organization_id), organization.id
          end

          it "moves users' tickets from any organization to new organization" do
            User.any_instance.expects(:move_tickets_to_organization).with(:any, organization.id).twice
            job.perform
          end
        end

        describe "with invalid organization id" do
          let(:organization_memberships) do
            [
              { user_id: user1.id, organization_id: organization.id },
              { user_id: user2.id, organization_id: 0 }
            ]
          end

          it "should return error" do
            job.perform
            assert_equal "completed", job.status.status
            assert_includes job.status["results"], "index" => 1, "error" => "OrganizationNotFound"
          end
        end

        describe "with invalid user id" do
          let(:organization_memberships) do
            [
              { user_id: user1.id, organization_id: organization.id },
              { user_id: 0, organization_id: organization.id }
            ]
          end

          it "should return error" do
            job.perform
            assert_equal "completed", job.status.status
            assert_includes job.status["results"], "index" => 1, "error" => "UserNotFound"
          end
        end

        describe "all tasks failed" do
          let(:organization_memberships) do
            [
              { user_id: user1.id, organization_id: 0 },
              { user_id: user2.id, organization_id: 0 }
            ]
          end

          it "should return error and set status failed" do
            job.perform
            assert_equal "failed", job.status.status
            assert_includes job.status["results"], "index" => 0, "error" => "OrganizationNotFound"
            assert_includes job.status["results"], "index" => 1, "error" => "OrganizationNotFound"
          end
        end
      end

      describe "with prior organizations memberships for a given user" do
        let(:organization2) { organizations(:minimum_organization2) }
        let(:organization3) { organizations(:minimum_organization3) }
        let(:organization_memberships) { [{ user_id: user1.id, organization_id: organization.id }] }

        before do
          user1.identities.delete_all
          user1.identities << UserEmailIdentity.new(user: user1, value: "abc@abc.com")
          user1.organization_memberships.destroy_all
          user1.organization_memberships << OrganizationMembership.new(user: user1, organization: organization2)
          user1.organization_memberships << OrganizationMembership.new(user: user1, organization: organization3)
          user1.save!
        end

        describe "when users are not allowed to belong to multiple organizations" do
          before do
            account.settings.multiple_organizations = false
            account.settings.save!
          end

          it "removes surplus organizations and sets user organization to the remaining one" do
            job.perform
            user1.reload
            assert_equal "completed", job.status.status
            assert_equal 1, user1.organization_memberships.size
            assert(user1.organization_memberships.first.default)
            refute_includes user1.organization_memberships.map(&:organization_id), organization2.id
            refute_includes user1.organization_memberships.map(&:organization_id), organization3.id
            assert_equal organization.id, user1.organization.id
          end

          it "moves user's tickets from previous organizations to new organization, but not from other organizations" do
            User.any_instance.expects(:move_tickets_to_organization).with(organization3.id, organization2.id)
            User.any_instance.expects(:move_tickets_to_organization).with(organization2.id, organization.id)
            User.any_instance.expects(:move_tickets_to_organization).with(:any, organization.id).never
            job.perform
          end
        end

        describe "when users are allowed to belong to multiple organizations" do
          before do
            account.settings.multiple_organizations = true
            account.settings.save!
          end

          it "does not remove existing organizations" do
            job.perform
            user1.reload
            assert_equal "completed", job.status.status
            assert_includes user1.organization_memberships.map(&:organization_id), organization2.id
            assert_includes user1.organization_memberships.map(&:organization_id), organization3.id
            assert_includes user1.organization_memberships.map(&:organization_id), organization.id
          end

          it "does not move user's tickets from any organization to new organization" do
            User.any_instance.expects(:move_tickets_to_organization).never
            job.perform
          end
        end
      end
    end
  end
end
