require_relative "../../support/job_helper"
require_relative "../../support/collaboration_settings_test_helper"

SingleCov.covered! uncovered: 5

describe TicketBulkUpdateJob do
  fixtures :all

  before do
    @account = accounts(:minimum)
    @tickets = @account.tickets.working.includes(TicketBulkUpdateJob.send(:ticket_includes)).to_a
    @job = TicketBulkUpdateJob.new('uuid',
      account_id: @account.id,
      user_id: @account.owner_id,
      ticket_ids: @tickets.map(&:nice_id),
      via_id: 2,
      quiet: true)
    @job.stubs(:current_account).returns(@account)
    @job.stubs(:current_user).returns(@account.owner)
    @job.stubs(:tickets).returns(@tickets.dup)
  end

  describe 'class properties' do
    it 'enforces in flight limit' do
      assert TicketBulkUpdateJob.send(:enforce_in_flight_limit?)
    end
  end

  describe 'with delete action' do
    before do
      @job.options[:submit_type] = 'delete'
    end

    it 'deletes the tickets' do
      @tickets.each do |ticket|
        ticket.expects(:will_be_saved_by).with(@account.owner, via_id: @job.options[:via_id])
        ticket.expects(:soft_delete!)
      end

      @job.perform
    end

    it "deletes the archived ticket" do
      @archived_ticket = tickets(:minimum_5)
      archive_and_delete(@archived_ticket)
      @job.options[:ticket_ids] = [@archived_ticket.nice_id]
      @job.stubs(:tickets).returns([@archived_ticket])
      @archived_ticket.expects(:will_be_saved_by).with(@account.owner, via_id: @job.options[:via_id])
      @archived_ticket.expects(:soft_delete!)
      @job.perform
    end

    it 'records CIA audits' do
      @job.options.merge!(TicketBulkUpdateJob.capture_environment)
      @job.perform
      @tickets.each do |ticket|
        assert ticket.cia_events.any?
      end
    end
  end

  describe 'with mark_as_spam' do
    before do
      @job.options[:mark_as_spam] = true
    end

    it 'marks the tickets as spam' do
      @tickets.each do |ticket|
        ticket.expects(:mark_as_spam_by!).with(@account.owner)
      end

      @job.perform
    end

    describe "with a shared ticket" do
      it "updates results with an error" do
        @tickets.first.shared_tickets.expects(:any?).returns(true)

        result = @job.perform
        assert result.last[:results].map { |m| m[:errors] }.include?("The requester is a shared user and cannot be suspended.")
      end
    end
  end

  describe 'with update action' do
    before do
      @job.options[:submit_type] = 'update'
    end

    describe 'refetching the ticket' do
      let(:delete_or_update_ticket_calls) { TicketBulkUpdateJob::RETRY_ATTEMPTS }

      before do
        # This overrides @tickets so the job only operates on a single ticket.
        # This is to make stubbing calls within the job simpler.
        @tickets = [@tickets.first]
        @job.stubs(:tickets).returns(@tickets)

        # The @tickets array is mutated when the job is executed. This stores
        # off the initial object id so it isn't lost.
        @initial_ticket_object_id = @tickets.first.object_id

        # The delete_or_update_ticket method is called within the retry code
        # block. These two variables are for storing off the id off the ticket
        # object that the method was called with first, and the most recent
        # call.
        @initial_ticket_object_id_call     = nil
        @most_recent_ticket_object_id_call = nil

        @job.
          expects(:delete_or_update_ticket).
          with do |*args|
            @initial_ticket_object_id_call ||= args.first.object_id
            @most_recent_ticket_object_id_call = args.first.object_id
          end.
          times(delete_or_update_ticket_calls).
          raises(
            ZendeskDatabaseSupport::MappedDatabaseExceptions::LostConnection,
            'connection lost'
          )
      end

      it 'does not refetch the ticket on the first attempt' do
        @job.perform

        assert_equal(
          @initial_ticket_object_id,
          @initial_ticket_object_id_call
        )
      end

      it 'refetches the ticket when retrying' do
        @job.perform

        assert_not_equal(
          @initial_ticket_object_id,
          @most_recent_ticket_object_id
        )
      end

      describe 'when fetching the ticket fails' do
        let(:delete_or_update_ticket_calls) { 1 }

        before do
          Ticket.stubs(:where).raises(ActiveRecord::RecordNotFound)
        end

        it 'fails to update' do
          assert_equal false, @job.perform.last[:results].first[:success]
        end
      end
    end

    it 'updates the ticket_attributes' do
      @job.options[:ticket] = { subject: 'hi' }
      @job.perform
      @tickets.each do |ticket|
        assert_equal 'hi', ticket.reload.subject
      end
    end

    describe_with_and_without_arturo_enabled :safe_update_bulk_ticket_jobs do
      let(:valid_stamp) { 3.minutes.ago }
      let(:invalid_stamp) { 7.minutes.ago }

      before do
        @tickets.each do |ticket|
          ticket.update_column(:updated_at, 5.minutes.ago)
        end

        @job.options[:ticket_ids] = @tickets.map(&:nice_id)
      end

      describe "with an updated_stamp" do
        it "passes with a current timestamp" do
          @job.options[:ticket] = { subject: 'whale hello!', safe_update: true, updated_stamp: valid_stamp}
          @job.perform

          @tickets.each do |ticket|
            assert_equal 'whale hello!', ticket.reload.subject
          end
        end

        it "rejects updates with an old timestamp" do
          @job.options[:ticket] = { subject: 'whale hello!', safe_update: true, updated_stamp: invalid_stamp}
          @job.perform

          @tickets.each do |ticket|
            refute_equal 'whale hello!', ticket.reload.subject
          end
        end

        describe_with_arturo_enabled :safe_update_bulk_ticket_jobs do
          before do
            @job.options[:ticket] = { subject: 'whale hello!'}
          end

          it "allows updates if no update has been made since enqueue" do
            @job.options[:enqueued_at] = valid_stamp
            @job.perform

            @tickets.each do |ticket|
              assert_equal 'whale hello!', ticket.reload.subject
            end
          end

          it "automatically rejects if an update has been made since enqueue" do
            @job.options[:enqueued_at] = invalid_stamp
            @job.perform

            @tickets.each do |ticket|
              refute_equal 'whale hello!', ticket.reload.subject
            end
          end

          it "allows partial updates if one update has been made since enqueue" do
            @job.options[:enqueued_at] = invalid_stamp
            valid_ticket = @tickets.shift
            valid_ticket.update_column(:updated_at, invalid_stamp - 10.seconds)

            @job.perform

            @tickets.each do |ticket|
              refute_equal 'whale hello!', ticket.reload.subject
            end

            assert_equal 'whale hello!', valid_ticket.reload.subject
          end
        end
      end
    end

    describe "with the disable_triggers option" do
      it 'triggers are skipped for the ticket updates' do
        Zendesk::Rules::Trigger::Execution.expects(:execute).never

        @job.options[:ticket] = { subject: 'hi' }
        @job.options[:disable_triggers] = true

        @job.perform
      end
    end

    describe "ticket followers" do
      let(:user) { users(:minimum_admin) }
      let(:initial_collaborators) { [{ user_id: user.id, action: :put }] }

      before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

      describe "when `followers` is a Hash" do
        let(:additional_follower) { { "0" => { user_id: follower_id, action: :put } } }

        describe "and user added as a follower is not already an email cc on the ticket" do
          let(:follower_id) { users(:minimum_agent).id }

          before do
            @tickets.each do |ticket|
              ticket.set_email_ccs = initial_collaborators
              ticket.will_be_saved_by(@account.owner)
              ticket.save!
            end
          end

          it "does not report there are no ticket changes" do
            @job.options[:ticket] = { followers: additional_follower }
            result = @job.perform
            @tickets.each { |ticket| assert ticket.followers.map(&:id) == [follower_id] }
            assert_empty result.last[:results].map { |m| m[:errors] }.reject(&:blank?)
          end
        end

        describe "and user added as a follower is not already a follower on the ticket" do
          let(:follower_id) { users(:minimum_agent).id }

          before do
            @tickets.each do |ticket|
              ticket.set_followers = initial_collaborators
              ticket.will_be_saved_by(@account.owner)
              ticket.save!
            end
          end

          it "does not report there are no ticket changes" do
            @job.options[:ticket] = { followers: additional_follower }
            result = @job.perform
            @tickets.each { |ticket| assert ticket.followers.map(&:id) == [user.id, follower_id] }
            assert_empty result.last[:results].map { |m| m[:errors] }.reject(&:blank?)
          end
        end

        describe "and user added as a follower is already an email cc on the ticket" do
          let(:follower_id) { users(:minimum_admin).id }

          before do
            @tickets.each do |ticket|
              ticket.set_email_ccs = [{ user_id: follower_id, action: :put }]
              ticket.will_be_saved_by(@account.owner)
              ticket.save!
            end
          end

          it "does not report there are no ticket changes" do
            @job.options[:ticket] = { followers: additional_follower }
            result = @job.perform
            @tickets.each { |ticket| assert ticket.followers.map(&:id) == [follower_id] }
            assert_empty result.last[:results].map { |m| m[:errors] }.reject(&:blank?)
          end
        end

        describe "and user added as a follower is already a follower on the ticket" do
          let(:follower_id) { users(:minimum_admin).id }

          before do
            @tickets.each do |ticket|
              ticket.set_followers = [{ user_id: follower_id, action: :put }]
              ticket.will_be_saved_by(@account.owner)
              ticket.save!
            end
          end

          it "reports there are no ticket changes" do
            @job.options[:ticket] = { followers: additional_follower }
            result = @job.perform
            @tickets.each { |ticket| assert ticket.followers.map(&:id) == [follower_id] }
            assert result.last[:results].map { |m| m[:errors] }.include?(I18n.t('txt.error_message.models.jobs.ticket_bulk_update_job.no_changes'))
          end
        end
      end

      describe "when user added as a follower is already an email_cc on the ticket" do
        before do
          @tickets.each do |ticket|
            ticket.set_email_ccs = initial_collaborators
            ticket.will_be_saved_by(@account.owner)
            ticket.save!
          end
        end

        it "does not report there are no ticket changes" do
          @job.options[:ticket] = { followers: initial_collaborators }
          result = @job.perform
          @tickets.each { |ticket| assert ticket.followers.map(&:id) == [user.id] }
          assert_empty result.last[:results].map { |m| m[:errors] }.reject(&:blank?)
        end
      end

      describe "when user added as a follower is already a follower on the ticket" do
        before do
          @tickets.each do |ticket|
            ticket.set_followers = initial_collaborators
            ticket.will_be_saved_by(@account.owner)
            ticket.save!
          end
        end

        it "reports there are no ticket changes" do
          @job.options[:ticket] = { followers: initial_collaborators }
          result = @job.perform
          @tickets.each { |ticket| assert ticket.followers.map(&:id) == [user.id] }
          assert result.last[:results].map { |m| m[:errors] }.include?(I18n.t('txt.error_message.models.jobs.ticket_bulk_update_job.no_changes'))
        end
      end
    end

    it 'maps ticket attributes' do
      @job.options[:ticket] = { status: 'pending' }
      @job.perform

      @tickets.each do |ticket|
        assert_equal StatusType.PENDING, ticket.reload.status_id
      end
    end

    it 'maps comment attributes' do
      @job.options[:ticket] = { comment: { public: false, value: "hello" } }
      @job.perform

      @tickets.each do |ticket|
        comment = ticket.latest_comment

        refute comment.is_public?
        assert_equal 'hello', comment.body
      end
    end

    it "does not set a blank subject" do
      @job.options[:ticket] = { subject: '' }
      @job.perform

      @tickets.each do |ticket|
        refute ticket.reload.subject.blank?
      end
    end

    it "does not update a ticket if there are no changes" do
      Ticket.any_instance.expects(:save).never

      @job.options[:ticket] = {}
      @job.perform
    end

    it "correctly interpolate {{ticket.title}} placeholder before saving" do
      @job.options[:ticket] = { subject: "{{ticket.id}} {{ticket.title}}" }
      expected_subjects = @tickets.map { |t| "{{ticket.id}} #{t.subject}" }
      @job.perform

      assert_equal @tickets.map(&:subject), expected_subjects
    end

    it "does not update fields with no-change" do
      text = ticket_fields(:field_text_custom)
      checkbox = ticket_fields(:field_checkbox_custom)

      @job.options[:ticket] = { fields: { checkbox.id => NO_CHANGE.dup, text.id => "baz" } }

      Ticket.any_instance.expects(:fields=).with(text.id => 'baz').times(@tickets.size)

      @job.perform
    end

    describe "when including archived tickets" do
      before { @job.unstub(:tickets) }

      it "does not update and report errors" do
        Arturo.enable_feature!(:include_archived_on_bulk_updates)

        @archived_ticket = tickets(:minimum_5)
        archive_and_delete(@archived_ticket)
        @job.options[:submit_type] = 'foo'
        @job.options[:ticket] = { subject: "whale hello!" }
        @job.options[:ticket_ids] = [@tickets.first.nice_id, @archived_ticket.nice_id]
        result = @job.perform

        assert result.last[:results].map { |m| m[:errors] }.include?("Status: closed prevents ticket update")
      end
    end

    it "normalizes ticket fields" do
      ticket_field = ticket_fields(:field_text_custom)
      fields = [{id: ticket_field.id, value: "baz"}]

      @job.options[:ticket] = {fields: fields}

      Ticket.any_instance.expects(:fields=).with(ticket_field.id => 'baz').times(@tickets.size)

      @job.perform
    end

    # This is a weird test as in there are already N+1 queries like :
    # SELECT COUNT(*) FROM `groups`  WHERE `groups`.`account_id` = 90538 AND (groups.is_active = 1)
    # Adding anything in account / user models fails this test. The job does not cache
    # information between tickets of same account causing these multiple queries
    # FIXME : Remove this test or fix the bulk update job to be more performant in terms of query count
    it "does not do N+1" do
      @tickets.each do |ticket|
        Zendesk::TicketMetric::StateMachine.all.map do |state_machine|
          state_machine.build(ticket)
        end.each(&:advance)
      end

      assert_sql_queries (0..179) do
        @job.perform
      end
    end

    it "adds client info" do
      @job.options[:request] = { remote_ip: "xxx", user_agent: "yyy" }
      @job.options[:ticket] = { subject: "Change me" }
      @job.perform

      @tickets.each do |ticket|
        assert_equal "Client: yyy\nIP address: xxx", ticket.audits.last.client
      end
    end

    it "channels back public comments to facebook" do
      @job.options[:comment] = {"value" => "asdasdasdas", "is_public" => "1"}
      ticket = @tickets.first
      ticket.update_column(:via_id, ViaType.FACEBOOK_POST)
      ::Channels::Converter::ReplyInitiator.any_instance.expects(:reply)
      ::Channels::Converter::ReplyParametersValidator.any_instance.expects(:validate).returns(
        metadata: {
          external_id: "external_id",
          source: {
            external_id: '123123',
            name: "Facebook page"
          },
          text: "Outgoing text"
        },
        source_id: facebook_pages(:minimum_facebook_page_1).id,
        via_id: ticket.via_id
      )
      assert_difference('ChannelBackEvent.count(:all)', 1) do
        @job.perform
      end
    end

    it "enforces a timely check for ability to edit the ticket to avoid concurrency issues" do
      @job.options[:ticket] = { subject: "hello" }
      @account.owner.stubs(:can?).returns(true)
      @account.owner.expects(:can?).with(:edit, is_a(Ticket)).times(@tickets.size)
      @job.perform
    end

    it "sets the selected group when the assignee doesn't belong to that new group" do
      @empty_group = @account.groups.create(name: 'Empty group')
      @job.options[:ticket] = { group_id: @empty_group.id.to_s, assignee_id: "-1" }

      @job.perform

      @tickets.each do |ticket|
        ticket.reload

        assert_equal @empty_group.id, ticket.group_id
        assert_nil ticket.assignee
      end
    end

    it "nevers change assignee to nil when the account has only one group" do
      @job.options[:ticket] = { subject: 'hi', group_id: @account.groups.first.id.to_s, assignee_id: "-1" }

      @job.perform

      @tickets.each do |ticket|
        assert_equal_with_nil ticket.assignee_id, ticket.reload.assignee_id
      end
    end

    it "sets the macro_ids" do
      @job.options[:macro_ids] = [1, 2]
      Ticket.any_instance.expects(:macro_ids=).with([1, 2]).at_least(@tickets.size)
      @job.perform
    end

    describe 'when the macro has the `set_tags` action' do
      let(:macro) do
        rules(:macro_incident_escalation).tap do |macro|
          macro.definition.actions.push(
            DefinitionItem.new('set_tags', nil, %w[only_tag_present])
          )

          macro.owner = group

          macro.save!
        end
      end

      let(:group) { groups(:minimum_group) }
      let(:user)  { @account.owner }

      before do
        GroupMacro.destroy_all

        macro.owner_id = user.account.all_group_ids.max

        macro.save!

        @job.options[:macro_ids] = [macro.id]

        @job.options[:ticket] = {
          remove_tags:     '',
          additional_tags: %w[only_tag_present]
        }
      end

      it 'does not include the tag' do
        @tickets.each do |ticket|
          refute ticket.current_tags.include?('only_tag_present')
        end
      end

      describe 'and the job runs' do
        before { @job.perform }

        it 'sets the tags' do
          @tickets.each do |ticket|
            assert_equal 'only_tag_present', ticket.current_tags
          end
        end
      end
    end

    it "adds tags when macro has add_tags action" do
      @macro = rules(:macro_incident_escalation)
      # TODO: add_tags is an invalid action @ Deepti
      # @macro.definition.actions.push(DefinitionItem.new("add_tags", nil, ["additional_tag"]))
      # @macro.save

      @tickets.each do |ticket|
        refute_includes ticket.current_tags, "additional_tag"
      end

      @job.options[:macro_ids] = [@macro.id]
      @job.options[:ticket] = { remove_tags: "", additional_tags: ["additional_tag"]}
      @job.perform

      # @tickets.reload # TODO this never worked @ Deepti

      @tickets.each do |ticket|
        assert_includes ticket.current_tags, "additional_tag"
      end
    end

    it "sends radar notifications" do
      @tickets.each do |ticket|
        radar_notification = mock('notification')
        RadarFactory.expects(:create_radar_notification).with(@account, "ticket/#{ticket.nice_id}").returns(radar_notification)
        radar_notification.expects(:send).with('updated', anything)
      end

      @job.options[:ticket] = { comment: { public: true, value: "hello" } }
      @job.perform
    end

    describe 'when user can only add private comments to ticket' do
      before do
        create_light_agent
        @job.stubs(:current_user).returns(@light_agent)
        @ticket = @tickets.first
      end

      it 'maps comment attributes if that is the only change' do
        @job.options[:ticket] = { comment: { public: true, value: "hello" } }
        @job.perform

        comment = @ticket.comment

        refute comment.is_public?
        assert_equal 'hello', comment.body
      end

      it 'throws error when they try to update other attributes' do
        @job.options[:ticket] = { subject: 'New subject', comment: { value: "hello" } }
        @job.perform

        assert_not_equal 'New subject', @ticket.reload.subject
      end

      it 'creates comments when html_body is passed' do
        Account.any_instance.stubs(:has_light_agents?).returns(true)

        # just load tickets that can be updated by the light agent
        targets = tickets(:minimum_2, :minimum_3)
        new_ticket = { comment: { html_body: "<div>Something</div>" } }
        ticket_ids = targets.map(&:nice_id)

        @job.stubs(:tickets).returns(targets)
        @job.options.merge!(ticket: new_ticket, ticket_id: ticket_ids)
        @job.perform
        assert @job.status['results'].map { |r| r['success'] }.all?
      end
    end

    describe 'when user does not have tag access' do
      let(:custom_field) { ticket_fields(:field_tagger_custom) }
      let(:custom_checkbox_field) { ticket_fields(:field_checkbox_custom) }
      let(:custom_field_option) { custom_field_options(:field_tagger_custom_option_1) }
      before do
        create_light_agent
        @light_agent.permission_set.permissions.ticket_editing = true
        @light_agent.save!
        @job.stubs(:current_user).returns(@light_agent)

        @ticket = tickets(:minimum_1)
        @job.stubs(:tickets).returns([@ticket])
      end

      it 'prevents tag updates if not whitelisted' do
        @ticket.fields = { custom_field.id => custom_field_option.value }
        @job.options[:ticket] = { remove_tags: "", additional_tags: ["blacklist_tag"]}

        @job.perform
        refute_includes @ticket.reload.current_tags, "blacklist_tag"
      end

      describe 'it allows tag updates if change is whitelisted, ignores previous entries' do
        it 'works for dropdown fields' do
          @ticket.fields = { custom_field.id => custom_field_option.value }
          @job.options[:ticket] = { remove_tags: "", additional_tags: [custom_field_option.value]}
          @job.perform

          assert_includes @ticket.reload.current_tags, custom_field_option.value
          assert_includes @ticket.current_tags, "hello"
        end

        it 'works for checkbox tags' do
          @ticket.fields = { custom_checkbox_field.id => true }
          @job.options[:ticket] = { remove_tags: "", additional_tags: [custom_checkbox_field.tag]}
          @job.perform

          assert_includes @ticket.reload.current_tags, custom_checkbox_field.tag
          assert_includes @ticket.reload.current_tags, "hello"
        end
      end
    end

    describe 'when the ticket update fails' do
      before do
        Ticket.any_instance.stubs(:save).raises(StandardError)
        @job.options[:ticket] = { subject: 'hi' }
      end

      it 'contains results for every ticket' do
        @job.perform
        assert_equal @tickets.count, @job.status['results'].count
      end

      it 'marks each ticket result as a failure' do
        @job.perform
        @job.status['results'].each do |result|
          assert_equal false, result['success']
        end
      end

      it 'retries the ticket `TicketBulkUpdateJob::RETRY_ATTEMPTS` times' do
        times = @tickets.count * TicketBulkUpdateJob::RETRY_ATTEMPTS
        Ticket.any_instance.expects(:save).times(times).
          raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound, 'required_parameter')
        @job.perform
      end

      it 'returns a localized message about the error' do
        Ticket.any_instance.stubs(:save).raises(ArgumentError)
        @job.perform
        @job.status['results'].each do |result|
          assert_equal "The task failed due to an unknown error.", result['errors']
        end
      end

      it 'returns a localized message about a database error' do
        Ticket.any_instance.stubs(:save).
          raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound, 'required_parameter')
        @job.perform
        @job.status['results'].each do |result|
          assert_equal "The task failed because of a database error.", result['errors']
        end
      end

      it 'returns a status in the result' do
        Ticket.any_instance.stubs(:save).raises(ArgumentError)
        @job.perform
        @job.status['results'].each do |result|
          assert result['status']
        end
      end
    end

    describe 'validate_event_integrity_after_ticket_bulk_update Arturo' do
      before do
        # This overrides @tickets so the job only operates on a single ticket.
        # This is to make stubbing calls within the job simpler.
        @tickets = [@tickets.first]
        @job.stubs(:tickets).returns(@tickets)

        def @job.delete_or_update_ticket(ticket)
          # Create some known bad events for testing purposes
          audit = Audit.new.tap do |a|
            a.account_id = ticket.account_id
            a.author = User.first
            a.ticket = ticket
            a.via_id = 5
            a.save!
          end
          audit.update_column(:account_id, 0)
          audit.update_column(:created_at, nil)
          audit.update_column(:updated_at, nil)

          Comment.new.tap do |c|
            c.account_id = ticket.account_id
            c.author = User.first
            c.ticket = ticket
            c.via_id = 5
            c.audit = audit
            c.body = 'testing'
            c.save!
          end

          bad_audit_event = Comment.new.tap do |c|
            c.account_id = ticket.account_id
            c.author = User.first
            c.ticket = ticket
            c.via_id = 5
            c.audit = audit
            c.body = 'testing'
            c.save!
          end
          bad_audit_event.update_column(:account_id, 0)
          bad_audit_event.update_column(:created_at, nil)
          bad_audit_event.update_column(:updated_at, nil)
        end
      end

      describe 'when Arturo not set' do
        before do
          Arturo.disable_feature!(:validate_event_integrity_after_ticket_bulk_update)
        end

        it 'does not log bad events' do
          ZendeskExceptions::Logger.expects(:record).never
          @job.options[:ticket] = { subject: 'hi' }
          @job.perform
        end
      end

      describe 'when Arturo set' do
        before do
          Arturo.enable_feature!(:validate_event_integrity_after_ticket_bulk_update)
        end

        it 'logs bad events' do
          @tickets.each do |t|
            ZendeskExceptions::Logger.expects(:record).with do |exception|
              assert_equal "z1 3687276 1 bad event(s) detected while running TicketBulkUpdateJob for ticket #{t.id}, try 1", exception.message
            end
          end
          @job.options[:ticket] = { subject: 'hi' }
          @job.perform
        end
      end
    end
  end

  describe "#tickets" do
    before do
      @account = accounts(:minimum)
      @tickets = @account.tickets.working.includes(TicketBulkUpdateJob.send(:ticket_includes))
      @job = TicketBulkUpdateJob.new('uuid',
        account_id: @account.id,
        user_id: @account.owner_id,
        ticket_ids: @tickets.map(&:nice_id),
        via_id: 2,
        quiet: true,
        submit_type: 'delete')
      @job.stubs(:current_account).returns(@account)
      @job.stubs(:current_user).returns(@account.owner)
      @archived_ticket = tickets(:minimum_5)
      archive_and_delete(@archived_ticket)
      assert @archived_ticket.reload
    end

    it "should retrieve a set of ticket_ids" do
      assert_equal @tickets.size, @job.send(:tickets, @tickets.map(&:nice_id)).size
    end

    it "should retrieve the archived ticket" do
      assert_equal 1, @job.send(:tickets, [@archived_ticket.nice_id]).size
    end

    describe_with_arturo_disabled :include_archived_on_bulk_updates do
      it "should not return archived tickets for non-delete submit_types" do
        @job.options[:submit_type] = 'foo'
        @job.options[:ticket_ids] = [@tickets.first.nice_id, @archived_ticket.nice_id]
        assert_equal 1, @job.send(:tickets, [@tickets.first.nice_id, @archived_ticket.nice_id]).size
      end
    end

    describe_with_arturo_enabled :include_archived_on_bulk_updates do
      it "should return archived tickets for non-delete submit_types" do
        Arturo.enable_feature!(:include_archived_on_bulk_updates)
        @job.options[:submit_type] = 'foo'
        @job.options[:ticket_ids] = [@tickets.first.nice_id, @archived_ticket.nice_id]
        @job.options[:ticket] = { subject: 'whale hello!' }
        assert_equal 2, @job.send(:tickets, [@tickets.first.nice_id, @archived_ticket.nice_id]).size
      end
    end
  end

  private

  def create_light_agent
    Account.any_instance.stubs(:has_permission_sets?).returns(true)

    @light_agent = User.create! do |user|
      user.account = accounts(:minimum)
      user.name = "Prince Light"
      user.email = "prince@example.org"
      user.roles = Role::AGENT.id
    end

    @light_agent.permission_set = PermissionSet.create_light_agent!(accounts(:minimum))
    @light_agent.permission_set.permissions.ticket_access = "all"
    @light_agent.save!
  end
end
