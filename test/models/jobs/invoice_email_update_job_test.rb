require_relative "../../support/job_helper"
require 'zendesk_billing_core/zuora/client'

SingleCov.covered! uncovered: 1

describe 'InvoiceEmailUpdateJob' do
  describe "#perform" do
    before do
      @zuora_client = stub
      ZendeskBillingCore::Zuora::Client.expects(:new).returns(@zuora_client)
    end

    it "handles nil email_addresses" do
      @zuora_client.expects(:update_invoice_email_addresses).with("")
      InvoiceEmailUpdateJob.perform('test', nil)
    end

    it "handles blank email_addresses" do
      @zuora_client.expects(:update_invoice_email_addresses).with("")
      InvoiceEmailUpdateJob.perform('test', [])
    end

    it "handles single email_address" do
      @email = 'abc@def.com'
      @zuora_client.expects(:update_invoice_email_addresses).with(@email)
      InvoiceEmailUpdateJob.perform('test', [@email])
    end

    it "handles multiple email_addresses" do
      @emails = ['abc@def.com', 'ghi@def.com', 'jkl@def.com']
      @zuora_client.expects(:update_invoice_email_addresses).with(@emails.join(","))
      InvoiceEmailUpdateJob.perform('test', @emails)
    end
  end
end
