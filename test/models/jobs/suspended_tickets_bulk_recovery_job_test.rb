require_relative "../../support/job_helper"
require_relative "../../support/test_helper"

SingleCov.covered!

describe SuspendedTicketsBulkRecoveryJob do
  fixtures :all

  before do
    @account = accounts(:minimum)
    @user = @account.owner
    @suspended_tickets = @account.suspended_tickets.where("cause != ?", 22)
    @job = SuspendedTicketsBulkRecoveryJob.new('uuid',
      account_id: @account.id,
      ids: @suspended_tickets.last(2).map(&:id).join(","),
      author: {id: @user.id, email: @user.email})
    @job.stubs(:current_account).returns(@account)
    @job.stubs(:current_user).returns(@account.owner)
  end

  describe "With N suspended tickets" do
    it "recover N suspended tickets" do
      ticket_count = Ticket.count(:all)
      suspended_tickets_count = SuspendedTicket.count(:all)
      @job.perform
      @job.completed.first["results"].each do |result|
        assert_equal "Recovered", result["status"]
      end
      assert_equal Ticket.count(:all), (ticket_count + 2)
      assert_equal SuspendedTicket.count(:all), (suspended_tickets_count - 2)
    end
  end

  describe "With unrecoverable tickets" do
    before do
      tickets = @account.suspended_tickets.where("cause = ?", 22)
      @job = SuspendedTicketsBulkRecoveryJob.new('uuid',
        account_id: @account.id,
        ids: tickets.map(&:id).join(","))
      @job.stubs(:current_account).returns(@account)
      @job.stubs(:current_user).returns(@account.owner)
    end

    it "should fail to recover" do
      @job.perform
      @job.completed.first["results"].each do |result|
        assert result["status"].include? "Failed recovering suspended ticket id:"
      end
    end
  end

  describe "With recoverable suspended tickets" do
    before do
      @suspended_tickets = @account.suspended_tickets.where("cause != ?", 22)
      @job = SuspendedTicketsBulkRecoveryJob.new('uuid',
        account_id: @account.id,
        ids: @suspended_tickets.map(&:id).join(","))
      @job.stubs(:current_account).returns(@account)
      @job.stubs(:current_user).returns(users(:minimum_admin))
    end

    it "recovers tickets and preserve requester" do
      @job.perform
      Ticket.last(@suspended_tickets.length).each_with_index do |t, i|
        assert_equal t.requester.email, @suspended_tickets[i].from_mail
      end
    end

    describe "that have flags" do
      before do
        @suspended_tickets.each do |ticket|
          ticket.add_flags(AuditFlags.new([EventFlagType.OTHER_USER_UPDATE]), nil)
          ticket.save!
        end
      end

      it "recovers tickets and preserves flags" do
        @job.perform
        assert_equal Ticket.last.audits.last.flags, AuditFlags.new([EventFlagType.OTHER_USER_UPDATE])
      end
    end
  end
end
