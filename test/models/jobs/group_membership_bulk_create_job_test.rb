require_relative "../../support/job_helper"

SingleCov.covered!

describe GroupMembershipBulkCreateJob do
  fixtures :accounts, :groups, :users

  let(:account) { accounts(:minimum) }
  let(:group1) { account.groups.create!(name: "The dingalings") }
  let(:group2) { groups(:minimum_group) }
  let(:agent) { users(:minimum_agent) }
  let(:admin) { users(:minimum_admin) }
  let(:group_memberships) do
    [
      { user_id: agent.id, group_id: group1.id },
      { user_id: admin.id, group_id: group2.id }
    ]
  end
  let(:job) do
    GroupMembershipBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      group_memberships: group_memberships)
  end

  describe "#perform" do
    before { job.perform }

    describe "with valid user_ids and group_ids" do
      it "creates group memberships" do
        assert_equal "completed", job.status.status
        assert_includes agent.memberships.map(&:group_id), group1.id
        assert_includes admin.memberships.map(&:group_id), group2.id
      end
    end

    describe "with invalid user ids" do
      let(:group_memberships) do
        [
          { user_id: agent.id, group_id: group1.id },
          { user_id: 0, group_id: group2.id }
        ]
      end

      it "returns errors" do
        assert_includes job.status["results"], "index" => 1, "error" => "UserNotFound"
      end
    end

    describe "with invalid group ids" do
      let(:group_memberships) do
        [
          { user_id: agent.id, group_id: 0 },
          { user_id: admin.id, group_id: group2.id }
        ]
      end

      it "returns errors" do
        assert_includes job.status["results"], "index" => 0, "error" => "GroupNotFound"
      end
    end

    describe "all tasks failed" do
      let(:group_memberships) do
        [
          { user_id: agent.id, group_id: 0 },
          { user_id: admin.id, group_id: 0 }
        ]
      end

      it "sets status to failed and returns errors" do
        assert_equal "failed", job.status.status
        assert_includes job.status["results"], "index" => 0, "error" => "GroupNotFound"
        assert_includes job.status["results"], "index" => 1, "error" => "GroupNotFound"
      end
    end
  end
end
