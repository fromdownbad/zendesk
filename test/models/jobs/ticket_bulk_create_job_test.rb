require_relative "../../support/job_helper"

SingleCov.covered!

describe TicketBulkCreateJob do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket1) { { "subject" => "Help", "comment" => { "body" => "Need someone's help" } } }
  let(:ticket2) { { "subject" => "Help!", "comment" => { "body" => "Not anyone can help" } } }
  let(:ticket3) { { "subject" => "Help!" } }
  let(:ticket4) { { "requester" => "someone@zd-dev.com", "custom_id_override" => 42, "subject" => "Help", "comment" => { "body" => "Need someone's help" } } }
  let(:job) do
    TicketBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      tickets: [ticket1, ticket2])
  end
  let(:job_that_fails) do
    TicketBulkCreateJob.new("79a03f83-8dd9-4af1-85bc-787a62037918",
      account_id: account.id,
      user_id: account.owner_id,
      tickets: [ticket1, ticket3])
  end
  let(:job_with_via) do
    TicketBulkCreateJob.new("9c2a9909-19e9-48c8-ace7-3ef07fff4742",
      account_id: account.id,
      user_id: account.owner_id,
      tickets: [ticket1, ticket2],
      from_api: true)
  end
  let(:job_with_custom_id_override) do
    TicketBulkCreateJob.new("9c2a9909-19e9-48c8-ace7-3ef07fff4742",
      account_id: account.id,
      user_id: account.owner_id,
      tickets: [ticket4],
      from_api: true)
  end
  let(:job_with_custom_id_override_by_system_user) do
    TicketBulkCreateJob.new("9c2a9909-19e9-48c8-ace7-3ef07fff4742",
      account_id: account.id,
      user_id: -1,
      tickets: [ticket4],
      from_api: true)
  end

  describe 'crowd_control' do
    before { Arturo.enable_feature!(:resque_crowd_control_job_with_status) }
    let(:crowd_control_props) { TicketBulkCreateJob.crowd_control_properties('3ba0e9b8-b44a-4ddc-a073-3004cc65e69e', account_id: 123) }
    let(:account_prop) { crowd_control_props.find { |p| p.name == :account_id } }
    it 'limits on account_id' do
      account_prop.wont_be_nil
      account_prop.value.must_equal 123
    end
  end

  describe "#perform" do
    before do
      TicketBulkCreateJob.any_instance.stubs(:sleep)
    end

    describe "with valid attributes" do
      before do
        job.perform
      end

      it "creates tickets" do
        assert account.tickets.find_by_nice_id(job.status["results"][0]["id"]).presence
        assert account.tickets.find_by_nice_id(job.status["results"][1]["id"]).presence
        assert_equal "Web form", account.tickets.find_by_nice_id(job.status["results"][1]["id"]).via
      end
    end

    describe "with valid attributes (including via)" do
      before do
        job_with_via.perform
      end

      it "creates tickets" do
        assert account.tickets.find_by_nice_id(job_with_via.status["results"][0]["id"]).presence
        assert account.tickets.find_by_nice_id(job_with_via.status["results"][1]["id"]).presence
        assert_equal "Web service", account.tickets.find_by_nice_id(job_with_via.status["results"][1]["id"]).via
      end
    end

    describe "with custom_id_override" do
      before do
        job_with_custom_id_override.perform
      end

      it "creates tickets" do
        result_id = job_with_custom_id_override.status["results"][0]["id"]
        assert account.tickets.find_by_nice_id(result_id).presence
        assert_equal 42, result_id
      end
    end

    describe "with custom_id_override by a system user" do
      before do
        job_with_custom_id_override_by_system_user.perform
      end

      it "creates tickets" do
        result_id = job_with_custom_id_override_by_system_user.status["results"][0]["id"]
        assert account.tickets.find_by_nice_id(result_id).presence
        assert_equal 42, result_id
      end
    end

    describe "all tasks failed" do
      before do
        Ticket.any_instance.stubs(:save!).raises(RuntimeError.new)
        job.perform
      end

      it "should return error and set status failed" do
        assert_equal "failed", job.status.status
        assert_includes job.status["results"], "index" => 0, "error" => "TicketCreateFailed", "details" => ""
        assert_includes job.status["results"], "index" => 1, "error" => "TicketCreateFailed", "details" => ""
      end
    end

    describe "task retries when ticket fails with KeyConstraintViolation" do
      before do
        Ticket.any_instance.expects(:save!).raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, "database exception").
          times(TicketBulkCreateJob::RETRY_COUNT * 2)
        job.perform
      end

      it "try #{TicketBulkCreateJob::RETRY_COUNT * 2} times per ticket and return error" do
        assert_equal "failed", job.status.status
        assert_includes job.status["results"], "index" => 0, "error" => "TicketCreateFailed", "details" => ""
        assert_includes job.status["results"], "index" => 1, "error" => "TicketCreateFailed", "details" => ""
      end
    end

    describe "one task succeeds and one task fails" do
      before do
        job_that_fails.perform
      end

      it "creates one ticket and job succeeds" do
        assert_equal "completed", job_that_fails.status.status
        assert account.tickets.find_by_nice_id(job_that_fails.status["results"][0]["id"]).presence
        assert_includes job_that_fails.status["results"], "index" => 1, "error" => "TicketCreateFailed", "details" => "Enter your request: cannot be blank"
      end
    end

    describe "all tasks completed" do
      before do
        job.perform
      end

      it "should return progress" do
        assert_equal job.status["results"].size, job.status["num"]
      end
    end
  end
end
