require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe TicketFieldEntryDeleteJob do
  fixtures :accounts, :ticket_fields, :ticket_field_entries

  describe '.work' do
    let(:ticket_field) { ticket_fields(:field_text_custom) }

    it "deletes ticket_field_entries for the ticket_field" do
      ticket_field.ticket_field_entries.count(:all).must_equal 2

      TicketFieldEntryDeleteJob.any_instance.expects(:sleep).with(1.0)
      TicketFieldEntryDeleteJob.perform(ticket_field.account_id, ticket_field.id)
      ticket_field.ticket_field_entries.count(:all).must_equal 0

      # But didn't delete other TicketFieldEntries
      TicketFieldEntry.count(:all).must_be :>, 0
    end

    it "uses Zendesk::DatabaseBackoff#backoff_duration to throttle the deletions" do
      Zendesk::DatabaseBackoff.any_instance.stubs(:backoff_duration).returns(2.seconds)
      TicketFieldEntryDeleteJob.any_instance.expects(:sleep).with(2.seconds)
      TicketFieldEntryDeleteJob.perform(ticket_field.account_id, ticket_field.id)
    end

    it "doesn't sleep for more than the maximum time" do
      backoff_duration = TicketFieldEntryDeleteJob::BATCH_MAXIMUM_SLEEP + 1.minute
      Zendesk::DatabaseBackoff.any_instance.stubs(:backoff_duration).returns(backoff_duration)
      TicketFieldEntryDeleteJob.any_instance.expects(:sleep).with(TicketFieldEntryDeleteJob::BATCH_MAXIMUM_SLEEP)
      TicketFieldEntryDeleteJob.perform(ticket_field.account_id, ticket_field.id)
    end
  end
end
