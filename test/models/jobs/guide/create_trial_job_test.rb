require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

describe Guide::CreateTrialJob do
  fixtures :accounts

  let(:described_class) { Guide::CreateTrialJob }
  let(:account) { accounts(:minimum) }
  let(:account_client) { stub }

  before do
    Account.stubs(:find_by_id).with(account.id).returns(account)
    Zendesk::Accounts::Client.stubs(:new).with(account, anything).returns(account_client)
    Timecop.freeze(2018, 1, 1, 9, 0, 0)
  end

  describe '#work' do
    it 'Creates a trial' do
      account_client.stubs(:product!).with('guide').raises(Kragle::ResourceNotFound)

      account_client.expects(:create_product!).with(
        'guide',
        {
          product: {
            state: :trial,
            trial_expires_at: '2018-01-30T23:59:59+00:00',
            plan_settings: {
              plan_type: 2,
              suite: false
            }
          }
        },
        context: "guide_create_trial_job"
      ).once

      described_class.work(account.id)
    end
  end
end
