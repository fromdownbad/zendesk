require_relative "../../support/job_helper"

SingleCov.covered!

describe SuiteTrialJob do
  fixtures :accounts

  let(:described_class) { SuiteTrialJob }
  let(:account) { accounts(:trial) }
  let(:suite_trial) { stub('Zendesk::SuiteTrial') }

  before do
    Zendesk::SuiteTrial.stubs(:new).with(account).returns(suite_trial)
    Zendesk::Voice::InternalApiClient.any_instance.stubs(:create_trial)
    suite_trial.stubs(:activate)
  end

  describe '#work' do
    it 'activates a Suite Trial' do
      suite_trial.expects(:activate)

      described_class.work(account.id)
    end

    describe_with_arturo_disabled :voice_disable_subscription_notifications do
      it 'triggers a Talk trial' do
        Zendesk::Voice::InternalApiClient.any_instance.expects(:create_trial).once

        described_class.work(account.id)
      end
    end

    describe_with_arturo_enabled :voice_disable_subscription_notifications do
      it 'does not trigger a Talk trial' do
        Zendesk::Voice::InternalApiClient.any_instance.expects(:create_trial).never

        described_class.work(account.id)
      end
    end
  end

  describe '#args_to_log' do
    it 'returns a hash with the arguments of the job' do
      expected = { account_id: account.id, audit_id: nil }
      assert_equal expected, described_class.args_to_log(account.id)
    end
  end

  describe '#delay' do
    it 'returns the delay on retry in seconds based on enqueue_count' do
      expected = [0, 60, 240, 540, 960]

      expected.each_with_index do |delay_value, enqueue_count|
        assert_equal described_class.delay(enqueue_count), delay_value
      end
    end
  end
end
