require_relative "../../support/job_helper"
require_relative "../../support/attachment_test_helper"

SingleCov.covered! uncovered: 5

describe XmlExportJob do
  fixtures :all
  include PerformJob

  before do
    @minimum_admin = users(:minimum_admin)
  end

  describe "class methods" do
    before do
      @account = @minimum_admin.account
      @subscription = Subscription.new
      stub_request(:put, %r{\.amazonaws.com/data/expirable_attachments/.*xml.zip})
    end

    describe ".perform" do
      def perform
        XmlExportJob.perform(@minimum_admin.account_id, @minimum_admin.id)
      end

      before { Subscription.any_instance.stubs(:has_xml_export?).returns(true) }

      it "creates an expirable_attachment and send an email" do
        assert_difference("ExpirableAttachment.count(:all)") do
          assert_emails_sent { perform }
        end
      end

      it "does not have n+1 queries" do
        assert_sql_queries(43..46) { perform_job(XmlExportJob, @minimum_admin.account_id, @minimum_admin.id) }
      end

      it "does not blow up when it fails" do
        User.expects(:find).raises
        perform
      end

      describe "job is not accessible" do
        before do
          Zendesk::Export::Configuration.any_instance.
            expects(:type_accessible?).
            with(XmlExportJob).
            returns(false)
        end

        it "does not create an expirable_attachment" do
          refute_difference("ExpirableAttachment.count(:all)") { perform }
        end
      end
    end

    describe "#identifier" do
      it "returns a identifier consisting of the account id use by resque-throttle" do
        assert_equal "account_id:#{@minimum_admin.account.id}", XmlExportJob.identifier(@minimum_admin.account_id, @minimum_admin.id)
      end
    end

    describe "#available_for?" do
      it "returns true if available for this account" do
        Subscription.any_instance.stubs(:has_xml_export?).returns(true)
        assert XmlExportJob.available_for?(@minimum_admin.account)
      end

      it "returns false if not available for this account" do
        Subscription.any_instance.stubs(:has_xml_export?).returns(false)
        refute XmlExportJob.available_for?(@minimum_admin.account)
      end
    end

    describe "#latest" do
      it "returns the latest expirable attachment that corresponds to a xml_export_job" do
        attachment = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", @account, created_via: XmlExportJob.name)
        assert_equal attachment, XmlExportJob.latest(@account)
      end
    end
  end
end
