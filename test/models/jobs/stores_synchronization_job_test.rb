require_relative "../../support/job_helper"
require_relative "../../support/attachment_test_helper"

SingleCov.covered! uncovered: 1

describe 'StoresSynchronizationJob' do
  fixtures :attachments, :accounts

  describe '#perform' do
    let(:att) do
      attachment = create_attachment("#{Rails.root}/test/files/normal_1.jpg")
      attachment.thumbnails.each(&:destroy)
      attachment.reload
      attachment
    end

    let(:account) do
      account = att.account
      Account.expects(:find).returns(account)
      account
    end

    # For reference, current setup for test env:
    #
    # Attachment.attachment_backends == [:fs, :s3, :s3eu]
    # Attachment.default_attachment_stores == [:fs]
    # Attachment secondary stores == [:s3]
    #
    it "#perform copies blob to preferred stores)" do
      assert_equal [:fs], att.stores

      Attachment.attachment_backends[:s3][:klass].any_instance.expects(:save_to_storage).at_least(1).returns(true)

      StoresSynchronizationJob.perform(att.account_id, att.id)
      att.reload

      assert_equal :fs, att.stores.first, "first added store is still first store"
      assert_equal [:fs, :s3].sort, att.stores.sort, "s3 added to stores"
      assert_equal [:fs, :s3].sort, att.thumbnails.first.stores.sort, "thumbnail stores agree with attachment stores"
    end
  end

  describe "#perform" do
    before do
      @attachment = attachments(:attachment_entry)

      stub_request(:put, %r{https://zendesk-test..*.amazonaws.com/data/attachments/\d+/a_thumb.png})
      stub_request(:put, %r{https://zendesk-test..*.amazonaws.com/data/attachments/\d+/a.png})
    end

    it "saves attachments on all configured stores" do
      Attachment.attachment_backends[:s3][:klass].stubs(:save_to_storage).returns(true)

      StoresSynchronizationJob.perform(@attachment.account_id, @attachment.id)

      @attachment.reload
      assert_same_elements [:fs, :s3], @attachment.stores
    end

    it "creates missing thumbnails" do
      Attachment.expects(:find).with(@attachment.id).returns(@attachment)
      @attachment.expects(:create_missing_thumbnails!)
      StoresSynchronizationJob.perform(@attachment.account_id, @attachment.id)
    end
  end

  describe "queue" do
    it "is enqueued in the cloud_storage queue" do
      assert_equal :cloud_storage, StoresSynchronizationJob.instance_eval { @queue }
    end
  end

  describe "with an attachment that only exists on s3" do
    before do
      @attachment = attachments(:on_s3)
      assert_equal [:s3], @attachment.stores

      stub_request(:get, @attachment.s3_url).to_return(
        body: "body from s3",
        headers: {
          content_length: 12,
          content_type: "image/jpeg"
        }
      )
    end

    it "stores the file on all stores" do
      Attachment.attachment_backends[:fs][:klass].any_instance.expects(:save_to_storage).returns(true)

      Attachment.any_instance.stubs(:create_missing_thumbnails!)

      StoresSynchronizationJob.perform(@attachment.account_id, @attachment.id)

      @attachment.reload
      assert_same_elements [:fs, :s3], @attachment.stores
    end
  end

  describe "account is moving within a region" do
    before do
      @attachment = attachments(:attachment_entry)
    end

    it "does not include s3eu store" do
      Attachment.attachment_backends[:s3][:klass].any_instance.expects(:save_to_storage).at_least(1).returns(true)

      StoresSynchronizationJob.perform(@attachment.account_id, @attachment.id)

      @attachment.reload
      assert_same_elements [:fs, :s3], @attachment.stores
    end
  end

  describe "cleaning up stores" do
    before do
      Attachment.any_instance.stubs(:valid_size?).returns(true)
      Attachment.any_instance.stubs(:create_missing_thumbnails!)

      url = %r{https://zendesk-test..*.amazonaws.com/data/attachments/.*.png}
      stub_request(:get, url)
      stub_request(:put, url)
    end

    it "doesn't remove eu stores from a moving account attachment" do
      attachment = attachments(:on_s3_and_s3eu)
      assert attachment.stores.include? :s3eu

      StoresSynchronizationJob.perform(attachment.account_id, attachment.id)

      attachment.reload
      assert attachment.stores.include? :s3eu
    end
  end
end
