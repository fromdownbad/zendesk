require_relative '../../support/job_helper'
require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 4

describe 'SpamCleanupJob' do
  fixtures :accounts, :users, :tickets

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:ticket_action_tag) { 'tag' }
  let(:ticket_action_delete) { 'delete' }
  let(:default_tag) { 'spam-cleanup-2018-10-10' }
  let(:default_days) { 7 }
  let(:spammy_pattern) { '123456' }
  let(:spammy_strings_to_block) { "csv, spammy_strings, #{spammy_pattern}" }
  let(:time_one_day_ago) { 1.days.ago }
  let(:z1_ticket_payload) do
    {
      data: {
        account_id: account.id,
        subdomain: account.subdomain,
        ticket_action: ticket_action_tag,
        tag_name: default_tag,
        action_count: 1,
        z1_ticket_type: 'spam_cleanup'
      },
      timestamp: Time.now
    }
  end

  describe 'when the SpamCleanupJob is enqueued' do
    before do
      # Prevent Arturo caching issues when testing different values in Arturo external_beta_subdomains
      Arturo::Feature.feature_cache.clear

      tickets = [tickets(:minimum_1), tickets(:minimum_2), tickets(:minimum_3)]
      tickets.each do |ticket|
        ticket.created_at = time_one_day_ago
        ticket.will_be_saved_by(user)
        ticket.save!
      end

      # Stub the account `domain_whitelist` because `minimum_user`'s email's
      # domain is on the `minimum` account's whitelist in the fixtures
      Account.any_instance.stubs(:domain_whitelist).returns("some.com domains.net here.edu")
      # Stub the account `organization_whitelist` because `minimum_user`'s
      # email's domain is on the `minimum` account's whitelist in the fixtures
      Account.any_instance.stubs(:organization_whitelist).returns("other.com ones.net there.edu")

      Arturo.enable_feature!(:spam_detector_blacklist_pattern_check)
      @pre_cleanup_job_ticket_count = account.tickets.count
    end

    describe 'when account has no tickets with string in spammy_string_blacklist or spammy_strings_to_block' do
      before do
        @account = accounts(:support)
        # none of these tickets contain `spammy_pattern`
        @account.stubs(:tickets).returns([tickets(:minimum_1)])
      end

      describe_with_arturo_enabled :orca_classic_spam_cleanup_job_spammy_strings do
        describe 'when the action is tag' do
          describe_with_and_without_arturo_enabled :orca_classic_tag_fraud_ticket do
            it 'does not tag the tickets' do
              SpamCleanupJob.work(@account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
              @account.reload.tickets.each do |ticket|
                refute ticket.current_tags.include?(default_tag)
              end
            end

            describe_with_arturo_enabled :orca_classic_create_spam_cleanup_z1_ticket do
              it 'does not send a spam cleanup e-mail' do
                SpamCleanupJob.any_instance.expects(:create_z1_ticket_with_fraud_service).with(z1_ticket_payload).never
                SpamCleanupJob.work(@account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
              end
            end
          end
        end

        describe 'when the action is delete' do
          before do
            @pre_cleanup_job_ticket_count = @account.tickets.count
          end

          describe_with_and_without_arturo_enabled :orca_classic_delete_fraud_ticket do
            it 'does not delete the tickets' do
              SpamCleanupJob.work(@account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
              assert_equal @account.reload.tickets.count, @pre_cleanup_job_ticket_count
            end
          end

          describe_with_arturo_enabled :orca_classic_create_spam_cleanup_z1_ticket do
            it 'does not send a spam cleanup e-mail' do
              SpamCleanupJob.any_instance.expects(:create_z1_ticket_with_fraud_service).with(z1_ticket_payload).never
              SpamCleanupJob.work(@account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
            end
          end
        end
      end
    end

    describe 'when the spammy_string_blacklist is updated to include a pattern found in some of the accounts tickets' do
      before do
        spammy_string_blacklist_list = ["longhu007.com", spammy_pattern]
        if spammy_string_blacklist = Arturo::Feature.find_feature(:spammy_string_blacklist)
          spammy_string_blacklist.external_beta_subdomains = spammy_string_blacklist_list
          spammy_string_blacklist.save!
        else
          Arturo::Feature.create!(
            symbol: :spammy_string_blacklist,
            external_beta_subdomains: spammy_string_blacklist_list
          )
        end
      end

      describe 'when the action is tag' do
        describe_with_arturo_enabled :orca_classic_tag_fraud_ticket do
          it 'tags the tickets with the pattern' do
            SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
            tagged_ticket = account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%").first
            assert tagged_ticket.current_tags.include?(default_tag)
          end

          it 'does not tag the tickets without the pattern' do
            SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
            tickets_without_spammy_strings = account.reload.tickets.where.not("description LIKE ?", "%#{spammy_pattern}%")
            refute tickets_without_spammy_strings.any? { |ticket| ticket.current_tags.include?(default_tag) }
          end

          it 'does not add more tags if other spam-cleanup tags already exist' do
            SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
            new_tag = 'spam-cleanup-new'
            SpamCleanupJob.work(account.id, ticket_action_tag, new_tag, default_days, spammy_strings_to_block)
            tagged_ticket = account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%").first
            refute tagged_ticket.current_tags.include?(new_tag)
          end

          describe 'when the tickets are in a closed state' do
            before { Ticket.any_instance.stubs(:status_id).returns(StatusType.CLOSED) }
            it 'does not add tags to closed tickets' do
              SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
              tagged_ticket = account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%").first
              refute tagged_ticket.current_tags.include?(default_tag)
            end
          end
        end

        describe_with_arturo_disabled :orca_classic_tag_fraud_ticket do
          before do
            SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
          end

          it 'does not tag the tickets without the pattern' do
            tickets_without_spammy_strings = account.reload.tickets.where.not("description LIKE ?", "%#{spammy_pattern}%")
            refute tickets_without_spammy_strings.any? { |ticket| ticket.current_tags.include?(default_tag) }
          end
        end

        describe_with_arturo_enabled :orca_classic_create_spam_cleanup_z1_ticket do
          before { Timecop.freeze }
          describe_with_arturo_enabled :orca_classic_tag_fraud_ticket do
            it 'should #create_z1_ticket_with_fraud_service' do
              SpamCleanupJob.any_instance.expects(:create_z1_ticket_with_fraud_service).with(z1_ticket_payload).once
              SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
            end
          end

          describe_with_arturo_disabled :orca_classic_tag_fraud_ticket do
            it 'should not #create_z1_ticket_with_fraud_service' do
              SpamCleanupJob.any_instance.expects(:create_z1_ticket_with_fraud_service).with(z1_ticket_payload).never
              SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
            end
          end
        end

        describe_with_arturo_disabled :orca_classic_create_spam_cleanup_z1_ticket do
          describe_with_arturo_enabled :orca_classic_tag_fraud_ticket do
            it 'should not #create_z1_ticket_with_fraud_service' do
              SpamCleanupJob.any_instance.expects(:create_z1_ticket_with_fraud_service).with(z1_ticket_payload).never
              SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
            end
          end
        end
      end

      describe 'when the action is delete' do
        before do
          # User::SecuritySettings need to exist in order to delete a user
          Zendesk::Users::SecuritySettings.any_instance.stubs(:security_policy_id).returns(Zendesk::SecurityPolicy::Medium.id)
          @spammy_ticket = account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%").first
          @spammy_requester = @spammy_ticket.requester
          @original_requester_ticket_counts = @spammy_requester.tickets.count
        end

        describe_with_arturo_enabled :orca_classic_delete_fraud_ticket do
          it 'deletes the ticket when a ticket has the default pattern of 123456 in the description' do
            SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
            assert_empty account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%")
          end

          describe_with_arturo_enabled :email_rspamd_training_enabled do
            describe_with_arturo_enabled :orca_classic_email_rspamd_training do
              describe "when there the spammy ticket has an raw_email_identifier" do
                before do
                  @job = SpamCleanupJob.new(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
                  @job.stubs(:should_send_ticket_to_rspamd?).returns(true)
                end
                it "reports the misclassification to Rspamd when there is an analyzable email" do
                  RspamdFeedbackJob.expects(:report_spam).with(user: User.system, identifier: @spammy_ticket.original_raw_email_identifier, recipient: @spammy_ticket.recipient)
                  @job.work
                end
              end

              describe "when raw_email_identifier is an empty string" do
                before do
                  spammy_ticket = account.tickets.where("description LIKE ?", "%#{spammy_pattern}%").first
                  spammy_ticket.will_be_saved_by(user)
                  spammy_ticket.recipient = 'test@support.zendesk.com'
                  spammy_ticket.audits.first.metadata[:system][:raw_email_identifier] = ''
                  spammy_ticket.save!
                end

                it "does not report the misclassification to Rspamd when there is no raw_email_identifier" do
                  RspamdFeedbackJob.expects(:report_spam).never
                  SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
                end
              end
            end

            describe_with_arturo_disabled :orca_classic_email_rspamd_training do
              it "does not report the misclassification to Rspamd when there is an analyzable email" do
                RspamdFeedbackJob.expects(:report_spam).never
                SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
              end
            end
          end

          describe_with_arturo_disabled :email_rspamd_training_enabled do
            describe_with_and_without_arturo_enabled :orca_classic_email_rspamd_training do
              it "does not report the misclassification to Rspamd when there is an analyzable email" do
                RspamdFeedbackJob.expects(:report_spam).never
                SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
              end
            end
          end

          describe 'when the requester has multiple tickets' do
            before do
              # A user with more than one ticket
              Ticket.any_instance.stubs(:requester).returns(users(:minimum_end_user))
            end
            it 'does not delete the requester' do
              SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
              assert @spammy_ticket.reload.requester.is_active?
            end
          end

          describe 'when the requester has only 1 spammy ticket' do
            before do
              # A user with no other tickets
              Ticket.any_instance.stubs(:requester).returns(users(:minimum_search_user))
            end

            describe 'when the requester was created at the same time as the ticket' do
              before do
                # Create date is same as ticket
                User.any_instance.stubs(:created_at).returns(@spammy_ticket.created_at)
              end
              it 'deletes the requester' do
                SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
                deleted_requester = @spammy_ticket.reload.requester
                deleted_requester_ticket_counts = deleted_requester.tickets.count

                refute deleted_requester.is_active?
                assert_equal deleted_requester_ticket_counts, 0
                assert_not_equal deleted_requester_ticket_counts, @original_requester_ticket_counts
              end
            end

            describe 'when the difference between user and ticket create is greater than 15 seconds' do
              before do
                # User is 1 day older than ticket
                User.any_instance.stubs(:created_at).returns(2.days.ago)
              end
              it 'does not delete the user' do
                SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
                requester = @spammy_ticket.reload.requester

                assert requester.reload.is_active?
              end
            end

            describe 'when the tickets have missing audits' do
              before do
                # Some tickets that are generated from Chat have missing audits which fails deletions
                Ticket.any_instance.stubs(:audits).returns(nil)
              end

              it 'does not delete any tickets or the spammy_requester' do
                SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
                refute @spammy_requester.suspended?
                assert_equal @pre_cleanup_job_ticket_count, account.reload.tickets.count
              end
            end
          end
        end

        describe_with_arturo_disabled :orca_classic_delete_fraud_ticket do
          it 'does not delete any tickets or the spammy_requester' do
            SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
            refute @spammy_requester.suspended?
            assert_equal @pre_cleanup_job_ticket_count, account.reload.tickets.count
          end

          describe_with_and_without_arturo_enabled :email_rspamd_training_enabled do
            describe_with_and_without_arturo_enabled :orca_classic_email_rspamd_training do
              it "does not report the misclassification to Rspamd when there is an analyzable email" do
                RspamdFeedbackJob.expects(:report_spam).never
                SpamCleanupJob.work(account.id, ticket_action_delete, default_tag, default_days, spammy_strings_to_block)
              end
            end
          end
        end
      end
    end

    describe 'when a monitor user passes in strings to block in spammy_strings_to_block' do
      describe_with_arturo_enabled :orca_classic_tag_fraud_ticket do
        describe_with_arturo_enabled :orca_classic_spam_cleanup_job_spammy_strings do
          describe 'when blocking with the default strings' do
            before do
              SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
            end

            it 'tags the tickets with the pattern' do
              tickets_with_spammy_pattern = account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%")
              assert tickets_with_spammy_pattern.all? { |ticket_with_spammy_pattern| ticket_with_spammy_pattern.current_tags.include?(default_tag) }
            end

            it 'does not tag the tickets without the pattern' do
              tickets_without_spammy_strings = account.reload.tickets.where.not("description LIKE ?", "%#{spammy_pattern}%")
              refute tickets_without_spammy_strings.any? { |ticket| ticket.current_tags.include?(default_tag) }
            end
          end

          describe 'when blocking more tickets than the limit' do
            before do
              SpamCleanupJob.send(:remove_const, :ACTION_LIMIT)
              SpamCleanupJob.const_set(:ACTION_LIMIT, 0)
              SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
            end

            it 'only tags the spam cleanup limit' do
              tickets_with_spammy_pattern = account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%")
              tagged_tickets = tickets_with_spammy_pattern.select { |ticket_with_spammy_pattern| ticket_with_spammy_pattern.current_tags.include?(default_tag) }
              refute_equal tickets_with_spammy_pattern.size, tagged_tickets.size
              assert_equal tagged_tickets.size, ENV['SPAM_CLEANUP_ACTION_LIMIT'].to_i
            end

            after do
              SpamCleanupJob.send(:remove_const, :ACTION_LIMIT)
              SpamCleanupJob.const_set(:ACTION_LIMIT, 5)
            end
          end

          describe 'when blocking with strings with extra white spaces' do
            before do
              spammy_strings_to_block_with_space = "csv, spammy_strings, th      ree"
              SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block_with_space)
            end

            it 'tags the tickets with the pattern' do
              tickets_with_spammy_pattern = account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%")
              assert tickets_with_spammy_pattern.all? { |ticket_with_spammy_pattern| ticket_with_spammy_pattern.current_tags.include?(default_tag) }
            end

            it 'does not tag the tickets without the pattern' do
              tickets_without_spammy_strings = account.reload.tickets.where.not("description LIKE ?", "%#{spammy_pattern}%")
              refute tickets_without_spammy_strings.any? { |ticket| ticket.current_tags.include?(default_tag) }
            end
          end
        end

        describe_with_arturo_disabled :orca_classic_spam_cleanup_job_spammy_strings do
          before do
            SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, spammy_strings_to_block)
          end

          it 'does not tag the tickets with the pattern' do
            tickets_with_spammy_pattern = account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%")
            refute tickets_with_spammy_pattern.all? { |ticket_with_spammy_pattern| ticket_with_spammy_pattern.current_tags.include?(default_tag) }
          end

          it 'does not tag the tickets without the pattern' do
            tickets_without_spammy_strings = account.reload.tickets.where.not("description LIKE ?", "%#{spammy_pattern}%")
            refute tickets_without_spammy_strings.any? { |ticket| ticket.current_tags.include?(default_tag) }
          end
        end

        describe 'when a monitor user passes in an empty string' do
          describe_with_arturo_enabled :orca_classic_spam_cleanup_job_spammy_strings do
            before do
              SpamCleanupJob.work(account.id, ticket_action_tag, default_tag, default_days, "")
            end

            it 'does not tag the tickets with the pattern' do
              tickets_with_spammy_pattern = account.reload.tickets.where("description LIKE ?", "%#{spammy_pattern}%")
              refute tickets_with_spammy_pattern.all? { |ticket_with_spammy_pattern| ticket_with_spammy_pattern.current_tags.include?(default_tag) }
            end

            it 'does not tag the tickets without the pattern' do
              tickets_without_spammy_strings = account.reload.tickets.where.not("description LIKE ?", "%#{spammy_pattern}%")
              refute tickets_without_spammy_strings.any? { |ticket| ticket.current_tags.include?(default_tag) }
            end
          end
        end
      end
    end
  end
end
