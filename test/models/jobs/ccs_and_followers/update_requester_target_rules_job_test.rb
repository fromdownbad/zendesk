require 'zendesk_logging'
require_relative "../../../support/job_helper"
require_relative '../../../support/rule'

SingleCov.covered!

describe CcsAndFollowers::UpdateRequesterTargetRulesJob do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper

  fixtures :accounts, :rules, :translation_locales, :users

  let!(:account) { accounts(:minimum) }
  let!(:admin) { account.admins.first }

  let(:namespace) { 'txt.admin.models.jobs.update_requester_target_rules_job.email' }

  let(:locales) do
    [
      translation_locales(:spanish),
      translation_locales(:english_by_zendesk),
      translation_locales(:japanese)
    ]
  end

  let(:translated_subject_english)  { "test subject english" }
  let(:translated_subject_spanish)  { "test subject spanish" }
  let(:translated_subject_japanese) { "test subject japanese" }

  let(:translated_body_english)  { "test body english" }
  let(:translated_body_spanish)  { "test body spanish" }
  let(:translated_body_japanese) { "test body japanese" }

  let(:translated_body_no_changes_english) { "test body: no changes english" }

  let(:translated_instructions_english)  { "test instructions english" }
  let(:translated_instructions_spanish)  { "test instructions spanish" }
  let(:translated_instructions_japanese) { "test instructions japanese" }

  let(:translated_link_english) { "https://english@example.com" }
  let(:translated_link_spanish) { "https://spanish@example.com" }
  let(:translated_link_japanese) { "https://japanese@example.com" }

  let(:trigger) do
    create_trigger(
      definition: build_trigger_definition(
        conditions_all: conditions,
        conditions_any: conditions
      )
    )
  end
  let(:conditions) do
    [
      build_definition_item(
        field: 'status_id',
        operator: 'less_than',
        value: 1
      )
    ]
  end
  let(:rule_diff) do
    [Zendesk::Rules::Diff.new(source: trigger, target: trigger)]
  end
  let(:expected_affected_automations) do
    [
      "Close ticket 4 days after status is set to solved, https://minimum.zendesk-test.com/agent/admin/automations/4401"
    ]
  end
  let(:expected_affected_rules) do
    [
      "Notify requester of solved ticket, https://minimum.zendesk-test.com/agent/admin/triggers/61381",
      "Notify requester of comment update, https://minimum.zendesk-test.com/agent/admin/triggers/70624",
      "Notify requester of received request, https://minimum.zendesk-test.com/agent/admin/triggers/71512"
    ]
  end
  let(:nps_rules) do
    [
      "Request customer satisfaction rating (System Automation), https://minimum.zendesk-test.com/agent/admin/automations/92754"
    ]
  end

  describe 'class properties' do
    it 'enforces in flight limit' do
      assert CcsAndFollowers::UpdateRequesterTargetRulesJob.send(:enforce_in_flight_limit?)
    end
  end

  describe '#build_list' do
    let(:dry_run) { true }
    let(:rules_diff) { Zendesk::CcsAndFollowers::RulesCompatibility.new(account.id).update_requester_target(dry_run: dry_run) }

    subject do
      job = CcsAndFollowers::UpdateRequesterTargetRulesJob.new(
        'uuid',
        account_id: account.id,
        admin_id: admin.id,
        dry_run: dry_run,
        onboarding_completed: false
      )
      job.send(:build_list, account, rules_diff)
    end

    it 'returns affected rules separated by newlines' do
      expected = (expected_affected_automations + expected_affected_rules).sort
      assert_equal expected, subject.split("\n").sort
    end
  end

  describe '#perform' do
    let(:dry_run)              { true }
    let(:onboarding_completed) { true }

    subject { CcsAndFollowers::UpdateRequesterTargetRulesJob.new('uuid', account_id: account.id, admin_id: admin.id, dry_run: dry_run, onboarding_completed: onboarding_completed).perform }

    before do
      I18n.stubs(:t).returns("translated whatever")
      stub_request(:put, %r{\.amazonaws\.com/data/expirable_attachments/\d+/affected_rules_list\.zip})
      Account.any_instance.stubs(:has_polaris?).returns(false)
    end

    it 'raises an exception when account cannot be found' do
      assert_raises(ActiveRecord::RecordNotFound) do
        CcsAndFollowers::UpdateRequesterTargetRulesJob.new('uuid', account_id: nil, admin_id: admin.id).perform
      end
    end

    it 'raises an exception when admin cannot be found' do
      assert_raises(ActiveRecord::RecordNotFound) do
        CcsAndFollowers::UpdateRequesterTargetRulesJob.new('uuid', account_id: account.id, admin_id: nil, dry_run: true, onboarding_completed: true).perform
      end
    end

    it 'calls CcsAndFollowers::RulesCompatibility#update_requester_target' do
      Zendesk::CcsAndFollowers::RulesCompatibility.any_instance.expects(:update_requester_target).
        with(dry_run: true).returns(rule_diff)
      subject
    end

    it 'sends metrics to the statsd client' do
      CcsAndFollowers::UpdateRequesterTargetRulesJob.any_instance.stubs(:store_rules_list).returns(mock('attachment', url: ''))
      CcsAndFollowers::UpdateRequesterTargetRulesJob.any_instance.stubs(:notify_admins)

      Zendesk::StatsD::Client.any_instance.expects(:increment).
        with(:enqueued, tags: ["dry_run:#{dry_run}", "onboarding_completed:#{dry_run}"])

      subject
    end

    describe 'email notifications' do
      let(:batch_size) { 2 }

      def setup_i18n_assertions
        admin_groups = account.admins.group_by(&:translation_locale)

        admin_groups.keys.each do |translation_locale|
          string_suffix = string_suffix_from_translation_locale(translation_locale)
          I18n.expects(:t).with("#{namespace}.instructions_v6", has_entries(locale: translation_locale, link: anything)).returns(send("translated_instructions_#{string_suffix}".to_sym))
          I18n.expects(:t).with("#{namespace}.instructions_link", has_entry(locale: translation_locale)).returns(send("translated_link_#{string_suffix}".to_sym))
        end

        admin_groups.values.each do |admin_group|
          admin_group.each_slice(batch_size) do |admin_batch|
            translation_locale = admin_batch.first.translation_locale
            string_suffix = string_suffix_from_translation_locale(translation_locale)

            I18n.expects(:t).with("#{namespace}.subject", has_entry(locale: translation_locale)).returns(send("translated_subject_#{string_suffix}".to_sym))
            I18n.expects(:t).with("#{namespace}.instructions_v6", has_entries(locale: translation_locale, link: anything)).returns(send("translated_instructions_#{string_suffix}".to_sym))
            I18n.expects(:t).with("#{namespace}.instructions_link", has_entry(locale: translation_locale)).returns(send("translated_link_#{string_suffix}".to_sym))
            I18n.expects(:t).with("#{namespace}.body", has_entry(locale: translation_locale)).returns(send("translated_body_#{string_suffix}".to_sym))
          end
        end
      end

      before do
        @notification_recipient_batch_size_value = CcsAndFollowers::UpdateRequesterTargetRulesJob.const_get(:NOTIFICATION_RECIPIENT_BATCH_SIZE)
        CcsAndFollowers::UpdateRequesterTargetRulesJob.send(:remove_const, :NOTIFICATION_RECIPIENT_BATCH_SIZE)
        CcsAndFollowers::UpdateRequesterTargetRulesJob.const_set(:NOTIFICATION_RECIPIENT_BATCH_SIZE, batch_size)
      end

      after do
        CcsAndFollowers::UpdateRequesterTargetRulesJob.send(:remove_const, :NOTIFICATION_RECIPIENT_BATCH_SIZE)
        CcsAndFollowers::UpdateRequesterTargetRulesJob.const_set(:NOTIFICATION_RECIPIENT_BATCH_SIZE, @notification_recipient_batch_size_value)
      end

      describe 'when the number of admins receiving an email notification exceeds `NOTIFICATION_RECIPIENT_BATCH_SIZE`' do
        before do
          setup_i18n_assertions
        end

        it "sends email notification in batches" do
          Zendesk::CcsAndFollowers::RulesCompatibility.any_instance.stubs(:update_requester_target).returns(rule_diff)
          JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).times(2).with do |_admins, _identifier, title, content|
            assert_equal translated_subject_english, title
            assert_equal (translated_body_english + "\n\n" + translated_instructions_english), content
          end

          subject
        end
      end

      describe 'when admins on the account have different locales' do
        before do
          set_admin_locales
          setup_i18n_assertions
        end

        after { reset_admin_locales }

        it "sends email notification in batches by locale" do
          Zendesk::CcsAndFollowers::RulesCompatibility.any_instance.stubs(:update_requester_target).returns(rule_diff)
          JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).times(3).with do |admins, _identifier, title, content|
            assert_equal 1, admins.size

            case admins.first.translation_locale
            when translation_locales(:spanish)
              assert_equal translated_subject_spanish, title
              assert_equal (translated_body_spanish + "\n\n" + translated_instructions_spanish), content
            when translation_locales(:english_by_zendesk)
              assert_equal translated_subject_english, title
              assert_equal (translated_body_english + "\n\n" + translated_instructions_english), content
            when translation_locales(:japanese)
              assert_equal translated_subject_japanese, title
              assert_equal (translated_body_japanese + "\n\n" + translated_instructions_japanese), content
            end
          end

          subject
        end
      end
    end

    it 'calls completed' do
      JobWithStatus.any_instance.expects(:completed).once
      subject
    end

    describe 'with `onboarding_completed` as true' do
      describe 'when there are no affected rules' do
        before do
          account.rules.clear
          account.save!
        end

        it 'notifies admins that there are no required rule changes' do
          locale = account.admins.first.translation_locale

          I18n.expects(:t).with("#{namespace}.subject", has_entry(locale: locale)).returns(translated_subject_english)
          I18n.expects(:t).with("#{namespace}.body_no_changes_v2", has_entry(locale: locale)).returns(translated_body_no_changes_english)

          JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).with do |admins, _identifier, title, content|
            assert_equal 3, admins.size
            assert_equal translated_subject_english, title
            assert_equal translated_body_no_changes_english, content
          end

          subject
        end

        it 'does not create a zip file' do
          Zip::OutputStream.any_instance.expects(:put_next_entry).never

          subject
        end

        it 'does not create a new ExpirableAttachment' do
          assert_no_difference("ExpirableAttachment.count") do
            subject
          end
        end

        it 'completes with an nil attachment url' do
          assert_nil subject[3][:results][:url]
        end

        it 'calls set_ccs_followers_rules_update_required' do
          CcsAndFollowers::UpdateRequesterTargetRulesJob.
            any_instance.expects(:set_ccs_followers_rules_update_required).
            with(account, false).
            once

          subject
        end

        describe '#set_ccs_followers_rules_update_required' do
          it 'correctly sets `ccs_followers_rules_update_required`' do
            assert account.settings.ccs_followers_rules_update_required

            subject

            refute account.settings.reload.ccs_followers_rules_update_required
          end
        end
      end

      describe 'when there are affected rules' do
        let(:affected_rules_list_entry) { mock }
        let(:affected_rules_string) { mock }
        let(:instructions_entry) { mock }
        let(:onboarding_completed) { true }

        before do
          CcsAndFollowers::UpdateRequesterTargetRulesJob.any_instance.stubs(:build_list).returns(affected_rules_string)
        end

        it 'sends an email notification to account admins' do
          JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).at_least_once

          subject
        end

        describe 'with `dry_run` as true' do
          let(:dry_run) { true }

          it 'notifies admins about affected rules and recommended changes' do
            locale = account.admins.first.translation_locale

            I18n.expects(:t).with("#{namespace}.subject", has_entry(locale: locale)).returns(translated_subject_english)
            I18n.expects(:t).twice.with("#{namespace}.instructions_v6", has_entries(locale: locale, link: anything)).returns(translated_instructions_english)
            I18n.expects(:t).twice.with("#{namespace}.instructions_link", has_entry(locale: locale)).returns(translated_link_english)
            I18n.expects(:t).with("#{namespace}.body", has_entry(locale: locale)).returns(translated_body_english)

            JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).with do |admins, _identifier, title, content|
              assert_equal 3, admins.size
              assert_equal translated_subject_english, title
              assert_equal (translated_body_english + "\n\n" + translated_instructions_english), content
            end

            subject
          end

          it 'creates a zip file containing the instructions and rules list files' do
            I18n.expects(:t).with("#{namespace}.instructions_v6", has_entries(locale: account.translation_locale, link: anything)).returns(translated_instructions_english)
            I18n.expects(:t).with("#{namespace}.instructions_link", has_entry(locale: account.translation_locale)).returns(translated_link_english)
            Zip::Entry.expects(:new).with(anything, 'affected_rules_list.txt', *([anything] * 7)).returns(affected_rules_list_entry)
            Zip::OutputStream.any_instance.expects(:put_next_entry).with(affected_rules_list_entry).once
            locale = account.admins.first.translation_locale.locale
            Zip::Entry.expects(:new).with(anything, "instructions-#{locale}.txt", *([anything] * 7)).returns(instructions_entry)
            Zip::OutputStream.any_instance.expects(:put_next_entry).with(instructions_entry).once
            Zip::OutputStream.any_instance.expects(:puts).with(affected_rules_string)
            Zip::OutputStream.any_instance.expects(:puts).with(translated_instructions_english)

            subject
          end

          describe 'when admins on the account have different locales' do
            before { set_admin_locales }

            after { reset_admin_locales }

            describe 'creating zip file' do
              it 'creates a zip file containing the instructions and rules list files' do
                Zip::Entry.expects(:new).with(anything, 'affected_rules_list.txt', *([anything] * 7)).returns(affected_rules_list_entry)
                Zip::OutputStream.any_instance.expects(:put_next_entry).with(affected_rules_list_entry).once
                account.admins.map(&:translation_locale).uniq.map do |translation_locale|
                  string_suffix = string_suffix_from_translation_locale(translation_locale)
                  instructions = send("translated_instructions_#{string_suffix}".to_sym)
                  instructions_link = send("translated_link_#{string_suffix}".to_sym)
                  Zip::Entry.expects(:new).with(anything, "instructions-#{translation_locale.locale}.txt", *([anything] * 7)).returns(instructions_entry)
                  I18n.expects(:t).with("#{namespace}.instructions_v6", has_entries(locale: translation_locale, link: anything)).returns(instructions)
                  I18n.expects(:t).with("#{namespace}.instructions_link", has_entry(locale: translation_locale)).returns(instructions_link)
                  Zip::OutputStream.any_instance.expects(:put_next_entry).with(instructions_entry)
                  Zip::OutputStream.any_instance.expects(:puts).with(instructions)
                end
                Zip::OutputStream.any_instance.expects(:puts).with(affected_rules_string)

                subject
              end
            end
          end

          it 'creates a new ExpirableAttachment' do
            Zendesk::CcsAndFollowers::RulesCompatibility.any_instance.stubs(:update_requester_target).returns(rule_diff)
            assert_difference("ExpirableAttachment.count") do
              subject
            end
          end

          it 'completes with an attachment url' do
            assert_match(
              /https:\/\/minimum.zendesk-test.com\/expirable_attachments\/token?\/.+\/?name=affected_rules_list.zip/,
              subject[3][:results][:url]
            )
          end

          it 'calls `set_ccs_followers_rules_update_required` with `true`' do
            CcsAndFollowers::UpdateRequesterTargetRulesJob.
              any_instance.expects(:set_ccs_followers_rules_update_required).
              with(account, true).
              once

            subject
          end
        end

        describe 'with `dry_run` as false' do
          let(:dry_run) { false }

          it 'notifies admins about affected rules' do
            locale = account.admins.first.translation_locale

            I18n.expects(:t).with("#{namespace}.subject", has_entry(locale: locale)).returns(translated_subject_english)
            I18n.expects(:t).with("#{namespace}.body", has_entry(locale: locale)).returns(translated_body_english)

            JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).with do |admins, _identifier, title, content|
              assert_equal 3, admins.size
              assert_equal translated_subject_english, title
              assert_equal translated_body_english, content
            end

            subject
          end

          it 'creates a zip file containing the instructions and rules list files' do
            Zip::Entry.expects(:new).with(anything, 'affected_rules_list.txt', *([anything] * 7)).returns(affected_rules_list_entry)
            Zip::OutputStream.any_instance.expects(:put_next_entry).with(affected_rules_list_entry).once
            Zip::OutputStream.any_instance.expects(:put_next_entry).with('instructions.txt').never
            Zip::OutputStream.any_instance.expects(:puts).with(affected_rules_string)
            subject
          end

          it 'creates a new ExpirableAttachment' do
            Zendesk::CcsAndFollowers::RulesCompatibility.any_instance.stubs(:update_requester_target).returns(rule_diff)
            assert_difference("ExpirableAttachment.count") do
              subject
            end
          end

          it 'completes with an attachment url' do
            assert_match(
              /https:\/\/minimum.zendesk-test.com\/expirable_attachments\/token?\/.+\/?name=affected_rules_list.zip/,
              subject[3][:results][:url]
            )
          end

          it 'calls `set_ccs_followers_rules_update_required` with `false`' do
            CcsAndFollowers::UpdateRequesterTargetRulesJob.
              any_instance.expects(:set_ccs_followers_rules_update_required).
              with(account, false).
              once

            subject
          end
        end
      end
    end

    describe 'with `onboarding_completed` as false' do
      let(:onboarding_completed) { false }

      it 'does not send an email notification to account admins' do
        JobsMailer.expects(:deliver_job_complete).never
        subject
      end
    end

    describe 'handled exceptions' do
      describe 'around persisting the file' do
        describe 'ActiveRecord errors' do
          let(:exception) { ActiveRecord::RecordInvalid }

          describe 'logging' do
            before do
              ExpirableAttachment.any_instance.expects(:save!).raises(exception.new(account.triggers.first))
            end

            it 'logs attachment info and re-raises' do
              CcsAndFollowers::UpdateRequesterTargetRulesJob.expects(:resque_error).with do |args|
                assert args.include?("Unable to create attachment. account_id: #{account.id}, admin_id: #{admin.id}")
                assert expected_affected_rules.all? { |rule| args.include?(rule) }
                assert args.include?("error: #{exception}")
              end
              assert_raises(exception) { subject }
            end
          end
        end

        describe 'AttachmentFu::NoBackendsAvailableException' do
          let(:exception) { ::Technoweenie::AttachmentFu::NoBackendsAvailableException }

          describe 'logging' do
            before do
              ExpirableAttachment.any_instance.expects(:save!).raises(exception)
            end

            it 'logs attachment info and re-raises' do
              CcsAndFollowers::UpdateRequesterTargetRulesJob.expects(:resque_error).with do |args|
                assert args.include?("Unable to create attachment. account_id: #{account.id}, admin_id: #{admin.id}")
                assert expected_affected_rules.all? { |rule| args.include?(rule) }
                assert args.include?("error: #{exception}")
              end
              assert_raises(exception) { subject }
            end
          end
        end
      end

      describe 'around updating rules' do
        let(:exception) { ActiveRecord::RecordInvalid }
        let(:error_message) do
          "Failed to update or diff rules. account_id: #{account.id}, admin_id: #{admin.id}, dry_run: true, error: #{exception}"
        end

        before do
          Zendesk::CcsAndFollowers::RulesCompatibility.any_instance.expects(:update_requester_target).
            raises(exception.new(account.triggers.first))
        end

        it 'logs info and re-raises' do
          CcsAndFollowers::UpdateRequesterTargetRulesJob.expects(:resque_error).with(error_message)
          assert_raises(exception) { subject }
        end
      end
    end
  end

  describe 'Zip::Entry' do
    let(:output_stream) { 'sample output stream' }
    let(:job) { CcsAndFollowers::UpdateRequesterTargetRulesJob.new('uuid', account_id: account.id, admin_id: admin.id) }
    let(:current_time) { Zip::DOSTime.parse(Time.current.strftime(CcsAndFollowers::UpdateRequesterTargetRulesJob::DATETIME_FORMAT)) }
    let(:zip_entry_options) { job.send(:zip_entry_options, current_time) }

    describe '#affected_rules_list_entry' do
      it 'returns the expected zip entry' do
        affected_rules_list_entry = job.send(:affected_rules_list_entry, output_stream, zip_entry_options)

        assert_equal output_stream, affected_rules_list_entry.zipfile
        assert_equal 'affected_rules_list.txt', affected_rules_list_entry.name
        assert_equal current_time, affected_rules_list_entry.time
      end
    end

    describe '#instructions_entry' do
      let(:locale) { 'en' }

      it 'returns the expected zip entry' do
        instructions_entry = job.send(:instructions_entry, output_stream, locale, zip_entry_options)

        assert_equal output_stream, instructions_entry.zipfile
        assert_equal "instructions-#{locale}.txt", instructions_entry.name
        assert_equal current_time, instructions_entry.time
      end
    end
  end

  private

  def set_admin_locales
    @account_admin_locales = []

    account.admins.take(locales.length).each_with_index do |admin, index|
      @account_admin_locales.push(admin.locale_id)
      admin.locale_id = locales[index].id
      admin.save!
    end
  end

  def reset_admin_locales
    account.admins.take(locales.length).each_with_index do |admin, index|
      admin.locale_id = @account_admin_locales[index]
      admin.save!
    end
  end

  def string_suffix_from_translation_locale(translation_locale)
    case translation_locale.id
    when 1 then 'english'
    when 4 then 'japanese'
    when 6 then 'spanish'
    else ''
    end
  end
end
