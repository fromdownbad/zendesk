require_relative '../../support/job_helper'
require_relative '../../support/test_helper'
require_relative '../../support/rule'

SingleCov.covered!

describe ApplyMacrosJob do
  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }
  let(:active)  { true }

  let(:macro1) do
    create_macro(
      definition: Definition.new.tap do |definition|
        definition.actions.push(DefinitionItem.new('set_tags', nil, ['macro1']))
      end,
      active: active
    )
  end

  let(:macro2) do
    create_macro(
      definition: Definition.new.tap do |definition|
        definition.actions.push(DefinitionItem.new('set_tags', nil, ['macro2']))
      end
    )
  end

  describe '.args_to_log' do
    it 'returns a hash' do
      assert_equal(
        {
          account_id: account.id,
          ticket_id:  ticket.id,
          macro_ids:  [macro1.id, macro2.id]
        },
        ApplyMacrosJob.args_to_log(
          account.id,
          ticket.id,
          [macro1.id, macro2.id]
        )
      )
    end
  end

  describe '#perform' do
    before do
      ApplyMacrosJob.perform(account.id, ticket.id, [macro1.id, macro2.id])
    end

    should_change('the number of audits', by: 2) do
      ticket.reload.audits.count(:all)
    end

    let(:audit)        { ticket.audits.last }
    let(:change_event) { audit.events.first }

    it 'records the `via_id`' do
      assert_equal ViaType.MACRO_REFERENCE, audit.via_id
    end

    it 'records a change event' do
      assert_instance_of Change, change_event
    end

    it 'records the changes made by macro' do
      assert_equal 'macro2', change_event.value
    end

    it 'records the assignee as the author' do
      assert_equal ticket.assignee, audit.author
    end

    describe 'when inactive macro is passed-in' do
      let(:active) { false }

      should_change('the number of audits', by: 1) do
        ticket.reload.audits.count(:all)
      end
    end

    describe 'when ticket does not have an assignee' do
      let(:ticket) { tickets(:minimum_2) }

      it 'records the system user as the author' do
        assert_equal User.system, audit.author
      end
    end
  end
end
