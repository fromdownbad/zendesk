require_relative "../../support/job_helper"

SingleCov.covered!

describe "SetAgentWorkspaceAvailabilityJob" do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:job) { SetAgentWorkspaceAvailabilityJob.new(account_id: account.id, available: available) }
  before do
    Zopim::InternalApiClient.any_instance.stubs(:notify_account_settings_change)
  end

  describe '#work' do
    let!(:accounts_client) do
      mock.tap do |mock_client|
        mock_client.stubs(:products).returns products
        mock_client.stubs(:account_from_account_service).returns(true)
        mock_client.stubs(:chat_product).returns(products.find { |p| p.name == Zendesk::Accounts::Client::CHAT_PRODUCT.to_sym })
        Zendesk::Accounts::Client.stubs(:new).returns mock_client
      end
    end

    describe 'when feature is available' do
      let(:available) { true }

      describe 'without support product' do
        let(:products) { [stub(name: :chat)] }
        before { job.work }

        before_should 'does not change settings' do
          account.settings.expects(:save!).never
        end

        before_should 'does not update products' do
          accounts_client.expects(:update_product!).never
        end
      end

      describe 'with support product' do
        let(:products) { [stub(name: :support)] }
        before do
          accounts_client.expects(:update_product!).with('support', { product: { plan_settings: { agent_workspace: available } } }, { context: 'SetAgentWorkspaceAvailabilityJob' })
          job.work
          account.reload
        end

        it 'saves settings' do
          refute_predicate account.settings, :polaris?
          assert_predicate account.settings, :check_group_name_uniqueness?
        end
      end

      describe 'with support and chat product' do
        let(:products) { [stub(name: :support), stub(name: :chat)] }
        before do
          accounts_client.expects(:update_product!)
          job.work
          account.reload
        end

        it 'does not change group uniqueness setting' do
          refute_predicate account.settings, :polaris?
          refute_predicate account.settings, :check_group_name_uniqueness?
        end
      end
    end

    describe 'when feature is unavailable' do
      let(:available) { false }

      describe 'with support product' do
        let(:products) { [stub(name: :support)] }
        before do
          Apps::ChatAppInstallation.any_instance.expects(:handle_disable_installation).never # Chat product absent
          account.activate_agent_workspace_for_trial

          accounts_client.expects(:update_product!).with('support', { product: { plan_settings: { agent_workspace: available } } }, { context: 'SetAgentWorkspaceAvailabilityJob' })
          job.work
          account.reload
        end

        it 'disables agent workspace completely' do
          refute_predicate account.settings, :polaris?
          refute_predicate account.settings, :check_group_name_uniqueness?
        end
      end

      describe 'with support and chat product' do
        let(:products) { [stub(name: :support), stub(name: :chat)] }
        before do
          Apps::ChatAppInstallation.any_instance.expects(:handle_disable_installation)
          account.activate_agent_workspace_for_trial

          accounts_client.expects(:update_product!)
          job.work
          account.reload
        end

        it 'does not change group uniqueness setting' do
          refute_predicate account.settings, :polaris?
          assert_predicate account.settings, :check_group_name_uniqueness?
        end
      end
    end
  end
end
