require_relative "../../support/job_helper"

SingleCov.covered!

describe 'CrmDataBulkDeleteJob' do
  fixtures :accounts, :external_ticket_datas, :external_user_datas

  let(:account) { accounts(:minimum) }
  let(:delay) { 123 }
  let(:job) { CrmDataBulkDeleteJob.new("79a03f83-8dd9-4af1-85bc-787a62037919", account_id: account.id) }

  describe "#perform" do
    before do
      job.stubs(:delay).returns(delay.minutes)

      ExternalTicketData.any_instance.stubs(:updated_at).returns(1.seconds.ago)
      ExternalUserData.any_instance.stubs(:updated_at).returns(1.seconds.ago)

      assert_not_empty ExternalTicketData.where(account_id: account.id).where('updated_at >= ?', delay)
      assert_not_empty ExternalUserData.where(account_id: account.id).where('updated_at >= ?', delay)

      job.perform
    end

    it "deletes crm_data" do
      assert_empty ExternalTicketData.where(account_id: account.id).where('updated_at >= ?', delay)
      assert_empty ExternalUserData.where(account_id: account.id).where('updated_at >= ?', delay)
    end
  end
end
