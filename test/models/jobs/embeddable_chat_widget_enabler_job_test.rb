require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 3

describe EmbeddableChatWidgetEnablerJob do
  fixtures :all

  let(:account)    { accounts(:minimum) }
  let(:api_client) { stub(connection: stub) }

  before do
    Zendesk::InternalApi::Client.stubs(:new).
      with(account.subdomain, user: 'zendesk').
      returns(api_client)
  end

  it "enables the embeddable chat widget" do
    api_client.connection.expects(:post).
      with(
        '/embeddable/api/internal/config_sets.json',
        config_set: { chat_enabled: true }
      )
    EmbeddableChatWidgetEnablerJob.perform(account.id)
  end
end
