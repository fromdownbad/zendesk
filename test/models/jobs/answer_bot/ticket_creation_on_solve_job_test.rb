require_relative "../../../support/job_helper"

SingleCov.covered!

describe AnswerBot::TicketCreationOnSolveJob do
  fixtures :accounts, :ticket_deflections, :tickets

  let(:account) { accounts(:minimum) }
  let(:deflection) { ticket_deflections(:without_ticket) }
  let(:article_id) { 666 }
  let(:resolution_channel_id) { ViaType.WEB_WIDGET }
  let(:ticket_id) { nil }
  let(:user_id) { nil }

  describe '.args_to_log' do
    it 'includes all args' do
      expected = {
        account_id: account.id,
        deflection_id: deflection.id,
        article_id: article_id,
        resolution_channel_id: resolution_channel_id,
      }

      actual = AnswerBot::TicketCreationOnSolveJob.args_to_log(
        account.id,
        deflection.id,
        article_id,
        resolution_channel_id
      )

      assert_equal(expected, actual)
    end
  end

  describe '.work' do
    let(:request_solver) { mock('AnswerBot::RequestSolver') }
    let(:created_ticket) { tickets(:minimum_1) }
    let(:success_log) do
      <<-LOG.squish
        Job completed:
          Account Id: #{account.id},
          Ticket Deflection Id: #{deflection.id},
          Article Id: #{article_id},
          Requester Id: #{created_ticket.requester_id},
          Ticket Id: #{created_ticket.id}
      LOG
    end
    let(:work!) do
      AnswerBot::TicketCreationOnSolveJob.work(
        account.id,
        deflection.id,
        article_id,
        resolution_channel_id
      )
    end

    before do
      AnswerBot::RequestSolver.stubs(:new).returns(request_solver)
      request_solver.stubs(:create_and_solve_ticket).returns(created_ticket)
      created_ticket.stubs(:ticket_deflection).returns(deflection)
    end

    it 'solves deflection via solver' do
      request_solver.expects(:create_and_solve_ticket).returns(created_ticket)
      work!
    end

    it 'logs success' do
      AnswerBot::TicketCreationOnSolveJob.expects(:resque_log).
        with(success_log).
        once

      work!
    end

    describe 'updates failed' do
      let(:error_message) { "I don't know how to do this!" }
      let(:error) { StandardError.new(error_message) }
      let(:failed_log) do
        <<-LOG.squish
          Job failed after 3 retry:
            Account Id: #{account.id},
            Ticket Deflection Id: #{deflection.id},
            Article Id: #{article_id},
            Error: #{error_message}
        LOG
      end

      before do
        request_solver.stubs(:create_and_solve_ticket).raises(error)
      end

      it 'raises error so that job can retry' do
        assert_raises error.class do
          work!
        end
      end

      describe 'when done retrying' do
        let(:ab_statsd_client) { stub_everything('ab_statsd_client') }

        before do
          AnswerBot::TicketCreationOnSolveJob.stubs(:retry_attempt).returns(3)

          Datadog::AnswerBot.stubs(:new).
            with(
              via_id: deflection.via_id,
              subdomain: deflection.brand.subdomain,
              deflection_channel_id: deflection.deflection_channel_id,
              resolution_channel_id: resolution_channel_id
            ).
            returns(ab_statsd_client)
        end

        it 'increments failure in datadog' do
          ab_statsd_client.expects(:ticket_creation_failed).once

          work!
        end

        it 'logs failed job in resque error' do
          AnswerBot::TicketCreationOnSolveJob.expects(:resque_error).
            with(failed_log).
            once

          work!
        end

        it 'swallows the exception' do
          work!
        end
      end
    end
  end
end
