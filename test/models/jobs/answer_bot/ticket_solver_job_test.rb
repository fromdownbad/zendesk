require_relative "../../../support/job_helper"

SingleCov.covered!

describe AnswerBot::TicketSolverJob do
  fixtures :all

  describe '.work' do
    let(:account) { accounts(:minimum) }
    let(:account_id) { account.id }
    let(:user) { users(:minimum_agent) }
    let!(:user_id) { user.id }
    let(:resolution_channel_id) { ViaType.WEB_WIDGET }
    let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }
    let(:ticket_deflection_id) { ticket_deflection.id }
    let(:article_id) { 85691 }
    let(:base_time) { Time.utc(2016, 3, 31) }
    let(:article_content) do
      {
        id: 1,
        title: 'title',
        html_url: 'http://whatever',
        url: 'http://whateveragain.com'
      }
    end
    let(:stats) { {} }
    let(:ab_statsd_client) { stub_everything('ab_stastd_client') }

    before do
      Timecop.freeze(base_time)

      Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:default_locale).returns('en-us')
      Zendesk::HelpCenter::InternalApiClient.any_instance.
        stubs(:fetch_article).
        returns(article_content)

      ticket_deflection.stubs(:solved_article_id).returns(article_id)
      TicketDeflection.stubs(:find).
        with(ticket_deflection_id).
        returns(ticket_deflection)

      Datadog::AnswerBot.stubs(:new).
        returns(ab_statsd_client)
    end

    describe 'args_to_log' do
      let(:args_to_log) do
        AnswerBot::TicketSolverJob.args_to_log(
          account_id,
          user_id,
          ticket_deflection_id,
          article_id,
          resolution_channel_id,
          stats
        )
      end

      it 'includes all args' do
        assert_equal(
          {
            account_id: account_id,
            user_id: user_id,
            ticket_deflection_id: ticket_deflection_id,
            article_id: article_id,
            resolution_channel_id: resolution_channel_id,
            stats: stats
          },
          args_to_log
        )
      end
    end

    describe '.work' do
      let(:work!) do
        AnswerBot::TicketSolverJob.work(
          account_id,
          user_id,
          ticket_deflection_id,
          article_id,
          resolution_channel_id,
          stats
        )
      end

      describe 'when ticket_deflection_id is invalid' do
        before do
          TicketDeflection.stubs(:find).
            with(ticket_deflection_id).
            raises(ActiveRecord::RecordNotFound)
        end

        it 'returns false' do
          refute work!
        end

        it 'logs failed job' do
          AnswerBot::TicketSolverJob.expects(:resque_error).
            with(
              <<-LOG.squish
                Job failed:
                  Account Id: #{account.id},
                  Ticket Deflection Id: #{ticket_deflection_id},
                  Article Id: #{article_id},
                  User Id: #{user_id},
                  Error: ActiveRecord::RecordNotFound
              LOG
            ).
            once

          work!
        end
      end

      describe 'when user_id is invalid' do
        before do
          User.stubs(:find).
            with(user_id).
            raises(ActiveRecord::RecordNotFound)
        end

        it 'returns false' do
          refute work!
        end

        it 'logs failed job' do
          AnswerBot::TicketSolverJob.expects(:resque_error).
            with(
              <<-LOG.squish
                Job failed:
                  Account Id: #{account.id},
                  Ticket Deflection Id: #{ticket_deflection_id},
                  Article Id: #{article_id},
                  User Id: #{user_id},
                  Error: ActiveRecord::RecordNotFound
              LOG
            ).
            once

          work!
        end
      end

      describe 'when ticket is already SOLVED' do
        let(:ticket_deflection) { ticket_deflections(:solved) }

        it 'returns false' do
          refute work!
        end
      end

      it 'marks ticket_deflection as SOLVED' do
        work!

        assert_equal TicketDeflection::STATE_SOLVED, ticket_deflection.state
      end

      it 'marks ticket as SOLVED' do
        work!

        assert_equal StatusType.SOLVED, ticket_deflection.ticket.reload.status_id
      end

      it 'sets the state_updated_at in the ticket_deflection' do
        work!

        assert_equal Time.now, ticket_deflection.state_updated_at
      end

      it 'sets the solved_article_id in the ticket_deflection' do
        work!

        assert_equal article_id.to_i, ticket_deflection.solved_article_id
      end

      it 'records the resolution channel id in ticket deflection' do
        work!

        assert_equal resolution_channel_id, ticket_deflection.resolution_channel_id
      end

      it 'records ticket audits' do
        work!

        audit_events = ticket_deflection.ticket.audits.map do |a|
          a.events.map(&:type)
        end.flatten
        assert_includes(audit_events, 'AutomaticAnswerSolve')
      end

      it 'logs successful job' do
        AnswerBot::TicketSolverJob.expects(:resque_log).
          with(
            <<-LOG.squish
              Job completed:
                Account Id: #{account.id},
                Ticket Deflection Id: #{ticket_deflection_id},
                Article Id: #{article_id},
                User Id: #{user_id}
            LOG
          ).
          once

        work!
      end

      describe 'stats recording' do
        let(:stats) do
          {
            subdomain: 'mondocam',
            via_id: 1,
            channel: 'webform',
            model_version: '041000',
            language: 'en'
          }
        end
        let(:enqueue!) do
          AnswerBot::TicketSolverJob.enqueue(
            account_id,
            user_id,
            ticket_deflection_id,
            article_id,
            resolution_channel_id,
            stats
          )
        end

        it 'initialize answer bot statsd client correctly' do
          Datadog::AnswerBot.expects(:new).
            with(
              via_id: 1,
              subdomain: 'mondocam',
              deflection_channel_id: ticket_deflection.deflection_channel_id,
              resolution_channel_id: resolution_channel_id
            ).
            returns(ab_statsd_client)

          enqueue!
        end

        describe 'when it is from mobile' do
          let(:stats) do
            {
              subdomain: 'mondocam',
              via_id: 1,
              channel: 'webform',
              model_version: '041000',
              language: 'en',
              mobile: true
            }
          end

          it 'records ticket_solved action' do
            ab_statsd_client.expects(:solved).with(
              channel: 'webform',
              model_version: '041000',
              language: 'en',
              mobile: true
            ).once

            enqueue!
          end
        end

        describe 'when it is NOT from mobile' do
          it 'records ticket_solved action' do
            ab_statsd_client.expects(:solved).with(
              channel: 'webform',
              model_version: '041000',
              language: 'en'
            ).once

            enqueue!
          end
        end
      end
    end
  end
end
