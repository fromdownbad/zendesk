require_relative "../../../support/job_helper"

SingleCov.covered! uncovered: 1

describe AnswerBot::TicketDeflectionTaggingJob do
  fixtures :tickets, :accounts

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:ab_tag) { "ab_suggest_true" }

  describe '#work' do
    it 'adds the given tag to the ticket' do
      existing_tag = ticket.current_tags

      AnswerBot::TicketDeflectionTaggingJob.perform(account.id, ticket.id, ab_tag)

      ticket.reload
      assert_equal ticket.current_tags, "#{ab_tag} #{existing_tag}"
    end
  end

  describe 'when exceptions are raised' do
    before do
      ticket.reload
    end
    it 'does fail to update tags' do
      Ticket.any_instance.stubs(:update_attributes!).raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, Ticket.new)

      AnswerBot::TicketDeflectionTaggingJob.perform(account.id, ticket.id, ab_tag)

      ticket.reload
      assert_equal ticket.current_tags, "hello"
    end
  end

  describe 'when ticket status is closed' do
    before do
      Ticket.any_instance.stubs(:status).returns('closed')
    end

    it 'does not try to do update' do
      Ticket.any_instance.expects(:save!).never

      AnswerBot::TicketDeflectionTaggingJob.perform(account.id, ticket.id, ab_tag)
    end
  end
end
