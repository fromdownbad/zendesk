require_relative "../../support/job_helper"

SingleCov.covered!

describe UserBulkUpdateJobV2 do
  include DomainEventsHelper
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }
  let(:span) { stub(set_tag: {}, finish: {}) }

  before do
    @account = FactoryBot.create(:account)
    @user1 = FactoryBot.create(:user, account: @account, name: 'User 1', role: :end_user, external_id: 'external_id 1')
    @user2 = FactoryBot.create(:user, account: @account, name: 'User 2', role: :end_user, external_id: 'external_id 2')
    @organization = FactoryBot.create(:organization, account: @account, name: 'Organization')
    @job = UserBulkUpdateJobV2.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: @account.id,
      user_id: @account.owner_id,
      user_ids: [@user1.id, @user2.id],
      user: { organization_id: @organization.id, tags: "tag1, tag2", alias: "my_alias" },
      key: :id)
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  it "emits correct domain events" do
    @job.perform
    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_created).size
    assert_equal 2, decode_user_events(domain_event_publisher.events, :user_organization_added).size
  end

  describe "updating users" do
    before do
      ZendeskAPM.stubs(:trace).returns(span)
    end

    describe 'instrumentation' do
      it 'sets correct APM facet tags' do
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :edit)
        span.expects(:set_tag).with('zendesk.status', 'Updated')
        @job.perform
      end
    end

    describe "with current user having suitable permissions" do
      describe "when referencing to ids " do
        before do
          @job.perform
        end

        it "updates the users" do
          assert_equal [], @job.status["results"]
          assert_equal @organization.id, @user1.reload.organization.id
          assert_equal @organization.id, @user2.reload.organization.id
        end

        it "emits correct user domain events" do
          assert_equal 2, decode_user_events(domain_event_publisher.events, :tags_changed).size
          assert_equal 2, decode_user_events(domain_event_publisher.events, :user_alias_changed).size
          assert_equal 2, decode_user_events(domain_event_publisher.events, :user_organization_added).size
        end
      end

      describe "when referencing to external_ids" do
        before do
          @job.options[:user_ids] = [@user1.external_id, @user2.external_id]
          @job.options[:key] = :external_id
          @job.perform
        end

        it "updates the users" do
          assert_equal [], @job.status["results"]
          assert_equal @organization.id, @user1.reload.organization.id
          assert_equal @organization.id, @user2.reload.organization.id
        end

        it "emits correct user domain events" do
          assert_equal 2, decode_user_events(domain_event_publisher.events, :tags_changed).size
          assert_equal 2, decode_user_events(domain_event_publisher.events, :user_alias_changed).size
          assert_equal 2, decode_user_events(domain_event_publisher.events, :user_organization_added).size
        end
      end

      describe "when referencing to external_ids with a different case" do
        before do
          @user3 = FactoryBot.create(:user, account: @account, name: 'User 3', role: :end_user, external_id: 'EXTERNAL_ID 3')
          @job.options[:user_ids] = [@user1.external_id.swapcase, @user3.external_id.swapcase]
          @job.options[:user] = { tags: "external_id_case", alias: "my_tag", organization_id: @organization.id }
          @job.options[:key] = "external_id"
          @job.perform
        end

        it "updates the users despite case difference" do
          assert_equal [], @job.status["results"]
          assert_equal ["external_id_case"], @user1.reload.tags
          assert_equal ["external_id_case"], @user1.reload.tags
          assert_equal 'EXTERNAL_ID 1', @user1.reload.external_id
          assert_equal 'external_id 3', @user3.reload.external_id
        end

        it "emits correct user domain events" do
          assert_equal 2, decode_user_events(domain_event_publisher.events, :tags_changed).size
          assert_equal 2, decode_user_events(domain_event_publisher.events, :user_alias_changed).size
          assert_equal 2, decode_user_events(domain_event_publisher.events, :user_organization_added).size
        end
      end

      describe "when referencing blank external_id" do
        before do
          @job.options[:user_ids] = [""]
          @job.options[:user] = { tags: "external_id_blank", alias: "my_tag", organization_id: @organization.id }
          @job.options[:key] = "external_id"
          @job.perform
        end

        it "fails to find the user" do
          assert_equal [{"id" => "", "error" => "UserNotFound"}], @job.status["results"]
        end

        it "does not emit user domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events).size
        end

        it 'sets correct APM facet tags' do
          span.expects(:set_tag).with('zendesk.id', any_parameters)
          span.expects(:set_tag).with('zendesk.action', :edit)
          span.expects(:set_tag).with('zendesk.status', 'Failed')
          span.expects(:set_tag).with('zendesk.error', 'UserNotFound')
          span.expects(:set_tag).with('zendesk.details', any_parameters).never
          @job.perform
        end
      end

      describe "with invalid ids" do
        before do
          @job.options[:user_ids] = [0, -1]
          @job.perform
        end

        it "returns errors" do
          assert_includes @job.status["results"], "id" => 0, "error" => "UserNotFound"
          assert_includes @job.status["results"], "id" => -1, "error" => "UserNotFound"
        end

        it "does not emit user domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events).size
        end
      end

      describe "when adding or editing tags" do
        before do
          @account.settings.has_user_tags = true
          @account.save!
          @job.options[:user] = { tags: 'moose, goose' }
          @job.perform
        end

        it "updates tags" do
          assert_equal [], @job.status["results"]
          assert_equal ["goose", "moose"], @user1.reload.tags.sort
          assert_equal ["goose", "moose"], @user2.reload.tags.sort
        end

        it "does not emit user domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :tags_changed).size, 2
        end
      end

      describe "when passing in a default group id" do
        before do
          @user3 = FactoryBot.create(:user, id: 45000, account: @account, name: 'User 3', role: :agent)
          @user4 = FactoryBot.create(:user, id: 45001, account: @account, name: 'User 3', role: :agent)
          @group1 = FactoryBot.create(:group, id: 45000, account: @account, name: 'Group 1')
          @group2 = FactoryBot.create(:group, id: 45001, account: @account, name: 'Group 2')

          @user3.groups << [@group1, @group2]
          @user4.groups << [@group1, @group2]

          @user3.save!
          @user4.save!

          @job.options[:user_ids] = [@user3.id, @user4.id]
          @job.options[:user] = { default_group_id: @group1.id }
        end

        it "updates the default groups" do
          @job.perform

          assert_equal @group1.id, @user3.reload.default_group_id
          assert_equal @group1.id, @user4.reload.default_group_id
        end
      end
    end

    describe "with current user not having suitable permissions" do
      before do
        @job.stubs(:current_user).returns(User.new)
      end

      it "does not update users and returns errors" do
        @job.perform
        assert_nil @user1.reload.organization
        assert_nil @user2.reload.organization
        assert_includes @job.status["results"], "id" => @user1.id, "error" => "PermissionDenied"
        assert_includes @job.status["results"], "id" => @user2.id, "error" => "PermissionDenied"
      end

      it "does not emit user domain events" do
        @job.perform
        assert_equal decode_user_events(domain_event_publisher.events).size, 0
      end

      it 'sets correct APM facet tags' do
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :edit)
        span.expects(:set_tag).with('zendesk.status', 'Failed')
        span.expects(:set_tag).with('zendesk.error', 'PermissionDenied')
        span.expects(:set_tag).with('zendesk.details', any_parameters).never
        @job.perform
      end
    end

    describe "with inexistent organization" do
      before do
        @job.options["user"].merge!(organization_id: 9999)
      end

      it "return errors on statuses" do
        @job.perform
        assert_includes @job.status["results"], "id" => @user1.id, "error" => "OrganizationNotFound"
        assert_includes @job.status["results"], "id" => @user2.id, "error" => "OrganizationNotFound"
      end

      it "does not emit user domain events" do
        @job.perform
        assert_equal decode_user_events(domain_event_publisher.events).size, 0
      end

      it 'sets correct APM facet tags' do
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :edit)
        span.expects(:set_tag).with('zendesk.status', 'Failed')
        span.expects(:set_tag).with('zendesk.error', 'OrganizationNotFound')
        span.expects(:set_tag).with('zendesk.details', any_parameters).never
        @job.perform
      end
    end

    describe "with no organization" do
      before { @job.options[:user] = {} }

      it "updates the users" do
        @job.perform
        refute @job.options["user"]["organization_id"]
        assert_equal [], @job.status["results"]
      end

      it "does not emit user domain events" do
        @job.perform
        assert_equal decode_user_events(domain_event_publisher.events).size, 0
      end
    end

    describe "with no name" do
      before { @job.options[:user].merge!(name: '') }

      it 'sets correct APM facet tags' do
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :edit)
        span.expects(:set_tag).with('zendesk.status', 'Failed')
        span.expects(:set_tag).with('zendesk.error', 'UserUpdateError')
        span.expects(:set_tag).with('zendesk.details', any_parameters)
        @job.perform
      end
    end
  end

  describe "deleting users" do
    before do
      @job.options[:submit_type] = 'delete'

      # stubs the GDPR soft-delete event to channels framework
      Channels::AnyChannel::UserDestroyEventHandler.stubs(:user_deleted)
    end

    describe "with invalid ids" do
      before do
        @job.options[:user_ids] = [0, -1]
      end

      it 'sets correct APM facet tags' do
        ZendeskAPM.stubs(:trace).returns(span)
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :delete)
        span.expects(:set_tag).with('zendesk.status', 'Failed')
        span.expects(:set_tag).with('zendesk.error', 'UserNotFound')
        span.expects(:set_tag).with('zendesk.details', any_parameters).never
        @job.perform
      end
    end

    describe "with current user having suitable permissions" do
      describe "when referencing to ids" do
        it "deletes the users" do
          @job.perform
          assert_equal [], @job.status["results"]
          assert_nil @account.users.find_by_id(@user1.id)
          assert_nil @account.users.find_by_id(@user2.id)
        end

        it "emits correct user_soft_deleted domain events" do
          @job.perform
          assert_equal decode_user_events(domain_event_publisher.events, :user_soft_deleted).size, 2
        end

        it 'sets correct APM facet tags' do
          ZendeskAPM.stubs(:trace).returns(span)
          span.expects(:set_tag).with('zendesk.id', any_parameters)
          span.expects(:set_tag).with('zendesk.action', :delete)
          span.expects(:set_tag).with('zendesk.status', 'Deleted')
          @job.perform
        end
      end

      describe "when referencing to external_ids" do
        before do
          @job.options[:user_ids] = [@user1.external_id, @user2.external_id]
          @job.options[:key] = :external_id
          @job.perform
        end

        it "deletes the users" do
          assert_equal [], @job.status["results"]
          assert_nil @account.users.find_by_external_id(@user1.external_id)
          assert_nil @account.users.find_by_external_id(@user2.external_id)
        end

        it "emits correct user_soft_deleted domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_soft_deleted).size, 2
        end
      end
    end

    describe "with current user not having suitable permissions" do
      before do
        @job.stubs(:current_user).returns(User.new)
      end

      it "does not delete users and returns errors" do
        @job.perform
        assert @user1.reload.is_active?
        assert @user2.reload.is_active?
        assert_includes @job.status["results"], "id" => @user1.id, "error" => "PermissionDenied"
        assert_includes @job.status["results"], "id" => @user2.id, "error" => "PermissionDenied"
      end

      it "does not emit domain events" do
        @job.perform
        assert_equal decode_user_events(domain_event_publisher.events, :user_soft_deleted).size, 0
      end

      it 'sets correct APM facet tags' do
        ZendeskAPM.stubs(:trace).returns(span)
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :delete)
        span.expects(:set_tag).with('zendesk.status', 'Failed')
        span.expects(:set_tag).with('zendesk.error', 'PermissionDenied')
        span.expects(:set_tag).with('zendesk.details', any_parameters).never
        @job.perform
      end
    end

    describe "when one user fails to be deleted" do
      before do
        @user3 = FactoryBot.create(:user, account: @account, name: 'User 3', role: :end_user)
        ticket = Ticket.new(requester: @user2, account: @account, subject: 'ticket subject', description: 'testing ticket')
        ticket.save
        @job.options[:user_ids] = [@user1.id, @user2.id, @user3.id]
      end

      it "deletes the others and leaves the failing user's identities intact" do
        assert_difference -> { @account.users.count }, -2 do
          @job.perform
        end
        expected_results = [{"id" => @user2.id, "error" => "UserDeleteError", "details" => "User is requester on 1 ticket that is not closed."}]
        assert_equal expected_results, @job.status["results"]
        assert_nil @account.users.find_by_id(@user1.id)
        assert_nil @account.users.find_by_id(@user3.id)
        assert_equal 1, @user2.identities.count
      end

      it "does not emit user_identity_removed events" do
        @job.perform
        assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
      end

      it "does not emit domain events for failed operation" do
        @job.perform
        deleted_events = decode_user_events(domain_event_publisher.events, :user_soft_deleted)
        assert_equal deleted_events.size, 2
        deleted_events.each do |event|
          assert_not_equal event.user.id.value, @user2.id
        end
      end

      it 'sets correct APM facet tags' do
        ZendeskAPM.stubs(:trace).returns(span)
        span.expects(:set_tag).with('zendesk.id', any_parameters)
        span.expects(:set_tag).with('zendesk.action', :delete)
        span.expects(:set_tag).with('zendesk.status', 'Failed')
        span.expects(:set_tag).with('zendesk.error', 'UserDeleteError')
        span.expects(:set_tag).with('zendesk.details', any_parameters)
        @job.perform
      end
    end
  end
end
