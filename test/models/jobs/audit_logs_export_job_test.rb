require_relative '../../support/test_helper'

SingleCov.covered!

describe AuditLogsExportJob do
  fixtures :accounts, :users

  describe '#perform' do
    let(:account) { accounts(:minimum) }
    let(:event) do
      CIA::Event.new(
        account: account,
        actor: user_agent,
        created_at: Time.parse('2019-01-01T00:00:00Z'),
        ip_address: '127.0.0.1',
        source: subscription,
        action: "update",
        visible: true
      )
    end
    let(:options) { {} }
    let(:subscription) { subscriptions(:minimum) }
    let(:user_admin) { users(:minimum_admin) }
    let(:user_agent) { users(:minimum_agent) }

    before do
      Timecop.freeze(2020, 4, 1, 10, 30)

      event.save!
      # Adding internal attribute change that is not in CIA::AttributeChange::VISIBLE_ATTRIBUTES
      CIA::AttributeChange.create!(event: event, source: event.source, attribute_name: "made_up", old_value: "1", new_value: "2")
      CIA::AttributeChange.create!(event: event, source: event.source, attribute_name: "max_agents", old_value: "1", new_value: "2")
      CIA::AttributeChange.create!(event: event, source: event.source, attribute_name: "plan_type", old_value: "2", new_value: "3")
    end

    it 'sends an email' do
      assert_emails_sent { AuditLogsExportJob.perform(account.id, user_admin.id) }
    end

    it 'creates an expirable attachment' do
      assert_difference('ExpirableAttachment.count(:all)') do
        AuditLogsExportJob.perform(account.id, user_admin.id)
      end
    end

    it 'is enqueued in the medium priority queue' do
      assert_equal :medium, Resque.queue_from_class(AuditLogsExportJob)
    end

    it 'is not throttled at resque level' do
      assert(AuditLogsExportJob.settings[:disabled])
    end

    describe 'attachment' do
      before do
        generate_csv
      end

      it 'has the correct content-type' do
        assert_equal 'application/zip', @attachment.content_type
      end

      it 'has the correct filename' do
        assert_equal "audit-logs-export-2020-04-01T10_30_00-csv.zip", @attachment.filename
      end

      it 'has the right headers in the csv' do
        headers = %("Time","Actor","IP","Type","Item changed","Changes")
        assert_equal headers, @csv_contents[0]
      end

      it 'has 2 rows => header + content' do
        assert_equal 2, @csv_contents.count
      end

      it 'has the right content in the csv' do
        expected = [
          event.created_at.utc.iso8601,
          user_agent.id,
          event.ip_address,
          'update',
          'Subscription',
          'Max. agents changed from 1 to 2,Plan type changed from 2 to 3'
        ].map { |v| "\"#{v}\"" }.join(",")
        assert_equal expected, @csv_contents[1]
      end

      describe 'CSV sanitization' do
        let(:event) do
          CIA::Event.new(
            account: account,
            actor: user_agent,
            source: subscription,
            action: "=AND(2>1)", # Try to inject some nastiness
            visible: true
          )
        end

        it "escapes CSV macros" do
          assert_equal "\"'=AND(2>1)\"", @csv_contents[1].split(',')[3]
        end
      end

      describe 'filtering' do
        let(:options) do
          { filter: { created_at: ['2020-01-01T00:00:00Z', '2020-02-01T00:00:00Z'] } }
        end

        it 'has no content' do
          assert_equal 1, @csv_contents.count
        end
      end
    end

    describe_with_arturo_enabled :audit_logs_export_include_name do
      before do
        generate_csv
      end

      it 'has the right headers in the csv' do
        headers = %("Time","Actor ID","Actor","IP","Type","Item changed","Changes")
        assert_equal headers, @csv_contents[0]
      end

      it 'has the right content in the csv' do
        expected = [
          event.created_at.utc.iso8601,
          user_agent.id,
          user_agent.name,
          event.ip_address,
          'update',
          'Subscription',
          'Max. agents changed from 1 to 2,Plan type changed from 2 to 3'
        ].map { |v| "\"#{v}\"" }.join(",")
        assert_equal expected, @csv_contents[1]
      end

      it 'handles a deleted actor' do
        CIA::Event.any_instance.stubs(:actor).returns(nil)
        generate_csv # regenerate the CSV
        expected = [
          event.created_at.utc.iso8601,
          user_agent.id,
          '',
          event.ip_address,
          'update',
          'Subscription',
          'Max. agents changed from 1 to 2,Plan type changed from 2 to 3'
        ].map { |v| "\"#{v}\"" }.join(",")
        assert_equal expected, @csv_contents[1]
      end

      describe 'with CSV injection attack' do
        let(:user_agent) do
          User.any_instance.stubs(name: '=AND(2>1)')
          users(:minimum_agent)
        end

        it 'has the right content in the csv' do
          expected = [
            event.created_at.utc.iso8601,
            user_agent.id,
            "'=AND(2>1)",
            event.ip_address,
            'update',
            'Subscription',
            'Max. agents changed from 1 to 2,Plan type changed from 2 to 3'
          ].map { |v| "\"#{v}\"" }.join(",")
          assert_equal expected, @csv_contents[1]
        end
      end
    end
  end

  def generate_csv
    AuditLogsExportJob.perform(account.id, user_admin.id, options)
    @attachment = ExpirableAttachment.last

    Zip::InputStream.open(@attachment.public_filename) do |io|
      io.get_next_entry
      @csv_contents = io.read.split("\n")
    end
  end
end
