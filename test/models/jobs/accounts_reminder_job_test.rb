require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'AccountsReminderJob' do
  fixtures :accounts, :users

  describe ".work" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_admin) }
    let(:inactive_account) { accounts(:inactive) }
    let(:suspended_account) { accounts(:extra_large) }

    before do
      inactive_account.update_attribute(:subdomain, "skynet1")
      suspended_account.update_attribute(:is_serviceable, false)
      UserContactInformation.create! do |new_contact_info|
        new_contact_info.account = account
        new_contact_info.user_id = user.id
        new_contact_info.email = user.email
        new_contact_info.name = user.name
        new_contact_info.phone = nil
        new_contact_info.role = user.roles
        new_contact_info.is_email_verified = true
      end
    end

    describe "without account_type" do
      describe "when accounts are found for the email" do
        let(:email) { user.email }

        it "finds owned account" do
          assert_equal [account], AccountsReminderJob.new(email, I18n.locale).send(:accounts)
        end

        it "sends reminder" do
          UsersMailer.expects(:deliver_reminder)
          AccountsReminderJob.work(email, I18n.locale)
        end
      end

      describe "when some contacts belong to inactive/deleted accounts" do
        it "filters out the missing accounts" do
          AccountsReminderJob.any_instance.expects(:contacts).returns([stub(account: account), stub(account: nil)])
          UsersMailer.expects(:deliver_reminder).with(user.email, anything, [account])
          AccountsReminderJob.work(user.email, I18n.locale)
        end
        it "filters out inactive accounts" do
          AccountsReminderJob.any_instance.expects(:contacts).returns([stub(account: account), stub(account: inactive_account)])
          UsersMailer.expects(:deliver_reminder).with(user.email, anything, [account])
          AccountsReminderJob.work(user.email, I18n.locale)
        end

        it "does not filter out suspended accounts" do
          AccountsReminderJob.any_instance.expects(:contacts).returns([stub(account: account), stub(account: suspended_account)])
          UsersMailer.expects(:deliver_reminder).with(user.email, anything, [account, suspended_account])
          AccountsReminderJob.work(user.email, I18n.locale)
        end
      end

      describe "when no accounts are found" do
        let(:email) { "unknown@email.com" }

        it "finds no owned accounts" do
          assert_equal [], AccountsReminderJob.new(email, I18n.locale).send(:accounts)
        end

        it "sends reminder_no_accounts" do
          UsersMailer.expects(:deliver_reminder_no_accounts)
          AccountsReminderJob.work(email, I18n.locale)
        end
      end
    end
  end
end
