require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 3

describe PushNotificationSdkRegistrationJob do
  fixtures :accounts, :mobile_sdk_apps, :user_identities

  describe "#work" do
    let(:account) { accounts(:minimum_sdk) }
    let(:mobile_sdk_app) { mobile_sdk_apps(:minimum_sdk) }
    let(:urban_airship_app_key) { "urban_airship_app_key" }
    let(:urban_airship_master_secret) { "urban_airship_master_secret" }

    before do
      mobile_sdk_app.settings.push_notifications_type = MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP
      mobile_sdk_app.settings.urban_airship_key = urban_airship_app_key
      mobile_sdk_app.settings.urban_airship_master_secret = urban_airship_master_secret
      mobile_sdk_app.save!

      PushNotifications::SdkAnonymousDeviceIdentifier.any_instance.stubs(:register).returns(true)
      @device = PushNotifications::SdkAnonymousDeviceIdentifier.create!(
        token: "foo",
        device_type: "iphone",
        account: account,
        user_sdk_identity: user_identities(:mobile_sdk_identity),
        mobile_sdk_app: mobile_sdk_app
      )
    end

    it "loads the correct Urban Airship credentials" do
      PushNotifications::UrbanAirshipClient.expects(:new).with(
        urban_airship_app_key,
        urban_airship_master_secret,
        statsd_tags: ["name:sdk_registration"]
      ).returns(
        mock(register: [stub])
      )

      PushNotificationSdkRegistrationJob.work(
        'account_id' => account.id,
        'mobile_sdk_app_id' => mobile_sdk_app.id,
        'identifier_id' => @device.id
      )
    end

    it "registers the device" do
      PushNotifications::UrbanAirshipClient.any_instance.expects(:register).with(
        @device
      ).returns(stub)

      PushNotificationSdkRegistrationJob.work(
        'account_id' => account.id,
        'mobile_sdk_app_id' => mobile_sdk_app.id,
        'identifier_id' => @device.id
      )
    end
  end
end
