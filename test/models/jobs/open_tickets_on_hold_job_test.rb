require_relative "../../support/job_helper"

SingleCov.covered! uncovered: 1

describe OpenTicketsOnHoldJob do
  fixtures :accounts, :tickets, :users

  before do
    @account = accounts(:minimum)
    @ticket  = tickets(:minimum_1)
  end

  describe "#work" do
    before do
      assert(@ticket.update_columns(status_id: StatusType.HOLD))
      assert_equal 1, @account.tickets.where(status_id: StatusType.HOLD).count(:all)
    end

    it "changes ticket status from hold to open" do
      OpenTicketsOnHoldJob.perform(@account.id)
      assert_equal 0, @account.tickets.where(status_id: StatusType.HOLD).count(:all)
    end
  end
end
