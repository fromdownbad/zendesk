require "zendesk_logging"
require_relative "../../../support/job_helper"
require_relative "../../../support/rule"
require_relative "../../../support/simplified_email_threading/helper"

SingleCov.covered! uncovered: 2

describe SimplifiedEmailThreading::OptOutJob do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper
  include TestSupport::SimplifiedEmailThreading::Helper

  fixtures :accounts, :rules, :translation_locales, :users

  let!(:account) { accounts(:minimum) }

  let!(:admin) { account.admins.first }

  let(:opt_in_results) do
    opt_in = Zendesk::SimplifiedEmailThreading::OptIn.new(account.id)
    opt_in.update_opt_in_rules(dry_run: false)
  end

  let(:locales) do
    [
      translation_locales(:spanish),
      translation_locales(:english_by_zendesk),
      translation_locales(:japanese)
    ]
  end

  let(:translated_subject_english)  { "test subject english" }
  let(:translated_subject_spanish)  { "test subject spanish" }
  let(:translated_subject_japanese) { "test subject japanese" }

  let(:translated_body_english)  { "test body english" }
  let(:translated_body_spanish)  { "test body spanish" }
  let(:translated_body_japanese) { "test body japanese" }

  let(:translated_instructions_english)  { "test instructions english" }
  let(:translated_instructions_spanish)  { "test instructions spanish" }
  let(:translated_instructions_japanese) { "test instructions japanese" }

  let(:translated_link_english) { "https://english@example.com" }
  let(:translated_link_spanish) { "https://spanish@example.com" }
  let(:translated_link_japanese) { "https://japanese@example.com" }

  before do
    assert_not_empty opt_in_results
  end

  describe "class properties" do
    it "enforces in flight limit" do
      assert SimplifiedEmailThreading::OptOutJob.send(:enforce_in_flight_limit?)
    end
  end

  describe "#build_list" do
    let(:dry_run) { true }
    let(:rules_diff) { Zendesk::SimplifiedEmailThreading::OptOut.new(account.id).update_opt_out_rules(dry_run: dry_run) }

    subject do
      job = SimplifiedEmailThreading::OptOutJob.new(
        "uuid",
        account_id: account.id,
        admin_id: admin.id,
        dry_run: dry_run
      )
      updated_rule_ids, _non_updated_rule_ids = rules_diff
      job.send(:build_list, account, updated_rule_ids)
    end

    it "returns affected rules separated by newlines" do
      assert_not_empty subject.split("\n").map { |s| s.split(/,/).map(&:strip) }
    end
  end

  describe "#perform" do
    let(:dry_run) { true }

    describe_with_arturo_enabled :email_simplified_threading do
      subject { SimplifiedEmailThreading::OptOutJob.new("uuid", account_id: account.id, admin_id: admin.id, dry_run: dry_run).perform }

      before do
        stub_request(:put, %r{\.amazonaws\.com/data/expirable_attachments/\d+/simplified_email_threading_opt_out_affected_rules\.zip})
      end

      describe "when account cannot be found" do
        it "raises an exception" do
          assert_raises(ActiveRecord::RecordNotFound) do
            SimplifiedEmailThreading::OptOutJob.new("uuid", account_id: nil, admin_id: admin.id).perform
          end
        end
      end

      describe "when admin cannot be found" do
        it "raises an exception when admin cannot be found" do
          assert_raises(ActiveRecord::RecordNotFound) do
            SimplifiedEmailThreading::OptOutJob.new("uuid", account_id: account.id, admin_id: nil, dry_run: true).perform
          end
        end
      end

      describe "when there is an exception running the opt out job" do
        before do
          # Lotus optimistically updates the simplified_email_threading setting
          # to false during the simplifie email threading opt-out flow.
          account.settings.simplified_email_threading = false
          account.settings.save!
          refute account.reload.settings.simplified_email_threading
          Zendesk::SimplifiedEmailThreading::OptOut.any_instance.expects(:update_opt_out_rules).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
        end

        it "sets the account's simplified_email_threading setting to true" do
          assert_raises(ActiveRecord::RecordNotSaved) { subject }
          assert account.reload.settings.simplified_email_threading
        end
      end

      it "calls SimplifiedEmailThreading::OptOut#update_opt_out_rules" do
        Zendesk::SimplifiedEmailThreading::OptOut.any_instance.expects(:update_opt_out_rules).with(dry_run: true).once
        subject
      end

      it "sends metrics to the statsd client" do
        SimplifiedEmailThreading::OptOutJob.any_instance.stubs(:store_rules_list).returns(mock("attachment", url: ""))
        SimplifiedEmailThreading::OptOutJob.any_instance.stubs(:notify_admins)
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(:enqueued, tags: ["dry_run:#{dry_run}"])
        subject
      end

      it "calls completed" do
        JobWithStatus.any_instance.expects(:completed).once
        subject
      end

      describe "when there are no affected rules" do
        before do
          account.rules.clear
          account.save!
        end

        it "does not create a zip file" do
          Zip::OutputStream.any_instance.expects(:put_next_entry).never
          subject
        end

        it "does not create a new ExpirableAttachment" do
          assert_no_difference("ExpirableAttachment.count") do
            subject
          end
        end

        it "completes with an nil attachment url" do
          assert_nil subject[3][:results][:url]
        end
      end

      describe "when there are affected rules" do
        let(:affected_rules_string) { mock }
        let(:instructions_entry) { mock }

        before do
          SimplifiedEmailThreading::OptOutJob.any_instance.stubs(:build_list).returns(affected_rules_string)
          # Stub automations being invalid, so secondary instructions are included
          Automation.any_instance.stubs(:valid?).returns(false)
        end

        describe "with `dry_run` as true" do
          let(:dry_run) { true }

          describe "and there is an html template to restore" do
            before do
              account.simplified_email_opt_in_settings.create!(name: "html_mail_template", value: "<div>Hello</div>")
            end

            it "creates a zip file containing the instructions and rules list files" do
              setup_base_i18n_assertions
              I18n.stubs(:t).with(regexp_matches(/txt.email/), anything).returns("stubbed value")
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.opt_out.email.instructions", has_entries(locale: account.translation_locale, link: anything)).returns(translated_instructions_english).at_least_once
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.instructions_link", has_entry(locale: account.translation_locale)).returns(translated_link_english).at_least_once
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.opt_out.email.secondary_instructions", has_entries(locale: account.translation_locale)).returns(translated_instructions_english).once
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.html_email_template_instructions", has_entries(locale: account.translation_locale)).returns(translated_instructions_english).once
              locale = account.admins.first.translation_locale.locale
              Zip::Entry.expects(:new).with(anything, "instructions-#{locale}.txt", *([anything] * 7)).returns(instructions_entry)
              Zip::OutputStream.any_instance.expects(:put_next_entry).with(instructions_entry).once
              Zip::OutputStream.any_instance.expects(:puts).with(affected_rules_string).twice
              Zip::OutputStream.any_instance.expects(:puts).with(translated_instructions_english).times(3)
              subject
            end
          end

          describe "when admins on the account have different locales" do
            before { set_admin_locales }

            after { reset_admin_locales }

            describe "creating zip file" do
              it "creates a zip file containing the instructions and rules list files" do
                setup_base_i18n_assertions
                account.admins.map(&:translation_locale).uniq.map do |translation_locale|
                  string_suffix = string_suffix_from_translation_locale(translation_locale)
                  instructions = send("translated_instructions_#{string_suffix}".to_sym)
                  instructions_link = send("translated_link_#{string_suffix}".to_sym)
                  Zip::Entry.expects(:new).with(anything, "instructions-#{translation_locale.locale}.txt", *([anything] * 7)).returns(instructions_entry)
                  I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.opt_out.email.instructions", has_entries(locale: translation_locale, link: anything)).returns(instructions).at_least_once
                  I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.instructions_link", has_entry(locale: translation_locale)).returns(instructions_link).at_least_once
                  I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.opt_out.email.secondary_instructions", has_entries(locale: translation_locale)).returns(instructions).once
                  Zip::OutputStream.any_instance.expects(:put_next_entry).with(instructions_entry).once
                  Zip::OutputStream.any_instance.expects(:puts).with(instructions).twice
                  Zip::OutputStream.any_instance.expects(:puts).with(affected_rules_string).twice
                end
                I18n.stubs(:t).with(regexp_matches(/txt.email/), anything).returns("stubbed value")
                subject
              end
            end
          end

          it "creates a new ExpirableAttachment" do
            assert_difference("ExpirableAttachment.count") { subject }
          end

          it "completes with an attachment url" do
            assert_match(
              /https:\/\/minimum.zendesk-test.com\/expirable_attachments\/token?\/.+\/?name=simplified_email_threading_opt_out_affected_rules.zip/,
              subject[3][:results][:url]
            )
          end
        end

        describe "with `dry_run` as false" do
          let(:dry_run) { false }

          it "creates a new ExpirableAttachment" do
            assert_difference("ExpirableAttachment.count") { subject }
          end

          it "completes with an attachment url" do
            assert_match(
              /https:\/\/minimum.zendesk-test.com\/expirable_attachments\/token?\/.+\/?name=simplified_email_threading_opt_out_affected_rules.zip/,
              subject[3][:results][:url]
            )
          end
        end
      end

      describe "handled exceptions" do
        describe "around persisting the file" do
          describe "ActiveRecord errors" do
            let(:exception) { ActiveRecord::RecordInvalid }

            describe "logging" do
              before do
                ExpirableAttachment.any_instance.expects(:save!).raises(exception.new(account.triggers.first))
              end

              it "logs attachment info and re-raises" do
                SimplifiedEmailThreading::OptOutJob.expects(:resque_error).with do |args|
                  assert args.include?("Unable to create attachment. account_id: #{account.id}, admin_id: #{admin.id}")
                  assert args.include?("error: #{exception}")
                end
                assert_raises(exception) { subject }
              end
            end
          end

          describe "AttachmentFu::NoBackendsAvailableException" do
            let(:exception) { ::Technoweenie::AttachmentFu::NoBackendsAvailableException }

            describe "logging" do
              before do
                ExpirableAttachment.any_instance.expects(:save!).raises(exception)
              end

              it "logs attachment info and re-raises" do
                SimplifiedEmailThreading::OptOutJob.expects(:resque_error).with do |args|
                  assert args.include?("Unable to create attachment. account_id: #{account.id}, admin_id: #{admin.id}")
                  assert args.include?("error: #{exception}")
                end
                assert_raises(exception) { subject }
              end
            end
          end
        end

        describe "around updating rules" do
          let(:exception) { ActiveRecord::RecordInvalid }
          let(:error_message) do
            "Failed to update or diff rules. account_id: #{account.id}, admin_id: #{admin.id}, dry_run: true, error: #{exception}"
          end

          before do
            Zendesk::SimplifiedEmailThreading::OptOut.any_instance.expects(:update_opt_out_rules).
              raises(exception.new(account.triggers.first))
          end

          it "logs info and re-raises" do
            SimplifiedEmailThreading::OptOutJob.expects(:resque_error).with(error_message)
            assert_raises(exception) { subject }
          end
        end
      end
    end
  end

  describe "Zip::Entry" do
    let(:output_stream) { "sample output stream" }
    let(:job) { SimplifiedEmailThreading::OptOutJob.new("uuid", account_id: account.id, admin_id: admin.id) }
    let(:current_time) { Zip::DOSTime.parse(Time.current.strftime(SimplifiedEmailThreading::OptOutJob::DATETIME_FORMAT)) }
    let(:zip_entry_options) { job.send(:zip_entry_options, current_time) }

    describe "#instructions_entry" do
      let(:locale) { "en" }

      it "returns the expected zip entry" do
        instructions_entry = job.send(:instructions_entry, output_stream, locale, zip_entry_options)

        assert_equal output_stream, instructions_entry.zipfile
        assert_equal "instructions-#{locale}.txt", instructions_entry.name
        assert_equal current_time, instructions_entry.time
      end
    end
  end
end
