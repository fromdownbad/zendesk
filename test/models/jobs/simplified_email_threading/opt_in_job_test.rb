require 'zendesk_logging'
require_relative "../../../support/job_helper"
require_relative '../../../support/rule'
require_relative '../../../support/simplified_email_threading/helper'

SingleCov.covered!

describe SimplifiedEmailThreading::OptInJob do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper
  include TestSupport::SimplifiedEmailThreading::Helper

  fixtures :accounts, :rules, :translation_locales, :users

  let!(:account) { accounts(:minimum) }
  let!(:admin) { account.admins.first }

  let(:locales) do
    [
      translation_locales(:spanish),
      translation_locales(:english_by_zendesk),
      translation_locales(:japanese)
    ]
  end

  let(:translated_subject_english)  { "test subject english" }
  let(:translated_subject_spanish)  { "test subject spanish" }
  let(:translated_subject_japanese) { "test subject japanese" }

  let(:translated_body_english)  { "test body english" }
  let(:translated_body_spanish)  { "test body spanish" }
  let(:translated_body_japanese) { "test body japanese" }

  let(:translated_body_no_changes_english) { "test body: no changes english" }

  let(:translated_instructions_english)  { "test instructions english" }
  let(:translated_instructions_spanish)  { "test instructions spanish" }
  let(:translated_instructions_japanese) { "test instructions japanese" }

  let(:translated_link_english) { "https://english@example.com" }
  let(:translated_link_spanish) { "https://spanish@example.com" }
  let(:translated_link_japanese) { "https://japanese@example.com" }

  let(:expected_update_email_template_calls) { 1 }
  let(:expected_update_html_template_calls) { 1 }
  let(:expected_clear_previously_saved_settings_calls) { 1 }

  before do
    if expected_update_email_template_calls >= 1
      Zendesk::SimplifiedEmailThreading::OptIn.any_instance.expects(:update_email_template).times(expected_update_email_template_calls)
    end

    if expected_update_html_template_calls >= 1
      Zendesk::SimplifiedEmailThreading::OptIn.any_instance.expects(:update_html_template).times(expected_update_html_template_calls)
    end

    if expected_clear_previously_saved_settings_calls >= 1
      Zendesk::SimplifiedEmailThreading::OptIn.any_instance.expects(:clear_previously_saved_settings).times(expected_clear_previously_saved_settings_calls)
    end
  end

  describe 'class properties' do
    let(:expected_update_email_template_calls) { 0 }
    let(:expected_update_html_template_calls) { 0 }
    let(:expected_clear_previously_saved_settings_calls) { 0 }

    it 'enforces in flight limit' do
      assert SimplifiedEmailThreading::OptInJob.send(:enforce_in_flight_limit?)
    end
  end

  describe '#build_list' do
    let(:dry_run) { true }
    let(:rules_diff) { Zendesk::SimplifiedEmailThreading::OptIn.new(account.id).update_opt_in_rules(dry_run: dry_run) }
    let(:expected_update_email_template_calls) { 0 }
    let(:expected_update_html_template_calls) { 0 }
    let(:expected_clear_previously_saved_settings_calls) { 0 }

    subject do
      job = SimplifiedEmailThreading::OptInJob.new(
        'uuid',
        account_id: account.id,
        admin_id: admin.id,
        dry_run: dry_run,
        onboarding_completed: false
      )
      job.send(:build_list, account, rules_diff)
    end

    describe_with_arturo_enabled :email_simplified_threading do
      it "returns affected rules separated by newlines" do
        assert_not_empty subject.split("\n").map { |s| s.split(/,/).map(&:strip) }
      end
    end
  end

  describe '#perform' do
    let(:dry_run)              { true }
    let(:onboarding_completed) { true }

    describe_with_arturo_enabled :email_simplified_threading do
      subject { SimplifiedEmailThreading::OptInJob.new('uuid', account_id: account.id, admin_id: admin.id, dry_run: dry_run, onboarding_completed: onboarding_completed).perform }

      before do
        stub_request(:put, %r{\.amazonaws\.com/data/expirable_attachments/\d+/simplified_email_threading_opt_in_affected_rules\.zip})
      end

      describe "when account cannot be found" do
        let(:expected_update_email_template_calls) { 0 }
        let(:expected_update_html_template_calls) { 0 }
        let(:expected_clear_previously_saved_settings_calls) { 0 }

        it 'raises an exception' do
          assert_raises(ActiveRecord::RecordNotFound) do
            SimplifiedEmailThreading::OptInJob.new('uuid', account_id: nil, admin_id: admin.id).perform
          end
        end
      end

      describe "when admin cannot be found" do
        let(:expected_update_email_template_calls) { 0 }
        let(:expected_update_html_template_calls) { 0 }
        let(:expected_clear_previously_saved_settings_calls) { 0 }

        it 'raises an exception when admin cannot be found' do
          assert_raises(ActiveRecord::RecordNotFound) do
            SimplifiedEmailThreading::OptInJob.new('uuid', account_id: account.id, admin_id: nil, dry_run: true, onboarding_completed: true).perform
          end
        end
      end

      describe "mail templates" do
        let(:expected_update_email_template_calls) { 1 }
        let(:expected_update_html_template_calls) { 1 }
        let(:expected_clear_previously_saved_settings_calls) { 1 }

        before do
          Zip::OutputStream.any_instance.expects(:puts).with(regexp_matches(/The following business rules will be updated/)).once
          Zip::OutputStream.any_instance.expects(:puts).with(regexp_matches(/Close ticket 4 days after status is set to solved/)).once
        end

        describe "when updates are not required" do
          it "does not include an additional line of text in the instructions" do
            Zip::OutputStream.any_instance.expects(:puts).with(I18n.t("txt.admin.models.jobs.simplified_email_threading.email.html_email_template_instructions")).never
            subject
          end
        end

        describe "when an update to html template is required" do
          let(:expected_update_html_template_calls) { 0 }

          it "includes an additional line of text in the instructions" do
            Zip::OutputStream.any_instance.expects(:puts).with(I18n.t("txt.admin.models.jobs.simplified_email_threading.email.html_email_template_instructions")).once
            subject
          end
        end

        describe "when updates are required" do
          before do
            account.cc_email_template = "Here is the latest comment — {{ticket.latest_comment}}"
            account.save
          end

          let(:expected_update_email_template_calls) { 0 }
          let(:expected_update_html_template_calls) { 0 }
          let(:expected_clear_previously_saved_settings_calls) { 0 }

          describe "when neither legacy ccs nor followers are enabled" do
            before do
              Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
              Account.any_instance.stubs(:has_ticket_followers_allowed_enabled?).returns(false)
            end

            it "includes additional lines of text in the instructions" do
              Zip::OutputStream.any_instance.expects(:puts).with(I18n.t("txt.admin.models.jobs.simplified_email_threading.email.cc_email_template_instructions")).never
              Zip::OutputStream.any_instance.expects(:puts).with(I18n.t("txt.admin.models.jobs.simplified_email_threading.email.follower_email_template_instructions")).never
              Zip::OutputStream.any_instance.expects(:puts).with(I18n.t("txt.admin.models.jobs.simplified_email_threading.email.html_email_template_instructions")).once
              subject
            end
          end

          describe "when legacy ccs are enabled" do
            before { Account.any_instance.stubs(:is_collaboration_enabled?).returns(true) }

            it "includes additional lines of text in the instructions" do
              Zip::OutputStream.any_instance.expects(:puts).with(I18n.t("txt.admin.models.jobs.simplified_email_threading.email.cc_email_template_instructions")).once
              Zip::OutputStream.any_instance.expects(:puts).with(I18n.t("txt.admin.models.jobs.simplified_email_threading.email.html_email_template_instructions")).once
              subject
            end
          end

          describe "when followers are enabled" do
            before { Account.any_instance.stubs(:has_ticket_followers_allowed_enabled?).returns(true) }

            it "includes additional lines of text in the instructions" do
              Zip::OutputStream.any_instance.expects(:puts).with(I18n.t("txt.admin.models.jobs.simplified_email_threading.email.follower_email_template_instructions")).once
              Zip::OutputStream.any_instance.expects(:puts).with(I18n.t("txt.admin.models.jobs.simplified_email_threading.email.html_email_template_instructions")).once
              subject
            end
          end
        end
      end

      describe "when there is an exception running the opt in job" do
        before do
          # Lotus optimistically updates the simplified_email_threading setting
          # to true during the simplifie email threading opt-in flow.
          account.settings.simplified_email_threading = true
          account.settings.save!
          assert account.reload.settings.simplified_email_threading
          Zendesk::SimplifiedEmailThreading::OptIn.any_instance.expects(:update_opt_in_rules).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
        end

        it "sets the account's simplified_email_threading setting to false" do
          assert_raises(ActiveRecord::RecordNotSaved) { subject }
          refute account.reload.settings.simplified_email_threading
        end
      end

      it 'calls SimplifiedEmailThreading::OptIn#update_opt_in_rules' do
        Zendesk::SimplifiedEmailThreading::OptIn.any_instance.expects(:update_opt_in_rules).with(dry_run: true).once
        subject
      end

      it 'sends metrics to the statsd client' do
        SimplifiedEmailThreading::OptInJob.any_instance.stubs(:store_rules_list).returns(mock('attachment', url: ''))
        SimplifiedEmailThreading::OptInJob.any_instance.stubs(:notify_admins)

        Zendesk::StatsD::Client.any_instance.expects(:increment).
          with(:enqueued, tags: ["dry_run:#{dry_run}", "onboarding_completed:#{dry_run}"])

        subject
      end

      describe 'email notifications' do
        let(:batch_size) { 2 }

        def setup_i18n_assertions
          setup_base_i18n_assertions

          admin_groups = account.admins.group_by(&:translation_locale)

          admin_groups.keys.each do |translation_locale|
            string_suffix = string_suffix_from_translation_locale(translation_locale)
            I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.opt_in.email.instructions", has_entries(locale: translation_locale, link: anything)).returns(send("translated_instructions_#{string_suffix}".to_sym))
            I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.instructions_link", has_entry(locale: translation_locale)).returns(send("translated_link_#{string_suffix}".to_sym))
          end

          admin_groups.values.each do |admin_group|
            admin_group.each_slice(batch_size) do |admin_batch|
              translation_locale = admin_batch.first.translation_locale
              string_suffix = string_suffix_from_translation_locale(translation_locale)

              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.subject", has_entry(locale: translation_locale)).returns(send("translated_subject_#{string_suffix}".to_sym))
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.opt_in.email.instructions", has_entries(locale: translation_locale, link: anything)).returns(send("translated_instructions_#{string_suffix}".to_sym))
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.instructions_link", has_entry(locale: translation_locale)).returns(send("translated_link_#{string_suffix}".to_sym))
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.body", has_entry(locale: translation_locale)).returns(send("translated_body_#{string_suffix}".to_sym))
            end
          end
        end

        before do
          @notification_recipient_batch_size_value = SimplifiedEmailThreading::BaseJob.const_get(:NOTIFICATION_RECIPIENT_BATCH_SIZE)
          SimplifiedEmailThreading::BaseJob.send(:remove_const, :NOTIFICATION_RECIPIENT_BATCH_SIZE)
          SimplifiedEmailThreading::BaseJob.const_set(:NOTIFICATION_RECIPIENT_BATCH_SIZE, batch_size)
        end

        after do
          SimplifiedEmailThreading::BaseJob.send(:remove_const, :NOTIFICATION_RECIPIENT_BATCH_SIZE)
          SimplifiedEmailThreading::BaseJob.const_set(:NOTIFICATION_RECIPIENT_BATCH_SIZE, @notification_recipient_batch_size_value)
        end

        describe 'when the number of admins receiving an email notification exceeds `NOTIFICATION_RECIPIENT_BATCH_SIZE`' do
          before do
            setup_i18n_assertions
          end

          it "sends email notification in batches" do
            JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).times(2).with do |_admins, _identifier, title, content|
              assert_equal translated_subject_english, title
              assert_equal (translated_body_english + "\n\n" + translated_instructions_english), content
            end

            subject
          end
        end

        describe 'when admins on the account have different locales' do
          before do
            set_admin_locales
            setup_i18n_assertions
          end

          after { reset_admin_locales }

          it "sends email notification in batches by locale" do
            JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).times(3).with do |admins, _identifier, title, content|
              assert_equal 1, admins.size

              case admins.first.translation_locale
              when translation_locales(:spanish)
                assert_equal translated_subject_spanish, title
                assert_equal (translated_body_spanish + "\n\n" + translated_instructions_spanish), content
              when translation_locales(:english_by_zendesk)
                assert_equal translated_subject_english, title
                assert_equal (translated_body_english + "\n\n" + translated_instructions_english), content
              when translation_locales(:japanese)
                assert_equal translated_subject_japanese, title
                assert_equal (translated_body_japanese + "\n\n" + translated_instructions_japanese), content
              end
            end

            subject
          end
        end
      end

      it 'calls completed' do
        JobWithStatus.any_instance.expects(:completed).once
        subject
      end

      describe 'with `onboarding_completed` as true' do
        describe 'when there are no affected rules' do
          before do
            account.rules.clear
            account.save!
          end

          it 'notifies admins that there are no required rule changes' do
            locale = account.admins.first.translation_locale

            I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.subject", has_entry(locale: locale)).returns(translated_subject_english)
            I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.body_no_changes", has_entry(locale: locale)).returns(translated_body_no_changes_english)

            JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).with do |admins, _identifier, title, content|
              assert_equal 3, admins.size
              assert_equal translated_subject_english, title
              assert_equal translated_body_no_changes_english, content
            end

            subject
          end

          it 'does not create a zip file' do
            Zip::OutputStream.any_instance.expects(:put_next_entry).never

            subject
          end

          it 'does not create a new ExpirableAttachment' do
            assert_no_difference("ExpirableAttachment.count") do
              subject
            end
          end

          it 'completes with an nil attachment url' do
            assert_nil subject[3][:results][:url]
          end
        end

        describe 'when there are affected rules' do
          let(:affected_rules_string) { mock }
          let(:instructions_entry) { mock }
          let(:onboarding_completed) { true }

          before do
            SimplifiedEmailThreading::OptInJob.any_instance.stubs(:build_list).returns(affected_rules_string)
          end

          it 'sends an email notification to account admins' do
            JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).at_least_once

            subject
          end

          def setup_admin_i18n_assertions
            account.admins.map(&:translation_locale).uniq.map do |translation_locale|
              string_suffix = string_suffix_from_translation_locale(translation_locale)
              subject = send("translated_subject_#{string_suffix}".to_sym)
              body = send("translated_body_#{string_suffix}".to_sym)
              instructions = send("translated_instructions_#{string_suffix}".to_sym)
              instructions_link = send("translated_link_#{string_suffix}".to_sym)
              Zip::Entry.expects(:new).with(anything, "instructions-#{translation_locale.locale}.txt", *([anything] * 7)).returns(instructions_entry)
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.subject", has_entry(locale: translation_locale)).returns(subject)
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.body", has_entry(locale: translation_locale)).returns(body)
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.opt_in.email.instructions", has_entries(locale: translation_locale, link: anything)).returns(instructions).at_least_once
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.instructions_link", has_entry(locale: translation_locale)).returns(instructions_link).at_least_once
              Zip::OutputStream.any_instance.expects(:put_next_entry).with(instructions_entry)
              Zip::OutputStream.any_instance.expects(:puts).with(instructions)
              Zip::OutputStream.any_instance.expects(:puts).with(affected_rules_string)
            end
          end

          describe 'with `dry_run` as true' do
            let(:dry_run) { true }

            it 'notifies admins about affected rules and recommended changes' do
              setup_base_i18n_assertions
              locale = account.admins.first.translation_locale
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.subject", has_entry(locale: locale)).returns(translated_subject_english)
              I18n.expects(:t).twice.with("txt.admin.models.jobs.simplified_email_threading.opt_in.email.instructions", has_entries(locale: locale, link: anything)).returns(translated_instructions_english)
              I18n.expects(:t).twice.with("txt.admin.models.jobs.simplified_email_threading.email.instructions_link", has_entry(locale: locale)).returns(translated_link_english)
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.body", has_entry(locale: locale)).returns(translated_body_english)

              JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).with do |admins, _identifier, title, content|
                assert_equal 3, admins.size
                assert_equal translated_subject_english, title
                assert_equal (translated_body_english + "\n\n" + translated_instructions_english), content
              end

              subject
            end

            it 'creates a zip file containing the instructions and rules list files' do
              setup_base_i18n_assertions
              I18n.stubs(:t).with(regexp_matches(/txt.email/), anything).returns("stubbed value")
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.subject", has_entry(locale: account.translation_locale)).returns(translated_subject_english).at_least_once
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.body", has_entry(locale: account.translation_locale)).returns(translated_body_english).at_least_once
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.opt_in.email.instructions", has_entries(locale: account.translation_locale, link: anything)).returns(translated_instructions_english).at_least_once
              I18n.expects(:t).with("txt.admin.models.jobs.simplified_email_threading.email.instructions_link", has_entry(locale: account.translation_locale)).returns(translated_link_english).at_least_once
              locale = account.admins.first.translation_locale.locale
              Zip::Entry.expects(:new).with(anything, "instructions-#{locale}.txt", *([anything] * 7)).returns(instructions_entry)
              Zip::OutputStream.any_instance.expects(:put_next_entry).with(instructions_entry).once
              Zip::OutputStream.any_instance.expects(:puts).with(affected_rules_string)
              Zip::OutputStream.any_instance.expects(:puts).with(translated_instructions_english)

              subject
            end

            describe 'when admins on the account have different locales' do
              before { set_admin_locales }

              after { reset_admin_locales }

              describe 'creating zip file' do
                it 'creates a zip file containing the instructions and rules list files' do
                  setup_base_i18n_assertions
                  setup_admin_i18n_assertions
                  I18n.stubs(:t).with(regexp_matches(/txt.email/), anything).returns("stubbed value")

                  subject
                end
              end
            end

            it 'creates a new ExpirableAttachment' do
              assert_difference("ExpirableAttachment.count") { subject }
            end

            it 'completes with an attachment url' do
              assert_match(
                /https:\/\/minimum.zendesk-test.com\/expirable_attachments\/token?\/.+\/?name=simplified_email_threading_opt_in_affected_rules.zip/,
                subject[3][:results][:url]
              )
            end
          end

          describe 'with `dry_run` as false' do
            let(:dry_run) { false }

            describe "when an exception occurs in the transaction" do
              let(:expected_update_email_template_calls) { 0 }
              let(:expected_update_html_template_calls) { 0 }
              let(:template) { "#{account.cc_email_template} {{ticket.comments}}" }
              let(:html_template) { "{{delimiter}} {{header}} {{content}} {{footer}}" }

              before do
                account.cc_email_template = template
                account.save!
                AccountSetting.any_instance.stubs(:save!).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
              end

              it "does not update anything" do
                refute account.settings.simplified_email_threading
                assert_equal template, account.cc_email_template
                assert_equal html_template, account.html_mail_template

                assert_raises(ActiveRecord::RecordNotSaved) { subject }

                refute account.settings.reload.simplified_email_threading
                assert_equal template, account.reload.cc_email_template
                assert_equal html_template, account.html_mail_template
              end
            end

            it 'notifies admins about affected rules' do
              setup_base_i18n_assertions
              setup_admin_i18n_assertions
              JobsMailer.expects(:deliver_job_complete_to_multiple_recipients).with do |admins, _identifier, title, content|
                assert_equal 3, admins.size
                assert_equal translated_subject_english, title
                assert_equal translated_body_english, content
              end

              subject
            end

            it "updates the simplified_email_threading account setting" do
              refute account.settings.simplified_email_threading
              subject
              assert account.settings.reload.simplified_email_threading
            end

            it 'creates a new ExpirableAttachment' do
              assert_difference("ExpirableAttachment.count") { subject }
            end

            it 'completes with an attachment url' do
              assert_match(
                /https:\/\/minimum.zendesk-test.com\/expirable_attachments\/token?\/.+\/?name=simplified_email_threading_opt_in_affected_rules.zip/,
                subject[3][:results][:url]
              )
            end
          end
        end
      end

      describe 'with `onboarding_completed` as false' do
        let(:onboarding_completed) { false }

        it 'does not send an email notification to account admins' do
          JobsMailer.expects(:deliver_job_complete).never
          subject
        end
      end

      describe 'handled exceptions' do
        describe 'around persisting the file' do
          describe 'ActiveRecord errors' do
            let(:exception) { ActiveRecord::RecordInvalid }

            describe 'logging' do
              before do
                ExpirableAttachment.any_instance.expects(:save!).raises(exception.new(account.triggers.first))
              end

              it 'logs attachment info and re-raises' do
                SimplifiedEmailThreading::OptInJob.expects(:resque_error).with do |args|
                  assert args.include?("Unable to create attachment. account_id: #{account.id}, admin_id: #{admin.id}")
                  assert args.include?("error: #{exception}")
                end
                assert_raises(exception) { subject }
              end
            end
          end

          describe 'AttachmentFu::NoBackendsAvailableException' do
            let(:exception) { ::Technoweenie::AttachmentFu::NoBackendsAvailableException }

            describe 'logging' do
              before do
                ExpirableAttachment.any_instance.expects(:save!).raises(exception)
              end

              it 'logs attachment info and re-raises' do
                SimplifiedEmailThreading::OptInJob.expects(:resque_error).with do |args|
                  assert args.include?("Unable to create attachment. account_id: #{account.id}, admin_id: #{admin.id}")
                  assert args.include?("error: #{exception}")
                end
                assert_raises(exception) { subject }
              end
            end
          end
        end

        describe 'around updating rules' do
          let(:exception) { ActiveRecord::RecordInvalid }
          let(:error_message) do
            "Failed to update or diff rules. account_id: #{account.id}, admin_id: #{admin.id}, dry_run: true, error: #{exception}"
          end

          before do
            Zendesk::SimplifiedEmailThreading::OptIn.any_instance.expects(:update_opt_in_rules).
              raises(exception.new(account.triggers.first))
          end

          it 'logs info and re-raises' do
            SimplifiedEmailThreading::OptInJob.expects(:resque_error).with(error_message)
            assert_raises(exception) { subject }
          end
        end
      end
    end
  end

  describe 'Zip::Entry' do
    let(:output_stream) { 'sample output stream' }
    let(:job) { SimplifiedEmailThreading::OptInJob.new('uuid', account_id: account.id, admin_id: admin.id) }
    let(:current_time) { Zip::DOSTime.parse(Time.current.strftime(SimplifiedEmailThreading::OptInJob::DATETIME_FORMAT)) }
    let(:zip_entry_options) { job.send(:zip_entry_options, current_time) }
    let(:expected_update_email_template_calls) { 0 }
    let(:expected_update_html_template_calls) { 0 }
    let(:expected_clear_previously_saved_settings_calls) { 0 }

    describe '#instructions_entry' do
      let(:locale) { 'en' }

      it 'returns the expected zip entry' do
        instructions_entry = job.send(:instructions_entry, output_stream, locale, zip_entry_options)

        assert_equal output_stream, instructions_entry.zipfile
        assert_equal "instructions-#{locale}.txt", instructions_entry.name
        assert_equal current_time, instructions_entry.time
      end
    end
  end
end
