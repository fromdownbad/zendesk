require_relative "../../support/job_helper"

SingleCov.covered!

describe TicketBatchUpdateJob do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket1) { tickets(:minimum_1) }
  let(:ticket2) { tickets(:minimum_2) }
  let(:user) { account.owner }
  let(:job) do
    TicketBatchUpdateJob.new("79a03f83-8dd9-4af1-85bc-787a62037919",
      account_id: account.id,
      user_id: account.owner_id,
      tickets: [
        { id: ticket1.nice_id, subject: "updated subject" },
        { id: ticket2.nice_id, tags: "updated_tag" }
      ])
  end

  describe "#perform" do
    before { job.stubs(current_user: user) }

    describe "safe update" do
      describe "with valid updated_stamp param" do
        before do
          job.options[:tickets] = [
            { id: ticket1.nice_id, subject: "updated subject", safe_update: true, updated_stamp: ticket1.updated_at.to_s },
            { id: ticket2.nice_id, tags: "updated_tag", safe_update: true, updated_stamp: ticket2.updated_at.to_s }
          ]

          @success_results = [
            {"action" => "update", "status" => "Updated", "success" => true, "id" => ticket1.nice_id},
            {"action" => "update", "status" => "Updated", "success" => true, "id" => ticket2.nice_id}
          ]

          job.perform
        end

        it "updates tickets" do
          assert_equal @success_results, job.status['results']
          assert_equal "updated subject", ticket1.reload.subject
          assert_equal "updated_tag", ticket2.reload.current_tags
        end
      end

      describe "when exceeding threshold" do
        before do
          class FakeLimit < Prop::RateLimited
            def initialize; end

            def message;
              "ticket_updates threshold of 15 tries per 600s exceeded for key blah hash whatever stuff";
            end
          end
          Zendesk::Tickets::V2::Initializer.expects(:new).twice.raises(FakeLimit)
          job.options[:tickets] = [
            { id: ticket1.nice_id, subject: "updated subject" },
            { id: ticket2.nice_id, tags: "updated_tag" }
          ]
          job.perform
        end

        it "returns errors" do
          expected_result = {
            "index"   => 0,
            "error"   => "RateLimited",
            "id"      => 1,
            "details" => "ticket_updates threshold of 15 tries per 600s exceeded"
          }
          assert_includes job.status["results"], expected_result
        end
      end

      describe "without updated_stamp param" do
        describe "with only a safe_update param" do
          before do
            job.options[:tickets] = [
              { id: ticket1.nice_id, subject: "updated subject", safe_update: true },
              { id: ticket2.nice_id, tags: "updated_tag" }
            ]

            job.perform
          end

          it "return errors" do
            expected_result = {
              "index"   => 0,
              "error"   => "UnprocessableEntity",
              "id"      => 1,
              "details" => "Safe Update requires an updated_stamp"
            }

            assert_includes job.status["results"], expected_result
          end
        end
      end

      describe "with an outdated updated_stamp param" do
        before do
          job.options[:tickets] = [
            { id: ticket1.nice_id, subject: "updated subject", safe_update: true, updated_stamp: (ticket1.updated_at - 3).to_s },
            { id: ticket2.nice_id, tags: "updated_tag" }
          ]

          job.perform
        end

        it "return errors" do
          expected_result = {
            "index"   => 0,
            "error"   => "UpdateConflict",
            "id"      => 1,
            "details" => "Safe update timestamp prevented the update due to outdated ticket data. Please fetch the latest ticket data and try again."
          }

          assert_includes job.status["results"], expected_result
        end
      end

      describe "with no safe_update parameters" do
        it "returns errors with an outdated updated_at" do
          job.options[:tickets] = [
            { id: ticket1.nice_id, subject: "updated subject" },
          ]

          job.options['enqueued_at'] = 1.day.ago

          ticket1.update_column(:updated_at, 1.hour.ago)

          job.perform

          expected_result = {
            "index"   => 0,
            "error"   => "UpdateConflict",
            "id"      => 1,
            "details" => "Safe update timestamp prevented the update due to outdated ticket data. Please fetch the latest ticket data and try again."
          }

          assert_includes job.status["results"], expected_result
        end

        it "updates tickets with a newer enqueued_at" do
          job.options[:tickets] = [
            { id: ticket1.nice_id, subject: "updated subject" },
          ]

          job.options['enqueued_at'] = 1.hour.ago

          ticket1.update_column(:updated_at, 2.hours.ago)

          job.perform

          success_results = [
            {"action" => "update", "status" => "Updated", "success" => true, "id" => ticket1.nice_id}
          ]
          assert_equal success_results, job.status['results']
          assert_equal "updated subject", ticket1.reload.subject
        end
      end
    end

    describe "with the disable_triggers option" do
      it 'triggers are skipped for the ticket updates' do
        Zendesk::Rules::Trigger::Execution.expects(:execute).never

        job.options[:disable_triggers] = true

        job.perform
      end
    end

    describe "with suitable permission to edit tickets" do
      before do
        job.perform
      end

      it "updates tickets" do
        assert_equal "updated subject", ticket1.reload.subject
        assert_equal "updated_tag", ticket2.reload.current_tags
      end
    end

    describe "without suitable permission to edit tickets" do
      before do
        initializer = mock
        Zendesk::Tickets::V2::Initializer.expects(:new).twice.returns(initializer)
        initializer.stubs(:ticket).returns(ticket1, ticket2)
        [ticket1, ticket2].each do |ticket|
          user.stubs(:can?).with(:edit, ticket).returns(false)
          user.stubs(:can?).with(:only_create_ticket_comments, ticket).returns(false)
        end
        job.perform
      end

      it "return errors" do
        assert_includes job.status["results"], "index" => 0, "error" => "PermissionDenied", "id" => ticket1.nice_id
        assert_includes job.status["results"], "index" => 1, "error" => "PermissionDenied", "id" => ticket2.nice_id
      end
    end

    describe "with invalid ids" do
      before do
        job.options[:tickets] = [
          { id: 0, subject: "updated subject" },
          { id: -1, subject: "updated subject" }
        ]
        job.perform
      end

      it "returns errors" do
        assert_includes job.status["results"], "index" => 0, "error" => "TicketNotFound", "id" => 0
        assert_includes job.status["results"], "index" => 1, "error" => "TicketNotFound", "id" => -1
      end
    end

    describe 'when user can only add private comments to ticket' do
      before do
        create_light_agent
        job.stubs(:current_user).returns(@light_agent)
        job.current_user.stubs(:is_light_agent?).returns(true)
        # use a ticket that a light agent has access to
        @ticket = account.tickets.working.for_user(@light_agent).last
      end

      it "does not allow editing of other fields" do
        job.options[:tickets] = [
          { id: @ticket.nice_id, subject: 'change of subject', comment: { public: true, value: "hello" }}
        ]
        job.perform
        assert_includes job.status["results"], "index" => 0, "error" => "PermissionDenied", "id" => @ticket.nice_id
      end

      it "succeeds in adding a comment" do
        job.options[:tickets] = [
          { id: @ticket.nice_id, comment: { public: true, value: "hello" } }
        ]
        job.perform

        comment = @ticket.reload.comments.last
        refute comment.is_public?, 'only internal comments are allowed'
        assert_equal 'hello', comment.body
      end
    end
  end

  private

  def create_light_agent
    Account.any_instance.stubs(:has_permission_sets?).returns(true)

    @light_agent = User.create! do |user|
      user.account = accounts(:minimum)
      user.name = "Prince Light"
      user.email = "prince@example.org"
      user.roles = Role::AGENT.id
    end

    @light_agent.permission_set = PermissionSet.create_light_agent!(accounts(:minimum))
    @light_agent.permission_set.permissions.ticket_access = "all"
    @light_agent.save!
  end
end
