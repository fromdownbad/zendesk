require_relative "../../support/job_helper"
require 'action_mailer/enqueable'

SingleCov.covered!

describe 'MailRenderingJob' do
  extend ArturoTestHelper

  fixtures :accounts, :users, :subscriptions, :account_property_sets

  class LocaleMailer < ActionMailer::Base
    extend ActionMailer::Enqueable
    self.queue = MailRenderingJob::Deferred

    def simple_mail(_account, to)
      headers(
        'time-zone' => Time.zone.name,
        'locale'    => I18n.locale
      )
      mail(to: to, from: "me@me.com", body: '')
    end
  end

  before do
    Zendesk::StatsD::Client.any_instance.stubs(:increment)
  end

  describe "Mail delivery job" do
    def delivery_failure(error)
      ::Mail::Message.any_instance.expects(:deliver).raises(error)
    end

    let(:job) { MailRenderingJob }
    let(:account) { accounts(:minimum) }
    let(:params) { { method_id: 'password_reset', mailer_name: 'UsersMailer' } }
    let(:enqueued_id) { 'local/42983/1308788541.45834' }

    before do
      ActionMailer::Base.perform_deliveries = true
      Resque::Durable::GUID.stubs(:generate).returns(enqueued_id)
    end

    describe "deferred queue interface" do
      let(:queue) { MailRenderingJob::Deferred }

      it "enqueues with the current time zone and locale" do
        deferred = ActionMailer::Enqueable::Deferred.new(params.merge(arguments: [account.owner, 'reset-token']))
        Resque.expects(:enqueue).with(job, account.id, deferred.encoded, { 'time_zone' => Time.zone.name, 'locale' => I18n.locale }, anything, enqueued_id)
        queue.enqueue(deferred)
      end

      it "fails on bad timezone data" do
        Time.zone.stubs(:name).returns('NotValidTimezoneName')
        deferred = ActionMailer::Enqueable::Deferred.new(params.merge(arguments: [account.owner, 'reset-token']))
        assert_raise(ArgumentError) { queue.enqueue(deferred) }
      end

      it "ensures the deferred mailer is valid on enqueue" do
        deferred = ActionMailer::Enqueable::Deferred.new({})

        refute deferred.valid?
        assert_raise(ActionMailer::Enqueable::Deferred::Invalid) { queue.enqueue(deferred) }

        deferred.params.merge!(params)
        deferred.params[:arguments] = [account.owner, 'reset-token']
        assert deferred.valid?
        assert queue.enqueue(deferred)
      end

      describe "inferring the account from the deferred mailer's arguments" do
        it "raises an exception if no account is found" do
          deferred = ActionMailer::Enqueable::Deferred.new(params.merge(arguments: []))
          assert_raise(MailRenderingJob::AccountDetection::NotFound) { queue.enqueue(deferred) }
        end

        it "finds the account id in nested arguments" do
          deferred = ActionMailer::Enqueable::Deferred.new(params.merge(arguments: [[account.owner, 'reset-token']]))
          job.expects(:work)
          queue.enqueue(deferred)
        end

        it "finds the account id from account objects" do
          deferred = ActionMailer::Enqueable::Deferred.new(params.merge(arguments: [account, 'reset-token']))
          job.expects(:work)
          queue.enqueue(deferred)
        end

        it "finds the account id from dunning notifications" do
          dunning_notification = account.subscription.dunning_notifications.build
          dunning_notification.id = 1234
          deferred = ActionMailer::Enqueable::Deferred.new(params.merge(arguments: [dunning_notification, 'reset-token']))
          job.expects(:work)
          queue.enqueue(deferred)
        end
      end
    end

    describe "perform" do
      let(:deferred) { ActionMailer::Enqueable::Deferred.new(mailer_name: LocaleMailer.name, method_id: 'simple_mail', arguments: [account, "e@example.com"]) }

      it "increments a queued metric" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(:queued)

        job.perform(account.id, deferred.encoded, 'time_zone' => Time.zone.name, 'locale' => I18n.locale)
      end

      it "delivers the deferred mailing" do
        deferred = ActionMailer::Enqueable::Deferred.new(params.merge(arguments: [account.owner, 'reset-token']))

        assert_difference('ActionMailer::Base.deliveries.size') do
          job.perform(account.id, deferred.encoded, 'time_zone' => Time.zone.name, 'locale' => I18n.locale)
        end
      end

      it "uses the locale and time zone it was enqueued in" do
        mail = job.perform(account.id, deferred.encoded, 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar')

        assert_equal 'Asia/Ulaanbaatar', mail['time-zone'].to_s, mail.headers
        assert_equal 'not_default',      mail['locale'].to_s,    mail.headers
      end
    end

    let(:identity) { users(:minimum_end_user).identities.email.first }
    let(:destinations) { [identity.value] }
    let(:deferred) { ActionMailer::Enqueable::Deferred.new(mailer_name: LocaleMailer.name, method_id: 'simple_mail', arguments: [account, destinations]) }
    let(:audit_id) { "fake/enqueued/id" }
    let(:audit) { stub(enqueue_count: 1) }

    describe "on success" do
      before do
        QueueAudit.expects(:find_by_enqueued_id).returns(audit)
        identity.increment_undeliverable_count!
        assert_equal 1, identity.undeliverable_count
      end

      it "does not blow up when no associated identity exists" do
        destinations.clear
        job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
      end

      it "doesn't touch the undeliverable count" do
        job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
        assert_equal 1, identity.reload.undeliverable_count
      end

      it "captures :processed, :completed and :completed_duration statsd metric" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(:processed, tags: ['result:success'])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(:completed, anything)
        Zendesk::StatsD::Client.any_instance.expects(:timing).with(:completed_duration, anything, tags: ['result:success'])

        job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
      end

      describe_with_arturo_disabled :email_mrj_record_event_based_slo do
        it "only increments processing success metric" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(:completed, anything).never
          Zendesk::StatsD::Client.any_instance.expects(:timing).with(:completed_duration, anything, tags: ['result:success']).never
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(:processed, tags: ['result:success']).once

          job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
        end
      end
    end

    describe "on failure" do
      before do
        QueueAudit.expects(:find_by_enqueued_id).returns(audit)
        assert_equal 0, identity.undeliverable_count
      end

      describe "bad address errors" do
        before do
          delivery_failure Net::SMTPSyntaxError.new("501 5.1.3 Bad recipient address syntax")
        end

        it "does not raise/retry, does not touch the undeliverable_count" do
          job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          assert_equal 0, identity.reload.undeliverable_count
        end

        it "captures :processed, :completed and :completed_duration statsd metric" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(:completed, anything)
          Zendesk::StatsD::Client.any_instance.expects(:timing).with(:completed_duration, anything, anything).at_least(1)
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(:processed, tags: ['result:failed'])

          job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
        end

        describe_with_arturo_disabled :email_mrj_record_event_based_slo do
          it "only increments processing failed metric" do
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(:completed, anything).never
            Zendesk::StatsD::Client.any_instance.expects(:timing).with(:completed_duration, anything, tags: ['result:failed']).never
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(:processed, tags: ['result:failed']).once

            job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          end
        end
      end

      describe "server busy errors" do
        before do
          delivery_failure Net::SMTPServerBusy.new("450 4.1.2 <test@z3nmail.com>:\nRecipient address rejected:\nDomain not found")
        end

        after { assert_equal 0, identity.reload.undeliverable_count }

        describe_with_arturo_enabled :email_track_smtp_server_busy_errors do
          it "increments smtp server busy error and processing failed metrics" do
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(:smtp_server_busy_errors).once
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(:processed, tags: ['result:failed']).once

            assert_raise(Zendesk::OutgoingMail::SMTPServerBusy) do
              job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
            end
          end
        end

        describe_with_arturo_disabled :email_track_smtp_server_busy_errors do
          it "only increments processing failed metric" do
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(:smtp_server_busy_errors).never
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(:processed, tags: ['result:failed']).once

            assert_raise(Net::SMTPServerBusy) do
              job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
            end
          end
        end
      end

      describe "net open timeout error" do
        before do
          delivery_failure Net::OpenTimeout.new
        end

        it "increments net_opentimeout_errors" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(:net_opentimeout_errors).once

          assert_raise(Net::OpenTimeout) do
            job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          end
        end
      end

      describe "net read timeout error" do
        before do
          delivery_failure Net::ReadTimeout.new
        end

        it "increments net_opentimeout_errors" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(:net_readtimeout_errors).once

          assert_raise(Net::ReadTimeout) do
            job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          end
        end
      end

      describe "standard failures" do
        before do
          delivery_failure StandardError.new
        end

        after { assert_equal 0, identity.reload.undeliverable_count }

        it "does not notify the identity of delivery failure when attempting to deliver to a single address" do
          assert_raise(StandardError) do
            job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          end
        end

        it "does not notify when the audit is missing" do
          audit = nil # rubocop:disable Lint/UselessAssignment
          assert_raise(StandardError) do
            job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          end
        end

        it "does not notify on subsequent failures" do
          audit = stub(enqueue_count: 2) # rubocop:disable Lint/UselessAssignment
          assert_raise(StandardError) do
            job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          end
        end

        it "does not notify an identity on delivery failure when attempting to deliver to multiple addresses" do
          destinations << "hello@example.com"
          assert_raise(StandardError) do
            job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          end
        end

        it "does not blow up when no associated identity exists" do
          destinations.clear
          assert_raise(StandardError) do
            job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          end
        end

        it "increments :processed result:failed statsd metric" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(:processed, tags: ['result:failed'])
          assert_raise(StandardError) do
            job.perform(account.id, deferred.encoded, { 'locale' => 'not_default', 'time_zone' => 'Asia/Ulaanbaatar' }, audit_id)
          end
        end
      end
    end
  end
end
