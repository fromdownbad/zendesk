require_relative '../../../support/test_helper'
require_relative '../../../support/arturo_test_helper'

SingleCov.covered!
module TdeWorkspaces
  describe AssignWorkspaceJob do
    fixtures :tickets, :accounts, :users
    resque_inline true

    let(:user) { users(:minimum_agent) }
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_3) }
    let(:statsd_client_stub) { stub_for_statsd }

    describe "when account has contextual workspaces" do
      before do
        Account.any_instance.stubs(:has_contextual_workspaces?).returns(true)
        TdeWorkspaces::AssignWorkspaceJob.any_instance.stubs(:statsd_client).returns(statsd_client_stub)
      end

      describe 'assign workspace job' do
        it 'calls reassign_workspace when we pass a ticket' do
          Ticket.any_instance.expects(:reassign_workspace)
          statsd_client_stub.expects(:histogram).with('job_execution_time', kind_of(Float)).once

          TdeWorkspaces::AssignWorkspaceJob.perform(ticket_id: ticket.id, account_id: account.id)
        end

        it 'does not call reassign_workspace when called without a ticket' do
          Ticket.any_instance.expects(:reassign_workspace).never
          statsd_client_stub.expects(:increment).with('not_assigned.no_ticket').once
          TdeWorkspaces::AssignWorkspaceJob.perform(ticket_id: nil, account_id: account.id)
        end

        it 'calls ZendeskExceptions::Logger when there is an exception' do
          Ticket.any_instance.stubs(:present?).raises(StandardError)
          statsd_client_stub.expects(:increment).with('not_assigned.exception').once
          ZendeskExceptions::Logger.expects(:record)
          TdeWorkspaces::AssignWorkspaceJob.perform(ticket_id: ticket.id, account_id: account.id)
        end

        it 'does not raise an exception for inaccessible tickets' do
          ticket.update_column(:status_id, 5)
          statsd_client_stub.expects(:increment).with('not_assigned.no_ticket').once
          TdeWorkspaces::AssignWorkspaceJob.perform(ticket_id: ticket.id, account_id: account.id)
        end
      end
    end

    describe "when account has no contextual workspaces" do
      before do
        Account.any_instance.stubs(:has_contextual_workspaces?).returns(false)
      end

      describe 'assign workspace job' do
        it 'does not call reassign_workspace called without a ticket and without the arturo' do
          TdeWorkspaces::AssignWorkspaceJob.expects(:reassign_workspace).never
          TdeWorkspaces::AssignWorkspaceJob.perform(ticket_id: ticket.id, account_id: account.id)
        end
      end
    end

    describe "when account has contextual workspaces in js " do
      before do
        Account.any_instance.stubs(:has_contextual_workspaces?).returns(true)
        Account.any_instance.stubs(:has_contextual_workspaces_in_js?).returns(true)
      end

      describe 'assign workspace job' do
        it 'does not call reassign_workspace' do
          TdeWorkspaces::AssignWorkspaceJob.expects(:reassign_workspace).never
          TdeWorkspaces::AssignWorkspaceJob.perform(ticket_id: ticket.id, account_id: account.id)
        end
      end
    end
  end
end
