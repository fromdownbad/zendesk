require_relative "../../support/job_helper"

SingleCov.covered!

describe 'ScreenrUpdatingJob' do
  fixtures :accounts, :users

  describe "Given a valid account, provided with Screenr, and a user" do
    before do
      @account = accounts(:minimum)
      @user    = users(:minimum_agent)
      screenr_tenant = @account.build_screenr_tenant(
        domain: "test",
        host: "test.screenrtest.com",
        upgrade_url: "https://screenr-stage.chargify.test/h/90248/subscriptions/new?reference=support80",
        upgraded_account: false
      )
      screenr_tenant.save!
    end

    describe "when all goes fine" do
      before do
        @job = ScreenrUpdatingJob.new('uuid',           account_id: @account.id,
                                                        user_id: @user.id)

        stub_request(:post, "https://api.example.com/tenants/test/users/updateowner").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"Success":true}'
          )
      end

      it "sets personal_data_sent_to_screenr to true" do
        @job.perform
        assert @account.screenr_tenant.reload.personal_data_sent_to_screenr
      end

      it "returns the upgrade url" do
        @job.perform
        assert_equal "https://screenr-stage.chargify.test/h/90248/subscriptions/new?reference=support80",
          @job.status["results"]["upgrade_url"]
      end
    end

    describe "when exception is raised" do
      before do
        @job = ScreenrUpdatingJob.new('uuid',           account_id: @account.id,
                                                        user_id: @user.id)

        stub_request(:post, "https://api.example.com/tenants/test/users/updateowner").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":400,"Message":[{"Key":"FirstName","Value":"The FirstName field is required."}]}'
          )
      end

      it "does not set personal_data_sent_to_screenr to true" do
        @job.perform
        refute @account.screenr_tenant.reload.personal_data_sent_to_screenr
      end

      it "reports a validation failed" do
        @job.perform
        assert_equal "validation_failed",
          @job.status["results"]["api_status"]
      end

      it "reports error messessage" do
        @job.perform
        assert_equal "The FirstName field is required.",
          @job.status["results"]["message"]
      end

      it "reports error_on" do
        @job.perform
        assert_equal "first_name",
          @job.status["results"]["error_on"]
      end
    end
  end
end
