require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe CreditCard do
  fixtures :accounts, :subscriptions, :payments
  should_validate_presence_of :subscription

  describe "Newly initialized credit cards" do
    it "identifys themselves as braintree" do
      assert CreditCard.new.braintree?
      refute CreditCard.new.quickpay?
    end
  end

  describe '#expired?' do
    before do
      @credit_card = FactoryBot.build(:credit_card, month: 6, year: 2000)
    end

    it 'returns true a month past expiry' do
      Timecop.travel '2000-07-01'.to_date
      assert @credit_card.expired?
    end

    it 'returns true a year past expiry' do
      Timecop.travel '2001-06-01'.to_date
      assert @credit_card.expired?
    end

    it 'returns true a year and a month past expiry' do
      Timecop.travel '2001-07-01'.to_date
      assert @credit_card.expired?
    end

    it 'returns false when year is < now and month is > now' do
      Timecop.travel '1999-10-01'.to_date
      refute @credit_card.expired?
    end

    it 'returns false on expiry month' do
      Timecop.travel '2000-06-01'.to_date
      refute @credit_card.expired?
    end

    it 'returns false a month before expiry' do
      Timecop.travel '2000-05-01'.to_date
      refute @credit_card.expired?
    end

    it 'returns false a year before expiry' do
      Timecop.travel '1999-06-01'.to_date
      refute @credit_card.expired?
    end
  end

  describe "#registered?" do
    it "does not be registered on initialization" do
      assert_equal false, CreditCard.new.registered?
    end

    it "is registered when #payment_gateway_reference is present" do
      card = CreditCard.new(payment_gateway_reference: "whoosh")
      assert card.registered?
    end
  end

  describe "Card Expiry" do
    before do
      @credit_card = CreditCard.new
    end

    describe "when set via #expiry=" do
      it "splits input of form MMYY correctly" do
        @credit_card.expiry = "1214"
        assert_equal 2014, @credit_card.year
        assert_equal 12,   @credit_card.month
      end
    end

    describe "when set via #year= and #month=" do
      before do
        @credit_card.year = 2012
        @credit_card.month = 10
      end

      it "returns '2012' for :year" do
        assert_equal 2012, @credit_card.year
      end

      it "returns '10' for :month" do
        assert_equal 10, @credit_card.month
      end
    end

    describe "default values" do
      it "returns the 5 years in the future for :year" do
        assert_equal((Date.today.year + 5), @credit_card.year)
      end

      it "returns the current month for :month" do
        assert_equal Date.today.month, @credit_card.month
      end
    end
  end
end
