require_relative "../../support/test_helper"

SingleCov.covered!

describe Ipm::AlertDismissal do
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_agent)
    @alert = FactoryBot.create(:ipm_alert)
    @dismissal = Ipm::AlertDismissal.create!(alert: @alert, account: @account, user: @user)
  end

  should_validate_presence_of :account, :user, :alert

  describe ".cleanup" do
    before do
      alert_to_be_deleted = FactoryBot.create(:ipm_alert)

      Ipm::AlertDismissal.create!(alert: alert_to_be_deleted, account: @account, user: @user)

      alert_to_be_deleted.destroy

      Ipm::AlertDismissal.cleanup
    end

    it "deletes the dismissals for which alerts do not exist" do
      assert_equal [@dismissal], Ipm::AlertDismissal.all
    end
  end
end
