require_relative "../../support/test_helper"

SingleCov.covered!

describe Ipm::FeatureNotificationDismissal do
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_agent)
    @feature_notification = FactoryBot.create(:ipm_feature_notification)
    @dismissal = Ipm::FeatureNotificationDismissal.create!(feature_notification: @feature_notification, account: @account, user: @user)
  end

  should_validate_presence_of :account, :user, :feature_notification

  describe ".cleanup" do
    before do
      feature_notification_to_be_deleted = FactoryBot.create(:ipm_feature_notification)

      Ipm::FeatureNotificationDismissal.create!(feature_notification: feature_notification_to_be_deleted, account: @account, user: @user)

      feature_notification_to_be_deleted.destroy

      Ipm::FeatureNotificationDismissal.cleanup
    end

    it "deletes the dismissals for which feature_notifications do not exist" do
      assert_equal [@dismissal], Ipm::FeatureNotificationDismissal.all
    end
  end
end
