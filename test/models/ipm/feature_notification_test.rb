require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Ipm::FeatureNotification do
  fixtures :users
  subject { FactoryBot.create(:ipm_feature_notification) }

  before do
    @user = users(:minimum_admin)
    @account = @user.account
    @account.stubs(:shard_id).returns(3)
    @account.stubs(:id).returns(12)
    @account.stubs(:subscription).returns(Object.new.tap do |s|
      s.stubs(:plan_type).returns(1)
      s.stubs(:account_type).returns("trial")
    end)
  end

  describe "initialize" do
    it "defaults to enabled" do
      assert Ipm::FeatureNotification.new.enabled?
    end
  end

  describe ".for_user" do
    describe "when there are no feature notifications" do
      it "returns empty array" do
        assert_equal [], Ipm::FeatureNotification.for_user(@user)
      end
    end

    describe "when there are feature notifications" do
      it "only return feature notifications that are to be shown for user" do
        feature_notification = FactoryBot.create(:ipm_feature_notification)

        # misdirection
        FactoryBot.create(:ipm_feature_notification, pods: [123])
        FactoryBot.create(:ipm_feature_notification, enabled: false)
        dismissed = FactoryBot.create(:ipm_feature_notification)
        Ipm::FeatureNotificationDismissal.create user: @user, account: @account, feature_notification: dismissed

        assert_equal [feature_notification], Ipm::FeatureNotification.for_user(@user)
      end
    end
  end

  describe "#show_for_user?" do
    describe "a disabled FeatureNotification targeting all" do
      before do
        subject.enabled = false
      end

      it 'returns false' do
        refute subject.show_for_user?(@user)
      end
    end

    describe "an enabled feature_notification" do
      describe "targeting all" do
        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "targeting the account's shard" do
        before do
          subject.shards = [1, 2, 3]
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting the account's shard" do
        before do
          subject.shards = [1, 2, 4]
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting pod" do
        before do
          subject.pods = [1, 2, 3]
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(2)
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting pod" do
        before do
          subject.pods = [1, 3]
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(2)
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting the user's plan" do
        before do
          subject.plans = [1]
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting the user's plan" do
        before do
          subject.plans = [2]
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting the user's role" do
        before do
          subject.roles = ['Admin']
        end

        it 'returns true' do
          assert subject.show_for_user?(users(:minimum_admin_not_owner))
        end
      end

      describe "targeting the user's language" do
        it 'returns true when no languages were selected for the notification' do
          assert subject.show_for_user?(@user)
        end

        it 'returns true when the user language is one of the selected languages' do
          subject.languages = [1, 2]
          @user.stubs(:locale_id).returns(1)
          assert subject.show_for_user?(@user)
        end

        it 'returns false when the user language is not one of the selected languages' do
          subject.languages = [1, 2]
          @user.stubs(:translation_locale).returns(stub(id: 3))
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting the user's account_id" do
        it 'returns true when no account_ids were selected for the notification' do
          assert subject.show_for_user?(@user)
        end

        it 'returns true when the account_id is one of the selected ids' do
          subject.account_ids = [1, 2]
          @user.stubs(:account_id).returns(1)
          assert subject.show_for_user?(@user)
        end

        it 'returns false when the user account_id is not one of the selected ids' do
          subject.account_ids = [1, 2]
          @user.stubs(:account_id).returns(3)
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting owners" do
        before do
          subject.roles = ['Owner']
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting the user's role" do
        before do
          subject.roles = ['End-user']
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting the user's account type" do
        before do
          subject.account_types = ['trial']
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting the user's account type" do
        before do
          subject.roles = ['Legacy Agent']
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "dismissed by user" do
        before do
          Ipm::FeatureNotificationDismissal.create user: @user, account: @account, feature_notification: subject
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end
    end
  end
end
