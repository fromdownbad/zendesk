require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Ipm::Alert do
  fixtures :users

  let(:subject) do
    Ipm::Alert.create text: "New alert", link_url: "http://www.zendesk.com/alert"
  end

  before do
    @user = users(:minimum_admin)
    @account = @user.account
    @account.stubs(:shard_id).returns(3)
    @account.stubs(:id).returns(12)
    @account.stubs(:subscription).returns(Object.new.tap do |s|
      s.stubs(:plan_type).returns(1)
      s.stubs(:account_type).returns("trial")
    end)
  end

  describe "initialize" do
    it "defaults to enabled" do
      assert Ipm::Alert.create(text: 'foo').enabled?
    end
  end

  describe ".for_user" do
    describe "when there are no alerts" do
      it "returns empty array" do
        assert_equal [], Ipm::Alert.for_user(@user)
      end
    end

    describe "when there are alerts" do
      before do
        @a, @b, @c, @d = [['classic'], ['lotus'], ['classic', 'lotus'], nil].map do |interfaces|
          FactoryBot.create(:ipm_alert, text: 'foo', interfaces: interfaces)
        end
      end

      it "allows segmenting by interface" do
        assert_equal [@a, @b, @c, @d], Ipm::Alert.for_user(@user)
        assert_equal [@b, @c, @d], Ipm::Alert.for_user(@user, interface: 'lotus')
        assert_equal [@a, @c, @d], Ipm::Alert.for_user(@user, interface: 'classic')
      end

      it "only does a few queries" do
        assert_sql_queries(3) do
          Ipm::Alert.for_user(@user)
        end
      end

      describe "when there are custom alerts" do
        before do
          @custom = FactoryBot.create(:ipm_alert, text: 'foo', custom_factors: "foo, bar")
        end

        describe "that are dismissed" do
          before do
            Ipm::AlertDismissal.create!(user: @user, alert: @custom, account: @account)
          end

          it "does not be retrieved" do
            assert_equal [@d], Ipm::Alert.for_user(@user, custom: ["foo"], interface: "not used")
          end
        end

        describe "that are not dismissed" do
          it "is retrieved" do
            assert_equal [@d, @custom], Ipm::Alert.for_user(@user, custom: ["foo"], interface: "not used")
          end

          it "does not be retrieved if given factors are empty" do
            assert_equal [@d], Ipm::Alert.for_user(@user, custom: [], interface: "not used")
          end
        end
      end
    end

    describe "when there are alerts" do
      it "only return alerts that are to be shown for user" do
        alert = FactoryBot.create(:ipm_alert)

        # misdirection
        FactoryBot.create(:ipm_alert, pods: [123])
        FactoryBot.create(:ipm_alert, enabled: false)
        dismissed = FactoryBot.create(:ipm_alert)
        Ipm::AlertDismissal.create user: @user, account: @account, alert: dismissed

        assert_equal [alert], Ipm::Alert.for_user(@user)
      end
    end
  end

  describe "#show_for_user?" do
    describe "a disabled Alert targeting all" do
      before do
        subject.enabled = false
      end

      it 'returns false' do
        refute subject.show_for_user?(@user)
      end
    end

    describe "an enabled alert" do
      describe "targeting all" do
        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "with a custom_factor" do
        before do
          subject.custom_factors = "foo"
        end

        it "returns false without the factor" do
          refute subject.show_for_user?(@user)
        end

        it "returns true with the factor" do
          assert subject.show_for_user?(@user, custom: 'foo')
        end

        it "returns true when ignoring factors" do
          assert subject.show_for_user?(@user, ignore_custom: true)
        end
      end

      describe "targeting the account's shard" do
        before do
          subject.shards = [1, 2, 3]
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting the account's shard" do
        before do
          subject.shards = [1, 2, 4]
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting pod" do
        before do
          subject.pods = [1, 2, 3]
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(2)
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting pod" do
        before do
          subject.pods = [1, 3]
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(2)
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting the user's plan" do
        before do
          subject.plans = [1]
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting the user's plan" do
        before do
          subject.plans = [2]
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting the user's role" do
        before do
          subject.roles = ['Admin']
        end

        it 'returns true' do
          assert subject.show_for_user?(users(:minimum_admin_not_owner))
        end
      end

      describe "targeting the user's language" do
        it 'returns true when no languages were selected for the notification' do
          assert subject.show_for_user?(@user)
        end

        it 'returns true when the user language is one of the selected languages' do
          subject.languages = [1, 2]
          @user.stubs(:locale_id).returns(1)
          assert subject.show_for_user?(@user)
        end

        it 'returns false when the user language is not one of the selected languages' do
          subject.languages = [1, 2]
          @user.stubs(:translation_locale).returns(stub(id: 3))
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting the user's account_id" do
        it 'returns true when no account_ids were selected for the notification' do
          assert subject.show_for_user?(@user)
        end

        it 'returns true when the account_id is one of the selected ids' do
          subject.account_ids = [1, 2]
          @user.stubs(:account_id).returns(1)
          assert subject.show_for_user?(@user)
        end

        it 'returns false when the user account_id is not one of the selected ids' do
          subject.account_ids = [1, 2]
          @user.stubs(:account_id).returns(3)
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting owners" do
        before do
          subject.roles = ['Owner']
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting the user's role" do
        before do
          subject.roles = ['End-user']
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "targeting the user's account type" do
        before do
          subject.account_types = ['trial']
        end

        it 'returns true' do
          assert subject.show_for_user?(@user)
        end
      end

      describe "not targeting the user's account type" do
        before do
          subject.roles = ['Legacy Agent']
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end

      describe "dismissed by user" do
        before do
          Ipm::AlertDismissal.create user: @user, account: @account, alert: subject
        end

        it 'returns false' do
          refute subject.show_for_user?(@user)
        end
      end
    end
  end
end
