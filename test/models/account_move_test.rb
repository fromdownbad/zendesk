require_relative "../support/test_helper"

SingleCov.covered!

describe AccountMove do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let!(:account_move) do
    account.account_moves.create! do |move|
      move.src_shard_id = 2
      move.target_shard_id = 3
      move.state = "done"
    end
  end

  it 'instantiates' do
    assert account_move.id
  end

  it 'responds to a query' do
    assert AccountMove.find(account_move.id)
  end

  it 'has an account' do
    assert account_move.account
  end
end
