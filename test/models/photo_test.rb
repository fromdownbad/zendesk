require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe Photo do
  fixtures :all

  has_imagemagick = system("which identify >/dev/null") || (ENV["CI"] && raise("identify issue"))

  conditional_describe Photo, has_imagemagick do
    should_validate_presence_of :account

    let(:photo) { photos(:minimum_photo) }

    it "is valid" do
      photo.expects(:valid_dimensions?).returns(true) # photo fixture's actual content is empty...
      assert photo.valid?
    end

    it "validates extension" do
      photo.filename = "xxx.yyy"
      refute photo.valid?
      assert_equal ["extension yyy is not allowed. Please use jpg, jpeg, gif, or png for your photo."], photo.errors[:extension]
    end

    it "does not blowup on system account" do
      photo.account = Account.find(-1)
      photo.expects(:valid_dimensions?).returns(true) # same as above
      assert photo.valid?
    end

    describe "#valid_image_file" do
      describe_with_arturo_disabled :email_increase_attachment_size_limit do
        it "validates size" do
          photo.size = 1_000_000_000_000
          refute photo.valid?
          assert_equal ["Attachment is too large. Limit 7 megabytes."], photo.errors[:base]
        end
      end

      describe_with_arturo_enabled :email_increase_attachment_size_limit do
        it "allows images within the file size limit" do
          photo.expects(:valid_dimensions?).returns(true) # photo fixture's actual content is empty
          photo.size = 15.megabytes
          assert photo.valid?
          assert_empty photo.errors
        end

        it "does not allow images over the photo attachment_fu max size limit" do
          photo.expects(:valid_dimensions?).returns(true) # photo fixture's actual content is empty
          photo.size = 21.megabytes
          refute photo.valid?
          assert_equal ["Size is not included in the list (1..20971520)"], photo.errors[:size]
          assert_empty photo.errors[:base]
        end

        it "does not allow images over the account attachment size limit" do
          photo.size = 51.megabytes
          refute photo.valid?
          assert_equal ["Attachment is too large. Limit 50 megabytes."], photo.errors[:base]
          assert_equal ["Size is not included in the list (1..20971520)"], photo.errors[:size]
        end
      end

      it "does not bother with misidentified files" do
        photo.uploaded_data = Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/notepad.exe.png", content_type: "image/jpeg")
        refute photo.valid?
        assert_equal ["Image dimensions are invalid. Must be 30..64000000 pixels."], photo.errors[:base]
      end

      it "validates large dimensions" do
        photo.uploaded_data = Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/lottapixel.jpg", content_type: "image/jpeg")
        refute photo.valid?
        assert_equal ["Image dimensions are invalid. Must be 30..64000000 pixels."], photo.errors[:base]
      end

      it "validates small dimensions (but lots of frames)" do
        skip 'this is super slow' if ENV['TRAVIS']
        photo.uploaded_data = Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/lottaframes.gif", content_type: "image/gif")
        refute photo.valid?
        assert_equal ["Image dimensions are invalid. Must be 30..64000000 pixels."], photo.errors[:base]
      end
    end

    describe "An oversized photo" do
      before do
        @photo  = FactoryBot.create(:photo, filename: "large_1.png")
        @result = MiniMagick::Image.new(@photo.full_filename)
        @thumb  = MiniMagick::Image.new(@photo.full_filename(:thumb))
      end

      it "resizes to a max-width of 80" do
        assert(@result[:width] <= 80)
      end

      it "creates thumbnails with a max-height of 32" do
        assert(@thumb)
        assert(@thumb[:height] <= 32)
      end
    end

    describe "#destroy" do
      it "does not need to check imageness or validity" do
        photo = FactoryBot.create(:photo, filename: "large_1.png")
        assert thumbnail = photo.thumbnails.first
        photo.expects(:image?).never
        photo.expects(:valid_dimensions?).never
        assert photo.destroy
        assert_raises(ActiveRecord::RecordNotFound) { thumbnail.reload }
      end
    end

    describe "#cleanup" do
      it "destroys all orphaned photos" do
        Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.stubs(:destroy_file)

        user = users(:minimum_end_user)

        assert_difference("Photo.count(:all)", 2) do
          user.set_photo(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png"))
          user.save!
        end

        Photo.without_arsi.order(id: :desc).limit(2).update_all(user_id: nil, parent_id: nil)

        assert_difference("Photo.count(:all)", -2) do
          Photo.cleanup
        end
      end
    end

    describe "with a bogus photo" do
      it "rescues the exception and add an error" do
        @user = users(:minimum_end_user)
        @user.set_photo(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/twitter_favorites_1.1.json", content_type: "image/png"))
        refute @user.save
        assert @user.errors.full_messages.grep(/photo/i)
      end
    end

    describe "with uploaded photo data coming from a non existent file" do
      it "raises an exception" do
        @user = users(:minimum_end_user)
        assert_raise ActiveRecord::RecordInvalid do
          @user.set_photo(uploaded_data: "null")
        end
      end
    end

    describe "a new photo with no available current_data" do
      before do
        @photo = Photo.new(account: accounts(:minimum), content_type: "image/png", filename: "still_copying.png")
        @photo.size = 1234
      end
      it "does not be valid" do
        refute @photo.valid?
        assert_equal ["Image dimensions are invalid. Must be 30..64000000 pixels."], @photo.errors[:base]
      end
    end
  end
end
