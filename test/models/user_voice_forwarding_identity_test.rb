require_relative "../support/test_helper"

SingleCov.covered! uncovered: 5

describe UserVoiceForwardingIdentity do
  fixtures :accounts, :account_property_sets, :subscriptions, :role_settings

  it "Provides a identity_type" do
    assert_equal(:agent_forwarding, UserVoiceForwardingIdentity.identity_type)
  end

  describe "create" do
    before do
      @uvni = UserVoiceForwardingIdentity.new(value: '14155551234')
      @user = FactoryBot.create(:end_user, account: accounts(:minimum))
      User.any_instance.stubs(:full_voice_number).returns('+14155551234')
      @uvni.user = @user
    end

    describe 'when validating' do
      # Removed 999 because Phony < 1.9.0 returns an odd string when normalizing:
      # Phony.normalize("999")
      # => "999[#<Phony::NationalCode:0x007f996d7aec08 @national_splitter=#<Phony::NationalSplitters::None:0x007f996af31960>, @local_splitter=#<Phony::NationalSplitters::Default:0x007f996d714a18>, @normalize=true>]"
      # This fails the value length validation.
      # https://github.com/floere/phony/issues/51

      %w[911 112].each do |number|
        it "shows errors for emergency number: #{number}" do
          @uvni.value = number

          refute(@uvni.valid?)
          assert_includes @uvni.errors[:value], 'You cannot forward to emergency numbers using this service.'
        end

        it "shorts circuit on emergency number #{number} detection" do
          @uvni.value = number

          refute(@uvni.valid?)
          assert_equal ['You cannot forward to emergency numbers using this service.'], @uvni.errors[:value]
        end
      end

      # This behaviour was previously caused by the bug in Phony < 1.9.0. e.g:
      #   > development >> Phony.normalize("+44")
      #   => development => "44[#<Phony::NationalCode:0x007fbac4c51b98 ..."
      # The behaviour should now be provided by Voice::Core::NumberSupport::E164Number.plausible?
      # This test is to ensure that no regressions occur.
      %w[+1 +353 +44].each do |code|
        it "does not allow numbers with only country code '#{code}'" do
          @uvni.value = code

          refute(@uvni.valid?)
        end

        it "does not allow numbers with only country code and extension '#{code}x1234'" do
          @uvni.value = "#{code}x1234"

          refute(@uvni.valid?)
        end
      end

      describe "agent forwarding country restrictions" do
        before do
          @account = @account = accounts(:minimum)
        end

        it "only allow numbers from zendesk voice countries if account setting is disabled" do
          AccountSetting.any_instance.stubs(:voice_make_calls_outside_zendesk_countries).returns(false)
          @uvni.value = "+37495872349"
          refute(@uvni.valid?)
          assert_includes @uvni.errors[:base], I18n.t('activerecord.errors.voice.agent_forwarding.please_contact_support')
          @uvni.value = "+14129996123"
          assert(@uvni.valid?)
        end

        it "allows numbers from any country if account setting is enabled" do
          @account.settings.voice_make_calls_outside_zendesk_countries = true
          @account.save!
          @uvni.value = "+37495872349"
          assert(@uvni.valid?)
        end
      end

      it "validates around an extension" do
        assert @uvni.valid?
        @uvni.value << 'x123'
        @uvni.valid?

        assert_equal '+14155551234x123', @uvni.value
      end

      it "only allow digits, #, w, or * in extension" do
        @uvni.value << 'xabcdefg1#*w$@%&'
        @uvni.valid?

        assert_equal '+14155551234x1#*w', @uvni.value
      end

      it "thens be verified" do
        @uvni.save!
        assert(@uvni.is_verified?)
      end

      describe "and identity exists with same value" do
        before do
          @uvni.save!
        end

        it "has errors for new identity" do
          i = UserVoiceForwardingIdentity.new(value: '14155551234')
          i.user = @user
          refute(i.valid?, 'UserVoiceForwardingIdentity should not be valid')
          assert_match(/is already being used by another user/, i.errors.full_messages.join(' '))
        end
      end
    end

    it "marks unavailable for voice on destroy" do
      @user.voice_number = "+16465551234"
      @uvni.save!; @user.reload
      @uvni.send(:api_client).connection.expects(:post)
      @uvni.destroy
    end

    it "appends unique string to end of value" do
      @uvni.save!
      assert_equal('+14155551234||UNIQ||', @uvni.class.connection.select_value("select value from user_identities where id = #{@uvni.id}"))
      assert_equal('+14155551234', @uvni.value)
      @uvni.reload
      assert_equal('+14155551234', @uvni.value)
      assert_equal('+14155551234', UserVoiceForwardingIdentity.find(@uvni.id).value)
    end
  end
end
