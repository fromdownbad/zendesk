require_relative "../support/test_helper"

SingleCov.covered!

describe DataDeletionAuditJob do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @account.cancel!
    @account.subscription.credit_card = CreditCard.new
    @account.subscription.canceled_on = 130.days.ago
    @account.subscription.save!
    @account.soft_delete!
    @account.deleted_at = 40.days.ago
    @account.save!

    # Record created by soft_delete!
    @audit = @account.data_deletion_audits.first
    @audit.created_at = 40.days.ago
    @audit.save!
    @job_audit = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
  end

  it 'should mass assign attributes' do
    assert_equal @account.id, @job_audit.account_id
    assert_equal @job_audit.job, Zendesk::Maintenance::Jobs::RemoveShardDataJob.name
  end

  it 'should know the reason for the deletion' do
    assert_equal 'canceled', @job_audit.reason
  end

  describe '#fail!' do
    it 'serializes the exception to JSON' do
      exception = -> { raise 'my message' }.must_raise RuntimeError
      @job_audit.fail!(exception)
      @job_audit.reload
      @job_audit.failed?.must_equal true
      @job_audit.failed_reason['klass'].must_equal 'RuntimeError'
      @job_audit.failed_reason['message'].must_equal 'my message'
    end
  end

  describe '#enqueue!' do
    it 'sets its status to enqueued' do
      Zendesk::Maintenance::Jobs::RemoveShardDataJob.stubs(:enqueue_in)
      job = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob)
      job.enqueue!
      job.status.must_equal 'enqueued'
    end

    it 'enqueues with the heartbeat seed' do
      Timecop.freeze do
        heartbeat_at = Time.now + 85.minutes
        Zendesk::Maintenance::Jobs::RemoveShardDataJob.expects(:enqueue_in).with(1.hour, @job_audit.id, "last_heartbeat" => heartbeat_at.to_i).once
        @job_audit.enqueue!(1.hour)
        @job_audit.heartbeat_at.must_equal heartbeat_at
      end
    end
  end

  describe '#optimistic_heartbeat!' do
    it 'should heartbeat successfully if the heartbeat is unchanged' do
      last_heartbeat = Time.now - 5
      @job_audit.update_attribute(:heartbeat_at, last_heartbeat)
      new_heartbeat = @job_audit.optimistic_heartbeat!(last_heartbeat)
      @job_audit.reload
      @job_audit.heartbeat_at.to_i.must_equal new_heartbeat.to_i
    end

    it 'should raise JobCollision if the heartbeat has changed' do
      last_heartbeat = Time.now - 5
      @job_audit.update_attribute(:heartbeat_at, last_heartbeat)
      proc { @job_audit.optimistic_heartbeat!(last_heartbeat + 1) }.must_raise DataDeletionAuditJob::JobCollision
    end
  end

  describe '#timed_out?' do
    it 'returns true if after exceeding the timeout' do
      @job_audit.heartbeat_at = 6.minutes.ago
      @job_audit.timed_out?.must_equal true
    end

    it 'return false if no timeout present' do
      @job_audit.heartbeat_at = nil
      @job_audit.timed_out?.must_equal false
    end

    it 'return false if when under the interval' do
      @job_audit.heartbeat_at = 4.minutes.ago
      @job_audit.timed_out?.must_equal false
    end
  end

  describe '.delete_failed_audit_jobs' do
    subject { DataDeletionAuditJob.delete_failed_audit_jobs(audit_id, job) }

    let(:audit_id) { @audit.id }
    let(:job) { 'Zendesk::Maintenance::Jobs::RemoveShardDataJob' }
    let(:job_audits) { @audit.job_audits.where(job: job) }

    describe 'when no failed audits' do
      it 'should not delete anything' do
        subject
        job_audits.count.must_equal 1
      end
    end

    describe 'when 1 failed audit' do
      it 'should not delete the failed audit' do
        Timecop.freeze(Time.now - 1.hour) do
          @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap do |job_audit|
            job_audit.status = 'failed'
            job_audit.save!
          end
        end

        subject
        job_audits.count.must_equal 2
      end
    end

    describe 'when more than 1 failed audit' do
      it 'should delete the first failed job' do
        Timecop.freeze(Time.now - 2.hours) do
          @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap do |job_audit|
            job_audit.status = 'failed'
            job_audit.save!
          end
        end

        Timecop.freeze(Time.now - 1.hour) do
          @second_fail_job_audit = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap do |job_audit|
            job_audit.status = 'failed'
            job_audit.save!
          end
        end

        subject
        job_audits.count.must_equal 2
        assert_equal job_audits.first, @job_audit
        assert_equal job_audits.second, @second_fail_job_audit
      end
    end

    describe 'when all failed should be deleted' do
      it 'should delete all failed jobs' do
        Timecop.freeze(Time.now - 1.hour) do
          @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap do |job_audit|
            job_audit.status = 'failed'
            job_audit.save!
          end
        end

        # `0` means delete all failed jobs
        DataDeletionAuditJob.delete_failed_audit_jobs(audit_id, job, 0)

        job_audits.count.must_equal 1
        assert_equal job_audits.first, @job_audit
      end
    end
  end
end
