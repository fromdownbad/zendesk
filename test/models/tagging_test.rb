require_relative "../support/test_helper"

SingleCov.covered!

describe Tagging do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }

  before do
    @tagging_child            = Tagging.new
    @tagging_child.account_id = 1
    Tagging.any_instance.stubs(:taggable).returns(@tagging_child)
  end

  describe "Spaceship operator" do
    before do
      @tagging_1 = Tagging.new(tag_name: "Test Name")
      @tagging_2 = Tagging.new(tag_name: "Test Name")
    end

    it "compares the tag's names" do
      assert_equal 0, @tagging_1 <=> @tagging_2
    end
  end

  it "sets the account_id before running validations" do
    @tagging_parent = Tagging.new
    assert_nil @tagging_parent.account_id
    @tagging_parent.valid?
    assert_equal @tagging_child.account_id, @tagging_parent.account_id
  end

  it "adds an error if the tag's name is blank" do
    @tagging = Tagging.new
    @tagging.valid?
    assert_equal [I18n.t('txt.admin.models.rules.rule.tag_cannot_be_blank')], @tagging.errors["base"]
  end

  it "adds an error if the tag's name has a space" do
    @tagging = Tagging.new(tag_name: "test tag")
    @tagging.valid?
    assert_equal [I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_special_characters')], @tagging.errors["base"]
  end

  describe_with_arturo_disabled :validate_pipes_for_tags do
    it "adds an error if edited tag's name includes a pipe character" do
      @tagging = Tagging.new(tag_name: 'wombats', account: account)
      @tagging.save!(validate: false)
      @tagging.tag_name = 'womb|ats'
      assert @tagging.valid?
    end
  end

  describe_with_arturo_enabled :validate_pipes_for_tags do
    it "adds an error if edited tag's name includes a pipe character" do
      @tagging = Tagging.new(tag_name: 'wombats', account: account)
      @tagging.save!(validate: false)
      @tagging.tag_name = 'womb|ats'
      @tagging.valid?
      assert_equal [I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_pipe_character', url: I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_pipe_documentation'))], @tagging.errors["base"]
    end

    it "doesnt add an error if edited tag's name already includes a pipe character" do
      @tagging = Tagging.new(tag_name: 'wom|bats', account: account)
      @tagging.save!(validate: false)
      @tagging.tag_name = 'wom|bats'
      @tagging.valid?
      assert_equal [], @tagging.errors["base"]
    end

    it "add an error if edited tag's name edits tag name with a pipe character" do
      @tagging = Tagging.new(tag_name: 'wombats', account: account)
      @tagging.save!(validate: false)
      @tagging.tag_name = 'w|ombats'
      @tagging.valid?
      assert_equal [I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_pipe_character', url: I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_pipe_documentation'))], @tagging.errors["base"]
    end

    it "adds an error if the tag's name includes a pipe character" do
      @tagging = Tagging.new(tag_name: "|")
      @tagging.valid?
      assert_equal [I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_special_characters')], @tagging.errors["base"]
    end
  end

  it "adds an error if the tag's name includes a special character" do
    @tagging = Tagging.new(tag_name: "$")
    @tagging.valid?
    assert_equal [I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_special_characters')], @tagging.errors["base"]
  end

  it "adds an error if the tag's name includes a non-printable character" do
    @tagging = Tagging.new(tag_name: "&#7;")
    @tagging.valid?
    assert_equal [I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_special_characters')], @tagging.errors["base"]
  end

  it "adds an error if the tag's name is too long" do
    @tagging = Tagging.new(tag_name: SecureRandom.hex(Tagging::TAG_NAME_MAX_LENGTH + 1).to_s)
    @tagging.valid?
    assert_equal [I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_value_too_long', length: Tagging::TAG_NAME_MAX_LENGTH)], @tagging.errors["base"]
  end

  it "adds the new tag name to TagScore table" do
    refute TagScore.exists?(account_id: @tagging_child.account_id, tag_name: "wombats")
    Tagging.create!(tag_name: 'wombats', account_id: @tagging_child.account_id)
    assert TagScore.exists?(account_id: @tagging_child.account_id, tag_name: "wombats")
  end

  it "rescues duplicate attempts to add the same tag" do
    stub = stub('stub')
    TagScore.expects(:where).returns(stub)
    stub.expects(:find_or_create_by).raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, "Duplicate entry")
    Rails.logger.expects(:info).with("Duplicate tagging attempt #{@tagging_child.account_id}, tag_name wombats")
    Zendesk::StatsD::Client.any_instance.expects(:increment)
    Tagging.create!(tag_name: 'wombats', account_id: @tagging_child.account_id)
  end

  it "rescues race condition to add the same tag" do
    stub = stub('stub')
    TagScore.expects(:where).returns(stub)
    stub.expects(:find_or_create_by).raises(ActiveRecord::RecordNotUnique, "Duplicate entry")
    Rails.logger.expects(:info).with("Duplicate tagging attempt #{@tagging_child.account_id}, tag_name wombats")
    Zendesk::StatsD::Client.any_instance.expects(:increment)
    Tagging.create!(tag_name: 'wombats', account_id: @tagging_child.account_id)
  end
end
