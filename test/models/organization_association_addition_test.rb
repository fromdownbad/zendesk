require_relative "../support/test_helper"

SingleCov.covered!

describe OrganizationAssociationAddition do
  fixtures :users, :accounts, :organizations, :organization_memberships, :tickets

  let(:account) { accounts(:minimum) }
  let(:organization1) { organizations(:minimum_organization1) }
  let(:organization2) { organizations(:minimum_organization2) }
  let(:organization3) { organizations(:minimum_organization3) }
  let(:user_without_association) { users(:minimum_search_user) }

  describe "a user without an organization" do
    it "sets the organization correctly on the user and the user's unclosed tickets" do
      user = users(:minimum_author)
      assert_nil user.organization
      assert_nil user.tickets.not_closed.first.organization_id
      Timecop.freeze
      updated_at = user.updated_at
      Timecop.travel(updated_at + 10.seconds) do
        OrganizationAssociationAddition.new(
          account.id, organization2.id, [user.id]
        ).call

        assert_equal organization2, user.reload.organization
        assert_equal organization2, user.tickets.not_closed.first.organization
        assert_not_equal updated_at, user.updated_at
      end
    end

    it "does not insert more than one default membership" do
      user = users(:minimum_author)
      organization_association = OrganizationAssociationAddition.new(
        account.id, organization2.id, [user.id]
      )
      guid1, guid2 = Array.new(2) { OrganizationMembership.generate_uid }

      OrganizationMembership.connection.execute(
        organization_association.send(:bulk_insert_statement) do
          [
            "(#{guid1},#{user.id},#{organization2.id},#{account.id},'#{Time.now}','#{Time.now}',1)",
            "(#{guid2},#{user.id},#{organization1.id},#{account.id},'#{Time.now}','#{Time.now}',1)"
          ].join(",")
        end
      )

      assert_equal 1, user.organization_memberships.where(default: true).count(:all)
    end

    it "inserts multiple non-default memberships" do
      user = users(:minimum_author)
      organization_association = OrganizationAssociationAddition.new(
        account.id, organization2.id, [user.id]
      )
      guid1, guid2 = Array.new(2) { OrganizationMembership.generate_uid }

      OrganizationMembership.connection.execute(
        organization_association.send(:bulk_insert_statement) do
          [
            "(#{guid1},#{user.id},#{organization2.id},#{account.id},'#{Time.now}','#{Time.now}',NULL)",
            "(#{guid2},#{user.id},#{organization1.id},#{account.id},'#{Time.now}','#{Time.now}',NULL)"
          ].join(",")
        end
      )

      assert_equal 2, user.organization_memberships.where(default: nil).count(:all)
    end
  end

  describe "multiple_organizations is true" do
    let(:user_with_existing_membership) do
      users(:minimum_end_user).tap do |user|
        user.update_attribute(:organizations, [organization1, organization2])
      end
    end

    let(:user_with_other_memberships) do
      users(:minimum_end_user2).tap do |user|
        user.update_attribute(:organization, organization3)
        user.update_attribute(:organizations, [organization1, organization3])
      end
    end

    let(:batch) do
      [
        user_with_existing_membership,
        user_with_other_memberships,
        user_without_association
      ]
    end

    let(:all_org_tickets) do
      Ticket.where("organization_id IS NOT NULL AND requester_id IN (?)", batch.map(&:id)).to_a
    end
    let(:closed_tickets) { all_org_tickets.select(&:closed?) }

    before do
      Account.any_instance.stubs(has_multiple_organizations_enabled?: true)

      tickets(:minimum_1).will_be_saved_by(users(:minimum_end_user))
      tickets(:minimum_1).update_attribute(:organization_id, organization2.id)
      tickets(:minimum_3).will_be_saved_by(users(:minimum_end_user2))
      tickets(:minimum_3).update_attribute(:requester_id, users(:minimum_end_user2).id)

      all_org_tickets.each { |ticket| assert ticket.reload.organization_id }

      assert_nil user_without_association.organization_id
      assert_empty(user_without_association.organization_memberships)

      Account.any_instance.stubs(has_multiple_organizations_enabled?: true)

      assert OrganizationAssociationAddition.new(
        account.id, organization2.id, batch.map(&:id)
      ).call
    end

    it "does not change the user's organization_id" do
      assert_equal organization1.id, user_with_existing_membership.reload.organization_id
      assert_equal organization3.id, user_with_other_memberships.reload.organization_id
    end

    it "sets the user's organization_id when it is null" do
      assert_equal organization2.id, user_without_association.reload.organization_id
    end

    it "does not create new organization_memberships when one is already present" do
      assert_same_elements(
        [organization1.id, organization2.id],
        user_with_existing_membership.reload.organization_memberships.map(&:organization_id)
      )
    end

    it "creates a corresponding organization_membership when one is not present" do
      assert_same_elements(
        [organization1.id, organization2.id, organization3.id],
        user_with_other_memberships.reload.organization_memberships.map(&:organization_id)
      )
    end

    it "creates a default_organization_membership when the user has none" do
      assert_equal(
        organization2.id,
        user_without_association.reload.default_organization_membership.organization_id
      )
    end

    it "does not unset any existing tickets" do
      all_org_tickets.each(&:reload).each do |ticket|
        assert ticket.organization_id
      end
    end
  end

  describe "multiple_organizations is false" do
    let(:with_organization1) do
      [
        users(:minimum_end_user),
        users(:minimum_end_user2)
      ]
    end

    let(:with_organization2) do
      [
        users(:minimum_author).tap do |user|
          user.update_attributes!(organization: organization2)
        end
      ]
    end

    let(:batch) { with_organization1 + with_organization2 + [user_without_association] }

    let(:all_org_tickets) do
      Ticket.where("organization_id IS NOT NULL AND requester_id IN (?)", batch.map(&:id)).to_a
    end

    let(:open_organization1_tickets) do
      all_org_tickets.select do |ticket|
        ticket.organization_id == organization1.id && !ticket.closed?
      end
    end
    let(:closed_tickets) { all_org_tickets.select(&:closed?) }
    let(:before_call) {}

    def assert_organization_association_for(user, organization)
      assert_equal organization.id, user.organization_id
      assert_equal organization.id, user.default_organization_membership.organization_id
      assert user.tickets.not_closed.map(&:organization_id).include?(organization.id)
    end

    before do
      tickets(:minimum_1).will_be_saved_by(users(:minimum_end_user))
      tickets(:minimum_1).update_attribute(:organization_id, organization2.id)
      tickets(:minimum_3).will_be_saved_by(users(:minimum_end_user2))
      tickets(:minimum_3).update_attribute(:requester_id, users(:minimum_end_user2).id)

      all_org_tickets.each { |ticket| assert ticket.organization_id }

      assert_nil user_without_association.organization_id
      assert_empty(user_without_association.organization_memberships)

      with_organization1.each do |user|
        assert_organization_association_for(user, organization1)
      end

      with_organization2.each do |user|
        assert_organization_association_for(user, organization2)
      end

      Account.any_instance.stubs(has_multiple_organizations_enabled?: false)

      before_call

      assert OrganizationAssociationAddition.new(
        account.id, organization2.id, batch.map(&:id)
      ).call
    end

    it "does not unset ticket organization_ids for users already associated with this organization" do
      users(:minimum_author).tickets.not_closed.each(&:reload).each do |ticket|
        assert_equal organization2.id, ticket.organization_id
      end
    end

    it "sets the organization_id" do
      batch.each(&:reload).each do |user|
        assert_equal organization2.id, user.organization_id
      end
    end

    it "sets the organization membership association to the provided organization id" do
      batch.each(&:reload).each do |user|
        assert_equal [organization2.id], user.organization_memberships.map(&:organization_id)
      end
    end

    it "changes the organization_id on all of the open tickets" do
      open_organization1_tickets.each(&:reload).each do |ticket|
        ticket.organization_id.must_equal organization2.id
      end
    end

    it "does not unset the organization_id on the closed tickets" do
      closed_tickets.each(&:reload).each do |ticket|
        assert ticket.organization_id
      end
    end
  end

  describe "#update_default_organization_memberships" do
    describe "when a user already has the org membership" do
      let(:user1) { users(:minimum_end_user) }
      let(:user2) { users(:minimum_end_user2) }
      let(:account) { user1.account }
      let(:organization2) { organizations(:minimum_organization2) }
      let(:organization3) { organizations(:minimum_organization3) }

      describe_with_arturo_enabled :org_association_update_default_only do
        before do
          user1.organization_memberships.create!(organization: organization3)
        end

        it "updates the IDs and memberships, and doesn't blow up" do
          OrganizationAssociationAddition.new(
            account.id, organization2.id, [user1.id, user2.id]
          ).call

          assert_equal organization2.id, user1.reload.organization_id
          assert_equal organization2.id, user2.reload.organization_id
          assert_includes user1.organization_ids, organization2.id
          assert_includes user2.organization_ids, organization2.id
        end
      end

      describe_with_arturo_disabled :org_association_update_default_only do
        before do
          user1.organization_memberships.create!(organization: organization3)
        end

        it "updates the IDs and memberships, and doesn't blow up" do
          assert_raise ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation do
            OrganizationAssociationAddition.new(
              account.id, organization2.id, [user1.id, user2.id]
            ).call
          end
        end
      end
    end
  end

  describe "#call" do
    describe "when the organization has been deleted" do
      before { assert organization2.destroy }

      it "returns nil" do
        assert_nil OrganizationAssociationAddition.new(
          account.id, organization2.id, [user_without_association.id]
        ).call
      end
    end

    describe "when the organization has not been deleted" do
      before { refute organization2.deleted? }

      it "returns the organization" do
        assert_equal organization2, OrganizationAssociationAddition.new(
          account.id, organization2.id, [user_without_association.id]
        ).call
      end
    end
  end
end
