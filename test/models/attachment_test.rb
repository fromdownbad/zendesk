require_relative "../support/test_helper"
require_relative "../support/attachment_test_helper"

require 'attachment_fu'

SingleCov.covered! uncovered: 2

describe Attachment do
  i_suck_and_my_tests_are_order_dependent! # TODO: remove later and fix failing ...

  fixtures :all

  let(:path_prefix) { "#{Rails.root}/test/files" }

  before do
    StoresSynchronizationJob.stubs(:enqueue)
  end

  describe "#file_name_without_extension" do
    let(:attachment) { create_attachment("#{path_prefix}/normal_1.jpg") }

    it "returns the name of the file without an extension" do
      assert_equal "normal_1", attachment.file_name_without_extension
    end
  end

  describe "#file_type" do
    let(:attachment) { create_attachment("#{path_prefix}/normal_1.jpg") }

    describe "when content type is mapped" do
      it "returns the file type" do
        assert_equal "image", attachment.file_type
      end
    end

    describe "when content type is not mapped" do
      before { Attachment.any_instance.stubs(:content_type).returns("invalid/type") }

      it "returns nil" do
        assert_nil attachment.file_type
      end
    end
  end

  describe "#file_extension" do
    let(:attachment) { create_attachment("#{path_prefix}/normal_1.jpg") }

    describe "when content type is mapped" do
      it "returns the file extension" do
        assert_equal "jpg", attachment.file_extension
      end
    end

    describe "when content type is not mapped" do
      before { Attachment.any_instance.stubs(:content_type).returns("invalid/type") }

      it "returns nil" do
        assert_nil attachment.file_extension
      end
    end
  end

  it "does not be susceptible to mysql coercion exploits" do
    attachment = attachments(:serialization_comment_attachment)
    attachment.token = Token.generate
    attachment.save!
    account_id = attachment.account.id

    assert_nil Attachment.find_by_account_id_and_token(account_id, 0)
    assert_raise(ActiveRecord::RecordNotFound) { Attachment.find_by_token_and_account_id!(0, account_id) }
  end

  describe "account scoping" do
    it "does not find attachments attached to other accounts" do
      account = accounts(:minimum)
      ticket = tickets(:minimum_3)
      ticket.add_comment(body: "{{ticket.id}} foobar", is_public: true, account: account, ticket: ticket)

      ticket.comment.attachments << attachments(:serialization_comment_attachment)
      ticket.comment.attachments << attachments(:comment_attachment)

      ticket.will_be_saved_by(account.owner)
      ticket.save!

      assert_sql_queries(1, /`attachments`.`account_id` = #{account.id}/) do
        assert_equal 1, ticket.reload.comments.first.attachments.count
      end
    end
  end

  describe "validations" do
    it "does basic length validations on filename and display_filename" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg")
      attachment.filename = "x" * 9999
      refute attachment.valid?
      expected_errors = ["Filename is too long (maximum is 255 characters after encoding)",
                         "Display filename is too long (maximum is 255 characters after encoding)"]
      assert_equal expected_errors, attachment.errors.to_a
    end

    describe "handles forbidden chars" do
      let(:forbidden_filename_chars) { "\u061C\u200E\u200F\u202A\u202B\u202C\u202D\u202E" }
      let(:forbidden_filename) { "confused_file_#{forbidden_filename_chars}.txt" }
      let(:replace_filename) { "confused_file_#{::Attachment::UNICODE_REPLACE_CHAR * forbidden_filename_chars.length}.txt" }
      let(:sanitized_filename) { "confused_file_#{'_' * forbidden_filename_chars.length}.txt" }

      let(:bad_filename_attachment) do
        attachment = create_attachment("#{path_prefix}/normal_1.jpg")
        attachment.filename = forbidden_filename
        attachment
      end

      describe "replacing them in filenames" do
        before do
          assert bad_filename_attachment.valid?
        end

        it "with underscore (sanitized) in filename" do
          assert_equal sanitized_filename, bad_filename_attachment.filename
        end

        it "with u+FFFD (replace char) in display filename" do
          assert_equal replace_filename, bad_filename_attachment.display_filename
        end
      end

      describe "when frozen" do
        before do
          bad_filename_attachment.filename.freeze
          bad_filename_attachment.display_filename.freeze
          assert bad_filename_attachment.valid?
        end

        it "still sanitizing filename" do
          assert_equal sanitized_filename, bad_filename_attachment.filename
        end

        it "but not replacing in display filename" do
          assert_equal forbidden_filename, bad_filename_attachment.display_filename
        end
      end
    end
  end

  describe '.first_by_token_and_filename' do
    it 'get the first attachment by the given account and filename' do
      attachment = attachments(:attachment_entry)
      result = Attachment.first_by_token_and_filename(attachment.token, attachment.filename)

      assert_equal attachment, result
    end

    it 'get the first attachment by the given account and display_filename' do
      attachment = attachments(:name_with_spaces)
      result = Attachment.first_by_token_and_filename(attachment.token, attachment.display_filename)

      assert_equal attachment, result
    end

    it 'get the first attachment by the given account and escaped display_filename' do
      attachment = attachments(:name_with_spaces)
      result = Attachment.first_by_token_and_filename(attachment.token, CGI.escape(attachment.display_filename))

      assert_equal attachment, result
    end
  end

  describe "#filename" do
    it "stores the in the display_filename field the original_filename (specifically for non-ascii characters that get converted to _)" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg")
      assert_equal "normal_1.jpg", attachment.filename
    end
  end

  describe "#display_filename" do
    it "shows the display_filename if the field is not empty. Otherwise show the filename" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg")
      attachment.update_attribute(:display_filename, nil)
      assert_equal "normal_1.jpg", attachment.display_filename
    end
  end

  describe "#redact!" do
    before do
      @attachment = create_attachment("#{path_prefix}/normal_1.jpg")
      @attachment.source = tickets(:minimum_1).comments.first
    end

    describe "when trying to call #add_redacted_attachment! on the attachment's source returns a truth-y value" do
      before { @attachment.source.stubs(:add_redacted_attachment!).with(anything).returns(true) }

      it "destroys the attachment after adding the new redacted attachment" do
        @attachment.redact!

        assert_raise ActiveRecord::RecordNotFound do
          @attachment.reload
        end
      end

      describe_with_arturo_enabled :email_redact_attachments_from_eml_and_json do
        it "attempts to delete the remote EML file associated with the source's Audit" do
          @attachment.source.audit.expects(:delete_eml_file!).once

          @attachment.redact!
        end

        it "attempts to redact the attachment from the remote JSON file associated with the source's Audit" do
          @attachment.source.audit.raw_email.expects(:redact_attachment_from_json!).once

          @attachment.redact!
        end
      end

      describe_with_arturo_disabled :email_redact_attachments_from_eml_and_json do
        it "does not attempt to delete the remote EML file associated with the source's Audit" do
          @attachment.source.audit.expects(:delete_eml_file!).never

          @attachment.redact!
        end

        it "does not attempt to redact the attachment from the remote JSON file associated with the source's Audit" do
          @attachment.source.audit.expects(:redact_attachment_from_json!).never

          @attachment.redact!
        end
      end
    end

    describe "when trying to call #add_redacted_attachment! on the attachment's source returns a false-y value" do
      before { @attachment.source.stubs(:add_redacted_attachment!).with(anything).returns(false) }

      it "does not destroy the attachment" do
        @attachment.redact!

        assert_not_nil @attachment.reload
      end

      describe_with_and_without_arturo_enabled :email_redact_attachments_from_eml_and_json do
        it "does not attempt to delete the remote EML file associated with the source's Audit" do
          @attachment.source.audit.expects(:delete_eml_file!).never

          @attachment.redact!
        end
      end

      describe_with_and_without_arturo_enabled :email_redact_attachments_from_eml_and_json do
        it "does not attempt to redact the attachment from the remote JSON file associated with the source's Audit" do
          @attachment.source.audit.raw_email.expects(:redact_attachment_from_json!).never

          @attachment.redact!
        end
      end
    end
  end

  describe "MIME types" do
    describe "xlsx" do
      before { @xlsx = create_attachment("#{path_prefix}/test.xlsx") }
      it "sets content_type to application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" do
        assert_equal "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", @xlsx.content_type
      end
    end

    describe "jpg" do
      before { @jpg = create_attachment("#{path_prefix}/normal_1.jpg") }
      it "sets content_type to image/jpeg" do
        assert_equal "image/jpeg", @jpg.content_type
      end
    end
  end

  describe "set_ticket_id" do
    before do
      @attachment = create_attachment("#{path_prefix}/normal_1.jpg")
      @attachment.source = tickets(:minimum_1).comments.first
    end

    it "denormalizes the ticket_id when the attachment belongs to a Comment" do
      @attachment.save!
      assert_equal @attachment.source.ticket_id, @attachment.ticket_id
    end

    describe "when an attachment changes from being owned by a comment (does this even happen?)" do
      it "nulls out the ticket_id" do
        @attachment.save!
        assert @attachment.ticket_id

        @attachment.source = @attachment.account
        @attachment.save!
        assert_nil @attachment.ticket_id
      end
    end
  end

  describe "#has_thumbnail?" do
    before do
      @with_thumbnails = create_attachment("#{path_prefix}/normal_1.jpg")
      @without_thumbnails = create_attachment("#{path_prefix}/harvest.xml")
    end

    it "returns true when thumbnail'd" do
      assert(@with_thumbnails.thumbnailed?)
    end

    it "returns false when not thumbnail'd" do
      assert_equal false, @without_thumbnails.thumbnailed?
    end
  end

  it "creates thumbnails if given a JPG file" do
    assert_equal 1, create_attachment("#{path_prefix}/normal_1.jpg").thumbnails.size
  end

  it "does not create thumbnails if given an XML file" do
    assert_equal 0, create_attachment("#{path_prefix}/harvest.xml").thumbnails.size
  end

  it "creates thumbnails if given an animated GIF" do
    3.times do |i|
      assert_equal 1, create_attachment("#{path_prefix}/animated_#{i + 1}.gif").thumbnails.size
    end
  end

  it "process an oddly dimensioned image nicely" do
    3.times do |i|
      assert_equal 1, create_attachment("#{path_prefix}/large_#{i + 1}.png").thumbnails.size
    end
  end

  it "does not create thumbnails for a dangerously large image" do
    assert_equal 0, create_attachment("#{path_prefix}/lottapixel.jpg").thumbnails.size
  end

  it "does not create thumbnails for a very small (yet still dangerous) image" do
    assert_equal 0, create_attachment("#{path_prefix}/lottaframes.gif").thumbnails.size
  end

  describe "#url" do
    it "generates a public url for the attachment" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
      assert_equal "https://#{attachment.account.subdomain}.zendesk-test.com/attachments/token/#{attachment.token}/?name=normal_1.jpg", attachment.url
    end
    it "generates a relative url for the attachment when relative arg is present" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
      assert_equal "/attachments/token/#{attachment.token}/?name=normal_1.jpg", attachment.url(relative: true)
    end
    it "generates a url for the attachment on a specified host when host arg is present" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
      assert_equal "lemur.zendesk.com/attachments/token/#{attachment.token}/?name=normal_1.jpg", attachment.url(host: "lemur.zendesk.com")
    end
    it "generates a url for the attachment on a specified brand when brand arg is present" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
      brand = FactoryBot.create(:brand, active: true, subdomain: 'wombat')
      assert_equal "https://wombat.zendesk-test.com/attachments/token/#{attachment.token}/?name=normal_1.jpg", attachment.url(brand: brand)
    end
  end

  describe "#generate_url" do
    it "generates a branded url given a brand" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
      brand = FactoryBot.create(:brand, active: true, subdomain: 'wombat')
      assert_equal "https://wombat.zendesk-test.com", attachment.generate_url(brand)
    end
    it "generates a url using the accounts route if no brand is given" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
      assert_equal "https://#{attachment.account.subdomain}.zendesk-test.com", attachment.generate_url(nil)
    end
  end

  describe "#to_liquid" do
    it "generates the url in liquid form" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
      liquid = {"url" => "https://#{attachment.account.subdomain}.zendesk-test.com/attachments/token/#{attachment.token}/?name=normal_1.jpg", "filename" => "normal_1.jpg"}
      assert_equal liquid, attachment.to_liquid
    end
  end

  describe "with an external url" do
    it "creates a blank attachment with a side model" do
      account = accounts(:minimum)
      a = Attachment.from_external_url(account,
        "http://www.gimbo.net/foo.html", "bar.html", "text/html", false, "bar_display.html")

      assert(a.save)
      assert_equal "text/html", a.content_type
      assert_equal "bar_display.html", a.display_filename
      assert_equal "bar.html", a.filename
    end

    it 'guess the content type' do
      account = accounts(:minimum)

      url = 'http://www.gimbo.net/foo.html'
      attachment = Attachment.from_external_url(account, url, 'foo.jpg')
      assert_equal('image/jpeg', attachment.content_type)

      url = 'http://www.gimbo.net/token/?query_param=foo.jpg'
      attachment = Attachment.from_external_url(account, url, 'foo.html')
      assert_equal('text/html', attachment.content_type)
    end

    it 'creates a thumbnail for images' do
      account = accounts(:minimum)
      url = 'http://www.gimbo.net/foo.jpg'
      attachment = Attachment.from_external_url(account, url, 'foo.jpg', 'image/jpeg', true)
      assert attachment.thumbnails.present?
      a = attachment.thumbnails.first
      assert_equal "image/jpeg", a.content_type
      assert_equal "foo.jpg", a.display_filename
      assert_equal "foo.jpg", a.filename
    end

    # existing behavior
    it "should not fail on current_data" do
      account = accounts(:minimum)
      url = 'http://www.gimbo.net/foo.jpg'
      attachment = Attachment.from_external_url(account, url, 'foo.jpg', 'image/jpeg', true)
      attachment.save!
      assert_equal " ", attachment.current_data
      assert_equal " ", attachment.thumbnails.first.current_data
    end

    it "should not be stored anywhere" do
      account = accounts(:minimum)
      url = 'http://www.gimbo.net/foo.jpg'
      attachment = Attachment.from_external_url(account, url, 'foo.jpg', 'image/jpeg', true)
      attachment.save!
      assert_equal [], attachment.stores
      assert_equal [], attachment.thumbnails.first.stores
    end
  end

  describe "#find_by_url" do
    it "is able to parse either a host-mapped or a standard .zendesk.com url" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
      Attachment.stubs(:find_via_routes?).returns(false)

      account = attachment.account
      account.host_mapping = nil
      account.save validate: false

      assert_equal attachment, Attachment.find_by_url(attachment.url)

      account.host_mapping = "foo.bar.com"
      account.save validate: false

      assert attachment.url =~ /foo.bar.com/ # sanity-check the test
      assert_equal attachment, Attachment.find_by_url(attachment.url)
    end

    it "ons a branded subdomain, be able to parse either a host-mapped or a standard .zendesk.com url" do
      attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
      Attachment.stubs(:find_via_routes?).returns(true)

      account = attachment.account
      brand = FactoryBot.create(:brand, account_id: account.id, subdomain: 'wombat', active: true)
      account.host_mapping = nil
      account.save validate: false

      # assert_equal attachment, Attachment.find_by_url(attachment.url(:brand => brand))

      account.host_mapping = "lemur.giraffe.com"
      account.save validate: false

      assert attachment.url =~ /lemur.giraffe.com/ # sanity-check the test
      assert_equal attachment, Attachment.find_by_url(attachment.url(brand: brand))
    end
  end

  describe "#find_by_cdn_domain_url" do
    let(:attachment) { attachments(:on_s3) }
    let(:attachment_domain) { "https://p1.zdusercontent.com" }
    let(:account) { attachment.account }
    let(:token) { Zendesk::Attachments::Token.new(account: account, attachment: attachment, user: account.anonymous_user) }
    let(:attachment_url) { "#{attachment_domain}/attachment/#{attachment.account_id}/#{attachment.token}?token=#{token.encrypt}" }

    it "is able to find an attachment by the CDN domain URL" do
      found = Attachment.find_by_cdn_domain_url attachment_url
      assert_equal attachment, found
    end
  end

  describe "#find_via_routes?" do
    describe "if multibrand feature is not enabled" do
      before do
        feature = stub(phase: "off")
        Arturo::Feature.stubs(:find_feature).returns(feature)
      end
      it "returns false if on the production environment" do
        Rails.env.stubs(production?: true)
        assert_equal false, Attachment.find_via_routes?
      end
      it "returns true if not on production environment" do
        Rails.env.stubs(production?: false)
        assert(Attachment.find_via_routes?)
      end
    end
  end

  describe "from s3" do
    before do
      @s3_file_url = "https://other_bucket.s3.amazonaws.com/foo%40bar/image.jpg"
      @s3_alt_file_url = "https://s3.us-west-2.amazonaws.com/other_bucket/foo%40bar/image.jpg"

      stub_request(:any, /us-west-2\.amazonaws\.com/)
      stub_request(:head, /us-west-2\.amazonaws\.com/).to_return(headers: { content_length: "1234567", content_type: "image/jpeg" })

      StoresSynchronizationJob.stubs(:enqueue)

      # attachments.yml has :fs as default in test
      Attachment.stubs(default_s3_stores: [:s3])

      @attachment = Attachment.new(account: accounts(:minimum))
      @attachment.copy_from_cloud(@s3_file_url)
    end

    it "instantiates an attachment" do
      assert @attachment
      assert_instance_of Attachment, @attachment
    end

    it "uses s3 meta data to get content_type and size" do
      assert_equal 1234567, @attachment.size
      assert_equal "image/jpeg", @attachment.content_type
    end

    describe_with_arturo_setting_disabled :no_mail_delimiter do
      describe "when saved" do
        it "deletes the source file from s3" do
          @attachment.save!
          assert_requested(:delete, @s3_alt_file_url)
        end
      end
    end

    describe_with_arturo_setting_enabled :no_mail_delimiter do
      describe "when saved" do
        before do
          @attachment.save!
        end

        it "copies the file to the right bucket without downloading the file" do
          assert_requested(
            :put,
            %r{amazonaws.com/data/attachments/#{@attachment.id}/image.jpg},
            headers: { x_amz_copy_source: "other_bucket/foo%40bar/image.jpg" }
          )
        end

        before_should "not create thumbnails" do
          @attachment.expects(:create_or_update_thumbnail).never
        end

        before_should "not enqueue a StoresSynchronizationJob" do
          StoresSynchronizationJob.expects(:enqueue).never
        end

        it "does not delete the source file from s3" do
          assert_not_requested(:delete, @s3_alt_file_url)
        end

        describe "and then updated" do
          it "does not download the file from s3" do
            @attachment = Attachment.find(@attachment.id)
            @attachment.size += 1
            @attachment.save!
          end
        end
      end
    end

    describe "#evaluate_processability on update" do
      before do
        @attachment.is_public = false
        @attachment.save!
      end

      it "fetches file from data store" do
        attachment = Attachment.find(@attachment.id)
        attachment.is_public = true
        attachment.save!

        assert_requested(:get, %r{amazonaws.com/data/attachments/#{attachment.id}/image.jpg})
      end

      describe_with_arturo_enabled :process_attachment_on_create_only do
        it "does not fetch file from data store" do
          attachment = Attachment.find(@attachment.id)
          attachment.is_public = true
          attachment.save!

          assert_not_requested(:get, %r{amazonaws.com/data/attachments/#{attachment.id}/image.jpg})
        end
      end
    end
  end

  describe "from remote_files" do
    before do
      config = RemoteFiles::CONFIGURATIONS.values.last
      @remote_file = RemoteFiles::File.new('identifier',
        content: 'content',
        content_type: 'text/plain',
        configuration: config.name)
      @remote_file.store_once!
    end

    it "creates an attachment with the content" do
      attachment = Attachment.new(account: accounts(:minimum))
      attachment.copy_from_cloud(@remote_file.current_url)
      attachment.save!

      assert_equal 'text/plain', attachment.content_type
      assert_equal 'content'.size, attachment.size
    end
  end

  describe "#create_missing_thumbnails!" do
    before do
      @attachment = Attachment.from_file(File.new("#{Rails.root}/test/files/normal_1.jpg"))
      @attachment.account = accounts(:minimum)
      @attachment.stubs(:enqueue_stores_synchronization_job)
      @attachment.create_without_image_processing!
      assert_empty(@attachment.reload.thumbnails)
    end

    it "creates the missing thumbnails" do
      @attachment.create_missing_thumbnails!
      assert @attachment.reload.thumbnails.present?
    end
  end

  describe "with default stores returning s3" do
    before do
      Attachment.any_instance.stubs(:default_attachment_stores).returns([:s3])
      StoresSynchronizationJob.stubs(:enqueue)

      stub_request(:any, /us-west-2\.amazonaws\.com/)
      stub_request(:head, /us-west-2\.amazonaws\.com/).to_return(headers: { content_length: "1234567", content_type: "image/jpeg" })
    end

    describe "should set default to s3" do
      it "should set the default value to s3" do
        attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
        assert_equal attachment.stores, [:s3]
      end
    end
  end

  describe "partitioned path stored on the fs" do
    it "has 2 partitions when id is less than 9999_9999" do
      attachment = attachments(:attachment_entry)
      attachment.fs.__getobj__.send(:write_attribute, :id, 12345678)
      attachment.full_filename.must_include "1234/5678"
    end

    it "has 3 partitions when id is greater than than 9999_9999" do
      attachment = attachments(:attachment_entry)
      attachment.fs.__getobj__.send(:write_attribute, :id, 123456789)
      attachment.full_filename.must_include "0001/2345/6789"
    end
  end

  it 'for partner' do
    attachment = create_attachment("#{path_prefix}/normal_1.jpg", users(:minimum_admin))
    ts_attachment = attachment.for_partner

    assert_equal(attachment.url, ts_attachment.url)
    assert_equal(attachment.filename, ts_attachment.filename)
    assert_equal(attachment.display_filename, ts_attachment.display_filename)
    assert_equal('image/jpeg', ts_attachment.content_type)
  end

  it 'synchronizable stores should match configuration' do
    stores = Attachment.secondary_stores
    assert_same_elements [:s3], stores
  end

  describe '#after_commit' do
    describe "when the attachment source is not an upload token" do
      it 'enqueues a StoresSynchronizationJob for non-thumbnailables' do
        StoresSynchronizationJob.expects(:enqueue)

        attachment = create_attachment("#{path_prefix}/test.xlsx")
        attachment.source = tickets(:minimum_1).comments.first
        attachment.save
      end

      it 'enqueues a StoresSynchronizationJob for non-thumbnailables' do
        StoresSynchronizationJob.expects(:enqueue)

        attachment = create_attachment("#{path_prefix}/normal_1.jpg")
        attachment.source = entries(:ponies)
        attachment.save
      end
    end

    describe "when the attachment source is an upload token" do
      it 'does not enque a StoresSynchronizationJob for non-thumbnailables' do
        StoresSynchronizationJob.expects(:enqueue).never
        create_attachment("#{path_prefix}/test.xlsx")
      end

      it 'does not enqueue a StoresSynchronizationJob for thumbnailables' do
        StoresSynchronizationJob.expects(:enqueue).never
        create_attachment("#{path_prefix}/normal_1.jpg")
      end
    end
  end

  describe 'a new attachment' do
    it 'stores to primary_stores when created' do
      attachment = Attachment.new
      assert_equal attachment.stores, Attachment.primary_stores
    end
  end

  describe "a new image that's not quite ready" do
    before do
      @attachment = Attachment.new(account: accounts(:minimum), content_type: "image/png", filename: "still_copying.png", uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/somefile.txt"))
      @attachment.size = 1234
    end
    it "saves" do
      assert @attachment.save!
      # TODO: this test only works for fs backend, so disabling. Cleanup
      # assert_nil @attachment.current_data
    end
  end

  describe 'public filenames' do
    it 'generateds compatible URL for attachments' do
      attachment = Attachment.last
      assert_equal attachment.fs.public_filename, attachment.path_for_url
    end

    it 'generateds compatible URL for attachments thumbnails' do
      attachment = Attachment.last
      assert_equal attachment.fs.public_filename(:thumb), attachment.path_for_url(:thumb)
    end

    it 'generateds compatible URL for attachments with big ids' do
      attachment = Attachment.last
      attachment.fs.__getobj__.send(:write_attribute, :id, 9259634529)
      assert_equal attachment.fs.public_filename, attachment.path_for_url
    end

    it 'generateds compatible URL for photos' do
      attachment = Photo.last
      assert_equal attachment.fs.public_filename, attachment.path_for_url
    end

    it 'generateds compatible URL for Logos' do
      attachment = HeaderLogo.last
      assert_equal attachment.fs.public_filename, attachment.path_for_url
    end

    it 'generateds compatible URL for Favicons' do
      attachment = Favicon.last
      assert_equal attachment.fs.public_filename, attachment.path_for_url
    end
  end

  describe "size" do
    # yes it seriously *does* matter

    before do
      @attachment = create_attachment("#{path_prefix}/normal_1.jpg")
      assert @attachment.valid?
    end

    describe "on suspended ticket" do
      before do
        @attachment.source = suspended_tickets(:minimum_loop)
        assert @attachment.valid?
      end

      describe_with_and_without_arturo_enabled :email_increase_attachment_size_limit do
        it "does not allow 0 megabytes" do
          @attachment.size = 0
          refute @attachment.valid?
        end
      end

      describe_with_arturo_enabled :email_increase_attachment_size_limit do
        it "allows attachments between 50 and 100 megabytes" do
          @attachment.size = 75.megabytes
          assert @attachment.valid?
        end

        it "allows 100 megabytes" do
          @attachment.size = 100.megabytes
          assert @attachment.valid?
        end

        it "does not allow more than 100 megabytes" do
          @attachment.size = 100.megabytes + 1
          refute @attachment.valid?

          assert_equal "Attachment is too large. Limit 100 megabytes.", @attachment.errors.full_messages.last
        end

        it "does not allow more than 100 megabytes, unless all stores removed" do
          @attachment.update_column :size, 100.megabytes + 1
          @attachment.reload
          refute @attachment.valid?

          @attachment.remove_from_remote!
          @attachment.reload
          assert @attachment.valid?
        end
      end

      describe_with_arturo_disabled :email_increase_attachment_size_limit do
        it "allows 50 megabytes" do
          @attachment.size = 50.megabytes
          assert @attachment.valid?
        end

        it "does not allow more than 50 megabytes" do
          @attachment.size = 50.megabytes + 1
          refute @attachment.valid?

          assert_equal "Attachment is too large. Limit 50 megabytes.", @attachment.errors.full_messages.last
        end

        it "does not allow more than 50 megabytes, unless all stores removed" do
          @attachment.update_column :size, 50.megabytes + 1
          @attachment.reload
          refute @attachment.valid?

          @attachment.remove_from_remote!
          @attachment.reload
          assert @attachment.valid?
        end
      end
    end

    describe "on a normal ticket" do
      it "does not allow 0 megabytes" do
        @attachment.size = 0
        refute @attachment.valid?
      end

      it "allows attachments with a size up to subscription plan's cap" do
        @attachment.size = @attachment.account.subscription.file_upload_cap.megabytes
        assert @attachment.valid?
      end

      it "does not allow attachments with a size more than the subscription plan's cap" do
        @attachment.size = @attachment.account.subscription.file_upload_cap.megabytes + 1
        refute @attachment.valid?

        assert_equal "Attachment is too large. Limit #{@attachment.account.subscription.file_upload_cap} megabytes.", @attachment.errors.full_messages.last
      end

      it "does not allow attachments with a size more than the subscription plan's cap, unless all stores removed" do
        curr_cap = @attachment.account.subscription.file_upload_cap
        @attachment.update_column :size, curr_cap.megabytes + 1
        @attachment.reload
        refute @attachment.valid?

        @attachment.remove_from_remote!
        @attachment.reload
        assert @attachment.valid?
      end
    end
  end

  describe "#from_attachment" do
    before do
      @attachment = attachments(:serialization_comment_attachment)
      @attachment.stubs(filename: "統合元１.txt", current_data: "DATA", display_filename: "friendly.txt")
    end

    it "does not sanitize the filename" do
      assert_equal @attachment.filename, Attachment.from_attachment(@attachment).filename
      assert_equal @attachment.display_filename, Attachment.from_attachment(@attachment).display_filename
    end

    it "sanitizes an unknown content_type" do
      @attachment.stubs(:content_type).returns("THIS IS SOME CRAZY GARBAGE")
      assert_equal "application/unknown", Attachment.from_attachment(@attachment).content_type
    end

    it "does not sanitize an known content_type" do
      @attachment.stubs(:content_type).returns("image/x-xbitmap")
      assert_equal "image/x-xbitmap", Attachment.from_attachment(@attachment).content_type
    end

    it "does not create a new attachment if image attachment does not have thumbnails" do
      @attachment.stubs(:image?).returns(true)
      @attachment.thumbnails = []
      assert_nil Attachment.from_attachment(@attachment)

      @attachment.thumbnails << Attachment.new
      assert_not_nil Attachment.from_attachment(@attachment)
    end
  end

  describe "#ensure_md5!" do
    before do
      @attachment = attachments(:attachment_entry)
    end

    it "returns md5 signature" do
      @attachment.stubs(:md5).returns("asdf")
      assert_equal @attachment.ensure_md5!, "asdf"
    end

    it "calls generate_md5 when no sha exists" do
      @attachment.stubs(:md5).returns(nil)
      @attachment.expects(:generate_md5)
      @attachment.expects(:save)
      @attachment.ensure_md5!
    end

    it "saves the new md5 signature after generate_md5" do
      Digest::MD5.any_instance.expects(:hexdigest).with(any_parameters).returns("foob")
      @attachment.ensure_md5!
      assert_equal "foob", @attachment.reload.md5
    end
  end

  describe "#remove_from_remote!" do
    before do
      @attachment = create_attachment("#{path_prefix}/normal_1.jpg")
    end

    it "remove the attachment from remote storage keeping the database record" do
      old_size = @attachment.size
      @attachment.remove_from_remote!
      @attachment.reload
      assert_empty @attachment.stores
      assert_equal old_size, @attachment.size
      assert_not_nil Attachment.find @attachment.id
    end

    it "is a no-op when no stores" do
      @attachment.remove_from_remote!
      @attachment.expects(:save!).never
      @attachment.remove_from_remote!
    end
  end

  describe "#destroy" do
    let(:attachment) { Attachment.new(content_type: 'image/jpeg') }

    it "does not invoke thumbnailable?" do
      attachment.expects(:thumbnailable?).never
      assert attachment.destroy
    end

    it "should not attempt to destroy thumbnails when it has an external_url" do
      attachment.build_external_url
      attachment.expects(:thumbnails).never
      assert attachment.destroy
    end
  end

  describe "#thumbnailable?" do
    let(:attachment) { Attachment.new(content_type: 'image/jpeg') }

    it 'is not thumbnailable with external_url' do
      attachment.expects(:image?).returns(true)
      attachment.expects(:valid_dimensions?).returns(true)
      assert attachment.thumbnailable?

      attachment.build_external_url
      refute attachment.thumbnailable?
    end

    it "checks image validity and dimensions" do
      attachment.expects(:image?).returns(true)
      attachment.expects(:valid_dimensions?).returns(true)
      assert attachment.thumbnailable?
    end

    describe "with a large attachment" do
      let(:attachment) { create_attachment("#{path_prefix}/example.tif") }

      it "valid_dimensions? returns in a amount of time" do
        Timeout.timeout(15) do # rubocop:disable Lint/Timeout
          assert attachment.send(:valid_dimensions?)
        end
      end
    end
  end

  describe "#send statsd message upon remove_from_remote error" do
    describe "AWS error" do
      before do
        @attachment = create_attachment("#{path_prefix}/example.tif")
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          'error',
          tags: ['exception:aws/s3/errors/no_such_key']
        )
      end

      it "should expose AWS error" do
        aws_error = Aws::S3::Errors::NoSuchKey.new('123', '456')
        Attachment.any_instance.stubs(:destroy_files).raises(aws_error)
        @attachment.remove_from_remote!
      end
    end
  end

  describe "#copy_from_cloud_params" do
    let(:attachment) { attachments(:serialization_comment_attachment) }

    let(:params) do
      {
        cloud: :s3,
        directory: "my-bucket",
        key: "my-key"
      }
    end

    before do
      Attachment.stubs(default_s3_stores: [:s3eu])

      aws_data = stub(content_type: "image/jpeg", content_length: "10")
      aws_client = stub(head_object: aws_data)
      Aws::S3::Client.stubs(new: aws_client)
    end

    it "sets #stores" do
      attachment.copy_from_cloud_params(params)
      assert attachment.stores == [:s3eu]
    end

    it "logs" do
      Rails.logger.expects(:info).with("Assigned attachment store s3eu for my-key")
      attachment.copy_from_cloud_params(params)
    end

    it "sets #filename" do
      attachment.copy_from_cloud_params(params)
      assert attachment.filename == "x.png"
    end

    it "sets #content_type" do
      attachment.copy_from_cloud_params(params)
      assert attachment.content_type == "image/png"
    end

    it "sets #content_length" do
      attachment.copy_from_cloud_params(params)
      assert attachment.size == 10
    end

    it "sets @no_thumbnails" do
      attachment.copy_from_cloud_params(params)
      assert attachment.instance_variable_get(:@no_thumbnails)
    end

    it "sets @no_resize" do
      attachment.copy_from_cloud_params(params)
      assert attachment.instance_variable_get(:@no_resize)
    end

    describe "if no default S3 store is configured" do
      before { Attachment.stubs(default_s3_stores: []) }

      it "raises a custom exception and logs to Datadog" do
        Zendesk::StatsD::Client.any_instance.
          expects(:increment).
          with('processing_error.error', tags: ['detail:Zendesk::AttachmentFromCloud::ConfigurationError'])

        assert_raises(Zendesk::AttachmentFromCloud::ConfigurationError) do
          attachment.copy_from_cloud_params(params)
        end
      end
    end
  end
end
