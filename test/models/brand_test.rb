require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe Brand do
  fixtures :accounts, :account_settings, :brands, :recipient_addresses, :channels_brands, :monitored_twitter_handles, :facebook_pages

  let(:brand) { brands(:minimum) }
  let(:account) { accounts(:minimum) }

  class TestChannel < ActiveRecord::Base
    self.table_name = "voice_phone_numbers"
    include Zendesk::Channels::Brandable
    belongs_to :account
  end

  describe "on a new empty Brand object" do
    should_validate_presence_of :account
    should_validate_presence_of :name
    should_validate_presence_of :route

    it "should not blow up" do
      # I really want to `assert_nothing_raised` here but that's been removed so this is practically good enough.
      assert !Brand.new.valid?
    end
  end

  describe "#delete_orphaned_route" do
    it 'deletes an orphaned route when invalid on creation' do
      @nameless_brand = Brand.new
      @nameless_brand.account_id = account.id
      @nameless_brand.route = FactoryBot.create(:route, account: account)
      refute @nameless_brand.save, "cannot save brand without name/subdomain"
      assert @nameless_brand.route.destroyed?
    end
  end

  describe "uniqueness validation" do
    it "does not allow two NON deleted brands with the same name in the same account" do
      bar = FactoryBot.create(:brand, name: "bar", active: false)

      assert_raise(ActiveRecord::RecordInvalid) do
        FactoryBot.create(:brand, name: "bar")
      end

      bar.soft_delete

      FactoryBot.create(:brand, name: "bar")
    end
  end

  describe "ensure_under_brand_limit" do
    let(:brand2) { FactoryBot.build(:brand, account_id: brand.account_id) }
    let(:brand3) { FactoryBot.build(:brand, account_id: brand.account_id) }

    describe 'with maximum_brands Arturo' do
      before do
        Arturo.enable_feature!(:maximum_brands)
      end

      it "should allow a second brand" do
        brand2.account.settings.stubs(:maximum_brands).returns(2)
        assert brand2.valid?
      end

      it "should not allow a second brand" do
        brand2.account.settings.stubs(:maximum_brands).returns(1)
        refute brand2.valid?
        assert_equal brand2.errors.full_messages.first, "You already have 1 brands, which is the limit for your account."
      end

      it "should not count deleted brands" do
        brand2.account.settings.stubs(:maximum_brands).returns(2)
        brand3.account.settings.stubs(:maximum_brands).returns(2)
        assert brand2.save!
        refute brand3.valid?
        brand2.soft_delete!
        assert brand3.valid?
      end
    end

    describe 'without maximum_brands Arturo' do
      before do
        Arturo.disable_feature!(:maximum_brands)
      end

      it "should allow a second brand" do
        brand2.account.settings.stubs(:maximum_brands).returns(1)
        assert brand2.valid?
      end
    end
  end

  describe "set the logo" do
    it "works when the logo is valid" do
      brand.build_logo(account: brand.account, uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png"))

      assert brand.save
      assert_equal "small.png", brand.logo.filename
    end

    # This test case also covers the 'validate_new_logo' validation
    it "rescues the exception and add an error when the logo is not valid" do
      brand.build_logo(account: brand.account, uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/twitter_favorites_1.1.json", content_type: "image/png"))

      # refute brand.save
      # assert brand.errors.full_messages.grep(/logo/i).present?
    end
  end

  describe "#build_recipient_address" do
    it "creates default recipient address" do
      brand = FactoryBot.create(:brand)
      address = brand.recipient_addresses.last

      assert_equal "support@#{brand.route.default_host}", address.email
      assert_equal brand, address.brand
      assert address.default?
      assert address.backup?

      assert recipient_addresses(:default).default? # did not change previous default
    end
  end

  describe "#invalid_subdomain_change?" do
    describe "on agent route" do
      before { brand.stubs(route_is_agent_route?: true) }

      it "returns true if user is not the owner" do
        brand.subdomain = "newsubdomain"
        assert brand.invalid_subdomain_change?(users(:minimum_admin_not_owner))
      end

      it "returns false if there was not a subdomain change" do
        refute brand.invalid_subdomain_change?(users(:minimum_admin_not_owner))
      end

      it "returns true if user is the owner but the account is sandbox" do
        brand.account.stubs(is_sandbox?: true)
        brand.subdomain = "newsubdomain"
        assert brand.invalid_subdomain_change?(users(:minimum_admin))
      end

      it "returns false if user is the owner and account is not sandbox" do
        brand.subdomain = "newsubdomain"
        refute brand.invalid_subdomain_change?(users(:minimum_admin))
      end
    end

    describe "on non agent route" do
      before { brand.stubs(route_is_agent_route?: false) }

      it "returns false" do
        brand.subdomain = "newsubdomain"
        refute brand.invalid_subdomain_change?(users(:minimum_admin))
      end
    end
  end

  describe "#destroy_recipient_addresses" do
    let(:account_route) { account.routes.create!(subdomain: "foobrand") }
    let(:brand) { account.brands.create! { |b| b.name = "Brando"; b.route = account_route } }

    it "deletes normally undeleteable recipient addresses on soft_delete" do
      brand.active = false
      assert brand.can_be_deactivated?

      credential = ExternalEmailCredential.new(
        account: account,
        username: "USERNAME@gmail.net",
        current_user: users(:minimum_agent)
      ).tap do |external_email_credential|
        external_email_credential.encrypted_value = "unencrypted_token"
        external_email_credential.save!
      end
      credential.recipient_address.update_attribute(:brand, brand)

      # delete backup + default + external_email_credential
      assert_difference "ExternalEmailCredential.count(:all)", -1 do
        assert_difference "RecipientAddress.count(:all)", -2 do
          assert brand.soft_delete!
        end
      end
    end
  end

  describe "#destroy_ticket_form_brand_restrictions" do
    let(:brand) { FactoryBot.create(:brand, subdomain: "foobrand") }

    it "deletes the ticket form brand restrictions" do
      form = account.ticket_forms.create!(name: "form", position: 1, default: true, in_all_brands: false)
      form.ticket_form_brand_restrictions.create! { |tfbr| tfbr.brand = brand }

      assert_difference "TicketFormBrandRestriction.count(:all)", -1 do
        assert brand.soft_delete!
      end
    end
  end

  describe "#update_channel_brands" do
    let(:account_route) { account.routes.create!(subdomain: "foobrand") }
    let(:brand) { account.brands.create! { |b| b.name = "Brando"; b.route = account_route } }
    let(:brandable) { TestChannel.new(account: account) }

    it "switchs channel brands to the default on soft_delete" do
      brandable.brand = brand
      brandable.save

      brand.soft_delete!
      brandable.reload
      assert_equal account.default_brand_id, brandable.brand_id
    end
  end

  describe "#rename_route_for_deleted_brand" do
    setup do
      Timecop.freeze
      Route.any_instance.stubs(:valid_cname_record?).returns(true)
    end

    it 'renames subdomain and remove host mapping on route after soft deleting brand' do
      subdomain = "dontusehello"
      brand = FactoryBot.create(:brand, subdomain: subdomain, host_mapping: "dontusehello.example.com")

      brand.soft_delete!

      assert_equal "#{subdomain}-zdeleted#{Time.now.to_i}", brand.subdomain
      assert_nil brand.host_mapping
      assert Zendesk::RoutingValidations.subdomain_available?(subdomain)
    end

    it 'does not run validations on the subdomain when deleting a subdomain' do
      subdomain = "averyverylongsubdomainnamethatgoesonandonandonandisgreaterjkl"
      brand = FactoryBot.create(:brand, subdomain: subdomain)

      brand.soft_delete!

      assert_equal "#{subdomain}-zdeleted#{Time.now.to_i}", brand.subdomain
    end
  end

  describe "#reassign_brand_on_delete" do
    it 'enqueues the job to reassign tickets to default brand after soft deleting brand' do
      brand = FactoryBot.create(:brand, account_id: account.id)
      TicketBrandReassignJob.expects(:enqueue).with(account.id, brand.id)
      brand.soft_delete!
    end

    it 'does not get enqueued if we had to rollback' do
      brand = FactoryBot.create(:brand, account_id: account.id)
      TicketBrandReassignJob.expects(:enqueue).never
      with_rollback(brand) { brand.soft_delete! }
    end
  end

  describe "#set_brand_logo" do
    describe "on brand without logo" do
      it "returns immediately if data hash is nil" do
        data = nil
        brand.expects(:build_logo).never
        brand.set_brand_logo(data)
      end

      it "returns immediately if data hash is empty" do
        data = {}
        brand.expects(:build_logo).never
        brand.set_brand_logo(data)
      end

      it "returns immediately if uploaded_data is blank" do
        data = { uploaded_data: "" }
        brand.expects(:build_logo).never
        brand.set_brand_logo(data)
      end

      it "sets the logo if the uploaded data is present and valid" do
        data = {uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png")}
        brand.set_brand_logo(data)

        assert brand.save
        assert_equal "small.png", brand.logo.filename
      end
    end

    describe "on brand with logo" do
      before do
        data = {uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/large_1.png")}
        brand.set_brand_logo(data)
        brand.save
        brand.reload
      end

      it "unsets logo if data hash is nil" do
        data = nil
        brand.expects(:build_logo).never
        brand.set_brand_logo(data)
        assert_nil brand.reload.logo
      end

      it "unsets logo if data hash is empty" do
        data = {}
        brand.expects(:build_logo).never
        brand.set_brand_logo(data)
        assert_nil brand.reload.logo
      end

      it "returns immediately if uploaded_data is blank" do
        data = { uploaded_data: "" }
        brand.expects(:build_logo).never
        brand.set_brand_logo(data)
        assert_nil brand.reload.logo
      end

      it "sets the logo if the uploaded data is present and valid" do
        assert_equal "large_1.png", brand.logo.filename
        data = {uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png")}
        brand.set_brand_logo(data)

        assert brand.save
        brand.reload
        assert_equal "small.png", brand.logo.filename
      end
    end
  end

  describe "#validate_can_be_deleted" do
    it "does not allow soft deleting the default brand" do
      refute brand.soft_delete
      assert brand.errors.full_messages.grep(/Cannot delete the default brand/i).present?
    end

    it "does not allow soft deleting brand with agent route" do
      new_brand = new_agent_brand
      refute new_brand.soft_delete
      assert new_brand.errors.full_messages.grep(/This brand shares its subdomain with your account and cannot be deleted/i).present?
    end
  end

  describe "#deactivate_as_part_of_deletion_process" do
    it "deactivates the brand as part of the deletion process" do
      new_brand = FactoryBot.create(:brand)
      assert new_brand.active?

      new_brand.soft_delete!

      assert new_brand.deleted?
      refute new_brand.reload.active?
    end
  end

  describe "#validate_active_state" do
    it "does not allow deactivating the default brand" do
      exception = assert_raise(ActiveRecord::RecordInvalid) do
        brand.update_attributes!(active: false)
      end
      assert_equal "Validation failed: Cannot make the default brand inactive", exception.message
    end

    it "does not allow deactivating the agent brand" do
      new_brand = new_agent_brand
      exception = assert_raise(ActiveRecord::RecordInvalid) do
        new_brand.update_attributes!(active: false)
      end
      assert_equal "Validation failed: This brand shares its subdomain with your account and cannot be deactivated.", exception.message
    end
  end

  describe "#under_active_brands_limit" do
    describe "given limited multibrand" do
      before do
        Account.any_instance.stubs(:has_unlimited_multibrand?).returns(false)
      end

      it "allows creating brand if under the maximum" do
        assert(brand.account.active_brands.count(:all) < 5)
        FactoryBot.create(:brand, account_id: brand.account_id)
      end

      it "allows creating inactive brand if at or above the maximum" do
        while brand.account.active_brands.count(:all) < Brand::MAXIMUM_ACTIVE_BRANDS
          FactoryBot.create(:brand, account_id: brand.account_id)
        end
        FactoryBot.create(:brand, account_id: brand.account_id, active: false)
      end

      it "does not allow creating active brand if at or above the maximum" do
        while brand.account.active_brands.count(:all) < Brand::MAXIMUM_ACTIVE_BRANDS
          FactoryBot.create(:brand, account_id: brand.account_id)
        end
        exception = assert_raise(ActiveRecord::RecordInvalid) do
          FactoryBot.create(:brand, account_id: brand.account_id)
        end
        assert_equal "Validation failed: Your account has already reached its maximum of five active brands. New brand cannot be created.", exception.message
      end

      it "does not allow activating brand if at or above the maximum" do
        while brand.account.active_brands.count(:all) < Brand::MAXIMUM_ACTIVE_BRANDS
          FactoryBot.create(:brand, account_id: brand.account_id)
        end
        inactive = FactoryBot.create(:brand, account_id: brand.account_id, active: false)
        exception = assert_raise(ActiveRecord::RecordInvalid) do
          inactive.update_attributes!(active: true)
        end
        assert_equal "Validation failed: Your account has already reached its maximum of five active brands. This brand cannot be activated.", exception.message
      end
    end

    describe "given unlimited multibrand" do
      before do
        Account.any_instance.stubs(:has_unlimited_multibrand?).returns(true)
      end

      it "allows creating brand if under the maximum" do
        assert(brand.account.active_brands.count(:all) < 5)
        FactoryBot.create(:brand, account_id: brand.account_id)
      end

      it "allows creating inactive brand if at or above the maximum" do
        while brand.account.active_brands.count(:all) < Brand::MAXIMUM_ACTIVE_BRANDS
          FactoryBot.create(:brand, account_id: brand.account_id)
        end
        FactoryBot.create(:brand, account_id: brand.account_id, active: false)
      end

      it "allows creating active brand if at or above the maximum" do
        while brand.account.active_brands.count(:all) < Brand::MAXIMUM_ACTIVE_BRANDS
          FactoryBot.create(:brand, account_id: brand.account_id)
        end
        FactoryBot.create(:brand, account_id: brand.account_id)
      end

      it "allows activating brand if at or above the maximum" do
        while brand.account.active_brands.count(:all) < Brand::MAXIMUM_ACTIVE_BRANDS
          FactoryBot.create(:brand, account_id: brand.account_id)
        end
        inactive = FactoryBot.create(:brand, account_id: brand.account_id, active: false)
        inactive.update_attributes!(active: true)
      end
    end
  end

  describe "#validate_subdomain" do
    before { @new_brand = FactoryBot.create(:brand) }
    it "does not allow subdomains with invalid characters" do
      @new_brand.subdomain = "W0mbat!"
      assert_equal false, @new_brand.valid?
    end

    it "does not allow subdomains longer than the DNS character limit" do
      @new_brand.subdomain = "Wombatsliketoeatlotsoffoodandliveintazmaniaandareamazingtheyalsoarethecoolestanimalever"
      assert_equal false,  @new_brand.valid?
    end
  end

  describe "#validate_signature_template" do
    before { @new_brand = FactoryBot.create(:brand) }

    it "does not allow invalid signature templates" do
      @new_brand.signature_template = "{{wombat.agent"
      assert_equal false, @new_brand.valid?
      assert @new_brand.errors.full_messages.grep(/"The brand signature template is invalid"/)
    end
  end

  describe "kasket" do
    it "caches on id" do
      account.settings.default_brand_id
      assert_sql_queries(1) { account.default_brand }
      account.reload
      assert_sql_queries(0) { account.default_brand }
    end

    it "caches on route_id for route has_one" do
      route = routes(:minimum)
      assert_sql_queries(1) { route.brand }
      route.reload
      assert_sql_queries(0) { route.brand }
    end

    it "caches on account_id" do
      account = accounts(:minimum)
      assert_sql_queries(1) { account.brands(true) }
      assert_sql_queries(0) { account.brands(true) }
    end
  end

  describe "#reply_address" do
    it "is backup when no default address exists (no longer necessary after backfill)" do
      brand.recipient_addresses.where(default: true).first.send(:delete)
      assert_equal "support@minimum.zendesk-test.com", brand.reply_address
    end

    it "is default recipient address" do
      assert_equal "support@zdtestlocalhost.com", brand.reply_address
    end
  end

  describe "#backup_email_address" do
    it "is support@ the routes domain" do
      assert_equal "support@minimum.zendesk-test.com", brand.backup_email_address
    end
  end

  describe "#help_center_in_use?" do
    it "returns false if brand has no help center" do
      assert !brand.help_center_in_use?
    end

    describe "and brand has a help center" do
      it "returns false if help center is archived" do
        HelpCenter.
          stubs(:find_by_brand_id).
          with(brand.id).
          returns(HelpCenter.new(id: 42, state: 'archived'))

        assert !brand.help_center_in_use?
      end

      it "returns true if help center is enabled" do
        HelpCenter.
          stubs(:find_by_brand_id).
          with(brand.id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))

        assert brand.help_center_in_use?
      end

      it "returns true if help center is restricted" do
        HelpCenter.
          stubs(:find_by_brand_id).
          with(brand.id).
          returns(HelpCenter.new(id: 42, state: 'restricted'))

        assert brand.help_center_in_use?
      end
    end
  end

  describe "#host_mapping_not_secured_by_cert?" do
    it "is false if brand does not have host_mapping" do
      brand.host_mapping = nil
      refute brand.host_mapping_not_secured_by_cert?
    end

    describe "with host_mapped brand" do
      before do
        brand.host_mapping = "blah.com"
      end

      it "is false if account does not have active_cert" do
        refute brand.host_mapping_not_secured_by_cert?
      end

      it "is false if account's active_cert secures brand's host_mapping" do
        brand.account.stubs(:active_cert).returns(mock(crt_object_secured_domains: ['blah.com']))
        refute brand.host_mapping_not_secured_by_cert?
      end

      it "is true if account's active_cert does not secure brand's host_mapping" do
        brand.account.stubs(:active_cert).returns(mock(crt_object_secured_domains: []))
        assert brand.host_mapping_not_secured_by_cert?
      end
    end
  end

  describe "#has_twitter_configured?" do
    it "returns true only if there's an active twitter account linked to the brand" do
      assert brand.has_twitter_configured?

      brand.twitter_handles.each(&:deactivate!)

      refute brand.reload.has_twitter_configured?
    end
  end

  describe "#has_facebook_configured?" do
    it "returns true only if there's an active facebook page linked to the brand" do
      brand.facebook_pages.each { |fp| fp.update_attribute(:state, Facebook::Page::State::ACTIVE) }
      assert brand.has_facebook_configured?

      brand.facebook_pages.each { |fp| fp.update_attribute(:state, Facebook::Page::State::INACTIVE) }
      refute brand.reload.has_facebook_configured?
    end
  end

  describe "#ticket_forms" do
    before do
      @default_form = account.ticket_forms.create!(name: "default form", position: 1, default: true, in_all_brands: false)
      @form_in_restrictions = account.ticket_forms.create!(name: "form in restrictions", position: 1, default: false, in_all_brands: false)
      @in_all_brands_form = account.ticket_forms.create!(name: "in all brands form", position: 1, default: false, in_all_brands: true)
      @in_all_brands_form_in_restrictions = account.ticket_forms.create!(name: "in all brands form in restrictions", position: 1, default: false, in_all_brands: true)
      @non_active_form_in_restrictions = account.ticket_forms.create!(name: "non active form in restrictions", position: 1, default: false, active: false, in_all_brands: false)
      @in_all_brands_non_active_form = account.ticket_forms.create!(name: "in all brands non active form", position: 1, default: false, active: false, in_all_brands: true)

      @form_in_restrictions.ticket_form_brand_restrictions.create { |tfbr| tfbr.brand = brand }
      @in_all_brands_form_in_restrictions.ticket_form_brand_restrictions.create { |tfbr| tfbr.brand = brand }
      @non_active_form_in_restrictions.ticket_form_brand_restrictions.create { |tfbr| tfbr.brand = brand }
    end

    it "returns a unique list of active and inactive ticket forms with in_all_brands=true or directly associated to the brand" do
      expected = [@in_all_brands_form, @in_all_brands_form_in_restrictions, @in_all_brands_non_active_form, @form_in_restrictions, @non_active_form_in_restrictions].map(&:reload).sort_by(&:position)
      assert_equal expected, brand.ticket_forms
    end
  end

  describe "#satisfaction_reasons" do
    before do
      Satisfaction::Reason.create_system_reasons_for(account)
    end

    # Satisfaction Reasons should be scoped to the current account ZD 5242550
    it 'scopes satisfaction reasons to current account' do
      assert_includes account.default_brand.satisfaction_reasons.to_sql, "account_id"
    end
  end

  describe "#inactive_satisfaction_reasons" do
    before do
      Satisfaction::Reason.create_system_reasons_for(account)
    end

    it "returns an empty list" do
      assert_empty brand.inactive_satisfaction_reasons
    end

    describe "when there is one inactive reason" do
      before do
        brand.satisfaction_reason_brand_restrictions.first.delete
      end

      it "returns one reason" do
        assert_equal 1, brand.inactive_satisfaction_reasons.count
      end
    end
  end

  private

  def new_agent_brand
    new_brand = FactoryBot.create(:brand, account_id: account.id)

    account.route = new_brand.route
    account.save!

    assert new_brand.reload.route_is_agent_route?
    new_brand
  end
end
