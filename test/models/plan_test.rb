require_relative "../support/test_helper"

SingleCov.covered!

describe Plan do
  describe "'Huh??' methods" do
    before { @plan = Plan.new }

    it "is free if price is zero" do
      @plan.price = 0
      assert @plan.is_free?
      @plan.price = 1
      refute @plan.is_free?
    end

    # should "be the opposite of is_free with is_brandable" do
    #  @plan.price = 0
    #  refute @plan.is_brandable?
    #  @plan.price = 1
    #  assert @plan.is_brandable?
    # end
  end

  describe "Summary" do
    before { @plan = Plan.new(name: "Test", max_end_users: 1, max_agents: 1) }

    # it "gives a custom summary message" do
    #  @plan.summary.must_include "Your help desk is running the Test Zendesk plan. It has the following features: "
    #  @plan.summary.must_include "Max <span class=warning>1</span> agent(s)"
    # end

    # describe "building with unlimited users or not" do
    #  it "prints 'Unlimited end-users' when there are unlimited users" do
    #    @plan.max_end_users = Plan::UNLIMITED_USERS
    #    @plan.summary.must_include "Unlimited end-users"
    #  end

    #  it "prints the max end-users" do
    #    assert_match  "Max <span class=warning>1</span> end-users", @plan.summary
    #  end
    # end

    describe "is brandable and has ssl" do
      before do
        @plan.price = 1
        @plan.has_ssl = true
      end

      # should "give a custom summary message" do
      #  @plan.summary.must_include "Mail/domain mapping"
      #  @plan.summary.must_include "SSL encryption"
      #  assert_no_match /Zendesk ads featured in mails and public forums/, @plan.summary
      # end
    end
  end
end
