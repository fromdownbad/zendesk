require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'Account::UrlShortenerSettings' do
  fixtures :accounts, :account_property_sets

  describe 'an Account' do
    before do
      @account = accounts(:minimum)
      @account.build_url_shortener name: :default
      @url_shortener = @account.url_shortener
    end

    it 'requires URL shortener settings if its Tweets are set to shorten URLs' do
      Account.any_instance.stubs(:has_end_user_ui?).returns(true)
      @account.settings.disable(:no_short_url_tweet_append)
      @account.url_shortener.update_attributes(name: nil)
      refute @account.valid?
    end

    describe 'with URL shortener settings and URL shortening on' do
      before do
        @account.settings.disable(:no_short_url_tweet_append)
        @account.url_shortener.update_attributes(name: 'default')
      end

      subject { @account.url_shortener }

      should_validate_presence_of :name

      should_allow_values_for :name, 'default', 'bitly', 'isgd', 'owly', 'custom'
      should_not_allow_values_for :name, 'fdjsal'

      describe 'with type = bitly' do
        before do
          @account.url_shortener.name = 'bitly'
        end

        should_validate_presence_of :username, :api_key
      end

      describe 'with type = owly' do
        before do
          @account.url_shortener.name = 'owly'
        end

        should_validate_presence_of :api_key
      end

      describe 'with type = custom' do
        before do
          @account.url_shortener.name = 'custom'
        end

        should_validate_presence_of :url
      end

      it 'saves URL shortener settings to account' do
        @account.url_shortener.update_attributes(
          name: 'bitly',
          username: 'wombats',
          api_key: '389dsnk3'
        )

        @account.account_property_set.reload
        assert_equal 'bitly', @account.url_shortener.name
        assert_equal 'wombats', @account.url_shortener.username
        assert_equal '389dsnk3', @account.url_shortener.api_key
      end
    end

    describe "without twitter links capability" do
      before do
        @url_shortener.account.stubs(:has_end_user_ui?).returns(false)
      end

      it "returns false when asked if url_shortening is on" do
        assert_equal false, @account.url_shortener.send(:url_shortening_on?)
      end
    end
  end
end
