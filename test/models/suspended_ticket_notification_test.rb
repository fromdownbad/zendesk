require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe SuspendedTicketNotification do
  should_validate_presence_of :account

  fixtures :accounts

  before do
    @account      = accounts(:minimum)
    @notification = @account.build_suspended_ticket_notification
  end

  describe "#ensure_default_last_sent_at" do
    it "sets default" do
      refute @notification.last_sent_at
      @notification.save!
      assert @notification.last_sent_at
    end

    it "does not overwrite" do
      @notification.save!
      old = @notification.last_sent_at
      @notification.save!
      @notification.last_sent_at.must_equal old
    end
  end

  describe "#validate_frequency" do
    before { @notification.email_list = 'xxx@yyy.com' }

    it "allows nil" do
      assert_valid @notification
      @notification.frequency.must_equal 0
    end

    it "allows other values" do
      @notification.frequency = 10
      assert_valid @notification
      @notification.frequency.must_equal 10
    end

    it "does not allow unknoqn values" do
      @notification.frequency = 11111
      refute_valid @notification
    end
  end

  describe "#validate_email_list" do
    describe "with empty entries" do
      it "is valid" do
        assert_valid @notification
      end

      it "sets frequency to last" do
        @notification.frequency = 10
        assert_valid @notification
        @notification.frequency.must_equal 0
      end
    end

    it "is invalid with invalid entries" do
      @notification.email_list = "hello@example.com wibble@example.org dingelinge"
      refute_valid @notification
    end

    describe "with valid entries" do
      before { @notification.email_list = "hello@example.com wibble@example.org" }

      it "is valid" do
        assert_valid @notification
      end

      it "does not reset frequency" do
        @notification.frequency = 10
        assert_valid @notification
        @notification.frequency.must_equal 10
      end
    end
  end
end
