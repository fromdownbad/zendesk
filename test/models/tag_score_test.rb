require_relative "../support/test_helper"

SingleCov.covered!

describe TagScore do
  fixtures :accounts, :tickets

  describe "#update" do
    let(:account)   { accounts("minimum") }
    let(:tag_name)  { "sometag" }

    before do
      Tagging.without_arsi.delete_all
    end

    it "updates the score for a specific @tag when score is > 0" do
      TagScore.expects(:delete_all).with(account_id: account.id, tag_name: tag_name)
      mock_tagging = mock
      Tagging.stubs(:where).with(account_id: account.id, tag_name: tag_name).returns(mock_tagging)
      mock_tagging.stubs(:where).with('created_at >= ?', 60.days.ago.beginning_of_day).returns(stub(count: 4))
      TagScore.expects(:replace_score).with(account, tag_name, 4)
      TagScore.update(account, tag_name)
    end

    it "removes the score for a specific @tag when the score is 0" do
      TagScore.expects(:delete_all).with(account_id: account.id, tag_name: tag_name)
      mock_tagging = mock
      Tagging.stubs(:where).with(account_id: account.id, tag_name: tag_name).returns(mock_tagging)
      mock_tagging.stubs(:where).with('created_at >= ?', 60.days.ago.beginning_of_day).returns(stub(count: 0))
      TagScore.expects(:replace_score).never

      TagScore.update(account, tag_name)
    end

    it 'only calculates tagging less than 60 days old' do
      TagScore.expects(:delete_all).with(account_id: account.id, tag_name: tag_name)
      Tagging.create!(account: account, tag_name: tag_name, taggable: tickets("minimum_1"))
      Tagging.without_arsi.update_all(created_at: 61.days.ago, tag_name: tag_name)
      Tagging.create!(account: account, tag_name: tag_name, taggable: tickets("minimum_2"))
      TagScore.expects(:replace_score).with(account, tag_name, 1)
      TagScore.update(account, tag_name)
    end

    it 'will retry the tag score update 2 times' do
      mock_tagging = mock
      Tagging.stubs(:where).with(account_id: account.id, tag_name: tag_name).returns(mock_tagging)
      mock_tagging.stubs(:where).with('created_at >= ?', 60.days.ago.beginning_of_day).returns(stub(count: 4))
      TagScore.expects(:delete_all).with(account_id: account.id, tag_name: tag_name).times(2)
      TagScore.expects(:replace_score).times(2).raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound, 'required_parameter')
      Rails.logger.expects(:info).with("TagScore.update failed after 2 tries on account id #{account.id}, tag_name #{tag_name}")
      Zendesk::StatsD::Client.any_instance.expects(:increment)
      TagScore.update(account, tag_name)
    end
  end

  describe "#replace_score" do
    let(:account)   { accounts("minimum") }
    let(:tag_name)  { "sometag" }

    # Commenting out these tests for now until we can bump zendesk_database_migrations
    # to have the following things in test DB:
    #   * unique index: https://github.com/zendesk/zendesk_database_migrations/pull/1176
    #   * updated_at column: https://github.com/zendesk/zendesk_database_migrations/pull/1307
    # REPLACE INTO queries only work with that unique index.
    it "creates or updates the score for a specific tag" do
      if TagScore.connection.column_exists?(:tag_scores, :updated_at)
        TagScore.replace_score(account, tag_name, 1)
        # assert_equal 1, TagScore.find_by_tag_name(tag_name).score
        # assert TagScore.find_by_tag_name(tag_name).updated_at.present?

        TagScore.replace_score(account, tag_name, 2)
        # assert_equal 2, TagScore.find_by_tag_name(tag_name).score
        # assert TagScore.find_by_tag_name(tag_name).updated_at.present?
      end
    end
  end
end
