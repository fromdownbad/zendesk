require_relative "../../support/test_helper"
require_relative "../../support/brand_test_helper"

SingleCov.covered!

describe Brand::HelpCenterSupport do
  include BrandTestHelper

  before do
    @account = FactoryBot.create(:account)
    @account.stubs(:has_multibrand?).returns(true)
  end

  describe "deactivating brand" do
    before do
      @active_brand = create_brand(@account, name: 'Brand1', subdomain: 'brand1')
    end

    describe "without help center" do
      it "does not send an API request to restrict help center" do
        refute @active_brand.has_help_center?
        @active_brand.update_attributes active: false
        url = "https://#{@account.subdomain}.zendesk-test.com/hc/api/v2/help_centers"
        assert_not_requested(:put, /#{url}/)
      end
    end

    describe 'with help center' do
      before do
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@active_brand.id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))
      end

      it "sends an API request to restrict help center" do
        assert @active_brand.has_help_center?
        url = "https://#{@account.subdomain}.zendesk-test.com/hc/api/v2/help_centers/42.json"
        stub_request(:put, url)
        @active_brand.update_attributes active: false
        assert_requested(:put, url)
      end
    end
  end

  describe "soft deleting brand" do
    before do
      @active_brand = create_brand(@account, name: 'Brand1', subdomain: 'brand1')
      @active_brand.update_attributes active: false
    end

    describe "without help center" do
      it "does not send an API request to restrict help center" do
        refute @active_brand.has_help_center?
        @active_brand.soft_delete!
        assert_not_requested(:delete, //)
      end
    end

    describe 'with help center' do
      before do
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@active_brand.id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))
      end

      it "sends an API request to delete help center" do
        url = "https://#{@account.subdomain}.zendesk-test.com/hc/api/v2/help_centers/42.json"
        stub_request(:delete, url)
        @active_brand.soft_delete!
        assert_requested(:delete, url)
      end
    end
  end
end
