require_relative "../support/test_helper"

SingleCov.covered!

describe UserAnyChannelIdentity do
  fixtures :accounts, :user_identities

  describe '::identity_type' do
    it 'returns any_channel' do
      assert_equal :any_channel, UserAnyChannelIdentity.identity_type
    end
  end

  describe '#subtype_name' do
    before do
      @identity = user_identities(:minimum_end_user_any_channel)
    end

    describe 'RegisteredIntegrationService found' do
      before do
        @ris = stub('RegisteredIntegrationService', name: 'test name')
        ::Channels::AnyChannel::RegisteredIntegrationService.expects(:where).returns([@ris])
      end

      it 'returns the name of the relevant RegisteredIntegrationService' do
        assert_equal @ris.name, @identity.subtype_name
      end
    end

    describe 'RegisteredIntegrationService not found' do
      it 'returns nil' do
        assert_nil @identity.subtype_name
      end
    end
  end
end
