require_relative "../support/test_helper"
require 'zendesk/inbound_mail'

SingleCov.covered! uncovered: 6

describe UserEmailIdentity do
  extend ArturoTestHelper

  fixtures :accounts,
    :brands,
    :users,
    :recipient_addresses,
    :suspended_tickets,
    :user_identities,
    :role_settings,
    :tickets

  let(:user) { users(:minimum_end_user) }
  let(:user_identity) { user_identities(:minimum_end_user) }
  let(:account) { accounts(:minimum) }

  before { Account.any_instance.stubs(:default_brand).returns(brands(:minimum)) }

  describe ".identity_type" do
    it "returns :email" do
      assert_equal :email, UserEmailIdentity.identity_type
    end
  end

  describe "deliverability" do
    before do
      @identity = user_identities(:deliverable_identity)
      assert_equal 0, @identity.undeliverable_count
      assert @identity.deliverable?
    end

    describe ".increment_undeliverable_count!" do
      it "increments the undeliverable_count for matching identities" do
        account.user_email_identities.increment_undeliverable_count!([@identity.value])
        assert_equal 1, @identity.reload.undeliverable_count

        account.user_email_identities.increment_undeliverable_count!([@identity.value])
        assert_equal 2, @identity.reload.undeliverable_count
      end

      it "does not blow up when no matching idientities exist" do
        account.user_email_identities.increment_undeliverable_count!(['', 'blah'])
      end

      describe "when it is marked as undeliverable max times" do
        describe "when the identity was previously deliverable" do
          it "flips the identity to `DeliverableStateType.UNDELIVERABLE`" do
            Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT.times { @identity.increment_undeliverable_count! }
            assert_equal(DeliverableStateType.UNDELIVERABLE, @identity.deliverable_state)
          end
        end

        describe "when the identity was previously deliverable" do
          it "retains the existing state" do
            @identity.mark_as_ticket_sharing_partner
            refute(@identity.deliverable?)
            Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT.times { @identity.increment_undeliverable_count! }
            assert_equal(DeliverableStateType.TICKET_SHARING_PARTNER, @identity.deliverable_state)
          end
        end
      end

      describe "when it is marked as undeliverable more than the max times" do
        before do
          (Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT + 1).times { @identity.increment_undeliverable_count! }
        end

        it "doesn't increment the count past the max" do
          assert_equal(Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT, @identity.undeliverable_count)
        end

        it "doesn't change the state past UNDELIVERABLE" do
          assert_equal(DeliverableStateType.UNDELIVERABLE, @identity.deliverable_state)
        end
      end
    end

    describe ".reset_undeliverable_count!" do
      it "resets the undeliverable_count for matching identities" do
        account.user_email_identities.increment_undeliverable_count!([@identity.value])
        @identity.reload
        assert_equal 1, @identity.undeliverable_count

        account.user_email_identities.reset_undeliverable_count!([@identity.value])
        @identity.reload
        assert_equal 0, @identity.undeliverable_count
      end

      it "does not blow up when no matching idientities exist" do
        account.user_email_identities.reset_undeliverable_count!(['', 'blah'])
      end
    end

    describe "#machine?" do
      describe "when email address is a machine" do
        let(:identity) do
          identity = UserEmailIdentity.new(user: user, value: 'me@somewhere.com')
          identity.deliverable_state = DeliverableStateType.MACHINE
          identity
        end

        it { assert identity.machine? }
      end

      describe "when email address is not a machine" do
        let(:identity) { UserEmailIdentity.new(user: user, value: 'me@somewhere.com') }

        it { refute identity.machine? }
      end
    end

    describe "#refresh_deliverable_state" do
      let(:identity) { UserEmailIdentity.new(user: user, value: 'me@valid.com') }

      before do
        identity.stubs(allow_invalid_addresses?: false)
      end

      describe "when the email address is a reserved_example" do
        before { identity.stubs(value: 'me@example.com') }

        it "marks identity as RESERVED_EXAMPLE" do
          identity.save # Triggers before_save :refresh_deliverable_state
          assert_equal DeliverableStateType.RESERVED_EXAMPLE, identity.deliverable_state
        end
      end

      describe "when the email address is mailer-daemon" do
        before { identity.stubs(value: 'mailer-daemon@anywhere.com') }

        it "marks identity as MAILER_DAEMON" do
          identity.save # Triggers before_save :refresh_deliverable_state
          assert_equal DeliverableStateType.MAILER_DAEMON, identity.deliverable_state
        end
      end

      describe "when the email address is a machine" do
        let(:identity) do
          identity = UserEmailIdentity.new(user: user, value: 'me@somewhere.com')
          identity.deliverable_state = DeliverableStateType.MACHINE
          identity
        end

        it "marks identity as MACHINE" do
          identity.save # Triggers before_save :refresh_deliverable_state
          assert_equal DeliverableStateType.MACHINE, identity.deliverable_state
        end
      end

      describe "when new identity is a ticket sharing partner support address" do
        it "marks identity as a ticket sharing partner" do
          Account.any_instance.stubs(:ticket_sharing_partner_support_addresses).returns(['me@example.com'])
          identity = UserEmailIdentity.new(user: user, value: 'me@example.com')
          identity.save
          assert_equal DeliverableStateType.TICKET_SHARING_PARTNER, identity.deliverable_state
        end
      end

      describe "when new identity is not a ticket sharing partner support address" do
        it "does not mark identity as a ticket sharing partner" do
          Account.any_instance.stubs(:ticket_sharing_partner_support_addresses).returns([])
          identity = UserEmailIdentity.new(user: user, value: 'me@example.com')
          identity.save
          refute_equal DeliverableStateType.TICKET_SHARING_PARTNER, identity.deliverable_state
        end
      end

      describe "when an existing identity's email address is changed to a ticket sharing partner support address" do
        it "marks identity as a ticket sharing partner" do
          Account.any_instance.stubs(:ticket_sharing_partner_support_addresses).returns(['someoneelse@example.com'])
          identity = user_identities(:deliverable_identity)
          identity.value = 'someoneelse@example.com'
          identity.save
          assert_equal DeliverableStateType.TICKET_SHARING_PARTNER, identity.deliverable_state
        end
      end

      describe "when an existing identity's email address is changed from a ticket sharing partner support address" do
        it "marks identity as deliverable" do
          Account.any_instance.stubs(:ticket_sharing_partner_support_addresses).returns(['ticket_sharing_partner@example.com'])
          identity = user_identities(:ticket_sharing_partner_identity)
          identity.value = 'notfoo@example.com'
          identity.save
          assert_equal DeliverableStateType.DELIVERABLE, identity.deliverable_state
        end
      end

      describe "when an UNDELIVERABLE identity's email address is changed" do
        before do
          Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT.times { @identity.increment_undeliverable_count! }
          assert_equal DeliverableStateType.UNDELIVERABLE, @identity.deliverable_state
        end

        it "marks identity as deliverable" do
          @identity.value = 'notfoo@example.com'
          @identity.save
          assert_equal DeliverableStateType.DELIVERABLE, @identity.deliverable_state
        end
      end

      describe "when an existing identity's verified state is modified" do
        it "is not called" do
          identity = user_identities(:admin_to_be_verified)
          identity.expects(:refresh_deliverable_state).never
          identity.verify
        end
      end
    end
  end

  describe "validations" do
    describe "on uniqueness" do
      it "succeeds when an identity with the same value exists but on different accounts" do
        identity = UserEmailIdentity.new(value: user.identities.email.first.value, user: users(:with_groups_end_user))
        assert_valid identity
      end

      it "fails when it is the same account and an identity with the same value already exists" do
        identity = UserEmailIdentity.new(user: user, value: user.identities.email.first.value)
        refute identity.save
        assert_equal ["#{user.identities.email.first.value} is already being used by another user"], identity.errors[:value]
      end

      it "is case insensitive" do
        identity = UserEmailIdentity.new(user: user, value: user.identities.email.first.value.upcase)
        refute identity.save
        assert_equal ["#{user.identities.email.first.value} is already being used by another user"], identity.errors[:value]
      end
    end

    it "fails when value does not look like an email address" do
      identity = UserEmailIdentity.new(user: user, value: 'not a valid email')
      refute_valid identity
      assert_equal ["not a valid email is not properly formatted"], identity.errors[:value]
    end

    it "fails and escape the input rendered in the error message when value includes HTML markup" do
      identity = UserEmailIdentity.new(user: user, value: '><img src="">')
      refute_valid identity
      assert_equal ["&gt;&lt;img src=&quot;&quot;&gt; is not properly formatted"], identity.errors[:value]
    end

    it "fails if the input has not been sanitized" do
      identity = UserEmailIdentity.new(user: user, value: "'hello@example.com'")
      refute_valid identity
      assert identity.errors[:value].any?
    end

    it "fails if the input includes utf-8 characters" do
      identity = UserEmailIdentity.new(user: user, value: 'ǨǨǨǨǨǨǨǨǨ@foo.com')
      refute_valid identity
      assert identity.errors[:value].any?
    end

    it "fails if the email is too long" do
      email = ("a" * 255) + "@gmail.com"
      identity = UserEmailIdentity.new(user: user, value: email)
      refute_valid identity
      assert identity.errors[:value].any?
    end

    it "strips whitespace before validating" do
      value_with_whitespace = 'beer@example.com '
      identity = UserEmailIdentity.new(user: user, value: value_with_whitespace)
      assert_equal value_with_whitespace.strip, identity.value
      assert_valid identity
    end

    describe "#value_is_not_a_domain_invalid_address" do
      describe "when the identity is not invalid" do
        let(:identity) { UserEmailIdentity.create(user: user, value: 'xyz@valid.com') }

        it { assert_valid identity }
        it { assert_empty identity.errors }
      end

      describe "when the identity is invalid" do
        let(:identity) { UserEmailIdentity.create(user: user, value: 'xyz@domain.invalid') }

        it { refute_valid identity }
        it { assert_equal ["Email: xyz@domain.invalid is not a valid address"], identity.errors[:value] }
      end
    end

    describe "#value_is_not_in_email_blacklist" do
      let(:identity) { UserEmailIdentity.create(user: user, value: email) }

      describe "when value is a valid email address" do
        let(:email) { "noreply@zopim.com.au" }

        it { assert_valid identity }
        it { assert_empty identity.errors }
      end

      UserEmailIdentity::EMAIL_BLACKLIST.each do |blacklisted|
        describe "when value is a blacklisted email address (#{blacklisted})" do
          let(:email) { blacklisted }

          it { refute_valid identity }
          it { assert_equal ["Email: #{email} is a restricted address"], identity.errors[:value] }

          it "is valid when skip_email_identity_blacklist_validation is set" do
            identity.user.skip_email_identity_blacklist_validation = true
            assert_valid identity
          end
        end
      end
    end

    describe "#value_is_not_in_use_by_account" do
      it "fails when value is a recipient address" do
        email = account.default_recipient_address.email
        identity = UserEmailIdentity.new(user: user, value: email)
        refute_valid identity
        assert_equal ["#{email} cannot be used; it is in use as a support address"], identity.errors[:value]
      end

      it "fails when value is a recognized support address" do
        email = "foobar@#{account.default_host}"
        identity = UserEmailIdentity.new(user: user, value: email)
        refute_valid identity
        assert_equal ["#{email} cannot be used; it is in use as a support address"], identity.errors[:value]
      end

      it "fails when value is a recognized support address on another host" do
        route = account.routes.create!(subdomain: "foobrand")
        account.brands.create! { |b| b.name = "Brando"; b.route = route }
        email = "foobar@foobrand.#{Zendesk::Configuration.fetch(:host)}"
        identity = UserEmailIdentity.new(user: user, value: email)
        refute_valid identity
        assert_equal ["#{email} cannot be used; it is in use as a support address"], identity.errors[:value]
      end
    end
  end

  it "is convertable into a Google identity" do
    identity = user.identities.first

    assert_equal UserEmailIdentity, identity.class
    refute identity.google?
    assert identity.email?

    identity.build_google_profile.save!
    assert identity.google?
    refute identity.email?
  end

  it 'extracts the email domain to the `domain` column' do
    user_identity.update_column(:domain, nil)

    user_identity.value = 'foo@some-domain.com'
    user_identity.save!
    user_identity.reload

    user_identity.value.must_equal 'foo@some-domain.com'
    user_identity.domain.must_equal 'some-domain.com'
  end

  it 'overwrites an empty string as a domain' do
    user_identity.update_column(:domain, '')

    user_identity.value = 'foo@some-domain.com'
    user_identity.save!
    user_identity.reload

    user_identity.value.must_equal 'foo@some-domain.com'
    user_identity.domain.must_equal 'some-domain.com'
  end

  it 'handles multiple @ symbols in email addresses' do
    user_identity.update_column(:domain, '')

    user_identity.value = '"foo@zendesk"@some-domain.com'
    user_identity.save!
    user_identity.reload

    user_identity.value.must_equal '"foo@zendesk"@some-domain.com'
    user_identity.domain.must_equal 'some-domain.com'
  end

  describe "#send_verification_email" do
    describe "if the user portal allows it" do
      before do
        Zendesk::UserPortalState.any_instance.expects(:send_identity_verification_email?).returns(true)
      end

      it "calls deliver_verification_email" do
        user_identity.expects(:deliver_verification_email)
        user_identity.send_verification_email
      end

      it "does not request a verification token at the time of request" do
        user_identity.expects(:verification_token).never
        user_identity.send_verification_email
      end
    end

    it "does not call deliver_verification_email if user portal doesn't allows it" do
      Zendesk::UserPortalState.any_instance.expects(:send_identity_verification_email?).returns(false)
      user_identity.expects(:deliver_verification_email).never
      user_identity.send_verification_email
    end
  end

  describe "#deliver_verification_email" do
    describe "with a shell account" do
      let(:account) { accounts(:shell_account_without_support) }
      let(:user) { users(:shell_owner_without_support) }
      let(:user_identity) { user_identities(:shell_owner_without_support) }

      describe "with a staff user" do
        it "sends a multiproduct verification email" do
          UsersMailer.expects(:deliver_verify).never
          UsersMailer.expects(:deliver_verify_multiproduct_staff).with(user_identity, nil)

          user_identity.send(:deliver_verification_email)
        end
      end

      describe "with a non-staff user" do
        before do
          User.any_instance.stubs(:is_end_user?).returns(true)
        end

        it "sends a non-multiproduct verification email" do
          UsersMailer.expects(:deliver_verify).
            with(user_identity, user.verify_email_text, user.verify_email_subject, nil, user.active_brand_id, nil)
          UsersMailer.expects(:deliver_verify_multiproduct_staff).never

          user_identity.send(:deliver_verification_email)
        end
      end
    end

    describe "with a non-shell account" do
      it "sends a non-multiproduct verification email" do
        UsersMailer.expects(:deliver_verify).
          with(user_identity, user.verify_email_text, user.verify_email_subject, nil, user.active_brand_id, nil)
        UsersMailer.expects(:deliver_verify_multiproduct_staff).never

        user_identity.send(:deliver_verification_email)
      end
    end
  end

  describe "#send_initial_verification_email" do
    it "calls deliver_verification_email if user portal allows it" do
      Zendesk::UserPortalState.any_instance.expects(:send_initial_identity_verification_email?).returns(true)
      user_identity.expects(:deliver_verification_email)
      user_identity.send(:send_initial_verification_email)
    end

    it "does not call deliver_verification_email if user portal doesn't allows it" do
      Zendesk::UserPortalState.any_instance.expects(:send_initial_identity_verification_email?).returns(false)
      user_identity.expects(:deliver_verification_email).never
      user_identity.send(:send_initial_verification_email)
    end
  end

  describe "#make_primary_and_destroy_former!" do
    before { @primary_email_identity = user.identities.detect(&:primary?) }

    it "calls make_primary! on the new identity" do
      @new_email_identity = UserEmailIdentity.create!(user: user, value: "foo@bar.com")
      @new_email_identity.expects(:make_primary!)
      @new_email_identity.send(:make_primary_and_destroy_former!)
    end

    it "destroys the former primary identity of the user" do
      @new_email_identity = UserEmailIdentity.create!(user: user, value: "foo@bar.com")
      @primary_email_identity.expects(:try).with(:destroy)
      @new_email_identity.send(:make_primary_and_destroy_former!)
    end
  end

  describe "#mailer_daemon?" do
    let!(:identity) { user.identities.first }

    describe "for development and testing" do
      it { refute identity.mailer_daemon? }
    end

    describe 'on master, staging, canary and production' do
      before { UserEmailIdentity.any_instance.stubs(:allow_invalid_addresses?).returns(false) }

      [
        "nobody@something.com",
        "nobody@otherexample.com",
        "nobody@mailer_daemon.com"
      ].each do |email|
        describe "when email is valid, like #{email}" do
          before { identity.value = email }
          it { refute identity.mailer_daemon? }
        end
      end

      [
        "nobody@example.com",
        "nobody@subdomain.example.com"
      ].each do |email|
        describe "when email is reserved_example, like #{email}" do
          before { identity.value = email }
          it { refute identity.mailer_daemon? }
        end
      end

      describe "when email is mailer-daemon" do
        before { identity.value = "mailer-daemon@anywhere.com" }
        it { assert identity.mailer_daemon? }
      end
    end
  end

  describe "#reserved_example?" do
    let!(:identity) { user.identities.first }

    describe "for development and testing" do
      it { refute identity.reserved_example? }
    end

    describe 'on master, staging, canary and production' do
      before { UserEmailIdentity.any_instance.stubs(:allow_invalid_addresses?).returns(false) }

      [
        "nobody@something.com",
        "nobody@otherexample.com"
      ].each do |email|
        describe "when email is valid, like #{email}" do
          before { identity.value = email }
          it { refute identity.reserved_example? }
        end
      end

      [
        "nobody@example.edu",
        "nobody@example.com",
        "nobody@subdomain.example.com"
      ].each do |email|
        describe "when email is reserved_example, like #{email}" do
          before { identity.value = email }
          it { assert identity.reserved_example? }
        end
      end

      describe "when email is mailer-daemon" do
        before { identity.value = "mailer-daemon@anywhere.com" }
        it { refute identity.reserved_example? }
      end
    end
  end

  describe "creating a new user" do
    before do
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
    end

    let(:user) do
      accounts(:minimum).users.new(name: 'Mick', email: 'mick@zendesk.com') do |user|
        user.password = '123456'
      end
    end

    describe "with skip_verification" do
      before do
        user.skip_verification = true
        user.save!
      end

      it "creates an identity which is verified" do
        assert_equal(user.identities.size, 1)
        assert(user.identities.first.is_verified?)
      end

      it "does not send a verification email" do
        assert_equal(0, ActionMailer::Base.deliveries.size)
      end

      it "does not send a verification email when send verify email is enabled" do
        user.send_verify_email = true

        refute_difference 'ActionMailer::Base.deliveries.size' do
          user.save!
        end
      end
    end

    describe "without skip_verification" do
      # NOTE this does not happen on accounts with web_portal_state as disabled
      before { user.skip_verification = false }

      it "creates an identity which is not verified" do
        user.save!
        assert_equal(user.identities.size, 1)
        assert(!user.identities.first.is_verified?)
      end

      describe "with send_verify_email" do
        before { user.send_verify_email = true }

        it "does not send a verification email" do
          refute_difference 'ActionMailer::Base.deliveries.size' do
            user.save!
          end
        end

        it "does not send a verification email if help_center is not enabled" do
          Zendesk::UserPortalState.any_instance.stubs(:send_initial_identity_verification_email?).returns(false)
          refute_difference 'ActionMailer::Base.deliveries.size' do
            user.save!
          end
        end
      end

      it "does not send a verification email" do
        refute_difference 'ActionMailer::Base.deliveries.size' do
          user.save!
        end
      end

      describe "Via Google App Marketplace" do
        before do
          user.via = Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET
        end

        describe 'if the account has no HC' do
          let(:google_app_market_user) do
            accounts(:minimum).users.new(name: 'Nick', email: 'nick@zendesk.com') do |user|
              user.password = '123456'
              user.via = Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET
            end
          end

          before do
            account.disable_help_center!
          end

          it "does not send a google specific verification email" do
            google_app_market_user.send_verify_email = true
            refute_difference 'ActionMailer::Base.deliveries.size' do
              google_app_market_user.save!
            end
          end
        end

        describe 'if the account has an HC' do
          before { user.account.enable_help_center! }

          it "sends a google specific verification email" do
            user.send_verify_email = true
            assert_difference 'ActionMailer::Base.deliveries.size' do
              user.save!
            end
          end
        end
      end
    end
  end

  describe ".create_or_assume_identity" do
    describe "when the email identity already exists on the user's account" do
      let(:user) { users(:minimum_agent) }
      let(:other_user) { users(:minimum_end_user) }

      before do
        @other_identity = UserEmailIdentity.create!(value: "wriggles@foo.com", user: other_user)
        @identity = UserEmailIdentity.create_or_assume_identity(user: user, email: "wriggles@foo.com")
      end

      it "returns the existing identity" do
        assert_equal @other_identity, @identity
      end

      it "transfers ownership of the identity to the specified user" do
        assert_equal user, @identity.user
      end
    end

    describe "when the email identity already exists on another account" do
      let(:user) { users(:minimum_agent) }
      let(:other_user) { users(:with_groups_end_user) }

      before do
        @other_account  = accounts(:with_groups)
        @other_identity = UserEmailIdentity.create!(value: "wriggles@foo.com", user: other_user)
        @new_identity   = UserEmailIdentity.create_or_assume_identity(user: user, email: "wriggles@foo.com")
        @other_identity.reload
      end

      it "does not assume the existing identity" do
        assert_equal other_user, @other_identity.user
        assert_equal @other_account, @other_identity.account
      end

      it "creates the new identity on the user's account" do
        assert_equal account, @new_identity.account
      end

      it "sets the new user as the identity owner" do
        assert_equal user, @new_identity.user
      end

      it "uses the same email address" do
        assert_equal "wriggles@foo.com", @new_identity.value
      end
    end

    describe "when the email identity does not exist on any account" do
      let!(:user) { users(:minimum_end_user) }
      let!(:identity) { UserEmailIdentity.create_or_assume_identity(user: user, email: "doesnotexist@example.com") }

      should_create :user_email_identity

      it "sets the specified user as the identity's owner" do
        assert_equal user, identity.user
      end

      it "stores the email address in the identity" do
        assert_equal "doesnotexist@example.com", identity.value
      end
    end
  end

  describe "#set_primary_when_verifying!" do
    let(:identity) { user_identities(:deliverable_identity) }

    it "flags it as primary" do
      refute identity.instance_variable_get(:@make_primary)
      identity.set_primary_when_verifying!
      assert identity.instance_variable_get(:@make_primary)
    end
  end

  describe "json" do
    describe "default" do
      it "has an email identity_type" do
        identity = UserEmailIdentity.new(value: "user@gmail.com")
        attributes = JSON.parse(identity.to_json)

        assert_equal('email', attributes['identity_type'])
      end

      it "has a google identity_type" do
        identity = UserEmailIdentity.new(value: "user@gmail.com")
        identity.build_google_profile
        attributes = JSON.parse(identity.to_json)

        assert_equal('google', attributes['identity_type'])
      end
    end

    describe "v2" do
      it "has an email identity_type" do
        identity = UserEmailIdentity.new(value: "user@gmail.com")
        attributes = JSON.parse(identity.to_json(version: 2))

        assert_equal('email', attributes['type'])
      end

      it "has a google identity_type" do
        identity = UserEmailIdentity.new(value: "user@gmail.com")
        identity.build_google_profile
        attributes = JSON.parse(identity.to_json(version: 2))

        assert_equal('google', attributes['type'])
      end
    end
  end

  describe "changing the identity's value" do
    let(:user) { account.owner }

    describe "when the user's has a zopim identity" do
      let(:zopim_identity) { stub }
      let(:identity) { user.identities.first }

      before { user.stubs(zopim_identity: zopim_identity) }

      describe "and it is the primary identity" do
        before do
          user.stubs(:zopim_email_format_for_value).
            with('acme@example.com').
            returns('acme+123@example.com')
          identity.stubs(primary?: true)
        end

        it "updates the zopim_identity's email address field" do
          zopim_identity.expects(:update_attribute).
            with(:email, 'acme+123@example.com').once
          identity.update_attribute(:value, 'acme@example.com')
        end
      end

      describe "and it is not the primary identity" do
        before { identity.stubs(primary?: false) }

        it "does not update the zopim_identity's email address field" do
          zopim_identity.expects(:update_attribute).never
          identity.update_attribute(:value, 'acme@example.com')
        end
      end
    end
  end

  describe "when the user has verification tokens" do
    def assert_no_verification_tokens_for(user)
      assert_empty user.verification_tokens
      assert_empty user.identities.map(&:verification_tokens).flatten
    end

    before do
      user = users(:minimum_admin)
      UserEmailIdentity.create!(account: user.account, user: user, value: 'foobar@bar.com')
      user.reload
      user.verification_tokens.create!
      user.identities.first.verification_tokens.create!
    end

    describe "when the primary email is updated" do
      before { user.identities.first.update_attributes!(value: 'foobar@gmail.com') }

      it "should delete all verification token for that user" do
        assert_no_verification_tokens_for(user)
      end
    end

    describe "when an identity is updated" do
      it "should not delete all verification token for that user" do
        assert_no_difference 'user.account.verification_tokens.count(:all)' do
          user.identities.first.save!
        end
      end
    end

    describe "when a new identity is added" do
      it "does not delete verification tokens" do
        assert_no_difference 'user.account.verification_tokens.count(:all)' do
          UserEmailIdentity.create!(account: user.account, user: user, value: 'foobar@foo.com')
        end
      end
    end

    describe "when a secondary email is updated" do
      before { user.identities.last.update_attributes!(value: 'foobar2@gmail.com') }

      it "should delete all verification token for that user" do
        assert_no_verification_tokens_for(user)
      end
    end

    describe "when the primary email is deleted" do
      before { assert user.identities.first.destroy, 'could not delete identity' }

      it "should delete all verification token for that user" do
        assert_no_verification_tokens_for(user)
      end
    end

    describe "when a secondary email is deleted" do
      before { assert user.identities.last.destroy, 'could not delete identity' }

      it "should delete all verification token for that user" do
        assert_no_verification_tokens_for(user)
      end
    end
  end

  describe "when a user identity has unverified ticket creations" do
    let(:user) { users(:minimum_end_user) }
    let(:user_identity) { user_identities(:minimum_end_user) }
    let(:user_identity2) { user_identities(:deliverable_identity) }
    let(:ticket1) { tickets(:minimum_1) }
    let(:ticket2) { tickets(:minimum_2) }
    let(:ticket3) { tickets(:minimum_3) }
    before do
      user_identity.update_attribute(:is_verified, false)
      user_identity2.update_attribute(:is_verified, false)
      ticket1.create_unverified_creation(
        account: account,
        user: user,
        user_identity: user_identity,
        from_address: user_identity.value
      )
      ticket2.create_unverified_creation(
        account: account,
        user: user,
        user_identity: user_identity,
        from_address: user_identity.value
      )
      ticket3.create_unverified_creation(
        account: account,
        user: user,
        user_identity: user_identity2,
        from_address: user_identity2.value
      )
    end

    describe "when the identity is verified" do
      before { user_identity.update_attribute(:is_verified, true) }

      it "should delete all unverified_ticket_creations belonging to the identity" do
        assert_equal 1, user.unverified_ticket_creations.count
        assert_nil user.unverified_ticket_creations.find_by_from_address(user_identity.value)
      end

      it "does not delete unverified_ticket_creations created by different user identities belonging to the same user" do
        assert_equal 1, user.unverified_ticket_creations.count
        assert_equal user_identity2, user.unverified_ticket_creations.find_by_from_address(user_identity2.value).user_identity
      end
    end

    describe "when the user deletes the identity used to create a ticket and then creates a new identity with the same address and verifies it" do
      it "deletes the unverified_ticket_creations belonging to that identity" do
        assert_equal 'minimum_end_user@aghassipour.com', user_identity.value
        assert_equal 2, user.unverified_ticket_creations.where(from_address: user_identity.value).count
        user_identity.destroy
        assert(user_identity.destroyed?)

        new_identity = UserEmailIdentity.create!(account: user.account, user: user, value: 'minimum_end_user@aghassipour.com', is_verified: false)
        new_identity.update_attribute(:is_verified, true)
        assert_nil user.unverified_ticket_creations.find_by_from_address(new_identity.value)
      end
    end

    it "is not called on update if verified is not changed" do
      user_identity.expects(:delete_unverified_ticket_creations).never
      user_identity.update_attribute(:priority, 3)
    end
  end

  describe "trigger updated zopim email callbacks" do
    let(:the_user) do
      users(:minimum_end_user)
    end

    let(:new_identity) do
      UserEmailIdentity.create!(user: the_user, value: "my.new.email@zendesk.com")
    end

    it "callbacks to update zopim email" do
      new_identity.expects(:update_zopim_identity_email)
      new_identity.make_primary!
    end
  end

  describe '#deliverable_state_name' do
    it "returns the deliverable state name" do
      assert_equal 'deliverable', user_identities(:deliverable_identity).deliverable_state_name
      assert_equal 'mailer_daemon', user_identities(:mailer_daemon_identity).deliverable_state_name
      assert_equal 'reserved_example', user_identities(:reserved_example_identity).deliverable_state_name
    end

    describe 'having an invalid deliverable_state' do
      before { user_identity.stubs(deliverable_state: nil) }
      it { assert_equal 'unknown', user_identity.deliverable_state_name }
    end
  end

  describe 'deliverable states' do
    {
      ticket_sharing_partner: DeliverableStateType.TICKET_SHARING_PARTNER,
      mailing_list:           DeliverableStateType.MAILING_LIST,
      reserved_example:       DeliverableStateType.RESERVED_EXAMPLE,
      mailer_daemon:          DeliverableStateType.MAILER_DAEMON,
      undeliverable:          DeliverableStateType.UNDELIVERABLE,
      machine:                DeliverableStateType.MACHINE
    }.each do |state, deliverable_state|
      describe "#mark_as_#{state}" do
        it "marks the deliverable state as '#{state}'" do
          user_identity.expects(:mark_deliverable_state).with(deliverable_state)
          user_identity.send("mark_as_#{state}".to_sym)
        end
      end
    end

    describe "#decrement_undeliverable_count!" do
      it "does not do anything if the count is already 0" do
        user_identity.update_attribute(:undeliverable_count, 0)
        user_identity.expects(:decrement!).never
        user_identity.send(:decrement_undeliverable_count!)
        assert_equal 0, user_identity.reload.undeliverable_count
      end

      it "decrements the undeliverable_count if the count is greater than 0" do
        user_identity.update_attribute(:undeliverable_count, 1)
        user_identity.send(:decrement_undeliverable_count!)
        assert_equal 0, user_identity.reload.undeliverable_count
      end

      it "sets the deliverable_state to DELIVERABLE if the count has been decremented below the max" do
        user_identity.update_attribute(:undeliverable_count, Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT)
        user_identity.update_attribute(:deliverable_state, DeliverableStateType.UNDELIVERABLE)
        user_identity.expects(:mark_deliverable_state)
        user_identity.send(:decrement_undeliverable_count!)
        assert_equal Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT - 1, user_identity.reload.undeliverable_count
      end
    end

    describe "#mark_deliverable" do
      let(:starting_state) { DeliverableStateType.UNDELIVERABLE }

      before do
        user_identity.update_attribute(:deliverable_state, starting_state)
      end

      describe "when made undeliverable due to soft bounces reaching the max" do
        before do
          user_identity.update_attribute(:undeliverable_count, Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT)
        end

        it "decrements undeliverable_count" do
          user_identity.expects(:decrement_undeliverable_count!)
          user_identity.mark_deliverable
        end

        it "marks deliverable_state deliverable" do
          user_identity.mark_deliverable
          assert_equal DeliverableStateType.DELIVERABLE, user_identity.reload.deliverable_state
        end
      end

      describe "when made undeliverable due to a hard bounce" do
        it "does not attempt to decrement the undeliverable_count" do
          user_identity.expects(:decrement_undeliverable_count!).never
          user_identity.mark_deliverable
        end

        it "marks deliverable_state deliverable" do
          user_identity.mark_deliverable
          assert_equal DeliverableStateType.DELIVERABLE, user_identity.reload.deliverable_state
        end
      end

      describe "if the deliverable_state is something other than UNDELIVERABLE" do
        let(:starting_state) { DeliverableStateType.TICKET_SHARING_PARTNER }

        it "does not do anything" do
          user_identity.expects(:reached_undeliverable_count_max?).never
          user_identity.mark_deliverable
          assert_equal starting_state, user_identity.reload.deliverable_state
        end
      end
    end
  end

  describe '#invalid_recipient?' do
    let(:identity) { user_identities(:deliverable_identity) }
    before { identity.stubs(allow_invalid_addresses?: false) }

    it "is true for mailer-daemon and reserved example addresses only" do
      identity.stubs(value: 'admin@mailer-daemon.com')
      assert identity.invalid_recipient?

      identity.stubs(value: 'test@example.com')
      assert identity.invalid_recipient?

      identity.stubs(value: 'test@real.com')
      refute identity.invalid_recipient?
    end
  end

  describe "when an account owner's email is updated" do
    describe "when the account is a trial account" do
      before do
        @owner = accounts(:minimum).owner
        @identity = UserEmailIdentity.create(user: @owner, value: 'test@somewhere.com')
        @identity.account.stubs(:is_trial?).returns(true)
      end

      describe_with_arturo_enabled :orca_classic_fraud_score_job_update_owner_email do
        it "enqueues a fraud_score_job" do
          FraudScoreJob.expects(:enqueue_account).with(@owner.account, Fraud::SourceEvent::UPDATE_OWNER_EMAIL).once
          @identity.update_attribute(:value, 'updated@baddomain.com')
        end
      end

      describe_with_arturo_disabled :orca_classic_fraud_score_job_update_owner_email do
        it "does not enqueue a fraud_score_job" do
          FraudScoreJob.expects(:enqueue_account).never
          @identity.update_attribute(:value, 'updated@baddomain.com')
        end
      end
    end

    describe "when the account is not a trial account" do
      before do
        @owner = accounts(:minimum).owner
        @identity = UserEmailIdentity.create(user: @owner, value: 'test@somewhere.com')
        @identity.account.stubs(:is_trial?).returns(false)
      end

      describe_with_and_without_arturo_enabled :orca_classic_fraud_score_job_update_owner_email do
        it "does not enqueue a fraud_score_job" do
          FraudScoreJob.expects(:enqueue_account).never
          @identity.update_attribute(:value, 'updated@baddomain.com')
        end
      end
    end
  end

  describe "when a non-owner user's email is updated" do
    before do
      @user = users(:minimum_agent)
      @identity = UserEmailIdentity.create(user: @user, value: 'test@somewhere.com')
    end

    describe_with_and_without_arturo_enabled :orca_classic_fraud_score_job_update_owner_email do
      it "does not enqueue a fraud_score_job" do
        FraudScoreJob.expects(:enqueue_account).never
        @identity.update_attribute(:value, 'updated@baddomain.com')
      end
    end
  end
end
