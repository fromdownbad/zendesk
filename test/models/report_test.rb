require_relative "../support/test_helper"

SingleCov.covered! uncovered: 17

describe Report do
  fixtures :accounts, :users, :user_identities, :groups, :tickets, :integers, :memberships, :ticket_fields, :custom_field_options, :subscriptions, :ticket_forms
  should_serialize :definition
  should_serialize :result

  before do
    Account.any_instance.stubs(:extended_ticket_metrics_visible?).returns(true)
  end

  describe "Reports" do
    before do
      @account = accounts(:support)
      Time.zone = @account.time_zone

      i = 0
      @interval = 30
      @days     = []
      @day_of_report = []
      beginning_of_today = Time.now.in_time_zone.beginning_of_day
      @account.tickets.each do |ticket|
        ticket.created_at = beginning_of_today - @interval * 60 * 60 * 24
        ticket.solved_at  = beginning_of_today - (@interval - i) * 60 * 60 * 24
        ticket.save(validate: false)
        @days[i] = (beginning_of_today - (@interval - i) * 60 * 60 * 24).to_date
        @day_of_report << i
        i += 1
      end

      @params = {
        sets: {
          "1" => {
            conditions: {
              "1" => { operator: "less_than", value: {"0" => "2"}, source: "status_id"},
              "2" => { operator: "is", value: {"0" => @account.agents.first.id.to_s}, source: "assignee_id"}
            },
            legend: 'Rocco',
            state: 'working'
          }
        }
      }
    end

    should_validate_presence_of :title, :author

    it "returns an empty array when :definition is nil" do
      assert_equal [], Report.new.sets
    end

    it "works with satisfaction score" do
      sets = {
        "rb_0.439427061158949" => {
          conditions: {
            "1" => {value: {"0" => "2"}, operator: "greater_than", source: "priority_id"},
            "2" => {value: {"0" => "0"}, operator: "greater_than", source: "satisfaction_score"}
          },
          legend: "High and Urgent",
          state: "any"
        },
        "rb_0.611509575896922" => {
          conditions: {
            "1" => {value: {"0" => "3"}, operator: "less_than", source: "priority_id"}
          },
          legend: "Low and Normal",
          state: "any"
        }
      }
      report = Report.new(relative_interval_in_days: 0)
      report.sets = sets

      expected_definition = [
        {
          data: [["priority_id", ">", "2"], ["satisfaction_score", ">", "0"]],
          legend: {color: "FF0D00", name: "High and Urgent"},
          state: "any"
        },
        {
          data: [["priority_id", "<", "3"]],
          legend: {color: "98C332", name: "Low and Normal"},
          state: "any"
        }
      ]

      assert_equal expected_definition, report.definition
    end

    describe "definition items" do
      it "provides all data as definition items" do
        report = Report.new
        sets   = [
          {
            data: [["organization_id", "=", "2"]],
            state: "working",
            legend: { name: "Backlog", color: "FF0D00"}
          },
          {
            data: [["name", "=", "roger rabbit"], ["type", "<", "second condition"]],
            state: "created_at",
            legend: { name: "New tickets", color: "98C332"}
          },
          {
            data: [],
            state: "solved_at",
            legend: { name: "Solved tickets", color: "96DEED"}
          }
        ]
        report.stubs(:sets).returns(sets)

        assert_equal 3, report.items.size
        report.items.each do |definition_item|
          assert ['organization_id', 'name', 'type'].include?(definition_item.source), "Not found #{definition_item}"
        end
      end
    end

    describe "#add_conditions" do
      before { @report = @account.reports.new }

      it "includes null values for != operator" do
        assert_equal "AND (tickets.organization_id != '2' OR tickets.organization_id IS NULL) ", @report.add_conditions([["organization_id", "!=", "2"]])
      end

      it "treats current_tags in a special way" do
        assert_equal "AND IFNULL(tickets.current_tags, '') includes '(^| )(test1|test2|test3)($| )' != 0 ", @report.add_conditions([["current_tags", "includes", "test1 test2 test3"]])
      end

      it "includes values with given value for = operator" do
        assert_equal "AND tickets.organization_id = '2' ", @report.add_conditions([["organization_id", "=", "2"]])
      end

      it "takes created_at, updated_at and id from tickets and not ticket metric sets" do
        assert_equal "AND tickets.created_at = '2' ", @report.add_conditions([["created_at", "=", "2"]])
        assert_equal "AND tickets.updated_at = '2' ", @report.add_conditions([["updated_at", "=", "2"]])
        assert_equal "AND tickets.id = '2' ", @report.add_conditions([["id", "=", "2"]])
      end

      describe "locale_id" do
        describe 'is default account language' do
          before { @id = @report.account.translation_locale.id }

          it 'includes values for locale_id' do
            assert_equal "AND (tickets.locale_id = '#{@id}' OR tickets.locale_id IS NULL) ", @report.add_conditions([["locale_id", "=", @id.to_s]])
            assert_equal "AND tickets.locale_id != '#{@id}' AND tickets.locale_id IS NOT NULL ", @report.add_conditions([["locale_id", "!=", @id.to_s]])
          end
        end

        describe 'is not default account language' do
          before do
            id = @report.account.translation_locale.id
            @id = id.to_i + 1
          end

          it 'includes values for locale_id' do
            assert_equal "AND tickets.locale_id = '#{@id}' ", @report.add_conditions([["locale_id", "=", @id.to_s]])
            assert_equal "AND (tickets.locale_id != '#{@id}' OR tickets.locale_id IS NULL) ", @report.add_conditions([["locale_id", "!=", @id.to_s]])
          end
        end
      end

      describe "ticket_form_id" do
        before { @report.account.ticket_forms << ticket_forms(:default_ticket_form) }

        describe "is default account form" do
          before { @id = @report.account.ticket_forms.default.first.id }

          it 'includes values for ticket_form_id' do
            assert_equal "AND (tickets.ticket_form_id = '#{@id}' OR tickets.ticket_form_id IS NULL) ", @report.add_conditions([["ticket_form_id", "=", @id.to_s]])
            assert_equal "AND tickets.ticket_form_id != '#{@id}' AND tickets.ticket_form_id IS NOT NULL ", @report.add_conditions([["ticket_form_id", "!=", @id.to_s]])
          end
        end

        describe "is the default account form" do
          before { @id = @report.account.ticket_forms.default.first.id + 1 }

          it 'includes values for ticket_form_id' do
            assert_equal "AND tickets.ticket_form_id = '#{@id}' ", @report.add_conditions([["ticket_form_id", "=", @id.to_s]])
            assert_equal "AND (tickets.ticket_form_id != '#{@id}' OR tickets.ticket_form_id IS NULL) ", @report.add_conditions([["ticket_form_id", "!=", @id.to_s]])
          end
        end
      end

      it "Treats custom fields (tagger) nice" do
        assert_equal "AND IFNULL(current_tags, '') REGEXP '(^| )not_bad($| )' != 0 ",
          @report.add_conditions([["ticket_fields_#{ticket_fields(:field_tagger_custom).id}", "REGEXP", custom_field_options(:field_tagger_custom_option_2).id.to_s]])
      end

      it "Treats custom fields (checkbox) nice" do
        assert_equal "AND IFNULL(current_tags, '') REGEXP '(^| )im_set($| )' != 0 ",
          @report.add_conditions([["ticket_fields_#{ticket_fields(:field_checkbox_custom).id}", "REGEXP", "true"]])
      end

      it "includes ticket_metrics_sets table for metrics fields and convert to minutes depending on the field name" do
        TicketMetricSet.attributes_for_reports.each do |attribute|
          value = attribute.include?("in_minutes") ? "120" : "2"
          assert_equal "AND ticket_metric_sets.#{attribute} = '#{value}' ", @report.add_conditions([[attribute, "=", "2"]])
        end
      end

      it "ignores ticket metrics conditions if not enabled in that account" do
        Account.any_instance.stubs(:extended_ticket_metrics_visible?).returns(false)
        TicketMetricSet.attributes_for_reports.each do |attribute|
          conditions = [[attribute, "=", "2"], ["organization_id", "=", "2"]]
          assert_equal "AND tickets.organization_id = '2' ", @report.add_conditions(conditions)
        end
      end
    end

    describe "#from_statement" do
      before { @report = @account.reports.new }

      it "returns only the tickets table when there are no conditions" do
        @report.from_statement([]).must_include "FROM tickets"
      end

      it "returns only the tickets table when there are no ticket metrics conditions" do
        @report.from_statement([["organization_id", "=", "2"]]).must_include "FROM tickets"
      end

      it "returns an inner join with the ticket metrics table when there are ticket metrics conditions" do
        @report.from_statement([["replies", "=", "2"]]).must_include "FROM tickets INNER JOIN ticket_metric_sets"
      end

      it "returns only the tickets table when ticket metrics are disabled" do
        Account.any_instance.stubs(:extended_ticket_metrics_visible?).returns(false)
        @report.from_statement([["replies", "=", "2"]]).must_include "FROM tickets"
      end
    end

    it "parses fields" do
      # Define a public method to call the private method in order to test this
      r = Report.new(account: @account)
      def r.public_parse_definition(args)
        parse_definition(args)
      end

      assert_equal [{
        state: 'working',
        legend: { name: 'Rocco', color: 'FF0D00' },
        data: [["status_id", "<", '2'], ["assignee_id", "=", @account.agents.first.id.to_s]]
      }], r.public_parse_definition(@params[:sets])

      r = @account.reports.build
      r.sets = @params[:sets]

      assert_equal [{
        state: 'working',
        legend: { name: 'Rocco', color: 'FF0D00' },
        data: [["status_id", "<", '2'], ["assignee_id", "=", @account.agents.first.id.to_s]]
      }], r.sets
    end

    it "parses priority_id" do
      mapping = Zendesk::Query::Fields.lookup('priority_id')
      assert_equal ['priority_id', '=', '2'], mapping.parse('is', '2', mapping.legal_values)
    end

    it "parses via_id" do
      mapping = Zendesk::Query::Fields.lookup('via_id')
      assert_equal ['via_id', '=', '0'], mapping.parse('is', '0', mapping.legal_values)
    end

    it "parses ticket_form_id" do
      @ticket_forms = [stub(id: 2, name: 'wombat form'), stub(id: 5, name: 'giraffe form')]
      @account.stubs(:ticket_forms).returns(@ticket_forms)

      mapping = Zendesk::Query::Fields.lookup('ticket_form_id')
      assert_equal ['ticket_form_id', '=', '2'], mapping.parse('is', '2', mapping.legal_values(@account))
    end

    it "parses brand_id" do
      @brands = stub(active: [stub(id: 2, name: 'foo'), stub(id: 5, name: 'bar')])
      @account.stubs(:brands).returns(@brands)

      mapping = Zendesk::Query::Fields.lookup('brand_id')
      assert_equal ['brand_id', '=', '2'], mapping.parse('is', '2', mapping.legal_values(@account))
    end

    describe "parse locale_id" do
      before do
        @languages = [stub(id: 1, locale: "en-US"), stub(id: 2, locale: "pt-BR")]
        @account.stubs(:available_languages).returns(@languages)
        @locale_field = Zendesk::Query::Fields.lookup('locale_id')
      end

      it 'maps accepted values' do
        assert_equal ['locale_id', '=', '2'], @locale_field.parse('is', '2', @locale_field.legal_values(@account))
      end

      it "gives exception if language isn't an available language" do
        assert_raises IllegalMappingException do
          @locale_field.parse('is', '3', @locale_field.legal_values(@account))
        end
      end
    end

    it "parses ticket metrics fields" do
      TicketMetricSet.attributes_for_reports.each do |attribute|
        field = Zendesk::Query::Fields.lookup(attribute)
        assert_equal [attribute, '=', '2'], field.parse('is', '2', field.legal_values)
      end
    end

    it "parses custom fields" do
      (@account.field_taggers.active + @account.field_checkboxes.active).each do |field|
        identifier = "ticket_fields_#{field.id}"
        assert_equal identifier, Zendesk::Query::Fields.lookup(identifier, @account).identifier
      end
    end

    it "raises an illegal mapping exception with an illegal value" do
      mapping = Zendesk::Query::Fields.lookup('priority_id')
      assert_equal ['priority_id', '=', '2'], mapping.parse('is', '2', mapping.legal_values(@account))
      assert_raise(IllegalMappingException) { mapping.parse('is', '-1', mapping.legal_values(@account)) }
    end

    it "basics report execution" do
      report = @account.reports.build
      report.sets = @params[:sets]
      report.execute
      assert report.table.is_a?(Hash)
      assert report.table[:data].is_a?(Hash)
      assert report.table[:legends].is_a?(Array)
    end

    it "stores the result and last run when execute is performed on a saved report" do
      params = { sets: { "1" => { conditions: {}, legend: 'Working tickets', state: 'working' } } }

      report = Report.create!(
        title: "report title",
        author: @account.agents.first,
        relative_interval_in_days: 30,
        sets: params[:sets],
        account: @account
      )
      report.execute
      assert report.reload.result.present?
      assert report.reload.last_run_at
    end
  end

  describe "Modifying the global timezone" do
    before do
      @account = accounts(:minimum)
      @account.update_attribute :time_zone, "Pacific Time (US & Canada)"
      Time.zone = "Pacific Time (US & Canada)"
      Timecop.freeze(Time.parse("2010-03-13 23:59:00"))

      @account.tickets.update_all(created_at: 15.days.ago, solved_at: 12.days.ago)

      @old_report = build_report
    end

    it "does not affect stat counts" do
      Time.zone   = "Tbilisi"
      @new_report = build_report

      expected    = @old_report.result[:data][Date.parse('2010-02-26')]["Working tickets"]
      actual      = @new_report.result[:data][Date.parse('2010-02-26')]["Working tickets"]

      assert_equal 6, expected
      assert_equal expected, actual
    end
  end

  describe "Time periods" do
    before do
      # Use non-local time to verify assumptions, change to Pacific Time if Zendesk moves back to Boston ;)
      Time.zone = "Eastern Time (US & Canada)"
      Timecop.freeze(Time.parse("2012-04-01 04:00:00 UT"))
      @account = accounts(:support)
      @report  = build_report
    end

    describe "using relative interval" do
      before do
        assert @report.is_relative_interval?
      end

      it "returns the right relative from" do
        assert_equal "2012-03-02", @report.from.to_date.to_s(:db)
      end

      it "returns the right relative to" do
        assert_equal "2012-04-01", @report.to.to_date.to_s(:db)
      end

      it "returns the right converted_end_of_day_sequence" do
        assert_equal "CONVERT_TZ(ADDDATE('2012-03-02 23:59:59', i), 'America/New_York', '+00:00')",
          @report.send(:converted_end_of_day_sequence)
      end
    end

    describe "using specific dates" do
      before do
        Timecop.return
        @report.is_relative_interval = false
        @report.from_date = '2012-03-01'
        @report.to_date   = '2012-04-01'
        refute @report.is_relative_interval?
      end

      it "returns the right relative from" do
        assert_equal "2012-03-01", @report.from.to_date.to_s(:db)
      end

      it "returns the right relative to" do
        assert_equal "2012-04-01", @report.to.to_date.to_s(:db)
      end

      it "returns the right converted_end_of_day_sequence" do
        assert_equal "CONVERT_TZ(ADDDATE('2012-03-01 23:59:59', i), 'America/New_York', '+00:00')",
          @report.send(:converted_end_of_day_sequence)
      end
    end
  end

  [:created_at, :solved_at].each do |state|
    describe "#event_query" do
      before do
        @account = accounts(:minimum)
        @account.stubs(:extended_ticket_metrics_visible?).returns(false)
        @report = Report.new(account: @account, is_relative_interval: false)
        @now = 0.days.ago
        @early_range = @now - 28.days # would go right up to 30 if I was sure the timezone stuff wouldn't bite me
        @late_range = @now - 2.days
        @tickets = @account.tickets
        @mysql_date_fmt = "%a, %d %b %Y"

        # remove all tickets from consideration
        @tickets.update_all(created_at: @now - 60.days, solved_at: @now - 60.days, updated_at: @now - 60.days)
        # put a ticket in time range, but deleted
        ActiveRecord::Base.connection.execute("update tickets set #{state} = '#{@early_range.to_s(:db)}', updated_at = '#{@early_range.to_s(:db)}', status_id = 5 where id = #{@tickets[0].id}")
        # put a ticket at beginning of time range (with some buffer for timezones)
        ActiveRecord::Base.connection.execute("update tickets set #{state} = '#{@early_range.to_s(:db)}', updated_at = '#{@early_range.to_s(:db)}', status_id = 1 where id = #{@tickets[1].id}")
        # put a ticket at end of time range
        ActiveRecord::Base.connection.execute("update tickets set #{state} = '#{@late_range.to_s(:db)}', updated_at = '#{@late_range.to_s(:db)}', status_id = 1 where id = #{@tickets[2].id}")
      end

      it "fors each date, find the tickets #{state} in the date range" do
        @report.from_date = (@now - 30.days).to_s(:db)
        @report.to_date   = @now.to_s(:db)

        query   = @report.event_query([], state)
        records = ActiveRecord::Base.connection.select_rows(query)
        assert_equal "[[#{@early_range.strftime(@mysql_date_fmt)}, 1], [#{@late_range.strftime(@mysql_date_fmt)}, 1]]", records.inspect.to_s
      end
    end
  end

  describe "#any_closed_query" do
    before do
      @account = accounts(:minimum)
      @account.stubs(:extended_ticket_metrics_visible?).returns(false)
      @report = Report.new(account: @account, is_relative_interval: false)
      @now    = 0.days.ago

      @account.tickets.update_all(solved_at: @now - 10.days)
      @account.tickets.limit(3).update_all(updated_at: @now - 31.days)
    end

    it "fors each date, find the tickets whose solved_at was on or after that date, including those updated before the from date" do
      @report.from_date = (@now - 30.days).to_s(:db)
      @report.to_date   = @now.to_s(:db)

      query   = @report.any_closed_query([])
      records = ActiveRecord::Base.connection.select_rows(query)
      data    = {}

      records.each do |row|
        date  = row.first
        date  = Date.parse(date) if date.is_a?(String)
        count = row.last.to_i
        data[date] = count
      end

      date_before      = Date.parse((@now - 11.days).to_s(:db))
      date_on_or_after = Date.parse((@now - 10.days).to_s(:db))

      assert_nil data[date_before]
      assert_equal(6, data[date_on_or_after])
    end
  end

  private

  def build_report
    params = { sets: { "1" => { conditions: {}, legend: 'Working tickets', state: 'working' } } }
    report = Report.create!(
      account: @account,
      author: @account.agents.first,
      title: "report title",
      sets: params[:sets],
      relative_interval_in_days: 30
    )
    report.execute
    report
  end
end
