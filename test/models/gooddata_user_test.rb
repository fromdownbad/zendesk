require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe GooddataUser do
  fixtures :users, :accounts

  before do
    @gooddata_user = GooddataUser.create! do |u|
      u.account = accounts(:minimum)
      u.user = users(:minimum_agent)
      u.gooddata_project_id = 'gooddata_project_id'
      u.gooddata_user_id = 'gooddata_user_id'
    end
  end

  describe ".for_user" do
    describe "when a GooddataUser exists for the user" do
      it "returns the GooddataUser" do
        assert_equal @gooddata_user, GooddataUser.for_user(@gooddata_user.user)
      end
    end

    describe "when a GooddataUser does not exist for the user" do
      it "returns nil" do
        assert_nil GooddataUser.for_user(users(:systemuser))
      end
    end
  end

  describe ".gooddata_login_for_user" do
    let(:user) { @gooddata_user.user }

    before do
      @prior_configuration = Rails.configuration.gooddata_login_domain

      Rails.configuration.gooddata_login_domain = 'test-domain.zendesk.com'
    end

    after do
      Rails.configuration.gooddata_login_domain = @prior_configuration
    end

    it "returns a login for the user with the correct domain" do
      assert_equal(
        "zendesk-gooddata+#{user.id}@test-domain.zendesk.com",
        GooddataUser.gooddata_login_for_user(user)
      )
    end
  end

  [:first_name, :last_name, :email].each do |attribute|
    describe "##{attribute}" do
      it "delegates to the user" do
        assert_equal(
          @gooddata_user.send(attribute),
          @gooddata_user.user.send(attribute)
        )
      end
    end
  end

  describe "#gooddata_login" do
    let(:user) { @gooddata_user.user }

    before do
      @prior_configuration = Rails.configuration.gooddata_login_domain

      Rails.configuration.gooddata_login_domain = 'test-domain.zendesk.com'
    end

    after do
      Rails.configuration.gooddata_login_domain = @prior_configuration
    end

    it "returns the gooddata login for the GoodData user" do
      assert_equal(
        "zendesk-gooddata+#{user.id}@test-domain.zendesk.com",
        @gooddata_user.gooddata_login
      )
    end
  end

  describe "#gooddata_role" do
    describe "when the user is the account owner" do
      before { @gooddata_user.user.account.owner_id = @gooddata_user.user.id }
      it "returns :editor" do
        assert_equal :editor, @gooddata_user.gooddata_role
      end
    end

    describe "when the user is an administrator" do
      before { @gooddata_user.user.roles = Role::ADMIN.id }
      it "returns :editor" do
        assert_equal :editor, @gooddata_user.gooddata_role
      end
    end

    describe "when the user is an agent" do
      before { @gooddata_user.user.roles = Role::AGENT.id }
      it "returns :editor" do
        assert_equal :editor, @gooddata_user.gooddata_role
      end
    end

    describe "when the user is a light agent" do
      before do
        user = @gooddata_user.user
        permission_set = PermissionSet.create_light_agent!(user.account)
        permission_set.permissions.report_access = 'none'
        user.roles = Role::AGENT.id
        user.account.stubs(:has_permission_sets?).returns(true)
        user.permission_set = permission_set
      end

      it "uses the permission set to determine access" do
        assert_equal :none, @gooddata_user.gooddata_role
      end
    end

    describe "when the user is an end user" do
      before { @gooddata_user.user.roles = Role::END_USER.id }
      it "returns :none" do
        assert_equal :none, @gooddata_user.gooddata_role
      end
    end

    describe "the user has a custom permission set" do
      let(:permission_set) { build_valid_permission_set }

      describe "the custom permission set has report access permission readonly" do
        before do
          permission_set.permissions.report_access = "readonly"
          @gooddata_user.user.permission_set = permission_set
          @gooddata_user.user.roles = Role::AGENT.id
          @gooddata_user.user.account.stubs(:has_permission_sets?).returns(true)
        end
        it "returns :read_only_user" do
          assert_equal :read_only_user, @gooddata_user.gooddata_role
        end
      end

      describe "the custom permission set has report access permission full" do
        before do
          permission_set.permissions.report_access = "full"
          @gooddata_user.user.permission_set = permission_set
          @gooddata_user.user.roles = Role::AGENT.id
          @gooddata_user.user.account.stubs(:has_permission_sets?).returns(true)
        end
        it "returns :editor" do
          assert_equal :editor, @gooddata_user.gooddata_role
        end
      end

      describe "the custom permission set has report access permission none" do
        before do
          permission_set.permissions.report_access = "none"
          @gooddata_user.user.permission_set = permission_set
          @gooddata_user.user.roles = Role::AGENT.id
          @gooddata_user.user.account.stubs(:has_permission_sets?).returns(true)
        end
        it "returns :none" do
          assert_equal :none, @gooddata_user.gooddata_role
        end
      end
    end
  end

  describe "#gooddata_role?" do
    before { @gooddata_user.stubs(:gooddata_role).returns(:editor) }

    it "returns true if the provided role matches" do
      assert @gooddata_user.gooddata_role?(:editor)
    end

    it "returns false if the provided role does not match" do
      refute @gooddata_user.gooddata_role?(:admin)
    end

    it "works if a string is provided" do
      assert @gooddata_user.gooddata_role?('editor')
    end
  end
end
