require_relative "../support/test_helper"

SingleCov.covered!

describe MobileSdkSettings do
  fixtures :accounts

  describe 'creation' do
    before do
      @account = accounts(:minimum)
      @auth_endpoint = "https://somesite.com/zd/auth"
      @auth_token = "aub7qw64ci7467c64738iubyret"
    end

    it 'fails to create without url and token' do
      record = @account.build_mobile_sdk_settings
      refute_valid record
      refute record.save

      assert record.errors[:auth_endpoint].present?, record.errors.inspect
      assert record.errors[:auth_token].present?,
        record.errors.inspect
    end

    it 'creates with no errors' do
      record = @account.build_mobile_sdk_settings(
        auth_endpoint: @auth_endpoint,
        auth_token: @auth_token
      )
      assert record.save!
    end
  end

  describe 'Mobile SDK Settings' do
    should_validate_presence_of :account, :auth_endpoint, :auth_token

    before do
      @sdk_config = FactoryBot.create(:mobile_sdk_settings)
      @sdk_config.auth_token
    end

    it 'does not update auth_token' do
      current_token = @sdk_config.auth_token
      assert_raise(StandardError) { @sdk_config.auth_token = "newtoken" }
      assert_equal current_token, @sdk_config.auth_token
    end

    it 'returns cropped token by default' do
      assert_equal @sdk_config.auth_token.length, 10
    end

    it 'returns a full token only on :full' do
      assert @sdk_config.auth_token(true).length > 10
    end
  end
end
