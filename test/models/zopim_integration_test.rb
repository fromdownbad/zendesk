require_relative '../support/test_helper'

SingleCov.covered! uncovered: 1

describe ZopimIntegration do
  fixtures :accounts

  should_validate_presence_of :account, :external_zopim_id, :zopim_key

  before do
    stub_request(:post, %r{embeddable/api/internal/config_sets.json})
    ::ChatAppInstallationJob.stubs(:enqueue)
    ZopimIntegration.any_instance.expects(:install_app).never
    ZopimIntegration.any_instance.stubs(uninstall_app: true)
    ZopimIntegration.any_instance.stubs(:ensure_not_phase_four)
  end

  describe 'uniqueness validation' do
    let(:account) { accounts(:minimum) }

    before do
      account.create_zopim_integration!(
        external_zopim_id: 1,
        zopim_key: 'zopim-key'
      )
    end

    should validate_uniqueness_of :external_zopim_id
    should validate_uniqueness_of :account_id
  end

  describe 'a zopim integration instance' do
    let(:zopim_integration) do
      ZopimIntegration.create!(
        account: account,
        external_zopim_id: 1,
        zopim_key: 'abc'
      )
    end

    describe '#phase_one?' do
      describe 'when account is associated with a zopim subscription' do
        let(:account) { accounts(:with_trial_zopim_subscription) }

        it 'returns false' do
          ZopimIntegration.any_instance.expects(:expire_cache_for_lotus).once
          refute zopim_integration.phase_one?
        end
      end

      describe 'when account is not associated with a zopim subscription' do
        let(:account) { accounts(:minimum) }

        it 'returns true' do
          assert zopim_integration.phase_one?
        end
      end
    end

    describe '#phase_two?' do
      describe 'when account is associated with a zopim subscription' do
        let(:account) { accounts(:with_trial_zopim_subscription) }

        it 'returns true' do
          assert zopim_integration.phase_two?
        end
      end

      describe 'when account is not associated with a zopim subscription' do
        let(:account) { accounts(:minimum) }

        it 'returns false' do
          refute zopim_integration.phase_two?
        end
      end
    end

    describe '#phase_three?' do
      let(:account) { accounts(:with_trial_zopim_subscription) }
      let(:zopim_integration) do
        account.create_zopim_integration!(
          external_zopim_id: 1,
          zopim_key: 'zopim-key',
          phase: 3
        )
      end

      it 'returns true' do
        assert zopim_integration.phase_three?
      end
    end

    describe '#has_agent_sync?' do
      let(:account) { accounts(:with_trial_zopim_subscription) }

      it 'resolves to #phase_two?' do
        actual   = zopim_integration.method(:has_agent_sync?)
        expected = zopim_integration.method(:phase_two?)
        assert_equal expected, actual
      end
    end

    describe '#has_billing?' do
      let(:account) { accounts(:with_trial_zopim_subscription) }

      it 'resolves to #phase_two?' do
        actual   = zopim_integration.method(:has_billing?)
        expected = zopim_integration.method(:phase_two?)
        assert_equal expected, actual
      end
    end
  end

  describe '#create' do
    let(:account) { accounts(:minimum) }

    it 'enqueues the chat app installation job and expires the cache' do
      ::ChatAppInstallationJob.expects(:enqueue).with(account.id).once
      ZopimIntegration.any_instance.expects(:expire_cache_for_lotus).once
      account.create_zopim_integration!(
        external_zopim_id: 1,
        zopim_key: 'zopim-key'
      )
    end

    it 'expires the cache when phase three integration' do
      ZopimIntegration.any_instance.expects(:expire_cache_for_lotus).once
      ZBC::Zuora::Synchronizer.any_instance.expects(:synchronize!).never
      account.create_zopim_integration!(
        external_zopim_id: 1,
        zopim_key: 'zopim-key',
        phase: 3
      )
    end

    describe 'when it is a phase one integration' do
      before do
        ZopimIntegration.any_instance.stubs(phase_one?: true)
      end

      it 'does not enqueue the embeddable chat widget enabler job' do
        ::EmbeddableChatWidgetEnablerJob.expects(:enqueue).never
        account.create_zopim_integration!(
          external_zopim_id: 12,
          zopim_key: 'zopim-key'
        )
      end
    end

    describe 'when it is not a phase one integration' do
      before do
        ZopimIntegration.any_instance.stubs(phase_one?: false)
      end

      it 'enqueues the embeddable chat widget enabler job' do
        ::EmbeddableChatWidgetEnablerJob.expects(:enqueue).
          with(account.id).once
        account.create_zopim_integration!(
          external_zopim_id: 123,
          zopim_key: 'zopim-key'
        )
      end
    end
  end

  describe '#sync_zuora_id' do
    let(:account) { accounts(:minimum) }

    let(:zopim_integration) do
      account.create_zopim_integration!(
        external_zopim_id: 1,
        zopim_key: 'zopim-key',
        phase: 1
      )
    end

    describe 'when not phase 3' do
      before do
        zopim_integration.update_attributes(
          external_zopim_id: 321
        )
      end

      it 'does not trigger zuora id sync job' do
        ZBC::Zuora::Jobs::ZopimIdSyncJob.expects(:enqueue).never
      end
    end

    describe 'when phase 3' do
      it 'triggers zuora id sync job' do
        ZBC::Zuora::Jobs::ZopimIdSyncJob.expects(:enqueue).
          with(account.id, -321).once
        zopim_integration.update_attributes(
          external_zopim_id: -321,
          phase: 3
        )
      end
    end
  end

  describe '#destroy' do
    let(:account) { accounts(:minimum) }

    let(:zopim_integration) do
      account.create_zopim_integration!(
        external_zopim_id: 1,
        zopim_key: 'zopim-key'
      )
    end

    it 'uninstalls the app' do
      ZopimIntegration.any_instance.expects(:uninstall_app).once
      assert zopim_integration.destroy
    end
  end

  describe 'AppMarketIntegration requirements' do
    let(:account) { accounts(:minimum) }

    let(:zopim_integration) do
      account.create_zopim_integration!(
        external_zopim_id: 1,
        zopim_key: 'zopim-key'
      )
    end

    it 'is able to return a Zendesk Account instance' do
      assert zopim_integration.account.instance_of? Account
    end

    describe '#app_market_name' do
      it 'implements the #app_market_name method' do
        assert zopim_integration.respond_to? :app_market_name
      end

      it 'is "Zendesk Chat"' do
        assert_equal 'Zendesk Chat', zopim_integration.app_market_name
      end
    end

    describe '#app_market_id' do
      let(:account_url) { account.url(ssl: true) }
      let(:chat_app_id) { 55 }

      it 'implements the #app_market_id method' do
        assert zopim_integration.respond_to? :app_market_id
      end

      it 'fetches the chat_app_id using an internal client' do
        ZBC::AppMarket::InternalApiClient.expects(:new).
          with(account_url).
          returns(stub(chat_app_id: chat_app_id))

        assert_equal chat_app_id, zopim_integration.app_market_id
      end
    end
  end

  describe 'app uninstallation' do
    let(:account) { accounts(:minimum) }

    before do
      ZopimIntegration.any_instance.stubs(:uninstall_app).raises(Exception)
    end

    describe 'when app uninstallation raises an exception' do
      it 'handles the exception' do
        account.create_zopim_integration!(
          external_zopim_id: 1,
          zopim_key: 'zopim-key'
        )
        assert account.zopim_integration.destroy
      end
    end
  end
end
