require_relative "../support/test_helper"

SingleCov.covered!

describe VisibilityRestriction do
  it("have an id") { assert_respond_to subject, :id }
  it("have a label") { assert_respond_to subject, :label }

  it "has many forums" do
    queries = sql_queries do
      VisibilityRestriction.new(id: 111).forums.to_a
    end
    queries.must_equal ["SELECT `forums`.* FROM `forums` WHERE `forums`.`deleted_at` IS NULL AND `forums`.`visibility_restriction_id` = 111"]
  end

  describe "with id=1" do
    subject { VisibilityRestriction.find(1) }

    it("respond with true to #everybody?") { assert subject.everybody? }
    it("respond with false to #logged_in_only?") { refute subject.logged_in_only? }
    it("respond with false to #agents_only?") { refute subject.agents_only? }
  end

  describe "with id=2" do
    subject { VisibilityRestriction.find(2) }

    it("respond with false to #everybody?") { refute subject.everybody? }
    it("respond with true to #logged_in_only?") { assert subject.logged_in_only? }
    it("respond with false to #agents_only?") { refute subject.agents_only? }
  end

  describe "with id=3" do
    subject { VisibilityRestriction.find(3) }

    it("respond with false to #everybody?") { refute subject.everybody? }
    it("respond with false to #logged_in_only?") { refute subject.logged_in_only? }
    it("respond with true to #agents_only?") { assert subject.agents_only? }
  end

  it "is accessible by name as a constant" do
    VisibilityRestriction.all.each do |visibility_restriction|
      constant_name = visibility_restriction.label.upcase.gsub(/\W/, "_")

      assert_equal VisibilityRestriction.const_get(constant_name), visibility_restriction
    end
  end
end
