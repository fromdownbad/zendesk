require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe PushNotifications::DeviceIdentifier do
  def raw_device_insert(data)
    data[:id] = rand(9999999)
    keys = data.keys.map { |x| "`#{x}`" }.join(", ")
    values = data.values.map(&:inspect).map { |v| v == "nil" ? "NULL" : v }.join(", ")
    sql = "INSERT INTO `device_identifiers` (#{keys}) VALUES (#{values})"
    ActiveRecord::Base.connection.execute sql
  end

  fixtures :users, :accounts, :mobile_apps

  should_validate_presence_of :token, :device_type, :account_id

  describe "Data compatibility" do
    before do
      PushNotifications::DeviceIdentifier.without_arsi.delete_all
    end

    # TODO: Get rid of this entire section when migration of the data in the 'type' column has been
    # performed.
    describe "Old data layout compatibility (Non STI)" do
      before do
        PushNotifications::DeviceIdentifier.any_instance.stubs(:register).returns(true)
        @agent = users(:minimum_agent)
        raw_device_insert(
          account_id: @agent.account.id,
          device_type: 'iphone',
          mobile_app_id: mobile_apps(:com_zendesk_agent).id,
          token: 'foo',
          user_id: @agent.id,
          type: nil
        )

        @identifier = @agent.device_identifiers.first
      end

      it "returns the correct MobileApp object" do
        assert_equal mobile_apps(:com_zendesk_agent), @identifier.mobile_app
      end

      it "returns PushNotifications::DeviceIdentifier when 'type' is set to nil" do
        assert_instance_of PushNotifications::DeviceIdentifier, @identifier
      end
    end

    describe "New data layout compatibility (STI)" do
      before do
        PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register).returns(true)
        @agent = users(:minimum_agent)
        raw_device_insert(
          account_id: @agent.account_id,
          device_type: 'iphone',
          mobile_app_id: mobile_apps(:com_zendesk_agent).id,
          token: 'foo',
          user_id: @agent.id,
          type: 'PushNotifications::FirstPartyDeviceIdentifier'
        )
        @identifier = @agent.device_identifiers.first
      end

      it "returns the correct MobileApp object" do
        assert_equal mobile_apps(:com_zendesk_agent), @identifier.mobile_app
      end

      it "returns an instance of the correct PushNotifications::* hierarchy according 'type'" do
        assert_instance_of PushNotifications::FirstPartyDeviceIdentifier, @identifier
      end
    end
  end

  describe "validations" do
    it "should validate token length" do
      device = PushNotifications::DeviceIdentifier.new(account: accounts(:minimum), device_type: 'Android', token: 'a' * 260)
      refute device.valid?
      expected_errors = ["Token is too long (maximum is 255 characters after encoding)"]
      assert_equal expected_errors, device.errors.to_a
    end
  end

  describe "#find_by_token_or_id!" do
    before do
      PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register).returns(true)
      @identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: "foo", device_type: "iphone", user: users(:minimum_agent), mobile_app: mobile_apps(:com_zendesk_agent)
      )
    end

    it "finds by the token" do
      assert_equal @identifier, PushNotifications::DeviceIdentifier.find_by_token_or_id!(@identifier.token, nil)
    end

    it "finds by the id" do
      assert_equal @identifier, PushNotifications::DeviceIdentifier.find_by_token_or_id!(nil, @identifier.id)
    end

    it "raises when unable to find by a token or id" do
      assert_raise(ActiveRecord::RecordNotFound) { PushNotifications::DeviceIdentifier.find_by_token_or_id!(nil, nil) }
    end

    it "does not be susceptible to mysql coercion exploits" do
      assert_nil PushNotifications::DeviceIdentifier.find_by_token(0)
      assert_raise(ActiveRecord::RecordNotFound) { PushNotifications::DeviceIdentifier.find_by_token_or_id!(0, 0) }
    end
  end

  describe "#android?" do
    it "is true for 'Android'" do
      assert PushNotifications::DeviceIdentifier.new(device_type: "Android").android?
    end

    it "is true for 'android'" do
      assert PushNotifications::DeviceIdentifier.new(device_type: "android").android?
    end

    it "is false for something else" do
      refute PushNotifications::DeviceIdentifier.new(device_type: "anders").android?
    end
  end

  describe "#ios?" do
    it "identifys 'iPad'" do
      assert PushNotifications::DeviceIdentifier.new(device_type: "iPad").ios?
    end

    it "identifys 'iPhone'" do
      assert PushNotifications::DeviceIdentifier.new(device_type: "iPhone").ios?
    end

    it "does not identify something else" do
      refute PushNotifications::DeviceIdentifier.new(device_type: "FunkyPhone").ios?
    end

    it "does not identify for something partially iphone" do
      refute PushNotifications::DeviceIdentifier.new(device_type: "iphone/foo").ios?
    end

    it "does not identify null or empty as ios or android" do
      refute PushNotifications::DeviceIdentifier.new(device_type: nil).ios?
      refute PushNotifications::DeviceIdentifier.new(device_type: "").ios?

      refute PushNotifications::DeviceIdentifier.new(device_type: nil).android?
      refute PushNotifications::DeviceIdentifier.new(device_type: "").android?
    end
  end

  describe "#token_type" do
    before do
      PushNotifications::DeviceIdentifier.any_instance.stubs(:register).returns(true)
      PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register).returns(true)
    end

    describe "when not provided at the moment of creation" do
      before do
        PushNotifications::DeviceIdentifier.without_arsi.delete_all
      end

      it "should return 'urban_airship_device_token' when the token is iOS" do
        identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
          token: "foo",
          device_type: "iphone",
          user: users(:minimum_agent),
          mobile_app: mobile_apps(:com_zendesk_agent)
        )

        assert_equal identifier.token_type, "urban_airship_device_token"
      end

      it "should return 'urban_airship_apid' when the token is Android" do
        identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
          token: "foo",
          device_type: "android",
          user: users(:minimum_agent),
          mobile_app: mobile_apps(:com_zendesk_agent)
        )

        assert_equal identifier.token_type, "urban_airship_apid"
      end
    end

    describe "when provided at the moment of creation" do
      it "returns the value provided" do
        identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
          token: "foo",
          device_type: "iphone",
          user: users(:minimum_agent),
          mobile_app: mobile_apps(:com_zendesk_agent),
          token_type: "urban_airship_channel_id"
        )

        assert_equal identifier.token_type, "urban_airship_channel_id"
      end
    end
  end

  describe "#gateway" do
    it "should return gcm" do
      device_identifier = PushNotifications::DeviceIdentifier.new(token_type: 'gcm')
      assert_equal 'gcm', device_identifier.gateway
    end

    it "should return raw" do
      device_identifier = PushNotifications::DeviceIdentifier.new(token_type: 'raw')
      assert_equal 'raw', device_identifier.gateway
    end

    it "should return urban_airship" do
      device_identifier = PushNotifications::DeviceIdentifier.new(token_type: 'urban_airship_channel_id')
      assert_equal 'urban_airship', device_identifier.gateway
      device_identifier = PushNotifications::DeviceIdentifier.new(token_type: 'urban_airship_apid')
      assert_equal 'urban_airship', device_identifier.gateway
      device_identifier = PushNotifications::DeviceIdentifier.new(token_type: 'urban_airship_device_token')
      assert_equal 'urban_airship', device_identifier.gateway
    end

    it "should return nil" do
      device_identifier = PushNotifications::DeviceIdentifier.new(token_type: 'foo')
      assert_nil device_identifier.gateway
    end
  end
end
