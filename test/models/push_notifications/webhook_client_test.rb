require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'PushNotifications::WebhookClientTest' do
  fixtures :users, :devices, :mobile_sdk_apps, :mobile_apps

  let(:mobile_sdk_app) { mobile_sdk_apps(:minimum) }
  let(:customer_callback) { "https://client.service.com/zendesk/push" }

  before do
    stub_request(:post, customer_callback)

    @client = PushNotifications::WebhookClient.new(customer_callback)
    PushNotificationRegistrationJob.stubs(:enqueue)
  end

  describe "#initialize" do
    it "sets the base_uri" do
      assert_equal customer_callback, @client.class.base_uri
    end

    describe "with options" do
      it "sets the statsd_tags" do
        @client = PushNotifications::WebhookClient.new(customer_callback, statsd_tags: ["tag:one"])
        refute @client.instance_variable_get(:@statsd_tags).empty?
      end
    end
  end

  describe "push notifications" do
    before do
      @ios_identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: "foofoofoofoofoofoofoo",
        device_type: "iphone",
        user: users(:minimum_end_user),
        mobile_app: mobile_apps(:com_zendesk_agent)
      )
      @android_identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: "foofoofoofoofoofoofoofand",
        device_type: "android",
        user: users(:minimum_end_user),
        mobile_app: mobile_apps(:com_zendesk_agent)
      )
      @basic_payload = {
        msg: 'Activity Title',
        user_id: 1234567,
        ticket_id: "1234",
        msg_short: 'Activity Short Title',
        detail: 'Activity Detail'
      }
    end

    it "send push notifications to ios" do
      @client.push([@ios_identifier.id], @basic_payload)
      assert_requested(:post, customer_callback)
    end

    it "send push notifications to android" do
      @client.push([@android_identifier.id], @basic_payload)
      assert_requested(:post, customer_callback)
    end

    describe "notification request body" do
      it "send ios push notifications with a correct body" do
        stub_request(:post, customer_callback).with do |request|
          request_body = JSON.parse(request.body)
          assert_equal 1, request_body["devices"].size
          assert_equal "ios", request_body["devices"].first["type"]
          assert_equal @ios_identifier.token, request_body["devices"].first["identifier"]
          assert_equal @basic_payload[:msg], request_body["notification"]["body"]
          assert_equal @basic_payload[:msg_short], request_body["notification"]["title"]
          assert_equal @basic_payload[:ticket_id], request_body["notification"]["ticket_id"]
        end

        @client.push([@ios_identifier.id], @basic_payload)
      end

      it "send android push notifications with a correct body" do
        stub_request(:post, customer_callback).with do |request|
          request_body = JSON.parse(request.body)
          assert_equal 1, request_body["devices"].size
          assert_equal "android", request_body["devices"].first["type"]
          assert_equal @android_identifier.token, request_body["devices"].first["identifier"]
          assert_equal @basic_payload[:msg], request_body["notification"]["body"]
          assert_equal @basic_payload[:msg_short], request_body["notification"]["title"]
          assert_equal @basic_payload[:ticket_id], request_body["notification"]["ticket_id"]
        end

        @client.push([@android_identifier.id], @basic_payload)
      end

      it "send push notifications to multiple devices with a correct body" do
        stub_request(:post, customer_callback).with do |request|
          request_body = JSON.parse(request.body)
          assert_equal 2, request_body["devices"].size
          assert_equal @ios_identifier.token, request_body["devices"].select { |d| d["type"] ==  "ios" }.first["identifier"]
          assert_equal @android_identifier.token, request_body["devices"].select { |d| d["type"] ==  "android" }.first["identifier"]
          assert_equal @basic_payload[:msg], request_body["notification"]["body"]
          assert_equal @basic_payload[:msg_short], request_body["notification"]["title"]
          assert_equal @basic_payload[:ticket_id], request_body["notification"]["ticket_id"]
        end

        @client.push([@android_identifier.id, @ios_identifier.id], @basic_payload)
      end
    end

    it "send to all ios and android clients" do
      banana = PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: "foofoofoofoofoofoofoo2",
        device_type: "banana",
        user: users(:minimum_end_user),
        mobile_app: mobile_apps(:com_zendesk_agent)
      )

      stub_request(:post, customer_callback).with do |request|
        request_body = JSON.parse(request.body)
        assert_equal 2, request_body["devices"].size
        assert_equal ["android", "ios"].sort, request_body["devices"].map { |d| d["type"] }.sort
        assert_equal @basic_payload[:msg], request_body["notification"]["body"]
        assert_equal @basic_payload[:msg_short], request_body["notification"]["title"]
        assert_equal @basic_payload[:ticket_id], request_body["notification"]["ticket_id"]
      end

      @client.push([@android_identifier.id, @ios_identifier.id, banana.id], @basic_payload)
    end

    describe "with invalid device_types" do
      before do
        @foo = PushNotifications::FirstPartyDeviceIdentifier.create!(
          token: "asdfasdfasdfasdfasdfasdfasdfasdfdsfasdf",
          device_type: "amazon",
          user: users(:minimum_end_user),
          mobile_app: mobile_apps(:com_zendesk_agent)
        )
      end

      it "not push" do
        assert_not_requested(:post, customer_callback)
        @client.push([@foo.id], @basic_payload)
      end
    end

    describe "causing exceptions" do
      it "not return garbage" do
        @client.class.expects(:post).raises(Timeout::Error.new("Boo!"))
        refute @client.push([@ios_identifier.id], @basic_payload)
      end

      it "fail silently for Timeout::Errors" do
        @client.class.expects(:post).raises(Timeout::Error.new("Boo!"))
        Rails.logger.expects(:warn).with { |msg| msg =~ /PushNotifications::WebhookClient Timeout::Error/ }
        @client.push([@ios_identifier.id], @basic_payload)
      end

      it "fail silently for SocketError Name or service not known" do
        @client.class.expects(:post).raises(SocketError.new("getaddrinfo: Name or service not known"))
        Rails.logger.expects(:warn).with { |msg| msg =~ /PushNotifications::WebhookClient SocketError/ }
        @client.push([@ios_identifier.id], @basic_payload)
      end

      it "fail silently for EHOSTUNREACH" do
        @client.class.expects(:post).raises(Errno::EHOSTUNREACH.new("stuff"))
        Rails.logger.expects(:warn).with { |msg| msg =~ /PushNotifications::WebhookClient Errno::EHOSTUNREACH/ }
        @client.push([@ios_identifier.id], @basic_payload)
      end

      it "fail noisily with unknown SocketError" do
        @client.class.expects(:post).raises(SocketError.new("Fuubar"))
        assert_raise(SocketError) { @client.push([@ios_identifier.id], @basic_payload) }
      end

      it "fail noisily for RuntimeErrors" do
        @client.class.expects(:post).raises(RuntimeError.new("Boo!"))
        assert_raise(RuntimeError) { @client.push([@ios_identifier.id], @basic_payload) }
      end
    end
  end
end
