require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe PushNotifications::FirstPartyDeviceIdentifier do
  fixtures :users, :accounts, :mobile_apps

  let(:valid_attributes) do
    {
      token: "foo",
      device_type: "iphone",
      account: accounts(:minimum),
      mobile_app: mobile_apps(:com_zendesk_agent),
      user: users(:minimum_agent)
    }
  end

  describe "#create" do
    describe "for the agent app" do
      it "successfully creates the model" do
        PushNotifications::FirstPartyDeviceIdentifier.
          any_instance.
          stubs(:register)

        identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
          valid_attributes
        )

        assert identifier.persisted?
      end
    end
  end

  describe "#mobile_app_identifier" do
    let(:device_identifier) { PushNotifications::FirstPartyDeviceIdentifier.new }

    describe "when there is no mobile app associated" do
      it "returns nil" do
        assert_nil device_identifier.mobile_app_identifier
      end
    end

    describe "when there is a mobile app associated" do
      before { device_identifier.mobile_app = mobile_apps(:com_zendesk_agent) }
      it "returns nil" do
        assert_equal "com.zendesk.agent", device_identifier.mobile_app_identifier
      end
    end
  end

  describe "#registration job" do
    it "registers and unregister device tokens in a resque job when identifiers are created/deleted" do
      PushNotificationRegistrationJob.expects(:enqueue).with do |args|
        args[:account_id] == users(:minimum_agent).account_id &&
        args[:operation] == :register &&
        args[:mobile_app_identifier] == 'com.zendesk.agent' &&
        args[:token] == 'foo'
      end

      identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
        valid_attributes
      )

      PushNotificationRegistrationJob.expects(:enqueue).with(
        account_id: users(:minimum_agent).account_id,
        operation: :unregister,
        mobile_app_identifier: 'com.zendesk.agent',
        token: identifier.token,
        device_type: identifier.device_type,
        sns_target_arn: identifier.sns_target_arn
      )

      identifier.destroy
    end

    it 'does not enqueue jobs when a create is rolled back' do
      PushNotificationRegistrationJob.expects(:enqueue).never

      notification = PushNotifications::FirstPartyDeviceIdentifier.new(
        valid_attributes
      )

      with_rollback(notification) { notification.save! }
    end

    it 'does not enqueue jobs when a destroy is rolled back' do
      PushNotificationRegistrationJob.expects(:enqueue).once # for the create call

      notification = PushNotifications::FirstPartyDeviceIdentifier.create!(
        valid_attributes
      )

      with_rollback(notification) { notification.destroy }
    end
  end

  describe "#registration_payload" do
    before do
      PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register)
      @android_identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: "APID123", device_type: "Android",
        user: users(:minimum_agent), mobile_app: mobile_apps(:com_zendesk_agent)
      )
    end
  end

  describe "#unregister" do
    before do
      @account = accounts(:minimum)
      PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register).returns(true)
      PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: "foo", device_type: "iphone", user: users(:minimum_agent),
        account: @account, mobile_app: mobile_apps(:com_zendesk_agent)
      )
      @identifier = PushNotifications::FirstPartyDeviceIdentifier.find_by_token("foo")
    end

    describe "when the device type is android" do
      before do
        @identifier.stubs(:android?).returns(true)
      end

      it "does not enqueue a registration job" do
        PushNotificationRegistrationJob.expects(:enqueue).never

        @identifier.unregister
      end
    end

    describe "when the device type is not android" do
      before do
        @identifier.stubs(:android?).returns(false)
      end

      it "enqueues a registration job" do
        PushNotificationRegistrationJob.expects(:enqueue).once

        @identifier.unregister
      end
    end
  end

  describe "#record_view!" do
    before do
      @account = accounts(:minimum)
      PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register).returns(true)
      PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: "foo", device_type: "iphone", user: users(:minimum_agent),
        account: @account, mobile_app: mobile_apps(:com_zendesk_agent)
      )
      @identifier = PushNotifications::FirstPartyDeviceIdentifier.find_by_token("foo")
    end

    it "should set badge_count to 0 when feature sns_push_notifications is enabled for the account" do
      Arturo.enable_feature!(:sns_push_notifications)
      @identifier.update_attribute(:badge_count, 9)
      @identifier.record_view!
      assert_equal 0, @identifier.badge_count
    end

    it "should set badge_count to 0 when token type is original" do
      Arturo.enable_feature!(:sns_push_notifications)
      @identifier.update_attribute(:badge_count, 9)
      @identifier.stubs(:token_type).returns('original')
      @identifier.record_view!
      assert_equal 0, @identifier.badge_count
    end

    it "should return nil when when token_type gcm or original" do
      Arturo.disable_feature!(:sns_push_notifications)
      @identifier.stubs(:token_type).returns('gcm')
      assert_nil @identifier.record_view!
    end
  end
end
