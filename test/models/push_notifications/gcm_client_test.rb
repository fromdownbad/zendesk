require_relative '../../support/test_helper'

SingleCov.covered!

describe PushNotifications::GCMClient do
  fixtures :all

  subject { PushNotifications::GCMClient.new('api_key' => 'secret-google-key') }

  describe 'push' do
    let(:agent_mobile_app) { mobile_apps(:com_zendesk_agent) }

    let(:device1) do
      PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: 'device1',
        token_type: 'gcm',
        device_type: 'android',
        user: users(:minimum_agent),
        mobile_app: agent_mobile_app
      )
    end

    let(:device2) do
      PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: 'device2',
        token_type: 'gcm',
        device_type: 'iphone',
        user: users(:minimum_agent),
        mobile_app: agent_mobile_app
      )
    end

    let(:device3) do
      PushNotifications::FirstPartyDeviceIdentifier.create!(
        token: 'device3',
        token_type: 'gcm',
        device_type: 'chrome',
        user: users(:minimum_agent),
        mobile_app: agent_mobile_app
      )
    end

    let(:payload) do
      {
        'msg' => 'Activity Title',
        'user_id' => 1234567,
        'ticket_id' => 1234,
        'msg_short' => 'Activity Short Title',
        'detail' => 'Activity Detail'
      }
    end

    before do
      stub_request(:post, 'https://gcm-http.googleapis.com/gcm/send').
        to_return(
          status: 200,
          headers: {
            'Content-Type' => 'application/json'
          },
          body: {
            multicast_id: 216,
            success: 2,
            failure: 1,
            canonical_ids: 1,
            results: [
              { message_id: '1:0408' },
              { message_id: '1:2342', registration_id: 'new_token_for_device2' },
              { error: 'NotRegistered'}
            ]
          }.to_json
        )

      subject.push([device1.id, device2.id, device3.id], payload)
    end

    it 'uses Google Cloud Messaging' do
      assert_requested(:post, 'https://gcm-http.googleapis.com/gcm/send',
        headers: {
          'Content-Type' => 'application/json',
          'Authorization' => 'key=secret-google-key',
        },
        body: {
          registration_ids: ['device1', 'device2', 'device3']
        }.to_json)
    end

    it 'deletes devices that have unregistered' do
      assert device1.reload, 'device1 should not be deleted'
      assert device2.reload, 'device2 should not be deleted'
      -> { device3.reload }.must_raise ActiveRecord::RecordNotFound, 'device3 should have been deleted'
    end

    it 'updates devices that have a new token' do
      device1.reload.token.must_equal 'device1'
      device2.reload.token.must_equal 'new_token_for_device2'
    end
  end
end
