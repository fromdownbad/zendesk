require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe PushNotifications::SdkAnonymousDeviceIdentifier do
  fixtures :user_identities, :mobile_sdk_apps, :accounts, :mobile_apps

  should_validate_presence_of :token, :device_type, :user_sdk_identity, :mobile_sdk_app

  describe "#create" do
    before do
      PushNotifications::SdkAnonymousDeviceIdentifier.any_instance.stubs(:register).returns(true)
    end

    describe "for sdk app" do
      it "works" do
        @identifier = PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
          sadi.token = "foo"
          sadi.device_type = "iphone"
          sadi.account = accounts(:minimum_sdk)
          sadi.user_sdk_identity = user_identities(:mobile_sdk_identity)
          sadi.mobile_sdk_app = mobile_sdk_apps(:minimum_sdk)
        end
        assert @identifier.id
      end

      it "validates user_sdk_identity type" do
        assert_raises(ActiveRecord::AssociationTypeMismatch) do
          PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
            sadi.token = "foo"
            sadi.device_type = "iphone"
            sadi.account = accounts(:minimum_sdk)
            sadi.user_sdk_identity = user_identities(:minimum_end_user_for_sdk_account)
            sadi.mobile_sdk_app = mobile_sdk_apps(:minimum_sdk)
          end
        end
      end

      it "validates mobile_sdk_app type" do
        assert_raises(ActiveRecord::AssociationTypeMismatch) do
          PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
            sadi.token = "foo"
            sadi.device_type = "iphone"
            sadi.account = accounts(:minimum_sdk)
            sadi.user_sdk_identity = user_identities(:mobile_sdk_identity)
            sadi.mobile_sdk_app = mobile_apps(:com_zendesk_agent)
          end
        end
      end

      it "validates the uniqueness of the token in the scope of user SDK identity" do
        user_sdk_identity = user_identities(:mobile_sdk_identity)
        token = "foo"
        PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
          sadi.token = token
          sadi.device_type = "iphone"
          sadi.account = accounts(:minimum_sdk)
          sadi.user_sdk_identity = user_sdk_identity
          sadi.mobile_sdk_app = mobile_sdk_apps(:minimum_sdk)
        end
        refute_difference('PushNotifications::SdkAnonymousDeviceIdentifier.count(:all)', 1) do
          PushNotifications::SdkAnonymousDeviceIdentifier.create do |sadi|
            sadi.token = token
            sadi.device_type = "iphone"
            sadi.account = accounts(:minimum)
            sadi.user_sdk_identity = user_sdk_identity
            sadi.mobile_sdk_app = mobile_sdk_apps(:minimum)
          end
        end
      end
    end
  end

  describe "#subscribe_to_ticket" do
    fixtures :accounts, :tickets

    before do
      @ticket = tickets(:minimum_mobile_sdk)
      @requester = @ticket.requester
      assert @requester.is_end_user?
      @account = @ticket.account
      @mobile_sdk_app = mobile_sdk_apps(:minimum_sdk)
    end

    it "works for correct requester" do
      user_sdk_identity = user_identities(:mobile_sdk_identity)
      identifier = PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
        sadi.token = "foo"
        sadi.device_type = "iphone"
        sadi.account = @account
        sadi.user_sdk_identity = user_sdk_identity
        sadi.mobile_sdk_app = @mobile_sdk_app
      end
      @requester.identities << user_sdk_identity
      @requester.save
      identifier.subscribe_to_ticket(@ticket)
      assert_equal @ticket, identifier.ticket
      assert @ticket.sdk_anonymous_device_identifiers
    end

    it "fails when not linked with requester" do
      other_sdk_identity = user_identities(:mobile_sdk_identity_2)
      identifier = PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
        sadi.token = "foo"
        sadi.device_type = "iphone"
        sadi.account = @account
        sadi.user_sdk_identity = other_sdk_identity
        sadi.mobile_sdk_app = @mobile_sdk_app
      end
      identifier.subscribe_to_ticket(@ticket)
      refute identifier.ticket
      assert_empty @ticket.sdk_anonymous_device_identifiers
    end
  end

  describe "Urban Ariship Push Notifications Registration / Deregistration" do
    before do
      @account = accounts(:minimum_sdk)
      @mobile_sdk_app = mobile_sdk_apps(:minimum_sdk)
      @mobile_sdk_app.stubs(:settings).returns(
        stub(push_notifications_type: MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP,
             push_notifications_enabled: true)
      )
    end

    it "registers the device on creation" do
      PushNotifications::SdkAnonymousDeviceIdentifier.any_instance.expects(:register)
      PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
        sadi.token = "foo"
        sadi.device_type = "iphone"
        sadi.account = @account
        sadi.user_sdk_identity = user_identities(:mobile_sdk_identity)
        sadi.mobile_sdk_app = @mobile_sdk_app
      end
    end

    it "deregisters the device on destruction" do
      PushNotifications::SdkAnonymousDeviceIdentifier.any_instance.stubs(:register).returns(true)

      identifier = PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
        sadi.token = "foo"
        sadi.device_type = "iphone"
        sadi.account = @account
        sadi.user_sdk_identity = user_identities(:mobile_sdk_identity)
        sadi.mobile_sdk_app = @mobile_sdk_app
      end

      PushNotifications::SdkAnonymousDeviceIdentifier.any_instance.expects(:unregister)
      identifier.destroy
    end
  end

  describe "scopes queries to account ID" do
    it "for user_sdk_identity association" do
      identifier = PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
        sadi.token = "foo"
        sadi.device_type = "iphone"
        sadi.account = accounts(:minimum_sdk)
        sadi.user_sdk_identity = user_identities(:mobile_sdk_identity)
        sadi.mobile_sdk_app = mobile_sdk_apps(:minimum_sdk)
      end
      identifier.reload

      assert_sql_queries(1, /`user_identities`.`account_id` = #{identifier.account_id}/) do
        identifier.user_sdk_identity
      end
    end
  end
end
