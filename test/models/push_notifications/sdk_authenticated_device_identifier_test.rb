require_relative "../../support/test_helper"

SingleCov.covered!

describe PushNotifications::SdkAuthenticatedDeviceIdentifier do
  fixtures :users, :accounts, :mobile_sdk_apps

  should_validate_presence_of :token, :device_type, :user, :mobile_sdk_app

  describe "#create" do
    before do
      PushNotifications::SdkAuthenticatedDeviceIdentifier.any_instance.stubs(:register).returns(true)
    end

    let(:valid_attributes) do
      {
        token: "foo",
        device_type: "iphone",
        account: accounts(:minimum),
        user: users(:minimum_end_user),
        mobile_sdk_app: mobile_sdk_apps(:minimum)
      }
    end

    describe "for sdk app" do
      it "works" do
        @identifier = PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(valid_attributes)
        assert @identifier.id
      end

      it "validates user type" do
        assert_raises(ActiveRecord::AssociationTypeMismatch) do
          invalid_attributes = valid_attributes.merge(user: mobile_sdk_apps(:minimum))
          PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(invalid_attributes)
        end
      end

      it "validates mobile_sdk_app type" do
        assert_raises(ActiveRecord::AssociationTypeMismatch) do
          invalid_attributes = valid_attributes.merge(mobile_sdk_app: users(:minimum_end_user))
          PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(invalid_attributes)
        end
      end
    end
  end

  describe "Urban Ariship Push Notifications Registration / Deregistration" do
    before do
      @account = accounts(:minimum_sdk)
      @mobile_sdk_app = mobile_sdk_apps(:minimum_sdk)
      @mobile_sdk_app.stubs(:settings).returns(
        stub(push_notifications_type: MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP,
             push_notifications_enabled: true)
      )
    end

    it "it enqueues a registration job on creation" do
      PushNotificationSdkRegistrationJob.expects(:enqueue).with do |args|
        args['account_id'] == @account.id &&
        args['mobile_sdk_app_id'] == @mobile_sdk_app.id
      end

      PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(
        token: "foo",
        device_type: "iphone",
        account: @account,
        user: users(:minimum_end_user),
        mobile_sdk_app: @mobile_sdk_app
      )
    end

    it 'does not enqueue a registration job when a create rolls back' do
      PushNotificationSdkRegistrationJob.expects(:enqueue).never

      notification = PushNotifications::SdkAuthenticatedDeviceIdentifier.new(
        token: "foo",
        device_type: "iphone",
        account: @account,
        user: users(:minimum_end_user),
        mobile_sdk_app: @mobile_sdk_app
      )

      with_rollback(notification) { notification.save! }
    end

    it "enqueues a deregister job on destroy" do
      PushNotificationSdkDeregistrationJob.expects(:enqueue).with do |args|
        args['account_id'] == @account.id &&
        args['mobile_sdk_app_id'] == @mobile_sdk_app.id &&
        args['device_type'] == 'iphone'
      end

      token_request = stub_request(:put, 'https://go.urbanairship.com/api/device_tokens/foo')

      identifier = PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(
        token: "foo",
        device_type: "iphone",
        account: @account,
        user: users(:minimum_end_user),
        mobile_sdk_app: @mobile_sdk_app
      )

      identifier.destroy
      assert_requested(token_request)
    end

    it "does not enqueue a deregister job when a destroy rolls back" do
      PushNotificationSdkDeregistrationJob.expects(:enqueue).never

      identifier = PushNotifications::SdkAuthenticatedDeviceIdentifier.new(
        token: "foo",
        device_type: "iphone",
        account: @account,
        user: users(:minimum_end_user),
        mobile_sdk_app: @mobile_sdk_app
      )

      with_rollback(identifier) { identifier.destroy }
    end
  end
end
