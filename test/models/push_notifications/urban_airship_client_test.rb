require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'UrbanAirshipClient' do
  fixtures :users, :devices, :mobile_apps

  [2, 3].each do |api_version|
    describe "API V#{api_version}" do
      let(:mobile_app_identifier) { "com.zendesk.agent" }
      let(:key) { Zendesk::Configuration.dig!(:urban_airship, mobile_app_identifier, :app_key) }
      let(:secret) { Zendesk::Configuration.dig!(:urban_airship, mobile_app_identifier, :app_secret) }
      let(:master_secret) { Zendesk::Configuration.dig!(:urban_airship, mobile_app_identifier, :master_secret) }

      before do
        stub_request(:put, "https://go.urbanairship.com/api/device_tokens/foofoofoofoofoofoofoo").
          with(basic_auth: [key, master_secret])
        stub_request(:put, "https://go.urbanairship.com/api/apids/foofoofoofoofoofoofoo").
          with(basic_auth: [key, master_secret])
        stub_request(:delete, "https://go.urbanairship.com/api/device_tokens/foofoofoofoofoofoofoo").
          with(basic_auth: [key, master_secret])
        stub_request(:post, "https://go.urbanairship.com/api/push/").
          with(basic_auth: [key, master_secret])

        @since_time = 1.day.ago
        stub_request(:get, "https://go.urbanairship.com/api/device_tokens/feedback?since=#{@since_time.iso8601}").
          with(basic_auth: [key, master_secret])

        @client = PushNotifications::UrbanAirshipClient.new(key, master_secret)
      end

      let(:token) { "foofoofoofoofoofoofoo" }
      let(:iphone_device_type) { "iphone" }
      let(:android_device_type) { "Android" }
      let(:windows_device_type) { "windows" }
      let(:minimum_agent) { users(:minimum_agent) }
      let(:zendesk_agent_mobile_app) { mobile_apps(:com_zendesk_agent) }

      describe "device token management" do
        it "register a device token" do
          identifier = PushNotifications::FirstPartyDeviceIdentifier.new(
            token: token,
            device_type: iphone_device_type,
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )

          @client.register(identifier)

          assert_requested(:put, "https://go.urbanairship.com/api/device_tokens/foofoofoofoofoofoofoo")
        end

        it "unregister a device token" do
          @client.unregister(token, iphone_device_type)

          assert_requested(:delete, "https://go.urbanairship.com/api/device_tokens/foofoofoofoofoofoofoo")
        end

        it "update a device token" do
          @client.update(token, iphone_device_type, badge: 0)
          assert_requested(:put, "https://go.urbanairship.com/api/device_tokens/foofoofoofoofoofoofoo")
        end

        it "register an android apid" do
          identifier = PushNotifications::FirstPartyDeviceIdentifier.new(
            token: token,
            device_type: android_device_type,
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )

          @client.register(identifier)

          assert_requested(:put, "https://go.urbanairship.com/api/apids/foofoofoofoofoofoofoo")
        end

        it "register a windows apid" do
          identifier = PushNotifications::FirstPartyDeviceIdentifier.new(
            token: token,
            device_type: windows_device_type,
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )

          @client.register(identifier)

          assert_requested(:put, "https://go.urbanairship.com/api/apids/foofoofoofoofoofoofoo")
        end
      end

      describe "push notifications" do
        before do
          PushNotificationRegistrationJob.stubs(:enqueue)
          @identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
            token: token,
            device_type: iphone_device_type,
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )

          @ios_channel_identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
            token: "foo-bar-baz-ios",
            token_type: "urban_airship_channel_id",
            device_type: iphone_device_type,
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )

          @android_identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
            token: "foo-bar-baz-android",
            device_type: android_device_type,
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )

          @android_channel_identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
            token: "foo-bar-baz-android-channel",
            token_type: "urban_airship_channel_id",
            device_type: android_device_type,
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )

          @windows_identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
            token: "foo-bar-baz-windows-apid",
            token_type: "urban_airship_apid",
            device_type: windows_device_type,
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )

          @basic_payload = {
            msg: 'Activity Title',
            user_id: 1234567,
            ticket_id: 1234,
            msg_short: 'Activity Short Title',
            detail: 'Activity Detail'
          }
          @payload_model = @client.instance_variable_get("@ua_payload")
        end

        it "send push notifications" do
          @client.push([@identifier.id], @basic_payload)

          assert_requested(:post, "https://go.urbanairship.com/api/push/")
        end

        describe "notification request body" do
          it "send ios push notifications with a correct body" do
            auth_header = { "Authorization" => "Basic RXJDRUQ3RzlRYy1GcWFVVWV1UGY1UTpmbzlLY1FnOFN6NnFlUkI5TERwU3JB" }
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal PushNotifications::UrbanAirshipPayloadV3.new.headers.merge(auth_header), request.headers
                assert_equal ["ios"], request_body["device_types"]
                assert request_body["notification"].key?("ios")
                assert_equal @basic_payload[:msg], request_body["notification"]["ios"]["alert"]
                assert_equal "+1", request_body["notification"]["ios"]["badge"]
                assert_equal "notification.aif", request_body["notification"]["ios"]["sound"]
                assert_equal @basic_payload[:user_id], request_body["notification"]["ios"]["extra"]["id"]
                assert_equal @basic_payload[:ticket_id], request_body["notification"]["ios"]["extra"]["tid"]
              end

            @client.push([@identifier.id], @basic_payload)
          end

          it "sends to ios device token" do
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal({
                  "device_token" => [@identifier.token]
                }, request_body["audience"])
              end

            @client.push([@identifier.id], @basic_payload)
          end

          it "sends to ios channel" do
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal({
                  "ios_channel" => [@ios_channel_identifier.token]
                }, request_body["audience"])
              end

            @client.push([@ios_channel_identifier.id], @basic_payload)
          end

          it "sends to both ios device token and channel" do
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal({
                  "OR" => [
                    {"device_token" => [@identifier.token]},
                    {"ios_channel" => [@ios_channel_identifier.token]}
                  ]
                }, request_body["audience"])
              end

            @client.push([@identifier.id, @ios_channel_identifier.id], @basic_payload)
          end

          it "send android push notifications with a correct body" do
            auth_header = { "Authorization" => "Basic RXJDRUQ3RzlRYy1GcWFVVWV1UGY1UTpmbzlLY1FnOFN6NnFlUkI5TERwU3JB" }
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal PushNotifications::UrbanAirshipPayloadV3.new.headers.merge(auth_header), request.headers
                assert_equal ["android"], request_body["device_types"]
                assert request_body["notification"].key?("android")
                assert_equal @basic_payload[:msg_short], request_body["notification"]["android"]["alert"]
                assert_equal @basic_payload[:ticket_id].to_s, request_body["notification"]["android"]["extra"]["tid"]
              end

            @client.push([@android_identifier.id], @basic_payload)
          end

          it "sends to android apids" do
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal({
                  "apid" => [@android_identifier.token]
                }, request_body["audience"])
              end

            @client.push([@android_identifier.id], @basic_payload)
          end

          it "sends to android channel" do
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal({
                  "android_channel" => [@android_channel_identifier.token]
                }, request_body["audience"])
              end

            @client.push([@android_channel_identifier.id], @basic_payload)
          end

          it "sends to both android apid and channels" do
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal({
                  "OR" => [
                    {"apid" => [@android_identifier.token]},
                    {"android_channel" => [@android_channel_identifier.token]}
                  ]
                }, request_body["audience"])
              end

            @client.push([@android_identifier.id, @android_channel_identifier.id], @basic_payload)
          end

          it "sends to windows apid" do
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal({
                  "wns" => [@windows_identifier.token]
                }, request_body["audience"])
              end

            @client.push([@windows_identifier.id], @basic_payload)
          end

          it "send windows push notifications with a correct body" do
            auth_header = { "Authorization" => "Basic RXJDRUQ3RzlRYy1GcWFVVWV1UGY1UTpmbzlLY1FnOFN6NnFlUkI5TERwU3JB" }
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_body = JSON.parse(request.body)
                assert_equal PushNotifications::UrbanAirshipPayloadV3.new.headers.merge(auth_header), request.headers
                assert_equal ["wns"], request_body["device_types"]
                assert request_body["notification"].key?("wns")
                assert request_body["notification"]["wns"].key?("toast")
                assert request_body["notification"]["wns"]["toast"].key?("binding")
                assert request_body["notification"]["wns"]["toast"]["binding"].key?("template")
                assert request_body["notification"]["wns"]["toast"]["binding"].key?("text")
                assert request_body["notification"]["wns"]["toast"]["binding"]["text"].is_a?(Array)
                assert_equal @basic_payload[:msg_short], request_body["notification"]["wns"]["toast"]["binding"]["text"][0]
                assert_equal @basic_payload[:ticket_id].to_s, request_body["notification"]["wns"]["toast"]["binding"]["text"][2]
              end

            @client.push([@windows_identifier.id], @basic_payload)
          end
        end

        it "not send to non-ios, non-android and non-windows clients" do
          ios = @identifier
          android = PushNotifications::FirstPartyDeviceIdentifier.create!(
            token: "foofoofoofoofoofoofoo1",
            device_type: android_device_type,
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )
          banana = PushNotifications::FirstPartyDeviceIdentifier.create!(
            token: "foofoofoofoofoofoofoo2",
            device_type: "banana",
            user: minimum_agent,
            mobile_app: zendesk_agent_mobile_app
          )
          @client.expects(:push_to_device_type).twice
          @client.push([android.id, ios.id, banana.id], @basic_payload)
        end

        describe "with invalid device_types" do
          before do
            @foo = PushNotifications::FirstPartyDeviceIdentifier.create!(
              token: "asdfasdfasdfasdfasdfasdfasdfasdfdsfasdf",
              device_type: "amazon",
              user: minimum_agent,
              mobile_app: zendesk_agent_mobile_app
            )
          end

          it "not push" do
            @client.expects(:push_to_android).never
            @client.expects(:push_to_ios).never
            @client.push([@foo.id], @basic_payload)
          end
        end

        describe "with clearly invalid tokens" do
          before do
            @phoney_phone = devices(:minimum)
          end

          it "not push to devices with token 'none'" do
            @phoney_phone.stubs(:token).returns("none")
            @phoney_phone.stubs(:device_type).returns("android")
            @client.expects(:push_to_android).never
          end

          it "not push to devices with tokens less than 10 characters" do
            @phoney_phone.stubs(:token).returns("1234")
            @phoney_phone.stubs(:device_type).returns("android")
            @client.expects(:push_to_android).never
          end
        end

        describe "causing exceptions" do
          it "not return garbage" do
            @client.class.expects(:post).raises(Timeout::Error.new("Boo!"))
            assert_equal [], @client.push([@identifier.id], @basic_payload)
          end

          it "fail silently for Timeout::Errors" do
            @client.class.expects(:post).raises(Timeout::Error.new("Boo!"))
            Rails.logger.expects(:warn).with { |msg| msg =~ /UrbanAirshipClient Timeout::Error/ }
            @client.push([@identifier.id], @basic_payload)
          end

          it "fail silently for SocketError Name or service not known" do
            @client.class.expects(:post).raises(SocketError.new("getaddrinfo: Name or service not known"))
            Rails.logger.expects(:warn).with { |msg| msg =~ /UrbanAirshipClient SocketError/ }
            @client.push([@identifier.id], @basic_payload)
          end

          it "fail noisily with unknown SocketError" do
            @client.class.expects(:post).raises(SocketError.new("Fuubar"))
            assert_raise(SocketError) { @client.push([@identifier.id], @basic_payload) }
          end

          it "fail noisily for RuntimeErrors" do
            @client.class.expects(:post).raises(RuntimeError.new("Boo!"))
            assert_raise(RuntimeError) { @client.push([@identifier.id], @basic_payload) }
          end
        end

        it "truncate long comments on ios push messages" do
          long_comment = "a" * 300
          truncated_message = @payload_model.send(:truncate_ios_message, 1234567, long_comment, "+1", 1234)
          apn_push_payload = @payload_model.send(:apn_push_payload, 1234567, "+1", "", 1234).to_json.bytesize
          assert_equal(253 - apn_push_payload, truncated_message.length)

          long_when_serialized = read_test_file('long_push_notification_message.txt')
          truncated_message = @payload_model.send(:truncate_ios_message, 1234567, long_when_serialized, "+1", 1234)
          payload_length = @payload_model.send(:apn_push_payload, 1234567, "+1", truncated_message, 1234).to_json.bytesize
          assert(payload_length <= 256)

          unicode_message = read_test_file('unicode_push_notification_message.txt')
          truncated_message = @payload_model.send(:truncate_ios_message, 1234567, unicode_message, "+1", 1234)
          payload_length = @payload_model.send(:apn_push_payload, 1234567, "+1", truncated_message, 1234).to_json.bytesize
          assert_equal(250, payload_length)
        end

        it "allow for unicode chars taking up two bytes when truncating" do
          problem_message = read_test_file('unicode_push_notification_message_2.txt')
          truncated_message = @payload_model.send(:truncate_ios_message, 1234567, problem_message, "+1", 1234)
          payload_length = @payload_model.send(:apn_push_payload, 1234567, "+1", truncated_message, 1234).to_json.bytesize
          assert_equal(254, payload_length)

          table_flip = read_test_file('unicode_push_notification_message_3.txt') * 30
          truncated_message = @payload_model.send(:truncate_ios_message, 1234567, table_flip, "+1", 1234)
          payload_length = @payload_model.send(:apn_push_payload, 1234567, "+1", truncated_message, 1234).to_json.bytesize
          assert_equal(253, payload_length)
        end

        it "truncate at the start of the word that exceeds bytes available and add an ellipsis" do
          one_seventy_three_byte_string = read_test_file('unicode_push_notification_message_4.txt')
          truncated_expected = read_test_file('unicode_push_notification_message_5.txt')

          truncated_message = @payload_model.send(:truncate_ios_message, 1234567, one_seventy_three_byte_string, "+1", 1234)
          assert_equal(truncated_expected, truncated_message)

          payload_length = @payload_model.send(:apn_push_payload, 1234567, "+1", truncated_message, 1234).to_json.bytesize
          assert_equal(252, payload_length)
        end

        it "remove line breaks on ios push messages and replace them with a single space" do
          line_break_comment = "first line\n\nsecond line\r\n\r\nthird line"

          truncated_message = @payload_model.send(:truncate_ios_message, 1234567, line_break_comment, "+1", 1234)
          assert_equal("first line second line third line", truncated_message)
        end

        describe "for android push messages" do
          before do
            @android_payload = {msg: 'bar', msg_short: 'baz', user_id: 1234567, ticket_id: 1234}
          end

          it "produce a message body that is #{PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_ANDROID} characters" do
            alert = "foo me dooby do"
            message = { alert: alert, extra: { tid: "123123" } }
            message = @payload_model.send(:truncate_android_payload, message)

            assert (PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_ANDROID >= message.to_json.size)
          end

          describe "for android push messages foo" do
            it "produce a message body that is #{PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_ANDROID} characters" do
              alert = "x" * 1025
              message = { alert: alert, extra: { tid: "123123" } }
              message = @payload_model.send(:truncate_android_payload, message)

              assert (PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_ANDROID >= message.to_json.size)
            end
          end

          it "truncate long comments on android push messages" do
            alert = "a" * 1024
            message = { alert: alert, extra: { tid: "123123" } }
            message = @payload_model.send(:truncate_android_payload, message)

            assert (message[:alert].length + {alert: {tid: "123123"}}.to_json.size <= 1024)
          end

          describe "android push messages with unicode chars" do
            it "allow for unicode chars taking up two bytes when truncating" do
              alert = read_test_file('unicode_push_notification_message_6.txt')

              message = { alert: alert, extra: { tid: "123123" } }
              message = @payload_model.send(:truncate_android_payload, message)

              assert_equal read_test_file_no_new_lines('unicode_push_notification_message_7.txt').bytesize,
                message[:alert].bytesize
              assert (PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_ANDROID >= message.to_json.bytesize)
            end
          end

          it "truncate at the start of the word that exceeds bytes available and add an ellipsis" do
            original_alert = read_test_file('unicode_push_notification_message_6.txt')
            truncated_expected = read_test_file_no_new_lines('unicode_push_notification_message_7.txt')

            message = { alert: original_alert, extra: { tid: "123123"} }
            truncated_message = @payload_model.send(:truncate_android_payload, message)

            assert_equal(truncated_expected, truncated_message[:alert])
            assert (PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_ANDROID >= message.to_json.size)
          end

          it "send collapse key if arturo feature is enabled" do
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_data = JSON.parse(request.body)
                case @client.version
                when 2
                  assert request_data["android"].key?("collapse_key")
                when 3
                  assert request_data["notification"]["android"].key?("collapse_key")
                end
              end
            @client.push([@android_identifier.id], @android_payload, enable_collapse_key: true)
          end

          it "not send collapse key if arturo feature is disabled" do
            stub_request(:post, "https://go.urbanairship.com/api/push/").
              with(basic_auth: [key, master_secret]).
              with do |request|
                request_data = JSON.parse(request.body)
                case @client.version
                when 2
                  refute request_data["android"].key?("collapse_key")
                when 3
                  refute request_data["notification"]["android"].key?("collapse_key")
                end
              end
            @client.push([@android_identifier.id], @android_payload, enable_collapse_key: false)
          end
        end

        describe "for windows push messages" do
          before do
            @ticket_id = 1234
            @windows_payload = {msg: 'bar', msg_short: 'baz', user_id: 1234567, ticket_id: @ticket_id}
          end

          it "produce a message body that is #{PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_WINDOWS} characters" do
            alert = "foo me dooby do"
            message = {
              toast: {
                binding: {
                  template: "ToastText01",
                  text: [alert, "", @ticket_id.to_s]
                }
              }
            }

            message = @payload_model.send(:truncate_windows_payload, message, @ticket_id)

            assert (PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_WINDOWS >= message.to_json.size)
          end

          describe "for windows push messages foo" do
            it "produce a message body that is #{PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_WINDOWS} characters" do
              alert = "x" * 1025
              message = {
                toast: {
                  binding: {
                    template: "ToastText01",
                    text: [alert, "", @ticket_id.to_s]
                  }
                }
              }
              message = @payload_model.send(:truncate_windows_payload, message, @ticket_id)

              assert (PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_WINDOWS >= message.to_json.size)
            end
          end

          it "truncate long comments on windows push messages" do
            alert = "a" * 5000
            message = {
              toast: {
                binding: {
                  template: "ToastText01",
                  text: [alert, "", @ticket_id.to_s]
                }
              }
            }

            message = @payload_model.send(:truncate_windows_payload, message, @ticket_id)

            assert(message[:toast][:binding][:text][0].length + {toast: {binding: {template: "ToastText01"}}}.to_json.size <= 5000)
          end

          describe "windows push messages with unicode chars" do
            it "allow for unicode chars taking up two bytes when truncating" do
              alert = read_test_file('unicode_push_notification_message_8.txt')

              message = {
                toast: {
                  binding: {
                    template: "ToastText01",
                    text: [alert, "", @ticket_id.to_s]
                  }
                }
              }

              message = @payload_model.send(:truncate_windows_payload, message, @ticket_id)

              assert_equal read_test_file_no_new_lines('unicode_push_notification_message_9.txt').bytesize,
                message[:toast][:binding][:text][0].bytesize
              assert(PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_WINDOWS >= message.to_json.bytesize)
            end
          end

          it "truncate at the start of the word that exceeds bytes available and add an ellipsis" do
            original_alert = read_test_file('unicode_push_notification_message_8.txt')
            truncated_expected = read_test_file_no_new_lines('unicode_push_notification_message_9.txt')

            message = {
              toast: {
                binding: {
                  template: "ToastText01",
                  text: [original_alert, "", @ticket_id.to_s]
                }
              }
            }

            truncated_message = @payload_model.send(:truncate_windows_payload, message, @ticket_id)

            assert_equal(truncated_expected, truncated_message[:toast][:binding][:text][0])
            assert(PushNotifications::UrbanAirshipPayloadBase::MAX_PAYLOAD_WINDOWS >= message.to_json.size)
          end
        end

        describe "feedback service" do
          it "fetch inactivated device tokens" do
            @client.tokens_deactivated_since(@since_time)

            assert_requested(:get, "https://go.urbanairship.com/api/device_tokens/feedback?since=#{@since_time.iso8601}")
          end
        end
      end
    end # each/describe
  end # each
end
