require_relative "../support/test_helper"

SingleCov.covered!

describe TrialExtension do
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:default_duration) { TrialExtension::TRIAL_EXTENSION_DEFAULT_DURATION }

  it 'has default duration' do
    trial_extension = TrialExtension.new(account: account)
    assert_equal default_duration, trial_extension.duration
  end

  it 'does not be valid until account and author assigned' do
    trial_extension = TrialExtension.new

    refute trial_extension.valid?
    trial_extension.account = account

    refute trial_extension.valid?
    trial_extension.author = account.owner

    assert trial_extension.valid?
  end

  it 'assigns author account_id when author is added' do
    trial_extension = TrialExtension.new(account: account)

    assert_nil trial_extension.author_account_id
    assert_nil trial_extension.author_user_id

    trial_extension.author = account.owner

    assert_equal account.id, trial_extension.author_account_id
    assert_equal account.owner.id, trial_extension.author_user_id
  end

  it 'extends trial expiration after save' do
    account.subscription.trial_expires_on = Date.today - 1.days
    account.subscription.save

    trial_extension = TrialExtension.new(account: account, author: account.owner)
    trial_extension.save
    assert_equal Date.today + default_duration.days, account.subscription.trial_expires_on

    trial_extension = TrialExtension.new(account: account, author: account.owner, duration: 10)
    trial_extension.save
    assert_equal Date.today + (10 + default_duration).days, account.subscription.trial_expires_on
  end

  it 'allows massive assignments of account, author, duration, lifecycle_survey_answers' do
    trial_extension = TrialExtension.new(
      account: account,
      author: account.owner,
      duration: default_duration,
      lifecycle_survey_answers: [LifecycleSurveyAnswer.new]
    )

    assert_not_nil trial_extension.account
    assert_not_nil trial_extension.author_account_id
    assert_not_nil trial_extension.author_user_id
    assert_not_nil trial_extension.lifecycle_survey_answers
  end
end
