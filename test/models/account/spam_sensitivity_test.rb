require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::SpamSensitivity do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  describe "default values" do
    it "spam_threshold_multiplier is set to 1.0" do
      assert_equal 1.0, account.settings.spam_threshold_multiplier
    end
  end

  describe '#spam_protection_tool' do
    subject { account.spam_protection_tool }

    describe_with_arturo_disabled(:email_rspamd) do
      it { assert_equal subject, Account::SpamSensitivity::CLOUDMARK }
    end

    describe_with_arturo_enabled(:email_rspamd) do
      it { assert_equal subject, Account::SpamSensitivity::RSPAMD }
    end
  end

  describe "#spam_threshold_multiplier" do
    subject { account.spam_threshold_multiplier }

    before { account.settings.spam_threshold_multiplier = 5.0 }

    it "delegates the method to settings" do
      assert_equal 5.0, subject
    end
  end
end
