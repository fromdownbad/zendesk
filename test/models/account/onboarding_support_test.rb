require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::OnboardingSupport do
  let(:account) { accounts(:minimum) }

  describe "#onboarding_url" do
    it 'returns mobile sdk url for accounts that signed up via mobile sdk landing page' do
      account.trial_extras.create!(key: 'features', value: 'sdk')

      assert_equal '/agent/admin/mobile_sdk', account.onboarding_url
    end

    it 'returns Talk url for accounts that signed up via Talk landing page' do
      account.trial_extras.create!(key: 'product_sign_up', value: 'zendesk_talk')

      assert_equal '/voice/admin/onboarding', account.onboarding_url
    end

    it 'returns Guide url for accounts that signed up via Guide landing page' do
      account.trial_extras.create!(key: 'product_sign_up', value: 'zendesk_guide')

      assert_equal '/hc/start', account.onboarding_url
    end

    it 'returns Gather url for accounts that signed up via Gather landing page' do
      account.trial_extras.create!(key: 'product_sign_up', value: 'zendesk_gather')

      assert_equal '/hc/start?gather=true', account.onboarding_url
    end

    it 'returns nil when there is no custom onboarding url' do
      account.stubs(:trial_extras_feature?).returns(false)
      account.stubs(:trial_extras_product_sign_up?).returns(false)

      assert_nil account.onboarding_url
    end

    describe "when customer onboards after clicking buy now" do
      before do
        TrialExtra.destroy_all
        account.trial_extras.create!(key: 'buy_now', value: 'true')
        account.trial_extras.create!(key: 'product_sign_up', value: product)
        account.trial_extras.create!(key: 'plan', value: 'essential')
      end

      describe "and the product is support" do
        let(:product) { 'zendesk_support' }

        it 'returns billing shopping cart url for accounts that signed up via direct buy support' do
          assert_equal '/billing/entry/purchase/support?plan=essential', account.onboarding_url
        end
      end

      describe "and the product is zendesk_suite" do
        let(:product) { 'zendesk_suite' }

        it 'returns billing shopping cart url for accounts that signed up via direct buy for zendesk suite' do
          assert_equal '/billing/entry/purchase/zendesk_suite?plan=essential', account.onboarding_url
        end
      end
    end

    describe "suite trial accounts" do
      before do
        account.settings.suite_trial = true
        account.save!
      end

      it "redirects to sample-ticket when suite_trial is active" do
        assert_equal '/agent/get-started/sample-ticket', account.onboarding_url
      end

      describe "account is not in suite trial" do
        before do
          account.settings.suite_trial = false
          account.save!
        end

        it "does not redirect to sample-ticket" do
          assert_not_equal '/agent/get-started/sample-ticket', account.onboarding_url
        end
      end
    end
  end
end
