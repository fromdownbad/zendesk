require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::TrialExtrasSupport do
  let(:account) { accounts(:minimum) }
  let(:redis_client) { Zendesk::RedisStore.redis_client = FakeRedis::Redis.new }

  before do
    account.stubs(:redis_client).returns(redis_client)
  end

  after do
    redis_client.flushall
  end

  describe '#add_trial_extras' do
    let(:statsd_stub) { stub_for_statsd }

    let(:trial_extras_hash) do
      {
        'features' => 'sdk',
        'product_sign_up' => 'zendesk_talk'
      }
    end

    before do
      Zendesk::StatsD::Client.stubs(:new).returns(statsd_stub)
    end

    describe 'when new record' do
      before do
        account.stubs(:new_record?).returns(true)
        TrialExtra.destroy_all
      end

      it 'creates trial_extras from trial_extras_hash' do
        account.send(:add_trial_extras, trial_extras_hash)
        account.stubs(:new_record?).returns(false)
        account.save!

        assert_equal trial_extras_hash, account.trial_extras.to_hash
      end

      it 'does not save trial extras to redis' do
        redis_client.expects(:setex).never
      end

      it 'tracks success to statsd when trial_extras_hash is present' do
        statsd_stub.expects(:increment).with('count', has_entries(tags: includes('status:success', 'precreated:false', "account_id:#{account.id}")))
        account.send(:add_trial_extras, trial_extras_hash)
      end

      it 'does not track success to statsd when trial_extras_hash is NOT present' do
        statsd_stub.expects(:increment).never
        account.send(:add_trial_extras)
      end
    end

    describe 'when existing record' do
      it 'saves trial extras to redis' do
        account.send(:add_trial_extras, trial_extras_hash)
        assert_equal trial_extras_hash, account.send(:redis_trial_extras)
      end

      it 'tracks success to statsd' do
        statsd_stub.expects(:increment).with('count', has_entries(tags: includes('status:success', 'precreated:true', "account_id:#{account.id}")))
        account.send(:add_trial_extras, trial_extras_hash)
      end

      describe 'when redis error' do
        it 'writes trial extras to db' do
          TrialExtra.destroy_all
          redis_client.expects(:setex).raises(StandardError)
          account.send(:add_trial_extras, trial_extras_hash)
          account.save!

          assert_equal trial_extras_hash, account.trial_extras.to_hash
        end

        it 'tracks failure to statsd' do
          redis_client.expects(:setex).raises(StandardError)
          statsd_stub.expects(:increment).with('count', has_entries(tags: includes('status:failure', 'precreated:true', "account_id:#{account.id}")))

          account.send(:add_trial_extras, trial_extras_hash)
        end
      end
    end
  end

  describe '#trial_extras_feature?' do
    before do
      TrialExtra.destroy_all
      account.stubs(:redis_trial_extras).returns({})
    end

    describe 'when feature exists' do
      it 'returns true when there is only one feature' do
        account.trial_extras.create!(key: 'features', value: 'sdk')
        assert account.send(:trial_extras_feature?, 'sdk')
      end

      it 'returns true when there are multiple features' do
        account.trial_extras.create!(key: 'features', value: 'other,something,sdk')
        assert account.send(:trial_extras_feature?, 'sdk')
      end
    end

    describe 'when feature does NOT exist' do
      it 'returns falsy when there is only one feature' do
        account.trial_extras.create!(key: 'features', value: 'nosdk')
        refute account.send(:trial_extras_feature?, 'sdk')
      end

      it 'returns falsy when there are multiple features' do
        account.trial_extras.create!(key: 'features', value: 'other,something,nosdk')
        refute account.send(:trial_extras_feature?, 'sdk')
      end
    end

    it 'returns falsy when there are no trial extras' do
      refute account.send(:trial_extras_feature?, 'sdk')
    end
  end

  describe '#trial_extras_product_sign_up?' do
    let(:product_name) { 'zendesk_talk' }

    before do
      TrialExtra.destroy_all
      account.stubs(:redis_trial_extras).returns({})
    end

    it 'returns true when trial extras product_sign_up matches product_name' do
      account.trial_extras.create!(key: 'product_sign_up', value: product_name)
      assert account.send(:trial_extras_product_sign_up?, product_name)
    end

    it 'returns false when trial extras product_sign_up does NOT match product_name' do
      account.trial_extras.create!(key: 'product_sign_up', value: 'not_talk')
      refute account.send(:trial_extras_product_sign_up?, product_name)
    end

    it 'returns false when there are no trial extras' do
      refute account.send(:trial_extras_product_sign_up?, product_name)
    end
  end

  describe "#trial_extras_buy_now?" do
    let(:product_name) { "zendesk_support" }

    before do
      TrialExtra.destroy_all
      account.stubs(:redis_trial_extras).returns({})
    end

    it "returns true when trial_extras have direct_buy support params" do
      account.trial_extras.create!(key: 'buy_now', value: "true")
      account.trial_extras.create!(key: 'product_sign_up', value: "zendesk_support")

      assert account.trial_extras_buy_now?('zendesk_support')
    end

    it "returns false when trial_extras do not have direct_buy support params" do
      refute account.trial_extras_buy_now?('zendesk_support')
    end
  end

  describe "#zendesk_suite_trial_extras_buy_now?" do
    let(:product_name) { "zendesk_suite" }

    before do
      TrialExtra.destroy_all
      account.stubs(:redis_trial_extras).returns({})
      account.trial_extras.create!(key: 'buy_now', value: "true")
      account.trial_extras.create!(key: 'product_sign_up', value: "zendesk_suite")
    end

    describe_with_arturo_enabled :billing_buy_now_zendesk_suite do
      it "returns true when trial_extras have direct_buy zendesk suite params" do
        assert account.zendesk_suite_trial_extras_buy_now?
      end
    end

    describe_with_arturo_disabled :billing_buy_now_zendesk_suite do
      it "returns false because the arturo is disabled" do
        refute account.zendesk_suite_trial_extras_buy_now?
      end
    end
  end

  describe "#trial_extras_support_plan" do
    let(:product_name) { "zendesk_support" }

    before do
      TrialExtra.destroy_all
      account.stubs(:redis_trial_extras).returns({})
      account.trial_extras.create!(key: 'plan', value: "essential")
    end

    it "returns plan from trial_extras" do
      assert "essential", account.trial_extras_plan
    end
  end

  describe_with_arturo_enabled :use_alternative_trial_length do
    before do
      TrialExtra.destroy_all
      account.stubs(:redis_trial_extras).returns({})
    end

    it 'returns true when the value of alternative_length is set to "true"' do
      account.trial_extras.create!(key: 'alternative_length', value: "true")
      assert account.send(:trial_extras_alternative_length?)
    end

    it 'returns true when the value of alternative_length is set to true' do
      account.trial_extras.create!(key: 'alternative_length', value: true)
      assert account.send(:trial_extras_alternative_length?)
    end

    it 'returns false when value of alternative_length is set to something other than "true" or true' do
      account.trial_extras.create!(key: 'alternative_length', value: 'bloop')
      refute account.send(:trial_extras_alternative_length?)
    end

    it 'returns false when alternative_length is not set' do
      refute account.send(:trial_extras_alternative_length?)
    end

    it 'when classic_14_day_trial arturo is set returns true when the alternative_length is not set' do
      Arturo.enable_feature!(:classic_14_day_trial)
      assert account.send(:trial_extras_alternative_length?)
    end
  end
end
