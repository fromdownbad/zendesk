require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::TrialExtensionSupport do
  fixtures :accounts
  let(:account) { accounts(:minimum) }

  describe 'is included in account' do
    it 'responds to trial_extensions' do
      account.respond_to? :trial_extensions
    end
  end
end
