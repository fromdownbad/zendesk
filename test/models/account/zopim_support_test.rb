require_relative '../../support/test_helper'

SingleCov.covered!

describe 'Account::ZopimSupport' do
  fixtures :accounts

  def stub_zopim_subscription(is_phase_three, is_cancelled = false)
    stub(
      zopim_account_id: 1234,
      phase_three?: is_phase_three,
      is_cancelled?: is_cancelled,
      destroy: true
    )
  end

  describe 'an account' do
    let(:account) { accounts(:minimum) }

    it 'should have a zopim_integration association' do
      assert account.respond_to? :zopim_integration
    end

    it 'should have a zopim_subscription association' do
      assert account.respond_to? :zopim_subscription
    end

    it 'should have a zopim_agents association' do
      assert account.respond_to? :zopim_agents
    end

    it 'should have a zopim_account_id attribute' do
      assert account.respond_to? :zopim_account_id
    end

    describe 'updating its subdomain' do
      let(:zopim_subscription) { stub_zopim_subscription(false) }

      describe 'when a zopim_subscription is present' do
        before do
          Zopim::Reseller.client.stubs(:update_account!)
          account.stubs(zopim_subscription: zopim_subscription)
        end

        it 'should also update its Zopim Reseller record' do
          Zopim::Reseller.client.expects(:update_account!).
            with(id: 1234, data: { zendesk_subdomain: 'somedomain' }).once
          account.update_attribute(:subdomain, 'somedomain')
        end

        it 'should update the subdomain field' do
          account.update_attribute(:subdomain, 'somedomain')
          assert_equal 'somedomain', account.subdomain
        end

        describe 'when zopim_subscription is cancelled' do
          let(:zopim_subscription) { stub_zopim_subscription(false, true) }

          it 'does not update Zopim Reseller record' do
            Zopim::Reseller.client.expects(:update_account!).never
            account.update_attribute(:subdomain, 'somedomain')
          end
        end
      end

      describe 'when there is no zopim_subscription' do
        before do
          account.stubs(zopim_subscription: nil)
        end

        it 'should not attempt to sync with Zopim' do
          Zopim::Reseller.client.expects(:update_account!).never
          account.update_attribute(:subdomain, 'somedomain')
        end
      end

      describe 'when the subscription is phase 3' do
        let(:zopim_subscription) { stub_zopim_subscription(true) }

        it 'should not attempt to sync with Zopim' do
          Zopim::Reseller.client.expects(:update_account!).never
          account.update_attribute(:subdomain, 'somedomain')
        end
      end
    end

    describe 'destroying zopim subscription' do
      let(:zopim_subscription) { stub_zopim_subscription(false) }

      before do
        account.update_attributes(is_serviceable: false, is_active: false)
        account.stubs(zopim_subscription: zopim_subscription)
      end

      describe 'when account gets soft deleted' do
        it 'destroys zopim subscription' do
          zopim_subscription.expects(:destroy)
          account.soft_delete!
        end
      end
    end

    describe 'canceling chat product record' do
      let(:zopim_subscription) { stub_zopim_subscription(true) }
      let(:client) { stub(update_product: true) }

      before do
        account.update_attributes(is_serviceable: false, is_active: false)
        account.stubs(zopim_subscription: zopim_subscription)
      end

      describe 'when the account gets soft deleted' do
        it 'destroys zopim_subscription' do
          zopim_subscription.expects(:destroy)
          account.soft_delete!
        end
      end
    end
  end
end
