require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'Account::Fraud' do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:pravda_response_body) { '{"account":{"is_abusive":true}}' }
  let(:sell_free_product) { FactoryBot.build(:sell_free_product) }

  describe '#auto_suspended?' do
    describe 'when account is not abusive' do
      before { account.stubs(:abusive?).returns(false) }

      it 'is not an auto suspended account' do
        refute account.auto_suspended?
      end
    end

    describe 'when account is abusive' do
      before { account.stubs(:abusive?).returns(true) }

      describe 'and fraud score was an auto-suspension' do
        before do
          FactoryBot.create(:fraud_score, account: account, source_event: Fraud::SourceEvent::TRIAL_SIGNUP, verified_fraud: true)
        end

        it 'is an auto suspended account' do
          assert account.auto_suspended?
        end
      end

      describe 'and fraud score was not an auto-suspension' do
        before do
          FactoryBot.create(:fraud_score, account: account, source_event: Fraud::SourceEvent::TAKEN_OVER_ACCOUNT, verified_fraud: true)
        end

        it 'is not an auto suspended account' do
          refute account.auto_suspended?
        end
      end
    end
  end

  describe '#abusive?' do
    describe "when an accounts settings.is_abusive is set to true" do
      before do
        account.settings.is_abusive = true
        account.save!
      end
      it "returns true if account is_abusive" do
        assert account.abusive?
      end

      it "returns false if account whitelisted" do
        account.stubs(:whitelisted?).returns(true)
        refute account.abusive?
      end
    end

    # TODO: add more test cases here for the different return values
    describe "when pravda_client.abusive returns false" do
      before { Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns(false) }

      it "returns false if account is not is_abusive" do
        account.fraud_scores.create!(score: false, verified_fraud: false, actions: {support_risky: false})
        account.settings.is_abusive = false
        account.save!
        refute account.abusive?
      end
    end
  end

  describe "#allow_email_template_customization?" do
    describe "when an account is trial" do
      before do
        account.subscription.update_attribute(:is_trial, true)
      end

      describe "and the arturo to restrict template editing is on" do
        it "returns false if trial does not have a fraud score" do
          refute account.allow_email_template_customization?
        end

        it "returns false if the trial is considered support risky in a fraud score" do
          FactoryBot.create(:fraud_score, account_id: account.id, actions: {support_risky: true})
          refute account.allow_email_template_customization?
        end

        it "returns true if the trial is not classified as support risky in a fraud score" do
          FactoryBot.create(:fraud_score, account_id: account.id, actions: {support_risky: false})
          assert account.allow_email_template_customization?
        end

        describe "if the account is whitelisted" do
          before do
            account.stubs(:whitelisted?).returns(true)
            account.settings.save!
          end

          it "returns true" do
            assert account.allow_email_template_customization?
          end
        end
      end
    end

    describe "when an account is paid" do
      before { account.subscription.update_attribute(:is_trial, false) }
      describe "and the arturo to restrict template editing is on" do
        describe "and risk score is less than threshold" do
          before do
            FactoryBot.create(:fraud_score, account_id: account.id, actions: {support_risky: true}, score_params: {'insights_risk_score' => Account::FraudSupport::MAXMIND_FRAUD_SCORE_THRESHOLD - 5})
          end

          describe_with_and_without_arturo_enabled :orca_classic_disable_email_template_risky_account do
            describe 'and the account is outbound email safe' do
              before { account.stubs(:outbound_email_safe?).returns(true) }
              it "returns true" do
                assert account.allow_email_template_customization?
              end
            end
          end

          describe "and the account is NOT outbound email safe" do
            before { account.stubs(:outbound_email_safe?).returns(false) }
            describe_with_arturo_enabled :orca_classic_disable_email_template_risky_account do
              it "returns false" do
                refute account.allow_email_template_customization?
              end
            end

            describe_with_arturo_disabled :orca_classic_disable_email_template_risky_account do
              it "returns true" do
                assert account.allow_email_template_customization?
              end
            end
          end
        end

        describe "and risk score is threshold or above" do
          before do
            FactoryBot.create(:fraud_score, account_id: account.id, actions: {support_risky: true}, score_params: {'insights_risk_score' => Account::FraudSupport::MAXMIND_FRAUD_SCORE_THRESHOLD})
          end

          it "returns false" do
            refute account.allow_email_template_customization?
          end

          describe "and the account is sales-assisted" do
            before do
              zuora_subscription = mock(assisted?: true)
              account.subscription.stubs(:zuora_subscription).returns(zuora_subscription)
            end

            it "returns true" do
              assert account.allow_email_template_customization?
            end
          end
        end

        describe "and account doesn't have any fraud scores" do
          it "returns true" do
            assert_empty account.fraud_scores
            assert account.allow_email_template_customization?
          end
        end

        describe "and fraud score is nil" do
          let(:fraud_score) { nil }

          it "returns true" do
            assert account.allow_email_template_customization?
          end
        end

        describe "if the whitelisted_from_fraud_restrictions account setting is set" do
          before do
            account.stubs(:whitelisted?).returns(true)
            account.settings.save!
          end

          describe "and risk score is less than threshold" do
            before do
              FactoryBot.create(:fraud_score, account_id: account.id, actions: {support_risky: true}, score_params: {'insights_risk_score' => Account::FraudSupport::MAXMIND_FRAUD_SCORE_THRESHOLD - 5})
            end
            it "returns true" do
              assert account.allow_email_template_customization?
            end
          end

          describe "and risk score is threshold or above" do
            before do
              FactoryBot.create(:fraud_score, account_id: account.id, actions: {support_risky: true}, score_params: {'insights_risk_score' => Account::FraudSupport::MAXMIND_FRAUD_SCORE_THRESHOLD})
            end
            it "returns false" do
              assert account.allow_email_template_customization?
            end
          end
        end
      end
    end
  end

  describe '#sell_product?' do
    describe 'when the account service says an account has a sell product' do
      before do
        # The cached method calls /products and not /product
        stub_products_response(:get, products: [sell_free_product])
      end
      describe_with_arturo_enabled :orca_classic_pravda_sell_product do
        it 'returns true' do
          assert account.sell_product?
        end
      end

      describe_with_arturo_disabled :orca_classic_pravda_sell_product do
        it 'returns false' do
          refute account.sell_product?
        end
      end
    end

    describe 'when the account service says an account does not have a sell product' do
      before { stub_products_response(:get, products: []) }
      describe_with_and_without_arturo_enabled :orca_classic_pravda_sell_product do
        it 'returns false' do
          refute account.sell_product?
        end
      end
    end
  end

  describe '#premium_support' do
    describe 'when an account has the priority_support add on' do
      before do
        account.subscription_feature_addons.create!(
          name:               'priority_support',
          zuora_rate_plan_id: '123456'
        )
      end

      it 'returns true' do
        assert account.premium_support?
      end
    end

    describe 'when an account has the premier_support add on' do
      before do
        account.subscription_feature_addons.create!(
          name:               'premier_support',
          zuora_rate_plan_id: '123456'
        )
      end

      it 'returns true' do
        assert account.premium_support?
      end
    end

    describe 'when an account does not have a premium support add on' do
      it 'returns false' do
        refute account.premium_support?
        refute_nil account.premium_support?
      end
    end
  end

  describe '#support_risky?' do
    it "returns true if at least one fraud score is support risky" do
      account.fraud_scores.create!(score: true, verified_fraud: false, actions: {support_risky: true})
      2.times { account.fraud_scores.create!(score: true, verified_fraud: false, actions: {support_risky: false}) }
      assert account.support_risky?
    end

    it "returns false if no fraud scores are support risky" do
      2.times { account.fraud_scores.create!(score: true, verified_fraud: false, actions: {support_risky: false}) }
      refute account.support_risky?
    end

    it "returns false if there are no fraud_scores" do
      refute account.support_risky?
    end

    it "returns false if whitelisted?" do
      account.stubs(:whitelisted?).returns(true)
      account.fraud_scores.create!(score: false, verified_fraud: false, actions: {support_risky: true})
      refute account.support_risky?
    end
  end

  describe '#talk_risky?' do
    it "returns true if at least one fraud score is talk risky" do
      account.fraud_scores.create!(score: true, verified_fraud: false, actions: {talk_risky: true})
      2.times { account.fraud_scores.create!(score: true, verified_fraud: false, actions: {talk_risky: false}) }
      assert account.talk_risky?
    end

    it "returns false if fraud scores are not talk risky" do
      2.times { account.fraud_scores.create!(score: true, verified_fraud: false, actions: {talk_risky: false}) }
      refute account.talk_risky?
    end

    it "returns false if there are no fraud_scores" do
      refute account.talk_risky?
    end

    it "returns false if whitelisted?" do
      account.stubs(:whitelisted?).returns(true)
      account.fraud_scores.create!(score: false, verified_fraud: false, actions: {talk_risky: true})
      refute account.talk_risky?
    end
  end

  describe '#whitelisted?' do
    it "defaults to false" do
      refute account.whitelisted?
    end

    it "delegates to the account setting" do
      account.settings.whitelisted_from_fraud_restrictions = true
      assert account.whitelisted?
    end

    it "allows sales assited accounts" do
      account.subscription.stubs(zuora_subscription: mock(assisted?: true))
      assert account.whitelisted?
    end

    it "memoizes" do
      account.subscription.expects(zuora_subscription: mock(assisted?: true)).once
      assert account.whitelisted?
    end

    describe "when subscription record doesn't exist" do
      it "returns false" do
        account.stubs(:subscription).returns(nil)
        refute account.whitelisted?
      end
    end
  end

  describe '#recently_created_account?' do
    it "returns true for minimum account" do
      assert account.recently_created_account?
    end

    it "returns false for an older minimum account" do
      account.stubs(:created_at).returns(6.weeks.ago)
      refute account.recently_created_account?
    end
  end

  describe '#trusted_bulk_uploader?' do
    describe 'when outbound email is not safe' do
      before { account.stubs(:outbound_email_safe?).returns(false) }

      it 'does not allow bulk upload' do
        refute account.trusted_bulk_uploader?
      end

      describe 'and bulk_user_upload setting is enabled' do
        before do
          account.settings.bulk_user_upload = true
          account.settings.save!
        end

        it 'allows bulk upload' do
          assert account.trusted_bulk_uploader?
        end
      end
    end

    describe 'when outbound email is safe' do
      before { account.stubs(:outbound_email_safe?).returns(true) }

      it 'allows bulk upload' do
        assert account.trusted_bulk_uploader?
      end
    end
  end

  describe '#outbound_email_safe?' do
    it "returns true by default" do
      assert account.outbound_email_safe?
    end

    it "returns true if whitelisted level" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::MONITOR_WHITELIST_RISK_ASSESSMENT)
      assert account.outbound_email_safe?
    end

    it "returns true if safe level" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::EMAIL_SAFE_RISK_ASSESSMENT)
      assert account.outbound_email_safe?
    end

    it "returns false if risky level" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT)
      refute account.outbound_email_safe?
    end

    it "returns false if suspended level" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT)
      refute account.outbound_email_safe?
    end
  end

  describe '#outbound_email_deliverable?' do
    it "returns true by default" do
      assert account.outbound_email_deliverable?
    end

    it "returns true if risky level" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT)
      assert account.outbound_email_deliverable?
    end

    it "returns true if safe level" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::EMAIL_SAFE_RISK_ASSESSMENT)
      assert account.outbound_email_deliverable?
    end

    it "returns false if suspended level" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT)
      refute account.outbound_email_deliverable?
    end
  end

  describe '#outbound_email_risky?' do
    it "returns false by default" do
      refute account.outbound_email_risky?
    end

    it "returns true if risky" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT)
      assert account.outbound_email_risky?
    end

    it "returns false if suspended" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT)
      refute account.outbound_email_risky?
    end

    it "returns false if whitelisted" do
      account.settings.stubs(:risk_assessment).returns(::Account::FraudSupport::MONITOR_WHITELIST_RISK_ASSESSMENT)
      refute account.outbound_email_risky?
    end
  end

  describe '#validate_fraud_score_limit' do
    before do
      ::Account::FraudSupport.send(:remove_const, :FRAUD_SCORE_LIMIT)
      ::Account::FraudSupport.const_set(:FRAUD_SCORE_LIMIT, 3)
    end

    it "raises an exception when limit is reached" do
      assert_raises RuntimeError, "Max number of fraud scores for account" do
        (::Account::FraudSupport::FRAUD_SCORE_LIMIT + 2).times do
          account.fraud_scores.build(score: true, verified_fraud: false, source_event: Fraud::SourceEvent::SC_ACCOUNT_TRIAL_SIGNUP)
          account.save!
        end
      end
      refute_equal ::Account::FraudSupport::FRAUD_SCORE_LIMIT + 1, account.fraud_scores.count
    end

    it "does not raise an exception when below limit" do
      (::Account::FraudSupport::FRAUD_SCORE_LIMIT - 1).times do
        account.fraud_scores.build(score: true, verified_fraud: false, source_event: Fraud::SourceEvent::TRIAL_SIGNUP)
        account.save!
      end
      assert_equal ::Account::FraudSupport::FRAUD_SCORE_LIMIT - 1, account.fraud_scores.count
    end
  end

  describe '#new_shell_account?' do
    before { account.stubs(:created_at).returns(30.seconds.ago) }
    it 'returns true if created today and does not already have shell account source event' do
      assert account.new_shell_account?
    end

    it 'returns false if account already has shell account source event' do
      account.fraud_scores.create!(score: true, verified_fraud: false, source_event: Fraud::SourceEvent::SHELL_CREATED)
      refute account.new_shell_account?
    end

    it 'returns false if not created today' do
      account.stubs(:created_at).returns(2.days.ago)
      refute account.new_shell_account?
    end
  end

  describe '#vendor_review_prohibited?' do
    before { account.subscription_feature_addons.destroy_all }
    it 'returns false if no addons' do
      refute account.vendor_review_prohibited?
    end

    it 'returns false if addon not in PROHIBITED_VENDOR_ADDON_NAMES' do
      account.subscription_feature_addons.create!(name: 'addon_test', boost_expires_at: 10.days.from_now)
      refute account.vendor_review_prohibited?
    end

    it 'returns true if addon included in PROHIBITED_VENDOR_ADDON_NAMES' do
      account.subscription_feature_addons.create!(name: 'eu_data_center', boost_expires_at: 10.days.from_now)
      assert account.vendor_review_prohibited?
    end
  end

  def stub_products_response(method, body, include_deleted_account = false)
    products_url = "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products" + (include_deleted_account ? '?include_deleted_account=true' : '')
    stub_request(method, products_url).
      to_return(body: body.to_json, headers: { content_type: 'application/json; charset=utf-8' })
  end
end
