require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Account::BrandSupport do
  fixtures :accounts, :account_settings, :subscriptions, :brands

  before do
    @account = accounts(:minimum)
    @account.stubs(:has_multibrand?).returns(true)
  end

  describe 'with one active brand' do
    before do
      @inactive_brand = FactoryBot.create(:brand, active: false)
    end

    it 'propagates name changes to brand' do
      @account.update_attributes name: 'hi'
      assert_equal 1, @account.brands.active.size
      assert_equal 'hi', @account.brands.active.first.name
    end
  end

  describe 'with multiple active brands' do
    before do
      @active_brand = FactoryBot.create(:brand)
    end

    it 'not propagates name changes to brand' do
      @account.update_attributes name: 'hi'
      assert_equal 2, @account.brands.active.size
      refute_equal 'hi', @account.brands.active.first.name
    end
  end

  describe 'default brand' do
    before do
      @active_brand = FactoryBot.create(:brand, active: true)
      @account.update_attributes default_brand: @active_brand

      @inactive_brand = FactoryBot.create(:brand, active: false)
    end

    it "ensures the default brand is not nil" do
      @account.settings.default_brand_id = nil
      refute @account.valid?
      assert_equal ['One default brand must always be present'], @account.errors.full_messages
    end

    it "ensures the default brand is valid" do
      @account.settings.default_brand_id = 999
      refute @account.valid?
      assert_equal ['One default brand must always be present'], @account.errors.full_messages
    end

    it "ensures the default brand is active" do
      @account.settings.default_brand_id = @inactive_brand.id
      refute @account.valid?
      assert_equal ['Cannot make an inactive brand the default brand'], @account.errors.full_messages
    end

    it "ensures the default brand belongs to the account" do
      @another_account = accounts(:with_groups)
      @another_brand = FactoryBot.create(:brand, account_id: @another_account.id)
      @account.settings.default_brand_id = @another_brand.id
      refute @account.valid?
      assert_equal ['One default brand must always be present'], @account.errors.full_messages
    end
  end

  describe "#default_brand" do
    it "finds the brand" do
      assert_equal brands(:minimum), @account.default_brand
    end

    it "does not find no brand" do
      @account.settings.default_brand_id = nil
      assert_nil @account.default_brand
    end
  end

  describe "#default_brand=" do
    it "sets default_brand_id setting" do
      brand = FactoryBot.create(:brand, active: true)
      @account.default_brand = brand
      assert_equal brand.id, @account.settings.default_brand_id
      assert_equal brand, @account.default_brand
    end
  end

  describe '#new_to_branding?' do
    describe 'without any brand modifications' do
      it 'is true' do
        assert(@account.new_to_branding?)
      end
    end

    describe 'has more than one brand' do
      let!(:brand2) { FactoryBot.create(:brand, account_id: @account.id, deleted_at: Time.now) }

      it 'is false' do
        assert_equal false, @account.new_to_branding?
      end
    end
  end

  describe '#has_multiple_brands?' do
    it 'is true for multiple brands' do
      FactoryBot.create(:brand, account_id: @account.id, active: false)
      assert(@account.reload.has_multiple_brands?)
    end

    it 'is false for one brand' do
      assert_equal false, @account.has_multiple_brands?
    end

    it 'is false for accounts without multibrand feature' do
      FactoryBot.create(:brand, account_id: @account.id, active: false)
      @account.stubs(has_multibrand?: false)
      assert_equal false, @account.has_multiple_brands?
    end
  end

  describe '#has_multiple_active_brands?' do
    it 'is true for multiple active brands' do
      FactoryBot.create(:brand, account_id: @account.id, active: true)
      assert(@account.has_multiple_active_brands?)
    end

    it 'is false for one active brand' do
      FactoryBot.create(:brand, account_id: @account.id, active: false)
      assert_equal false, @account.has_multiple_active_brands?
    end

    it 'is false for accounts without multibrand feature' do
      @account.stubs(has_multibrand?: false)
      assert_equal false, @account.has_multiple_active_brands?
    end
  end

  describe "#inactive_brands" do
    before do
      @active_brand = FactoryBot.create(:brand, active: true)
      @inactive_brand = FactoryBot.create(:brand, active: false)
    end

    it "finds all the inactive brands" do
      assert_equal [@inactive_brand], @account.inactive_brands
    end
  end

  describe "#active_brands" do
    before do
      @active_brand = FactoryBot.create(:brand, active: true)
      @inactive_brand = FactoryBot.create(:brand, active: false)
    end

    it "finds all the active brands" do
      assert_equal [@account.default_brand, @active_brand],
        @account.active_brands
    end
  end

  describe 'under_active_brands_limit?' do
    before do
      FactoryBot.create(:brand, name: 'Wombat', active: true, account_id: @account.id)
      FactoryBot.create(:brand, name: 'Lemur',   active: true, account_id: @account.id)
      FactoryBot.create(:brand, name: 'Giraffe', active: true, account_id: @account.id)
      @cheetar = FactoryBot.create(:brand, name: 'Cheetar', active: true,  account_id: @account.id)
    end

    describe 'with unlimited_multibrand feature' do
      it 'always returns true' do
        @account.stubs(:has_unlimited_multibrand?).returns(true)
        assert_equal 5, @account.brands.active.count(:all)
        assert @account.under_active_brands_limit?
      end
    end

    describe 'returns false without unlimited_multibrand feature' do
      before do
        @account.stubs(:has_unlimited_multibrand?).returns(false)
      end

      it 'returns true if the account has less than 5 active brands' do
        @cheetar.update_attributes(active: false)
        @cheetar.reload
        assert_equal 4, @account.brands.active.count(:all)
        assert @account.under_active_brands_limit?
      end

      it 'returns false if the account already has 5 active brands' do
        assert_equal 5, @account.brands.active.count(:all)
        refute @account.under_active_brands_limit?
      end
    end
  end

  describe 'under_maximum_brands_limit?' do
    before do
      FactoryBot.create(:brand, name: 'Wombat', active: true, account_id: @account.id)
      FactoryBot.create(:brand, name: 'Lemur',   active: true, account_id: @account.id)
      FactoryBot.create(:brand, name: 'Giraffe', active: true, account_id: @account.id)
      @cheetar = FactoryBot.create(:brand, name: 'Cheetar', active: true,  account_id: @account.id)
    end

    describe 'with number of brands bigger than maximum_brands setting' do
      before do
        @account.settings.stubs(:maximum_brands).returns(2)
      end

      it 'returns false if maximum_brands arturo feature is enabled' do
        @account.stubs(:has_maximum_brands?).returns(true)
        assert_equal 5, @account.brands.count(:all)
        refute @account.under_maximum_brands_limit?
      end

      it 'returns true if maximum_brands arturo feature is disabled' do
        @account.stubs(:has_maximum_brands?).returns(false)
        assert_equal 5, @account.brands.active.count(:all)
        assert @account.under_maximum_brands_limit?
      end
    end

    describe 'with number of brands lower than maximum_brands setting' do
      before do
        @account.settings.stubs(:maximum_brands).returns(6)
      end

      it 'returns true' do
        assert_equal 5, @account.brands.count(:all)
        assert @account.under_maximum_brands_limit?
      end
    end
  end
end
