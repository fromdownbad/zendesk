require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::SuiteSupport do
  fixtures :accounts

  describe '#has_active_suite?' do
    let(:account)  { accounts(:minimum) }
    let(:client)   { stub }
    let(:suite)    { true }
    let(:active)   { true }

    let(:products) do
      [stub(name: :support, active?: active, plan_settings: { 'suite' => suite })]
    end

    before do
      account.stubs(:products).returns(products)
    end

    describe "when support is active & from a Suite" do
      let(:suite)  { true }
      let(:active) { true }

      it { assert account.has_active_suite? }
    end

    describe "when non-support products are Suite but support is not" do
      let(:products) do
        [
          stub(name: :support, active?: true, plan_settings: { 'suite' => false }),
          stub(name: :social_messaging, active?: true, plan_settings: { 'suite' => true })
        ]
      end

      it { refute account.has_active_suite? }
    end

    describe "when none of the products are active & from a Suite" do
      let(:suite)  { false }
      let(:active) { false }

      it { refute account.has_active_suite? }
    end

    it 'does not raise when Pravda fails' do
      account.stubs(:products).raises(Net::ReadTimeout)
      Rails.application.config.statsd.client.expects(:increment)
      account.has_active_suite?
    end
  end
end
