require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::WebWidgetSupport do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:domain) { 'zd-dev.com' }
  let(:brand) { account.default_brand }

  before do
    stub_request(:get, %r{/snippets/web_widget/minimum.zd-dev.com}).
      to_return(body: '<script id="snippet-id" src="snippet.js"></script>')
  end

  describe '#brand_code_snippet' do
    it 'returns web widget snippet for default brand' do
      expected = <<~SNIPPET
        <!-- Start of #{brand.subdomain} Zendesk Widget script -->
        <script id="snippet-id" src="snippet.js"></script>
        <!-- End of #{brand.subdomain} Zendesk Widget script -->
      SNIPPET
      assert_equal expected, account.brand_code_snippet(brand, domain)
    end

    it 'fetches the snippet from the new ekr endpoint' do
      account.brand_code_snippet(brand, domain)

      assert_requested(:get, "https://ekr.zdassets.com/snippets/web_widget/minimum.zd-dev.com?dynamic_snippet=true")
    end
  end
end
