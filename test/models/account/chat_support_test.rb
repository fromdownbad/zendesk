require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2 # lines 18 and 22 missing coverage

describe 'Account::ChatSupport' do
  fixtures :accounts

  describe "Given an Account" do
    before do
      @account = accounts(:minimum)
      @account.extend(Account::ChatSupport)
    end

    describe "for chat properties" do
      before do
        @account = accounts(:minimum)
      end

      it "sets maximum_chat_requests" do
        assert_equal 5, @account.maximum_chat_requests
      end

      describe "with maximum_chat_requests unset" do
        it "returns default of 1" do
          @account.maximum_chat_requests = nil
          @account.save!
          @account.reload

          assert_equal 1, @account.maximum_chat_requests
        end
      end

      describe "with maximum_chat_requests set" do
        it "returns persisted value" do
          @account.maximum_chat_requests = 10
          @account.save!
          @account.reload

          assert_equal 10, @account.maximum_chat_requests
        end
      end
    end

    describe 'with the chat feature deployed and a subscription that has chat' do
      before do
        @account.subscription.stubs(:has_chat?).returns(true)
      end

      it 'has chat available' do
        assert @account.chat_available?
      end
    end

    describe 'with the chat feature deployed and a subscription that has chat' do
      before do
        @account.subscription.stubs(:has_chat?).returns(false)
      end

      it 'does not have chat available' do
        refute @account.chat_available?
      end
    end

    describe 'on a plan that lacks chat' do
      before do
        @account.subscription.stubs(:has_chat?).returns(false)
      end
      it 'not has chat available' do
        refute @account.chat_available?
      end
    end

    describe "#has_chat_permission_set?" do
      describe "without chat permission set" do
        before do
          @permission_sets = stub
          @permission_sets.expects(:exists?).with(
            role_type: ::PermissionSet::Type::CHAT_AGENT
          ).returns(false)
          @account.expects(:permission_sets).returns(@permission_sets)
        end

        it "returns false" do
          refute @account.has_chat_permission_set?
        end
      end

      describe "without zopim subscription" do
        before do
          @permission_sets = stub
          @permission_sets.expects(:exists?).with(
            role_type: ::PermissionSet::Type::CHAT_AGENT
          ).returns(true)
          @account.expects(:permission_sets).returns(@permission_sets)
          @account.expects(:zopim_subscription).returns(nil)
        end

        it "returns false" do
          refute @account.has_chat_permission_set?
        end
      end

      describe "without serviceable zopim subscription" do
        before do
          @permission_sets = stub
          @permission_sets.expects(:exists?).with(
            role_type: ::PermissionSet::Type::CHAT_AGENT
          ).returns(true)
          @account.expects(:permission_sets).returns(@permission_sets)
          @zopim_subscription = stub(is_serviceable?: false)
          @account.expects(:zopim_subscription).returns(@zopim_subscription).twice
        end

        it "returns false" do
          refute @account.has_chat_permission_set?
        end
      end

      describe "with zopim billing, chat permission set and serviceable zopim subscription" do
        before do
          @permission_sets = stub
          @permission_sets.expects(:exists?).with(
            role_type: ::PermissionSet::Type::CHAT_AGENT
          ).returns(true)
          @account.expects(:permission_sets).returns(@permission_sets)
          @zopim_subscription = stub(is_serviceable?: true)
          @account.expects(:zopim_subscription).returns(@zopim_subscription).twice
        end

        it "returns true" do
          assert @account.has_chat_permission_set?
        end
      end
    end

    describe "#has_chat_permission_set_or_active_suite?" do
      before do
        @account.stubs(
          has_chat_permission_set?: has_chat_permission_set,
          has_active_suite?:        has_active_suite
        )
      end

      describe "with a chat permission_set" do
        let(:has_chat_permission_set) { true }
        let(:has_active_suite)        { false }

        it { assert @account.has_chat_permission_set_or_active_suite? }
      end

      describe "with the Suite product" do
        let(:has_chat_permission_set) { false }
        let(:has_active_suite)        { true }

        it { assert @account.has_chat_permission_set_or_active_suite? }
      end

      describe "without a chat permission_set or Suite product" do
        let(:has_chat_permission_set) { false }
        let(:has_active_suite)        { false }

        it { refute @account.has_chat_permission_set_or_active_suite? }
      end
    end

    describe 'without chat permission set' do
      before do
        @permission_sets = stub
        @permission_sets.expects(:where).with(
          role_type: ::PermissionSet::Type::CHAT_AGENT
        ).returns(stub(first: nil))
        @account.expects(:permission_sets).returns(@permission_sets)
        @account.expects(:has_chat_permission_set?).returns(true)
      end

      it 'has nil as chat permission set' do
        assert_nil @account.chat_permission_set
      end
    end

    describe 'with chat permission set' do
      before do
        @permission_sets = stub
        @permission_sets.expects(:where).with(
          role_type: ::PermissionSet::Type::CHAT_AGENT
        ).returns(stub(first: stub))
        @account.expects(:permission_sets).returns(@permission_sets)
      end

      describe "with has_permission_set? returning true" do
        before do
          @account.expects(:has_chat_permission_set?).returns(true)
        end

        it 'not has nil as chat permission set' do
          assert_not_nil @account.chat_permission_set
        end
      end

      describe "with has_permission_set? returning false" do
        before do
          @account.expects(:has_chat_permission_set?).returns(false)
        end

        it 'has nil as chat permission set' do
          assert_nil @account.chat_permission_set
        end
      end
    end

    describe '#zopim_identity_available?' do
      describe_with_arturo_enabled :ocp_chat_only_agent_deprecation do
        describe 'without chat integrated' do
          before do
            @account.expects(:chat_integrated?).returns(false)
          end

          it 'returns false' do
            refute @account.zopim_identity_available?
          end
        end

        describe 'without zopim subscription present ' do
          before do
            @account.expects(:chat_integrated?).returns(true)

            @zopim_subscription = stub(is_present?: false)
            @zopim_subscription = stub(is_serviceable?: false)

            @account.expects(:zopim_subscription).twice.returns(@zopim_subscription)
          end

          it 'returns false' do
            refute @account.zopim_identity_available?
          end
        end

        describe 'without serviceable zopim subscription' do
          before do
            @account.expects(:chat_integrated?).returns(true)

            @zopim_subscription = stub(is_present?: true)
            @zopim_subscription = stub(is_serviceable?: false)

            @account.expects(:zopim_subscription).twice.returns(@zopim_subscription)
          end

          it 'returns false' do
            refute @account.zopim_identity_available?
          end
        end

        describe 'with zopim integration and serviceable zopim subscription' do
          before do
            @account.expects(:chat_integrated?).returns(true)

            @zopim_subscription = stub(is_present?: true)
            @zopim_subscription = stub(is_serviceable?: true)
            @account.expects(:zopim_subscription).twice.returns(@zopim_subscription)
          end

          it 'returns true' do
            assert @account.zopim_identity_available?
          end
        end
      end
    end
  end
end
