require_relative "../../support/test_helper"

SingleCov.not_covered! # this is in core somewhere

describe 'AccountLocking' do
  fixtures :accounts

  describe "an open account" do
    before do
      @account = accounts(:minimum)
      assert_equal(Account::LockState::OPEN, @account.lock_state)
    end

    it "is found without problems" do
      assert(Account.find(@account.id))
    end

    it "is lockable" do
      @account.lock!
      assert_not_equal(Account::LockState::OPEN, @account.lock_state)
    end
  end

  describe "a locked account" do
    before do
      @account = accounts(:minimum)
      @account.lock!
      assert_not_equal(Account::LockState::OPEN, @account.lock_state)
    end

    it "raises AccountLockedException when found" do
      assert_raise(Account::LockedException) { Account.find(@account.id) }
    end

    it "is unlockable" do
      @account.unlock!
      assert_equal(Account::LockState::OPEN, @account.lock_state)
    end

    it "does not be cachable" do
      refute @account.kasket_cacheable?
    end

    describe "in a 'without_locking' block" do
      it "is found wihout problems" do
        Account.without_locking do
          assert(Account.find(@account.id))
        end
      end
    end
  end

  describe "a Account::LockedException" do
    before do
      @account = accounts(:minimum)
      @account.lock!
      assert_not_equal(Account::LockState::OPEN, @account.lock_state)
      @exception = assert_raises Account::LockedException do
        Account.find(@account.id)
      end
    end

    it "has a reference to the locked account" do
      assert_equal(@account, @exception.account)
    end

    it "provides the account's information in the message" do
      message = "account ##{@account.id} (subdomain: #{@account.subdomain}) is locked"
      assert_equal message, @exception.to_s
    end
  end
end
