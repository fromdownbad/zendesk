require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 7

describe 'TrackingProperties' do
  fixtures :accounts, :users, :subscriptions, :experiment_participations

  describe "tracks properties" do
    let(:account) { accounts(:mixpanel) }
    let(:user) { users(:mixpanel_user) }
    let(:voice_body)    { read_test_file('voice/voice_account.json') }
    let(:voice_headers) { { content_type: 'application/json' } }
    let(:voice_request_path) { %r{/api/v2/channels/voice/voice_account.json} }
    let(:zopim_subscription) do
      stub(
        created_at: DateTime.new(2018, 1, 4),
        max_agents: 3,
        present_plan_type:  1,
        present_plan_name:  'test'
      )
    end
    let(:explore_subscription) do
      stub(
        created_at: DateTime.new(2018, 1, 4),
        max_agents: nil,
        plan_type:  1,
        plan_name:  'test'
      )
    end

    it "is able to track various properties" do
      assert account.respond_to? :basic_properties
      assert account.respond_to? :subscription_properties
      assert account.respond_to? :split_testing_properties
      assert account.respond_to? :time_to_action_properties
      assert account.respond_to? :voice_subscription_properties
    end

    it "includes basic properties" do
      basic = account.basic_properties(user.id)
      assert_equal [
        :subdomain,
        :help_desk_size,
        :customer_type,
        :account_created,
        :account_created_week,
        :account_created_month,
        :locale_id,
        :language,
        :account_id,
        :user_id,
        :signup_source,
        :is_partner,
        :identity,
        :user_role,
        :created,
        :created_week,
        :created_month,
        :is_owner,
        :is_end_user
      ], basic.keys
    end

    it "includes subscription properties" do
      subscription = account.subscription_properties
      assert_equal [
        :account_type,
        :plan_type,
        :plan_name,
        :is_trial,
        :max_agents,
        :bc_type
      ], subscription.keys
    end

    it "includes voice subscription properties" do
      subscription = account.voice_subscription_properties
      assert_equal [
        :voice_plan_type,
        :voice_plan_name,
        :voice_is_trial,
        :voice_max_agents
      ], subscription.keys
    end

    it "includes support group properties" do
      prop = account.support_group_properties
      assert_equal [
        :account_id,
        :account_name,
        :language,
        :locale_id,
        :signup_source,
        :subdomain,
        :company_size,
        :has_active_suite
      ], prop.keys
    end

    it "includes support subscription properties" do
      subscription = account.support_subscription_properties
      assert_equal [
        :support_account_created,
        :support_account_created_week,
        :support_account_created_month,
        :support_account_type,
        :support_max_agents,
        :support_plan_name,
        :support_plan_type
      ], subscription.keys
    end

    it "includes guide subscription properties" do
      FactoryBot.create(:guide_subscription, account: account)
      subscription = account.guide_subscription_properties
      assert_equal [
        :guide_account_created,
        :guide_account_created_week,
        :guide_account_created_month,
        :guide_max_agents,
        :guide_plan_name,
        :guide_plan_type
      ], subscription.keys
    end

    it "includes chat subscription properties" do
      account.stubs(:zopim_subscription).returns(zopim_subscription)
      subscription = account.chat_subscription_properties
      assert_equal [
        :chat_account_created,
        :chat_account_created_week,
        :chat_account_created_month,
        :chat_max_agents,
        :chat_plan_name,
        :chat_plan_type
      ], subscription.keys
    end

    it "includes talk subscription properties" do
      stub_request(:get, voice_request_path).to_return(body: voice_body, headers: voice_headers)
      account.stubs(:voice_enabled?).returns(true)
      subscription = account.talk_subscription_properties
      assert_equal [
        :talk_max_agents,
        :talk_plan_name,
        :talk_plan_type,
        :talk_account_created,
        :talk_account_created_week,
        :talk_account_created_month
      ], subscription.keys
    end

    describe "support user properties" do
      it "includes basic fields when user is invalid" do
        prop = account.support_user_properties(nil)
        assert_equal [
          :user_id,
          :support_language,
          :support_locale_id,
          :support_locale
        ], prop.keys
      end

      it "includes user fields when user is valid" do
        prop = account.support_user_properties(user.id)
        assert_equal [
          :user_id,
          :support_language,
          :support_locale_id,
          :support_is_admin,
          :support_is_end_user,
          :support_is_owner,
          :support_user_role,
          :support_locale
        ].sort, prop.keys.sort
      end
    end

    describe "split testing properties" do
      it "includes split testing properties while experiment is running" do
        result = account.split_testing_properties
        assert_equal 'variation1', result[:ab_mixpanel_v1]
        refute result[:ab_mixpanel_v1_finished]
      end

      it "includes split testing properties when experiment is finished" do
        result = account.split_testing_properties
        assert_equal 'variation2', result[:ab_mixpanel]
        assert result[:ab_mixpanel_finished]
      end
    end

    describe 'trial extra properties' do
      it 'reacts get mixpanel_ prefixed keys' do
        account.trial_extras << TrialExtra.from_hash('mixpanel_test_prop1' => '1', 'mixpanel_test_prop2' => 'ba', 'not_included' => 'not_shown')

        expected = {
          test_prop1: '1',
          test_prop2: 'ba'
        }.stringify_keys
        assert_equal expected, account.send(:trial_extra_properties)
      end

      it 'tracks features' do
        account.trial_extras << TrialExtra.from_hash('features' => 'voice, help_center, facebook')

        expected = {
          feature0: 'voice',
          feature1: 'help_center',
          feature2: 'facebook'
        }.stringify_keys
        assert_equal expected, account.send(:trial_extra_properties)
      end

      it 'tracks convertro id' do
        account.trial_extras << TrialExtra.from_hash('Convertro_SID__c' => 'someFakeId')

        expected = {
          convertroId: 'someFakeId'
        }.stringify_keys
        assert_equal expected, account.send(:trial_extra_properties)
      end
    end

    describe 'landing and utm properties' do
      it 'gets utms when uri is valid' do
        account.trial_extras << TrialExtra.from_hash('Session_Landing__c' => 'http://www.zendesk.com/?utm_source=infusionsoft&amp;utm_medium=appstore&amp;utm_campaign=profile')

        expected = {
          "utm_source"   => "infusionsoft",
          "utm_medium"   => "appstore",
          "utm_campaign" => "profile",
          "landing_site" => "www.zendesk.com",
          "landing_path" => "/"
        }
        assert_equal expected, account.send(:landing_utm_properties)
      end

      it 'returns site info when query is null' do
        account.trial_extras << TrialExtra.from_hash('Session_Landing__c' => 'http://www.zendesk.br/company/info')

        expected = {
          "landing_site" => "www.zendesk.br",
          "landing_path" => "/company/info"
        }
        assert_equal expected, account.send(:landing_utm_properties)
      end

      it 'supports url fragment' do
        account.trial_extras << TrialExtra.from_hash('Session_Landing__c' => 'http://www.zendesk.br/company/info#anchor')

        expected = {
          "landing_site" => "www.zendesk.br",
          "landing_path" => "/company/info#anchor"
        }
        assert_equal expected, account.send(:landing_utm_properties)
      end

      it 'returns {} when query is null' do
        account.trial_extras << TrialExtra.from_hash('Session_Landing__c' => 'utm_source=infusionsoft&amp;utm_medium=appstore&amp;utm_campaign=profile')
        assert_equal({}, account.send(:landing_utm_properties))
      end

      it 'returns {} when uri is a random string' do
        account.trial_extras << TrialExtra.from_hash('Session_Landing__c' => 'unknown')
        assert_equal({}, account.send(:landing_utm_properties))
      end

      it 'returns {} when uri is empty' do
        account.trial_extras << TrialExtra.from_hash('Session_Landing__c' => '')
        assert_equal({}, account.send(:landing_utm_properties))
      end
    end
  end
end
