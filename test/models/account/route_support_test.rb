require_relative "../../support/test_helper"
require_relative "../../support/brand_test_helper"

SingleCov.covered!

describe 'Account::RouteSupport' do
  include BrandTestHelper
  fixtures :all

  describe Account do
    let(:account) { accounts(:minimum) }
    let(:account_route) { account.route }

    before do
      account.stubs(:valid_cname_record?).returns(true)
      account.stubs(:has_publish_brand_entities?).returns(true)
    end

    it "propagates subdomain changes to the route" do
      account.subdomain = "foo"
      account.save!
      assert_equal "foo", account_route.subdomain
    end

    it "uses save! (skipping validation) for subdomain propagation" do
      account_route.expects(:save!).with(validate: false)
      account.subdomain = "foo"
      account.save!
    end

    it "propagates host_mapping changes to the route" do
      account.host_mapping = "foo.bar.com"
      account.save!
      assert_equal "foo.bar.com", account_route.host_mapping
    end

    it "syncs subdomain from route change" do
      account.subscription.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge)
      brand = create_brand(account, name: 'New brand', subdomain: 'newbrand', host_mapping: nil)

      account.route = brand.route
      account.save!

      assert_equal "newbrand", account.reload.subdomain
    end

    it "syncs host_mapping from route change" do
      Route.any_instance.stubs(:valid_cname_record?).returns(true)

      account.subscription.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge)
      brand = create_brand(account, name: 'New brand', subdomain: 'newbrand', host_mapping: 'test.example.com')

      account.route = brand.route
      account.save!

      assert_equal "test.example.com", account.reload.host_mapping
    end

    describe "account has the publish_brand_entities settings enabled" do
      let(:brand) { brands(:minimum) }
      let(:route2) { account.routes.create!(subdomain: "amazingsupport") }
      let(:brand2) { account.brands.create! { |b| b.name = "amazingsupport"; b.route = route2 } }

      before do
        account.default_brand = brand
        account.save!
      end

      describe 'and the subdomain has changed' do
        it 'sends a change event to the brand publisher' do
          [account.default_brand.id, brand2.id].each do |brand_id|
            EscKafkaMessage.expects(:create!).with(
              has_entries(
                topic: "guide.brand_entities",
                key: "#{account.id}:#{brand_id}"
              )
            )
          end

          account.subdomain = 'greatsupport'
          account.save!
        end

        it 'logs errors and continues execution' do
          BrandPublisher.any_instance.stubs(:publish_for_account).raises(StandardError, 'something is wrong')
          ZendeskExceptions::Logger.expects(:record).with(instance_of(StandardError), anything).at_least_once

          account.subdomain = 'badsupport'
          account.save!
        end
      end

      describe 'and the host_mapping has changed' do
        before do
          account.subscription.expects(:has_host_mapping?).returns(true)
          account.stubs(:valid_cname_record?).returns(true)
        end

        it 'sends a change event to the brand publisher' do
          [account.default_brand.id, brand2.id].each do |brand_id|
            EscKafkaMessage.expects(:create!).with(
              has_entries(
                topic: "guide.brand_entities",
                key: "#{account.id}:#{brand_id}"
              )
            )
          end

          account.host_mapping = "greatsupport.zendesk.com"
          account.save!
        end

        it 'logs errors and continues execution' do
          BrandPublisher.any_instance.stubs(:publish_for_account).raises(StandardError, 'something is wrong')
          ZendeskExceptions::Logger.expects(:record).with(instance_of(StandardError), anything).at_least_once

          account.host_mapping = 'badsupport.zendesk.com'
          account.save!
        end
      end

      describe 'and the account in question has 300 or more brands' do
        before do
          account.subscription.expects(:has_host_mapping?).returns(true)
          account.stubs(:valid_cname_record?).returns(true)
          Account.any_instance.stubs(:has_unlimited_multibrand?).returns(true)
        end

        it "publishes messages to all brands within a reasonable time" do
          skip "this is not a valid way to test performance"

          (0..300).map do |num|
            r = account.routes.create!(subdomain: "amazingsupport#{num}")
            account.brands.create! { |b| b.name = "amazingsupport#{num}"; b.route = r }
          end

          EscKafkaMessage.expects(:create!).times(302)

          started_time = Time.now
          account.host_mapping = "greatsupport.zendesk.com"
          account.save!
          finished_time = Time.now
          total_time = finished_time - started_time

          expected_time = 13.seconds
          assert(total_time < expected_time, "Expected the message to take #{expected_time}, but took #{total_time}")
        end
      end

      describe "account doesn't have the publish_brand_entities setting enabled" do
        let(:brand) { brands(:minimum) }

        before do
          account.default_brand = brand
          account.save!
          account.stubs(:has_publish_brand_entities?).returns(false)
        end

        it 'does not publish the subdomain change event' do
          BrandPublisher.any_instance.expects(:publish_for_account).never

          account.subdomain = 'thebestsupport'
          account.save!
        end

        it 'does not publish the host_mapping change event' do
          BrandPublisher.any_instance.expects(:publish_for_account).never

          account.host_mapping = 'thebestsupport.zendesk.com'
          account.save!
        end
      end
    end
  end
end
