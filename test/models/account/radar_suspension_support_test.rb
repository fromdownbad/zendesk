require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::RadarSuspensionSupport do
  fixtures :accounts, :account_settings
  let(:account) { accounts(:minimum) }
  let(:audit_attr) do
    {
      "actor_id" => -1,
      "actor_type" => "User",
      "ip_address" => "0.0.0.0",
      "source_type" => "Account",
      "message" => "Radar unsuspended for trial",
      "via_id" => 43,
      "via_reference_id" => 4645
    }
  end

  describe '#radar_enabled?' do
    it 'returns true enabled setting is true' do
      account.settings.radar_enabled = true
      account.settings.save!
      assert account.radar_enabled?
    end
    it 'returns false enabled setting is false' do
      account.settings.radar_enabled = false
      account.settings.save!
      refute account.radar_enabled?
    end
  end

  describe '#suspend_radar' do
    let(:redis) { ::RadarFactory.create_radar_client(account).redis }

    before do
      account.suspend_radar(audit_attr)
    end

    it "sets the radar_enabled setting to false" do
      refute account.settings.reload.radar_enabled
    end

    it "creates audit log" do
      assert_present CIA::Event.find_by_source_type('Account')
    end

    it 'publishes kill_switch message to redis' do
      value = redis.hgetall('status:/minimum/reconfigure')
      assert_equal "kill_switch", JSON.parse(value["reconfigure"])
    end

    after do
      redis.flushdb
    end
  end

  describe '#unsuspend_radar' do
    before do
      account.unsuspend_radar(audit_attr)
    end

    it "sets the radar_enabled setting to true" do
      assert account.settings.reload.radar_enabled
    end

    it "creates audit log" do
      assert_present CIA::Event.find_by_source_type('Account')
    end
  end
end
