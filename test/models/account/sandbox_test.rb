require_relative "../../support/test_helper"
require_relative "../../support/suite_test_helper"
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered!

describe 'Account::Sandbox' do
  include MultiproductTestHelper
  include SuiteTestHelper

  fixtures :accounts, :users, :user_identities, :routes, :brands, :account_settings, :role_settings

  describe Account do
    before do
      stub_account_service_products(products: [Zendesk::Accounts::Product.new(FactoryBot.build(:support_subscribed_product))])
      stub_request(:delete, Regexp.new("/api/v2/internal/monitor/account.json")).to_return(body: {}.to_json, headers: {content_type: 'application/json'})
    end

    describe '#add_sandbox' do
      it 'uses the primary address when present' do
        account = accounts(:minimum)
        sandbox = account.add_sandbox(sandbox_type: 'standard')

        assert account.address

        ignored_attrs  = %w[id account_id created_at updated_at]
        expected_attrs = sandbox.address.attributes.except(*ignored_attrs)
        actual_attrs = account.address.attributes.except(*ignored_attrs)
        assert_equal expected_attrs, actual_attrs
      end

      it 'obtains a valid address when the primary has none' do
        account = accounts(:minimum)
        account.update_attributes!(address: nil)
        sandbox = account.add_sandbox(sandbox_type: 'standard')

        assert_nil account.address
        assert sandbox.address.valid?
      end

      it 'accepts other sandbox types' do
        ['standard', 'metadata', 'partial', 'production'].each do |type|
          account = accounts(:minimum)
          account.update_attributes!(address: nil)
          sandbox = account.add_sandbox(sandbox_type: type)

          assert_equal type, sandbox.settings.sandbox_type
        end
      end
    end

    describe '#reactivate!' do
      it 'reactivates the sandbox' do
        account = accounts(:minimum)
        Timecop.travel(Time.now + 1.seconds)
        sandbox = account.reset_sandbox

        sandbox.update_attributes!(is_serviceable: false, is_active: false)

        refute sandbox.is_serviceable?
        refute sandbox.is_active?

        sandbox.reactivate!

        assert(sandbox.is_serviceable?)
        assert(sandbox.is_active?)
      end
    end

    describe ".exclude_sandbox" do
      it "only reacts return accounts where sandbox_master_id is null" do
        accounts(:trial).update_attribute(:sandbox_master_id, 1)

        refute_includes Account.exclude_sandbox.map(&:id), accounts(:trial).id
      end
    end

    describe ".sandbox_products" do
      let(:pravda_client) { stub(product!: nil, create_product!: nil) }
      before do
        @minimum_account = accounts(:minimum)
        ::Zendesk::Accounts::Client.stubs(:new).returns(pravda_client)
        pravda_client.stubs(:products).returns(
          [
            ::Zendesk::Accounts::Product.new(
              FactoryBot.build(:explore_cancelled_product)
            ),
            ::Zendesk::Accounts::Product.new(
              FactoryBot.build(:support_subscribed_product)
            )
          ]
        )
      end

      it 'only calls update_or_create_product for products with a state of free, trial or subscribed' do
        pravda_client.expects(:update_or_create_product).once
        @minimum_account.reset_sandbox
      end

      it 'copies trial products as trial (instead of free)' do
        pravda_client.stubs(:products).returns(
          [
            ::Zendesk::Accounts::Product.new(
              FactoryBot.build(:chat_trial_product, trial_expires_at: '2019-10-11 00:20:40')
            )
          ]
        )
        pravda_client.expects(:update_or_create_product).with(
          :chat,
          {
            product: {
              state: ::Zendesk::Accounts::Product::TRIAL,
              plan_settings: {
                'max_agents' => 100,
                'plan_type' => 0
              },
              trial_expires_at: '2019-10-11 00:20:40'.try(:to_datetime)
            }
          },
          context: 'sandbox_creation'
        )
        @minimum_account.reset_sandbox
      end

      it 'does not copies product if the product is on the DO_NOT_COPY_PRODUCT_LIST' do
        pravda_client.stubs(:products).returns(
          [
            ::Zendesk::Accounts::Product.new(
              FactoryBot.build(:answer_bot_trial_product, trial_expires_at: '2020-10-11 00:20:40')
            ),
            ::Zendesk::Accounts::Product.new(
              FactoryBot.build(:outbound_trial_product, trial_expires_at: '2022-10-11 00:20:40')
            ),
            ::Zendesk::Accounts::Product.new(
              FactoryBot.build(:talk_trial_product, trial_expires_at: '2022-10-11 00:20:40')
            )
          ]
        )
        pravda_client.expects(:update_or_create_product).never
        @minimum_account.reset_sandbox
      end

      it 'mutates chat from CP4 to CP3 upon copy if sandbox is non-multiproduct' do
        pravda_client.stubs(:products).returns(
          [
            ::Zendesk::Accounts::Product.new(
              FactoryBot.build(:chat_free_product, plan_settings: { max_agents: 100, plan_type: 3, phase: 4 })
            )
          ]
        )
        pravda_client.expects(:update_or_create_product).with(
          :chat,
          {
            product: {
              state: ::Zendesk::Accounts::Product::FREE,
              plan_settings: {
                'max_agents' => 100,
                'plan_type' => 3,
                'phase' => 3
              }
            }
          },
          context: 'sandbox_creation'
        )
        @minimum_account.reset_sandbox
      end
    end

    describe "#is_sandbox_master_of" do
      before do
        @account = accounts(:minimum)
        @sb = accounts(:trial)
      end

      it "returns true when the account_id matches that of the sandbox" do
        @sb.update_attribute(:sandbox_master_id, @account.id)
        assert @account.is_sandbox_master_of?(@sb.id)
      end

      it "returns false when the account_id does not match that of the sandbox" do
        @sb.update_attribute(:sandbox_master_id, @account.id + 1)
        refute @account.is_sandbox_master_of?(@sb.id)
      end

      describe "with deleted accounts" do
        before { @sb.update_attribute(:deleted_at, DateTime.yesterday) }

        it "does not raise error and returns false" do
          refute @account.is_sandbox_master_of?(@sb.id)
        end
      end
    end

    describe "#is_sandbox_master?" do
      before do
        @account = accounts(:minimum)
        Timecop.travel(Time.now + 1.seconds)
        @sandbox = @account.reset_sandbox
        assert @account.reload.is_sandbox_master?
      end

      it "returns false after the sandbox account is cancelled" do
        @sandbox.subscription.stubs(:should_validate_presence_of_credit_card?).returns(false)
        @sandbox.cancel!
        refute @account.reload.is_sandbox_master?
      end

      it "returns true if the sandbox account is reset" do
        Timecop.travel(Time.now + 1.seconds)
        @account.reset_sandbox
        assert @account.reload.is_sandbox_master?
      end
    end

    describe "#reset_sandbox" do
      before do
        @minimum_account = accounts(:minimum)
      end

      it "does not send UserIdentityChanged security notifications" do
        @master_account = accounts(:minimum)
        # TODO: NGI-998 This arturo is set to enabled by default in TEST. It should just be removed anyway
        Account.any_instance.stubs(:has_email_suppress_email_identity_update_notifications?).returns false
        SecurityMailer.expects(:deliver_user_auxillary_email_added).never
        @master_account.reset_sandbox
      end

      describe "when setting sandbox_creation_in_progress" do
        it "sets it to false when the sandbox is created" do
          @master_account = accounts(:minimum)
          sandbox = @master_account.reset_sandbox
          refute sandbox.settings.sandbox_creation_in_progress
        end

        it "does not persist sandbox_creation_in_progress when sandbox saving raises" do
          @master_account = accounts(:minimum)
          Account.any_instance.expects(:save!).raises "some error"
          assert_raises(RuntimeError) { @master_account.reset_sandbox }
          assert_equal @master_account.sandboxes.count, 0
          assert_empty AccountSetting.where(name: "sandbox_creation_in_progress")
        end
      end

      describe "when master account has admins with permission sets" do
        before do
          @master_account = accounts(:minimum)
          @admin = @master_account.admins.first
          @admin.permission_set = PermissionSet.create!(name: "Light agent", account: @master_account, role_type: 1)
          @admin.save!
        end

        it "does not carry over the permission set to the admin created in the sandbox" do
          Timecop.travel(Time.now + 1.seconds)
          @master_account.reset_sandbox
          assert_nil @master_account.reload.sandbox.find_user_by_email(@admin.email, scope: @master_account.sandbox.agents).permission_set_id
        end
      end

      describe "when master account has google_apps_domain" do
        let(:domain) { "foo.com" }
        before do
          @master_account = accounts(:minimum)
          @master_account.subscription.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge)
          @master_account.settings.google_apps_domain = domain
          @master_account.settings.save!
          @master_account.route.update_column(:gam_domain, domain)
        end

        it "skips adding google_apps_domain route for sandbox" do
          Timecop.travel(Time.now + 1.seconds)
          @master_account.reset_sandbox
          sandbox = @master_account.reload.sandbox
          assert_equal SubscriptionPlanType.ExtraLarge, sandbox.subscription.plan_type
          assert_nil sandbox.route.gam_domain
        end
      end

      describe "inheriting subscription from master account" do
        before do
          @master_account = accounts(:minimum)
        end

        it "returns at least 25 max_agent counts" do
          @master_account.subscription.update_attribute(:base_agents, 10)
          Timecop.travel(Time.now + 1.seconds)
          @master_account.reset_sandbox
          assert_equal 25, @master_account.reload.sandbox.subscription.max_agents

          @master_account.subscription.update_attribute(:base_agents, 30)
          Timecop.travel(Time.now + 1.seconds)
          @master_account.reset_sandbox
          assert_equal 30, @master_account.reload.sandbox.subscription.max_agents
        end

        # reset_sandbox is only used by the old sandbox screen/controller
        it 'sets sandbox_type to standard' do
          Timecop.travel(Time.now + 1.seconds)
          @master_account.reset_sandbox
          assert_equal 'standard', @master_account.reload.sandbox.settings.sandbox_type
        end

        describe "for a plus account" do
          before do
            @master_account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
          end

          it "returns a plus account" do
            Timecop.travel(Time.now + 1.seconds)
            @master_account.reset_sandbox
            assert_equal SubscriptionPlanType.Large, @master_account.reload.sandbox.subscription.plan_type
          end
        end

        describe "for a legacy pricing model account" do
          before do
            @master_account.subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE)
          end

          it "returns a subscription with enterprise elite pricing model" do
            Timecop.travel(Time.now + 1.seconds)
            @master_account.reset_sandbox
            assert_equal ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE, @master_account.reload.sandbox.subscription.pricing_model_revision
          end
        end

        describe "for a patagonia pricing model account" do
          before do
            @master_account.subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::PATAGONIA)
          end

          it "returns a subscription with enterprise elite pricing model" do
            Timecop.travel(Time.now + 1.seconds)
            @master_account.reset_sandbox
            assert_equal ZBC::Zendesk::PricingModelRevision::PATAGONIA, @master_account.reload.sandbox.subscription.pricing_model_revision
          end
        end

        describe "inheriting subscription features from master account" do
          before do
            Zendesk::Features::SubscriptionFeatureService.any_instance.expects(:create_sandbox_features!).with(@master_account.subscription)
          end

          it "calls SubscriptionFeatureService with create sandbox features" do
            Timecop.travel(Time.now + 1.seconds)
            @master_account.reset_sandbox
          end
        end
      end

      it "when upgrading to a large account, create the sandbox if it doesn't already exist" do
        user = users(:minimum_agent)
        CIA.stubs(:current_actor).returns(user)
        user.update_attribute(:roles, Role::ADMIN.id)
        @minimum_account.subscription.update_attributes!(plan_type: SubscriptionPlanType.Large)
        refute @minimum_account.is_sandbox_master?

        old = @minimum_account.settings.count(:all)
        @minimum_account.settings.hide_dashboard = 0
        @minimum_account.save!
        # After save, the account may have more settings as some validations query
        # a setting which results in a "build" on the settings association
        assert @minimum_account.settings.count(:all) > old

        Timecop.travel(Time.now + 1.seconds)
        @minimum_account.reset_sandbox
        assert @minimum_account.reload.is_sandbox_master?
        sandbox = @minimum_account.reload.sandbox
        assert sandbox.is_sandbox?

        assert_equal @minimum_account, sandbox.sandbox_master
        assert sandbox.is_sandbox?
        assert sandbox.address
        assert sandbox.owner

        master_admin_identities  = @minimum_account.user_identities.where(user_id: @minimum_account.admins.pluck(:id)).count(:all)
        sandbox_admin_identities = sandbox.user_identities.where(user_id: sandbox.admins.pluck(:id)).count(:all)

        assert_equal @minimum_account.admins.count(:all), master_admin_identities
        assert_equal master_admin_identities, sandbox_admin_identities

        # Sandbox may have more settings than the parent as some validations query
        # a setting which results in a "build" on the settings association
        @minimum_account.settings.each do |setting|
          name = setting.name
          next if name == 'default_brand_id' # these aren't supposed to match
          if sandbox.settings.detect { |s| s.name == name }
            assert_equal @minimum_account.settings.send(name), sandbox.settings.send(name)
          end
        end
      end

      it "resets some account properties" do
        master_acc = accounts(:minimum)
        master_acc.settings.end_user_phone_number_validation = true
        master_acc.settings.screencasts_for_tickets = true
        master_acc.settings.help_center_state = "enabled"
        master_acc.settings.customer_satisfaction = true
        master_acc.settings.automatic_answers = true
        master_acc.settings.save!
        Timecop.travel(Time.now + 1.seconds)
        master_acc.reset_sandbox

        sandbox = master_acc.reload.sandbox
        refute sandbox.settings.end_user_phone_number_validation
        refute sandbox.settings.screencasts_for_tickets?
        assert_equal(sandbox.settings.help_center_state, "disabled")
        refute sandbox.settings.customer_satisfaction?
        refute sandbox.settings.automatic_answers
      end

      it "skip copy agent workspace related settings" do
        agent_workspace_settings = %w[
          polaris check_group_name_uniqueness social_messaging_agent_workspace social_messaging native_messaging
        ].freeze

        Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation).returns(true)
        Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns(true)
        Zendesk::Accounts::Client.any_instance.stubs(:chat_product).returns(true)
        Zopim::InternalApiClient.any_instance.stubs(:notify_account_settings_change).returns(true)

        master_acc = accounts(:minimum)
        agent_workspace_settings.each do |setting|
          master_acc.settings.send("#{setting}=", true)
        end
        master_acc.settings.save!

        Timecop.travel(Time.now + 1.seconds)
        master_acc.reset_sandbox

        sandbox = master_acc.reload.sandbox
        agent_workspace_settings.each do |setting|
          refute sandbox.settings.send(setting)
        end
      end

      describe "#assumption_expiration?" do
        it "leaves the sandbox unassumable when false, and assumption was off" do
          @minimum_account.settings.assumption_duration = nil
          @minimum_account.settings.assumption_expiration = nil
          @minimum_account.reset_sandbox
          sandbox = @minimum_account.reload.sandbox
          refute sandbox.assumable?
        end

        it "leaves the sandbox unassumable when false, and assumption was expired" do
          @minimum_account.settings.assumption_duration = "week"
          @minimum_account.settings.assumption_expiration = 1.month.ago
          @minimum_account.reset_sandbox
          sandbox = @minimum_account.reload.sandbox
          refute sandbox.assumable?
        end

        it "makes the sandbox unassumable when false" do
          @minimum_account.settings.assumption_duration = "week"
          @minimum_account.settings.assumption_expiration = 1.week.from_now
          @minimum_account.reset_sandbox
          sandbox = @minimum_account.reload.sandbox
          refute sandbox.assumable?
        end
      end

      it "creates sandbox admins with same verified identities skipping UserSdkIdentity" do
        @owner = @minimum_account.owner
        UserPhoneNumberIdentity.create!(user: @owner, value: "+15551234567")
        UserSdkIdentity.create!(user: @owner, value: "abcdabcd-abcd-4d8e-abcd-a2a1f85dac4a")
        @admin = users(:minimum_admin_not_owner)
        UserPhoneNumberIdentity.create!(user: @admin, value: "+15551234568")
        UserSdkIdentity.create!(user: @admin, value: "bbcdabcd-abcd-4d8e-abcd-a2a1f85dac4a")

        @owner.reload
        @admin.reload

        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        sandbox = @minimum_account.reload.sandbox

        owner_copied_identities = @owner.identities.where.not(type: "UserSdkIdentity").sort_by(&:value)
        sandbox_owner_identities = sandbox.owner.identities.sort_by(&:value)
        assert_equal owner_copied_identities.map(&:value), sandbox_owner_identities.map(&:value)
        assert_equal owner_copied_identities.map(&:is_verified), sandbox_owner_identities.map(&:is_verified)

        sandbox_admin_not_owner = sandbox.admins.last
        admin_copied_identities = @admin.identities.where.not(type: "UserSdkIdentity").sort_by(&:value)
        sandbox_admin_not_owner_identities = sandbox_admin_not_owner.identities.sort_by(&:value)
        assert_equal admin_copied_identities.map(&:value), sandbox_admin_not_owner_identities.map(&:value)
        assert_equal admin_copied_identities.map(&:is_verified), sandbox_admin_not_owner_identities.map(&:is_verified)
      end

      it "creates sandbox admins and owners with same 2FA settings as master account" do
        secret_key = "smpghucddy4smzr2"
        owner_secret_key = "OWNER-smpghucddy4smzr2"
        @owner = @minimum_account.owner
        @admin = users(:minimum_admin_not_owner)
        UserOtpSetting.create!(user: @admin, account: @minimum_account, secret_key: secret_key, confirmed: true, phone_based: false)
        UserOtpSetting.create!(user: @owner, account: @minimum_account, secret_key: owner_secret_key, confirmed: true, phone_based: false)

        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        sandbox = @minimum_account.reload.sandbox

        sandbox_secret_keys = UserOtpSetting.where(account_id: sandbox.id).pluck(:secret_key)

        assert_includes sandbox_secret_keys, secret_key
        assert_includes sandbox_secret_keys, owner_secret_key
      end

      it "creates sandbox admins with same verified identities skipping UserTwiterIdentity" do
        @owner = @minimum_account.owner
        UserPhoneNumberIdentity.create!(user: @owner, value: "+15551234272")
        UserTwitterIdentity.create!(user: @owner, value: "731001937751990272")
        @admin = users(:minimum_admin_not_owner)
        UserPhoneNumberIdentity.create!(user: @admin, value: "+15551234277")
        UserTwitterIdentity.create!(user: @admin, value: "231001937751990277")

        @owner.reload
        @admin.reload

        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        sandbox = @minimum_account.reload.sandbox

        owner_copied_identities = @owner.identities.where.not(type: "UserTwitterIdentity").sort_by(&:value)
        sandbox_owner_identities = sandbox.owner.identities.sort_by(&:value)
        assert_equal owner_copied_identities.map(&:value), sandbox_owner_identities.map(&:value)
        assert_equal owner_copied_identities.map(&:is_verified), sandbox_owner_identities.map(&:is_verified)

        sandbox_admin_not_owner = sandbox.admins.last
        admin_copied_identities = @admin.identities.where.not(type: "UserTwitterIdentity").sort_by(&:value)
        sandbox_admin_not_owner_identities = sandbox_admin_not_owner.identities.sort_by(&:value)
        assert_equal admin_copied_identities.map(&:value), sandbox_admin_not_owner_identities.map(&:value)
        assert_equal admin_copied_identities.map(&:is_verified), sandbox_admin_not_owner_identities.map(&:is_verified)
      end

      it "is still create a sandbox, even the if the manual discount is 100%" do
        @minimum_account.subscription.update_attributes!(
          plan_type: SubscriptionPlanType.Large,
          manual_discount: 100,
          manual_discount_expires_on: 30.days.from_now.to_date
        )

        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
      end

      it "does not raise for an account without a help_desk_size" do
        @minimum_account.help_desk_size = nil
        Timecop.travel(Time.now + 1.seconds)
        @minimum_account.reset_sandbox
      end

      it "skips password validation" do
        @minimum_account.subscription.update_attributes!(plan_type: SubscriptionPlanType.Large)

        owner = users(:minimum_admin)
        owner.update_attribute(:password, nil)
        owner.update_attribute(:crypted_password, nil)

        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
      end

      it "queues up a resque SandboxDestroyJob" do
        @minimum_account.subscription.update_attributes!(plan_type: SubscriptionPlanType.Large)
        Timecop.travel(Time.now + 1.seconds)
        @minimum_account.reset_sandbox
        original_sandbox = @minimum_account.reload.sandbox
        assert_equal original_sandbox.sandbox_master_id, @minimum_account.id
        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
      end

      it "strips hyphens from subdomain" do
        @minimum_account.update_attribute(:subdomain, "i-like-hyphens")
        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        assert_includes @minimum_account.reload.sandbox.subdomain, "ilikehyphens"
      end

      it "updates and return sandbox without requiring reload" do
        refute @minimum_account.sandbox
        Timecop.travel(Time.now + 1.seconds)
        sandbox = @minimum_account.reset_sandbox
        assert sandbox
        refute @minimum_account.sandbox
        assert_equal sandbox.id, @minimum_account.reload.sandbox.id
      end

      it "does not be sandbox master if just destroyed sandbox but cache/replication is catching up" do
        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        @minimum_account.reload
        assert @minimum_account.is_sandbox_master?
        Account.stubs(:count).returns(1)
        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        assert @minimum_account.reload.sandbox
      end

      it "creates sandbox with same security policy" do
        Account.any_instance.stubs(:sandbox_master).returns(@minimum_account)
        @minimum_account.role_settings.update_attributes!(
          agent_security_policy_id: Zendesk::SecurityPolicy::Medium.id,
          end_user_security_policy_id: Zendesk::SecurityPolicy::High.id
        )

        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        sandbox = @minimum_account.reload.sandbox
        assert_equal Zendesk::SecurityPolicy::Medium.id, sandbox.role_settings.agent_security_policy_id
        assert_equal Zendesk::SecurityPolicy::High.id, sandbox.role_settings.end_user_security_policy_id
      end

      describe "custom security policy" do
        before do
          Account.any_instance.stubs(:sandbox_master).returns(@minimum_account)
          @minimum_account.role_settings.update_attributes!(
            agent_security_policy_id: Zendesk::SecurityPolicy::Custom.id
          )
        end

        describe "if present on sandbox_master" do
          before do
            @custom_security_policy = CustomSecurityPolicy.build_from_current_policy(@minimum_account)
            @custom_security_policy.save!
          end

          it "creates custom security policy on the sandbox with a different id" do
            Timecop.travel(Time.now + 1.seconds)
            assert @minimum_account.reset_sandbox
            sandbox = @minimum_account.reload.sandbox

            assert sandbox.custom_security_policy
            assert_not_equal sandbox.custom_security_policy.id, @custom_security_policy.id
          end
        end

        describe "if not present on sandbox_master" do
          before { refute @minimum_account.custom_security_policy }

          it "does not create custom security policy" do
            Timecop.travel(Time.now + 1.seconds)
            assert @minimum_account.reset_sandbox
            sandbox = @minimum_account.reload.sandbox

            refute sandbox.custom_security_policy
          end
        end
      end

      describe "account texts" do
        let(:ip_ranges) { "2.2.2.2/32 3.3.3.3" }
        before do
          Account.any_instance.stubs(:sandbox_master).returns(@minimum_account)
        end

        describe "if present on sandbox_master" do
          before do
            @minimum_account.texts.ip_restriction = ip_ranges
          end

          it "copies over to sandbox" do
            Timecop.travel(Time.now + 1.seconds)
            assert @minimum_account.reset_sandbox
            sandbox = @minimum_account.reload.sandbox

            assert_equal sandbox.texts.ip_restriction, ip_ranges
          end
        end
      end

      it "creates sandbox with same signature_template" do
        Account.any_instance.stubs(:sandbox_master).returns(@minimum_account)
        @minimum_account.default_brand.update_attribute(:signature_template, "Master signature")

        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        sandbox = @minimum_account.reload.sandbox
        assert_equal "Master signature", sandbox.signature_template
      end

      it "creates the sandbox name with a suffix" do
        @minimum_account.locale_id = 1
        Account.any_instance.stubs(:sandbox_master).returns(@minimum_account)

        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        sandbox = @minimum_account.reload.sandbox
        assert_equal "#{@minimum_account.name} #{I18n.t("txt.admin.models.account.sandbox_name_suffix")}", sandbox.name
      end

      it "creates the sandbox name with a suffix in a different languages" do
        @minimum_account.locale_id = 2
        Account.any_instance.stubs(:sandbox_master).returns(@minimum_account)

        Timecop.travel(Time.now + 1.seconds)
        assert @minimum_account.reset_sandbox
        sandbox = @minimum_account.reload.sandbox
        assert_equal "#{@minimum_account.name} #{I18n.t("txt.admin.models.account.sandbox_name_suffix", locale: @minimum_account.translation_locale)}", sandbox.name
      end

      describe "whitelist_from_fraud_restrictions state" do
        it "whitelists the sandbox if the master account is sales assisted" do
          @minimum_account.subscription.expects(zuora_subscription: mock(assisted?: true)).once
          Account.any_instance.stubs(:sandbox_master).returns(@minimum_account)

          Timecop.travel(Time.now + 1.seconds)
          assert @minimum_account.reset_sandbox
          sandbox = @minimum_account.reload.sandbox

          assert(sandbox.settings.whitelisted_from_fraud_restrictions)
        end
      end

      describe "help center state" do
        describe "when help center is enabled" do
          it "creates sandbox with help center disabled" do
            @minimum_account.enable_help_center!
            Timecop.travel(Time.now + 1.seconds)

            assert @minimum_account.reset_sandbox
            sandbox = @minimum_account.reload.sandbox
            assert sandbox.help_center_disabled?
          end
        end

        describe "when help center is restricted" do
          it "creates sandbox with help center disabled" do
            @minimum_account.restrict_help_center!
            Timecop.travel(Time.now + 1.seconds)
            assert @minimum_account.reset_sandbox
            sandbox = @minimum_account.reload.sandbox
            assert sandbox.help_center_disabled?
          end
        end

        describe "when help center is disabled" do
          it "creates sandbox with help center disabled" do
            @minimum_account.disable_help_center!
            Timecop.travel(Time.now + 1.seconds)
            assert @minimum_account.reset_sandbox
            sandbox = @minimum_account.reload.sandbox
            assert sandbox.help_center_disabled?
          end
        end
      end

      describe "For an account whose sandbox is being reset" do
        before do
          @minimum_account = accounts(:minimum)
          @minimum_account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
          Timecop.travel(Time.now + 1.seconds)
          @old_sandbox = @minimum_account.reset_sandbox
          assert @minimum_account.reload.sandbox.present?
          Timecop.travel(Time.now + 1.seconds)
          @new_sandbox = @minimum_account.reset_sandbox
        end

        describe "the account" do
          it "has one sandbox and sandboxes retain old master ids" do
            assert_equal @new_sandbox.id, @minimum_account.reload.sandbox.id
            assert_equal @minimum_account.id, @new_sandbox.sandbox_master_id
            assert_equal @minimum_account.id, @old_sandbox.sandbox_master_id
          end
        end
      end

      describe 'when shell account with Support added' do
        it 'creates sandbox' do
          account = setup_shell_account_with_support
          stub_account_service_support_product(account)
          Timecop.travel(Time.now + 1.seconds)
          sandbox = account.reset_sandbox

          assert_equal account.id, sandbox.sandbox_master_id
        end
      end

      describe 'when account has billing_id' do
        it 'creates sandbox' do
          account = accounts(:minimum)
          account.update_attribute(:billing_id, 1)
          Timecop.travel(Time.now + 1.seconds)
          sandbox = account.reset_sandbox

          assert_equal account.id, sandbox.sandbox_master_id
        end
      end

      describe 'with account service product records' do
        let(:pravda_client) { stub(product!: nil, create_product!: nil) }
        before do
          ::Zendesk::Accounts::Client.stubs(:new).returns(pravda_client)
          pravda_client.stubs(:product).with(::Account::FraudSupport::SELL_PRODUCT, use_cache: true).returns(nil)
          pravda_client.stubs(:products).returns(
            [
              ::Zendesk::Accounts::Product.new(
                FactoryBot.build(:support_subscribed_product)
              )
            ]
          )
        end

        it 'copies the product records from master to sandbox, setting to free state' do
          pravda_client.expects(:update_or_create_product).with(
            :support,
            {
              product: {
                state: ::Zendesk::Accounts::Product::FREE,
                plan_settings: {
                  'max_agents' => 5,
                  'plan_type' => 1
                }
              }
            },
            context: 'sandbox_creation'
          )
          @minimum_account.reset_sandbox
        end
      end
    end

    describe "#destroy_sandbox" do
      before do
        @account = accounts(:minimum)
        Timecop.travel(Time.now + 1.seconds)
        @account.reset_sandbox
        @account.reload
      end

      it "destroys via api" do
        assert @account.destroy_sandbox
      end

      it "does not destroy when api call fails" do
        ZendeskExceptions::Logger.expects(:record).with { |_e, params| assert_includes(params[:message], "failed to destroy account") }
        stub_request(:delete, Regexp.new("/api/v2/internal/monitor/account.json")).to_return(body: {}.to_json, headers: {content_type: 'application/json'}, status: 422)
        refute @account.destroy_sandbox
      end
    end

    describe "a locked sandbox" do
      before do
        @account = accounts(:minimum)
        Timecop.travel(Time.now + 1.seconds)
        @sandbox = @account.reset_sandbox
        @account.reload
        @sandbox.lock_state = Account::LockState::LOCKED
        @sandbox.save!
      end
      it "does not raise" do
        assert @account.sandbox
      end

      it "raises if called from master without_nolocking" do
        assert_raise Zendesk::Accounts::Locking::LockedException do
          @account.sandbox_without_nolocking
        end
      end
    end

    [:multiple_sandboxes, :admin_center_framework_view_sandbox].each do |feature|
      describe "with #{feature}" do
        let(:account) { accounts(:minimum) }
        before do
          accounts(:trial).update_column(:sandbox_master_id, account.id)
          Arturo.enable_feature! feature
        end

        describe "#add_sandbox" do
          it "adds another sandbox" do
            account.add_sandbox(sandbox_type: 'standard')
            assert_equal account.sandboxes.count, 2
          end
          it "does not add if over the sandboxes limit" do
            Account.any_instance.stubs(:sandboxes_maxed?).returns(true)
            assert_raise Account::SandboxLimitError do
              account.add_sandbox(sandbox_type: 'standard')
            end
            assert_equal account.sandboxes.count, 1
          end
        end

        describe "#destroy" do
          it "does nothing if there is no target sandbox defined " do
            refute account.destroy_sandbox
          end

          it "destroys the target sandbox" do
            account.assign_current_sandbox(accounts(:trial).subdomain)
            assert account.destroy_sandbox
          end
        end
      end
    end
  end
end
