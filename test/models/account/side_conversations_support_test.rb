require_relative '../../support/test_helper'

SingleCov.covered!

describe Account do
  describe 'an account that does not have ticket_threads enabled' do
    subject { accounts(:minimum) }

    before do
      subject.stubs(:has_ticket_threads?).returns(true)
      subject.stubs(:has_side_conversations_enabled?).returns(false)
    end

    describe 'and no collaboration recipient address exists' do
      before do
        subject.recipient_addresses.where(product: 'collaboration').delete_all
      end

      describe 'enabling the old ticket_threads setting' do
        before do
          subject.settings.ticket_threads = true
          subject.save!
        end

        it 'creates a collaboration recipient address' do
          subject.recipient_addresses.where(product: 'collaboration').count.must_equal 1
        end
      end

      describe 'enabling the new side_conversations_email setting' do
        before do
          subject.settings.side_conversations_email = true
          subject.save!
        end

        it 'creates a collaboration recipient address' do
          subject.recipient_addresses.where(product: 'collaboration').count.must_equal 1
        end
      end
    end

    describe 'and has a collaboration recipient address' do
      before do
        subject.recipient_addresses.where(product: 'collaboration').delete_all
        subject.recipient_addresses.where(product: 'collaboration').create!(
          email: 'collaboration@minimum.zendesk-test.com'
        )
      end

      describe 'enabling the setting' do
        before do
          subject.settings.ticket_threads = true
          subject.save!
        end

        it 'does not create additional collaboration recipient addresses' do
          subject.recipient_addresses.where(product: 'collaboration').count.must_equal 1
        end
      end
    end
  end

  describe '#has_side_conversations_enabled?' do
    subject { accounts(:minimum) }

    describe 'when has_side_conversations? is false' do
      before do
        subject.stubs(:has_side_conversations?).returns(false)
      end

      it 'is false' do
        subject.has_side_conversations_enabled?.must_equal false
      end
    end

    describe 'when has_side_conversations? is true' do
      before do
        subject.stubs(:has_side_conversations?).returns(true)
      end

      describe 'and the slack feature is enabled' do
        before do
          subject.stubs(:has_side_conversations_slack?).returns(true)
        end

        it 'is true' do
          subject.has_side_conversations_enabled?.must_equal true
        end
      end

      describe 'and the email feature is enabled' do
        before do
          subject.stubs(:has_side_conversations_email?).returns(true)
        end

        it 'is true' do
          subject.has_side_conversations_enabled?.must_equal true
        end
      end

      describe 'and the ticket feature is enabled' do
        before do
          subject.stubs(:has_side_conversations_tickets?).returns(true)
        end

        it 'is true' do
          subject.has_side_conversations_enabled?.must_equal true
        end
      end
    end
  end
end
