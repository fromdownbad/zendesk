require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::CfaMigration do
  describe "#migrate_cfa_to_ctf" do
    describe "#find_cfa_data" do
      it "should query the app tables for CFA data based on the account and join rules together into one string" do
        # There are no App models to use so populate test data directly
        account = accounts(:systemaccount)

        # Set up installation, the account + app pairing
        installation_id = rand(10000)
        app_data_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, product_code) VALUES (#{installation_id.to_i}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, true, '#{Date.today}', '#{Date.today}', 1)"
        ActiveRecord::Base.connection.insert(app_data_query)

        # Set up rules data linked to the installation
        [["rules", "rules part 1"], ["rules_1", "rules part 2"], ["user_rules", "user rules part 1"], ["user_rules_1", "user rules part 2"]].each_with_index do |kv, i|
          key, value = kv
          app_data_query = "INSERT INTO apps_settings (id, account_id, installation_id, `key`, `value`, created_at, updated_at, parameter_id) VALUES (#{i}, #{account.id.to_i}, #{installation_id.to_i}, '#{key}', '#{value}', '#{Date.today}', '#{Date.today}', 1)"
          ActiveRecord::Base.connection.insert(app_data_query)
        end

        # Assert queries run successfully and join together multi-row data and pass to `parse_cfa_into_ctf`
        empty_migration_obj = { successful: [], failed: [] }
        account.expects(:parse_cfa_into_ctf).with(:agent, "rules part 1rules part 2", false).returns(empty_migration_obj)
        account.expects(:parse_cfa_into_ctf).with(:end_user, "user rules part 1user rules part 2", false).returns(empty_migration_obj)

        account.migrate_cfa_to_ctf
      end

      it "should skip 'null' value from processing" do
        # There are no App models to use so populate test data directly
        account = accounts(:systemaccount)

        # Set up installation, the account + app pairing
        installation_id = rand(10000)
        app_data_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, product_code) VALUES (#{installation_id.to_i}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, true, '#{Date.today}', '#{Date.today}', 1)"
        ActiveRecord::Base.connection.insert(app_data_query)

        # Set up rules data linked to the installation
        [["rules", "null"], ["rules_1", "null"], ["user_rules", "null"], ["user_rules_1", "null"]].each_with_index do |kv, i|
          key, value = kv
          app_data_query = "INSERT INTO apps_settings (id, account_id, installation_id, `key`, `value`, created_at, updated_at, parameter_id) VALUES (#{i}, #{account.id.to_i}, #{installation_id.to_i}, '#{key}', '#{value}', '#{Date.today}', '#{Date.today}', 1)"
          ActiveRecord::Base.connection.insert(app_data_query)
        end

        # Assert queries run successfully and join together multi-row data and pass to `parse_cfa_into_ctf`
        empty_migration_obj = { successful: [], failed: [] }
        account.expects(:parse_cfa_into_ctf).with(:agent, "", false).returns(empty_migration_obj)
        account.expects(:parse_cfa_into_ctf).with(:end_user, "", false).returns(empty_migration_obj)

        account.migrate_cfa_to_ctf
      end

      it "should use the correct installation_id when there are other soft deleted installations" do
        # There are no App models to use so populate test data directly
        account = accounts(:systemaccount)

        # Insert soft deleted installation
        soft_deleted_installation_id = rand(10000)
        app_data_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{soft_deleted_installation_id.to_i}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, false, '#{Date.today}', '#{Date.today}', '#{Date.today}', 1)"
        ActiveRecord::Base.connection.insert(app_data_query)

        # Set up rules data linked to the soft deleted installation
        [["rules", "soft deleted rules part 1"], ["rules_1", "soft deleted rules part 2"], ["user_rules", "soft deleted user rules part 1"], ["user_rules_1", "soft deleted rules part 2"]].each do |kv|
          key, value = kv
          app_data_query = "INSERT INTO apps_settings (id, account_id, installation_id, `key`, `value`, created_at, updated_at, parameter_id) VALUES (#{rand(10000)}, #{account.id.to_i}, #{soft_deleted_installation_id.to_i}, '#{key}', '#{value}', '#{Date.today}', '#{Date.today}', 1)"
          ActiveRecord::Base.connection.insert(app_data_query)
        end

        # Insert active installation
        active_installation_id = rand(10000)
        app_data_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{active_installation_id.to_i}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, true, '#{Date.today}', '#{Date.today}', NULL, 2)"
        ActiveRecord::Base.connection.insert(app_data_query)

        # Set up rules data linked to the active installation
        [["rules", "active rules part 1"], ["rules_1", "active rules part 2"], ["user_rules", "active user rules part 1"], ["user_rules_1", "active user rules part 2"]].each do |kv|
          key, value = kv
          app_data_query = "INSERT INTO apps_settings (id, account_id, installation_id, `key`, `value`, created_at, updated_at, parameter_id) VALUES (#{rand(10000)}, #{account.id.to_i}, #{active_installation_id.to_i}, '#{key}', '#{value}', '#{Date.today}', '#{Date.today}', 1)"
          ActiveRecord::Base.connection.insert(app_data_query)
        end

        # Assert queries run successfully and join together multi-row data and pass to `parse_cfa_into_ctf`
        empty_migration_obj = { successful: [], failed: [] }
        account.expects(:parse_cfa_into_ctf).with(:agent, "active rules part 1active rules part 2", false).returns(empty_migration_obj)
        account.expects(:parse_cfa_into_ctf).with(:end_user, "active user rules part 1active user rules part 2", false).returns(empty_migration_obj)

        account.migrate_cfa_to_ctf
      end

      it "returns empty results when CFA is not installed" do
        account = accounts(:systemaccount)
        empty_result = { successful: [], failed: [] }
        assert_equal empty_result, account.migrate_cfa_to_ctf
      end

      it "returns empty results when CFA is disabled" do
        account = accounts(:systemaccount)

        # Insert disabled installation
        installation_id = rand(10000)
        app_data_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, product_code) VALUES (#{installation_id.to_i}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, false, '#{Date.today}', '#{Date.today}', 1)"
        ActiveRecord::Base.connection.insert(app_data_query)

        # Set up rules data linked to the installation
        [["rules", "rules part 1"], ["rules_1", "rules part 2"], ["user_rules", "user rules part 1"], ["user_rules_1", "user rules part 2"]].each_with_index do |kv, i|
          key, value = kv
          app_data_query = "INSERT INTO apps_settings (id, account_id, installation_id, `key`, `value`, created_at, updated_at, parameter_id) VALUES (#{i}, #{account.id.to_i}, #{installation_id.to_i}, '#{key}', '#{value}', '#{Date.today}', '#{Date.today}', 1)"
          ActiveRecord::Base.connection.insert(app_data_query)
        end

        empty_result = { successful: [], failed: [] }
        # Check that we don't parse data using the available settings of the disabled installation
        account.expects(:parse_cfa_into_ctf).with(:agent, "", false).returns(empty_result)
        account.expects(:parse_cfa_into_ctf).with(:end_user, "", false).returns(empty_result)
        assert_equal empty_result, account.migrate_cfa_to_ctf
      end

      it "should delete the previous conditions" do
        account, _tfc_attributes = setup_cfa_agent_condition
        FactoryBot.create_list(:ticket_field_condition, 2, account: account)
        assert account.reload.ticket_field_conditions.size > 1
        account.migrate_cfa_to_ctf
        assert_equal 1, account.reload.ticket_field_conditions.size
      end

      it "should not delete the previous conditions in dry_run mode" do
        account, _tfc_attributes = setup_cfa_agent_condition
        FactoryBot.create_list(:ticket_field_condition, 2, account: account)
        assert_difference 'TicketFieldCondition.count', 0 do
          account.migrate_cfa_to_ctf(dry_run: true)
        end
        assert account.reload.ticket_field_conditions.size > 1
      end

      it "correctly passes dry_run option to parse_cfa_into_ctf" do
        account, _tfc_attributes = setup_cfa_agent_condition

        empty_result = { successful: [], failed: [] }
        account.expects(:parse_cfa_into_ctf).with(:agent, anything, true).returns(empty_result)
        account.expects(:parse_cfa_into_ctf).with(:end_user, anything, true).returns(empty_result)

        account.migrate_cfa_to_ctf(dry_run: true)
      end
    end

    describe "#parse_cfa_into_ctf" do
      it "should not create conditions in dry_run mode" do
        account, _tfc_attributes = setup_cfa_agent_condition(child_required: true)

        assert_difference 'TicketFieldCondition.count', 0 do
          migration_data = account.migrate_cfa_to_ctf(dry_run: true)
          assert migration_data[:successful].is_a?(Array)
          assert migration_data[:failed].is_a?(Array)
          assert_equal 1, migration_data[:successful].size
        end
      end

      it "should parse agent values and return a Hash of with Array values" do
        account, tfc_attributes = setup_cfa_agent_condition(child_required: true)

        assert_difference 'TicketFieldCondition.count', 1 do
          migration_data = account.migrate_cfa_to_ctf
          assert migration_data[:successful].is_a?(Array)
          assert migration_data[:failed].is_a?(Array)
        end

        new_condition = TicketFieldCondition.order(:created_at).last
        assert_equal new_condition.user_type, "agent"
        [:account_id, :ticket_form_id, :parent_field_id, :child_field_id, :value].each do |attr|
          assert_equal tfc_attributes[attr.to_s], new_condition.send(attr)
        end
      end

      it "should parse agent values and return a Hash of with Array values" do
        # Easiest way to set up all the right associations to target a new import is to use the factory then destroy the record in the db
        tfc = FactoryBot.create(:ticket_field_condition, :end_user)
        tfc_attributes = tfc.attributes
        account = tfc.account
        account.stubs(:find_cfa_data).returns(["", ctf_to_cta_string(tfc)])
        tfc.destroy

        assert_difference 'TicketFieldCondition.count', 1 do
          migration_data = account.migrate_cfa_to_ctf
          assert migration_data[:successful].is_a?(Array)
          assert migration_data[:failed].is_a?(Array)
        end

        new_condition = TicketFieldCondition.order(:created_at).last
        assert_equal new_condition.user_type, "end_user"
        [:account_id, :ticket_form_id, :parent_field_id, :child_field_id, :value].each do |attr|
          assert_equal tfc_attributes[attr.to_s], new_condition.send(attr)
        end
      end

      describe "required" do
        it "should mark `is_required` as true if the child field is included in the cfa Array" do
          account, = setup_cfa_agent_condition(child_required: true)

          account.migrate_cfa_to_ctf
          new_condition = TicketFieldCondition.order(:created_at).last
          assert new_condition.is_required
        end

        it "should mark `is_required` as false if the child field is not included in the cfa Array" do
          account, = setup_cfa_agent_condition(child_required: false)

          account.migrate_cfa_to_ctf
          new_condition = TicketFieldCondition.order(:created_at).last
          refute new_condition.is_required
        end

        it "should mark `is_required` as false if the `requireds` key is missing" do
          account, = setup_cfa_agent_condition(child_required: nil)

          account.migrate_cfa_to_ctf
          new_condition = TicketFieldCondition.order(:created_at).last
          refute new_condition.is_required
        end
      end

      describe "required_on_statuses" do
        it "should set the value of required_on_statuses on migration with agent condition" do
          account, = setup_cfa_agent_condition(child_required: true)

          TicketFieldCondition.any_instance.expects(:required_on_statuses=).with(StatusType.SOLVED.to_s).at_least_once
          account.migrate_cfa_to_ctf
        end

        it "should not set the value of required_on_statuses on migration with agent condition that is not required" do
          account, = setup_cfa_agent_condition(child_required: false)

          TicketFieldCondition.any_instance.expects(:required_on_statuses=).never
          account.migrate_cfa_to_ctf
        end

        it "should not set the value of required_on_statuses on migration with end user condition" do
          account, = setup_cfa_end_user_condition(child_required: true)

          TicketFieldCondition.any_instance.expects(:required_on_statuses=).never
          account.migrate_cfa_to_ctf
        end
      end
    end
  end

  describe "#cfa_to_ctf_dry_run" do
    let(:account) { accounts(:systemaccount) }

    it "calls migrate_cfa_to_ctf" do
      account.expects(:migrate_cfa_to_ctf).with(dry_run: true)
      account.cfa_to_ctf_dry_run
    end
  end

  describe "#multiple_enabled_cfa_installations?" do
    let(:account) { accounts(:systemaccount) }

    it "should return false when there is only one enabled installation" do
      # Enabled app
      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{rand(10000)}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, true, '#{Date.today}', '#{Date.today}', '#{Date.today}', 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      # Disabled app
      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{rand(10000)}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, false, '#{Date.today}', '#{Date.today}', '#{Date.today}', 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      refute account.multiple_enabled_cfa_installations?
    end

    it "should return true when there are multiple enabled installations" do
      # Enabled app
      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{rand(10000)}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, true, '#{Date.today}', '#{Date.today}', NULL, 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      # Enabled app
      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{rand(10000)}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, true, '#{Date.today}', '#{Date.today}', NULL, 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      assert account.multiple_enabled_cfa_installations?
    end
  end

  describe "#cfa_enabled?, #cfa_installed? and #cfa_installation_id" do
    let(:account) { accounts(:systemaccount) }
    let(:cfa_installation_id) { rand(10000) }

    it "should return false on both when CFA is not installed" do
      refute account.cfa_installed?
      refute account.cfa_enabled?
      assert_nil account.cfa_installation_id
    end

    it "should return the correct values when CFA is disabled" do
      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{cfa_installation_id}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, false, '#{Date.today}', '#{Date.today}', NULL, 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      assert account.cfa_installed?
      refute account.cfa_enabled?
      assert_equal cfa_installation_id, account.cfa_installation_id
    end

    it "should return true on both when CFA is enabled" do
      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{cfa_installation_id}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, true, '#{Date.today}', '#{Date.today}', NULL, 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      assert account.cfa_installed?
      assert account.cfa_enabled?
      assert_equal cfa_installation_id, account.cfa_installation_id
    end

    it "should return the correct values when CFA is soft deleted" do
      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{cfa_installation_id}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, false, '#{Date.today}', '#{Date.today}', '#{Date.today}', 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      refute account.cfa_installed?
      refute account.cfa_enabled?
      assert_nil account.cfa_installation_id
    end

    it "should return the correct values when there are several soft deleted CFA installations" do
      deleted_installation_id_1 = rand(10000)
      deleted_installation_id_2 = rand(10000)

      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{deleted_installation_id_1}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, false, '#{Date.today}', '#{Date.today}', '#{Date.today}', 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{deleted_installation_id_2}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, false, '#{Date.today}', '#{Date.today}', '#{Date.today}', 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      refute account.cfa_installed?
      refute account.cfa_enabled?
      assert_nil account.cfa_installation_id
    end

    it "should return the correct values with one active and several soft deleted CFA installations" do
      deleted_installation_id_1 = rand(10000)
      deleted_installation_id_2 = rand(10000)
      active_installation_id = rand(10000)

      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{deleted_installation_id_1}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, false, '#{Date.today}', '#{Date.today}', '#{Date.today}', 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{deleted_installation_id_2}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, false, '#{Date.today}', '#{Date.today}', '#{Date.today}', 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      insert_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, deleted_at, product_code) VALUES (#{active_installation_id}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, true, '#{Date.today}', '#{Date.today}', NULL, 1)"
      ActiveRecord::Base.connection.insert(insert_query)

      assert account.cfa_installed?
      assert account.cfa_enabled?
      assert_equal active_installation_id, account.cfa_installation_id
    end
  end

  describe "#cfa_migration_needed?" do
    let(:account) { accounts(:systemaccount) }

    describe "and account has NOT completed migration" do
      before { account.settings.native_conditional_fields_migrated = false }

      it "should return true when CFA is installed" do
        account.stubs(:cfa_installed?).returns(true)
        assert account.cfa_migration_needed?
      end

      it "should return false when CFA is not installed" do
        account.stubs(:cfa_installed?).returns(false)
        refute account.cfa_migration_needed?
      end
    end

    describe "and account has completed migration" do
      before { account.settings.native_conditional_fields_migrated = true }

      it "should return false when CFA is enabled" do
        account.stubs(:cfa_enabled?).returns(true)
        refute account.cfa_migration_needed?
      end

      it "should return false when CFA is not enabled" do
        account.stubs(:cfa_enabled?).returns(false)
        refute account.cfa_migration_needed?
      end
    end
  end

  describe "#reentry_ctf_eap" do
    let(:account) { accounts(:minimum) }

    before do
      FactoryBot.create(:ticket_field_condition, account: account)
    end

    it "changes the CTF flags and deletes built-in conditions" do
      account.update_attributes!(settings: { native_conditional_fields_abandoned: true })
      assert account.settings.native_conditional_fields_migrated?
      assert account.settings.native_conditional_fields_abandoned?
      refute account.ticket_field_conditions.empty?
      account.reentry_ctf_eap
      refute account.settings.native_conditional_fields_migrated?
      refute account.settings.native_conditional_fields_abandoned?
      assert_empty(account.ticket_field_conditions)
    end

    it "raises an error and doesn't do anything if the account didn't exit CTF EAP" do
      refute account.settings.native_conditional_fields_abandoned?
      assert_raise(StandardError) do
        account.reentry_ctf_eap
      end
      refute account.settings.native_conditional_fields_abandoned?
      refute account.ticket_field_conditions.empty?
    end
  end

  def ctf_to_cta_string(tfc, child_required = true)
    requireds = if child_required == true
      %("requireds":[#{tfc.child_field_id}],)
    elsif child_required == false
      %("requireds":[#{requireds}],)
    elsif child_required.nil?
      ""
    end

    %([{"field":#{tfc.parent_field_id},"value":"#{tfc.value}","select":[#{tfc.child_field_id}],"formId":#{tfc.ticket_form_id},"dirty":false,"creationDate":#{1.day.ago.to_i},#{requireds}"index":0,"valueText":"Field Value Text","fieldsText":"Name of Field","selected":false})
  end

  def setup_cfa_agent_condition(child_required: true)
    # Easiest way to set up all the right associations to target a new import is to use the factory then destroy the record in the db
    tfc = FactoryBot.create(:ticket_field_condition)
    tfc_attributes = tfc.attributes
    account = tfc.account
    account.stubs(:find_cfa_data).returns([ctf_to_cta_string(tfc, child_required), ""])
    tfc.destroy

    [account, tfc_attributes]
  end

  def setup_cfa_end_user_condition(child_required: true)
    # Easiest way to set up all the right associations to target a new import is to use the factory then destroy the record in the db
    tfc = FactoryBot.create(:ticket_field_condition, :end_user)
    tfc_attributes = tfc.attributes
    account = tfc.account
    account.stubs(:find_cfa_data).returns(["", ctf_to_cta_string(tfc, child_required)])
    tfc.destroy

    [account, tfc_attributes]
  end
end
