require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 11

describe Account::Localization do
  fixtures :accounts, :translation_locales

  describe '#set_time_zone' do
    before do
      Time.zone = 'UTC'
      @account  = Accounts::Classic.new
    end

    it 'sets timezone for valid number' do
      @account.set_time_zone('+8')
      assert_equal 'Beijing', @account.time_zone
      @account.set_time_zone('-8')
      assert_equal 'Pacific Time (US & Canada)', @account.time_zone
    end

    it 'sets to default for invalid number' do
      @account.set_time_zone('+911')
      assert_equal 'Eastern Time (US & Canada)', @account.time_zone
    end

    it 'sets timezone for valid time zone string' do
      @account.set_time_zone('Australia/Melbourne')
      assert_equal 'Australia/Melbourne', @account.time_zone
    end

    it 'sets to default for invalid time zone string' do
      @account.set_time_zone('NoSuchPlace')
      assert_equal 'Eastern Time (US & Canada)', @account.time_zone
    end

    it 'sets to default for flase value' do
      @account.set_time_zone(false)
      refute(@account.valid?)
      assert_equal 'Eastern Time (US & Canada)', @account.time_zone
    end
  end

  describe '#clear_locale_of_end_user' do
    let(:account) { accounts(:minimum) }

    before do
      account.locale_id = 1
      account.time_zone = 'UTC'
      account.save! # create a known starting point for the account
    end

    it 'calls clear_locale_of_end_user if locale changed' do
      account.expects(:clear_locale_of_end_user)

      account.locale_id = 34
      account.save!
    end

    it 'does not do translation queries when nothing was changed' do
      assert_sql_queries(0, /translation/) { account.save! }
    end

    it 'enqueues ResetEndUsersLocaleJob' do
      ResetEndUsersLocaleJob.expects(:enqueue).once

      account.locale_id = 34
      account.save!
    end

    it 'does not enqueue ResetEndUsersLocaleJob if locale_id is unchanged' do
      ResetEndUsersLocaleJob.expects(:enqueue).never

      account.time_zone = 'Pacific Time (US & Canada)'
      account.save!
    end

    it 'does not enqueue ResetEndUsersLocaleJob if the account was not saved' do
      ResetEndUsersLocaleJob.expects(:enqueue).never

      account.locale_id = 989
      with_rollback(account) { account.save! }
    end
  end

  describe '#restore_default_content' do
    let(:account) { accounts(:minimum) }
    let(:spanish) { translation_locales(:spanish) }
    let(:japanese) { translation_locales(:japanese) }

    before do
      account.locale_id = spanish.id
      account.time_zone = 'UTC'
      account.save!
    end

    it 'gets called if the locale_id changes' do
      account.expects(:restore_default_content).once

      account.locale_id = japanese.id
      account.save!
    end

    it 'enqueues the restore job if locale_id changes' do
      RestoreDefaultContentJob.expects(:enqueue).
        with(account.id, spanish.id, japanese.id).
        once

      account.locale_id = japanese.id
      account.save!
    end

    it 'wont enqueue the restore job if precreated account' do
      account.stubs(:pre_created_account?).returns(true)

      RestoreDefaultContentJob.expects(:enqueue).never
      account.locale_id = japanese.id
      account.save!
    end

    it 'wont enqueue the restore job if locale_id is unchanged' do
      RestoreDefaultContentJob.expects(:enqueue).never

      account.locale_id = spanish.id
      account.save!
    end

    it 'wont enqueue the restore job if the account fails to save' do
      RestoreDefaultContentJob.expects(:enqueue).never
      account.locale_id = japanese.id

      with_rollback(account) { account.save! }
    end
  end

  describe '#locales_for_selection' do
    let(:account)  { accounts(:with_groups) }
    let(:spanish)  { translation_locales(:spanish) }
    let(:japanese) { translation_locales(:japanese_x) }
    let(:keys)     { translation_locales(:norwegian) }

    before { keys.update_column(:locale, 'en-x-keys') }

    it 'returns the available_for_account' do
      account.stubs(:web_portal_state).returns(:disabled)
      TranslationLocale.expects(:available_for_account).with(account).returns([spanish])
      assert_equal [spanish], account.locales_for_selection
    end

    describe 'when the account has test_locales enabled' do
      before { account.settings.allow_test_locales = true }

      it 'adds the locale to the list' do
        TranslationLocale.expects(:available_for_account).with(account).returns([spanish])
        assert_equal [spanish, japanese], account.locales_for_selection
      end

      describe 'when a test locale was already available' do
        before { TranslationLocale.stubs(:test_locales).returns([spanish]) }

        it 'does not list the test locale twice' do
          TranslationLocale.expects(:available_for_account).with(account).returns([spanish])
          assert_equal [spanish], account.locales_for_selection
        end
      end
    end

    describe 'when the account has keys_locales enabled' do
      before { account.settings.allow_keys_locales = true }

      it 'adds the locale to the list' do
        TranslationLocale.expects(:available_for_account).with(account).returns([spanish])
        assert_equal [spanish, keys], account.locales_for_selection
      end
    end
  end

  def create_account_from_hash(values, set_owner = true, owner_is_verified = false)
    @new_account = Accounts::Classic.new do |a|
      a.name            = values[:name]
      a.subdomain       = values[:subdomain]
      a.time_zone       = values[:time_zone]
      a.help_desk_size  = values[:help_desk_size]
      a.trial_coupon_code = values[:trial_coupon_code]
    end

    @new_account.translation_locale = TranslationLocale.last

    if set_owner
      owner_attributes = {name: 'Random Person', email: 'radnom.person@example.com', is_verified: owner_is_verified}
      @new_account.set_owner(owner_attributes)
    end

    @new_account.set_address(country_code: 'SI', phone: '123456')
    @new_account
  end

  def default_account_hash
    {
      name: 'NewTestAccount',
      subdomain: Token.generate(10),
      time_zone: 'Copenhagen',
      help_desk_size: 'Small team'
    }
  end
end
