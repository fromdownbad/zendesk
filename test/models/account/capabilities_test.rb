require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 65

describe Account do
  fixtures :all

  describe "capabilities" do
    let(:account) { accounts(:minimum) }

    describe 'subscription-based features' do
      %i[
        play_tickets
        trigger_revision_history
        unlimited_automations
        unlimited_views
        unlimited_triggers
        user_and_organization_fields
        skill_based_ticket_routing
        eu_data_center
        germany_data_center
        us_data_center
        advanced_security
      ].each do |feature|
        describe "#has_#{feature}?" do
          describe 'when the subscription supports the feature' do
            before { account.subscription.stubs("has_#{feature}?" => true) }

            it 'returns true' do
              assert account.send("has_#{feature}?")
            end
          end

          describe 'when the subscription does not support the feature' do
            before { account.subscription.stubs("has_#{feature}?" => false) }

            it 'returns false' do
              refute account.send("has_#{feature}?")
            end
          end
        end
      end
    end

    describe '#has_skill_based_attribute_ticket_mapping?' do
      describe 'when the subscription supports the feature' do
        before { account.subscription.stubs("has_skill_based_attribute_ticket_mapping?" => true) }

        describe 'when skill_based_ticket_routing is enabled' do
          before { account.stubs(has_skill_based_ticket_routing?: true) }

          it 'returns true' do
            assert account.send("has_skill_based_attribute_ticket_mapping?")
          end
        end

        describe 'when skill_based_ticket_routing is disabled' do
          before { account.stubs(has_skill_based_ticket_routing?: false) }

          it 'returns false' do
            refute account.send("has_skill_based_attribute_ticket_mapping?")
          end
        end
      end

      describe 'when the subscription does not support the feature' do
        before { account.subscription.stubs("has_skill_based_attribute_ticket_mapping?" => false) }

        it 'returns false' do
          refute account.send("has_skill_based_attribute_ticket_mapping?")
        end
      end
    end

    describe '#has_rerun_skill_based_routing_rules?' do
      describe 'when skill_based_ticket_routing is enabled' do
        before do
          account.stubs(has_skill_based_ticket_routing?: true)
          Arturo.enable_feature!(:rerun_skill_based_routing_rules)
        end

        it 'returns true' do
          assert account.send("has_rerun_skill_based_routing_rules?")
        end
      end

      describe 'when skill_based_ticket_routing is disabled' do
        before { account.stubs(has_skill_based_ticket_routing?: false) }

        it 'returns false' do
          refute account.send("has_rerun_skill_based_routing_rules?")
        end
      end
    end

    describe 'polaris feature' do
      # TO-DO: Update tests after GA
      before do
        account.stubs(has_active_suite?: true)
      end

      it 'is enabled if both the arturo and setting are true' do
        Arturo.enable_feature!(:polaris)
        account.settings.polaris = true

        assert_predicate account, :has_polaris?
      end

      it 'is disabled if the setting is false' do
        Arturo.enable_feature!(:polaris)
        account.settings.polaris = false
        refute_predicate account.settings, :polaris? # setting is disabled

        refute_predicate account, :has_polaris?
      end

      it 'is disabled if the arturo is false' do
        Arturo.disable_feature!(:polaris)
        account.settings.polaris = true

        refute_predicate account, :has_polaris?
      end

      it 'is disabled if both arturo and setting is false' do
        Arturo.disable_feature!(:polaris)

        account.settings.polaris = false
        refute_predicate account.settings, :polaris? # setting is disabled

        refute_predicate account, :has_polaris?
      end
    end

    describe 'trigger_categories_api feature' do
      it 'is enabled if both the arturo and setting are true' do
        Arturo.enable_feature!(:trigger_categories)
        account.settings.trigger_categories_api = true

        assert_predicate account, :has_trigger_categories_api?
      end

      it 'is disabled if the setting is false' do
        Arturo.enable_feature!(:trigger_categories)
        account.settings.trigger_categories_api = false
        refute_predicate account.settings, :trigger_categories_api? # setting is disabled

        refute_predicate account, :has_trigger_categories_api?
      end

      it 'is disabled if the arturo is false' do
        Arturo.disable_feature!(:trigger_categories)
        account.settings.trigger_categories_api = true

        refute_predicate account, :has_trigger_categories_api?
      end

      it 'is disabled if both arturo and setting is false' do
        Arturo.disable_feature!(:trigger_categories)

        account.settings.trigger_categories_api = false
        refute_predicate account.settings, :trigger_categories_api? # setting is disabled

        refute_predicate account, :has_trigger_categories_api?
      end
    end

    describe 'agent workspace social messaging' do
      describe_with_and_without_arturo_setting_enabled :polaris do
        it { refute_predicate account, :has_social_messaging_agent_workspace? }
      end

      describe 'when feature is available but not enabled' do
        before do
          Arturo.enable_feature!(:social_messaging_agent_workspace)
          account.settings.social_messaging_agent_workspace = false
        end

        describe_with_arturo_enabled :polaris do
          it { refute_predicate account, :has_social_messaging_agent_workspace? }
        end

        describe 'when feature is available and enabled' do
          before { account.settings.social_messaging_agent_workspace = true }

          describe_with_arturo_disabled :polaris do
            it { refute_predicate account, :has_social_messaging_agent_workspace? }
          end

          describe_with_arturo_enabled :polaris do
            it { assert_predicate account, :has_social_messaging_agent_workspace? }
          end
        end
      end
    end

    describe 'agent workspace native messaging' do
      describe_with_and_without_arturo_setting_enabled :polaris do
        describe_with_and_without_arturo_setting_enabled :social_messaging_agent_workspace do
          it { refute_predicate account, :has_native_messaging? }
        end
      end

      describe 'when feature is available and enabled' do
        before do
          Arturo.enable_feature!(:polaris)
          account.settings.polaris = true
          account.settings.native_messaging = true
        end

        # both polaris and social_messaging_agent_workspace are true
        describe_with_arturo_enabled :social_messaging_agent_workspace do
          it { assert_predicate account, :has_native_messaging? }
        end
      end

      describe 'when feature is not available (social_messaging_agent_workspace is false) and enabled' do
        before do
          account.settings.social_messaging_agent_workspace = false
          account.settings.native_messaging = true
        end

        describe_with_and_without_arturo_setting_enabled :polaris do
          it { refute_predicate account, :has_native_messaging? }
        end
      end

      describe 'when feature is not available (polaris is false) and enabled' do
        before do
          account.settings.polaris = false
          account.settings.native_messaging = true
        end

        describe_with_and_without_arturo_setting_enabled :social_messaging_agent_workspace do
          it { refute_predicate account, :has_native_messaging? }
        end
      end
    end

    describe 'social messaging account setting' do
      describe_with_and_without_arturo_setting_enabled :polaris do
        describe_with_and_without_arturo_setting_enabled :social_messaging_agent_workspace do
          it { refute_predicate account, :has_social_messaging? }
        end
      end

      describe 'when feature is available and enabled' do
        before do
          Arturo.enable_feature!(:polaris)
          account.settings.polaris = true
          account.settings.social_messaging = true
        end

        # both social_messaging_agent_workspace and polaris are true
        describe_with_arturo_enabled :social_messaging_agent_workspace do
          it { assert_predicate account, :has_social_messaging? }
        end
      end

      describe 'when feature is not available (social_messaging_agent_workspace is false) and enabled' do
        before do
          account.settings.social_messaging_agent_workspace = false
          account.settings.social_messaging = true
        end

        describe_with_and_without_arturo_setting_enabled :polaris do
          it { refute_predicate account, :has_social_messaging? }
        end
      end

      describe 'when feature is not available (polaris is false) and enabled' do
        before do
          account.settings.polaris = false
          account.settings.social_messaging = true
        end

        describe_with_and_without_arturo_setting_enabled :social_messaging_agent_workspace do
          it { refute_predicate account, :has_social_messaging? }
        end
      end
    end

    describe "#has_native_messaging_csat?" do
      describe "has native_messaging, enable csat setting, csat arturo, native_messaging_csat arturo" do
        before do
          account.stubs(has_native_messaging?: true)
          Arturo.enable_feature!(:customer_satisfaction)
          account.settings.customer_satisfaction = true
          Arturo.enable_feature!(:native_messaging_csat)
        end

        it { assert_predicate account, :has_native_messaging_csat? }
      end

      describe "no native_messaging, enable csat setting, csat arturo, native_messaging_csat arturo" do
        before do
          account.stubs(has_native_messaging?: false)
          Arturo.enable_feature!(:customer_satisfaction)
          account.settings.customer_satisfaction = true
          Arturo.enable_feature!(:native_messaging_csat)
        end

        it { refute_predicate account, :has_native_messaging_csat? }
      end

      describe "has native_messaging, disable csat setting, enable csat arturo, native_messaging_csat arturo" do
        before do
          account.stubs(has_native_messaging?: true)
          Arturo.enable_feature!(:customer_satisfaction)
          account.settings.customer_satisfaction = false
          Arturo.enable_feature!(:native_messaging_csat)
        end

        it { refute_predicate account, :has_native_messaging_csat? }
      end

      describe "has native_messaging, disable csat arturo, enable native_messaging_csat arturo" do
        before do
          account.stubs(has_native_messaging?: true)
          Arturo.disable_feature!(:customer_satisfaction)
          Arturo.enable_feature!(:native_messaging_csat)
        end

        it { refute_predicate account, :has_native_messaging_csat? }
      end

      describe "has native_messaging, enable csat setting, disable native_messaging_csat arturo" do
        before do
          account.stubs(has_native_messaging?: true)
          Arturo.enable_feature!(:customer_satisfaction)
          account.settings.customer_satisfaction = true
          Arturo.disable_feature!(:native_messaging_csat)
        end

        it { refute_predicate account, :has_native_messaging_csat? }
      end
    end

    describe 'features related to Side Conversations' do
      let(:account) { accounts(:multiproduct) }

      it { refute account.is_side_conversations_email_enabled }
      it { refute account.is_side_conversations_slack_enabled }
      it { refute account.settings.side_conversations_email }
      it { refute account.settings.side_conversations_slack }
      it { refute account.settings.ticket_threads }
      it { refute account.has_side_conversations? }

      describe '#is_side_conversations_slack_enabled' do
        before do
          Arturo.disable_feature!(:collaboration_slack)
          Arturo.disable_feature!(:side_conversations_slack_ga)
        end
        describe 'when collaboration_slack is on and the side_conversations_slack_ga arturo is on' do
          before do
            Arturo.enable_feature!(:collaboration_slack)
            Arturo.enable_feature!(:side_conversations_slack_ga)
            AccountSetting.where(name: 'side_conversations_slack').destroy_all
          end

          it 'returns the value of the setting' do
            refute account.is_side_conversations_slack_enabled
            account.settings.side_conversations_slack = true
            assert account.is_side_conversations_slack_enabled
          end
        end

        describe 'when the arturo is on and setting does not exist yet' do
          before do
            Arturo.enable_feature!(:collaboration_slack)
            AccountSetting.where(name: 'side_conversations_slack').destroy_all
          end

          it 'returns true' do
            assert account.is_side_conversations_slack_enabled
          end
        end

        describe 'when the "side_conversations_slack" setting is enabled' do
          before { account.settings.side_conversations_slack = true }

          it 'returns true' do
            assert account.is_side_conversations_slack_enabled
          end
        end
      end

      describe '#is_side_conversations_email_enabled' do
        describe 'when the "ticket_threads" setting is enabled' do
          before { account.settings.ticket_threads = true }

          it 'returns true' do
            assert account.is_side_conversations_email_enabled
          end
        end

        describe 'when the "ticket_threads" setting is disabled' do
          before { account.settings.ticket_threads = false }

          describe 'when the "side_conversations_email" setting is enabled' do
            before { account.settings.side_conversations_email = true }

            it 'returns true' do
              assert account.is_side_conversations_email_enabled
            end
          end
        end
      end

      describe '#is_side_conversations_email_enabled=' do
        it 'sets both side_conversations_email and ticket_threads setting' do
          account.settings.ticket_threads = false
          account.settings.side_conversations_email = false

          account.is_side_conversations_email_enabled = true

          assert account.settings.ticket_threads
          assert account.settings.side_conversations_email
        end
      end

      describe '#has_side_conversations?' do
        describe 'ticket_threads subscription feature' do
          describe 'is off' do
            before do
              account.subscription.stubs(has_ticket_threads?: false)
            end

            it 'is false' do
              refute account.has_side_conversations?
            end
          end

          describe 'is on' do
            before do
              account.subscription.stubs(has_ticket_threads?: true)
            end

            it 'is true' do
              assert account.has_side_conversations?
            end
          end
        end

        describe 'ticket_threads_eap feature' do
          describe 'is off' do
            before do
              Arturo.disable_feature!(:ticket_threads_eap)
            end

            it 'is false' do
              refute account.has_side_conversations?
            end
          end

          describe 'is on' do
            before do
              Arturo.enable_feature!(:ticket_threads_eap)
            end

            it 'is true' do
              assert account.has_side_conversations?
            end
          end
        end
      end

      describe '#side_conversations_tickets' do
        describe_with_arturo_enabled :side_conversations do
          describe 'with the arturo turned on' do
            before do
              Arturo.enable_feature!(:lotus_feature_side_conversations_ticket_channel)
            end

            describe 'and the setting off' do
              before do
                account.settings.side_conversations_tickets = false
              end

              it 'is false' do
                refute account.has_side_conversations_tickets?
              end
            end

            describe 'and the setting on' do
              before do
                account.settings.side_conversations_tickets = true
              end

              it 'is true' do
                assert account.has_side_conversations_tickets?
              end
            end
          end

          describe 'with the arturo turned off' do
            before do
              Arturo.disable_feature!(:lotus_feature_side_conversations_ticket_channel)
            end

            describe 'and the setting on' do
              before do
                account.settings.side_conversations_tickets = true
              end

              it 'is false' do
                refute account.has_side_conversations_tickets?
              end
            end
          end
        end

        describe_with_arturo_disabled :side_conversations do
          describe 'with the arturo turned on' do
            before do
              Arturo.enable_feature!(:lotus_feature_side_conversations_ticket_channel)
            end

            describe 'and the setting on' do
              before do
                account.settings.side_conversations_tickets = true
              end

              it 'is false' do
                refute account.has_side_conversations_tickets?
              end
            end
          end
        end
      end
    end

    describe 'features related to collaborations' do
      let(:account) { accounts(:multiproduct) }

      def arturo_enabled?
        account.send("has_#{feature}?".to_sym)
      end

      def feature_enabled?
        account.send("has_#{feature}_enabled?".to_sym)
      end

      describe '#collaboration_enabled' do
        let(:feature) { :collaboration_enabled }

        # By default: false (In account/properties.rb)
        it { refute account.settings.collaboration_enabled }
        it { refute account.is_collaboration_enabled? } # AKA

        describe 'when the "collaboration_enabled" setting is enabled' do
          # Delegated to `account.settings.collaboration_enabled` setting in `capabilities.rb`.
          before { account.is_collaboration_enabled = true }

          describe 'when the new collaboration is disabled' do
            before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: false) }
            it { assert feature_enabled? }
            it { assert arturo_enabled? }
            it { assert account.is_collaboration_enabled? } # AKA
          end

          describe 'when the new collaboration is enabled' do
            before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }
            it { refute feature_enabled? }
            it { refute arturo_enabled? }
            it { refute account.is_collaboration_enabled? } # AKA
          end
        end

        describe 'when the "collaboration_enabled" setting is disabled' do
          # Delegated to `account.settings.collaboration_enabled` setting in `capabilities.rb`.
          before { account.is_collaboration_enabled = false }

          describe 'when the new collaboration is disabled' do
            before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: false) }
            it { refute feature_enabled? }
            it { refute account.is_collaboration_enabled? } # AKA
          end

          describe 'when the new collaboration is enabled' do
            before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }
            it { refute feature_enabled? }
            it { refute account.is_collaboration_enabled? } # AKA
          end
        end
      end

      describe '#follower_and_email_cc_collaborations' do
        let(:feature) { :follower_and_email_cc_collaborations }

        # By default: false (In account/properties.rb)
        it { refute account.settings.follower_and_email_cc_collaborations }

        describe 'when the setting is enabled' do
          before do
            account.settings.follower_and_email_cc_collaborations = true
            account.settings.save!
          end

          describe 'when the email_ccs Arturo is disabled' do
            before { account.stubs(has_email_ccs?: false) }
            it { refute feature_enabled? }
            it { refute arturo_enabled? }
          end

          describe 'when the email_ccs Arturo is enabled' do
            before { account.stubs(has_email_ccs?: true) }
            it { assert feature_enabled? }
            it { assert arturo_enabled? }
          end
        end
      end

      describe '#comment_email_ccs_allowed' do
        let(:feature) { :comment_email_ccs_allowed }

        # By default: true (In account/properties.rb)
        it { assert account.settings.comment_email_ccs_allowed }

        describe 'when the new collaboration is disabled' do
          before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: false) }
          it { refute feature_enabled? }
          it { refute arturo_enabled? }
        end

        describe 'when the new collaboration is enabled' do
          before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }
          it { assert feature_enabled? }
          it { assert arturo_enabled? }
        end
      end

      describe '#light_agent_email_ccs_allowed' do
        let(:feature) { :light_agent_email_ccs_allowed }

        # By default: false (In account/properties.rb)
        it { refute account.settings.light_agent_email_ccs_allowed }

        describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
          it { refute feature_enabled? }
          it { refute arturo_enabled? }
        end

        describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it { refute feature_enabled? }
            it { assert arturo_enabled? }
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it { refute feature_enabled? }
            it { refute arturo_enabled? }
          end
        end
      end

      describe '#ticket_followers_allowed' do
        let(:feature) { :ticket_followers_allowed }

        # By default: true (In account/properties.rb)
        it { assert account.settings.ticket_followers_allowed }

        describe 'when the new collaboration is disabled' do
          before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: false) }
          it { refute feature_enabled? }
          it { refute arturo_enabled? }
        end

        describe 'when the new collaboration is enabled' do
          before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }
          it { assert feature_enabled? }
          it { assert arturo_enabled? }
        end
      end

      describe '#agent_can_change_requester' do
        let(:feature) { :agent_can_change_requester }

        # By default: true (In account/properties.rb)
        it { assert account.settings.agent_can_change_requester }

        describe 'when the new collaboration is disabled' do
          before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: false) }
          it { refute feature_enabled? }
          it { refute arturo_enabled? }
        end

        describe 'when the new collaboration is enabled' do
          before { account.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }
          it { assert feature_enabled? }
          it { assert arturo_enabled? }
        end
      end

      describe '#agent_email_ccs_become_followers' do
        let(:feature) { :agent_email_ccs_become_followers }

        # By default: true (In account/properties.rb)
        it { assert account.settings.agent_can_change_requester }

        describe 'when either "Email CCs" or "Followers" settings are disabled' do
          before do
            account.stubs(
              has_comment_email_ccs_allowed_enabled?: false,
              has_ticket_followers_allowed_enabled?: true
            )
          end

          it { refute feature_enabled? }
          it { refute arturo_enabled? }
        end

        describe 'when both "Email CCs" or "Followers" settings are enabled' do
          before do
            account.stubs(
              has_comment_email_ccs_allowed_enabled?: true,
              has_ticket_followers_allowed_enabled?: true
            )
          end

          it { assert feature_enabled? }
          it { assert arturo_enabled? }
        end
      end
    end

    describe '#is_collaborators_addable_only_by_agents=' do
      let(:collaborators_addable_only_by_agents) { true }

      before do
        account.is_collaborators_addable_only_by_agents = collaborators_addable_only_by_agents
        account.save!
      end

      describe 'when new value is true' do
        it { assert account.settings.collaborators_addable_only_by_agents }
      end

      describe 'when new value is false' do
        let(:collaborators_addable_only_by_agents) { false }

        it { refute account.settings.collaborators_addable_only_by_agents }
      end
    end

    describe '#is_collaborators_addable_only_by_agents?' do
      let(:collaborators_addable_only_by_agents) { true }

      before do
        account.settings.collaborators_addable_only_by_agents = collaborators_addable_only_by_agents
        account.settings.save!
      end

      describe 'when setting is enabled' do
        describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
          it { assert account.is_collaborators_addable_only_by_agents? }
        end

        describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
          it { refute account.is_collaborators_addable_only_by_agents? }
        end
      end

      describe 'when setting is disabled' do
        let(:collaborators_addable_only_by_agents) { false }

        describe_with_and_without_arturo_setting_enabled :follower_and_email_cc_collaborations do
          it { refute account.is_collaborators_addable_only_by_agents? }
        end
      end
    end

    describe '#has_rule_admin_rollbar?' do
      describe 'when the `rule_admin_rollbar` arturo is enabled' do
        before { Arturo.enable_feature!(:rule_admin_rollbar) }

        it 'returns true' do
          assert account.has_rule_admin_rollbar?
        end

        describe 'and the enviroment is development' do
          before { Rails.env.stubs(development?: true) }

          it 'returns false' do
            refute account.reload.has_rule_admin_rollbar?
          end
        end
      end

      describe 'when the `rule_admin_rollbar` arturo is disabled' do
        before { Arturo.disable_feature!(:rule_admin_rollbar) }

        it 'returns false' do
          refute account.has_rule_admin_rollbar?
        end
      end
    end

    describe '#has_side_conversation_macros?' do
      describe 'and the subscription has the ticket_threads feature' do
        before do
          account.subscription.features.ticket_threads = true
          account.subscription.save!
        end

        describe 'and ticket_threads setting is enabled' do
          before do
            account.settings.ticket_threads = true
          end

          it 'is true' do
            assert account.has_side_conversation_macros?
          end
        end

        describe 'and ticket_threads setting is disabled' do
          before do
            account.settings.ticket_threads = false
          end

          it 'is false' do
            refute account.has_side_conversation_macros?
          end
        end
      end

      describe 'when ticket_threads_eap is disabled' do
        before do
          Arturo.disable_feature!(:ticket_threads_eap)
        end

        describe 'and ticket_threads setting is disabled' do
          before do
            account.settings.ticket_threads = false
            account.settings.save!
          end

          it 'is false' do
            refute account.has_side_conversation_macros?
          end
        end

        describe 'and ticket_threads setting is enabled' do
          before do
            account.settings.ticket_threads = true
            account.settings.save!
          end

          it 'is false' do
            refute account.has_side_conversation_macros?
          end
        end
      end

      describe 'when ticket_threads_eap is enabled' do
        before do
          Arturo.enable_feature!(:ticket_threads_eap)
        end

        describe 'and the ticket_threads setting is enabled' do
          before do
            account.settings.ticket_threads = true
            account.settings.save!
          end

          it 'is true' do
            assert account.has_side_conversation_macros?
          end
        end

        describe 'and the ticket_threads setting is disabled' do
          before do
            account.settings.ticket_threads = false
            account.settings.save!
          end

          it 'is false' do
            refute account.has_side_conversation_macros?
          end
        end
      end
    end

    describe '#has_side_conversation_slack_macros?' do
      describe 'the account has Side Conversations and the slack macros arturo' do
        before do
          account.stubs(has_ticket_threads?: true)
        end

        describe 'and Side Conversations Slack is ON' do
          before do
            account.settings.side_conversations_slack = true
            account.settings.save!
          end

          it 'is true' do
            assert account.has_side_conversation_slack_macros?
          end
        end

        describe 'and Side Conversations Slack is OFF' do
          before do
            account.settings.side_conversations_slack = false
            account.settings.save!
          end

          it 'is false' do
            refute account.has_side_conversation_slack_macros?
          end
        end
      end

      describe 'when the account does not have Side Conversations' do
        before do
          account.stubs(has_ticket_threads?: false)
        end

        it 'returns false' do
          refute account.has_side_conversation_slack_macros?
        end
      end
    end

    describe '#has_side_conversation_ticket_macros?' do
      describe 'the account has Side Conversations and the child tickets macros arturo is on' do
        before do
          Arturo.enable_feature!(:lotus_feature_side_conversations_ticket_channel)
          Arturo.enable_feature!(:side_conversation_ticket_macros)
          account.stubs(has_ticket_threads?: true)
        end

        describe 'and Side Conversations child tickets feature is ON' do
          before do
            account.settings.side_conversations_tickets = true
            account.settings.save!
          end

          it 'is true' do
            assert account.has_side_conversation_ticket_macros?
          end
        end

        describe 'and Side Conversations child tickets feature is OFF' do
          before do
            account.settings.side_conversations_tickets = false
            account.settings.save!
          end

          it 'is false' do
            refute account.has_side_conversation_slack_macros?
          end
        end
      end

      describe 'when the account does not have Side Conversations' do
        before do
          account.stubs(has_ticket_threads?: false)
        end

        it 'returns false' do
          refute account.has_side_conversation_ticket_macros?
        end
      end
    end

    describe "#has_side_conversations_email?" do
      it 'returns false if both settings are false' do
        account.settings.side_conversations_email = false
        account.settings.ticket_threads = false
        refute account.has_side_conversations_email?
      end

      it 'returns true if the setting "side_conversations_email" is true' do
        account.settings.side_conversations_email = true
        account.settings.ticket_threads = false
        assert account.has_side_conversations_email?
      end

      it 'returns true if the setting "ticket_threads" is true' do
        account.settings.side_conversations_email = false
        account.settings.ticket_threads = true
        assert account.has_side_conversations_email?
      end
    end

    describe "#has_side_conversations_slack?" do
      before do
        AccountSetting.where(name: 'side_conversations_slack').destroy_all
      end

      it 'returns false if both the setting "side_conversations_slack" and arturo is false' do
        account.settings.side_conversations_slack = false
        Arturo.disable_feature!(:collaboration_slack)
        refute account.has_side_conversations_slack?
      end

      it 'returns true if the setting "side_conversations_slack" is true and arturo is off' do
        account.settings.side_conversations_slack = true
        Arturo.disable_feature!(:collaboration_slack)
        assert account.has_side_conversations_slack?
      end

      it 'returns true if the arturo is on and the setting does not exist' do
        Arturo.enable_feature!(:collaboration_slack)
        assert account.has_side_conversations_slack?
      end

      it 'does not use the arturo if the setting "side_conversations_slack" has been set' do
        Arturo.enable_feature!(:collaboration_slack)
        Arturo.disable_feature!(:side_conversations_slack_ga)
        account.settings.side_conversations_slack = false
        Arturo.expects(:feature_enabled_for?).with(:side_conversations_slack_ga, account).once
        Arturo.expects(:feature_enabled_for?).with(:collaboration_slack, account).never
        assert_equal false, account.has_side_conversations_slack?
      end
    end

    describe "#has_agent_collision?" do
      before do
        agents = stub
        agents.stubs(:count).returns(3)
        account.stubs(:agents).returns(agents)
        account.subscription.stubs(has_agent_collision?: true)
      end

      it "returns true if the agent count, the subscription all say so" do
        assert account.has_agent_collision?
      end

      it "returns false if the account only has one agent" do
        account.stubs(:agents).returns([Object.new])
        refute account.has_agent_collision?
      end

      it "returns false if the subscription does not support it" do
        account.subscription.stubs(has_agent_collision?: false)
        refute account.has_agent_collision?
      end
    end

    describe "#has_polaris_option?" do
      it "returns true if has polaris feature and compatible plan" do
        account.stubs(polaris_compatible_plan?: true)
        Arturo.enable_feature!(:polaris)
        assert account.has_polaris_option?
      end

      it "returns true if has polaris override feature" do
        Arturo.enable_feature!(:lotus_feature_polaris_override)
        assert account.has_polaris_option?
      end
    end

    describe "#display_group_and_department_migration?" do
      before do
        account.stubs(has_polaris_option?: true)
      end

      it "returns true if chat departments migration is not done" do
        account.stubs(polaris_chat_compatible_plan?: true)
        account.settings.stubs(check_group_name_uniqueness: false)
        assert account.display_group_and_department_migration?
      end

      it "returns false if chat departments migration is done" do
        account.stubs(polaris_chat_compatible_plan?: true)
        account.settings.stubs(check_group_name_uniqueness: true)
        refute account.display_group_and_department_migration?
      end

      it "returns true if account is standalone polaris" do
        account.stubs(polaris_chat_compatible_plan?: false)
        refute account.display_group_and_department_migration?
      end
    end

    describe "#has_finished_group_and_department_migration?" do
      before do
        account.stubs(has_polaris_option?: true)
      end

      it "returns true if chat departments migration done" do
        account.stubs(polaris_chat_compatible_plan?: true)
        account.settings.stubs(check_group_name_uniqueness: true)
        assert account.has_finished_group_and_department_migration?
      end

      it "returns false if chat departments migration not done" do
        account.stubs(polaris_chat_compatible_plan?: true)
        account.settings.stubs(check_group_name_uniqueness: false)
        refute account.has_finished_group_and_department_migration?
      end

      it "returns true if account is standalone polaris" do
        account.stubs(polaris_chat_compatible_plan?: false)
        assert account.has_finished_group_and_department_migration?
      end

      it "returns false if has_polaris_option? is false" do
        account.stubs(polaris_chat_compatible_plan?: false)
        account.stubs(has_polaris_option?: false)
        refute account.has_finished_group_and_department_migration?
      end
    end

    describe "#has_reengagement_enabled?" do
      describe "when native messaging is disabled" do
        before do
          account.stubs(has_native_messaging?: false)
          account.settings.reengagement = true
          Arturo.enable_feature!(:reengagement_enabled)
        end

        it "returns false" do
          refute account.has_reengagement_enabled?
        end
      end

      describe "when reengagement is disabled" do
        before do
          account.stubs(has_native_messaging?: true)
          account.settings.reengagement = false
          Arturo.enable_feature!(:reengagement_enabled)
        end

        it "returns false" do
          refute account.has_reengagement_enabled?
        end
      end

      describe "when arturo gate is disabled" do
        before do
          account.stubs(has_native_messaging?: true)
          account.settings.reengagement = true
          Arturo.disable_feature!(:reengagement_enabled)
        end

        it "returns false" do
          refute account.has_reengagement_enabled?
        end
      end

      describe "when native messaging, reenagement, and gate are enabled" do
        before do
          account.stubs(has_native_messaging?: true)
          account.settings.reengagement = true
          Arturo.enable_feature!(:reengagement_enabled)
        end

        it "returns true" do
          assert account.has_reengagement_enabled?
        end
      end
    end

    describe "#has_user_views_sampling_enabled?" do
      before do
        account.settings.user_count = UserView::SAMPLING_THRESHOLD + 1
      end

      it "returns true if all conditions are satisfied" do
        assert account.has_user_views_sampling_enabled?
      end

      it "returns false if the number of users is below the sampling threshold" do
        account.settings.user_count = UserView::SAMPLING_THRESHOLD - 1
        refute account.has_user_views_sampling_enabled?
      end
    end

    describe "#has_article_labels?" do
      it "returns true when arturo feature is enabled" do
        Arturo.enable_feature!(:help_center_grandfather_labels)

        assert account.has_article_labels?
      end

      it "returns true when subscription is plus or enterprise" do
        supported_plans = [SubscriptionPlanType.LARGE, SubscriptionPlanType.EXTRALARGE]
        supported_plans.each do |type|
          account.subscription.plan_type = type
          Zendesk::Features::SubscriptionFeatureService.new(account).execute!

          assert account.has_article_labels?
        end
      end

      it "returns false when subscription is regular or starter" do
        non_supported_plans = [SubscriptionPlanType.SMALL, SubscriptionPlanType.MEDIUM]
        non_supported_plans.each do |type|
          account.subscription.plan_type = type

          refute account.has_article_labels?
        end
      end
    end

    describe "#has_knowledge_bank_list_management" do
      it "returns true when subscription is plus or enterprise" do
        supported_plans = [SubscriptionPlanType.LARGE, SubscriptionPlanType.EXTRALARGE]
        supported_plans.each do |type|
          account.subscription.plan_type = type
          Zendesk::Features::SubscriptionFeatureService.new(account).execute!

          assert account.has_knowledge_bank_list_management?
        end
      end

      it "returns false when subscription is regular or starter" do
        non_supported_plans = [SubscriptionPlanType.SMALL, SubscriptionPlanType.MEDIUM]
        non_supported_plans.each do |type|
          account.subscription.plan_type = type

          refute account.has_knowledge_bank_list_management?
        end
      end
    end

    describe "#has_credit_card_sanitization_enabled?" do
      before do
        account.subscription.send(:write_attribute, :plan_type, SubscriptionPlanType.EXTRALARGE)
        Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        account.settings.credit_card_redaction = true
      end

      it "returns true if all conditions are satisfied" do
        assert account.has_credit_card_sanitization_enabled?
      end

      it "returns false if the setting is disabled" do
        account.settings.credit_card_redaction = false

        refute account.has_credit_card_sanitization_enabled?
      end

      it "returns false if the subscription is less than Plus" do
        account.subscription.send(:write_attribute, :plan_type, SubscriptionPlanType.SMALL)
        Zendesk::Features::SubscriptionFeatureService.new(account).execute!

        refute account.has_credit_card_sanitization_enabled?
      end
    end

    describe "#has_credit_card_sanitization?" do
      it "returns true when subscription is plus or enterprise" do
        supported_plans = [SubscriptionPlanType.LARGE, SubscriptionPlanType.EXTRALARGE]
        supported_plans.each do |type|
          account.subscription.send(:write_attribute, :plan_type, type)
          Zendesk::Features::SubscriptionFeatureService.new(account).execute!

          assert account.has_credit_card_sanitization?
        end
      end

      it "returns false when subscription is starter or regular" do
        unsupported_plans = [SubscriptionPlanType.SMALL, SubscriptionPlanType.MEDIUM]
        unsupported_plans.each do |type|
          account.subscription.send(:write_attribute, :plan_type, type)

          refute account.has_credit_card_sanitization?
        end
      end
    end

    describe "#has_help_center_google_analytics?" do
      it "returns true when arturo feature is enabled" do
        Arturo.enable_feature!(:help_center_grandfather_ga)

        assert account.has_help_center_google_analytics?
      end

      it "returns true when subscription is regular, plus or enterprise" do
        supported_plans = [SubscriptionPlanType.MEDIUM, SubscriptionPlanType.LARGE, SubscriptionPlanType.EXTRALARGE]
        supported_plans.each do |type|
          account.subscription.stubs(plan_type: type)

          assert account.has_help_center_google_analytics?
        end
      end

      it "returns false when subscription is starter" do
        account.subscription.plan_type = SubscriptionPlanType.SMALL
        Zendesk::Features::SubscriptionFeatureService.new(account).execute!

        refute account.has_help_center_google_analytics?
      end
    end

    describe "#enterprise_roles_in_user_views_enabled?" do
      describe "Enterprise customer" do
        before do
          account.subscription.plan_type = SubscriptionPlanType.ExtraLarge
        end

        it "returns true" do
          assert account.enterprise_roles_in_user_views_enabled?
        end
      end

      describe "Not Enterprise customer" do
        before do
          account.subscription.plan_type = SubscriptionPlanType.Large
        end

        it "returns false" do
          refute account.enterprise_roles_in_user_views_enabled?
        end
      end
    end

    describe "#has_hpm2_classic?" do
      describe "when the flag is enabled" do
        before do
          Arturo.enable_feature!(:hpm2_classic)
        end

        it "returns true" do
          assert account.has_hpm2_classic?
        end
      end

      describe "when the flag is disabled" do
        before do
          Arturo.disable_feature!(:hpm2_classic)
        end

        it "returns false" do
          refute account.has_hpm2_classic?
        end
      end
    end

    describe "#has_voice_recharge?" do
      describe "when the flag is enabled" do
        before do
          Arturo.enable_feature!(:voice_recharge)
        end

        it "returns true" do
          assert account.has_voice_recharge?
        end
      end
    end

    describe '#has_classic_reporting?' do
      describe 'when the account was created after April 30 2015' do
        before do
          account.created_at = Date.new(2015, 5, 10)
          account.save!
        end

        it 'returns false' do
          refute account.has_classic_reporting?
        end
      end

      describe 'when the account was created before April 30 2015' do
        before do
          account.created_at = Date.new(2015, 3, 1)
          account.save!
        end

        it 'returns true' do
          assert account.has_classic_reporting?
        end
      end
    end

    describe "#has_hc_powered_by_setting?" do
      describe "when has_hc_powered_by? is true" do
        before do
          account.stubs(has_hc_powered_by?: true)
        end

        describe "when subscription is Starter" do
          before do
            account.subscription.plan_type = SubscriptionPlanType.Small
          end

          it "returns false" do
            refute account.has_hc_powered_by_setting?
          end
        end

        describe "when subscription is Regular" do
          before do
            account.subscription.plan_type = SubscriptionPlanType.Medium
          end

          it "returns true" do
            assert account.has_hc_powered_by_setting?
          end
        end

        describe "when subscription is Plus" do
          before do
            account.subscription.plan_type = SubscriptionPlanType.Large
          end

          it "returns true" do
            assert account.has_hc_powered_by_setting?
          end
        end
      end
    end

    describe '#has_multiple_schedules?' do
      describe 'when the account has the `multiple_schedules` feature' do
        before do
          account.subscription.plan_type = SubscriptionPlanType.ExtraLarge

          Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        end

        it 'returns true' do
          assert account.has_multiple_schedules?
        end
      end

      describe 'when the account does not have the `multiple_schedules` feature' do
        before do
          account.subscription.plan_type = SubscriptionPlanType.Large

          Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        end

        it 'returns false' do
          refute account.has_multiple_schedules?
        end
      end
    end

    describe '#has_personal_rules?' do
      describe 'when the account has the `personal_rules` feature' do
        before do
          account.subscription.plan_type = SubscriptionPlanType.Large

          Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        end

        it 'returns true' do
          assert account.has_personal_rules?
        end
      end

      describe 'when the account does not have the `personal_rules` feature' do
        before do
          account.subscription.plan_type = SubscriptionPlanType.Small

          Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        end

        it 'returns false' do
          refute account.has_personal_rules?
        end
      end
    end

    describe '#has_group_rules?' do
      describe 'when the account has the `group_rules` feature' do
        before do
          account.subscription.plan_type = SubscriptionPlanType.Large

          Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        end

        it 'returns true' do
          assert account.has_group_rules?
        end
      end

      describe 'when the account does not have the `group_rules` feature' do
        before do
          account.subscription.plan_type = SubscriptionPlanType.Small

          Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        end

        it 'returns false' do
          refute account.has_group_rules?
        end
      end
    end

    describe '#business_hours_active?' do
      describe 'when the account has the `business_hours` feature' do
        before { account.stubs(has_business_hours?: true) }

        describe 'and the account has the `multiple_schedules` feature' do
          before { account.stubs(has_multiple_schedules?: true) }

          describe 'and the account has an active schedule' do
            before do
              Zendesk::BusinessHours::Schedule.create_default_for(account)
            end

            it 'returns true' do
              assert account.business_hours_active?
            end
          end

          describe 'and the account does not have an active schedule' do
            before do
              Zendesk::BusinessHours::Schedule.create_default_for(account).
                soft_delete
            end

            it 'returns false' do
              refute account.business_hours_active?
            end
          end
        end

        describe 'and the account does not have the `multiple_schedules` feature' do
          before { account.stubs(has_multiple_schedules?: false) }

          describe 'and the `business_hours` setting is enabled' do
            before { account.settings.enable(:business_hours) }

            describe 'and the account has an active schedule' do
              before do
                Zendesk::BusinessHours::Schedule.create_default_for(account)
              end

              it 'returns true' do
                assert account.business_hours_active?
              end
            end

            describe 'and the account does not have an active schedule' do
              before do
                Zendesk::BusinessHours::Schedule.create_default_for(account).
                  soft_delete
              end

              it 'returns false' do
                refute account.business_hours_active?
              end
            end
          end

          describe 'and the `business_hours` setting is disabled' do
            before { account.settings.disable(:business_hours) }

            it 'returns false' do
              refute account.business_hours_active?
            end

            describe 'and the account has an active schedule' do
              before do
                Zendesk::BusinessHours::Schedule.create_default_for(account)
              end

              it 'returns true' do
                assert account.business_hours_active?
              end
            end

            describe 'and the account does not have an active schedule' do
              before do
                Zendesk::BusinessHours::Schedule.create_default_for(account).
                  soft_delete
              end

              it 'returns false' do
                refute account.business_hours_active?
              end
            end
          end
        end
      end

      describe 'when the account does not have the `business_hours` feature' do
        before { account.stubs(has_business_hours?: false) }

        it 'returns false' do
          refute account.business_hours_active?
        end
      end
    end

    describe "#has_email_ccs?" do
      let(:account) { accounts(:minimum) }

      subject { account.has_email_ccs? }

      it { assert subject }
    end

    describe "#has_collaborators_settable_in_help_center?" do
      before do
        configure_account_for_settable_ccs_in_help_center!(account)
      end

      it "returns true for non-trials if CCs and CCs settable in HC are enabled, and agent-only CCs is disabled" do
        assert account.has_collaborators_settable_in_help_center?
      end

      it "returns false if the account is a trial" do
        account = accounts(:trial)
        configure_account_for_settable_ccs_in_help_center!(account)
        refute account.has_collaborators_settable_in_help_center?
      end

      it "returns false if CCs are disabled" do
        account.settings.collaboration_enabled = false
        refute account.has_collaborators_settable_in_help_center?
      end

      it "returns false if CCs settable in help center is disabled" do
        account.settings.collaborators_settable_in_help_center = false
        refute account.has_collaborators_settable_in_help_center?
      end

      it "returns true when the account is multiproduct" do
        account.stubs(:multiproduct?).returns(true)

        assert account.has_collaborators_settable_in_help_center?
      end

      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        it "returns false if agent-only CCs is enabled" do
          account.settings.collaborators_addable_only_by_agents = true
          refute account.has_collaborators_settable_in_help_center?
        end
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        it "returns true if agent-only CCs is enabled" do
          account.settings.collaborators_addable_only_by_agents = true
          assert account.has_collaborators_settable_in_help_center?
        end

        it "returns true if agent-only CCs is disabled" do
          account.settings.collaborators_addable_only_by_agents = false
          assert account.has_collaborators_settable_in_help_center?
        end
      end

      def configure_account_for_settable_ccs_in_help_center!(account)
        account.settings.collaboration_enabled = true
        account.settings.collaborators_addable_only_by_agents = false
        account.settings.collaborators_settable_in_help_center = true
      end
    end

    describe "#all_collaboration_disabled?" do
      let(:collaboration_enabled) { false }

      before { Account.any_instance.stubs(is_collaboration_enabled?: collaboration_enabled) }

      describe "when collaboration_enabled is false" do
        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          describe_with_arturo_setting_disabled :ticket_followers_allowed do
            it { assert(account.all_collaboration_disabled?) }
          end

          describe_with_arturo_setting_enabled :ticket_followers_allowed do
            it { assert_equal false, account.all_collaboration_disabled? }
          end
        end

        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          describe_with_and_without_arturo_setting_enabled :ticket_followers_allowed do
            it { assert_equal false, account.all_collaboration_disabled? }
          end
        end
      end

      describe "when collaboration_enabled is true" do
        let(:collaboration_enabled) { true }

        describe_with_and_without_arturo_setting_enabled :comment_email_ccs_allowed do
          describe_with_and_without_arturo_setting_enabled :ticket_followers_allowed do
            it { assert_equal false, account.all_collaboration_disabled? }
          end
        end
      end
    end

    describe "#has_satisfaction_prediction_enabled?" do
      before do
        Arturo.enable_feature!(:satisfaction_prediction)
        account.subscription.send(:write_attribute, :plan_type, SubscriptionPlanType.EXTRALARGE)
        Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        account.settings.satisfaction_prediction = true
      end

      it "returns true if all conditions are satisfied" do
        assert account.has_satisfaction_prediction_enabled?
      end

      it "returns false if the setting is disabled" do
        account.settings.satisfaction_prediction = false

        refute account.has_satisfaction_prediction_enabled?
      end

      it "returns false if the subscription is less than Plus" do
        account.subscription.send(:write_attribute, :plan_type, SubscriptionPlanType.SMALL)
        Zendesk::Features::SubscriptionFeatureService.new(account).execute!

        refute account.has_satisfaction_prediction_enabled?
      end

      it "returns false when the feature is not available" do
        Arturo.disable_feature!(:satisfaction_prediction)

        refute account.has_satisfaction_prediction_enabled?
      end
    end

    describe ".pci_required_accounts_on_shard" do
      it "does not return the account without the field" do
        assert_empty FieldPartialCreditCard.where(account_id: account.id)
        assert_empty Account.pci_required_accounts_on_shard
      end

      describe "when an account has a FieldPartialCreditCard" do
        before { @credit_card_field = FieldPartialCreditCard.create!(account: account, title: 'credit card') }

        it "returns the account with the field" do
          assert_equal([account], Account.pci_required_accounts_on_shard)
        end

        it "returns the account even if the field is not enabled" do
          @credit_card_field.update_attributes!(is_active: false)
          assert_equal([account], Account.pci_required_accounts_on_shard)
        end
      end
    end

    [:patagonia_enterprise_help_widget, :patagonia_plus_help_widget].each do |feature|
      describe "##{feature}" do
        before do
          Arturo.enable_feature!(feature.to_s.to_sym)
        end

        it "returns true when pricing model is = patagonia price plan" do
          account.subscription.send(:write_attribute, :pricing_model_revision, ZBC::Zendesk::PricingModelRevision::PATAGONIA)
          assert account.send("has_#{feature}?")
        end

        it "returns false when pricing model is < patagonia price plan" do
          account.subscription.send(:write_attribute, :pricing_model_revision, ZBC::Zendesk::PricingModelRevision::PATAGONIA - 1)
          refute account.send("has_#{feature}?")
        end
      end
    end
    [:help_widget, :plus_help_widget].each do |feature|
      describe "##{feature}" do
        before do
          Arturo.enable_feature!(feature.to_s.to_sym)
        end

        it "returns true when pricing model is < patagonia price plan" do
          account.subscription.send(:write_attribute, :pricing_model_revision, ZBC::Zendesk::PricingModelRevision::PATAGONIA - 1)
          assert account.send("has_#{feature}?")
        end

        it "returns false when pricing model is = patagonia price plan" do
          account.subscription.send(:write_attribute, :pricing_model_revision, ZBC::Zendesk::PricingModelRevision::PATAGONIA)
          refute account.send("has_#{feature}?")
        end
      end
    end
    [:plus_help_widget_chat, :plus_help_widget_question].each do |feature|
      describe "##{feature}" do
        before do
          Arturo.enable_feature!(feature.to_s.to_sym)
        end

        it "returns true when account is plus" do
          account.subscription.send(:write_attribute, :plan_type, SubscriptionPlanType.Large)
          assert account.send("has_#{feature}?")
        end
        it "returns false when account is account is not plus" do
          account.subscription.send(:write_attribute, :plan_type, SubscriptionPlanType.Small)
          refute account.send("has_#{feature}?")
        end
      end
    end
    [:regular_help_widget_question].each do |feature|
      describe "##{feature}" do
        before do
          Arturo.enable_feature!(feature.to_s.to_sym)
        end

        it "returns true when account is regular" do
          account.subscription.send(:write_attribute, :plan_type, SubscriptionPlanType.Medium)
          assert account.send("has_#{feature}?")
        end
        it "returns false when account is account is not plus" do
          account.subscription.send(:write_attribute, :plan_type, SubscriptionPlanType.Small)
          refute account.send("has_#{feature}?")
        end
      end
    end

    describe '#has_translations_from_cdn?' do
      cdns      = %w[disabled default edgecast cloudfront fastly freeviruses]
      good_cdns = %w[disabled default edgecast cloudfront fastly]

      cdns.each do |cdn_provider|
        describe "when the accounts cdn is set to #{cdn_provider}" do
          before { account.stubs(cdn_provider: cdn_provider) }

          describe "with translations_from_cdn arturo enabled" do
            before { Arturo.enable_feature!(:translations_from_cdn) }

            unless good_cdns.include?(cdn_provider)
              it 'returns false for an unsupported cdn' do
                refute account.has_translations_from_cdn?, "account must not support translations from #{cdn_provider}"
              end
            end

            good_cdns.each do |cdn|
              describe "with translations_from_#{cdn}_cdn enabled" do
                before { Arturo.enable_feature!(:"translations_from_#{cdn}_cdn") }

                if cdn == cdn_provider
                  it 'returns true when the accounts cdn matches the arturo' do
                    assert account.has_translations_from_cdn?, "account must support translations from #{cdn_provider}"
                  end
                else
                  it 'returns false when the accounts cdn does not match the arturo' do
                    refute account.has_translations_from_cdn?, "account must not support translations from #{cdn_provider}"
                  end
                end
              end

              describe "with translations_from_#{cdn}_cdn disabled" do
                before { Arturo.disable_feature!(:"translations_from_#{cdn}_cdn") }

                it 'returns false' do
                  refute account.has_translations_from_cdn?, "account must not support translations from #{cdn_provider}"
                end
              end
            end
          end

          describe 'with translations_from_cdn arturo disabled' do
            before { Arturo.disable_feature!(:translations_from_cdn) }

            good_cdns.each do |cdn|
              describe "with translations_from_#{cdn}_cdn enabled" do
                before { Arturo.enable_feature!(:"translations_from_#{cdn}_cdn") }

                it 'returns false' do
                  refute account.has_translations_from_cdn?, "account must not support translations from #{cdn_provider}"
                end
              end

              describe "with translations_from_#{cdn}_cdn disabled" do
                before { Arturo.disable_feature!(:"translations_from_#{cdn}_cdn") }

                it 'returns false' do
                  refute account.has_translations_from_cdn?, "account must not support translations from #{cdn_provider}"
                end
              end
            end
          end
        end
      end
    end

    describe '#has_suite_trial?' do
      before do
        account.settings.suite_trial = suite_trial
        account.settings.save!
      end

      describe 'suite_trial setting is false' do
        let(:suite_trial) { false }

        it 'returns false' do
          assert_equal false, account.has_suite_trial?
        end
      end

      describe 'suite_trial setting is true' do
        let(:suite_trial) { true }

        it 'returns true' do
          assert(account.has_suite_trial?)
        end
      end
    end

    describe 'No-Delimiter features' do
      describe '#email_no_delimiter' do
        before do
          # NOTE: can't use Arturo helpers because it will stub the entire check
          account.settings.no_mail_delimiter = no_mail_delimiter
          account.settings.save
        end

        describe "with 'email_no_delimiter' arturo enabled" do
          before { Arturo.enable_feature!(:email_no_delimiter) }

          describe "with 'no_mail_delimiter' setting enabled" do
            let(:no_mail_delimiter) { true }
            it { assert account.has_no_mail_delimiter_enabled? }
          end

          describe "with 'no_mail_delimiter' setting disabled" do
            let(:no_mail_delimiter) { false }
            it { refute account.has_no_mail_delimiter_enabled? }
          end
        end

        describe "with 'email_no_delimiter' arturo disabled" do
          before { Arturo.disable_feature!(:email_no_delimiter) }

          describe "with 'no_mail_delimiter' setting enabled" do
            let(:no_mail_delimiter) { true }
            it { refute account.has_no_mail_delimiter_enabled? }
          end

          describe "with 'no_mail_delimiter' setting disabled" do
            let(:no_mail_delimiter) { false }
            it { refute account.has_no_mail_delimiter_enabled? }
          end
        end
      end
    end

    describe 'Simplified email threading features' do
      describe '#email_simplified_threading' do
        before do
          # NOTE: can't use Arturo helpers because it will stub the entire check
          account.settings.simplified_email_threading = simplified_email_threading
          account.settings.save
        end

        describe "with 'email_simplified_threading' arturo enabled" do
          before { Arturo.enable_feature!(:email_simplified_threading) }

          describe "with 'simplified_email_threading' setting enabled" do
            let(:simplified_email_threading) { true }
            it { assert account.has_email_simplified_threading? }
          end

          describe "with 'simplified_email_threading' setting disabled" do
            let(:simplified_email_threading) { false }
            it { refute account.has_email_simplified_threading? }
          end
        end

        describe "with 'email_simplified_threading' arturo disabled" do
          before { Arturo.disable_feature!(:email_simplified_threading) }

          describe "with 'simplified_email_threading' setting enabled" do
            let(:simplified_email_threading) { true }
            it { refute account.has_email_simplified_threading? }
          end

          describe "with 'simplified_email_threading' setting disabled" do
            let(:simplified_email_threading) { false }
            it { refute account.has_email_simplified_threading? }
          end
        end
      end
    end

    describe "Custom Statuses" do
      describe_with_arturo_disabled :custom_statuses do
        describe "and account setting disabled" do
          before { account.settings.custom_statuses_enabled = false }

          it "should return false for custom_statuses" do
            refute account.has_custom_statuses?
          end

          it "should return false for custom_statuses_enabled" do
            refute account.has_custom_statuses_enabled?
          end
        end

        describe "and account setting enabled" do
          before { account.settings.custom_statuses_enabled = true }

          it "should return false for custom_statuses" do
            refute account.has_custom_statuses?
          end

          it "should return false for custom_statuses_enabled" do
            refute account.has_custom_statuses_enabled?
          end
        end
      end

      describe_with_arturo_enabled :custom_statuses do
        describe "and account setting disabled" do
          before { account.settings.custom_statuses_enabled = false }

          it "should return true for custom_statuses" do
            assert account.has_custom_statuses?
          end

          it "should return false for custom_statuses_enabled" do
            refute account.has_custom_statuses_enabled?
          end
        end

        describe "and account setting enabled" do
          before { account.settings.custom_statuses_enabled = true }

          it "should return true for custom_statuses" do
            assert account.has_custom_statuses?
          end

          it "should return true for custom_statuses_enabled" do
            assert account.has_custom_statuses_enabled?
          end
        end
      end
    end

    describe "Native Conditional Fields" do
      describe_with_arturo_disabled :ticket_forms do
        it "should return false for native_conditional_fields_enabled?" do
          refute account.has_native_conditional_fields_enabled?
        end

        it "should return false for native_conditional_fields_migration_needed?" do
          refute account.has_native_conditional_fields_migration_needed?
        end

        it "should return false for conditional_fields_app_enabled?" do
          account.stubs(:cfa_enabled?).returns(true)
          refute account.has_conditional_fields_app_enabled?
        end

        describe "and account has NOT abandoned EAP" do
          before { account.settings.native_conditional_fields_abandoned = false }

          it "should return false for native_conditional_fields_abandoned?" do
            refute account.has_native_conditional_fields_abandoned?
          end
        end

        describe "and account has abandoned EAP" do
          before { account.settings.native_conditional_fields_abandoned = true }

          it "should return true for native_conditional_fields_abandoned?" do
            assert account.has_native_conditional_fields_abandoned?
          end
        end
      end

      describe_with_arturo_enabled :ticket_forms do
        describe "and account needs CFA migration" do
          before { account.stubs(:cfa_migration_needed?).returns(true) }

          it "should return true for native_conditional_fields_migration_needed?" do
            assert account.has_native_conditional_fields_migration_needed?
          end

          it "should return false for native_conditional_fields_enabled?" do
            refute account.has_native_conditional_fields_enabled?
          end
        end

        describe "and account has completed migration" do
          before { account.settings.native_conditional_fields_migrated = true }

          it "should return false for native_conditional_fields_migration_needed?" do
            refute account.has_native_conditional_fields_migration_needed?
          end

          it "should return true for native_conditional_fields_enabled?" do
            assert account.has_native_conditional_fields_enabled?
          end
        end

        describe "and account has NOT completed migration" do
          before { account.settings.native_conditional_fields_migrated = false }

          describe "and account doesn't have CFA installed" do
            before do
              account.stubs(:cfa_installed?).returns(false)
              account.stubs(:cfa_enabled?).returns(false)
            end

            it "should return false for native_conditional_fields_migration_needed?" do
              refute account.has_native_conditional_fields_migration_needed?
            end

            it "should return true for native_conditional_fields_enabled?" do
              assert account.has_native_conditional_fields_enabled?
            end

            it "should return false for conditional_fields_app_installed?" do
              refute account.has_conditional_fields_app_installed?
            end

            it "should return false for conditional_fields_app_enabled?" do
              refute account.has_conditional_fields_app_enabled?
            end
          end

          describe "and account has CFA installed and disabled" do
            before do
              account.stubs(:cfa_installed?).returns(true)
              account.stubs(:cfa_enabled?).returns(false)
            end

            it "should return true for native_conditional_fields_migration_needed?" do
              assert account.has_native_conditional_fields_migration_needed?
            end

            it "should return false for native_conditional_fields_enabled?" do
              refute account.has_native_conditional_fields_enabled?
            end

            it "should return true for conditional_fields_app_installed?" do
              assert account.has_conditional_fields_app_installed?
            end

            it "should return false for conditional_fields_app_enabled?" do
              refute account.has_conditional_fields_app_enabled?
            end
          end

          describe "and account has CFA installed and enabled" do
            before do
              account.stubs(:cfa_installed?).returns(true)
              account.stubs(:cfa_enabled?).returns(true)
            end

            it "should return true for native_conditional_fields_migration_needed?" do
              assert account.has_native_conditional_fields_migration_needed?
            end

            it "should return false for native_conditional_fields_enabled?" do
              refute account.has_native_conditional_fields_enabled?
            end

            it "should return true for conditional_fields_app_installed?" do
              assert account.has_conditional_fields_app_installed?
            end

            it "should return true for conditional_fields_app_enabled?" do
              assert account.has_conditional_fields_app_enabled?
            end
          end
        end
      end
    end

    describe '#has_ccs_requester_excluded_public_comments_enabled?' do
      describe 'when ccs_requester_excluded_public_comments setting is true' do
        before do
          account.settings.ccs_requester_excluded_public_comments = true
          account.settings.save!
        end

        describe_with_arturo_enabled :email_end_user_comment_privacy_settings do
          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it 'returns true' do
              assert account.has_ccs_requester_excluded_public_comments_enabled?
            end
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it 'returns false' do
              refute account.has_ccs_requester_excluded_public_comments_enabled?
            end
          end
        end

        describe_with_arturo_disabled :email_end_user_comment_privacy_settings do
          it 'returns false' do
            refute account.has_ccs_requester_excluded_public_comments_enabled?
          end
        end
      end

      describe 'when ccs_requester_excluded_public_comments setting is false' do
        before do
          account.settings.ccs_requester_excluded_public_comments = false
          account.settings.save!
        end

        describe_with_and_without_arturo_enabled :email_end_user_comment_privacy_settings do
          describe_with_and_without_arturo_setting_enabled :comment_email_ccs_allowed do
            it 'returns false' do
              refute account.has_ccs_requester_excluded_public_comments_enabled?
            end
          end
        end
      end
    end

    describe '#has_third_party_end_user_public_comments_enabled?' do
      describe 'when third_party_end_user_public_comments setting is true' do
        before do
          account.settings.third_party_end_user_public_comments = true
          account.settings.save!
        end

        it 'returns true' do
          assert account.has_third_party_end_user_public_comments_enabled?
        end

        describe_with_arturo_disabled :third_party_end_user_public_comments_setting do
          it 'returns false' do
            refute account.has_third_party_end_user_public_comments_enabled?
          end
        end
      end

      describe 'when third_party_end_user_public_comments setting is false' do
        before do
          account.settings.third_party_end_user_public_comments = false
          account.settings.save!
        end

        describe_with_arturo_disabled :third_party_end_user_public_comments_setting do
          it 'returns false' do
            refute account.has_third_party_end_user_public_comments_enabled?
          end
        end
      end
    end
  end
end
