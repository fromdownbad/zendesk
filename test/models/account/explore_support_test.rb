require_relative '../../support/test_helper'

SingleCov.covered!

describe 'Account::ExploreSupport' do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:client) { stub }

  before do
    account.stubs(:account_service_client).returns(client)
    client.stubs(:products).returns([support_product, explore_product])
  end

  let(:support_product) do
    Zendesk::Accounts::Product.new(
      FactoryBot.build(:support_trial_product, plan_settings: { 'insights' => insights })
    )
  end
  let(:insights) { nil }

  let(:explore_product) do
    Zendesk::Accounts::Product.new(
      FactoryBot.build(:explore_free_product, active: explore_active)
    )
  end
  let(:explore_active) { false }

  describe '#account' do
    it 'returns self' do
      assert_equal account.account, account
    end
  end

  describe '#eligible_for_explore_lite?' do
    def setup_talk(talk_plan_type)
      voice_sub = talk_plan_type.nil? ? nil : stub(plan_type: talk_plan_type.plan_type)
      account.stubs(:voice_subscription).returns(voice_sub)
    end

    def setup_guide(guide_plan_type)
      guide_sub = guide_plan_type.nil? ? nil : stub(plan_type: guide_plan_type.plan_type)
      account.stubs(:guide_subscription).returns(guide_sub)
    end

    let(:kc_app) do
      {
        'app' => {
          'name' => 'Knowledge Capture'
        }
      }
    end

    def setup_knowledge_capture(has_kc_app)
      installations = []
      installations.push(kc_app) if has_kc_app

      account.stubs(:app_market_client).returns(
        stub(
          installations_detailed: installations
        )
      )
    end

    def setup_answer_bot(has_answer_bot)
      answer_bot_sub = has_answer_bot ? stub : nil
      account.stubs(:answer_bot_subscription).returns(answer_bot_sub)
    end

    def self.it_returns_false
      it 'returns false' do
        refute account.eligible_for_explore_lite?
      end
    end

    def self.it_returns_true
      it 'returns true' do
        assert account.eligible_for_explore_lite?
      end
    end

    describe 'when insights setting is good_data' do
      let(:insights) { 'good_data' }

      it_returns_false
    end

    describe 'and insights setting is explore' do
      let(:insights) { 'explore' }

      describe 'and Support plan is Professional' do
        before do
          setup_support(ZBC::Zendesk::PlanType::Professional)
        end

        it_returns_true
      end

      describe 'and Support plan is Enterprise' do
        before do
          setup_support(ZBC::Zendesk::PlanType::Enterprise)
        end

        it_returns_true
      end

      describe 'and Support plan is NOT Professional OR Enterprise' do
        before do
          setup_support(ZBC::Zendesk::PlanType::Team)
        end

        describe 'and Talk plan is Professional' do
          before do
            setup_talk(ZBC::Voice::PlanType::Professional)
          end

          it_returns_true
        end

        describe 'and Talk plan is Enterprise' do
          before do
            setup_talk(ZBC::Voice::PlanType::Enterprise)
          end

          it_returns_true
        end

        describe 'and Talk plan is NOT Professional OR Enterprise' do
          before do
            setup_talk(ZBC::Voice::PlanType::Team)
          end

          describe 'and Guide plan is Professional' do
            before do
              setup_guide(ZBC::Guide::PlanType::Professional)
            end

            describe 'and Knowledge Capture app is installed' do
              before do
                setup_knowledge_capture(true)
              end

              it_returns_true
            end

            describe 'and Knowledge Capture app is NOT installed' do
              before do
                setup_knowledge_capture(false)
              end

              describe 'and account has Answer Bot' do
                before do
                  setup_answer_bot(true)
                end

                it_returns_true
              end

              describe 'and account does not have Answer Bot' do
                before do
                  setup_answer_bot(false)
                end

                it_returns_false
              end
            end
          end

          describe 'and Guide plan is not Professional' do
            before do
              setup_guide(ZBC::Guide::PlanType::Lite)
            end

            it_returns_false
          end

          describe 'and account does not have a Guide plan' do
            before do
              setup_guide(nil)
            end

            it_returns_false
          end
        end

        describe 'and account does not have a Talk plan' do
          before do
            setup_talk(nil)
          end

          describe 'and Guide plan is Professional' do
            before do
              setup_guide(ZBC::Guide::PlanType::Professional)
            end

            describe 'and Knowledge Capture app is installed' do
              before do
                setup_knowledge_capture(true)
              end

              it_returns_true
            end

            describe 'and Knowledge Capture app is NOT installed' do
              before do
                setup_knowledge_capture(false)
              end

              describe 'and account has Answer Bot' do
                before do
                  setup_answer_bot(true)
                end

                it_returns_true
              end

              describe 'and account does not have Answer Bot' do
                before do
                  setup_answer_bot(false)
                end

                it_returns_false
              end
            end
          end

          describe 'and Guide plan is not Professional' do
            before do
              setup_guide(ZBC::Guide::PlanType::Lite)
            end

            it_returns_false
          end

          describe 'and account does not have a Guide plan' do
            before do
              setup_guide(nil)
            end

            it_returns_false
          end
        end
      end
    end

    describe 'and insights setting is good_data_and_explore' do
      let(:insights) { 'good_data_and_explore' }

      describe 'and Support plan is Professional' do
        before do
          setup_support(ZBC::Zendesk::PlanType::Professional)
        end

        it_returns_true
      end

      describe 'and Support plan is Enterprise' do
        before do
          setup_support(ZBC::Zendesk::PlanType::Enterprise)
        end

        it_returns_true
      end

      describe 'and Support plan is NOT Professional OR Enterprise' do
        before do
          setup_support(ZBC::Zendesk::PlanType::Team)
        end

        describe 'and Talk plan is Professional' do
          before do
            setup_talk(ZBC::Voice::PlanType::Professional)
          end

          it_returns_true
        end

        describe 'and Talk plan is Enterprise' do
          before do
            setup_talk(ZBC::Voice::PlanType::Enterprise)
          end

          it_returns_true
        end

        describe 'and Talk plan is NOT Professional OR Enterprise' do
          before do
            setup_talk(ZBC::Voice::PlanType::Team)
          end

          describe 'and Guide plan is Professional' do
            before do
              setup_guide(ZBC::Guide::PlanType::Professional)
            end

            describe 'and Knowledge Capture app is installed' do
              before do
                setup_knowledge_capture(true)
              end

              it_returns_true
            end

            describe 'and Knowledge Capture app is NOT installed' do
              before do
                setup_knowledge_capture(false)
              end

              describe 'and account has Answer Bot' do
                before do
                  setup_answer_bot(true)
                end

                it_returns_true
              end

              describe 'and account does not have Answer Bot' do
                before do
                  setup_answer_bot(false)
                end

                it_returns_false
              end
            end
          end

          describe 'and Guide plan is not Professional' do
            before do
              setup_guide(ZBC::Guide::PlanType::Lite)
            end

            it_returns_false
          end

          describe 'and account does not have a Guide plan' do
            before do
              setup_guide(nil)
            end

            it_returns_false
          end
        end

        describe 'and account does not have a Talk plan' do
          before do
            setup_talk(nil)
          end

          describe 'and Guide plan is Professional' do
            before do
              setup_guide(ZBC::Guide::PlanType::Professional)
            end

            describe 'and Knowledge Capture app is installed' do
              before do
                setup_knowledge_capture(true)
              end

              it_returns_true
            end

            describe 'and Knowledge Capture app is NOT installed' do
              before do
                setup_knowledge_capture(false)
              end

              describe 'and account has Answer Bot' do
                before do
                  setup_answer_bot(true)
                end

                it_returns_true
              end

              describe 'and account does not have Answer Bot' do
                before do
                  setup_answer_bot(false)
                end

                it_returns_false
              end
            end
          end

          describe 'and Guide plan is not Professional' do
            before do
              setup_guide(ZBC::Guide::PlanType::Lite)
            end

            it_returns_false
          end

          describe 'and account does not have a Guide plan' do
            before do
              setup_guide(nil)
            end

            it_returns_false
          end
        end
      end
    end

    describe 'and insights setting is nil' do
      it_returns_false
    end
  end

  describe '#enterprise_explore?' do
    describe 'plan setting of explore' do
      let(:insights) { 'explore' }

      describe 'enterprise support customer' do
        before { setup_support(ZBC::Zendesk::PlanType::Enterprise) }
        it 'returns true' do
          assert account.enterprise_explore?
        end
      end

      [ZBC::Zendesk::PlanType::Essential,
       ZBC::Zendesk::PlanType::Team,
       ZBC::Zendesk::PlanType::Professional].each do |plan_type|
        describe "support plan type #{plan_type}" do
          before { setup_support(plan_type) }
          it 'returns false' do
            refute account.enterprise_explore?
          end
        end
      end
    end
    ['good_data_and_explore', 'good_data'].each do |insights_val|
      describe "plan setting of #{insights_val}" do
        let(:insights) { insights_val }

        [ZBC::Zendesk::PlanType::Essential,
         ZBC::Zendesk::PlanType::Team,
         ZBC::Zendesk::PlanType::Professional,
         ZBC::Zendesk::PlanType::Enterprise].each do |plan_type|
          describe "support plan type #{plan_type}" do
            before { setup_support(plan_type) }
            it 'returns false' do
              refute account.enterprise_explore?
            end
          end
        end
      end
    end
  end

  describe '#enterprise_good_data_and_explore?' do
    describe 'plan setting of good_data_and_explore' do
      let(:insights) { 'good_data_and_explore' }

      describe 'enterprise support customer' do
        before { setup_support(ZBC::Zendesk::PlanType::Enterprise) }
        it 'returns true' do
          assert account.enterprise_good_data_and_explore?
        end
      end

      [ZBC::Zendesk::PlanType::Essential,
       ZBC::Zendesk::PlanType::Team,
       ZBC::Zendesk::PlanType::Professional].each do |plan_type|
        describe "support plan type #{plan_type}" do
          before { setup_support(plan_type) }
          it 'returns false' do
            refute account.enterprise_good_data_and_explore?
          end
        end
      end
    end

    ['explore', 'good_data'].each do |insights_val|
      describe "plan setting of #{insights_val}" do
        let(:insights) { insights_val }
        [ZBC::Zendesk::PlanType::Essential,
         ZBC::Zendesk::PlanType::Team,
         ZBC::Zendesk::PlanType::Professional,
         ZBC::Zendesk::PlanType::Enterprise].each do |plan_type|
          describe "support plan type #{plan_type}" do
            before { setup_support(plan_type) }
            it 'returns false' do
              refute account.enterprise_good_data_and_explore?
            end
          end
        end
      end
    end
  end

  describe '#professional_support_plus_explore?' do
    describe 'plan setting of explore' do
      describe 'active Explore product' do
        let(:explore_active) { true }

        ['explore', 'good_data_and_explore'].each do |insights_val|
          describe "professional support customer with plan_setting #{insights_val}" do
            let(:insights) { insights_val }
            before do
              setup_support(ZBC::Zendesk::PlanType::Professional)
            end
            it 'returns true' do
              assert account.professional_support_plus_explore?
            end
          end

          [ZBC::Zendesk::PlanType::Essential,
           ZBC::Zendesk::PlanType::Team,
           ZBC::Zendesk::PlanType::Enterprise].each do |plan_type|
            describe "support plan type #{plan_type}" do
              let(:insights) { 'good_data' }
              before do
                setup_support(plan_type)
              end
              it 'returns false' do
                refute account.professional_support_plus_explore?
              end
            end
          end
        end

        ['good_data'].each do |insights_val|
          describe "plan setting of #{insights_val}" do
            let(:insights) { insights_val }

            [ZBC::Zendesk::PlanType::Essential,
             ZBC::Zendesk::PlanType::Team,
             ZBC::Zendesk::PlanType::Professional,
             ZBC::Zendesk::PlanType::Enterprise].each do |plan_type|
              describe "support plan type #{plan_type}" do
                before { setup_support(plan_type) }
                it 'returns false' do
                  refute account.professional_support_plus_explore?
                end
              end
            end
          end
        end
      end

      describe 'without active Explore product' do
        let(:explore_active) { false }

        describe 'professional support customer' do
          before do
            setup_support(ZBC::Zendesk::PlanType::Professional)
          end
          it 'returns false' do
            refute account.professional_support_plus_explore?
          end
        end

        [ZBC::Zendesk::PlanType::Essential,
         ZBC::Zendesk::PlanType::Team,
         ZBC::Zendesk::PlanType::Enterprise].each do |plan_type|
          describe "support plan type #{plan_type}" do
            before { setup_support(plan_type) }
            it 'returns false' do
              refute account.professional_support_plus_explore?
            end
          end
        end

        ['good_data_and_explore', 'good_data'].each do |insights_val|
          describe "plan setting of #{insights_val}" do
            let(:insights) { insights_val }

            [ZBC::Zendesk::PlanType::Essential,
             ZBC::Zendesk::PlanType::Team,
             ZBC::Zendesk::PlanType::Professional,
             ZBC::Zendesk::PlanType::Enterprise].each do |plan_type|
              describe "support plan type #{plan_type}" do
                before { setup_support(plan_type) }
                it 'returns false' do
                  refute account.professional_support_plus_explore?
                end
              end
            end
          end
        end
      end
    end
  end

  describe '#enterprise_support_plus_explore?' do
    describe 'plan setting of explore' do
      describe 'active Explore product' do
        let(:explore_active) { true }

        ['explore', 'good_data_and_explore'].each do |insights_val|
          describe "enterprise support customer with plan_setting #{insights_val}" do
            let(:insights) { insights_val }
            before do
              setup_support(ZBC::Zendesk::PlanType::Enterprise)
            end
            it 'returns true' do
              assert account.enterprise_support_plus_explore?
            end
          end

          [ZBC::Zendesk::PlanType::Essential,
           ZBC::Zendesk::PlanType::Team,
           ZBC::Zendesk::PlanType::Professional].each do |plan_type|
            describe "support plan type #{plan_type}" do
              let(:insights) { 'good_data' }
              before do
                setup_support(plan_type)
              end
              it 'returns false' do
                refute account.enterprise_support_plus_explore?
              end
            end
          end
        end

        ['good_data'].each do |insights_val|
          describe "plan setting of #{insights_val}" do
            let(:insights) { insights_val }

            [ZBC::Zendesk::PlanType::Essential,
             ZBC::Zendesk::PlanType::Team,
             ZBC::Zendesk::PlanType::Professional,
             ZBC::Zendesk::PlanType::Enterprise].each do |plan_type|
              describe "support plan type #{plan_type}" do
                before { setup_support(plan_type) }
                it 'returns false' do
                  refute account.enterprise_support_plus_explore?
                end
              end
            end
          end
        end
      end

      describe 'without active Explore product' do
        let(:explore_active) { false }

        describe 'enterprise support customer' do
          before do
            setup_support(ZBC::Zendesk::PlanType::Enterprise)
          end
          it 'returns false' do
            refute account.enterprise_support_plus_explore?
          end
        end

        [ZBC::Zendesk::PlanType::Essential,
         ZBC::Zendesk::PlanType::Team,
         ZBC::Zendesk::PlanType::Professional].each do |plan_type|
          describe "support plan type #{plan_type}" do
            before { setup_support(plan_type) }
            it 'returns false' do
              refute account.enterprise_support_plus_explore?
            end
          end
        end

        ['good_data_and_explore', 'good_data'].each do |insights_val|
          describe "plan setting of #{insights_val}" do
            let(:insights) { insights_val }
            [ZBC::Zendesk::PlanType::Essential,
             ZBC::Zendesk::PlanType::Team,
             ZBC::Zendesk::PlanType::Professional,
             ZBC::Zendesk::PlanType::Enterprise].each do |plan_type|
              describe "support plan type #{plan_type}" do
                before { setup_support(plan_type) }
                it 'returns false' do
                  refute account.enterprise_support_plus_explore?
                end
              end
            end
          end
        end
      end
    end
  end

  describe '#explore_plan_setting?' do
    describe 'account with explore plan setting' do
      let(:insights) { 'explore' }
      it 'returns true' do
        assert account.explore_plan_setting?
      end
    end

    describe 'account without explore plan setting' do
      ['good_data', 'good_data_and_explore'].each do |insights_val|
        let(:insights) { insights_val }
        it 'returns false' do
          refute account.explore_plan_setting?
        end
      end
    end
  end

  describe '#good_data_and_explore_plan_setting?' do
    describe 'account with good_data_and_explore plan setting' do
      let(:insights) { 'good_data_and_explore' }
      it 'returns true' do
        assert account.good_data_and_explore_plan_setting?
      end
    end

    describe 'account without good_data_and_explore plan setting' do
      ['good_data', 'explore'].each do |insights_val|
        let(:insights) { insights_val }
        it 'returns false' do
          refute account.good_data_and_explore_plan_setting?
        end
      end
    end
  end

  def setup_support(support_plan_type)
    account.subscription.stubs(:plan_type).returns(support_plan_type.plan_type)
  end
end
