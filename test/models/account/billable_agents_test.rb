require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'Account::BillableAgent' do
  fixtures :accounts, :subscriptions, :users, :user_identities

  describe "An a account" do
    before { @account = accounts(:minimum) }

    describe "when has_permission_sets? returns true" do
      before do
        @account.stubs(:has_permission_sets?).returns(true)

        @light_permission_set = PermissionSet.create_light_agent!(@account)
        @chat_permission_set = PermissionSet.create_chat_agent!(@account)
      end

      describe "with light agents enabled" do
        before do
          @user = users(:minimum_agent)
          @user.update_attribute(:permission_set, @light_permission_set)
        end

        it "has 1 light agent" do
          assert_equal 1, @account.light_agents.count(:all)
        end

        describe "#billable_agents" do
          it "includes regular agents in the billable agent count" do
            (@account.agents - @account.light_agents).each do |agent|
              assert @account.billable_agents.include?(agent)
            end
          end

          it "includes admins in the billable agent count" do
            @account.admins.each do |agent|
              assert @account.billable_agents.include?(agent)
            end
          end

          it "only reacts include admins and agents" do
            assert_equal @account.agents.count(:all) - @account.light_agents.count(:all),
              @account.billable_agents.count
          end

          it "does not include light agents in the billable agent count" do
            @account.light_agents.each do |agent|
              refute @account.billable_agents.include?(agent)
            end
          end
        end

        describe "#unbillable_agents" do
          it "does does does not include regular agents in the unbillable agent count" do
            assert_equal 1, @account.unbillable_agents.count
          end
        end
      end

      describe "with chat agents enabled" do
        before do
          @user = users(:minimum_agent)
          @user.update_attribute(:permission_set, @chat_permission_set)
        end

        it "has 1 chat agent" do
          assert_equal 1, @account.chat_agents.count(:all)
        end

        describe "#billable_agents" do
          it "includes regular agents in the billable agent count" do
            (@account.agents - @account.chat_agents).each do |agent|
              assert @account.billable_agents.include?(agent)
            end
          end

          it "includes admins in the billable agent count" do
            @account.admins.each do |agent|
              assert @account.billable_agents.include?(agent)
            end
          end

          it "only reacts include admins and agents" do
            assert_equal @account.agents.count(:all) - @account.chat_agents.count(:all),
              @account.billable_agents.count
          end

          it "does does not include chat agents in the billable agent count" do
            @account.chat_agents.each do |agent|
              refute @account.billable_agents.include?(agent)
            end
          end
        end

        describe "#unbillable_agents" do
          it "does does does not include regular agents in the unbillable agent count" do
            assert_equal 1, @account.unbillable_agents.count
          end
        end
      end

      describe "with light and chat agents enabled" do
        before do
          @light = users(:minimum_agent)
          @light.update_attribute(:permission_set, @light_permission_set)

          @chat = users(:minimum_end_user)
          @chat.update_attribute(:permission_set, @chat_permission_set)
          @chat.update_attribute(:roles, Role::AGENT.id)
          @unbillable_agents = [@light, @chat]
        end

        it "has 1 light agent" do
          assert_equal 1, @account.light_agents.count(:all)
        end

        it "has 1 chat agent" do
          assert_equal 1, @account.chat_agents.count(:all)
        end

        describe "#billable_agents" do
          it "includes regular agents in the billable agent count" do
            (@account.agents - @unbillable_agents).each do |agent|
              assert @account.billable_agents.include?(agent)
            end
          end

          it "includes admins in the billable agent count" do
            @account.admins.each do |agent|
              assert @account.billable_agents.include?(agent)
            end
          end

          it "only reacts include admins and agents" do
            assert_equal @account.agents.count(:all) - @unbillable_agents.count,
              @account.billable_agents.count
          end

          it "does does not include chat agents in the billable agent count" do
            @unbillable_agents.each do |agent|
              refute @account.billable_agents.include?(agent)
            end
          end
        end

        describe "#unbillable_agents" do
          it "does does does not include regular agents in the unbillable agent count" do
            assert_equal 2, @account.unbillable_agents.count
          end
        end
      end

      describe '#agents_to_be_downgraded' do
        let(:owner) { @account.billable_agents.select(&:is_account_owner?).first }
        let(:newest_agent) { @account.billable_agents.sort_by(&:created_at).last }

        it 'will not downgrade account owner' do
          refute_includes @account.agents_to_be_downgraded(0).map(&:id), owner.id
        end

        describe 'when decreasing agent count by one' do
          let(:new_agent_count) { @account.billable_agents.count - 1 }

          let(:downgraded) { @account.agents_to_be_downgraded(new_agent_count).map(&:id) }

          it 'only downgrades most recently created agent' do
            assert_equal [newest_agent.id], downgraded
          end
        end
      end
    end

    describe "when has_permission_sets? returns false" do
      before do
        @account.stubs(:has_permission_sets?).returns(false)

        @permission_set = PermissionSet.create_light_agent!(@account)
        @user = users(:minimum_agent)
        @user.update_attributes! permission_set: @permission_set

        assert_equal 1, @account.light_agents.count(:all)
      end

      describe "#billable_agents" do
        it "includes only non-light agents" do
          assert_equal @account.agents.count(:all) - @account.light_agents.count(:all), @account.billable_agents.count
        end
      end

      describe "#unbillable_agents" do
        it "includes all light_agents" do
          assert_equal @account.light_agents.count(:all), @account.unbillable_agents.count
        end
      end
    end
  end
end
