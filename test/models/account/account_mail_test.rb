require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 9

describe Account::AccountMail do
  fixtures :accounts, :account_texts, :subscriptions, :recipient_addresses

  let(:account) { accounts(:minimum) }
  let(:unmapped_address) { "foo@#{account.default_host}" }
  let(:mapped_address) { "foo@mapped.com" }
  let(:new_unmapped_address) { "foo@newsub.zendesk-test.com" }

  describe "#update_recipient_addresses_on_subdomain_change" do
    before do
      account.recipient_addresses.delete_all
      account.recipient_addresses.create!(email: unmapped_address)
      account.recipient_addresses.create!(email: mapped_address, default: true) { |a| a.forwarding_verified_at = Time.now }
    end

    it "does not change recipient addresses on normal save" do
      account.save!
      assert_equal [mapped_address, unmapped_address].sort, account.recipient_addresses.map(&:email).sort
    end

    it "changes recipient addresses on subdomain changes" do
      account.subdomain = "newsub"
      account.save!
      assert_equal [new_unmapped_address, mapped_address].sort, account.recipient_addresses.map(&:email).sort
    end

    it "skips changes if is sandbox" do
      account.subdomain = "newsub"
      account.stubs(:is_sandbox?).returns(true)
      account.save!
      assert_equal [mapped_address, unmapped_address].sort, account.recipient_addresses.map(&:email).sort
    end

    it "does not change duplicate recipient addresses" do
      account.recipient_addresses.create!(email: 'xxx@yyy.com').update_attribute(:email, new_unmapped_address)
      assert_equal [new_unmapped_address, unmapped_address, mapped_address].sort, account.recipient_addresses.map(&:email).sort
    end
  end

  describe "#default_hosts" do
    it "returns the default hosts of the routes with valid brands" do
      brand1 = FactoryBot.create(:brand, account_id: account.id)
      brand2 = FactoryBot.create(:brand, account_id: account.id)

      brand2.soft_delete!

      account.instance_variable_set(:@default_hosts, nil)
      assert_equal [account.route.default_host, brand1.route.default_host], account.reload.default_hosts
    end
  end

  describe "#email_route_brand_map" do
    let(:account) { accounts(:minimum) }

    it "shows simple account" do
      assert_equal({"minimum.zendesk-test.com" => account.default_brand_id}, account.domain_brand_map)
    end

    it "shows routes with brand" do
      route = account.routes.create!(subdomain: "foobrand")
      brand = account.brands.create! { |b| b.name = "Brando"; b.route = route }
      assert_equal({"minimum.zendesk-test.com" => account.default_brand_id, "foobrand.zendesk-test.com" => brand.id}, account.domain_brand_map)
    end

    it "shows routes without brand" do
      account.routes.create!(subdomain: "nobrand")
      assert_equal({"minimum.zendesk-test.com" => account.default_brand_id, "nobrand.zendesk-test.com" => account.default_brand_id}, account.domain_brand_map)
    end
  end

  describe "#strip_mail_delimiter" do
    before do
      account_texts(:minimum_mail_delimiter).update_column(:value, ' strip me I really hate being padded ')
      Rails.cache.clear
    end

    it "does not update when not changed to save queries" do
      assert_sql_queries(0, /account_texts/) { account.save! }
      account.mail_delimiter.must_equal ' strip me I really hate being padded '
    end

    it "strips when changed" do
      assert_sql_queries(3, /account_texts/) do # load ... unqiue check ... save
        account.mail_delimiter = ' strip me I really hate being padded 2 '
        account.save!
      end
      account.mail_delimiter.must_equal 'strip me I really hate being padded 2'
    end
  end
end
