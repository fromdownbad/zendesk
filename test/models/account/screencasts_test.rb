require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Account::Screencasts' do
  fixtures :accounts, :subscriptions

  def screencasts_for_tickets_enabled_on(plan, account)
    account.settings.stubs(:screencasts_for_tickets?).returns(true)
    account.subscription.plan_type = plan
    account.has_screencasts_for_tickets_enabled?
  end

  describe "Given an account, an user " do
    before do
      @account = accounts(:minimum)
    end

    describe "and an available domain" do
      before do
        @domain = "minimum"

        params_from_api = {
          domain: "minimum",
          host: "minimum.sfssdev.com",
          upgraded_account: false,
          upgrade_url: "https://screenr-upgrade.com/test",
          recorders: {
            tickets_recorder_id: "TICKETS-ID",
            forums_recorder_id: "FORUMS-ID"
          }
        }

        @client = stub("client", create_tenant: params_from_api)
        Screenr::BusinessPartnerClient.stubs(:new).returns(@client)
      end

      it "does not raises" do
        @account.screenr_integration.provide_tenant(@domain)
      end

      it "returns the associated Screenr::Tenant" do
        result = @account.screenr_integration.provide_tenant(@domain)
        assert_equal @account.reload.screenr_tenant, result
      end

      it "creates a new Screenr::Tenant" do
        assert_difference 'Screenr::Tenant.count(:all)' do
          @account.screenr_integration.provide_tenant(@domain)
        end
      end

      it "associates account to Screenr::Tenant" do
        @account.screenr_integration.provide_tenant(@domain)
        assert_not_nil @account.screenr_tenant
      end

      it "saves domain coming from API" do
        @account.screenr_integration.provide_tenant(@domain)
        assert_equal "minimum", @account.screenr_tenant.domain
      end

      it "marks the Account as provsioned" do
        @account.screenr_integration.provide_tenant(@domain)
        assert @account.screenr_integration.has_completed_provisioning?
      end

      it "returns tickets_recorder_id" do
        @account.screenr_integration.provide_tenant(@domain)
        assert_equal "TICKETS-ID", @account.screenr_integration.tickets_recorder_id
      end

      it "returns forums_recorder_id" do
        @account.screenr_integration.provide_tenant(@domain)
        assert_equal "FORUMS-ID", @account.screenr_integration.forums_recorder_id
      end

      it "returns screenr host name" do
        @account.screenr_integration.provide_tenant(@domain)
        assert_equal "minimum.sfssdev.com", @account.screenr_integration.host
      end
    end

    describe "and a not available domain" do
      before do
        @domain = "minimum"
        stub_request(:post, "https://api.example.com/tenants").
          with(basic_auth: ['zentest', 'thisisatestpassword']).
          to_return(
            body: '{"ErrorCode":409,
              "Message":"Domain is already registered.",
              "AvailableDomains":["minimum1","minimum2","minimum3","minimum4","minimum5"]}'
          )
      end

      it "raises" do
        assert_raises Screenr::BusinessPartnerClient::DomainAlreadyInUse do
          @account.screenr_integration.provide_tenant(@domain)
        end
      end
    end
  end
end
