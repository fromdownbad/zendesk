require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'Capability' do
  fixtures :accounts, :subscriptions, :subscription_features, :remote_authentications, :role_settings

  let(:capability) { Account::Capability }

  describe Account::Capability do
    before do
      @account = Account.new
    end

    it "uses Arturo by default" do
      capability = capability().new(:agent_collision)
      arturo     = capability.arturo
      assert arturo
      assert_equal :agent_collision, arturo.name
    end

    it "is available when all availabilities are available" do
      capability = capability().new(:agent_collision)

      capability.stubs(:availabilities).returns([stub(available?: true)])
      assert(capability.available?(@account))

      capability.stubs(:availabilities).returns([stub(available?: true), stub(available?: false)])
      assert_equal false, capability.available?(@account)
    end

    it "is available when nothing is configured to support :arturo => Rails.env.production?" do
      capability = capability().new(:agent_collision)
      capability.stubs(:availabilities).returns([])
      assert(capability.available?(@account))
    end

    it "is enabled when all configurations are enabled" do
      capability = capability().new(:agent_collision)

      capability.stubs(:configurations).returns([stub(enabled?: true)])
      assert(capability.enabled?(@account))

      capability.stubs(:configurations).returns([stub(enabled?: true), stub(enabled?: false)])
      assert_equal false, capability.enabled?(@account)
    end

    it "configures backends from the given options" do
      capability = capability().new(:agent_collision, arturo: false, subscription: true, setting: :hello)

      assert_nil capability.arturo

      subscription = capability.subscription
      assert_equal :agent_collision, subscription.name

      setting = capability.setting
      assert_equal :hello, setting.name
    end

    describe "availability backends" do
      it "supports subscriptions" do
        subscription = capability::Backend::Subscription.new(:agent_collision)

        @account.stubs(:subscription).returns(stub(has_agent_collision?: false))
        assert_equal false, subscription.available?(@account)

        @account.subscription.stubs(has_agent_collision?: true)
        assert(subscription.available?(@account))
      end

      it "supports arturo" do
        arturo = capability::Backend::Arturo.new(:agent_collision)

        Arturo.stubs(:feature_enabled_for?).with(:agent_collision, @account).returns(false)
        assert_equal false, arturo.available?(@account)

        Arturo.stubs(:feature_enabled_for?).with(:agent_collision, @account).returns(true)
        assert(arturo.available?(@account))
      end

      it "supports custom checks by proc" do
        custom = capability::Backend::Custom.new(lambda { |_account| @available == true })

        @available = false
        assert_equal false, custom.available?(@account)

        @available = true
        assert(custom.available?(@account))
      end
    end

    describe "configuration backends" do
      before do
        @account.stubs(:settings)
      end

      it "supports settings" do
        setting = capability::Backend::Setting.new(:agent_collision)
        @account.stubs(:settings).returns(stub(agent_collision?: false))
        assert_equal false, setting.enabled?(@account)

        @account.settings.stubs(agent_collision?: true)
        assert(setting.enabled?(@account))
      end
    end

    describe "available_and_enabled?" do
      describe "with enabled configurations" do
        [true, false].each do |available|
          it "returns #{available} when the feature #{available ? 'is' : 'is not'} available" do
            capability = capability().new(:agent_collision)

            capability.stubs(:configurations).returns([stub(enabled?: true)])
            capability.stubs(:available?).returns(available)

            assert_equal available, capability.available_and_enabled?(@account)
          end
        end
      end

      describe "with disabled configurations" do
        [true, false].each do |available|
          it "returns false when the feature #{available ? 'is' : 'is not'} available" do
            capability = capability().new(:agent_collision)

            capability.stubs(:configurations).returns([stub(enabled?: false)])
            capability.stubs(:available?).returns(available)

            assert_equal false, capability.available_and_enabled?(@account)
          end
        end
      end

      describe "with no configurations" do
        [true, false].each do |available|
          it "returns #{available} when the feature #{available ? 'is' : 'is not'} available" do
            capability = capability().new(:agent_collision)

            capability.stubs(:configurations).returns([])
            capability.stubs(:available?).returns(available)

            assert_equal available, capability.available_and_enabled?(@account)
          end
        end
      end

      describe "without any configurations" do
        [true, false].each do |available|
          it "returns #{available} when the feature #{available ? 'is' : 'is not'} available" do
            capability = capability().new(:agent_collision)

            capability.stubs(:available?).returns(available)

            assert_equal available, capability.available_and_enabled?(@account)
          end
        end
      end
    end

    describe "has_zuora_managed_billing_enabled?" do
      describe "For non Zuora managed Accounts" do
        describe "For spoke accounts" do
          before do
            @account = accounts(:minimum)
            @account.subscription.stubs(credit_card: stub, invoicing?: false, is_sponsored?: false, is_spoke?: true, zuora_managed?: false)
          end

          it "returns false" do
            assert_equal false, @account.has_zuora_managed_billing_enabled?
          end
        end
      end
    end

    describe "has_user_views?" do
      before do
        @account = accounts(:minimum)
      end

      [[{ arturo: true, subscription: true, user_views_disabled: false }, true],
       [{ arturo: true, subscription: true, user_views_disabled: true }, false],
       [{ arturo: true, subscription: false, user_views_disabled: false }, false],
       [{ arturo: true, subscription: false, user_views_disabled: true }, false],
        # We don't want to rely on the Arturo bit
       [{ arturo: false, subscription: true, user_views_disabled: false }, true],
       [{ arturo: false, subscription: true, user_views_disabled: true }, false],
       [{ arturo: false, subscription: false, user_views_disabled: false }, false],
       [{ arturo: false, subscription: false, user_views_disabled: true }, false]].each do |(settings, expected_result)|
        describe "when user_views (#{settings})" do
          before do
            Arturo.set_feature!(:user_views, settings[:arturo])
            Arturo.set_feature!(:user_views_disabled, settings[:user_views_disabled])
            @account.subscription.features.destroy_all unless settings[:subscription]
          end

          it "should be #{expected_result ? 'enabled' : 'disabled'}" do
            assert_equal expected_result, @account.has_user_views?
          end
        end
      end
    end
  end
end
