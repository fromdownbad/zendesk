require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'Account::CrmIntegration' do
  fixtures :accounts, :account_property_sets

  describe 'an Account' do
    let(:account) { @account }
    before do
      @account = accounts(:minimum)
    end

    describe "#crm_integration" do
      before do
        account.stubs(:sugar_crm_integration).returns(nil)
        account.stubs(:salesforce_integration).returns(nil)
        account.stubs(:ms_dynamics_integration).returns(nil)
      end

      it "returns the sugar_crm_integration if available" do
        sugar_crm_integration = SugarCrmIntegration.new
        account.stubs(:sugar_crm_integration).returns(sugar_crm_integration)
        assert_equal sugar_crm_integration, account.crm_integration
      end

      it "returns the salesforce_integration if available" do
        salesforce_integration = SalesforceIntegration.new
        account.stubs(:salesforce_integration).returns(salesforce_integration)
        assert_equal salesforce_integration, account.crm_integration
      end

      it "returns the ms_dynamics_integration if available" do
        ms_dynamics_integration = MsDynamicsIntegration.new
        account.stubs(:ms_dynamics_integration).returns(ms_dynamics_integration)
        assert_equal ms_dynamics_integration, account.crm_integration
      end
    end

    describe("#crm_integration_configured?") do
      before do
        crm_integration = Object.new
        crm_integration.stubs(:configured?).returns(true)
        account.stubs(:crm_integration).returns(crm_integration)
      end

      it('returns crm_integration.configured?') do
        assert_equal account.crm_integration_configured?, true
      end
    end

    describe("#crm_integration_enabled?") do
      before do
        crm_integration = Object.new
        crm_integration.stubs(:enabled?).returns(true)
        account.stubs(:crm_integration).returns(crm_integration)
      end

      it('returns crm_integration.enabled?') do
        assert_equal account.crm_integration_enabled?, true
      end
    end

    describe("#salesforce_configuration_enabled?") do
      before do
        salesforce_integration = Object.new
        salesforce_integration.stubs(:configured?).returns(true)
        account.stubs(:salesforce_integration).returns(salesforce_integration)
      end

      it('returns salesforce_integration.configured?') do
        assert_equal account.salesforce_configuration_enabled?, true
      end
    end

    describe("#salesforce_integration") do
      describe('when feature central_admin_salesforce_integration is enabled') do
        before do
          account.stubs(:has_central_admin_salesforce_integration?).returns(true)
        end

        describe('when integration is enabled') do
          before do
            Salesforce::NextGeneration::Integration.any_instance.stubs(:configured_and_enabled?).returns(true)
          end

          it "returns next_generation_salesforce object" do
            assert_equal account.salesforce_integration.is_a?(Salesforce::NextGeneration::Integration), true
          end
        end

        describe('when integration is disabled') do
          before do
            Salesforce::NextGeneration::Integration.any_instance.stubs(:configured_and_enabled?).returns(false)
          end

          it "returns classic_generation_salesforce object" do
            assert_equal account.salesforce_integration.is_a?(Salesforce::NextGeneration::Integration), false
          end
        end

        describe('when data cache is enabled') do
          before do
            Salesforce::NextGeneration::Integration.any_instance.stubs(:configured_and_enabled?).returns(true)
            Salesforce::NextGeneration::Integration.any_instance.stubs(:data_cache_enabled?).returns(true)
          end

          it "returns crm_integration.data_cache_enabled? value" do
            assert_equal account.crm_integration_enabled?, true
          end
        end

        describe('when data cache is disabled') do
          before do
            Salesforce::NextGeneration::Integration.any_instance.stubs(:configured_and_enabled?).returns(true)
            Salesforce::NextGeneration::Integration.any_instance.stubs(:data_cache_enabled?).returns(false)
          end

          it "returns crm_integration.data_cache_enabled? value" do
            assert_equal account.crm_integration_enabled?, false
          end
        end
      end

      describe('when feature central_admin_salesforce_integration is disabled') do
        let(:salesforce_production) { SalesforceIntegration.new(is_sandbox: false, access_token: "access", refresh_token: "refresh", instance_url: "https://na2.salesforce.com") }
        let(:salesforce_sandbox) { SalesforceIntegration.new(is_sandbox: true, access_token: "access", refresh_token: "refresh", instance_url: "https://na2.salesforce.com") }

        before do
          account.stubs(:has_central_admin_salesforce_integration?).returns(false)
        end

        it "returns salesforce_production if only that one is available" do
          account.stubs(:salesforce_production).returns(salesforce_production)
          account.stubs(:salesforce_sandbox).returns(nil)
          assert_equal salesforce_production, account.salesforce_integration
        end

        it "returns salesforce_sandbox if only that one is available" do
          account.stubs(:salesforce_production).returns(nil)
          account.stubs(:salesforce_sandbox).returns(salesforce_sandbox)
          assert_equal salesforce_sandbox, account.salesforce_integration
        end

        it "returns salesforce_production by default if both are available" do
          account.stubs(:salesforce_production).returns(salesforce_production)
          account.stubs(:salesforce_sandbox).returns(salesforce_sandbox)
          assert_equal salesforce_production, account.salesforce_integration
        end

        it "returns salesforce_sandbox if both are available and admin chose that one" do
          account.stubs(:salesforce_production).returns(salesforce_production)
          account.stubs(:salesforce_sandbox).returns(salesforce_sandbox)
          account.settings.disable(:use_salesforce_production)
          assert_equal salesforce_sandbox, account.salesforce_integration
        end

        it "does not return non configured connections" do
          salesforce_production = SalesforceIntegration.new(is_sandbox: false)
          account.stubs(:salesforce_production).returns(salesforce_production)
          account.stubs(:salesforce_sandbox).returns(salesforce_sandbox)
          assert_equal salesforce_sandbox, account.salesforce_integration
        end
      end
    end
  end
end
