require_relative '../../support/test_helper'
require_relative '../../support/multiproduct_test_helper'

SingleCov.covered! uncovered: 23

describe 'Accounts creation' do
  fixtures :translation_locales, :tickets, :accounts
  include MultiproductTestHelper
  fixtures :translation_locales, :tickets
  let(:locale1) { TranslationLocale.create!(locale: "ja", name: "ja") }
  let(:ticket) { tickets(:minimum_1) }

  describe ".select_pod" do
    it "chooses current pod when there is only one pod available" do
      AccountCreationShard.stubs(:pluck).returns([1])
      assert_equal 1, Account.select_pod("us")
    end

    describe "for development environment" do
      before do
        Rails.env.stubs(:development?).returns(true)
      end

      it "uses current pod" do
        assert_includes [1], Account.select_pod('us', subdomain: "doesnotstartwithzeethreeen")
      end
    end

    describe "for staging environment" do
      let (:pods) { anything }

      before do
        Rails.env.stubs(:production?).returns(false)
        Rails.env.stubs(:staging?).returns(true)
        Rails.env.stubs(:test?).returns(false)
        Rails.env.stubs(:development?).returns(false)
        AccountCreationShard.stubs(:where).returns(pods)
      end

      describe "account creation shard database table is not populated" do
        before do
          pods.stubs(:pluck).returns([])
        end

        it "fail back to hard coded pods and send out rollbar" do
          ZendeskExceptions::Logger.expects(:record).once
          assert_includes [999, 998], Account.select_pod('us', subdomain: "doesnotstartwithzeethreeen")
        end
      end

      describe "account creation shard database table is populated" do
        before do
          pods.stubs(:pluck).returns([999, 998])
        end

        it "uses the database pods information" do
          assert_includes [999, 998], Account.select_pod('us', subdomain: "doesnotstartwithzeethreeen")
        end
      end
    end

    describe "for production environment" do
      let (:pods) { anything }

      before do
        Rails.env.stubs(:production?).returns(true)
        Rails.env.stubs(:staging?).returns(false)
        Rails.env.stubs(:test?).returns(false)
        Rails.env.stubs(:development?).returns(false)
        AccountCreationShard.stubs(:where).returns(pods)
      end

      describe "account creation region" do
        before do
          pods.stubs(:pluck).returns([])
        end

        describe "region is blank" do
          it "increment region_blank in datadog" do
            Zendesk::StatsD::Client.any_instance.expects(:increment).once.with(
              'region_blank',
              tags: ['region:']
            )

            Account.select_pod(nil)
          end
        end

        describe "region is valid" do
          it "increment region_valid in datadog with region name" do
            Zendesk::StatsD::Client.any_instance.expects(:increment).once.with(
              'region_valid',
              tags: ['region:us']
            )

            Account.select_pod('us')
          end
        end
      end

      describe "account creation shard database table is not populated" do
        before do
          pods.stubs(:pluck).returns([])
        end

        it "fail back to hard coded pods and send out rollbar" do
          ZendeskExceptions::Logger.expects(:record).once
          assert_includes [13, 14], Account.select_pod('us', subdomain: "doesnotstartwithzeethreeen")
        end
      end

      describe "account creation shard database table is populated" do
        before do
          pods.stubs(:pluck).returns([12, 13, 14, 15])
        end

        it "uses the database pods information" do
          assert_includes [12, 13, 14, 15], Account.select_pod('us', subdomain: "doesnotstartwithzeethreeen")
        end
      end
    end
  end

  describe '#add_default_permission_sets' do
    let(:account) { accounts(:minimum) }
    let(:default_permission_sets) { PermissionSet.build_default_permission_sets(account) }
    let(:number_default_permission_sets) { default_permission_sets.count }

    before do
      default_permission_sets.each_with_index do |ps, index|
        ps.stubs(:id).returns(index)
      end
      ::PermissionSet.stubs(:build_default_permission_sets).returns(default_permission_sets)
    end

    it 'creates the default permission sets' do
      assert_equal account.permission_sets.count, 0
      account.add_default_permission_sets
      assert_equal account.reload.permission_sets.count, number_default_permission_sets
    end

    it 'can be called more than once' do
      account.add_default_permission_sets
      original = account.reload.permission_sets.count
      account.add_default_permission_sets
      assert_equal account.reload.permission_sets.count, original
    end

    it 'syncs all default sets to core services' do
      default_permission_sets.each do |permission_set|
        Omnichannel::RoleSyncJob.expects(:enqueue).with(
          account_id: account.id,
          permission_set_id: permission_set.id,
          role_name: "custom_#{permission_set.id}",
          context: 'updated_plan',
          permissions_changed: []
        )
      end
      account.add_default_permission_sets
    end
  end

  describe '.store_id_on_shard' do
    describe "for multiproduct accounts" do
      it 'can be called after account created' do
        account = setup_shell_account
        account.send(:store_id_on_shard)
        account.send(:store_id_on_shard)
      end
    end

    describe "for non multiproduct accounts" do
      it 'can not be called after account created' do
        account = accounts(:trial)
        assert_raises ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation do
          account.send(:store_id_on_shard)
        end
      end
    end
  end
end
