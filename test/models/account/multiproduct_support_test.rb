require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::MultiproductSupport do
  fixtures :accounts

  let(:account) { accounts(:multiproduct) }

  describe '#start_support_trial' do
    before do
      account.expects(:save_job_id).once
    end

    describe 'with a user specified' do
      let(:user) { account.owner }

      it 'enqueues a job to create Support subscription' do
        SupportCreationJob.expects(:enqueue).with(account_id: account.id, user_id: user.id)
        account.start_support_trial(user)
      end
    end

    describe 'with no user specified' do
      it 'enqueues a job to create Support subscription' do
        SupportCreationJob.expects(:enqueue).with(account_id: account.id, user_id: User.system.id)
        account.start_support_trial
      end
    end
  end

  describe '#support_creation_cache_key' do
    it 'returns the expected string' do
      expected = "support-creation-job-id-for-#{account.id}"
      assert_equal expected, account.send(:support_creation_cache_key)
    end
  end

  describe '#save_job_id' do
    let(:job_id) { '123' }

    before do
      Rails.cache.clear
    end

    it 'saves job_id in cache' do
      account.send(:save_job_id, job_id)
      assert_equal Rails.cache.read(account.support_creation_cache_key), job_id
    end
  end
end
