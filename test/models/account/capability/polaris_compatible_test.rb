require_relative '../../../support/test_helper'
require_relative '../../../support/account_products_test_helper'

SingleCov.covered!

describe "Polaris Compability Test" do
  include AccountProductsTestHelper

  fixtures :accounts

  let(:has_groups) { true }

  before do
    subscription = stub('subscription', has_groups?: has_groups, plan_type: has_groups ? SubscriptionPlanType.Medium : SubscriptionPlanType.Small)
    @account = accounts(:minimum)
    @account.stubs(:subscription).returns(subscription)
  end

  def generate_chat_product(plan_type, phase)
    Zendesk::Accounts::Product.new(make_product(product_name: 'chat', plan_settings: { 'phase' => phase, 'plan_type': plan_type }))
  end

  def stub_chat_request(chat_phase)
    chat_product = generate_chat_product(ZBC::Zopim::PlanType::Lite, chat_phase)

    Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns(true)
    Zendesk::Accounts::Client.any_instance.stubs(:chat_product).returns(chat_product)
  end

  describe "account settings with no suite" do
    before do
      @account.stubs(:has_active_suite?).returns(false)
    end

    describe_with_arturo_enabled :aw_broad_availability_multiproduct do
      before do
        Arturo.enable_feature!(:aw_broad_availability_multiproduct)
      end

      describe "and has Chat enabled" do
        before do
          @account.subscription.stubs(has_chat?: true)
        end

        describe "without account being accessible from Account Service" do
          before do
            Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns(false)
          end

          it "returns false for Agent Workspace compability" do
            refute(@account.polaris_compatible_plan?)
          end
        end

        describe "with Chat Phase 2" do
          before do
            stub_chat_request(2)
          end

          it "returns false for Agent Workspace compability" do
            refute(@account.polaris_compatible_plan?)
          end
        end

        describe "with Chat Phase 3" do
          before do
            stub_chat_request(3)
          end

          it "returns true for Agent Workspace compability" do
            assert(@account.polaris_compatible_plan?)
          end
        end

        describe "with Chat Phase 4" do
          before do
            stub_chat_request(4)
          end

          it "returns true for Agent Workspace compability" do
            assert(@account.polaris_compatible_plan?)
          end
        end

        describe 'with valid chat but without groups support' do
          let(:has_groups) { false }
          before { stub_chat_request(4) }

          it 'returns false for agent workspace compatibility' do
            refute_predicate @account, :polaris_compatible_plan?
          end
        end
      end

      describe "and does not have Chat" do
        before do
          @account.subscription.stubs(has_chat?: false)
        end

        it "returns false for Agent Workspace compability" do
          refute(@account.polaris_compatible_plan?)
        end
      end
    end

    describe_with_arturo_disabled :aw_broad_availability_multiproduct do
      before do
        Arturo.disable_feature!(:aw_broad_availability_multiproduct)
      end

      describe "with Chat product, Chat Phase 3" do
        before do
          stub_chat_request(3)
        end

        it "returns false for Agent Workspace compability" do
          refute(@account.polaris_compatible_plan?)
        end
      end
    end
  end

  describe "has active suite" do
    before do
      @account.stubs(:has_active_suite?).returns(true)
    end

    it "returns true for Agent Workspace compability" do
      assert_equal @account.polaris_compatible_plan?, true
    end
  end

  describe 'with agent_workspace plan setting' do
    let(:support_trial_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_trial_product).with_indifferent_access) }

    before do
      support_trial_product.plan_settings[:agent_workspace] = true
      @account.stubs(:products).returns([support_trial_product])
    end

    it 'is a polaris compatible account' do
      assert_predicate @account, :polaris_compatible_plan?
    end

    describe 'without groups support' do
      let(:has_groups) { false }

      it 'is not a polaris compatible plan' do
        refute_predicate @account, :polaris_compatible_plan?
      end
    end
  end
end
