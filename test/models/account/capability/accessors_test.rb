require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'CapabilityAccessor' do
  class SuperHero
    include Account::Capability::Accessors
  end

  describe "Capability accessors" do
    before do
      SuperHero.feature :superpowers, arturo: false
    end

    describe "feature" do
      it "adds a new capability" do
        capability = SuperHero.capabilities[:superpowers]
        assert_equal :superpowers, capability.name
      end

      it "adds an availability query method" do
        capability = SuperHero.capabilities[:superpowers]
        batman     = SuperHero.new

        capability.stubs(:available?).returns(false)
        batman.instance_variable_get(:@memoize).try(:clear)
        assert_equal false, batman.has_superpowers?

        capability.stubs(:available?).returns(true)
        batman.instance_variable_get(:@memoize).try(:clear)
        assert(batman.has_superpowers?)
      end

      it "does not add an available and enabled query when the capability can't be enabled" do
        class Villian
          include Account::Capability::Accessors
          feature :superpowers, setting: false
        end
        batman = Villian.new

        assert_equal false, batman.respond_to?(:has_superpowers_enabled?)
      end

      it "adds an available and enabled query method when the capability can be enabled" do
        SuperHero.feature :superpowers, setting: true
        capability = SuperHero.capabilities[:superpowers]
        batman     = SuperHero.new

        capability.stubs(:enabled?).returns(false)
        capability.stubs(:available?).returns(false)
        batman.instance_variable_get(:@memoize).try(:clear)
        assert_equal false, batman.has_superpowers_enabled?

        capability.stubs(:enabled?).returns(false)
        capability.stubs(:available?).returns(true)
        batman.instance_variable_get(:@memoize).try(:clear)
        assert_equal false, batman.has_superpowers_enabled?

        capability.stubs(:enabled?).returns(true)
        capability.stubs(:available?).returns(true)
        batman.instance_variable_get(:@memoize).try(:clear)
        assert(batman.has_superpowers_enabled?)
      end
    end
  end
end
