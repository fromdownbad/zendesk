require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Account::CustomerSatisfactionSupport do
  fixtures :accounts, :rules, :account_property_sets, :subscriptions

  let(:default_automation_name) { 'Request customer satisfaction rating (System Automation)' }
  let(:default_view_name) { '{{zd.recently_rated_tickets}}' }

  before do
    @account = accounts(:minimum)
    @account.automations.delete_all
    @account.changed_customer_satisfaction = true
  end

  describe 'that can have CSR' do
    it 'automatically create an automation and view when enabling CSR' do
      assert_equal(0, @account.automations.count(:all))
      assert_difference('@account.automations.count(:all)', +1) do
        assert_difference('@account.views.count(:all)', +1) do
          @account.update_attributes!(settings: { customer_satisfaction: 1 })
        end
      end
    end

    it 'does not create an automation and view when locked' do
      @account.expects(:is_locked?).returns(true)
      refute_difference('@account.automations.count(:all)') do
        refute_difference('@account.views.count(:all)') do
          @account.update_attributes!(settings: { customer_satisfaction: 1 })
        end
      end
    end

    describe 'when CSR settings are updated' do
      before do
        @account.settings.stubs(:where).with(name: 'customer_satisfaction').returns([false])
      end

      it 'does not create or enable the default automation' do
        assert_difference('@account.automations.count(:all)', 0) do
          @account.update_attributes!(settings: { customer_satisfaction: 1 })
        end
      end

      it 'does not create or enable the default view' do
        assert_difference('@account.views.count(:all)', 0) do
          @account.update_attributes!(settings: { customer_satisfaction: 1 })
        end
      end
    end

    describe "when enabled" do
      before do
        @account.update_attributes!(settings: { customer_satisfaction: 1 })
      end

      it 'does not update when saving unrelated' do
        Rule.any_instance.expects(:update_attributes).never
        @account.update_attributes(name: "Unrelated change")
      end

      it 'does not update when saving same state' do
        Rule.any_instance.expects(:update_attributes).never
        @account.update_attributes!(settings: { customer_satisfaction: 1 })
      end
    end

    it 'caches nils' do
      refute @account.send(:default_automation)
      refute @account.send(:default_view)
      Automation.expects(:find_by_sql).never
      View.expects(:find_by_sql).never
      refute @account.send(:default_automation)
      refute @account.send(:default_view)
    end

    describe "create automation and activate" do
      before do
        @account.update_attributes!(settings: { customer_satisfaction: 1 })
        assert_not_nil @account.automations.find_by_title(default_automation_name)
      end

      it "checks if automation is activated" do
        assert(@account.automations.find_by_title(default_automation_name).is_active)
      end
    end

    it "adds 'Ticket is public' condition to the default automation" do
      @account.update_attributes!(settings: { customer_satisfaction: 1 })
      automation = @account.automations.find_by_title(default_automation_name)
      assert automation.definition.conditions_all.find { |d| d.source == "ticket_is_public" }
    end

    describe "create view and activate" do
      before do
        @account.update_attributes!(settings: { customer_satisfaction: 1 })
        assert_not_nil @account.views.find_by_title(default_view_name)
      end

      it "checks if view is activated" do
        assert(@account.views.find_by_title(default_view_name).is_active)
      end
    end

    describe "create automation and deactivate" do
      before do
        @account.update_attributes!(settings: { customer_satisfaction: 1 })
        assert_not_nil @account.automations.find_by_title(default_automation_name)
        @account.update_attributes!(settings: { customer_satisfaction: 0 })
      end

      it "checks if automation is deactivated" do
        assert_equal false, @account.automations.find_by_title(default_automation_name).is_active
      end
    end

    describe "create view and deactivate" do
      before do
        @account.update_attributes!(settings: { customer_satisfaction: 1 })
        assert_not_nil @account.views.find_by_title(default_view_name)
        @account.update_attributes!(settings: { customer_satisfaction: 0 })
      end

      it "checks if view is deactivated" do
        assert_equal false, @account.views.find_by_title(default_view_name).is_active
      end
    end
  end

  describe '#csat_reason_code_enabled?' do
    describe_with_arturo_setting_disabled :customer_satisfaction do
      it "returns false" do
        refute @account.csat_reason_code_enabled?
      end
    end

    describe_with_arturo_setting_enabled :customer_satisfaction do
      describe_with_arturo_enabled :csat_reason_code do
        describe_with_arturo_setting_disabled :csat_reason_code do
          it 'returns false' do
            refute @account.csat_reason_code_enabled?
          end
        end

        describe_with_arturo_setting_enabled :csat_reason_code do
          it "returns true" do
            assert @account.csat_reason_code_enabled?
          end
        end
      end

      describe_with_arturo_disabled :csat_reason_code do
        it 'returns false' do
          refute @account.csat_reason_code_enabled?
        end
      end
    end
  end
end
