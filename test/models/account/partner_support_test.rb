require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Account::PartnerSupport' do
  fixtures :all

  describe "an account" do
    let(:required_params) do
      {
        account: {
          name: 'Evil LLC',
          subdomain: 'evil',
          help_desk_size: 'Small Team',
        },
        owner: {
          name: 'Dr. Evil',
          email: 'doctor@evil.com'
        },
        address: {
          phone: '+1-123-456-7890'
        }
      }
    end

    let(:account) do
      Zendesk::Accounts::Initializer.new(params, '0.0.0.0', :us).
        account.tap(&:save)
    end

    describe "when created with partner information" do
      let(:params) do
        required_params.merge(
          partner: {
            name: 'Mini Me',
            url: 'http://minime.example.com',
          }
        )
      end

      it "#via_partner? returns true" do
        assert account.via_partner?
      end
    end

    describe "when created without partner information" do
      let(:params) { required_params.dup }

      it "#via_partner? returns false" do
        refute account.via_partner?
      end
    end
  end
end
