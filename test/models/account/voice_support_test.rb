require_relative "../../support/test_helper"
require_relative "../../support/voice_test_helper"

SingleCov.covered! uncovered: 5

describe 'Account::VoiceSupport' do
  fixtures :accounts, :subscriptions, :users, :user_identities

  before do
    @account = accounts(:minimum)
    ZendeskBillingCore::Zuora::VoiceUsage.any_instance.stubs(:upload_usage)
  end

  describe 'mark_available_agents_unavailable' do
    it 'calls voice to mark all agents as unavailable' do
      @account.settings.voice = true

      request = stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/voice/internal/sub_accounts/disable_voice.json").
        to_return(status: 200, body: "", headers: {})

      @account.update_attributes(settings: { voice: 0 })

      assert_requested request
    end
  end

  describe "updating voice callback URL" do
    before do
      @account.settings.set(voice: 1)
    end

    describe "when the subdomain of an account changes" do
      it "updates the callback url at Twilio" do
        request = stub_request(:put, %r{zendesk-test\.com/api/v2/channels/voice/internal/sub_accounts/update_subdomain})
        @account.subdomain = "subdomain"
        @account.save(validate: false)
        assert_requested(request)
      end
    end

    describe "when the subdomain of an account doesn't change" do
      it "not enqueue a job to updates the callback url at Twilio" do
        request = stub_request(:put, %r{zendesk-test\.com/api/v2/channels/voice/internal/sub_accounts/update_subdomain})
        @account.save(validate: false)
        assert_not_requested(request)
      end
    end
  end

  describe "#suspend_sub_account" do
    it "skips suspending the sub-account on Voice when voice account is not enabled" do
      request = stub_request(:put, %r{/api/v2/channels/voice/internal/sub_accounts/suspend})
      @account.suspend_sub_account

      refute @account.voice_sub_account_active?
      assert_not_requested(request)
    end

    it "suspends the sub-account on Voice" do
      @account.stubs(:voice_sub_account_active?).returns(true)
      request = stub_request(:put, %r{/api/v2/channels/voice/internal/sub_accounts/suspend})
      @account.suspend_sub_account
      assert_requested(request)
    end
  end

  describe "account becomes unserviceable" do
    it 'suspends the sub-account' do
      @account.stubs(:voice_sub_account_active?).returns(true)
      request = stub_request(:put, %r{/api/v2/channels/voice/internal/sub_accounts/suspend})
      @account.update_attributes(is_serviceable: false)
      assert_requested(request)
    end
  end

  describe "#can_have_voice_enabled?" do
    describe "with an incoming phone number on a serviceable account" do
      before do
        @account.stubs(:phone_numbers).returns([Voice::PhoneNumber.new])
        @account.stubs(:is_serviceable?).returns(true)
      end

      it "returns true" do
        assert(@account.can_have_voice_enabled?)
      end
    end

    describe "with an incoming phone number on an unserviceable account" do
      before do
        @account.stubs(:phone_numbers).returns([Voice::PhoneNumber.new])
        @account.stubs(:is_serviceable?).returns(false)
      end

      it "returns true" do
        assert_equal(false, @account.can_have_voice_enabled?)
      end
    end

    describe "on an account without incoming phone number" do
      it "returns false" do
        assert_equal(false, @account.can_have_voice_enabled?)
      end
    end
  end

  describe '#voice_enabled?' do
    it "returns true if voice account exists and sub account is not suspended" do
      FactoryBot.create(:voice_account, account: @account)
      FactoryBot.create(:voice_sub_account, :active, account: @account)
      assert(@account.voice_enabled?)
    end

    it "returns false if voice account exists and sub account is suspended" do
      FactoryBot.create(:voice_account, account: @account)
      FactoryBot.create(:voice_sub_account, :suspended, account: @account)
      assert_equal(false, @account.voice_enabled?)
    end

    it "returns false if voice account does not exist" do
      assert_equal(false, @account.voice_enabled?)
    end
  end

  describe "when purchased account is being reactivated after suspension/cancellation" do
    let(:cancellation_date) { Time.utc(2019, 12, 16, 12, 10) }

    before do
      @account.stubs(:zuora_subscription).returns(stub(zuora_account_id: "zuora-id"))
      FactoryBot.create(:voice_sub_account, account: @account)
      Timecop.travel(cancellation_date) do
        @account.cancel!
      end
    end

    it "creates Talk usage subscription in Zuora starting on previous cancellation date" do
      ZBC::Zuora::Jobs::AddVoiceJob.expects(:work).
        with(@account.id, cancellation_date.to_date)

      @account.reactivate!
    end
  end
end
