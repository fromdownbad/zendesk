require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Account::SoftDelete' do
  fixtures :all

  describe Account do
    before do
      Zendesk::Net::AddressUtil.stubs(:safe_url?).returns(true)
      @account = accounts(:minimum)
      @account.stubs(:is_active?).returns(false)
      @account.stubs(:is_serviceable?).returns(false)
      @account.subscription.canceled_on = '2013-11-10T12:00:00Z'
      @account.subscription.save!
    end

    it "does not be found" do
      @account.soft_delete!
      assert_nil Account.find_by_subdomain(@account.subdomain)
    end

    it "does not delete dependencies" do
      @account.soft_delete!
      account = accounts(:minimum)
      assert_equal [], account.soft_delete_dependencies
    end

    it "revokes the active certificates" do
      certificate = Certificate.create! account: @account
      certificate.update_column :state, 'active'
      assert_difference "@account.certificates.active.count(:all)", -1 do
        @account.soft_delete!
      end
    end

    describe 'with a sharing agreement' do
      let(:outbound_account) { accounts(:support) }
      let!(:outbound_agreement) do
        stub_request(:post, %r{/sharing/agreements/})
        outbound_account.sharing_agreements.create!(
          subdomain: outbound_account.subdomain,
          direction: 'out',
          remote_url: 'https://test.zd-dev.com/sharing',
          status_id: Sharing::Agreement::STATUS_MAP[:accepted]
        )
      end
      let!(:inbound_agreement) do
        @account.sharing_agreements.new.tap do |agreement|
          agreement.setup_from_partner(outbound_agreement.for_partner)
          agreement.status = :accepted
          agreement.save
        end
      end

      it 'deactivates existing ticket sharing agreements' do
        @account.soft_delete!
        assert_equal outbound_agreement.reload.inactive?, true
        assert_equal inbound_agreement.reload.inactive?, true
      end
    end
  end
end
