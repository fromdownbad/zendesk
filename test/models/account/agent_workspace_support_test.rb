require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::AgentWorkspaceSupport do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  describe '#activate_agent_workspace_for_trial' do
    before do
      Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns(true)
      Zendesk::Accounts::Client.any_instance.stubs(:chat_product).returns chat_product
      Zopim::InternalApiClient.any_instance.stubs(:notify_account_settings_change)
    end

    describe 'with Chat product' do
      let(:chat_product) { stub(name: :chat) }

      before do
        Apps::ChatAppInstallation.any_instance.expects(:handle_disable_installation)
        account.activate_agent_workspace_for_trial
      end

      it 'sets settings.polaris to true' do
        assert_predicate account.settings, :polaris
      end

      it 'sets settings.check_group_name_uniqueness to true' do
        assert_predicate account.settings, :check_group_name_uniqueness
      end
    end

    describe 'without Chat product' do
      let(:chat_product) { nil }

      before do
        Apps::ChatAppInstallation.any_instance.expects(:handle_disable_installation).never
        account.activate_agent_workspace_for_trial
      end

      it 'sets settings.polaris to true' do
        assert_predicate account.settings, :polaris
      end

      it 'sets settings.check_group_name_uniqueness to true' do
        assert_predicate account.settings, :check_group_name_uniqueness
      end
    end
  end
end
