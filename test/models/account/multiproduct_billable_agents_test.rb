require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Account::MultiproductBillableAgent' do
  fixtures :accounts, :users, :subscriptions, :zopim_subscriptions, :permission_sets, :zopim_agents

  describe '#multiproduct_billable_agent_count!' do
    describe_with_and_without_arturo_enabled :ocp_compare_classic_staff_service_agent_count do
      describe 'account is non multiproduct and non Suite' do
        let(:current_account) { accounts(:minimum) }

        before do
          users(:minimum_admin)
          users(:minimum_agent)
        end

        it 'returns just the Support billable agents count' do
          # fixture account come with 2 agents by default
          assert_equal(4, current_account.multiproduct_billable_agent_count!)
        end
      end

      describe 'when account is mulitproduct' do
        let(:current_account) { accounts(:multiproduct) }

        before do
          Zendesk::StaffClient.any_instance.stubs(:seats_occupied_support_or_suite!).returns(8)
        end

        it 'gets value from Staff Service' do
          assert_equal(8, current_account.multiproduct_billable_agent_count!)
        end
      end

      describe 'when Staff Service occupied seats number is greater than subscription max_agents' do
        let(:current_account) { accounts(:multiproduct) }

        before do
          Zendesk::StaffClient.any_instance.stubs(:seats_occupied_support_or_suite!).returns(33)
        end

        it 'gets value from Staff Service' do
          assert_equal(33, current_account.multiproduct_billable_agent_count!)
        end
      end

      describe 'when Staff Service response do not have the seat key' do
        let(:current_account) { accounts(:multiproduct) }

        before do
          Zendesk::StaffClient.any_instance.stubs(:seats_occupied_support_or_suite!).returns(nil)
        end

        it 'returns nil' do
          assert_nil(current_account.multiproduct_billable_agent_count!)
        end
      end

      describe 'when account is spp' do
        let(:current_account) { accounts(:minimum) }

        before do
          current_account.stubs(:spp?).returns(true)
          Zendesk::StaffClient.any_instance.stubs(:seats_occupied_support_or_suite!).returns(8)
        end

        it 'gets value from Staff Service' do
          # fixture come with 10 max_agents by default
          assert_equal(8, current_account.multiproduct_billable_agent_count!)
        end
      end

      describe 'when account is not spp or multiproduct' do
        let(:current_account) { accounts(:minimum) }

        before do
          current_account.stubs(:mulitproduct).returns(false)
          current_account.stubs(:is_spp?).returns(false)
          Zendesk::StaffClient.any_instance.stubs(:seats_occupied_support_or_suite!).returns(7)
        end

        describe_with_arturo_disabled :ocp_check_seats_left_in_staff_service do
          before do
            users(:minimum_admin)
            users(:minimum_agent)
          end

          it 'returns just the Support billable agents count' do
            # fixture account come with 2 agents by default
            assert_equal(4, current_account.multiproduct_billable_agent_count!)
          end
        end

        describe_with_arturo_enabled :ocp_check_seats_left_in_staff_service do
          it 'gets the value from Staff service' do
            # occupied at staff service 7. Max agents: 10
            assert_equal(7, current_account.multiproduct_billable_agent_count!)
          end
        end
      end
    end
  end
end
