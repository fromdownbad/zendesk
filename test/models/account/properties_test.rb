require_relative "../../support/test_helper"

SingleCov.covered!

describe 'account properties' do
  fixtures :all

  before do
    @account   = accounts(:minimum)
    @requester = @account.end_users.last
    @assignee  = @account.groups.last.users.last
    @ticket    = Ticket.new(description: "Hello", account: @account)
  end

  describe "#fetch_settings" do
    before do
      @expected = {
        display_pinned_entries: true,
        display_pinned_entry_content: false
      }
      @account.settings.set(@expected.dup)
      @account.save!
      @account.reload
    end

    it "returns a hash of settings and values" do
      assert_equal @expected, @account.fetch_settings(:display_pinned_entries, :display_pinned_entry_content)
    end
  end

  describe "#polaris" do
    it "defaults to false" do
      refute @account.settings.polaris
    end
  end

  describe "#render_custom_uri_hyperlinks" do
    it "defaults to empty string" do
      assert_equal "", @account.texts.render_custom_uri_hyperlinks
    end

    it "returns the correct value when set" do
      @account.texts.render_custom_uri_hyperlinks = "dlr, cli"
      assert "dlr, cli", @account.texts.render_custom_uri_hyperlinks
    end
  end

  describe '#ccs_requester_excluded_public_comments' do
    it 'defaults to false' do
      refute @account.settings.ccs_requester_excluded_public_comments
    end
  end

  describe '#third_party_end_user_public_comments' do
    it 'defaults to false' do
      refute @account.settings.third_party_end_user_public_comments
    end
  end

  describe "#export_accessible_types" do
    it "is limited to DEFAULT jobs" do
      assert_equal(
        ImportExportJobPolicy::DEFAULT_JOBS,
        @account.settings.export_accessible_types
      )
    end
  end

  describe "#skill_based_filtered_views" do
    it "is set to empty array" do
      assert_equal(
        [],
        @account.settings.skill_based_filtered_views
      )
    end
  end

  describe "#business_rules_limit" do
    it "is set to default value" do
      assert_equal(
        Rule::RULES_TABLE_LIMITS,
        @account.settings.business_rules_limit
      )
    end
  end

  describe '#light_agent_email_ccs_allowed' do
    it 'defaults to false' do
      refute @account.settings.light_agent_email_ccs_allowed
    end
  end

  describe "#session_timeout" do
    it "does not validate not accepted values" do
      @account.settings.set(session_timeout: 333)

      assert_raise ActiveRecord::RecordInvalid do
        @account.settings.save!
      end
    end
  end

  describe "#export_role_id" do
    it "defaults to Role::ADMIN id" do
      assert_equal Role::ADMIN.id, @account.settings.export_role_id
    end
  end

  describe "#multibrand_includes_help_centers" do
    it 'returns true if property is set' do
      @account.settings.multibrand_includes_help_centers = true
      assert(@account.settings.multibrand_includes_help_centers)
    end

    it 'returns false if no property is set' do
      assert_equal false, @account.settings.multibrand_includes_help_centers
    end
  end

  describe "#rich_content_in_emails" do
    it "defaults to false" do
      refute @account.settings.rich_content_in_emails
    end
  end

  describe "No-Delimiter properties" do
    describe '#no_mail_delimiter' do
      describe 'for an account that does not have the no_mail_delimiter setting set' do
        before { @account = accounts(:support) }

        it 'defaults to false' do
          refute @account.settings.no_mail_delimiter
        end
      end
    end
  end

  describe "assignment" do
    describe "validations" do
      it "validates length" do
        @account.settings.set(cc_subject_template: 'a' * 256)
        @account.save
        @account.errors[:"settings.cc_subject_template"].must_include I18n.t("txt.admin.controllers.settings.account_controller.account_settings_invalid")
      end
    end

    describe "having legacy CCs disabled" do
      before { Account.any_instance.stubs(is_collaboration_enabled?: false) }

      it "does not set a requester object if the ticket is does not a new record" do
        ticket = @account.tickets.working.where.not(requester_id: @requester.id).last
        assert ticket.requester
        refute ticket.new_record?
        ticket.current_user = @assignee
        ticket.properties   = { requester_id: @requester.id }
        assert_equal ticket.requester, Ticket.find(ticket.id).requester
        ticket.stubs(:new_record?).returns(true)
        ticket.properties = { requester_id: @requester.id }
        assert ticket.requester != Ticket.find(ticket.id).requester
      end
    end

    describe "having legacy CCs enabled" do
      before { Account.any_instance.stubs(is_collaboration_enabled?: true) }

      it "sets a requester object even if the ticket is not a new record" do
        ticket = @account.tickets.working.where.not(requester_id: @requester.id).last
        assert ticket.requester != @requester
        refute ticket.new_record?
        ticket.current_user = @assignee
        ticket.properties   = { requester_id: @requester.id }
        assert_equal ticket.requester, @requester
      end
    end

    it "sets assignee property by id" do
      ticket = @account.tickets.working.last
      ticket.current_user = @assignee
      ticket.assignee     = nil
      assert ticket.assignee.nil?
      ticket.properties = { assignee_id: @assignee.id }
      assert_equal ticket.assignee, @assignee
    end

    it "sets assignee property by name" do
      ticket = @account.tickets.working.last
      ticket.current_user = @assignee
      ticket.assignee     = nil
      assert ticket.assignee.nil?
      ticket.properties = { assignee_name: @assignee.name.upcase }
      assert_equal ticket.assignee, @assignee
    end

    it "saves new requesters for new tickets" do
      Access::Validations::TicketValidator.any_instance.stubs(:flags_allow_comment?).returns(false)
      @ticket.current_user = @assignee
      assert_difference 'User.count(:all)' do
        @ticket.properties = { requester_email: "hello.there@example.com" }
      end
    end

    it "rolls back if turned into an invalid state" do
      @ticket.group        = accounts(:minimum).groups.first
      @ticket.current_user = @assignee
      @ticket.requester    = @requester
      @ticket.status_id    = StatusType.OPEN
      @ticket.will_be_saved_by(@ticket.submitter)
      @ticket.save!

      assert @ticket.status?(:open)
      @ticket.properties = { status_id: StatusType.PENDING }
      assert @ticket.status?(:pending)
      @ticket.properties = { status_id: StatusType.OPEN }
      assert @ticket.status?(:open)

      @ticket.expects(:valid?).returns(false)
      @ticket.properties = { status_id: StatusType.PENDING }
      assert @ticket.status?(:open)
    end
  end

  describe "normalization" do
    it "does nothing when given nothing" do
      results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, { })
      assert_empty properties_not_applied
      assert_equal({ }, results)
    end

    it "filters away unknown attributes" do
      results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, nisse: "per")
      assert_empty properties_not_applied
      assert_equal({ }, results)
    end

    describe "requester" do
      describe "using id" do
        it "sets a user object if said user exists" do
          assert @ticket.requester.nil?
          @ticket.properties = { requester_id: @requester.id }
          assert_equal @ticket.requester, @requester
        end

        it "does not sets a user object if no such user exists" do
          @ticket.properties = { requester_id: -800 }
          assert @ticket.requester.nil?
        end
      end

      describe "using email" do
        describe "of an existing user" do
          it "sets a user object" do
            assert @ticket.requester.nil?
            @ticket.properties = { requester_email: @requester.email }
            assert_equal @ticket.requester, @requester
          end
        end

        describe "of a non-existing user" do
          before { ActionMailer::Base.perform_deliveries = true }

          it "creates a user object if no such user exists" do
            assert @ticket.requester.nil?
            @ticket.properties = { requester_email: "hello.there.someone@example.com" }
            assert @ticket.requester
            refute @ticket.requester.new_record?
            assert_equal @ticket.requester.email, "hello.there.someone@example.com"
            assert_equal @ticket.requester.name,  "Hello There Someone"
          end

          it "does not sends verification emails if the account specifies such" do
            @account.update_attribute(:is_welcome_email_when_agent_register_enabled, true)
            assert @ticket.requester.nil?
            refute_difference 'ActionMailer::Base.deliveries.size' do
              @ticket.properties = { requester_email: "hello.there.someone@example.com" }
            end
          end

          it "not sends verification emails if the account specifies such" do
            @account.update_attribute(:is_welcome_email_when_agent_register_enabled, false)
            assert @ticket.requester.nil?
            refute_difference 'ActionMailer::Base.deliveries.size' do
              @ticket.properties = { requester_email: "hello.there.someone@example.com" }
            end
          end
        end
      end

      describe "using phone number" do
        describe "of an existing user" do
          before do
            Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true)
            @caller = @account.users.create(name: "Caller")
            @phone = UserPhoneNumberIdentity.create(user: @caller, value: '4129996294')
          end

          it "sets a user object" do
            assert @ticket.requester.nil?
            @ticket.properties = { requester_phone: @caller.identities.phone.first.value }
            assert_equal @caller, @ticket.requester
          end
        end

        describe "of a non-existing user" do
          it "creates a user object if no such user exists" do
            assert @ticket.requester.nil?
            phone_number = "19980187401"
            @ticket.properties = { requester_phone: phone_number }
            assert @ticket.requester
            refute @ticket.requester.new_record?
            assert_equal Voice::Core::NumberSupport::E164Number.internal(phone_number), @ticket.requester.identities.phone.first.value
            assert_equal Voice::Core::NumberSupport::E164Number.internal(phone_number), @ticket.requester.name
          end
        end
      end
    end

    describe "assignee" do
      before do
        @ticket.requester = @requester
      end

      describe "using id" do
        it "sets an agent object if said agent exists" do
          assert @ticket.assignee.nil?
          @ticket.properties = { assignee_id: @assignee.id }
          assert_equal @assignee, @ticket.assignee
        end

        it "ignores users who are not an agent" do
          assert @ticket.assignee.nil?
          @ticket.properties = { assignee_id: @requester.id }
          assert @ticket.assignee.nil?
        end

        it "does does not set an agent object if no such agent exists" do
          assert @ticket.assignee.nil?
          @ticket.properties = { assignee_id: -123 }
          assert @ticket.assignee.nil?
        end
      end

      describe "using email" do
        it "sets an agent object if said agent exists" do
          assert @ticket.assignee.nil?
          @ticket.properties = { assignee_email: @assignee.email }
          assert_equal @assignee, @ticket.assignee
        end

        it "ignores users who are not an agent" do
          assert @ticket.assignee.nil?
          @ticket.properties = { assignee_email: @requester.email }
          assert @ticket.assignee.nil?
        end

        it "does does not set an agent object if no such agent exists" do
          assert @ticket.assignee.nil?
          @ticket.properties = { assignee_email: "hello.you.are.special@example.com" }
          assert @ticket.assignee.nil?
        end
      end
    end

    describe "group" do
      before do
        @group = @account.groups.first
        assert @group
      end

      it "is set when given a valid group id" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, group_id: @group.id)
        assert_empty properties_not_applied
        assert_equal({ group: @group }, results)
      end

      it "is set when given a valid group name" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, group_name: @group.name)
        assert_empty properties_not_applied
        assert_equal({ group: @group }, results)
      end

      it "does not be set when given an invalid group id" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, group_id: -700)
        assert_equal [:group_id], properties_not_applied
        assert_equal({ }, results)
      end

      it "does not be set when given an invalid group name" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, group_name: "Does not exist 12631")
        assert_equal [:group_name], properties_not_applied
        assert_equal({ }, results)
      end
    end

    describe "priority" do
      it "is set when given a valid priority_id" do
        PriorityType.fields.each do |priority_id|
          results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, priority_id: priority_id)
          assert_empty properties_not_applied
          assert_equal({ priority_id: priority_id }, results)
        end
      end

      it "does not be set when given a invalid priority_id" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, priority_id: "hello")
        assert_empty properties_not_applied
        assert_equal({ }, results)
      end
    end

    describe "ticket_type" do
      it "is set when given a valid ticket_type_id" do
        TicketType.fields.each do |ticket_type_id|
          results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, ticket_type_id: ticket_type_id)
          assert_empty properties_not_applied
          assert_equal({ ticket_type_id: ticket_type_id }, results)
        end
      end

      it "does not be set when given a invalid ticket_type_id" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, ticket_type_id: "hello")
        assert_empty properties_not_applied
        assert_equal({ }, results)
      end
    end

    describe "status_id" do
      it "is set when given a valid status_id" do
        StatusType.fields.each do |status_id|
          results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, status_id: status_id)
          assert_empty properties_not_applied
          assert_equal({ status_id: status_id }, results)
        end
      end

      it "does not be set when given a invalid status_id" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, status_id: "hello")
        assert_empty properties_not_applied
        assert_equal({ }, results)
      end
    end

    describe "set_tags" do
      it "is set when given a string of tags" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, set_tags: "foo fum")
        assert_empty properties_not_applied
        assert_equal({ set_tags: "foo fum" }, results)
      end
    end

    describe "is_public" do
      it "does not be retained during normalization" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, is_public: true)
        assert_empty properties_not_applied
        assert_equal({ }, results)
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, is_public: false)
        assert_empty properties_not_applied
        assert_equal({ }, results)
      end
    end

    describe "locale_id" do
      it "is set when given a locale_id" do
        results, properties_not_applied = Zendesk::Tickets::Properties.normalize(@account, locale_id: 4)
        assert_empty properties_not_applied
        assert_equal({ locale_id: 4 }, results)
      end
    end
  end

  describe 'audit' do
    it 'tracks modifications of the use_feature_framework_persistence property' do
      @account.settings.use_feature_framework_persistence = false
      @account.save!
      @agent = @account.users.first

      CIA.audit actor: @agent, ip_address: '127.0.0.1' do
        @account.settings.use_feature_framework_persistence = true
        @account.save!
      end

      changes = @account.settings.where(name: 'use_feature_framework_persistence').first.cia_attribute_changes.last
      assert changes.new_value
    end
  end

  describe "misc" do
    before do
      @account       = accounts(:minimum)
      @settings      = @account.settings
      @old_cache_key = @account.scoped_cache_key(:settings)
    end

    describe "Updating an account setting" do
      before do
        @settings.emoji_autocompletion = !@settings.emoji_autocompletion
        @settings.save!
      end

      it "updates the account's cache key" do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal @old_cache_key, @account.scoped_cache_key(:settings)
      end
    end

    describe "Updating the prefer_lotus property" do
      before do
        Account.any_instance.stubs(:has_multibrand?).returns(true)
        @settings.prefer_lotus = true
        @settings.save!
      end

      it "does not allow setting it to false for accounts using multiple brands" do
        FactoryBot.create(:brand, account_id: @account.id)
        assert(@account.reload.has_multiple_brands?)
        assert(@settings.prefer_lotus)
        @settings.prefer_lotus = false

        assert_raise ActiveRecord::RecordInvalid do
          @settings.save!
        end
      end

      it "does allow setting it to false for accounts not using multiple brands" do
        assert(@settings.prefer_lotus)
        @settings.prefer_lotus = false

        @settings.save!
        assert_equal false, @settings.prefer_lotus
      end

      it "always allows setting it to true" do
        @settings.prefer_lotus = false
        @settings.save!
        assert_equal false, @settings.prefer_lotus

        @settings.prefer_lotus = true
        @settings.save!
        assert(@settings.prefer_lotus)
      end
    end
  end

  describe 'texts' do
    let(:account) { accounts(:minimum) }

    describe 'validating the `conditional_rate_limits` text' do
      before do
        account.texts.conditional_rate_limits = [{
          'prop_key'         => 'limit_1',
          'limit'            => 1,
          'interval_seconds' => 1,
          'expires_on'       => '2020-01-21',
          'owner_email'      => nil,
          'properties'       => {
            'request_method' => 'users/create_many',
            'endpoint'       => nil,
            'user_agent'     => nil,
            'query_string'   => nil,
            'user_id'        => nil
          }
        }]
      end

      it 'enforces the prop keys are unique' do
        account.texts.conditional_rate_limits << {
          'prop_key'         => 'limit_1',
          'limit'            => 1,
          'interval_seconds' => 1,
          'expires_on'       => nil,
          'owner_email'      => nil,
          'properties'       => {
            'request_method' => 'organizations/create_many',
            'endpoint'       => nil,
            'user_agent'     => nil,
            'query_string'   => nil,
            'user_id'        => nil
          }
        }

        refute_valid account
      end

      it 'enforces at least one property in the limit' do
        account.texts.conditional_rate_limits << {
          'prop_key'         => 'limit_2',
          'limit'            => 1,
          'interval_seconds' => 1,
          'expires_on'       => nil,
          'owner_email'      => nil,
          'properties'       => {
            'request_method' => nil,
            'endpoint'       => nil,
            'user_agent'     => nil,
            'query_string'   => nil,
            'user_id'        => nil
          }
        }

        refute_valid account
      end
    end
  end
end
