require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 7

describe Account::Cleanup do
  fixtures :accounts, :groups

  describe '#delete_bogus_account_ids_entry' do
    before do
      ActiveRecord::Base.connection.insert('insert into account_ids values (98765)')
      ActiveRecord::Base.connection.insert('insert into account_ids values (87654)')
    end

    describe 'with dependent children present' do
      before do
        # work around problems where an observer is trying to clear the cache for a non-existant account.
        ActiveRecord::Base.connection.insert("insert into groups set id=12310983, account_id=98765, name='foo', created_at = now()")
        ActiveRecord::Base.connection.insert("insert into account_settings set id=123, account_id=98765, name='foo_setting', created_at = now(), value='blah'")
        @bogus_audit = QueueAudit.create!(
          account_id: 98765,
          enqueued_id: "random",
          job_klass: MailRenderingJob,
          payload: "SecurityMailer sent during signup"
        )
        @bogus_group = Group.last
        @bogus_setting = AccountSetting.where(name: 'foo_setting').first
        assert_equal 'blah', @bogus_setting.value
      end

      it 'succeeds when called with extant bogus account id' do
        Account.delete_bogus_account_ids_entry(nil, 98765)

        refute ActiveRecord::Base.connection.select_values('select * from account_ids').map(&:to_i).member?(98765)
        assert ActiveRecord::Base.connection.select_values('select * from account_ids').map(&:to_i).member?(87654)

        refute Group.exists?(@bogus_group.id)
        refute QueueAudit.exists?(@bogus_audit.id)
        refute AccountSetting.exists?(@bogus_setting.id)
      end
    end

    it 'succeeds when called with extant bogus account id' do
      Account.delete_bogus_account_ids_entry(nil, 98765)

      refute ActiveRecord::Base.connection.select_values('select * from account_ids').map(&:to_i).member?(98765)
      assert ActiveRecord::Base.connection.select_values('select * from account_ids').map(&:to_i).member?(87654)
    end

    it 'logs a Rails.logsger warning when called with a non existant bogus account id' do
      Rails.logger.expects(:warn).at_least_once
      Account.delete_bogus_account_ids_entry(nil, 234567)
    end

    it 'raises when called with a real account id' do
      assert_raise RuntimeError do
        Account.delete_bogus_account_ids_entry(nil, accounts(:minimum).id)
      end
    end
  end

  describe '#get_bogus_accounts_on_current_shard' do
    describe 'with a known bogus account id' do
      before do
        ActiveRecord::Base.connection.insert('insert into account_ids values (98765)')
      end

      it 'returns the bogus id' do
        assert Account.get_bogus_accounts_on_current_shard.include?(98765)
      end
    end
  end

  describe '#account_id_table_entries_current_shard' do
    it 'returns at least one valid account id' do
      real_accounts = Account.all.map(&:id)
      assert Account.account_id_table_entries_current_shard.select { |a_id| real_accounts.include? a_id }.size >= 1
    end
  end

  describe '#real_account_ids_current_shard' do
    it 'returns the same number of unique account ids as there are accounts' do
      assert_equal Account.count(:all), Account.real_account_ids_current_shard.uniq.size
    end
  end
end
