require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'Tracking' do
  let(:account) { accounts(:trial) }

  it 'normalizes customer type' do
    {
      nil => 'Unknown',
      'Inbox' => 'Unknown',
      'null' => 'Unknown',
      'Small team' => 'VSB',
      'medium' => 'SMB',
      '4' => 'VSB',
      '1-9' => 'VSB',
      '244' => 'SMB',
      '25-49' => 'SMB',
      'Large group' => 'MM',
      'large' => 'MM',
      '1000-4999' => 'MM'
    }.each do |raw_type, expected|
      account.help_desk_size = raw_type
      assert_equal expected, account.customer_type
    end
  end

  it 'supports time-to-action' do
    account.created_at = Time.now - 2.days - 10.hours - 2.minutes

    assert_equal 2.42, account.send(:time_to_action).in_days
    assert_equal 58.03, account.send(:time_to_action).in_hours
    assert_equal 3482, account.send(:time_to_action).in_minutes
  end
end
