require_relative "../../support/test_helper"

SingleCov.covered!

describe 'AssumptionSupport' do
  let(:account) { accounts(:minimum) }
  before { Timecop.freeze('2016-01-01') }

  describe '#assumable?' do
    describe 'when assumption_expiration is not set' do
      it 'is not assumable' do
        refute account.assumable?
      end
    end

    describe 'when assumption_expiration date is in the future' do
      before do
        account.settings.assumption_expiration = 1.year.from_now
      end

      it 'is assumable' do
        assert account.assumable?
      end
    end

    describe 'when assumption_expiration date is in the past' do
      before do
        account.settings.assumption_expiration = 1.year.ago
      end

      it 'is not assumable' do
        refute account.assumable?
      end
    end

    describe 'when account_assumption_control arturo is OFF' do
      before { account.stubs(has_account_assumption_control?: false) }

      it 'is assumable' do
        assert account.assumable?
      end
    end

    describe 'when account is an assumable_account_type' do
      let(:account) { accounts(:trial) }

      it 'is assumable' do
        assert account.assumable?
      end
    end
  end

  describe '#assumable_account_type?' do
    describe 'multiproduct account' do
      let(:account) { accounts(:multiproduct) }
      let(:sell_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:sell_free_product, plan_settings: { billing_source: 'migrated_sell_recurly'}).with_indifferent_access) }
      let(:sell_trial_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:sell_free_product, plan_settings: { billing_source: ''}).with_indifferent_access) }
      let(:products) { [sell_product] }

      # as of now, multiproduct accounts that are CP4 chat-only shell accounts
      # ( without a support subscription ) do not seem to have subscription records.
      describe 'has sell product' do
        before do
          account.subscription.destroy
          account.reload
          assert_nil account.subscription
          response_body = { products: [sell_product] }.to_json
          stub_products_response(response_body, account)
        end

        describe 'when a sandbox' do
          before { account.stubs(is_sandbox?: true) }

          it 'it is not an assumable account type' do
            refute account.assumable_account_type?
          end
        end

        describe 'when not a sandbox' do
          describe 'has no biling_id' do
            before do
              account.stubs(is_sandbox?: false)
              account.stubs(billing_id: nil)
            end
            # since it's multiproduct, has nil billing_id, and not being a sandbox
            # and it's a sell product, it would imply that it's not assumable
            # account type.
            it 'it is not an assumable account type' do
              refute account.assumable_account_type?
            end
          end
          # for sell trial case billing_source is empty
          describe 'is a sell trial' do
            before do
              response_body = { products: [sell_trial_product] }.to_json
              stub_products_response(response_body, account)
              account.stubs(is_sandbox?: false)
              account.stubs(billing_id: nil)
              it 'is an assumable account type' do
                assert account.assumable_account_type?
              end
            end
          end
        end
      end

      describe 'no sell product' do
        before do
          response_body = { products: [] }.to_json
          stub_products_response(response_body, account)
        end

        describe 'with a billing_id' do
          before do
            account.stubs(billing_id: '123')
          end

          it 'is not an assumable account type' do
            refute account.assumable_account_type?
          end
        end

        describe 'with no billing_id' do
          before { assert_nil account.billing_id }

          it 'is an assumable account type' do
            assert account.assumable_account_type?
          end
        end
      end
    end

    describe 'non multiproduct account' do
      before { assert_equal false, account.multiproduct? }
      describe 'when a trial' do
        let(:account) { accounts(:trial) }

        it 'is an assumable account type' do
          assert account.assumable_account_type?
        end
      end
    end
  end

  describe '#always_assumable?' do
    describe 'when assumption_expiration is not set' do
      it 'is not assumable' do
        refute account.always_assumable?
      end
    end

    describe 'when assumption_expiration date is always assumable date of Jan 1, 2200' do
      before do
        account.settings.assumption_expiration = '22000101'.to_datetime
      end

      it 'is always assumable' do
        assert account.always_assumable?
      end
    end

    describe 'when assumption_expiration date is any other date' do
      before do
        account.settings.assumption_expiration = 1.year.from_now
      end

      it 'is not always assumable' do
        refute account.always_assumable?
      end
    end
  end

  def stub_products_response(response_body, account)
    stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
      to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
  end
end
