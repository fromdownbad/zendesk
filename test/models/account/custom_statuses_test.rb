require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::CustomStatuses do
  fixtures :accounts, :custom_statuses

  let(:account) { accounts(:minimum) }

  describe "#create_default_custom_statuses" do
    describe "for accounts without any custom statuses" do
      before { account.custom_statuses.destroy_all }

      it "creates the correct default custom statuses" do
        assert_difference 'CustomStatus.count', CustomStatus::CATEGORIES.size do
          account.create_default_custom_statuses
        end

        CustomStatus::CATEGORIES.each do |status_id|
          for_category = account.custom_statuses.for_status(status_id)
          assert_equal 1, for_category.size

          cs = for_category.first
          category = StatusType[status_id].name.to_s.downcase
          assert_equal "{{zd.status_#{category}}}", cs.agent_label
          assert_equal "{{zd.status_#{category}}}", cs.end_user_label
          assert_equal "{{zd.status_#{category}_description}}", cs.description
          assert_equal CustomStatus::POSITION_RANGES[status_id].first, cs.position
          assert cs.default?
          assert cs.active?
        end
      end
    end

    describe "for accounts with existing default custom statuses" do
      it "doesn't create extra custom statuses" do
        assert_difference 'CustomStatus.count', 0 do
          account.create_default_custom_statuses
        end
      end
    end

    describe "for accounts with some missing default custom statuses" do
      before do
        account.custom_statuses.for_status(StatusType.OPEN).delete_all
        assert_equal 0, account.custom_statuses.for_status(StatusType.OPEN).size
      end

      it "creates the missing custom statuses" do
        assert_difference 'CustomStatus.count', 1 do
          account.create_default_custom_statuses
        end
        assert_equal 1, account.custom_statuses.for_status(StatusType.OPEN).size
      end
    end
  end
end
