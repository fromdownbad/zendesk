require_relative "../../support/test_helper"
require_relative "../../support/suite_test_helper"
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered!

describe 'SecurityFeatureSupport' do
  include MultiproductTestHelper
  include SuiteTestHelper

  describe '#chat_product' do
    let(:products) { [support_product, chat_product] }
    let(:support_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_inactive_product).with_indifferent_access) }
    let(:chat_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:chat_inactive_product).with_indifferent_access) }
    let(:account) { accounts(:multiproduct) }

    before { Zendesk::Accounts::Client.any_instance.stubs(:products).returns(products) }

    it 'returns the chat product' do
      assert_equal chat_product, account.chat_product
    end
  end

  describe '#support_expired?' do
    let(:products) { [support_product] }
    let(:account) { accounts(:multiproduct) }

    before { Zendesk::Accounts::Client.any_instance.stubs(:products).returns(products) }

    describe 'support expired trial product' do
      let(:support_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_expired_trial_product).with_indifferent_access) }

      it 'returns true' do
        assert account.support_expired?
      end
    end

    describe 'support inactive product' do
      let(:support_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_inactive_product).with_indifferent_access) }

      it 'returns false' do
        refute account.support_expired?
      end
    end

    describe 'support active trial' do
      let(:support_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_trial_product).with_indifferent_access) }

      it 'returns false' do
        refute account.support_expired?
      end
    end

    describe 'no support product' do
      let(:account) { setup_shell_account }
      let(:chat_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:chat_inactive_product).with_indifferent_access) }
      let(:products) { [chat_product] }

      it 'returns false' do
        refute account.support_expired?
      end
    end
  end

  describe '#downgrade_support_trial_specific_support_features!' do
    let!(:account) { setup_shell_account_with_support }
    let(:feature_change_object) { mock('feature_change_object') }
    let(:pravda_chat_product) { FactoryBot.build(:chat_subscribed_product, plan_settings: { plan_type: ZBC::Zopim::PlanType::Professional.plan_type }) }
    let(:pravda_support_product) { FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: ZBC::Zendesk::PlanType::Enterprise.plan_type }) }

    before do
      response_body = { products: [pravda_support_product, pravda_chat_product] }.to_json
      stub_products_response(response_body, account)
    end

    describe 'with security_settings_for_all_accounts arturo off' do
      Account::SecurityFeatureSupport::SUPPORT_SPECIFIC_FEATURES.each do |feature|
        let(:feature_change_object) { mock('feature_change_object') }

        before do
          name = feature.to_s.camelize
          @klass = "Zendesk::Features::Change::#{name}Change".safe_constantize
          @klass.stubs(:new).with(account.subscription).returns(feature_change_object)
          feature_change_object.expects(:downgrade)
        end

        it "calls downgrade on #{@klass}" do
          account.downgrade_support_trial_specific_support_features!
        end

        it "sets account.has_#{feature}? to false" do
          assert account.subscription.send("has_#{feature}?")
          account.downgrade_support_trial_specific_support_features!

          refute account.subscription.send("has_#{feature}?")
        end
      end
    end

    [:saml, :jwt, :ip_restrictions, :custom_security_policy, :custom_session_timeout].each do |security_feature|
      describe 'with security_settings_for_all_accounts arturo on' do
        let(:feature_change_object) { mock('feature_change_object') }

        describe "for #{security_feature}" do
          before do
            name = security_feature.to_s.camelize
            Arturo.enable_feature!(:security_settings_for_all_accounts)
            @klass = "Zendesk::Features::Change::#{name}Change".safe_constantize
            @klass.stubs(:new).with(account.subscription).returns(feature_change_object)
          end

          it "does not call downgrade on #{@klass}" do
            feature_change_object.expects(:downgrade).never
            account.downgrade_support_trial_specific_support_features!
          end
        end
      end
    end
  end

  describe '#has_security_feature?' do
    describe 'non multiproduct account' do
      describe_with_arturo_enabled :central_admin_non_multiproduct do
        let(:account) { accounts(:minimum) }
        let(:plan_type) { nil }
        let(:pravda_support_product) { FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: plan_type }) }

        before do
          account.multiproduct = false
          stub_products_response({ products: [pravda_support_product] }.to_json, account)
        end

        describe 'with security_settings_for_all_accounts arturo on' do
          before { Arturo.enable_feature!(:security_settings_for_all_accounts) }

          it 'has ip restrictions available' do
            account.expects(:support_has_ip_restrictions?).never
            assert account.has_security_feature?(:ip_restrictions)
          end
        end

        [:saml, :jwt, :custom_security_policy, :custom_session_timeout].each do |security_feature|
          describe 'with security_settings_for_all_accounts arturo on' do
            before do
              Arturo.enable_feature!(:security_settings_for_all_accounts)
            end

            describe "with a subsciption present but #{security_feature} not available on the subscription" do
              before do
                assert account.subscription
                account.subscription.stubs("has_#{security_feature}?").returns(false)
              end

              it "still has #{security_feature} available" do
                assert account.has_security_feature?(security_feature)
              end
            end

            describe "with a subsciption present and #{security_feature} available on the subscription" do
              before do
                assert account.subscription
                account.subscription.stubs("has_#{security_feature}?").returns(true)
              end

              it "has #{security_feature} available" do
                assert account.has_security_feature?(security_feature)
              end
            end
          end

          describe 'with security_settings_for_all_accounts arturo off' do
            before do
              Arturo.disable_feature!(:security_settings_for_all_accounts)
            end

            describe "with a subsciption present but #{security_feature} not available on the subscription" do
              before do
                assert account.subscription
                account.subscription.stubs("has_#{security_feature}?").returns(false)
              end

              it "#{security_feature} is not available" do
                refute account.has_security_feature?(security_feature)
              end
            end

            describe "with a subsciption present and #{security_feature} available on the subscription" do
              before do
                assert account.subscription
                account.subscription.stubs("has_#{security_feature}?").returns(true)
              end

              it "has #{security_feature} available" do
                assert account.has_security_feature?(security_feature)
              end
            end
          end
        end

        describe 'account with patagonia pricing' do
          before do
            account.subscription.pricing_model_revision = ZBC::Zendesk::PricingModelRevision::PATAGONIA
          end

          describe 'professional plan' do
            let(:plan_type) { ZBC::Zendesk::PlanType::Professional.plan_type }

            it 'has ip_restrictions unavailable' do
              refute account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'enterprise plan' do
            let(:plan_type) { ZBC::Zendesk::PlanType::Enterprise.plan_type }

            it 'has ip_restrictions available' do
              assert account.has_security_feature?(:ip_restrictions)
            end
          end
        end

        describe 'account with legacy pricing' do
          before do
            account.subscription.pricing_model_revision = ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE
          end

          describe 'plus plan' do
            let(:plan_type) { ZBC::Zendesk::PlanType::Plus.plan_type }

            it 'has ip_restrictions available' do
              assert account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'enterprise plan' do
            let(:plan_type) { ZBC::Zendesk::PlanType::Enterprise.plan_type }

            it 'has ip_restrictions available' do
              assert account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'team plan' do
            let(:plan_type) { ZBC::Zendesk::PlanType::Team.plan_type }

            it 'has ip_restrictions unavailable' do
              refute account.has_security_feature?(:ip_restrictions)
            end
          end
        end
      end
    end

    describe 'multiproduct account' do
      let(:account) { accounts(:multiproduct) }
      let!(:pravda_chat_product) { FactoryBot.build(:chat_subscribed_product, plan_settings: { plan_type: nil }) }
      let!(:pravda_support_product) { FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: nil }) }

      describe 'Shell account without Support' do
        let!(:account) { setup_shell_account }
        let!(:zuora_support_product) { nil }

        describe 'only Chat' do
          before do
            response_body = { products: [pravda_chat_product] }.to_json
            stub_products_response(response_body, account)
          end
          describe 'without a subscription' do
            before { refute account.subscription.present? }

            describe 'with security_settings_for_all_accounts arturo on' do
              before { Arturo.enable_feature!(:security_settings_for_all_accounts) }

              it 'still has saml available' do
                assert account.has_security_feature?(:saml)
              end
            end

            describe 'with security_settings_for_all_accounts arturo off' do
              before { Arturo.disable_feature!(:security_settings_for_all_accounts) }

              it 'still has saml available' do
                refute account.has_security_feature?(:saml)
              end
            end
          end

          describe 'Chat is Enterprise in Pravda, no boosts' do
            let(:pravda_chat_product) { FactoryBot.build(:chat_subscribed_product, plan_settings: { plan_type: ZBC::Zopim::PlanType::Enterprise.plan_type }) }

            it 'ip_restriction available' do
              assert account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'inactive Chat Enterprise' do
            let(:pravda_chat_product) { FactoryBot.build(:chat_inactive_product, plan_settings: { plan_type: ZBC::Zopim::PlanType::Enterprise.plan_type }) }

            it 'ip_restriction not available' do
              refute account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'Chat is Trial, no boosts' do
            let(:pravda_chat_product) { FactoryBot.build(:chat_trial_product, plan_settings: { plan_type: ZBC::Zopim::PlanType::Trial.plan_type }) }

            it 'ip_restriction is available' do
              assert account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'Chat is Team, no boosts' do
            let(:pravda_chat_product) { FactoryBot.build(:chat_trial_product, plan_settings: { plan_type: ZBC::Zopim::PlanType::Team.plan_type }) }

            it 'ip_restriction not available' do
              refute account.has_security_feature?(:ip_restrictions)
            end

            describe 'with security_settings_for_all_accounts arturo on' do
              before { Arturo.enable_feature!(:security_settings_for_all_accounts) }

              it 'ip_restriction available' do
                assert account.has_security_feature?(:ip_restrictions)
              end
            end
          end

          describe 'Chat is Professional, active Enterprise boost' do
            let(:pravda_chat_product) do
              FactoryBot.build(
                :chat_subscribed_product,
                plan_settings: {
                  plan_type: ZBC::Zopim::PlanType::Professional.plan_type,
                  boosted_plan_type: ZBC::Zopim::PlanType::Enterprise.plan_type,
                  boost_expires_at: 2.days.from_now
                }
              )
            end

            it 'ip_restriction is available' do
              assert account.has_security_feature?(:ip_restrictions)
            end
          end
        end
      end

      describe 'Shell account with Support' do
        let!(:account) { setup_shell_account_with_support }

        describe 'only Support' do
          before do
            response_body = { products: [pravda_support_product] }.to_json
            stub_products_response(response_body, account)
          end

          describe 'Support is Professional, no boosts' do
            let(:pravda_support_product) { FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: ZBC::Zendesk::PlanType::Professional.plan_type }) }

            it 'ip_restriction not available' do
              refute account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'Support is Enterprise' do
            let(:pravda_support_product) { FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: ZBC::Zendesk::PlanType::Enterprise.plan_type }) }

            it 'ip_restriction is available' do
              assert account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'inactive Support Enterprise' do
            let(:pravda_support_product) { FactoryBot.build(:support_inactive_product, plan_settings: { plan_type: ZBC::Zendesk::PlanType::Enterprise.plan_type }) }

            it 'ip_restriction is not available' do
              refute account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'Support is Professional, boosted to Enterprise' do
            let(:pravda_support_product) do
              FactoryBot.build(
                :support_subscribed_product,
                plan_settings: {
                  plan_type: ZBC::Zendesk::PlanType::Professional.plan_type,
                }
              )
            end

            before do
              account.build_feature_boost(valid_until: 15.days.from_now).save!
              account.feature_boost.update_columns(boost_level: ZBC::Zendesk::PlanType::Enterprise.plan_type)
            end

            it 'ip_restriction is available' do
              assert account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'Support Team in Pravda, boosted to Professional' do
            let(:pravda_support_product) do
              FactoryBot.build(
                :support_subscribed_product,
                plan_settings: {
                  plan_type: ZBC::Zendesk::PlanType::Team.plan_type,
                }
              )
            end

            before do
              account.build_feature_boost(valid_until: 15.days.from_now).save!
              account.feature_boost.update_columns(boost_level: ZBC::Zendesk::PlanType::Professional.plan_type)
            end

            it 'ip_restriction not available' do
              refute account.has_security_feature?(:ip_restrictions)
            end
          end
        end
      end

      describe 'Shell account with Support and Chat' do
        let!(:account) { setup_shell_account_with_support }

        describe 'feature not implemented' do
          let(:feature) { :not_implemented }

          it 'logs the feature, account_id, account subdomain' do
            Rails.logger.expects(:info).with("feature: #{feature} not implemented for account - ID: #{account.id}, SUBDOMAIN: #{account.subdomain}")

            account.has_security_feature?(feature)
          end

          it 'returns false' do
            refute account.has_security_feature?(feature)
          end
        end

        describe 'both support and chat' do
          before do
            response_body = { products: [pravda_support_product, pravda_chat_product] }.to_json
            stub_products_response(response_body, account)
          end

          describe 'Support is Enterprise; Chat is Professional, no boosts' do
            let(:pravda_chat_product) { FactoryBot.build(:chat_subscribed_product, plan_settings: { plan_type: ZBC::Zopim::PlanType::Professional.plan_type }) }
            let(:pravda_support_product) { FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: ZBC::Zendesk::PlanType::Enterprise.plan_type }) }

            it 'ip_restriction available' do
              assert account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'when Pravda does not have the Support plan type' do
            let(:pravda_support_product) { FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: nil }) }

            describe 'Pravda does not have Chat plan type, no boosts' do
              let(:pravda_chat_product) { FactoryBot.build(:chat_subscribed_product, plan_settings: { plan_type: nil }) }

              it 'ip_restriction not available' do
                refute account.has_security_feature?(:ip_restrictions)
              end
            end
          end

          describe 'Chat is Team, Support is Team' do
            let(:pravda_chat_product) { FactoryBot.build(:chat_subscribed_product, plan_settings: { plan_type: ZBC::Zopim::PlanType::Team.plan_type }) }
            let(:pravda_support_product) { FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: ZBC::Zendesk::PlanType::Team.plan_type }) }

            it 'ip_restriction not available' do
              refute account.has_security_feature?(:ip_restrictions)
            end
          end

          describe 'Chat is Professional, Support is Professional,' do
            let(:pravda_chat_product) { FactoryBot.build(:chat_subscribed_product, plan_settings: { plan_type: ZBC::Zopim::PlanType::Professional.plan_type }) }
            let(:pravda_support_product) { FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: ZBC::Zendesk::PlanType::Professional.plan_type }) }

            describe 'no boosts' do
              it 'ip_restriction not available' do
                refute account.has_security_feature?(:ip_restrictions)
              end
            end

            describe 'Support boosted to Enterprise' do
              let(:pravda_support_product) do
                FactoryBot.build(
                  :support_subscribed_product,
                  plan_settings: {
                    plan_type: ZBC::Zendesk::PlanType::Professional.plan_type,
                  }
                )
              end

              before do
                account.build_feature_boost(valid_until: 15.days.from_now).save!
                account.feature_boost.update_columns(boost_level: ZBC::Zendesk::PlanType::Enterprise.plan_type)
              end

              it 'ip_restriction is available' do
                assert account.has_security_feature?(:ip_restrictions)
              end
            end
          end
        end
      end
    end
  end

  def stub_products_response(response_body, account)
    stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
      to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
  end
end
