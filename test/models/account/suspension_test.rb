require_relative "../../support/test_helper"
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered! uncovered: 17

describe 'AccountSuspension' do
  fixtures :all

  include MultiproductTestHelper

  let(:account) { accounts(:minimum) }
  let(:multiproduct_account) { setup_shell_account }

  describe "#disable_features!" do
    before { @account = accounts(:minimum) }
    it "is called when the account iscomes suspended" do
      @account.stubs(:is_serviceable_by_billing_state?).returns(false)
      @account.expects(:disable_features!)
      @account.update_attributes!(is_serviceable: false)
    end

    it "does not be called when the account is does not suspended" do
      @account.expects(:disable_features!).never
      @account.update_attributes!(is_serviceable: true)
    end

    it "destroys the GoodData integration" do
      Zendesk::Gooddata::IntegrationProvisioning.expects(:destroy_for_account).with(@account)
      @account.update_attributes!(is_serviceable: false)
    end

    describe "when the account has a certificate" do
      let(:certificate) do
        c = @account.certificates.create
        c.state = 'active'
        c.save
        c
      end

      before do
        assert_equal 'active', certificate.state
      end

      it "revokes active certificates" do
        @account.update_attributes!(is_serviceable: false)
        certificate.reload
        assert_empty @account.certificates.active
        assert_equal 'revoked', certificate.state
      end
    end

    describe "with monitored twitter accounts" do
      it "deactivate!" do
        num_mths = MonitoredTwitterHandle.where(account_id: @account.id).count(:all)
        assert num_mths > 0
        num_mths.times do
          MonitoredTwitterHandle.any_instance.expects(:deactivate!)
        end
        @account.stubs(:is_serviceable_by_billing_state?).returns(false)
        @account.update_attributes!(is_serviceable: false)
      end
    end
  end

  describe "#reactivate!" do
    before do
      @account = accounts(:minimum)
    end

    it "calls cancel! for subscription" do
      @account.expects(:set_use_feature_framework_persistence).with(true)
      Zendesk::Features::SubscriptionFeatureService.any_instance.expects(:execute!)
      @account.reactivate!
    end

    describe 'when multiproduct account' do
      it 'reactivates account' do
        multiproduct_account.reactivate!
        assert(multiproduct_account.is_active)
        assert_equal false, multiproduct_account.is_serviceable
      end
    end

    describe 'when subscribed multiproduct account' do
      before do
        multiproduct_account.stubs(:billing_id).returns('billingid')
      end

      it 'reactivates' do
        multiproduct_account.reactivate!
        assert(multiproduct_account.is_active)
        assert(multiproduct_account.is_serviceable)
      end
    end
  end

  describe "#cancel!" do
    before do
      account.host_mapping = "foo"
      @subdomain = account.subdomain
    end

    it 'calls destroy_sandbox' do
      account.expects(:destroy_sandbox)
      account.cancel!
    end

    it "calls cancel! for subscription" do
      Subscription.any_instance.expects(:cancel!)
      account.cancel!
    end

    describe "when account has a G Suite domain" do
      before do
        account.route.update_attribute(:gam_domain, 'foo.com')
      end

      it "deletes the G Suite domain from the account route" do
        account.cancel!
        refute account.route.gam_domain
      end
    end

    describe "A non Zuora managed account" do
      before do
        account.subscription.expects(:cancel!)
      end

      it "deactivates the account" do
        account.cancel!
        assert_equal false, account.is_active
        assert_equal false, account.is_serviceable
      end

      it "clears the host mapping" do
        account.cancel!
        assert_nil account.host_mapping
      end
    end

    describe "when cancel! raises Exception" do
      before do
        Subscription.any_instance.expects(:cancel!).raises(ZBC::Zuora::Errors::InvalidConfiguration)
      end

      it "logs exception in zuora logs" do
        ZUORA_LOG.expects(:warn)
        assert_raises ZBC::Zuora::Errors::InvalidConfiguration do
          account.cancel!
        end
      end
    end

    describe "a zuora managed account" do
      before do
        Subscription.any_instance.stubs(:zuora_managed?).returns(true)
        @zuora_subscription = account.subscription.build_zuora_subscription(
          pricing_model_revision: account.subscription.pricing_model_revision
        )
        Subscription.any_instance.stubs(:zuora_subscription).returns(@zuora_subscription)

        @zuora_subscription.expects(:cancel!).returns(true)
      end

      it "calls #cancel! on the Zuora subscription" do
        assert account.cancel!
      end
    end

    describe 'when multiproduct account' do
      it "cancels account" do
        assert multiproduct_account.cancel!
        refute multiproduct_account.reload.is_active
        refute multiproduct_account.reload.is_serviceable
      end
    end
  end

  describe "#is_serviceable_by_billing_state？" do
    describe "when zuora managed" do
      let(:account) { accounts(:minimum) }

      before do
        account.expects(:zuora_managed?).returns(true)
      end

      it "doesn`t check dunning state and return true" do
        ZendeskBillingCore::States::Dunning.any_instance.expects(:current_state).never
        assert account.is_serviceable_by_billing_state?
      end
    end
  end

  describe '#update_account_suspension_state' do
    before { stubs(skip_update_account_suspension_state?: false) }

    describe 'when multiproduct account' do
      it { assert multiproduct_account.update_account_suspension_state }
    end
  end

  describe '#skip_update_account_suspension_state?' do
    let(:account) { accounts(:minimum) }

    describe 'on test-env' do
      it { assert account.send(:skip_update_account_suspension_state?) }
    end
  end
end
