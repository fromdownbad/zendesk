require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Account::BccArchiving' do
  fixtures :accounts, :subscriptions

  describe Account do
    before do
      @account = accounts(:minimum)
    end

    it "ensures the BCC archive address is valid" do
      @account.settings.bcc_archive_address = 'mentor@example.com'
      assert @account.valid?

      @account.settings.bcc_archive_address = 'example.com'
      refute @account.valid?
      assert_equal ['Bcc archive address:  is invalid'], @account.errors.full_messages
    end

    describe "with BCC archiving" do
      before do
        @account.subscription.expects(:has_bcc_archiving?).times(3).returns(true)
      end

      it "has BCC archiving enabled when the archive email address is present" do
        assert_equal false, @account.has_bcc_archiving_enabled?

        @account.settings.bcc_archive_address = ''
        @account.save!
        assert_equal false, @account.has_bcc_archiving_enabled?

        @account.settings.bcc_archive_address = 'mentor@example.com'
        @account.save!
        assert(@account.has_bcc_archiving_enabled?)
      end
    end

    describe "without BCC archiving" do
      before do
        assert_equal false, @account.subscription.has_bcc_archiving?
      end

      it "not has BCC archiving enabled when the archive email address is present" do
        assert_equal false, @account.has_bcc_archiving_enabled?
        @account.settings.bcc_archive_address = 'mentor@example.com'
        @account.save!
        assert_equal false, @account.has_bcc_archiving_enabled?
      end
    end
  end
end
