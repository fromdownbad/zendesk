require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::AttachmentSupport do
  fixtures :accounts

  describe_with_arturo_enabled :email_increase_attachment_size_limit do
    describe "#max_attachment_size" do
      describe "when the account has a subscription" do
        let(:account) { accounts(:minimum) }
        let(:subscription_size_limit) { account.subscription.file_upload_cap.megabytes }

        it "returns the subscription `file_upload_cap`" do
          assert_equal subscription_size_limit, account.max_attachment_size
        end
      end

      describe "when the account has no subscription" do
        let(:account_no_subscription) { Account.new }
        let(:attachment_fu_size_limit) { Attachment.attachment_options[:max_size] }

        it "returns size_limit from attachment_fu max_size config" do
          assert_equal attachment_fu_size_limit, account_no_subscription.max_attachment_size
        end
      end
    end

    describe "#max_attachment_megabytes" do
      describe "when the account has a subscription" do
        let(:account) { accounts(:minimum) }
        let(:subscription_size_limit_megabytes) { account.subscription.file_upload_cap }

        it "returns the subscription `file_upload_cap` in megabytes" do
          assert_equal subscription_size_limit_megabytes, account.max_attachment_megabytes
        end
      end

      describe "when the account has no subscription" do
        let(:account_no_subscription) { Account.new }
        let(:attachment_fu_size_limit_megabytes) { Attachment.attachment_options[:max_size] / 1.megabytes }

        it "returns size_limit from attachment_fu max_size config in megabytes" do
          assert_equal attachment_fu_size_limit_megabytes, account_no_subscription.max_attachment_megabytes
        end
      end
    end
  end

  # TODO: Remove this block of tests when removing `email_increase_attachment_size_limit`
  describe_with_arturo_disabled :email_increase_attachment_size_limit do
    let(:account) { accounts(:minimum) }
    let(:config_size_limit_bytes) { Attachment.attachment_options[:max_size] }
    let(:config_size_limit_megabytes) { config_size_limit_bytes / 1.megabytes }
    let(:larger_file_upload_cap_megabytes) { 1025 }
    let(:smaller_file_upload_cap_megabytes) { 1023 }

    describe "#max_attachment_size" do
      describe "when account has subscription" do
        describe "and file_upload_cap is larger" do
          before do
            account.subscription.stubs(:file_upload_cap).returns(larger_file_upload_cap_megabytes)
          end

          it "returns size_limit from attachment_fu max_size config" do
            assert_equal config_size_limit_bytes, account.max_attachment_size
          end
        end

        describe "and file_upload_cap is smaller" do
          before do
            account.subscription.stubs(:file_upload_cap).returns(smaller_file_upload_cap_megabytes)
          end

          it "returns file_upload_cap of subscription" do
            assert_equal smaller_file_upload_cap_megabytes.megabytes, account.max_attachment_size
          end
        end
      end

      describe "when account has no subscription" do
        let(:account) { Account.new }

        it "returns size_limit from attachment_fu max_size config" do
          assert_equal config_size_limit_bytes, account.max_attachment_size
        end
      end
    end

    describe "#max_attachment_megabytes" do
      describe "when account has a subscription" do
        describe "and file_upload_cap is larger" do
          before do
            account.subscription.stubs(:file_upload_cap).returns(larger_file_upload_cap_megabytes)
          end

          it "returns size_limit from attachment_fu max_size config in megabytes" do
            assert_equal config_size_limit_megabytes, account.max_attachment_megabytes
          end
        end

        describe "and file_upload_cap is smaller" do
          before do
            account.subscription.stubs(:file_upload_cap).returns(smaller_file_upload_cap_megabytes)
          end

          it "returns file_upload_cap of subscription in megabytes" do
            assert_equal smaller_file_upload_cap_megabytes, account.max_attachment_megabytes
          end
        end
      end

      describe "when account has no subscription" do
        let(:account) { Account.new }

        it "returns size_limit from attachment_fu max_size config in megabytes" do
          assert_equal config_size_limit_megabytes, account.max_attachment_megabytes
        end
      end
    end
  end
end
