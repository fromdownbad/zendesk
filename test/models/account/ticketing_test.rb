require_relative "../../support/test_helper"
require_relative "../../support/rule"

SingleCov.covered! uncovered: 39

describe Account::Ticketing do
  include TestSupport::Rule::Helper

  fixtures :accounts, :tickets, :ticket_fields

  let(:account) { accounts(:minimum) }
  let(:ticket1) { tickets(:minimum_1) }
  let(:ticket2) { tickets(:minimum_2) }

  describe "#deleted_tickets" do
    before do
      ticket1.update_column(:status_id, StatusType.DELETED)
      Timecop.travel(31.days.ago) do
        ticket2.will_be_saved_by(User.system)
        ticket2.soft_delete!
      end
    end

    describe_with_arturo_disabled :extend_recoverable_ticket_age do
      it "shows tickets that have been deleted in the last 30 days" do
        assert_includes Ticket.with_deleted { account.deleted_tickets.to_a }, ticket1
      end

      it "does not show tickets that have been deleted for more than 30 days" do
        refute_includes Ticket.with_deleted { account.deleted_tickets.to_a }, ticket2
      end
    end

    describe_with_arturo_enabled :extend_recoverable_ticket_age do
      it "shows tickets that have been deleted in the last 60 days" do
        assert_equal 60, account.settings.extended_recoverable_ticket_age
        assert_includes Ticket.with_deleted { account.deleted_tickets.to_a }, ticket1
        assert_includes Ticket.with_deleted { account.deleted_tickets.to_a }, ticket2
      end
    end
  end

  describe '#ticket_fields' do
    it 'does not eagerly pre-query the base association' do
      assert_sql_queries 1, %r{SELECT .* FROM `ticket_fields`} do
        account.ticket_fields.active.to_a
      end
    end
  end

  describe '#ticket_fields_by_ids' do
    let!(:ids) { [ticket_fields(:field_tagger_custom).id] }
    describe 'when collection is pre-loaded in memory' do
      it 'returns a hash without querying SQL' do
        account.ticket_fields(include_voice_insights: true).to_a
        assert_sql_queries 0 do
          account.ticket_fields_by_ids(ids)
        end
      end
    end

    describe 'when collection is not loaded' do
      it 'queries SQL just for the requested ids' do
        assert_sql_queries 1, %r{SELECT .* FROM `ticket_fields` WHERE.*`ticket_fields`.`id` =} do
          account.ticket_fields_by_ids(ids)
        end
      end
    end
  end

  describe '#fetch_active_triggers' do
    let(:cache_key) { "#{account.scoped_cache_key(:triggers)}-active-triggers" }
    let(:active_triggers) { account.fetch_active_triggers }

    before do
      RuleCategory.destroy_all
      Trigger.destroy_all

      @category_1 = create_category(position: 1)
      @category_2 = create_category(position: 2)

      @trigger_1 = create_trigger(position: 5, rules_category_id: @category_1.id)
      @trigger_2 = create_trigger(position: 6, rules_category_id: @category_1.id)
      @trigger_3 = create_trigger(position: 7, rules_category_id: @category_1.id)

      @trigger_4 = create_trigger(position: 1, rules_category_id: @category_2.id)
      @trigger_5 = create_trigger(position: 2, rules_category_id: @category_2.id)
      @trigger_6 = create_trigger(position: 3, rules_category_id: @category_2.id)
      @trigger_7 = create_trigger(title: 'A', position: 4, rules_category_id: @category_2.id)
    end

    describe_with_arturo_enabled :skip_triggers_caching do
      it 'never tries to write to the cache' do
        Rails.cache.expects(:fetch).once
        active_triggers
      end

      it 'returns active triggers ordered by position' do
        assert_equal(
          [@trigger_4, @trigger_5, @trigger_6, @trigger_7, @trigger_1, @trigger_2, @trigger_3],
          active_triggers
        )
      end
    end

    describe_with_arturo_disabled :skip_triggers_caching do
      describe 'when the cache has triggers and is valid' do
        before { Rails.cache.write(cache_key, [@trigger_1]) }

        it 'returns the triggers in the cache' do
          assert_equal active_triggers, [@trigger_1]
        end
      end

      describe 'when the cache does not have active triggers' do
        before { Rails.cache.delete(cache_key) }

        describe_with_arturo_setting_disabled :trigger_categories_api do
          it 'returns triggers by trigger position values' do
            assert_equal(
              [@trigger_4, @trigger_5, @trigger_6, @trigger_7, @trigger_1, @trigger_2, @trigger_3],
              active_triggers
            )
          end

          it 'queries the rules table once' do
            assert_sql_queries 1 do
              active_triggers
            end
          end
        end

        describe_with_arturo_setting_enabled :trigger_categories_api do
          it 'returns triggers by category position, trigger position, and title' do
            @trigger_8 = create_trigger(title: 'B', position: 4, rules_category_id: @category_2.id)

            assert_equal(
              [@trigger_1, @trigger_2, @trigger_3, @trigger_4, @trigger_5, @trigger_6, @trigger_7, @trigger_8],
              active_triggers
            )
          end

          it 'queries the rules table once' do
            assert_sql_queries 1, %r{SELECT `rules`.*} do
              active_triggers
            end
          end
        end
      end
    end
  end
end
