require_relative "../../support/multiproduct_test_helper"
require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::PatagoniaApiRateLimiter do
  include MultiproductTestHelper

  fixtures :accounts

  before { @account = accounts(:minimum) }

  describe "#api_rate_limit" do
    describe "api rate limit boosted" do
      before do
        @account.stubs(api_rate_limit_boosted?: true)
      end

      it "is 2500" do
        assert_equal 2500, @account.api_rate_limit
      end
    end

    describe "account has api rate limit setting" do
      before do
        @account.settings.expects(api_rate_limit: 2500).at_least_once
      end

      it "is based on the account setting" do
        assert_equal 2500, @account.api_rate_limit
      end
    end

    describe "account is both boosted and has api rate limit setting > 2500" do
      before do
        @account.stubs(api_rate_limit_boosted?: true)
        @account.settings.expects(api_rate_limit: 5000).at_least_once
      end

      it "is based on the account setting" do
        assert_equal 5000, @account.api_rate_limit
      end
    end

    describe "account is both boosted and has api rate limit setting > 2500" do
      before do
        @account.stubs(api_rate_limit_boosted?: true)
        @account.settings.expects(api_rate_limit: 900).at_least_once
      end

      it "is based on the boost" do
        assert_equal 2500, @account.api_rate_limit
      end
    end

    describe "account has no api rate limit setting" do
      it "uses default value" do
        @account.expects(:default_api_rate_limit)
        @account.api_rate_limit
      end
    end
  end

  describe "#default_api_rate_limit" do
    describe "when the account has_high_volume_api" do
      before do
        subscription_settings = { has_high_volume_api?: true }
        @account.subscription.stubs(subscription_settings)
      end

      it "is 2500" do
        assert_equal 2500, @account.default_api_rate_limit
      end
    end

    describe "when the account has_api_limit_700_rpm_legacy" do
      before do
        subscription_settings = {
          has_high_volume_api?: false,
          has_api_limit_700_rpm_legacy?: true
        }
        @account.subscription.stubs(subscription_settings)
      end

      it "is 700" do
        assert_equal 700, @account.default_api_rate_limit
      end
    end

    describe "when the account has_api_limit_200_rpm" do
      before do
        subscription_settings = {
          has_high_volume_api?: false,
          has_api_limit_700_rpm_legacy?: false,
          has_api_limit_700_rpm?: false,
          has_api_limit_400_rpm?: false,
          has_api_limit_200_rpm?: true
        }
        @account.subscription.stubs(subscription_settings)
      end

      it "is 200" do
        assert_equal 200, @account.default_api_rate_limit
      end
    end

    describe "when the account has_api_limit_400_rpm" do
      before do
        subscription_settings = {
          has_high_volume_api?: false,
          has_api_limit_700_rpm_legacy?: false,
          has_api_limit_700_rpm?: false,
          has_api_limit_400_rpm?: true
        }
        @account.subscription.stubs(subscription_settings)
      end

      it "is 400" do
        assert_equal 400, @account.default_api_rate_limit
      end
    end

    describe "when the account has_api_limit_700_rpm" do
      before do
        subscription_settings = {
          has_high_volume_api?: false,
          has_api_limit_700_rpm_legacy?: false,
          has_api_limit_700_rpm?: true
        }
        @account.subscription.stubs(subscription_settings)
      end

      it "is 700" do
        assert_equal 700, @account.default_api_rate_limit
      end
    end

    describe "when the account is on Bronze plan" do
      before do
        subscription_settings = {
          has_high_volume_api?: false,
          has_api_limit_700_rpm_legacy?: false,
          has_api_limit_700_rpm?: false,
          has_api_limit_400_rpm?: false,
          has_api_limit_200_rpm?: false
        }
        @account.subscription.stubs(subscription_settings)
      end

      it "is 10" do
        assert_equal 10, @account.default_api_rate_limit
      end
    end

    describe "shell account without Support product" do
      let(:shell_account) { setup_shell_account }

      it 'returns default rate limit' do
        assert_equal Account::PatagoniaApiRateLimiter::DEFAULT_RATE_LIMIT, shell_account.default_api_rate_limit
      end
    end
  end

  describe "#can_be_boosted?" do
    describe "legacy account" do
      before do
        @account.subscription.stubs(has_api_limit_700_rpm_legacy?: true)
      end

      it "returns true" do
        assert @account.can_be_boosted?
      end
    end

    describe "has high volume api add-on" do
      before do
        @account.subscription.stubs(has_high_volume_api?: true)
      end

      it "returns true" do
        assert @account.can_be_boosted?
      end
    end
  end

  describe '#default_api_incremental_exports_rate_limit' do
    it 'returns default api_incremental_exports_rate_limit' do
      expected_value = @account.settings.default(:api_incremental_exports_rate_limit)
      assert expected_value > 0

      assert_equal expected_value, @account.default_api_incremental_exports_rate_limit
    end

    it 'returns INCREMENTAL_EXPORT_REQS_HIGH_VOLUME_ADD_ON if has high volume addon' do
      @account.subscription.stubs(has_high_volume_api?: true)
      expected_value = Account::PatagoniaApiRateLimiter::INCREMENTAL_EXPORT_REQS_HIGH_VOLUME_ADD_ON
      assert_equal expected_value, @account.default_api_incremental_exports_rate_limit
    end
  end

  describe '#api_incremental_exports_rate_limit' do
    it 'returns api_incremental_exports_rate_limit setting' do
      @account.settings.api_incremental_exports_rate_limit = 42
      assert_equal false, @account.subscription.has_high_volume_api?

      assert_equal 42, @account.api_incremental_exports_rate_limit
    end

    describe 'with has_high_volume_api' do
      before do
        @account.subscription.stubs(has_high_volume_api?: true)
      end

      it 'returns INCREMENTAL_EXPORT_REQS_HIGH_VOLUME_ADD_ON if setting value is lower' do
        constant = Account::PatagoniaApiRateLimiter::INCREMENTAL_EXPORT_REQS_HIGH_VOLUME_ADD_ON
        @account.settings.api_incremental_exports_rate_limit = constant - 1

        assert_equal constant, @account.api_incremental_exports_rate_limit
      end

      it 'returns setting value if greater than INCREMENTAL_EXPORT_REQS_HIGH_VOLUME_ADD_ON' do
        constant = Account::PatagoniaApiRateLimiter::INCREMENTAL_EXPORT_REQS_HIGH_VOLUME_ADD_ON
        val = @account.settings.api_incremental_exports_rate_limit = constant + 1

        assert_equal val, @account.api_incremental_exports_rate_limit
      end
    end
  end
end
