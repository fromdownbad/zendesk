require_relative '../support/test_helper'

SingleCov.covered!

describe TicketSchedule do
  fixtures :accounts,
    :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }

  let!(:schedule_1) do
    account.schedules.create(name: 'schedule_1', time_zone: account.time_zone)
  end
  let!(:schedule_2) do
    account.schedules.create(name: 'schedule_2', time_zone: account.time_zone)
  end

  before do
    ticket.create_ticket_schedule(account: account, schedule: schedule_1)
  end

  should validate_uniqueness_of(:ticket_id)

  describe 'when in memory only' do
    before do
      ticket.build_ticket_schedule(account: account, schedule: schedule_2)
    end

    it 'returns the correct schedule' do
      assert_equal schedule_2, ticket.schedule
    end
  end

  describe 'when there is no assignment' do
    before { ticket.ticket_schedule.destroy }

    it 'returns the account schedule' do
      assert_equal account.schedule, ticket.reload.schedule
    end
  end
end
