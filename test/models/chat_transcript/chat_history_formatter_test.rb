require_relative '../../support/test_helper'

SingleCov.covered!

describe ChatTranscript::ChatHistoryFormatter do
  let(:timestamp) { 1583403564_000 }
  let(:history) do
    [
      {
        type: 'ChatJoin',
        actor_name: 'mr smith',
        actor_type: 'end-user',
        timestamp: timestamp
      },
      {
        type: 'ChatMessage',
        actor_name: 'mr smith',
        actor_type: 'end-user',
        message: 'all your base',
        timestamp: timestamp,
      },
      {
        type: 'ChatJoin',
        actor_name: 'zero',
        actor_type: 'agent',
        timestamp: timestamp
      },
      {
        type: 'ChatMessage',
        actor_name: 'zero',
        actor_type: 'agent',
        message: 'what you say?',
        timestamp: timestamp
      },
      {
        type: 'ChatNameChanged',
        actor_type: 'end-user',
        previous: 'mr smith',
        current: 'cats',
        timestamp: timestamp
      },
      {
        type: 'ChatMessage',
        actor_name: 'cats',
        actor_type: 'end-user',
        message: 'you have no chance to survive, make your time',
        timestamp: timestamp
      },
      {
        actor_name: "cats",
        timestamp: timestamp,
        actor_type: 'end-user',
        type: "ChatFileAttachment",
        filename: 'the-bomb.png',
        mime_type: 'image/png',
        size: 36496,
        url: 'https://example.org/the-bomb.png'
      },
      {
        type: 'ChatLeave',
        actor_name: 'cats',
        actor_type: 'end-user',
        timestamp: timestamp
      }
    ].map &:with_indifferent_access
  end

  let(:history_with_translation) do
    [
      {
        type: 'ChatJoin',
        actor_name: 'mr smith',
        actor_type: 'end-user',
        timestamp: timestamp
      },
      {
        type: 'ChatMessage',
        actor_name: 'mr smith',
        actor_type: 'end-user',
        message: 'bonjour',
        timestamp: timestamp,
      },
      {
        type: 'ChatJoin',
        actor_name: 'agent zero',
        actor_type: 'agent',
        timestamp: timestamp
      },
      {
        type: 'ChatMessage',
        actor_name: 'agent zero',
        actor_type: 'agent',
        message: 'hello',
        translation: {
          msg: 'bonjour',
          from: 'en',
          to: 'fr'
        },
        timestamp: timestamp
      },
      {
        type: 'ChatMessage',
        actor_name: 'mr smith',
        actor_type: 'end-user',
        message: 'I can speak English actually',
        timestamp: timestamp
      },
      {
        type: 'ChatMessage',
        actor_name: 'agent zero',
        actor_type: 'agent',
        message: 'ok, I have turned off the auto-translation',
        timestamp: timestamp
      },
      {
        type: 'ChatLeave',
        actor_name: 'mr smith',
        actor_type: 'end-user',
        timestamp: timestamp
      }
    ].map &:with_indifferent_access
  end

  it 'formats chat messages' do
    formatter = ChatTranscript::ChatHistoryFormatter.new(history)
    expected = <<~CHAT.chomp
      (10:19:24 AM) *** mr smith joined the chat ***
      (10:19:24 AM) mr smith: all your base
      (10:19:24 AM) zero: what you say?
      (10:19:24 AM) mr smith is now known as cats.
      (10:19:24 AM) cats: you have no chance to survive, make your time
      (10:19:24 AM) cats uploaded: the-bomb.png
      URL: https://example.org/the-bomb.png
      Type: image/png
      Size: 36496
      (10:19:24 AM) *** cats left the chat ***
    CHAT
    assert_equal expected, formatter.to_s
  end

  it 'formats chat messages with translation' do
    formatter = ChatTranscript::ChatHistoryFormatter.new(history_with_translation)
    expected = <<~CHAT.chomp
      (10:19:24 AM) *** mr smith joined the chat ***
      (10:19:24 AM) mr smith: bonjour
      (10:19:24 AM) agent zero: hello
      (10:19:24 AM) agent zero: bonjour
      (10:19:24 AM) mr smith: I can speak English actually
      (10:19:24 AM) agent zero: ok, I have turned off the auto-translation
      (10:19:24 AM) *** mr smith left the chat ***
    CHAT
    assert_equal expected, formatter.to_s
  end
end
