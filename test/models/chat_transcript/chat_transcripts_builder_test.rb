require_relative '../../support/test_helper'

SingleCov.covered!

describe ChatTranscript::ChatTranscriptsBuilder do
  let(:chat_messages) { ['hello there', 'can you hear me?', 'talk to me!'] }

  def history_chat_message(message)
    {
      type: 'ChatMessage',
      actor_id: '123',
      actor_type: 'agent',
      actor_name: 'mr smith',
      timestamp: Time.zone.now.to_i,
      message: message
    }
  end

  def web_path(url, title)
    {
      url: url,
      title: title,
      timestamp: Time.zone.now.to_i
    }
  end

  def all_transcript_resources(chat_transcripts, resource, field)
    chat_transcripts.map do |transcript|
      transcript.public_send(resource).map { |h| h[field] }
    end.flatten
  end

  def all_chat_messages(chat_transcripts)
    all_transcript_resources(chat_transcripts, :chat_history, :message)
  end

  describe 'without a limit' do
    it 'builds a single transcript' do
      result = ChatTranscript::ChatTranscriptsBuilder.build(account_id: 1, audit_id: 1, ticket_id: 1) do |builder|
        builder.add_chat_history chat_messages.map(&method(:history_chat_message))
      end
      assert_equal 1, result.size
      assert_equal chat_messages, all_chat_messages(result)
    end
  end

  describe 'with a limit' do
    describe 'with a very low limit' do
      it 'builds one transcript for each chat message' do
        # 50 bytes is less than a single decoded transcript item (~ 130 bytes)
        ChatTranscript.stubs(:value_limit).returns(50.bytes)
        result = ChatTranscript::ChatTranscriptsBuilder.
          build(account_id: 1, audit_id: 1, ticket_id: 1) do |builder|
            builder.add_chat_history chat_messages.map(&method(:history_chat_message))
          end
        assert_equal 3, result.size
        assert_equal chat_messages, all_chat_messages(result)
      end
    end

    describe 'with a higher limit' do
      before { ChatTranscript.stubs(:value_limit).returns(320.bytes) }

      it 'builds two transcripts' do
        result = ChatTranscript::ChatTranscriptsBuilder.build(account_id: 1, audit_id: 1, ticket_id: 1) do |builder|
          builder.add_chat_history chat_messages.map(&method(:history_chat_message))
        end
        assert_equal 2, result.size
        assert_equal chat_messages, all_chat_messages(result)
      end

      it 'takes web paths into account' do
        result = ChatTranscript::ChatTranscriptsBuilder.build(account_id: 1, audit_id: 1, ticket_id: 1) do |builder|
          builder.add_chat_history [history_chat_message('hello')]
          builder.add_web_paths [
            web_path('https://example.org/path', 'x' * 50),
            web_path('https://example.org/path2', 'y' * 50)
          ]
        end
        assert_equal 2, result.size
        assert_equal ['hello'], all_chat_messages(result)
        assert_equal ['x' * 50, 'y' * 50], all_transcript_resources(result, :web_paths, :title)
      end

      it 'uses byte size instead of character count' do
        result = ChatTranscript::ChatTranscriptsBuilder.build(account_id: 1, audit_id: 1, ticket_id: 1) do |builder|
          # base size of each message is 120 + 1
          # split across two messages, that requires 39 bytes (or 10 emoji of 4 bytes each)
          builder.add_chat_history [history_chat_message('🔥' * 10)] * 2
        end
        assert_equal 2, result.size
      end
    end
  end
end
