require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe TicketPrediction do
  fixtures :tickets, :users, :accounts

  describe 'update_satisfaction_probability' do
    let(:ticket) { tickets(:minimum_1) }

    it 'does nothing when new_probability is blank' do
      assert_nil TicketPrediction.update_satisfaction_probability(ticket, nil)
    end

    it 'records changes to ticket probability' do
      new_probability = 0.9
      TicketPrediction.update_satisfaction_probability(ticket, new_probability)

      assert ticket.satisfaction_probability_delta_changed?
    end

    it 'does not record a change if the new probability matches the current' do
      probability = 0.920938217498
      ticket.satisfaction_probability = probability
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!

      TicketPrediction.update_satisfaction_probability(ticket, probability)

      refute ticket.satisfaction_probability_delta_changed?
    end

    it 'rounds the given probability to match the database truncation' do
      probability = 0.920938217498
      ticket.satisfaction_probability = probability
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!

      TicketPrediction.update_satisfaction_probability(ticket, probability)

      assert_equal probability.round(6), Ticket.find(ticket.id).satisfaction_probability
    end
  end

  describe 'current_satisfaction_probability' do
    let(:ticket) { tickets(:minimum_1) }

    it 'returns nil if a ticket prediction does not exist' do
      assert_nil TicketPrediction.current_satisfaction_probability(ticket)
    end

    it 'returns a satisfaction_probability value if a ticket prediction exists' do
      probability = 0.123456
      ticket.satisfaction_probability = probability
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!

      assert_equal probability, TicketPrediction.current_satisfaction_probability(ticket)
    end
  end

  describe 'satisfaction_prediction_model_available?' do
    let(:account) { accounts(:minimum) }
    let!(:api_client) { Zendesk::SatisfactionPredictionApp::InternalApiClient.new(account.subdomain) }

    before do
      Zendesk::SatisfactionPredictionApp::InternalApiClient.stubs(:new).returns(api_client)
    end

    it 'returns true if satisfaction prediction model is available' do
      api_client.stubs(:fetch_satisfaction_prediction_available).returns(true)

      assert TicketPrediction.satisfaction_prediction_model_available? account
    end

    it 'returns false if satisfaction prediction model is missing' do
      api_client.stubs(:fetch_satisfaction_prediction_available).returns(false)

      refute TicketPrediction.satisfaction_prediction_model_available? account
    end

    it 'returns false if unable to call batch analytics api to check on the existence of the prediction model' do
      api_client.stubs(:fetch_satisfaction_prediction_available).raises(RuntimeError, "network_error")

      refute TicketPrediction.satisfaction_prediction_model_available? account
    end
  end
end
