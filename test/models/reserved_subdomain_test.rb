require_relative "../support/test_helper"

SingleCov.covered!

describe ReservedSubdomain do
  fixtures :reserved_subdomains

  describe '.reserved?' do
    it 'handles strings' do
      assert(ReservedSubdomain.reserved?('accounts'))
      assert_equal false, ReservedSubdomain.reserved?('this-is-my-account')
    end

    it 'handles regular expressions' do
      assert(ReservedSubdomain.reserved?('ns1'))
      assert(ReservedSubdomain.reserved?('ns12345'))
      assert_equal false, ReservedSubdomain.reserved?('fake-ns1')
    end

    it 'only issues one query to check a string or a regex' do
      assert_sql_queries(1) { ReservedSubdomain.reserved?('accounts') }
      assert_sql_queries(1) { ReservedSubdomain.reserved?('ns1') }
    end
  end
end
