require_relative "../support/test_helper"

SingleCov.covered!

describe 'DefaultLocaleChooser' do
  fixtures :translation_locales
  describe "#choose" do
    before { @chooser = DefaultLocaleChooser }

    it "returns ENGLISH_BY_ZENDESK when locale_name is nil" do
      assert_equal ENGLISH_BY_ZENDESK, @chooser.choose(nil)
    end

    it "finds by locale if name is not nil" do
      locale = translation_locales(:brazilian_portuguese)
      assert_equal locale, @chooser.choose("pt-BR")
    end
  end
end
