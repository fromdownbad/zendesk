require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe FieldMultiselect do
  fixtures :accounts, :custom_field_options, :ticket_fields, :tickets

  let(:account) { accounts(:minimum) }
  let(:multiselect) { FieldMultiselect.new(account: accounts(:minimum), title: "Multiselect") }

  it "extends FieldTagger" do
    assert multiselect.is_a? FieldTagger
  end

  it "correctly saves the field" do
    multiselect.custom_field_options.build(name: "Option", value: "option")

    assert multiselect.save
    assert_equal FieldMultiselect.name, multiselect.type
  end

  describe "#aggregate_and_add_new_value" do
    describe_with_and_without_arturo_enabled :special_chars_in_custom_field_options do
      let(:ticket) { tickets(:minimum_1) }
      let(:multiselect_wombat_field) do
        FieldMultiselect.create!(
          account: accounts(:minimum),
          title: "Multiselect Wombat",
          custom_field_options: [
            CustomFieldOption.new(name: "Wombats", value: "wombats"),
            CustomFieldOption.new(name: "Giraffes", value: "giraffes"),
            CustomFieldOption.new(name: "Tazzmanian Devils", value: "tazzies"),
            CustomFieldOption.new(name: "Lemurs", value: "lemurs")
          ]
        )
      end
      let(:tf_entry) { ticket.ticket_field_entries.detect { |t| t.ticket_field_id == multiselect_wombat_field.id } }

      before do
        ticket.fields = {multiselect_wombat_field.id => ['wombats', 'giraffes']}
        ticket.will_be_saved_by(users(:minimum_admin))
        ticket.save!
      end

      it "with a valid new value, returns the fields current value as well as value being added" do
        assert_equal "giraffes wombats", tf_entry.value

        tazzie_option = ticket.account.custom_field_options.detect { |o| o.value == 'tazzies' }
        assert_equal ["giraffes", "wombats", "tazzies"], multiselect_wombat_field.aggregate_and_add_new_value(tazzie_option.id, ticket)
      end

      it "with an invalid value returns the fields current value" do
        assert_equal "giraffes wombats", tf_entry.value
        assert_equal ["giraffes", "wombats"], multiselect_wombat_field.aggregate_and_add_new_value(1234, ticket)
      end

      it "with a nil ticket returns only the value being added" do
        tazzie_option = account.custom_field_options.detect { |o| o.value == 'tazzies' }
        assert_equal ["tazzies"], multiselect_wombat_field.aggregate_and_add_new_value(tazzie_option.id, nil)
      end

      it "with an invalid value and a nil ticket returns an empty array" do
        assert_equal [], multiselect_wombat_field.aggregate_and_add_new_value(123, nil)
      end
    end
  end

  describe "#normalize_value_and_sync_tags" do
    let(:ticket) { tickets(:minimum_1) }

    let(:multiselect_field) do
      FieldMultiselect.create!(
        account: accounts(:minimum),
        title: "Multiselect Wombat",
        custom_field_options: [
          CustomFieldOption.new(name: "Wombats", value: "wombats"),
          CustomFieldOption.new(name: "Russian", value: "Верификация_селфи")
        ]
      )
    end

    before do
      multiselect_field.custom_field_options.last.update_column(:value, "Верификация_селфи")
    end

    describe_with_arturo_enabled :downcase_tags_as_ascii do
      it "handles non-ascii tags as case sensitive" do
        assert_equal 'Верификация_селфи', multiselect_field.normalize_value_and_sync_tags(ticket, %w[Верификация_селфи])
      end
    end

    describe_with_arturo_disabled :downcase_tags_as_ascii do
      it "handles non-ascii tags as case insensitive" do
        assert_nil multiselect_field.normalize_value_and_sync_tags(ticket, %w[Верификация_селфи])
      end
    end
  end

  describe "#compare" do
    let(:current_field_value) { "purple_wombat blue_wombat верификация_селфи" }
    let(:current_field_novalue) { '' }
    let(:included_value_id) { account.custom_field_options.find_by_value("purple_wombat").id }
    let(:not_included_value_id) { account.custom_field_options.find_by_value("pink_wombat").id }
    let(:included_non_ascii_value_id) { account.custom_field_options.find_by_value("Верификация_селфи").id }
    before do
      multiselect.custom_field_options.build(name: "Purple Wombat", value: "purple_wombat")
      multiselect.custom_field_options.build(name: "Blue Wombat", value: "blue_wombat")
      multiselect.custom_field_options.build(name: "Pink Wombat", value: "pink_wombat")
      multiselect.custom_field_options.build(name: "Just Blue", value: "blue")
      multiselect.custom_field_options.build(name: "Russian", value: "Верификация_селфи")
      multiselect.save!
    end

    it "returns false if operator is not recognized" do
      assert_equal false, multiselect.compare(current_field_value, 'wombat', included_value_id)
    end

    describe "operator is includes" do
      let(:operator) { 'includes' }
      it "returns true if string is included in current value" do
        assert(multiselect.compare(current_field_value, operator, included_value_id))
      end

      it "returns false if string is not included in current value" do
        assert_equal false, multiselect.compare(current_field_value, operator, not_included_value_id)
      end
    end

    describe "operator is not_includes" do
      let(:operator) { 'not_includes' }
      it "returns true if string is not included in current value" do
        assert(multiselect.compare(current_field_value, operator, not_included_value_id))
      end

      it "returns false if string is included in current value" do
        assert_equal false, multiselect.compare(current_field_value, operator, included_value_id)
      end
    end

    describe "operator is present" do
      let(:operator) { 'present' }
      it "returns true if the field has a current value" do
        assert(multiselect.compare(current_field_value, operator, ""))
      end

      it "returns false if the field does not have a current value" do
        assert_equal false, multiselect.compare(current_field_novalue, operator, "")
      end
    end

    describe "operator is not_present" do
      let(:operator) { 'not_present' }
      it "returns true if the field does not have a current value" do
        assert(multiselect.compare(current_field_novalue, operator, ""))
      end

      it "returns false if the field has a current value" do
        assert_equal false, multiselect.compare(current_field_value, operator, "")
      end
    end

    describe "#includes_value?" do
      let(:subset_string_value_id) { account.custom_field_options.find_by_value("blue").id }

      it "returns true if a tag is included in the current value of the field" do
        assert(multiselect.includes_value?(current_field_value, included_value_id))
      end

      it "returns true if the tag is blank and the current value of the field is blank" do
        assert(multiselect.includes_value?('', ''))
      end

      it "returns false if the tag is blank and the current value of the field is not" do
        assert_equal false, multiselect.includes_value?(current_field_value, '')
      end

      it "returns false if a tag is not included in the current value of the field" do
        assert_equal false, multiselect.includes_value?(current_field_value, not_included_value_id)
      end

      it "returns false if the tag is a subset of a value in the field" do
        assert_equal false, multiselect.includes_value?(current_field_value, subset_string_value_id)
      end

      it "returns false if the custom field id passed in is a valid custom field option" do
        assert_equal false, multiselect.includes_value?(current_field_value, 5)
      end

      describe 'non-ascii capitalized characters in the DB' do
        before do
          multiselect.custom_field_options.last.update_column(:value, "Верификация_селфи")
        end

        describe_with_arturo_enabled :downcase_tags_as_ascii do
          it "handles non-ascii characteres as case sensitive" do
            assert_equal false, multiselect.includes_value?(current_field_value, included_non_ascii_value_id)
          end
        end

        describe_with_arturo_disabled :downcase_tags_as_ascii do
          it "handles non-ascii characteres as case insensitive" do
            assert(multiselect.includes_value?(current_field_value, included_non_ascii_value_id))
          end
        end
      end
    end
  end
end
