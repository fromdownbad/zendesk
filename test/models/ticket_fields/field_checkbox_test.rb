require_relative "../../support/test_helper"

SingleCov.covered!
describe 'FieldCheckbox' do
  fixtures :accounts, :tickets, :ticket_fields, :users

  before do
    @field_checkbox = FieldCheckbox.new
  end

  describe "A FieldCheckbox" do
    subject { @field_checkbox }

    describe "#normalize_value_and_sync_tags" do
      let(:ticket) { tickets(:minimum_1) }
      let(:field) { FieldCheckbox.new(tag: "foobar", account: ticket.account) }

      describe "when updating to true" do
        before do
          ticket.current_tags = ""
        end

        CustomField::Checkbox::TRUTHY_VALUES.each do |field_value|
          it "adds the tag to the ticket" do
            refute ticket.current_tags.include?("foobar")
            field.normalize_value_and_sync_tags(ticket, field_value)
            assert ticket.current_tags.include?("foobar")
          end
        end
      end

      describe "when updating to false" do
        before do
          ticket.current_tags = "foobar"
        end

        CustomField::Checkbox::FALSY_VALUES.each do |field_value|
          it "removes the tag from the ticket" do
            assert ticket.current_tags.include?("foobar")
            field.normalize_value_and_sync_tags(ticket, field_value)
            refute ticket.current_tags.include?("foobar")
          end
        end
      end
    end

    describe "#assignable_value?" do
      it "returns true if the value is '1' or '0'" do
        assert subject.assignable_value?("1")
        assert subject.assignable_value?("0")
      end

      it "returns false if the value is not '1' or '0'" do
        refute subject.assignable_value?("something not '1' or '0'")
      end
    end

    describe "#validate_tags" do
      it "does not allow to save with special characters in tag" do
        @field_checkbox = FieldCheckbox.new(title: "Checkbox", tag: "&#7;", account: accounts(:minimum))

        Zendesk::CustomField::DropdownTagger.expects(:validate_unique)
        @field_checkbox.valid?
        expected_response = [
          { error: "InvalidValue",
            description: "Tag cannot contain special characters. Use only underscore and regular characters."}
        ]
        assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(@field_checkbox)[:details][:base]
      end

      it "does not allow to save with tags already used by another custom field" do
        @field_checkbox = FieldCheckbox.create!(title: "Wombat Checkbox", account: accounts(:minimum), tag: "wombats")
        @field_checkbox2 = FieldCheckbox.new(title: "Second Wombat", tag: "wombats", account: accounts(:minimum))

        @field_checkbox2.valid?
        expected_response = [
          {
            description: "The tag <strong>wombats</strong> is already used in a custom field drop-down, multi-select or checkbox."
          }
        ]
        assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(@field_checkbox2)[:details][:base]
      end

      describe "pipe validation" do
        let(:account) { accounts(:minimum) }
        let(:field_checkbox) { FieldCheckbox.new(title: "Checkbox", tag: "wom|bats", account: account) }

        describe_with_arturo_enabled :validate_pipes_for_tags do
          it "does not allow to save with | character in tag" do
            refute field_checkbox.valid?
            expected_response = [{
              error: "InvalidValue",
              description: "Tag cannot contain the | character."
            }]
            assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(field_checkbox)[:details][:base]
          end
        end

        describe_with_arturo_disabled :validate_pipes_for_tags do
          it "allows to save with | character in tag" do
            assert field_checkbox.valid?
          end
        end
      end
    end

    describe "#is_blank?" do
      before do
        @ticket = tickets(:minimum_3)
        @ticket_field = FieldCheckbox.create!(title: 'Test checkbox', account: accounts(:minimum))
        @ticket_field.stubs(:is_required_in_portal).returns(true)
        assert @ticket_field.value(@ticket).blank?
      end

      describe "when checkbox is unchecked" do
        before do
          @ticket.fields = {@ticket_field.id.to_s => '0'}
          @ticket.will_be_saved_by(users(:minimum_admin))
          @ticket.save!
        end

        it "returns true" do
          assert_equal '0', @ticket_field.value(@ticket)
          assert(@ticket_field.is_blank?(@ticket))
        end
      end

      describe "when checkbox is checked" do
        before do
          @ticket.fields = {@ticket_field.id.to_s => '1'}
          @ticket.will_be_saved_by(users(:minimum_admin))
          @ticket.save!
        end

        it "returns false" do
          assert_equal '1', @ticket_field.value(@ticket)
          assert_equal false, @ticket_field.is_blank?(@ticket)
        end
      end
    end
  end
end
