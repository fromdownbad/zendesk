require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe FieldTagger do
  fixtures :accounts, :custom_field_options, :ticket_fields, :tickets

  let(:account) { accounts(:minimum) }

  before do
    @field_tagger = FieldTagger.new(account: accounts(:minimum))
  end

  subject { @field_tagger }

  it "orders its custom_field_options by position and then id" do
    @field_tagger.custom_field_options.to_sql.must_match(/ORDER BY position, id ASC$/)
  end

  it "calls assign_custom_field_options_positions before saving" do
    @field_tagger.expects(:assign_custom_field_options_positions)
    @field_tagger.send :run_callbacks, :save
  end

  it "calls touch_updated_at_on_options_change after positions are changed in assign_custom_field_options_positions callback" do
    field_save = sequence('field_save')
    @field_tagger.expects(:assign_custom_field_options_positions).in_sequence(field_save)
    @field_tagger.expects(:touch_updated_at_on_options_change).in_sequence(field_save)
    @field_tagger.send :run_callbacks, :save
  end

  describe "#touch_updated_at_on_options_change" do
    before do
      @field_tagger.title = "wombat tagger"
      @field_tagger.custom_field_options.build(name: "are", value: "are")
      @field_tagger.custom_field_options.build(name: "wombats", value: "wombats")
      @field_tagger.custom_field_options.build(name: "amazing", value: "amazing")
      @field_tagger.save!
    end

    it "updates the ticket field timestamp when positions of custom field options are changed" do
      old_timestamp = @field_tagger.updated_at
      Timecop.travel(Time.now + 1.seconds)

      @field_tagger.custom_field_options[0].position = 1
      @field_tagger.custom_field_options[1].position = 0
      @field_tagger.custom_field_options[2].position = 2
      @field_tagger.send(:touch_updated_at_on_options_change)
      assert_not_nil old_timestamp
      assert_not_equal old_timestamp, @field_tagger.updated_at
    end
  end

  describe "serialized as JSON" do
    before do
      @field_tagger.title = "Department"
      @field_tagger.custom_field_options.build(name: "Sales", value: "sales")
      @decoded_serialization = ActiveSupport::JSON.decode(@field_tagger.to_json)
    end

    it "includes its custom_field_options" do
      assert_not_nil @decoded_serialization["custom_field_options"]
    end # include its custom_field_options association

    it "only include the name and value for each of its custom_field_options" do
      @decoded_serialization["custom_field_options"].each do |custom_field_option|
        assert_equal ["name", "value"], custom_field_option.keys.sort - ["id"]
      end
    end
  end

  describe "#assignable_value?" do
    it "returns true if the value is one of the custom field options" do
      subject.stubs(custom_field_options: [stub(value: 'some valid option')])

      assert subject.assignable_value?('some valid option')
    end

    it "returns false if the value is not one of the custom field options" do
      subject.stubs(custom_field_options: [])

      refute subject.assignable_value?('some invalid option')
    end
  end

  describe "#normalize_value_and_sync_tags" do
    let(:setup_tagger) do
      @field_tagger.title = "wombat tagger"
      @field_tagger.custom_field_options.build(name: "wombat", value: "wombat")
      @field_tagger.save!
      @ticket = tickets(:minimum_1)
    end

    describe_with_and_without_arturo_enabled :special_chars_in_custom_field_options do
      before { setup_tagger }

      it "accepts options that exist" do
        assert_equal("wombat", subject.normalize_value_and_sync_tags(@ticket, "wombat"))
      end
      it "refutes options that don't exist" do
        assert_nil(subject.normalize_value_and_sync_tags(@ticket, "batman"))
      end
      it "accepts and downcases option values" do
        assert_equal("wombat", subject.normalize_value_and_sync_tags(@ticket, "Wombat"))
      end
    end
  end

  describe "#assign_custom_field_options_positions" do
    let(:custom_field_options) { @field_tagger.custom_field_options }

    before do
      custom_field_options.build(name: "Bob", value: "bob")
      custom_field_options.build(name: "Alice", value: "alice")
      custom_field_options.build(name: "aardvark", value: "alice")
    end

    describe "when field is new" do
      describe "with sort_alphabetically set to true" do
        before do
          @field_tagger.sort_alphabetically = true
          @field_tagger.send(:assign_custom_field_options_positions)
        end

        it "assigns positions by alphabetical name order" do
          alphabetical = custom_field_options.sort_by { |o| o.name.downcase }

          alphabetical.each_with_index do |option, expected_position|
            assert_equal expected_position, option.position
          end

          assert_equal(
            ["aardvark", "Alice", "Bob"],
            custom_field_options.sort_by(&:position).map(&:name)
          )
        end
      end

      describe "with sort_alphabetically set to false" do
        before do
          @field_tagger.sort_alphabetically = false
          @field_tagger.send(:assign_custom_field_options_positions)
        end

        it "assigns positions by index value" do
          custom_field_options.each_with_index do |option, expected_position|
            assert_equal expected_position, option.position
          end

          assert_equal(
            ["Bob", "Alice", "aardvark"],
            custom_field_options.sort_by(&:position).map(&:name)
          )
        end
      end
    end

    describe "when field is not new" do
      before do
        @field_tagger.stubs(:new_record?).returns(false)
      end

      describe "and custom field option positions are changed" do
        before do
          custom_field_options[0].position = 1
          custom_field_options[1].position = 2
          custom_field_options[2].position = 0
        end

        it "assigns positions by index value" do
          @field_tagger.send(:assign_custom_field_options_positions)

          custom_field_options.each_with_index do |option, expected_position|
            assert_equal expected_position, option.position
          end

          assert_equal(
            ["Bob", "Alice", "aardvark"],
            custom_field_options.sort_by(&:position).map(&:name)
          )
        end

        describe "and a position is made nil" do
          before do
            custom_field_options[1].position = nil

            @field_tagger.send(:assign_custom_field_options_positions)
          end

          it "assigns positions by index value" do
            assert_equal(
              ["Bob", "Alice", "aardvark",],
              custom_field_options.sort_by(&:position).map(&:name)
            )
          end
        end
      end
    end
  end

  describe "#validate" do
    before do
      Arturo.enable_feature!(:custom_field_options_limit)
      @field_tagger = ticket_fields(:field_tagger_custom)
      assert @field_tagger.valid?
    end

    describe "custom field options limits" do
      describe "grandfathered account" do
        before { Timecop.freeze(2020, 1, 1, 9, 0, 0) }
        after { Timecop.return }

        it "allows too many custom field options" do
          account.created_at = DateTime.parse("2019-6-2")
          account.settings.custom_field_options_limit = 2
          account.save!
          @field_tagger.reload
          assert @field_tagger.valid?
        end
      end

      describe "without arturo" do
        before do
          Arturo.disable_feature!(:custom_field_options_limit)
          account.created_at = DateTime.parse("2022-6-2")
          Timecop.freeze(2023, 1, 1, 9, 0, 0)
        end
        after { Timecop.return }

        it "allowed with more custom field options than custom_field_options_limit" do
          account.settings.custom_field_options_limit = 2
          account.save!
          @field_tagger.reload
          assert @field_tagger.valid?
        end
      end

      describe "non-grandfathered account" do
        before do
          account.created_at = DateTime.parse("2022-6-2")
          Timecop.freeze(2023, 1, 1, 9, 0, 0)
        end
        after { Timecop.return }

        it "does not allow with too many custom field options" do
          account.settings.custom_field_options_limit = 2
          account.save!
          @field_tagger.reload
          refute @field_tagger.valid?

          message = I18n.t('txt.admin.models.ticket_fields.field_tagger.must_not_contain_too_many_options', number_of_options: 2)
          refute_empty message
          assert_equal [message], @field_tagger.errors[:base]
        end

        it "allow with an allowed number of custom field options" do
          account.settings.custom_field_options_limit = 20
          account.save!
          @field_tagger.reload
          assert @field_tagger.valid?
        end
      end
    end

    it "does not allow a custom field with a blank value" do
      @field_tagger.custom_field_options.first.value = ""
      refute @field_tagger.valid?
      assert_equal ["must have a tag"], @field_tagger.errors[:field_options]
    end

    it "does not allow custom field to use tags already used by another custom field" do
      @field_checkbox = FieldCheckbox.create!(title: "Wombat Checkbox", account: accounts(:minimum), tag: "wombats")
      @field_tagger.custom_field_options.build(name: "Wombats", value: "wombats")

      @field_tagger.valid?
      expected_response = [
        {
          description: "The tag <strong>wombats</strong> is already used in a custom field drop-down, multi-select or checkbox."
        }
      ]
      assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(@field_tagger)[:details][:base]
    end

    it "to avoid cookie overflow, do not show absurd high number of duplicate tags in error messages" do
      @field_tagger_dup = FieldTagger.new(account: @field_tagger.account, title: "Duplicate", type: FieldTagger)
      (0..10).each do |i|
        @field_tagger.custom_field_options.build(name: "Name#{i}", value: "tag#{i}")
        @field_tagger_dup.custom_field_options.build(name: "Name#{i}", value: "tag#{i}")
      end
      Timecop.travel(Time.now + 1.seconds)
      @field_tagger.save!

      assert_equal false, @field_tagger_dup.valid?
      expected_response = [
        {
          description: "The tags <strong>tag0, tag1, tag2, tag3, tag4, tag5, tag6, tag7, tag8, tag9</strong>, and 1 more, have already been used in another custom field drop-down, multi-select or checkbox."
        }
      ]
      assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(@field_tagger_dup)[:details][:base]
    end
  end

  describe "validating SPECIAL CHARS" do
    describe_with_arturo_enabled :special_chars_in_custom_field_options do
      let(:ticket_field_condition) { FactoryBot.create(:ticket_field_condition, account: accounts(:minimum)) }
      let(:ticket_form) { ticket_field_condition.ticket_form }
      let!(:tagger) { FactoryBot.create(:field_tagger, account: accounts(:minimum)) }
      let!(:custom_field_option) { tagger.custom_field_options.create!(name: "GDPR Option", value: "questionable_$!?", account: tagger.account) }

      it 'should update the field_tagger with ' do
        assert tagger.valid?
      end
    end

    describe_with_arturo_disabled :special_chars_in_custom_field_options do
      let(:ticket_field_condition) { FactoryBot.create(:ticket_field_condition, account: accounts(:minimum)) }
      let(:ticket_form) { ticket_field_condition.ticket_form }
      let!(:tagger) { FactoryBot.create(:field_tagger, account: accounts(:minimum)) }
      let!(:custom_field_option) do
        option = tagger.custom_field_options.new(name: "GDPR Option", value: "questionable_$!?", account: tagger.account)
        option.save!(validate: false)
      end

      it 'should update the field_tagger with ' do
        assert tagger.persisted?
        refute tagger.valid?
        assert tagger.errors.messages[:field_options].first.include?('cannot contain special characters')
      end
    end
  end

  describe "#validate_default_uniqueness" do
    let(:field_tagger) { ticket_fields(:field_tagger_custom) }

    it "makes the field not valid if there are multiple defaults" do
      field_tagger.custom_field_options.load_target
      field_tagger.custom_field_options.second.default = true
      field_tagger.custom_field_options.third .default = true
      refute field_tagger.valid?
      assert_equal ["can have at most one default."], field_tagger.errors[:field_options]
    end

    it "doesn't make the field not valid if there's only one default" do
      field_tagger.custom_field_options.second.default = true
      field_tagger.custom_field_options.third.default = false
      assert field_tagger.valid?
    end

    it "doesn't make the field not valid if there's no default" do
      field_tagger.custom_field_options.second.default = false
      field_tagger.custom_field_options.third.default = false
      assert field_tagger.valid?
    end

    it "doesn't make the field not valid if there are multiple defaults but only one not deleted" do
      field_tagger.custom_field_options.second.stubs(:deleted?).returns(true)
      field_tagger.custom_field_options.second.default = true
      field_tagger.custom_field_options.third.default = true
      assert field_tagger.valid?
    end
  end

  describe ".taggers_tag_to_name_lookup" do
    it "shoulds return" do
      assert_equal({"booring" => "Booring", "dc_content" => "{{dc.welcome_wombat}}", "hilarious" => "Hilarious", "not_bad" => "Not bad"}, FieldTagger.taggers_tag_to_name_lookup(accounts(:minimum)))
    end
  end

  describe "mass assignment of custom_field_options" do
    describe_with_and_without_arturo_enabled :bulk_insert_or_update_custom_field_options do
      before do
        @field_tagger.title = "wombat tagger"
        @field_tagger.custom_field_options.build(name: "are", value: "are")
        @field_tagger.custom_field_options.build(name: "wombats", value: "wombats")
        @field_tagger.custom_field_options.build(name: "amazing", value: "amazing")

        expected_sql_queries = account.has_bulk_insert_or_update_custom_field_options? ? 9 : 11

        assert_sql_queries(expected_sql_queries, filter: /auto_increment_increment/) do
          @field_tagger.save!
        end
      end

      it "works when creating new ticket field" do
        @field_tagger.reload

        assert_equal ["are", "wombats", "amazing"], @field_tagger.custom_field_options.map(&:name)
        assert_equal ["are", "wombats", "amazing"], @field_tagger.custom_field_options.map(&:value)
        assert_equal [0, 1, 2], @field_tagger.custom_field_options.map(&:position)
      end

      it "works when updating existing ticket field" do
        @field_tagger.custom_field_options.create!(name: "deleted", value: "deleted", position: 3).soft_delete!
        @field_tagger.reload
        CustomFieldOption.with_deleted do
          cfo1, cfo2, cfo3, cfo4 = @field_tagger.custom_field_options.reload
          cfo1.name = "aren't"
          cfo2.value = "wombat_value"
          cfo3.mark_as_deleted
          cfo4.mark_as_undeleted
          cfo4.position = 2
          cfo4.name = "super"
          @field_tagger.custom_field_options.build(name: "dope", value: "dope")
        end

        expected_sql_queries = account.has_bulk_insert_or_update_custom_field_options? ? 9..10 : 13..14
        assert_sql_queries(expected_sql_queries, filter: /auto_increment_increment/) do
          @field_tagger.save!
        end

        @field_tagger.reload
        assert_equal ["aren't", "wombats", "super", "dope"], @field_tagger.custom_field_options.map(&:name)
        assert_equal ["are", "wombat_value", "deleted", "dope"], @field_tagger.custom_field_options.map(&:value)
        assert_equal [0, 1, 2, 3], @field_tagger.custom_field_options.map(&:position)
      end
    end
  end
end
