require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 11

describe TicketFieldEntry do
  fixtures :accounts, :tickets, :ticket_fields, :ticket_field_entries, :taggings, :users, :user_identities
  should_allow_values_for :value, nil, ""

  it "does not allow a system field as its ticket field" do
    TicketField::SYSTEM_FIELDS.each do |ticket_field_name|
      ticket_field = ticket_field_name.constantize.new(title_in_portal: ticket_field_name)
      ticket_field_entry = TicketFieldEntry.new(ticket_field: ticket_field)

      ticket_field_entry.valid?
      assert_includes ticket_field_entry.errors[:base].join, "#{ticket_field_name}: cannot be saved"
    end
  end # not allow a system field as its ticket field

  describe "with a ticket_field with a regexp_for_validation" do
    before do
      ticket_field = TicketField.new(regexp_for_validation: "a+", title_in_portal: "test field")
      @ticket_field_entry = TicketFieldEntry.new(ticket_field: ticket_field, ticket: tickets(:minimum_1))
    end

    it "allows values that match the ticket_field's regexp_for_validation" do
      @ticket_field_entry.value = "aaa"

      @ticket_field_entry.valid?

      assert_equal [], @ticket_field_entry.errors[:base]
    end # allow values that match the ticket_field's regexp_for_validation

    it "does not allow values that don't match the ticket_field's regexp_for_validation" do
      @ticket_field_entry.value = "bbb"

      @ticket_field_entry.valid?

      assert_includes @ticket_field_entry.errors[:base].join, 'test field: is invalid'
    end # not allow values that don't match the ticket_field's regexp_for_validation

    it "does not crash if the value is a number" do
      @ticket_field_entry.value = 123
      @ticket_field_entry.valid?
    end

    describe "with SafeRegexp" do
      before do
        @tf = TicketField.new(regexp_for_validation: 'aa?' * 84, title_in_portal: 'tf_title', account: accounts(:minimum))
        @tfe = TicketFieldEntry.new(ticket_field: @tf, ticket: tickets(:minimum_1))
        @tfe.value = 'a' * 40
      end

      describe_with_arturo_disabled :use_safe_regexp do
        it "refutes the match slowly" do
          time = Benchmark.realtime { refute @tfe.valid? }
          assert_operator time, :>, 2
        end
      end

      describe_with_arturo_enabled :use_safe_regexp do
        it "refutes the match quickly with reporting, logging, and a nice error message" do
          Rails.logger.expects(:warn).with(regexp_matches(/SafeRegexp::RegexpTimeout: TicketFieldEntry: account_id: #{@tf.account_id}, field id:/))
          Zendesk::StatsD::Client.any_instance.expects(:increment).with("safe_regexp_regexp_timeout", tags: ["type:TicketFieldEntry", "account_id:#{@tf.account_id}"])

          time = Benchmark.realtime { refute @tfe.valid? }
          assert_operator time, :<, 2
          assert_equal @tfe.errors[:base].join, "tf_title: The regex field should be modified to prevent slow regex matching"
        end

        it "falls back to traditional regex.match if safe_regexp raises Errno::ENOMEM" do
          Rails.logger.expects(:warn).with(regexp_matches(/Errno::ENOMEM: TicketFieldEntry: account_id: #{@tf.account_id}, field id:/))
          Zendesk::StatsD::Client.any_instance.expects(:increment).with("safe_regexp_enomem", tags: ["type:TicketFieldEntry", "account_id:#{@tf.account_id}"])
          SafeRegexp.stubs(:execute).raises(Errno::ENOMEM)
          @tfe.value = 'a'

          refute @tfe.valid?
          assert_includes @tfe.errors[:base].join, 'tf_title: is invalid'
        end

        it "reports a successful safe_regexp match" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with("safe_regexp_success", tags: ["type:TicketFieldEntry"])
          @ticket_field_entry.value = "aaa"

          @ticket_field_entry.valid?
          assert_equal [], @ticket_field_entry.errors[:base]
        end
      end
    end
  end # with a ticket_field with a regexp_for_validation

  describe "with a PartialCreditCardNumber field" do
    before do
      ticket_field = FieldPartialCreditCard.new(title_in_portal: "test field")
      @ticket_field_entry = TicketFieldEntry.new(ticket_field: ticket_field, ticket: tickets(:minimum_1))
    end

    it "allows an empty string" do
      @ticket_field_entry.value = ""
      assert @ticket_field_entry.valid?
      assert_empty @ticket_field_entry.errors[:base]
    end

    it "allows nils" do
      @ticket_field_entry.value = nil
      assert @ticket_field_entry.valid?
      assert_empty @ticket_field_entry.errors[:base]
    end

    it "allows redacted credit card numbers" do
      @ticket_field_entry.value = "XXXXXXXXXXXX1234"
      assert @ticket_field_entry.valid?
      assert_empty @ticket_field_entry.errors[:base]
    end

    it "rejects unredacted numbers" do
      @ticket_field_entry.value = "1234123412341234"
      refute @ticket_field_entry.valid?
      assert_includes @ticket_field_entry.errors[:base].join, 'test field: The credit card number is not redacted'
    end

    it "rejects invalid numbers" do
      @ticket_field_entry.value = "1234abcd1234abcd"
      refute @ticket_field_entry.valid?
      assert_includes @ticket_field_entry.errors[:base].join, 'test field: The credit card number is invalid'
    end

    it "rejects an 12 digit redacted number" do
      @ticket_field_entry.value = "XXXXXXXX1234"
      refute @ticket_field_entry.valid?
      assert_includes @ticket_field_entry.errors[:base].join, 'test field: The credit card number is too short'
    end

    it "accepts a 13 digit redacted number" do
      @ticket_field_entry.value = "XXXXXXXXX1234"
      assert @ticket_field_entry.valid?
      assert_empty @ticket_field_entry.errors[:base]
    end

    it "accepts a 19 digit redacted number" do
      @ticket_field_entry.value = "XXXXXXXXXXXXXXX1234"
      assert @ticket_field_entry.valid?
      assert_empty @ticket_field_entry.errors[:base]
    end

    it "rejects a 20 digit redacted number" do
      @ticket_field_entry.value = "XXXXXXXXXXXXXXXX1234"
      refute @ticket_field_entry.valid?
      assert_includes @ticket_field_entry.errors[:base].join, 'test field: The credit card number is too long'
    end

    it "does not crash if the value is a number" do
      @ticket_field_entry.value = 123
      refute @ticket_field_entry.valid?
    end
  end # with a PartialCreditCardNumber field

  describe "Validating a TicketFieldEntry with no TicketField" do
    before { @ticket_field_entry = TicketFieldEntry.create(ticket: tickets(:minimum_1)) }

    it "causes a validation error" do
      refute @ticket_field_entry.valid?
      assert_equal 1, @ticket_field_entry.errors[:ticket_field].size
    end

    it "has the correct attributes" do
      refute @ticket_field_entry.persisted?
    end

    it "does not raise on ticket update" do
      ticket = tickets(:minimum_1)
      TicketFieldEntry.create!(ticket: tickets(:minimum_1), ticket_field: ticket_fields(:field_integer_custom), value: '12')

      # Triggered in Ticket#validate_misc and Ticket#save_updated_ticket_fields
      TicketFieldEntry.any_instance.expects(:log_missing_ticket_field).twice
      tfe = ticket.ticket_field_entries.to_a.first
      tfe.value = 'changed'
      tfe.stubs(:ticket_field).returns(false)
      ticket.will_be_saved_by(ticket.account.owner)
      ticket.save!
    end
  end

  describe "Saving a TicketFieldEntry" do
    before do
      @ticket_field_entry = TicketFieldEntry.create!(ticket: tickets(:minimum_1), ticket_field: ticket_fields(:field_integer_custom), value: '12')
    end

    subject { @ticket_field_entry }

    should validate_uniqueness_of(:ticket_field_id).scoped_to(:ticket_id)

    it "sets account_id from the ticket" do
      assert_equal @ticket_field_entry.account, accounts(:minimum)
    end
  end # Saving a TicketFieldEntry

  describe "TicketFieldEntry#normalize_checkbox_values" do
    before do
      @ticket_field_entry = TicketFieldEntry.new
    end

    it "gets called when validating a TicketFieldEntry" do
      @ticket_field_entry.expects(:normalize_checkbox_values)

      @ticket_field_entry.valid?
    end # get called when validating a TicketFieldEntry

    describe "for a FieldCheckbox ticket_field" do
      before do
        @ticket_field_entry.ticket_field = FieldCheckbox.new(title_in_portal: "Urgent?")
      end

      it "calls a cast method to determine the value's truthiness" do
        @ticket_field_entry.value = "a-value"

        msg = RAILS4 ? :type_cast_from_user : :cast
        ActiveRecord::Type::Boolean.any_instance.expects(msg).with(@ticket_field_entry.value)

        @ticket_field_entry.send(:normalize_checkbox_values)
      end # call AR::ConnectionAdapters::Column.value_to_boolean to determine the value's truthiness

      it "sets the value to '1' when the value is truthy" do
        Zendesk::TRUE_VALUES.each do |truthy_value|
          @ticket_field_entry.value = truthy_value

          @ticket_field_entry.send(:normalize_checkbox_values)

          assert_equal '1', @ticket_field_entry.value
        end
      end # set the value to '1' when the value is truthy

      it "sets the value to '0' when the value is falsey" do
        Zendesk::FALSE_VALUES.each do |falsey_value|
          @ticket_field_entry.value = falsey_value

          @ticket_field_entry.send(:normalize_checkbox_values)

          assert_equal '0', @ticket_field_entry.value
        end
      end # set the value to '0' when the value is falsey
    end # when the ticket field is a FieldCheckbox
  end # TicketFieldEntry#normalize_checkbox_values

  describe "#redaction_event" do
    before do
      @ticket = tickets(:minimum_2)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @ticket_field_entry = @ticket.ticket_field_entries.first
      @ticket_field_entry.value = "something to make this field save"
      @ticket_field_entry.redaction_events.build(audit: @ticket.audit)
      @ticket_field_entry.save!
      @ticket_field_entry.reload
      @ticket.reload
    end

    it "autosaves" do
      assert @ticket_field_entry.redaction_events.any?
    end

    it "associates with the ticket" do
      assert(@ticket.redactions?)
    end
  end

  it "tests integer" do
    field = TicketFieldEntry.new(ticket: tickets(:minimum_1), ticket_field: ticket_fields(:field_integer_custom))

    field.value = "12"
    assert field.valid?

    field.value = "12,2"
    refute field.valid?
  end

  it "tests regexp" do
    field = TicketFieldEntry.new(ticket: tickets(:minimum_1), ticket_field: ticket_fields(:field_regexp_custom))

    field.value = "12.2"
    assert field.valid?

    field.value = "12a"
    refute field.valid?
  end

  it "tests invalid tagger tag" do
    field = TicketFieldEntry.new(ticket: tickets(:minimum_1), ticket_field: ticket_fields(:field_tagger_custom), value: 'booring')
    assert field.valid?
  end

  describe "validations" do
    it "should validate value length" do
      field = TicketFieldEntry.new(ticket: tickets(:minimum_1), ticket_field: ticket_fields(:field_textarea_custom), value: "a" * 65536)
      refute field.valid?
      expected_errors = ["Value is too long (maximum is 65535 characters after encoding)"]
      assert_equal expected_errors, field.errors.to_a
    end
  end
end
