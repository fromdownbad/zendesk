require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 11

describe 'TicketField' do
  fixtures :accounts, :tickets, :ticket_fields, :ticket_field_entries, :ticket_forms,
    :custom_field_options, :taggings, :translation_locales, :rules

  describe "base" do
    it "includes Zendesk::PreventCollectionModificationMixin" do
      assert(TicketField.include?(Zendesk::PreventCollectionModificationMixin))
    end

    describe "#sub_type_id" do
      # Inverse case covered in submodel tests
      it "should nullify sub_type_id on save for non-SYSTEM_ACCEPTS_SUB_TYPE fields" do
        field = accounts(:minimum).send (TicketField::SYSTEM_FIELDS - TicketField::SYSTEM_ACCEPTS_SUB_TYPE).first.underscore
        field.sub_type_id = 0
        field.save

        assert_nil field.sub_type_id
      end
    end

    describe "#present_condition_value" do
      it "returns the same value for normal fields" do
        value = 'value1'
        ticket_field = TicketField.new(title: 'test')
        assert_equal value, ticket_field.present_condition_value(value)
      end
    end

    describe "#accepts_sub_type?" do
      it "should be true when the field is of a type listed in SYSTEM_ACCEPTS_SUB_TYPE" do
        field = accounts(:minimum).send TicketField::SYSTEM_ACCEPTS_SUB_TYPE.first.underscore

        assert field.accepts_sub_type?
      end

      it "should be false when the field is NOT of a type listed in SYSTEM_ACCEPTS_SUB_TYPE" do
        field = accounts(:minimum).send (TicketField::SYSTEM_FIELDS - TicketField::SYSTEM_ACCEPTS_SUB_TYPE).first.underscore

        assert !field.accepts_sub_type?
      end
    end

    describe "activation" do
      let(:account) { accounts(:minimum) }
      it "does not allow a partial credit card field to be activated without the pci_credit_card_custom_field feature" do
        field = FieldPartialCreditCard.create!(account: account, title: 'credit card field', is_active: false)
        field.is_active = true

        refute field.save
        assert_equal 1, field.errors.size
        field.errors.full_messages.first.must_include 'Advanced Security Add-on required to activate this field'
      end

      it "allows a partial credit card field to be activated with the pci_credit_card_custom_field feature" do
        Account.any_instance.stubs(has_pci_credit_card_custom_field?: true)
        field = FieldPartialCreditCard.create!(account: account, title: 'credit card field', is_active: false)
        field.is_active = true

        assert field.save
        assert_empty field.errors
      end

      it "allows a normal custom field to be activated" do
        field = FieldText.create!(account: account, title: 'text field', is_active: false)
        field.is_active = true

        assert field.save
        assert_empty field.errors
      end
    end

    describe "#toggle!" do
      it "sets #is_active to true if it was false" do
        ticket_field = FactoryBot.create(:field_tagger, is_active: false)
        ticket_field.toggle!
        assert ticket_field.is_active?
      end

      it "sets #is_active to false if it was true" do
        ticket_field = FactoryBot.create(:field_tagger, is_active: true)
        ticket_field.toggle!
        refute ticket_field.is_active?
      end

      it "does not change #is_active if the model has an app requirement" do
        ticket_field = FactoryBot.create(:field_tagger, is_active: false)
        ticket_field.expects(:can_modify_collection?).returns(false)
        refute ticket_field.is_active?
        ticket_field.toggle!
        refute ticket_field.reload.is_active?
      end
    end

    describe "#create" do
      it "creates" do
        ticket_field = TicketField.new(title: 'test')
        ticket_field.account = accounts(:minimum)
        ticket_field.is_required = true
        ticket_field.is_required_in_portal = false

        assert ticket_field.save
        assert_equal 'test', ticket_field.description
        assert_equal false, ticket_field.is_required_in_portal?
        assert_equal false, ticket_field.is_visible_in_portal?
      end

      it "is not valid if the title is empty" do
        ticket_field = TicketField.new(
          account: accounts(:minimum),
          title: ''
        )

        refute_valid ticket_field
        ticket_field.errors.must_include(:title)
      end

      it "validates if title_in_portal is empty but not visible in portal" do
        ticket_field = TicketField.new(
          account: accounts(:minimum),
          title: 'test',
          title_in_portal: "",
          is_visible_in_portal: false
        )

        assert_valid ticket_field
      end

      it "is not valid if the title_in_portal is empty and it is visible in portal" do
        ticket_field = TicketField.new(account: accounts(:minimum), title: 'test', title_in_portal: "", is_visible_in_portal: true)

        refute_valid ticket_field
        ticket_field.errors.must_include(:title_in_portal)
      end

      it "is valid if title_in_portal is empty and it is visible in portal but the feature is disabled" do
        account = accounts(:minimum)
        account.stubs(:has_title_in_portal_presence_validation?).returns(false)
        ticket_field = TicketField.new(account: account, title: 'test', title_in_portal: "", is_visible_in_portal: true)

        assert_valid ticket_field
      end

      it "is not valid if title is > 255 characters" do
        ticket_field = TicketField.new(title: "x" * 256, account: accounts(:minimum))

        refute_valid ticket_field
        ticket_field.errors.must_include(:title)
      end

      it "is not valid if title contains ideographic space \u3000" do
        ideographic_space_title = "\u3000"
        ticket_field = TicketField.new(title: ideographic_space_title, account: accounts(:minimum))

        refute_valid ticket_field
      end

      it "does not raise an error if title is nil" do
        ticket_field = TicketField.new(title: nil, account: accounts(:minimum))

        refute_valid ticket_field
      end
    end

    describe "#destroy" do
      let(:ticket_field) { ticket_fields(:field_tagger_custom) }

      it "deletes TicketFieldEntry associations" do
        assert_equal 1, ticket_field.ticket_field_entries.count(:all)
        ticket_field.destroy
        assert_equal 0, ticket_field.ticket_field_entries.count(:all)
      end

      it "refuses to delete if the field is still in use" do
        rule = rules(:trigger_notify_assignee_of_comment_update)
        rule.definition.conditions_all.push DefinitionItem.new("ticket_fields_#{ticket_field.id}", "is", [ticket_field.custom_field_options.first.id])
        rule.save!
        refute ticket_field.destroy
        ticket_field.errors.full_messages.must_equal ["Field cannot be deleted because it is still in use by: #{rule.lotus_permalink}"]
      end

      it 'enqueues a ticket field entry delete job on destroy' do
        TicketFieldEntryDeleteJob.expects(:enqueue).with do |account_id|
          account_id == ticket_field.account.id
        end

        ticket_field.destroy
      end

      it 'does not enqueues a ticket field entry delete job when a destroy rolls back' do
        TicketFieldEntryDeleteJob.expects(:enqueue).never

        with_rollback(ticket_field) { ticket_field.destroy }
      end
    end

    describe "ticket field conditions" do
      describe "when the account has Conditional Ticket Fields active" do
        before(:each) do
          @condition = FactoryBot.create(:ticket_field_condition)
          @condition.account.stubs(:has_native_conditional_fields_enabled?).returns(true)
        end

        describe "validations" do
          before(:each) do
            @condition.update_attributes user_type: :end_user
          end

          describe "update" do
            it "should not allow disabling end-user-editability when used as parent field in an end-user condition" do
              @end_user_parent_field = @condition.parent_field
              @end_user_parent_field.is_editable_in_portal = false

              refute @end_user_parent_field.valid?

              assert_equal @end_user_parent_field.errors[:base].first, I18n.t('txt.admin.models.ticket_field.ticket_field.field_with_end_user_conditions_must_be_end_user_editable', field_name: @end_user_parent_field.title)

              @end_user_parent_field.is_editable_in_portal = true
              assert @end_user_parent_field.valid?
            end

            it "should not allow disabling end-user-editability when used as child field in an end-user condition" do
              @end_user_child_field = @condition.child_field
              @end_user_child_field.is_editable_in_portal = false

              refute @end_user_child_field.valid?

              assert_equal @end_user_child_field.errors[:base].first, I18n.t('txt.admin.models.ticket_field.ticket_field.field_with_end_user_conditions_must_be_end_user_editable', field_name: @end_user_child_field.title)

              @end_user_child_field.is_editable_in_portal = true
              assert @end_user_child_field.valid?
            end
          end

          describe "deactivation" do
            it "should not allow deactivation if the field is a parent field in a condition" do
              field = @condition.parent_field

              field.is_active = false

              refute field.valid?
              assert_equal field.errors[:base].first, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_deactivated_by_condition', field_name: field.title)
            end

            it "should allow deactivation if the field not a parent field in any condition" do
              field = @condition.parent_field
              @condition.destroy

              field.is_active = false

              assert field.valid?
            end

            it "should not allow deactivation if the field is a child field in a condition" do
              field = @condition.child_field

              field.is_active = false

              refute field.valid?
              assert_equal field.errors[:base].first, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_deactivated_by_condition', field_name: field.title)
            end

            it "should allow deactivation if the field not a child field in any condition" do
              field = @condition.child_field
              @condition.destroy

              field.is_active = false

              assert field.valid?
            end
          end

          describe "deletion" do
            it "should not allow deletion if the field is a parent field in a condition" do
              field = @condition.parent_field

              field.destroy

              assert field
              assert_equal field.errors[:base].first, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_deleted_by_condition', field_name: field.title)
            end

            it "should allow deletion if the field not a parent field in any condition" do
              field = @condition.parent_field
              @condition.destroy

              field.destroy

              assert TicketField.where(id: field.id).none?
            end

            it "should not allow deletion if the field is a child field in a condition" do
              field = @condition.child_field

              field.destroy

              assert field
              assert_equal field.errors[:base].first, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_deleted_by_condition', field_name: field.title)

              # Assert field not destroyed
              field.reload
              assert field.id
            end

            it "should allow deletion if the field not a child field in any condition" do
              field = @condition.child_field
              @condition.destroy

              field.destroy

              assert TicketField.where(id: field.id).none?
            end
          end
        end
      end

      describe "when the account is not using Conditional Ticket Fields" do
        before(:each) do
          @condition = FactoryBot.create(:ticket_field_condition)
          @condition.account.stubs(:has_native_conditional_fields_enabled?).returns(false)
        end

        it "should not attempt to validate against conditions even if it's a parent field to a condition" do
          field = @condition.parent_field
          field.expects(:ticket_field_conditions).never

          field.is_active = false
          assert field.valid?
        end

        it "should not attempt to validate against conditions even if it's a child field to a condition" do
          condition = FactoryBot.create(:ticket_field_condition)
          condition.account.stubs(:has_native_conditional_fields_enabled?).returns(false)

          field = @condition.child_field
          field.expects(:ticket_field_conditions).never

          field.is_active = false
          assert field.valid?
        end
      end
    end

    it "orders Account#ticket_fields by positions" do
      account = accounts(:minimum)
      ordered_ids = []
      total = account.ticket_fields.count(:all)
      account.ticket_fields.each_with_index do |f, idx|
        f.update_attribute(:position, total - idx)
        ordered_ids << f.id
      end

      assert_equal ordered_ids.reverse, account.ticket_fields.reload.map(&:id)
    end

    it "has collapsible fields" do
      assert_equal 1, accounts(:minimum).ticket_fields.collapsed_for_agents.count(:all)
    end

    it "has non-collapsible fields" do
      assert_equal 16, accounts(:minimum).ticket_fields.not_collapsed_for_agents.count(:all)
    end

    describe "#update_default_ticket_form" do
      before do
        @account = accounts(:minimum)
        params = { ticket_form: { name: 'Default form', display_name: 'Default display name',
                                  default: true, end_user_visible: true, position: 1 } }
        @default_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        @default_form.save!
      end

      it "adds the new ticket field to the default ticket form if the field is not a system field" do
        ticket_field = FieldText.new(title: 'custom text field')
        ticket_field.account = @account
        assert ticket_field.save
        @default_form.reload

        assert @default_form.ticket_fields.map(&:id).include?(ticket_field.id)
      end

      it "does not add the new ticket field to the default ticket form if the field is a system field" do
        sf = @account.ticket_fields.select { |tf| tf.class == FieldTicketType }.first
        assert sf.delete
        ticket_field = FieldTicketType.new(title: 'My Type')
        ticket_field.account = @account

        ticket_field.expects(:update_default_ticket_form).never
        ticket_field.save!
      end

      it "does not add the ticket field to the default ticket form if the account has more than one active ticket form" do
        params = { ticket_form: { name: 'New form', display_name: 'new name', active: true,
                                  default: false, end_user_visible: true, position: 2 } }
        form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
        form.save!

        assert_equal 2, @account.ticket_forms.count(:all)

        ticket_field = FieldText.new(title: 'custom text field')
        ticket_field.account = @account
        Zendesk::TicketForms::Initializer.expects(:new).never
        assert ticket_field.save
        @default_form.reload

        refute @default_form.ticket_fields.map(&:id).include?(ticket_field.id)
      end
    end

    describe '#multilingual_field' do
      before do
        @account = accounts(:minimum)
        @subject = @account.field_subject
        @japanese = translation_locales(:japanese)
      end

      describe "when the field's stored value matches the default content for the account locale" do
        before do
          @subject.description = ""
          @subject.title_in_portal = "Subject"
          @subject.save!
        end

        it "returns a value translated into the user locale" do
          I18n.expects(:t).with("txt.default.fields.subject.title", locale: @account.translation_locale).returns('Subject').twice
          I18n.expects(:t).with("txt.default.fields.subject.title", locale: @japanese).returns('Japanese subject title').twice
          I18n.expects(:t).with("txt.default.fields.subject.description", locale: @account.translation_locale).returns('')
          I18n.expects(:t).with("txt.default.fields.subject.description", locale: @japanese).returns('Japanese subject description')
          assert_equal "Japanese subject title", @subject.multilingual_field(user_locale: @japanese, account_locale: @account.translation_locale, field_type: :title)
          assert_equal "Japanese subject title", @subject.multilingual_field(user_locale: @japanese, account_locale: @account.translation_locale, field_type: :title_in_portal)
          assert_equal "Japanese subject description", @subject.multilingual_field(user_locale: @japanese, account_locale: @account.translation_locale, field_type: :description)
        end
      end

      describe "when the field's stored value does not match the default content for the account locale" do
        before do
          @stored_subject_title = 'Non-default value for subject title'
          @subject.title = @stored_subject_title
          @subject.save
        end

        it "returns the stored value" do
          I18n.expects(:t).with("txt.default.fields.subject.title", locale: @account.translation_locale).returns('Subject')
          assert_equal @stored_subject_title, @subject.multilingual_field(user_locale: @japanese, account_locale: @account.translation_locale, field_type: :title)
        end
      end
    end
  end

  describe "System Ticket Field" do
    let(:system_field) { accounts(:minimum).field_subject }

    it "validates title is not changed" do
      system_field.title = 'world'
      refute system_field.save
      system_field.errors[:base].to_s.must_include "System ticket field titles cannot be changed."
    end
  end

  describe "FieldDecimal" do
    let(:field) { FieldDecimal.create!(account: accounts(:minimum), title: 'fancy') }

    it "validates" do
      entry = field.ticket_field_entries.new(value: '1.0', account: field.account)
      assert entry.valid?, entry.errors.full_messages.join(' ')

      entry = field.ticket_field_entries.new(value: '.0', account: field.account)
      assert entry.valid?, entry.errors.full_messages.join(' ')

      entry = field.ticket_field_entries.new(value: 'a', account: field.account)
      refute entry.valid?
    end

    it "validates against potential XSS" do
      entry = field.ticket_field_entries.new(value: "<script>alert('hi')</script>\n0.1", account: field.account)
      refute entry.valid?
    end
  end

  #  context "FieldAssignee" do
  #    should_eventually "have tests"
  #  end
  #
  #  context "FieldCheckbox" do
  #    should_eventually "have tests"
  #  end

  describe "FieldDescription" do
    subject { accounts(:minimum).field_description }

    describe "#toggle!" do
      it "shoulds not validate model" do
        refute subject.is_active?

        subject.toggle!

        assert subject.reload.is_active?
      end
    end

    it "validates is_active? on update" do
      refute subject.is_active?

      subject.is_active = true
      refute subject.save

      subject.errors[:base].to_s.must_include "deactivated"
    end
  end

  describe "FieldText" do
    describe "save cache expiration" do
      it "expires cache without blowing up" do
        field = FieldText.new(title: 'test')
        field.account = accounts(:minimum)
        field.is_required = false
        field.account

        Account.any_instance.expects(:expire_scoped_cache_key).with(:settings).once
        Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_fields).once
        Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_forms).once

        field.save!
      end
    end
  end
  #
  #  describe "FieldTextarea" do
  #    should_eventually "have tests"
  #  end
  #
  #  describe "FieldTicketType" do
  #    should_eventually "have tests"
  #  end
  #
  #  describe "FieldSubject" do
  #    should_eventually "have tests"
  #  end
  #

  describe "FieldRegexp" do
    it "validates a supported regex" do
      field = FieldRegexp.create(account: accounts(:minimum), title: 'fancy', regexp_for_validation: "(\\d{1,2})\\/(\\d{1,2})\\/(\\d{4})")
      assert(field.valid?)
    end
    it "does not yet allow 1.9 extensions that are unsupported in 1.8" do
      field = FieldRegexp.create(account: accounts(:minimum), title: 'fancy', regexp_for_validation: "(?<month>\\d{1,2})\\/(?<day>\\d{1,2})\\/(?<year>\\d{4})")
      assert_equal false, field.valid?
    end
  end

  describe "FieldGroup" do
    it "does not allow disabling" do
      field = accounts(:minimum).field_group
      field.is_active = false
      refute field.save
    end
  end

  describe "FieldDate" do
    let(:field) { FieldDate.create!(account: accounts(:minimum), title: 'date') }

    it "validates against potential XSS" do
      entry = field.ticket_field_entries.new(value: "<script>alert('hi')</script>\n2013-01-01", account: field.account)
      refute entry.valid?
    end
  end

  describe "FieldInteger" do
    let(:field) { FieldInteger.create(account: accounts(:minimum), title: 'hej') }

    it "provides a validation regexp" do
      assert_equal '\A[-+]?\d+\z', field.regexp_for_validation
    end

    it "validates against potential XSS" do
      entry = field.ticket_field_entries.new(value: "<script>alert('hi')</script>\n2", account: field.account)
      refute entry.valid?
    end
  end

  describe "FieldPriority" do
    it "allows change of portal visibility" do
      field = accounts(:minimum).field_priority
      field.is_visible_in_portal = true
      assert field.save
      assert field.reload.is_visible_in_portal
    end

    it "defaults its sub_type_id to FULL" do
      field = accounts(:minimum).field_priority
      field.sub_type_id = nil
      field.save

      assert_equal field.sub_type_id, PrioritySet.FULL
    end

    it "allows mutation of sub_type_id" do
      field = accounts(:minimum).field_priority
      field.sub_type_id = PrioritySet.BASIC
      field.save

      assert_equal field.sub_type_id, PrioritySet.BASIC
    end

    describe "when the field is used as a parent in conditions" do
      let(:condition) { FactoryBot.create(:ticket_field_condition, :priority_parent, value: PriorityType.LOW) }
      let(:priority_field) { condition.parent_field }

      describe "and CTF is enabled" do
        before do
          condition.account.stubs(:has_native_conditional_fields_enabled?).returns(true)
        end

        it "allows mutation of sub_type_id to FULL" do
          priority_field.update_attribute(:sub_type_id, PrioritySet.BASIC)

          priority_field.sub_type_id = PrioritySet.FULL
          assert priority_field.valid?
        end

        it "should allow mutation to BASIC when the condition uses a value that is part of that type" do
          condition.update_attribute(:value, PriorityType.NORMAL)
          priority_field.update_attribute(:sub_type_id, PrioritySet.FULL)

          priority_field.sub_type_id = PrioritySet.BASIC
          assert priority_field.valid?
        end

        it "should not allow mutation to BASIC when the condition value is 'Low'" do
          priority_field.update_attribute(:sub_type_id, PrioritySet.FULL)

          priority_field.sub_type_id = PrioritySet.BASIC

          refute priority_field.valid?
          assert_equal priority_field.errors[:base].first, I18n.t('txt.admin.models.ticket_field.ticket_field.priority_type_cannot_be_changed_by_condition')
        end

        it "should not allow mutation to BASIC when the condition value is 'Urgent'" do
          condition.update_attribute(:value, PriorityType.URGENT)
          priority_field.update_attribute(:sub_type_id, PrioritySet.FULL)

          priority_field.sub_type_id = PrioritySet.BASIC

          refute priority_field.valid?
          assert_equal priority_field.errors[:base].first, I18n.t('txt.admin.models.ticket_field.ticket_field.priority_type_cannot_be_changed_by_condition')
        end
      end

      describe "and CTF is disabled" do
        before do
          condition.account.stubs(:has_native_conditional_fields_enabled?).returns(false)
        end

        it "allows mutation of sub_type_id to BASIC" do
          priority_field.update_attribute(:sub_type_id, PrioritySet.FULL)

          priority_field.sub_type_id = PrioritySet.BASIC
          assert priority_field.valid?
        end
      end
    end
  end

  describe "FieldStatus" do
    it "is unique" do
      field_status = FieldStatus.create(account: accounts(:minimum), title: 'hej')
      assert !field_status.errors.empty?
      assert field_status.errors.full_messages.join(' ') =~ /Type FieldStatus has already been taken/
    end

    describe "existing field" do
      subject { accounts(:minimum).field_status }

      it "allows change of portal visibility" do
        subject.is_visible_in_portal = true
        assert subject.save
        assert subject.reload.is_visible_in_portal
      end

      it "does not be destroyable" do
        subject.destroy
        assert_not_nil subject.reload
      end
    end

    it "defaults its sub_type_id to 0" do
      field = accounts(:minimum).field_status
      field.sub_type_id = nil
      field.save

      assert_equal field.sub_type_id, 0
    end

    it "allows mutation of sub_type_id" do
      field = accounts(:minimum).field_status
      field.sub_type_id = not_default = 1
      field.save

      assert_equal field.sub_type_id, not_default
    end
  end

  describe "FieldTagger" do
    describe "save cache expiration" do
      it "only expires cache once" do
        tagger = FieldTagger.new(title: 'test')
        tagger.stubs(id: 123)
        tagger.account = accounts(:minimum)
        tagger.is_required = false

        [{'name' => 'hej title', 'value' => 'hej'}, {'name' => 'hello title', 'value' => 'hello'}].each do |cfo|
          tagger.custom_field_options.build(cfo)
        end

        Account.any_instance.expects(:expire_scoped_cache_key).with(:settings).once
        Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_fields).once
        Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_forms).once
        Account.any_instance.expects(:expire_scoped_cache_key).with(:field_taggers).once
        Account.any_instance.expects(:expire_scoped_cache_key).with([:field_tagger, 123]).once

        tagger.save!
      end

      it "expires the specific cache of the updated field" do
        tagger = ticket_fields(:field_tagger_custom)
        tagger.title = 'New title'

        Account.any_instance.expects(:expire_scoped_cache_key).with(:settings).once
        Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_fields).once
        Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_forms).once
        Account.any_instance.expects(:expire_scoped_cache_key).with(:field_taggers).once
        Account.any_instance.expects(:expire_scoped_cache_key).with([:field_tagger, tagger.id]).once

        tagger.save!
      end
    end

    it "persists options and values" do
      tagger = FieldTagger.new(title: 'test')
      tagger.account = accounts(:minimum)
      tagger.is_required = false

      [{'name' => 'hej title', 'value' => 'hej'}, {'name' => 'hello title', 'value' => 'hello'}].each do |cfo|
        tagger.custom_field_options.build(cfo)
      end

      tagger.save!

      assert_equal 2, tagger.custom_field_options.size
      assert_equal ['hej', 'hello'].sort, tagger.custom_field_options.map(&:value).sort
    end

    describe "limit number of fields on a default form" do
      let(:tagger) { FieldTagger.new(title: 'test', account: accounts(:minimum)) }
      let(:account) { accounts(:minimum) }
      let!(:ticket_form) do
        params = {
          ticket_form: {
            name: "default wombat",
            display_name: "default wombat",
            position: 1,
            default: true,
            active: true,
            end_user_visible: true
          }
        }
        default_form = Zendesk::TicketForms::Initializer.new(account, params).ticket_form
        default_form.save!
      end

      describe_with_arturo_enabled :ticket_field_limits do
        it "validates less than 400" do
          tagger.account.ticket_fields.stubs(:count).returns(400)
          tagger.custom_field_options.build('name' => 'hej title', 'value' => 'haps')

          expected_response = {
            error: "RecordInvalid",
            description: "Record validation errors",
            details: {
              base: [
                { description: "must have less than 400 ticket fields on a form" }
              ]
            }
          }

          refute tagger.save
          assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(tagger)
        end
      end
    end

    it "validates format of options" do
      tagger = FieldTagger.new(title: 'test')
      tagger.account = accounts(:minimum)
      tagger.is_required = false

      [{'name' => 'hej title', 'value' => 'hej'}, {'name' => 'hello title', 'value' => 'hello?there'}].each do |cfo|
        tagger.custom_field_options.build(cfo)
      end

      expected_response = {
        error: "RecordInvalid",
        description: "Record validation errors",
        details: {
          field_options: [
            { error: "ValueNotAccepted", description: "Field options: cannot contain special characters. Use only underscore and regular characters." }
          ]
        }
      }

      refute tagger.save
      assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(tagger)
    end

    it "prevents multiple tags from being saved" do
      tagger = FieldTagger.new(title: 'test')
      tagger.account = accounts(:minimum)
      tagger.is_required = false

      [{'name' => 'hej title', 'value' => 'hej'}, {'name' => 'hello title', 'value' => 'hello there'}].each do |cfo|
        tagger.custom_field_options.build(cfo)
      end

      expected_response = {
        error: "RecordInvalid",
        description: "Record validation errors",
        details: {
          field_options: [
            { error: "ValueNotAccepted", description: "Field options: cannot contain special characters. Use only underscore and regular characters." }
          ]
        }
      }

      refute tagger.save
      assert_equal expected_response, Api::V2::ErrorsPresenter.new.present(tagger)
    end

    it "handles trailing whitespace in tag values from " do
      tagger = FieldTagger.new(title: 'test')
      tagger.account = accounts(:minimum)
      tagger.is_required = false

      [{'name' => 'hej title', 'value' => 'hej '}, {'name' => 'hello title', 'value' => 'hello'}].each do |cfo|
        tagger.custom_field_options.build(cfo)
      end

      assert tagger.save
    end

    it "ensures unique tag names among checkboxes and taggers" do
      FieldCheckbox.create!(title: 'test', tag: 'haps', account: accounts(:minimum))
      field_tagger = ticket_fields(:field_tagger_custom)
      field_tagger.custom_field_options.build('name' => 'hej title', 'value' => 'haps')
      refute field_tagger.save
      assert_equal 'The tag <strong>haps</strong> is already used in a custom field drop-down, multi-select or checkbox.', field_tagger.errors.full_messages.first
    end

    it "allows tag collision if the collision is with soft deleted options" do
      checkbox = FieldCheckbox.create(title: 'test', tag: 'booring', account: accounts(:minimum))
      # 'booring' tag is already being used by a custom_field_option
      refute checkbox.save

      # Soft delete the existing custom_field_option that uses the 'booring' tag
      field_tagger = ticket_fields(:field_tagger_custom)
      field_tagger.custom_field_options.where(value: 'booring').first.soft_delete!
      assert field_tagger.save

      assert checkbox.save
    end

    it "allows updates of the title" do
      ticket_fields(:field_tagger_custom).update_attributes(title: 'hej')
      assert_equal 'hej', ticket_fields(:field_tagger_custom).title
    end

    it "gracefully handles duplicates at the database level with callback #filter_key_constraint_violation" do
      # Ok heres the deal. Originally this test meant to test that we had a unique index on the custom_field_option table and also that #filter_key_constraint_violation callback
      # would translate any KeyConstraintViolations into human readable error text. This test likely got put here because the module that the above callback
      # lives in has no test file of its own.
      # In the case that this arturo is enabled, we do a direct insert on duplicate key, which won't raise a KeyConstraintViolation
      # since it does an update of the row if it's already in the table.
      Arturo.disable_feature!(:bulk_insert_or_update_custom_field_options)
      field_tagger = ticket_fields(:field_tagger_custom)
      field_tagger.custom_field_options.build('name' => 'hilarious dupe', 'value' => 'hilarious')
      refute field_tagger.save(validate: false)
      assert_equal 'Field options:  must each have a unique tag. The tag <strong>hilarious</strong> is currently used more than once.', field_tagger.errors.full_messages.first
    end

    it "allows adding additional options" do
      field_tagger = ticket_fields(:field_tagger_custom)
      old_custom_field_options_count = field_tagger.custom_field_options.size

      [{'name' => 'hej title', 'value' => 'hej'}, {'name' => 'hello title', 'value' => 'hello'}].each do |cfo|
        field_tagger.custom_field_options.build(cfo)
      end

      assert field_tagger.save
      assert_equal old_custom_field_options_count + 2, field_tagger.custom_field_options.size
    end

    it "is destroyable" do
      assert_difference("TicketField.count(:all)", -1) do
        assert ticket_fields(:field_tagger_custom).destroy
      end
    end

    it "retrives correct tagger value and tagger value title for a ticket" do
      tagger = ticket_fields(:field_tagger_custom)
      tickets(:minimum_1).current_tags = "hilarious"
      tickets(:minimum_1).will_be_saved_by(tickets(:minimum_1).submitter)
      tickets(:minimum_1).save

      value = tagger.value(tickets(:minimum_1).reload)
      assert_equal "hilarious", value
      assert_equal "Hilarious", tagger.humanize_value(value)
    end

    it "is set when the ticket gets the tag" do
      ticket       = tickets(:minimum_1)
      ticket_field = ticket_fields(:field_tagger_custom)

      ticket_field_entry = ticket.ticket_field_entries.find_by_ticket_field_id(ticket_field.id)
      assert_nil ticket_field_entry

      ticket.current_tags = "hilarious"
      ticket.will_be_saved_by(tickets(:minimum_1).submitter)
      ticket.save!
      ticket.reload

      ticket_field_entry = ticket.ticket_field_entries.find_by_ticket_field_id(ticket_field.id)
      assert ticket_field_entry
      assert_equal "hilarious", ticket_field_entry.value

      ticket.current_tags = "booring"
      ticket.will_be_saved_by(tickets(:minimum_1).submitter)
      ticket.save!
      ticket.reload

      ticket_field_entry = ticket.ticket_field_entries.find_by_ticket_field_id(ticket_field.id)
      assert ticket_field_entry
      assert_equal "booring", ticket_field_entry.value
    end
  end

  describe "FieldCheckbox" do
    it "validates format of tag" do
      checkbox = FieldCheckbox.create(title: 'test', tag: 'hello?there', account: accounts(:minimum))
      assert_equal 'Tag cannot contain special characters. Use only underscore and regular characters.', checkbox.errors.full_messages.first
    end

    it "ensures unique tag names among checkboxes" do
      FieldCheckbox.create!(title: 'test1', tag: 'hello', account: accounts(:minimum))
      checkbox2 = FieldCheckbox.create(title: 'test2', tag: 'hello', account: accounts(:minimum))
      assert_equal 'The tag <strong>hello</strong> is already used in a custom field drop-down, multi-select or checkbox.', checkbox2.errors.full_messages.first
    end

    it "ensures unique tag names among checkboxes and taggers" do
      FieldCheckbox.create!(title: 'test1', tag: 'hello', account: accounts(:minimum))
      checkbox = FieldCheckbox.create(title: 'test', tag: 'booring', account: accounts(:minimum).reload)
      assert_equal 'The tag <strong>booring</strong> is already used in a custom field drop-down, multi-select or checkbox.', checkbox.errors.full_messages.first
    end
  end

  describe "serialization" do
    before do
      @ticket_field = TicketField.new(title: "A Generic Ticket Field")
    end

    describe "in JSON" do
      it "include its type" do
        @ticket_field.expects(:type).returns("TicketField")
        @ticket_field.to_json.must_include "TicketField"
      end
    end

    describe "in XML" do
      it "include its type" do
        @ticket_field.expects(:type).returns("TicketField")
        @ticket_field.to_xml.must_include "TicketField"
      end
    end
  end

  describe "TicketField app methods" do
    let (:app_ticket_field) { ticket_fields(:field_integer_custom) }
    let(:url) do
      "https://minimum.zendesk-test.com/api/v2/apps/requirements.json?" \
      "includes=installation"
    end

    let(:response) do
      {
        "requirements" => [
          {
            "requirement_id"   => app_ticket_field.id,
            "requirement_type" => "ticket_fields",
            "installation"     => {"settings" => {"title" => "Wombat App"}, "app_id" => 22222}
          }
        ]
      }
    end

    before do
      # Makes this ticket field part of a resource collection, important because
      # app ticket fields are tracked as part of a resource collection so that
      # is_apps_requirement? returns true
      FactoryBot.create(:resource_collection_resource,
        account_id: app_ticket_field.account.id,
        resource: app_ticket_field)
    end

    describe "#creator_app_id" do
      it "returns nil if the ticket field is not associated to an app" do
        ticket_field = ticket_fields(:field_decimal_custom)
        assert_nil ticket_field.creator_app_id
      end

      it "does not blow up if app market api call errors out " do
        stub_request(:get, url).to_raise(StandardError)
        assert_nil app_ticket_field.creator_app_id
      end

      it "returns the app_id of the app the ticket field was created by if it belongs to an app" do
        stub_request(:get, url).to_return(body: response.to_json, headers: {'content-type' => 'application/json'})
        assert_equal 22222, app_ticket_field.creator_app_id
      end

      it "caches the call to app market for app names" do
        stub_request(:get, url).to_return(body: response.to_json, headers: {'Content-Type' => 'application/json'}).times(1)
        app_ticket_field.creator_app_id
        app_ticket_field.creator_app_id
        assert_requested(:get, url, times: 1)
      end

      it "expires the cache if a new ticket_field is added" do
        # First request to app requirements api which will get cached
        stub_request(:get, url).to_return(body: response.to_json, headers: {'Content-Type' => 'application/json'}).times(1)
        app_ticket_field.creator_app_id

        # Create a new ticket field on the account, this should invalidate the cache
        app_ticket_field2 = TicketField.new(title: "Lemur App Field", account: app_ticket_field.account)
        app_ticket_field2.type = 'FieldInteger'
        app_ticket_field2.save!
        Timecop.travel(Time.now + 1.seconds)
        FactoryBot.create(:resource_collection_resource,
          account_id: app_ticket_field2.account.id,
          resource: app_ticket_field2)
        app_ticket_field2.reload

        response2 =
          {
            "requirements" => [
              {
                "requirement_id"   => app_ticket_field.id,
                "requirement_type" => "ticket_fields",
                "installation"     => {"settings" => {"title" => "Wombat App"}, app_id: '33333'}
              },
              {
                "requirement_id"   => app_ticket_field2.id,
                "requirement_type" => "ticket_fields",
                "installation"     => {"settings" => {"title" => "Lemur App"}, app_id: '35111'}
              },
            ]
          }
        stub_request(:get, url).to_return(body: response2.to_json, headers: {'Content-Type' => 'application/json'}).times(1)

        # Second request made which will make a new call since the cache has been invalidated
        assert_equal "33333", app_ticket_field.creator_app_id
        assert_equal "35111", app_ticket_field2.creator_app_id
        assert_requested(:get, url, times: 2)
      end
    end

    describe "#creator_app_name" do
      it "returns nil if the ticket field is not associated to an app" do
        ticket_field = ticket_fields(:field_decimal_custom)
        assert_nil ticket_field.creator_app_name
      end

      it "does not blow up if app market api call errors out " do
        stub_request(:get, url).to_raise(StandardError)
        assert_nil app_ticket_field.creator_app_name
      end

      it "returns the name of the app the ticket field was created by if it belongs to an app" do
        stub_request(:get, url).to_return(body: response.to_json, headers: {'content-type' => 'application/json'})
        assert_equal "Wombat App", app_ticket_field.creator_app_name
      end

      it "caches the call to app market for app names" do
        stub_request(:get, url).to_return(body: response.to_json, headers: {'Content-Type' => 'application/json'}).times(1)
        app_ticket_field.creator_app_name
        app_ticket_field.creator_app_name
        assert_requested(:get, url, times: 1)
      end

      it "expires the cache if a new ticket_field is added" do
        # First request to app requirements api which will get cached
        stub_request(:get, url).to_return(body: response.to_json, headers: {'Content-Type' => 'application/json'}).times(1)
        app_ticket_field.creator_app_name

        # Create a new ticket field on the account, this should invalidate the cache
        app_ticket_field2 = TicketField.new(title: "Lemur App Field", account: app_ticket_field.account)
        app_ticket_field2.type = 'FieldInteger'
        app_ticket_field2.save!
        Timecop.travel(Time.now + 1.seconds)
        FactoryBot.create(:resource_collection_resource,
          account_id: app_ticket_field2.account.id,
          resource: app_ticket_field2)
        app_ticket_field2.reload

        response2 =
          {
            "requirements" => [
              {
                "requirement_id"   => app_ticket_field.id,
                "requirement_type" => "ticket_fields",
                "installation"     => {"settings" => {"title" => "Wombat App"}}
              },
              {
                "requirement_id"   => app_ticket_field2.id,
                "requirement_type" => "ticket_fields",
                "installation"     => {"settings" => {"title" => "Lemur App"}}
              },
            ]
          }
        stub_request(:get, url).to_return(body: response2.to_json, headers: {'Content-Type' => 'application/json'}).times(1)

        # Second request made which will make a new call since the cache has been invalidated
        assert_equal "Wombat App", app_ticket_field.creator_app_name
        assert_equal "Lemur App", app_ticket_field2.creator_app_name
        assert_requested(:get, url, times: 2)
      end
    end

    describe "#app_field?" do
      it "returns false if the ticket field is not associated with an app" do
        ticket_field = ticket_fields(:field_decimal_custom)
        assert !ticket_field.app_field?
      end
      it "returns true if the ticket field is associated with an app" do
        stub_request(:get, url).to_return(body: response.to_json, headers: {'content-type' => 'application/json'})
        assert app_ticket_field.app_field?
      end
    end
  end
end
