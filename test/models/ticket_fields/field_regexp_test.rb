require_relative "../../support/test_helper"

SingleCov.covered!

describe 'FieldRegexp' do
  fixtures :accounts, :custom_field_options, :ticket_fields

  describe "field regexp" do
    subject do
      FieldRegexp.new(title: "test field", account: accounts(:minimum), regexp_for_validation: regexp)
    end

    describe "length > 255" do
      let(:regexp) do
        "^([a-z0-9]([-a-z0-9]*[a-z0-9])?\\\\.)+((a[cdefgilmnoqrstuwxz]|aero|arpa)|(b[abdefghijmnorstvwyz]|biz)|(c[acdfghiklmnorsuvxyz]|cat|com|coop)|d[ejkmoz]|(e[ceghrstu]|edu)|f[ijkmor]|(g[abdefghilmnpqrstuwy]|gov)|h[kmnrtu]|(i[delmnoqrst]|info|int)|(j[emop]|jobs)|k[eghimnprwyz]|l[abcikrstuvy]|(m[acdghklmnopqrstuvwxyz]|mil|mobi|museum)|(n[acefgilopruz]|name|net)|(om|org)|(p[aefghklmnrstwy]|pro)|qa|r[eouw]|s[abcdeghijklmnortvyz]|(t[cdfghjklmnoprtvwz]|travel)|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw])$"
      end

      it "is invalid" do
        assert subject.invalid?, subject.errors.inspect
      end
    end

    describe "invalid regexp" do
      let(:regexp) { "^[a-z0-9]+(hello$" }

      it "is invalid" do
        assert subject.invalid?, subject.errors.inspect
      end
    end

    describe "valid regexp" do
      let(:regexp) { "^steven$" }

      it "is valid" do
        assert subject.valid?, subject.errors.inspect
      end
    end

    describe "nil" do
      let(:regexp) { nil }

      it "is invalid" do
        assert subject.invalid?, subject.errors.inspect
      end
    end
  end
end
