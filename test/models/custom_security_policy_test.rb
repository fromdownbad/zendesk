require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe CustomSecurityPolicy do
  fixtures :accounts

  describe ".build_from_current_policy" do
    let(:account) { accounts(:minimum) }

    it "copies low policy if policy is low" do
      policy = CustomSecurityPolicy.build_from_current_policy(account)
      assert_equal 0, policy.password_history_length
    end

    it "copies a high policy if policy is custom" do
      account.role_settings.agent_security_policy_id = Zendesk::SecurityPolicy::Custom.id
      policy = CustomSecurityPolicy.build_from_current_policy(account)
      assert_equal Zendesk::SecurityPolicy::High.password_history_length, policy.password_history_length
    end
  end

  describe ".update_or_build_custom_policy_from_non_custom_policy" do
    non_custom_policies = [Zendesk::SecurityPolicy::Low, Zendesk::SecurityPolicy::Medium, Zendesk::SecurityPolicy::High]
    let(:account) { accounts(:minimum) }

    non_custom_policies.each do |non_custom_policy|
      it "copies #{non_custom_policy} to custom" do
        policy = CustomSecurityPolicy.update_or_build_custom_policy_from_non_custom_policy(non_custom_policy.id, account)
        assert_equal non_custom_policy.password_length, policy.password_length
      end
    end
  end

  describe "policy level" do
    before do
      @account = accounts(:minimum)
      @custom_security_policy = CustomSecurityPolicy.build_from_current_policy(@account)
      @custom_security_policy.save
    end

    it "is increased if password length is bigger" do
      @custom_security_policy.update_attribute(:password_length, 10)
      assert_equal (Zendesk::SecurityPolicy::Custom.id + 1), @custom_security_policy.policy_level
    end

    it "does not be increased if password length is smaller" do
      @custom_security_policy.update_attribute(:password_length, 2)
      assert_equal Zendesk::SecurityPolicy::Custom.id, @custom_security_policy.policy_level
    end

    it "is increased if password complexity is higher" do
      @custom_security_policy.update_attribute(:password_complexity, CustomSecurityPolicy::SPECIAL_COMPLEXITY)
      assert_equal (Zendesk::SecurityPolicy::Custom.id + 1), @custom_security_policy.policy_level
    end

    it "does not be increased if password complexity is the same" do
      @custom_security_policy.update_attribute(:password_complexity, @custom_security_policy.password_complexity)
      assert_equal Zendesk::SecurityPolicy::Custom.id, @custom_security_policy.policy_level
    end

    it "is increased if password must be in mixed case now" do
      @custom_security_policy.update_attribute(:password_in_mixed_case, true)
      assert_equal (Zendesk::SecurityPolicy::Custom.id + 1), @custom_security_policy.policy_level
    end
  end
end
