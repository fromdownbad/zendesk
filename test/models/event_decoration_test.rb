require_relative '../support/test_helper'

SingleCov.covered!

describe 'EventDecoration' do
  fixtures :accounts, :tickets, :events

  def create_event_decoration(account, ticket, event, data = nil)
    EventDecoration.new(ticket: ticket, event: event, data: data).tap do |event_decoration|
      event_decoration.account = account
      event_decoration.save!
    end
  end

  describe '#EventDecoration' do
    before do
      @account = accounts(:minimum)
      @event   = events(:duplicate_text_for_minimum_ticket_1)
    end

    describe 'creating an EventDecoration' do
      it 'throws validation error if account is not set' do
        assert_raise(ActiveRecord::RecordInvalid) do
          create_event_decoration(nil, @event.ticket, @event)
        end
      end

      it 'throws validation error if ticket is not set' do
        assert_raise(ActiveRecord::RecordInvalid) do
          create_event_decoration(@account, nil, @event)
        end
      end

      it 'throws validation error if event is not set' do
        assert_raise(ActiveRecord::RecordInvalid) do
          create_event_decoration(@account, @event.ticket, nil)
        end
      end

      it 'succeeds if required info supplied' do
        create_event_decoration(@account, @event.ticket, @event)
      end
    end

    describe 'inactive?' do
      it 'should return the value of data.inactive' do
        @event_decoration = create_event_decoration(@account, @event.ticket, @event)
        refute @event_decoration.inactive?
        @event_decoration.deactivate
        assert @event_decoration.inactive?
      end
    end

    describe '#deactivate' do
      describe 'when inactive is not defined' do
        before do
          @event_decoration = create_event_decoration(@account, @event.ticket, @event)
          assert_nil @event_decoration.data.inactive
          @event_decoration.deactivate
        end

        it 'marks the state inactive' do
          assert @event_decoration.reload.data.inactive
        end
      end

      describe 'when state is active' do
        before do
          @event_decoration = create_event_decoration(
            @account,
            @event.ticket,
            @event,
            inactive: true
          )

          @event_decoration.deactivate
        end

        it 'marks the state inactive' do
          assert @event_decoration.data.inactive
        end
      end
    end
  end
end
