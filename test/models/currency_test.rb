require_relative "../support/test_helper"

SingleCov.covered! uncovered: 16

describe Currency do
  fixtures :currencies

  should validate_uniqueness_of :code
  should validate_presence_of :rate
end
