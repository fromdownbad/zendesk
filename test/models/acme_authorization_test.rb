require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe AcmeAuthorization do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:registration) { account.acme_registrations.build }
  let(:authorization) { account.acme_authorizations.build(acme_registration: registration, identifier: account.host_mapping) }

  before do
    registration.save!
    account.update_attribute(:host_mapping, 'support.example.com')
  end

  after do
    VCR.eject_cassette
  end

  describe 'lets_encrypt_acme_v2 arturo on' do
    before { Arturo.enable_feature!(:lets_encrypt_acme_v2) }

    describe "creating an authorization" do
      it "sets the state to pending" do
        authorization.save!
        assert_equal 'pending', authorization.state
      end
    end

    describe "is_authorized_v2?" do
      it "returns false unless we have requested authorization" do
        assert_nil authorization.challenge_token
        assert_equal false, authorization.is_authorized_v2?
      end

      describe "when successful" do
        before do
          authorization.stubs(:authorization_requested?).returns(true)
          authorization.update_attributes!(accesses: 1)
          authorization.stubs(current_authorization_status: 'valid')
        end

        it "returns true if LE verified our authorization" do
          assert(authorization.is_authorized_v2?)
          assert_equal 'valid', authorization.state
        end
      end

      describe "when failed" do
        let(:acme_challenge) { stub }

        before do
          authorization.stubs(:authorization_requested?).returns(true)
          authorization.update_attributes!(accesses: 1)
          AcmeV2::Client.any_instance.stubs(:challenge).returns(acme_challenge)
        end

        it "raises an AuthorizationFailedError if LE returns an error" do
          assert_raise(Zendesk::AcmeJob::AuthorizationFailedError) do
            authorization.expects(current_authorization_status: 'error').twice
            authorization.is_authorized_v2?
          end
        end

        it "requests verification again if LE returns pending" do
          acme_challenge.expects(:reload).returns(true).at_least_once
          authorization.expects(current_authorization_status: 'pending').at_least_once
          authorization.expects(:request_verification_v2!).once
          authorization.is_authorized_v2?
        end
      end

      describe "when invalid" do
        before do
          authorization.stubs(:authorization_requested?).returns(true)
          authorization.update_attributes!(accesses: 1)
        end

        it "returns false" do
          authorization.expects(current_authorization_status: 'invalid').at_least_once
          refute authorization.is_authorized_v2?
        end
      end

      describe "when LE deletes the challenge" do
        before do
          authorization.stubs(:authorization_requested?).returns(true)
          authorization.update_attributes!(accesses: 1)
        end

        it "returns false" do
          authorization.expects(current_authorization_status: 'deleted').at_least_once
          refute authorization.is_authorized_v2?
        end
      end
    end

    describe "uniqueness" do
      before { assert authorization.save }

      it "does not allow multiple registrations per account" do
        refute account.acme_authorizations.build(acme_registration: registration, identifier: account.host_mapping).save
      end

      it "allows a second if the first registrations is soft deleted" do
        authorization.destroy
        assert account.acme_authorizations.build(acme_registration: registration, identifier: account.host_mapping).save
      end
    end
  end

  describe 'lets_encrypt_acme_v2 arturo off' do
    before { Arturo.disable_feature!(:lets_encrypt_acme_v2) }

    describe "creating an authorization" do
      it "sets the state to pending" do
        authorization.save!
        assert_equal 'pending', authorization.state
      end

      describe "requesting authorization" do
        describe "and the registration succeeds" do
          before do
            VCR.turn_on!
            VCR.insert_cassette("acme_authorization_v2", allow_unused_http_interactions: false)
            Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)
            registration.register!
            authorization.attempt_to_authorize!
          end

          after { VCR.eject_cassette }

          it "marks the authorization as in progress" do
            assert_equal 'verifying', authorization.state
          end

          it "stores the challenge infomation" do
            # if updating the cassette use
            # puts authorization.attributes.inspect
            # to update values below
            assert_equal 'http-01', authorization.challenge_type
            assert_equal "https://acme-staging.api.letsencrypt.org/acme/chall-v3/19064757/6D560w", authorization.challenge_uri
            assert_equal "__zcKN6aUbqt_W5tdqon2CPe6zVVFLLBRta2R07hf1w", authorization.challenge_token
            assert authorization.challenge_content.start_with?('__zcKN6aUbqt_W5tdqon2CPe6zVVFLLBRta2R07hf1w')
          end
        end

        describe "and the authorization fails" do
          before do
            VCR.turn_on!
            VCR.insert_cassette("acme_registration_v6", allow_unused_http_interactions: false)
            Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)
            registration.register!
            Acme::Client.any_instance.stubs(:authorize).with(domain: account.host_mapping).raises(Acme::Client::Error).once
          end

          it "raises an Zendesk::AcmeJob::AuthorizationError on errors" do
            assert_raise Zendesk::AcmeJob::AuthorizationError do
              authorization.attempt_to_authorize!
            end
          end
        end
      end

      describe "is_authorized?" do
        it "returns false unless we have requested authorization" do
          assert_nil authorization.challenge_token
          assert_equal false, authorization.is_authorized?
        end

        describe "when successful" do
          before do
            VCR.turn_on!
            VCR.insert_cassette("acme_authorization_successful_v2", allow_unused_http_interactions: false)
            Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)
            registration.register!
            authorization.attempt_to_authorize!
            authorization.update_attributes!(accesses: 1)
            authorization.stubs(current_authorization_status: 'valid').once
          end

          it "returns true if LE verified our authorization" do
            assert(authorization.is_authorized?)
            assert_equal 'valid', authorization.state
          end
        end

        describe "when failed" do
          before do
            VCR.turn_on!
            VCR.insert_cassette("acme_authorization_failed_v2", allow_unused_http_interactions: false)
            Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)
            registration.register!
            authorization.attempt_to_authorize!
            authorization.update_attributes!(accesses: 1)
          end

          it "raises an AuthorizationFailedError if LE returns an error" do
            assert_raise(Zendesk::AcmeJob::AuthorizationFailedError) do
              authorization.expects(current_authorization_status: 'error').once
              authorization.is_authorized?
            end
          end

          it "requests verification again if LE returns pending" do
            authorization.expects(current_authorization_status: 'pending').once
            authorization.expects(:request_verification!).once
            authorization.is_authorized?
          end
        end

        describe "when invalid" do
          before do
            VCR.turn_on!
            VCR.insert_cassette("acme_authorization_invalid_v2", allow_unused_http_interactions: false)
            Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)
            registration.register!
            authorization.attempt_to_authorize!
            authorization.update_attributes!(accesses: 1)
          end

          it "resets and requests verification again if LE returns invalid" do
            old_token = authorization.challenge_token
            authorization.expects(current_authorization_status: 'invalid').once
            authorization.expects(:request_verification!).once
            authorization.is_authorized?
            refute_equal old_token, authorization.challenge_token
          end
        end

        describe "when LE revokes the challenge" do
          before do
            VCR.turn_on!
            VCR.insert_cassette("acme_authorization_revoked_v2", allow_unused_http_interactions: false)
            Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)
            registration.register!
            Acme::Client::Resources::Challenges::HTTP01.any_instance.stubs(:request_verification).raises(Acme::Client::Error::Malformed, 'Response does not complete challenge')
          end

          it "requests a token" do
            assert_raises(Zendesk::AcmeJob::AuthorizationError) { authorization.attempt_to_authorize!(verify_now: true) }
            old_token = authorization.challenge_token
            assert_raises(Zendesk::AcmeJob::AuthorizationError) { authorization.attempt_to_authorize!(verify_now: true) }
            # LE now returns older valid tokens - https://community.letsencrypt.org/t/upcoming-change-valid-authz-reuse/16982
            assert_equal old_token, authorization.challenge_token
            assert_equal authorization.challenge_token, authorization.send(:acme_challenge).token
          end
        end

        describe "when LE deletes the challenge" do
          before do
            VCR.turn_on!
            VCR.insert_cassette("acme_authorization_deleted_v2", allow_unused_http_interactions: false)
            Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)
            registration.register!
            authorization.attempt_to_authorize!
            authorization.update_attributes!(accesses: 1)
          end

          it "recovers and requests a new token" do
            Acme::Client::Resources::Challenges::HTTP01.any_instance.stubs(:verify_status).raises(Acme::Client::Error::NotFound, 'not found')
            Acme::Client::Resources::Challenges::HTTP01.any_instance.stubs(:request_verification).raises(Acme::Client::Error::NotFound, 'not found')

            assert_equal 'deleted', authorization.send(:current_authorization_status)
            old_token = authorization.challenge_token

            assert_raises(Zendesk::AcmeJob::AuthorizationError) { authorization.is_authorized? }
            refute_equal old_token, authorization.challenge_token
          end
        end
      end
    end

    describe "uniqueness" do
      before { assert authorization.save }

      it "does not allow multiple registrations per account" do
        refute account.acme_authorizations.build(acme_registration: registration, identifier: account.host_mapping).save
      end

      it "allows a second if the first registrations is soft deleted" do
        authorization.destroy
        assert account.acme_authorizations.build(acme_registration: registration, identifier: account.host_mapping).save
      end
    end
  end
end
