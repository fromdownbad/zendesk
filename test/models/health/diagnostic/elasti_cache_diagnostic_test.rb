require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::ElastiCacheDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::ElastiCacheDiagnostic.new
  end

  describe '#resource' do
    before do
      @old_prop = ENV['PROP_ELASTICACHE_ENDPOINT']
      @old_kasket = ENV['KASKET_ELASTICACHE_ENDPOINT']
      @old_dalli = ENV['DALLI_ELASTICACHE_ENDPOINT']
    end

    after do
      ENV['PROP_ELASTICACHE_ENDPOINT'] = @old_prop
      ENV['KASKET_ELASTICACHE_ENDPOINT'] = @old_kasket
      ENV['DALLI_ELASTICACHE_ENDPOINT'] = @old_dalli
    end

    describe 'some env vars are set' do
      before do
        ENV['PROP_ELASTICACHE_ENDPOINT'] = 'test_one'
        ENV['KASKET_ELASTICACHE_ENDPOINT'] = 'test_one'
        ENV['DALLI_ELASTICACHE_ENDPOINT'] = 'test_two'
      end

      it 'returns env values, compacting duplicates' do
        assert_equal 'test_one, test_two', @diagnostic.resource
      end
    end

    describe 'no env vars set' do
      before do
        ENV.delete('PROP_ELASTICACHE_ENDPOINT')
        ENV.delete('KASKET_ELASTICACHE_ENDPOINT')
        ENV.delete('DALLI_ELASTICACHE_ENDPOINT')
      end

      it 'returns the zero length string' do
        assert_equal '', @diagnostic.resource
      end
    end
  end

  describe '#check' do
    before do
      @old_using_prop_elasticache = ENV['PROP_USE_ELASTICACHE']
      @old_using_kasket_elasticache = ENV['KASKET_USE_ELASTICACHE']
      @old_using_dalli_elasticache = ENV['USE_DALLI_ELASTICACHE']
      ENV.delete('PROP_USE_ELASTICACHE')
      ENV.delete('KASKET_USE_ELASTICACHE')
      ENV.delete('USE_DALLI_ELASTICACHE')
    end

    after do
      ENV['PROP_USE_ELASTICACHE'] = @old_using_prop_elasticache
      ENV['KASKET_USE_ELASTICACHE'] = @old_using_kasket_elasticache
      ENV['USE_DALLI_ELASTICACHE'] = @old_using_dalli_elasticache
    end

    describe 'not using elasticache' do
      it 'succeeds' do
        result = @diagnostic.check
        assert_equal 'ElastiCache not used', result.message
        assert result.passed
      end
    end

    describe 'using elasticache for props' do
      before do
        ENV['PROP_USE_ELASTICACHE'] = 'true'
        ENV['PROP_ELASTICACHE_ENDPOINT'] = 'test_endpoint'
      end

      describe 'no servers found' do
        before do
          client = stub('elasticache client', servers: [])
          Dalli::ElastiCache.expects(:new).with('test_endpoint').returns(client)
        end

        it 'reports error' do
          result = @diagnostic.check
          assert_equal "No ElastiCache servers found for props (test_endpoint)", result.message
          refute result.passed
        end
      end

      describe 'servers found' do
        before do
          client = stub('elasticache client', servers: ['first'])
          Dalli::ElastiCache.expects(:new).with('test_endpoint').returns(client)
        end

        it 'succeeds' do
          result = @diagnostic.check
          assert_equal 'Successfully validated ElastiCache', result.message
          assert result.passed
        end
      end
    end

    describe 'using elasticache for kasket' do
      before do
        ENV['KASKET_USE_ELASTICACHE'] = 'true'
        ENV['KASKET_ELASTICACHE_ENDPOINT'] = 'test_endpoint'
      end

      describe 'no servers found' do
        before do
          client = stub('elasticache client', servers: [])
          Dalli::ElastiCache.expects(:new).with('test_endpoint').returns(client)
        end

        it 'reports error' do
          result = @diagnostic.check
          assert_equal "No ElastiCache servers found for kasket (test_endpoint)", result.message
          refute result.passed
        end
      end

      describe 'servers found' do
        before do
          client = stub('elasticache client', servers: ['first'])
          Dalli::ElastiCache.expects(:new).with('test_endpoint').returns(client)
        end

        it 'succeeds' do
          result = @diagnostic.check
          assert_equal 'Successfully validated ElastiCache', result.message
          assert result.passed
        end
      end
    end

    describe 'using elasticache for dalli' do
      before do
        ENV['USE_DALLI_ELASTICACHE'] = 'true'
        ENV['DALLI_ELASTICACHE_ENDPOINT'] = 'test_endpoint'
      end

      describe 'no servers found' do
        before do
          client = stub('elasticache client', servers: [])
          Dalli::ElastiCache.expects(:new).with('test_endpoint').returns(client)
        end

        it 'reports error' do
          result = @diagnostic.check
          assert_equal "No ElastiCache servers found for dalli (test_endpoint)", result.message
          refute result.passed
        end
      end

      describe 'servers found' do
        before do
          client = stub('elasticache client', servers: ['first'])
          Dalli::ElastiCache.expects(:new).with('test_endpoint').returns(client)
        end

        it 'succeeds' do
          result = @diagnostic.check
          assert_equal 'Successfully validated ElastiCache', result.message
          assert result.passed
        end
      end
    end

    describe 'exception raised' do
      before do
        @diagnostic.stubs(:using_elasticache).raises(StandardError)
      end

      it 'reports error' do
        result = @diagnostic.check
        assert_equal 'ElastiCache check failed with error StandardError', result.message
        refute result.passed
      end
    end
  end
end
