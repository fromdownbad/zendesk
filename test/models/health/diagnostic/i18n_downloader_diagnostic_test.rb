require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::I18nDownloaderDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::I18nDownloaderDiagnostic.new
  end

  describe '#check' do
    describe 'i18n directory does not exist' do
      before do
        @diagnostic.stubs(:download_directory).returns('/not/a/legit/directory')
      end

      it 'fails' do
        result = @diagnostic.check
        assert_not result.passed
        assert_equal 'I18n directory expected at /not/a/legit/directory but not found', result.message
      end
    end

    describe 'i18n directory is empty' do
      it 'fails' do
        Dir.mktmpdir do |dir|
          @diagnostic.stubs(:download_directory).returns(dir)
          result = @diagnostic.check
          assert_not result.passed
          assert_equal "I18n directory #{dir} is empty", result.message
        end
      end
    end

    describe 'newest file in i18n directory is too old' do
      it 'fails' do
        Timecop.freeze
        Dir.mktmpdir do |dir|
          @diagnostic.stubs(:download_directory).returns(dir)
          Dir.chdir(dir) do
            FileUtils.touch 'example.txt', mtime: Time.now.utc - (@diagnostic.max_age_in_seconds + 1.0)
          end
          result = @diagnostic.check
          assert_not result.passed
          assert_equal "example.txt is the newest file in #{dir} and is #{@diagnostic.max_age_in_seconds + 1.0} seconds old, which is over the maximum allowed value (#{@diagnostic.max_age_in_seconds})", result.message
        end
      end
    end

    describe 'file operation raises an exception' do
      before do
        Dir.expects(:exist?).raises(StandardError)
      end

      it 'fails' do
        result = @diagnostic.check
        assert_not result.passed
        assert_equal "I18n downloader check failed with error StandardError", result.message
      end
    end

    describe 'newest file in i18n directory is not too old' do
      it 'succeeds' do
        Timecop.freeze
        Dir.mktmpdir do |dir|
          @diagnostic.stubs(:download_directory).returns(dir)
          Dir.chdir(dir) do
            FileUtils.touch 'too_old_example.txt', mtime: Time.now.utc - (@diagnostic.max_age_in_seconds + 1.0)
            FileUtils.touch 'not_too_old_example.txt', mtime: Time.now.utc - (@diagnostic.max_age_in_seconds - 1.0)
          end
          result = @diagnostic.check
          assert result.passed
          assert_equal "I18n downloads directory (#{dir}) contains fresh content", result.message
        end
      end
    end
  end

  describe '#download_directory' do
    it 'returns hard-coded value from library' do
      assert_equal '/data/zendesk_i18n/static', @diagnostic.download_directory
    end
  end

  describe '#max_age_in_seconds' do
    it 'returns hard-coded value' do
      assert_equal (4 * 60 * 60), @diagnostic.max_age_in_seconds
    end
  end
end
