require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::SupportRoutingAdminResourceDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::SupportRoutingAdminResourceDiagnostic.new
  end

  describe '#resource_name' do
    it 'returns routing-admin' do
      assert_equal 'routing-admin', @diagnostic.resource_name
    end
  end

  describe '#check_name' do
    it 'returns Support routing admin' do
      assert_equal 'Support routing admin', @diagnostic.check_name
    end
  end
end
