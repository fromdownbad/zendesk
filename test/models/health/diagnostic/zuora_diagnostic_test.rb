require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::ZuoraDiagnostic do
  describe 'ZendeskBillingCore::Zuora::Client method availability' do
    # To ensure the test fails when method name changes on
    # ZendeskBillingCore::Zuora::Client for `zuora_account`
    it 'has instance method #zuora_account' do
      assert ZendeskBillingCore::Zuora::Client.instance_methods.include?(:zuora_account)
    end
  end

  describe 'success' do
    fixtures :accounts, :users
    before do
      ZendeskBillingCore::Zuora::Configuration.expects(:username).at_least_once.returns('test_user')
      acct = stub('account', subscription: stub('sub', zuora_subscription: stub('zs', zuora_account_id: '123')))
      Account.expects(:find).with(1).returns(acct)
      ZendeskBillingCore::Zuora::Client.expects(:new).with('123').returns(stub('zc', zuora_account: true))
      zuora = Health::Diagnostic::ZuoraDiagnostic.new
      @diagnostic = zuora.check
    end

    describe '#name' do
      it 'returns Zuora' do
        assert_equal 'ZuoraDiagnostic', @diagnostic.name
      end
    end

    describe '#mandatory' do
      it 'returns true' do
        assert @diagnostic.mandatory
      end
    end

    describe '#resource' do
      it 'returns the resource' do
        assert_equal 'Zuora user: test_user', @diagnostic.resource
      end
    end

    describe '#passed' do
      it 'returns true' do
        assert @diagnostic.passed
      end
    end
  end

  describe 'zuora_account raises an exception' do
    before do
      ZendeskBillingCore::Zuora::Configuration.expects(:username).at_least_once.returns('test_user')
      acct = stub('account', subscription: stub('sub', zuora_subscription: stub('zs', zuora_account_id: '123')))
      Account.expects(:find).with(1).returns(acct)
      zuora_client = stub('zc')
      zuora_client.expects(:zuora_account).raises('test error')
      ZendeskBillingCore::Zuora::Client.expects(:new).with('123').returns(zuora_client)
      zuora = Health::Diagnostic::ZuoraDiagnostic.new
      @diagnostic = zuora.check
    end

    describe '#passed' do
      it 'returns false' do
        refute @diagnostic.passed
      end
    end

    describe '#message' do
      it 'returns a descriptive message' do
        assert_equal 'Access failed with error test error', @diagnostic.message
      end
    end
  end

  describe '`ZendeskBillingCore::Zuora::Configuration.username` raises an exception' do
    before do
      ZendeskBillingCore::Zuora::Configuration.expects(:username).raises('test error')
      acct = stub('account', subscription: stub('sub', zuora_subscription: stub('zs', zuora_account_id: '123')))
      Account.expects(:find).with(1).returns(acct)
      ZendeskBillingCore::Zuora::Client.expects(:new).with('123').returns(stub('zc', zuora_account: nil))
      zuora = Health::Diagnostic::ZuoraDiagnostic.new
      @diagnostic = zuora.check
    end

    describe '#passed' do
      it 'returns false' do
        refute @diagnostic.passed
      end
    end

    describe '#resource' do
      it 'returns Unknown' do
        assert_includes @diagnostic.resource, 'Unknown'
      end
    end

    describe '#message' do
      it 'returns a descriptive message' do
        assert_equal "Unable to retrieve Zuora account for account ID 123", @diagnostic.message
      end
    end
  end

  describe 'Z1 account does not have a Zuora subscription' do
    before do
      ZendeskBillingCore::Zuora::Configuration.expects(:username).never
      acct = stub('account', subscription: stub('sub', zuora_subscription: nil))
      Account.expects(:find).with(1).at_least_once.returns(acct)
      zuora = Health::Diagnostic::ZuoraDiagnostic.new
      @diagnostic = zuora.check
    end

    describe '#passed' do
      it 'returns true' do
        assert @diagnostic.passed
      end
    end

    describe '#resource' do
      it 'returns `(Zuora not in use)`' do
        assert_equal '(Zuora not in use)', @diagnostic.resource
      end
    end
  end

  describe 'Zuora does not return account information' do
    before do
      ZendeskBillingCore::Zuora::Configuration.expects(:username).returns('test_user')
      acct = stub('account', subscription: stub('sub', zuora_subscription: stub('zs', zuora_account_id: '123')))
      Account.expects(:find).with(1).returns(acct)
      ZendeskBillingCore::Zuora::Client.expects(:new).with('123').returns(stub('zc', zuora_account: nil))
      zuora = Health::Diagnostic::ZuoraDiagnostic.new
      @diagnostic = zuora.check
    end

    describe '#passed' do
      it 'returns false' do
        refute @diagnostic.passed
      end
    end

    describe '#message' do
      it 'returns description' do
        assert_equal 'Unable to retrieve Zuora account for account ID 123', @diagnostic.message
      end
    end
  end
end
