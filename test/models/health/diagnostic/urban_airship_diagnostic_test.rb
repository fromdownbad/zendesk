require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::UrbanAirshipDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::UrbanAirshipDiagnostic.new
  end

  describe '#resource' do
    it 'returns setting' do
      assert_equal PushNotifications::UrbanAirshipClient.default_options[:base_uri], @diagnostic.resource
    end
  end

  describe '#check' do
    describe 'Call returns result other than 401' do
      before do
        stub_response = stub('response', code: 500)
        PushNotifications::UrbanAirshipClient.any_instance.stubs(:tokens_deactivated_since).returns(stub_response)
      end

      it 'reports error' do
        result = @diagnostic.check
        assert_equal 'UrbanAirship check failed.  HTTP response is expected to be 401, but is 500.', result.message
        refute result.passed
      end
    end

    describe 'Call raises exception' do
      before do
        PushNotifications::UrbanAirshipClient.any_instance.stubs(:tokens_deactivated_since).raises(StandardError)
      end

      it 'reports error' do
        result = @diagnostic.check
        assert_equal 'UrbanAirship check failed with error StandardError', result.message
        refute result.passed
      end
    end

    describe 'Call returns 401' do
      before do
        stub_response = stub('response', code: 401)
        PushNotifications::UrbanAirshipClient.any_instance.stubs(:tokens_deactivated_since).returns(stub_response)
      end

      it 'succeeds' do
        result = @diagnostic.check
        assert_equal 'Successfully connected to UrbanAirship', result.message
        assert result.passed
      end
    end
  end
end
