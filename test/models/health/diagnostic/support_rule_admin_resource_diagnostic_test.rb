require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::SupportRuleAdminResourceDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::SupportRuleAdminResourceDiagnostic.new
  end

  describe '#resource_name' do
    it 'returns rule-admin' do
      assert_equal 'rule-admin', @diagnostic.resource_name
    end
  end

  describe '#check_name' do
    it 'returns Support rule admin' do
      assert_equal 'Support rule admin', @diagnostic.check_name
    end
  end
end
