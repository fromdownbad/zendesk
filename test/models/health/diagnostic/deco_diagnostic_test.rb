require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::DecoDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::DecoDiagnostic.new
  end

  describe '#resource' do
    it 'returns the Deco base_url' do
      stub_url = stub('deco base url')
      Zendesk::Deco::Client.any_instance.expects(:base_url).returns(stub_url)
      assert_equal stub_url, @diagnostic.resource
    end

    describe 'with no active accounts in a pod' do
      before do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
      end

      it 'returns a meaningful message' do
        assert_equal 'No active account found in pod', @diagnostic.resource
      end
    end
  end

  describe '#check' do
    describe 'unable to find active account in pod' do
      before do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
      end

      it 'reports error' do
        result = @diagnostic.check
        refute result.passed
        assert_equal "Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}", result.message
      end
    end

    describe 'exception while calling Deco' do
      before do
        Zendesk::Deco::Client.any_instance.stubs(:attributes).raises(StandardError)
      end

      it 'reports error' do
        result = @diagnostic.check
        refute result.passed
        assert_equal 'Deco check failed with error StandardError', result.message
      end
    end

    describe 'Deco client returns attributes' do
      before do
        attributes = stub('attributes', index: [])
        Zendesk::Deco::Client.any_instance.stubs(:attributes).returns(attributes)
      end

      it 'succeeds' do
        result = @diagnostic.check
        assert result.passed
        assert_equal 'Successfully retrieved attribute list from Deco', result.message
      end
    end
  end
end
