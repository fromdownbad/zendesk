require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::SatisfactionPredictionAppDiagnostic do
  fixtures :accounts

  before do
    @diagnostic = Health::Diagnostic::SatisfactionPredictionAppDiagnostic.new
  end

  describe '#resource' do
    describe 'unable to find active account in pod' do
      it 'returns the zero length string' do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
        assert_equal '', @diagnostic.resource
      end
    end

    describe 'able to find active account in pod' do
      it 'returns the URL from the client' do
        client_stub = stub('client stub', url_prefix: 'test_url_prefix')
        ::Zendesk::SatisfactionPredictionApp::InternalApiClient.stubs(:new).returns(client_stub)
        assert_equal 'test_url_prefix', @diagnostic.resource
      end
    end
  end

  describe '#check' do
    describe 'unable to find active account in pod' do
      before do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
      end

      it 'reports error' do
        result = @diagnostic.check
        refute result.passed
        assert_equal "Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}", result.message
      end
    end

    describe 'exception while calling satisfaction prediction app' do
      before do
        client_stub = stub('client stub', url_prefix: 'test_url_prefix')
        client_stub.stubs(:fetch_satisfaction_prediction_available).raises(StandardError)
        ::Zendesk::SatisfactionPredictionApp::InternalApiClient.stubs(:new).returns(client_stub)
      end

      it 'reports error' do
        result = @diagnostic.check
        refute result.passed
        assert_equal 'Satisfaction prediction app check failed with error StandardError', result.message
      end
    end

    describe 'Satisfaction prediction app returns availability without erroring' do
      before do
        client_stub = stub('client stub', url_prefix: 'test_url_prefix', fetch_satisfaction_prediction_available: true)
        ::Zendesk::SatisfactionPredictionApp::InternalApiClient.stubs(:new).returns(client_stub)
      end

      it 'succeeds' do
        result = @diagnostic.check
        assert result.passed
        assert_equal 'Successfully retrieved account info from satisfaction prediction app', result.message
      end
    end
  end
end
