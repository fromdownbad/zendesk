require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::PigeonDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::PigeonDiagnostic.new
  end

  describe '#resource' do
    it 'returns setting' do
      assert_equal Zendesk::Configuration.fetch(:pigeon)['url'], @diagnostic.resource
    end
  end

  describe '#check' do
    describe 'GET to Pigeon' do
      let(:response) do
        {
          status: 200,
          body: "super successful",
          headers: {}
        }
      end

      before do
        full_url = "#{Zendesk::Configuration.fetch(:pigeon)['url']}/z/ping"
        stub_request(:get, full_url).to_return(response)
      end

      describe 'GET succeeds' do
        it 'succeeds' do
          result = @diagnostic.check
          assert result.passed
          assert_equal 'Successfully pinged Pigeon', result.message
        end
      end

      describe 'GET returns error' do
        let(:response) do
          {
            status: 500,
            body: "error info",
            headers: {}
          }
        end

        it 'reports error' do
          result = @diagnostic.check
          refute result.passed
          assert_equal 'Pigeon check failed: 500 -- error info', result.message
        end
      end

      describe 'GET raises an exception' do
        before do
          Net::HTTP.stubs(:get_response).raises(StandardError)
        end

        it 'reports error' do
          result = @diagnostic.check
          refute result.passed
          assert_equal 'Pigeon check failed with error StandardError', result.message
        end
      end
    end
  end
end
