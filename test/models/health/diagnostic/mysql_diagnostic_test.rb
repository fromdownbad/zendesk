require_relative "../../../support/test_helper"

SingleCov.covered!

class Health::Diagnostic::MysqlMockDiagnostic < Health::Diagnostic::MysqlDiagnostic
  def initialize(select_result = 1)
    @select_result = select_result
    super()
  end

  def name
    'MySQL mock'
  end

  def perform_select
    @select_result
  end

  def resource
    'test resource'
  end
end

describe Health::Diagnostic::MysqlDiagnostic do
  describe 'success' do
    before do
      mysql = Health::Diagnostic::MysqlMockDiagnostic.new
      @diagnostic = mysql.check
    end

    describe '#name' do
      it 'returns MySQL mock' do
        assert_equal 'MySQL mock', @diagnostic.name
      end
    end

    describe '#mandatory' do
      it 'returns true' do
        assert @diagnostic.mandatory
      end
    end

    describe '#resource' do
      it 'returns the resource' do
        assert_equal 'test resource', @diagnostic.resource
      end
    end

    describe '#success' do
      it 'returns true' do
        assert @diagnostic.passed
      end
    end
  end

  describe '`select` call returns wrong value' do
    before do
      mysql = Health::Diagnostic::MysqlMockDiagnostic.new(2)
      @diagnostic = mysql.check
    end

    describe '#success' do
      it 'returns false' do
        refute @diagnostic.passed
      end
    end

    describe '#message' do
      it 'returns a descriptive message' do
        assert_equal "Select was expected to return '1' but returned '2'", @diagnostic.message
      end
    end
  end

  describe '`select` call raises an exception' do
    before do
      Health::Diagnostic::MysqlMockDiagnostic.any_instance.expects(:perform_select).raises('test error')
      mysql = Health::Diagnostic::MysqlMockDiagnostic.new(1)
      @diagnostic = mysql.check
    end

    describe '#success' do
      it 'returns false' do
        refute @diagnostic.passed
      end
    end

    describe '#message' do
      it 'returns a descriptive message' do
        assert_equal "Select failed with error test error", @diagnostic.message
      end
    end
  end
end
