require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::SamsonSecretsPullerDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::SamsonSecretsPullerDiagnostic.new
  end

  describe '#resource' do
    it 'returns hard-coded value' do
      assert_equal SamsonSecretPuller::FOLDER, @diagnostic.resource
    end
  end

  describe '#check' do
    describe 'folder not available' do
      before do
        @diagnostic.stubs(:samson_secret_path).returns('/test/folder/does/not/exist')
      end

      it 'reports error' do
        result = @diagnostic.check
        assert_equal "Samson Secrets Puller folder /test/folder/does/not/exist not found", result.message
        refute result.passed
      end
    end

    describe 'folder available' do
      before do
        @tmpdir = Dir.mktmpdir
        @diagnostic.stubs(:samson_secret_path).returns(@tmpdir)
      end

      after do
        Dir.delete(@tmpdir)
      end

      describe 'SamsonSecretPuller.to_hash raises exception' do
        before do
          SamsonSecretPuller.stubs(:to_hash).raises(StandardError)
        end

        it 'reports error' do
          result = @diagnostic.check
          assert_equal 'Samson Secrets Puller check failed with error StandardError', result.message
          refute result.passed
        end
      end

      describe 'SamsonSecretPuller.to_hash succeeds' do
        before do
          SamsonSecretPuller.stubs(:to_hash)
        end

        it 'succeeds' do
          result = @diagnostic.check
          assert_equal 'Successfully accessed Samson Secrets Puller', result.message
          assert result.passed
        end
      end
    end
  end

  describe '#samson_secret_path' do
    it 'returns Samson value' do
      assert_equal SamsonSecretPuller::FOLDER, @diagnostic.samson_secret_path
    end
  end
end
