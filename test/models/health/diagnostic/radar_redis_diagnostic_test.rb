require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::RadarRedisDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::RadarRedisDiagnostic.new
  end

  describe '#resource' do
    describe 'unable to find active account in pod' do
      before do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
      end

      it 'returns the zero length string' do
        assert_equal '', @diagnostic.resource
      end
    end

    it 'returns info from config' do
      shard_id = @diagnostic.send(:active_account_in_pod).shard_id
      expected_resource = ::Zendesk::Radar::Configuration.send(:configuration_for_shard, shard_id).to_json
      assert_equal expected_resource, @diagnostic.resource
    end
  end

  describe '#check' do
    describe 'when there is no active account in pod' do
      before do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
      end

      it 'reports error' do
        result = @diagnostic.check
        assert_equal "Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}", result.message
        refute result.passed
      end
    end

    describe 'when checking for account on shards' do
      before do
        @diagnostic.stubs(:radar_shard_map).returns(123 => 456)

        @stub_acct = stub('account for shard', id: 789)
        ActiveRecord::Relation.any_instance.stubs(:first).returns(accounts(:minimum)).then.returns(@stub_acct)
      end

      describe 'and account check succeeds' do
        before do
          @diagnostic.expects(:check_account).with(@stub_acct)
        end

        it 'retuns succcess' do
          result = @diagnostic.check

          assert result.passed
        end

        it 'succeeds with a message' do
          result = @diagnostic.check

          assert_equal 'Successfully accessed Redis for Radar', result.message
        end
      end

      describe 'and account check fails' do
        let(:failure_stub) { stub('failure') }

        before do
          @diagnostic.expects(:check_account).with(@stub_acct).returns(failure_stub)
        end

        it 'reports error' do
          assert_equal failure_stub, @diagnostic.check
        end
      end
    end
  end

  describe '#check_account' do
    let(:account) { accounts(:minimum) }

    describe 'presence fails' do
      it 'reports error' do
        ::Radar::Client.any_instance.stubs(:presence).with('ticket/1').returns(stub('presence', get: false))
        result = @diagnostic.check_account(account)
        assert_equal "Failed to get presence for radar shard #{account.shard_id}", result.message
        refute result.passed
      end
    end

    describe 'presence succeeds' do
      before do
        ::Radar::Client.any_instance.stubs(:presence).with('ticket/1').returns(stub('presence', get: true))
      end

      it 'succeeds' do
        Redis::Client.any_instance.stubs(:host).returns(true)
        assert_nil @diagnostic.check_account(account)
      end
    end
  end

  describe '#radar_shard_map' do
    it 'returns constant' do
      assert_equal Zendesk::Radar::Configuration::SHARD_MAP, @diagnostic.radar_shard_map
    end
  end
end
