require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::AccountServiceDiagnostic do
  fixtures :accounts

  before do
    @diagnostic = Health::Diagnostic::AccountServiceDiagnostic.new
  end

  describe '#resource' do
    describe 'unable to find active account in pod' do
      it 'returns the zero length string' do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
        assert_equal '', @diagnostic.resource
      end
    end

    describe 'able to find active account in pod' do
      it 'returns the URL to products' do
        Zendesk::Accounts::Client.any_instance.expects(:url_prefix).returns('test_url_prefix')
        Zendesk::Accounts::Client.any_instance.expects(:products_url).returns('test_products_url')
        assert_equal 'test_url_prefixtest_products_url', @diagnostic.resource
      end
    end
  end

  describe '#check' do
    describe 'unable to find active account in pod' do
      before do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
      end

      it 'reports error' do
        result = @diagnostic.check
        refute result.passed
        assert_equal "Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}", result.message
      end
    end

    describe 'exception while calling account service' do
      before do
        Zendesk::Accounts::Client.any_instance.stubs(:product!).raises(StandardError)
      end

      it 'reports error' do
        result = @diagnostic.check
        refute result.passed
        assert_equal 'Account service check failed with error StandardError', result.message
      end
    end

    describe 'Account service client returns products without erroring' do
      before do
        Zendesk::Accounts::Client.any_instance.expects(:product!).with('guide')
      end

      it 'succeeds' do
        result = @diagnostic.check
        assert result.passed
        assert_equal 'Successfully retrieved account info from account service', result.message
      end
    end
  end
end
