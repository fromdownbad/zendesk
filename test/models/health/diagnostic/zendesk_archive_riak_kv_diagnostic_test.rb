require_relative "../../../support/test_helper"
require 'zendesk_archive/configuration'

SingleCov.covered!

describe Health::Diagnostic::ZendeskArchiveRiakKvDiagnostic do
  before do
    ZendeskArchive.configure do |config|
      config.riak_key_version = 3
    end
    ZendeskArchive::RiakConfiguration.stubs(:nodes).returns(nodes)
  end

  let(:diagnostic) { Health::Diagnostic::ZendeskArchiveRiakKvDiagnostic.new }
  let(:nodes) { [{ host: "riak1.zd-dev.com", http_port: 8098, pb_port: 8087 }] }

  describe '#resource' do
    describe 'riak_kv is not available' do
      it 'returns the zero length string' do
        ZendeskArchive.stubs(:riak_kv).returns(nil)
        assert_equal '', diagnostic.resource
      end
    end

    describe 'riak_kv is available' do
      describe 'riak_kv does not have a riak client' do
        before do
          ZendeskArchive.riak_kv.stubs(:riak_client).returns(nil)
        end

        it 'returns the zero length string' do
          assert_equal '', diagnostic.resource
        end
      end

      describe 'riak_kv does have a riak_client' do
        describe 'nodes is empty' do
          let(:nodes) { [] }
          # This is weird fallback behavior in the riak-client gem itself.
          # If we fix it, it will happen in the zendesk_archive gem.
          it 'falls back to localhost, because riak-client is coping but should probably raise instead' do
            assert_equal '127.0.0.1:8087', diagnostic.resource
          end
        end

        describe 'nodes contains multiple nodes' do
          it 'describes the nodes' do
            assert_equal 'riak1.zd-dev.com:8087', diagnostic.resource
          end
        end
      end
    end
  end

  describe '#check' do
    let(:result) { diagnostic.check }

    describe 'riak_kv is not available' do
      before do
        ZendeskArchive.stubs(:riak_kv).returns(nil)
      end

      it 'reports error' do
        refute result.passed
        assert_equal 'Unable to access ZendeskArchive.riak_kv', result.message
      end
    end

    describe 'riak_kv is available' do
      describe 'riak_kv does not have a bucket' do
        before do
          ZendeskArchive.riak_kv.stubs(:bucket).returns(nil)
        end

        it 'reports error' do
          refute result.passed
          assert_equal 'Unable to access ZendeskArchive.riak_kv.bucket', result.message
        end
      end

      describe 'riak_kv does have a bucket' do
        describe 'riak_kv does not have a riak_client' do
          before do
            ZendeskArchive.riak_kv.stubs(:riak_client).returns(nil)
          end

          it 'reports error' do
            refute result.passed
            assert_equal 'Unable to access ZendeskArchive.riak_kv.riak_client', result.message
          end
        end

        describe 'riak_kv does not have a key_version' do
          before do
            ZendeskArchive.riak_kv.stubs(:key_version).returns(nil)
          end

          it 'reports error' do
            refute result.passed
            assert_equal 'Unable to access ZendeskArchive.riak_kv.key_version', result.message
          end
        end

        describe 'riak_kv does have a key_version' do
          describe 'riak_kv does have a riak_client' do
            describe 'riak_kv.exists? raises exception' do
              before do
                ZendeskArchive.riak_kv.stubs(:exists?).raises(StandardError)
              end

              it 'reports error' do
                refute result.passed
                assert_equal 'ZendeskArchive Riak Kv check failed with error StandardError', result.message
              end
            end

            describe 'call to riak_kv.exists? succeeds' do
              before do
                ZendeskArchive.riak_kv.stubs(:exists?).returns(false)
              end

              it 'succeeds' do
                assert result.passed
                assert_equal 'Successfully retrieved information from ZendeskArchive Riak KV store', result.message
              end
            end
          end
        end
      end
    end
  end
end
