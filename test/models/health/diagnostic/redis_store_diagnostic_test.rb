require_relative "../../../support/test_helper"

SingleCov.covered!

class Health::Diagnostic::RedisMockDiagnostic < Health::Diagnostic::RedisStoreDiagnostic
  attr_reader :redis_client

  def initialize(redis_client)
    @redis_client = redis_client
    super()
  end

  def name
    'Redis mock'
  end
end

describe Health::Diagnostic::RedisStoreDiagnostic do
  before do
    @redis_client = stub('redis client', redis: stub('redis', client: stub('client', options: { url: 'http://test.url.com'})))
  end

  describe 'success' do
    before do
      @redis_client.expects(:set).with('_diagnostic_check', '1').returns('OK')
      Zendesk::RedisStore.expects(:client).at_least_once.returns(@redis_client)
      redis = Health::Diagnostic::RedisStoreDiagnostic.new
      @diagnostic = redis.check
    end

    describe '#name' do
      it 'returns RedisStoreDiagnostic' do
        assert_equal 'RedisStoreDiagnostic', @diagnostic.name
      end
    end

    describe '#mandatory' do
      it 'returns true' do
        assert @diagnostic.mandatory
      end
    end

    describe 'resource' do
      it 'returns the URL' do
        assert_equal 'http://test.url.com', @diagnostic.resource
      end
    end

    describe 'passed' do
      it 'returns true' do
        assert @diagnostic.passed
      end
    end
  end

  describe '`set` call returns wrong value' do
    before do
      @redis_client.expects(:set).with('_diagnostic_check', '1').returns('FAIL')
      Zendesk::RedisStore.expects(:client).at_least_once.returns(@redis_client)
      redis = Health::Diagnostic::RedisStoreDiagnostic.new
      @diagnostic = redis.check
    end

    describe 'passed' do
      it 'returns false' do
        refute @diagnostic.passed
      end
    end

    describe 'message' do
      it 'returns a descriptive message' do
        assert_equal "Set was expected to return 'OK' but returned 'FAIL'", @diagnostic.message
      end
    end
  end

  describe '`set` call raises an exception' do
    before do
      @redis_client.expects(:set).with('_diagnostic_check', '1').raises('test exception')
      Zendesk::RedisStore.expects(:client).at_least_once.returns(@redis_client)
      redis = Health::Diagnostic::RedisStoreDiagnostic.new
      @diagnostic = redis.check
    end

    describe 'passed' do
      it 'returns false' do
        refute @diagnostic.passed
      end
    end

    describe 'message' do
      it 'returns a descriptive message' do
        assert_equal 'Access failed with error test exception', @diagnostic.message
      end
    end
  end

  describe 'exception while calculating resource' do
    before do
      Zendesk::RedisStore.expects(:client).at_least_once.returns(nil)
      redis = Health::Diagnostic::RedisStoreDiagnostic.new
      @diagnostic = redis.check
    end

    describe 'passed' do
      it 'returns false' do
        refute @diagnostic.passed
      end
    end

    describe 'message' do
      it 'returns a descriptive message' do
        assert_includes @diagnostic.message, 'Access failed with error undefined method'
      end
    end

    describe 'resource' do
      it 'returns unknown' do
        assert_includes @diagnostic.resource, 'Unknown:'
      end
    end
  end
end
