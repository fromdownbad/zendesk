require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::TessaDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::TessaDiagnostic.new
  end

  describe '#resource' do
    it 'returns the Tessa base_url' do
      mock_url = "http://pod-x.tessa.service.consul:1337"
      TessaClient::V1.stubs(:tessa_host_url).returns(mock_url)
      assert_equal "#{mock_url}/z/ping", @diagnostic.resource
    end
  end

  describe '#check' do
    it 'passes when the fetched diagnostic was a success' do
      mock_response_body = '{"message": "OK"}'
      response = Faraday::Response.new(body: mock_response_body.to_json)
      TessaClient::V1.stubs(:fetch_health).returns(response)
      assert @diagnostic.check.passed
    end

    it 'fails when the fetched diagnostic was a failure' do
      TessaClient::V1.stubs(:fetch_health).throws(Exception.new)
      refute @diagnostic.check.passed
    end
  end
end
