require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Health::Diagnostic::MysqlShardDiagnostic do
  before do
    @mysql = Health::Diagnostic::MysqlShardDiagnostic.new
    @mysql.stubs(:shard_id).returns(1)
  end

  describe '#name' do
    it 'returns MySQL shard' do
      @diagnostic = @mysql.check
      assert_equal 'MysqlShardDiagnostic', @diagnostic.name
    end
  end

  describe '#resource' do
    it 'runs on shard' do
      ActiveRecord::Base.expects(:on_shard).with(1).twice.yields
      diagnostic = @mysql.check
      diagnostic.resource
    end

    it 'returns host and port' do
      Ticket.connection.raw_connection.expects(:query_options).at_least_once.returns(
        host: 'test_host',
        port: 1234
      )
      diagnostic = @mysql.check
      assert_equal 'mysql://test_host:1234', diagnostic.resource
    end
  end

  describe '#perform_select' do
    it 'runs on shard' do
      ActiveRecord::Base.expects(:on_shard).with(1).yields
      @mysql.perform_select
    end

    it 'performs a select against the shard connection' do
      Ticket.connection.expects(:select_value).with('SELECT 1 AS val').returns(123)
      assert_equal 123, @mysql.perform_select
    end
  end
end
