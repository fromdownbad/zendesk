require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::VoyagerDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::VoyagerDiagnostic.new
  end

  describe '#resource' do
    describe 'active account found' do
      it 'returns the Voyager location' do
        stub_url = stub('voyager url')
        Zendesk::VoyagerClientBuilder.stubs(:location).returns(stub_url)
        assert_equal stub_url, @diagnostic.resource
      end
    end

    describe 'active account not found' do
      it 'returns the zero length string' do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
        assert_equal '', @diagnostic.resource
      end
    end
  end

  describe '#check' do
    before do
      @voyager_client = stub('voyager client')
      Zendesk::VoyagerClientBuilder.stubs(:build).returns(@voyager_client)
    end

    describe 'shard management' do
      it 'calls `on_shard`' do
        ActiveRecord::Base.expects(:on_shard).with(@diagnostic.send(:active_account_in_pod).shard_id)
        @diagnostic.check
      end
    end

    describe 'unable to find active account in pod' do
      before do
        @diagnostic.stubs(:active_account_in_pod).returns(nil)
      end

      it 'reports error' do
        result = @diagnostic.check
        refute result.passed
        assert_equal "Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}", result.message
      end
    end

    describe 'exception while calling Voyager' do
      before do
        @voyager_client.stubs(:list_exports).raises(StandardError)
      end

      it 'reports error' do
        result = @diagnostic.check
        refute result.passed
        assert_equal 'Error accessing Voyager service: StandardError', result.message
      end
    end

    describe 'call to list_exports reports error' do
      before do
        response = stub('response', successful?: false, errors: 'some errors', status: 500)
        @voyager_client.expects(:list_exports).returns(response)
      end

      it 'reports error' do
        result = @diagnostic.check
        refute result.passed
        assert_equal "Error accessing Voyager service: list_exports returned some errors (500)", result.message
      end
    end

    describe 'calls to voyager are successful' do
      before do
        list_exports_response = stub('response', successful?: true)
        expected_list_exports_params = {
          page: 0,
          per_page: 1,
          sort_order: 'DESC',
          all: true
        }
        @voyager_client.expects(:list_exports).with(expected_list_exports_params).returns(list_exports_response)
      end

      it 'succeeds' do
        result = @diagnostic.check
        assert result.passed
        assert_equal 'Successfully created export in Voyager service', result.message
      end
    end
  end

  describe '#test_user' do
    it 'returns an active admin user' do
      assert_equal users(:support_morten), @diagnostic.test_user
    end
  end
end
