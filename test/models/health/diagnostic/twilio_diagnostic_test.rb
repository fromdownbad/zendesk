require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::TwilioDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::TwilioDiagnostic.new
  end

  describe '#check' do
    describe 'Twilio account list succeeds' do
      before do
        client = stub('client', accounts: stub('accounts', list: nil))
        ::Twilio::REST::Client.expects(:new).with('bogus_sid', 'bogus_token').returns(client)
      end

      it 'reports error' do
        result = @diagnostic.check
        assert_equal 'Unknown failure checking Twilio- error expected but none returned', result.message
        refute result.passed
      end
    end

    describe 'unexpected error' do
      before do
        client = stub('client')
        client.stubs(:accounts).raises(StandardError)
        ::Twilio::REST::Client.expects(:new).with('bogus_sid', 'bogus_token').returns(client)
      end

      it 'reports error' do
        result = @diagnostic.check
        assert_equal 'Twilio check failed with error StandardError', result.message
        refute result.passed
      end
    end

    describe 'RequestError' do
      before do
        client = stub('client')
        client.stubs(:accounts).raises(Twilio::REST::RequestError.new(message: "Error"))
        ::Twilio::REST::Client.expects(:new).with('bogus_sid', 'bogus_token').returns(client)
      end

      it 'succeeds' do
        result = @diagnostic.check
        assert_equal 'Successfully connected to Twilio', result.message
        assert result.passed
      end
    end
  end
end
