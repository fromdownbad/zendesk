require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::StaffServiceDiagnostic do
  fixtures :accounts

  let(:staff_service_diagnostic) do
    Health::Diagnostic::StaffServiceDiagnostic.new
  end

  describe '#resource' do
    it 'builds the uri for `z/ping` endpoint' do
      assert_equal(
        'http://staff-service.service.consul:12080/z/ping',
        staff_service_diagnostic.resource
      )
    end
  end

  describe '#check' do
    let(:check) { staff_service_diagnostic.check }

    let(:response) do
      {
        status: 200,
        body: "{ping: 'ok'}",
        headers: {}
      }
    end

    before do
      Account.stubs(:pod_local).returns([accounts(:minimum)])

      stub_request(
        :get,
        'http://staff-service.service.consul:12080/z/ping'
      ).to_return(response)
    end

    describe 'when the staff service is accessible' do
      it 'passes' do
        assert check.passed
      end

      it 'returns a descriptive message' do
        assert_equal('Staff service is accessible', check.message)
      end
    end

    describe 'when the staff service is inaccessible' do
      let(:response) { {status: 404, body: "{}", headers: {}} }

      it 'fails' do
        refute check.passed
      end

      it 'returns a descriptive message' do
        assert_equal(
          'Staff service is inaccessible',
          check.message
        )
      end
    end

    describe 'when the check returns a `StandardError`' do
      before do
        stub_request(
          :get,
          'http://staff-service.service.consul:12080/z/ping'
        ).to_raise(StandardError)
      end

      it 'fails' do
        refute check.passed
      end

      it 'returns a descriptive message' do
        assert_equal(
          'Staff service is inaccessible: Exception from WebMock',
          check.message
        )
      end
    end

    describe 'when the check returns a `Net::OpenTimeout`' do
      before do
        stub_request(
          :get,
          'http://staff-service.service.consul:12080/z/ping'
        ).to_raise(Net::OpenTimeout.new('execution expired'))
      end

      it 'fails' do
        refute check.passed
      end

      it 'returns a descriptive message' do
        assert_equal(
          'Staff service is inaccessible: execution expired',
          check.message
        )
      end
    end
  end
end
