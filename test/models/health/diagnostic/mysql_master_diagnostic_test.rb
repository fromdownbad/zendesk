require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Health::Diagnostic::MysqlMasterDiagnostic do
  before do
    @mysql = Health::Diagnostic::MysqlMasterDiagnostic.new
  end

  describe '#name' do
    it 'returns MySQL master' do
      diagnostic = @mysql.check
      assert_equal 'MysqlMasterDiagnostic', diagnostic.name
    end
  end

  describe '#resource' do
    it 'returns host and port' do
      Account.connection.raw_connection.expects(:query_options).at_least_once.returns(
        host: 'test_host',
        port: 1234
      )
      diagnostic = @mysql.check
      assert_equal 'mysql://test_host:1234', diagnostic.resource
    end
  end

  describe '#perform_select' do
    it 'performs a select against the master connection' do
      Account.connection.expects(:select_value).with('SELECT 1 AS val').returns(123)
      assert_equal 123, @mysql.perform_select
    end
  end
end
