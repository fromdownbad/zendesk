require_relative "../../../support/test_helper"

SingleCov.covered!

class AdminResourceTestDiagnostic < Health::Diagnostic::AdminResourceDiagnostic
  def resource_name
    'test_diagnostic'
  end

  def check_name
    'Test diagnostic'
  end
end

describe Health::Diagnostic::AdminResourceDiagnostic do
  before do
    @diagnostic = AdminResourceTestDiagnostic.new
  end

  describe '#resource' do
    it 'includes consul name, resource name, and pod ID in URL' do
      @diagnostic.stubs(:consul_hostname).returns('test_consul')
      @diagnostic.stubs(:resource_name).returns('test_resource')
      @diagnostic.stubs(:pod_id).returns('123')
      assert_equal 'http://test_consul/v1/kv/test_resource/pod123/manifest.json?raw', @diagnostic.resource
    end
  end

  describe '#check' do
    let(:http_status) { 200 }

    let(:response_body) do
      {
        version: {
          sha: 'abcd',
          tag: 'v123',
          timestamp: Time.zone.now.to_s
        },
        js: {
          path: '/js',
          fingerprint: 'abc'
        },
        css: {
          path: '/css',
          fingerprint: 'def'
        }
      }.to_json
    end

    before do
      stub_request(:get, @diagnostic.resource).
        to_return(status: http_status, body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
    end

    describe 'valid response' do
      it 'reports success' do
        result = @diagnostic.check
        assert result.passed
      end
    end

    describe 'JSON response missing fields' do
      let(:response_body) do
        {
          version: {
            sha: 'abcd',
            tag2: 'v123',
            timestamp: Time.zone.now.to_s
          },
          js: {
            path: '/js',
            fingerprint: 'abc'
          },
          css: {
            path: '/css',
            fingerprint2: 'def'
          }
        }.to_json
      end

      it 'reports failure' do
        result = @diagnostic.check
        refute result.passed
        assert_match %r{Test diagnostic manifest invalid: \["The property '#/version' did not contain a required property of 'tag' in schema \S+", "The property '#/css' did not contain a required property of 'fingerprint' in schema \S+"\]}, result.message
      end
    end

    describe 'invalid JSON' do
      let(:response_body) { 'this is not JSON' }

      it 'reports failure' do
        result = @diagnostic.check
        refute result.passed
        assert_equal 'Test diagnostic manifest unparseable', result.message
      end
    end

    describe 'http error' do
      let(:http_status) { 500 }

      it 'reports failure' do
        result = @diagnostic.check
        refute result.passed
        assert_equal 'Test diagnostic manifest is inaccessible: 500 ', result.message
      end
    end

    describe 'network timeout' do
      before do
        stub_request(:get, @diagnostic.resource).to_raise(Timeout::Error)
      end

      it 'reports failure' do
        result = @diagnostic.check
        refute result.passed
        assert_match %r{Test diagnostic manifest is inaccessible: .*}, result.message
      end
    end
  end

  describe '#pod_id' do
    it 'gets value from config' do
      assert_equal Zendesk::Configuration.fetch(:pod_id), @diagnostic.pod_id
    end
  end

  describe '#consul_hostname' do
    describe 'env var set' do
      before do
        ENV['CONSUL_HTTP_ADDR'] = 'test_val'
      end

      after do
        ENV.delete('CONSUL_HTTP_ADDR')
      end

      it 'returns env var value' do
        assert_equal 'test_val', @diagnostic.consul_hostname
      end
    end

    describe 'env var not set' do
      before do
        ENV.delete('CONSUL_HTTP_ADDR')
      end

      it 'returns default value' do
        assert_equal 'localhost:8500', @diagnostic.consul_hostname
      end
    end
  end
end
