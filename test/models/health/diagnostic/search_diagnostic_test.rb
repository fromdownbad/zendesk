require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::SearchDiagnostic do
  let(:search_diagnostic) do
    Health::Diagnostic::SearchDiagnostic.new
  end

  let(:blue_endpoint) do
    'http://blue-test-search-service:8085'
  end

  let(:green_endpoint) do
    'http://green-test-search-service:8085'
  end

  before do
    ZendeskSearch.expects(:search_service_endpoint_blue).returns(blue_endpoint)
    ZendeskSearch.expects(:search_service_endpoint_green).returns(green_endpoint)
  end

  describe '#check' do
    let(:check) { search_diagnostic.check }

    let(:response) do
      {
        status: 200,
        body: "{ping: 'ok'}",
        headers: {}
      }
    end

    before do
      stub_request(
        :get,
        "#{blue_endpoint}/z/ping"
      ).to_return(response)

      stub_request(
        :get,
        "#{green_endpoint}/z/ping"
      ).to_return(response)
    end

    describe 'when Search Service is accessible' do
      it 'passes' do
        assert check.passed
      end

      it 'returns a descriptive message' do
        assert_equal('Search Service is accessible', check.message)
      end
    end

    describe 'when Search Service is inaccessible' do
      let(:response) { {status: 404, body: "{}", headers: {}} }

      it 'fails' do
        refute check.passed
      end

      it 'returns a descriptive message' do
        assert_equal(
          'Search Service is inaccessible. Response code: 404',
          check.message
        )
      end
    end

    describe 'when the check returns a `StandardError`' do
      before do
        stub_request(
          :get,
          "#{blue_endpoint}/z/ping"
        ).to_raise(StandardError)

        stub_request(
          :get,
          "#{green_endpoint}/z/ping"
        ).to_raise(StandardError)
      end

      it 'fails' do
        refute check.passed
      end

      it 'returns a descriptive message' do
        assert_equal(
          'Search Service is inaccessible: Exception from WebMock',
          check.message
        )
      end
    end

    describe 'when the check returns a `Net::OpenTimeout`' do
      before do
        stub_request(
          :get,
          "#{blue_endpoint}/z/ping"
        ).to_raise(Net::OpenTimeout.new('execution expired'))

        stub_request(
          :get,
          "#{green_endpoint}/z/ping"
        ).to_raise(Net::OpenTimeout.new('execution expired'))
      end

      it 'fails' do
        refute check.passed
      end

      it 'returns a descriptive message' do
        assert_equal(
          'Search Service is inaccessible: execution expired',
          check.message
        )
      end
    end
  end
end
