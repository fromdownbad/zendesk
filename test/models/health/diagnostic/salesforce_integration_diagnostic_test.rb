require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::SalesforceIntegrationDiagnostic do
  before do
    @diagnostic = Health::Diagnostic::SalesforceIntegrationDiagnostic.new
  end

  describe '#resource' do
    it 'returns config value' do
      ::Zendesk::Configuration.stubs(:dig).with(:salesforce, :oauth_options, 'site').returns('test val')
      assert_equal 'test val', @diagnostic.resource
    end
  end

  describe '#check' do
    describe 'call to OAuth2 client raises exception' do
      before do
        OAuth2::Strategy::AuthCode.any_instance.stubs(:get_token).raises(StandardError)
      end

      it 'reports error' do
        result = @diagnostic.check
        assert_equal 'Salesforce Integration check failed with error StandardError', result.message
        refute result.passed
      end
    end

    describe 'call to OAuth2 client succeeds' do
      before do
        retval = {'error' => 'invalid_grant', 'error_description' => 'invalid authorization code'}
        OAuth2::Strategy::AuthCode.any_instance.stubs(:get_token).returns(retval)
      end

      it 'succeeds' do
        result = @diagnostic.check
        assert_equal 'Successfully validated Salesforce Integration', result.message
        assert result.passed
      end
    end
  end
end
