require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::AccountFraudServiceDiagnostic do
  let(:account_fraud_service_diagnostic) do
    Health::Diagnostic::AccountFraudServiceDiagnostic.new
  end

  describe '#resource' do
    before do
      Fraud::FraudServiceClient.
        stubs(:build_url_prefix).
        returns('https://some-fake-url-prefix')
    end

    it 'builds the diagnostic endpoint' do
      assert_equal(
        'https://some-fake-url-prefix/z/diagnostic',
        account_fraud_service_diagnostic.resource
      )
    end
  end

  describe '#check' do
    let(:check) { account_fraud_service_diagnostic.check }

    let(:fraud_service_url) do
      'http://pod-1.account-fraud-service.service.consul:5000/z/diagnostic'
    end

    let(:response) do
      {
        status: 200,
        body: "{checks: [{message: 'Minimal Diagnostic is Good'}]}",
        headers: {}
      }
    end

    describe 'when the account fraud service is accessible' do
      before do
        stub_request(:get, fraud_service_url).to_return(response)
      end

      describe 'and it returns valid data' do
        it 'passes' do
          assert check.passed
        end

        it 'returns a descriptive message' do
          assert_equal(
            "Account Fraud service is accessible: {checks: [{message: 'Minimal " \
            "Diagnostic is Good'}]}",
            check.message
          )
        end
      end

      describe 'and it encounters an internal error' do
        let(:response) do
          {
            status: 500,
            body: 'Internal Server Error',
            headers: {}
          }
        end

        it 'fails' do
          refute check.passed
        end

        it 'returns a descriptive message' do
          assert_equal(
            'Account Fraud service is inaccessible: 500 -- Internal Server Error',
            check.message
          )
        end
      end
    end

    describe 'when the account fraud service is inacessible' do
      before do
        stub_request(:get, fraud_service_url).
          to_raise(SocketError.new('Name or service not known'))
      end

      it 'fails' do
        refute check.passed
      end

      it 'returns a descriptive message' do
        assert_equal(
          'Account Fraud service is inaccessible: Name or service not known',
          check.message
        )
      end
    end
  end
end
