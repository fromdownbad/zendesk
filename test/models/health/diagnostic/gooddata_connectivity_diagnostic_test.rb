require_relative "../../../support/test_helper"

SingleCov.covered!

describe Health::Diagnostic::GooddataConnectivityDiagnostic do
  let(:subject) { Health::Diagnostic::GooddataConnectivityDiagnostic.new }

  let(:gooddata_client)     { Zendesk::Gooddata::Client.v2 }
  let(:gooddata_connection) { stub('GD connection') }

  describe '#resource' do
    it 'returns the resource name' do
      assert_equal 'GooddataConnectivityDiagnostic', subject.resource
    end
  end

  describe '#check' do
    describe 'when there is a exception' do
      before do
        gooddata_client.stubs(:connection).raises(StandardError)
      end

      it 'reports error' do
        result = subject.check
        refute result.passed
        assert_equal 'Gooddata connectivity check failed with error: StandardError', result.message
      end
    end

    describe 'when connected to gooddata' do
      before do
        login_stub = stub('GD login')
        gooddata_client.expects(:connection).returns(login_stub)
        login_stub.expects(:log_in).returns(gooddata_connection)
      end

      describe 'and login was successful' do
        before do
          gooddata_connection.stubs(:status).returns(:logged_in)
        end

        it 'succeeds' do
          result = subject.check
          assert result.passed
          assert_equal 'Successfully connected to Gooddata', result.message
        end
      end

      describe 'and login failed' do
        before do
          gooddata_connection.stubs(:status).returns(:error)
        end

        it 'succeeds' do
          result = subject.check
          refute result.passed
          assert_equal 'Failed to verify login to gooddata', result.message
        end
      end
    end
  end
end
