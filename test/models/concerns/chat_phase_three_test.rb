require_relative '../../support/test_helper'
require_relative '../../support/chat_phase_3_helper'
require_relative '../../support/entitlement_test_helper'
require_relative '../../support/suite_test_helper'

SingleCov.covered!

describe ChatPhaseThree do
  include Api::Presentation::TestHelper
  include ChatPhase3Helper
  include EntitlementTestHelper
  include SuiteTestHelper

  ZBC::Zopim::Subscription.include(ChatPhaseThree)

  let(:account) { accounts(:multiproduct) }
  let(:subject) { account.zopim_subscription }
  let(:current_chat_product) { stub(attributes: FactoryBot.build(:chat_trial_product)) }
  let(:expected_payload) { {} }
  let(:expected_context) {}
  trial_plan_type = ZBC::Zopim::PlanType::Trial

  before do
    stub_staff_service_patch_request(account.subdomain, '')
    Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).never
    create_phase_3_zopim_subscription(account, plan_type: trial_plan_type.name)
    ZBC::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)
    ZopimIntegration.any_instance.stubs(:ensure_not_phase_four)
  end

  describe 'when the skip_chat_product_sync flag is set' do
    it 'does not call the account service on updates' do
      subject.skip_chat_product_sync = true
      subject.purchased_at = Time.now
      subject.save
    end
  end

  describe 'when the skip_chat_product_sync flag is not set' do
    before do
      Zendesk::Accounts::Client.any_instance.stubs(chat_product: current_chat_product)
      Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).
        with('chat', { product: expected_payload }, context: expected_context).once
    end

    describe 'when chat is purchased' do
      let(:expected_payload) { { state: 'subscribed', plan_settings: { plan_type: 0, max_agents: 100 } } }
      let(:expected_context) { "cp3_sync_record" }

      it 'includes changes to the state when calling the account service' do
        subject.purchased_at = Time.now
        subject.save!
      end
    end

    describe 'when chat plan_type is updated' do
      updated_plan_type = ZBC::Zopim::PlanType::Premium
      let(:expected_payload) { { state: 'trial', plan_settings: { plan_type: 4, max_agents: 100 } } }
      let(:expected_context) { "cp3_sync_record" }

      it 'includes changes to the plan_type when calling the account service' do
        subject.plan_type = updated_plan_type.name
        subject.save!
      end
    end

    describe 'when chat plan_type is updated to lite and purchased_at is not present' do
      updated_plan_type = ZBC::Zopim::PlanType::Lite
      let(:expected_payload) { { state: 'free', plan_settings: { plan_type: 1, max_agents: 100 } } }
      let(:expected_context) { "cp3_sync_record" }

      it 'includes changes to the plan_type and state when calling the account service' do
        subject.plan_type = updated_plan_type.name
        subject.save!
      end
    end

    describe 'when chat plan_type is updated to lite and purchased_at is present' do
      updated_plan_type = ZBC::Zopim::PlanType::Lite
      let(:expected_payload) { { state: 'subscribed', plan_settings: { plan_type: 1, max_agents: 100 } } }
      let(:expected_context) { "cp3_sync_record" }

      it 'includes changes to the state when calling the account service' do
        subject.purchased_at = Time.now
        subject.plan_type = updated_plan_type.name
        subject.save!
      end
    end

    describe 'when chat max_agents is updated' do
      updated_max_agents = 200
      let(:expected_payload) { { state: 'trial', plan_settings: { plan_type: 0, max_agents: 200 } } }
      let(:expected_context) { "cp3_sync_record" }

      it 'includes the changes to the max_agents count when calling the account service' do
        subject.max_agents = updated_max_agents
        subject.save!
      end
    end

    describe 'when the zopim subscription is destroyed' do
      let(:expected_payload) { { state: 'cancelled' } }
      let(:expected_context) { "cp3_cancel_record" }

      it 'sets the product record state to cancelled' do
        subject.destroy
      end
    end

    describe "when the zopim subscription's status is set to cancelled" do
      let(:expected_payload) { { state: 'cancelled', plan_settings: { plan_type: 0, max_agents: 100 } } }
      let(:expected_context) { "cp3_sync_record" }

      it 'sets the product record state to cancelled' do
        subject.status = ZBC::Zopim::SubscriptionStatus::Cancelled
        subject.save!
      end
    end

    describe 'when calls to the account service fails' do
      before do
        Zendesk::Accounts::Client.any_instance.unstub(:update_or_create_product!)
        Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).raises(Kragle::ClientError)
      end

      it 'raises an exception when trying to delete the record' do
        err = assert_raises ActiveRecord::RecordNotSaved do
          subject.destroy
        end

        assert_match(/Cannot delete your chat subscription/, err.message)
      end

      it 'raises an exception when trying to update the record' do
        err = assert_raises ActiveRecord::RecordNotSaved do
          subject.max_agents = 193
          subject.save!
        end

        assert_match(/Cannot update your chat subscription/, err.message)
      end
    end
  end

  describe '#make_phase_three' do
    let(:expected_payload) { { state: 'trial', plan_settings: { phase: 3, plan_type: 0, max_agents: 100 } } }

    before do
      Zendesk::Accounts::Client.any_instance.stubs(chat_product: current_chat_product)
      Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).
        with('chat', { product: expected_payload }, context: "cp3_make_p3").once
    end

    it 'calls the account service with the current zopim subscription info as payload' do
      subject.make_phase_three
    end
  end

  describe 'when the skip_chat_only_permission_set flag is set' do
    before do
      stub_staff_service_patch_request(account.subdomain, '')
      PermissionSet.expects(:create_chat_agent!).with(account).never
    end

    it 'does not call the zopim subscription enable_chat_permission_set function' do
      create_phase_3_zopim_subscription(account, plan_type: trial_plan_type.name, skip_chat_only_permission_set: true)
    end
  end

  describe 'when the skip_chat_only_permission_set flag is not set' do
    before do
      stub_staff_service_patch_request(account.subdomain, '')
      PermissionSet.expects(:create_chat_agent!).with(account).once
    end

    it 'does call the zopim subscription enable_chat_permission_set function' do
      create_phase_3_zopim_subscription(account, plan_type: trial_plan_type.name)
    end
  end
end
