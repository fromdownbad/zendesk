require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 11

describe TrialLimit do
  Zendesk::RedisStore.redis_client = FakeRedis::Redis.new
  let(:account) { accounts(:minimum) }
  let(:redis_client) { TrialLimit.redis_client }
  let(:model) { :user_identity }
  let(:duration) { 45.days.to_i }
  let(:trial_limit_key) { TrialLimit.send(:trial_limit_key, account, model) }

  it 'checks that limit method is defined' do
    assert account.send("trial_account_#{model}_limit")
  end

  # ensure a clean state both before *AND* after tests to prevent trial limits causing other test files to fail
  before { redis_client.del(trial_limit_key) }
  after  { redis_client.del(trial_limit_key) }

  describe '#increment_trial_limit_count' do
    it 'increments limit count' do
      TrialLimit.increment_trial_limit_count(account, model, duration)

      assert_equal '1', redis_client.get(trial_limit_key)
    end

    it 'sets redis tll' do
      TrialLimit.increment_trial_limit_count(account, model, duration)
      assert_operator redis_client.ttl(trial_limit_key), :>, duration - 1
      assert_operator redis_client.ttl(trial_limit_key), :<=, duration + 1
    end
  end

  describe '#trial_limit_key' do
    it 'returns correct redis key' do
      assert_equal "account_#{account.id}_trial_limit_#{model}", TrialLimit.send(:trial_limit_key, account, model)
    end
  end

  describe '#has_trial_limit?' do
    let(:settings_key) { "#{model}_limit_for_trialers".to_sym }

    describe 'when arturo is enabled' do
      before do
        # Default settings which return true for #has_trial_limit
        account.stubs(:arturo_enabled?).with(settings_key).returns(true)
        account.stubs(:is_trial?).returns(true)
        account.stubs(:whitelisted?).returns(false)
        account.stubs(:is_sandbox?).returns(false)
        account.stubs(:sell_product?).returns(false)
      end

      it 'returns true if trial account and trial limit setting is true' do
        account.settings.stubs(settings_key).returns(true)

        assert TrialLimit.has_trial_limit?(account, model)
      end

      it 'returns false if trial account and trial limit setting is false' do
        account.stubs(:is_trial?).returns(true)
        account.settings.stubs(settings_key).returns(false)

        refute TrialLimit.has_trial_limit?(account, model)
      end

      it 'returns false if not in trial and trial limit setting is true' do
        account.stubs(:is_trial?).returns(false)
        account.settings.stubs(settings_key).returns(true)

        refute TrialLimit.has_trial_limit?(account, model)
      end

      it 'returns false if not in trial and trial limit setting is false' do
        account.stubs(:is_trial?).returns(false)
        account.settings.stubs(settings_key).returns(false)

        refute TrialLimit.has_trial_limit?(account, model)
      end

      it 'returns false for whitelisted accounts' do
        account.stubs(:whitelisted?).returns(true)

        refute TrialLimit.has_trial_limit?(account, model)
      end

      it 'returns true for not whitelisted accounts' do
        account.stubs(:whitelisted?).returns(false)

        assert TrialLimit.has_trial_limit?(account, model)
      end

      it 'returns false for sell accounts' do
        account.stubs(:sell_product?).returns(true)

        refute TrialLimit.has_trial_limit?(account, model)
      end

      it 'returns true for not sell_product accounts' do
        account.stubs(:sell_product?).returns(false)

        assert TrialLimit.has_trial_limit?(account, model)
      end

      it 'returns false for sandbox accounts' do
        account.stubs(:is_sandbox?).returns(true)

        refute TrialLimit.has_trial_limit?(account, model)
      end

      it 'returns true for not sandbox accounts' do
        account.stubs(:is_sandbox?).returns(false)

        assert TrialLimit.has_trial_limit?(account, model)
      end
    end

    describe 'when arturo is disabled' do
      before do
        account.stubs(:arturo_enabled?).with(settings_key).returns(false)
      end

      it 'returns false' do
        account.stubs(:is_trial?).returns(true)
        account.settings.stubs(settings_key).returns(true)

        refute TrialLimit.has_trial_limit?(account, model)
      end
    end
  end

  describe '#reached_trial_limit?' do
    before do
      TrialLimit.stubs(:has_trial_limit?).with(account, model).returns(true)
    end

    it 'returns true when count equals limit' do
      redis_client.set(trial_limit_key, '10')

      assert TrialLimit.reached_trial_limit?(account, model, 10)
    end

    it 'returns true when count is greater than limit' do
      redis_client.set(trial_limit_key, '10')

      assert TrialLimit.reached_trial_limit?(account, model, 5)
    end

    it 'returns false when count is less than limit' do
      redis_client.set(trial_limit_key, '5')

      refute TrialLimit.reached_trial_limit?(account, model, 10)
    end
  end

  describe '#trial_limit_count' do
    it 'returns correct count' do
      redis_client.set(trial_limit_key, '11')

      assert_equal 11, TrialLimit.trial_limit_count(account, model)
    end

    it "returns 0 if key doesn't exist" do
      assert_equal 0, TrialLimit.trial_limit_count(account, model)
    end
  end

  describe 'reset_limit_count' do
    it 'deletes existing redis key' do
      redis_client.set(trial_limit_key, 1000)
      assert_equal 1000, TrialLimit.trial_limit_count(account, model)
      TrialLimit.reset_limit_count(account, model)
      assert_equal 0, TrialLimit.trial_limit_count(account, model)
    end

    it 'returns 0 for non-existing redis key' do
      random_model = :random_model
      TrialLimit.reset_limit_count(account, random_model)
      assert_equal 0, TrialLimit.trial_limit_count(account, random_model)
    end
  end
end
