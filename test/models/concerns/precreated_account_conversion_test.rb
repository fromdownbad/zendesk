require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'Tickets::PrecreatedAccountConversion' do
  fixtures :tickets, :events, :users

  let(:user) do
    users(:minimum_agent)
  end

  let(:ticket) do
    ticket = tickets(:minimum_1)
    ticket.will_be_saved_by(user)
    ticket
  end

  describe 'updates timestamps' do
    before { travel_to Time.now }
    after { travel_back }

    it "on user" do
      future = Time.zone.now + 1.hour
      travel 1.hour do
        user.run_callbacks(:account_convert)
      end
      assert_equal user.account.created_at, user.created_at
      assert_equal future, user.updated_at
    end

    it "on ticket" do
      future = Time.zone.now + 1.hour

      travel 1.hour do
        ticket.run_callbacks(:account_convert)
      end

      assert_equal ticket.account.created_at, ticket.created_at
      assert_equal future, ticket.updated_at
    end

    describe 'when a timestamps are updated on a ticket' do
      it 'publishes the ticket via entity publisher' do
        ViewsObservers::EntityPublisher.
          any_instance.
          expects(:publish).
          with(ticket)

        ticket.run_callbacks(:account_convert)
      end
    end
  end

  describe 'radar' do
    describe 'a ticket' do
      it 'does not notify radar when updated by a precreated account' do
        assert ticket.audit.present?
        refute ticket.audit.disable_triggers
        RadarFactory.expects(:create_radar_notification).never
        ticket.run_callbacks(:account_convert)
      end

      it 'does not skip radar notification for a regular account' do
        ticket.expects(:disable_audit).never
        RadarFactory.expects(:create_radar_notification).returns(stub(updated: true))
        ticket.save!
      end
    end

    describe 'a user' do
      it 'does not notify radar when updated by a precreated account' do
        refute user.instance_variable_get(:@is_during_account_conversion)
        RadarFactory.expects(:create_radar_notification).never
        user.run_callbacks(:account_convert)
        assert user.instance_variable_get(:@is_during_account_conversion)
      end

      it 'does not skip radar notification for a regular account' do
        user.tickets << ticket
        user.expects(:mark_as_during_account_conversion).never
        RadarFactory.expects(:create_radar_notification)
        user.save!
      end
    end
  end
end
