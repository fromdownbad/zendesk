require_relative '../../support/test_helper'

SingleCov.covered!

describe RawHttp do
  include ArturoTestHelper

  fixtures :accounts, :tickets, :events, :targets

  let(:account) { accounts(:minimum) }
  let(:event) { events(:create_external_for_minimum_ticket_1) }
  let(:raw_request) { "GET /api/v2/users/me.json HTTP/1.1\r\nAccept-Encoding: gzip;q=1.0,deflate;q=0.6,identity;q=0.3\r\nAccept: */*\r\nUser-Agent: Ruby\r\nAuthorization: admin@zendesk.com/token:YfgfkNRkVY4DtisXUnKuIbC0vHnRAfneVBvdWRlI\r\nConnection: close\r\nHost: support.zd-dev.com\r\n\r\n" }

  describe "TargetFailure" do
    let(:target) { targets(:url_valid) }
    let(:target_failure) { new_target_failure }
    let(:response_body) { 'response body' }

    describe "with details" do
      before do
        target.raw_http_capture.transactions.last.request.stubs(raw: 'request')
        target.raw_http_capture.transactions.last.response.stubs(headers: "HTTP/1.1 400 Bad Request")
      end

      it "sets raw_response and raw_request" do
        target_failure.save
        assert_equal "HTTP/1.1 400 Bad Request\r\n\r\nresponse body", target_failure.raw_response
        assert_equal 'request', target_failure.raw_request
      end
    end

    describe "when there is an empty response" do
      let(:response_body) { '' }

      before do
        target.raw_http_capture.transactions.last.response.stubs(headers: nil, body: nil, raw: nil)
      end

      it "sets raw_response to nil" do
        assert_nil new_target_failure.raw_response
      end
    end

    describe "when there is an empty request" do
      before do
        target.raw_http_capture.transactions.last.request.stubs(raw: '')
      end

      it "sets raw_request to nil" do
        assert_nil new_target_failure.raw_request
      end
    end

    describe "when the response contains invalid UTF-8" do
      let(:response_body) { "Ol\xAD" }

      before do
        target.raw_http_capture.transactions.last.response.stubs(headers: "HTTP/1.1 400 Bad Request")
      end

      it "forces the encoding to UTF-8" do
        target_failure.save
        assert target_failure.raw_response.ends_with?("Ol�")
      end
    end

    describe "when Authorization header is present" do
      before do
        target.raw_http_capture.transactions.last.request.stubs(raw: raw_request)
        target.raw_http_capture.transactions.last.response.stubs(headers: "HTTP/1.1 400 Bad Request")
      end

      describe_with_arturo_disabled :targets_remove_authorization_header do
        before { target_failure.save! }

        it "saves all the raw request" do
          assert_equal target_failure.raw_request, raw_request
        end
      end

      describe_with_arturo_enabled :targets_remove_authorization_header do
        before { target_failure.save! }

        it "does not include the Authorization header" do
          refute target_failure.raw_request.include?("Authorization")
        end

        it "includes the other header item" do
          target_raw_request = target_failure.raw_request
          assert target_raw_request.include?("GET")
          assert target_raw_request.include?("Accept-Encoding")
          assert target_raw_request.include?("User-Agent")
          assert target_raw_request.include?("Host")
        end
      end
    end
  end

  describe "UrlTargetFailure" do
    let(:ticket) { event.ticket }
    let(:target) do
      UrlTargetV2.create(
        title: "Url Target",
        account: accounts(:minimum),
        url: "http://www.example.com/a",
        method: "get"
      )
    end

    let(:url_target_failure) do
      UrlTargetFailure.new do |new_fail|
        new_fail.account_id = accounts(:minimum).id
        new_fail.event = event
        new_fail.exception = Exception.to_s
        new_fail.message = "message"
        new_fail.target = target
        new_fail.consecutive_failure_count = 5
        new_fail.raw_http_capture = target.raw_http_capture
      end
    end

    describe "when Authorization header is present" do
      before do
        target.raw_http_capture.transactions.last.request.stubs(raw: raw_request)
        target.raw_http_capture.transactions.last.response.stubs(headers: "HTTP/1.1 400 Bad Request")
      end

      describe_with_arturo_disabled :targets_remove_authorization_header do
        before { url_target_failure.save! }

        it "saves all the raw request" do
          assert_equal url_target_failure.raw_request, raw_request
        end
      end

      describe_with_arturo_enabled :targets_remove_authorization_header do
        before { url_target_failure.save! }

        it "does not include the Authorization header" do
          refute url_target_failure.raw_request.include?("Authorization")
        end

        it "includes the other header item" do
          target_raw_request = url_target_failure.raw_request
          assert target_raw_request.include?("GET")
          assert target_raw_request.include?("Accept-Encoding")
          assert target_raw_request.include?("User-Agent")
          assert target_raw_request.include?("Host")
        end
      end
    end
  end

  private

  def new_target_failure
    TargetFailure.new(
      account: account,
      event: event,
      exception: Exception.to_s,
      message: "message",
      target: target,
      consecutive_failure_count: 5,
      raw_http_capture: target.raw_http_capture,
      response_body: response_body
    )
  end
end
