require_relative '../../support/test_helper'
require_relative '../../support/mailer_test_helper'

SingleCov.covered!

describe TrialLimit::Observer do
  include MailerTestHelper

  Zendesk::RedisStore.redis_client = FakeRedis::Redis.new
  let(:user) { users(:minimum_admin) }
  let(:account) { user.account }
  let(:redis_client) { TrialLimit.redis_client }
  let(:statsd_client_trial_limit) { Zendesk::StatsD::Client.new(namespace: 'trial_limit') }
  let(:model) { :user_identity }
  let(:admins) { account.admins }
  let(:subject) { I18n.t('txt.email.trial_limit_mailer.user_identity_limit_exceeded_notice_v2.subject') }
  let(:new_user) do
    User.new(
      account: account,
      email: 'limit@example.com',
      name: 'random'
    )
  end

  describe '#send_trial_limit_notification' do
    let(:key) { 'user_identity_notice' }
    let(:trial_limit_key) { TrialLimit.send(:trial_limit_key, account, key) }
    let(:new_identity) do
      UserEmailIdentity.new(
        user:    user,
        account: account,
        value:   'new-id@zendesk.com'
      )
    end

    before do
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries.clear
      User.any_instance.stubs(:whitelisted_from_user_limits?).returns(false)
      UserIdentity.any_instance.stubs(:whitelisted_from_user_limits?).returns(false)
    end

    describe 'when user identity limit exceeded' do
      before do
        TrialLimit.stubs(:reached_trial_limit?).with(account, model, 1000).returns(true)
        FraudScoreJob.stubs(:enqueue_account)
      end

      describe 'when a notification trial_limit key does not exist' do
        before do
          redis_client.del(trial_limit_key)
        end

        describe 'when creating new identity for existing user' do
          it 'queues a fraud score job' do
            FraudScoreJob.expects(:enqueue_account).with(account, :user_identity_trial_limit_exceeded).once
            new_identity.save
          end

          it 'sends email to all admins' do
            new_identity.save
            assert_equal admins.count, ActionMailer::Base.deliveries.count { |email| email.subject == subject }
          end

          it 'calls #increment_trial_limit_count with correct params' do
            TrialLimit.expects(:increment_trial_limit_count).with(account, key, 1.day.to_i).once
            new_identity.save
          end

          it 'increments notification count' do
            new_identity.save
            assert_equal 1, TrialLimit.trial_limit_count(account, key)
          end

          it 'only sends emails once' do
            # once within 24 hours
            new_identity.save
            ActionMailer::Base.deliveries.clear
            id2 = UserEmailIdentity.new(
              user:    user,
              account: account,
              value:   'id2@zendesk.com'
            )
            id2.save
            assert_equal 0, ActionMailer::Base.deliveries.count { |email| email.subject == subject }
          end
        end

        describe 'when creating a new user' do
          it 'queues a fraud score job' do
            FraudScoreJob.expects(:enqueue_account).with(account, :user_identity_trial_limit_exceeded).once
            new_user.save
          end

          it 'sends email' do
            new_user.save
            assert_equal admins.count, ActionMailer::Base.deliveries.count { |email| email.subject == subject }
          end

          it 'calls #increment_trial_limit_count with correct params' do
            TrialLimit.expects(:increment_trial_limit_count).with(account, key, 1.day.to_i).once
            new_user.save
          end

          it 'increments notification count' do
            new_user.save
            assert_equal 1, TrialLimit.trial_limit_count(account, key)
          end

          it 'only sends emails once' do
            # once within 24 hours
            new_user.save
            ActionMailer::Base.deliveries.clear
            user2 = User.new(
              account: account,
              email: 'limit2@example.com',
              name: 'random2'
            )
            user2.save
            assert_equal 0, ActionMailer::Base.deliveries.count { |email| email.subject == subject }
          end
        end
      end

      describe 'when notification trial_limit key equals 1' do
        before do
          redis_client.set(trial_limit_key, '1')
          FraudScoreJob.expects(:enqueue_account).never
        end
        it 'does not send email' do
          new_identity.save
          assert_equal 0, ActionMailer::Base.deliveries.count { |email| email.subject == subject }
        end

        it 'does not increment notification count' do
          new_identity.save
          assert_equal 1, TrialLimit.trial_limit_count(account, key)
        end
      end
    end

    describe 'when below user identity limit' do
      describe 'when creating new identity for existing user' do
        it 'does not send user identity limit exceeded notification email' do
          new_identity.save
          assert_equal 0, ActionMailer::Base.deliveries.count { |email| email.subject == subject }
        end
      end

      describe 'when creating new user' do
        it 'does not send user identity limit exceeded notification email' do
          new_user.save
          assert_equal 0, ActionMailer::Base.deliveries.count { |email| email.subject == subject }
        end
      end
    end
  end

  describe '#disable_account_for_exceeding_trial_limit' do
    describe 'when user identity limit exceeded' do
      before do
        TrialLimit.stubs(:reached_trial_limit?).with(account, model, 1000).returns(true)
        User.any_instance.stubs(:whitelisted_from_user_limits?).returns(false)
        FraudScoreJob.stubs(:enqueue_account)
      end

      it 'enqueues fraud_score_job' do
        FraudScoreJob.expects(:enqueue_account).with(account, Fraud::SourceEvent::USER_IDENTITY_TRIAL_LIMIT_EXCEEDED).once
        new_user.save
      end

      it 'creates audit entry' do
        new_user.save
        assert_includes CIA::Event.for_account(account).map(&:message), I18n.t('txt.audit_message.user_identity_limit.message')
      end

      describe 'when the Account is #whitelisted_from_user_limits' do
        before do
          User.any_instance.stubs(:whitelisted_from_user_limits?).returns(true)
        end
        describe 'when creating new identity for existing user' do
          it 'does not send user identity limit exceeded notification email' do
            new_user.save
            refute account.settings.is_abusive
            assert account.is_serviceable
          end
        end
      end
    end

    describe 'when user identity limit NOT exceeded' do
      before do
        TrialLimit.stubs(:reached_trial_limit?).with(account, model, 1000).returns(false)
      end

      it 'does not update account settings' do
        new_user.save
        refute account.settings.is_abusive
        assert account.is_serviceable
      end
    end
  end
end
