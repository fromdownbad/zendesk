require_relative '../../support/test_helper'
require_relative '../../support/account_products_test_helper'
require_relative '../../support/zopim_test_helper'

SingleCov.covered!

describe ChatPhaseFour do
  include Api::Presentation::TestHelper
  include AccountProductsTestHelper
  include ZopimTestHelper

  ZBC::Zopim::Subscription.include(ChatPhaseFour)

  let(:account) { accounts(:multiproduct) }

  let(:client_product) { Zendesk::Accounts::Client.any_instance.expects(:product!).with('chat').once }

  let(:phase_four_chat_product) do
    Zendesk::Accounts::Product.new(make_product(product_name: 'chat', plan_settings: { 'phase' => 4 }))
  end

  let(:phase_three_chat_product) do
    Zendesk::Accounts::Product.new(make_product(product_name: 'chat', plan_settings: { 'phase' => 3 }))
  end

  before do
    setup_zopim_stubs
    ZopimIntegration.any_instance.unstub(:ensure_not_phase_four)
    ZendeskBillingCore::Zopim::Subscription.any_instance.unstub(:ensure_not_phase_four)
  end

  describe 'when chat product phase is 4' do
    before do
      client_product.returns(phase_four_chat_product)
    end

    it 'does not allow zopim_integration to be created' do
      refute create_zopim_integration
    end

    it 'does not allow zopim_subscription to be created' do
      refute create_zopim_subscription
    end
  end

  describe 'when an error is raised while fetching the chat product' do
    before do
      client_product.raises(Kragle::ResponseError)
    end

    it 'does not allow zopim_integration to be created' do
      refute create_zopim_integration
    end

    it 'does not allow zopim_subscription to be created' do
      refute create_zopim_subscription
    end
  end

  describe 'when chat product does not exist' do
    before do
      client_product.raises(Kragle::ResourceNotFound)
    end

    it 'allows zopim_integration to be created' do
      assert create_zopim_integration
    end

    it 'allows zopim_subscription to be created' do
      assert create_zopim_subscription
    end
  end

  describe 'when chat product phase is not 4' do
    before do
      client_product.returns(phase_three_chat_product)
    end

    it 'allows zopim_integration to be created' do
      assert create_zopim_integration
    end

    it 'allows zopim_subscription to be created' do
      assert create_zopim_subscription
    end
  end

  describe 'when phase attribute is 3' do
    before do
      stub_request(:post, %r{embeddable/api/internal/config_sets.json})
      client_product.never
    end

    it 'allows zopim_integration to be created' do
      assert create_zopim_integration(true)
    end

    it 'allows zopim_subscription to be created' do
      assert create_zopim_subscription(true)
    end
  end

  describe 'existing data' do
    before do
      client_product.raises(Kragle::ResourceNotFound)
    end

    describe 'zopim_integration' do
      before do
        create_zopim_integration
        assert account.zopim_integration.reload.present?
      end

      it 'allows existing zopim_integration to be updated' do
        account.zopim_integration.zopim_key = 'abc'
        assert account.zopim_integration.save
      end

      it 'allows existing zopim_integration to be destroyed' do
        assert account.zopim_integration.destroy
      end
    end

    describe 'zopim_subscription' do
      before do
        create_zopim_subscription
        assert account.zopim_subscription.reload.present?
      end

      it 'allows existing zopim_subscription to be updated' do
        account.zopim_subscription.purchased_at = Time.now
        assert account.zopim_subscription.save
      end

      it 'allows existing zopim_subscription to be destroyed' do
        assert account.zopim_subscription.destroy
      end
    end
  end

  def create_zopim_integration(phase_three = false)
    zi = account.build_zopim_integration(external_zopim_id: 1234, zopim_key: 'abcd')
    zi.phase = 3 if phase_three
    zi.save
  end

  def create_zopim_subscription(phase_three = false)
    zs = account.build_zopim_subscription(
      status: ZBC::Zopim::SubscriptionStatus::Active,
      plan_type: ZBC::Zopim::PlanType::Lite.name,
      max_agents: 100
    )
    zs.zopim_account_id = 1
    zs.phase = 3 if phase_three
    zs.save
  end
end
