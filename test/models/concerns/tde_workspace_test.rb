require_relative '../../support/test_helper'
require_relative '../../support/arturo_test_helper'
require_relative '../../support/rule'

SingleCov.covered!

describe TdeWorkspace do
  include TestSupport::Rule::Helper
  include ArturoTestHelper

  fixtures :tickets, :accounts, :users, :workspaces
  resque_inline false

  describe 'workspace' do
    let(:ticket_1) { tickets(:minimum_1) }
    let(:ticket_2) { tickets(:minimum_2) }
    let(:definition_1) { {"conditions": {"all": [{"field": "update_type", "operator": "is", "value": "Change"}], "any": []}} }
    let(:definition_2) { {"conditions": {"all": [{"field": "update_type", "operator": "is", "value": "Change"}], "any": []}} }

    let(:workspace_1) do
      workspace = Workspace.new
      workspace.account_id = ticket_1.account_id
      workspace.title = "Returns"
      workspace.description = "Returns workspace"
      workspace.position = 1
      workspace.definition = definition_1
      workspace.save
      workspace
    end

    let(:workspace_2) do
      workspace = Workspace.new
      workspace.account_id = ticket_2.account_id
      workspace.title = "Workspace 2"
      workspace.description = "Returns workspace"
      workspace.position = 2
      workspace.definition = definition_2
      workspace.save
      workspace
    end

    it 'returns the correct workspace on first assignment' do
      ticket_1.workspace = workspace_1
      ticket_2.workspace = workspace_1
      assert_equal ticket_1.workspace, workspace_1
      assert_equal ticket_2.workspace, workspace_1
    end

    it 'returns the correct workspace on updates' do
      ticket_1.workspace = workspace_1
      ticket_1.save
      ticket_1.reload
      assert_equal ticket_1.workspace, workspace_1

      ticket_1.workspace = workspace_2
      ticket_1.save
      ticket_1.reload
      assert_equal ticket_1.workspace, workspace_2
    end

    it 'set workspace to nil if nil is passed' do
      ticket_1.workspace = workspace_1
      ticket_1.save
      ticket_1.reload
      assert_equal ticket_1.workspace, workspace_1

      ticket_1.workspace = nil
      ticket_1.save
      ticket_1.reload

      assert_nil ticket_1.workspace
      assert_empty ticket_1.ticket_workspaces
    end
  end

  describe "when account has contextual workspaces" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_agent) }
    let(:ticket) { tickets(:minimum_1) }

    before do
      Account.any_instance.stubs(:has_contextual_workspaces?).returns(true)
    end

    describe 'enqueues_workspace_assignment' do
      it 'enqueues a workspace assignment' do
        TdeWorkspaces::AssignWorkspaceJob.expects(:enqueue).once

        ticket.will_be_saved_by(user)
        ticket.description = "What is happening?"
        ticket.save!
      end
    end

    describe 'when contextual_workspaces_in_js arturo is enabled' do
      it 'does not enqueue a workspace job' do
        Account.any_instance.stubs(:has_contextual_workspaces_in_js?).returns(true)
        Account.any_instance.stubs(:has_contextual_workspaces?).returns(true)

        TdeWorkspaces::AssignWorkspaceJob.expects(:enqueue).never

        ticket.will_be_saved_by(user)
        ticket.requester = user
        ticket.description = "What is happening?"
        ticket.save
      end
    end
  end

  describe "reassign_workspace" do
    let(:ticket) { tickets(:minimum_1) }
    let(:inactive_workspace) { workspaces(:inactive_workspace) }
    let(:workspace_1) { workspaces(:refund_workspace) }
    let(:workspace_2) { workspaces(:billing_workspace) }
    let(:admin) { users(:minimum_admin) }

    it "assigns the workspace to the ticket if it is a match, higher priority, and active" do
      ticket.workspace = workspace_2
      ticket.will_be_saved_by(admin)
      ticket.save!

      assert_equal workspace_2, ticket.workspace

      Zendesk::Rules::Match.stubs(:match?).returns(true)

      ticket.reassign_workspace
      ticket.reload

      # reassign_workspace method sorts workspace by position and so workspace_1 has higher priority
      refute_equal inactive_workspace, ticket.workspace
      assert_equal workspace_1, ticket.workspace
    end

    it "assigns workspace to nil if there is no matching workspace" do
      ticket.workspace = workspace_2
      ticket.will_be_saved_by(admin)
      ticket.save!

      assert_equal workspace_2, ticket.workspace

      Zendesk::Rules::Match.stubs(:match?).returns(false)

      ticket.reassign_workspace
      ticket.reload

      assert_nil ticket.workspace
    end

    it "throws an error when there is an exception during the match method execution" do
      Zendesk::Rules::Match.stubs(:match?).raises(StandardError)
      ZendeskExceptions::Logger.expects(:logger).at_least_once
      ticket.reassign_workspace
    end
  end

  describe_with_arturo_enabled(:contextual_workspaces_in_js) do
    describe "add_contextual_workspace_audit_event" do
      let(:account) { accounts(:minimum) }
      let(:ticket) { account.tickets.new }
      let(:ticket_1) { tickets(:minimum_1) }
      let(:workspace_1) { workspaces(:refund_workspace) }
      let(:workspace_2) { workspaces(:billing_workspace) }
      let(:workspace_1_attributes) { { id: workspace_1.id, title: workspace_1.title } }
      let(:workspace_2_attributes) { { id: workspace_2.id, title: workspace_2.title } }

      it "adds a new workspace audit event for a new ticket with an workspace" do
        WorkspaceChanged.any_instance.expects(:add_new_workspace_audit!).once
        ticket.add_contextual_workspace_audit_event(workspace: workspace_1_attributes, type: 'ADD')
      end

      it "adds a new workspace audit event for a ticket with a different workspace" do
        ticket_1.will_be_saved_by(User.system)
        ticket_1.add_contextual_workspace_audit_event(workspace: workspace_1_attributes, type: 'ADD')
        ticket_1.save!

        assert_equal 1, ticket_1.events.where(type: 'WorkspaceChanged', account_id: ticket_1.account_id).count

        ticket_1.will_be_saved_by(User.system)
        ticket_1.add_contextual_workspace_audit_event(workspace: workspace_2_attributes, type: 'CHANGE', previous_workspace: workspace_1_attributes)
        ticket_1.save!

        assert_equal 2, ticket_1.events.where(type: 'WorkspaceChanged', account_id: ticket_1.account_id).count
      end

      it "adds a new workspace audit event during a delete workspace" do
        ticket_1.will_be_saved_by(User.system)
        ticket_1.add_contextual_workspace_audit_event(workspace: workspace_1_attributes, type: 'ADD')
        ticket_1.save!

        ticket_1.will_be_saved_by(User.system)
        ticket_1.add_contextual_workspace_audit_event(workspace: workspace_1_attributes, type: 'DELETE')
        ticket_1.save!

        assert_equal 2, ticket_1.events.where(type: 'WorkspaceChanged', account_id: ticket_1.account_id).count
      end

      it "should not create an additional audit if one already exists" do
        ticket_1.will_be_saved_by(User.system)
        Ticket.any_instance.expects(:will_be_saved_by).never
        ticket_1.add_contextual_workspace_audit_event(workspace_1_attributes)
      end

      it "should not add an audit event if the action is not supported" do
        ticket_1.will_be_saved_by(User.system)
        Ticket.any_instance.expects(:will_be_saved_by).never
        ticket_1.add_contextual_workspace_audit_event(workspace: { id: workspace_1.id })
        ticket_1.save!

        assert_equal 0, ticket_1.events.where(type: 'WorkspaceChanged', account_id: ticket_1.account_id).count
      end
    end
  end
end
