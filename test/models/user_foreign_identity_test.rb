require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe UserForeignIdentity do
  fixtures :accounts, :users

  describe "validations" do
    let(:ufi) do
      UserForeignIdentity.new value: Digest::SHA1.hexdigest('foo'), user: users(:minimum_end_user)
    end

    it "is valid" do
      assert ufi.valid?
    end

    it "is invalid with non-hex value" do
      ufi.value = ",.,/zc,zcx"
      refute ufi.valid?
      assert_equal [",.,/zc,zcx is not properly formatted"], ufi.errors[:value]
    end
  end

  describe ".identity_type" do
    it "is foreign" do
      assert_equal(:foreign, UserForeignIdentity.identity_type)
    end
  end

  describe "#is_verified?" do
    it "is true" do
      ufi = UserForeignIdentity.create!(value: Digest::SHA1.hexdigest('foo'), user: users(:minimum_end_user))
      assert ufi.is_verified?
    end
  end
end
