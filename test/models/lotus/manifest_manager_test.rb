require_relative '../../support/test_helper'

SingleCov.covered!

describe Lotus::ManifestManager do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:subject) { Lotus::ManifestManager.new(account) }
  let(:mock_connection) { Faraday::Connection.new }
  let(:sha) { 'b79d7a033597fe3e28bc94d1892911bad1c5b042' }
  let(:version) { 3000 }
  let(:mock_manifest) do
    {
      'createdAt' => '2019-01-29T23:11:40Z',
      'css' => [],
      'js' => [
        {
          'name' => 'application',
          'path' => '/agent/assets/application-95f9efde48c76d4e8a6de6419d763058.js'
        }
      ],
      'sha' => sha,
      'version' => version
    }
  end

  describe '#initialize' do
    it 'properly sets account' do
      assert_equal subject.account, account
    end

    it 'properly uses @develop' do
      assert Lotus::ManifestManager.new(account, develop: true).instance_variable_get(:@develop)
    end

    it 'properly uses @nocache' do
      assert Lotus::ManifestManager.new(account, develop: true, nocache: true).instance_variable_get(:@nocache)
    end
  end

  describe '#find' do
    describe 'when in development' do
      before do
        Rails.env.stubs(:development?).returns(true)
      end

      it 'returns the manifest returned by the correct network request' do
        mock_configuration_settings = mock
        mock_configuration_settings.expects(:response).with(:raise_error)
        mock_configuration_settings.expects(:adapter).with(Faraday.default_adapter)
        mock_response = Faraday::Response.new(body: mock_manifest.to_json)
        mock_connection.expects(:get).with('/agent/tessaManifest.json').returns(mock_response)

        Faraday.expects(:new).with('http://minimum.zendesk-test.com/').yields(mock_configuration_settings).returns(mock_connection)
        subject.find(:current)
      end

      it 'raises the correct error when unsuccessful' do
        mock_configuration_settings = mock
        mock_configuration_settings.expects(:response).with(:raise_error)
        mock_configuration_settings.expects(:adapter).with(Faraday.default_adapter)
        mock_connection.expects(:get).with('/agent/tessaManifest.json').raises(Faraday::Error::ClientError, 'Some Error Text')

        Faraday.expects(:new).with('http://minimum.zendesk-test.com/').yields(mock_configuration_settings).returns(mock_connection)
        err = assert_raises do
          subject.find(:current)
        end

        assert_equal "Unable to access the Asset Manifest at http://minimum.zendesk-test.com/agent/tessaManifest.json. Lotus must be running and available in your environment.\n  #<Faraday::ClientError #<Faraday::ClientError: Some Error Text>> Some Error Text", err.message
      end
    end

    describe 'when in production' do
      before do
        Rails.env.stubs(:production?).returns(true)
      end

      it 'returns a correctly instantiated AssetManifest with content provided by the TessaClient::V1 response when the current manifest is requested' do
        mock_client = mock
        mock_client.expects(:fetch_current_manifest).returns(mock_manifest)
        TessaClient::V1.expects(:new).with(:lotus).returns(mock_client)
        mock_asset_manifest = mock
        Lotus::AssetManifest.expects(:new).with(account, mock_manifest, develop: false, nocache: false).returns(mock_asset_manifest)
        assert_equal subject.find(:current), mock_asset_manifest
      end

      it 'returns a correctly instantiated AssetManifest with content provided by the TessaClient::V1 response when a manifest is requested by sha' do
        mock_client = mock
        TessaClient::V1.expects(:new).with(:lotus).returns(mock_client)
        mock_client.expects(:fetch_manifest_by_sha).with(sha).returns(mock_manifest)
        mock_asset_manifest = mock
        Lotus::AssetManifest.expects(:new).with(account, mock_manifest, develop: false, nocache: false).returns(mock_asset_manifest)
        assert_equal subject.find(sha), mock_asset_manifest
      end

      it 'returns a correctly instantiated AssetManifest with content provided by the TessaClient::V1 response when a manifest is requested by version' do
        mock_client = mock
        TessaClient::V1.expects(:new).with(:lotus).returns(mock_client)
        mock_client.expects(:fetch_manifest_by_version).with(version.to_s).returns(mock_manifest)
        mock_asset_manifest = mock
        Lotus::AssetManifest.expects(:new).with(account, mock_manifest, develop: false, nocache: false).returns(mock_asset_manifest)
        assert_equal subject.find("v#{version}"), mock_asset_manifest
      end

      it 'returns a correctly instantiated AssetManifest with content provided by the TessaClient::V1 response when develop is passed as true' do
        mock_client = mock
        mock_client.expects(:fetch_current_manifest).returns(mock_manifest)
        TessaClient::V1.expects(:new).with(:lotus).returns(mock_client)
        mock_asset_manifest = mock
        Lotus::AssetManifest.expects(:new).with(account, mock_manifest, develop: true, nocache: false).returns(mock_asset_manifest)
        # As it currently stands, the controller will pass 'develop' as the find argument and
        # develop: true to the AssetManifest.
        dev_subject = Lotus::ManifestManager.new(account, develop: true, nocache: false)
        assert_equal dev_subject.find('develop'), mock_asset_manifest
      end

      it 'passes malformed refs to TessaClient::V1 as if they were properly formed shas' do
        malformed_sha = '1234567'
        mock_client = mock
        TessaClient::V1.expects(:new).with(:lotus).returns(mock_client)
        mock_client.expects(:fetch_manifest_by_sha).with(malformed_sha).returns(mock_manifest)
        mock_asset_manifest = mock
        Lotus::AssetManifest.expects(:new).with(account, mock_manifest, develop: false, nocache: false).returns(mock_asset_manifest)
        assert_equal subject.find(malformed_sha), mock_asset_manifest
      end
    end
  end

  describe '#version_information' do
    describe 'when in development' do
      before do
        Rails.env.stubs(:development?).returns(true)
      end

      it 'returns the correct asset manifest version information' do
        mock_configuration_settings = mock
        mock_configuration_settings.expects(:response).with(:raise_error)
        mock_configuration_settings.expects(:adapter).with(Faraday.default_adapter)
        mock_response = Faraday::Response.new(body: mock_manifest.to_json)
        mock_connection.expects(:get).with('/agent/tessaManifest.json').returns(mock_response)
        Faraday.expects(:new).with('http://minimum.zendesk-test.com/').yields(mock_configuration_settings).returns(mock_connection)
        mock_asset_manifest = mock
        mock_version_information = mock
        mock_asset_manifest.expects(:version_information).with(sha).returns(mock_version_information)
        version_information = subject.version_information(mock_asset_manifest)
        assert_equal mock_version_information, version_information
      end
    end

    describe 'when in production' do
      before do
        Rails.env.stubs(:production?).returns(true)
      end

      it 'returns the correct asset manifest version information' do
        mock_client = mock
        mock_client.expects(:fetch_current_manifest).returns(mock_manifest)
        TessaClient::V1.expects(:new).with(:lotus).returns(mock_client)
        mock_asset_manifest = mock
        mock_version_information = mock
        mock_asset_manifest.expects(:version_information).with(sha).returns(mock_version_information)
        version_information = subject.version_information(mock_asset_manifest)
        assert_equal mock_version_information, version_information
      end
    end
  end

  describe 'versions' do
    it 'returns the TessaClient\'s versions lookup response' do
      mock_client = mock
      mock_manifests = [{}, {}]
      mock_client.expects(:fetch_latest_deployed_manifests).returns(mock_manifests)
      TessaClient::V1.expects(:new).with(:lotus).returns(mock_client)
      assert_equal mock_manifests, subject.versions
    end
  end
end
