require_relative '../../support/test_helper'
require 'ffaker'

SingleCov.covered!

describe 'Lotus::AssetManifest' do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:data) do
    {
      'createdAt' => '2018-10-30T21:31:29Z',
      'css' => [{ 'path' => '/app.css' }],
      'js' => [{ 'path' => '/app.js' }, { 'path' => '/agent/assets/lr/main.54ba3d44.js' }],
      'sha' => '24878ba895f48a11a777cb3b6ff4640dd0f446f1',
      'version' => 2497
    }
  end
  # We will follow a naming convention where app.{fingerprint}.js will be the primary entry point.
  # The react javascripts should be sorted in such a manner that the runtime manifest is evaluated first.
  # The runtime will following the naming convention of runtime.{fingerprint}.js.
  # The primary entry point should be evaluated last.
  let(:multi_entry_data) do
    {
      'createdAt' => '2018-10-30T21:31:29Z',
      'css' => [
        { 'path' => '/app.css' },
        { 'path' => '/agent/assets/lr/vendor.o0ofu098.css' },
        { 'path' => '/agent/assets/lr/app.o0ofu098.css' }
      ],
      'js' => [
        { 'path' => '/app.js' },
        { 'path' => '/agent/assets/lr/app.54ba3d44.js' },
        { 'path' => '/agent/assets/lr/vendor.k3ofu098.js' },
        { 'path' => '/agent/assets/lr/runtime.dd09t8wk.js' }
      ],
      'sha' => '24878ba895f48a11a777cb3b6ff4640dd0f446f1',
      'version' => 2497
    }
  end

  describe '#intialize' do
    it 'accepts an account' do
      asset_manifest = Lotus::AssetManifest.new(account, data)
      assert_equal account, asset_manifest.account
    end

    it 'accepts json content' do
      asset_manifest = Lotus::AssetManifest.new(account, data)
      assert_equal data, asset_manifest.content
    end

    it 'knows the manifest sha' do
      asset_manifest = Lotus::AssetManifest.new(account, data)
      assert_equal data['sha'], asset_manifest.sha
    end

    it 'knows the manifest sha' do
      asset_manifest = Lotus::AssetManifest.new(account, data)
      assert_equal data['version'], asset_manifest.version
    end

    it 'accepts the develop option' do
      assert Lotus::AssetManifest.new(account, data, develop: true).develop
    end
  end

  describe '#manifest' do
    it 'returns dev assets' do
      asset_manifest = Lotus::AssetManifest.new(account, data, develop: true)
      assert asset_manifest.manifest['js'].all? { |script| script['path'].start_with?('/') }
      assert asset_manifest.manifest['css'].all? { |script| script['path'].start_with?('/') }
    end

    it 'does not alter React lazily loaded chunked JS assets when sha=develop' do
      chunked_data = {
        'js' => [
          { 'path' => '/agent/assets/lr/0.55e8cfab.chunk.js', 'lazy' => true }
        ]
      }
      asset_manifest = Lotus::AssetManifest.new(account, chunked_data, develop: true)
      expected = {
        'js' => [
          { 'path' => '/agent/assets/lr/0.55e8cfab.chunk.js', 'lazy' => true }
        ]
      }
      expected_javascripts = expected['js'].map { |asset| asset['path'] }.sort!
      subject_javascripts = asset_manifest.manifest['js'].map { |asset| asset['path'] }.sort!
      assert_equal expected_javascripts, subject_javascripts
    end

    it 'removes the fingerprint from lotus react assets in development' do
      js = "/app-#{FFaker.hexify('#' * 32).downcase}.js"
      css = "/app-#{FFaker.hexify('#' * 32).downcase}.css"
      data = {
        'css' => [{ 'path' => css }],
        'js' => [{ 'path' => js }],
      }
      runtime = "/agent/assets/lr/runtime.#{FFaker.hexify('#' * 16)}.js"
      vendor = "/agent/assets/lr/vendor.#{FFaker.hexify('#' * 16)}.js"
      app = "/agent/assets/lr/app.#{FFaker.hexify('#' * 16)}.js"
      data['js'].concat(
        [
          { 'path' => runtime },
          { 'path' => vendor },
          { 'path' => app }
        ]
      )
      asset_manifest = Lotus::AssetManifest.new(account, data, develop: true)
      expected = {
        'css' => [{ 'path' => '/app.css' }],
        'js' => [
          { 'path' => '/app.js' },
          { 'path' => '/agent/assets/react/js/runtime.js' },
          { 'path' => '/agent/assets/react/js/vendor.js' },
          { 'path' => '/agent/assets/react/js/app.js' }
        ]
      }
      assert_equal expected, asset_manifest.manifest
    end
  end

  describe '#initial_js_assets' do
    it 'returns the javascripts key from manifest' do
      asset_manifest = Lotus::AssetManifest.new(account, data)
      assert_equal [
        { 'path' => '/app.js' },
        { 'path' => '/agent/assets/lr/main.54ba3d44.js' }
      ], asset_manifest.initial_js_assets
    end

    it 'does not include lazy js assets' do
      data = multi_entry_data
      data['js'].concat [
        { 'path' => '/agent/assets/lr/app.b54a3224.chunk.js', 'lazy' => true },
        { 'path' => '/agent/assets/lr/app.83j954ba.chunk.js', 'lazy' => true },
      ]
      subject = Lotus::AssetManifest.new(account, data)
      javascripts = subject.initial_js_assets.map { |asset| asset['path'] }
      assert_equal [
        '/app.js',
        '/agent/assets/lr/runtime.dd09t8wk.js',
        '/agent/assets/lr/vendor.k3ofu098.js',
        '/agent/assets/lr/app.54ba3d44.js'
      ], javascripts
    end

    it 'does not alter React lazily loaded chunked javascripts when sha=develop' do
      chunked_data = {
        'js' => [
          { 'path' => '/agent/assets/lr/0.55e8cfab.chunk.js', 'lazy' => true }
        ],
        'createdAt' => '2018-10-30T21:31:29Z',
        'sha' => '24878ba895f48a11a777cb3b6ff4640dd0f446f1',
        'version' => 2497
      }
      subject = Lotus::AssetManifest.new(account, chunked_data, develop: true)
      expected = {
        'js' => [
          { 'path' => '/agent/assets/lr/0.55e8cfab.chunk.js', 'lazy' => true }
        ],
        'createdAt' => '2018-10-30T21:31:29Z',
        'sha' => '24878ba895f48a11a777cb3b6ff4640dd0f446f1',
        'version' => 2497
      }

      expected_javascripts = expected['js'].map { |asset| asset['path'] }.sort
      subject_javascripts = subject.manifest['js'].map { |asset| asset['path'] }.sort
      assert_equal expected_javascripts, subject_javascripts
    end

    # Lotus React will begin serving multiple entry points.
    describe 'when lotus react provides multiple entry points' do
      describe 'in development' do
        let(:expected_filenames) do
          [
            '/app.js',
            '/agent/assets/react/js/runtime.js',
            '/agent/assets/react/js/vendor.js',
            '/agent/assets/react/js/app.js'
          ]
        end

        it 'strips React asset fingerprints' do
          asset_manifest = Lotus::AssetManifest.new(account, multi_entry_data, develop: true)
          filenames = asset_manifest.initial_js_assets.map { |asset| asset['path'] }
          # Sort both since ordering is tested below.
          assert_equal expected_filenames.sort, filenames.sort
        end

        it 'returns the js key from manifest in the correct order with the correct prefix' do
          asset_manifest = Lotus::AssetManifest.new(account, multi_entry_data, develop: true)
          filenames = asset_manifest.initial_js_assets.map do |asset|
            asset['path'].sub(%r{\.w+\.js}, '.js')
          end
          assert_equal expected_filenames, filenames
        end
      end

      describe 'not in development' do
        it 'returns the javascripts key from manifest in the correct order' do
          asset_manifest = Lotus::AssetManifest.new(account, multi_entry_data)
          assert_equal [
            { 'path' => '/app.js' },
            { 'path' => '/agent/assets/lr/runtime.dd09t8wk.js' },
            { 'path' => '/agent/assets/lr/vendor.k3ofu098.js' },
            { 'path' => '/agent/assets/lr/app.54ba3d44.js' }
          ], asset_manifest.initial_js_assets
        end
      end
    end
  end

  describe '#initial_css_assets' do
    it 'returns the stylesheets key from manifest' do
      asset_manifest = Lotus::AssetManifest.new(account, data)
      assert_equal ['path' => '/app.css'], asset_manifest.initial_css_assets
    end

    it 'removes asynchronous stylesheet chunks' do
      data = multi_entry_data
      data['css'].concat [
        { 'path' => '/agent/assets/lr/app.b54a3224.chunk.css' },
        { 'path' => '/agent/assets/lr/app.83j954ba.chunk.css' },
      ]
      subject = Lotus::AssetManifest.new(account, data)
      stylesheets = subject.initial_css_assets.map { |asset| asset['path'] }
      assert_equal [
        '/app.css',
        '/agent/assets/lr/vendor.o0ofu098.css',
        '/agent/assets/lr/app.o0ofu098.css'
      ], stylesheets
    end

    describe 'when in development' do
      # The Webpack build does not produce CSS files in development.
      # This is for purposes of hot reloading.
      it 'does not return the React css assets' do
        asset_manifest = Lotus::AssetManifest.new(account, multi_entry_data, develop: true)
        stylesheets = asset_manifest.initial_css_assets.map { |asset| asset['path'] }
        assert_equal ['/app.css'], stylesheets
      end
    end

    describe 'when not in development' do
      it 'returns the React stylesheets' do
        asset_manifest = Lotus::AssetManifest.new(account, multi_entry_data)
        stylesheets = asset_manifest.initial_css_assets.map { |asset| asset['path'] }
        assert_equal [
          '/app.css',
          '/agent/assets/lr/vendor.o0ofu098.css',
          '/agent/assets/lr/app.o0ofu098.css'
        ], stylesheets
      end
    end
  end

  describe '#version_information' do
    let(:created_at) { '2018-10-30T21:31:29Z' }
    let(:sha) { '24878ba895f48a11a777cb3b6ff4640dd0f446f1' }
    let(:version) { 3000 }
    let(:mock_complete_manifest) do
      {
        'createdAt' => created_at,
        'css' => [{ 'path' => '/app.css' }],
        'js' => [{ 'path' => '/app.js' }, { 'path' => '/agent/assets/lr/main.54ba3d44.js' }],
        'sha' => sha,
        'version' => version
      }
    end

    it 'returns the correct object when all attributes are present and requesting current' do
      asset_manifest = Lotus::AssetManifest.new(account, mock_complete_manifest)
      expected = {
        'createdAt' => created_at,
        'sha' => sha,
        'specified' => false,
        'tag' => version
      }
      assert_equal expected, asset_manifest.version_information(sha)
    end

    it 'returns the correct object when createdAt is not present and requesting current' do
      manifest_without_created_at = mock_complete_manifest.merge('createdAt' => nil)
      asset_manifest = Lotus::AssetManifest.new(account, manifest_without_created_at)
      expected = {
        'createdAt' => '',
        'sha' => sha,
        'specified' => false,
        'tag' => version
      }
      assert_equal expected, asset_manifest.version_information(sha)
    end

    it 'returns the correct object when sha is not present and requesting current' do
      manifest_without_sha = mock_complete_manifest.merge('sha' => nil)
      asset_manifest = Lotus::AssetManifest.new(account, manifest_without_sha)
      expected = {
        'createdAt' => created_at,
        'sha' => '',
        'specified' => true,
        'tag' => version
      }
      assert_equal expected, asset_manifest.version_information(sha)
    end

    it 'returns the correct object when version is not present and requesting current' do
      manifest_without_version = mock_complete_manifest.merge('version' => nil)
      asset_manifest = Lotus::AssetManifest.new(account, manifest_without_version)
      expected = {
        'createdAt' => created_at,
        'sha' => sha,
        'specified' => false,
        'tag' => ''
      }
      assert_equal expected, asset_manifest.version_information(sha)
    end

    it 'returns the correct object when requesting anything other than current' do
      asset_manifest = Lotus::AssetManifest.new(account, mock_complete_manifest)
      expected = {
        'createdAt' => created_at,
        'sha' => sha,
        'specified' => true,
        'tag' => version
      }
      assert_equal expected, asset_manifest.version_information('develop')
    end
  end

  describe '#empty?' do
    it 'returns true when fed an empty manifest' do
      asset_manifest = Lotus::AssetManifest.new(account, {})
      assert_empty(asset_manifest)
    end

    it 'returns false when fed a non-empty manifest' do
      asset_manifest = Lotus::AssetManifest.new(account, data)
      assert asset_manifest.empty? == false
    end
  end

  describe '#rollbar_identifier' do
    it 'returns the version when available' do
      asset_manifest = Lotus::AssetManifest.new(account, data)
      assert_equal data['version'], asset_manifest.rollbar_identifier
    end

    it 'returns the git sha when the version is not available' do
      data_without_version = {
        'createdAt' => '2018-10-30T21:31:29Z',
        'css' => [{ 'path' => '/app.css' }],
        'js' => [{ 'path' => '/app.js' }, { 'path' => '/agent/assets/lr/main.54ba3d44.js' }],
        'sha' => '24878ba895f48a11a777cb3b6ff4640dd0f446f1',
      }
      asset_manifest = Lotus::AssetManifest.new(account, data_without_version)
      assert_equal data_without_version['sha'], asset_manifest.rollbar_identifier
    end

    it 'returns the git sha when the version is not available' do
      asset_manifest = Lotus::AssetManifest.new(account, {})
      assert_equal 'unknown', asset_manifest.rollbar_identifier
    end
  end
end
