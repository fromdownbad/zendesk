require_relative "../support/test_helper"

SingleCov.covered!

describe InstanceValue do
  fixtures :instance_values, :users

  let(:agent) { users(:minimum_agent) }

  describe 'persistence' do
    let(:instance_value) { instance_values(:agent_1_skill_1) }

    it 'an existing object is readonly' do
      instance_value.account_id = 123
      assert_raise ActiveRecord::ReadOnlyRecord do
        instance_value.save!
      end
    end
  end

  describe '#not_deleted' do
    it 'includes expected' do
      assert_includes InstanceValue.not_deleted, instance_values(:agent_1_skill_1)
      refute_includes InstanceValue.not_deleted, instance_values(:agent_deleted)
    end
  end

  describe '#ticket_skills' do
    it 'includes expected' do
      assert_includes InstanceValue.ticket_skills, instance_values(:ticket_1)
      refute_includes InstanceValue.ticket_skills, instance_values(:ticket_deleted)
      refute_includes InstanceValue.ticket_skills, instance_values(:agent_1_skill_1)
    end
  end

  describe '#agent_skills' do
    it 'includes expected' do
      assert_includes InstanceValue.agent_skills, instance_values(:agent_1_skill_1)
      refute_includes InstanceValue.agent_skills, instance_values(:agent_deleted)
      refute_includes InstanceValue.agent_skills, instance_values(:ticket_1)
    end
  end

  describe '#skills_for' do
    describe 'for agent with valid id and account_id' do
      before do
        agent.stubs(:id).returns(2)
        agent.stubs(:account_id).returns(1)
      end

      it 'includes expected' do
        assert_includes InstanceValue.skills_for(agent), instance_values(:agent_1_skill_1)
        assert_includes InstanceValue.skills_for(agent), instance_values(:agent_1_skill_2)
        refute_includes InstanceValue.skills_for(agent), instance_values(:agent_deleted)
        refute_includes InstanceValue.skills_for(agent), instance_values(:agent_2)
        refute_includes InstanceValue.skills_for(agent), instance_values(:agent_other_account)
      end

      it 'returns an empty array when user has no skills' do
        assert_equal [], InstanceValue.skills_for(users(:minimum_admin))
      end
    end

    describe 'when agent is nil' do
      it 'returns an empty array' do
        assert_equal [], InstanceValue.skills_for(nil)
      end
    end
  end

  describe '#skill_ids_for_agent' do
    before do
      agent.stubs(:id).returns(2)
      agent.stubs(:account_id).returns(1)
    end

    it 'includes expected' do
      assert_includes InstanceValue.skill_ids_for_agent(agent), instance_values(:agent_1_skill_1).attribute_value_id.unpack1('H*')
      assert_includes InstanceValue.skill_ids_for_agent(agent), instance_values(:agent_1_skill_2).attribute_value_id.unpack1('H*')
      refute_includes InstanceValue.skill_ids_for_agent(agent), instance_values(:agent_deleted).attribute_value_id.unpack1('H*')
      refute_includes InstanceValue.skill_ids_for_agent(agent), instance_values(:agent_2).attribute_value_id.unpack1('H*')
      refute_includes InstanceValue.skill_ids_for_agent(agent), instance_values(:agent_other_account).attribute_value_id.unpack1('H*')
    end

    it 'returns an empty array when user has no skills' do
      assert_equal [], InstanceValue.skill_ids_for_agent(users(:minimum_admin))
    end
  end
end
