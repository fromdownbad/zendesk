require_relative "../support/test_helper"

SingleCov.covered!

describe 'PasswordChange' do
  fixtures :accounts, :account_property_sets, :subscriptions, :global_oauth_clients

  describe "An auditable user" do
    before do
      @user = accounts(:minimum).users.new(name: 'Men Tor', email: 'mentor@example.com')

      # The user is changing information as themself
      @actor = @user

      # Simulate request's information injection operated by ApplicationController
      @user.ip_address   = '127.0.0.1'

      # Simulate request's information injection operated by Zendesk::Users::ControllerSupport
      # and in People::PasswordController#update
      @user.current_user = @actor
    end

    it "keeps a record of successful changes to password on new records" do
      assert_difference 'PasswordChange.count(:all)', 1 do
        @user.password = '12345'
        @user.save!
      end

      audit = @user.password_changes.last
      assert_equal @user.account, audit.account
      assert_nil audit.previous_value
      assert_equal @user.crypted_password.to_s, audit.new_value

      assert_equal @actor,                 audit.actor
      assert_equal '127.0.0.1',            audit.ip_address
    end

    it "keeps a record of successful changes to password on existing records" do
      @user.password = '12345'
      @user.save!
      previous_crypted_password = @user.crypted_password.checksum

      assert_difference 'PasswordChange.count(:all)', 1 do
        @user.password = 'abcde'
        @user.save!
      end

      audit = @user.password_changes.last
      assert_equal @user.account,               audit.account
      assert_equal previous_crypted_password,   audit.previous_value
      assert_equal @user.crypted_password.to_s, audit.new_value

      assert_equal @actor,                 audit.actor
      assert_equal '127.0.0.1',            audit.ip_address
    end

    it "does not keep a record of no changes" do
      @user.save!

      refute_difference 'PasswordChange.count(:all)' do
        @user.save!
      end
    end

    it "does not keep a record of unsuccessful changes to password" do
      refute_difference 'User.count(:all)', 'PasswordChange.count(:all)' do
        @user.name     = nil
        @user.password = '12345'
        @user.save
      end

      # Ensure successful retries don't cause previous failed changed to also save
      assert_difference 'PasswordChange.count(:all)', 1 do
        @user.name     = 'bud dha'
        @user.password = '12345'
        @user.save!
      end
    end

    it "does not blow up when recording fails" do
      mock = ""
      mock.expects(:create!).raises("XXX")
      @user.expects(password_changes: mock)
      ZendeskExceptions::Logger.expects(:record)

      @user.password = '12345'
      @user.save!
    end

    it 'deletes verification tokens after a reset' do
      VerificationToken.create!(account: @user.account, source: @user)
      @user.reload.password = '12345'
      @user.save!

      assert_equal [], @user.verification_tokens
    end

    it 'deletes mobile OAuth tokens and device identifiers after a reset' do
      @user = users(:minimum_end_user)
      client = global_oauth_clients(:system_users)
      client.update_column(:identifier, "zendesk_mobile_ios")
      token = Zendesk::OAuth::Token.create!(user_id: @user.id, account_id: @user.account.id, global_client_id: client.id)
      identifiers = stub("device_identifiers")
      @user.stubs(:device_identifiers).returns(identifiers)

      MobileDevice.create!(token: token.id) do |device|
        device.user = @user
        device.account = @user.account
      end

      identifiers.expects(:destroy_all)
      @user.reload.password = '12345'
      @user.save!

      assert_equal [], @user.reload.tokens
      assert_equal [], @user.devices
    end
  end
end
