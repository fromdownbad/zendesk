require_relative '../support/test_helper'

SingleCov.covered!

describe 'ComplianceDeletionFeedback' do
  fixtures :compliance_deletion_statuses

  let(:status)  { compliance_deletion_statuses(:hard_deleted) }
  let(:account) { status.account }

  let(:feedback) do
    FactoryBot.create(
      :compliance_deletion_feedback,
      compliance_deletion_status: status
    )
  end

  describe 'validations' do
    before { feedback }

    it 'cannot create duplicate feedback' do
      dup_feedback = feedback.dup
      assert_raises (ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation) do
        dup_feedback.save!
      end
    end

    it 'requires account_id to be set' do
      feedback.account_id = nil
      refute feedback.valid?
    end

    it 'requires user_id to be set' do
      feedback.user_id = nil
      refute feedback.valid?
    end

    it 'requires compliance_deletion_status_id to be set' do
      feedback.compliance_deletion_status_id = nil
      refute feedback.valid?
    end

    it 'requires pod_id to be set' do
      feedback.pod_id = nil
      refute feedback.valid?
    end

    it 'requires state to be set' do
      feedback.state = nil
      refute feedback.valid?
    end

    it 'requires application to be set' do
      feedback.application = nil
      refute feedback.valid?
    end

    it 'requires required to be set' do
      feedback.required = nil
      refute feedback.valid?
    end

    it 'fails if the state is invalid' do
      feedback.state = 'invalid'
      refute feedback.valid?
    end

    it 'fails if the application is invalid' do
      feedback.application = 'all'
      refute feedback.valid?
    end

    it 'works with valid states' do
      ComplianceDeletionFeedback::STATES.each do |state|
        feedback.state = state
        assert feedback.valid?
      end
    end

    it 'works with valid applications' do
      ComplianceDeletionFeedback::VALID_APPLICATIONS.each do |application|
        feedback.application = application
        assert feedback.valid?
      end
    end
  end

  describe 'scopes' do
    describe 'complete' do
      let!(:complete_feedback) do
        feedback = FactoryBot.create(
          :compliance_deletion_feedback,
          compliance_deletion_status: status,
          application: ComplianceDeletionStatus::EXPLORE
        )
        feedback.complete!
        feedback
      end

      let!(:incomplete_feedback) { feedback }

      it 'only selects complete feedback' do
        complete_list = ComplianceDeletionFeedback.complete
        assert_includes complete_list, complete_feedback
        refute_includes complete_list, incomplete_feedback
      end
    end

    describe 'incomplete' do
      let!(:complete_feedback) do
        feedback = FactoryBot.create(
          :compliance_deletion_feedback,
          compliance_deletion_status: status,
          application: ComplianceDeletionStatus::EXPLORE
        )
        feedback.complete!
        feedback
      end

      let!(:incomplete_feedback) { feedback }

      it 'only selects incomplete feedback' do
        incomplete_list = ComplianceDeletionFeedback.incomplete
        assert_includes incomplete_list, incomplete_feedback
        refute_includes incomplete_list, complete_feedback
      end
    end

    describe 'required' do
      let!(:required_feedback) { feedback }

      let!(:non_required_feedback) do
        FactoryBot.create(
          :compliance_deletion_feedback,
          compliance_deletion_status: status,
          application: ComplianceDeletionStatus::EXPLORE,
          required: false
        )
      end

      it 'only selects non-required feedback' do
        required_list = ComplianceDeletionFeedback.required
        assert_includes required_list, required_feedback
        refute_includes required_list, non_required_feedback
      end
    end
  end

  describe '#complete?' do
    it 'returns true when state is complete' do
      feedback.complete!
      assert feedback.complete?
    end

    it 'returns false when state is not complete' do
      feedback.redo!
      refute feedback.complete?
    end
  end

  describe '#complete!' do
    it 'updates state to complete' do
      refute feedback.complete?
      feedback.complete!
      assert feedback.complete?
    end
  end

  describe '#redo?' do
    it 'returns true when state is redo' do
      feedback.redo!
      assert feedback.redo?
    end

    it 'returns false when state is not redo' do
      feedback.complete!
      refute feedback.redo?
    end
  end

  describe '#redo!' do
    it 'updates state to redo and sets current pod_id' do
      feedback.update_column(:pod_id, account.pod_id + 1)
      refute feedback.redo?
      feedback.redo!
      assert feedback.redo?
      assert_equal feedback.pod_id, account.pod_id
    end
  end

  describe '.create_feedback!' do
    let(:required_app) { ComplianceDeletionStatus::PIGEON }
    let(:non_required_app) { ComplianceDeletionStatus::EXPLORE }
    let(:applications) do
      {
        required_app => true,
        non_required_app => false
      }
    end

    before do
      status.compliance_deletion_feedback.destroy_all
      ComplianceDeletionFeedback.create_feedback!(status: status, applications: applications)
    end

    it 'creates feedback records' do
      assert_equal applications.size, ComplianceDeletionFeedback.count
      assert_equal applications.keys.sort, ComplianceDeletionFeedback.all.pluck(:application).sort
    end

    it 'sets required field to true for required applications' do
      feedback = ComplianceDeletionFeedback.where(application: required_app).first
      assert feedback.required?
    end

    it 'sets required field to false for non-required applications' do
      feedback = ComplianceDeletionFeedback.where(application: non_required_app).first
      refute feedback.required?
    end

    it 'creates records with pending state' do
      assert ComplianceDeletionFeedback.pluck(:state).all? { |state| state == ComplianceDeletionFeedback::PENDING }
    end

    it 'creates records with correct user_id' do
      assert ComplianceDeletionFeedback.pluck(:user_id).all? { |id| id == status.user_id }
    end

    it 'creates records with correct account_id' do
      assert ComplianceDeletionFeedback.pluck(:account_id).all? { |id| id == status.account_id }
    end

    it 'creates records with correct compliance_deletion_status_id' do
      assert ComplianceDeletionFeedback.pluck(:compliance_deletion_status_id).all? { |id| id == status.id }
    end

    it 'creates records with correct pod_id' do
      assert ComplianceDeletionFeedback.pluck(:pod_id).all? { |id| id == status.account.pod_id }
    end
  end
end
