require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe CsvExportRecord do
  fixtures :accounts, :csv_export_records, :users

  let(:record) { csv_export_records(:minimum_1) }

  describe "latest" do
    let(:account) { accounts(:minimum) }
    let(:latest) { account.csv_export_records.latest }

    before do
      Timecop.travel(1.week.ago) do
        account.csv_export_records.create!(url: "test.url", user_id: users(:minimum_agent).id)
      end
    end

    it "is the most recent export for that account" do
      assert_equal [record], latest
    end
  end

  describe "#filename" do
    it "is just the filename" do
      assert_equal "hello.zip", record.filename
    end

    describe "with query parameters" do
      before { record.stubs(url: "https://www.example.com/api/v2/hello.zip?ACCESS_KEY=123&BLAH=123&HELLO&SIG=hello/goodbye") }

      it "is just the filename" do
        assert_equal "hello.zip", record.filename
      end
    end
  end

  describe "#content_type" do
    it "is Mime[:zip]" do
      assert_equal Mime[:zip], record.content_type
    end
  end
end
