require_relative "../support/test_helper"

SingleCov.covered! uncovered: 8
SingleCov.covered! file: "app/models/recipient_addresses/cname_status.rb"
SingleCov.covered! file: "app/models/recipient_addresses/dns_status.rb"
SingleCov.covered! file: "app/models/recipient_addresses/spf_status.rb"
SingleCov.covered! file: "app/models/recipient_addresses/forwarding_status.rb"

describe RecipientAddress do
  fixtures :accounts, :account_settings, :users, :recipient_addresses, :routes, :brands

  let(:recipient_address) { RecipientAddress.new { |r| r.account = account; r.email = "foo@z3nmail.com" } }
  let(:account)           { accounts(:minimum) }
  let(:default_recipient) { recipient_addresses(:default) }
  let(:regular_recipient) { recipient_addresses(:not_default) }
  let(:backup_recipient) { recipient_addresses(:backup) }
  let(:valid_attributes) { Hash[:email, " HELLO@example.com "] }
  let(:unverified_recipient) { account.recipient_addresses.create!(email: "foo@z3nmail.com") }
  let(:external_email_credential) do
    ExternalEmailCredential.new(
      account: account,
      username: "USERNAME@z3nmail.com",
      current_user: users(:minimum_agent)
    ).tap do |external_email_credential|
      external_email_credential.encrypted_value = "unencrypted_token"
      external_email_credential.save!
    end
  end
  let(:account_route) { account.routes.create!(subdomain: "foobrand") }
  let(:brand) do
    brand = account.brands.create! { |b| b.name = "Brando"; b.route = account_route }
    account_route.reload # make route use the has_one
    account.instance_variable_set(:@default_hosts, nil)
    brand
  end
  let(:brand2) do
    brand = account.brands.create! { |b| b.name = "Brando2"; b.route = account.routes.create!(subdomain: "somebrand") }
    account_route.reload # make route use the has_one
    account.instance_variable_set(:@default_hosts, nil)
    brand
  end

  before do
    Account.any_instance.stubs(has_multibrand?: true)
  end

  should_validate_presence_of :email, :account

  it "is valid" do
    assert_valid recipient_address
  end

  it "validates uniqueness of email" do
    account.recipient_addresses.create!(email: "foo@z3nmail.com")
    accounts(:support).recipient_addresses.create!(email: "foo@z3nmail.com")
    refute_valid account.recipient_addresses.build(email: "foo@z3nmail.com")
  end

  describe "#validate_email_unchanged" do
    let(:recipient_address) { regular_recipient }

    it "is invalid if email changed" do
      recipient_address.email = "GOODBYE@example.com"
      refute_valid recipient_address
    end
  end

  describe "#validate_email_on_proper_zendesk_domain" do
    it "allows myaccount.zendesk.com addresses" do
      recipient_address.email = "foo@#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}"
      assert_valid recipient_address
    end

    it "allows zendesky addresses" do
      recipient_address.email = "foo@my#{Zendesk::Configuration.fetch(:host)}"
      assert_valid recipient_address
    end

    it "does not allow zendesk.com addresses" do
      recipient_address.email = "foo@#{Zendesk::Configuration.fetch(:host)}"
      refute_valid recipient_address
      assert_includes recipient_address.errors.full_messages.join(" "), "cannot be another Zendesk subdomain or zendesk.com"
    end

    it "does not allow otheraccount.zendesk.com addresses" do
      recipient_address.email = "foo@xxx.#{Zendesk::Configuration.fetch(:host)}"
      refute_valid recipient_address
    end

    it "allows zendesk.com addresses for owned_by_zendesk accounts" do
      recipient_address.email = "foo@#{Zendesk::Configuration.fetch(:host)}"
      recipient_address.account.settings.owned_by_zendesk = true
      assert_valid recipient_address
    end

    describe "with route address" do
      before do
        brand
        assert_equal brand, account_route.brand
        recipient_address.email = "foo@#{account_route.default_host}"
      end

      it "allows" do
        assert_valid recipient_address
      end

      it "does not allow for deleted brand" do
        brand.soft_delete!
        Rails.cache.clear
        account_route.reload
        recipient_address.account.reload
        refute_valid recipient_address
      end
    end
  end

  describe "#validate_do_not_remove_default" do
    before do
      regular_recipient.forwarding_verified_at = Time.now
    end

    it "allows to add default" do
      regular_recipient.update_attributes!(default: true)
    end

    it "does not allow to remove default to prevent missing default" do
      refute default_recipient.update_attributes(default: false)
    end

    it "does not allow default removal via brand change" do
      default_recipient.brand_id = brand2.id
      refute_valid default_recipient
      assert_raises(ActiveRecord::RecordInvalid,
        I18n.t("activerecord.errors.models.recipient_address.attributes.default_invalid", brand: brand2.name)) do
        default_recipient.save!
      end
    end
  end

  describe "#validate_no_default_on_unverified" do
    let(:recipient_address) { unverified_recipient }

    before { recipient_address.default = true }

    it "does not allow to add default to unverified" do
      refute_valid recipient_address
    end

    it "allows to add default for zendesk" do
      recipient_address = account.recipient_addresses.build(email: "asasdf@#{account.default_host}", default: true)
      assert_valid recipient_address
    end

    it "allows to add default for verified" do
      recipient_address.forwarding_verified_at = Time.now
      assert_valid recipient_address
    end
  end

  describe "#validate_correct_brand" do
    it "is valid with any brand and no route" do
      recipient_address.brand = brand
      recipient_address.email = "foo@z3nmail.com"
      assert_valid recipient_address
    end

    describe "with route" do
      before do
        recipient_address.email = "foo@#{account_route.default_host}"
        brand # trigger load
      end

      it "is valid with correct brand" do
        recipient_address.brand = brand
        assert_valid recipient_address
      end

      it "is invalid with wrong brand" do
        recipient_address.brand = brands(:minimum)
        refute_valid recipient_address
      end
    end
  end

  describe "#validate_email_properly_formatted" do
    it "is invalid if email is invalid" do
      recipient_address.email = "help!@example.com"
      refute_valid recipient_address
    end
  end

  it "validates the email address does not belong to a user" do
    recipient_address.account = account
    recipient_address.email   = users(:minimum_agent).email
    refute recipient_address.valid?
    assert_equal(
      ["is in use by a user. Remove it from the user before adding it here."],
      recipient_address.errors.all_on(:email)
    )
  end

  describe "#validate_safe_local_part" do
    it "validates the local part safety while on zendesk.com subdomains" do
      RecipientAddress::UNSAFE_LOCAL_PARTS.each do |local_part|
        recipient_address.email = "#{local_part}@#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}"
        refute_valid recipient_address
      end
    end

    it "validates the local part case insensitive" do
      recipient_address.email = "hOstmAsteR@#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}"
      refute_valid recipient_address
    end

    it "does not validate local part safety for customers own domain" do
      recipient_address.email = "hostmaster@example.com"
      assert_valid recipient_address
    end

    it "does not invalidate safe local parts" do
      recipient_address.email = "help@#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}"
      assert_valid recipient_address
    end
  end

  describe "scopes" do
    def assert_scope_conditions(scope, conditions_hash)
      assert_equal(
        conditions_hash,
        scope.where_values_hash.stringify_keys
      )
    end

    describe ".by_email" do
      it "returns RecipientAddress records with matching emails" do
        assert_scope_conditions(
          RecipientAddress.by_email(default_recipient.email), "email" => default_recipient.email
        )
      end
    end
  end

  describe "being created" do
    it "sets the default to NULL when the argument provided is false" do
      recipient_address = account.recipient_addresses.create!(valid_attributes.merge(default: false))
      assert_nil recipient_address.default
    end

    describe "mail forwarding check" do
      it "sends out " do
        AccountsMailer.expects(:deliver_forwarding_status_verification)
        accounts(:support).recipient_addresses.create!(valid_attributes)
      end

      it "does not re-verify when created verified (can go when reply_address= is gone)" do
        AccountsMailer.expects(:deliver_forwarding_status_verification).never
        recipient_address = account.recipient_addresses.create!(valid_attributes.merge(default: false)) { |ra| ra.forwarding_verified_at = Time.now }
        assert_equal :verified, recipient_address.forwarding_status
      end

      it "does not send multiple emails when saving multiple times" do
        AccountsMailer.expects(:deliver_forwarding_status_verification)
        recipient_address = accounts(:support).recipient_addresses.create!(valid_attributes)
        recipient_address.save!
      end
    end

    describe "when there is an existing default" do
      it "does not demote it" do
        assert default_recipient.default?
        recipient_address = account.recipient_addresses.create!(valid_attributes)
        refute recipient_address.default?
        assert default_recipient.reload.default?
      end

      it "demotes others when inserting a another default recipient address" do
        assert default_recipient.default?
        account.recipient_addresses.create!(valid_attributes.merge(default: true)) { |r| r.forwarding_verified_at = Time.now }
        refute default_recipient.reload.default?
      end
    end
  end

  describe "being updated" do
    before do
      assert default_recipient.default?
      refute regular_recipient.default?
    end

    it "sets the default to NULL when it is assigned false" do
      regular_recipient.update_attribute(:default, false)
      assert_nil regular_recipient.default
    end

    describe "and demoting the current default" do
      it "allows demotion of the default when it is the only recipient address" do
        RecipientAddress.delete_all(account_id: account.id, default: nil)
        default_recipient.update_attribute(:default, nil)
        refute default_recipient.reload.default?
      end

      it "does not set another recipient address to be the default" do
        default_recipient.update_attribute(:default, nil)
        refute regular_recipient.reload.default?
        refute default_recipient.reload.default?
      end
    end

    describe "and promoting a regular recipient address to be the default" do
      it "demotes the current default recipient address" do
        regular_recipient.update_attribute(:default, true)
        assert regular_recipient.reload.default?
        refute default_recipient.reload.default?
      end

      it "does not demote the default recipient_address for other accounts" do
        regular_recipient.update_attribute(:default, true)
        assert recipient_addresses(:support_backup).default?
      end
    end
  end

  describe "#name" do
    it "is optional" do
      address = account.recipient_addresses.build(valid_attributes)
      assert_nil address.name
      assert address.valid?
    end
  end

  describe "#default" do
    it "defaults to false" do
      refute recipient_address.default?
    end
  end

  describe "#position" do
    it "returns 0 for a backup" do
      assert_equal 0, backup_recipient.position.first
    end

    it "returns 1 for a default" do
      assert_equal 1, default_recipient.position.first
    end

    it "returns 2 for a non-backup, non-default" do
      assert_equal 2, recipient_address.position.first
    end
  end

  describe "#email" do
    let(:address) { account.recipient_addresses.build(valid_attributes) }

    it "is downcased" do
      assert_equal "hello@example.com", address.email
    end

    it "removes excess whitespace" do
      assert_equal "hello@example.com", address.email
    end

    it "validates that it looks like an email address" do
      address = account.recipient_addresses.build(email: 'nice2example.com')
      refute address.valid?
      assert_equal(["is not properly formatted"], address.errors[:email])
    end
  end

  describe "#destroy" do
    it "is able to destroy normal" do
      assert regular_recipient.destroy
      assert_raise(ActiveRecord::RecordNotFound) { regular_recipient.reload }
    end

    it "does not be able to destroy backup" do
      refute backup_recipient.destroy
      backup_recipient.reload
    end

    it "does not be able to destroy non-default-brand backup" do
      backup_recipient = brand.recipient_addresses.first
      backup_recipient.update_column(:default, false)
      refute backup_recipient.destroy
      backup_recipient.reload
    end

    it "does not be able to destroy default" do
      refute default_recipient.destroy
      default_recipient.reload
    end

    describe "and there is an associated external email credential" do
      let(:recipient_address) { external_email_credential.recipient_address }
      before { external_email_credential }

      it "aborts the destroy" do
        refute_difference "RecipientAddress.count(:all)" do
          refute recipient_address.destroy
        end
        assert_equal(
          ["The support address cannot be destroyed with an associated email credential. Destroy the email credential instead."],
          recipient_address.reload.errors.full_messages
        )
      end

      it "allows the destroy when the external_email_credential is being destroyed" do
        assert_difference "RecipientAddress.count(:all)", -1 do
          assert external_email_credential.destroy
        end
      end
    end

    it "prevents deleting the last collaboration address" do
      Account.any_instance.stubs(has_side_conversations_enabled?: true)
      ra1 = account.recipient_addresses.create!(email: 'col1@minimum.zendesk-test.com', product: RecipientAddress::COLLABORATION_PRODUCT)
      ra2 = account.recipient_addresses.create!(email: 'col2@minimum.zendesk-test.com', product: RecipientAddress::COLLABORATION_PRODUCT)
      assert ra1.destroy
      refute ra2.destroy
      assert_equal(
        [I18n.t('activerecord.errors.models.recipient_address.destroy_only_collaboration_address')],
        ra2.errors.full_messages
      )
    end
  end

  describe "#set_brand_id" do
    it "sets brand id" do
      recipient_address.save!
      assert_equal account.default_brand_id, recipient_address.reload.send(:brand_id)
    end

    it "sets brand id from route for addresses created via the api" do
      brand # trigger load
      recipient_address.email = "foo@#{account_route.default_host}"
      recipient_address.save!
      assert_equal brand.id, recipient_address.reload.send(:brand_id)
    end

    it "does not overwrite brand_id" do
      recipient_address.brand_id = brand.id
      recipient_address.save!
      assert_equal brand.id, recipient_address.reload.send(:brand_id)
    end
  end

  describe "#forwarding_status" do
    let(:recipient_address) { unverified_recipient }

    it "is unknown by default" do
      recipient_address.forwarding_sent_at = nil
      assert_equal :unknown, recipient_address.forwarding_status
    end

    it "is waiting when sent out" do
      recipient_address.forwarding_sent_at = Time.now
      assert_equal :waiting, recipient_address.forwarding_status
    end

    it "is waiting when when sent out recently and not yet received" do
      recipient_address.forwarding_sent_at = 3.minutes.ago
      recipient_address.forwarding_verified_at = 10.minutes.ago
      assert_equal :waiting, recipient_address.forwarding_status
    end

    it "is failed when when sent out long time ago and not yet received" do
      recipient_address.forwarding_sent_at = 9.minutes.ago
      recipient_address.forwarding_verified_at = 10.minutes.ago
      assert_equal :failed, recipient_address.forwarding_status
    end

    it "is failed when sent out too long ago" do
      recipient_address.forwarding_sent_at = 10.minutes.ago
      assert_equal :failed, recipient_address.forwarding_status
    end

    it "is verified when received" do
      recipient_address.forwarding_sent_at = 10.minutes.ago
      recipient_address.forwarding_verified_at = 10.minutes.ago
      assert_equal :verified, recipient_address.forwarding_status
    end

    it "is verified when received and not sent" do
      recipient_address.forwarding_sent_at = nil
      recipient_address.forwarding_verified_at = 10.minutes.ago
      assert_equal :verified, recipient_address.forwarding_status
    end

    it "always be :verified for default host" do
      recipient_address.email = "mooh@#{account.default_host}"
      assert_equal :verified, recipient_address.forwarding_status
    end

    it "always be :verified for route host" do
      brand
      recipient_address.email = "mooh@#{account_route.default_host}"
      assert_equal :verified, recipient_address.forwarding_status
    end
  end

  describe "#send_emails?" do
    before { recipient_address.forwarding_verified_at = 10.minutes.ago }

    it "sends emails when verified" do
      assert recipient_address.send_emails?
    end

    it "sends emails when recently re-verified" do
      recipient_address.forwarding_sent_at = 4.minutes.ago
      recipient_address.forwarding_verified_at = 10.minutes.ago
      assert recipient_address.send_emails?
    end

    it "does not send emails when re-verify failed" do
      recipient_address.forwarding_sent_at = 6.minutes.ago
      recipient_address.forwarding_verified_at = 10.minutes.ago
      refute recipient_address.send_emails?
    end

    it "does not send emails when not verified" do
      recipient_address.forwarding_verified_at = nil
      recipient_address.forwarding_sent_at = 16.minutes.ago
      refute recipient_address.send_emails?
    end

    it "does not send from addresses with inactive brand" do
      brand = FactoryBot.create(:brand, account_id: account.id, active: false)
      recipient_address.brand = brand
      refute recipient_address.send_emails?
    end
  end

  describe "#verify_forwarding_status!" do
    let(:recipient_address) { unverified_recipient }

    before do
      recipient_address
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries.clear
    end

    it "lies in waiting" do
      recipient_address.verify_forwarding_status!
      assert_equal :waiting, recipient_address.forwarding_status
    end

    it "lies in waiting for verified" do
      Timecop.travel(Time.now + 1.seconds)
      recipient_address.update_column(:forwarding_verified_at, Time.now)
      recipient_address.verify_forwarding_status!
      assert_equal :waiting, recipient_address.forwarding_status
    end

    it "does not send for default_host" do
      recipient_address.update_column(:email, "moo@#{account.default_host}")
      recipient_address.verify_forwarding_status!

      assert_equal :verified, recipient_address.forwarding_status
      assert_equal 0, ActionMailer::Base.deliveries.size
    end

    it "sends out a test mail that marks it as verified" do
      recipient_address.verify_forwarding_status!

      assert_equal :waiting, recipient_address.forwarding_status
      assert_equal 1, ActionMailer::Base.deliveries.size

      assert_includes ActionMailer::Base.deliveries.last.joined_bodies.to_s, RecipientAddresses::ForwardingStatus::CODE_START
    end

    it "does not send when creating a verified (for recipient_address=)" do
      recipient_address = account.recipient_addresses.create!(email: "recipient@z3nmail.com") do |ra|
        ra.forwarding_verified_at = Time.now
      end

      assert_equal 0, ActionMailer::Base.deliveries.size
      assert recipient_address.forwarding_verified_at
    end
  end

  describe "#forwarding_verified!" do
    let(:recipient_address) { unverified_recipient }

    it "gos from waiting to verified" do
      recipient_address.update_column(:forwarding_sent_at, 1.minute.ago)
      recipient_address.forwarding_verified!
      assert_equal :verified, recipient_address.forwarding_status
      assert_equal :verified, recipient_address.reload.forwarding_status
    end
  end

  describe "#forwarding_code" do
    let(:recipient_address) { default_recipient }

    it "is readable" do
      Timecop.freeze
      expected = {
        "recipient_addresses_id" => default_recipient.id,
        "sent_at" => Time.now.to_i
      }
      recipient_address.forwarding_sent_at = Time.now
      assert_equal expected, Zendesk::Verifier.verify(recipient_address.forwarding_code.split(":").last)
    end

    it "is constant" do
      Timecop.freeze "2014-01-01"
      recipient_address.id = 123
      assert_equal "ZFVTest:BAh7B0kiG3JlY2lwaWVudF9hZGRyZXNzZXNfaWQGOgZFVGkBe0kiDHNlbnRfYXQGOwBUaQA=--93988c322de83e2a894bf63315fed8e955199c58", recipient_address.forwarding_code
    end
  end

  describe ".verify_forwarding_code" do
    def verify(*args)
      RecipientAddresses::ForwardingStatus.verify_forwarding_code(*args)
    end

    let(:recipient_address) { unverified_recipient }

    it "signals failure on invalid" do
      refute verify(account, "XYZ--SDF")
    end

    it "signals failure on valid unfound" do
      recipient_address = recipient_addresses(:not_default)
      code = recipient_address.forwarding_code
      recipient_address.destroy
      refute verify(recipient_address.account, code)
    end

    it "signals success and change to verified on valid" do
      recipient_address.update_column(:forwarding_sent_at, 1.minute.ago)
      verify(account, recipient_address.forwarding_code).must_equal 'verified'
      assert_equal :verified, recipient_address.reload.forwarding_status
    end

    it "signals success and not change on valid delivery delivery for outdated mail" do
      code = recipient_address.forwarding_code
      recipient_address.update_column(:forwarding_sent_at, 10.minutes.ago)
      verify(account, code).must_include 'unverified'
      assert_equal :failed, recipient_address.reload.forwarding_status
    end

    it "signals success and change on valid late delivery" do
      recipient_address.update_column(:forwarding_sent_at, 10.minutes.ago)
      verify(account, recipient_address.forwarding_code).must_include 'verified'
      assert_equal :verified, recipient_address.reload.forwarding_status
    end
  end

  describe "#ticket_received!" do
    let(:recipient_address) { unverified_recipient }
    before { travel_to Time.now }
    after { travel_back }

    it "does not verifies unverified" do
      recipient_address.ticket_received!
      assert_nil recipient_address.forwarding_verified_at
      assert_equal :waiting, recipient_address.forwarding_status
    end

    it "does not verify retried" do
      old = 5.minute.ago
      recipient_address.forwarding_sent_at = 4.minute.ago
      recipient_address.forwarding_verified_at = old
      recipient_address.ticket_received!
      assert_equal old.to_i, recipient_address.forwarding_verified_at.to_i
      assert_equal :waiting, recipient_address.forwarding_status
    end

    it "extends verified" do
      recipient_address.forwarding_sent_at = 3.hours.ago
      recipient_address.forwarding_verified_at = 2.hours.ago
      recipient_address.ticket_received!
      assert_equal Time.now.to_i, recipient_address.forwarding_verified_at.to_i
      assert_equal Time.now.to_i, recipient_address.reload.forwarding_verified_at.to_i
      assert_equal :verified, recipient_address.forwarding_status
    end

    it "does not extend recently verified" do
      recipient_address.forwarding_sent_at = 2.minute.ago
      recipient_address.forwarding_verified_at = 1.minute.ago
      recipient_address.ticket_received!
      assert_equal 1.minute.ago, recipient_address.forwarding_verified_at
      assert_equal :verified, recipient_address.forwarding_status
    end

    it "does not extend always verified" do
      recipient_address.email = account.backup_email_address
      recipient_address.ticket_received!
      assert_nil recipient_address.forwarding_verified_at
      assert_equal :verified, recipient_address.forwarding_status
    end
  end

  describe "#verify_spf_status" do
    def stub_dns_txt(domain, value)
      RecipientAddress.any_instance.stubs(:stubbed_spf_status?).returns(false) # make it use real lookups
      Resolv::DNS.any_instance.expects(:getresources).with("#{domain}.", Resolv::DNS::Resource::IN::TXT).returns [stub(data: value)]
    end

    def stub_dns_txt_regions(domain, value)
      (1..4).each do |txt_record|
        stub_dns_txt("zendesk#{txt_record}.#{domain}", value)
      end
    end

    before { recipient_address.email = "something@z3nmail.com" }

    it "verifies when DNS TXT record includes Zendesk" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 include:support.z3nmail.com include:mail.zendesk-test.com ~all'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      recipient_address.save!
      assert_equal :verified, recipient_address.spf_status
    end

    it "verifies when DNS TXT record has mixed case" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 include:support.z3nmail.com Include:mail.zendesk-test.com ~ALL'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      recipient_address.save!
      assert_equal :verified, recipient_address.spf_status
    end

    it "verifies when DNS TXT record does not have an all mechanism" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 include:support.z3nmail.com include:mail.zendesk-test.com'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      recipient_address.save!
      assert_equal :verified, recipient_address.spf_status
    end

    it "verifies when DNS TXT record does not have an all mechanism and ends with mx" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 include:support.z3nmail.com include:mail.zendesk-test.com MX'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      recipient_address.save!
      assert_equal :verified, recipient_address.spf_status
    end

    it "verifies when using a Zendesk addresses, but does not lookup DNS" do
      Resolv::DNS.any_instance.expects(:getresources).never
      recipient_address.email = "xxx@#{account.default_host}"
      recipient_address.save!
      assert_equal :verified, recipient_address.spf_status
    end

    it "verifies even when + qualifier is on the include directives" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 +include:support.z3nmail.com'
      stub_dns_txt 'support.z3nmail.com', 'v=spf1 +include:mail.zendesk-test.com ~all'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      recipient_address.save!
      assert_equal :verified, recipient_address.spf_status
    end

    it "fails when DNS TXT record does not include Zendesk" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 -all'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      recipient_address.save!
      assert_equal :failed, recipient_address.spf_status
    end

    it "warns when deprecated DNS record is found" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 include:smtp.zendesk-test.com +all'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      recipient_address.save!
      assert_equal :deprecated, recipient_address.spf_status
    end

    it "warns when deprecated records are found" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 include:_spf.zdsys.com +all'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'

      recipient_address.save!
      assert_equal :deprecated, recipient_address.spf_status
    end

    it "warns on deprecated for included non-zendesk domains" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 include:support.example.com include:foo.example.com ~all'
      stub_dns_txt 'support.example.com', 'v=spf1 include:zap.example.com ~all'
      stub_dns_txt 'zap.example.com', 'v=spf1 ip:foobar ~all'
      stub_dns_txt 'foo.example.com', 'v=spf1 include:mail.zendesk-test.com ~all'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      recipient_address.save!
      assert_equal :verified, recipient_address.spf_status
    end

    it "traverses max 10 entries" do
      stub_dns_txt 'z3nmail.com', "v=spf1 #{Array.new(12) { |i| "include:#{i}.z3nmail.com" }.join(" ")} ~all"
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      10.times do |i|
        stub_dns_txt "#{i}.z3nmail.com", 'v=spf1 ip:foobar ~all'
      end
      recipient_address.save!
      assert_equal :failed, recipient_address.spf_status
    end

    it "stills find after traversing max entries" do
      stub_dns_txt 'z3nmail.com', "v=spf1 #{Array.new(12) { |i| "include:#{i}.z3nmail.com" }.join(" ")} ~all"
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      9.times do |i|
        stub_dns_txt "#{i}.z3nmail.com", 'v=spf1 ip:foobar ~all'
      end
      stub_dns_txt "9.z3nmail.com", 'v=spf1 include:mail.zendesk-test.com ~all'
      recipient_address.save!
      assert_equal :verified, recipient_address.spf_status
    end

    it "is failed when blowing up" do
      RecipientAddress.any_instance.stubs(:stubbed_spf_status?).returns(false) # make it use real lookups
      Resolv::DNS.any_instance.expects(:getresources).raises(Resolv::ResolvTimeout)
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'

      recipient_address.save!
      assert_equal :failed, recipient_address.spf_status
    end

    it "is failed when no valid dns records are found" do
      recipient_address.stubs(:dns_txt_records).returns []
      recipient_address.save!
      assert_equal :failed, recipient_address.spf_status
    end

    it "should only match empty or + qualifier on include directives" do
      stub_dns_txt 'z3nmail.com', 'v=spf1 -include:support.z3nmail.com'
      stub_dns_txt_regions 'z3nmail.com', 'something.amazonaws.com'
      recipient_address.save!
      assert_equal :failed, recipient_address.spf_status
    end
  end

  describe "#verify_cname_status!" do
    let(:recipient_address) { unverified_recipient }

    describe_with_arturo_enabled :email_deprecate_cname_checks do
      it "does not call verify_cname_status" do
        RecipientAddress.any_instance.expects(:verify_cname_status).never

        recipient_address.verify_cname_status!
      end
    end

    describe_with_arturo_disabled :email_deprecate_cname_checks do
      it "calls verify_cname_status" do
        RecipientAddress.any_instance.expects(:verify_cname_status).at_least_once

        recipient_address.verify_cname_status!
      end

      it "updates #cname_status" do
        recipient_address.update_column(:cname_status_id, RecipientAddress::CNAME_STATUS.fetch(:unknown))

        assert_equal :unknown, recipient_address.cname_status

        recipient_address.verify_cname_status!

        assert_equal :failed, recipient_address.reload.cname_status
      end

      it "updates metadata" do
        recipient_address.update_column(:metadata, nil)
        recipient_address.verify_cname_status!

        assert recipient_address.metadata.dig("cname")
      end
    end
  end

  describe "#verify_cname_status" do
    def stub_dns_cname(domain, value)
      RecipientAddress.any_instance.stubs(:stubbed_cname_status?).returns(false) # make it use real lookups
      Resolv::DNS.any_instance.expects(:getresources).with(domain, Resolv::DNS::Resource::IN::CNAME).returns [stub(name: stub(to_s: value))]
    end

    before { recipient_address.email = "something@z3nmail.com" }

    describe_with_arturo_disabled :email_deprecate_cname_checks do
      it "verifies when all 4 required CNAME records are set" do
        stub_dns_cname 'zendesk1.z3nmail.com', 'mail1.zendesk-test.com'
        stub_dns_cname 'zendesk2.z3nmail.com', 'mail2.zendesk-test.com'
        stub_dns_cname 'zendesk3.z3nmail.com', 'mail3.zendesk-test.com'
        stub_dns_cname 'zendesk4.z3nmail.com', 'mail4.zendesk-test.com'

        assert_nil recipient_address.metadata
        recipient_address.save!
        assert recipient_address.all_cnames_valid?
        assert_equal recipient_address.metadata['verification_method'], 'cname'
      end

      it "verifies when all 4 required CNAME records are set irrespective of case" do
        stub_dns_cname 'zendesk1.z3nmail.com', 'MAIL1.zendesk-test.com'
        stub_dns_cname 'zendesk2.z3nmail.com', 'mail2.ZENDESK-TEST.com'
        stub_dns_cname 'zendesk3.z3nmail.com', 'mail3.zendesk-test.COM'
        stub_dns_cname 'zendesk4.z3nmail.com', 'MaIl4.ZeNdEsK-tEsT.cOm'

        assert_nil recipient_address.metadata
        recipient_address.save!
        assert recipient_address.all_cnames_valid?
        assert_equal recipient_address.metadata['verification_method'], 'cname'
      end

      it "fails when an incorrect CNAME record is set" do
        stub_dns_cname 'zendesk1.z3nmail.com', 'mail1.zendesk-test.com'
        stub_dns_cname 'zendesk2.z3nmail.com', 'mail2.zendesk-test.com'
        stub_dns_cname 'zendesk3.z3nmail.com', 'mail3.zendesk-test.com'
        stub_dns_cname 'zendesk4.z3nmail.com', 'anotherdomain.zendesk-test.com'

        recipient_address.save!
        refute recipient_address.all_cnames_valid?
      end

      it "fails when a CNAME record is missing" do
        stub_dns_cname 'zendesk1.z3nmail.com', 'mail1.zendesk-test.com'
        stub_dns_cname 'zendesk2.z3nmail.com', 'mail2.zendesk-test.com'
        stub_dns_cname 'zendesk3.z3nmail.com', ''
        stub_dns_cname 'zendesk4.z3nmail.com', 'mail4.zendesk-test.com'

        recipient_address.save!
        refute recipient_address.all_cnames_valid?
      end

      it "always be :verified for default host" do
        recipient_address.email = "mooh@#{account.default_host}"
        assert_equal :verified, recipient_address.cname_status
      end

      describe "for Zendesk-owned accounts" do
        before { account.settings.owned_by_zendesk = true }

        it "always be :verified for Zendesk owned accounts using zendesk.com" do
          recipient_address.metadata = {}
          recipient_address.email = "support@zendesk-test.com"
          assert_equal :verified, recipient_address.cname_status
        end
      end
    end
  end

  describe "#all_cnames_valid?" do
    let(:subject) { recipient_address.all_cnames_valid? }

    describe "when using CNAME verification" do
      before { RecipientAddress.any_instance.stubs(:using_cname_verification?).returns(true) }

      describe "with no records present" do
        before { recipient_address.metadata = {} }

        it { refute subject }
      end

      describe "when there is an incorrect record" do
        before do
          recipient_address.metadata = valid_metadata(%w[cname])
          recipient_address.metadata = recipient_address.metadata.merge("cname" => {"zendesk2" => {"status" => "failed"}})
        end

        it { refute subject }
      end

      describe "with incomplete results set" do
        before { recipient_address.metadata = valid_metadata(%w[cname], (2..4)) }

        it { refute subject }
      end

      describe "with all required records present and valid" do
        before { recipient_address.metadata = valid_metadata(%w[cname]) }

        it { assert subject }
      end
    end

    describe "when not using CNAME verification" do
      before { RecipientAddress.any_instance.stubs(:using_cname_verification?).returns(false) }

      describe "with no records present" do
        before { recipient_address.metadata = {} }

        describe "for Zendesk-owned accounts with zendesk.com emails" do
          before do
            account.settings.owned_by_zendesk = true
            recipient_address.email = "something@zendesk-test.com"
          end

          it { assert subject }
        end

        describe "for non-Zendesk owned accounts" do
          it { refute subject }
        end
      end

      describe "with no MX records present" do
        before { recipient_address.metadata = valid_metadata(%w[txt]) }

        it { refute subject }
      end

      describe "with no TXT records present" do
        before { recipient_address.metadata = valid_metadata(%w[mx]) }

        it { refute subject }
      end

      describe "when there is an incorrect record" do
        before do
          recipient_address.metadata = valid_metadata(%w[mx])
          recipient_address.metadata = recipient_address.metadata.merge("mx" => {"zendesk2" => {"status" => "failed"}})
        end

        it { refute subject }
      end

      describe "with incomplete results set" do
        before { recipient_address.metadata = valid_metadata(%w[mx], (2..4)) }

        it { refute subject }
      end

      describe "with all required records present and valid" do
        before { recipient_address.metadata = valid_metadata(%w[mx txt]) }

        it { assert subject }
      end
    end
  end

  describe "#using_cname_verification?" do
    let(:subject) { recipient_address.using_cname_verification? }

    before do
      recipient_address.save!
      recipient_address.update_column(:metadata, valid_metadata(valid_records))
      recipient_address.update_verification_method
    end

    describe "with no metadata" do
      let(:valid_records) { [nil] }

      it { assert subject }
    end

    describe "when metadata has valid CNAME checks" do
      let(:valid_records) { %w[cname] }

      it { assert subject }
    end

    describe "when metadata only has valid MX checks" do
      let(:valid_records) { %w[mx] }

      it { assert subject }
    end

    describe "when metadata only has valid TXT checks" do
      let(:valid_records) { %w[txt] }

      it { assert subject }
    end

    describe "when metadata has valid MX and TXT checks" do
      let(:valid_records) { %w[mx txt] }

      it { refute subject }
    end
  end

  describe "#verify_dns_status!" do
    let(:recipient_address) { RecipientAddress.new { |r| r.account = account; r.email = "foo@z3nmail.com" }.tap(&:save!) }
    let(:subject) { recipient_address.verify_dns_status! }

    describe_with_arturo_disabled :email_deprecate_cname_checks do
      it "calls verify_cname_status" do
        recipient_address.expects(:verify_cname_status).at_least_once

        subject
      end
    end

    describe_with_arturo_enabled :email_deprecate_cname_checks do
      it "does not call verify_cname_status" do
        recipient_address.expects(:verify_cname_status).never

        subject
      end
    end

    it "calls verify_dns_status" do
      recipient_address.expects(:verify_dns_status).at_least_once

      subject
    end

    describe_with_arturo_disabled :email_deprecate_cname_checks do
      describe "when CNAME status is verified" do
        before do
          recipient_address.stubs(:all_records_verified?).returns(true)
        end

        it "does not attempt to verify SPF status" do
          recipient_address.expects(:verify_spf_records).with(any_parameters).never

          subject
        end

        it "does not attempt to verify MX status" do
          recipient_address.expects(:verify_mx_status).with(anything).never

          subject
        end
      end

      describe "when CNAME status is not verified" do
        before do
          recipient_address.stubs(:all_records_verified?).with(:cname).returns(false)
          recipient_address.stubs(:all_records_verified?).with(anything)
        end

        it "calls verify_spf_status" do
          recipient_address.expects(:verify_spf_records).at_least_once

          subject
        end

        it "calls verify_mx_status" do
          recipient_address.expects(:verify_mx_status).at_least_once

          subject
        end
      end
    end
  end

  describe "#verify_dns_status" do
    before { recipient_address.email = "something@z3nmail.com" }

    it "verifies the required TXT record is set" do
      stub_dns_txt 'zendesk_verification.z3nmail.com', '88c8f1c4dcd7070f'
      stub_dns_txt 'zendeskverification.z3nmail.com', ''

      recipient_address.save!
      assert_equal "verified", recipient_address.metadata["domain_verification"]["status"]
      assert_equal :verified, recipient_address.domain_verification_status
    end

    it "verifies the required TXT record without an underscore" do
      stub_dns_txt 'zendesk_verification.z3nmail.com', ''
      stub_dns_txt 'zendeskverification.z3nmail.com', '88c8f1c4dcd7070f'

      recipient_address.save!
      assert_equal "verified", recipient_address.metadata["domain_verification"]["status"]
    end

    it "fails when an incorrect CNAME record is set" do
      stub_dns_txt 'zendesk_verification.z3nmail.com', 'incorrect_value'
      stub_dns_txt 'zendeskverification.z3nmail.com', 'another_incorrect_value'

      recipient_address.save!
      assert_equal "failed", recipient_address.metadata["domain_verification"]["status"]
    end

    it "fails when the verification record is missing" do
      RecipientAddress.any_instance.stubs(:stubbed_txt_status?).returns(false) # make it use real lookups

      recipient_address.save!
      assert_equal "failed", recipient_address.metadata["domain_verification"]["status"]
    end

    it "fails when there is a timeout" do
      RecipientAddress.any_instance.stubs(:stubbed_txt_status?).returns(false) # make it use real lookups
      Resolv::DNS.any_instance.expects(:getresources).raises(Resolv::ResolvTimeout)
      recipient_address.save!
      assert_equal "failed", recipient_address.metadata["domain_verification"]["status"]
    end
  end

  describe "#domain_verified?" do
    before { recipient_address.email = "something@z3nmail.com" }

    describe "for a trusted domain" do
      before { RecipientAddress.any_instance.stubs(:trusted_domain?).returns(true) }

      it { recipient_address.domain_verified? }
    end

    describe "for a domain with verified DNS status" do
      it "shows the DNS status as verified" do
        stub_dns_txt 'zendeskverification.z3nmail.com', '88c8f1c4dcd7070f'
        stub_dns_txt 'zendesk_verification.z3nmail.com', '88c8f1c4dcd7070f'
        recipient_address.save!

        assert recipient_address.domain_verified?
      end
    end

    describe "for a domain with no DNS status" do
      it "shows the DNS status as failed" do
        recipient_address.metadata = {}

        refute recipient_address.domain_verified?
      end
    end
  end

  describe "#update_verification_method" do
    let(:subject) { recipient_address.update_verification_method }

    before { recipient_address.email = "something@z3nmail.com" }

    describe "when CNAME records are valid" do
      before { recipient_address.metadata = valid_metadata(%w[cname]) }

      it "sets verification method to CNAME" do
        subject
        assert_equal "cname", recipient_address.verification_method
      end

      it { assert subject }
    end

    describe "when MX and TXT records are valid" do
      before { recipient_address.metadata = valid_metadata(%w[mx txt]) }

      it "sets verification method to MX" do
        subject
        assert_equal "mx", recipient_address.verification_method
      end

      it { assert subject }
    end

    %w[mx txt].each do |record_type|
      describe "when only #{record_type} records are valid" do
        before { recipient_address.metadata = valid_metadata(%w[record_type]) }

        it "sets verification method to default value " do
          subject
          assert_equal "cname", recipient_address.verification_method
        end
      end
    end

    describe "when multiple record types are valid" do
      before { recipient_address.metadata = valid_metadata(%w[cname mx]) }

      it "prefers CNAME for verification" do
        subject

        assert_equal "cname", recipient_address.verification_method
      end
    end

    describe "when no record sets are valid" do
      before { recipient_address.metadata = {} }

      it "sets verification method to default value " do
        subject
        assert_equal "cname", recipient_address.verification_method
      end
    end
  end

  describe "#domain_verified?" do
    before { recipient_address.email = "something@z3nmail.com" }

    it "verifies the domain when at least one record is valid" do
      stub_dns_txt 'zendesk_verification.z3nmail.com', '88c8f1c4dcd7070f'
      stub_dns_txt 'zendeskverification.z3nmail.com', 'invalidvalue'

      recipient_address.save!

      assert recipient_address.domain_verified?
    end

    it "does not verify the domain when the value is incorrect" do
      stub_dns_txt 'zendesk_verification.z3nmail.com', 'invalidvalue'
      stub_dns_txt 'zendeskverification.z3nmail.com', 'anotherinvalidvalue'

      recipient_address.save!

      refute recipient_address.domain_verified?
    end

    it "does not verify the domain when the value is missing" do
      recipient_address.save!
      recipient_address.update_column(:metadata, nil)

      refute recipient_address.domain_verified?
    end
  end

  describe "#verify_spf_status!" do
    let(:recipient_address) { unverified_recipient }

    it "updates #spf_status" do
      recipient_address.update_column(:spf_status_id, RecipientAddress::SPF_STATUS.fetch(:failed))
      assert_equal :failed, recipient_address.spf_status
      recipient_address.verify_spf_status!
      assert_equal :verified, recipient_address.reload.spf_status
    end
  end

  describe "#verify_dns_status!" do
    let(:recipient_address) { unverified_recipient }

    describe_with_arturo_disabled :email_deprecate_cname_checks do
      it "updates #cname_status" do
        recipient_address.update_column(:cname_status_id, RecipientAddress::SPF_STATUS.fetch(:unknown))
        recipient_address.update_column(:metadata, nil)
        assert_equal :unknown, recipient_address.cname_status
        recipient_address.verify_dns_status!
        assert_equal :failed, recipient_address.reload.cname_status
        assert recipient_address.reload.metadata.key?("cname")
      end
    end

    it "updates #dns_status" do
      recipient_address.update_column(:metadata, nil)
      assert_nil recipient_address.metadata
      recipient_address.verify_dns_status!
      assert recipient_address.reload.metadata.key?("domain_verification")
    end
  end

  describe "#verify_mx_status!" do
    let(:recipient_address) { unverified_recipient }
    let(:valid_records) { %w[mx] }

    it "updates mx_status" do
      recipient_address.update_column(:metadata, valid_metadata(valid_records))
      assert_equal :verified, recipient_address.mx_status
      recipient_address.verify_mx_status!
      assert_equal :failed, recipient_address.reload.mx_status
    end

    it "updates mx_status" do
      recipient_address.update_column(:metadata, nil)
      assert_nil recipient_address.metadata
      recipient_address.verify_mx_status!
      assert recipient_address.reload.metadata.key?("mx")
    end

    describe "with invalid MX records" do
      before do
        stub_dns_mx('zendesk1.z3nmail.com', 'mx1.example.com')
        stub_dns_mx('zendesk2.z3nmail.com', 'mx2.example.com')
        stub_dns_mx('zendesk3.z3nmail.com', 'mx3.example.com')
        stub_dns_mx('zendesk4.z3nmail.com', 'mx4.example.com')
      end

      it 'updates metadata with failed' do
        recipient_address.verify_mx_status!

        recipient_address.metadata&.dig("mx")&.each do |_, data|
          assert_equal 'failed', data['status']
        end
      end
    end
  end

  describe "#spf_valid?" do
    it "is false by default / unknown" do
      refute recipient_address.spf_valid?
    end

    it "is false when failed" do
      recipient_address.spf_status_id = RecipientAddress::SPF_STATUS.fetch(:failed)
      refute recipient_address.spf_valid?
    end

    it "is true when verified" do
      recipient_address.spf_status_id = RecipientAddress::SPF_STATUS.fetch(:verified)
      assert recipient_address.spf_valid?
    end

    it "is true when deprecated" do
      recipient_address.spf_status_id = RecipientAddress::SPF_STATUS.fetch(:deprecated)
      assert recipient_address.spf_valid?
    end
  end

  describe "#all_txt_records_valid?" do
    it "is invalid with no records" do
      recipient_address.metadata = nil
      refute recipient_address.all_txt_records_valid?
    end

    it "is invalid with empty hash" do
      recipient_address.metadata = {"txt" => {}}
      refute recipient_address.all_txt_records_valid?
    end

    it "is valid when all results are verified" do
      recipient_address.metadata = valid_metadata(%w[txt])
      assert recipient_address.all_txt_records_valid?
    end
  end

  describe "#brand" do
    it "is brand" do
      recipient_address.brand = brand
      assert_equal recipient_address.brand, brand
    end

    it "is default_brand when brand is missing" do
      recipient_address.brand = nil
      assert_equal recipient_address.brand.id, account.default_brand_id
    end
  end

  describe "#active_brand" do
    before { recipient_address.brand = brand }

    it "is brand" do
      assert_equal brand.id, recipient_address.active_brand.id
    end

    it "is default_brand when brand is inactive" do
      brand.update_column(:active, false)
      assert_equal account.default_brand_id, recipient_address.active_brand.id
    end

    it "is default_brand when brand is missing" do
      recipient_address.brand = nil
      assert_equal account.default_brand_id, recipient_address.active_brand.id
    end
  end

  describe "#delete" do
    it "is private" do
      assert_raise NoMethodError do
        recipient_address.delete
      end
    end
  end

  describe "#force_destroy" do
    it "cans delete default" do
      assert_difference "RecipientAddress.count(:all)", -1 do
        recipient_addresses(:default).force_destroy
      end
    end

    it "cans delete backup" do
      assert_difference "RecipientAddress.count(:all)", -1 do
        recipient_addresses(:backup).force_destroy
      end
    end

    it "cans delete external email" do
      external_email_credential
      assert_difference "RecipientAddress.count(:all)", -1 do
        assert_difference "ExternalEmailCredential.count(:all)", -1 do
          external_email_credential.recipient_address.force_destroy
        end
      end
    end
  end

  describe "#product" do
    subject { RecipientAddress.new }
    it "defaults to support" do
      subject.product.must_equal RecipientAddress::SUPPORT_PRODUCT
      subject.product = RecipientAddress::COLLABORATION_PRODUCT
      subject.product.must_equal RecipientAddress::COLLABORATION_PRODUCT
    end
  end

  describe "#domain_verification_code" do
    let(:support_account) { accounts(:support) }
    let(:recipient_address) { RecipientAddress.new { |r| r.account = support_account; r.email = "foo@z3nmail.com" } }

    it "obfuscates" do
      recipient_address.domain_verification_code.must_equal "df7c2b39d625ff4b"
    end

    describe "for the same domain" do
      let(:minimum_account) { accounts(:minimum) }
      let(:minimum_recipient_address) { RecipientAddress.new { |r| r.account = minimum_account; r.email = "foo@z3nmail.com" } }

      it "obfuscates differently for different accounts" do
        assert_not_equal recipient_address.domain_verification_code, minimum_recipient_address.domain_verification_code
      end
    end

    describe "for the same account" do
      let(:second_recipient_address) { RecipientAddress.new { |r| r.account = support_account; r.email = "foo@example.com" } }

      it "obfuscates differently for different domains" do
        assert_not_equal recipient_address.domain_verification_code, second_recipient_address.domain_verification_code
      end
    end
  end

  def stub_dns_txt(domain, value)
    RecipientAddress.any_instance.stubs(:stubbed_txt_status?).returns(false) # make it use real lookups
    Resolv::DNS.any_instance.expects(:getresources).with("#{domain}.", Resolv::DNS::Resource::IN::TXT).returns [stub(data: value)]
  end

  def stub_dns_mx(domain, value)
    RecipientAddress.any_instance.stubs(:stubbed_mx_status?).returns(false) # make it use real lookups
    value = Resolv::DNS::Name.create(value)
    Resolv::DNS.any_instance.expects(:getresources).with(domain, Resolv::DNS::Resource::IN::MX).at_least_once.returns [stub(exchange: value, preference: 10)]
  end

  def stub_metadata(type, record, metadata = {}, status = "verified")
    type_record = metadata.dig(type) || {}
    server_number = record[/\d+/].to_i
    lookup_result = "mail#{server_number}.#{Zendesk::Configuration.fetch(:host)}"
    metadata.merge(type => type_record.merge(record => { "lookup_result" => lookup_result, "status" => status }))
  end

  def valid_metadata(types, range = (1..4))
    metadata = {}
    types.each do |type|
      range.each do |n|
        metadata.merge!(stub_metadata(type, "zendesk#{n}", metadata))
      end
    end

    metadata
  end
end
