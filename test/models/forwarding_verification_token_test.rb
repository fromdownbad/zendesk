require_relative "../support/test_helper"

SingleCov.covered!

describe ForwardingVerificationToken do
  fixtures :accounts

  it "saves" do
    account = accounts(:minimum)
    token = account.forwarding_verification_tokens.new(from_email: "test@mpix.com")
    token.value = '123456789'
    token.save!
  end
end
