require_relative "../support/test_helper"

SingleCov.covered!

describe HelpCenter do
  describe ".change_state" do
    before do
      HelpCenter.create(
        id: 100,
        state: "enabled",
        account_id: 200,
        brand_id: 300
      )
    end

    it "changes the state of a HelpCenter" do
      HelpCenter.change_state(state: "restricted", brand_id: 300)
      help_center = HelpCenter.find_by_brand_id(300)
      assert_equal "restricted", help_center.state
    end
  end

  describe ".exists_for_brand_id?" do
    it "returns a HelpCenter when a record with matching brand_id exists" do
      HelpCenter.create(
        id: 100,
        state: "enabled",
        account_id: 200,
        brand_id: 300
      )

      assert HelpCenter.exists_for_brand_id?(300)
    end

    it "returns nil when no record with matching brand_id exists" do
      refute HelpCenter.exists_for_brand_id?(999)
    end
  end

  describe ".find_by_brand_id" do
    it "returns a HelpCenter when a record with matching brand_id exists" do
      HelpCenter.create(
        id: 100,
        state: "enabled",
        account_id: 200,
        brand_id: 300
      )

      help_center = HelpCenter.find_by_brand_id(300)
      assert_equal help_center.id, 100
    end

    it "returns nil when no record with matching brand_id exists" do
      assert_nil HelpCenter.find_by_brand_id(999)
    end
  end

  describe '.destroy_by_brand_id' do
    it "destroys the HelpCenter" do
      HelpCenter.create(
        id: 100,
        state: "enabled",
        account_id: 200,
        brand_id: 300
      )

      HelpCenter.destroy_by_brand_id(300)
      assert_nil HelpCenter.find_by_brand_id(300)
    end

    it "doesn't complain about destroying a non-existing HelpCenter" do
      HelpCenter.destroy_by_brand_id(444)
      assert_nil HelpCenter.find_by_brand_id(444)
    end
  end

  describe "#id" do
    it "returns the ID" do
      help_center = HelpCenter.new(id: 42, state: "enabled")
      assert_equal help_center.id, 42
    end
  end

  describe "#state" do
    it "returns the ID" do
      help_center = HelpCenter.new(id: 42, state: "enabled")
      assert_equal help_center.state, "enabled"
    end
  end

  describe "#restricted?" do
    it "returns true for a restricted HelpCenter" do
      help_center = HelpCenter.new(id: 42, state: "restricted")
      assert help_center.restricted?
    end

    it "returns false for an enabled HelpCenter" do
      help_center = HelpCenter.new(id: 42, state: "enabled")
      refute help_center.restricted?
    end

    it "returns false for an archived HelpCenter" do
      help_center = HelpCenter.new(id: 42, state: "archived")
      refute help_center.restricted?
    end
  end

  describe "#enabled?" do
    it "returns false for a restricted HelpCenter" do
      help_center = HelpCenter.new(id: 42, state: "restricted")
      refute help_center.enabled?
    end

    it "returns true for an enabled HelpCenter" do
      help_center = HelpCenter.new(id: 42, state: "enabled")
      assert help_center.enabled?
    end

    it "returns false for an archived HelpCenter" do
      help_center = HelpCenter.new(id: 42, state: "archived")
      refute help_center.enabled?
    end
  end

  describe "#archived?" do
    it "returns false for a restricted HelpCenter" do
      help_center = HelpCenter.new(id: 42, state: "restricted")
      refute help_center.archived?
    end

    it "returns false for an enabled HelpCenter" do
      help_center = HelpCenter.new(id: 42, state: "enabled")
      refute help_center.archived?
    end

    it "returns true for a archived HelpCenter" do
      help_center = HelpCenter.new(id: 42, state: "archived")
      assert help_center.archived?
    end
  end
end
