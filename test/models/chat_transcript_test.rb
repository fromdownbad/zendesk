require_relative "../support/test_helper"

SingleCov.covered!

describe ChatTranscript do
  fixtures :accounts, :tickets

  should validate_presence_of :account
  should validate_presence_of :ticket
  should validate_presence_of :chat_started_event
  should validate_presence_of :chat_id
  should validate_presence_of :visitor_id

  let(:ticket) { tickets(:minimum_1) }
  let(:chat_started_event) do
    ChatStartedEvent.new(value: { chat_id: 'foo', visitor_id: 'bar' }).tap do |event|
      ticket.will_be_saved_by(users(:minimum_admin))
      ticket.audit.events << event
      ticket.save!
    end
  end
  let(:transcript) do
    ChatTranscript.new(account: accounts(:minimum), chat_started_event: chat_started_event, ticket: ticket)
  end

  describe 'sets default values' do
    it('sets chat_history') { assert_equal [], transcript.chat_history }
    it('sets web_paths') { assert_equal [], transcript.web_paths }
    it('sets chat_id') { assert_equal 'foo', transcript.chat_id }
    it('sets visitor_id') { assert_equal 'bar', transcript.visitor_id }
    it('sets initial_value_length') { assert_equal 34, transcript.initial_value_length }
  end

  describe '.value_limit' do
    it 'returns the column size for value' do
      assert_equal 65535, ChatTranscript.value_limit
    end
  end

  describe '.last_message_for_tickets' do
    let(:all_tickets) { Ticket.where(id: ticket.id) }
    let(:chat_message) do
      {
        'type' => 'ChatMessage',
        'actor_id' => '123',
        'actor_type' => 'end-user',
        'actor_name' => 'jack',
        'timestamp' => 0,
        'message' => 'testing'
      }
    end
    let(:chat_leave) do
      {
        'type' => 'ChatLeave',
        'actor_id' => '123',
        'actor_type' => 'agent',
        'actor_name' => 'rose',
        'timestamp' => 0
      }
    end

    describe 'for tickets without any chat events' do
      it 'returns an empty hash' do
        assert_equal({}, ChatTranscript.last_message_for_tickets(all_tickets))
      end
    end

    describe 'for tickets with one chat started event' do
      before do
        ticket.will_be_saved_by(nil)
        ChatStartedEvent.new(ticket: ticket, value: { chat_id: 'foo', visitor_id: 'bar' }).tap(&:apply!)
      end

      describe 'with empty transcripts' do
        it 'returns nil for last message' do
          assert_equal({ ticket.id => nil }, ChatTranscript.last_message_for_tickets(all_tickets))
        end
      end

      describe 'with transcripts' do
        let(:transcript_format) { :json }
        before do
          # force new transcript for each item
          ChatTranscript.stubs(:value_limit).returns(1)
          transcripts = ChatTranscript::ChatTranscriptsBuilder.build(format: transcript_format) do |builder|
            builder.add_chat_history(chat_history)
          end
          ChatStartedEvent.any_instance.stubs(:chat_transcripts).returns transcripts
        end

        describe 'with single chat message' do
          let(:chat_history) { [chat_message] }

          it 'returns message for each ticket' do
            assert_equal 'testing', ChatTranscript.last_message_for_tickets(all_tickets)[ticket.id].body
          end
        end

        describe 'with trailing non-chat message' do
          let(:chat_history) { [chat_message, chat_leave] }

          it 'returns message for each ticket' do
            assert_equal 'testing', ChatTranscript.last_message_for_tickets(all_tickets)[ticket.id].body
          end
        end

        describe 'with transient history' do
          let(:chat_history) { [chat_message] }
          let(:transcript_format) { :transient_json }

          it 'skips transient transcripts' do
            assert_equal({ ticket.id => nil }, ChatTranscript.last_message_for_tickets(all_tickets))
          end
        end
      end
    end

    describe 'for tickets with two chat started events' do
      before do
        ticket.will_be_saved_by(nil)
        event = ChatStartedEvent.new(ticket: ticket, value: { chat_id: 'foo', visitor_id: 'bar' }).tap(&:apply!)
        event.chat_transcripts << ChatTranscript::ChatTranscriptsBuilder.build(account: accounts(:minimum), chat_started_event: event, ticket: ticket) do |builder|
          builder.add_chat_history([chat_message])
        end
        # default ordering of ticket events is based on created_at, which is problematic in test cases like these
        Timecop.travel(1.minute) do
          ticket.will_be_saved_by(nil)
          ChatStartedEvent.new(ticket: ticket, value: { chat_id: 'foo2', visitor_id: 'bar2' }).tap(&:apply!)
        end
      end

      it 'returns nil if last event has no transcripts' do
        assert_nil ChatTranscript.last_message_for_tickets(all_tickets)[ticket.id]
      end
    end
  end

  describe '#array_item_size' do
    it 'returns storage size of a new item' do
      transcript = ChatTranscript.new
      assert_equal 14, transcript.array_item_size('foo' => 'bar')
    end
  end
end
