require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe ForumDisplayType do
  it("have an id") { assert_respond_to subject, :id }
  it("have a name") { assert_respond_to subject, :name }

  it("have many forums") do
    mock_forums_collection = [Forum.new]
    # RAILS5UPGRADE: Forum. Triggers a gem modification because of https://github.com/zilkey/active_hash/blob/v2.0.0/lib/associations/associations.rb#L111. Create User Story when validated
    Forum.stubs(:scoped).with(conditions: {display_type_id: 111}).returns(mock_forums_collection)
    assert_equal mock_forums_collection, ForumDisplayType.new(id: 111).forums
  end

  describe "whose name is Articles" do
    subject { ForumDisplayType.find_by_name("Articles") }

    it("respond with true to #articles?") { assert subject.articles? }
    it("respond with false to #ideas?") { refute subject.ideas? }
    it("respond with false to #questions?") { refute subject.questions? }
  end

  describe "whose name is Ideas" do
    subject { ForumDisplayType.find_by_name("Ideas") }

    it("respond with false to #articles?") { refute subject.articles? }
    it("respond with true to #ideas?") { assert subject.ideas? }
    it("respond with false to #questions?") { refute subject.questions? }
  end

  describe "whose name is Questions" do
    subject { ForumDisplayType.find_by_name("Questions") }

    it("respond with false to #articles?") { refute subject.articles? }
    it("respond with false to #ideas?") { refute subject.ideas? }
    it("respond with true to #questions?") { assert subject.questions? }
  end

  it "is accessible by name as a constant" do
    ForumDisplayType.all.each do |forum_display_type|
      constant_name = forum_display_type.name.upcase.gsub(/\W/, "_")

      assert_equal ForumDisplayType.const_get(constant_name), forum_display_type
    end
  end
end
