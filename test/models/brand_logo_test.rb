require_relative "../support/test_helper"

SingleCov.covered! uncovered: 19

describe BrandLogo do
  has_imagemagick = system("which identify >/dev/null") || (ENV["CI"] && raise("identify issue"))

  conditional_describe BrandLogo, has_imagemagick do
    let(:logo) { FactoryBot.create(:brand_logo, filename: "large_1.png") }

    it "is valid" do
      assert logo.valid?
    end

    describe "image with valid dimensions" do
      before do
        BrandLogo.any_instance.stubs(:valid_dimensions?).returns(true)
      end

      should_validate_presence_of :brand, :account

      it "validates extension" do
        logo.filename = "xxx.yyy"
        refute logo.valid?
        assert_equal ["extension yyy is not allowed. Please use jpg, jpeg, gif, or png for your logo."], logo.errors.all_on(:extension)
      end
    end

    describe "#destroy" do
      it "does not need to check imageness or validity" do
        assert thumbnail = logo.thumbnails.first
        logo.expects(:image?).never
        logo.expects(:valid_dimensions?).never
        assert logo.destroy
        assert_raises(ActiveRecord::RecordNotFound) { thumbnail.reload }
      end
    end

    describe "#valid_image_file" do
      describe_with_arturo_disabled :email_increase_attachment_size_limit do
        it "validates size" do
          logo.size = 1_000_000_000_000
          refute logo.valid?
          assert_equal ["Attachment is too large. Limit 7 megabytes."], logo.errors[:base]
        end
      end

      describe_with_arturo_enabled :email_increase_attachment_size_limit do
        it "allows images within the file size limit" do
          logo.expects(:valid_dimensions?).returns(true) # photo fixture's actual content is empty
          logo.size = 15.megabytes
          assert logo.valid?
          assert_empty logo.errors
        end

        it "does not allow images over the brand logo attachment_fu max size limit" do
          logo.expects(:valid_dimensions?).returns(true) # photo fixture's actual content is empty
          logo.size = 21.megabytes
          refute logo.valid?
          assert_equal ["Size is not included in the list (1..20971520)"], logo.errors[:size]
          assert_empty logo.errors[:base]
        end

        it "does not allow brand logos over the account attachment size limit" do
          logo.size = 51.megabytes
          refute logo.valid?
          assert_equal ["Attachment is too large. Limit 50 megabytes."], logo.errors[:base]
          assert_equal ["Size is not included in the list (1..20971520)"], logo.errors[:size]
        end
      end

      it "validates dimensions" do
        logo.uploaded_data = Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/lottapixel.jpg", content_type: "image/jpeg")
        refute logo.valid?
        assert_equal ["Image dimensions are invalid. Must be 30..64000000 pixels."], logo.errors[:base]
      end
    end

    describe "with an oversized logo" do
      before do
        @result = MiniMagick::Image.new(logo.full_filename)
        @thumb  = MiniMagick::Image.new(logo.full_filename(:thumb))
        @small  = MiniMagick::Image.new(logo.full_filename(:small))
      end

      it "resizes to a max-width of 80" do
        assert(@result[:width] <= 80)
      end

      it "creates two thumbnails" do
        assert(@thumb)
        assert(@thumb[:height] <= 32)

        assert(@small)
        assert(@small[:height] <= 24)
      end
    end
  end
end
