require_relative "../../support/test_helper"

SingleCov.covered!

describe TicketActivity do
  fixtures :accounts, :users, :user_identities, :tickets, :events

  before do
    Activity.without_arsi.delete_all

    @user = tickets(:minimum_1).assignee

    @assignment_on_create = Create.create(
      account: accounts(:minimum),
      audit: events(:create_audit_for_minimum_ticket_1),
      ticket: tickets(:minimum_1),
      value: tickets(:minimum_1).assignee.id,
      value_previous: nil,
      value_reference: "assignee_id",
      author: accounts(:minimum).owner
    )

    @assignment_on_update = Change.create(
      account: accounts(:minimum),
      audit: events(:create_audit_for_minimum_ticket_1),
      ticket: tickets(:minimum_1),
      value: tickets(:minimum_1).assignee.id,
      value_previous: nil,
      value_reference: "assignee_id",
      author: accounts(:minimum).owner
    )

    @assignment_on_solved = Change.create(
      account: accounts(:minimum),
      audit: events(:create_audit_for_minimum_ticket_1),
      ticket: tickets(:minimum_4),
      value: tickets(:minimum_4).assignee.id,
      value_previous: nil,
      value_reference: "assignee_id",
      author: accounts(:minimum).owner
    )

    @comment = Comment.create(
      account: accounts(:minimum),
      audit: events(:create_audit_for_minimum_ticket_1),
      ticket: tickets(:minimum_1),
      body: "Foo",
      value_previous: nil,
      value_reference: nil,
      author: accounts(:minimum).owner
    )

    @priority_bump = Change.create(
      account: accounts(:minimum),
      audit: events(:create_audit_for_minimum_ticket_1),
      ticket: tickets(:minimum_1),
      value: "2",
      value_previous: "1",
      value_reference: "priority_id",
      author: accounts(:minimum).owner
    )
  end

  it "implements .activity_type" do
    assert_equal "tickets.assignment", TicketActivity.send(:activity_type, @user, @assignment_on_create, tickets(:minimum_1))
    assert_equal "tickets.assignment", TicketActivity.send(:activity_type, @user, @assignment_on_update, tickets(:minimum_1))
    assert_equal "tickets.comment", TicketActivity.send(:activity_type, @user, @comment, tickets(:minimum_1))
    assert_equal "tickets.priority_increase", TicketActivity.send(:activity_type, @user, @priority_bump, tickets(:minimum_1))
  end

  describe ".create_activity_for" do
    describe "for ticket assignment on ticket creation" do
      before do
        assert_equal 0, TicketActivity.count(:all)
        TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @assignment_on_create, update_type: :create)
      end

      should_change("the number of ticket_activities", by: 1) { TicketActivity.count(:all) }

      it "creates a TicketActivity object" do
        activity = TicketActivity.first
        assert_equal "tickets.assignment", activity.verb
        assert_equal "#{accounts(:minimum).owner.name} assigned ticket ##{tickets(:minimum_1).nice_id} to you.", activity.title
        assert_equal tickets(:minimum_1).description, activity.detail

        assert_equal accounts(:minimum).owner, activity.actor
        assert_equal tickets(:minimum_1).assignee, activity.user
        assert_equal @assignment_on_create.ticket, activity.activity_object
        assert_equal tickets(:minimum_1), activity.activity_target
      end
    end

    describe "for ticket assignment on ticket update" do
      before do
        assert_equal 0, TicketActivity.count(:all)
        TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @assignment_on_update)
      end

      should_change("the number of ticket_activities", by: 1) { TicketActivity.count(:all) }

      it "creates a TicketActivity object" do
        activity = TicketActivity.first
        assert_equal "tickets.assignment", activity.verb
        assert_equal "#{accounts(:minimum).owner.name} assigned ticket ##{tickets(:minimum_1).nice_id} to you.", activity.title
        assert_equal tickets(:minimum_1).description, activity.detail
        assert_equal accounts(:minimum).owner, activity.actor
        assert_equal tickets(:minimum_1).assignee, activity.user
        assert_equal @assignment_on_update.ticket, activity.activity_object
        assert_equal tickets(:minimum_1), activity.activity_target
      end
    end

    describe "for ticket assignment on ticket solving" do
      before do
        assert_equal 0, TicketActivity.count(:all)
        TicketActivity.create_activity_for(tickets(:minimum_4).assignee, event: @assignment_on_solved)
      end

      should_not_change("the number of ticket_activities") { TicketActivity.count(:all) }
    end

    describe "for ticket comment" do
      before do
        assert_equal 0, TicketActivity.count(:all)
        TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @comment)
      end

      should_change("the number of ticket_activities", by: 1) { TicketActivity.count(:all) }

      it "creates a TicketActivity object" do
        activity = TicketActivity.first
        assert_equal "tickets.comment", activity.verb
        assert_equal "#{accounts(:minimum).owner.name} commented on ticket ##{tickets(:minimum_1).nice_id}: #{@comment.body}.", activity.title
        assert_equal tickets(:minimum_1).comments.last.body, activity.detail

        assert_equal accounts(:minimum).owner, activity.actor
        assert_equal tickets(:minimum_1).assignee, activity.user
        assert_equal @comment, activity.activity_object
        assert_equal tickets(:minimum_1), activity.activity_target
      end
    end

    describe "for ticket priority bump" do
      before do
        assert_equal 0, TicketActivity.count(:all)
        TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @priority_bump)
      end

      should_change("the number of ticket_activities", by: 1) { TicketActivity.count(:all) }

      it "creates a TicketActivity object" do
        activity = TicketActivity.first
        assert_equal "tickets.priority_increase", activity.verb
        assert_equal "#{accounts(:minimum).owner.name} increased priority of ticket ##{tickets(:minimum_1).nice_id} to #{PriorityType[@priority_bump.value.to_i]}.", activity.title

        assert_equal ("%s to %s" % [PriorityType[@priority_bump.value_previous.to_i], PriorityType[@priority_bump.value.to_i]]), activity.detail

        assert_equal accounts(:minimum).owner, activity.actor
        assert_equal tickets(:minimum_1).assignee, activity.user
        assert_equal @priority_bump, activity.activity_object
        assert_equal tickets(:minimum_1), activity.activity_target
      end
    end
  end

  describe "named scopes and serialization" do
    before do
      Timecop.freeze
      TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @assignment_on_create)
      TicketActivity.last.update_attribute(:created_at, 5.minutes.ago)
      TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @assignment_on_update)
      TicketActivity.last.update_attribute(:created_at, 4.minutes.ago)
      TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @comment)
      TicketActivity.last.update_attribute(:created_at, 3.minutes.ago)
      TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @priority_bump)
      TicketActivity.last.update_attribute(:created_at, 2.minutes.ago)
    end

    it "provides named scopes to obtain recent activities and activities since a timestamp" do
      all = TicketActivity.order("created_at DESC").to_a
      assert_equal all, tickets(:minimum_1).assignee.activities.recent
      assert_equal all[0..2], tickets(:minimum_1).assignee.activities.recent.since(all[3].created_at)

      assert_equal all[0..1], tickets(:minimum_1).assignee.activities.recent.since(all[2].created_at)

      assert_equal [], tickets(:minimum_1).assignee.activities.recent.since(all[0].created_at)
    end

    it "serializes to XML format" do
      xml = tickets(:minimum_1).assignee.activities.includes([:user, :actor, :activity_object, :activity_target]).to_a.to_xml(root: "activities")
      data = Nokogiri::XML(xml)
      activities = data.xpath("//activity")
      assert_equal 4, activities.size
      assert_equal ["tickets.priority_increase", "tickets.comment", "tickets.assignment", "tickets.assignment"], activities.map { |a| a.at("verb").text }

      activities.each do |activity|
        ["created-at", "id", "verb", "actor", "user", "object", "target"].each do |sel|
          assert_not_nil activity.at(sel)
        end

        assert_equal accounts(:minimum).owner.name, activity.at("actor/name").text
        assert_equal accounts(:minimum).owner.id, activity.at("actor/id").text.to_i
        assert_equal accounts(:minimum).owner.is_verified.to_s, activity.at("actor/is-verified").text
        assert_equal accounts(:minimum).owner.email, activity.at("actor/email").text
        assert_equal accounts(:minimum).owner.photo_url, activity.at("actor/photo-url").text

        assert_equal tickets(:minimum_1).assignee.name, activity.at("user/name").text
        assert_equal tickets(:minimum_1).assignee.id, activity.at("user/id").text.to_i
        assert_equal tickets(:minimum_1).assignee.is_verified.to_s, activity.at("user/is-verified").text
        assert_equal tickets(:minimum_1).assignee.email, activity.at("user/email").text
        assert_equal tickets(:minimum_1).assignee.photo_url, activity.at("user/photo-url").text

        assert_not_nil activity.at("target/ticket")
        assert_equal "1", activity.at("target/ticket/nice-id").text
        assert_equal tickets(:minimum_1).subject, activity.at("target/ticket/subject").text

        case activity.at("verb").text
        when "tickets.assignment"
          assert_equal "#{accounts(:minimum).owner.name} assigned ticket ##{tickets(:minimum_1).nice_id} to you.", activity.at("title").text
          assert_not_nil activity.at("object/ticket")
          assert_equal "1", activity.at("object/ticket/nice-id").text
          assert_equal tickets(:minimum_1).subject, activity.at("object/ticket/subject").text
        when "tickets.comment"
          assert_equal "#{accounts(:minimum).owner.name} commented on ticket ##{tickets(:minimum_1).nice_id}: #{@comment.body}.", activity.at("title").text
          assert_not_nil activity.at("object/comment")
          assert_equal "false", activity.at("object/comment/is-public").text
          assert_equal "Foo", activity.at("object/comment/value").text
        when "tickets.priority_increase"
          assert_not_nil activity.at("object/change")
          assert_equal "#{accounts(:minimum).owner.name} increased priority of ticket ##{tickets(:minimum_1).nice_id} to #{PriorityType[@priority_bump.value.to_i]}.", activity.at("title").text
          assert_equal "2", activity.at("object/change/value").text
          assert_equal "1", activity.at("object/change/value-previous").text
        end
      end
    end

    it "serializes to JSON format" do
      json = tickets(:minimum_1).assignee.activities.includes([:user, :actor, :activity_object, :activity_target]).to_a.to_json

      activities = JSON.parse(json)

      assert_equal 4, activities.size
      assert_equal ["tickets.priority_increase", "tickets.comment", "tickets.assignment", "tickets.assignment"], activities.map { |a| a["verb"] }

      activities.each do |activity|
        ["created_at", "id", "verb", "actor", "user", "object", "target"].each do |sel|
          assert_not_nil activity[sel]
        end

        assert_equal accounts(:minimum).owner.name, activity["actor"]["name"]
        assert_equal accounts(:minimum).owner.id, activity["actor"]["id"]
        assert_equal accounts(:minimum).owner.email, activity["actor"]["email"]
        assert_equal accounts(:minimum).owner.is_verified, activity["actor"]["is_verified"]
        assert_equal accounts(:minimum).owner.photo_url, activity["actor"]["photo_url"]

        assert_equal tickets(:minimum_1).assignee.name, activity["user"]["name"]
        assert_equal tickets(:minimum_1).assignee.id, activity["user"]["id"]
        assert_equal tickets(:minimum_1).assignee.email, activity["user"]["email"]
        assert_equal tickets(:minimum_1).assignee.is_verified, activity["user"]["is_verified"]
        assert_equal tickets(:minimum_1).assignee.photo_url, activity["user"]["photo_url"]

        assert_not_nil activity["target"]["ticket"]
        assert_equal 1, activity["target"]["ticket"]["nice_id"]
        assert_equal tickets(:minimum_1).subject, activity["target"]["ticket"]["subject"]

        case activity["verb"]
        when "tickets.assignment"
          assert_equal "#{accounts(:minimum).owner.name} assigned ticket ##{tickets(:minimum_1).nice_id} to you.", activity["title"]
          assert_not_nil activity["object"]["ticket"]
          assert_equal 1, activity["object"]["ticket"]["nice_id"]

        when "tickets.comment"
          assert_not_nil activity["object"]["comment"]
          assert_equal "#{accounts(:minimum).owner.name} commented on ticket ##{tickets(:minimum_1).nice_id}: #{@comment.body}.", activity["title"]
          assert_equal false, activity["object"]["comment"]["is_public"]
          assert_equal "Foo", activity["object"]["comment"]["value"]
        when "tickets.priority_increase"
          assert_not_nil activity["object"]["change"]
          assert_equal "#{accounts(:minimum).owner.name} increased priority of ticket ##{tickets(:minimum_1).nice_id} to #{PriorityType[@priority_bump.value.to_i]}.", activity["title"]
          assert_equal "2", activity["object"]["change"]["value"]
          assert_equal "1", activity["object"]["change"]["value_previous"]
        end
      end
    end
  end
end
