require_relative "../../support/test_helper"

SingleCov.covered!

describe Activity do
  fixtures :accounts, :users, :tickets, :events

  should_validate_presence_of :account_id, :user_id, :actor_id, :activity_object_id, :activity_object_type

  describe "as the base class for activities" do
    it "raises on .create_activity" do
      assert_raise RuntimeError, "Must be implemented in subclass" do
        Activity.create_activity_for(nil, {})
      end
    end

    it "raises on serialization" do
      assert_raise RuntimeError, "Must be implemented in subclass" do
        Activity.new.serialization_options
      end
    end
  end

  describe "#cleanup" do
    before do
      Activity.without_arsi.delete_all

      @user = tickets(:minimum_1).assignee

      @comment = Comment.create(
        account: accounts(:minimum),
        audit: events(:create_audit_for_minimum_ticket_1),
        ticket: tickets(:minimum_1),
        body: "Foo",
        value_previous: nil,
        value_reference: nil,
        author: accounts(:minimum).owner
      )
      Timecop.freeze

      TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @comment)
      TicketActivity.last.update_attribute(:created_at, 2.months.ago)
      TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @comment)
      TicketActivity.last.update_attribute(:created_at, 2.months.ago)

      @new_activities = []
      @new_activities << TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @comment)
      TicketActivity.last.update_attribute(:created_at, 3.minutes.ago)
      @new_activities << TicketActivity.create_activity_for(tickets(:minimum_1).assignee, event: @comment)
      TicketActivity.last.update_attribute(:created_at, 2.minutes.ago)
    end

    it "deletes old activities" do
      assert_equal 4, Activity.count(:all)
      Activity.cleanup
      assert_equal 2, Activity.count(:all)
      @new_activities.each do |activity|
        assert Activity.exists?(id: activity.id)
      end
    end
  end
end
