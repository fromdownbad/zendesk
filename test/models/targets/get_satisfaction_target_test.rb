require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'GetSatisfactionTarget' do
  fixtures :accounts, :users, :tickets, :events

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @user = users(:minimum_agent)
    @ticket.will_be_saved_by(@user, via_id: ViaType.RULE)
    @ticket.via_id = ViaType.GET_SATISFACTION
    @ticket.external_id = "1234"
    @event = events(:create_external_for_minimum_ticket_1)
    @ticket.audit.events << @event
    @event.stubs(:ticket).returns(@ticket)
    @message = "Test message"
    @target = GetSatisfactionTarget.new(
      title: "GetSatisfaction Target",
      account: accounts(:minimum),
      email: "someone@example.com",
      password: '123456',
      account_name: "the_account"
    )
    @target.current_user = @user
    @target.encrypt!
  end

  describe "save" do
    it "saves the target when valid" do
      assert @target.save
    end
  end

  describe "validation" do
    it "is valid if title and settings(token, subdomain, room) are provided" do
      assert @target.valid?
    end

    it "does not be valid if title is missing" do
      @target.title = nil
      refute @target.valid?
      assert @target.errors[:title].any?
    end

    it "is not valid if password is missing" do
      @target.password = nil
      refute @target.valid?
      assert @target.errors[:password].any?
    end

    describe "settings" do
      it "does not be valid if email is missing" do
        @target.settings.delete(:email)
        refute @target.valid?
        assert @target.errors[:email].any?
      end

      it "does not be valid if account_name is missing" do
        @target.settings.delete(:account_name)
        refute @target.valid?
        assert @target.errors[:account_name].any?
      end
    end
  end

  describe "#to_s" do
    it "prints out a simple string detailing the url, account and email" do
      assert_equal "GetSatisfactionTarget, url: #{@target.url}, :account_name: #{@target.account_name}, email: #{@target.email}", @target.to_s
    end
  end

  describe '#url for a target with no saved URL loaded through .find' do
    before do
      @target.url = nil
      @target.save!
    end

    it 'stills have the default URL' do
      assert_equal GetSatisfactionTarget::DEFAULT_URL, GetSatisfactionTarget.find(@target.id).url
    end
  end

  describe "#send_message" do
    before do
      stub_request(:post, /http:\/\/api.getsatisfaction.com/).
        with(basic_auth: ['someone@example.com', '123456']).
        to_return(body: "Reply created.", status: 201)
      stub_request(:put, /http:\/\/api.getsatisfaction.com/).
        with(basic_auth: ['someone@example.com', '123456']).
        to_return(body: "Reply created.")
      @expected_headers = {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Authorization' => 'Basic c29tZW9uZUBleGFtcGxlLmNvbToxMjM0NTY=',
        'User-Agent' => 'Ruby'
      }
    end

    it "raises an exception if via type is not ViaType.GET_SATISFACTION" do
      @ticket.via_id = ViaType.WEB_FORM
      assert_raises(RuntimeError) { @target.send_message(@message, @event) }
    end

    it "posts a reply in the get satisfaction topic and update the status of the topic" do
      @target.send_message(@message, @event)
      assert_requested(:post, "http://api.getsatisfaction.com/topics/1234/replies", headers: @expected_headers)
      assert_requested(:put, "http://api.getsatisfaction.com/topics/1234/status", headers: @expected_headers)
    end

    it "only update the status of the topic without post a new reply if the message is blank" do
      @target.send_message('', @event)
      assert_requested(:put, "http://api.getsatisfaction.com/topics/1234/status", headers: @expected_headers)
    end

    it "raises an exception if the reply could not be created" do
      stub_request(:post, /http:\/\/api.getsatisfaction.com/).to_return(status: 401)
      assert_raises(RuntimeError) { @target.send_message(@message, @event) }
    end

    it "raises an exception if the status of the topic could not be updated" do
      stub_request(:put, /http:\/\/api.getsatisfaction.com/).to_return(status: 401)
      assert_raises(RuntimeError) { @target.send_message(@message, @event) }
    end

    it "does not post a reply to a GetSatisfaction topic if no GetSatisfaction topic linking or external ID exists on the ticket" do
      Net::HTTP.expects(:new).never
      @ticket.external_id = nil
      assert_raises(RuntimeError) { @target.send_message(@message, @event) }
    end

    it "posts a reply to a GetSatisfaction topic if an external ID exists on the ticket and no GetSatisfaction topic linking exists" do
      @target.send_message(@message, @event)
      assert_requested(:post, "http://api.getsatisfaction.com/topics/1234/replies", headers: @expected_headers)
      assert_requested(:put, "http://api.getsatisfaction.com/topics/1234/status", headers: @expected_headers)
    end

    it "posts a reply to a GetSatisfaction topic if a GetSatisfaction topic linking exists and no external ID exists on the ticket" do
      ExternalLink.create!(@ticket, type: "GetSatisfactionTopic", topic_id: "5678")
      @ticket.external_id = nil
      @target.send_message(@message, @event)
      assert_requested(:post, "http://api.getsatisfaction.com/topics/5678/replies", headers: @expected_headers)
      assert_requested(:put, "http://api.getsatisfaction.com/topics/5678/status", headers: @expected_headers)
    end

    it "picks the GetSatisfaction topic linking over the external ID if both are present" do
      @event.ticket.external_id = "1234"
      ExternalLink.create!(@ticket, type: "GetSatisfactionTopic", topic_id: "FOO-PICK-ME-100")
      @target.send_message(@message, @event)
      assert_requested(:post, "http://api.getsatisfaction.com/topics/FOO-PICK-ME-100/replies", headers: @expected_headers)
      assert_requested(:put, "http://api.getsatisfaction.com/topics/FOO-PICK-ME-100/status", headers: @expected_headers)
    end
  end
end
