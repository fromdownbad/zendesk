require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'TwilioTarget' do
  fixtures :accounts, :events

  let(:account_sid) { Zendesk::Configuration.dig!(:voice, :twilio, :account_sid) }
  let(:token) { Zendesk::Configuration.dig!(:voice, :twilio, :token) }
  let(:from) { "+14159978967" }
  let(:to) { "+12125553434" }

  before do
    @target = TwilioTarget.new(
      title: "Twilio Target",
      account: accounts(:minimum),
      sid: account_sid,
      token: token,
      from: from,
      to: to
    )
    @target.encrypt!
  end

  describe "#save" do
    it "saves the target when valid" do
      assert @target.save
    end
  end

  describe "#to_s" do
    it "displays a string representation of the target" do
      assert @target.to_s.is_a?(String)
    end

    it "includes the sid" do
      assert_match(/sid: [a-zA-Z0-9]+/, @target.to_s)
    end

    it "includes To:" do
      assert_match(/To: /, @target.to_s)
    end

    it "includes From:" do
      assert_match(/From: /, @target.to_s)
    end
  end

  describe "#send_message" do
    let(:event) { events(:create_external_for_minimum_ticket_1) }
    let(:message_body) { "Test message from Twilio" }
    let(:params) do
      { "From" => from, "To" => to, "Body" => message_body }
    end
    let(:mock_response_json) do
      {
        'sid' => 'SM12345678123456781234567812345678',
        'date_created' => 'Thu, 17 Dec 2020 16:20:30 +0000',
        'date_updated' => 'Thu, 17 Dec 2020 16:20:30 +0000',
        'date_sent' => nil,
        'account_sid' => account_sid,
        'to' => to,
        'from' => from,
        'messaging_service_sid' => nil,
        'body' => message_body,
        'error_code' => nil
      }.to_json
    end

    describe "when successful" do
      let(:success) do
        { status: 200, body: mock_response_json, headers: {} }
      end

      before do
        stub_request(:post, "https://api.twilio.com/2010-04-01/Accounts/#{account_sid}/Messages.json").
          with(body: params).
          to_return(success)
      end

      it "initiates the SMS message" do
        @target.send_message(message_body, event)
      end

      it "truncates the message_body to 1600 characters" do
        message_body.expects(:truncate).with(1600).returns(message_body)
        @target.send_message(message_body, event)
      end

      it "returns an instance of Twilio::REST::Message" do
        resp = @target.send_message(message_body, event)
        assert_equal(::Twilio::REST::Message, resp.class)
      end
    end

    describe "when Twilio returns an error" do
      let(:error) { ::Twilio::REST::RequestError.new(message: "Bad Request") }
      let(:response) do
        { status: 401, body: error.to_json, headers: {} }
      end

      before do
        stub_request(:post, "https://api.twilio.com/2010-04-01/Accounts/#{account_sid}/Messages.json").
          with(body: params).
          to_return(response)
      end

      it "raises a TwilioException" do
        assert_raises(TwilioTarget::TwilioException) { @target.send_message(message_body, event) }
      end
    end
  end
end
