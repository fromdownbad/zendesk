require_relative "../../support/test_helper"

SingleCov.covered!

describe UrlTargetFailure do
  require 'raw_net_capture'

  fixtures :accounts, :tickets, :events, :targets

  let(:account) { accounts(:minimum) }
  let(:event) { events(:create_external_for_minimum_ticket_1) }
  let(:ticket) { event.ticket }
  let(:target) { targets(:url_v2) }

  let(:url_target_failure) { new_url_target_failure }

  describe "new url target failure" do
    describe "when properly initialized and valid" do
      it "creates a new url target failure object" do
        assert url_target_failure.save
      end
    end

    describe "when the target has been deleted before new failure is saved" do
      it "does not persist a failure object" do
        failure = url_target_failure
        target.destroy
        refute failure.save
      end
    end
  end

  describe "keep only #{UrlTargetFailure::URL_TARGET_FAILURES_LIMIT} failures per target" do
    before do
      build_and_save_url_target_failures
      url_target_failure
    end

    it "removes failures" do
      url_target_failure.save
      assert_equal UrlTargetFailure::URL_TARGET_FAILURES_LIMIT, target.url_target_failures.reload.count
    end
  end

  private

  def new_url_target_failure
    UrlTargetFailure.new do |new_fail|
      new_fail.account_id = accounts(:minimum).id
      new_fail.event = event
      new_fail.exception = Exception.to_s
      new_fail.message = "message"
      new_fail.target = target
      new_fail.consecutive_failure_count = 5
      new_fail.raw_http_capture = target.raw_http_capture
    end
  end

  def build_and_save_url_target_failures
    UrlTargetFailure::URL_TARGET_FAILURES_LIMIT.times do
      new_url_target_failure.save
    end
  end
end
