require_relative "../../support/test_helper"
require 'johnny_five'

SingleCov.covered!

describe UrlTarget do
  fixtures :accounts, :tickets, :users, :events

  let(:exception) { Exception.new("Message") }

  before do
    @account = accounts(:minimum)
    @event   = events(:create_external_for_minimum_ticket_1)
    @ticket  = @event.ticket
    @user    = @event.author
    @target  = UrlTarget.new(title: "Url Target", account: @account, url: "http://www.example.com/a?foo=bar", attribute: "message", method: "GET")
  end

  describe "internal recon with private url" do
    ['127.0.0.1', '127.0.0.10', '10.0.1.1', '192.168.0.1'].each do |ip|
      it "is invalid for #{ip}" do
        @target.url = ip
        refute @target.save
      end

      ["http://", "https://"].each do |scheme|
        it "is invalid for #{ip} if you add #{scheme} in front too" do
          @target.url = scheme + ip
          refute @target.save
        end

        it "is not invalid when it's in development environment" do
          Rails.env.stubs(:development?).returns(true)
          @target.url = scheme + ip
          assert @target.save
        end
      end

      it "is not invalid if it's a parameter" do
        @target.url = @target.url + "&ip=#{ip}"
        assert @target.save
      end
    end

    it 'is invalid when using internal fqdn' do
      @target.url = "http://app27.pod1.ord.zdsys.com"
      refute @target.save
    end

    it "is invalid for ftp" do
      @target.url = "ftp://56.1.2.3"
      refute @target.save
    end
  end

  describe "save" do
    it "saves the target when valid" do
      @target.method = "GET"
      assert @target.save!
    end

    it "downcases the method" do
      @target.method = "GET"
      @target.save!
      assert_equal "get", @target.tap(&:save!).method
      refute_equal "GET", @target.tap(&:save!).method
    end

    describe "with a nil method" do
      before { @target.method = nil }

      it "does not blow up for nil target" do
        @target.save!
      end

      it "sets target as a get if method is nil" do
        @target.save!
        assert_equal "get", @target.reload.method
      end
    end
  end

  describe "validation" do
    it "is valid if title and settings(url, attribute) are provided" do
      assert @target.valid?
    end

    it "is not valid if title is missing" do
      @target.title = nil
      refute @target.valid?
      assert @target.errors[:title].any?
    end

    it "is not valid if it fails to resolve" do
      Zendesk::Net::AddressUtil.expects(:safe_url?).raises(Resolv::ResolvTimeout)
      refute @target.valid?
      assert @target.errors[:url].any?
    end

    it "is not valid if it fails to parse" do
      Zendesk::Net::AddressUtil.expects(:safe_url?).returns(true)
      URI.expects(:parse).raises(URI::InvalidURIError)
      refute @target.valid?
      assert @target.errors[:url].any?
    end

    it "turns spaces into + for customer convenience" do
      @target.url = "http://moo.com/?message=This is Bob"
      assert @target.valid?
      assert_equal "http://moo.com/?message=This+is+Bob", @target.url
    end

    describe "settings" do
      it "is not be valid if url is missing" do
        @target.settings.delete(:url)
        refute @target.valid?
        assert @target.errors[:url].any?
      end

      it "is not be valid if attribute is missing" do
        @target.settings.delete(:attribute)
        refute @target.valid?
        assert @target.errors[:attribute].any?
      end

      it "is not be valid if URL is invalid" do
        ['http://invalid', "http://71.193.151.a/partner_system/zen_desk.php"].each do |u|
          @target.settings[:url] = u
          refute @target.valid?
          assert @target.errors[:url].any?
        end
      end

      it "is valid if the URL is valid" do
        ['http://foo.com', "http://71.193.151.1/meep/"].each do |u|
          @target.settings[:url] = u
          assert @target.valid?
          assert_equal [], @target.errors[:url]
        end
      end
    end
  end

  describe "#to_s" do
    it "prints out a simple string detailing the url and method" do
      assert_equal "UrlTarget, URL: #{@target.url}, method: #{@target.method}", @target.to_s
    end
  end

  describe "#send_message" do
    let(:statsd_client) { stub_for_statsd }

    before do
      @message = 'Test message from Zendesk'
      UrlTarget.any_instance.stubs(:statsd_client).returns(statsd_client)
    end

    describe "GET" do
      before do
        @target.settings[:method] = 'get'
        @the_url = "#{@target.url}&#{@target.attribute}=#{URI.encode(@message)}"
      end

      describe "without basic auth" do
        before do
          stub_request(:get, @the_url)
        end

        it "sends a get request with the right parameters" do
          @target.send_message(@message, @event)
          assert_requested(:get, @the_url)
        end

        it "does not use basic authentication if the target has no username and password" do
          Faraday::Connection.any_instance.expects(:basic_auth).never
          @target.stubs(:username).returns("")
          @target.stubs(:password).returns("")
          @target.send_message(@message, @event)
        end
      end

      describe "with basic auth" do
        before do
          @target.stubs(:username).returns("username")
          @target.stubs(:password).returns("123456")
          stub_request(:get, @the_url).with(basic_auth: ['username', '123456'])
        end

        it "use basic authentication if the target has username and password" do
          statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
          @target.send_message(@message, @event)
          assert_requested(:get, @the_url, times: 1)
        end
      end
    end

    describe "POST" do
      before do
        @target.settings[:method] = 'post'
        stub_request(:post, "http://www.example.com/a")
      end

      it "send a post request with the right parameters" do
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
        @target.send_message(@message, @event)
        assert_requested(:post, "http://www.example.com/a", body: {
          foo: "bar",
          message: @message
        }, times: 1)
      end
    end

    describe "PUT" do
      before do
        @target.settings[:method] = 'put'
        stub_request(:put, "http://www.example.com/a")
      end

      it "sends a put request with the right parameters" do
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
        @target.send_message(@message, @event)
        assert_requested(:put, "http://www.example.com/a", body: {
          foo: "bar",
          message: @message
        }, times: 1)
      end
    end

    describe 'a POST to a URL Target with placeholders' do
      before do
        @target.settings[:method] = 'post'
        @target.settings[:url] = 'http://www.example.com/a?comment={{ticket.latest_public_comment_formatted}}'
        @latest_comment        = 'Did you go to A&M? I did.'
        comment                = Comment.new(body: @latest_comment, author: User.new(name: "Pablo"), created_at: Time.now, account: Account.new)
        comment.stubs(:trusted?).returns true
        @ticket.expects(:latest_public_comment).returns(comment)
        stub_request(:post, "http://www.example.com/a")
      end

      it 'escapes the values before inserting them into the template' do
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
        @target.send_message(@message, @event)
        assert_requested(:post, "http://www.example.com/a", body: /#{Regexp.escape(Rack::Utils.escape(@latest_comment))}/)
      end
    end

    describe 'a PUT to a URL Target with placeholders' do
      before do
        @target.settings[:method] = 'put'
        @target.settings[:url] = 'http://www.example.com/a?comment={{ticket.latest_public_comment_formatted}}'
        @latest_comment        = 'Did you go to A&M? I did.'
        comment                = Comment.new(body: @latest_comment, author: User.new(name: "Pablo"), created_at: Time.now, account: Account.new)
        comment.stubs(:trusted?).returns true
        @ticket.expects(:latest_public_comment).returns(comment)
        stub_request(:put, "http://www.example.com/a")
      end

      it 'escapes the values before inserting them into the template' do
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
        @target.send_message(@message, @event)
        assert_requested(:put, "http://www.example.com/a", body: /#{Regexp.escape(Rack::Utils.escape(@latest_comment))}/)
      end
    end

    [["200", "OK"], ["201", "Created"], ["202", "Accepted"],
     ["203", "NonAuthoritativeInformation"], ["204", "NoContent"],
     ["205", "ResetContent"], ["206", "PartialContent"]].each do |status|
      describe "A #{status.inspect} response" do
        before do
          @target.settings[:method] = 'get'
          @the_url = "#{@target.url}&#{@target.attribute}=#{URI.encode(@message)}"
          stub_request(:get, @the_url).to_return(status: status)
          @status = status[0]
        end

        it "is accepted as a valid one" do
          statsd_client.expects(:increment).with("response.status_code", tags: %W[source:url_target status_code:#{@status}]).once
          @target.send_message(@message, @event)
          assert_requested(:get, @the_url)
        end
      end
    end

    describe "A Moved Permanently response" do
      before do
        @target.settings[:method] = 'get'
        @the_url = "#{@target.url}&#{@target.attribute}=#{URI.encode(@message)}"
        @new_url = "#{@target.url}&#{@target.attribute}=#{URI.encode(@message)}&more"

        stub_request(:get, @the_url).to_return(status: 301, headers: {location: @new_url})
        stub_request(:get, @new_url).to_return(status: 200)
      end

      it "follows the redirect and returns proper response" do
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
        @target.send_message(@message, @event)
        assert_requested(:get, @the_url)
      end
    end

    describe "A Not Found response" do
      before do
        @target.settings[:method] = 'get'
        the_url = "#{@target.url}&#{@target.attribute}=#{URI.encode(@message)}"
        stub_request(:get, the_url).to_return(status: 404)
      end

      it "is not accepted as a valid one and raises an exception" do
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:404]).once
        error = assert_raises(HTTPResponseStatusError) { @target.send_message(@message, @event) }
        assert_match "HTTP client call failed", error.message
      end
    end

    describe "encoding" do
      before do
        @account = accounts(:minimum)
        @event   = events(:create_external_for_minimum_ticket_1)
        @ticket  = @event.ticket
        @user    = @event.author
        @target  = UrlTarget.new(title: "Url Target", account: @account, url: "http://www.example.com/a?foo=bar", attribute: "message")
        @message = "Test message"
      end

      describe "GET" do
        before do
          @target.settings[:method] = "get"
          stub_request(:get, /http:\/\/www.example.com\/a/)
        end

        it "does not encode given URL parameters" do
          @target.url  = "http://www.example.com/a?foo=a+b&bar=d:e&boo=%2Ftickets%2F{{ticket.id}}"
          expected_url = "http://www.example.com/a?foo=a%20b&bar=d%3Ae&boo=%2Ftickets%2F#{@ticket.nice_id}&#{@target.attribute}=#{URI.encode(@message)}"
          statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
          @target.send_message(@message, @event)

          assert_requested(:get, expected_url)
        end

        it "encodes the message content and parameters" do
          @target.url  = "http://www.example.com/a?foo=a+b&x=y%20z&bar=d:e"
          expected_url = "http://www.example.com/a?x=y%20z&foo=a%20b&bar=d%3Ae&message=a%3Ab"
          statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
          @target.send_message("a:b", @event)

          assert_requested(:get, expected_url)
        end

        it "overrides params in URL with target attribute" do
          @target.url  = "http://www.example.com/a?#{@target.attribute}=bar"
          expected_url = "http://www.example.com/a?#{@target.attribute}=foo"
          statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
          @target.send_message("foo", @event)

          assert_requested(:get, expected_url)
        end
      end

      describe "POST" do
        before do
          @target.settings[:method] = "post"
          stub_request(:post, "http://www.example.com/a")
        end

        it "does not encode given URL parameters" do
          @target.url = "http://www.example.com/a?foo=a+b&bar=d:e&boo=%2Ftickets%2F{{ticket.id}}"
          statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
          @target.send_message(@message, @event)

          assert_requested(:post, "http://www.example.com/a", body: {
            bar: "d:e",
            foo: "a b",
            boo: "/tickets/#{@event.ticket.nice_id}",
            message: @message
          })
        end

        it "encodes the message content" do
          @target.url = "http://www.example.com/a?foo=a+b&bar=d:e"
          statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
          @target.send_message("a:b", @event)

          assert_requested(:post, "http://www.example.com/a", body: {
            :bar => "d:e",
            :foo => "a b",
            @target.attribute => "a:b"
          })
        end

        it "NOTs override params in URL with target attribute" do
          @target.url = "http://www.example.com/a?#{@target.attribute}=bar"
          statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target status_code:200]).once
          @target.send_message("foo", @event)

          assert_requested(:post, "http://www.example.com/a", body: {
            @target.attribute => "bar"
          })
        end
      end
    end
  end

  describe "#kind" do
    it "returns the URL target string" do
      I18n.expects(:t).with("txt.admin.models.targets.url_target.URL_target")
      @target.kind
    end
  end

  describe "#message_failed" do
    before do
      @account = accounts(:with_ticket_form)
      @failed_target = UrlTarget.new(title: "Failed Url Target", account: @account, url: "http://www.example.com/a?foo=bar", attribute: "message", method: "GET")

      Arturo.enable_feature!(:target_failure)
      @failed_target.save
    end

    after do
      Rails.cache.delete("targets/failure_counter/v1/#{@failed_target.id}")
    end

    describe "toggle build target failure" do
      it "builds a TargetFailure" do
        Arturo.enable_feature!(:target_failure)

        assert_difference("TargetFailure.count", 1) do
          @failed_target.message_failed(exception, @event)
        end

        target_failure_count = @account.target_failures.count
        assert_equal 1, target_failure_count
      end

      it "doesn't build TargetFailure if Arturo is off" do
        Arturo.disable_feature!(:target_failure)

        @failed_target.message_failed(exception, @event)
        assert_equal @account.target_failures.count, @account.target_failures.reload.count

        target_failure_count = @account.target_failures.count
        assert_equal 0, target_failure_count
      end
    end

    describe "target failure without bulk update" do
      it "should create target failure with failure count directly" do
        target_failure_count = @failed_target.failures
        assert_equal 0, target_failure_count

        Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(false)
        assert_difference("TargetFailure.count", 1) do
          @failed_target.message_failed(exception, @event)
        end

        latest_failure = TargetFailure.where(target: @failed_target).last(1)[0]
        assert_equal 1, latest_failure.consecutive_failure_count

        target_failure_count = @failed_target.failures
        assert_equal 1, target_failure_count
      end

      it "should create target failure with same failure count directly and circuit tripped as failure reason" do
        target_failure_count = @failed_target.failures
        assert_equal 0, target_failure_count

        Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(false)
        assert_difference("TargetFailure.count", 1) do
          @failed_target.message_failed(::JohnnyFive::CircuitTrippedError.new, @event)
        end

        latest_failure = TargetFailure.where(target: @failed_target).last(1)[0]
        assert_equal 0, latest_failure.consecutive_failure_count

        target_failure_count = @failed_target.failures
        assert_equal 0, target_failure_count
      end
    end

    describe "target failure with bulk update and rails cache" do
      it "should create target failure with failure count with existing failure count in cache" do
        target_failure_count = @failed_target.failures
        assert_equal 0, target_failure_count

        Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(true)
        Rails.cache.write("targets/failure_counter/v1/#{@failed_target.id}", 7, raw: true)

        assert_difference("TargetFailure.count", 1) do
          @failed_target.message_failed(exception, @event)
        end

        latest_failure = TargetFailure.where(target: @failed_target).last(1)[0]
        assert_equal 8, latest_failure.consecutive_failure_count

        target_failure_count = @failed_target.failures
        # I did not persist cache value back yet... so this is fine
        assert_equal 0, target_failure_count
      end

      it "should create target failure with same failure count with existing failure count in cache and circuit tripped as failure reason" do
        target_failure_count = @failed_target.failures
        assert_equal 0, target_failure_count

        Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(true)
        Rails.cache.write("targets/failure_counter/v1/#{@failed_target.id}", 7, raw: true)

        assert_difference("TargetFailure.count", 1) do
          @failed_target.message_failed(::JohnnyFive::CircuitTrippedError.new, @event)
        end

        latest_failure = TargetFailure.where(target: @failed_target).last(1)[0]
        assert_equal 7, latest_failure.consecutive_failure_count

        target_failure_count = @failed_target.failures
        # I did not persist cache value back yet... so this is fine
        assert_equal 0, target_failure_count
      end
    end
  end
end
