require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 21

describe 'BasecampTarget' do
  fixtures :accounts, :events, :tickets

  before do
    @target = BasecampTarget.new(
      title: "Basecamp Target",
      account: accounts(:minimum),
      token: 'the_token',
      url: "http://subdomain.basecamphq.com",
      project_id: '123456',
      resource: "message"
    )
    @target.encrypt!
  end

  describe "save" do
    it "saves the target when valid" do
      assert @target.save
    end
  end

  describe "validation" do
    it "is valid if title and settings(token, url, project_id) are provided" do
      assert @target.valid?
    end

    it "does not be valid if title is missing" do
      @target.title = nil
      refute @target.valid?
      assert @target.errors[:title].any?
    end

    it "is not valid if token is missing" do
      @target.token = nil
      refute @target.valid?
      assert @target.errors[:token].any?
    end

    describe "settings" do
      it "does not be valid if resource is missing" do
        @target.settings.delete(:resource)
        refute @target.valid?
        assert @target.errors[:resource].any?
      end

      it "does not be valid if resource is invalid" do
        @target.settings[:resource] = "foo"
        refute @target.valid?
        assert @target.errors[:resource].any?
      end

      it "does not be valid if url is missing" do
        @target.settings.delete(:url)
        refute @target.valid?
        assert @target.errors[:url].any?
      end

      it "does not be valid if url is invalid" do
        @target.settings[:url] = 'http://'
        refute @target.valid?
        assert @target.errors[:url].any?
      end

      it "does not be valid if project_id is missing" do
        @target.settings.delete(:project_id)
        refute @target.valid?
        assert @target.errors[:project_id].any?
      end
    end
  end

  describe "#to_s" do
    it "prints out a simple string detailing the url, project_id and token" do
      assert_equal "BasecampTarget, url: #{@target.url}, token: #{@target.token}, project: #{@target.project_id}", @target.to_s
    end
  end

  describe "#is_for_messages?" do
    describe "with message as resource" do
      before { @target.settings[:resource] = 'message' }
      it "returns true" do
        assert @target.is_for_messages?
      end
    end

    describe "with todo as resource" do
      before { @target.settings[:resource] = 'todo' }
      it "returns false" do
        refute @target.is_for_messages?
      end
    end
  end

  describe "#is_test_supported?" do
    describe "with blank message_id" do
      before { @target.settings.merge!(resource: 'message', message_id: '') }
      it "returns true" do
        assert @target.is_test_supported?
      end
    end

    describe "without placeholder in message_id" do
      before { @target.settings.merge!(resource: 'message', message_id: '123456') }
      it "returns true" do
        assert @target.is_test_supported?
      end
    end

    describe "with placeholder in message_id" do
      before { @target.settings.merge!(resource: 'message', message_id: '{{ticket.external_id}}') }
      it "returns false" do
        refute @target.is_test_supported?
      end
    end

    describe "with blank todo_list_id" do
      before { @target.settings.merge!(resource: 'todo', todo_list_id: '') }
      it "returns true" do
        assert @target.is_test_supported?
      end
    end

    describe "without placeholder in todo_list_id" do
      before { @target.settings.merge!(resource: 'todo', todo_list_id: '123456') }
      it "returns true" do
        assert @target.is_test_supported?
      end
    end

    describe "with placeholder in todo_list_id" do
      before { @target.settings.merge!(resource: 'todo', todo_list_id: '{{ticket.external_id}}') }
      it "returns false" do
        refute @target.is_test_supported?
      end
    end
  end

  describe "#send_message" do
    before do
      @account = accounts(:minimum)
      # @ticket = tickets(:minimum_1)
      # @ticket.external_id = '222'
      # @user = users(:minimum_agent)
      @event = events(:create_external_for_minimum_ticket_1)
      @ticket = @event.ticket
      @ticket.external_id = '222'
      @user = @event.author

      @message = 'Testing...'

      @message = Basecamp::Message.new
      @message.stubs(:save).returns(true)
      Basecamp::Message.stubs(:new).returns(@message)

      @comment = Basecamp::Comment.new
      @comment.stubs(:save).returns(true)
      Basecamp::Comment.stubs(:new).returns(@comment)

      @todo_list = Basecamp::TodoList.new
      @todo_list.stubs(:save).returns(true)
      Basecamp::TodoList.stubs(:new).returns(@todo_list)

      @todo_item = Basecamp::TodoItem.new
      @todo_item.stubs(:save).returns(true)
      Basecamp::TodoItem.stubs(:new).returns(@todo_item)
    end

    describe "with a blank message" do
      it "raises an exception" do
        assert_raises(RuntimeError) { @target.send_message('', @event) }
        assert_raises(RuntimeError) { @target.send_message(nil, @event) }
      end
    end

    describe "with http url" do
      it "establishs a http connection" do
        Basecamp.expects(:establish_connection!).with(URI.parse(@target.url).host, @target.token, 'x', false)
        @target.send_message(@message, @event)
      end
    end

    describe "with https url" do
      before { @target.settings[:url] = 'https://subdomain.basecamphq.com' }
      it "establishs a https connection" do
        Basecamp.expects(:establish_connection!).with(URI.parse(@target.url).host, @target.token, 'x', true)
        @target.send_message(@message, @event)
      end
    end

    describe "with 'message' as resource" do
      before { @target.settings.merge!(resource: 'message', message_id: '') }

      it "creates a message" do
        Basecamp::Message.expects(:new).with(project_id: @target.project_id).returns(@message)
        @message.expects(:title=)
        @message.expects(:body=).with(@message)
        @message.expects(:save).returns(true)
        @target.send_message(@message, @event)
      end

      describe "and ticket with subject" do
        it "creates a message with the subject" do
          @message.expects(:title=).with(@ticket.subject)
          @target.send_message(@message, @event)
        end
      end

      describe "and ticket without subject" do
        before { @ticket.subject = '' }
        it "creates a message with the description" do
          @message.expects(:title=).with(@ticket.description)
          @target.send_message(@message, @event)
        end
      end
    end

    describe "with message_id" do
      before { @target.settings.merge!(resource: 'message', message_id: '123456') }

      it "creates a comment" do
        Basecamp::Comment.expects(:new).with(post_id: @target.message_id).returns(@comment)
        @comment.expects(:body=).with(@message)
        @comment.expects(:save).returns(true)
        @target.send_message(@message, @event)
      end

      describe "and placeholder in message_id" do
        before { @target.settings.merge!(resource: 'message', message_id: '{{ticket.external_id}}') }
        it "uses external_id as message_id" do
          Basecamp::Comment.expects(:new).with(post_id: @ticket.external_id).returns(@comment)
          @target.send_message(@message, @event)
        end
      end
    end

    describe "with 'todo' as resource" do
      before { @target.settings.merge!(resource: 'todo') }

      it "creates a todo list" do
        Basecamp::TodoList.expects(:new).with(project_id: @target.project_id).returns(@todo_list)
        @todo_list.expects(:name=)
        @todo_list.expects(:description=).with(@message)
        @todo_list.expects(:save).returns(true)
        @target.send_message(@message, @event)
      end

      describe "and ticket with subject" do
        it "creates a message with the subject" do
          @todo_list.expects(:name=).with(@ticket.subject)
          @target.send_message(@message, @event)
        end
      end

      describe "and ticket without subject" do
        before { @ticket.subject = '' }
        it "creates a message with the description" do
          @todo_list.expects(:name=).with(@ticket.description)
          @target.send_message(@message, @event)
        end
      end
    end

    describe "with todo_list_id" do
      before { @target.settings.merge!(resource: 'todo', todo_list_id: '123456') }

      it "creates a todo item" do
        Basecamp::TodoItem.expects(:new).with(todo_list_id: @target.todo_list_id).returns(@todo_item)
        @todo_item.expects(:content=).with(@message)
        @todo_item.expects(:save).returns(true)
        @target.send_message(@message, @event)
      end

      describe "and placeholder in todo_list_id" do
        before { @target.settings.merge!(resource: 'todo', todo_list_id: '{{ticket.external_id}}') }
        it "uses external_id as todo_list_id" do
          Basecamp::TodoItem.expects(:new).with(todo_list_id: @ticket.external_id).returns(@todo_item)
          @target.send_message(@message, @event)
        end
      end
    end
  end
end
