require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 7

describe 'SugarCrmTarget' do
  fixtures :accounts, :events, :tickets, :users

  before do
    @sugar_integration = SugarCrmIntegration.new
    @sugar_integration.stubs(:configured?).returns(true)
    Account.any_instance.stubs(:sugar_crm_integration).returns(@sugar_integration)
    @target = SugarCrmTarget.new(title: "SugarCRM Target", account: accounts(:minimum))
    @target.current_user = users(:minimum_agent)
  end

  describe "save" do
    it "saves the target when valid" do
      assert @target.save
    end
  end

  describe '#to_s' do
    it 'prints out a simple string' do
      assert_equal("SugarCRMTarget: (id: #{@target.id})", @target.to_s)
    end
  end

  describe "target properties" do
    it "reports properties properly" do
      assert @target.is_test_supported?
    end
  end

  describe "#send_message" do
    before do
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(users(:minimum_agent))
      @external = External.new(via_id: ViaType.WEB_FORM, audit: @ticket.audit)
      @external.ticket = @ticket
      @external.author = users(:minimum_agent)
      @ticket.audit.events << @external
    end

    describe "when there's no configured sugar integration" do
      before do
        @sugar_integration.expects(:create_case).never
        Account.any_instance.stubs(:sugar_crm_integration).returns(nil)
      end
      it "raises an exception" do
        assert_raises(RuntimeError) { @target.send_message("Foo", @external) }
      end
    end

    describe "when there's a configured sugar integration" do
      before do
        @sugar_integration.expects(:create_case).with(@ticket).returns(true)
      end
      it "creates the case using the integration" do
        @target.send_message("Foo", @external)
      end
    end
  end

  describe "#create_integration" do
    describe "creates trigger" do
      it "thats uses the target" do
        assert_difference "users(:minimum_agent).account.triggers.size", 1 do
          @target.create_integration(users(:minimum_agent))
        end

        trigger = users(:minimum_agent).account.triggers.last

        expected = [['status_id', 'value', ['3']], ['update_type', 'is', ['Change']]]

        assert_equal expected, trigger.definition.conditions_all.map { |d| [d.source, d.operator, d.value] }.sort_by { |x| x[0] }

        assert_equal 0, trigger.definition.conditions_any.size
      end
    end
  end
end
