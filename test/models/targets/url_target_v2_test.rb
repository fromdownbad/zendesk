require_relative "../../support/test_helper"
require 'johnny_five'

SingleCov.covered! uncovered: 5

describe UrlTargetV2 do
  fixtures :accounts, :tickets, :users, :events, :translation_locales

  let(:account) { accounts(:minimum) }
  let(:event) { events(:create_external_for_minimum_ticket_1) }
  let(:ticket) { event.ticket }
  let(:target) do
    UrlTargetV2.new(
      title: "Url Target",
      account: accounts(:minimum),
      url: "http://www.example.com/a",
      method: "get"
    )
  end

  describe 'creating a new UrlTargetV2' do
    it 'requires a username if a password is provided' do
      target.password = '123456'
      target.encrypt!

      exception = assert_raise(ActiveRecord::RecordInvalid) do
        target.save!
      end
      assert_equal "Validation failed: Username:  cannot be blank", exception.message
    end

    it 'requires a username if an password is provided' do
      target.password = '123456'
      target.encrypt!

      exception = assert_raise(ActiveRecord::RecordInvalid) { target.save! }
      assert_equal "Validation failed: Username:  cannot be blank", exception.message
    end
  end

  describe '.format' do
    describe 'when the HTTP method is `get`' do
      it 'returns `query`' do
        assert_equal :query, UrlTargetV2.format('application/json', 'get')
      end
    end

    describe 'when the HTTP method is not `get`' do
      describe 'and the content type is `application/json`' do
        it 'returns `json`' do
          assert_equal :json, UrlTargetV2.format('application/json', 'post')
        end
      end

      describe 'and the content type is `application/xml`' do
        it 'returns `xml`' do
          assert_equal :xml, UrlTargetV2.format('application/xml', 'post')
        end
      end

      describe 'and the content type is `application/x-www-form-urlencoded`' do
        it 'returns `form`' do
          assert_equal(
            :form,
            UrlTargetV2.format('application/x-www-form-urlencoded', 'post')
          )
        end
      end

      describe 'and the content type is not supported' do
        it 'returns `none`' do
          assert_equal :none, UrlTargetV2.format('html', 'post')
        end
      end
    end
  end

  describe "internal recon with private url" do
    ['127.0.0.1', '10.0.1.1', '192.168.0.1'].each do |ip|
      before do
        target.url = ip
        target.save
      end

      it "is invalid for #{ip}" do
        assert_includes target.errors.full_messages, "URL:  is invalid"
      end
    end

    it 'is invalid when using internal fqdn' do
      target.url = "http://app27.pod1.ord.zdsys.com"
      target.save
      assert_includes target.errors.full_messages, "URL:  is invalid"
    end
  end

  describe "hitting the api/v2/tickets endpoint" do
    let(:statsd_client) { stub_for_statsd }

    before do
      Account.any_instance.stubs(:has_ticket_target_retry?).returns(true)
      @message = '{"ticket":{"status":"open"}'
      @the_url = "https://example.com/api/v2/tickets/#{ticket.nice_id}.json"
      UrlTargetV2.any_instance.stubs(:statsd_client).returns(statsd_client)
      target.content_type = "application/json"
      target.url = "https://example.com/api/v2/tickets/{{ticket.id}}.json"
      target.method = "put"
      target.save
      target.activate

      @event = External.new(
        via_id: ticket.audits.last.via_id,
        author: ticket.account.owner,
        via_reference_id: 0,
        resource: target.id,
        body: "HTTP URL v2 target",
        account: ticket.account,
        ticket: ticket,
        audit: ticket.audits.last,
        value: target.id
      )
    end

    describe "retries should be attempted" do
      before do
        UrlTargetV2.any_instance.stubs(:calculate_sleep_time).returns(5)
        Resque.inline = true
      end

      after do
        Resque.inline = false
      end

      it "retries more than once" do
        stub_request(:put, @the_url).
          to_return(status: 409).then.
          to_return(status: 409).then.
          to_return(status: 200)

        statsd_client.expects(:histogram).with("retry.backoff_time", 5, tags: %w[source:url_target_v2]).twice
        statsd_client.expects(:increment).with("retry.count", tags: %w[source:url_target_v2]).twice
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:409]).twice
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:200]).once

        # save event will trigger target job which invokes url target v2 send_message_with_retry
        @event.save(validate: false)
        assert_requested(:put, @the_url, times: 3)
      end

      it "retries if a 409 error is seen" do
        stub_request(:put, @the_url).
          to_return(status: 409).then.
          to_return(status: 200)

        statsd_client.expects(:histogram).with("retry.backoff_time", 5, tags: %w[source:url_target_v2]).once
        statsd_client.expects(:increment).with("retry.count", tags: %w[source:url_target_v2]).once
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:409]).once
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:200]).once

        # save event will trigger target job which invokes url target v2 send_message_with_retry
        @event.save(validate: false)

        assert_requested(:put, @the_url, times: 2)
      end

      it "retries if a 429 error is seen and there is a Retry-After" do
        stub_request(:put, @the_url).
          to_return(status: 429, headers: { 'Retry-After' => 0 }).then.
          to_return(status: 200)

        statsd_client.expects(:histogram).with("retry.backoff_time", 5, tags: %w[source:url_target_v2]).once
        statsd_client.expects(:increment).with("retry.count", tags: %w[source:url_target_v2]).once
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:429]).once
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:200]).once

        # save event will trigger target job which invokes url target v2 send_message_with_retry
        @event.save(validate: false)

        assert_requested(:put, @the_url, times: 2)
      end

      it "retries if a 503 error is seen and there is a Retry-After" do
        stub_request(:put, @the_url).
          to_return(status: 503, headers: { 'Retry-After' => 0 }).then.
          to_return(status: 200)

        statsd_client.expects(:histogram).with("retry.backoff_time", 5, tags: %w[source:url_target_v2]).once
        statsd_client.expects(:increment).with("retry.count", tags: %w[source:url_target_v2]).once
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:503]).once
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:200]).once

        # save event will trigger target job which invokes url target v2 send_message_with_retry
        @event.save(validate: false)

        assert_requested(:put, @the_url, times: 2)
      end
    end

    describe "retries should not be attempted" do
      it "does not retry if the arturo ticket_target_retry is not enabled" do
        Account.any_instance.stubs(:has_ticket_target_retry?).returns(false)
        stub_request(:put, @the_url).
          to_return(status: 409).then.
          to_return(status: 200)

        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:409]).once
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:200]).never
        assert_raise HTTPResponseStatusError do
          target.send_message_with_retry(@message, event)
        end
        assert_requested(:put, @the_url, times: 1)
        TargetJob.expects(:enqueue_in).never
      end

      it "only retries on a 429 if there is a retry_after header" do
        stub_request(:put, @the_url).
          to_return(status: 429).then.
          to_return(status: 200)

        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:429]).once
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:200]).never
        assert_raise HTTPResponseStatusError do
          target.send_message_with_retry(@message, event)
        end
        assert_requested(:put, @the_url, times: 1)
        TargetJob.expects(:enqueue_in).never
      end

      it "does not retry on an error 500" do
        stub_request(:put, @the_url).
          to_return(status: 500).then.
          to_return(status: 200)

        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:500]).once
        statsd_client.expects(:increment).with("response.status_code", tags: %w[source:url_target_v2 status_code:200]).never
        assert_raise HTTPResponseStatusError do
          target.send_message_with_retry(@message, event)
        end
        assert_requested(:put, @the_url, times: 1)
        TargetJob.expects(:enqueue_in).never
      end
    end

    describe "when retrying after failure" do
      before do
        Resque.inline = true
      end

      after do
        Resque.inline = false
      end

      it "it won't exceed the maximum number of retries" do
        upper_limit = 1 + UrlTargetV2.new.default_max_retries_threshold
        stub_request(:put, @the_url).
          to_return(status: 409).times(upper_limit * 2)

        @event.save(validate: false)

        assert_requested(:put, @the_url, times: upper_limit) # initial + retries
      end
    end
  end

  describe "#kind" do
    it "uses the right i18n key" do
      I18n.expects(:t).with('txt.admin.models.targets.url_target.HTTP_target')
      target.kind
    end
  end

  describe "#render_message" do
    let(:ticket) { tickets(:minimum_1) }
    let(:external) do
      External.new(
        account: ticket.account,
        ticket: ticket,
        audit: ticket.audits.last,
        via_id: ticket.audits.last.via_id,
        author: ticket.account.owner,
        value: target.id,
        body: body
      )
    end

    describe "the body is not nil" do
      let(:body) { [["this", "{{ticket.description}}"]] }

      describe "and the target uses parameters" do
        before do
          external.target = target
          target.stubs(uses_parameters?: true)
        end

        it "returns an array of rendered parameters" do
          assert_equal [["this", "----------------------------------------------\n\nAgent Minimum, Jan 1, 2007, 5:32\n\nminimum ticket 1"]],
            target.render_message(external)
        end
      end

      describe "and the target does not use parameters" do
        let(:body) { "The subject is {{ticket.description}}" }

        before { target.stubs(uses_parameters?: false) }

        it "returns an array of rendered parameters" do
          assert_equal "The subject is ----------------------------------------------\n\nAgent Minimum, Jan 1, 2007, 5:32\n\nminimum ticket 1",
            target.render_message(external)
        end
      end
    end

    describe "and the body is nil (no parameters)" do
      let(:body) { nil }

      it "returns nil" do
        assert_nil target.render_message(external)
      end
    end

    describe "when the content-type is json" do
      before do
        target.content_type = "application/json"
      end

      describe "and the body is not nil" do
        let(:body) { %({"description":"{{ticket.description}}"}) }

        it "should return json escaped rendered placeholders" do
          expected_result = "{\"description\":\"----------------------------------------------\\n\\nAgent Minimum, Jan 1, 2007, 5:32\\n\\nminimum ticket 1\"}"
          assert_equal expected_result, target.render_message(external)
        end
      end

      describe "and the body is nil (no parameters)" do
        let(:body) { nil }

        it "returns nil" do
          assert_nil target.render_message(external)
        end
      end
    end
  end

  describe "#send_message_with_retry" do
    describe "GET" do
      before do
        target.method = 'get'
        @message = [["ping", "pong"]]
        @the_url = "#{target.url}?ping=pong"
      end

      describe "without basic auth" do
        before do
          stub_request(:get, @the_url)
        end

        it "sends a get request with the right parameters" do
          target.send_message_with_retry(@message, event)
          assert_requested(:get, @the_url)
        end

        it "not use basic authentication if the target has no username" do
          Faraday::Connection.any_instance.expects(:basic_auth).never
          target.username = ''
          target.password = 'asdasd'
          target.encrypt!
          target.send_message_with_retry(@message, event)
        end
      end

      describe "with basic auth" do
        before do
          target.username = "username"
          target.password = '123456'
          target.encrypt!
          stub_request(:get, @the_url).with(basic_auth: ['username', '123456'])
        end

        it "uses basic authentication if the target has username and password" do
          target.send_message_with_retry(@message, event)
          assert_requested(:get, @the_url)
        end
      end
    end

    describe "POST" do
      before do
        target.method = 'post'
        @message = 'Test message from Zendesk'
        stub_request(:post, "http://www.example.com/a")
      end

      it "sends a post request with a raw message body" do
        target.send_message_with_retry(@message, event)
        assert_requested(:post, "http://www.example.com/a", body: @message)
      end
    end

    describe "PUT" do
      before do
        target.method = 'put'
        @message = 'Test message from Zendesk'
        stub_request(:put, "http://www.example.com/a")
      end

      it "sends a put request with the raw message body" do
        target.send_message_with_retry(@message, event)
        assert_requested(:put, "http://www.example.com/a", body: @message)
      end
    end

    describe "DELETE" do
      before do
        target.method = 'delete'
        @message = 'Test message from Zendesk'
        stub_request(:delete, "http://www.example.com/a")
      end

      it "sends a delete request with the raw message body" do
        target.send_message_with_retry(@message, event)
        assert_requested(:delete, "http://www.example.com/a", body: @message)
      end
    end

    [
      ["200", "OK"], ["201", "Created"], ["202", "Accepted"], ["203", "NonAuthoritativeInformation"],
      ["204", "NoContent"], ["205", "ResetContent"], ["206", "PartialContent"]
    ].each do |status|
      describe "A #{status.inspect} response" do
        before do
          target.method = 'get'
          @message = [["ping", "pong"]]
          @the_url = "#{target.url}?ping=pong"
          stub_request(:get, @the_url).to_return(status: status)
        end

        it "is accepted as a valid one" do
          target.send_message_with_retry(@message, event)
          assert_requested(:get, @the_url)
        end
      end
    end

    describe "A Moved Permanently response" do
      before do
        target.method = 'get'
        @message = [["ping", "pong"]]
        @the_url = "#{target.url}?ping=pong"
        @new_url = "#{target.url}?ping=pong&more"

        stub_request(:get, @the_url).to_return(status: 301, headers: {location: @new_url})
        stub_request(:get, @new_url).to_return(status: 200)
      end

      it "follows redirect and return proper response" do
        target.send_message_with_retry(@message, event)
        assert_requested(:get, @the_url)
        assert_requested(:get, @new_url)
      end
    end

    describe "A Moved Permanently response to a attacker target" do
      before do
        target.method = 'get'
        @message = [["ping", "pong"]]
        @the_url = "#{target.url}?ping=pong"
        @new_url = "http://0.0.0.0"

        stub_request(:get, @the_url).to_return(status: 301, headers: {location: @new_url})
        stub_request(:get, @new_url).to_return(status: 200)
      end

      it "doesn't follow the redirect" do
        assert_raises Faraday::RestrictIPAddresses::AddressNotAllowed do
          target.send_message_with_retry(@message, event)
          assert_requested(:get, @the_url)
        end
      end
    end

    describe "A Not Found response" do
      before do
        target.method = 'get'
        @message = [["ping", "pong"]]

        the_url = "#{target.url}?ping=pong"
        stub_request(:get, the_url).to_return(status: 404)
      end

      it "is not be accepted as a valid one and raise an exception" do
        assert_raises(HTTPResponseStatusError) { target.send_message_with_retry(@message, event) }
      end
    end
  end

  describe "#fix_content_type" do
    before { target.content_type = "application/json" }

    describe "when the method is get or delete" do
      before { target.method = "get"  }

      it "changes the content_type to an empty string" do
        assert_equal "", target.tap(&:valid?).content_type
      end
    end

    describe "when the method is not get or delete" do
      before { target.method = "post" }

      it "does not change the content_type" do
        assert_equal "application/json", target.tap(&:valid?).content_type
      end
    end
  end

  describe "#is_message_supported?" do
    describe "when the method is delete" do
      before { target.method = "delete" }

      it "returns true" do
        assert target.is_message_supported?
      end
    end

    describe "when the method is not delete" do
      before { target.method = "post" }

      it "returns false" do
        assert target.is_message_supported?
      end
    end
  end

  describe "#needs_content_type?" do
    ["post", "put", "patch"].each do |method|
      describe "when the method is #{method}" do
        before { target.method = method }

        it "returns true" do
          assert target.needs_content_type?
        end
      end
    end

    describe "when the method is get" do
      before { target.method = 'get' }

      it "returns false" do
        refute target.needs_content_type?
      end
    end
  end

  describe "#uses_parameters?" do
    describe "when the method is get" do
      it "returns true" do
        assert target.uses_parameters?
      end
    end

    describe "when the method is not get" do
      before { target.method = "post" }

      describe "when the content-type is not application/x-www-form-urlencoded" do
        before { target.content_type = "application/json" }

        it "returns false" do
          refute target.uses_parameters?
        end
      end

      describe "when the content-type is application/x-www-form-urlencoded" do
        before { target.content_type = "application/x-www-form-urlencoded" }

        it "returns true" do
          assert target.uses_parameters?
        end
      end
    end
  end

  describe "#message_failed" do
    let(:exception) { Exception.new("Message") }

    before do
      target.save
    end

    after do
      Rails.cache.delete("targets/failure_counter/v1/#{target.id}")
    end

    describe "target failure without bulk update" do
      it "should create target failure with failure count directly" do
        target_failure_count = target.failures
        assert_equal 0, target_failure_count

        Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(false)

        assert_difference("target.url_target_failures.count(:all)", 1) do
          target.message_failed(exception, event)
        end

        latest_failure = target.url_target_failures.latest[0]
        assert_equal 1, latest_failure.consecutive_failure_count

        target_failure_count = target.failures
        assert_equal 1, target_failure_count
      end
    end

    describe "target failure without bulk update and circuit tripped as failure reason" do
      it "should create target failure with failure count directly" do
        target_failure_count = target.failures
        assert_equal 0, target_failure_count

        Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(false)

        assert_difference("target.url_target_failures.count(:all)", 1) do
          target.message_failed(::JohnnyFive::CircuitTrippedError.new, event)
        end

        latest_failure = target.url_target_failures.latest[0]
        assert_equal 0, latest_failure.consecutive_failure_count

        target_failure_count = target.failures
        assert_equal 0, target_failure_count
      end
    end

    describe "target failure with bulk update and rails cache" do
      it "should create target failure with failure count with existing failure count in cache" do
        target_failure_count = target.failures
        assert_equal 0, target_failure_count

        Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(true)
        Rails.cache.write("targets/failure_counter/v1/#{target.id}", 7, raw: true)

        assert_difference("target.url_target_failures.count(:all)", 1) do
          target.message_failed(exception, event)
        end

        latest_failure = target.url_target_failures.latest[0]
        assert_equal 8, latest_failure.consecutive_failure_count

        target_failure_count = target.failures
        # I did not persist cache value back yet... so this is fine
        assert_equal 0, target_failure_count
      end
    end

    describe "target failure with bulk update, rails cache and circuit tripped as failure reason" do
      it "should create target failure without bump up total failure count" do
        target_failure_count = target.failures
        assert_equal 0, target_failure_count

        Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(true)
        Rails.cache.write("targets/failure_counter/v1/#{target.id}", 1, raw: true)

        assert_difference("target.url_target_failures.count(:all)", 1) do
          target.message_failed(::JohnnyFive::CircuitTrippedError.new, event)
        end

        latest_failure = target.url_target_failures.latest[0]
        assert_equal 1, latest_failure.consecutive_failure_count

        target_failure_count = target.failures
        # I did not persist cache value back yet... so this is fine
        assert_equal 0, target_failure_count
      end
    end
  end

  describe "#send_test_message" do
    describe "when an exception is not raised" do
      before do
        target.stubs(:send_message_with_retry)
        target.send_test_message("message", event)
      end

      it "builds an http transaction without an exception_message" do
        assert_instance_of RawHTTPCapture::Transaction, target.http_transactions.first
        assert_nil target.exception_message
      end
    end

    describe "when an exception is raised" do
      before do
        target.stubs(:send_message_with_retry).raises(StandardError.new("test message"))
        target.send_test_message("message", event)
      end

      it "builds an http transactions with an exception_message on the target" do
        assert_instance_of RawHTTPCapture::Transaction, target.http_transactions.first
        assert_equal "test message", target.exception_message
      end
    end
  end

  describe "#attribute" do
    it "raises an exception" do
      assert_raise NoMethodError do
        target.attribute
      end
    end
  end

  describe "#attribute=" do
    it "raises an exception" do
      assert_raise NoMethodError do
        target.attribute = "attribute"
      end
    end
  end

  describe "#edit_template" do
    it "returns the right path to the template" do
      assert_equal 'targets/edit/http_target', target.edit_template
    end
  end

  describe "#locale" do
    let(:spanish) { translation_locales(:spanish) }

    before do
      @requester = users(:minimum_end_user)
      @requester.update_column(:locale_id, spanish.id)
      ticket.requester = @requester
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save!
    end

    it "returns the requester locale if requester present" do
      assert_equal spanish, target.send(:requester_locale, ticket)
    end
  end
end
