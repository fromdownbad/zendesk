require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 14

describe 'SalesforceTarget' do
  fixtures :accounts, :events, :tickets, :users, :ticket_metric_sets

  before do
    @target = SalesforceTarget.new(title: "Salesforce Target", account: accounts(:minimum))
    @target.current_user = users(:minimum_agent)
  end

  describe "save" do
    it "saves the target when valid" do
      assert @target.save
    end

    it "retains the token attribute" do
      @target.token = "token"
      @target.encrypt!

      @target.save
      @target.reload
      target = Target.find(@target.id)

      assert_equal("token", target.token)
    end
  end

  it "does not be valid if title is missing" do
    @target.title = nil
    refute @target.valid?
    assert @target.errors[:title].any?
  end

  describe "oauth related" do
    it "responds properly to oauth related methods" do
      assert @target.requires_authorization?
    end
  end

  describe '#to_s' do
    it 'prints out a simple string' do
      assert_equal("SalesforceTarget: (id: #{@target.id})", @target.to_s)
    end
  end

  describe "#authorized?" do
    it "responds properly based on access token and secret" do
      @target.token = "foo"
      @target.secret = "bar"
      @target.encrypt!
      assert @target.authorized?

      @target.token = nil
      @target.secret = nil
      @target.encrypt!
      refute @target.authorized?

      @target.token = ""
      @target.secret = ""
      @target.encrypt!
      refute @target.authorized?
    end
  end

  describe "#send_xml_message" do
    it "constructs XML representation for salesforce" do
      xml = @target.send(:ticket_xml_for_salesforce, tickets(:minimum_1))

      doc = Nokogiri::XML(xml)
      assert_equal("ticket", doc.root.name)
      ["ticket/comments", "ticket/requester", "ticket/requester/name", "ticket/requester-id"].each do |sel|
        assert_not_nil doc.at(sel)
      end
    end

    it "calls SOAP request if authorized" do
      @target.token = "foo"
      @target.secret = "bar"
      @target.encrypt!

      @target.expects(:get_salesforce_session).returns({})
      driver = mock
      driver.expects(:syncTicketData).with(kind_of(Salesforce::Integration::SyncTicketData)).returns(nil)

      Salesforce::Integration::Driver.expects(:get_driver).returns(driver)

      ticket = tickets(:minimum_1)
      ticket.will_be_saved_by(users(:minimum_agent))
      external = External.new(via_id: ViaType.WEB_FORM, audit: ticket.audit)
      external.ticket = ticket
      external.author = users(:minimum_agent)
      ticket.audit.events << external
      @target.send_message("Foo", external)
    end
  end

  describe "target properties" do
    it "reports properties properly" do
      assert @target.requires_authorization?
      refute @target.is_message_supported?
    end
  end

  describe "#get_salesforce_session" do
    before do
      @salesforce_integration = SalesforceIntegration.new
      @salesforce_integration.stubs(:configured?).returns(true)
      Account.any_instance.stubs(:salesforce_integration).returns(@salesforce_integration)
    end

    it "gets session using integration when is a system target and the integration is configured" do
      @target.stubs(:is_system_target?).returns(true)

      @salesforce_integration.expects(:get_salesforce_session)
      @target.expects(:get_session).never

      @target.send(:get_salesforce_session)
    end

    it "gets session using target keys when is not a system target" do
      @target.stubs(:is_system_target?).returns(false)

      @target.expects(:get_session)
      @salesforce_integration.expects(:get_salesforce_session).never

      @target.send(:get_salesforce_session)
    end
  end

  describe "#send_message" do
    before do
      @ticket = tickets(:minimum_1)
      @external = External.new(ticket: @ticket)
      @target.stubs(:authorized?).returns(true)
      @target.stubs(:get_salesforce_session).returns({})
    end

    it "sends xml message if the ticket_information_mode option is set to partial mode" do
      @target.expects(:ticket_information_mode).returns(SalesforceTarget::PARTIAL_MODE)
      @target.expects(:send_xml_message).with({}, @ticket).returns(true)
      @target.send_message("Hello", @external)
    end

    it "sends json message if the ticket_information_mode option is set to full mode" do
      @target.expects(:ticket_information_mode).returns(SalesforceTarget::FULL_MODE)
      @target.expects(:send_json_message).with({}, @ticket).returns(true)
      @target.send_message("Hello", @external)
    end
  end

  describe "#send_json_message" do
    before do
      @salesforce_integration = SalesforceIntegration.new
      @salesforce_integration.stubs(:configured?).returns(true)
      Account.any_instance.stubs(:salesforce_integration).returns(@salesforce_integration)

      @ticket = tickets(:minimum_1)
      @external = External.new(ticket: @ticket)
      @target.stubs(:authorized?).returns(true)
      @target.stubs(:get_salesforce_session).returns({})
    end

    it "uses the salesforce presenter to construct JSON representation and obtain the correct time format" do
      ticket = tickets(:minimum_1)
      # We just need to test the time format here
      # The tests for the content are in the salesforce presenter specific test file
      json = { ticket: { created_at: Time.parse("2012-05-31 19:02:12 UTC") } }
      Api::V2::SalesforcePresenter.any_instance.expects(:present).with(ticket).returns(json)
      result = @target.send(:ticket_json_for_salesforce, ticket)
      expected = '{"ticket":{"created_at":"2012-05-31T19:02:12Z"}}'
      assert_equal expected, result
    end

    it "sends message through the integration on system targets" do
      @target.expects(:ticket_information_mode).returns(SalesforceTarget::FULL_MODE)
      @target.expects(:use_integration_keys?).returns(true)
      @salesforce_integration.expects(:http_post)
      @target.send_message("Hello", @external)
    end
  end

  describe "#create_integration" do
    describe "creates a trigger" do
      before do
        @minimum_agent = users(:minimum_agent)
      end

      it "creates a Trigger that uses the target" do
        assert_difference "users(:minimum_agent).account.triggers.size", 1 do
          @target.create_integration(@minimum_agent)
        end

        trigger = @minimum_agent.account.triggers.last

        expected = [['status_id', 'value', ['3']], ['update_type', 'is', ['Change']]]

        assert_equal expected, trigger.definition.conditions_all.map { |d| [d.source, d.operator, d.value] }.sort_by { |x| x[0] }

        assert_equal 0, trigger.definition.conditions_any.size
      end

      it "creates a trigger that is a system rule" do
        @target.create_integration(@minimum_agent)
        trigger = @minimum_agent.account.triggers.last
        assert trigger.is_system_rule?
      end

      it "does not be a locked rule" do
        @target.create_integration(@minimum_agent)
        trigger = @minimum_agent.account.triggers.last
        refute trigger.is_locked?
      end
    end
  end

  describe "url builder" do
    it "builds urls with the correct account protocol and host name" do
      account = accounts(:minimum)
      ticket = tickets(:minimum_1)

      url_builder = SalesforceTarget::UrlBuilder.new(account)
      assert_equal "https://minimum.zendesk-test.com/api/v2/tickets/#{ticket.nice_id}", url_builder.send("api_v2_ticket_url", ticket)

      account.update_attribute(:host_mapping, "foobar.com")
      url_builder = SalesforceTarget::UrlBuilder.new(account)
      assert_equal "http://foobar.com/api/v2/tickets/#{ticket.nice_id}", url_builder.send("api_v2_ticket_url", ticket)
    end
  end
end
