require_relative "../../support/test_helper"

SingleCov.covered!

describe TargetFailure do
  fixtures :accounts, :tickets, :events, :targets

  let(:account) { accounts(:minimum) }
  let(:event) { events(:create_external_for_minimum_ticket_1) }
  let(:target) { targets(:url_valid) }
  let(:target_failure) { new_target_failure }
  let(:response_body) { 'response body' }

  describe "new target failure" do
    describe "when the target has been deleted before new failure is saved" do
      it "does not persist a failure object" do
        failure = new_target_failure
        target.destroy
        refute failure.save
      end
    end
  end

  describe "keep only #{TargetFailure::TARGET_FAILURES_LIMIT} failures per target" do
    before do
      build_target_failures
      target_failure
    end

    it "removes failures" do
      target_failure.save
      assert_equal TargetFailure::TARGET_FAILURES_LIMIT, target.target_failures.reload.map(&:id).count
    end
  end

  private

  def new_target_failure
    TargetFailure.new(
      account: account,
      event: event,
      exception: Exception.to_s,
      message: "message",
      target: target,
      consecutive_failure_count: 5,
      raw_http_capture: target.raw_http_capture,
      response_body: response_body
    )
  end

  def build_target_failures
    TargetFailure::TARGET_FAILURES_LIMIT.times.each do
      new_target_failure.save
    end
  end
end
