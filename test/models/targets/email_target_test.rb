require_relative "../../support/test_helper"

SingleCov.covered!

describe EmailTarget do
  fixtures :all

  let(:email_target) { targets(:email_valid) }

  describe "#to_s" do
    it "renders the correct string" do
      assert_equal "EmailTarget, email: #{email_target.email}", email_target.to_s
    end
  end

  describe "#send_message" do
    it "uses TargetsMailer to deliver the message" do
      external = External.new(audit: events(:create_audit_for_minimum_ticket_1), ticket: tickets(:minimum_1), via_id: ViaType.RULE, via_reference_id: 0, resource: email_target.id, body: "Send and email to target")
      external.save

      ActionMailer::Base.perform_deliveries = true
      assert_difference 'TargetsMailer.deliveries.size', +1 do
        email_target.send_message("Example Message", external)
      end
    end
  end

  describe "#kind" do
    it "renders the correct string" do
      assert_equal I18n.t('txt.admin.models.targets.email_target.Email_target'), email_target.kind
    end
  end

  describe "#clean_up_address" do
    it "cleans up the email address" do
      email_target.email = "<example@example.com>"
      email_target.save

      assert_equal "example@example.com", email_target.email
    end
  end

  describe "#validate_email" do
    it "adds an error message to target" do
      email_target.email = "invalid @example.com"
      email_target.save

      assert_equal ["Email:  is invalid"], email_target.errors.full_messages
    end
  end

  describe "#render_message" do
    it "should return nil" do
      assert_nil email_target.render_message("<html></html>")
    end
  end

  describe "#message_failed" do
    it "deactivates the target when max of #{Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT} failures has been reached" do
      email_target.failures = Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT - 1
      assert email_target.is_active
      email_target.message_failed

      refute email_target.is_active
    end
  end
end
