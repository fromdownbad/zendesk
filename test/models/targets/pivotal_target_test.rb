require_relative "../../support/test_helper"
require_relative "../../../lib/zendesk/pivotal_tracker"

SingleCov.covered!

describe 'PivotalTarget' do
  fixtures :accounts, :subscriptions, :tickets, :users, :events, :translation_locales, :rules

  def self.it_behaves_like_a_normal_request(&block)
    it "has requested_by_id in the request params" do
      Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected_request_params).returns(
        "kind" => "story",
        "story_url" => "some_url"
      )
      instance_exec(&block) if block
      @target.send_message("Hello, a message", @event)
    end
  end

  before do
    @user = users(:minimum_agent)
    @target = PivotalTarget.new(
      title: "new pivotal tracker target",
      account: accounts(:minimum),
      token: 'the_token'
    )
    @target.encrypt!
  end

  describe "validation" do
    it "is valid if title, :account, and settings(token, project_id, story_type, story_title) are provided" do
      @target.settings = settings
      assert @target.valid?
    end

    it "is not valid if title is missing" do
      @target.title = nil
      refute @target.valid?
      assert @target.errors[:title].any?
    end

    it "is not valid if token is missing" do
      @target.token = nil
      refute @target.valid?
      assert @target.errors[:token].any?
    end

    describe "settings" do
      it "is not valid if project_id is missing" do
        @target.settings = settings(project_id: nil)
        refute @target.valid?
        assert @target.errors[:project_id].any?
      end

      describe "story_type" do
        it "is not valid if missing" do
          @target.settings = settings(story_type: nil)
          refute @target.valid?
          assert @target.errors[:story_type].any?
        end

        it "is not valid if not either a feature, chore bug or release" do
          @target.settings = settings(story_type: "blah")
          refute @target.valid?
          assert @target.errors[:story_type].any?
        end
      end

      it "is valid if requested_by is missing" do
        @target.settings = settings(requested_by: nil)
        assert @target.valid?
      end

      it "is valid if owned_by is missing" do
        @target.settings = settings(owned_by: nil)
        assert @target.valid?
      end
    end
  end

  describe "#save" do
    it "saves the target when valid" do
      @target.settings = settings
      assert @target.save
    end
  end

  describe "#to_s" do
    it "prints out a simple string detailing the token and project_id" do
      @target.settings = settings
      assert_equal "PivotalTarget, token: #{@target.token}, project_id: #{settings[:project_id]}", @target.to_s
    end
  end

  describe "#story_title" do
    it "returns settings[:story_title] when present" do
      @target.settings = settings
      assert @target.story_title, "this is the title"
    end

    it "returns 'From ticket \#{{ticket.id}}' when settings[:story_title] is not present" do
      @target.settings = settings(story_title: nil)
      assert "From ticket \#{{ticket.id}}", @target.story_title
    end
  end

  describe "#story_labels" do
    it "returns settings[:story_labels] when present" do
      @target.settings = settings
      assert @target.story_labels, "story_label"
    end

    it "returns settings[:labels] when settings[:story_labels] is not present" do
      @target.settings = settings(story_labels: nil)
      assert @target.story_labels, "label"
    end
  end

  describe "#kind" do
    it "calls I18n" do
      I18n.expects(:t).with('txt.admin.models.targets.pivotal_target.Pivotal_target')
      @target.kind
    end
  end

  describe "#show_story_url" do
    it "returns settings[:show_story_url] when present" do
      @target.settings = settings(show_story_url: "some_url")
      assert @target.show_story_url, "some_url"
    end

    it "returns nil if settings[:show_story_url] is missing" do
      @target.settings = settings
      assert @target.show_story_url.nil?
    end
  end

  describe "#send_message" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_agent)
      @user.account = @account
      @ticket = tickets(:minimum_1)
      @ticket.will_be_saved_by(@user, via_id: ViaType.RULE)
      @event = events(:create_external_for_minimum_ticket_1)
      @ticket.audit.events << @event
    end
    describe "with different locale" do
      before do
        @spanish = translation_locales(:spanish)
        @account.translation_locale = @spanish
        @target_with_spanish = PivotalTarget.new(account: accounts(:minimum))

        @target_with_spanish.settings = settings(story_title: "El ticket es un {{ticket.ticket_type}}")
        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([])
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([])
      end

      it "resolves liquide in locale" do
        spanish_expected_request_params = expected_request_params.merge!(
          name: 'El ticket es un Incident',
          description: 'Hola un mensaje'
        )
        Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], spanish_expected_request_params).returns(
          "kind" => "story",
          "story_url" => "some_url"
        )
        @target_with_spanish.send_message("Hola un mensaje", @event)
      end
    end

    describe "when ticket is updated" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))
        @target.settings = settings

        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([])
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([])
      end

      it_behaves_like_a_normal_request do
        @rule = rules(:trigger_notify_requester_of_received_request)
        @rule.definition.stubs(:conditions_all).returns([Definition.new(operator: "less_than", source: "status_id", value: 4), Definition.new(operator: "is", source: "update_type", value: ["Change"])])
        @rule.expects(:apply_and_collect_changes).with(any_parameters).never
      end
    end

    describe "with requested_by" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))

        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([])
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([])
      end

      describe "requested_by is part of the project members" do
        before do
          @target.settings = settings(requested_by: "Joe Schmoe")
          Zendesk::PivotalTracker.any_instance.expects(:project_memberships).with(settings[:project_id]).returns(stub_project_membership_response)
        end

        it "has requested_by_id in the request params" do
          updated_expected_request_params = expected_request_params(
            requested_by_id: "some_id"
          )
          Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], updated_expected_request_params).returns(
            "kind" => "story",
            "story_url" => "some_url"
          )
          @target.send_message("Hello, a message", @event)
        end
      end

      describe "requested_by is not part of the project members" do
        before do
          @target.settings = settings(requested_by: "John Doe")
          Zendesk::PivotalTracker.any_instance.expects(:project_memberships).with(settings[:project_id]).returns(stub_project_membership_response)
        end

        it_behaves_like_a_normal_request
      end
    end

    describe "with owned_by" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))
        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([])
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([])
      end

      describe "owned_by is part of the project members" do
        before do
          @target.settings = settings(owned_by: "Doug Dimmadome")
          Zendesk::PivotalTracker.any_instance.expects(:project_memberships).with(settings[:project_id]).returns(stub_project_membership_response)
        end

        it "has requested_by_id in the request params" do
          updated_expected_request_params = expected_request_params(
            owner_ids: ["doug_id"]
          )
          Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], updated_expected_request_params).returns(
            "kind" => "story",
            "story_url" => "some_url"
          )
          @target.send_message("Hello, a message", @event)
        end
      end

      describe "owned_by is not part of the project members" do
        before do
          @target.settings = settings(owned_by: "John Doe")
          Zendesk::PivotalTracker.any_instance.expects(:project_memberships).with(settings[:project_id]).returns(stub_project_membership_response)
        end

        it_behaves_like_a_normal_request
      end
    end

    describe "with labels" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([])
      end

      describe "no labels returned from pivotal" do
        before do
          @target.settings = settings
          Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([])
        end

        it_behaves_like_a_normal_request
      end

      describe "with labels returned from pivotal" do
        before do
          Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([{
            "kind": "label",
            "id": 1,
            "name": "story_label"
          }])
        end

        describe "with matching labels between pivotal and pivotal_target instance" do
          before do
            @target.settings = settings
          end

          it "labels is an array with a hash" do
            expected = expected_request_params(labels: [{ id: 1, name: 'story_label' }])
            Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected).returns(
              "kind" => "story",
              "story_url" => "some_url"
            )
            @target.send_message("Hello, a message", @event)
          end
        end

        describe "with matching labels between pivotal and pivotal_target instance and creating new label" do
          before do
            @target.settings = settings(story_labels: "story_label, new_label")
          end

          it "labels is an array with a hash and a string" do
            expected = expected_request_params(labels: [{ id: 1, name: 'story_label' }, "new_label"])
            Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected).returns(
              "kind" => "story",
              "story_url" => "some_url"
            )
            @target.send_message("Hello, a message", @event)
          end
        end
      end
    end

    describe "with integration" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))
        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([])
      end

      describe "no integrations returned from pivotal" do
        before do
          @target.settings = settings
          Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([])
        end

        it_behaves_like_a_normal_request
      end

      describe "with integrations returned from pivotal" do
        describe "but not zendesk_integration" do
          before do
            @target.settings = settings
            Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([{
              "kind": "some_integration",
              "id": 1,
              "story_name": "Ticket",
              "name": "Some Integration",
              "active": true
            }])
          end

          it "params has no external_id and integration_id" do
            Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected_request_params).returns(
              "kind" => "story",
              "story_url" => "some_url"
            )
            @target.send_message("Hello, a message", @event)
          end
        end

        describe "with zendesk_integration" do
          before do
            @target.settings = settings
            Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([{
              "kind": "some_integration",
              "id": 1,
              "story_name": "Ticket",
              "name": "Some Integration",
              "active": true
            }, {
              "kind": "zendesk_integration",
              "id": 2,
              "story_name": "Ticket",
              "name": "Some Integration",
              "active": true
            }])
          end

          it "params has external_id and integration_id" do
            expected = expected_request_params(
              external_id: @ticket.nice_id.to_s,
              integration_id: 2
            )

            Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected).returns(
              "kind" => "story",
              "story_url" => "some_url"
            )

            @target.send_message("Hello, a message", @event)
          end
        end
      end
    end

    describe "with blank message" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))
        @target.settings = settings
        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).never
        Zendesk::PivotalTracker.any_instance.expects(:project_memberships).with(settings[:project_id]).never
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).never
        Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected_request_params).never
      end

      it 'raises an error' do
        exception = assert_raises RuntimeError do
          @target.send_message('  ', @event)
        end

        message = exception.message
        assert message.include?(@account.id.to_s)
        assert message.include?(@ticket.id.to_s)
        assert message.include?(@ticket.nice_id.to_s)
        assert message.include?("Message is blank")
      end
    end

    describe "with error returned when posting /stories to Pivotal" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))
        @target.settings = settings
        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([])
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([])
        Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected_request_params).returns(
          "kind": "error",
          "code": "invalid_parameter",
          "error": "One or more request parameters was missing or invalid."
        )
      end

      it 'raises an error' do
        exception = assert_raises RuntimeError do
          @target.send_message('Hello, a message', @event)
        end

        message = exception.message
        assert message.include?(@account.id.to_s)
        assert message.include?(@ticket.id.to_s)
        assert message.include?(@ticket.nice_id.to_s)
        assert message.include?("Invalid parameter - One or more request parameters was missing or invalid.")
      end
    end

    describe "with error returned when getting memberships from Pivotal" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))
        @target.settings = settings(requested_by: "John Doe")
        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([])
        Zendesk::PivotalTracker.any_instance.expects(:project_memberships).with(settings[:project_id]).returns(
          "kind": "error",
          "code": "unauthorized_operation",
          "error": "Authorization failure."
        )
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).never
        Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected_request_params).returns([]).never
      end

      it 'raises an error' do
        exception = assert_raises RuntimeError do
          @target.send_message('Hello, a message', @event)
        end

        message = exception.message
        assert message.include?(@account.id.to_s)
        assert message.include?(@ticket.id.to_s)
        assert message.include?(@ticket.nice_id.to_s)
        assert message.include?("Unauthorized operation - Authorization failure.")
      end
    end

    describe "with error returned when getting labels from Pivotal" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))
        @target.settings = settings(requested_by: "John Doe")
        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns(
          "kind": "error",
          "code": "unauthorized_operation",
          "error": "Authorization failure."
        )
        Zendesk::PivotalTracker.any_instance.expects(:project_memberships).with(settings[:project_id]).never
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).never
        Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected_request_params).returns([]).never
      end

      it 'raises an error' do
        exception = assert_raises RuntimeError do
          @target.send_message('Hello, a message', @event)
        end

        message = exception.message
        assert message.include?(@account.id.to_s)
        assert message.include?(@ticket.id.to_s)
        assert message.include?(@ticket.nice_id.to_s)
        assert message.include?("Unauthorized operation - Authorization failure.")
      end
    end

    describe "creates new event for ticket" do
      before do
        @target = PivotalTarget.new(title: "new pivotal tracker target", account: accounts(:minimum))
        @target.settings = settings

        Zendesk::PivotalTracker.any_instance.expects(:project_labels).with(settings[:project_id]).returns([])
        Zendesk::PivotalTracker.any_instance.expects(:project_integrations).with(settings[:project_id]).returns([])
        Zendesk::PivotalTracker.any_instance.expects(:create_story).with(settings[:project_id], expected_request_params).returns(
          "kind" => "story",
          "story_url" => "some_url"
        )
      end

      it "adds a new audit to the ticket" do
        old_count = @ticket.audits.size
        @target.send_message("Hello, a message", @event)
        assert old_count + 1, @ticket.audits.size
      end

      it "adds a new push event to the new audit" do
        @target.send_message("Hello, a message", @event)
        audit = @ticket.audits.where(via_id: @event.via_id, author: @event.author).first
        e = audit.events.first

        assert e.is_a?(Push)
        assert_nil e.via_reference_id # @event.via_refernce_id is nil
        assert e.value_reference, "Pivotal Tracker"
        assert e.value, "<li>Story created from this ticket: <a href='some_url' target='_blank'>some_url</a></li"
      end

      describe 'when the audit fails to save' do
        it 'raises an exception' do
          Audit.any_instance.stubs(:save).returns(false)
          exception = assert_raises RuntimeError do
            @target.send_message('Hello, a message', @event)
          end

          message = exception.message
          assert message.include?("Unable to save Pivotal Tracker story creation event")
        end
      end
    end
  end

  private

  def settings(options = {})
    {
      project_id: 1234,
      story_type: "bug",
      story_title: "this is the title",
      story_labels: "story_label",
      labels: "label",
    }.merge!(options)
  end

  def expected_request_params(options = {})
    {
      story_type: "bug",
      name: "this is the title",
      description: "Hello, a message",
      labels: ["story_label"],
    }.merge!(options)
  end

  def stub_project_membership_response
    [
      {
        "kind" => "project_memberships",
        "person" => {
          "id" => "some_id",
          "name" => "Joe Schmoe"
        }
      },
      {
        "kind" => "project_memberships",
        "person" => {
          "id" => "doug_id",
          "name" => "Doug Dimmadome"
        }
      }
    ]
  end
end
