require_relative "../../support/test_helper"
require 'yammer'

SingleCov.covered! uncovered: 31

describe YammerTarget do
  fixtures :accounts, :events

  def create_send_message_stub(code, body)
    stub_request(:post, "https://www.yammer.com/api/v1/messages").
      with(body: {"body" => "test message", "group_id" => "888"}).
      to_return(status: code, body: body, headers: {})
  end

  before do
    @account = accounts(:minimum)
    @target = YammerTarget.new(title: "Yammer Target", account: @account)
  end

  describe "save" do
    describe "when valid" do
      before do
        @target.oauth2_token = "token"
      end

      it "saves the target" do
        assert @target.save
      end

      it "retains the token attribute" do
        @target.save
        @target.reload
        target = Target.find(@target.id)

        assert_equal("token", target.oauth2_token)
      end
    end

    it "does not save the target when token not present" do
      refute @target.save
    end
  end

  it "is not valid if title is missing" do
    @target.title = nil
    refute @target.valid?
    assert @target.errors[:title].any?
  end

  describe "oauth2 related" do
    it "responds properly to oauth related methods" do
      assert @target.requires_authorization?
    end
  end

  describe '#to_s' do
    it 'prints out a simple string' do
      assert_equal("YammerTarget: (id: #{@target.id})", @target.to_s)
    end
  end

  describe "#send_message" do
    describe "with a token" do
      before do
        @target.oauth2_token = 12345
        @target.group_id = 888
        create_send_message_stub(201, "")
      end
      it "creates message" do
        @response = @target.send_message("test message", nil)
        assert @response.created?
      end
    end

    describe "without a token" do
      before do
        @target.oauth2_token = nil
      end
      it "raises a YammerTarget::YammerTargetError" do
        assert_raise(YammerTarget::YammerTargetError) { @target.send_message("test message", nil) }
      end
    end

    describe "responds with a Yammer::ApiResponse" do
      before do
        @target.send(:client).stubs(:create_message).returns Yammer::ApiResponse.new("header", "body", 400)
      end
      before "raise a YammerTarget::YammerTargetError" do
        assert_raise(YammerTarget::YammerTargetError) { @target.send_message("test message", nil) }
      end
    end

    describe "responds with 401" do
      before do
        create_send_message_stub(401, '{"foo": "bar"')
      end
      it "raises a YammerTarget::YammerTargetError" do
        assert_raise(YammerTarget::YammerTargetError) { @target.send_message("test message", nil) }
      end
    end

    describe "responds with unknown response" do
      before do
        create_send_message_stub(200, "")
      end
      it "raises a YammerTarget::YammerTargetError" do
        assert_raise(YammerTarget::YammerTargetError) { @target.send_message("test message", nil) }
      end
    end

    describe "an account with host mapping" do
      before do
        @non_hostmapped_domain = @account.url(mapped: false, ssl: true)
        @account.route.host_mapping = "example.com"
      end
      it "uses the non-host mapped url" do
        assert_equal(@target.send(:account_url), @non_hostmapped_domain)
      end
    end

    describe 'for an account without ssl' do
      before do
        @account.stubs(:is_ssl_enabled?).returns(false)
      end
      it 'forces the redirect uri to HTTPS since that is what Yammer expects' do
        url = @target.authorize_url
        assert url.include? 'redirect_uri=https'
      end
    end
  end
end
