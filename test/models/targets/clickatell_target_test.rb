require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe ClickatellTarget do
  fixtures :accounts, :tickets, :users, :events

  before do
    @event  = events(:create_external_for_minimum_ticket_1)
    @ticket = @event.ticket
    @user   = @event.author
    @account = @user.account
    @target = ClickatellTarget.new(
      title: "Clickatell Target",
      account: @account,
      username: 'user',
      password: 'password',
      api_id: '1234',
      to: '448311234567'
    )
    @target.encrypt!
    @message = 'Test message from Zendesk sent on: 2011-xx-xx xx:xx:xx UTC'
  end

  describe "save" do
    it "saves the target when valid" do
      assert @target.save
    end
  end

  describe "validation" do
    subject { @target }

    it 'does not be valid if for a small-business account without a "from" number' do
      @target.us_small_business_account = '1'
      @target.from = nil
      refute @target.valid?
      assert @target.errors[:from].any?
    end

    it 'does not require a "from" number for a non-small-business account' do
      @target.us_small_business_account = '0'
      @target.from = nil
      assert @target.valid?
    end

    it "is not valid if password is missing" do
      @target.password = nil
      refute @target.valid?
      assert @target.errors[:password].any?
    end

    should validate_presence_of :title
    should validate_presence_of :username
    should validate_presence_of :password
    should validate_presence_of :api_id
    should validate_presence_of :to
  end

  describe "Clickatell logging" do
    before do
      assert @target.valid?

      @client  = @target.client(@message, account: @account)
      @the_url = @client.url.gsub("+", "%20")
    end

    it "Logs error responses via HTTP 200 OK from a Clickatell SMS target" do
      stub_request(:get, @the_url).
        with(basic_auth: ['user', 'password'])
      @target.expects(:log_response).once
      @target.send_message(@message, @event)
    end

    it 'Displays Clickatell Message ID for successful response HTTP 200 OK' do
      stub_request(:get, @the_url).to_return(body: "ID: e1ec98d249d463d4ff25788db7434333")
      Rails.logger.expects(:info).at_least_once
      result = @target.send_message(@message, @event)
      assert_match(/^ID: [a-z0-9]+$/, result.body)
    end

    it 'Catchs xxxx and raise Clickatell responses with error codes in HTTP 200 OK xxxx' do
      error_codes = %w[001 002 101 105 106 108 114 301]
      Rails.logger.expects(:error).times(error_codes.length)
      error_codes.each do |code|
        exception = assert_raise(ClickatellAPIError) do
          stub_request(:get, @the_url).to_return(body: "ERR: #{code}, Some error message")
          @target.send_message(@message, @event)
        end
        assert_match(/ERROR/, exception.message)
      end
    end

    it 'Catchs and raise unrecognized Clickatell response error status codes' do
      Rails.logger.expects(:error).once
      stub_request(:get, @the_url).to_return(body: "ERR: 999, Some unkown message")
      exception = assert_raises(ClickatellAPIError) { @target.send_message(@message, @event) }
      assert_match("Unknown Clickatell error status", exception.message)
    end
  end

  describe "#to_s" do
    it "prints out a simple string detailing the subdomain, room and token" do
      assert_equal "ClickatellTarget, URL: #{@target.url}, method: get", @target.to_s
    end
  end

  describe "#truncate_message" do
    it "truncates normal messages to 160" do
      assert_equal 160, @target.send(:truncate_message, "a" * 160).size
      assert_equal 160, @target.send(:truncate_message, "a" * 161).size
    end

    it "truncates messages with special characters so they will fit expanded into 160" do
      ClickatellTarget::HEAVIES.each do |ch|
        truncated = @target.send(:truncate_message, "a" * 159 + ch)
        assert_equal "a" * 159, truncated
        assert_equal 159, truncated.size

        truncated = @target.send(:truncate_message, "a" * 158 + ch)
        assert_equal "a" * 158 + ch, truncated
        assert_equal 159, truncated.mb_chars.size
      end
    end

    it "truncates special characters (data from ZD:168410)" do
      msg = "ALERT: [Unicorn Media Support System] FileReplicationWorker Job Failed: 4a25dfa9-768b-4c29-88ec-9c70... (187802)\nDetails: http://support.unicornmedia.com/tickets/187802"
      assert_equal 157, @target.send(:truncate_message, msg).size
    end
  end

  describe "#url" do
    it "returns the expected value" do
      assert_equal("http://api.clickatell.com/http/sendmsg", @target.url)
    end
  end

  describe "#parameters" do
    before do
      @params = { "user" => @target.username, "password" => @target.password, "api_id" => @target.api_id, "to" => @target.to }
    end

    describe "without sender id" do
      it "returns the correct pairs" do
        assert_equal(@params, @target.send(:parameters))
      end
    end

    describe "with blank sender id" do
      before { @target.settings[:from] = "" }
      it "generates the correct url" do
        assert_equal(@params, @target.send(:parameters))
      end
    end

    describe "with sender id" do
      before { @target.settings[:from] = '448311234567' }
      it "generates the correct url" do
        assert_equal(@params.merge("from" => "448311234567"), @target.send(:parameters))
      end
    end

    describe 'for a US Small Business account' do
      before do
        @target.us_small_business_account = true
        @target.from = '1234567890'
      end

      it 'adds the "mo=1" parameter' do
        assert_match %r{[?&]mo=1}, @target.url + "?" + @target.send(:parameters).to_query
      end
    end
  end
end
