require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe CampfireTarget do
  fixtures :accounts, :events

  before do
    @target = CampfireTarget.new(
      title: "Campfire Target",
      account: accounts(:minimum),
      token: 'the_token',
      subdomain: "subdomain",
      room: "The Room",
      ssl: "0",
      preserve_format: "0"
    )
    @target.encrypt!
  end

  describe "save" do
    it "saves the target when valid" do
      assert @target.save
    end
  end

  describe "validation" do
    subject { @target }

    should validate_presence_of :title
    should validate_presence_of :token
    should validate_presence_of :subdomain
    should validate_presence_of :room

    it "is not valid if token is missing" do
      @target.token = nil
      refute @target.valid?
      assert @target.errors[:token].any?
    end
  end

  describe "#to_s" do
    it "prints out a simple string detailing the subdomain, room and token" do
      assert_equal "CampfireTarget, subdomain: #{@target.subdomain}, room: #{@target.room}, token: #{@target.token}", @target.to_s
    end
  end

  describe "#send_message" do
    before do
      @account = accounts(:minimum)
      @event = events(:create_external_for_minimum_ticket_1)
      @ticket = @event.ticket
      @user = @event.author
      @campfire_connection = Tinder::Campfire.new('subdomain')
      Tinder::Campfire.stubs(:new).returns(@campfire_connection)
      @room = Tinder::Room.new(@campfire_connection)
      @room.stubs(:speak)
      @room.stubs(:paste)
      @room.stubs(:leave)
      @campfire_connection.stubs(:find_room_by_name).returns(@room)
      @message = 'Testing...'
    end

    describe "with a blank message" do
      it "raises an exception" do
        assert_raises(RuntimeError) { @target.send_message('', @event) }
        assert_raises(RuntimeError) { @target.send_message(nil, @event) }
      end
    end

    describe "with ssl disabled" do
      before { @target.ssl = false }

      it "creates a http campfire connection" do
        Tinder::Campfire.expects(:new).with(@target.subdomain, ssl: false).returns(@campfire_connection)
        @target.send_message(@message, @event)
      end
    end

    describe "with ssl enabled" do
      before { @target.ssl = true }

      it "creates a http campfire connection" do
        Tinder::Campfire.expects(:new).with(@target.subdomain, ssl: true).returns(@campfire_connection)
        @target.send_message(@message, @event)
      end
    end

    describe "with an invalid room" do
      before { @campfire_connection.stubs(:find_room_by_name).returns(nil) }

      it "raises an exception" do
        assert_raises(RuntimeError) { @target.send_message(@message, @event) }
      end
    end

    describe "without preserving format" do
      before { @target.preserve_format = false }

      it "speaks in the room" do
        @room.expects(:speak).with(@message)
        @target.send_message(@message, @event)
      end

      it "leaves the room" do
        @room.expects(:leave)
        @target.send_message(@message, @event)
      end
    end

    describe "preserving format" do
      before { @target.preserve_format = true }
      it "pastes in the room" do
        @room.expects(:paste).with(@message)
        @target.send_message(@message, @event)
      end

      it "leaves the room" do
        @room.expects(:leave)
        @target.send_message(@message, @event)
      end
    end

    it "logins using the token" do
      @campfire_connection.expects(:login).with(@target.token, 'x')
      @target.send_message(@message, @event)
    end
  end
end
