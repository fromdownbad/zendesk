require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'FlowdockTarget' do
  def self.it_has_setting(name, default = nil, required = false)
    describe "##{name}" do
      it "defaults to '#{default.inspect}'" do
        assert_equal_with_nil default, @target.send(name)
      end
      it "supports getting and setting" do
        @target.send "#{name}=", 'foobar'
        assert_equal 'foobar', @target.send(name)
      end
      if required
        it 'is required' do
          @target.send "#{name}=", nil
          @target.valid?
          assert @target.errors[name].present?, "Expected errors on #{name}. Errors: #{@target.errors.inspect}"
        end
      end
    end
  end

  before do
    @account = Account.new.tap do |a|
      a.stubs(:url).returns('https://support.mycompany.com')
      a.stubs(:in_business_hours?).returns(true)
    end
    @target = FlowdockTarget.new(
      account: @account,
      api_token: 'token'
    )
    @target.encrypt!
  end

  it 'describes itself as a Flowdock target' do
    assert_equal 'Flowdock target', @target.kind
  end

  describe 'validations' do
    it "is not valid if api_token is missing" do
      @target.api_token = nil
      refute @target.valid?
      assert @target.errors[:api_token].any?
    end
  end

  describe 'messages' do
    it 'supports them' do
      assert @target.is_message_supported?
    end
  end

  it 'sends messages via its Gateway' do
    message = Object.new
    evt = Object.new
    FlowdockTarget::Gateway.expects(:send).with(@target, message, evt)
    @target.send_message(message, evt)
  end

  describe 'Gateway' do
    before do
      @target.api_token = 'abc123'
      @target.encrypt!
      @ticket = Ticket.new(subject: 'Help', account: @account)
      @ticket.stubs(:nice_id).returns(13)
      @ticket.current_tags = 'foo bar'
      @author = User.new(name: 'Susan').tap { |u| u.stubs(:email).returns('susan@example.org') }
      @evt = stub('Event', ticket: @ticket, author: @author)
    end

    it 'uses the Flowdock API URL' do
      stub_request(:post, "https://api.flowdock.com/v1/zendesk/abc123").
        with(body: "id=13&in_business_hours=true&message=%7B%7Bticket.latest_comment_formatted%7D%7D&priority=-&status=New&tags=foo+bar&title=Help&type=Ticket&url=-&user%5Bemail%5D=susan%40example.org&user%5Bfirst_name%5D=Susan&user%5Blast_name%5D=Susan&user%5Bname%5D=Susan&via=Web+service").
        to_return(status: 200)
      FlowdockTarget::Gateway.send(@target, '{{ticket.latest_comment_formatted}}', @evt)
    end

    it 'raises error on bad post' do
      stub_request(:post, "https://api.flowdock.com/v1/zendesk/abc123").to_return(status: 500, body: "", headers: {})

      assert_raise(RuntimeError) do
        FlowdockTarget::Gateway.send(@target, '{{ticket.latest_comment_formatted}}', @evt)
      end
    end
  end
end
