require_relative "../../support/test_helper"
require 'johnny_five'

SingleCov.covered! uncovered: 34

describe Target do
  fixtures :all
  should_validate_presence_of :title

  after do
    Target.instance_variable_set(:@setting_writers, nil) # We modify this in some test and it leaks from one to another
  end

  describe '.format' do
    it 'returns `plain`' do
      assert_equal :plain, Target.format
    end
  end

  describe "A new target" do
    before { @target = Target.new }

    it "is inactive" do
      refute @target.is_active?
    end

    it "can be tested by default" do
      assert @target.is_test_supported?
      assert_equal '', @target.test_no_supported_reason
    end

    it "has no failures or error message" do
      assert_equal 0, @target.failures
      assert_equal '', @target.error
    end

    it "has no sent messages" do
      assert_equal 0, @target.sent
    end

    describe "that is a system target" do
      before do
        @account = accounts(:minimum)
        @target.title = 'Target title'
        @target.account = @account
        @target.save!
      end

      it "is marked as such" do
        @target.feature_identifier = "FeatureIdentifier"
        assert @target.is_system_target?
      end

      describe "and is locked" do
        before do
          @target.is_locked = true
        end

        it "is not updateable" do
          assert_raise ActiveRecord::ReadOnlyRecord do
            @target.update_attributes(title: 'New title')
          end
        end

        it "is deletable" do
          assert @target.is_locked
          assert @target.destroy
        end
      end
    end

    describe "that has an AppsRequirement" do
      before do
        @account = accounts(:minimum)
        @target.title = 'Target title'
        @target.account = @account
        @target.save!
        FactoryBot.create(:resource_collection_resource,
          account_id: @target.account_id,
          resource: @target)
        @target.reload
      end

      describe "when modifications are made" do
        describe "to any attributes but failures count" do
          it "it is not modifiable" do
            @target.title = "something else"
            assert_equal false, @target.save
            assert_includes @target.errors[:base],
              I18n.t("txt.admin.models.apps_requirements.validation.cannot_modify_requirement")
          end
        end

        describe "to failures count" do
          it "it updates failures count" do
            @target.failures = @target.failures + 1
            assert(@target.save)
          end
        end

        describe "to error message" do
          it "it updates error message" do
            @target.error = "oh nooo"
            assert(@target.save)
          end
        end

        describe "to sent count" do
          it "it updates sent count" do
            @target.sent = @target.sent + 1
            assert(@target.save)
          end
        end

        describe "to is_active state" do
          it "it updates active state" do
            @target.is_active = !@target.is_active
            assert(@target.save)
          end
        end
      end

      it "is not destroyable" do
        assert_equal false, @target.destroy
        assert_equal false, @target.destroyed?
        assert_includes @target.errors[:base],
          I18n.t("txt.admin.models.apps_requirements.validation.cannot_modify_requirement")
      end
    end
  end

  describe "active named scope" do
    before do
      @active_targets = Target.active
    end

    it "returns only active targets" do
      @active_targets.each do |target|
        assert target.is_active?
      end
    end

    it "returns active targets sorted by title" do
      assert_equal @active_targets.sort_by(&:title), @active_targets
    end
  end

  describe "inactive named scope" do
    before do
      @inactive_targets = Target.inactive
    end

    it "returns only inactive targets" do
      @inactive_targets.each do |target|
        refute target.is_active?
      end
    end

    it "returns inactive targets sorted by title" do
      assert_equal @inactive_targets.sort_by(&:title), @inactive_targets
    end
  end

  describe "#send_test_message" do
    it "delegates to #send_message" do
      @target = Target.new
      @external = stub
      @target.expects(:send_message).with("foo", @external)
      @target.send_test_message("foo", @external)
    end
  end

  describe "#message_sent" do
    describe_with_arturo_disabled :bulk_update_targets_status do
      before do
        @account = accounts(:minimum)
        @target = Target.new(sent: 1, failures: 3, error: 'Some error', account: @account)
        @target.message_sent
      end

      it "increases the number of sent messages" do
        assert_equal 2, @target.sent
      end

      it "resets the failures" do
        assert_equal 0, @target.failures
      end

      it "resets the error message" do
        assert_equal '', @target.error
      end
    end
    describe_with_arturo_enabled :bulk_update_targets_status do
      before do
        @account = accounts(:minimum)
        @target = Target.new(account: @account)
      end
      it "calls the method increment_cached_success_count" do
        @target.expects(:increment_cached_success_count)
        @target.message_sent
      end
    end
  end

  describe "#message_failed" do
    describe_with_arturo_disabled :bulk_update_targets_status do
      before do
        @account = accounts(:minimum)
        @target = Target.new(sent: 1, failures: 3, error: 'Some error', account: @account)
        @target.message_failed(Exception.new('New error'))
      end

      it "increases the number of failures" do
        assert_equal 4, @target.failures
      end

      it "registers the new error message" do
        assert_equal 'New error', @target.error
      end

      it "does not change the number of sent messages" do
        assert_equal 1, @target.sent
      end

      describe "failures exceeds max_target_failures" do
        before do
          @target.failures = @account.settings.max_target_failures
          TargetsMailer.expects(:deliver_target_disabled).times(1).with(@target)
          @target.message_failed(Exception.new)
        end

        it "deactivates the target" do
          refute @target.is_active?
        end
      end

      describe "failures exceeds max_target_failures racily" do
        before do
          TargetsMailer.expects(:deliver_target_disabled).times(1).with(@target)
          # ehhh, kinda cheesy way to simulate the race condition
          @target.failures = @account.settings.max_target_failures
          @target.message_failed(Exception.new)
          @target.failures = @account.settings.max_target_failures
          @target.message_failed(Exception.new)
        end

        it "deactivates the target" do
          refute @target.is_active?
        end
      end
    end

    describe_with_arturo_enabled :bulk_update_targets_status do
      it "calls the method increment_cached_failure_count" do
        @account = accounts(:minimum)
        target = Target.new(account: @account)
        target.expects(:increment_cached_failure_count)
        target.expects(:deactivate_if_over_limit)
        target.message_failed
      end

      describe "failures exceeds max_target_failures" do
        it "deactivates the target" do
          @account = accounts(:minimum)
          target = Target.new(sent: 1, is_active: true, account: @account, title: "Deactivates Test Target")
          target.save
          target.failures = @account.settings.max_target_failures - 3
          target.save

          Rails.cache.write(Target.cached_failure_count_key(target.id), 3, raw: true)
          TargetsMailer.expects(:deliver_target_disabled).times(1).with(target)
          target.message_failed(Exception.new)
          refute target.is_active?
        end
      end

      describe "failures not exceeds max_target_failures since cache value takes over database value" do
        it "not deactivates the target" do
          @account = accounts(:minimum)
          target = Target.new(sent: 1, is_active: true, account: @account, title: "Not deactivates Test Target")
          target.save
          target.failures = @account.settings.max_target_failures - 3
          target.save

          Rails.cache.write(Target.cached_success_count_key(target.id), 5, raw: true)
          Rails.cache.write(Target.cached_failure_count_key(target.id), 8, raw: true)
          TargetsMailer.expects(:deliver_target_disabled).times(0).with(target)
          target.message_failed(Exception.new)
          assert target.is_active?
        end
      end
    end
  end

  describe "activate target" do
    before do
      @account = accounts(:minimum)
      @target = Target.new(sent: 10, failures: 21, error: 'This is a funny error, right?', is_active: false, account: @account, title: "Activation Test Target")
      @target.save
    end

    it "should reset failure and error by manually activation" do
      assert_equal 21, @target.failures
      assert_equal 'This is a funny error, right?', @target.error
      refute @target.is_active

      @target.activate

      assert_equal 0, @target.failures
      assert_equal '', @target.error
      assert @target.is_active
    end

    it "should reset failure and error by update active value" do
      assert_equal 21, @target.failures
      assert_equal 'This is a funny error, right?', @target.error
      refute @target.is_active

      @target.is_active = true
      @target.save

      assert_equal 0, @target.failures
      assert_equal '', @target.error
      assert @target.is_active
    end
  end

  describe "#reset_target_and_cache" do
    before do
      @account = accounts(:minimum)
      @target = Target.new(sent: 10, failures: 0, error: '', is_active: true, account: @account, title: "Reset Cache Test Target")
    end

    it "should reset target IDs array when new record be created" do
      Rails.cache.write("targets_to_update_shard#{@account.shard_id}", ['1', '2'])

      @target.save

      assert_nil Rails.cache.read("targets_to_update_shard#{@account.shard_id}")
    end

    it "should reset target IDs array and target cache when target becomes active" do
      @target.error = 'This is a funny error, right?'
      @target.failures = 21
      @target.is_active = false
      @target.save

      assert_equal 21, @target.failures
      assert_equal 'This is a funny error, right?', @target.error
      refute @target.is_active

      Rails.cache.write("targets_to_update_shard#{@account.shard_id}", ['1', '2'])
      Rails.cache.write("targets/success_counter/v1/#{@target.id}", 10, raw: true)
      Rails.cache.write("targets/failure_counter/v1/#{@target.id}", 30, raw: true)

      @target.is_active = true
      @target.save

      assert_nil Rails.cache.read("targets_to_update_shard#{@account.shard_id}")
      assert_nil Rails.cache.read("targets/success_counter/v1/#{@target.id}")
      assert_nil Rails.cache.read("targets/failure_counter/v1/#{@target.id}")

      assert_equal 0, @target.failures
      assert_equal '', @target.error
      assert @target.is_active
    end

    it "should reset target IDs array and target cache when target becomes inactive" do
      assert_equal 0, @target.failures
      assert_equal '', @target.error
      assert @target.is_active

      Rails.cache.write("targets_to_update_shard#{@account.shard_id}", ['1', '2'])
      Rails.cache.write("targets/success_counter/v1/#{@target.id}", 10, raw: true)
      Rails.cache.write("targets/failure_counter/v1/#{@target.id}", 30, raw: true)

      @target.error = 'This is a funny error, right?'
      @target.failures = 21
      @target.is_active = false

      @target.save

      assert_equal 21, @target.failures
      assert_equal 'This is a funny error, right?', @target.error
      refute @target.is_active

      assert_nil Rails.cache.read("targets_to_update_shard#{@account.shard_id}")
      assert_nil Rails.cache.read("targets/success_counter/v1/#{@target.id}")
      assert_nil Rails.cache.read("targets/failure_counter/v1/#{@target.id}")
    end
  end

  describe "#current_failures" do
    before do
      @account = accounts(:minimum)
      @target = Target.new(sent: 10, failures: 1, error: '', is_active: true, account: @account, title: "Current Failure Count Test Target")
    end

    it "should get current faliure without cache value when arturo is disabled" do
      Rails.cache.write("targets/failure_counter/v1/#{@target.id}", 30, raw: true)
      Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(false)
      assert_equal 1, @target.send(:current_failures)
    end

    it "should get current faliure when both sent and failure exist in cache and arturo is enabled" do
      Rails.cache.write("targets/success_counter/v1/#{@target.id}", 10, raw: true)
      Rails.cache.write("targets/failure_counter/v1/#{@target.id}", 30, raw: true)
      Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(true)
      assert_equal 30, @target.send(:current_failures)
    end

    it "should get current faliure when only failure exists in cache and arturo is enabled" do
      Rails.cache.write("targets/failure_counter/v1/#{@target.id}", 30, raw: true)
      Account.any_instance.stubs(:has_bulk_update_targets_status?).returns(true)
      assert_equal 31, @target.send(:current_failures)
    end
  end

  describe "#increment_current_failures" do
    before do
      @account = accounts(:minimum)
      @target = Target.new(account: @account, title: "Increment Failures Test Target")
    end

    it "should increment current faliure" do
      @target.stubs(:current_failures).returns(5)
      assert_equal 6, @target.send(:increment_current_failures, StandardError.new)
    end

    it "should retain same faliure when the error is circuit tripped" do
      @target.stubs(:current_failures).returns(5)
      assert_equal 5, @target.send(:increment_current_failures, ::JohnnyFive::CircuitTrippedError.new)
    end
  end

  describe "#deactivate notifications" do
    before do
      @account = accounts(:minimum)
      @target = Target.new(sent: 1, failures: 0, account: @account, title: "A Target")
    end

    it "does not notify account owner if not requested" do
      TargetsMailer.expects(:deliver_target_disabled).never
      @target.deactivate
      refute @target.is_active
    end

    it "notifies account owner if requested" do
      TargetsMailer.expects(:deliver_target_disabled).times(1).with(@target)
      @target.deactivate(send_notification: true)
      refute @target.is_active
    end
  end

  describe ".settings_reader" do
    it "adds corresponding reader methods" do
      target = Target.new
      target.settings = { foo: "foo" }
      assert_raise(NoMethodError) { target.foo }
      Target.send :settings_reader, :foo
      assert_equal("foo", target.foo)
    end
  end

  describe ".settings_writer" do
    it "adds corresponding writer methods" do
      Target.send :settings_reader, :bar
      target = Target.new
      assert_raise(NoMethodError) { target.bar = "bar" }
      Target.send :settings_writer, :bar
      target.bar = "bar"
      assert_equal("bar", target.bar)
    end
  end

  describe ".setting_writers" do
    it "is empty for base" do
      assert_equal [], Target.setting_writers
    end

    it "does not get polluted by inheritance" do
      ClickatellTarget # rubocop:disable Lint/Void
      assert_equal [:url, :method, :attribute, :username], UrlTarget.setting_writers
    end

    it "includes everything for top-level" do
      assert_equal [:url, :method, :attribute, :username, :api_id, :to, :from, :us_small_business_account], ClickatellTarget.setting_writers
    end
  end

  describe ".settings_accessor" do
    it "adds corresponding reader and writer methods" do
      target = Target.new
      assert_raise(NoMethodError) { target.baz }
      assert_raise(NoMethodError) { target.baz = "baz" }

      Target.send :settings_accessor, :baz
      target.baz = "baz"
      assert_equal("baz", target.baz)
    end

    it "persists settings to data column" do
      target = Target.new
      Target.send :settings_accessor, :baz
      target.baz = "baz"
      assert_equal("baz", target.baz)
      assert_equal({baz: "baz"}, target.data)
    end

    describe "boolean accessors" do
      it "stores string values when setting" do
        target = CampfireTarget.new
        {"1" => "1", "0" => "0", "xxx" => "0", true => "1", false => "0"}.each do |given, expected|
          target.ssl = given
          assert_equal expected, target.settings[:ssl], "itset #{expected} with #{given}"
        end
      end

      it "reads boolean values" do
        target = CampfireTarget.new
        {"1" => true, "0" => false, nil => false}.each do |given, expected|
          target.settings[:ssl] = given
          assert_equal expected, target.ssl, "itget #{expected} with #{given}"
        end
      end
    end
  end

  describe ".remove_settings_accessor" do
    # this can't be split up in multiple blocks because class variables and methods persist between them :() or I just can't figure out how to
    it "removes methods and setting_writers but not from parent" do
      target = Target.new

      assert_raise(NoMethodError) { target.attr }
      assert_raise(NoMethodError) { target.attr = "bar" }

      Target.send :settings_accessor, :attr

      target.attr
      target.attr = "bar"

      class TestTarget < Target; end
      test_target = TestTarget.new

      assert_includes TestTarget.setting_writers, :attr

      test_target.attr
      test_target.attr = "bar"

      TestTarget.send :remove_settings_accessor, :attr

      assert_raise(NoMethodError) { test_target.attr }
      assert_raise(NoMethodError) { test_target.attr = "bar" }

      refute_includes TestTarget.setting_writers, :attr

      target.attr
      target.attr = "bar"

      assert_includes Target.setting_writers, :attr
    end

    after { Target.send :remove_settings_accessor, :attr }
  end

  describe "#render_message" do
    let(:ticket) { tickets(:minimum_1) }
    let(:external) do
      External.new(
        account: ticket.account,
        ticket: ticket,
        audit: ticket.audits.last,
        via_id: ticket.audits.last.via_id,
        author: ticket.account.owner,
        value: target.id,
        body: body
      )
    end

    describe "when the target is a UrlTargetV2" do
      let(:target) { targets(:url_v2) }

      describe "and the body is a serialized array" do
        let(:body) { "--- \n- - this\n  - that\n" }

        it "returns an array of arrays" do
          assert_equal [["this", "that"]], target.render_message(external)
        end
      end

      describe "and the body is nil (no parameters)" do
        let(:body) { nil }

        it "returns nil" do
          assert_nil target.render_message(external)
        end
      end
    end

    describe "when the target is not a UrlTargetV2" do
      let(:target) { targets(:url_valid) }
      let(:body) { "The ticket type is {{ticket.ticket_type}}" }

      it "returns an interpolated string" do
        assert_equal "The ticket type is Incident", target.render_message(external)
      end
    end
  end

  describe "#apps_requirements" do
    describe "finds requirements" do
      before do
        stub_request(:get, "https://something.zendesk-test.com/api/v2/apps/requirements.json?includes=installation").
          to_return(
            body: JSON.dump(
              "requirements" => [
                {
                  "requirement_id" => 1,
                  "requirement_type" => "targets",
                  "installation" => {
                    "settings" => { "title" => "Test1" }
                  }
                },
                {
                  "requirement_id" => 2,
                  "requirement_type" => "targets",
                  "installation" => {
                    "settings" => { "title" => "Test2" }
                  }
                },
                {
                  "requirement_id" => 3,
                  "requirement_type" => "targets",
                  "installation" => {
                    "settings" => { "title" => "Test3" }
                  }
                }
              ]
            ),
            status: 200, headers: { content_type: 'application/json' }
          )
      end

      it "finds the installation names" do
        assert_equal({1 => "Test1", 3 => "Test3"},
          Target.apps_requirements(
            stub(
              id: 2,
              subdomain: "something",
              targets: stub(pluck: [1, 3])
            )
          ))
      end
    end

    describe "does not find requirements" do
      before do
        stub_request(:get, "https://something.zendesk-test.com/api/v2/apps/requirements.json?includes=installation").
          to_return(body: { "requirements" => [] }.to_json, status: 200)
      end

      it "returns empty hash" do
        assert_equal({},
          Target.apps_requirements(
            stub(
              id: 2,
              subdomain: "something",
              targets: stub(pluck: [1, 2, 3])
            )
          ))
      end
    end
  end

  describe "#uses_parameters?" do
    let(:target) { Target.new }

    it "returns false" do
      refute target.uses_parameters?
    end
  end

  describe '#encrypt!' do
    let(:target) { JiraTarget.new(password: '123456') }

    before do
      Timecop.freeze
      target.encrypt!
    end

    it 'sets credential_updated_at' do
      assert_equal target.credential_updated_at, Time.now
    end

    it 'calls super' do
      assert_equal target.password, '123456'
    end
  end

  describe "#types" do
    it "returns a list of all the target types" do
      assert_equal [
        CampfireTarget,
        UrlTarget,
        UrlTargetV2,
        EmailTarget,
        ClickatellTarget,
        TwitterTarget,
        GetSatisfactionTarget,
        YammerTarget,
        BasecampTarget,
        JiraTarget,
        PivotalTarget,
        TwilioTarget,
        SalesforceTarget,
        SugarCrmTarget,
        FlowdockTarget,
        MsDynamicsTarget,
      ], Target.types
    end
  end

  describe "#metrics_name" do
    let(:account) { accounts(:minimum) }

    it "should use class name as metrics name" do
      target = EmailTarget.new
      assert_equal :email_target, target.metrics_name

      target = UrlTarget.new(title: "test", account: account, url: "https://wibble.org/a/b", attribute: "message", method: "get")
      assert_equal :url_target, target.metrics_name

      target = UrlTargetV2.new(url: "https://wibble.org/a/b", method: "get", account: account)
      assert_equal :url_target_v2, target.metrics_name
    end
  end

  describe "#circuit_tripped?" do
    let(:target) { UrlTargetV2.new(url: "https://wibble.org/a/b", method: "get", account: accounts(:minimum)) }

    it "should be circuit tripped by johnny five circuit tripped exception" do
      e = ::JohnnyFive::CircuitTrippedError.new("tripped")
      assert target.send(:circuit_tripped?, e)
    end

    it "should not be circuit tripped by other exception" do
      e = StandardError.new("generic error")
      refute target.send(:circuit_tripped?, e)
    end

    it "should not be circuit tripped by nil exception" do
      refute target.send(:circuit_tripped?, nil)
    end
  end

  describe "target is failed by circuit tripped error" do
    it "should total failure count of target not be incremented but not be deactivated" do
      target = Target.new(sent: 0, failures: 0, is_active: true, account: accounts(:minimum))
      target.expects(:deliver_target_disabled_notification).times(0)

      e = ::JohnnyFive::CircuitTrippedError.new("tripped")
      target.message_failed(e)

      assert_equal 0, target.sent
      assert_equal 0, target.failures
      assert_equal '', target.error
      assert target.is_active
    end

    it "should total failure count of target not be incremented but be deactivated" do
      target = Target.new(sent: 0, failures: 21, is_active: true, account: accounts(:minimum))
      target.expects(:deliver_target_disabled_notification).times(1)

      e = ::JohnnyFive::CircuitTrippedError.new("tripped")
      target.message_failed(e)

      assert_equal 0, target.sent
      assert_equal 21, target.failures
      assert_equal '', target.error
      refute target.is_active
    end
  end
end
