require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 9

describe 'TwitterTarget' do
  fixtures :accounts, :events

  before do
    @target = TwitterTarget.new(title: "Twitter Target", account: accounts(:minimum))
  end

  describe "save" do
    describe "when valid" do
      before do
        @target.assign_attributes(
          token: 'token',
          secret: 'secret'
        )
        @target.encrypt!
      end

      it "saves the target" do
        assert @target.save
      end

      it "retains the token attribute" do
        @target.save
        @target.reload
        target = Target.find(@target.id)

        assert_equal("token", target.token)
      end
    end

    it "does not save the target when token and secret not present" do
      refute @target.save
    end

    it "does not save the target when token not present" do
      @target.secret = 'secret'
      @target.encrypt!
      refute @target.save
    end

    it "does not save the target when secret not present" do
      @target.token = 'token'
      @target.encrypt!
      refute @target.save
    end

    it "does not save the target when the token is missing" do
      @target.token = nil
      refute @target.valid?
      assert @target.errors[:token].any?
    end

    it "does not save the target when the secret is missing" do
      @target.secret = nil
      refute @target.valid?
      assert @target.errors[:secret].any?
    end
  end

  it "does not be valid if title is missing" do
    @target.title = nil
    refute @target.valid?
    assert @target.errors[:title].any?
  end

  describe "oauth related" do
    it "responds properly to oauth related methods" do
      assert @target.requires_authorization?
    end
  end

  describe '#to_s' do
    it 'prints out a simple string' do
      assert_equal("TwitterTarget: (id: #{@target.id})", @target.to_s)
    end
  end

  describe "#send_message" do
    it "uses OAuth when authorized" do
      @target.expects(:authorized?).returns(true)
      @target.expects(:send_message_with_oauth)
      @target.send_message("test message", nil)
    end
  end

  describe "#send_test_message" do
    it "appends a random number to the message" do
      Kernel.stubs(:rand).returns(1234)
      @target.expects(:send_message).with(regexp_matches(/1234/), anything)
      @target.send_test_message("test message", nil)
    end
  end

  describe "#send_message_with_oauth" do
    before do
      @target.assign_attributes(
        token: 'token',
        secret: 'secret'
      )
      @target.encrypt!
    end

    it "truncates the message to 140 characters" do
      message = "test message" * 500
      post_request =
        stub_request(:post, "https://api.twitter.com/1.1/statuses/update.json").
          with(body: { "status" => message.truncate(140) }).
          to_return(
            status: 200,
            body: read_test_file('twitter_status_1.1.json'),
            headers: { 'Content-Type' => 'application/json' }
          )

      @target.send_message_with_oauth(message)
      assert_requested post_request
    end

    it "updates the user's twitter stream with the message" do
      post_request =
        stub_request(:post, "https://api.twitter.com/1.1/statuses/update.json").
          with(body: {"status" => "test message"}).
          to_return(
            status: 200,
            body: read_test_file('twitter_status_1.1.json'),
            headers: { 'Content-Type' => 'application/json' }
          )
      @target.send_message_with_oauth("test message")
      assert_requested post_request
    end
  end

  describe "#authorized?" do
    it "does not query Twitter more than once per request" do
      @target.token = 'abc'
      @target.secret = 'def'
      @target.encrypt!
      get_request = stub_request(:get, "https://api.twitter.com/1.1/account/verify_credentials.json").
        to_return(
          status: 200,
          body: read_test_file('twitter_users_show_110542029_1.1.json'),
          headers: { 'Content-Type' => 'application/json' }
        )
      assert @target.authorized?
      assert_requested get_request
    end

    it "returns false if there is an api error" do
      @target.token = 'abc'
      @target.secret = 'def'
      @target.encrypt!
      get_request = stub_request(:get, "https://api.twitter.com/1.1/account/verify_credentials.json").
        to_return(status: 404, headers: { 'Content-Type' => 'application/json' })
      refute @target.authorized?
      assert_requested get_request
    end

    it "does not be authorized if token and secret not present" do
      refute @target.authorized?
    end

    it "does not be authorized if token not present" do
      @target.secret = 'secret'
      @target.encrypt!
      refute @target.authorized?
    end

    it "does not be authorized if secret not present" do
      @target.token = 'token'
      @target.encrypt!
      refute @target.authorized?
    end

    it "creates oauth client with empty string if there are no credentials" do
      refute @target.authorized?
    end
  end

  describe "#update_attributes" do
    it "preserves the token and the secret" do
      @target.token = '<token>'
      @target.secret = '<secret>'
      @target.encrypt!

      assert_equal("<token>", @target.token)
      assert_equal("<secret>", @target.secret)

      @target.update_attributes(callback_url: "http://woot.com")

      assert_equal("<token>", @target.token)
      assert_equal("<secret>", @target.secret)
    end

    it "does not blow up when updating other attributes" do
      @target.update_attributes(title: "foo")
      assert_equal("foo", @target.title)
    end
  end
end
