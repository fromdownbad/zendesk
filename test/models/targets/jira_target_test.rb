require_relative "../../support/test_helper"
require 'jira4r'
require 'jira4r/v2/jiraService'

SingleCov.covered! uncovered: 3

describe 'JiraTarget' do
  fixtures :accounts, :tickets, :events

  describe "Jira target" do
    before do
      @username = "user"
      @password = "password"
      @url = "http://jira.test.com"
      @target = JiraTarget.new(
        title: "JIRA Target", account: accounts(:minimum),
        username: @username, url: @url,
        password: @password
      )
      @target.encrypt!
      @account = accounts(:minimum)
      @event = events(:create_external_for_minimum_ticket_1)

      tickets(:minimum_1).jira_issues.clear

      @message = "I am a JIRA notification"
    end

    describe 'validations' do
      should_validate_presence_of :username
      should_validate_presence_of :password
      should_validate_presence_of :url

      it "is not valid if password is missing" do
        @target.password = nil
        refute @target.valid?
        assert @target.errors[:password].any?
      end
    end

    it "shoulds not support testing on the target configuration page" do
      refute @target.is_test_supported?
    end

    describe "#send_message" do
      it "does not send a message if no external ID or JIRA issue linking is present" do
        Jira4R::JiraTool.expects(:new).never
        Jira4R::JiraTool.any_instance.expects(:login).never
        @target.send_message(@message, @event)
      end

      it "sets protocol.http.ssl_config.verify_mode to nil" do
        @event.ticket.external_id = "FOO-100"
        comment = mock
        comment.expects(:body=).with(@message)
        jira = mock
        driver = stub(options: {})
        jira.expects(:driver).returns(driver)
        jira.expects(:login).with(@username, @password)
        jira.expects(:addComment).with(@event.ticket.external_id, comment)
        Jira4R::JiraTool.expects(:new).with(2, @url).returns(jira)
        Jira4R::V2::RemoteComment.expects(:new).returns(comment)
        @target.send_message(@message, @event)

        assert driver.options.keys.member?("protocol.http.ssl_config.verify_mode")
        assert driver.options["protocol.http.ssl_config.verify_mode"].nil?
      end

      it "connects with username and password using a JIRA issue linking if present and no external ID is present" do
        ExternalLink.create!(@event.ticket, type: "JiraIssue", issue_id: "FOO-100")
        comment = mock
        comment.expects(:body=).with(@message)
        jira = mock(driver: mock(options: {}))
        jira.expects(:login).with(@username, @password)
        jira.expects(:addComment).with(@event.ticket.jira_issues.first.issue_id, comment)
        Jira4R::JiraTool.expects(:new).with(2, @url).returns(jira)
        Jira4R::V2::RemoteComment.expects(:new).returns(comment)
        @target.send_message(@message, @event)
      end

      it "connects with username and password using the external ID if no JIRA issue linking is present" do
        @event.ticket.external_id = "FOO-100"
        comment = mock
        comment.expects(:body=).with(@message)
        jira = mock
        jira.expects(:driver).returns(mock(options: {}))
        jira.expects(:login).with(@username, @password)
        jira.expects(:addComment).with(@event.ticket.external_id, comment)
        Jira4R::JiraTool.expects(:new).with(2, @url).returns(jira)
        Jira4R::V2::RemoteComment.expects(:new).returns(comment)
        @target.send_message(@message, @event)
      end

      it "picks the JIRA issue linking over the external ID if both are present" do
        @event.ticket.external_id = "FOO-100"
        ExternalLink.create!(@event.ticket, type: "JiraIssue", issue_id: "FOO-PICK-ME-100")
        comment = mock
        comment.expects(:body=).with(@message)
        jira = mock
        jira.expects(:driver).returns(mock(options: {}))
        jira.expects(:login).with(@username, @password)
        jira.expects(:addComment).with(@event.ticket.jira_issues.first.issue_id, comment)
        Jira4R::JiraTool.expects(:new).with(2, @url).returns(jira)
        Jira4R::V2::RemoteComment.expects(:new).returns(comment)
        @target.send_message(@message, @event)
      end
    end
  end
end
