require_relative '../support/test_helper'

SingleCov.covered!

describe 'UserContactInformation' do
  fixtures :accounts, :users, :user_identities

  before do
    @account = accounts(:minimum)
    @user = users(:minimum_admin)
    @user_identity = @user.identities.first
  end

  it 'returns the last updated date' do
    inserted_data = UserContactInformation.create! do |new_contact_info|
      new_contact_info.account = @account
      new_contact_info.user_id = @user.id
      new_contact_info.email = @user_identity
      new_contact_info.name = @user.name
      new_contact_info.phone = nil
      new_contact_info.role = @user.roles
      new_contact_info.is_email_verified = true
    end
    result = UserContactInformation.last_updated_date(@account.id)
    assert_not_nil result
    assert_equal inserted_data.updated_at.to_s, result.to_s
  end

  it 'returns nil if the table doesn' do
    assert_nil UserContactInformation.last_updated_date(@account.id)
  end

  it 'returns the primary id if the user contact information exists' do
    inserted_data = UserContactInformation.create! do |new_contact_info|
      new_contact_info.account = @account
      new_contact_info.user_id = @user.id
      new_contact_info.email = @user_identity
      new_contact_info.name = @user.name
      new_contact_info.phone = nil
      new_contact_info.role = @user.roles
      new_contact_info.is_email_verified = true
    end
    result = UserContactInformation.get_user_contact_information_id(@account.id, @user.id)
    assert_not_nil result
    assert_equal inserted_data.id, result
  end

  it 'returns nil when user contact information is not present' do
    result = UserContactInformation.get_user_contact_information_id(@account.id, @user.id)
    assert_nil result
  end

  it "returns nil if the table doesn\'t have any records" do
    assert_nil UserContactInformation.last_updated_date(@account.id)
  end

  it 'returns the primary id if the user contact information exists' do
    inserted_data = UserContactInformation.create! do |new_contact_info|
      new_contact_info.account = @account
      new_contact_info.user_id = @user.id
      new_contact_info.email = @user_identity
      new_contact_info.name = @user.name
      new_contact_info.phone = nil
      new_contact_info.role = @user.roles
      new_contact_info.is_email_verified = true
    end
    result = UserContactInformation.get_user_contact_information_id(@account.id, @user.id)
    assert_not_nil result
    assert_equal inserted_data.id, result
  end

  it 'returns nil when user contact information is not present' do
    result = UserContactInformation.get_user_contact_information_id(@account.id, @user.id)
    assert_nil result
  end
end
