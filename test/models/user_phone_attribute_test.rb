require_relative "../support/test_helper"

SingleCov.covered!

describe UserPhoneAttribute do
  fixtures :users, :accounts

  before do
    Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
    Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true)
  end

  let(:user_phone_attribute) do
    UserPhoneNumberIdentity.create!(user: users(:minimum_end_user), value: "+15551234567x123").user_phone_extension
  end

  describe "scopes queries to account ID" do
    it "for user association" do
      # TODO: There seems to be a bug where instances of UserPhoneExtension are created with user_id: nil
      # See UserPhoneNumberIdentity#normalize and Users::PhoneNumberBehavior#add_shared_phone_number_extension
      # Work around this bug by setting user_id
      user_phone_attribute.update_attribute(:user_id, users(:minimum_end_user).id)

      assert_sql_queries(1, /`users`.`account_id` = #{user_phone_attribute.account_id}/) do
        user_phone_attribute.user
      end
    end

    it "for user_phone_number_identity association" do
      user_phone_attribute.reload

      assert_sql_queries(1, /`user_identities`.`account_id` = #{user_phone_attribute.account_id}/) do
        user_phone_attribute.user_phone_number_identity
      end
    end
  end
end
