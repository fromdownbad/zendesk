require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'UnverifiedEmailAddress' do
  fixtures :users, :accounts, :recipient_addresses, :account_property_sets, :subscriptions, :remote_authentications

  before do
    UserIdentity.without_arsi.delete_all # fixture cleanup
  end

  describe "Given a user" do
    before do
      @account = accounts(:minimum)
      @user = FactoryBot.create(:end_user, account: @account)
    end

    describe "validation" do
      let(:unverified_email_address) { UnverifiedEmailAddress.new(user: @user, email: email) }

      describe "when the unverified email address belongs to a non-mergeable entity" do
        let(:email) { FactoryBot.create(:verified_admin, account: @account).email }

        it "fails" do
          refute unverified_email_address.valid?
          assert_includes(
            unverified_email_address.errors.all_on(:email),
            "#{email} has already been taken"
          )
        end
      end

      describe "when the email is in use by the account" do
        let(:email) { @account.reply_address }

        it "fails" do
          refute unverified_email_address.valid?
          assert_includes(
            unverified_email_address.errors.all_on(:email),
            "#{email} cannot be used; it is in use as a support address"
          )
        end
      end
    end

    describe "When building an unverified email address" do
      before do
        @uea = UnverifiedEmailAddress.new(user: @user, email: 'email@example.com')
        Zendesk::UserPortalState.any_instance.stubs(:send_verify_email?).returns(true)
      end

      it "fails validation with a duplicate unverified email" do
        @uea.save!
        @uea2 = UnverifiedEmailAddress.new(user: @user, email: @uea.email)
        refute @uea2.valid?
        assert_match(/already been taken/i, @uea2.errors[:email].join)
      end

      it "delivers a verification on create by default" do
        ActionMailer::Base.perform_deliveries = true
        assert_difference "ActionMailer::Base.deliveries.size", 1 do
          @uea.save!
        end
      end

      it "does not deliver a verification on create if skip_verification" do
        @uea.skip_verification = true
        ActionMailer::Base.perform_deliveries = true
        refute_difference "ActionMailer::Base.deliveries.size" do
          @uea.save!
        end
      end

      it "does not deliver a verification if the account has no web_portal nor help center available" do
        ActionMailer::Base.perform_deliveries = true
        Zendesk::UserPortalState.any_instance.stubs(:send_verify_email?).returns(false)
        refute_difference "ActionMailer::Base.deliveries.size" do
          @uea.save!
        end
      end

      it "fails validation with an email already identifying the user" do
        @uea.user.reload
        @uea.email = @uea.user.email
        refute @uea.valid?
      end
    end

    describe "When creating an unverified email address" do
      before do
        @uea = UnverifiedEmailAddress.create!(user: @user, email: 'email@example.com')
      end

      it "has a token" do
        assert_not_nil(@uea.token)
      end

      it "has an account_id" do
        assert_equal(@user.account_id, @uea.account_id)
      end

      it "has an expires_at in the future" do
        assert @uea.expires_at > Time.now
      end
    end

    describe "When verifying an unverified email address" do
      before do
        @uea = UnverifiedEmailAddress.create!(user: @user, email: 'email@example.com')
      end

      describe "when the user is active and the email address does not already exist" do
        it "indicates that verification will not cause a merge" do
          refute @uea.will_merge_with_user.present?
        end

        it "gets a new verified UserEmailIdentity for the user" do
          @new_identity = nil
          assert_difference "UserEmailIdentity.count(:all)", 1 do
            @new_identity = @uea.verify!
          end
          new_identity = @user.identities(true).last
          assert_equal('email@example.com', new_identity.value)
          assert(new_identity.is_verified?)
          assert_equal(new_identity, @new_identity)
        end

        it "deletes the unverified identity" do
          assert_difference "UnverifiedEmailAddress.count(:all)", -1 do
            @uea.verify!
          end
        end

        describe 'when user primary email is unverified' do
          before do
            @user_to_be_verified = users(:minimum_agent)
            @unverified_identity = UserEmailIdentity.create!(user: @user_to_be_verified, account: @user_to_be_verified.account, value: "bar@example.com")
            @uea_primary = UnverifiedEmailAddress.create!(user: @user_to_be_verified, email: 'email@example.com')
          end

          it 'makes primary the new verified email' do
            email = @uea_primary.email

            refute @unverified_identity.is_verified?
            assert_not_equal @uea_primary.email, @unverified_identity.value

            @uea_primary.verify!

            identity = @user_to_be_verified.reload.identities.email.first
            assert identity.primary?
            assert identity.is_verified?
            assert_equal email, identity.value
          end
        end

        describe 'when user primary email is verified' do
          before do
            @user_with_verified = users(:minimum_agent)
            @verified_identity = UserEmailIdentity.create!(user: @user_with_verified, account: @user_with_verified.account, value: "bar@example.com")
            @verified_identity.verify!
            @uea_non_primary = UnverifiedEmailAddress.create!(user: @user_with_verified, email: 'email@example.com')
          end

          it 'does not change the primary email' do
            email = @uea_non_primary.email

            assert @verified_identity.is_verified?
            assert_not_equal @uea_non_primary.email, @verified_identity.value

            @uea_non_primary.verify!

            identity = @user_with_verified.identities.email.first
            assert identity.primary?
            assert identity.is_verified?
            assert_not_equal email, identity.value
          end
        end
      end

      describe "when the user is inactive" do
        before { @user.stubs(:is_active).returns(false) }
        it "does not verify" do
          refute_difference "UnverifiedEmailAddress.count(:all)" do
            refute_difference "UserEmailIdentity.count(:all)" do
              refute @uea.verify!
            end
          end
        end
      end

      describe "And When the email already exists on another user" do
        before do
          @user2 = FactoryBot.create(:end_user, account: accounts(:minimum), email: 'email@example.com')
        end

        it "indicates that verification will cause a merge" do
          assert @uea.will_merge_with_user.present?
        end

        it "merges the two users when confirmed" do
          UserMergeJob.expects(:enqueue).with(@user.account_id, @user.id, @user2.id)
          assert_difference "UnverifiedEmailAddress.count(:all)", -1 do
            assert @uea.verify!(true)
          end
        end

        it "moves the identity when confirmed" do
          refute_difference "UserIdentity.count(:all)" do
            assert @uea.verify!(true)
          end
          @user2.reload
          assert_equal(0, @user2.identities.email.size)
          @user.reload
          assert @user.identities.email.map(&:value).include?('email@example.com')
        end

        it "Nots change anything when not confirmed" do
          refute_difference "UnverifiedEmailAddress.count(:all)" do
            refute @uea.verify!
          end
          @user2.reload
          assert_equal(1, @user2.identities.email.size)
          @user.reload
          refute @user.identities.email.map(&:value).include?('email@example.com')
        end
      end
    end
  end

  describe "Given a non-end-user user" do
    before do
      @admin = FactoryBot.create(:verified_admin, account: accounts(:minimum))
    end

    describe "And the email they want is already in use" do
      before do
        @user = FactoryBot.create(:user, email: 'wantedemail@example.com', account: accounts(:minimum))
      end

      it "fails validation with a duplicate email" do
        @uea = UnverifiedEmailAddress.new(user: @admin, email: @user.email)
        refute @uea.valid?
        assert_match(/already been taken/i, @uea.errors[:email].join)
      end
    end
  end
end
