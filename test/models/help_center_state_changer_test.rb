require_relative "../support/test_helper"

SingleCov.covered!

describe 'HelpCenterStateChanger' do
  fixtures :accounts, :brands

  let(:account) { accounts(:minimum) }
  let(:brand) { brands(:minimum) }
  let(:changer) { HelpCenterStateChanger.new(account: account, brand: brand) }

  before do
    HelpCenter.create(
      id: 42,
      state: 'enabled',
      account_id: account.id,
      brand_id: brand.id
    )
  end

  describe '#update' do
    it "will set Account HC state to enabled, and Brand HC state to restricted" do
      changer.update(
        account_help_center_state: 'enable',
        brand_help_center_state: 'restricted',
        help_center_id: 42
      )

      assert_equal :enabled, account.reload.help_center_state
      assert HelpCenter.find_by_brand_id(brand.id).restricted?
      assert_equal 42, HelpCenter.find_by_brand_id(brand.id).id
    end

    it "will set Account HC state to disabled, and Brand HC state to restricted" do
      changer.update(
        account_help_center_state: 'disable',
        brand_help_center_state: 'restricted',
        help_center_id: 42
      )

      assert_equal :disabled, account.reload.help_center_state
      assert HelpCenter.find_by_brand_id(brand.id).restricted?
      assert_equal 42, HelpCenter.find_by_brand_id(brand.id).id
    end

    it "will set Account HC state to restricted, and Brand HC state to restricted" do
      changer.update(
        account_help_center_state: 'restrict',
        brand_help_center_state: 'restricted',
        help_center_id: 42
      )

      assert_equal :restricted, account.reload.help_center_state
      assert HelpCenter.find_by_brand_id(brand.id).restricted?
    end

    it "will set Account HC state to enabled, and Brand HC state to enabled" do
      changer.update(
        account_help_center_state: 'enable',
        brand_help_center_state: 'enabled',
        help_center_id: 42
      )

      assert_equal :enabled, account.reload.help_center_state
      assert HelpCenter.find_by_brand_id(brand.id).enabled?
    end

    it "will set Account HC state to disabled, and Brand HC state to enabled" do
      changer.update(
        account_help_center_state: 'disable',
        brand_help_center_state: 'enabled',
        help_center_id: 42
      )

      assert_equal :disabled, account.reload.help_center_state
      assert HelpCenter.find_by_brand_id(brand.id).enabled?
    end

    it "will set Account HC state to restricted, and Brand HC state to enabled" do
      changer.update(
        account_help_center_state: 'restrict',
        brand_help_center_state: 'enabled',
        help_center_id: 42
      )

      assert_equal :restricted, account.reload.help_center_state
      assert HelpCenter.find_by_brand_id(brand.id).enabled?
    end

    it "will set Account HC state to enabled, and Brand HC state to archived" do
      changer.update(
        account_help_center_state: 'enable',
        brand_help_center_state: 'archived',
        help_center_id: 42
      )

      assert_equal :enabled, account.reload.help_center_state
      assert HelpCenter.find_by_brand_id(brand.id).archived?
    end

    it "will set Account HC state to disabled, and Brand HC state to archived" do
      changer.update(
        account_help_center_state: 'disable',
        brand_help_center_state: 'archived',
        help_center_id: 42
      )

      assert_equal :disabled, account.reload.help_center_state
      assert HelpCenter.find_by_brand_id(brand.id).archived?
    end

    it "will set Account HC state to restricted, and Brand HC state to archived" do
      changer.update(
        account_help_center_state: 'restrict',
        brand_help_center_state: 'archived',
        help_center_id: 42
      )

      assert_equal :restricted, account.reload.help_center_state
      assert HelpCenter.find_by_brand_id(brand.id).archived?
    end

    it "cannot change an Account's Help Center state to an unknown state" do
      assert_raises(ArgumentError) do
        changer.update(
          account_help_center_state: 'sold',
          brand_help_center_state: 'enabled',
          help_center_id: 42
        )
      end
    end

    it "cannot change an Help Center to an unknown state" do
      assert_raises(ArgumentError) do
        changer.update(
          account_help_center_state: 'enable',
          brand_help_center_state: 'sold',
          help_center_id: 42
        )
      end
    end

    describe "when HelpCenter row does not exist" do
      it "will create the Brand HC record with correct state" do
        HelpCenter.destroy_by_brand_id(brand.id)

        changer.update(
          account_help_center_state: 'enable',
          brand_help_center_state: 'restricted',
          help_center_id: 4000
        )

        assert_equal :enabled, account.reload.help_center_state
        assert HelpCenter.find_by_brand_id(brand.id).restricted?
        assert_equal 4000, HelpCenter.find_by_brand_id(brand.id).id
      end
    end
  end

  describe '#destroy' do
    # when a Help Center is being destroyed
    it "removes the HelpCenter record" do
      refute_nil HelpCenter.find_by_brand_id(brand.id)

      changer.destroy(account_help_center_state: 'enable')

      assert_equal :enabled, account.reload.help_center_state
      assert_nil HelpCenter.find_by_brand_id(brand.id)
    end

    it "is idempotent (can be called twice)" do
      refute_nil HelpCenter.find_by_brand_id(brand.id)

      changer.destroy(account_help_center_state: 'enable')
      changer.destroy(account_help_center_state: 'enable')

      assert_equal :enabled, account.reload.help_center_state
      assert_nil HelpCenter.find_by_brand_id(brand.id)
    end
  end
end
