require_relative "../../support/test_helper"

SingleCov.covered!

describe AnswerBot::Webform do
  fixtures :accounts, :tickets

  let(:account) { accounts(:multiproduct) }
  let(:ticket) { tickets(:minimum_1) }
  let(:webform) { AnswerBot::Webform.new(account: account, ticket: ticket) }
  let(:help_center_client) { Zendesk::HelpCenter::InternalApiClient.any_instance }

  before do
    help_center_client.stubs(:answer_bot_setting_enabled?).returns(true)
    help_center_client.stubs(:answer_bot_form_setting_enabled?).returns(true)
  end

  describe '#infer_already_deflected?' do
    describe 'when via id is webform' do
      before { ticket.stubs(:via_id).returns(ViaType.WEB_FORM) }

      it 'fetches the answer bot setting from help center' do
        help_center_client.expects(:answer_bot_setting_enabled?)

        webform.infer_already_deflected?
      end

      describe 'when the account has a default ticket form' do
        before { ticket.stubs(:ticket_form_id).returns(123) }

        it 'fetches the answer bot form setting from help center with the ticket form id' do
          help_center_client.expects(:answer_bot_form_setting_enabled?).
            with(brand_id: ticket.brand_id, form_id: ticket.ticket_form_id)

          webform.infer_already_deflected?
        end
      end

      describe 'when the account does not have a default ticket form' do
        let(:null_ticket_form_id) { AnswerBot::Webform::NULL_TICKET_FORM_ID }

        before { ticket.stubs(:ticket_form_id).returns(nil) }

        it 'fetches the answer bot form setting from help center with the null ticket form id' do
          help_center_client.expects(:answer_bot_form_setting_enabled?).
            with(brand_id: ticket.brand_id, form_id: null_ticket_form_id)

          webform.infer_already_deflected?
        end
      end

      describe 'and the answer bot setting is enabled' do
        describe 'and the answer bot form setting is enabled' do
          it 'returns true' do
            assert webform.infer_already_deflected?
          end
        end

        describe 'and the answer bot form setting is disabled' do
          before { help_center_client.stubs(:answer_bot_form_setting_enabled?).returns(false) }

          it 'returns false' do
            refute webform.infer_already_deflected?
          end
        end
      end

      describe 'and the answer bot form setting is disabled' do
        before do
          help_center_client.stubs(:answer_bot_setting_enabled?).returns(false)
        end

        it 'returns false' do
          refute webform.infer_already_deflected?
        end
      end
    end

    describe 'when via id is not webform' do
      before { ticket.stubs(:via_id).returns(ViaType.MAIL) }

      it 'returns false' do
        refute webform.infer_already_deflected?
      end
    end
  end
end
