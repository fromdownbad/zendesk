require_relative "../../support/test_helper"

SingleCov.covered!

describe AnswerBot::EventCreator do
  fixtures :tickets, :users, :accounts, :rules

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:user) { users(:minimum_agent) }
  let(:article_content) do
    {
      id: 82,
      title: 'title',
      url: 'http://www.url.com',
      html_url: 'http://www.html-url.com',
      body: 'Mobile phones have greatly reduced bathroom graffiti.',
      locale: 'en-us'
    }
  end

  before do
    ticket.account_id = account.id
    ticket.will_be_saved_by(user)
    ticket.save!
  end

  describe '#send_event' do
    let(:articles) { [article_content, article_content] }
    let(:event) { AnswerBot::EventCreator.send_event(articles) }

    describe 'when articles exist' do
      it 'returns a send event' do
        assert_equal 'AutomaticAnswerSend', event.type
      end

      it 'sets the value to the suggested articles' do
        assert_equal articles, event.value.fetch(:articles)
      end
    end

    describe 'when articles are empty' do
      let(:articles) { [] }

      it 'returns nil' do
        assert_nil event
      end
    end
  end

  describe '#email_notification_event' do
    let(:audit) { ticket.audits.first }
    let(:requester_id) { ticket.requester.id }
    let(:rule_id) { 123 }
    let(:subject) { 'This is a subject' }
    let(:body) { 'This is a body' }
    let(:event) { AnswerBot::EventCreator.email_notification_event(audit, requester_id, rule_id, subject, body) }

    before do
      DeflectionMailer.stubs(:deliver_answer_bot_email)
    end

    describe 'when valid parameters exist' do
      it 'returns an email notification event' do
        assert_equal 'AnswerBotNotification', event.type
      end

      it 'sets the event via id to Rule' do
        assert_equal ViaType.RULE, event.via_id
      end

      it 'sets the event via reference id to the rule id' do
        assert_equal rule_id, event.via_reference_id
      end
    end

    describe 'when an audit does not exist' do
      let(:audit) { nil }
      it { assert_nil event }
    end

    describe 'when a requester id does not exist' do
      let(:requester_id) { nil }
      it { assert_nil event }
    end

    describe 'when a rule id does not exist' do
      let(:rule_id) { nil }
      it { assert_nil event }
    end

    describe 'when a subject does not exist' do
      let(:subject) { nil }
      it { assert_nil event }
    end

    describe 'when a body does not exist' do
      let(:body) { nil }
      it { assert_nil event }
    end
  end

  describe '#email_notification_with_ccs_event' do
    let(:audit) { ticket.audits.first }
    let(:requester_id) { ticket.requester.id }
    let(:rule_id) { 123 }
    let(:subject) { 'This is a subject' }
    let(:body) { 'This is a body' }
    let(:ccs) { ticket.email_ccs }
    let(:notification_event) { { audit: audit, recipient_id: requester_id, rule_id: rule_id, subject: subject, body: body, recipient_ccs: ccs } }
    let(:event) { AnswerBot::EventCreator.email_notification_with_ccs_event(notification_event) }

    before do
      DeflectionMailer.stubs(:deliver_answer_bot_email)
    end

    describe 'when valid parameters exist' do
      it 'returns an email notification event' do
        assert_equal 'AnswerBotNotification', event.type
      end

      it 'sets the event via id to Rule' do
        assert_equal ViaType.RULE, event.via_id
      end

      it 'sets the event via reference id to the rule id' do
        assert_equal rule_id, event.via_reference_id
      end
    end

    describe 'when an audit does not exist' do
      let(:audit) { nil }
      it { assert_nil event }
    end

    describe 'when a requester id does not exist' do
      let(:requester_id) { nil }
      it { assert_nil event }
    end

    describe 'when a rule id does not exist' do
      let(:rule_id) { nil }
      it { assert_nil event }
    end

    describe 'when a subject does not exist' do
      let(:subject) { nil }
      it { assert_nil event }
    end

    describe 'when a body does not exist' do
      let(:body) { nil }
      it { assert_nil event }
    end
  end

  describe '#solve_event' do
    let(:event) { AnswerBot::EventCreator.solve_event(article_content) }

    describe 'when an article exists' do
      it 'returns a solve event' do
        assert_equal 'AutomaticAnswerSolve', event.type
      end

      it 'sets the value to the article provided' do
        assert_equal article_content, event.value.fetch(:solved_article)
      end
    end

    describe 'when article is nil' do
      it 'returns nil' do
        assert_nil AnswerBot::EventCreator.solve_event(nil)
      end
    end
  end

  describe '#reject_event' do
    let(:article) { article_content.slice(:id, :html_url, :url, :title) }
    let(:reviewer_id) { 3 }
    let(:reviewer_name) { 'John Doe' }
    let(:irrelevant) { TicketDeflectionArticle::REASON_IRRELEVANT }
    let(:reason) { TicketDeflectionArticle::REASON_STRINGS[irrelevant] }
    let(:event) do
      AnswerBot::EventCreator.reject_event(
        article: article,
        reviewer_id: reviewer_id,
        reviewer_name: reviewer_name,
        irrelevant: irrelevant,
        reason: reason
      )
    end

    it 'returns a reject event object' do
      assert_equal 'AutomaticAnswerReject', event.type
    end

    describe 'sets the value of' do
      it 'article' do
        assert_equal article, event.article
      end

      it 'reviewer id' do
        assert_equal reviewer_id, event.reviewer_id
      end

      it 'reviewer_name' do
        assert_equal reviewer_name, event.reviewer_name
      end

      it 'irrelevant' do
        assert_equal irrelevant, event.irrelevant
      end

      it 'reason' do
        assert_equal reason, event.reason
      end
    end
  end
end
