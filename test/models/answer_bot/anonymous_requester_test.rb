require_relative "../../support/test_helper"

SingleCov.covered!

describe AnswerBot::AnonymousRequester do
  fixtures :accounts, :users, :user_settings

  let(:account) { accounts(:minimum_answer_bot) }
  let!(:user) { users(:minimum_answer_bot_user) }
  let!(:settings) { user_settings(:minimum_answer_bot_user_1) }
  let(:answer_bot_user_count) do
    lambda { account.reload.answer_bot_users.count }
  end

  describe '.find_or_create' do
    describe 'when an anonymous user exists' do
      it 'returns the existing user' do
        assert_equal user, AnswerBot::AnonymousRequester.find_or_create(account: account)
      end

      it 'does not create a new user' do
        assert_difference answer_bot_user_count, 0 do
          AnswerBot::AnonymousRequester.find_or_create(account: account)
        end
      end
    end

    describe 'when an anonymous user does not exist' do
      before { account.answer_bot_users.destroy_all }

      it 'creates a new user' do
        assert_difference answer_bot_user_count, 1 do
          AnswerBot::AnonymousRequester.find_or_create(account: account)
        end
      end

      describe 'creates a user with' do
        let(:answer_bot_user) { account.answer_bot_users.first }

        before { AnswerBot::AnonymousRequester.find_or_create(account: account) }

        it 'an anonymous requester name' do
          assert_equal AnswerBot::AnonymousRequester::NAME, answer_bot_user.name
        end

        it 'the answer bot setting enabled' do
          assert answer_bot_user.answer_bot?
        end
      end
    end
  end
end
