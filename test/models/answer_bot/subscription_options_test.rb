require_relative '../../support/test_helper'
SingleCov.covered!

describe AnswerBot::SubscriptionOptions do
  let(:described_class) { AnswerBot::SubscriptionOptions }
  let(:account)         { accounts(:minimum) }

  let(:support_params) do
    {
      max_agents:             5,
      billing_cycle_type:     4,
      plan_type:              3,
      pricing_model_revision: 7
    }
  end

  let(:subscription_options) do
    ZBC::Zuora::SubscriptionOptions.new(account.id, params)
  end

  describe '::build_for' do
    subject { described_class.build_for(account, subscription_options) }

    describe 'Answer Bot with 0 resolutions passed as option' do
      let(:params) do
        support_params.merge(
          answer_bot_max_resolutions: 0,
          answer_bot_plan_type:       1
        )
      end

      describe 'not subscribed to Answer Bot' do
        let(:answer_bot_subscription) { stub(subscribed?: false) }

        before do
          AnswerBot::Subscription.expects(:find_for_account).
            returns(answer_bot_subscription)
        end

        it 'removes answer bot from options' do
          assert_nil subject.answer_bot_max_resolutions
          assert_nil subject.answer_bot_plan_type
        end
      end

      describe 'subscribed to Answer Bot' do
        let(:answer_bot_subscription) { stub(subscribed?: true) }

        before do
          AnswerBot::Subscription.expects(:find_for_account).
            returns(answer_bot_subscription)
        end

        it 'does not touch the options' do
          assert_equal 0, subject.answer_bot_max_resolutions
        end
      end
    end

    describe 'Answer Bot with > 0 resolutions passed as option' do
      let(:max_resolutions) { 50 }

      let(:answer_bot_params) do
        support_params.merge(
          answer_bot_max_resolutions: max_resolutions,
          answer_bot_plan_type:       1
        )
      end

      describe 'and guide enterprise subscription passed as option' do
        let(:params) do
          answer_bot_params.merge(
            guide_plan_type: 4,
            guide_max_agents: 5
          )
        end

        it 'does not touch the options' do
          assert_equal max_resolutions, subject.answer_bot_max_resolutions
        end

        describe 'invalid max resolutions' do
          let(:max_resolutions) { 666 }

          it 'raises InvalidMaxResolutions' do
            assert_raises(AnswerBot::SubscriptionOptions::InvalidMaxResolutions) { subject }
          end
        end
      end

      describe 'and guide pro subscription passed as option' do
        let(:params) do
          answer_bot_params.merge(
            guide_plan_type: 2,
            guide_max_agents: 5
          )
        end

        it 'does not touch the options' do
          assert_equal max_resolutions, subject.answer_bot_max_resolutions
        end

        describe 'invalid max resolutions' do
          let(:max_resolutions) { 666 }

          it 'raises InvalidMaxResolutions' do
            assert_raises(AnswerBot::SubscriptionOptions::InvalidMaxResolutions) { subject }
          end
        end
      end

      describe 'and guide lite subscription passed as option' do
        let(:params) do
          answer_bot_params.merge(
            guide_plan_type: 1,
            guide_max_agents: 5
          )
        end

        it 'raises InvalidGuidePlan' do
          assert_raises(AnswerBot::SubscriptionOptions::InvalidGuidePlan) { subject }
        end
      end

      describe 'and guide legacy (no guide params passed in options)' do
        let(:guide_subscription) { stub(legacy?: true) }

        let(:params) { answer_bot_params }

        before do
          account.subscription.
            expects(:guide_preview).
            returns(guide_subscription)
        end

        it 'does not touch the options' do
          assert_equal max_resolutions, subject.answer_bot_max_resolutions
        end

        describe 'invalid max resolutions' do
          let(:max_resolutions) { 666 }

          it 'raises InvalidMaxResolutions' do
            assert_raises(AnswerBot::SubscriptionOptions::InvalidMaxResolutions) { subject }
          end
        end
      end

      describe 'no guide legacy, no guide params passed in options' do
        let(:guide_subscription) { stub(legacy?: false) }

        let(:params) { answer_bot_params }

        before do
          account.subscription.
            expects(:guide_preview).
            returns(guide_subscription)
        end

        it 'raises InvalidGuidePlan' do
          assert_raises(AnswerBot::SubscriptionOptions::InvalidGuidePlan) { subject }
        end
      end
    end
  end
end
