require_relative "../../support/test_helper"

SingleCov.covered!

describe AnswerBot::Deflectability do
  fixtures :accounts, :tickets, :ticket_deflections

  let(:account) { accounts(:multiproduct) }
  let(:ticket) { tickets(:minimum_1) }
  let(:account_id) { account.id }
  let(:deflectability) { AnswerBot::Deflectability.new(account: account, ticket: ticket) }

  describe '#deflectable_via_email?' do
    describe 'when the ticket does not exist' do
      let(:ticket) { nil }

      it 'returns false' do
        refute deflectability.deflectable_via_email?
      end
    end

    describe 'and the ticket was not deflected by another channel' do
      before do
        deflectability.stubs(:deflected_by_another_channel?).returns(false)
      end

      it 'returns true' do
        assert deflectability.deflectable_via_email?
      end
    end

    describe 'and the ticket was deflected by another channel' do
      before do
        deflectability.stubs(:deflected_by_another_channel?).returns(true)
      end

      it 'returns false' do
        refute deflectability.deflectable_via_email?
      end
    end

    describe 'and the ticket is a follow up' do
      before { ticket.stubs(:followup_source).returns(tickets(:minimum_2)) }

      it 'returns false' do
        refute deflectability.deflectable_via_email?
      end
    end
  end

  describe '#deflected_by_another_channel?' do
    let(:webform) { AnswerBot::Webform.any_instance }

    describe 'when a previous webform deflection is inferred as true' do
      before { webform.stubs(:infer_already_deflected?).returns(true) }

      it 'returns true' do
        assert deflectability.deflected_by_another_channel?
      end
    end

    describe 'when a previous webform deflection is inferred as false' do
      before { webform.stubs(:infer_already_deflected?).returns(false) }

      it 'returns false' do
        refute deflectability.deflected_by_another_channel?
      end
    end
  end
end
