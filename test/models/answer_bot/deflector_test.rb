require_relative '../../support/test_helper'
require_relative "../../support/collaboration_settings_test_helper"
SingleCov.covered!

describe AnswerBot::Deflector do
  fixtures :accounts, :tickets, :events

  let(:account) { accounts(:minimum) }
  let(:ticket) do
    Ticket.new(account: account, description: 'Old Ticket').tap do |ticket|
      ticket.requester = requester
      ticket.set_collaborators_with_type(all_cc_users.map(&:id), CollaboratorType.EMAIL_CC)
      ticket.will_be_saved_by(agent, via_id: ViaType.MAIL)
      ticket.save!
    end
  end
  let(:requester) { users(:minimum_end_user) }
  let(:agent) { users(:minimum_agent) }
  let(:filtered_user) do
    user = users(:minimum_end_user2)
    user.email = 'example@example.com'
    user.identities.first.mark_as_machine
    user.save!
    user
  end
  let(:valid_cc_users) do
    [
      users(:minimum_admin),
      agent
    ]
  end
  let(:invalid_cc_users) do
    [
      filtered_user,
      users(:inactive_user)
    ]
  end
  let(:all_cc_users) { valid_cc_users + invalid_cc_users }
  let(:rule) { account.rules.find_by(title: 'Deflection via Answer Bot') }
  let(:rule_id) { rule.id }
  let(:subject) { 'subject' }
  let(:body) { 'body' }
  let(:labels_for_filter) { ['foo', 'bar'] }
  let(:answer_bot_service_client) { stub }
  let(:deflector) { AnswerBot::Deflector.new(account) }
  let(:deflection_channel_id) { ViaType.MAIL }
  let(:event) { ticket.audits.first.events.to_a.select { |e| e.type == "AnswerBotNotification" } }

  describe '#deflect' do
    before do
      AnswerBot::Deflectability.any_instance.stubs(:deflectable_via_email?).returns(true)
      Zendesk::AnswerBotService::InternalApiClient.stubs(:new).with(account).returns(answer_bot_service_client)
      AnswerBot::Trigger.stubs(:config).with(account, rule_id).returns(subject_template: subject, body_template: body, labels_for_filter: labels_for_filter)
    end

    it 'deflects via baa' do
      answer_bot_service_client.expects(:deflect).with(
        ticket: ticket,
        label_names: labels_for_filter,
        deflection_channel_id: deflection_channel_id,
        deflection_channel_reference_id: rule_id
      )
      deflector.deflect(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: deflection_channel_id)
    end

    describe 'email notification event creation' do
      def notification_event
        ticket.reload
        ticket.audits.first.events.to_a.select { |e| e.type == "AnswerBotNotification" }.count
      end

      describe 'when valid parameters rule_id, subject, and body are provided' do
        it 'creates a deflection email notification event' do
          assert_difference lambda { notification_event }, 1 do
            answer_bot_service_client.stubs(:deflect)
            deflector.deflect(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: deflection_channel_id)
          end
        end

        describe 'when the ticket has email CCs' do
          before do
            CollaborationSettingsTestHelper.enable_all_new_collaboration_settings

            answer_bot_service_client.stubs(:deflect)
          end

          describe 'when rule does not include requester_and_ccs' do
            before do
              answer_bot_service_client.stubs(:deflect)
              deflector.deflect(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: deflection_channel_id)
              ticket.reload
            end

            it 'does not send to ticket ccs' do
              event = ticket.audits.first.events.to_a.select { |e| e.type == "AnswerBotNotification" }
              all_cc_users.each { |cc| refute_includes event.first.value, cc.id }
            end
          end

          describe 'when rule includes requester_and_ccs' do
            let(:rule) { account.rules.find_by(title: "Deflection via Answer Bot with CCs") }
            let(:rule_id) { rule.id }

            before do
              deflector.deflect(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: deflection_channel_id)
              ticket.reload
            end

            it 'includes email CCs in the notification event' do
              event = ticket.audits.first.events.to_a.select { |e| e.type == "AnswerBotNotification" }
              assert_includes event.first.value, requester.id
              valid_cc_users.each { |cc| assert_includes event.first.value, cc.id }
            end

            it 'does not send deflection to users that should be filtered' do
              event = ticket.audits.first.events.to_a.select { |e| e.type == "AnswerBotNotification" }
              invalid_cc_users.each { |cc| refute_includes event.first.value, cc.id }
            end
          end
        end
      end

      describe 'when invalid parameters are provided' do
        before do
          AnswerBot::Trigger.stubs(:config).with(account, rule_id).returns(body_template: body, labels_for_filter: labels_for_filter)
        end

        it 'does not create a deflection email notification event' do
          assert_no_difference lambda { notification_event } do
            answer_bot_service_client.stubs(:deflect)
            deflector.deflect(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: deflection_channel_id)
          end
        end
      end
    end

    describe 'when account has reached rate limit for ticket deflection' do
      before do
        account_id = account.id.to_s
        Prop.stubs(:throttle).with(:ticket_deflection_account, account_id).returns(true)
      end

      it 'does not deflect' do
        answer_bot_service_client.expects(:deflect).never
        deflector.deflect(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: deflection_channel_id)
      end
    end

    describe 'when account or ticket is not deflectable via email' do
      before do
        AnswerBot::Deflectability.any_instance.stubs(:deflectable_via_email?).returns(false)
      end

      it 'does not deflect' do
        answer_bot_service_client.expects(:deflect).never
        deflector.deflect(ticket_id: ticket.id, rule_id: rule_id, deflection_channel_id: deflection_channel_id)
      end
    end
  end
end
