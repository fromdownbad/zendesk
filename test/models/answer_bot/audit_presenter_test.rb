require_relative '../../support/test_helper'

SingleCov.covered!

describe AnswerBot::AuditPresenter do
  let(:account) { stub('account') }
  let(:brand) { stub('brand', url: 'https://support.zd-dev.com') }
  let(:ticket_deflection_articles) do
    [
      stub('ticket_deflection_article', article_id: 111, locale: 'en-us', brand: brand)
    ]
  end

  let(:deflection) do
    mock(
      'TicketDeflection',
      account: account,
      brand: brand,
      ticket_deflection_articles: ticket_deflection_articles
    )
  end
  let(:requester) { mock('User') }
  let(:hc_articles) { [] }
  let(:api_client) do
    stub('api client', fetch_articles_for_ids_and_locales: hc_articles)
  end
  let(:presenter) do
    AnswerBot::AuditPresenter.new(
      deflection: deflection,
      requester: requester
    )
  end

  before do
    Zendesk::HelpCenter::InternalApiClient.stubs(:new).returns(api_client)
  end

  describe '#present' do
    it 'invokes Help Center API with correct arguments' do
      Zendesk::HelpCenter::InternalApiClient.expects(:new).with(
        account: account,
        requester: requester,
        brand: brand
      ).returns(api_client)

      presenter.present
    end

    describe 'when Help Center request is successful' do
      let(:hc_articles) do
        [
          {
            id: 111,
            title: 'title',
            url: 'http://example.com',
            html_url: 'http://example.com',
          }
        ]
      end

      it 'decorates the response with title and url from help center' do
        assert_equal hc_articles, presenter.present
      end
    end

    describe 'when Help Center request failed' do
      let(:hc_articles) { [] }
      let(:expected_suggested_articles) do
        [
          {
            id: 111,
            title: 'https://support.zd-dev.com/hc/en-us/articles/111',
            url: 'https://support.zd-dev.com/hc/en-us/articles/111',
            html_url: 'https://support.zd-dev.com/hc/en-us/articles/111'
          }
        ]
      end

      it 'falls back to auto generated title' do
        assert_equal expected_suggested_articles, presenter.present
      end
    end
  end
end
