require_relative '../../support/test_helper'
SingleCov.covered!

describe AnswerBot::RequestSolver do
  let(:article_id) { 111 }
  let(:deflection_id) { 333444 }
  let(:subdomain) { 'foo' }
  let(:resolution_channel_id) { ViaType.WEB_WIDGET }
  let(:deflection_channel_id) { ViaType.WEB_FORM }
  let(:via_id) { ViaType.WEB_FORM }
  let(:anonymous_requester) { stub('user', id: 777, name: AnswerBot::AnonymousRequester::NAME, answer_bot?: true) }
  let(:account) do
    stub('account', tickets: stub('new': ticket))
  end
  let(:brand) do
    stub('brand', url: 'https://support.zd-dev.com', subdomain: subdomain)
  end
  let(:solved_article) do
    {
      id: article_id,
      title: 'title',
      url: 'http://example.com',
      html_url: 'http://example.com'
    }
  end
  let(:unsolved_article) do
    {
      id: 222,
      title: 'title',
      url: 'http://example.com',
      html_url: 'http://example.com'
    }
  end
  let(:presented_articles) { [solved_article, unsolved_article] }

  let(:deflection) do
    suggested_articles = [
      stub('ticket_deflection_article', article_id: article_id, locale: 'en-us', brand: brand),
      stub('ticket_deflection_article', article_id: 222, locale: 'en-us', brand: brand)
    ]

    stub(
      'ticket_deflection',
      id: deflection_id,
      via_id: via_id,
      deflection_channel_id: deflection_channel_id,
      brand: brand,
      account: account,
      solve_with_article: nil,
      solve_with_article_no_via_id: nil,
      ticket: nil,
      'update_attributes!': true,
      ticket_deflection_articles: suggested_articles,
      detected_locale: 'en',
      model_version: '40100',
      deconstruct_enquiry: { subject: 'Kittens', description: 'are cute' }
    )
  end
  let(:ticket) do
    stub_everything(
      'ticket',
      will_be_saved_by: nil,
      audit: stub(events: []),
      set_nice_id: 1000,
      solve: true,
      'save!': true
    )
  end
  let(:solver) do
    AnswerBot::RequestSolver.new(
      deflection_id: deflection_id,
      article_id: article_id,
      resolution_channel_id: resolution_channel_id
    )
  end
  let(:send_event) { stub('AutomaticAnswerSend') }
  let(:solve_event) { stub('AutomaticAnswerSolve') }
  let(:presenter) { stub('AnswerBot::SuggestedArticlesPresenter', present: presented_articles) }

  before do
    TicketDeflection.stubs(:find).with(deflection_id).returns(deflection)
    AnswerBot::AuditPresenter.stubs(:new).returns(presenter)

    AnswerBot::EventCreator.stubs(:send_event).returns(send_event)
    AnswerBot::EventCreator.stubs(:solve_event).returns(solve_event)
    AnswerBot::AnonymousRequester.stubs(:find_or_create).returns(anonymous_requester)
  end

  describe '#solve_deflection' do
    let(:deflection_channel_id) { ViaType.ANSWER_BOT_FOR_SLACK }
    let(:resolution_channel_id) { ViaType.ANSWER_BOT_FOR_SLACK }
    let(:via_id) { nil }

    it 'updates deflection to solve' do
      deflection.expects(:solve_with_article_no_via_id).
        with(article_id, resolution_channel_id)

      solver.solve_deflection
    end

    describe 'datadog stats' do
      it 'has basic tags' do
        ab_statsd_client = stub('Datadog::Answerbot', solved: nil)
        Datadog::AnswerBot.
          expects(:new).
          with(
            via_id: via_id,
            subdomain: subdomain,
            deflection_channel_id: deflection_channel_id,
            resolution_channel_id: resolution_channel_id
          ).
          once.
          returns(ab_statsd_client)

        solver.solve_deflection
      end

      it 'records solved stats' do
        Datadog::AnswerBot.any_instance.
          expects(:solved).
          with(model_version: '40100', language: 'en').
          once

        solver.solve_deflection
      end
    end
  end

  describe '#create_and_solve_ticket' do
    it 'creates ticket' do
      ticket.expects(:save!)
      assert_equal(ticket, solver.create_and_solve_ticket)
    end

    it 'presents suggested articles via presenter' do
      presenter.expects(:present)
      solver.create_and_solve_ticket
    end

    it 'adds send event with the list of suggested articles' do
      AnswerBot::EventCreator.expects(:send_event).
        with(presented_articles).
        returns(send_event)

      solver.create_and_solve_ticket
      assert_includes ticket.audit.events, send_event
    end

    it 'adds solve event with the solving articles' do
      AnswerBot::EventCreator.expects(:solve_event).
        with(presented_articles.first).
        returns(solve_event)

      solver.create_and_solve_ticket
      assert_includes ticket.audit.events, solve_event
    end

    it 'solves the ticket with the anonymous requester' do
      ticket.expects(:solve).with(anonymous_requester)

      solver.create_and_solve_ticket
    end

    it 'updates deflection to solve' do
      deflection.expects(:solve_with_article).
        with(article_id, resolution_channel_id)

      solver.create_and_solve_ticket
    end

    describe 'datadog stats' do
      it 'has basic tags' do
        ab_statsd_client = stub('Datadog::Answerbot', solved: nil)
        Datadog::AnswerBot.
          expects(:new).
          with(
            via_id: via_id,
            subdomain: subdomain,
            deflection_channel_id: deflection_channel_id,
            resolution_channel_id: resolution_channel_id
          ).
          once.
          returns(ab_statsd_client)

        solver.create_and_solve_ticket
      end

      it 'records solved stats' do
        Datadog::AnswerBot.any_instance.
          expects(:solved).
          with(model_version: '40100', language: 'en').
          once

        solver.create_and_solve_ticket
      end
    end

    describe 'ticket already exists' do
      let(:ticket) { stub('ticket') }

      before do
        deflection.stubs(:ticket).returns(ticket)
      end

      it 'does nothing and returns the ticket' do
        assert_equal(ticket, solver.create_and_solve_ticket)
      end
    end
  end
end
