require_relative "../../support/test_helper"

SingleCov.covered!

describe AnswerBot::Trigger do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:deflection_config) do
    {recipient: 'requester_id', subject: 'email subject', body: 'email body'}
  end
  let(:deflection_config_with_filter) do
    deflection_config.merge(labels: 'foo,bar')
  end
  let(:deflection_trigger) do
    d = Definition.new
    d.actions.push(DefinitionItem.new("deflection", "is", deflection_config))
    d.conditions_all.push(DefinitionItem.new("status_id", "is", [StatusType.NEW]))
    Trigger.create!(account: account, title: 'deflection trigger', definition: d, owner: account)
  end
  let(:deflection_trigger_with_filter) do
    d = Definition.new
    d.actions.push(DefinitionItem.new("deflection", "is", deflection_config_with_filter))
    d.conditions_all.push(DefinitionItem.new("status_id", "is", [StatusType.NEW]))
    Trigger.create!(account: account, title: 'deflection trigger', definition: d, owner: account)
  end

  let(:rule_id) { deflection_trigger.id }
  let(:rule_with_filter_id) { deflection_trigger_with_filter.id }

  before do
    Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
  end

  describe '#config' do
    let(:trigger) { AnswerBot::Trigger.config(account, rule_id) }

    it 'should load the subject template from the trigger' do
      assert_equal 'email subject', trigger.fetch(:subject_template)
    end

    it 'should load the body template from the trigger' do
      assert_equal 'email body', trigger.fetch(:body_template)
    end

    describe 'with a legacy trigger with no label for filter section' do
      it 'should set the labels_for_filter as nil if the trigger does not have labels section' do
        assert_nil trigger.fetch(:labels_for_filter)
      end
    end

    describe 'with a trigger with labels for filter section' do
      let(:trigger) { AnswerBot::Trigger.config(account, rule_with_filter_id) }

      it 'should extract the labels_for_filter from the trigger' do
        assert_equal ['foo', 'bar'], trigger.fetch(:labels_for_filter)
      end
    end
  end
end
