require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

# Extensive coverage of Collaboration::LegacyTypeTransformer happens in /test/models/ticket_test.rb.
# It didn't seem like a good idea to duplicate all those tests yet again over here.
describe Collaboration::LegacyTypeTransformer do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:comment_email_ccs_allowed_enabled) { false }
  let(:email_ccs_light_agents_v2) { false }
  let(:follower_and_email_cc_collaborations_enabled) { false }
  let(:light_agent_email_ccs_allowed_enabled) { false }
  let(:ticket_followers_allowed_enabled) { false }
  let(:settings) do
    {
      comment_email_ccs_allowed_enabled: comment_email_ccs_allowed_enabled,
      email_ccs_light_agents_v2: email_ccs_light_agents_v2,
      follower_and_email_cc_collaborations_enabled: follower_and_email_cc_collaborations_enabled,
      light_agent_email_ccs_allowed_enabled: light_agent_email_ccs_allowed_enabled,
      ticket_followers_allowed_enabled: ticket_followers_allowed_enabled
    }
  end

  describe '.transform' do
    subject do
      [1, 2, 3].map do |type|
        input = Minitest::Mock.new
        output = FactoryBot.build_stubbed(:collaboration, collaborator_type: type)
        input.expect :transform, output, [true]
        input
      end
    end

    it 'calls #transform on each collaboration' do
      Collaboration::LegacyTypeTransformer.transform(subject, true)
      subject.each(&:verify)
    end
  end

  describe '.filtered_collaborations' do
    describe 'when follower_and_email_cc_collaborations_enabled is true' do
      let(:comment_email_ccs_allowed_enabled) { true }
      let(:ticket_followers_allowed_enabled) { true }
      let(:follower_and_email_cc_collaborations_enabled) { true }

      it 'calls #filter_collaboration? for each collaboration' do
        collaborations = [1, 2, 3].map do |type|
          FactoryBot.build_stubbed(:collaboration, collaborator_type: type)
        end
        Collaboration::LegacyTypeTransformer.any_instance.expects(:filter_collaboration?).times(3).returns(false)
        Collaboration::LegacyTypeTransformer.filtered_collaborators(collaborations: collaborations, settings: settings)
      end
    end

    describe 'when follower_and_email_cc_collaborations_enabled is false' do
      it 'returns collaboration users' do
        collaborations = [1, 2, 3].map do |type|
          FactoryBot.build_stubbed(:collaboration, collaborator_type: type, user: create_user)
        end
        Collaboration::LegacyTypeTransformer.any_instance.expects(:filter_collaboration?).never
        assert_equal collaborations.map(&:user).sort, Collaboration::LegacyTypeTransformer.filtered_collaborators(collaborations: collaborations).sort
      end
    end

    it 'sorts the returned collaborators by name' do
      types = [2, 1, 3]
      collaborations = types.map do |type|
        FactoryBot.build_stubbed(:collaboration, collaborator_type: type, user: create_user(name: "CollabUser Type#{type}"))
      end
      Collaboration::LegacyTypeTransformer.any_instance.expects(:filter_collaboration?).never
      collaborator_names = Collaboration::LegacyTypeTransformer.filtered_collaborators(collaborations: collaborations).map(&:name)
      assert_equal types.sort.map { |type| "CollabUser Type#{type}" }, collaborator_names
    end
  end

  describe '.email_ccs' do
    let(:user_1) { create_user(name: "User 1") }
    let(:collaborator_type_1) { CollaboratorType.LEGACY_CC }
    let(:user_2) { create_user(name: "User 2") }
    let(:collaborator_type_2) { CollaboratorType.LEGACY_CC }
    let(:users_preloaded) { false }

    subject do
      users = [user_1, user_2]
      collaborations = [collaborator_type_1, collaborator_type_2].each_with_index.map do |type, index|
        FactoryBot.build_stubbed(:collaboration, collaborator_type: type, user: users[index])
      end
      Collaboration::LegacyTypeTransformer.email_ccs(
        collaborations: collaborations,
        settings: settings,
        account_id: account.id,
        users_preloaded: users_preloaded
      )
    end

    describe 'when an agent that cannot publicly comment is added as an email cc' do
      let(:user_2) { create_light_agent }
      let(:collaborator_type_2) { CollaboratorType.EMAIL_CC }

      describe 'and email_ccs_light_agents_v2 arturo is enabled' do
        describe 'and light_agent_email_ccs_allowed setting is disabled' do
          let(:comment_email_ccs_allowed_enabled) { true }
          let(:email_ccs_light_agents_v2) { true }
          let(:follower_and_email_cc_collaborations_enabled) { true }

          it 'email ccs exclude agents that cannot publicly comment' do
            assert_equal [user_1], subject
          end
        end
      end

      describe 'and email_ccs_light_agents_v2 arturo is enabled' do
        describe 'and light_agent_email_ccs_allowed setting is enabled' do
          let(:comment_email_ccs_allowed_enabled) { true }
          let(:email_ccs_light_agents_v2) { true }
          let(:follower_and_email_cc_collaborations_enabled) { true }
          let(:light_agent_email_ccs_allowed_enabled) { true }
          let(:ticket_followers_allowed_enabled) { true }

          it 'email ccs include agents that cannot publicly comment' do
            assert_equal [user_2, user_1], subject
          end
        end
      end

      describe 'and email_ccs_light_agents_v2 arturo is disabled' do
        describe 'and light_agent_email_ccs_allowed setting is enabled' do
          let(:comment_email_ccs_allowed_enabled) { true }
          let(:follower_and_email_cc_collaborations_enabled) { true }
          let(:light_agent_email_ccs_allowed_enabled) { true }
          let(:ticket_followers_allowed_enabled) { true }

          it 'email ccs exclude agents that cannot publicly comment' do
            assert_equal [user_1], subject
          end
        end
      end

      describe 'and email_ccs_light_agents_v2 arturo is disabled' do
        describe 'and light_agent_email_ccs_allowed setting is disabled' do
          let(:comment_email_ccs_allowed_enabled) { true }
          let(:follower_and_email_cc_collaborations_enabled) { true }
          let(:ticket_followers_allowed_enabled) { true }

          it 'email ccs exclude agents that cannot publicly comment' do
            assert_equal [user_1], subject
          end
        end
      end
    end

    describe 'when CCs/Followers is enabled and user role is checked' do
      let(:comment_email_ccs_allowed_enabled) { true }
      let(:follower_and_email_cc_collaborations_enabled) { true }
      let(:ticket_followers_allowed_enabled) { true }
      let(:user_1) { users(:minimum_end_user) }
      let(:user_2) { users(:minimum_search_user) }
      let(:user_map) { [user_1, user_2].index_by(&:id) }

      describe 'when users_preloaded is false' do
        it 'uses user_id_to_user_map' do
          Collaboration::LegacyTypeTransformer.any_instance.expects(user_id_to_user_map: user_map).at_least_once
          subject
        end
      end

      describe 'when users_preloaded is true' do
        let(:users_preloaded) { true }

        it 'does not use user_id_to_user_map' do
          Collaboration::LegacyTypeTransformer.any_instance.expects(:user_id_to_user_map).never
          subject
        end
      end
    end
  end

  describe '.followers' do
    let(:user_1) { create_user(name: "User 1") }
    let(:collaborator_type_1) { CollaboratorType.LEGACY_CC }
    let(:user_2) { create_user(name: "User 2") }
    let(:collaborator_type_2) { CollaboratorType.LEGACY_CC }
    let(:users_preloaded) { false }

    subject do
      users = [user_1, user_2]
      collaborations = [collaborator_type_1, collaborator_type_2].each_with_index.map do |type, index|
        FactoryBot.build_stubbed(:collaboration, collaborator_type: type, user: users[index])
      end
      Collaboration::LegacyTypeTransformer.followers(
        collaborations: collaborations,
        settings: settings,
        account_id: account.id,
        users_preloaded: users_preloaded
      )
    end

    describe 'when CCs/Followers is enabled and user role is checked' do
      let(:comment_email_ccs_allowed_enabled) { true }
      let(:follower_and_email_cc_collaborations_enabled) { true }
      let(:ticket_followers_allowed_enabled) { true }
      let(:user_1) { users(:minimum_agent) }
      let(:user_2) { users(:minimum_search_user) }
      let(:user_map) { [user_1, user_2].index_by(&:id) }

      describe 'when users_preloaded is false' do
        it 'uses user_id_to_user_map' do
          Collaboration::LegacyTypeTransformer.any_instance.expects(user_id_to_user_map: user_map).at_least_once
          subject
        end
      end

      describe 'when users_preloaded is true' do
        let(:users_preloaded) { true }

        it 'does not use user_id_to_user_map' do
          Collaboration::LegacyTypeTransformer.any_instance.expects(:user_id_to_user_map).never
          subject
        end
      end
    end
  end

  private

  def create_user(options = {})
    FactoryBot.build_stubbed(:user, options)
  end

  def create_light_agent
    Account.any_instance.stubs(:has_permission_sets?).returns(true)

    light_agent = User.create! do |user|
      user.account = account
      user.name = "Prince Light"
      user.email = "prince@example.org"
      user.roles = Role::AGENT.id
    end

    light_agent.permission_set = PermissionSet.create_light_agent!(account)
    light_agent.save!
    light_agent
  end
end
