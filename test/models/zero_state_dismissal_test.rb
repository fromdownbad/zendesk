require_relative "../support/test_helper"

SingleCov.covered!

describe ZeroStateDismissal do
  fixtures :accounts, :users, :tickets, :subscriptions
  let(:valid_params) do
    [
      {
        zero_state: "setup_email_forwarding",
        level: 0,
        status: 0
      },
      {
        zero_state: "generate_new_tickets",
        level: 1,
        status: 1
      },
      {
        zero_state: "your_tickets",
        level: 0,
        status: 2
      }
    ]
  end
  should_validate_presence_of :user, :zero_state, :status

  before do
    @user = users(:minimum_admin)
  end

  describe '.from_dismissals' do
    it 'create three zero state dismissals' do
      result = ZeroStateDismissal.from_dismissals(@user, valid_params)
      assert_equal 3, result.length

      index = 0
      result.each do |dismissal|
        assert_equal @user.account.id, dismissal['account_id']
        assert_equal @user.id, dismissal['user_id']
        assert_equal valid_params[index][:zero_state], dismissal['zero_state']
        assert_equal valid_params[index][:level], dismissal['level']
        assert_equal valid_params[index][:status], dismissal['status']
        index += 1
      end
    end

    it 'return none for invalid params' do
      result = ZeroStateDismissal.from_dismissals(@user, [{invalid: 'invalid'}])
      assert_equal [], result
    end
  end
end
