require_relative '../support/test_helper'
require_relative '../support/gooddata_session_helper'

SingleCov.covered! uncovered: 1

describe GooddataIntegration do
  include GooddataSessionHelper

  def stub_sso_sign_in_url(gooddata_login, project_id, dashboard_id)
    GoodData::SSO.any_instance.stubs(:sign_in_url).with(
      user_email: gooddata_login,
      target_url: '/dashboard.html#project=' \
      "/gdc/projects/#{project_id}&dashboard=/dashboard/#{dashboard_id}"
    ).returns("https://sso_url_#{dashboard_id}.com")
  end

  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:admin)   { users(:minimum_admin) }

  let(:integration) do
    account.gooddata_integration.try(:delete)
    account.build_gooddata_integration(project_id: 'project_id')
  end

  let(:gooddata_user) do
    GooddataUser.create! do |u|
      u.account = account
      u.user = admin
      u.gooddata_project_id = 'gooddata_project_id'
      u.gooddata_user_id = 'gooddata_user_id'
    end
  end

  describe 'setting the account' do
    it 'does not allow setting account_id to nil' do
      assert_raises(ActiveRecord::RecordInvalid) do
        integration.account_id = nil
        integration.save!
      end
    end
  end

  describe ".tour_project_sso_url" do
    before do
      GoodData::SSO.any_instance.stubs(:sign_in_url).with(
        user_email: 'your-tour-email@zendesk.com',
        target_url: '/dashboard.html#project=' \
          '/gdc/projects/your-tour-project-id&' \
          'dashboard=/gdc/md/your-tour-project-id/obj/your-tour-project-dashboard-id'
      ).returns('https://tour_project_sso_url.com')
    end

    it "returns the portal SSO URL" do
      assert_equal(
        'https://tour_project_sso_url.com',
        GooddataIntegration.tour_project_sso_url
      )
    end
  end

  describe "when assigning values to attributes" do
    [
      :project_id,
      :admin_id,
      :status,
      :scheduled_at,
      :version
    ].each do |attribute|
      should allow_mass_assignment_of(attribute)
    end if RAILS4
  end

  describe "#project_id" do
    before { integration.save! }

    should validate_uniqueness_of(:project_id).scoped_to(:deleted_at)
  end

  describe "#status" do
    it "defaults to 'pending'" do
      assert integration.pending?
    end

    it "assigns a default value to the attribute" do
      assert_equal 'pending', integration.status
    end

    describe "when in the 'pending' state" do
      before { integration.status = :pending }

      describe "and the 'integration_created' event is triggered" do
        before { integration.integration_created! }

        it "transitions to the 'created' state" do
          assert integration.created?
        end
      end

      describe "and a different event is triggered" do
        it "raises an exception" do
          assert_raise AASM::InvalidTransition do
            integration.finished!
          end
        end
      end
    end

    describe "when in the 'created' state" do
      before { integration.status = :created }

      describe "and the 'integration_configured' event is triggered" do
        before { integration.integration_configured! }

        it "transitions to the 'configured' state" do
          assert integration.configured?
        end
      end

      describe "and a different event is triggered" do
        it "raises an exception" do
          assert_raise AASM::InvalidTransition do
            integration.finished!
          end
        end
      end
    end

    describe "when in the 'configured' state" do
      before { integration.status = :configured }

      describe "and the 'admin_invited' event is triggered" do
        let(:invitation_id) { 1 }

        before do
          integration.admin_invited!(admin.id, invitation_id)
        end

        it "transitions to the 'invited' state" do
          assert integration.invited?
        end

        it "saves the user ID of the invited user" do
          assert_equal admin.id, integration.admin_id
        end

        it "saves the invitation ID" do
          assert_equal invitation_id.to_s, integration.reload.invitation_id
        end
      end

      describe "and the 'finished' event is triggered" do
        before do
          integration.finished!
        end

        it "transitions to the 'complete' state" do
          assert integration.complete?
        end
      end

      describe "and a different event is triggered" do
        it "raises an exception" do
          assert_raise AASM::InvalidTransition do
            integration.integration_created!
          end
        end
      end
    end

    describe "when in the 'invited' state" do
      before { integration.status = :invited }

      describe "and the 'invite_receipt_confirmed' event is triggered" do
        before { integration.invite_receipt_confirmed! }

        it "transitions to the 'complete' state" do
          assert integration.complete?
        end
      end

      describe "and a different event is triggered" do
        it "raises an exception" do
          assert_raise AASM::InvalidTransition do
            integration.integration_created!
          end
        end
      end
    end
  end

  describe "#as_json" do
    it "returns the correct representation" do
      assert_equal(
        {gooddata_integration: {'project_id' => 'project_id'}},
        integration.as_json(only: [:project_id])
      )
    end
  end

  describe "#dashboards" do
    describe "when in the 'complete' state" do
      before do
        integration.status = :complete

        stub_gooddata_session

        stub_request(
          :get,
          %r{/gdc/md/#{integration.project_id}/query/projectdashboards}
        ).to_return(
          body: {
            query: {
              entries: [
                {title: 'title1', link: 'link1', foo: 'foo1', unlisted: 0, identifier: 'foo1'},
                {title: 'title2', link: 'link2', foo: 'foo2', unlisted: 0, identifier: 'foo2'},
                {title: 'title3', link: 'link3', foo: 'foo3', unlisted: 1, identifier: 'foo3'}
              ]
            }
          }.to_json
        )
      end

      it "returns the public dashboards for the project" do
        assert_equal(
          [
            {
              title: 'title1',
              link: 'https://secure.gooddata.com/dashboard.html' \
                '#project=/gdc/projects/project_id&dashboard=link1'
            },
            {
              title: 'title2',
              link: 'https://secure.gooddata.com/dashboard.html' \
                '#project=/gdc/projects/project_id&dashboard=link2'
            }
          ],
          integration.dashboards
        )
      end

      describe "and the 'disabled' event is triggered" do
        before { integration.disabled! }

        it "transitions to the 'disabled' state" do
          assert integration.disabled?
        end
      end
    end

    describe "when in the 'disabled' state" do
      before do
        integration.status = :disabled
      end

      describe "and the 're_enable' event is triggered" do
        before { integration.re_enable! }

        it "transitions to the 're_enabling' state" do
          assert integration.re_enabling?
        end
      end
    end

    describe "when in the 're_enabling' state" do
      before do
        integration.status = :re_enabling
      end

      describe "and the 're_enable_complete' event is triggered" do
        before { integration.re_enable_complete! }

        it "transitions to the 'complete' state" do
          assert integration.complete?
        end
      end
    end

    describe "when not in the 'complete' state" do
      before { integration.status = :pending }

      it "returns an empty array" do
        assert_equal [], integration.dashboards
      end
    end
  end

  describe "#portal_sso_url" do
    before do
      gooddata_user.stubs(:gooddata_login).returns('login@zendesk.com')

      GoodData::SSO.any_instance.stubs(:sign_in_url).with(
        user_email: 'login@zendesk.com',
        target_url:           "/#s=/gdc/projects/#{integration.project_id}|projectDashboardPage|"
      ).returns('https://portal_sso_url.com')
    end

    it "returns the portal SSO URL" do
      assert_equal(
        'https://portal_sso_url.com',
        integration.portal_sso_url(gooddata_user)
      )
    end
  end

  describe "#sso_dashboards" do
    let(:gooddata_login) { 'user+12917@zendesk.com' }
    let(:gooddata_user_id) { 'abc123' }

    before do
      stub_gooddata_session
      integration.version = 2

      gooddata_user.stubs(:gooddata_login).returns(gooddata_login)
      gooddata_user.stubs(:gooddata_user_id).returns(gooddata_user_id)

      stub_sso_sign_in_url(gooddata_login, integration.project_id, 1)
      stub_sso_sign_in_url(gooddata_login, integration.project_id, 2)
      stub_sso_sign_in_url(gooddata_login, integration.project_id, 3)
      stub_sso_sign_in_url(gooddata_login, integration.project_id, 4)

      user_dashboards = [
        {title: 'title2', link: '/dashboard/2', foo: 'foo2', unlisted: 0, identifier: 'foo2'},
        {title: 'title1', link: '/dashboard/1', foo: 'foo1', unlisted: 0, identifier: 'foo1'},
        {title: 'Title3', link: '/dashboard/3', foo: 'foo3', unlisted: 0, identifier: 'foo3'},
        {title: 'Title4', link: '/dashboard/4', foo: 'foo4', unlisted: 1, author: gooddata_user_id, identifier: 'foo4'},
      ]

      GoodData::Project.any_instance.stubs(:dashboards_for_user).with(
        gooddata_login: gooddata_login,
        user_id: gooddata_user_id
      ).returns(user_dashboards)
    end

    it "returns the sorted dashboards by name for the project" do
      assert_equal(
        [
          {name: 'title1', sso_url: 'https://sso_url_1.com', personal: false, identifier: 'foo1'},
          {name: 'title2', sso_url: 'https://sso_url_2.com', personal: false, identifier: 'foo2'},
          {name: 'Title3', sso_url: 'https://sso_url_3.com', personal: false, identifier: 'foo3'},
          {name: 'Title4', sso_url: 'https://sso_url_4.com', personal: true, identifier: 'foo4'}
        ],
        integration.sso_dashboards(gooddata_user)
      )
    end
  end

  describe "#role_id_for" do
    before do
      integration.editor_role_id = 'editor_role_id'
      integration.save!
    end

    it "returns the role ID for the provided role" do
      assert_equal 'editor_role_id', integration.role_id_for(:editor)
    end
  end

  describe "#scheduled_at" do
    it "defaults to '12am'" do
      assert_equal '12am', integration.scheduled_at
    end

    describe "when set with a time in '%l%P' form" do
      before { integration.scheduled_at = '2pm' }

      it "returns the time in '%l%P' form" do
        assert_equal '2pm', integration.scheduled_at
      end
    end

    describe "when set to nil" do
      before do
        integration.instance_eval { write_attribute(:scheduled_at, nil) }
      end

      it "returns the default value" do
        assert_equal '12am', integration.scheduled_at
      end
    end
  end

  describe "#version" do
    it "defaults to '2'" do
      assert_equal 2, integration.version
    end
  end

  describe '#subdomain_matches_project' do
    before { stub_gooddata_session }

    describe 'when the account matches GoodData' do
      let(:settings_response) do
        { api_domain: "https://minimum.zendesk-test.com" }
      end

      before { integration.stubs(:project_settings).returns(settings_response) }

      it 'returns true' do
        assert integration.subdomain_matches_project?
      end
    end

    describe 'when the account does not GoodData' do
      let(:settings_response) do
        { api_domain: "https://someotherdomain.zendesk-test.com" }
      end

      before { integration.stubs(:project_settings).returns(settings_response) }

      it 'returns false' do
        refute integration.subdomain_matches_project?
      end
    end
  end

  describe '#last_successful_process' do
    let(:last_successful_process) do
      {
        status: { code: "SYNCHRONIZED", detail: nil, description: nil },
        started: "2014-07-21T07:01:32.000Z",
        finished: "2014-07-21T07:10:55.000Z",
        links: { self: "/gdc/projects/#{integration.project_id}/connectors/zendesk4/integration/processes/owWfrhTbLpEp5lScAsvV" }
      }
    end

    before do
      stub_gooddata_session
      integration.version = 2
    end

    describe 'when the integration is complete' do
      before do
        integration.status = 'complete'
        body = {
          integration: {
            projectTemplate: "/projectTemplates/ZendeskAnalytics/10",
            active: true,
            lastFinishedProcess: {
              status: { code: "SYNCHRONIZED", detail: nil, description: nil },
              started: "2014-07-21T07:01:32.000Z",
              finished: "2014-07-21T07:10:55.000Z",
              links: { self: "/gdc/projects/#{integration.project_id}/connectors/zendesk4/integration/processes/owWfrhTbLpEp5lScAsvV" }
            },
            lastSuccessfulProcess: last_successful_process,
            runningProcess: nil,
            link: {
              self: "/gdc/projects/#{integration.project_id}/connectors/zendesk4/integration",
              processes: "/gdc/projects/#{integration.project_id}/connectors/zendesk4/integration/processes",
              configuration: "/gdc/projects/#{integration.project_id}/connectors/zendesk4/integration/settings"
            },
            ui: {}
          }
        }.to_json
        stub_request(:get, %r{/gdc/projects/#{integration.project_id}/connectors/zendesk4/integration}).
          to_return(status: 200, body: body)
      end

      it 'retruns the last successful process from GoodData' do
        assert_equal last_successful_process, integration.last_successful_process
      end
    end

    describe 'when the integration is incomplete' do
      it 'does not blow up' do
        assert_nil integration.last_successful_process
      end
    end
  end
end
