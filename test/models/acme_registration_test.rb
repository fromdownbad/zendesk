require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe AcmeRegistration do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:registration) { account.acme_registrations.build }

  describe "creating a registration" do
    it "sets the state to pending" do
      registration.save!
      assert_equal 'pending', registration.state
    end

    it "generates and encrypts the private key" do
      registration.send(:generate_key)
      refute_match /-BEGIN RSA PRIVATE KEY-/, registration.encrypted_private_key
    end

    it "can decrypt the encrypted private key" do
      registration.send(:generate_key)
      refute_nil registration.send(:private_key)
    end

    it "is not valid" do
      assert_equal false, registration.registration_valid?
    end

    describe "#register!" do
      describe "and the registration succeeds" do
        before do
          VCR.turn_on!
          VCR.insert_cassette("acme_registration_v6")
          Timecop.freeze(VCR.current_cassette.originally_recorded_at || Time.now)
          registration.register!
        end

        after { VCR.eject_cassette }

        it "sets the state to active" do
          assert_equal 'active', registration.state
        end

        it "doesn't mutate the state after registration" do
          assert_equal 'active', AcmeRegistration.find(registration.id).state
        end

        it "is valid" do
          assert(registration.registration_valid?)
        end

        it "saves the registration id" do
          assert_equal 11488518, registration.registration_id
        end
      end

      describe "and the registration fails" do
        before do
          AcmeV2::Client.any_instance.stubs(:new_account).raises(AcmeV2::Client::Error)
        end

        it "raises an Zendesk::AcmeJob::RegistrationError exception" do
          assert_raise Zendesk::AcmeJob::RegistrationError do
            registration.register!
          end
        end
      end
    end
  end

  describe "#client" do
    it "can instantiate the client" do
      refute_nil registration.client
    end
  end

  describe "uniqueness" do
    before do
      @registration = AcmeRegistration.new(account: account)
      assert @registration.save
    end

    it "does not allow multiple registrations per account" do
      refute AcmeRegistration.new(account: account).save
    end

    it "allows a second if the first registrations is soft deleted" do
      @registration.soft_delete
      assert AcmeRegistration.new(account: account).save
    end
  end
end
