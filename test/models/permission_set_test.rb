require_relative "../support/test_helper"

SingleCov.covered! uncovered: 21

describe PermissionSet do
  fixtures :accounts, :subscriptions, :account_property_sets, :translation_locales

  before do
    @account        = accounts(:minimum)
    @permission_set = PermissionSet.new(account: @account, name: "things")
    @permissions    = @permission_set.permissions
  end

  describe "user_view_access" do
    before do
      @permission_set.permissions.user_view_access = "full"
    end

    it "updates user_view_access if available_user_lists is off" do
      @permission_set.permissions.available_user_lists = "off"
      @permission_set.save
      assert_equal @permission_set.permissions.user_view_access, "none"
    end

    it "does not update user_view_access if available_user_lists is off" do
      @permission_set.permissions.available_user_lists = "all"
      @permission_set.save
      assert_equal @permission_set.permissions.user_view_access, "full"
    end
  end

  describe 'validations' do
    it 'is valid by default' do
      assert @permission_set.valid?
    end

    it 'is valid even if there is a light agent in the system' do
      PermissionSet.create_light_agent!(accounts(:minimum))
      assert @permission_set.valid?
    end

    it 'is invalid if name has length greater than 255 characters' do
      long_name_permission_set = PermissionSet.new(account: @account, name: "a" * 256)
      assert long_name_permission_set.invalid?
    end

    it 'is invalid if description has length greater than 65535 characters' do
      long_description_permission_set = PermissionSet.new(account: @account, name: "invalid", description: "a" * 16384)
      assert long_description_permission_set.invalid?
    end

    describe 'permission set limit' do
      let(:custom_roles) { (0..number_of_roles).map { |i| PermissionSet.new(account: @account, name: i, role_type: ::PermissionSet::Type::CUSTOM) } }
      let(:permission_sets) { custom_roles }

      before do
        @account.stubs(:permission_sets).returns(permission_sets)
        permission_sets.stubs(:custom).returns(custom_roles)
      end

      describe "on update" do
        # We don't set the arturo first to simulate an account that is already past the limit of 197
        # See: https://support.zendesk.com/agent/tickets/4844587
        let(:number_of_roles) { 197 }
        let(:permission_set) { PermissionSet.new(account: @account, name: '198', role_type: ::PermissionSet::Type::CUSTOM) }

        before do
          permission_set.save(validate: false)
        end

        it 'is valid when updating existing permission set regardless' do
          permission_set.name = '198 Update'
          assert permission_set.valid?
        end
      end

      describe 'just under the limit' do
        let(:number_of_roles) { 196 }

        it 'adding one more is valid' do
          assert @permission_set.valid?
        end

        describe 'with system role' do
          let(:light_agent_role) { PermissionSet.new(account: @account, role_type: ::PermissionSet::Type::LIGHT_AGENT) }
          let(:permission_sets) { custom_roles + [light_agent_role] }

          it 'is still valid to add one more' do
            assert @permission_set.valid?
          end
        end
      end

      describe 'on the limit' do
        let(:number_of_roles) { 197 }

        it 'adding one more custom role is invalid' do
          assert @permission_set.invalid?
          assert_equal 'You have reached your maximum number of custom roles', @permission_set.errors.messages[:base].first
        end

        it 'adding one more system role is valid' do
          @permission_set = PermissionSet.new(account: @account, name: "things", role_type: ::PermissionSet::Type::LIGHT_AGENT)
          @permission_set.stubs(:valid_light_agent)
          assert @permission_set.valid?
        end
      end
    end
  end

  describe '.create_light_agent!' do
    describe "when the the account has a custom role_type named 'Light Agent'" do
      let!(:permission_set) do
        PermissionSet.create!(
          account:   @account,
          name:      'Light Agent',
          role_type: role_type
        )
      end

      before { PermissionSet.create_light_agent!(@account) }

      describe 'when the role_type is `CUSTOM`' do
        let(:role_type) { ::PermissionSet::Type::CUSTOM }

        it "renames permission_set's name" do
          refute_equal 'Light Agent',          permission_set.reload.name
          assert_equal 'Light Agent (Custom)', permission_set.reload.name
        end
      end

      describe 'when the role_type is `LIGHT_AGENT`' do
        let(:role_type) { ::PermissionSet::Type::LIGHT_AGENT }

        it "does not name the permission_set's name" do
          assert_equal 'Light Agent', permission_set.reload.name
        end
      end
    end
  end

  it "returns the configured description" do
    @permission_set.description = "A description"
    assert @permission_set.save
    assert_equal "A description", @permission_set.description
  end

  describe "permissions" do
    it "inherits the account from the permission_set, even before save" do
      assert_equal @account.id, @permission_set.permissions.lookup(:ticket_access).account_id
    end

    describe "Given a permission set" do
      before do
        Timecop.freeze(2.days.ago) do
          @permission_set.save!
        end
        @original_cache_key = @permission_set.cache_key
      end

      describe "Updating a permission" do
        before do
          @permissions.user_view_access = "full"
          @permissions.save!
        end

        it "changes the permission set's cache key" do
          assert_not_equal @original_cache_key, @permission_set.reload.cache_key
        end
      end
    end
  end

  describe "#destroy" do
    before do
      @permission_set.save!
    end

    describe "when user exist" do
      before do
        @user = @permission_set.users.create! do |user|
          user.name = "Ingo"
          user.email = "the.ingo@example.org"
          user.roles = Role::AGENT.id
          user.account = @account
        end
      end

      it "fails with a single user in a role and returns the correct error message" do
        refute @permission_set.destroy, 'must not be able to destroy a permission set containing a user'
        assert_equal @permission_set.agent_count, 1
        assert_includes @permission_set.errors[:base], 'One agent has this role. Please remove the agent from role before deleting'
      end

      it "fails when there are multiple users in a role and returns the correct error message" do
        @permission_set.users.create! do |user|
          user.name = "Ingo2"
          user.email = "the2.ingo@example.org"
          user.roles = Role::AGENT.id
          user.account = @account
        end

        refute @permission_set.destroy, 'must not be able to destroy a permission set containing multiple users'
        assert_equal @permission_set.agent_count, 2
        assert_includes @permission_set.errors[:base], '2 agents have this role. Please remove agents from role before deleting'
      end
    end

    it "succeeds when there are no users with the role" do
      assert_equal @permission_set.agent_count, 0
      assert @permission_set.destroy
    end

    describe "with admins" do
      before do
        @user = @permission_set.users.create! do |user|
          CIA.stubs(:current_actor).returns(user)
          user.name = "Ingo"
          user.email = "the.ingo@example.org"
          user.roles = Role::ADMIN.id
          user.account = @account
        end
        assert_equal @permission_set.users.count(:all), 1
      end

      it "succeeds" do
        assert_equal @permission_set.agent_count, 0
        assert @permission_set.destroy
      end

      it "nullifys the permission set of the users" do
        @permission_set.destroy
        assert_nil @user.reload.permission_set_id
      end
    end
  end

  describe "#to_md5" do
    before do
      @permission_set.permissions.forum_access = "full"
      @md5 = @permission_set.to_md5
    end
    it "is always the same if nothing changes" do
      assert_equal @md5, @permission_set.to_md5
    end

    it "changes if something change" do
      @permission_set.permissions.forum_access = "readonly"
      assert_not_equal @md5, @permission_set.to_md5
    end
  end

  describe "#configuration" do
    it "returns hash of permissions including default values for permissions not defined" do
      assert_equal "all", @permission_set.configuration[:ticket_access]
      @permission_set.permissions.ticket_access = "within-groups"
      assert_equal "within-groups", @permission_set.configuration[:ticket_access]
    end
  end

  describe "ticket access" do
    it "defaults to 'all'" do
      assert_equal "all", @permissions.ticket_access
    end
  end

  describe "explore access" do
    it "defaults to 'readonly'" do
      assert_equal "readonly", @permissions.explore_access
    end
  end

  describe "ticket editing" do
    it "defaults to 'true'" do
      assert @permissions.ticket_editing?
    end

    describe "when disabled" do
      before do
        @permission_set.permissions.disable(:ticket_editing)
        @permission_set.save!
      end

      it "disables ticket deletion" do
        refute @permissions.ticket_deletion?
      end

      it "disables ticket merge" do
        refute @permissions.ticket_merge?
      end

      it "disables viewing and editing of tags" do
        refute @permissions.edit_ticket_tags?
      end

      it "disables view deleted tickets" do
        refute @permissions.view_deleted_tickets?
      end
    end
  end

  describe 'Given ticket editing is disabled' do
    before do
      @permissions.disable(:ticket_editing)
      refute @permissions.ticket_editing?
    end

    describe "Setting ticket access to 'tickets assigned to me'" do
      before do
        @permissions.ticket_access = 'assigned-only'
        @permission_set.save!
      end

      it 'grants the ticket editing permission' do
        assert @permissions.ticket_editing?
      end
    end
  end

  describe "ticket deletion" do
    it "defaults to true" do
      assert @permissions.ticket_deletion?
    end
  end

  describe "ticket merge" do
    it "defaults to true" do
      assert @permissions.ticket_merge?
    end
  end

  describe "view and edit tags" do
    it "defaults to true" do
      assert @permissions.edit_ticket_tags?
    end
  end

  describe "view ticket satisfaction prediction" do
    it "defaults to true" do
      assert @permissions.view_ticket_satisfaction_prediction?
    end
  end

  describe "view deleted tickets" do
    it "defaults to true" do
      assert @permissions.view_deleted_tickets?
    end
  end

  describe "bulk edit tickets" do
    it "defaults to true" do
      assert @permissions.ticket_bulk_edit?
    end
  end

  describe "assign tickets to any group" do
    it "defaults to false" do
      refute @permissions.assign_tickets_to_any_group?
    end
  end

  describe 'comment access' do
    it 'has a comment access key' do
      assert @permissions.respond_to?(:comment_access)
      assert_equal 'public', @permissions.comment_access
    end
  end

  describe "access to reports" do
    it "defaults to 'full'" do
      assert_equal 'full', @permissions.report_access
    end
  end

  describe "access to views" do
    it "defaults to 'full'" do
      assert_equal 'full', @permissions.view_access
    end
  end

  describe "access to macros" do
    it "defaults to 'full'" do
      assert_equal 'full', @permissions.macro_access
    end
  end

  describe "listing users" do
    it "defaults to allow all lists to be viewed" do
      assert_equal 'all',  @permissions.available_user_lists
    end
  end

  describe "access/editing of end user profiles" do
    it "defaults to 'full'" do
      assert_equal 'full', @permissions.end_user_profile
    end

    describe "when set to 'view only'" do
      before do
        @permission_set.permissions.set(end_user_profile: "readonly")
        @permission_set.save!
      end

      it "disables organization editing" do
        refute @permissions.edit_organizations?
      end
    end

    describe "when set to 'add and edit within their organization'" do
      before do
        @permission_set.permissions.set(end_user_profile: 'edit-within-org')
        @permission_set.save!
      end

      it "disables organization editing" do
        refute @permissions.edit_organizations?
      end
    end
  end

  describe "editing of organizations" do
    it "defaults to true" do
      assert @permissions.edit_organizations?
    end
  end

  describe "forum access" do
    it "defaults to 'full'" do
      assert_equal 'full', @permissions.forum_access
    end
  end

  describe 'Given forum access is not full, and restricted access' do
    before do
      @permissions.forum_access = 'readonly'
      assert_not_equal('full', @permissions.forum_access)
      refute @permissions.forum_access_restricted_content?
    end

    describe "Setting forum access to full" do
      before do
        @permissions.forum_access = 'full'
        @permission_set.save!
      end

      it 'grants access to restricted content' do
        assert @permissions.forum_access_restricted_content?
      end
    end
  end

  describe "editing of business rules (rules, automations, triggers, SLA targets)" do
    it "defaults to on" do
      assert @permissions.business_rule_management?
    end
  end

  describe "editing of channels and extensions (Twitter/Email/etc)" do
    it "defaults to off" do
      refute @permissions.extensions_and_channel_management?
    end
  end

  describe "Viewing twitter searches" do
    it "defaults to on" do
      assert @permissions.view_twitter_searches?
    end
  end

  describe "Managing dynamic content" do
    it "defaults to off" do
      refute @permissions.manage_dynamic_content?
    end
  end

  describe "Managing facebook" do
    it "defaults to off" do
      refute @permissions.manage_facebook?
    end
  end

  describe "Create Side Conversations" do
    it "defaults to on" do
      assert @permissions.side_conversation_create?
    end
  end

  describe "access to accepting calls" do
    it "defaults to true" do
      assert @permissions.voice_availability_access?
    end
  end

  describe "access to accepting chats" do
    it "defaults to true" do
      assert @permissions.chat_availability_access?
    end
  end

  describe "#manage_help_center" do
    it "returns true if forum_access is full" do
      @permissions.forum_access = 'full'
      assert @permission_set.manage_help_center
    end

    it "returns true if forum_access is edit-topics" do
      @permissions.forum_access = 'edit-topics'
      assert @permission_set.manage_help_center
    end

    it "returns false if forum_access is readonly" do
      @permissions.forum_access = 'readonly'
      refute @permission_set.manage_help_center
    end
  end

  describe "#set_user_restriction_ids" do
    before do
      @user = users(:minimum_agent)
      @user.permission_set = @permission_set
      @user.save!

      @permission_set.permissions.ticket_access = 'within-groups'
      @permission_set.save!
    end

    it "updates all restriction_ids on users" do
      assert_equal RoleRestrictionType.NONE, @user.restriction_id
      @user.reload
      assert_equal RoleRestrictionType.GROUPS, @user.restriction_id
    end
  end

  describe 'when account locale and user locale are different' do
    before do
      @english = @account.translation_locale
      @spanish = translation_locales(:spanish)
      @english_description = I18n.t('txt.default.roles.advisor.description', locale: @english)
      @spanish_description = I18n.t('txt.default.roles.advisor.description', locale: @spanish)
      @advisor = PermissionSet.new(
        account: @account,
        name: "Advisor",
        description: @english_description
      )
      I18n.translation_locale = @spanish
    end

    describe '#translated_name' do
      it 'returns the translated name for default-populated roles' do
        I18n.expects(:t).with('txt.default.roles.advisor.name', locale: @english).returns("Advisor")
        I18n.expects(:t).with('txt.default.roles.advisor.name').returns("Asesor")
        assert_equal "Asesor", @advisor.translated_name
      end

      it 'evaluates system placeholders' do
        Arturo.enable_feature!(:dc_in_role_names)
        @advisor.update_attributes!(name: '{{zd.advisor_role}}')

        assert_equal "Advisor", @advisor.translated_name
      end
    end

    describe '#translated_description' do
      describe 'when the role name is a placeholder' do
        before do
          @advisor.update_attributes!(name: '{{zd.advisor_role}}')
        end

        it 'returns the localized description' do
          assert_equal @english_description, @advisor.translated_description
        end
      end

      describe 'when the role name does not match the account locale' do
        before do
          @advisor.update_attributes!(name: 'Asesor')
        end

        it 'returns the localized description in the current language' do
          assert_equal @english_description, @advisor.description
        end
      end

      describe 'when the role name matches the account locale' do
        before do
          @advisor.update_attributes(name: 'Advisor')
        end

        it 'returns the localized description in the current language' do
          assert_equal @english_description, @advisor.description
        end
      end
    end
  end

  describe '#pravda_role_name' do
    let(:permission_set) { PermissionSet.new(role_type: role_type) }
    let(:pravda_role_name) { permission_set.pravda_role_name }

    describe 'for Light Agent' do
      let(:role_type) { PermissionSet::Type::LIGHT_AGENT }

      it 'returns light_agent' do
        assert_equal pravda_role_name, 'light_agent'
      end
    end

    describe 'for Chat Agent' do
      let(:role_type) { PermissionSet::Type::CHAT_AGENT }

      it 'returns nil' do
        assert_nil pravda_role_name
      end
    end

    describe 'for custom role' do
      let(:role_type) { PermissionSet::Type::CUSTOM }

      it 'returns custom_ prefix with record id' do
        permission_set.id = 12345
        assert_equal pravda_role_name, 'custom_12345'
      end
    end

    describe 'for Contributor' do
      let(:role_type) { PermissionSet::Type::CONTRIBUTOR }

      it 'returns nil' do
        assert_nil pravda_role_name
      end
    end
  end

  describe '#legacy_role_type' do
    let(:billing_admin) { PermissionSet.create_billing_admin!(@account) }
    let(:custom_agent) { PermissionSet.new(account: @account, name: 'custom', description: 'desc') }

    it 'return Admin type for billing admin' do
      assert_equal Role::ADMIN, billing_admin.legacy_role_type
    end

    it 'return Agent type for custom role' do
      assert_equal Role::AGENT, custom_agent.legacy_role_type
    end
  end
end
