require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe LogMeInRescueSession do
  fixtures :all

  should_validate_presence_of :session_id, :account

  describe ".find_from_params" do
    it "does not raise when the session id key is a symbol" do
      LogMeInRescueSession.find_from_params(session_id: "123")
    end

    it "does not raise when the session id key is a string" do
      LogMeInRescueSession.find_from_params("session_id" => "session_id_token")
    end

    it "calls find_by_session_id with the specified session_id value" do
      params = {session_id: "session_id_token"}
      LogMeInRescueSession.expects(:find_by_session_id).with(params[:session_id])

      LogMeInRescueSession.find_from_params(params)
    end
  end

  describe ".ticket_callback" do
    describe "with a linkings object containing valid data" do
      before do
        @comment = "<comment from LogMeIn Rescue>"
        comment_hash = { value: @comment, is_public: true }
        logmein_transcript = {body: "<valid_transcript_body>"}
        @data = { comment_hash: comment_hash, logmein_rescue_transcript: logmein_transcript, tags: "tag1 tag2 tag3" }
        @session_id = "<session_id>"
        @account = accounts(:minimum)
        LogMeInRescueSession.create!(session_id: @session_id, data: @data, account: @account)
      end

      it "creates a new comment containing LogMeIn Rescue session data on a new ticket" do
        ticket = Ticket.new(account: accounts(:minimum), via_id: ViaType.WEB_FORM, description: "first comment")
        ticket.external_links = [{ type: "LogMeInRescueSession", session_id: @session_id }]
        ticket.will_be_saved_by(users(:minimum_agent))
        ticket.save!
        assert_equal(ticket.comments.first.body, @comment)
      end

      it "appends tags stored in LogMeInRescueSession when creating a new comment on a new ticket" do
        ticket = Ticket.new(account: accounts(:minimum), via_id: ViaType.WEB_FORM, description: "first comment")
        ticket.additional_tags = 'abc'
        ticket.external_links = [{ type: "LogMeInRescueSession", session_id: @session_id }]
        ticket.will_be_saved_by(users(:minimum_agent))
        ticket.save!
        assert_equal("abc #{@data[:tags]}", ticket.current_tags)
      end
    end
  end
end
