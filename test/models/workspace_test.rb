require_relative "../support/test_helper"
require_relative "../support/arturo_test_helper"

SingleCov.covered! uncovered: 4
describe Workspace do
  include ArturoTestHelper
  fixtures :accounts, :tickets, :workspaces, :rules, :ticket_forms

  let(:definition) { {all: [{field: "update_type", operator: "is", value: "Change"}], any: []} }
  let(:macro_1) { rules(:macro_for_minimum_agent) }
  let(:macro_2) { rules(:active_macro_for_minimum_group) }
  let(:macro_3) { rules(:macro_incident_escalation) }
  let(:inactive_macro) { rules(:inactive_macro_for_minimum_account) }
  let(:macro_ids) { [macro_1.id, macro_2.id, macro_3.id] }
  let(:account) { accounts(:minimum) }

  before do
    @account = accounts(:minimum)
    account.subscription.plan_type = 4
    account.subscription.save!

    Account.any_instance.stubs(:has_contextual_workspaces?).returns(true)
  end

  it "should have a title" do
    workspace = Workspace.new
    assert_nil workspace.title
  end

  it "should have a account_id" do
    workspace = Workspace.new
    assert_nil workspace.account_id
  end

  it "should retrieve all the tickets associated with the workspace" do
    workspace = @account.workspaces.new
    workspace.title = "Workspace 1"
    workspace.definition = definition
    workspace.save!

    ticket_1 = tickets(:minimum_1)
    ticket_2 = tickets(:minimum_2)

    ticket_1.workspace = workspace
    ticket_2.workspace = workspace

    workspace.reload

    assert_equal workspace.tickets.pluck(:id), [ticket_1.id, ticket_2.id]
  end

  describe '#rule_type' do
    let(:workspace) { Workspace.new }

    it 'returns the rule type' do
      assert_equal 'workspace', workspace.rule_type
    end
  end

  describe "#apps" do
    let(:workspace) { workspaces(:billing_workspace) }

    it "returns apps in the open eap format if the request is from open eap controller " do
      params = { apps: [{ id: 1, expand: true, position: 1 }, { id: 2, expand: false, position: 2 }] }
      workspace.save_with(params, {})
      assert_equal workspace.apps, params[:apps]
    end
  end

  describe "#selected_macros" do
    let(:workspace) { workspaces(:refund_workspace) }
    let(:macro_1) { rules(:active_macro_for_minimum_group) }
    let(:macro_2) { rules(:macro_for_minimum_agent) }

    describe_with_arturo_enabled(:tde_workspaces_open_eap) do
      it "returns macros from workspace elements and also updates macros column" do
        params = { macros: [macro_1.id] }
        workspace.save_with(params, {})
        workspace.reload

        assert_equal [macro_1.id], workspace.macro_ids
        assert_equal [macro_1], workspace.selected_macros

        params = { macros: [macro_2.id] }
        workspace.save_with(params, {})
        workspace.reload

        assert_equal [macro_2.id], workspace.macro_ids
        assert_equal [macro_2], workspace.selected_macros
      end
    end
  end

  describe "workspace_elements" do
    describe "build_element_for" do
      let(:workspace) { workspaces(:billing_workspace) }

      it "returns a workspace_element" do
        element = workspace.send(:build_element_for, "Macro", 1)

        assert_equal "Macro", element.element_type
        assert_equal 1, element.element_id
        assert_equal workspace.id, element.workspace_id
        assert_equal workspace.account_id, element.account_id
      end

      it "returns nil if an unsupported element type is passed" do
        element = workspace.send(:build_element_for, "View", 1)

        refute element
      end
    end

    describe "build_app_element" do
      let(:workspace) { workspaces(:billing_workspace) }

      it "returns a workspace_element" do
        attributes = { expand: false, id: 3, position: 1 }
        element = workspace.send(:build_app_element, attributes)

        assert_equal "App", element.element_type
        assert_equal workspace.id, element.workspace_id
        assert_equal workspace.account_id, element.account_id
        assert_equal attributes[:id], element.element_id
        assert_equal attributes[:expand], element.expand
        assert_equal attributes[:position], element.position
      end
    end

    describe "build_workspace_elements" do
      let(:workspace) { workspaces(:billing_workspace) }

      it "creates workspace elements for workspace" do
        workspace.send(:build_workspace_elements, macro_ids.first(2), [{ expand: false, id: 3, position: 1 }, { expand: true, id: 4, position: 2 }])
        assert_equal 0, workspace.workspace_elements.count

        workspace.save!
        assert_equal 4, workspace.workspace_elements.count
      end
    end
  end

  describe "definitions" do
    describe "#definition=" do
      let(:workspace) { workspaces(:billing_workspace) }
      let(:definition) { { all: [{field: "update_type", operator: "is", value: "Change"}], any: []} }

      it "creates a valid definition object with conditions" do
        workspace.definition = definition

        assert_instance_of Definition, workspace.definition
        assert_equal 1, workspace.definition.conditions_all.length
        assert_instance_of DefinitionItem, workspace.definition.conditions_all.first
        assert_equal ["Change"], workspace.definition.conditions_all[0].value
        assert_equal [], workspace.definition.conditions_any
      end

      it "returns nil when passed an invalid hash" do
        workspace.definition = { invalid_conditions: { all: {} } }

        assert_nil workspace.definition
      end
    end

    describe '#map_definition_items' do
      let(:params) { {} }
      let(:workspace) { workspaces(:billing_workspace) }
      let(:definition) { { all: [{field: "update_type", operator: "is", value: "Change"}], any: []} }

      it 'calls `Api::V2::Rules::ConditionMappings.map_definition_item!`' do
        items = [stub, stub]

        items.each do |param|
          Api::V2::Rules::ConditionMappings.expects(
            :map_definition_item!
          ).once.with(workspace.account, param)
        end

        workspace.send(:map_definition_items, items)
      end
    end
  end

  describe "#save_with" do
    let(:definition) { { all: [{field: "update_type", operator: "is", value: "Change"}], any: []} }
    let(:params) { { title: "Workspace name", description: "Description of the workspace", macros: macro_ids, apps: [{ expand: false, id: 1, position: 1 }, { expand: true, id: 2, position: 2 }, { expand: false, id: 4, position: 3 }], ticket_form_id: nil, position: 1, conditions: definition } }
    let(:new_workspace) { account.workspaces.new }

    it "builds a workspace and its associated workspace elements" do
      new_workspace.save_with(params, current_user: account.owner)

      assert_instance_of Definition, new_workspace.definition
      assert_equal 6, new_workspace.workspace_elements.length

      assert new_workspace.valid?
    end

    it "builds an invalid workspace if any of the attributes are incorrect but does not error out" do
      params[:definition] = nil
      new_workspace.save_with(params, current_user: account.owner)

      refute_instance_of Definition, new_workspace.definition
      assert_equal 6, new_workspace.workspace_elements.length
      refute new_workspace.valid?
    end
  end

  describe "#self.reorder" do
    describe "with invalid workspace ids" do
      let(:account) { accounts(:minimum) }
      let(:refund_workspace) { workspaces(:refund_workspace) }
      let(:invalid_workspace) { workspaces(:invalid_workspace) }
      let(:billing_workspace) { workspaces(:billing_workspace) }

      it "raises an error if a workspace cannot be found" do
        assert_raises StandardError do
          Workspace.reorder([-123, 54321], account)
        end
      end

      it "raises an error if a workspace cannot be saved" do
        assert_raises StandardError do
          Workspace.reorder([invalid_workspace.id, refund_workspace.id])
        end
      end

      it "doesn't raise an error and reorders position column without changing 'updated_at'" do
        Workspace.reorder([refund_workspace.id, billing_workspace.id], account)

        refund_workspace.reload
        billing_workspace.reload

        old_timestamps = [refund_workspace.updated_at, billing_workspace.updated_at]

        assert_equal refund_workspace.position, 1
        assert_equal billing_workspace.position, 2

        new_timestamps = [refund_workspace.updated_at, billing_workspace.updated_at]

        assert_equal new_timestamps, old_timestamps
      end
    end

    describe "with valid workspace ids" do
      let(:account) { accounts(:minimum) }
      let(:refund_workspace) { workspaces(:refund_workspace) }
      let(:billing_workspace) { workspaces(:billing_workspace) }

      it "returns the reordered workspaces" do
        Workspace.reorder([billing_workspace.id, refund_workspace.id], account)
        billing_workspace.reload
        refund_workspace.reload
        assert_equal 1, billing_workspace.position
        assert_equal 2, refund_workspace.position

        Workspace.reorder([refund_workspace.id, billing_workspace.id], account)
        refund_workspace.reload
        billing_workspace.reload
        assert_equal 1, refund_workspace.position
        assert_equal 2, billing_workspace.position
      end
    end
  end

  describe "active workspaces product limit" do
    describe "#check_product_limit_for_create_workspace" do
      let(:workspace) { workspaces(:billing_workspace) }
      let(:new_workspace) { workspace.dup }

      describe "when the account has more than product limit and is created as active" do
        it "throws an error with a message" do
          new_workspace.activated = true
          Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES)
          assert_raises StandardError do
            new_workspace.save
          end
        end
      end

      describe "when the account has more than product limit and is created as inactive" do
        it "does not raise an error" do
          new_workspace.activated = false
          Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES)
          new_workspace.save

          assert new_workspace.valid?
          assert_not_nil new_workspace.id
        end
      end

      describe "when the account has less than product limit" do
        it "does not raise an error" do
          Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES - 1)
          new_workspace.save

          assert new_workspace.valid?
          assert_not_nil new_workspace.id
        end
      end
    end

    describe "#check_product_limit_for_update_workspace" do
      let(:inactive_workspace) { workspaces(:inactive_workspace) }

      describe "when the account has more than product limit" do
        it "throws an error with a message" do
          Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES)

          assert_raises StandardError do
            inactive_workspace.activated = true
            inactive_workspace.save
          end
        end
      end

      describe "when the account has less than product limit" do
        it "does not an raise and error" do
          Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES - 1)
          inactive_workspace.activated = true
          inactive_workspace.save

          assert inactive_workspace.valid?
          assert inactive_workspace.activated
        end
      end
    end

    describe "#check_rules_limit_for_workspace" do
      let(:workspace) { workspaces(:billing_workspace) }
      let(:new_workspace) { workspace.dup }

      describe "when the workspace has more rules than product limit" do
        it "raises an error on save" do
          Workspace.any_instance.stubs(:number_of_rules).returns(Workspace::MAX_RULES_IN_WORKSPACE + 1)
          assert_raises StandardError do
            new_workspace.save
          end
        end
      end

      describe "when the workspace has rules count equal to the product limit" do
        it "saves the workspace" do
          Workspace.any_instance.stubs(:number_of_rules).returns(Workspace::MAX_RULES_IN_WORKSPACE)
          new_workspace.save

          assert new_workspace.valid?
          assert_not_nil new_workspace.id
        end
      end

      describe "when the account has fewer rules than product limit" do
        it "saves the workspace" do
          Workspace.any_instance.stubs(:number_of_rules).returns(Workspace::MAX_RULES_IN_WORKSPACE - 1)
          new_workspace.save

          assert new_workspace.valid?
          assert_not_nil new_workspace.id
        end
      end
    end
  end

  describe "#set_position_to_last" do
    let(:workspace) { workspaces(:billing_workspace) }
    let(:new_workspace) { workspace.dup }
    let(:inactive_workspace) { workspaces(:inactive_workspace) }

    it "sets position to last for a new workspace" do
      Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES - 1)
      new_workspace.save

      assert_equal new_workspace.position, Workspace::MAX_ACTIVE_WORKSPACES
    end

    it "sets position to last when activating a workspace" do
      Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES - 1)
      inactive_workspace.activated = true
      inactive_workspace.save

      assert_equal inactive_workspace.position, Workspace::MAX_ACTIVE_WORKSPACES
    end
  end

  describe "#normalize_workspace_positions" do
    let(:workspace_1) { workspaces(:refund_workspace) } # position 2
    let(:workspace_2) { workspaces(:general_workspace) } # position 4
    let(:workspace_3) { workspaces(:general_workspace_2) } # position 5
    let(:workspace_4) { workspaces(:billing_workspace) } # position 9999

    describe "when a workspace is deactivated" do
      it "normalizes the workspace positions" do
        workspace_2.activated = false
        workspace_2.save
        workspace_1.reload
        workspace_3.reload
        workspace_4.reload

        assert_equal workspace_1.position, 1
        assert_equal workspace_3.position, 2
        assert_equal workspace_4.position, 3
      end
    end

    describe "when a workspace is destroyed" do
      it "normalizes the workspace positions" do
        workspace_3.destroy
        workspace_1.reload
        workspace_2.reload
        workspace_4.reload

        assert_equal workspace_1.position, 1
        assert_equal workspace_2.position, 2
        assert_equal workspace_4.position, 3
      end
    end
  end

  describe "workspace validations" do
    let(:workspace) { workspaces(:billing_workspace) }
    let(:active_ticket_form) { ticket_forms(:default_ticket_form) }
    let(:inactive_ticket_form) { ticket_forms(:inactive_ticket_form) }

    describe "definition validations" do
      let(:definition) { { all: [{field: "update_type", operator: "is", value: "C" * 70000}], any: []} }

      it "does not allow a definition with size greater than 65535" do
        workspace.definition = definition
        refute workspace.save
        assert_equal 1, workspace.errors.full_messages.length
        assert_equal I18n.t('activerecord.errors.messages.too_long', count: Rule::MAX_DEFINITION_SIZE), workspace.errors.full_messages[0]
      end

      it "does not allow a definition if it is not of type 'Definition'" do
        workspace[:definition] = "Some random string"
        refute workspace.save
        assert_equal 1, workspace.errors.full_messages.length
        assert_equal I18n.t('txt.models.rules.rule.invalid_conditions_you_must_select_condition'), workspace.errors.full_messages[0]
      end

      it "does not allow a definition to be saved if definition contains invalid condition" do
        workspace.current_user = workspace.account.owner
        workspace.definition = { all: [{field: "status_id", operator: "is", value: 123456}] }

        refute workspace.save
        assert workspace.errors.full_messages.grep(/Invalid value/).any?
      end
    end

    describe "#check_elements_validity_for_workspace" do
      describe "for ticket form" do
        let(:workspace) { workspaces(:billing_workspace) }
        let(:active_ticket_form) { ticket_forms(:default_ticket_form) }

        it("active ticket form should save without errors") do
          active_ticket_form.account_id = workspace.account_id
          active_ticket_form.save

          workspace.ticket_form = active_ticket_form
          assert workspace.save
        end

        it("inactive ticket form should return error message") do
          workspace.ticket_form = inactive_ticket_form
          refute workspace.save
        end

        it("invalid ticket form should return error message") do
          workspace.ticket_form_id = 3245
          refute workspace.save
        end

        it("ticket form can have a nil value") do
          workspace.ticket_form_id = nil
          assert workspace.save
        end

        it("ticket form with id 0 should be converted to nil") do
          workspace.ticket_form_id = "0"
          workspace.save!
          assert workspace.ticket_form_id.nil?
        end
      end

      describe "macros" do
        it "passes if all macros in the workspace are active" do
          workspace.send(:build_workspace_elements, macro_ids.first(2), [{ expand: false, id: 3, position: 1 }, { expand: true, id: 4, position: 2 }])
          assert workspace.valid?
        end

        it "fails if any macros contain a non-existent macro" do
          workspace.send(:build_workspace_elements, [macro_ids.first, "invalid macro id"], [{ expand: false, id: 3, position: 1 }, { expand: true, id: 4, position: 2 }])
          refute workspace.valid?
        end

        it "allows inactive macros" do
          workspace.send(:build_workspace_elements, [macro_ids.first, inactive_macro.id], [{ expand: false, id: 3, position: 1 }, { expand: true, id: 4, position: 2 }])
          assert workspace.valid?
        end
      end
    end
  end
end
