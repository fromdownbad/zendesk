require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Sequence do
  fixtures :sequences

  describe '#next' do
    before do
      @sequence = sequences(:minimum_invoice)
    end

    describe "when not within transaction" do
      before { Sequence.any_instance.stubs(:number_of_open_transactions).returns(0) }

      it "does not log error" do
        ZendeskExceptions::Logger.expects(:record).never
        @sequence.next
      end

      it 'generates a sequence of ids' do
        ids = Array.new(5) { @sequence.next }
        4.downto(1) do |i|
          ids[i].must_equal ids[i - 1] + 1, ids.inspect
        end
      end
    end

    describe "when within transaction" do
      it "logs error" do
        Sequence.any_instance.stubs(:number_of_open_transactions).returns(1)
        ZendeskExceptions::Logger.expects(:record).with do |exception, params|
          assert_equal(Sequence::SequenceTransactionError, exception.class)
          assert_equal(@sequence, params[:location])
          assert_equal("Sequence#next called within transaction", params[:message])
        end
        @sequence.next
      end
    end
  end
end
