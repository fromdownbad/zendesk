require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'DC::Synchronizer' do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:locale1) { create_locale("da") }
  let(:locale2) { create_locale("ru") }

  describe ".synchronize_snippet" do
    it "adds all translations" do
      text = create_text("foo", value: "bar", translation_locale: locale1)
      text.variants.create!(value: "bim", translation_locale: locale2)

      snippet = DC::Synchronizer.synchronize_snippet(text)

      assert_equal "bar", snippet.value_for_locale(locale1)
      assert_equal "bim", snippet.value_for_locale(locale2)
    end

    it "copies the name of the snippet" do
      text = create_text("foo", value: "bar", translation_locale: locale1)

      snippet = DC::Synchronizer.synchronize_snippet(text)

      assert_equal "foo", snippet.name
    end

    it "sets the default translation to the fallback variant" do
      text = create_text("foo", value: "bar", translation_locale: locale1)
      text.variants.create!(
        value: "xoxo",
        translation_locale: locale2,
        is_fallback: true
      )

      snippet = DC::Synchronizer.synchronize_snippet(text)

      assert_equal "xoxo", snippet.default_value
    end

    it "preserves the timestamps when creating a new snippet" do
      created_at = 2.days.ago
      updated_at = 1.day.ago

      text = Timecop.freeze(created_at) { create_text("foo") }
      Timecop.freeze(updated_at) { text.touch }

      snippet = DC::Synchronizer.synchronize_snippet(text)

      assert_same_time created_at, snippet.created_at
    end

    it "preserves the timestamps on each translation" do
      created_at = 2.days.ago
      updated_at = 1.day.ago

      text = create_text("foo")

      variant = Timecop.freeze(created_at) do
        text.variants.create!(value: "bim", translation_locale: locale2)
      end

      Timecop.freeze(updated_at) { variant.touch }

      snippet = DC::Synchronizer.synchronize_snippet(text)
      translation = snippet.translation_for_locale(locale2)

      assert_same_time created_at, translation.created_at
      assert_same_time updated_at, translation.updated_at
    end

    it "updates existing snippets" do
      snippet = DC::Snippet.create!(identifier: "boom", account: account)
      snippet.create_translation!("a", locale: locale1)

      text = create_text("boom", value: "bar", translation_locale: locale1)

      snippet = DC::Synchronizer.synchronize_snippet(text)

      assert_equal "bar", snippet.value_for_locale(locale1)
    end

    it "updates the active status on translations" do
      text = create_text("foo")
      variant = text.variants.create!(
        value: "xoxo",
        translation_locale: locale2,
        active: false
      )

      snippet = DC::Synchronizer.synchronize_snippet(text)
      refute snippet.translation_for_locale(locale2).active?

      variant.active = true
      variant.save!
      text.reload

      snippet = DC::Synchronizer.synchronize_snippet(text)
      assert snippet.translation_for_locale(locale2).active?
    end

    it "removes translations not on the text" do
      snippet = DC::Snippet.create!(identifier: "boom", account: account)
      snippet.create_translation!("bar", locale: locale2)

      text = create_text("boom", value: "foo", translation_locale: locale1)

      snippet = DC::Synchronizer.synchronize_snippet(text)

      refute snippet.translated_to_locale?(locale2)
    end
  end

  describe ".delete_snippet" do
    it "removes the DC2 snippet" do
      text = create_text("boom", value: "foo")
      DC::Snippet.create!(identifier: "boom", account: account)

      DC::Synchronizer.delete_snippet(text)

      assert_nil DC::Snippet.find_by_identifier("boom")
    end

    it "ignores snippets not in DC2" do
      text = create_text("boom", value: "foo")

      DC::Synchronizer.delete_snippet(text)
    end
  end

  def create_text(name, fallback_options = {})
    account.cms_texts.create!(
      name: name,
      fallback_attributes: {
        is_fallback: true,
        active: true,
        nested: true,
        translation_locale: locale1,
        value: "RANDOM"
      }.update(fallback_options)
    )
  end

  def create_locale(locale)
    TranslationLocale.create!(locale: locale, name: locale)
  end

  def value_for_identifier(identifier, locale)
    DC::Snippet.value_for_identifier(identifier, locale: locale, account: account)
  end

  def assert_same_time(expected, actual)
    assert_equal expected.to_i, actual.to_i, "#{expected.inspect} expected but was\n#{actual.inspect}"
  end
end
