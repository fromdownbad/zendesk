require_relative "../../support/test_helper"

SingleCov.covered!

describe 'DC::Snippet' do
  fixtures :accounts, :translation_locales

  let(:account) { accounts(:minimum) }

  let(:snippet) do
    DC::Snippet.create!(
      account: account,
      identifier: "foo.bar"
    )
  end

  it "updates when its translation get updated" do
    t1 = 2.days.ago

    translation = Timecop.freeze(t1) do
      snippet.create_translation!("fiddle", locale: ENGLISH_BY_ZENDESK)
    end

    translation.update_value!('baz')

    assert_not_equal t1, snippet.reload.updated_at
  end

  describe "#value_for_locale" do
    it "returns the value for the given locale" do
      snippet.create_translation!("diggle", locale: TranslationLocale.find_by_locale("ja"))
      snippet.create_translation!("fiddle", locale: ENGLISH_BY_ZENDESK)

      assert_equal "fiddle", snippet.value_for_locale(ENGLISH_BY_ZENDESK)
    end

    it "returns nil if there is no value in the given locale" do
      assert_nil snippet.value_for_locale(ENGLISH_BY_ZENDESK)
    end
  end

  describe "#value_for_locale_or_default" do
    it "returns the value for the locale if present" do
      snippet.create_translation!("fiddle", locale: ENGLISH_BY_ZENDESK)

      assert_equal "fiddle", snippet.value_for_locale_or_default(ENGLISH_BY_ZENDESK)
    end

    it "falls back to the default value if there's no matching translation" do
      snippet.create_translation!("fiddle", locale: ENGLISH_BY_ZENDESK)

      assert_equal "fiddle", snippet.value_for_locale_or_default(TranslationLocale.find_by_locale("ja"))
    end

    it "returns nil if there is neither a matching or default value" do
      assert_nil snippet.value_for_locale_or_default(TranslationLocale.find_by_locale("ja"))
    end
  end

  describe ".value_for_identifier" do
    it "returns the value of the snippet in the given locale" do
      snippet = DC::Snippet.create!(account: account, identifier: "foo")
      snippet.create_translation!("bar", locale: ENGLISH_BY_ZENDESK)

      assert_equal "bar", value_for_identifier("foo", ENGLISH_BY_ZENDESK)
    end

    it "returns nil if there is no snippet with the given identifier" do
      assert_nil value_for_identifier("foo", ENGLISH_BY_ZENDESK)
    end

    it "returns the fallback if there is no value in the given locale" do
      snippet = DC::Snippet.create!(account: account, identifier: "foo")
      snippet.create_translation!("bar", locale: ENGLISH_BY_ZENDESK)

      assert_equal "bar", value_for_identifier("foo", TranslationLocale.find_by_locale("ja"))
    end

    it "returns nil if there are no translations" do
      DC::Snippet.create!(account: account, identifier: "foo")
      assert_nil value_for_identifier("foo", ENGLISH_BY_ZENDESK)
    end

    def value_for_identifier(identifier, locale)
      DC::Snippet.value_for_identifier(identifier,
        account: account,
        locale: locale)
    end
  end
end
