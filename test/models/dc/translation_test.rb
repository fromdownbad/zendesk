require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'DC::Translation' do
  fixtures :accounts

  describe "#hidden=" do
    it "sets the translation as hidden" do
      translation = DC::Translation.new
      translation.hidden = true

      assert translation.hidden?
    end

    it "sets the translation as not hidden" do
      translation = DC::Translation.new(hidden: true)
      translation.hidden = false

      refute translation.hidden?
    end
  end
end
