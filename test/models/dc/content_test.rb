require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 5

describe 'DC::Content' do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:content) { DC::Content.create!(account: account) }

  describe "#create_translation!" do
    it "creates a translation in the given locale" do
      translation = content.create_translation!("foo", locale: ENGLISH_BY_ZENDESK)
      assert_equal [translation], content.translations
    end

    it "sets the translation as default if it is the first translation" do
      translation = content.create_translation!("foo", locale: ENGLISH_BY_ZENDESK)
      assert_equal translation, content.default_translation
    end
  end

  describe "#translate!" do
    it "allows setting the timestamps" do
      created_at = 2.days.ago
      updated_at = 1.day.ago

      translation = content.translate!(
        "foo",
        locale: ENGLISH_BY_ZENDESK,
        created_at: created_at,
        updated_at: updated_at
      )

      assert_equal created_at.to_s, translation.created_at.to_s
      assert_equal updated_at.to_s, translation.updated_at.to_s
    end
  end
end
