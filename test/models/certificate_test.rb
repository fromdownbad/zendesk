require_relative "../support/test_helper"
require_relative "../support/certificate_test_helper"
require_relative "../support/brand_test_helper"

SingleCov.covered! uncovered: 28

describe Certificate do
  include CertificateTestHelper
  include BrandTestHelper

  fixtures :accounts, :account_property_sets, :addresses, :brands, :routes, :users, :user_identities, :certificates, :certificate_ips, :certificate_authorities

  before do
    # ActionMailer::Base.perform_deliveries = true
    @account = accounts(:minimum)
    @brand = brands(:minimum)
    @route = routes(:minimum)
    @account.update_attribute(:host_mapping, 'www.google.com')
    @account.set_address(country: "DK")
    @cert = @account.certificates.create
  end

  describe "#expired?" do
    describe "when expired" do
      before { @cert.valid_until = Time.zone.now - 1.day }

      it "returns true" do
        assert @cert.expired?
      end
    end

    describe "when not expired" do
      before { @cert.valid_until = Time.zone.now + 1.day }

      it "returns false" do
        refute @cert.expired?
      end
    end
  end

  describe "#nginx_server_names" do
    describe "with additional brand without hostmapping" do
      it "only includes the hostmapping" do
        create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: nil)
        cert = @account.certificates.create
        assert_equal 'www.google.com', cert.send(:nginx_server_names)
      end
    end

    describe "with an additional hostmapped brand" do
      before do
        @brand = create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'b.com')
        @cert = @account.certificates.create
      end

      it "adds the brand's hostmapping" do
        assert_equal 'www.google.com b.com', @cert.send(:nginx_server_names)
      end

      it "removes the hostmapping after the brand is deleted" do
        @brand.soft_delete
        @cert.reload
        assert_equal 'www.google.com', @cert.send(:nginx_server_names)
      end
    end
  end

  describe "#cert_san" do
    describe "when account route has hostmapping" do
      describe "without additional brand" do
        it "is empty" do
          assert_empty @account.reload.certificates.create.cert_san
        end
      end

      describe "with additional brand without hostmapping" do
        it "is empty" do
          create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: nil)
          cert = @account.certificates.create
          assert_empty cert.cert_san
        end
      end

      describe "with additional hostmapped brand" do
        before do
          create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'b.com')
          @cert = @account.certificates.create
        end

        it "includes brand's hostmapping" do
          assert_equal ['b.com'], @cert.cert_san
        end

        it "leaves subject domain alone" do
          assert_equal @account.host_mapping, @cert.subject_domain
        end
      end

      describe "with additional hostmapped inactive brand" do
        before do
          brand = create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'b.com')
          brand.update_attributes!(active: false)
          @cert = @account.certificates.create
        end

        it "includes brand's hostmapping" do
          assert_equal ['b.com'], @cert.cert_san
        end

        it "leaves subject domain alone" do
          assert_equal @account.host_mapping, @cert.subject_domain
        end
      end

      describe "with additional hostmapped deleted brand" do
        before do
          brand = create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'b.com')
          brand.delete
          @cert = @account.reload.certificates.create
        end

        it "is empty" do
          assert_empty @cert.cert_san
        end

        it "leaves subject domain alone" do
          assert_equal @account.host_mapping, @cert.subject_domain
        end
      end

      describe "with brandless hostmapped routes" do
        before do
          @account.routes.create subdomain: 'brand1', host_mapping: 'b.com'
          @cert = @account.certificates.create
        end

        it "is empty" do
          assert_empty @cert.cert_san
        end

        it "leaves subject domain alone" do
          assert_equal @account.host_mapping, @cert.subject_domain
        end
      end
    end

    describe "when account route does not have hostmapping" do
      before do
        @account.update_attribute(:host_mapping, nil)
      end

      describe "with another hostmapped brand" do
        before do
          create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'b.com')
          @cert = @account.certificates.create
        end

        it "is empty" do
          assert_empty @cert.cert_san
        end

        it "uses brand's hostmapping as subject domain" do
          assert_equal 'b.com', @cert.subject_domain
        end
      end

      describe "with another inactive brand" do
        before do
          brand = create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'b.com')
          brand.update_attributes!(active: false)
          @cert = @account.certificates.create
        end

        it "is empty" do
          assert_empty @cert.cert_san
        end

        it "uses brand's hostmapping as subject domain" do
          assert_equal 'b.com', @cert.subject_domain
        end
      end

      describe "with multiple additional hostmapped brands" do
        before do
          create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'b.com')
          create_brand(@account, name: 'brand2', subdomain: 'brand2', host_mapping: 'c.com')
          @account.default_brand = @account.brands.detect { |b| b.name == 'brand2' }
          @cert = @account.certificates.create
        end

        it "isn't empty" do
          assert_equal @cert.cert_san, ['b.com']
        end

        it "uses the default brand's hostmapping as subject domain" do
          assert_equal 'c.com', @cert.subject_domain
        end

        describe "with no default brand" do
          before do
            @account.settings.default_brand_id = nil
          end

          it "uses the first brand's hostmapping as subject domain" do
            assert_equal 'b.com', @cert.subject_domain
          end
        end
      end
    end

    describe "without multiple brands" do
      it "does not be included in actual CSR" do
        @cert.generate_temporary
        csr = R509::CSR.new(csr: @cert.csr)
        assert_equal '/C=/ST=City/L=City/O=Foo/OU=Foo/CN=www.google.com', csr.subject.to_s
        assert_nil csr.san
      end
    end

    describe "with multiple brands" do
      it "is included in actual CSR" do
        @cert.stubs(:cert_san).returns(['a.com', 'b.com'])
        @cert.generate_temporary
        csr = R509::CSR.new(csr: @cert.csr)
        assert_equal '/C=/ST=City/L=City/O=Foo/OU=Foo/CN=www.google.com', csr.subject.to_s
        assert_equal ['a.com', 'b.com'], csr.san.dns_names
      end
    end
  end

  describe "#subject_state" do
    before do
      @cert2 = Certificate.new
      @cert2.stubs(:account).returns(stub(address: Address.new))
    end

    it "returns the address state if applicable" do
      @cert2.account.address.state = "Wibble"
      assert_equal "Wibble", @cert2.subject_state
    end

    it "returns the address province if applicable when there's no state" do
      @cert2.account.address.province = "Wibble"
      assert_equal "Wibble", @cert2.subject_state
    end

    it "returns the address city if applicable when there's no state or province" do
      @cert2.account.address.city = "Wibble"
      assert_equal "Wibble", @cert2.subject_state
    end

    it "returns \"Unknown\" when there's nothing better to do" do
      assert_equal "Unknown", @cert2.subject_state
    end
  end

  describe "subject_org_unit" do
    it "returns the address name if theres no csr_org_unit_name setting" do
      @account.address.name = "address_name"
      assert_equal "address_name", @cert.subject_org_unit
    end

    it "returns the csr_org_unit setting if present" do
      @account.settings.csr_org_unit = "org_unit"
      assert_equal "org_unit", @cert.subject_org_unit
    end
  end

  it "does not require the account to have a host mapping" do
    @account.update_attribute(:host_mapping, nil)
    cert = @account.certificates.new

    # validating host_mapping causing too much grief
    # assert(!cert.valid?)
    assert_valid cert

    @account.update_attribute(:host_mapping, 'www.google.com')
    cert = @account.certificates.new
    assert_valid cert
  end

  describe "#generate_temporary" do
    it "allows commas in X509 subject values" do
      @account.address.update_attribute(:name, 'Foo, Inc.')
      cert = @account.certificates.create
      assert(cert.generate_temporary)
    end

    it "signs the CSR with SHA2" do
      cert = @account.certificates.create
      cert.generate_temporary
      cert.csr_object.signature_algorithm.must_equal 'sha256WithRSAEncryption'
    end
  end

  describe "in initial state" do
    describe "when transitioning to temporary state" do
      should_eventually "generate a key and csr files and update subject" do
        assert(@cert.subject.nil?)
        assert(@cert.generate_temporary)
        assert(@cert.csr)
        assert_equal(@cert.csr_object.subject.to_s, @cert.subject)
        assert(@cert.key)
        key = Certificate.send(:decrypt_field, @cert.key) # key must be encrypted
        assert_not_nil(key)
        assert_equal('-----BEGIN RSA PRIVATE KEY-----', key.lines.to_a.first.strip)
        assert_equal('-----BEGIN CERTIFICATE REQUEST-----', @cert.csr.lines.to_a.first.strip)
        assert_equal('/C=/ST=City/L=City/O=Foo/OU=Foo/CN=www.google.com', @cert.subject)
      end
    end
  end

  describe "set_uploaded_data" do
    before do
      @support_exp_date = Date.parse('Tue Aug 14 2012')
      @real_cert = @account.certificates.new
      @account.update_attribute(:host_mapping, 'support.harlanseymour.com')
    end

    it "is in initial state" do
      assert_equal(:initial, @cert.aasm.current_state)
    end

    describe "to be properly validated" do
      it "has a key paired with CSR or passed in as key_data" do
        assert_equal([Certificate::ErrorCode::NEED_KEY], @real_cert.set_uploaded_data("xxx"))
      end

      it "fails if key is loaded with cert and key is not PKCS#12 with correct passphrase" do
        assert_equal([Certificate::ErrorCode::NEED_CRT], @real_cert.set_uploaded_data(nil, "xxx", "yyyyyy"))
      end

      # TODO: find valid .pfx file to test with
    # should "work if crt is empty and key has valid PKCS#12 pfx file with correct passphrase" do
    #   assert_nil(@real_cert.set_uploaded_data(nil, "valid pfx file", "correct p/w for pfx file"))
    # end

      it "has passphrase if key is DES3 encrypted" do
        des3key = FixtureHelper::Certificate.read('BigMoose.pem')
        assert_equal([Certificate::ErrorCode::BAD_PASSPHRASE], @real_cert.set_uploaded_data("xxx", des3key))
        assert_equal([Certificate::ErrorCode::BAD_KEY], @real_cert.set_uploaded_data("xxx", des3key, "wrong_passphrase"))
        assert_equal([Certificate::ErrorCode::BAD_CRT], @real_cert.set_uploaded_data("xxx", des3key, "BigMoose")) # key OK, but CRT isn't
      end

      it "has a crt that matches the key (this doesn't)" do
        crt = FixtureHelper::Certificate.read('account.crt')
        des3key = FixtureHelper::Certificate.read('BigMoose.pem')
        assert_equal([Certificate::ErrorCode::BAD_KEY], @real_cert.set_uploaded_data(crt, des3key, "wrong_passphrase"))
        assert_equal([Certificate::ErrorCode::MISMATCH], @real_cert.set_uploaded_data(crt, des3key, "BigMoose"))
      end

      it "has a crt that matches the key" do
        crt = FixtureHelper::Certificate.read('support.crt')
        key = FixtureHelper::Certificate.read('support.key')
        @account.update_attribute(:host_mapping, 'notsupport.harlanseymour.com')
        # no longer check for BAD_HOST
        # [ Certificate::ErrorCode::BAD_HOST, '/OU=Domain Control Validated/OU=Free SSL/CN=support.harlanseymour.com' ]
        assert_nil @real_cert.set_uploaded_data(crt, key)
      end

      it "has correct host mapping" do
        crt = FixtureHelper::Certificate.read('support.crt')
        key = FixtureHelper::Certificate.read('support.key')
        assert_nil(@real_cert.set_uploaded_data(crt, key)) # all good
        # since all good, check other stuff
        assert(@real_cert.crt)
        assert_equal(@support_exp_date, @real_cert.valid_until)
        assert(@real_cert.crt_chain)
        assert_equal(3, @real_cert.crt_chain.scan('-BEGIN CERTIFICATE-').length)
      end

      it "musts no longer check expiration (just display in GUI)" do
        crt = FixtureHelper::Certificate.read('support.crt')
        key = FixtureHelper::Certificate.read('support.key')
        # expires_soon = [ Certificate::ErrorCode::EXPIRES_SOON, @support_exp_date.to_s(:db) ]
        # assert_equal(expires_soon, @real_cert.set_uploaded_data(crt, key))
        assert_nil(@real_cert.set_uploaded_data(crt, key)) # no longer checking expiration
      end

      it "rejects SHA1 signed algorithms" do
        Arturo.enable_feature!(:ssl_validation_sha1_signature)
        crt = FixtureHelper::Certificate.read('sha1.crt')
        key = FixtureHelper::Certificate.read('sha1.key')
        assert_equal([Certificate::ErrorCode::SHA1_SIGNATURE], @real_cert.set_uploaded_data(crt, key))
      end

      it "builds out certificate chain" do
        # delete intermediate
        ess_crt = OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('EssentialSSLCA_2.crt'))
        index = @real_cert.store.ca_intermediates.index { |crt| crt.subject.to_s == ess_crt.subject.to_s }
        assert(index) # make sure we find intermediate
        assert(@real_cert.store.ca_intermediates.delete_at(index)) # delete it
        # now test
        crt = FixtureHelper::Certificate.read('support.crt')
        key = FixtureHelper::Certificate.read('support.key')
        assert_equal([Certificate::ErrorCode::NEED_INTERMEDIATE], @real_cert.set_uploaded_data(crt, key))
        assert_nil(@real_cert.crt_chain) # chain not built
        assert_equal('/C=GB/ST=Greater Manchester/L=Salford/O=COMODO CA Limited/CN=EssentialSSL CA', @real_cert.ca_chain)
        # put it back and test again
        @real_cert.store.ca_intermediates << ess_crt
        @real_cert.instance_variable_set(:@ca_chain, nil) # reset
        assert_nil(@real_cert.set_uploaded_data(crt, key))
        assert_equal(5677, @real_cert.crt_chain.length)
      end

   # TODO: ca_chain gets reset so this test won't work for now
   # should "have chain that ends with subject = issuer" do
   #    crt = FixtureHelper::Certificate.read('support.crt')
   #    key = FixtureHelper::Certificate.read('support.key')
   #    assert_nil(@real_cert.set_uploaded_data(crt, key))
   #    # append subject!=issuer cert to chain
   #    ess_crt = OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('EssentialSSLCA_2.crt'))
   #    ca_chain = (@real_cert.instance_variable_get(:@ca_chain) << ess_crt)
   #    @real_cert.instance_variable_set(:@ca_chain, ca_chain)
   #    assert_equal([ Certificate::ErrorCode::BAD_CHAIN ], @real_cert.set_uploaded_data(crt, key))
   #    # this would resolve BAD_CHAIN error by checking cert on NO_VERIFY_LIST
   #    no_verify_crt = OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('no_verify.crt'))
   #    @real_cert.store.verify(ca_chain, no_verify_crt)
   #  end
    end
  end

  describe "#upload_certificate" do
    describe "when sni enabled" do
      before do
        @cert.sni_enabled = true
      end

      describe "with crt_chain" do
        before do
          @cert.crt_chain = "support_crt"
          @cert.upload_certificate
        end

        it "sets state to active" do
          assert_equal "active", @cert.state
        end

        it "builds certificate_ips" do
          assert_present @cert.certificate_ips
        end
      end

      describe "without crt chain" do
        before do
          @cert.upload_certificate
        end

        it "sets state to pending" do
          assert_equal "pending", @cert.state
        end
      end
    end

    describe "when sni disabled" do
      before do
        @cert.sni_enabled = false
        @cert.upload_certificate
      end

      it "sets state to pending" do
        assert_equal "pending", @cert.state
      end
    end
  end

  describe "in temporary state" do
    before do
      ca_store = Zendesk::Certificate::Store.new
      ca_store.ca_roots = Zendesk::Certificate::Store.load_certificates(FixtureHelper::Certificate.read('ca.crt'))
      @cert.stubs(:store).returns(ca_store)
      setup_certs
      force_certificate_into_state(:temporary)
      @real_cert = cert_from_csr(@cert.csr_object)
    end

    it "is in temporary state" do
      assert_equal(:temporary, @cert.aasm.current_state)
    end

    describe "when transitioning to pending state" do
      it "verifies and saves the uploaded crt file" do
        assert_nil(@cert.set_uploaded_data(@real_cert.to_pem))
        assert(@cert.upload_certificate)
        assert(@cert.crt)
        assert_equal('-----BEGIN CERTIFICATE-----', @cert.crt.lines.to_a.first.strip)
      end

      it "updates the valid_until time" do
        assert(@cert.valid_until.nil?)
        assert_nil(@cert.set_uploaded_data(@real_cert.to_pem))
        assert(@cert.upload_certificate)
        assert_equal(@real_cert.not_after.to_date, @cert.valid_until)
      end

      it "updates the subject" do
        assert_nil(@cert.subject)
        assert_nil(@cert.set_uploaded_data(@real_cert.to_pem))
        assert(@cert.upload_certificate)
        assert_equal('/C=US/ST=/L=City/O=Foo/CN=www.google.com', @cert.subject)
      end

      describe "a SNI certificate" do
        let(:certificate_ip) { @cert.certificate_ips[0] }

        before do
          @cert.sni_enabled = true
          @cert.set_uploaded_data(@real_cert.to_pem)
          assert @cert.upload_certificate
        end

        it "auto-activates the certificate if the chain is present" do
          assert_equal 'active', @cert.state
        end

        it "leaves the certificate as pending if the chain is missing" do
          @cert.reload
          @cert.stubs(:store).returns(Zendesk::Certificate::Store.new)

          assert_equal [Certificate::ErrorCode::NEED_INTERMEDIATE], @cert.set_uploaded_data(@real_cert.to_pem)
          assert(@cert.upload_certificate)
          assert_equal 'pending', @cert.state
        end

        it "creates a sni certificate ip record" do
          assert_equal 1, @cert.certificate_ips.size
          assert(certificate_ip.sni)
        end

        it "assigns the pod_id" do
          assert_equal @account.pod_id, certificate_ip.pod_id
        end

        it "doesn't host via ip" do
          assert_nil certificate_ip.ip
          assert_nil certificate_ip.port
          assert_nil certificate_ip.alias_record
        end

        describe "nginx config" do
          let(:config) { @cert.send(:generate_nginx_conf, pod_id: @account.pod_id) }

          before do
            assert_equal 'active', @cert.state
          end

          it "generates a config" do
            config.must_include 'server_name www.google.com'
            config.must_include 'listen 443'  # standard ssl port
            config.wont_include 'listen 8443' # aws ssl port
          end

          it "adds aws ssl port when on aws" do
            with_aws_pod(@cert.account.pod_id) { config }
            config.must_include 'server_name www.google.com'
            config.must_include 'listen 443'  # standard ssl port
            config.must_include 'listen 8443 ssl proxy_protocol' # aws ssl port
            config.must_include 'include /etc/nginx/include/proxy_protocol_trusted_ips.incl;' # file containing the trusted IPs
            config.must_include 'real_ip_header proxy_protocol;' # set the IP from proxy_protocol
          end

          it "does not generate a config without host mapping" do
            @account.update_attribute(:host_mapping, nil)
            config.must_equal ''
          end
        end
      end

      describe "a hostmapped AWS certificate" do
        before do
          @cert.crt = 'abc'
          @cert.certificate_ips.build do |cip|
            cip.alias_record = 'foobar'
            cip.pod_id = 1
          end
        end

        it "does not generate a config" do
          # @cert.set_uploaded_data(@real_cert.to_pem)
          # assert(@cert.upload_certificate)
          # assert_equal 'active', @cert.state
          assert_equal '', @cert.send(:generate_nginx_conf, pod_id: @account.pod_id)
        end
      end
    end
  end

  describe "in pending state" do
    before do
      ca_store = Zendesk::Certificate::Store.new
      ca_store.ca_roots = Zendesk::Certificate::Store.load_certificates(FixtureHelper::Certificate.read('ca.crt'))
      @cert.stubs(:store).returns(ca_store)
      setup_certs
      force_certificate_into_state(:temporary)
      @real_cert = cert_from_csr(@cert.csr_object)
      @cert.set_uploaded_data(@real_cert.to_pem)
      @cert.upload_certificate
      force_certificate_into_state(:pending)
    end

    describe "when transitioning to cancelled state" do
      it "goes from pending to cancelled" do
        assert(@cert.do_cancel_pending)
      end
    end

    describe "when transitioning to active state" do
      it "has a valid certificate chain" do
        chain = @cert.store.chain(@cert.crt_object)
        assert(@cert.store.verify(chain, @cert.crt_object))
        assert_equal chain.first.issuer.to_s, chain.last.subject.to_s
        assert_equal chain.last.issuer.to_s, chain.last.subject.to_s
      end

      it "outputs a pem formatted certificate and chain for nginx" do
        assert_equal @cert.crt_object.to_pem, @cert.send(:generate_crt_chain_to_pem)
      end

      it "builds account ssl data for deployment to remote ssl hosts" do
        ssl_data = build_account_ssl_data(@cert)
        dir = Zendesk::Configuration.dig!(:certificates, :ssl_nginx_directory)
        assert_match(/-----BEGIN CERTIFICATE----/, ssl_data[:cert_data])
        assert_match(/-----BEGIN RSA PRIVATE KEY-----/, ssl_data[:key_data])
        assert_match(%r{ssl_certificate #{dir}/minimum.crt;}, ssl_data[:nginx_conf])
        assert_match(%r{ssl_certificate_key #{dir}/minimum.key;}, ssl_data[:nginx_conf])
      end
    end
  end

  describe "in active state" do
    before do
      force_certificate_into_state(:active)
    end

    describe "when transitioning to revoked state" do
      it "does not care about the account host mapping" do
        @cert.account.update_attribute(:host_mapping, nil)
        assert(@cert.revoke)
        assert(@cert.save)
      end

      it "releases associated certificate_ips" do
        @cert = certificates(:active1)
        @cert.certificate_ips.count(:all).must_equal 1
        @cert.revoke!
        @cert.certificate_ips.count(:all).must_equal 0
      end

      it "calls detach_certificate_ips!" do
        @cert = certificates(:active1)
        @cert.certificate_ips.count(:all).must_equal 1
        @cert.expects(:detach_certificate_ips!).once
        @cert.revoke!
      end

      it "does not remove any IPMs about this certificate" do
        @cert.create_ipm_alert!(text: 'your cert is expired')
        @cert.revoke!

        refute_nil @cert.ipm_alert
        assert_equal 1, Ipm::Alert.count
      end

      describe "#notify_customer_of_activation!" do
        it "send e-mail to customer" do
          SecurityMailer.expects(:deliver_certificate_approval).with(@cert)
          @cert.notify_customer_of_activation!
        end
      end
    end
  end

  describe "#crt_covers_all_domains?" do
    let(:add_brand1) do
      create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'brand1.com')
    end

    let(:new_cert) { @account.reload.certificates.new }

    before do
      add_brand1
    end

    describe "crt covers all domains" do
      before do
        @cert = new_cert
        csr   = new_csr('brand1.com', ['www.google.com'])
        self_sign_cert(@cert, csr)
      end

      it "returns true" do
        assert(@cert.crt_covers_all_domains?)
      end
    end

    describe "crt is missing a domain" do
      before do
        @cert = new_cert
        csr   = new_csr('brand1.com')
        self_sign_cert(@cert, csr)
      end

      it "returns false" do
        assert_equal false, @cert.crt_covers_all_domains?
      end
    end

    describe "crt has extra domain" do
      before do
        @cert = new_cert
        csr   = new_csr('brand1.com', ['google.com', 'extradomain.com'])
        self_sign_cert(@cert, csr)
      end

      it "returns false" do
        assert_equal false, @cert.crt_covers_all_domains?
      end
    end

    describe "crt is for a different domains" do
      before do
        @cert = new_cert
        csr   = new_csr('brand2.com', ['brand3.com'])
        self_sign_cert(@cert, csr)
      end

      it "returns false" do
        assert_equal false, @cert.crt_covers_all_domains?
      end
    end

    describe "when wildcard domain is used" do
      before do
        create_brand(@account, name: 'a.example.com', subdomain: 'branda', host_mapping: 'a.example.com')
        create_brand(@account, name: 'b.example.com', subdomain: 'brandb', host_mapping: 'b.example.com')
        create_brand(@account, name: 'upper', subdomain: 'brandu', host_mapping: 'upper.example.com')
        @cert = new_cert
        csr = new_csr('*.example.com', ["google.com", "brand1.com"])
        self_sign_cert(@cert, csr)
      end

      it "returns false" do
        assert_equal false, @cert.crt_covers_all_domains?
      end
    end
  end

  describe "#crt_object_secured_domains" do
    before do
      create_brand(@account, name: 'yahoo', subdomain: 'yahoo', host_mapping: 'www.yahoo.com')
      create_brand(@account, name: 'a.example.com', subdomain: 'branda', host_mapping: 'a.example.com')
      create_brand(@account, name: 'b.example.com', subdomain: 'brandb', host_mapping: 'b.example.com')
      create_brand(@account, name: 'upper', subdomain: 'brandu', host_mapping: 'upper.example.com')

      @cert = @account.certificates.new
    end

    describe "without SAN" do
      it "properly read subject domain" do
        csr = new_csr('www.google.com')
        self_sign_cert(@cert, csr)
        assert_equal ['www.google.com'], @cert.crt_object_secured_domains
      end

      it "properly read wildcard subject domain" do
        csr = new_csr('*.example.com')
        self_sign_cert(@cert, csr)
        assert_equal ['a.example.com', 'b.example.com', 'upper.example.com'], @cert.crt_object_secured_domains
      end

      it "match names with upper case" do
        csr = new_csr('Upper.example.com')
        self_sign_cert(@cert, csr)
        assert_equal ['upper.example.com'], @cert.crt_object_secured_domains
      end
    end

    describe "with SAN" do
      it "includes subject domain" do
        csr = new_csr('www.google.com', ['www.yahoo.com'])
        self_sign_cert(@cert, csr)
        assert_equal ['www.google.com', 'www.yahoo.com'], @cert.crt_object_secured_domains
      end

      it "does not have duplicates" do
        csr = new_csr('www.google.com', ['www.yahoo.com', 'www.google.com'])
        self_sign_cert(@cert, csr)
        assert_equal ['www.google.com', 'www.yahoo.com'], @cert.crt_object_secured_domains
      end

      it "respects wildcards" do
        csr = new_csr('*.example.com', ['*.yahoo.com'])
        self_sign_cert(@cert, csr)
        assert_equal ['www.yahoo.com', 'a.example.com', 'b.example.com', 'upper.example.com'], @cert.crt_object_secured_domains
      end
    end
  end

  describe "with a malformed SAN cert" do
    before do
      @cert = Certificate.new
      @cert.crt = FixtureHelper::Certificate.read('malformed_san.crt')
      @cert.account = @account
      create_brand(@account, name: 'helpdesk', subdomain: 'helpdesk', host_mapping: 'helpdesk.digidentity.eu')
    end

    it "returns the CN" do
      assert_equal ['helpdesk.digidentity.eu'], @cert.crt_object_secured_domains
    end
  end

  describe "#convert_to_sni!" do
    let(:certificate_ip) { @cert.certificate_ips[0] }

    before do
      @cert.certificate_ips.create! { |cip| cip.ip = '1.2.3.4'; cip.port = 1234; cip.pod_id = 1 }
      @cert.save!
      refute @cert.sni_enabled?
    end

    it "creates a per pod sni record" do
      @cert.convert_to_sni!

      assert_equal 1, @cert.certificate_ips.count
      assert(certificate_ip.sni)
      assert_equal @account.pod_id, certificate_ip.pod_id

      assert_nil certificate_ip.ip
      assert_nil certificate_ip.port
      assert_nil certificate_ip.alias_record
    end

    it "frees the existing ip record" do
      ip_address = @cert.certificate_ips.first
      @cert.convert_to_sni!

      assert_nil ip_address.reload.certificate_id
    end
  end

  describe "#activate" do
    before do
      @certificate = certificates(:pending_no_chain)
      @certificate_ip = certificate_ips(:one)
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries.clear
    end

    it "raises an error when the certificate is aleady active" do
      @certificate.activate_certificate!(certificate_ip: @certificate_ip, notify_customer: true)

      assert_raises(Certificate::CertificateAlreadyActive) do
        @certificate.activate_certificate!(certificate_ip: @certificate_ip, notify_customer: true)
      end
    end

    it "raises an error when the certificate_ip is assoicated with another certificate" do
      @certificate_ip.certificate = certificates(:pending)
      @certificate_ip.save!

      assert_raises(Certificate::CertificateIpInUse) do
        @certificate.activate_certificate!(certificate_ip: @certificate_ip, notify_customer: true)
      end
    end

    it "raises an error when the certificate_ip is missing" do
      assert_raises(Certificate::CertificateIpRequired) do
        @certificate.activate_certificate!(certificate_ip: nil, notify_customer: true)
      end
    end

    describe "when automatic_certificate_provisioning is enabled" do
      before do
        @account.settings.automatic_certificate_provisioning = true
        disable_lets_encrypt_observers { @account.settings.save! }
      end

      it "disables automatic_certificate_provisioning after activation" do
        @certificate.activate_certificate!(certificate_ip: @certificate_ip, notify_customer: true)
        assert_equal false, @account.reload.settings.automatic_certificate_provisioning
      end

      it "doesn't disable automatic_certificate_provisioning if the certificate is from lets encrypt" do
        @certificate.autoprovisioned = true
        @certificate.sni_enabled = true
        @certificate.save!

        @certificate.activate_certificate!(certificate_ip: nil, notify_customer: true)
        assert(@account.reload.settings.automatic_certificate_provisioning)
      end
    end

    describe "for a single certificate" do
      before do
        @certificate.activate_certificate!(certificate_ip: @certificate_ip, notify_customer: true)
      end

      it "changes state to active" do
        @certificate.state.must_equal "active"
      end

      it "associates the certificate IP" do
        @certificate.certificate_ips.reload.first.must_equal @certificate_ip
      end

      it "notifies the customer" do
        ActionMailer::Base.deliveries.size.must_equal 1
        ActionMailer::Base.deliveries.last.subject.must_match /^Zendesk SSL certificate approved for/
      end
    end

    describe "for an sni certificate" do
      before do
        @certificate.sni_enabled = true
        @certificate.activate_certificate!(certificate_ip: nil, notify_customer: true)
      end

      it "changes state to active" do
        @certificate.state.must_equal "active"
      end

      it "does not assign an ip address" do
        @certificate.certificate_ips.reload.must_be_empty
      end

      it "notifies the customer" do
        ActionMailer::Base.deliveries.size.must_equal 1
        ActionMailer::Base.deliveries.last.subject.must_match /^Zendesk SSL certificate approved for/
      end

      describe "and then activating a hosted ip certificate" do
        before do
          @another_cert = @account.certificates.build
          force_certificate_into_state(:pending, @another_cert)
          @another_cert.activate_certificate!(certificate_ip: @certificate_ip, notify_customer: true)
        end

        it "activates the certificate" do
          @another_cert.state.must_equal "active"
        end

        it "revokes the existing certificate" do
          @certificate.reload.state.must_equal 'revoked'
        end

        it "assigns the requested ip address" do
          @another_cert.certificate_ips.first.must_equal @certificate_ip
        end
      end
    end

    describe "for multiple certificates" do
      before do
        @certificate.activate_certificate!(certificate_ip: @certificate_ip, notify_customer: true)
        @another_cert = @account.certificates.build
        force_certificate_into_state('pending', @another_cert)

        @another_cert.activate_certificate!(certificate_ip: @certificate_ip, notify_customer: true)
      end

      it "activates the certificate" do
        @another_cert.state.must_equal 'active'
        @another_cert.certificate_ips.first.must_equal @certificate_ip
      end

      it "revokes other certificates for the same account" do
        @certificate.reload.state = 'revoked'
        @certificate.certificate_ips.must_be_empty
      end
    end
  end

  describe "#should_be_terminated" do
    it "should be terminated" do
      # support.harlanseymour.com is one of the names on the support.crt
      # setting the host mapping to support.harlanseymour.com allows us to have a
      # valid crt to host mapping
      create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'support.harlanseymour.com')
      @account.routes.create subdomain: 'brand1', host_mapping: 'support.harlanseymour.com'
      cert = @account.certificates.create

      crt = FixtureHelper::Certificate.read('support.crt')
      key = FixtureHelper::Certificate.read('support.key')
      cert.set_uploaded_data(crt, key)
      build_account_ssl_data(cert)

      assert(cert.crt)
      assert cert.send(:should_be_terminated?)
    end

    it "should not be terminated if hostmappings not covered by crt" do
      # support.harlanseymour.com is one of the names on the support.crt
      # setting the host mapping to support.harlanseymour.com allows us to have a
      # valid crt to host mapping
      create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'www.harlanseymour.com')
      @account.routes.create subdomain: 'brand1', host_mapping: 'www.harlanseymour.com'
      cert = @account.reload.certificates.create

      crt = FixtureHelper::Certificate.read('support.crt')
      key = FixtureHelper::Certificate.read('support.key')
      cert.set_uploaded_data(crt, key)
      build_account_ssl_data(cert)

      assert(cert.crt)
      refute cert.send(:should_be_terminated?)
    end

    it "should not be terminated crt" do
      create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: nil)
      @account.routes.create subdomain: 'brand1', host_mapping: 'b.com'
      cert = @account.reload.certificates.create

      refute(cert.crt)
      refute cert.send(:should_be_terminated?)
    end

    it "should not be terminated nginx servers not present" do
      # need to create an account without host mapping so
      # nginx_server_names.present? will return false
      acct = accounts(:minimum)
      acct.update_attribute(:host_mapping, '')
      cert = acct.reload.certificates.create

      crt = FixtureHelper::Certificate.read('support.crt')
      key = FixtureHelper::Certificate.read('support.key')
      cert.set_uploaded_data(crt, key)

      assert(cert.crt)
      refute cert.send(:should_be_terminated?)
    end

    it "should not be terminated crt and nginx servers not present" do
      acct = accounts(:minimum)
      cert = acct.reload.certificates.create

      refute(cert.crt)
      refute cert.send(:should_be_terminated?)
    end
  end

  def setup_certs
    @cert.key = Certificate.send(:encrypt_field, FixtureHelper::Certificate.read('account.key'))
    @cert.csr = FixtureHelper::Certificate.read('account.csr')
  end

  def force_certificate_into_state(state, certificate = @cert)
    certificate.state = state.to_s
    certificate.save!
  end

  def build_account_ssl_data(cert)
    ip = CertificateIp.new
    ip.ip = "192.168.0.1"
    ip.port = 80
    cert.certificate_ips << ip

    {
      nginx_conf: cert.send(:generate_nginx_conf, pod_id: ip.pod_id),
      cert_data: cert.send(:generate_crt_chain_to_pem),
      key_data: Certificate.send(:decrypt_field, cert.key)
    }
  end

  def with_aws_pod(pod_id)
    CertificateIp::AWS_POD_IDS << pod_id
    yield
  ensure
    CertificateIp::AWS_POD_IDS.pop
  end
end
