require_relative "../support/test_helper"
require_relative "../support/pre_account_creation_helper"

SingleCov.covered!

describe PreAccountCreation do
  include PreAccountCreationHelper

  fixtures :accounts, :pre_account_creations

  let(:target_locale_id) { 1 }
  let(:freshness_threshold) do
    14.days.ago
  end

  let(:minimum) { accounts(:minimum) }
  let(:support) { accounts(:support) }

  let(:preaccount1) do
    get_precreated_account(
      name: :precreation_one,
      account_id: minimum.id,
      target_locale_id: target_locale_id
    )
  end
  let(:preaccount2) do
    get_precreated_account(
      name: :precreation_two,
      account_id: support.id,
      target_locale_id: target_locale_id
    )
  end

  describe '.find_precreated_account' do
    before do
      PreAccountCreation.any_instance.stubs(:need_more_accounts?).returns(false)
      preaccount1.update_attribute(:id, 10001)
      preaccount2.update_attribute(:id, 10002)
    end
    let(:find_precreated_account) { PreAccountCreation.find_precreated_account }

    it 'returns earliest matching record that is still fresh' do
      assert_equal find_precreated_account.id, preaccount1.id
    end

    it 'start binding process by updating status' do
      assert_equal find_precreated_account.status, PreAccountCreation::ProvisionStatus::ONGOING
    end

    it 'calls precreate new account function' do
      PreAccountCreation.any_instance.expects(:precreate_new_account)
      PreAccountCreation.find_precreated_account
    end

    it 'only uses accounts created no more than 2 weeks ago' do
      preaccount1.update_attribute(:created_at, freshness_threshold - 1.day)
      assert_equal find_precreated_account.id, preaccount2.id
    end

    it 'fetch the next fresh account when called more than once' do
      PreAccountCreation.find_precreated_account
      assert_equal find_precreated_account.id, preaccount2.id
    end
  end

  describe '.precreation_supported?' do
    it 'only supports classic and Google App Market accounts' do
      assert PreAccountCreation.precreation_supported?(Accounts::Classic)
      refute PreAccountCreation.precreation_supported?(Accounts::Magento)
      assert PreAccountCreation.precreation_supported?(Accounts::GoogleAppMarket)
    end
  end

  describe '.precreate_new_account' do
    it 'creates a new precreated account if needed' do
      preaccount1.expects(:need_more_accounts?).returns(true)
      preaccount1.expects(:enqueue_precreate_account_job)

      preaccount1.precreate_new_account
    end

    it 'does not create a new precreated account if there are enough' do
      preaccount1.expects(:need_more_accounts?).returns(false)
      preaccount1.expects(:enqueue_precreate_account_job).never

      preaccount1.precreate_new_account
    end
  end

  describe '.need_more_accounts?' do
    it 'should check need more accounts against pod_id and locale_id and region' do
      pre_account_creation_us = PreAccountCreation.create!(
        account_id: 1,
        locale_id: 1,
        pod_id: 1,
        source: 'Marketing website one',
        account_class: 'Accounts::Classic',
        region: 'us'
      )

      pre_account_creation_emea = PreAccountCreation.create!(
        account_id: 1,
        locale_id: 1,
        pod_id: 1,
        source: 'Marketing website one',
        account_class: 'Accounts::Classic',
        region: 'emea'
      )

      assert pre_account_creation_us.need_more_accounts?

      PreAccountCreation::NUM_ACCOUNT_PRECREATE.times do |n|
        pre_account_creation_us = PreAccountCreation.create!(
          account_id: n + 100,
          locale_id: 1,
          pod_id: 1,
          source: 'Marketing website one',
          account_class: 'Accounts::Classic',
          region: 'us'
        )
      end

      refute pre_account_creation_us.need_more_accounts?

      assert pre_account_creation_emea.need_more_accounts?

      pre_account_creation = PreAccountCreation.create!(
        account_id: 1,
        locale_id: 2,
        pod_id: 1,
        source: 'Marketing website one',
        account_class: 'Accounts::Classic',
        region: 'us'
      )

      assert pre_account_creation.need_more_accounts?

      pre_account_creation = PreAccountCreation.create!(
        account_id: 1,
        locale_id: 1,
        pod_id: 2,
        source: 'Marketing website one',
        account_class: 'Accounts::Classic',
        region: 'us'
      )

      assert pre_account_creation.need_more_accounts?
    end
  end

  describe '.next_subdomain' do
    it 'matches z3nprecreated\d+ pattern' do
      p = PreAccountCreation.new
      assert_match /z3nprecreated\d+/, p.send(:next_subdomain)
    end
  end

  describe '#account' do
    it 'uses the correct account class for lookups' do
      preaccount1.update_column(:account_class, 'Accounts::GoogleAppMarket')
      Accounts::GoogleAppMarket.expects(:find_by_id)

      preaccount1.account
    end
  end

  describe '#is_binding?' do
    it 'is true for ongoing conversion status' do
      preaccount1.update_column(:status, PreAccountCreation::ProvisionStatus::INIT)
      refute preaccount1.is_binding?

      preaccount1.update_column(:status, PreAccountCreation::ProvisionStatus::ONGOING)
      assert preaccount1.is_binding?
    end
  end

  describe '#bounded?' do
    it 'is true for finished conversion status' do
      preaccount1.update_column(:status, PreAccountCreation::ProvisionStatus::ONGOING)
      refute preaccount1.bounded?

      preaccount1.update_column(:status, PreAccountCreation::ProvisionStatus::FINISHED)
      assert preaccount1.bounded?
    end
  end

  describe '#enqueue_precreate_account_job' do
    it 'enqueues AccountsPrecreationJob' do
      AccountsPrecreationJob.expects(:enqueue)

      preaccount1.enqueue_precreate_account_job
    end
  end
end
