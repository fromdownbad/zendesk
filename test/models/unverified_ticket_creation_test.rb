require_relative "../support/test_helper"

SingleCov.covered!

describe 'UnverifiedTicketCreation' do
  fixtures :users, :accounts, :user_identities, :tickets

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_end_user) }
  let(:user_identity) { user_identities(:minimum_end_user) }
  let(:ticket) { tickets(:minimum_2) }
  let(:unverified_ticket_creation) do
    ticket.create_unverified_creation(
      account: account,
      user: user,
      user_identity: user_identity,
      from_address: user_identity.value
    )
  end

  describe "#identity_currently_verified?" do
    describe "when the user identity is still unverified" do
      before { user_identity.update_attribute(:is_verified, false) }

      it "returns false" do
        refute unverified_ticket_creation.identity_currently_verified?
      end
    end

    describe "when the user identity is verified now" do
      before { user_identity.update_attribute(:is_verified, true) }

      it "returns true" do
        assert unverified_ticket_creation.identity_currently_verified?
      end
    end

    describe "when the user identity was deleted" do
      before { user_identity.destroy }

      it "returns false" do
        refute unverified_ticket_creation.reload.identity_currently_verified?
      end
    end
  end
end
