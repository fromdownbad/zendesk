require_relative "../support/test_helper"

SingleCov.covered!

describe AlertDismissal do
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_agent)
    @alert   = Alert.create!(value: "Hello")
    @orphan  = AlertDismissal.create!(alert: @alert, account: @account, user: @user)
    @exists  = AlertDismissal.create!(alert: @alert, account: @account, user: @user)

    assert(@orphan.update_columns(alert_id: @alert.id + 1))
  end

  describe ".cleanup" do
    before { AlertDismissal.cleanup }
    it "deletes the dismissals for which alerts do not exist" do
      assert_equal [@exists], AlertDismissal.all
    end
  end
end
