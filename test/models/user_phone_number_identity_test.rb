require_relative "../support/test_helper"
require 'user_phone_number_identity'

SingleCov.covered! uncovered: 2

describe UserPhoneNumberIdentity do
  fixtures :users, :accounts

  let(:account) { accounts(:minimum) }

  before do
    @minimum_end_user = users(:minimum_end_user)
  end

  describe "validations" do
    before do
      Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
      Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true)
    end

    describe "when apply_prefix_and_normalize_phone_numbers arturo is disabled" do
      before do
        Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(false)
        Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(false)
      end
      it "does not change the number with +1" do
        identity = UserPhoneNumberIdentity.new(value: "3531234567", user: @minimum_end_user)

        identity.valid?

        assert_equal('3531234567', identity.value)
      end
    end

    it "normalizes and add default international prefix" do
      identity = UserPhoneNumberIdentity.new(value: "2223334444", user: @minimum_end_user)

      identity.valid?

      assert_equal('+12223334444', identity.value)
    end

    it "validates against E164 format" do
      identity = UserPhoneNumberIdentity.new(value: "8479173909", user: @minimum_end_user)
      assert identity.valid?

      identity = UserPhoneNumberIdentity.new(value: "+18479173909", user: @minimum_end_user)
      assert identity.valid?
    end

    it "does not revalidate if value did not change" do
      identity = UserPhoneNumberIdentity.create!(user: @minimum_end_user, value: "+15551234567")
      identity.update_attribute(:value, "+6195551222") # invalid Australian number
      identity.reload

      assert identity.valid?
    end

    it 'phone numbers starting with x should not be considered as extensions' do
      Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(false)
      Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(false)

      identity = UserPhoneNumberIdentity.create!(user: @minimum_end_user, value: "x123")
      identity.reload

      assert_equal '', identity.extension
      assert_equal 'x123', identity.value
    end

    describe 'when account setting is true' do
      before do
        Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
      end

      it 'validates uniqueness of phone number without extension' do
        user = accounts(:minimum).users.new(name: 'Mick')
        UserPhoneNumberIdentity.create!(user: user, value: "+15551234567x123")

        accounts(:minimum).users.new(name: 'Tom')
        identity = UserPhoneNumberIdentity.new(user: user, value: "+15551234567x999")
        refute identity.valid?
      end

      it 'stores the number without extension' do
        user = accounts(:minimum).users.new(name: 'Mick')
        identity = UserPhoneNumberIdentity.create!(user: user, value: "+15551234567")
        identity.reload

        assert_equal '+15551234567', identity.value
      end

      it 'stores the extension separately' do
        user = accounts(:minimum).users.new(name: 'Mick')
        identity = UserPhoneNumberIdentity.create!(user: user, value: "+15551234567x123")
        identity.reload

        assert_equal 'x123', identity.extension
        assert_equal '+15551234567x123', identity.value + identity.extension
      end

      it '#extension returns blank string when no extension exists' do
        user = accounts(:minimum).users.new(name: 'Mick')
        identity = UserPhoneNumberIdentity.create!(user: user, value: "+15551234567")
        identity.reload

        assert_equal '', identity.extension
      end
    end

    it 'allows creation of more then 1 number for user' do
      user = accounts(:minimum).users.new(name: 'Mick')
      UserPhoneNumberIdentity.create!(user: user, value: "+15551234567")
      identity = UserPhoneNumberIdentity.new(user: user, value: "+15551234568")
      assert identity.valid?
    end

    describe "uniqueness of value" do
      subject { users(:minimum_author) }

      let(:uniqueness_query) do
        /SELECT  `user_identities`\.\* FROM `user_identities` WHERE `user_identities`\.`type` IN \('UserPhoneNumberIdentity'\) AND \(account_id = \d+ AND value = '\+\d+' AND type = 'UserPhoneNumberIdentity'/
      end

      before do
        UserPhoneNumberIdentity.create!(user: subject, value: '+15551234567')
      end

      describe "when creating an identity that already exists on a different user" do
        it "validates unsuccessfully, and includes a link in error message" do
          another_user = FactoryBot.create(:user, account: subject.account)
          another_user_identity = UserPhoneNumberIdentity.new(user: another_user, value: '+15551234567')

          refute another_user_identity.save

          assert_sql_queries(1, uniqueness_query) do
            refute another_user_identity.valid?
          end

          assert another_user_identity.errors.full_messages.join.include?(I18n.t('activerecord.errors.voice.user.phone.duplicate_direct_line', user_id: subject.id, user_name: subject.name, value: '+15551234567'))
        end
      end

      describe "when creating an identity that already exists on the same user" do
        it "validates unsuccessfully, and includes a relevant error message" do
          assert_equal 1, subject.identities.phone.length

          identity = UserPhoneNumberIdentity.new(user: subject, value: '+15551234567')

          refute identity.save

          assert_sql_queries(1, uniqueness_query) do
            refute identity.valid?
          end

          refute identity.errors.full_messages.join.include?(I18n.t('activerecord.errors.voice.user.phone.duplicate_direct_line', user_id: subject.id, user_name: subject.name, value: '+15551234567'))
          assert identity.errors.full_messages.join.include?(I18n.t('activerecord.errors.voice.user.phone.cannot_add_already_existing_direct_line', value: '+15551234567'))

          assert_equal 1, subject.identities.phone.length
        end
      end

      describe "when updating an identity to a value that already exists on a different user" do
        it "validates unsuccessfully, and includes a link in error message" do
          another_user = users(:minimum_search_user)
          another_user_identity = UserPhoneNumberIdentity.create!(user: another_user, value: '+18888376537')

          another_user_identity.value = '+15551234567'

          assert_sql_queries(1, uniqueness_query) do
            refute another_user_identity.valid?
          end

          assert another_user_identity.errors.full_messages.join.include?(I18n.t('activerecord.errors.voice.user.phone.duplicate_direct_line', user_id: subject.id, user_name: subject.name, value: '+15551234567'))
        end
      end

      describe "when creating a phone identity without a user" do
        it "validates unsuccessfully, including the appropriate error message" do
          identity = UserPhoneNumberIdentity.new(user: subject, value: '+15551234589')
          identity.save!
          identity.update_column(:user_id, 0)

          another_user_identity = UserPhoneNumberIdentity.new(user: subject, value: '+15551234589')
          refute another_user_identity.save

          assert another_user_identity.errors.full_messages.join.include?(I18n.t('activerecord.errors.voice.user.phone.duplicate_direct_line_no_user', value: '+15551234589'))
        end
      end

      describe "when updating an identity without changing the value" do
        it "does not validate uniqueness and allows saving" do
          identity = UserPhoneNumberIdentity.create!(user: subject, value: '+16742384221')
          refute identity.primary?
          identity.make_primary
          assert identity.primary?
          assert_sql_queries(0, uniqueness_query) do
            assert identity.valid?
          end
          assert identity.save
        end
      end
    end
  end

  describe "creating a new user" do
    before do
      @user = accounts(:minimum).users.new(name: 'Mick')
    end

    it "creates and associate phone number identity" do
      @user.identities << UserPhoneNumberIdentity.new(user: @user, value: "+15551234567")
      @user.save!
    end

    it "adds identity errors on the user" do
      UserPhoneNumberIdentity.create!(user: @minimum_end_user, value: "+15551234567")

      @user.identities << UserPhoneNumberIdentity.new(user: @user, value: "+15551234567")
      refute @user.valid?
      assert @user.errors[:phone_number].any?
    end
  end

  describe "destroying" do
    before do
      @identity = UserPhoneNumberIdentity.create!(user: @minimum_end_user, value: "+15551234567")
    end

    it "does not modify the value if destroyed" do
      @identity.expects(:value=).never

      @identity.destroy

      @identity.valid?
    end

    describe 'when associations are present' do
      before do
        @account = accounts(:minimum)
        user = @account.users.new(name: 'Mick')
        user.identities << UserIdentity.create!(account: @account, user: user, value: 'test@test.com')
        @identity = UserPhoneNumberIdentity.create!(account: @account, user: user, value: "+1234567890")
      end

      it 'destroys the sms_capability record' do
        @identity.sms_capability = Voice::UserPhoneNumberSmsCapability.create!(account: @account, value: true)
        @identity.destroy
        @identity.sms_capability.persisted?.must_equal false
      end

      it 'destroys the user_phone_extension record' do
        @identity.user_phone_extension = UserPhoneExtension.create!(account: @account, value: true)
        @identity.destroy
        @identity.user_phone_extension.persisted?.must_equal false
      end
    end
  end

  describe '#add_default_international_prefix' do
    before { @identity = UserPhoneNumberIdentity.new(value: '', user: @minimum_end_user, account: account) }

    it 'returns blank if value is blank' do
      @identity.value = nil

      @identity.send(:add_default_international_prefix)
      assert_nil @identity.value
    end

    it 'returns if destroyed' do
      @identity.destroy
      old = @identity.value.dup

      @identity.send(:add_default_international_prefix)
      assert_equal old, @identity.value
    end

    it 'prepends +1 if value starts with neither + nor 1' do
      @identity.value = '2223334444'

      @identity.send(:add_default_international_prefix)

      assert_equal('+12223334444', @identity.value)
    end

    it 'prepends + if value starts with 1' do
      @identity.value = '12223334444'

      @identity.send(:add_default_international_prefix)

      assert_equal('+12223334444', @identity.value)
    end

    it 'does not prepend anything if value starts with +1' do
      @identity.value = '+12223334444'

      @identity.send(:add_default_international_prefix)

      assert_equal('+12223334444', @identity.value)
    end

    describe 'when the number is too short or too long to be a NANP number' do
      it 'does not prepend anything if value is 12 digits' do
        @identity.value = '122233344445'

        @identity.send(:add_default_international_prefix)

        assert_equal('122233344445', @identity.value)
      end

      it 'does not prepend anything if value is 9 digits' do
        @identity.value = '222333444'

        @identity.send(:add_default_international_prefix)

        assert_equal('222333444', @identity.value)
      end
    end
  end

  describe '#update_capability' do
    before do
      account = accounts(:minimum)
      user = account.users.new(name: 'Mick')
      @identity = UserPhoneNumberIdentity.create!(account: account, user: user, value: "+15551234567")
      @identity.sms_capable?.must_equal false
    end

    describe 'when passed capable: true' do
      it '#sms_capable? returns true' do
        @identity.update_sms_capability(capable: true)
        @identity.reload
        @identity.sms_capable?.must_equal true
      end
    end

    describe 'when passed capable: false' do
      it '#sms_capable? returns false' do
        @identity.update_sms_capability(capable: false)
        @identity.reload
        @identity.sms_capable?.must_equal false
      end
    end
  end

  describe "scopes queries to account ID" do
    it "for user_phone_extension association" do
      identity = UserPhoneNumberIdentity.create!(user: @minimum_end_user, value: "+15551234567")
      identity.reload

      assert_sql_queries(1, /`user_phone_attributes`.`account_id` = #{identity.account_id}/) do
        identity.user_phone_extension
      end
    end
  end
end
