require_relative "../support/test_helper"

SingleCov.covered!

describe AccountServiceSubscription do
  let(:account)         { accounts(:minimum) }
  let(:product_name)    { AccountServiceSubscription::PRODUCT_NAME }
  let(:client)          { stub }

  let(:expected_product) do
    Zendesk::Accounts::Product.new(
      'state'            => Zendesk::Accounts::Product::SUBSCRIBED,
      'name'             => product_name,
      'trial_expires_at' => 10.days.ago,
      'plan_settings'    => {
        'attr_foo' => 3,
        'attr_bar' => 5
      }
    )
  end

  AccountServiceSubscription.const_set(:PRODUCT_NAME, 'product_foo')
  AccountServiceSubscription.const_set(:PLAN_SETTINGS, [:attr_foo, :attr_bar])
  AccountServiceSubscription.send(:define_attr_accessors)

  before do
    Zendesk::Accounts::Client.expects(:new).returns(client)
    client.stubs(product: expected_product)
  end

  describe '.create_for_account' do
    describe 'with no attributes' do
      let(:expected_payload) { { product: { plan_settings: {} } } }

      let(:subject) { AccountServiceSubscription.create_for_account(account) }

      it 'creates an empty product record in account service' do
        client.expects(:update_or_create_product!).with(product_name, expected_payload, context: "account_service_subscription")
        subject
      end

      it 'returns the created instance' do
        client.expects(:update_or_create_product!).with(product_name, expected_payload, context: "account_service_subscription")
        assert_equal AccountServiceSubscription, subject.class
      end
    end

    describe 'with attributes' do
      let(:expiration_date) { 1.month.from_now }

      let(:expected_attributes) do
        {
          product: {
            state: Zendesk::Accounts::Product::TRIAL,
            trial_expires_at: expiration_date.iso8601,
            plan_settings: {
              attr_foo: 3,
              attr_bar: 5
            }
          }
        }
      end

      it 'creates a  product record with passed attributes' do
        client.expects(:update_or_create_product!).with(product_name, expected_attributes, context: "account_service_subscription")
        AccountServiceSubscription.create_for_account(
          account,
          state: Zendesk::Accounts::Product::TRIAL,
          trial_expires_at: expiration_date,
          attr_foo: 3,
          attr_bar: 5
        )
      end
    end
  end

  describe 'when the product exists in account service' do
    let(:subject) { AccountServiceSubscription.find_for_account(account) }

    describe '.find_for_account' do
      it 'returns an AccountServiceSubscription' do
        assert_equal AccountServiceSubscription, subject.class
      end
    end

    describe 'the AccountServiceSubscription instance' do
      let(:delegated_methods) do
        %i[
          trial?
          expired?
          cancelled?
          subscribed?
          state
          trial_expires_at
        ]
      end

      it 'delegates expected methods to remote product' do
        remote = subject.remote
        delegated_methods.each do |attr_method|
          assert_equal remote.send(attr_method), subject.send(attr_method)
        end
      end

      describe 'dynamically-defined attr accessors' do
        it 'returns the attr stored in account service plan_settings' do
          expected_val = expected_product.plan_settings['attr_foo']
          assert_equal expected_val, subject.attr_foo
        end
      end
    end

    describe '#update_attributes' do
      describe 'with an invalid attribute' do
        it 'fails to update attributes' do
          assert_raises ArgumentError, 'Not a valid attribute' do
            subject.update_attributes(invalid_attr: 'invalid_val')
          end
        end
      end

      describe 'with an invalid state value' do
        it 'fails to update state' do
          assert_raises ArgumentError, 'Not a valid state' do
            subject.update_attributes(state: :foo)
          end
          assert_equal expected_product.state, subject.state
        end
      end

      describe 'with valid attributes' do
        let(:expiration_date) { 1.day.ago }

        let(:expected_update_params) do
          {
            product: {
              state: Zendesk::Accounts::Product::SUBSCRIBED,
              trial_expires_at: expiration_date.iso8601,
              plan_settings: {
                attr_foo: 3,
                attr_bar: 7
              }
            }
          }
        end

        it 'updates the attributes in account service' do
          client.expects(:update_or_create_product!).with(
            product_name,
            expected_update_params,
            context: "account_service_subscription"
          )

          assert subject.update_attributes(
            state: Zendesk::Accounts::Product::SUBSCRIBED,
            trial_expires_at: expiration_date,
            attr_foo: 3,
            attr_bar: 7
          )
        end

        describe 'passed as strings' do
          it 'updates the attributes in account service' do
            client.expects(:update_or_create_product!).with(
              product_name,
              expected_update_params,
              context: "account_service_subscription"
            )
            assert subject.update_attributes(
              'state'            => Zendesk::Accounts::Product::SUBSCRIBED,
              'trial_expires_at' => expiration_date,
              'attr_foo'         => 3,
              'attr_bar'         => 7
            )
          end
        end
      end
    end

    describe '#remote' do
      describe 'when account service client raises BadGateway error' do
        it 'returns nil' do
          client.expects(:product).raises(Kragle::BadGateway)
          assert_nil subject
        end
      end

      describe 'after being called initially' do
        before do
          subject.remote
        end

        it 'does not re-query account service on subsequent calls' do
          client.expects(:product).never
          subject.remote
        end

        describe 'calling #reload' do
          it 're-queries account service' do
            client.expects(:product).once
            subject.reload
          end
        end
      end
    end

    describe '#cancel!' do
      it 'sets state attribute in account service to "cancelled"' do
        subject.expects(:update_attributes).with(
          state: Zendesk::Accounts::Product::CANCELLED
        )
        subject.cancel!
      end
    end

    describe '#account_service_payload' do
      let(:expected_date) { 1.month.from_now }

      it 'converts dates to iso8601' do
        payload = subject.send(
          :account_service_payload,
          trial_expires_at: expected_date
        )
        assert_equal expected_date.iso8601, payload[:product][:trial_expires_at]
      end
    end
  end

  describe 'when no product exists' do
    let(:subject) { AccountServiceSubscription.find_for_account(account) }

    before do
      client.stubs(product: nil)
    end

    describe '.find_for_account' do
      it 'returns nil' do
        assert_nil subject
      end
    end
  end
end
