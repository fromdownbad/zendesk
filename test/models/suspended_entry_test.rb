require_relative "../support/test_helper"

SingleCov.covered!

describe 'SuspendedEntry' do
  fixtures :forums, :users, :entries, :accounts

  describe "Given an account with a forum and an entry" do
    let(:account) { accounts(:minimum) }
    let(:forum) { account.forums.first }

    let(:user) do
      users(:minimum_end_user).tap do |user|
        user.stubs(whitelisted_from_moderation?: false)
      end
    end

    it "has an account without suspended entries" do
      Entry.with_deleted do
        entries = account.entries.
          where('entries.deleted_at IS NOT NULL').
          where(flag_type_id: EntryFlagType::SUSPENDED.id)

        assert_empty(entries)
      end
    end

    describe "when forum has no suspended entries" do
      it "has some visible entries" do
        assert forum.entries.any?
      end

      describe ".for_account" do
        it "returns empty" do
          assert_empty(SuspendedEntry.for_account(account))
        end
      end

      describe ".submitted_by" do
        it "returns empty" do
          assert_empty(SuspendedEntry.submitted_by(account.users))
        end
      end

      describe ".for_forum" do
        it "returns empty" do
          assert_empty(SuspendedEntry.for_forum(forum))
        end
      end
    end

    describe "when account has moderated entries" do
      before do
        # Turn on moderation for account
        account.settings.moderated_entries = true
        account.settings.save!

        # The following will create a suspended entry.
        attrs = { title: "test", text: "test", account: account }
        @entry = forum.entries.create!(attrs) { |e| e.current_user = user }
      end

      it "has a suspended entry" do
        assert @entry.suspended?
      end

      describe ".for_account" do
        it "returns entry" do
          assert SuspendedEntry.for_account(account) == [@entry]
        end
      end

      describe ".submitted_by" do
        it "returns entry" do
          assert SuspendedEntry.submitted_by(account.users) == [@entry]
        end
      end

      describe ".for_forum" do
        it "returns entry" do
          assert SuspendedEntry.for_forum(forum) == [@entry]
        end
      end

      describe "multi-tenancy check" do
        let (:other_account) { accounts(:support) }
        let (:other_forum) do
          Forum.create(
            account: other_account,
            name: "bar",
            display_type_id: ForumDisplayType::QUESTIONS.id,
            visibility_restriction_id: VisibilityRestriction::EVERYBODY.id
          )
        end

        let(:other_user) do
          users(:support_report_user).tap do |user|
            user.stubs(whitelisted_from_moderation?: false)
          end
        end

        before do
          # Turn on moderation for other_account
          other_account.settings.moderated_entries = true
          other_account.settings.save!

          # The following will create a suspended entry.
          attrs = { title: "other", text: "test", account: other_account }
          @other_entry = other_forum.entries.create!(attrs) { |e| e.current_user = other_user }
        end

        it "has suspended entry" do
          assert @other_entry.suspended?
        end

        describe ".for_account" do
          it "does not return entry belonging to a different account" do
            refute SuspendedEntry.for_account(other_account).include?(@entry)
          end
        end

        describe ".submitted_by" do
          it "does not return entry submitted by users on other account" do
            refute SuspendedEntry.submitted_by(other_account.users).include?(@entry)
          end
        end

        describe ".for_forum" do
          it "does not return entry belonging to a forum on an other account" do
            refute SuspendedEntry.for_forum(other_forum).include?(@entry)
          end
        end
      end
    end
  end
end
