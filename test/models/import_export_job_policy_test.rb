require_relative "../support/test_helper"

SingleCov.covered!

describe ImportExportJobPolicy do
  fixtures :accounts

  let(:account)         { accounts(:minimum) }
  let(:user)            { users(:minimum_agent) }
  let(:job_policy)      { ImportExportJobPolicy.new(account, user) }
  let(:mock_export_configuration) { stub }

  describe "initialization" do
    it "takes an optional job key" do
      assert ImportExportJobPolicy.new(account, account.owner, "xml_export")
    end
  end

  describe "#klass" do
    it "returns the correct class for a given key" do
      job_policy.key = "xml_export"
      assert_equal XmlExportJob, job_policy.klass
    end

    it "returns nil if the given key isn't found" do
      job_policy.key = "foo"
      assert_nil job_policy.klass
    end
  end

  describe "#accessible_to_user?" do
    it "returns false if the user can't manage the Report" do
      refute user.can?(:manage, Report)
      refute job_policy.accessible_to_user?
    end

    describe "when user can manage the Report" do
      before { user.expects(:can?).with(:manage, Report).returns(true) }

      it "returns true if the user is whitelisted" do
        assert job_policy.accessible_to_user?
      end

      it "returns false if the user is not whitelisted" do
        Zendesk::Export::Configuration.
          expects(:new).with(account).returns(mock_export_configuration)
        mock_export_configuration.expects(:whitelisted?).with(user).returns(false)
        refute job_policy.accessible_to_user?
      end
    end
  end

  describe "#unavailable?" do
    it "returns false if the job does not respond to available_for" do
      refute ::UsersJob.respond_to?(:available_for?)
      job_policy.key = "user_import"

      refute job_policy.unavailable?
    end

    it "returns false if the job is available for the current_account" do
      assert ::ViewCsvJob.respond_to?(:available_for?)
      job_policy.key = "view_csv"

      refute job_policy.unavailable?
    end

    it "returns true if the job is not available for the current_account" do
      assert ::XmlExportJob.respond_to?(:available_for?)
      job_policy.key = "xml_export"

      assert job_policy.unavailable?
    end
  end

  describe "#inaccessible?" do
    it "returns false if the job is an import job" do
      job_policy.key = "user_import"
      refute job_policy.inaccessible?
    end

    it "returns false if the job is a view_csv job" do
      Zendesk::Export::Configuration.any_instance.stubs(whitelisted?: false)
      job_policy.key = "view_csv"
      refute job_policy.inaccessible?
    end

    it "returns true if the job type is not accessible" do
      mock_export_configuration.expects(:type_accessible?).with("xml_export").returns(false)
      Zendesk::Export::Configuration.
        expects(:new).with(account).returns(mock_export_configuration)
      job_policy.key = "xml_export"

      assert job_policy.inaccessible?
    end

    describe "the type is accessible for this account" do
      before do
        mock_export_configuration.stubs(whitelisted?: true)
        mock_export_configuration.
          expects(:type_accessible?).with("xml_export").returns(true)
        Zendesk::Export::Configuration.
          expects(:new).with(account).returns(mock_export_configuration)
        job_policy.key = "xml_export"
      end

      it "returns true if the user does not have permission" do
        refute user.can?(:manage, Report)
        assert job_policy.inaccessible?
      end

      describe "and user has permission to manage Report" do
        before { user.expects(:can?).with(:manage, Report).returns(true) }

        it "returns false if the user has permission and is whitelisted" do
          refute job_policy.inaccessible?
        end

        it "returns true if the configuration.whitelisted? returns false" do
          mock_export_configuration.expects(:whitelisted?).with(user).returns(false)
          assert job_policy.inaccessible?
        end
      end
    end
  end

  describe "#ticket_count_exceeds_max?" do
    describe "and the job class is not an XmlExportJob" do
      before { job_policy.key = "view_csv" }

      it "returns false if the job is not an xml_export" do
        Zendesk::Export::Configuration.expects(:new).never

        refute job_policy.ticket_count_exceeds_max?
      end
    end

    describe "and the job class is an XmlExportJob" do
      before do
        job_policy.key = "xml_export"
      end

      it "returns false if the export count is less than what the configuration allows" do
        account.tickets.stubs(count_with_archived: account.settings.export_ticket_max - 1)

        refute job_policy.ticket_count_exceeds_max?
      end

      it "returns true if the export count is more than what the configuration allows" do
        account.tickets.stubs(count_with_archived: account.settings.export_ticket_max + 1)

        assert job_policy.ticket_count_exceeds_max?
      end
    end
  end
end
