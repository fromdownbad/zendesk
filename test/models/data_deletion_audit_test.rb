require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'DataDeletionAudit' do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    ActiveRecord::Base.connection.execute <<-MYSQL
    UPDATE `subscriptions`
    SET
      `manual_discount` = 0,
      `is_trial` = 0,
      `churned_on` = '#{130.days.ago.to_s(:db)}',
      `updated_at` = '#{Time.now.to_s(:db)}'
    WHERE
      `subscriptions`.`id` = #{@account.subscription.id}
    MYSQL
    @account.update_column(:deleted_at, 40.days.ago)

    @audit = @account.data_deletion_audits.build_for_cancellation
    @audit.created_at = 40.days.ago
    @audit.save!
  end

  it '#build_for_cancellation' do
    audit = DataDeletionAudit.build_for_cancellation

    assert_equal 'canceled', audit.reason
  end

  it '#account' do
    assert_equal @account, @audit.account
  end

  describe '#attempts_by_job' do
    it 'returns records for that provided klass' do
      first_attempt  = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
      second_attempt = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
      other_attempt  = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveRiakDataJob).tap(&:save!)

      attempts = @audit.attempts_by_job(Zendesk::Maintenance::Jobs::RemoveShardDataJob)
      assert_includes attempts, first_attempt
      assert_includes attempts, second_attempt
      refute_includes attempts, other_attempt
    end

    it 'only executes the query to the master once and caches, for performance' do
      @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
      assert_sql_queries 1 do
        @audit.attempts_by_job(Zendesk::Maintenance::Jobs::RemoveShardDataJob).to_a
        @audit.attempts_by_job(Zendesk::Maintenance::Jobs::RemoveRiakDataJob).to_a
      end
    end
  end

  describe '#build_job_audit_for_klass' do
    it 'creates a new job record' do
      job = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
      assert_includes @audit.job_audits, job
    end

    it 'copies the shard_id from the audit' do
      @audit.shard_id = 1
      job = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
      job.shard_id.must_equal @audit.shard_id
    end
  end

  it '#cancel on waiting audit' do
    @audit.cancel!
    assert @audit.reload.canceled?
  end

  it '#cancel on started audit' do
    @audit.start!
    -> { @audit.cancel! }.must_raise(RuntimeError)
  end

  describe '#fail!' do
    it 'serializes the exception to JSON' do
      exception = -> { raise 'my message' }.must_raise RuntimeError
      @audit.fail!(exception)
      @audit.failed?.must_equal true
      @audit.failed_reason['klass'].must_equal 'RuntimeError'
      @audit.failed_reason['message'].must_equal 'my message'
    end
  end

  describe 'scopes' do
    it '#waiting' do
      dda = accounts(:minimum).data_deletion_audits.create!
      assert dda.status == 'waiting'
      assert DataDeletionAudit.waiting.include?(dda)
    end

    it '#active' do
      dda = accounts(:minimum).data_deletion_audits.create!
      dda.update_column(:status, 'started')
      assert DataDeletionAudit.active.include?(dda)
    end

    it '#for_cancellation' do
      dda = accounts(:minimum).data_deletion_audits.create!(reason: 'moved')
      dda.update_column(:reason, 'canceled')
      assert DataDeletionAudit.for_cancellation.include?(dda)
    end

    it '#enqueued' do
      dda = accounts(:minimum).data_deletion_audits.create!
      dda.update_column(:status, 'enqueued')
      assert DataDeletionAudit.enqueued.include?(dda)
    end

    describe '#pod_local_moved' do
      it 'include pod_local move' do
        dda = accounts(:minimum).data_deletion_audits.create!(reason: 'moved')
        dda.update_column(:shard_id, 1)
        assert DataDeletionAudit.pod_local_moved.include?(dda)
      end

      it 'excludes other pod moved' do
        dda = accounts(:minimum).data_deletion_audits.create!(reason: 'moved')
        dda.update_column(:shard_id, 99)
        refute DataDeletionAudit.pod_local_moved.include?(dda)
      end
    end

    describe '#pod_local_canceled' do
      it 'include pod_local canceled' do
        dda = accounts(:minimum).data_deletion_audits.create!(reason: 'canceled')
        assert DataDeletionAudit.pod_local_canceled.include?(dda)
      end
    end
  end
end
