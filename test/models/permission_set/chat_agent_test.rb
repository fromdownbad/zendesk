require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'ChatAgent' do
  fixtures :accounts, :users

  describe "create_chat_agent!" do
    let(:account)        { accounts(:minimum) }
    let(:permission_set) { PermissionSet.create_chat_agent!(account) }
    let(:method_call_instrumentation) { stub }

    before do
      account.settings.suite_subscription = suite_subscription
      account.settings.save!
      Zendesk::MethodCallInstrumentation.stubs(:new).returns(method_call_instrumentation)
      method_call_instrumentation.stubs(:perform)
    end

    describe 'when the account has a :suite_subscription setting' do
      let(:suite_subscription) { true }

      it "does not create a permission set" do
        assert_no_difference 'account.permission_sets.count' do
          permission_set
        end
      end

      it "returns nil" do
        assert_nil permission_set
      end

      it 'performs method call instrumentation' do
        method_call_instrumentation.expects(:perform)
        permission_set
      end
    end

    describe 'when the account does not have a :suite_subscription setting' do
      let(:suite_subscription) { false }

      it "produces a correctly configured permission set" do
        assert_equal  "within-groups", permission_set.permissions.ticket_access
        assert_equal  false, permission_set.permissions.ticket_editing?
        assert_equal  false, permission_set.permissions.ticket_deletion?
        assert_equal  false, permission_set.permissions.view_deleted_tickets?
        assert_equal  false, permission_set.permissions.ticket_merge?
        assert_equal  false, permission_set.permissions.edit_ticket_tags?
        assert_equal  false, permission_set.permissions.ticket_bulk_edit?

        assert_equal  "private", permission_set.permissions.comment_access
        assert_equal  "none", permission_set.permissions.explore_access
        assert_equal  "none", permission_set.permissions.report_access
        assert_equal  "readonly", permission_set.permissions.view_access
        assert_equal  "readonly", permission_set.permissions.macro_access

        assert_equal  "readonly", permission_set.permissions.end_user_profile
        assert_equal  "none", permission_set.permissions.available_user_lists
        assert_equal  false, permission_set.permissions.edit_organizations?

        assert_equal  "readonly", permission_set.permissions.forum_access
        assert_equal  false, permission_set.permissions.forum_access_restricted_content?

        assert_equal  false, permission_set.permissions.voice_availability_access?
        assert(permission_set.permissions.chat_availability_access?)

        assert_equal  false, permission_set.permissions.business_rule_management?
        assert_equal  false, permission_set.permissions.extensions_and_channel_management?

        assert_equal  false, permission_set.permissions.view_twitter_searches?

        assert_equal  false, permission_set.permissions.manage_dynamic_content?
      end

      it 'performs method call instrumentation' do
        method_call_instrumentation.expects(:perform)
        permission_set
      end

      describe_with_arturo_enabled :ocp_chat_only_agent_deprecation do
        it 'enables contributor for account' do
          PermissionSet.expects(:enable_contributor!).with(account)
          permission_set
        end
      end

      describe '#translated_name' do
        it "always return the static name" do
          permission_set.update_attribute(:name, "A name")
          assert_equal I18n.t("txt.default.roles.chat_agent.name"), permission_set.translated_name
        end
      end

      describe '#translated_description' do
        it "always return the static description" do
          permission_set.update_attribute(:description, "A description")
          assert_equal I18n.t("txt.default.roles.chat_agent.description"), permission_set.translated_description
        end
      end

      it "does not allow ticket access changes" do
        permission_set.permissions.ticket_access = "all"
        refute permission_set.valid?
      end

      it "does not allow 'assign tickets to any group' changes" do
        permission_set.permissions.enable(:assign_tickets_to_any_group)
        refute permission_set.valid?
      end

      it "does not allow changes in other permissions" do
        permission_set.permissions.forum_access = "full"
        refute permission_set.valid?
      end

      it "does not allow access to reports to be readonly" do
        permission_set.permissions.report_access = "readonly"
        refute permission_set.valid?
      end

      it "does not allow full access to reporting" do
        permission_set.permissions.report_access = "full"
        refute permission_set.valid?
      end

      describe "called multiple times" do
        it "does not create multiple chat agent permission sets" do
          PermissionSet.create_chat_agent!(accounts(:minimum))
          refute_difference 'PermissionSet.count(:all)' do
            PermissionSet.create_chat_agent!(accounts(:minimum))
          end
        end
      end
    end
  end

  describe "destroy_chat_agent!" do
    let(:account)        { accounts(:minimum) }
    let(:chat_role_type) { PermissionSet::Type::CHAT_AGENT }

    subject { PermissionSet.destroy_chat_agent!(account) }

    describe 'when the account has a CHAT_AGENT permission_set' do
      let!(:permission_set) { PermissionSet.create_chat_agent!(account) }

      before do
        assert account.permission_sets.find_by_role_type(chat_role_type)
      end

      it 'destroys the permission_set' do
        subject
        refute account.permission_sets.find_by_role_type(chat_role_type)
      end
    end

    describe 'when the account does not have a CHAT_AGENT permission_set' do
      before do
        refute account.permission_sets.find_by_role_type(chat_role_type)
      end

      it 'does not destroy any permission_sets' do
        PermissionSet.any_instance.expects(:try).with(:destroy).never
        subject
      end
    end
  end
end
