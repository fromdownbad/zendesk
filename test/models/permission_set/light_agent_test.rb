require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 5

describe 'LightAgent' do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }

  describe "create_light_agent!" do
    let(:permission_set) { PermissionSet.create_light_agent!(account) }

    it "produces a correctly configured permission set" do
      assert_equal  "within-groups", permission_set.permissions.ticket_access
      assert_equal  false, permission_set.permissions.ticket_editing?
      assert_equal  false, permission_set.permissions.ticket_deletion?
      assert_equal  false, permission_set.permissions.view_deleted_tickets?
      assert_equal  false, permission_set.permissions.ticket_merge?
      assert_equal  false, permission_set.permissions.edit_ticket_tags?
      assert(permission_set.permissions.ticket_bulk_edit?)

      assert_equal  "private", permission_set.permissions.comment_access
      assert_equal  "readonly", permission_set.permissions.explore_access
      assert_equal  "readonly", permission_set.permissions.report_access
      assert_equal  "readonly", permission_set.permissions.view_access
      assert_equal  "readonly", permission_set.permissions.macro_access

      assert_equal  "readonly", permission_set.permissions.end_user_profile
      assert_equal  "all", permission_set.permissions.available_user_lists
      assert_equal  false, permission_set.permissions.edit_organizations?

      assert_equal  "readonly", permission_set.permissions.forum_access
      assert_equal  false, permission_set.permissions.forum_access_restricted_content?

      assert_equal  false, permission_set.permissions.voice_availability_access?
      assert_equal  false, permission_set.permissions.chat_availability_access?

      assert_equal  false, permission_set.permissions.business_rule_management?
      assert_equal  false, permission_set.permissions.extensions_and_channel_management?

      assert(permission_set.permissions.view_twitter_searches?)

      assert_equal false, permission_set.permissions.manage_dynamic_content?
    end

    describe '#translated_name' do
      it "always return the static name" do
        permission_set.update_attribute(:name, "A name")
        assert_equal I18n.t("txt.default.roles.light_agent.name"), permission_set.translated_name
      end
    end

    describe '#translated_description' do
      it "always return the static description" do
        permission_set.update_attribute(:description, "A description")
        assert_equal I18n.t("txt.default.roles.light_agent.description"), permission_set.translated_description
      end
    end

    it "allows ticket access changes" do
      permission_set.permissions.ticket_access = "all"
      assert permission_set.valid?
    end

    it "allows access to view satisfaction prediction on ticket" do
      permission_set.permissions.enable(:view_ticket_satisfaction_prediction)
      assert permission_set.valid?
    end

    it "allows accesss to view satisfaction prediction on ticket to be removed" do
      permission_set.permissions.view_ticket_satisfaction_prediction = 0
      assert permission_set.valid?
    end

    it "allows 'assign tickets to any group' changes" do
      permission_set.permissions.enable(:assign_tickets_to_any_group)
      assert permission_set.valid?
    end

    it "does not allow changes in other permissions" do
      permission_set.permissions.forum_access = "full"
      refute permission_set.valid?
    end

    it "allows access to reports to be removed" do
      permission_set.permissions.report_access = "none"
      assert permission_set.valid?
    end

    it "does not allow full access to reporting" do
      permission_set.permissions.report_access = "full"
      refute permission_set.valid?
    end

    describe "called multiple times" do
      it "does not create multiple light agent permission sets" do
        PermissionSet.create_light_agent!(accounts(:minimum))
        refute_difference 'PermissionSet.count(:all)' do
          PermissionSet.create_light_agent!(accounts(:minimum))
        end
      end
    end

    it 'syncs the role to Pravda' do
      Omnichannel::RoleSyncJob.expects(:enqueue).with(has_entries(
        account_id: account.id,
        role_name: 'light_agent',
        context: 'light_agent_enabled'
      ))
      permission_set
    end
  end

  describe '.destroy_light_agent!' do
    let(:account) { accounts(:minimum) }
    let!(:permission_set) { PermissionSet.create_light_agent!(account) }

    it 'destroys the permission_set' do
      PermissionSet.destroy_light_agent!(account)
      assert_nil account.reload.light_agent_permission_set
    end

    it 'deactivates the role in Pravda' do
      Omnichannel::RoleSyncJob.expects(:enqueue).with(
        account_id: account.id,
        permission_set_id: permission_set.id,
        role_name: 'light_agent',
        context: 'light_agent_disabled',
        permissions_changed: []
      )
      PermissionSet.destroy_light_agent!(account)
    end

    describe 'multiple calls' do
      it 'is idempotent' do
        PermissionSet.destroy_light_agent!(account)
        PermissionSet.destroy_light_agent!(account.reload)
        assert_nil account.reload.light_agent_permission_set
      end
    end
  end
end
