require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'Contributor' do
  fixtures :accounts, :users

  describe "enable_contributor!" do
    let(:account)        { accounts(:minimum) }
    let(:permission_set) { PermissionSet.enable_contributor!(account) }

    it 'can only access tickets that are assigned to them' do
      assert_equal 'within-groups', permission_set.permissions.ticket_access
    end

    it 'can not edit tickets' do
      assert_equal  false, permission_set.permissions.ticket_editing?
    end

    it 'can not delete tickets' do
      assert_equal  false, permission_set.permissions.ticket_deletion?
    end

    it 'can not view deleted tickets' do
      assert_equal  false, permission_set.permissions.view_deleted_tickets?
    end

    it 'can not merge tickets' do
      assert_equal  false, permission_set.permissions.ticket_merge?
    end

    it 'can not edit ticket tags' do
      assert_equal  false, permission_set.permissions.edit_ticket_tags?
    end

    it "can not bulk edit tickets" do
      assert_equal  false, permission_set.permissions.ticket_bulk_edit?
    end

    it 'can not access comments' do
      assert_equal  "private", permission_set.permissions.comment_access
    end

    it 'can not access explore' do
      assert_equal  "none", permission_set.permissions.explore_access
    end

    it 'can not access reports' do
      assert_equal  "none", permission_set.permissions.report_access
    end

    it 'has readonly views access' do
      assert_equal  "readonly", permission_set.permissions.view_access
    end

    it 'has readonly macro access' do
      assert_equal  "readonly", permission_set.permissions.macro_access
    end

    it 'can only view end users' do
      assert_equal  "readonly", permission_set.permissions.end_user_profile
    end

    it 'can not access user lists' do
      assert_equal  "none", permission_set.permissions.available_user_lists
    end

    it 'can not edit organizations' do
      assert_equal  false, permission_set.permissions.edit_organizations?
    end

    it 'can only view forums' do
      assert_equal  "readonly", permission_set.permissions.forum_access
    end

    it 'can not access restricted forums' do
      assert_equal  false, permission_set.permissions.forum_access_restricted_content?
    end

    it 'can not access voice' do
      assert_equal  false, permission_set.permissions.voice_availability_access?
    end

    it 'can not access chat' do
      assert_equal  false, permission_set.permissions.chat_availability_access?
    end

    it 'can not manage business rules' do
      assert_equal  false, permission_set.permissions.business_rule_management?
    end

    it 'can not manage channels' do
      assert_equal  false, permission_set.permissions.extensions_and_channel_management?
    end

    it 'can not view twitter searches' do
      assert_equal  false, permission_set.permissions.view_twitter_searches?
    end

    it 'can not mange dynamic content' do
      assert_equal  false, permission_set.permissions.manage_dynamic_content?
    end

    describe '#translated_name' do
      it "always return the static name" do
        permission_set.update_attribute(:name, "A name")
        assert_equal I18n.t("txt.default.roles.contributor.name"), permission_set.translated_name
      end
    end

    describe '#translated_description' do
      it "always return the static description" do
        permission_set.update_attribute(:description, "A description")
        assert_equal I18n.t("txt.default.roles.contributor.description"), permission_set.translated_description
      end
    end

    it "does not allow ticket access changes" do
      permission_set.permissions.ticket_access = "all"
      refute permission_set.valid?
    end

    it "does not allow 'assign tickets to any group' changes" do
      permission_set.permissions.enable(:assign_tickets_to_any_group)
      refute permission_set.valid?
    end

    it "does not allow changes in other permissions" do
      permission_set.permissions.forum_access = "full"
      refute permission_set.valid?
    end

    it "does not allow access to reports to be readonly" do
      permission_set.permissions.report_access = "readonly"
      refute permission_set.valid?
    end

    it "does not allow full access to reporting" do
      permission_set.permissions.report_access = "full"
      refute permission_set.valid?
    end

    describe "called multiple times" do
      it "does not create multiple restricted agent permission sets" do
        PermissionSet.enable_contributor!(accounts(:minimum))
        refute_difference 'PermissionSet.count(:all)' do
          PermissionSet.enable_contributor!(accounts(:minimum))
        end
      end
    end

    describe "when account already has custom role named Contributor" do
      let(:custom_role) do
        account.permission_sets.create(
          name: I18n.t('txt.default.roles.contributor.name'),
          description: 'Custom role with the same name'
        )
      end

      it 'renames custom role with prefixed name' do
        custom_role
        permission_set
        assert_equal custom_role.reload.name, 'Contributor (Custom)'
        assert_equal permission_set.name, 'Contributor'
      end
    end
  end

  describe "destroy_contributor!" do
    let(:account) { accounts(:multiproduct) }
    let(:restricted_role_type) { PermissionSet::Type::CONTRIBUTOR }

    subject { PermissionSet.destroy_contributor!(account) }

    describe 'when the account has a CONTRIBUTOR permission_set' do
      before do
        PermissionSet.enable_contributor!(account)
        assert account.permission_sets.find_by_role_type(restricted_role_type)
      end

      it 'tries to destroy the permission_set' do
        PermissionSet.any_instance.expects(:try).with(:destroy).once
        subject
      end
    end

    describe 'when the account does not have a CONTRIBUTOR permission_set' do
      let(:account) { accounts(:minimum) }

      before do
        refute account.permission_sets.find_by_role_type(restricted_role_type)
      end

      it 'does not destroy any permission_sets' do
        PermissionSet.any_instance.expects(:try).with(:destroy).never
        subject
      end
    end
  end

  describe '.contributor_exists?' do
    let(:account) { accounts(:minimum) }

    describe 'with Contributor permission set' do
      before do
        PermissionSet.enable_contributor!(account)
      end

      it 'is true' do
        assert PermissionSet.contributor_exists?(account)
      end
    end

    describe 'without Contributor permission set' do
      before do
        PermissionSet.destroy_contributor!(account)
      end

      it 'is false' do
        refute PermissionSet.contributor_exists?(account)
      end
    end
  end
end
