require_relative '../../support/test_helper'

SingleCov.covered!

describe 'DefaultRoles' do
  fixtures :accounts

  describe '#build_default_permission_sets' do
    let(:staff) { PermissionSet.build_default_permission_sets(accounts(:minimum))[0] }
    let(:team_leader) { PermissionSet.build_default_permission_sets(accounts(:minimum))[1] }
    let(:advisor) { PermissionSet.build_default_permission_sets(accounts(:minimum))[2] }

    it 'returns valid permission sets' do
      [staff, team_leader, advisor].each do |permission_set|
        assert permission_set.save
      end
    end

    describe_with_arturo_disabled :central_admin_staff_mgmt_roles_tab do
      it 'builds the default permission sets' do
        assert_equal 'Staff', staff.name
        assert_equal PermissionSet::Type::CUSTOM, staff.role_type
        assert_equal(
          {
            manage_dynamic_content:            false,
            manage_facebook:                   false,
            ticket_editing:                    true,
            ticket_access:                     'within-groups',
            edit_ticket_tags:                  false,
            ticket_deletion:                   false,
            view_deleted_tickets:              false,
            ticket_merge:                      false,
            ticket_bulk_edit:                  true,
            comment_access:                    'public',
            explore_access:                    'readonly',
            report_access:                     'readonly',
            view_access:                       'manage-personal',
            macro_access:                      'manage-personal',
            user_view_access:                  'manage-personal',
            end_user_profile:                  'readonly',
            forum_access:                      'readonly',
            forum_access_restricted_content:   false,
            voice_dashboard_access:            false,
            business_rule_management:          false,
            extensions_and_channel_management: false,
            view_twitter_searches:             true
          }.stringify_keys,
          staff.permissions.get(staff.permissions.map(&:name))
        )

        assert_equal 'Team lead', team_leader.name
        assert_equal PermissionSet::Type::CUSTOM, team_leader.role_type
        assert_equal(
          {
            manage_dynamic_content:            false,
            manage_facebook:                   false,
            ticket_editing:                    true,
            ticket_access:                     'all',
            edit_ticket_tags:                  true,
            ticket_deletion:                   true,
            view_deleted_tickets:              true,
            ticket_merge:                      true,
            ticket_bulk_edit:                  true,
            comment_access:                    'public',
            explore_access:                    'edit',
            report_access:                     'full',
            view_access:                       'full',
            macro_access:                      'full',
            user_view_access:                  'full',
            end_user_profile:                  'full',
            forum_access:                      'full',
            forum_access_restricted_content:   true,
            voice_dashboard_access:            true,
            business_rule_management:          false,
            extensions_and_channel_management: false,
            view_twitter_searches:             true
          }.stringify_keys,
          team_leader.permissions.get(team_leader.permissions.map(&:name))
        )

        assert_equal 'Advisor', advisor.name
        assert_equal PermissionSet::Type::CUSTOM, advisor.role_type
        assert_equal(
          {
            manage_dynamic_content:            false,
            manage_facebook:                   false,
            ticket_editing:                    true,
            ticket_access:                     'within-groups',
            edit_ticket_tags:                  true,
            ticket_deletion:                   false,
            view_deleted_tickets:              false,
            ticket_merge:                      false,
            ticket_bulk_edit:                  true,
            comment_access:                    'private',
            explore_access:                    'none',
            report_access:                     'none',
            view_access:                       'full',
            macro_access:                      'full',
            user_view_access:                  'full',
            end_user_profile:                  'readonly',
            forum_access:                      'readonly',
            forum_access_restricted_content:   false,
            voice_dashboard_access:            false,
            business_rule_management:          true,
            extensions_and_channel_management: true,
            view_twitter_searches:             true
          }.stringify_keys,
          advisor.permissions.get(advisor.permissions.map(&:name))
        )
      end
    end

    describe_with_arturo_enabled :central_admin_staff_mgmt_roles_tab do
      it 'builds the default permission sets' do
        assert_equal 'Staff', staff.name
        assert_equal PermissionSet::Type::CUSTOM, staff.role_type
        assert_equal(
          {
            manage_dynamic_content:            false,
            manage_facebook:                   false,
            ticket_editing:                    true,
            ticket_access:                     'within-groups',
            edit_ticket_tags:                  false,
            ticket_deletion:                   false,
            view_deleted_tickets:              false,
            ticket_merge:                      false,
            ticket_bulk_edit:                  true,
            comment_access:                    'public',
            explore_access:                    'readonly',
            report_access:                     'readonly',
            view_access:                       'manage-personal',
            macro_access:                      'manage-personal',
            user_view_access:                  'manage-personal',
            end_user_profile:                  'readonly',
            forum_access:                      'readonly',
            forum_access_restricted_content:   false,
            voice_dashboard_access:            false,
            business_rule_management:          false,
            extensions_and_channel_management: false,
            view_twitter_searches:             true
          }.stringify_keys,
          staff.permissions.get(staff.permissions.map(&:name))
        )

        assert_equal 'Team lead', team_leader.name
        assert_equal PermissionSet::Type::CUSTOM, team_leader.role_type
        assert_equal(
          {
            manage_dynamic_content:            false,
            manage_facebook:                   false,
            ticket_editing:                    true,
            ticket_access:                     'all',
            edit_ticket_tags:                  true,
            ticket_deletion:                   true,
            view_deleted_tickets:              true,
            ticket_merge:                      true,
            ticket_bulk_edit:                  true,
            comment_access:                    'public',
            explore_access:                    'edit',
            report_access:                     'full',
            view_access:                       'full',
            macro_access:                      'full',
            user_view_access:                  'full',
            end_user_profile:                  'full',
            forum_access:                      'full',
            forum_access_restricted_content:   true,
            voice_dashboard_access:            true,
            business_rule_management:          false,
            extensions_and_channel_management: false,
            view_twitter_searches:             true
          }.stringify_keys,
          team_leader.permissions.get(team_leader.permissions.map(&:name))
        )

        assert_equal 'Advisor', advisor.name
        assert_equal PermissionSet::Type::CUSTOM, advisor.role_type
        assert_equal(
          {
            manage_dynamic_content:            false,
            manage_facebook:                   false,
            ticket_editing:                    true,
            ticket_access:                     'within-groups',
            edit_ticket_tags:                  true,
            ticket_deletion:                   false,
            view_deleted_tickets:              false,
            ticket_merge:                      false,
            ticket_bulk_edit:                  true,
            comment_access:                    'private',
            explore_access:                    'readonly',
            report_access:                     'none',
            view_access:                       'full',
            macro_access:                      'full',
            user_view_access:                  'full',
            end_user_profile:                  'readonly',
            forum_access:                      'readonly',
            forum_access_restricted_content:   false,
            voice_dashboard_access:            false,
            business_rule_management:          true,
            extensions_and_channel_management: true,
            view_twitter_searches:             true
          }.stringify_keys,
          advisor.permissions.get(advisor.permissions.map(&:name))
        )
      end
    end
  end

  describe '#build_new_permission_set' do
    let(:role) { PermissionSet.build_new_permission_set(accounts(:minimum)) }

    it 'builds the permission set with the correct default permissions' do
      assert_nil role.name
      assert_nil role.description
      assert_equal PermissionSet::Type::CUSTOM, role.role_type
      assert_equal(
        {
          manage_dynamic_content:            false,
          manage_facebook:                   false,
          ticket_editing:                    true,
          ticket_access:                     'all',
          edit_ticket_tags:                  false,
          ticket_deletion:                   false,
          view_deleted_tickets:              false,
          ticket_merge:                      false,
          ticket_bulk_edit:                  true,
          comment_access:                    'public',
          explore_access:                    'readonly',
          report_access:                     'none',
          view_access:                       'readonly',
          macro_access:                      'readonly',
          user_view_access:                  'readonly',
          end_user_profile:                  'readonly',
          edit_organizations:                false,
          forum_access:                      'readonly',
          forum_access_restricted_content:   false,
          voice_dashboard_access:            false,
          business_rule_management:          false,
          extensions_and_channel_management: false,
          view_twitter_searches:             false,
          chat_availability_access:          false
        }.stringify_keys,
        role.permissions.get(role.permissions.map(&:name))
      )
    end
  end
end
