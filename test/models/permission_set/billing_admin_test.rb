require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 5

describe 'BillingAdmin' do
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }

  describe "create_billing_admin!" do
    let(:permission_set) { PermissionSet.create_billing_admin!(account) }

    it "produces a correctly configured permission set" do
      assert_equal "all", permission_set.permissions.ticket_access
      assert(permission_set.permissions.ticket_editing?)
      assert(permission_set.permissions.ticket_deletion?)
      assert(permission_set.permissions.view_deleted_tickets?)
      assert(permission_set.permissions.ticket_merge?)
      assert(permission_set.permissions.edit_ticket_tags?)
      assert(permission_set.permissions.ticket_bulk_edit?)

      assert_equal "public", permission_set.permissions.comment_access
      assert_equal "full", permission_set.permissions.explore_access
      assert_equal "full", permission_set.permissions.report_access
      assert_equal "full", permission_set.permissions.view_access
      assert_equal "full", permission_set.permissions.macro_access

      assert_equal "full", permission_set.permissions.end_user_profile
      assert_equal "all", permission_set.permissions.available_user_lists
      assert(permission_set.permissions.edit_organizations?)

      assert_equal "full", permission_set.permissions.forum_access
      assert(permission_set.permissions.forum_access_restricted_content?)

      assert(permission_set.permissions.voice_availability_access?)
      assert(permission_set.permissions.chat_availability_access?)

      assert(permission_set.permissions.business_rule_management?)
      assert(permission_set.permissions.extensions_and_channel_management?)

      assert(permission_set.permissions.view_twitter_searches?)

      assert(permission_set.permissions.manage_dynamic_content?)
      assert(permission_set.permissions.manage_facebook?)
    end

    describe '#translated_name' do
      it "always return the static name" do
        permission_set.update_attribute(:name, "A name")
        assert_equal I18n.t("txt.default.roles.billing_admin.name"), permission_set.translated_name
      end
    end

    describe '#translated_description' do
      it "always return the static description" do
        permission_set.update_attribute(:description, "A description")
        assert_equal I18n.t("txt.default.roles.billing_admin.description"), permission_set.translated_description
      end
    end

    describe "called multiple times" do
      it "does not create multiple billing admin permission sets" do
        PermissionSet.create_billing_admin!(accounts(:minimum))
        refute_difference 'PermissionSet.count(:all)' do
          PermissionSet.create_billing_admin!(accounts(:minimum))
        end
      end
    end
  end

  describe '.destroy_billing_admin!' do
    let(:account) { accounts(:minimum) }
    let(:admin_role_type) { PermissionSet::Type::BILLING_ADMIN }

    subject { PermissionSet.destroy_billing_admin!(account) }

    describe 'when the account has a BILLING_ADMIN permission_set' do
      before do
        PermissionSet.create_billing_admin!(account)
        assert account.permission_sets.find_by_role_type(admin_role_type)
      end

      it 'tries to destroy the permission_set' do
        PermissionSet.any_instance.expects(:try).with(:destroy).once
        subject
      end
    end

    describe 'when the account does not have a BILLING_ADMIN permission_set' do
      let(:account) { accounts(:minimum) }

      before do
        refute account.permission_sets.find_by_role_type(admin_role_type)
      end

      it 'does not destroy any permission_sets' do
        PermissionSet.any_instance.expects(:try).with(:destroy).never
        subject
      end
    end
  end
end
