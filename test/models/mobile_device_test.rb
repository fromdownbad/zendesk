require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'MobileDevice' do
  fixtures :accounts, :users, :devices

  before do
    Zendesk::GeoLocation.stubs(:locate).returns(city: 'Oconto', country: 'USA')
    @account    = accounts(:minimum)
    @user       = users(:minimum_agent)
    @user_agent = 'Zendesk for iPhone'
    @params = {ip: '1.2.3.4', user_agent: @user_agent}
  end

  describe 'name extraction' do
    describe 'on creation' do
      it "parses the useragent" do
        device = @user.mobile_devices.create!(@params.merge(account: @account, token: 'abc123'))
        assert_equal @user_agent, device.reload.name
      end
    end

    describe 'on update' do
      before do
        @device = @user.mobile_devices.create!(@params.merge(account: @account))
      end

      it 'does not update activity' do
        timestamp = @device.last_active_at
        @device.update_attribute(:name, 'Work Phone')
        assert_equal timestamp.to_s, @device.reload.last_active_at.to_s
      end

      it 'does not overwrite a name' do
        @device.update_attribute(:name, 'Work Phone')
        @device.update_attribute(:ip, '1.2.3.4')
        assert_equal 'Work Phone', @device.reload.name
      end

      it 'parses over an empty name' do
        @device.update_attribute(:name, '')
        assert_equal @user_agent, @device.reload.name
      end

      it 'parses over a nil name' do
        @device.update_attribute(:name, nil)
        assert_equal @user_agent, @device.reload.name
      end
    end
  end

  describe '#mobile?' do
    before do
      @device = MobileDevice.new
    end

    it 'is true' do
      assert @device.mobile?
    end
  end

  describe 'removing a device with active oauth token' do
    let(:client) { @account.clients.create!(identifier: "zendesk_mobile_ios", name: "test", secret: "test") }
    let(:token) { client.tokens.create!(account_id: @account.id, user_id: @user.id, scopes: "read") }

    before do
      @device = @user.mobile_devices.build(@params.merge(account: @account))
    end

    it 'destroys the oauth token linked via token' do
      @device.update_attributes!(token: token.id)
      assert_difference('Zendesk::OAuth::Token.count(:all)', -1) do
        @device.destroy
      end
    end

    it 'destroys the oauth token linked via token_id' do
      @device.oauth_token = token; @device.save!
      assert_difference('Zendesk::OAuth::Token.count(:all)', -1) do
        @device.destroy
      end
    end
  end

  describe '#revoke_token' do
    let(:client) { @account.clients.create!(identifier: "zendesk_mobile_ios", name: "test", secret: "test") }
    let(:token) { client.tokens.create!(account_id: @account.id, user_id: @user.id, scopes: "read") }

    before do
      @device = @user.mobile_devices.build(@params.merge(account: @account))
    end

    it 'destroys the oauth token linked via token' do
      @device.update_attributes!(token: token.id)
      assert_difference('Zendesk::OAuth::Token.count(:all)', -1) do
        @device.revoke_token
      end
    end

    it 'destroys the oauth token linked via token_id' do
      @device.oauth_token = token; @device.save!
      assert_difference('Zendesk::OAuth::Token.count(:all)', -1) do
        @device.revoke_token
      end
    end
  end

  describe "#register_token!" do
    let(:client) { @account.clients.create!(identifier: "zendesk_mobile_ios", name: "test", secret: "test") }
    let(:token) { client.tokens.create!(account_id: @account.id, user_id: @user.id, scopes: "read") }
    let(:new_token) { client.tokens.create!(account_id: @account.id, user_id: @user.id, scopes: "read") }

    before do
      d = @device = @user.mobile_devices.create
      d.account = @account
      d.oauth_token = token
      d.save
    end

    it 'deletes the existing token' do
      @device.register_token!(new_token)

      assert_raises(ActiveRecord::RecordNotFound) { token.reload }
    end

    it 'assigns the token to this device' do
      @device.register_token!(new_token)
      assert_equal new_token, @device.oauth_token
    end

    it 'does not blow up if the existing token_id does not exist' do
      token.destroy
      @device.reload.register_token!(new_token)
    end
  end

  describe 'removing a device with no oauth token' do
    let(:client) { @account.clients.create!(identifier: "zendesk_mobile_ios", name: "test", secret: "test") }
    let(:token) { client.tokens.create!(account_id: @account.id, user_id: @user.id, scopes: "read") }

    before do
      @device = @user.mobile_devices.build(@params.merge(account: @account))
      @device.token = '0000'
      @device.save!
    end

    it 'is okay if oauth token is missing' do
      refute_difference('Zendesk::OAuth::Token.count(:all)') do
        @device.destroy
      end
    end
  end
end
