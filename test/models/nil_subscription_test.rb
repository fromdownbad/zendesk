require_relative '../support/test_helper'

SingleCov.covered!

describe NilSubscription do
  let(:account) { accounts(:minimum) }
  let(:nil_subscription) { NilSubscription.new(account) }

  describe '.nil_plan_type' do
    it 'is not nil' do
      assert_not_nil NilSubscription.nil_plan_type
    end

    it 'is not an existing plan type' do
      assert(
        [
          SubscriptionPlanType.Small,
          SubscriptionPlanType.Medium,
          SubscriptionPlanType.Large,
          SubscriptionPlanType.ExtraLarge
        ].none? { |plan_type| NilSubscription.nil_plan_type == plan_type },
        'matches an existing plan type when it should not'
      )
    end
  end

  describe '#is_trial?' do
    describe 'shell account' do
      let(:account) { accounts(:multiproduct) }

      it 'is false' do
        assert_equal false, nil_subscription.is_trial?
      end
    end

    describe 'non-shell account' do
      let(:account) { accounts(:minimum) }

      it 'raises an error' do
        assert_raises RuntimeError do
          nil_subscription.is_trial?
        end
      end
    end
  end

  describe '#present?' do
    it 'is false' do
      assert_equal false, nil_subscription.present?
    end
  end

  describe '#blank?' do
    it 'is true' do
      assert(nil_subscription.blank?)
    end
  end

  describe '#nil?' do
    it 'is true' do
      assert(nil_subscription.nil?)
    end
  end

  describe '#plan_type' do
    it 'returns the nil plan type' do
      assert_equal NilSubscription.nil_plan_type, nil_subscription.plan_type
    end
  end

  describe '#payments' do
    it 'returns an empty collection' do
      assert_empty nil_subscription.payments
    end
  end

  describe 'a feature check without explicitly defining the feature method' do
    it 'returns false' do
      assert_equal false, nil_subscription.has_high_volume_api?
    end
  end
end
