require_relative '../support/test_helper'
require_relative '../support/billing_test_helper'
require_relative '../support/suite_test_helper'
require 'zendesk_billing_core/zuora/client'

SingleCov.covered! uncovered: 61

describe Subscription do
  fixtures :all

  include SuiteTestHelper
  include BillingTestHelper
  include ZBC::ZuoraTestHelper

  before do
    AlignUserTimeZonesJob.stubs(:enqueue)
    UpdateAppsFeatureJob.stubs(:enqueue)

    Account.any_instance.stubs(:use_status_hold?).returns(true)
  end

  describe '#initialize' do
    before do
      @subscription = Subscription.new
      @subscription.account = accounts(:minimum)
    end

    it 'has correct default values' do
      assert_equal BillingCycleType.Annually,    @subscription.billing_cycle_type
      assert_equal Subscription::DEFAULT_AGENTS, @subscription.base_agents
      assert_equal Subscription::DEFAULT_AGENTS, @subscription.max_agents
      assert_equal SubscriptionPlanType.Large,   @subscription.plan_type
    end

    it 'sets base_agents to equal max_agents' do
      @subscription.save!
      assert_equal @subscription.max_agents, @subscription.base_agents
    end

    describe '#plan_name' do
      it 'has the correct plan_name' do
        assert_equal 'Professional', @subscription.plan_name
      end
    end

    describe '#file_upload_cap' do
      describe_with_arturo_enabled :email_increase_attachment_size_limit do
        it 'has the correct file_upload_cap' do
          assert_equal 50, @subscription.file_upload_cap
        end
      end

      describe_with_arturo_disabled :email_increase_attachment_size_limit do
        it 'has the correct file_upload_cap' do
          assert_equal 20, @subscription.file_upload_cap
        end
      end
    end

    describe '#max_monitored_twitter_handles' do
      it 'returns 10' do
        assert_equal 10, @subscription.max_monitored_twitter_handles
      end
    end

    describe '#pricing_model_revision' do
      it 'sets new subscriptions to PATAGONIA PRICING MODEL REVISION' do
        assert_equal ZBC::Zendesk::PricingModelRevision::PATAGONIA, @subscription.pricing_model_revision
      end

      describe '#plan_name' do
        it 'has the correct plan_name' do
          assert_equal 'Professional', @subscription.plan_name
        end
      end
    end
  end

  describe '#file_upload_cap' do
    let (:subscription) { Subscription.new(account: accounts(:minimum)) }

    describe_with_arturo_enabled :email_increase_attachment_size_limit do
      describe "without feature boost" do
        SubscriptionPlanType.fields.each do |plan_type|
          describe "for an account with plan_type #{plan_type}" do
            before do
              subscription.plan_type = plan_type
            end

            it "has a file_upload_cap of 50MB" do
              assert_equal 50, subscription.file_upload_cap
            end
          end
        end
      end

      describe "with feature boost" do
        before do
          subscription.account.create_feature_boost(boost_level: SubscriptionPlanType.ExtraLarge, valid_until: 2.days.from_now)
        end

        SubscriptionPlanType.fields.each do |plan_type|
          describe "for an account with plan_type #{plan_type}" do
            before do
              subscription.plan_type = plan_type
            end

            it "the file_upload_cap is still 50MB" do
              assert_equal 50, subscription.file_upload_cap
            end
          end
        end
      end
    end

    describe_with_arturo_disabled :email_increase_attachment_size_limit do
      before do
        @subscription = Subscription.new(plan_type: 2)
        @subscription.account = accounts(:minimum)
      end

      describe 'when account does not have feature boost' do
        it "is the plan type's file_upload_cap" do
          assert_equal 7, @subscription.file_upload_cap
        end
      end

      describe 'when account has feature boost' do
        before do
          @subscription.account.create_feature_boost(boost_level: 4, valid_until: 2.days.from_now)
        end

        it "is the boosted plan type's file_upload_cap" do
          assert_equal 20, @subscription.file_upload_cap
        end
      end
    end
  end

  describe '#pricing_model_revision' do
    describe 'on trial subscription' do
      before do
        @trial = subscriptions(:trial)
      end

      it 'masks the pricing_model_revision and returns PATAGONIA_PRICING_MODEL_REVISION' do
        assert_equal ZBC::Zendesk::PricingModelRevision::PATAGONIA, @trial.pricing_model_revision
      end
    end

    describe 'on paid subscription' do
      before do
        @subscription = subscriptions(:minimum)
      end

      it 'returns the pricing_model_revision from the database' do
        assert_equal ZBC::Zendesk::PricingModelRevision::NEW_FORUMS, @subscription.pricing_model_revision
      end
    end
  end

  describe 'set catalog features' do
    let(:subscription) { subscriptions(:minimum) }
    let(:features) do
      features = {}
      Subscription::DelegatedFeatures.delegated_feature_bits.each do |b|
        features[b] = true
      end
      features
    end
    let(:delegated_feature_bit_names) { Subscription::DelegatedFeatures.delegated_feature_bits.map(&:to_s).sort }
    it 'should set all delegated feature bits on subscription' do
      subscription.features.set(features)
      subscription.save
      assert_equal delegated_feature_bit_names, subscription.features.map(&:name).sort
    end
  end

  describe '#pricing_model_revision on new subscription' do
    it 'has pricing model version = ZBC::Zendesk::PricingModelRevision::PATAGONIA' do
      subscription = Subscription.new
      assert_equal ZBC::Zendesk::PricingModelRevision::PATAGONIA, subscription[:pricing_model_revision]
    end
  end

  describe '#preview!' do
    before do
      zopim_subscriptions(:unprovisioned_trial).delete
      @account = paying_account(_account = {}, plan_type: SubscriptionPlanType.Small)
      @subscription = @account.subscription
    end

    describe 'with an invalid configuration' do
      it 'raises an ActiveRecord::RecordInvalid' do
        assert_raise ActiveRecord::RecordInvalid do
          subscription_options = ZBC::Zuora::SubscriptionOptions.new(
            accounts(:minimum).id,
            max_agents: -1,
            plan_type: SubscriptionPlanType.Small,
            billing_cycle_type: BillingCycleType.Annually
          )
          @subscription.preview!(subscription_options)
        end
      end
    end

    describe 'with a valid configuration' do
      before do
        @opts = {
          max_agents: 999,
          plan_type: SubscriptionPlanType.Small,
          billing_cycle_type: BillingCycleType.Annually,
          promo_code: 'XYZ123'
        }
        subscription_options = ZBC::Zuora::SubscriptionOptions.new(
          accounts(:minimum).id, @opts
        )
        @preview = @subscription.preview!(subscription_options)
        assert @preview.frozen?
      end

      it 'has the billing changes' do
        assert_equal 999,      @preview.max_agents
        assert_equal 999,      @preview.base_agents
        assert_equal "XYZ123", @preview.promo_code
      end

      it "matches the source's unchanged attributes" do
        ignored  = ["id", "max_agents", "created_at", "updated_at", "base_agents"]
        expected = @subscription.attributes.except(*ignored)
        actual   = @preview.attributes.except(*ignored)
        assert_equal expected, actual
      end

      it 'does not allow itself to be saved' do
        assert_raise(RuntimeError) { @preview.save! }
      end

      describe 'voice product preview' do
        describe 'customer with voice trial' do
          describe 'when previewing with voice attributes in the subscription_options' do
            before do
              @opts[:voice_max_agents] = 12
              @opts[:voice_plan_type] = ZBC::Voice::PlanType::Advanced.plan_type
              subscription_options = ZBC::Zuora::SubscriptionOptions.new(
                accounts(:minimum).id, @opts
              )
              @preview = @subscription.preview!(subscription_options)
              assert @preview.frozen?
            end

            it 'has the voice billing changes' do
              assert_equal 12, @preview.voice_preview.max_agents
              assert_equal ZBC::Voice::PlanType::Advanced.plan_type, @preview.voice_preview.plan_type
            end
          end

          describe 'when previewing without voice attributes for keep trial situation' do
            before do
              @opts[:voice_max_agents] = nil
              @opts[:voice_plan_type] = nil
              subscription_options = ZBC::Zuora::SubscriptionOptions.new(
                accounts(:minimum).id, @opts
              )
              @preview = @subscription.preview!(subscription_options)
              assert @preview.frozen?
            end

            it 'returns an empty hash for voice_preview' do
              assert_nil @preview.voice_preview
            end
          end
        end

        describe 'customer with voice_subscription' do
          before do
            @voice_subscription = Voice::Subscription.new(
              account_id: accounts(:minimum).id,
              plan_type: ZBC::Voice::PlanType::Advanced.plan_type,
              max_agents: 2
            )
            @subscription.stubs(:voice_subscription).returns(@voice_subscription)
          end

          describe 'without voice_subscription options (no voice preview)' do
            before do
              @subscription_options = ZBC::Zuora::SubscriptionOptions.new(
                accounts(:minimum).id, @opts
              )
              @preview = @subscription.preview!(@subscription_options)
              assert @preview.frozen?
            end

            it 'should not show a voice subscription' do
              assert_nil @preview.voice_preview
            end
          end

          describe 'with voice_subscription options' do
            before do
              @opts[:voice_max_agents] = 13
              @opts[:voice_plan_type] = ZBC::Voice::PlanType::Basic.plan_type
              @subscription_options = ZBC::Zuora::SubscriptionOptions.new(
                accounts(:minimum).id, @opts
              )
              @preview = @subscription.preview!(@subscription_options)
              assert @preview.frozen?
            end

            it 'shows the values from the subscription options' do
              assert_equal 13, @preview.voice_preview.max_agents
              assert_equal ZBC::Voice::PlanType::Basic.plan_type, @preview.voice_preview.plan_type
            end
          end
        end

        describe 'customer with legacy voice setup' do
          before do
            voice_account = {
              id: 432,
              plan_name: "legacy",
              humanized_plan_name: "Legacy Voice",
              plan_type: 3,
              billing_plan_type: nil,
              trial: false,
              boost: false,
              trial_expired: true,
              balance: 1,
              currency_symbol: "$",
              max_voice_agents: 5,
              voice_agents: 1
            }
            @subscription.stubs(:voice_account).returns(voice_account)
            @voice_subscription = nil
            @subscription.stubs(:voice_subscription).returns(@voice_subscription)
          end

          describe 'without voice_subscription options (no voice preview)' do
            before do
              @subscription_options = ZBC::Zuora::SubscriptionOptions.new(
                accounts(:minimum).id, @opts
              )
              @preview = @subscription.preview!(@subscription_options)
              assert @preview.frozen?
            end

            it 'should not show a voice subscription' do
              assert_nil @preview.voice_preview
            end
          end

          describe 'with voice_subscription options' do
            before do
              @opts[:voice_max_agents] = 13
              @opts[:voice_plan_type] = ZBC::Voice::PlanType::Basic.plan_type
              @subscription_options = ZBC::Zuora::SubscriptionOptions.new(
                accounts(:minimum).id, @opts
              )
              @preview = @subscription.preview!(@subscription_options)
              assert @preview.frozen?
            end

            it 'shows the values from the subscription options' do
              assert_equal 13, @preview.voice_preview.max_agents
              assert_equal ZBC::Voice::PlanType::Basic.plan_type, @preview.voice_preview.plan_type
            end
          end
        end
      end

      describe '#preview_answer_bot_subscription' do
        before do
          AnswerBot::Subscription.expects(:find_for_account).
            returns(answer_bot_trial_product)
        end

        describe 'customer with answer bot trial' do
          let(:answer_bot_trial_product) do
            stub(
              trial?:           true,
              trial_expires_at: 10.days.from_now
            )
          end

          describe 'when previewing with answer bot attributes in the subscription_options' do
            before do
              @opts[:answer_bot_max_resolutions] = 200
              @opts[:answer_bot_plan_type]       = AnswerBot::Subscription::DEFAULT_PLAN_TYPE

              subscription_options = ZBC::Zuora::SubscriptionOptions.new(
                accounts(:minimum).id, @opts
              )

              @preview = @subscription.preview!(subscription_options)
              assert @preview.frozen?
            end

            it 'has the answer_bot billing changes' do
              assert_equal 200, @preview.answer_bot_preview.max_resolutions
              assert_equal AnswerBot::Subscription::DEFAULT_PLAN_TYPE, @preview.answer_bot_preview.plan_type
            end
          end
        end
      end

      describe '#preview_outbound_subscription' do
        before do
          Outbound::Subscription.expects(:find_for_account).
            returns(outbound_trial_product)
        end

        describe 'customer with outbound trial' do
          let(:outbound_trial_product) do
            stub(
              trial?:           true,
              trial_expires_at: 10.days.from_now
            )
          end

          describe 'when previewing with outbound attributes in the subscription_options' do
            before do
              @opts[:outbound_monthly_messaged_users] = 200
              @opts[:outbound_plan_type]              = Outbound::Subscription::DEFAULT_PLAN_TYPE

              subscription_options = ZBC::Zuora::SubscriptionOptions.new(
                accounts(:minimum).id, @opts
              )

              @preview = @subscription.preview!(subscription_options)
              assert @preview.frozen?
            end

            it 'has the outbound billing changes' do
              assert_equal 200, @preview.outbound_preview.monthly_messaged_users
              assert_equal Outbound::Subscription::DEFAULT_PLAN_TYPE, @preview.outbound_preview.plan_type
            end
          end
        end
      end

      describe '#preview_explore_subscription' do
        describe 'customer with explore trial' do
          describe 'when previewing with explore attributes in the subscription_options' do
            let(:explore_plan_type) { ZBC::Explore::PlanType::Professional.plan_type }
            let(:explore_max_agents) { 4 }

            before do
              @opts[:explore_max_agents] = explore_max_agents
              @opts[:explore_plan_type]  = explore_plan_type

              subscription_options = ZBC::Zuora::SubscriptionOptions.new(
                accounts(:minimum).id, @opts
              )

              @preview = @subscription.preview!(subscription_options)
              assert @preview.frozen?
            end

            it 'has the explore billing changes' do
              assert_equal 4, @preview.explore_preview.max_agents
              assert_equal explore_plan_type, @preview.explore_preview.plan_type
            end

            it 'is not presented as a trial' do
              assert @preview.explore_preview.subscribed?
              refute @preview.explore_preview.trial?
              refute @preview.explore_preview.trial_expires_at
            end
          end
        end
      end

      describe 'without zopim subscription' do
        it 'does not have a zopim subscription' do
          assert_nil @preview.zopim_subscription
        end
      end

      describe 'zopim subscription' do
        describe 'without existing zopim subscription' do
          before do
            @opts[:zopim_max_agents] = 42
            @opts[:zopim_plan_type] = 'basic'
            subscription_options = ZBC::Zuora::SubscriptionOptions.new(
              accounts(:minimum).id, @opts
            )
            @preview = @subscription.preview!(subscription_options)
            assert @preview.frozen?
          end

          it 'has the zopim billing changes' do
            assert_equal 42,      @preview.zopim_subscription.max_agents
            assert_equal 'basic', @preview.zopim_subscription.plan_type
            assert_equal 'active', @preview.zopim_subscription.status
          end
        end

        describe 'existing zopim subscription' do
          before do
            @subscription_options = ZBC::Zuora::SubscriptionOptions.new(
              accounts(:minimum).id, @opts
            )
            @zopim_subscription = ZBC::Zopim::Subscription.new(
              account_id: accounts(:minimum).id,
              plan_type: 'advanced',
              max_agents: 2,
              billing_cycle_type: 1,
              status: ZBC::Zopim::SubscriptionStatus::Active
            )
            @subscription.stubs(:zopim_subscription).returns(@zopim_subscription)
          end

          describe 'active' do
            before do
              @zopim_subscription.stubs(:status=)
              @zopim_subscription.expects(:is_active?).returns(true)
              @preview = @subscription.preview!(@subscription_options)
              assert @preview.frozen?
            end

            it 'has the zopim billing changes' do
              assert_equal 2, @preview.zopim_subscription.max_agents
              assert_equal 'advanced', @preview.zopim_subscription.plan_type
              assert_equal 'active', @preview.zopim_subscription.status
            end
          end

          describe 'cancelled' do
            before do
              @zopim_subscription.stubs(:status=)
              @zopim_subscription.expects(:is_active?).returns(false)
              @preview = @subscription.preview!(@subscription_options)
              assert @preview.frozen?
            end

            it 'has the zopim billing changes' do
              assert_nil @preview.zopim_subscription
            end
          end
        end
      end
    end

    describe '#promo_code' do
      before do
        coupon             = stub(coupon_code: 'ZYX321')
        coupon_application = stub(coupon: coupon)
        Account.any_instance.stubs(active_legacy_coupon: coupon_application)
      end

      describe 'when preview options include a coupon code' do
        it 'references the coupon code supplied in preview' do
          subscription_options = ZBC::Zuora::SubscriptionOptions.new(
            accounts(:minimum).id,
            promo_code: 'XYZ123',
            max_agents: 99,
            plan_type: SubscriptionPlanType.Small,
            billing_cycle_type: BillingCycleType.Annually
          )
          preview = @subscription.preview!(subscription_options)
          assert_equal 'XYZ123', preview.promo_code
        end
      end

      describe 'when preview options does not include a coupon code' do
        it "does not reference the source's active coupon code" do
          subscription_options = ZBC::Zuora::SubscriptionOptions.new(
            accounts(:minimum).id,
            max_agents: 99,
            plan_type: SubscriptionPlanType.Small,
            billing_cycle_type: BillingCycleType.Annually
          )
          preview = @subscription.preview!(subscription_options)
          assert_nil preview.promo_code
        end
      end
    end
  end

  describe '#answer_bot_subscription' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { account.subscription }

    subject { subscription.answer_bot_subscription }

    it 'queries the accounts service' do
      AnswerBot::Subscription.expects(:find_for_account).with(account)
      assert_nil subject
    end
  end

  describe '#answer_bot_preview' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { account.subscription }

    subject { subscription.answer_bot_preview }

    describe 'when the anwer bot trial has not started' do
      before do
        AnswerBot::Subscription.
          expects(:find_for_account).
          with(account).
          returns(nil)
      end

      it 'returns nil' do
        assert_nil subject
      end
    end

    describe 'when there is an answer bot product' do
      let(:answer_bot_product) { stub }

      before do
        AnswerBot::Subscription.
          expects(:find_for_account).
          with(account).
          returns(answer_bot_product)
      end

      it 'returns the answer bot product' do
        assert_equal answer_bot_product, subject
      end
    end
  end

  describe '#outbound_subscription' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { account.subscription }

    subject { subscription.outbound_subscription }

    it 'queries the accounts service' do
      Outbound::Subscription.expects(:find_for_account).with(account)
      assert_nil subject
    end
  end

  describe '#outbound_preview' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { account.subscription }

    subject { subscription.outbound_preview }

    describe 'when the outbound trial has not started' do
      before do
        Outbound::Subscription.
          expects(:find_for_account).
          with(account).
          returns(nil)
      end

      it 'returns nil' do
        assert_nil subject
      end
    end

    describe 'when there is an outbound product' do
      let(:outbound_product) { stub }

      before do
        Outbound::Subscription.
          expects(:find_for_account).
          with(account).
          returns(outbound_product)
      end

      it 'returns the outbound product' do
        assert_equal outbound_product, subject
      end
    end
  end

  describe '#guide_preview' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { account.subscription }

    subject { subscription.guide_preview }

    describe 'when the @guide_preview ivar has been set' do
      let(:guide_preview) { stub }

      before do
        subscription.instance_variable_set(:@guide_preview, guide_preview)
      end

      it 'returns @guide_preview' do
        assert_equal guide_preview, subject
      end
    end

    describe 'when the @guide_preview ivar has not been set' do
      before do
        assert_nil subscription.instance_variable_get(:@guide_preview)
      end

      it 'calls Guide::Subscription::Previewer.for_account' do
        Guide::Subscription::Previewer.expects(:for_account).with(account)
        subject
      end
    end
  end

  describe '#explore_subscription' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { account.subscription }

    subject { subscription.explore_subscription }

    it 'queries the accounts service' do
      Explore::Subscription.expects(:find_for_account).with(account)
      assert_nil subject
    end
  end

  describe '#explore_preview' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { account.subscription }

    subject { subscription.explore_preview }

    before do
      Explore::Subscription.
        expects(:find_for_account).
        with(account).
        returns(explore_subscription)
    end

    describe 'when the explore trial has not started' do
      let(:explore_subscription) { nil }

      it 'returns nil' do
        assert_nil subject
      end
    end

    describe 'when there is an explore product' do
      let(:explore_subscription) { stub }

      it 'returns the explore product' do
        assert_equal explore_subscription, subject
      end
    end
  end

  describe '#voice_preview' do
    let(:account)      { accounts(:minimum) }
    let(:subscription) { account.subscription }

    subject { subscription.voice_preview }

    describe 'when a voice subscription exists' do
      let(:voice_subscription) do
        Voice::Subscription.new(
          account:    account,
          plan_type:  ZBC::Voice::PlanType::Advanced.plan_type,
          max_agents: 2
        )
      end

      before do
        account.stubs(:voice_subscription).returns(voice_subscription)
      end

      it 'correctly returns the voice subscription attributes' do
        assert_equal ZBC::Voice::PlanType::Advanced.plan_type, subject.plan_type
        assert_equal 2, subject.max_agents
        refute subject.trial
      end
    end

    describe 'when a voice subscription does not exist' do
      before do
        assert_nil account.voice_subscription
        account.stubs(voice_enabled?: voice_enabled)
      end

      describe 'when the account is not voice_enabled' do
        let(:voice_enabled) { false }

        it 'returns nil' do
          assert_nil subject
        end
      end

      describe 'when the account is voice_enabled' do
        let(:voice_enabled)              { true }
        let(:voice_subscription_details) { stub(body: body) }
        let(:body) do
          {
            'voice_account' => {
              account_id:        account.id,
              billing_plan_type: billing_plan_type,
              trial:             trial,
              trial_expired:     trial_expired
            }
          }
        end

        before do
          Zendesk::Voice::InternalApiClient.any_instance.
            stubs(:voice_subscription_details).returns(voice_subscription_details)
        end

        describe 'for an active voice product trial' do
          let(:billing_plan_type) { ZBC::Voice::PlanType::Advanced.plan_type }
          let(:trial)             { true }
          let(:trial_expired)     { false }

          it 'returns a correctly returns the voice subscription attributes' do
            assert_equal ZBC::Voice::PlanType::Advanced.plan_type, subject.plan_type
            assert_equal subscription.max_agents, subject.max_agents
            assert subject.trial
          end
        end

        describe 'for an expired trial of a voice product' do
          let(:billing_plan_type) { ZBC::Voice::PlanType::Advanced.plan_type }
          let(:trial)             { true }
          let(:trial_expired)     { true }

          it 'correctly returns the voice subscription attributes' do
            assert_nil subject
          end
        end

        describe 'for a legacy voice product' do
          let(:billing_plan_type) { nil }
          let(:trial)             { false }
          let(:trial_expired)     { false }

          it 'correctly returns the voice subscription attributes' do
            assert_nil subject.plan_type
            assert_equal subscription.max_agents, subject.max_agents
            refute subject.trial
            assert subject.legacy
          end
        end
      end
    end
  end

  describe '#promo_code' do
    before do
      @subscription = paying_account.subscription
    end

    describe 'when managed by Zuora' do
      before do
        Subscription.any_instance.stubs(zuora_managed?: true)
      end

      it 'references the active coupon code if exists' do
        coupon = stub(coupon_code: 'XYZ123')
        Account.any_instance.expects(:active_zuora_coupon).returns(coupon)
        assert_equal 'XYZ123', @subscription.promo_code
      end

      it 'returns nil if there is no active coupon code' do
        Account.any_instance.stubs(active_zuora_coupon: nil)
        refute @subscription.promo_code
      end
    end

    describe 'when managed by Zendesk' do
      before do
        Subscription.any_instance.stubs(zuora_managed?: false)
      end

      it 'references the active coupon code if exists' do
        coupon             = stub(coupon_code: 'XYZ123')
        coupon_application = stub(coupon: coupon)
        Account.any_instance.expects(:active_legacy_coupon).returns(coupon_application)
        assert_equal 'XYZ123', @subscription.promo_code
      end

      it 'returns nil if there is no active coupon code' do
        Account.any_instance.stubs(active_legacy_coupon: nil)
        refute @subscription.promo_code
      end
    end
  end

  describe '#sales_model_value' do
    let(:support_product) { stub(plan_settings: {}) }

    before do
      Zendesk::Accounts::Client.any_instance.stubs(:product).returns(support_product)

      @zuora_subscription = build_zuora_subscription
      @zuora_subscription.sales_model = ZBC::States::SalesModel::ASSISTED
      @zuora_subscription.save!
      @subscription = @zuora_subscription.subscription
    end

    it 'returns SELF_SERVICE if zuora_managed? is false' do
      Subscription.any_instance.stubs(:zuora_managed?).returns(false)
      assert_equal ZBC::States::SalesModel::SELF_SERVICE, @subscription.sales_model_value
    end

    it 'returns the value of sales_model on the zuora_subscription' do
      Subscription.any_instance.stubs(:zuora_managed?).returns(true)
      assert_equal ZBC::States::SalesModel::ASSISTED, @subscription.sales_model_value
    end
  end

  describe '#self_service?' do
    let(:subscription) { subscriptions(:minimum) }

    before do
      Subscription.any_instance.stubs(:sales_model_value).returns(sales_model)
    end

    describe 'When sales_model is "Self-service"' do
      let(:sales_model) { ZBC::States::SalesModel::SELF_SERVICE }
      it                { assert subscription.self_service? }
    end

    describe 'When sales_model is "Assisted"' do
      let(:sales_model) { ZBC::States::SalesModel::ASSISTED }
      it                { refute subscription.self_service? }
    end
  end

  describe '#is_elite' do
    before do
      @zuora_subscription = build_zuora_subscription
      @zuora_subscription.save!
      @subscription = @zuora_subscription.subscription
    end

    it 'returns true if is_elite is true' do
      ZBC::Zuora::Subscription.any_instance.stubs(:is_elite?).returns(true)
      assert @subscription.is_elite?
    end

    it 'returns false if is_elite is false' do
      ZBC::Zuora::Subscription.any_instance.stubs(:is_elite?).returns(false)
      refute @subscription.is_elite?
    end
  end

  describe '#is_churned?' do
    before do
      @subscription = accounts(:minimum).subscription
    end

    describe 'when zuora_managed and not active' do
      before do
        @subscription.stubs(:zuora_managed?).returns(true)
        @subscription.account.update_attribute(:is_active, false)
      end

      it 'returns true' do
        assert @subscription.is_churned?
      end
    end

    describe 'when is not zuora managed' do
      before do
        @subscription.stubs(:zuora_managed?).returns(false)
      end

      it 'returns false' do
        @subscription.account.update_attribute(:is_active, false)
        refute @subscription.is_churned?

        @subscription.account.update_attribute(:is_active, true)
        refute @subscription.is_churned?
      end
    end

    describe 'when is still active' do
      before do
        @subscription.account.update_attribute(:is_active, true)
      end

      it 'returns false' do
        @subscription.stubs(:zuora_managed?).returns(true)
        refute @subscription.is_churned?

        @subscription.stubs(:zuora_managed?).returns(false)
        refute @subscription.is_churned?
      end
    end
  end

  describe '#with_compulsory_spam_prevention?' do
    before do
      @subscription = Subscription.new
    end

    describe 'when plan_type is starter' do
      before { @subscription.plan_type = SubscriptionPlanType.Small }
      it 'returns true' do
        assert @subscription.with_compulsory_spam_prevention?
      end
    end

    describe 'when plan_type is not starter' do
      before { @subscription.plan_type = SubscriptionPlanType.Medium }
      it 'returns false' do
        refute @subscription.with_compulsory_spam_prevention?
      end
    end
  end

  describe '#apply_billing_and_coupon_changes' do
    before do
      @subscription = FactoryBot.build :subscription, is_trial: false
      @subscription.account = accounts(:minimum)

      @context = {
        plan_type: SubscriptionPlanType.Medium,
        max_agents: 99,
        billing_cycle_type: BillingCycleType.Monthly,
        pricing_model_revision: ZBC::Zendesk::PricingModelRevision::APRIL_2010
      }
    end

    it 'apply symbol versions of all Subscription::BILLING_INPUTS attributes' do
      @subscription.apply_billing_and_coupon_changes @context
      assert @subscription.plan_type_changed?
      assert @subscription.max_agents_changed?
      assert @subscription.billing_cycle_type_changed?
      assert @subscription.pricing_model_revision_changed?

      assert_equal SubscriptionPlanType.Medium, @subscription.plan_type
      assert_equal 99, @subscription.max_agents
      assert_equal BillingCycleType.Monthly, @subscription.billing_cycle_type
      assert_equal ZBC::Zendesk::PricingModelRevision::APRIL_2010, @subscription.pricing_model_revision
    end

    it 'apply string versions of all Subscription::BILLING_INPUTS attributes' do
      @strings_context = {}
      @context.each_pair do |k, v|
        @strings_context.merge!(k.to_s => v)
      end

      @subscription.apply_billing_and_coupon_changes @strings_context
      assert @subscription.plan_type_changed?
      assert @subscription.max_agents_changed?
      assert @subscription.billing_cycle_type_changed?
      assert @subscription.pricing_model_revision_changed?

      assert_equal SubscriptionPlanType.Medium, @subscription.plan_type
      assert_equal 99, @subscription.max_agents
      assert_equal BillingCycleType.Monthly, @subscription.billing_cycle_type
      assert_equal ZBC::Zendesk::PricingModelRevision::APRIL_2010, @subscription.pricing_model_revision
    end

    it 'quietly ignore attributes not in Subscription::BILLING_INPUTS' do
      @subscription.apply_billing_and_coupon_changes @context.merge(not_there_at_all: 'completely_fake')

      assert @subscription.plan_type_changed?
      assert @subscription.max_agents_changed?
      assert @subscription.billing_cycle_type_changed?
      assert @subscription.pricing_model_revision_changed?

      assert_equal SubscriptionPlanType.Medium, @subscription.plan_type
      assert_equal 99, @subscription.max_agents
      assert_equal BillingCycleType.Monthly, @subscription.billing_cycle_type
      assert_equal ZBC::Zendesk::PricingModelRevision::APRIL_2010, @subscription.pricing_model_revision
    end
  end

  # These tests should be refactored into the right pricing model test suite.
  describe 'Association Helper Methods' do
    before do
      @subscription = Subscription.new
    end

    describe 'Status' do
      before do
        @account = Account.new(is_active: true)
        @account.created_at = 31.days.ago
        @payments = [Payment.new]
        @payments.stubs(:any_paid?).returns(false)
        @payments.stubs(:count).returns(1)
        @subscription.stubs(:payments).returns(@payments)
        @subscription.stubs(:account).returns(@account)
      end

      it "has a status of 'inactive'" do
        @account.is_active = false
        assert_equal 'canceled', @subscription.account_status
      end

      it "has a status of active for zuora-managed account" do
        @subscription.stubs(:zuora_managed?).returns(true)
        assert_equal 'active', @subscription.account_status
      end

      it "has a status of 'paying'" do
        @payments = []
        @payments.stubs(:any_paid?).returns(true)
        @subscription.stubs(:payments).returns(@payments)
        assert_equal 'trial', @subscription.account_type
      end

      it "has a status of 'trial'" do
        @payments.first.status = PaymentType.PENDING
        @account.created_at = 29.days.ago
        assert_equal 'trial', @subscription.account_type
      end

      it "has a status of 'trial' if new record" do
        @payments.first.status = PaymentType.PENDING
        assert @subscription.new_record?
        assert_equal 'trial', @subscription.account_type
      end
    end

    describe '#account_type' do
      before do
        @account = Account.new(is_active: true)
        @account.created_at = 31.days.ago
        @payments = [Payment.new]
        @payments.stubs(:count).returns(1)
        @payments.stubs(:any_paid?).returns(false)
        @subscription.stubs(:payments).returns(@payments)
        @subscription.stubs(:account).returns(@account)
        @subscription.stubs(:new_record?).returns(false)
        @subscription.stubs(:is_trial?).returns(false)
      end

      it 'returns "trial" if in trial' do
        @subscription.stubs(:is_trial?).returns(true)
        assert_equal 'trial', @subscription.account_type
      end

      it 'returns "churned" if is churned' do
        @subscription.stubs(:is_churned?).returns(true)
        assert_equal 'churned', @subscription.account_type
      end

      it 'returns "customer" if there is a paid payment and the account is active' do
        @payments.stubs(:any_paid?).returns(true)
        assert_equal 'customer', @subscription.account_type
      end

      it 'returns "customer" if there is an invoiced payment, the account is in invoicing and the account is active' do
        @payments.stubs(:any_paid?).returns(true)
        @subscription.stubs(:invoicing?).returns(true)
        assert_equal 'customer', @subscription.account_type
      end

      it 'returns "customer" if the account is sponsored, active, and not set to pay be credit card' do
        @subscription.stubs(:is_sponsored?).returns(true)
        @subscription.stubs(:payment_method_type).returns(PaymentMethodType.MANUAL)
        assert_equal 'customer', @subscription.account_type
      end

      it 'returns "customer" if the accounts is active and managed by zuora' do
        @subscription.stubs(:zuora_managed?).returns(true)
        assert_equal 'customer', @subscription.account_type
      end

      it 'returns "sandbox" if the account is a sandbox' do
        @account.stubs(:is_sandbox?).returns(true)
        assert_equal 'sandbox', @subscription.account_type
      end

      it 'returns "sponsored" if the account is sponsored, paying by credit card, and active' do
        @subscription.stubs(:is_sponsored?).returns(true)
        @subscription.stubs(:payment_method_type).returns(PaymentMethodType.CREDIT_CARD)
        assert_equal 'sponsored', @subscription.account_type
      end

      it 'returns "sponsored" if the account is sponsored and inactive' do
        @subscription.stubs(:is_sponsored?).returns(true)
        @account.stubs(:is_active).returns(false)
        assert_equal 'sponsored', @subscription.account_type
      end

      it 'returns "trial" if the account is an expired trial' do
        @account = accounts(:trial)
        @subscription = @account.subscription
        @subscription.trial_expires_on = 10.days.ago
        @subscription.is_trial = false
        @subscription.account.is_serviceable = false
        assert_equal 'trial', @subscription.account_type
      end
    end
  end

  describe "#propagate_trial_extension" do
    before do
      Account.any_instance.stubs(:read_feature_bits_from_db).returns(false)
    end

    describe "On an expired trial" do
      before do
        @account = accounts(:trial)
        @account.update_attributes!(is_serviceable: false)
        @account.subscription.update_attribute(:trial_expires_on, 30.days.ago)
      end

      it "makes the account serviceable if the trial is extended" do
        @account.subscription.update_attributes!(trial_expires_on: 1.day.from_now)
        assert(@account.reload.is_serviceable)
        assert(@account.subscription.is_trial)
      end

      describe "when account does not have `use_feature_framework_persistence` setting" do
        before do
          @account.settings.where(name: 'use_feature_framework_persistence').destroy_all
          @account.subscription.update_attributes!(trial_expires_on: 1.day.from_now)
        end

        it "enables use feature framework" do
          assert @account.settings.where(name: 'use_feature_framework_persistence', value: true).present?
        end
      end
    end
  end

  describe "#days_left_in_trial" do
    before { @subscription = Subscription.new }

    describe "For an expired trial" do
      before do
        @subscription.stubs(:expiration_date).returns(2.days.ago)
      end

      it "returns 0  " do
        assert_equal 0, @subscription.days_left_in_trial
      end
    end

    describe "For trial expiry date in future " do
      before do
        @subscription.stubs(:expiration_date).returns(2.days.from_now.to_date)
      end

      it "returns correct days (floored)" do
        assert_equal 1, @subscription.days_left_in_trial
      end
    end
  end

  describe "#billing_backend" do
    before do
      @subscription = subscriptions(:minimum)
    end

    describe "For a non zuora managed account" do
      it "returns 'internal'" do
        assert_equal 'internal', @subscription.billing_backend
      end
    end

    describe " For a zuora managed account" do
      before do
        @subscription.stubs(:zuora_managed?).returns(true)
      end

      it "returns 'zuora'" do
        assert_equal 'zuora', @subscription.billing_backend
      end
    end
  end

  describe "#propagate_subscription_settings" do
    describe "When plan_type changes" do
      before do
        @subscription = paying_account.subscription
        @subscription.account.settings.customer_satisfaction = 1
        @subscription.account.settings.save!
        Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
        assert @subscription.plan_type != SubscriptionPlanType.Small
      end

      describe "to Small" do
        it "sets all forum types to article" do
          forum = FactoryBot.create(:forum, display_type_id: ForumDisplayType::QUESTIONS.id, account: @subscription.account)
          assert_equal ForumDisplayType::QUESTIONS.id, forum.display_type_id
          @subscription.update_attributes! plan_type: SubscriptionPlanType.Small
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
          assert_equal ForumDisplayType::ARTICLES.id, forum.reload.display_type_id
        end

        it "destroys categories" do
          category = FactoryBot.create(:category, account: @subscription.account)

          @subscription.update_attributes! plan_type: SubscriptionPlanType.Small
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!

          assert Category.find_by_id(category.id).nil?
        end

        it "removes host mapping" do
          Account.connection.execute("update accounts set host_mapping='foo.bar' where id = #{@subscription.account_id}")

          a = @subscription.account
          a.clear_kasket_indices

          assert_equal 'foo.bar', @subscription.account.reload.host_mapping
          @subscription.update_attributes! plan_type: SubscriptionPlanType.Small
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
          assert_nil @subscription.account.reload.host_mapping
        end

        it "adjusts time zones" do
          AlignUserTimeZonesJob.expects(:enqueue).with(@subscription.account.id)
          @subscription.update_attributes! plan_type: SubscriptionPlanType.Small
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
        end

        it "converts group views to global views" do
          group = @subscription.account.groups.first
          FactoryBot.create(:view, account: @subscription.account, owner: group)

          assert group.views.count(:all) > 0
          @subscription.update_attributes!(plan_type: SubscriptionPlanType.Small)
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
          assert_equal 0, group.views.count(:all)
        end

        it "disables customer satisfaction ratings" do
          assert_equal "1", @subscription.account.settings.customer_satisfaction
          @subscription.update_attributes! plan_type: SubscriptionPlanType.Small
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
          @subscription.account.settings.reload
          assert_equal "0", @subscription.account.settings.customer_satisfaction
        end
      end

      describe "to Medium" do
        it "adjusts time zones" do
          AlignUserTimeZonesJob.expects(:enqueue).with(@subscription.account.id)
          @subscription.update_attributes! plan_type: SubscriptionPlanType.Medium
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
        end

        it "converts group views to global views" do
          group = @subscription.account.groups.first
          FactoryBot.create(:view, account: @subscription.account, owner: group)

          assert group.views.count(:all) > 0
          @subscription.update_attributes!(plan_type: SubscriptionPlanType.Medium)
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
          assert_equal 0, group.views.count(:all)
        end
      end

      describe "on an SPP account" do
        before do
          @subscription.account.stubs(:spp?).returns(true)
          @subscription.account.settings.stubs(:suite_subscription?).returns(false)
          @subscription.update_attributes!(base_agents: 5)
          @subscription.account.settings.save!
        end

        describe_with_arturo_enabled :ocp_unified_suite_downgrade do
          it "calls downgrade_agents_for_suite_unified!" do
            @subscription.expects(:downgrade_agents_for_suite_unified!)
            @subscription.save
          end
        end

        describe_with_arturo_disabled :ocp_unified_suite_downgrade do
          it "calls downgrade_agents_for_suite_multiproduct!" do
            @subscription.expects(:downgrade_agents_for_suite_multiproduct!)
            @subscription.save
          end
        end
      end
    end

    describe "when agent downgrade required due to decrease in max agents" do
      describe_with_arturo_disabled :ocp_unified_suite_downgrade do
        def do_downgrade!
          account.subscription.update_attributes(base_agents: 5) # downgrade 2 of 7 agents
        end

        let(:contributor_permission_set) { account.permission_sets.find_by_role_type(PermissionSet::Type::CONTRIBUTOR) }
        let(:account) { accounts(:multiproduct) }

        before do
          account.agents.each_with_index do |agent, index|
            agent.update_columns(created_at: index.days.ago)
          end
        end

        describe 'and account is non-multiproduct' do
          before do
            account.multiproduct = false
          end

          describe "with a Suite" do
            let(:downgradeable_agents) { account.agents.where.not(id: account.owner.id).order(created_at: :desc) }

            before do
              account.settings.suite_subscription = true
              account.settings.save!
            end

            describe "when all agents are contributors" do
              before do
                account.agents.each do |agent|
                  agent.permission_set = contributor_permission_set
                  agent.save!
                end
              end

              # note: for Suite, we always need to downgrade both Support and Chat
              # since their Support role may need to change even if not currently
              # billable (e.g. Contributor -> End User)
              it "downgrades latest agents on Support AND Chat" do
                Zendesk::Users::AgentDowngrader.expects(:perform).never.
                  with(agent: downgradeable_agents.first, products: [:support, :chat])
                Zendesk::Users::AgentDowngrader.expects(:perform).never.
                  with(agent: downgradeable_agents.second, products: [:support, :chat])
                do_downgrade!
              end
            end
          end
        end

        describe "and account is multiproduct" do
          describe "with a Suite" do
            let(:downgradeable_agents) { account.agents.where.not(id: account.owner.id).order(created_at: :desc) }

            before do
              account.settings.suite_subscription = true
              account.settings.save!
            end

            describe "when all agents are Chat-only" do
              before do
                account.agents.each do |agent|
                  agent.permission_set = contributor_permission_set
                  agent.settings.chat_entitlement = { chat: 'agent' }
                  agent.save!
                end
              end

              # note: for Suite, we always need to downgrade both Support and Chat
              # since their Support role may need to change even if not currently
              # billable (e.g. Contributor -> End User)
              it "downgrades latest agents on Support AND Chat" do
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.first, products: [:support, :chat])
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.second, products: [:support, :chat])
                do_downgrade!
              end
            end

            describe "when all agents are Support-only" do
              before do
                account.agents.each do |agent|
                  agent.permission_set = nil
                  agent.save!
                end
              end

              # note: for Suite, we always need to downgrade both Support and Chat
              # since their Support role may need to change even if not currently
              # billable (e.g. Contributor -> End User)
              it "downgrades latest agents on Support AND Chat" do
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.first, products: [:support, :chat])
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.second, products: [:support, :chat])
                do_downgrade!
              end
            end

            describe "when all agents have both Support and Chat enabled" do
              before do
                account.agents.each do |agent|
                  agent.permission_set = nil # billable Support agent
                  agent.settings.chat_entitlement = { chat: 'agent' } # billable Chat agent
                  agent.save!
                end
              end

              it "downgrades latest agents on Support AND Chat" do
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.first, products: [:support, :chat])
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.second, products: [:support, :chat])
                do_downgrade!
              end
            end

            describe "when owner is most recent agent" do
              before do
                account.owner.update_columns(created_at: 0.days.ago)
              end

              it "does not attempt to downgrade the owner" do
                Zendesk::Users::AgentDowngrader.expects(:perform).never.
                  with(agent: account.owner, products: [:support])
                Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).never
                do_downgrade!
              end
            end
          end
        end
      end

      describe_with_arturo_enabled :ocp_unified_suite_downgrade do
        def do_downgrade!
          account.subscription.update_attributes(base_agents: 5) # downgrade 2 of 7 agents
        end

        let(:contributor_permission_set) { account.permission_sets.find_by_role_type(PermissionSet::Type::CONTRIBUTOR) }
        let(:account) { accounts(:multiproduct) }
        let(:staff_client) { stub }
        let(:chat_staff_list) { account.agents.map { |agent| { id: agent.id } } }

        before do
          account.agents.each_with_index do |agent, index|
            agent.update_columns(created_at: index.days.ago)
          end
          Zendesk::StaffClient.stubs(:new).returns(staff_client)
          staff_client.stubs(:get_staff!).returns(chat_staff_list)
        end

        describe 'and account is non-multiproduct' do
          before do
            account.multiproduct = false
          end

          describe "with a Suite" do
            let(:downgradeable_agents) { account.agents.where.not(id: account.owner.id).order(created_at: :desc) }

            before do
              account.settings.suite_subscription = true
              account.settings.save!
            end

            describe "when all agents are contributors" do
              before do
                account.agents.each do |agent|
                  agent.permission_set = contributor_permission_set
                  agent.save!
                end
              end

              describe 'with chat entitlements' do
                it "downgrades latest agents on Support AND Chat" do
                  Zendesk::Users::AgentDowngrader.expects(:perform).with(agent: downgradeable_agents.first, products: [:support, :chat])
                  Zendesk::Users::AgentDowngrader.expects(:perform).with(agent: downgradeable_agents.second, products: [:support, :chat])
                  do_downgrade!
                end
              end

              describe 'without chat entitlements' do
                let(:chat_staff_list) { [] }

                it "does not downgrade latest agents on Support AND Chat" do
                  Zendesk::Users::AgentDowngrader.expects(:perform).never.
                    with(agent: downgradeable_agents.first, products: [:support, :chat])
                  Zendesk::Users::AgentDowngrader.expects(:perform).never.
                    with(agent: downgradeable_agents.second, products: [:support, :chat])
                  do_downgrade!
                end
              end
            end
          end
        end

        describe "and account is multiproduct" do
          describe "with a Suite" do
            let(:downgradeable_agents) { account.agents.where.not(id: account.owner.id).order(created_at: :desc) }

            before do
              account.settings.suite_subscription = true
              account.settings.save!
            end

            describe "when all agents are Chat-only" do
              before do
                account.agents.each do |agent|
                  agent.permission_set = contributor_permission_set
                  agent.settings.chat_entitlement = { chat: 'agent' }
                  agent.save!
                end
              end

              # note: for Suite, we always need to downgrade both Support and Chat
              # since their Support role may need to change even if not currently
              # billable (e.g. Contributor -> End User)
              it "downgrades latest agents on Support AND Chat" do
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.first, products: [:support, :chat])
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.second, products: [:support, :chat])
                do_downgrade!
              end
            end

            describe "when all agents are Support-only" do
              before do
                account.agents.each do |agent|
                  agent.permission_set = nil
                  agent.save!
                end
              end

              # note: for Suite, we always need to downgrade both Support and Chat
              # since their Support role may need to change even if not currently
              # billable (e.g. Contributor -> End User)
              it "downgrades latest agents on Support AND Chat" do
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.first, products: [:support, :chat])
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.second, products: [:support, :chat])
                do_downgrade!
              end
            end

            describe "when all agents have both Support and Chat enabled" do
              before do
                account.agents.each do |agent|
                  agent.permission_set = nil # billable Support agent
                  agent.settings.chat_entitlement = { chat: 'agent' } # billable Chat agent
                  agent.save!
                end
              end

              it "downgrades latest agents on Support AND Chat" do
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.first, products: [:support, :chat])
                Zendesk::Users::AgentDowngrader.expects(:perform).once.
                  with(agent: downgradeable_agents.second, products: [:support, :chat])
                do_downgrade!
              end
            end
          end
        end
      end
    end

    describe "On a new subscription" do
      before do
        @account = accounts(:minimum)
        @subscription = Subscription.new(account: @account)
      end

      it "does not call propagate_subscription_settings" do
        @subscription.expects(:propagate_subscription_settings).never
        @subscription.save
      end
    end
  end

  describe "Last day of trial" do
    before do
      @subscription = Subscription.new
      @subscription.stubs(:account).returns(Account.new)
    end

    describe "for a regular account" do
      before do
        @subscription.trial_expires_on = 20.days.from_now
      end

      it "reports that the last day of trial is 20 days from now" do
        assert_equal 20.days.from_now.to_date, @subscription.last_day_of_trial.to_date
      end
    end

    describe "for a sponsored account" do
      before do
        @subscription.stubs(:is_sponsored?).returns(true)
        @subscription.manual_discount_expires_on = 100.days.from_now.to_date
      end

      it "reports that the last day of the trial is 100 days from now" do
        assert_equal 100.days.from_now.to_date, @subscription.last_day_of_trial.to_date
      end
    end

    describe "a non trial account" do
      before do
        @subscription.stubs(:is_trial?).returns(false)
      end

      it "returns nil" do
        assert_nil @subscription.last_day_of_trial
      end
    end
  end

  describe "Changing a subscription from a Trial" do
    before do
      @account = accounts(:trial)
      @subscription = @account.subscription
      assert(@subscription.is_trial?)
      Subscription.any_instance.stubs(:has_permission_sets?).returns(false)
    end

    describe "while trying to swap ownership between an admin and a owner during trial" do
      before do
        @admin = users(:minimum_admin)
        @account.owner = @admin
      end

      it "pass validation on subscription model even when credit card information is NOT provided" do
        assert @subscription.valid?
      end
    end

    describe "to a Large subscription plan with 5 max_agents a Annually Billing cycle" do
      before do
        # NOTE: Seven (6 agents + 1 owner) agents with access to Support
        @support_agents = FactoryBot.create_list(:agent, 6, account: @account, is_moderator: true)
        @support_agents[1].update_attribute(:created_at, @support_agents[0].created_at + 1.second)
        @support_agents[2].update_attribute(:created_at, @support_agents[0].created_at + 2.second)
        @support_agents[3].update_attribute(:created_at, @support_agents[0].created_at + 3.second)
        @support_agents[4].update_attribute(:created_at, @support_agents[0].created_at + 4.second)
        @support_agents[5].update_attribute(:created_at, @support_agents[0].created_at + 5.second)

        # NOTE: Three 'Chat-only' agents
        @chat_agents = FactoryBot.create_list(:chat_only_agent, 3, account: @account, is_moderator: true)
        @chat_agents[1].update_attribute(:created_at, @chat_agents[0].created_at + 1.second)
        @chat_agents[2].update_attribute(:created_at, @chat_agents[0].created_at + 2.second)

        @light_agents = FactoryBot.create_list(:light_agent, 2, account: @account)
        @light_agents[1].update_attribute(:created_at, @light_agents[0].created_at + 1.second)

        @account.settings.suite_subscription = suite_subscription
        @account.settings.save!

        manually_set_credit_card(@subscription, FactoryBot.attributes_for(:credit_card))

        refute_difference '@account.users.count(:all)' do
          @subscription.update_attributes!(
            plan_type:          SubscriptionPlanType.Large,
            base_agents:        5,
            billing_cycle_type: BillingCycleType.Annually
          )
          @payment         = @account.subscription.payments.pending.first
          @previous_amount = @payment.amount
        end
      end

      describe 'when the subscription is part of a Suite' do
        let(:suite_subscription) { true }

        it "updates the pending payment" do
          assert_equal SubscriptionPlanType.Large, @payment.plan_type
          assert_equal 5,                          @payment.max_agents
          assert_equal BillingCycleType.Annually,  @payment.billing_cycle_type
        end

        it "has no one time payments" do
          assert_equal 0, @subscription.payments.pending.one_time.count(:all)
        end

        it "limits number of agents to 7" do
          assert_equal 7, @account.agents.count(:all)
        end

        it "downgrades 3 Chat-only agents & 2 Support-only agents to end users" do
          assert_equal 5, @account.end_users.count(:all)

          assert_equal Role::END_USER.id, @chat_agents[0].reload.roles
          assert_equal Role::END_USER.id, @chat_agents[1].reload.roles
          assert_equal Role::END_USER.id, @chat_agents[2].reload.roles

          assert_equal Role::AGENT.id,    @support_agents[0].reload.roles
          assert_equal Role::AGENT.id,    @support_agents[1].reload.roles
          assert_equal Role::AGENT.id,    @support_agents[2].reload.roles
          assert_equal Role::AGENT.id,    @support_agents[3].reload.roles
          assert_equal Role::END_USER.id, @support_agents[4].reload.roles
          assert_equal Role::END_USER.id, @support_agents[5].reload.roles

          assert_equal Role::AGENT.id,    @light_agents[0].reload.roles
          assert_equal Role::AGENT.id,    @light_agents[1].reload.roles
        end

        it "keeps the current permissions on agents" do
          light_agent_ids = @light_agents.map(&:id)
          @account.users.where(roles: Role::AGENT.id).where('id NOT IN (?)', light_agent_ids).each do |agent|
            assert_equal RoleRestrictionType.NONE, agent.restriction_id
            assert       agent.is_moderator
            refute       agent.is_private_comments_only
          end
        end

        it "is not a trial" do
          refute @subscription.is_trial?
        end
      end

      describe 'when the subscription is not part of a Suite' do
        let(:suite_subscription) { false }

        it "updates the pending payment" do
          assert_equal SubscriptionPlanType.Large, @payment.plan_type
          assert_equal 5,                          @payment.max_agents
          assert_equal BillingCycleType.Annually,  @payment.billing_cycle_type
        end

        it "has no one time payments" do
          assert_equal 0, @subscription.payments.pending.one_time.count(:all)
        end

        it "limits number of Support agents to 5" do
          support_only_agent_count = @account.non_chat_only_non_light_agents.count(:all)
          assert_equal 5, support_only_agent_count
        end

        it "downgrades 2 Support agents to end users" do
          assert_equal 2, @account.end_users.count(:all)
        end

        it "does not downgrade any Chat-only agents to end users" do
          assert_equal 3, @account.chat_only_agents.count(:all)

          assert_equal Role::AGENT.id, @chat_agents[0].roles
          assert_equal Role::AGENT.id, @chat_agents[1].roles
          assert_equal Role::AGENT.id, @chat_agents[2].roles
        end

        it "keeps the current permissions on agents" do
          light_agent_ids = @light_agents.map(&:id)
          @account.users.where(roles: Role::AGENT.id).where('id NOT IN (?)', light_agent_ids).each do |agent|
            assert_equal RoleRestrictionType.NONE, agent.restriction_id
            assert       agent.is_moderator
            refute       agent.is_private_comments_only
          end
        end

        it "is not a trial" do
          refute @subscription.is_trial?
        end
      end
    end
  end

  describe "Changing a subscription from ExtraLarge" do
    before do
      ZBC::Zopim::Subscription.any_instance.stubs(is_serviceable?: true)

      @account = accounts(:trial)
      @account.stubs(has_permission_sets?: true)
      @subscription = @account.subscription

      @subscription.update_attributes!(
        plan_type: SubscriptionPlanType.ExtraLarge,
        base_agents: 10,
        billing_cycle_type: BillingCycleType.Annually
      )

      @agent1 = FactoryBot.create(
        :user,
        account: @account, roles: Role::AGENT.id,
        permission_set: build_valid_permission_set(@account, "Role1")
      )

      @agent2 = FactoryBot.create(
        :user,
        account: @account, roles: Role::AGENT.id,
        permission_set: build_valid_permission_set(
          @account,
          "Role2",
          ticket_access: "within-organization",
          forum_access: "edit-topics",
          comment_access: "public"
        )
      )

      @agent3 = FactoryBot.create(
        :user,
        account: @account, roles: Role::AGENT.id,
        permission_set: build_valid_permission_set(
          @account,
          "Role3",
          ticket_access: "assigned-only",
          forum_access: "readonly",
          comment_access: "private"
        )
      )

      @legacy = FactoryBot.create(
        :user,
        account: @account, roles: Role::AGENT.id,
        restriction_id: RoleRestrictionType.GROUPS, is_moderator: false,
        is_private_comments_only: true
      )

      manually_set_credit_card(@subscription, FactoryBot.attributes_for(:credit_card))
      @subscription.reload
    end

    describe "to a Large subscription plan with the same max_agents" do
      before do
        refute_difference '@account.users.count(:all)' do
          @subscription.update_attributes!(
            plan_type: SubscriptionPlanType.Large,
            base_agents: 10,
            billing_cycle_type: BillingCycleType.Annually
          )
        end
      end

      it "copies the settings from the permission set into the per-agent permissions for non legacy agents" do
        assert_equal RoleRestrictionType.NONE, @agent1.reload.restriction_id
        assert @agent1.reload.is_moderator?
        refute @agent1.is_private_comments_only?

        assert_equal RoleRestrictionType.ORGANIZATION, @agent2.reload.restriction_id
        assert @agent2.reload.is_moderator?
        refute @agent2.is_private_comments_only?

        assert_equal RoleRestrictionType.ASSIGNED, @agent3.reload.restriction_id
        refute @agent3.reload.is_moderator?
        assert @agent3.is_private_comments_only?
      end

      it "removes the permission_set association" do
        assert_nil @agent1.reload.permission_set
        assert_nil @agent2.reload.permission_set
        assert_nil @agent3.reload.permission_set
      end

      it "keeps the old settings on legacy agents" do
        assert_equal RoleRestrictionType.GROUPS, @legacy.reload.restriction_id
        refute @legacy.reload.is_moderator?
        assert @legacy.is_private_comments_only?
      end
    end

    describe "to a Large subscription plan with less max_agents" do
      before do
        @chat_permission = ::PermissionSet.create_chat_agent!(@account)
        Account.any_instance.stubs(has_chat_permission_set?: true)
        Zopim::Reseller.client.stubs(
          update_account!: true,
          create_account_agent!: stub(id: 1),
          account_agent!: stub(id: 1)
        )

        @agent4 = FactoryBot.create(
          :user,
          account: @account, roles: Role::AGENT.id,
          permission_set: build_valid_permission_set(
            @account,
            "Role4",
            ticket_access: "assigned-only",
            forum_access: "readonly",
            comment_access: "private"
          )
        )

        Zopim::Agent.any_instance.stubs(:synchronize!)
        @agent4.build_zopim_identity.tap do |record|
          record.zopim_agent_id = @agent4.id
          record.save!
        end

        refute_difference '@account.users.count(:all)' do
          @subscription.update_attributes!(
            plan_type: SubscriptionPlanType.Large,
            base_agents: 3,
            billing_cycle_type: BillingCycleType.Annually
          )
        end
      end

      it "limits number of agents to 3" do
        assert_equal 3, @account.assignable_agents.count(:all)
      end

      it "downgrades 2 or 3 agents to end users" do
        assert [2, 3].include?(@account.end_users.count(:all))
      end

      it "downgrades 1 agent to chat-only agent" do
        assert_equal 1, @account.chat_agents.count(:all)
      end
    end
  end

  describe "Downgrade to plan with no multibrand" do
    before do
      @account = accounts(:minimum)
      @account.subscription.update_attributes!(plan_type: SubscriptionPlanType.ExtraLarge)
      Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
    end

    describe "in account with one brand" do
      before do
        @account.subscription.update_attributes!(plan_type: SubscriptionPlanType.Large)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
      end

      it "keeps the single brand active" do
        assert @account.reload.default_brand.active?
      end
    end

    describe "in account with multiple brands" do
      before do
        @brand = FactoryBot.create(:brand, account_id: @account.id, active: true)
        @account.reload.update_attributes(name: "New Name")
        assert_not_equal @account.route.brand.name, @account.name

        @account.subscription.update_attributes!(plan_type: SubscriptionPlanType.Large)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!

        @account.reload
        @brand.reload
      end

      it "deactivates extra brands" do
        refute @brand.active?
      end

      it "changes the account name to match the brand name" do
        assert_equal @account.route.brand.name, @account.name
      end
    end

    describe "in account with default brand different than agent brand" do
      before do
        @brand = FactoryBot.create(:brand, account_id: @account.id, active: true)
        @account.update_attributes(default_brand: @brand)

        @account.reload.subscription.update_attributes!(plan_type: SubscriptionPlanType.Large)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
        @account.reload
        @brand.reload
      end

      it "changes the default brand to match the brand of the agent route" do
        refute @brand.default?
        assert @account.route.brand.default?
      end
    end
  end

  describe "Downgrade to plan with no SAML in account with SAML enabled" do
    before do
      Arturo.enable_feature!(:saml)
      @account = accounts(:minimum)
      @account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
      Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
      @account.reload

      @auth = remote_authentications(:minimum_remote_authentication)
      @auth.update_attributes!(
        is_active: true, account: @account, fingerprint: Token.generate(50),
        auth_mode: RemoteAuthentication::SAML, remote_login_url: "https://www.url.com"
      )

      assert @auth.saml_mode_active?

      @account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Medium)
    end

    describe "with security_settings_for_all_accounts arturo off" do
      before do
        Arturo.disable_feature!(:security_settings_for_all_accounts)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
      end

      it "deactivates the remote authentcation" do
        assert_equal [], @account.remote_authentications.saml.active.all
      end
    end

    describe "with security_settings_for_all_accounts arturo on" do
      before do
        Arturo.enable_feature!(:security_settings_for_all_accounts)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
      end

      it "does not deactivate the remote authentcation" do
        assert_not_empty @account.remote_authentications.saml.active.all
      end
    end
  end

  describe "Downgrade to plan with no custom security policy in account using it" do
    before do
      @account = accounts(:minimum)

      @account.subscription.update_attributes!(
        plan_type: SubscriptionPlanType.Large,
        base_agents: 10,
        billing_cycle_type: BillingCycleType.Annually
      )
      Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
      @account.reload

      @account.role_settings.update_attributes!(
        agent_security_policy_id: Zendesk::SecurityPolicy::Custom.id,
        end_user_security_policy_id: Zendesk::SecurityPolicy::Custom.id
      )

      assert @account.subscription.has_custom_security_policy?

      @account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Medium)
    end

    it "changes the account security policy to High" do
      Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
      @account.reload

      assert_equal Zendesk::SecurityPolicy::High.id, @account.role_settings.agent_security_policy_id
      assert_equal Zendesk::SecurityPolicy::High.id, @account.role_settings.end_user_security_policy_id
      refute @account.subscription.has_custom_security_policy?
    end

    describe "with security_settings_for_all_accounts arturo on" do
      before do
        Arturo.enable_feature!(:security_settings_for_all_accounts)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
        @account.reload
      end

      it "policy remains custom" do
        assert_equal Zendesk::SecurityPolicy::Custom.id, @account.role_settings.agent_security_policy_id
      end
    end
  end

  describe "Downgrade to plan with no JWT in account with JWT enabled" do
    before do
      @account = accounts(:minimum)
      @account.role_settings.update_attributes!(
        agent_remote_login: true,
        end_user_remote_login: true,
        agent_zendesk_login: false,
        end_user_zendesk_login: false
      )

      @auth = remote_authentications(:minimum_remote_authentication)
      @auth.update_attributes!(
        is_active: true, account: @account, fingerprint: Token.generate(50),
        auth_mode: RemoteAuthentication::JWT, remote_login_url: "https://www.url.com"
      )

      @account.reload
      assert @auth.jwt_mode_active?

      @account.subscription.stubs(:has_jwt?).returns(false)
    end

    it "deactivates the remote authentcation" do
      Zendesk::Features::Change::JwtChange.new(@account.subscription).downgrade
      assert_equal [], @account.remote_authentications.jwt.active.all
    end

    it "changes login to Zendesk's user authentication" do
      Zendesk::Features::Change::JwtChange.new(@account.subscription).downgrade
      @account.reload

      refute @account.role_settings.end_user_remote_login
      refute @account.role_settings.agent_remote_login
      assert @account.role_settings.agent_zendesk_login
      assert @account.role_settings.end_user_zendesk_login
    end

    describe "with security_settings_for_all_accounts arturo on" do
      before do
        Arturo.enable_feature!(:security_settings_for_all_accounts)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
      end

      it "does not deactivate the remote authentcation" do
        assert_not_empty @account.remote_authentications.jwt.active.all
      end
    end
  end

  describe "Multiple organizations feature" do
    let(:subscription) { accounts(:minimum).subscription }

    before { Arturo.enable_feature!(:multiple_organizations) }

    describe "for a Medium plan" do
      before { refute subscription.has_multiple_organizations? }

      describe "upgrading to Plus" do
        it "gains the multiple organizations feature" do
          subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
          Zendesk::Features::SubscriptionFeatureService.new(subscription.account).execute!
          assert Subscription.find(subscription.id).reload.has_multiple_organizations?
        end

        it "does not enqueue a RemoveRegularOrganizationMembershipsJob" do
          RemoveRegularOrganizationMembershipsJob.expects(:enqueue).never
          subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
          Zendesk::Features::SubscriptionFeatureService.new(subscription.account).execute!
        end
      end

      describe "downgrading to Small" do
        it "does not gain the multiple organizations feature" do
          subscription.update_attribute(:plan_type, SubscriptionPlanType.Small)
          Zendesk::Features::SubscriptionFeatureService.new(subscription.account).execute!
          refute Subscription.find(subscription.id).has_multiple_organizations?
        end

        it "does not enqueue a RemoveMultipleOrganizationsJob" do
          RemoveRegularOrganizationMembershipsJob.expects(:enqueue).never
          subscription.update_attribute(:plan_type, SubscriptionPlanType.Small)
          Zendesk::Features::SubscriptionFeatureService.new(subscription.account).execute!
        end
      end
    end

    describe "for a Plus plan" do
      before do
        subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
        Zendesk::Features::SubscriptionFeatureService.new(subscription.account).execute!
        assert Subscription.find(subscription.id).has_multiple_organizations?
      end

      describe "downgrading to Medium" do
        let(:memberships_association) { stub(regular: []) }

        before do
          Account.any_instance.stubs(organization_memberships: memberships_association)
        end

        it "removes the multiple organizations feature" do
          subscription.update_attribute(:plan_type, SubscriptionPlanType.Medium)
          Zendesk::Features::SubscriptionFeatureService.new(subscription.account).execute!
          refute Subscription.find(subscription.id).has_multiple_organizations?
        end

        it "enqueues a RemoveMultipleOrganizationsJob if there are regular organization memberships" do
          memberships_association.expects(:regular).returns([1])

          RemoveRegularOrganizationMembershipsJob.
            expects(:enqueue).once.with(subscription.account_id)

          subscription.update_attribute(:plan_type, SubscriptionPlanType.Medium)
          Zendesk::Features::SubscriptionFeatureService.new(subscription.account).execute!
        end

        it "does not enqueue a RemoveMultipleOrganizationsJob if there are no regular organization memberships present" do
          memberships_association.expects(:regular).returns([])

          RemoveRegularOrganizationMembershipsJob.expects(:enqueue).never

          subscription.update_attribute(:plan_type, SubscriptionPlanType.Medium)
          Zendesk::Features::SubscriptionFeatureService.new(subscription.account).execute!
        end
      end
    end
  end

  describe "subscription comments" do
    before do
      @account      = paying_account
      @subscription = @account.subscription
    end

    it "accepts more than 255 characters" do
      @subscription.comment = "ab" * 128
      @subscription.save!
      @subscription.reload
      assert_equal 256, @subscription.comment.length
    end

    it "does not call the handle_one_time code" do
      @subscription.expects(:handle_one_time).never
      @subscription.comment = "ab" * 128
      @subscription.save!
    end
  end

  describe "Subscription#has_personal_rules?" do
    before do
      @account      = paying_account
      @subscription = @account.subscription
      Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
    end

    it "returns true when the associated account has the :personal_rules feature" do
      @subscription.account.settings.stubs(:personal_rules?).returns(true)
      assert @subscription.has_personal_rules?
    end
  end

  describe "#registered_with_gateway?" do
    before do
      @account      = accounts(:minimum)
      @subscription = @account.subscription
      @credit_card  = CreditCard.new
      @subscription.credit_card = @credit_card
    end

    it "returns falsely when credit card is not present" do
      @subscription.credit_card = nil
      assert_equal false, @subscription.registered_with_gateway?
    end

    it "returns whatever credit_card.registered? returns" do
      @credit_card.expects(:registered?).returns(true)
      assert(@subscription.registered_with_gateway?)
    end

    it "returns whatever credit_card.registered? returns, part deux" do
      @credit_card.expects(:registered?).returns(false)
      assert_equal false, @subscription.registered_with_gateway?
    end
  end

  describe "#invoicing?" do
    before do
      @account      = accounts(:minimum)
      @subscription = @account.subscription
    end

    it "returns true when payment method is check" do
      @subscription.payment_method_type = PaymentMethodType.MANUAL
      assert @subscription.invoicing?
    end

    it "returns true when payment method is wire" do
      @subscription.payment_method_type = PaymentMethodType.MANUAL
      assert @subscription.invoicing?
    end

    it "returns false when payment method is credit card" do
      @subscription.payment_method_type = PaymentMethodType.CREDIT_CARD
      assert_equal false, @subscription.invoicing?
    end
  end

  describe "#crediting?" do
    before do
      @account      = accounts(:minimum)
      @subscription = @account.subscription
    end

    it "returns false when payment method is check" do
      @subscription.payment_method_type = PaymentMethodType.MANUAL
      refute @subscription.crediting?
    end

    it "returns false when payment method is wire" do
      @subscription.payment_method_type = PaymentMethodType.MANUAL
      refute @subscription.crediting?
    end

    it "returns true when payment method is credit card" do
      @subscription.payment_method_type = PaymentMethodType.CREDIT_CARD
      assert @subscription.crediting?
    end
  end

  def prep(additional = nil)
    vals = @payment_defaults.merge additional
    vals[:period_end_at] = (vals[:period_begin_at] + vals[:duration].days)
    vals.delete :duration
    @subscription.payments.create! vals
  end

  describe 'Current Dunning State' do
    before do
      @subscription = subscriptions(:minimum)
    end

    it 'returns OK when there are no payments' do
      assert_equal "OK", @subscription.current_dunning_state
    end

    it 'returns SECONDWARNING for zuora managed subscription' do
      @subscription.stubs(:zuora_managed?).returns(true)
      @zuora_subscription = @subscription.build_zuora_subscription(dunning_level: 2)
      @subscription.stubs(:zuora_subscription).returns(@zuora_subscription)
      assert_equal "SECONDWARNING", @subscription.current_dunning_state
    end

    it 'returns OK for subscription without zuora_subscription' do
      @subscription.stubs(:zuora_managed?).returns(true)
      @subscription.stubs(:zuora_subscription).returns(nil)
      assert_equal "OK", @subscription.current_dunning_state
    end
  end

  describe 'An invoicing subscription' do
    before do
      @account      = accounts(:minimum)
      @subscription = Subscription.new(account: @account, is_trial: false)

      @subscription.credit_card = nil
      @subscription.payment_method_type = PaymentMethodType.MANUAL

      assert(@subscription.invoicing?)
    end

    it 'returns false when asked if it is a trial subscription' do
      assert_equal(false, @subscription.is_trial?)
    end
  end

  describe "#add_to_comment_field!" do
    before do
      @subscription = subscriptions(:minimum)
      CIA.audit actor: users(:minimum_agent) do
        @subscription.add_to_comment_field!("FOO")
      end
    end

    it "appends a message to the comment field via auditing" do
      assert_equal("FOO", @subscription.cia_events.last.message)
    end
  end

  describe "has_forums2" do
    before do
      @subscription = subscriptions(:minimum)
    end

    describe "pricing model supports the toggle ability (has_forums2_toggle = true)" do
      before do
        @subscription.expects(:has_forums2_toggle?).returns(true)
      end

      it "returns true if the state of the account.settings.forums2 is true" do
        @subscription.account.expects(:has_forums2?).returns(true)
        assert @subscription.has_forums2?
      end

      it "returns false if the state of the account.settings.forums2 is false" do
        @subscription.account.expects(:has_forums2?).returns(false)
        refute @subscription.has_forums2?
      end
    end

    describe "pricing model does not support toggle ability (has_forums2_toggle = false) (new pricing models)" do
      it "returns true" do
        @subscription.expects(:has_forums2_toggle?).returns(false)
        assert @subscription.has_forums2?
      end
    end
  end

  describe '#cancel!' do
    describe 'a paid zendesk subscription' do
      describe 'standalone zendesk' do
        let(:paid_zendesk) { subscriptions(:support) }
        let(:zuora_subscription) { stub }

        before do
          paid_zendesk.expects(:zuora_managed?).returns(true)
          paid_zendesk.expects(:zuora_subscription).returns(zuora_subscription)
        end

        it 'should cancel via zuora' do
          zuora_subscription.expects(:cancel!)
          paid_zendesk.cancel!
        end
      end

      describe 'with a zopim trial' do
        let(:paid_zendesk) { subscriptions(:with_trial_zopim_subscription) }
        let(:zuora_subscription) { stub }
        let(:zopim_trial) { paid_zendesk.zopim_subscription }

        before do
          paid_zendesk.expects(:zuora_managed?).returns(true)
          paid_zendesk.expects(:zuora_subscription).returns(zuora_subscription)
          zuora_subscription.expects(:cancel!)
        end

        it 'destroys the zopim trial' do
          zopim_trial.expects(:destroy)
          paid_zendesk.cancel!
        end
      end

      describe 'with an expired zopim trial/lite subscription' do
        let(:paid_zendesk) { subscriptions(:with_trial_zopim_subscription) }
        let(:zuora_subscription) { stub }
        let(:zopim_trial) { paid_zendesk.zopim_subscription }

        before do
          zopim_trial.update_column(:plan_type, 'lite')
          paid_zendesk.expects(:zuora_managed?).returns(true)
          paid_zendesk.expects(:zuora_subscription).returns(zuora_subscription)
          zuora_subscription.expects(:cancel!)
        end

        it 'destroys the unpaid zopim lite subscription' do
          zopim_trial.expects(:destroy)
          paid_zendesk.cancel!
        end
      end

      describe 'with a purchased zopim subscription' do
        let(:paid_zendesk) { subscriptions(:with_paid_zopim_subscription) }
        let(:zuora_subscription) { stub }
        let(:zopim_subscription) { paid_zendesk.zopim_subscription }

        before do
          paid_zendesk.expects(:zuora_managed?).returns(true)
          paid_zendesk.expects(:zuora_subscription).returns(zuora_subscription)
          zuora_subscription.expects(:cancel!)
        end

        it 'does not destroy the paid zopim subscription' do
          zopim_subscription.expects(:destroy).never
          paid_zendesk.cancel!
        end
      end
    end

    describe 'a trial zendesk subscription' do
      let(:subscription) { subscriptions(:trial) }

      before do
        assert subscription.is_trial?
        subscription.cancel!
      end

      it 'should set the trial to false' do
        refute subscription.is_trial?
      end

      describe 'with a zopim trial' do
        let(:zendesk_trial_subscription) { subscriptions(:with_trial_zendesk_and_zopim) }
        let(:zopim_trial_subscription) { zendesk_trial_subscription.zopim_subscription }

        before do
          zopim_trial_subscription.expects(:destroy)
          zendesk_trial_subscription.cancel!
        end

        it 'sets the zendesk trial to false' do
          refute zendesk_trial_subscription.is_trial?
        end
      end
    end
  end

  describe 'saving a :zuora_managed? subscription' do
    before do
      @subscription = subscriptions(:minimum)
      @subscription.stubs(:zuora_subscription).returns(stub(pricing_model_revision: 5))
      @subscription.stubs(:zuora_managed?).returns(true)
    end

    it "allows 'BiAnnual' BillingCyleType" do
      @subscription.billing_cycle_type = BillingCycleType.Biannually
      assert @subscription.save!
    end
  end

  describe "Conversion" do
    describe "From trial" do
      before do
        @account = accounts(:trial)
      end

      describe "To invoicing" do
        before do
          @account.subscription.update_attributes!(payment_method_type: PaymentMethodType.MANUAL)
        end
        it "sets is_trial to false" do
          assert_equal false, @account.subscription.is_trial
        end
      end

      describe "To sponsored" do
        before do
          @account.subscription.managed_update = true
          @account.subscription.update_attributes!(manual_discount: 100, manual_discount_expires_on: 30.days.from_now.to_date)
          assert @account.subscription.is_sponsored?
        end
        it "sets the is_trial to false" do
          assert_equal false, @account.subscription.is_trial
        end
      end
    end
  end

  describe "In Hub and spoke" do
    describe "An unserviceable account" do
      before do
        @spoke_account = accounts(:trial)
        @spoke_account.subscription.managed_update = true
        @spoke_account.subscription.update_attributes!(trial_expires_on: 3.days.ago)
        @spoke_account.update_attributes!(is_serviceable: false)
        @hub_account = accounts(:minimum)
        refute @spoke_account.is_serviceable?
      end

      describe "which becomes spoke" do
        before do
          @spoke_account.subscription.update_attributes!(hub_account_id: @hub_account.id)
        end

        it "becomes serviceable automatically" do
          assert @spoke_account.reload.is_serviceable
        end

        it "is returned in the list of hub_account's spokes" do
          assert_equal [@spoke_account], @hub_account.reload.spoke_accounts
        end
      end
    end

    describe "a hub that has had all of its spokes deleted" do
      before do
        @spoke_account = accounts(:trial)
        @spoke_account.subscription.managed_update = true
        @hub_account = accounts(:minimum)
        @spoke_account.subscription.update_attributes!(hub_account_id: @hub_account.id)
        @spoke_account.update_column(:deleted_at, Time.now)
        assert @spoke_account.deleted?
      end

      it "should not count as a hub" do
        refute @hub_account.hub?
      end
    end

    describe "An account becoming spoke" do
      before do
        @spoke_account = accounts(:trial)
        @spoke_account.subscription.managed_update = true
        @hub_account = accounts(:minimum)
        assert @spoke_account.subscription.is_trial
        @spoke_account.subscription.update_attributes!(hub_account_id: @hub_account.id)
      end

      it "has the is_trial column set to false" do
        assert_equal false, @spoke_account.subscription.is_trial
      end
    end
  end

  describe "copy_invoice_list" do
    before do
      @subscription = Subscription.new
    end

    it "returns the invoice list correctly for the emails seperated by space" do
      @subscription.copy_invoices_to = "willy.wonka@example.com hickory.dickory@example.com"
      assert_equal @subscription.copy_invoice_list, ["willy.wonka@example.com", "hickory.dickory@example.com"]
    end

    it "returns the invoice list correctly for the emails seperated by comma" do
      @subscription.copy_invoices_to = "willy.wonka@example.com, hickory.dickory@example.com"
      assert_equal @subscription.copy_invoice_list, ["willy.wonka@example.com", "hickory.dickory@example.com"]
    end

    it "returns the invoice list correctly for the emails with leading and trialing spaces" do
      @subscription.copy_invoices_to = "     willy.wonka@example.com, hickory.dickory@example.com         "
      assert_equal @subscription.copy_invoice_list, ["willy.wonka@example.com", "hickory.dickory@example.com"]
    end

    it "returns nil for empty invoice list" do
      @subscription.copy_invoices_to = ""
      assert_nil @subscription.copy_invoice_list
    end
  end

  describe '#has_multibrand?' do
    before do
      @subscription = subscriptions(:minimum)
    end

    describe "For Pre-Patagonia pricing model" do
      describe "For Plus account" do
        before do
          @subscription.plan_type = 3
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
        end

        it "does not have multibrand enabled" do
          refute @subscription.has_multibrand?
        end
      end

      describe "For Enterprise account" do
        before do
          @subscription.plan_type = 4
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
        end

        it "has multibrand enabled" do
          assert @subscription.has_multibrand?
        end
      end
    end

    describe "For Patagonia pricing model" do
      before do
        @subscription.pricing_model_revision = 7
      end

      describe "For Enterprise account" do
        before do
          @subscription.plan_type = 4
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
        end

        it "has multibrand enabled" do
          assert @subscription.has_multibrand?
        end
      end

      describe "For Regular account" do
        before do
          @subscription.plan_type = 2
          Zendesk::Features::SubscriptionFeatureService.new(@subscription.account).execute!
        end

        it "does not have multibrand enabled" do
          refute @subscription.has_multibrand?
        end
      end
    end

    describe "With feature framework" do
      describe "with limited multibrand" do
        before do
          Subscription.any_instance.stubs(:feature_present?).with(:unlimited_multibrand).returns(false)
          Subscription.any_instance.stubs(:feature_present?).with(:limited_multibrand).returns(true)
        end

        it "has multibrand enabled" do
          assert @subscription.has_multibrand?
        end
      end

      describe "with unlimited multibrand" do
        before do
          Subscription.any_instance.stubs(:feature_present?).with(:unlimited_multibrand).returns(true)
          Subscription.any_instance.stubs(:feature_present?).with(:limited_multibrand).returns(false)
        end

        it "has multibrand enabled" do
          assert @subscription.has_multibrand?
        end
      end

      describe "without limited or unlimited multibrand" do
        before do
          Subscription.any_instance.stubs(:feature_present?).with(:unlimited_multibrand).returns(false)
          Subscription.any_instance.stubs(:feature_present?).with(:limited_multibrand).returns(false)
        end

        it "does not have multibrand enabled" do
          refute @subscription.has_multibrand?
        end
      end

      describe "caching" do
        let(:feature_names) { [:limited_multibrand, :unlimited_multibrand] }

        before do
          feature_names.each do |feature_name|
            @subscription.features.where(
              account_id:      @subscription.account.id,
              subscription_id: @subscription.id,
              name:            feature_name.to_s,
              value:           '0'
            ).first_or_initialize.save!
          end
        end

        it "hits the database once for each feature" do
          assert_sql_queries 1 do
            10.times { @subscription.has_multibrand? }
          end
        end
      end
    end
  end

  describe '#currency_symbol' do
    before do
      @subscription = subscriptions(:minimum)
      @subscription.stubs(currency_type: 2)
    end

    it "returns the '€' symbol for EUR currency_type" do
      assert_equal '€', @subscription.currency_symbol
    end
  end

  describe '#currency_name' do
    before do
      @subscription = subscriptions(:minimum)
      @subscription.stubs(currency_type: 2)
    end

    it "returns the 'EUR' name for EUR currency_type" do
      assert_equal 'EUR', @subscription.currency_name
    end
  end

  describe '#currency=' do
    let(:subscription) { subscriptions(:minimum) }

    describe "set to 'USD'" do
      before { subscription.currency = 'USD' }

      it "returns 'USD'" do
        assert_equal 'USD', subscription.currency_name
      end
    end

    describe "set to 'GBP'" do
      before { subscription.currency = 'GBP' }

      it "returns 'GBP'" do
        assert_equal 'GBP', subscription.currency_name
      end
    end

    describe "with invalid value 'FOO'" do
      before { subscription.currency = 'FOO' }

      it "returns 'USD'" do
        assert_equal 'USD', subscription.currency_name
      end
    end
  end

  describe '#configure_trial' do
    it 'is run when creating a subscription for a classic account' do
      Subscription.any_instance.expects(:configure_trial).once

      account = Accounts::Classic.new do |a|
        a.name            = 'NewTestAccount'
        a.subdomain       = Token.generate(10)
        a.time_zone       = 'Copenhagen'
        a.help_desk_size  = 'Small team'
        a.set_owner(name: 'Random Person', email: 'radnom.person@example.com', is_verified: true)
        a.set_address(country_code: 'SI', phone: '123456')
      end

      account.save
    end
  end

  describe '#set_trial_expiry' do
    subject { account.subscription }

    describe 'for a normal account' do
      let(:account) do
        account = Accounts::Classic.new do |a|
          a.name            = 'NewTestAccount'
          a.subdomain       = Token.generate(10)
          a.time_zone       = 'Copenhagen'
          a.help_desk_size  = 'Small team'
          a.set_owner(name: 'Random Person', email: 'radnom.person@example.com', is_verified: true)
          a.set_address(country_code: 'SI', phone: '123456')
        end

        account.save!
        account
      end

      it 'sets is_trial: true and trial expiry for 30 days from now' do
        assert subject.is_trial
        assert_equal 30.days.from_now.to_date, subject.trial_expires_on
      end
    end

    describe 'for an account in 14-day trial treatment group' do
      let(:account) do
        account = Accounts::Classic.new do |a|
          a.name            = 'NewTestAccount'
          a.subdomain       = Token.generate(10)
          a.time_zone       = 'Copenhagen'
          a.help_desk_size  = 'Small team'
          a.set_owner(name: 'Random Person', email: 'radnom.person@example.com', is_verified: true)
          a.set_address(country_code: 'SI', phone: '123456')
        end

        account.add_trial_extras(alternative_length: true)

        account.save!
        account
      end

      it 'sets trial expiry for 14 days from now' do
        assert_equal 14.days.from_now.to_date, subject.trial_expires_on
      end
    end

    describe 'for an account with the classic_14_day_trial arturo enabled' do
      let(:account) do
        account = Accounts::Classic.new do |a|
          a.name            = 'NewTestAccount'
          a.subdomain       = Token.generate(10)
          a.time_zone       = 'Copenhagen'
          a.help_desk_size  = 'Small team'
          a.set_owner(name: 'Random Person', email: 'radnom.person@example.com', is_verified: true)
          a.set_address(country_code: 'SI', phone: '123456')
        end

        account.add_trial_extras(alternative_length: false)
        Arturo.enable_feature!(:classic_14_day_trial)

        account.save!
        account
      end

      it 'sets trial expiry for 14 days from now' do
        assert_equal 14.days.from_now.to_date, subject.trial_expires_on
      end
    end
  end

  describe "value added tax" do
    subject { accounts(:minimum).subscription }

    let(:address) { accounts(:minimum).address }

    describe "account address in the United States" do
      before do
        address.country_id = 156
        Account.any_instance.stubs(:address).returns(address)
      end

      it "does not return a #vat_name" do
        assert_nil subject.vat_name
      end

      it "does not #collect_vat?" do
        refute subject.collect_vat?
      end
    end

    describe "account address in Italy" do
      before do
        address.country_id = 74
        Account.any_instance.stubs(:address).returns(address)
      end

      it "returns IVA Italy's #vat_name" do
        assert_equal "IVA", subject.vat_name
      end

      it "#collect_vat?s for Italy" do
        assert subject.collect_vat?
      end
    end

    describe "account address not set (nil)" do
      before do
        address.country_id = nil
        Account.any_instance.stubs(:address).returns(address)
      end

      it "does not have a #vat_name" do
        assert_nil subject.vat_name
      end

      it "does not #collect_vat?" do
        refute subject.collect_vat?
      end
    end
  end

  describe "touch account after save" do
    subject { accounts(:minimum).subscription }

    it "touchs account after save" do
      updated_at = subject.account.updated_at
      subject.update_attribute(:plan_type, SubscriptionPlanType.Inbox)

      refute_equal updated_at, subject.account.updated_at
    end

    it "skips touch account during pre creation" do
      PreAccountCreation.create(
        account_id: subject.account.id,
        locale_id: 1,
        pod_id: 1,
        status: 0,
        account_class: 'classic',
        region: 'us',
        source: 'web marketing'
      )

      updated_at = subject.account.updated_at
      subject.account.expects(:touch).never
      subject.update_attribute(:plan_type, SubscriptionPlanType.Inbox)

      assert_equal updated_at, subject.account.updated_at
    end
  end

  describe "#subscription_feature_service" do
    subject { account.subscription }

    describe "when the plan type changes on a trial" do
      let(:account) { accounts(:trial) }
      it "calls the subscription feature with correct parameters" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account, nil, nil, {}, false).returns(stub(execute!: true))
        subject.update_attribute(:plan_type, SubscriptionPlanType.Inbox)
      end
    end

    describe "when the plan type changes to inbox" do
      let(:account) { accounts(:minimum) }

      it "calls the subscription feature with correct parameters" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account, nil, nil, {}, false).returns(stub(execute!: true))
        subject.update_attribute(:plan_type, SubscriptionPlanType.Inbox)
      end
    end

    describe "when the plan type changes from inbox" do
      let(:account) { accounts(:minimum) }

      before do
        subject.update_attribute(:plan_type, SubscriptionPlanType.Inbox)
      end

      it "calls the subscription feature with correct parameters" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account, nil, nil, {}, false).returns(stub(execute!: true))
        subject.update_attribute(:plan_type, SubscriptionPlanType.Large)
      end
    end

    describe "when the plan type changes on a trial from monitor" do
      let(:account) { accounts(:trial) }

      before do
        subject.update_from_monitor_pod = true
      end

      it "calls the subscription feature with skip feature triggers as true" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account, nil, nil, {}, true).returns(stub(execute!: true))
        subject.update_attribute(:plan_type, SubscriptionPlanType.Inbox)
      end
    end

    describe "when the plan type changes on a manual account" do
      let(:account) { accounts(:trial) }

      before do
        subject.is_trial = false
        subject.payment_method_type = PaymentMethodType.MANUAL
      end

      it "calls the subscription feature service" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account, nil, nil, {}, false).returns(stub(execute!: true))
        subject.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge)
      end
    end

    describe "when agent count changes on a trial" do
      let(:account) { accounts(:trial) }
      it "calls the subscription feature" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account).never
        subject.update_attribute(:base_agents, 42)
      end
    end

    describe "when the plan type changes on a paid account" do
      let(:account) { accounts(:minimum) }
      it "does not update the subscription features" do
        Zendesk::Features::SubscriptionFeatureService.stubs(:new).returns(stub(execute!: true))
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account).never
        subject.update_attribute(:plan_type, SubscriptionPlanType.Inbox)
      end
    end

    describe "when an expired trial comes out of expiration" do
      let(:account) { accounts(:trial) }
      let(:expired_subscription) do
        account.subscription.update_attribute(:is_trial, false)
        account.reload
        account.subscription
      end
      let(:reactivate) { expired_subscription.update_attribute(:is_trial, true) }

      it "calls the subscription feature with correct parameters" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account, nil, nil, {}, false).returns(stub(execute!: true))
        reactivate
      end
    end

    describe "when the payment method type changed to manual" do
      let(:account) { accounts(:minimum) }

      it "updates subscription features" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).with(account, nil, nil, {}, false).returns(stub(execute!: true))
        subject.update_attribute(:payment_method_type, PaymentMethodType.MANUAL)
      end
    end

    describe "when the payment method type changed to non-manual" do
      let(:account) { accounts(:minimum) }

      it "does not update subscription features" do
        Zendesk::Features::SubscriptionFeatureService.expects(:new).never
        subject.update_attribute(:payment_method_type, PaymentMethodType.CREDIT_CARD)
      end
    end
    #  NOTE SOMETHING IN SANDBOX IS TRYING TO SET MAX AGENtS
    describe "when account is sandbox" do
      let(:account) { accounts(:minimum) }
      let(:sandbox) { account.reset_sandbox }
      let(:subject) { sandbox.subscription }

      let(:feature_service_stub) { stub(create_sandbox_features!: nil, execute!: nil) }

      describe "after updating plan type" do
        it "updates subscription features" do
          Zendesk::Features::SubscriptionFeatureService.stubs(:new).returns(feature_service_stub)
          subject.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge)
        end
      end

      describe "after updating pricing model revision" do
        it "updates subscription features" do
          Zendesk::Features::SubscriptionFeatureService.stubs(:new).returns(feature_service_stub)
          subject.update_attribute(:pricing_model_revision, 1)
        end
      end

      describe "after updating other attributes" do
        it "updates subscription features" do
          Zendesk::Features::SubscriptionFeatureService.any_instance.expects(:create_sandbox_features!)
          subject.update_attribute(:base_agents, 100)
        end
      end
    end
  end

  describe '#generate_max_agents' do
    before { @subscription = subscriptions(:minimum) }

    describe 'with NO temporary_zendesk_agents' do
      describe "base_agent change" do
        it 'sets max_agents equal to base_agents' do
          assert_equal 10, @subscription.base_agents
          assert_equal 10, @subscription.max_agents
          @subscription.update_attributes!(base_agents: 7)
          assert_equal 7, @subscription.base_agents
          assert_equal 7, @subscription.max_agents
        end
      end

      describe "attempt to set max_agents" do
        it "should not alter max or base agents" do
          assert_equal 10, @subscription.base_agents
          assert_equal 10, @subscription.max_agents
          @subscription.update_attributes!(max_agents: 11)
          assert_equal 10, @subscription.base_agents
          assert_equal 10, @subscription.max_agents
        end
      end
    end

    describe 'with temporary_zendesk_agents' do
      before do
        @account = accounts(:minimum)
        @temp_zendesk_agents_addon = SubscriptionFeatureAddon.create(
          account_id: @account.id,
          zuora_rate_plan_id: 'foo',
          name: 'temporary_zendesk_agents',
          quantity: 10,
          starts_at: 3.months.ago,
          expires_at: 3.months.from_now
        )
      end

      describe 'when subscription is saved' do
        it 'calculates max_agents correctly' do
          expected = @subscription.base_agents + @temp_zendesk_agents_addon.quantity
          assert_equal @subscription.base_agents, @subscription.max_agents
          @subscription.save
          assert_equal expected, @subscription.max_agents
        end

        describe 'with multiple overlapping temporary_zendesk_agents addons' do
          before do
            @temp_zendesk_agents_addon_two = SubscriptionFeatureAddon.create(
              account_id: @account.id,
              zuora_rate_plan_id: 'bar',
              name: 'temporary_zendesk_agents',
              quantity: 17,
              starts_at: 3.months.ago,
              expires_at: 3.months.from_now
            )
          end

          it 'calculates max_agents correctly' do
            expected = @subscription.base_agents + @temp_zendesk_agents_addon.quantity + @temp_zendesk_agents_addon_two.quantity
            assert_equal @subscription.base_agents, @subscription.max_agents
            @subscription.save
            assert_equal expected, @subscription.max_agents
          end
        end
      end

      describe '#base_agemts=' do
        it 'sets base_agents' do
          assert_equal 10, @subscription.base_agents
          @subscription.base_agents = @subscription.base_agents + 5
          assert_equal 15, @subscription.base_agents
        end

        it 'sets max_agents to base_agents + temp_agents' do
          assert_equal 10, @subscription.max_agents
          @subscription.base_agents = @subscription.base_agents + 5
          expected = @subscription.base_agents + @temp_zendesk_agents_addon.quantity
          assert_equal expected, @subscription.max_agents
        end
      end

      describe '#max_agents=' do
        it 'ignores the parameter and instead re-calculates max_agents from base_agents' do
          @subscription.max_agents = 100
          expected_max_agents = @subscription.base_agents + @temp_zendesk_agents_addon.quantity
          assert_equal expected_max_agents, @subscription.max_agents
        end
      end

      describe 'when temporary_zendesk_agents have expired' do
        describe 'but are still within grace period' do
          it 'should still include the expired temporary agents in max_agents calculation' do
            temp_addon = @account.subscription_feature_addons.first
            temp_addon.expires_at = 6.days.ago
            temp_addon.save
            @subscription.save

            expected_max_agents = @subscription.base_agents + temp_addon.quantity
            assert_equal expected_max_agents, @subscription.max_agents
          end
        end

        describe 'and are outside grace period' do
          it 'should not include the expired temporary agents in max_agents calculation' do
            temp_addon = @account.subscription_feature_addons.first
            temp_addon.expires_at = 8.days.ago
            temp_addon.save
            @subscription.save

            assert_equal @subscription.base_agents, @subscription.max_agents
          end
        end
      end
    end
  end

  describe '#add_agents_available?' do
    let(:zendesk_product) { stub(plan_settings: {}) }

    before do
      @zuora_subscription = build_zuora_subscription
      Zendesk::Accounts::Client.any_instance.stubs(:product).returns(zendesk_product)
    end

    describe 'sales assisted' do
      before do
        @zuora_subscription.sales_model = ZBC::States::SalesModel::ASSISTED
      end

      describe 'assisted_agent_add_enabled YES' do
        before do
          @zuora_subscription.assisted_agent_add_enabled = true
          @zuora_subscription.save!
          @subscription = @zuora_subscription.subscription
        end

        it 'returns true' do
          assert @subscription.add_agents_available?
        end

        describe 'with a future subscription' do
          before do
            @zuora_subscription.future_subscription_starts_at = 3.days.from_now
            @zuora_subscription.save!
            @subscription = @zuora_subscription.subscription
          end

          it 'returns false' do
            refute @subscription.add_agents_available?
          end
        end

        describe 'with an addon' do
          before do
            @subscription.account.subscription_feature_addons.create!(
              name:               'advanced_security',
              zuora_rate_plan_id: '123456'
            )
          end

          it 'returns false' do
            refute @subscription.add_agents_available?
          end
        end

        describe 'a starter customer' do
          let(:subscription) do
            subscriptions(:minimum)
          end

          before do
            subscription.update_attributes(
              plan_type:   SubscriptionPlanType.Small,
              base_agents: 3,
              pricing_model_revision: ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE
            )
          end

          describe "pricing model revision before ZBC::Zendesk::PricingModelRevision::PATAGONIA" do
            it 'returns false' do
              refute subscription.add_agents_available?
            end
          end

          describe "ZBC::Zendesk::PricingModelRevision::PATAGONIA pricing model Essential" do
            before do
              subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::PATAGONIA)
            end

            it 'returns true' do
              assert subscription.add_agents_available?
            end
          end
        end
      end

      describe 'assisted_agent_add_enabled NO' do
        before do
          @zuora_subscription.assisted_agent_add_enabled = false
          @zuora_subscription.save!
          @subscription = @zuora_subscription.subscription
        end

        it 'returns false' do
          refute @subscription.add_agents_available?
        end
      end
    end

    describe 'starter customer' do
      before do
        @zuora_subscription.plan_type = 1
        @zuora_subscription.pricing_model_revision = 2
        @zuora_subscription.save!
        @subscription = @zuora_subscription.subscription
      end

      it 'returns false' do
        refute @subscription.add_agents_available?
      end
    end

    describe 'suite customer' do
      let(:settings) { stub }

      before do
        @subscription = @zuora_subscription.subscription
        @subscription.account.stubs(:settings).returns(settings)
        settings.stubs(:suite_subscription?).returns(true)
      end

      it 'returns false' do
        refute @subscription.add_agents_available?
      end
    end

    describe 'customer with a future subscription' do
      before do
        @zuora_subscription.future_subscription_starts_at = 3.days.from_now
        @zuora_subscription.save!
        @subscription = @zuora_subscription.subscription
      end

      it 'returns false' do
        refute @subscription.add_agents_available?
      end
    end

    describe 'customer with a guide subscription' do
      describe 'record in account service' do
        let(:guide_sub) do
          stub(plan_type: ZBC::Guide::PlanType::Professional.plan_type)
        end

        before do
          @zuora_subscription.save!
          @subscription = @zuora_subscription.subscription
          guide_sub.stubs(:active?).returns(true)
          Zendesk::Accounts::Client.any_instance.stubs(:product).with('guide', use_cache: false).returns(guide_sub)
          Zendesk::Accounts::Client.any_instance.stubs(:product).with('guide', use_cache: true).returns(guide_sub)
        end

        describe 'no billing subscription for guide' do
          before do
            guide_sub.stubs(:subscribed?).returns(false)
          end

          it 'returns true' do
            assert @subscription.add_agents_available?
          end
        end

        describe 'paid guide product' do
          before do
            guide_sub.stubs(:subscribed?).returns(true)
          end

          it 'returns false' do
            refute @subscription.add_agents_available?
          end
        end

        describe 'caching behavior' do
          before do
            Zendesk::Accounts::Client.any_instance.expects(:product).with('guide', use_cache: use_cache)
            Zendesk::Accounts::Client.any_instance.expects(:product).with('explore', use_cache: use_cache)
          end

          let(:use_cache) { true }

          it 'finds pravda product using cache' do
            @subscription.add_agents_available?
          end
        end
      end

      describe 'record not in account service' do
        before do
          @zuora_subscription.save!
          @subscription = @zuora_subscription.subscription
          Zendesk::Accounts::Client.any_instance.stubs(:product).with('guide', use_cache: true).returns(nil)
        end

        it 'returns true' do
          assert @subscription.add_agents_available?
        end
      end

      describe 'account service returns error' do
        before do
          @zuora_subscription.save!
          @subscription = @zuora_subscription.subscription
          Zendesk::Accounts::Client.any_instance.stubs(:product).with('guide').raises(Kragle::ResourceNotFound)
        end

        it 'returns true' do
          assert @subscription.add_agents_available?
        end
      end
    end

    describe 'customer with an explore subscription' do
      describe 'record in account service' do
        let(:explore_sub) do
          stub(plan_type: ZBC::Explore::PlanType::Professional.plan_type)
        end

        before do
          @zuora_subscription.save!
          @subscription = @zuora_subscription.subscription
          explore_sub.stubs(:active?).returns(true)
          Zendesk::Accounts::Client.any_instance.stubs(:product).with('explore', use_cache: true).returns(explore_sub)
          Zendesk::Accounts::Client.any_instance.stubs(:product).with('explore', use_cache: false).returns(explore_sub)
        end

        describe 'no billing subscription for explore' do
          before do
            explore_sub.stubs(:subscribed?).returns(false)
          end

          it 'returns true' do
            assert @subscription.add_agents_available?
          end
        end

        describe 'paid explore product' do
          before do
            explore_sub.stubs(:subscribed?).returns(true)
          end

          it 'returns false' do
            refute @subscription.add_agents_available?
          end
        end
      end

      describe 'record not in account service' do
        before do
          @zuora_subscription.save!
          @subscription = @zuora_subscription.subscription
          Zendesk::Accounts::Client.any_instance.stubs(:product).with('explore').returns(nil)
        end

        it 'returns true' do
          assert @subscription.add_agents_available?
        end
      end

      describe 'account service returns error' do
        before do
          @zuora_subscription.save!
          @subscription = @zuora_subscription.subscription
          Zendesk::Accounts::Client.any_instance.stubs(:product).with('explore').raises(Kragle::ResourceNotFound)
        end

        it 'returns true' do
          assert @subscription.add_agents_available?
        end
      end
    end
  end

  describe '#future_subscription?' do
    before do
      @zuora_subscription = build_zuora_subscription
    end

    describe 'zuora_subscription with a future_subscription' do
      before do
        @zuora_subscription.future_subscription_starts_at = 3.days.from_now
        @zuora_subscription.save!
        @subscription = @zuora_subscription.subscription
      end

      it 'returns true' do
        assert @subscription.future_subscription?
      end
    end

    describe 'zuora_subscription without a future subscription' do
      before do
        @zuora_subscription.save!
        @subscription = @zuora_subscription.subscription
      end

      it 'returns false' do
        refute @subscription.future_subscription?
      end
    end
  end

  describe '#term_end_date' do
    before do
      @zuora_subscription = build_zuora_subscription
      @zuora_subscription.save!
      @subscription = @zuora_subscription.subscription
    end

    describe 'with nil value in zuora_subscription' do
      before do
        @zuora_subscription.term_end_date = nil
      end

      it 'returns nil' do
        assert_nil @subscription.term_end_date
      end
    end

    describe 'with no zuora_subscription' do
      before do
        @zuora_subscription = nil
      end

      it 'returns nil' do
        assert_nil @subscription.term_end_date
      end
    end

    describe 'with date value' do
      before do
        @test_date = Time.zone.now.to_date
        @zuora_subscription.term_end_date = @test_date
      end

      it 'returns nil' do
        assert_equal @subscription.term_end_date, @test_date
      end
    end
  end

  describe '#is_expired?' do
    before do
      @subscription = Subscription.new
      @subscription.account = accounts(:minimum)
    end

    describe 'when plan_type is inbox' do
      before do
        @subscription.plan_type = SubscriptionPlanType.Inbox
        @subscription.trial_expires_on = nil
      end
      it 'returns false' do
        refute @subscription.is_expired?
      end
    end
  end

  describe '#zuora_payment_method_type' do
    before do
      @subscription = Subscription.new
    end

    describe 'when zuora_subscription not present' do
      it 'returns none' do
        assert_equal 'none', @subscription.zuora_payment_method_type
      end
    end
    describe 'when zuora_subscription present' do
      describe 'when credit card' do
        before do
          @subscription.build_zuora_subscription(payment_method_type: 1)
        end
        it 'returns none' do
          assert_equal 'credit card', @subscription.zuora_payment_method_type
        end
      end

      describe 'when paypal' do
        before do
          @subscription.build_zuora_subscription(payment_method_type: 2)
        end
        it 'returns none' do
          assert_equal 'paypal', @subscription.zuora_payment_method_type
        end
      end

      describe 'when manual invoice' do
        before do
          @subscription.build_zuora_subscription(payment_method_type: 3)
        end
        it 'returns none' do
          assert_equal 'manual invoice', @subscription.zuora_payment_method_type
        end
      end
    end
  end

  describe "account product sync validation" do
    let(:subscription) { subscriptions(:minimum) }

    it "enqueues an AccountProductValidationJob when saving subscription record" do
      subscription.expects(:instrument_product_sync_validation).at_least_once
      subscription.base_agents = 999
      subscription.save!
    end
  end

  describe '#seasonal_quantity' do
    let(:support_product) do
      Zendesk::Accounts::Product.new(
        'state'         => :subscribed,
        'name'          => :support,
        'plan_settings' => plan_settings
      )
    end

    let(:subscription) { subscriptions(:minimum) }

    before do
      Zendesk::Accounts::Client.any_instance.stubs(:product).
        returns(support_product)
    end

    subject { subscription.seasonal_quantity }

    describe 'seasonal quantity nil in Pravda' do
      let(:plan_settings) do
        { 'seasonal_quantity' => nil }
      end

      it 'returns 0' do
        assert_equal 0, subject
      end
    end

    describe 'seasonal quantity set to 0 in Pravda' do
      let(:plan_settings) do
        { 'seasonal_quantity' => 0 }
      end

      it 'returns the seasonal quantity from Pravda' do
        assert_equal 0, subject
      end
    end

    describe 'seasonal quantity set to 1 in Pravda' do
      let(:plan_settings) do
        { 'seasonal_quantity' => 1 }
      end

      it 'returns the seasonal quantity from Pravda' do
        assert_equal 1, subject
      end
    end
  end

  describe '#primary_quantity' do
    let(:support_product) do
      Zendesk::Accounts::Product.new(
        'state'         => :subscribed,
        'name'          => :support,
        'plan_settings' => plan_settings
      )
    end

    let(:subscription) { subscriptions(:minimum) }

    before do
      Zendesk::Accounts::Client.any_instance.stubs(:product).
        returns(support_product)
    end

    subject { subscription.primary_quantity }

    describe 'seasonal quantity nil in Pravda' do
      let(:plan_settings) do
        { 'seasonal_quantity' => nil }
      end

      it 'is equal to #base_agents' do
        assert_equal subscription.base_agents, subject
      end
    end

    describe 'seasonal quantity set to 0 in Pravda' do
      let(:plan_settings) do
        { 'seasonal_quantity' => 0 }
      end

      it 'is equal to #base_agents' do
        assert_equal subscription.base_agents, subject
      end
    end

    describe 'seasonal quantity set to 1 in Pravda' do
      let(:plan_settings) do
        { 'seasonal_quantity' => 1 }
      end

      it 'substracts the seasonal quantity from base agents' do
        assert_equal subscription.base_agents - 1, subject
      end
    end
  end

  describe '#has_groups?' do
    let(:subscription) do
      subscriptions(:minimum).tap do |subscription|
        subscription.pricing_model_revision = ZBC::Zendesk::PricingModelRevision::PATAGONIA
        subscription.plan_type = SubscriptionPlanType.Small
        subscription.save!
      end
    end

    describe 'enable_essential_groups feature enabled' do
      before { Arturo.enable_feature!(:enable_essential_groups) }

      it 'groups feature is available' do
        assert subscription.has_groups?
      end
    end

    describe 'enable_essential_groups feature disabled' do
      before { Arturo.disable_feature!(:enable_essential_groups) }

      it 'groups feature not available for essential patagonia account' do
        refute subscription.has_groups?
      end

      describe 'not patagonia' do
        before do
          subscription.update_attributes!(
            pricing_model_revision: ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE
          )
        end

        it 'groups feature is available' do
          assert subscription.has_groups?
        end
      end

      describe 'not small plan' do
        before do
          subscription.update_attributes!(
            plan_type: SubscriptionPlanType.Medium
          )
        end

        it 'groups feature is available' do
          assert subscription.has_groups?
        end
      end
    end
  end
end
