require_relative "../support/test_helper"

SingleCov.covered! uncovered: 9

describe Organization do
  fixtures :accounts,
    :organizations,
    :taggings,
    :cf_fields,
    :cf_values

  let(:organization) { organizations(:minimum_organization1) }
  let(:account) { accounts(:minimum) }
  let(:account_2) { accounts(:support) }

  describe "validation" do
    let(:organization) do
      account.organizations.new(
        name: "test   \u200D",
        notes: "test   \u200B",
        domain_names: "TEST.com",
        details: "test   \u200C",
        is_shared_comments: true
      )
    end

    before { organization.stubs(is_shared?: false) }

    describe "name" do
      # prevent unrelated failure with a stub
      before { Organization.any_instance.stubs(:lock_create_and_update?).returns(false) }

      should_validate_presence_of :name
      should validate_uniqueness_of(:name).case_insensitive
    end

    describe "on name" do
      it "converts invalid bytes to valid utf8" do
        organization.name = "  \x90 "
        assert_valid organization
        assert_equal "�", organization.name
      end

      it "strips trailing whitespace from the name" do
        assert_valid organization
        assert_equal "test", organization.name
      end

      # https://support.zendesk.com/agent/tickets/1532703
      describe "weird Swedish characters" do
        let(:org1) { organization.dup }
        let(:org2) { organization.dup }

        before do
          # prevent unrelated failure with a stub
          org1.stubs(:lock_create_and_update?).returns(false)
          org2.stubs(:lock_create_and_update?).returns(false)

          org1.name = "Svenska Våg AB"
          org1.save!
        end

        it "allows the same name using å vs ä" do
          org2.name = "Svenska Väg AB"

          assert_valid org2
          org2.save!
          assert_equal "Svenska Väg AB", org2.reload.name
        end

        it "prevents names that are actually the same" do
          org2.name = "Svenska Våg AB"

          refute_valid org2
          refute org2.save, "org with a non-unique name should not be saved"
          org2.errors.full_messages.must_include "Name:  #{org2.name} has already been taken"
        end

        it "compares all orgs that mysql thinks have the same name" do
          org2.name = "Svenska Väg AB"
          org2.save!
          org3 = org2.dup

          refute_valid org3
          org3.errors.full_messages.must_include "Name:  #{org2.name} has already been taken"
        end
      end

      describe "redis lock for duplicate organization names" do
        let(:organization_different_account) do
          account_2.organizations.new(
            name: "test",
            notes: "notes",
            domain_names: "TEST.com",
            details: "details",
            is_shared_comments: true
          )
        end
        let(:organization_different_name) do
          account.organizations.new(
            name: "different name",
            notes: "notes",
            domain_names: "TEST.com",
            details: "details",
            is_shared_comments: true
          )
        end

        describe 'the lock key name' do
          it "is the same for the same account, name combination" do
            # single_execution_key is deterministic and defined the same for different instances
            key_1 = organization.send(:single_execution_key, organization.account_id, organization.name)
            key_2 = organization_different_name.send(:single_execution_key, organization.account_id, organization.name)
            assert_equal key_1, key_2
          end

          it "is distinct for organizations with distinct names" do
            key_1 = organization.send(:single_execution_key, account.id, "test")
            key_2 = organization_different_name.send(:single_execution_key, account.id, "different name")
            assert_not_equal key_1, key_2
          end

          it "is distinct for organizations for different accounts" do
            key_1 = organization.send(:single_execution_key, account.id, "test")
            key_2 = organization_different_account.send(:single_execution_key, account_2.id, "test")
            assert_not_equal key_1, key_2
          end
        end

        describe "preventing duplicate organization names" do
          let(:organization) { organizations(:minimum_organization1) }
          describe "when the name is unchanged" do
            it "does not lock" do
              organization.expects(:lock_create_and_update).never
              organization.notes = "different notes"
              organization.save!
            end
          end

          describe "when the name is changed" do
            it "locks on the new name and not the old name" do
              organization.expects(:single_execution_key).with(account.id, "minimum organization").never
              organization.expects(:single_execution_key).with(account.id, "different name").times(4)
              organization.name = "different name"
              organization.save!
            end
          end

          describe 'when the lock is held by another thread' do
            it "rejects additional update attempts" do
              organization_different_name.send(:lock_create_and_update)
              assert_raise Organization::WriteInProgressError do
                # update existing record to have a locked name
                organization.name = "different name"
                organization.save!
              end
              organization_different_name.send(:unlock_create_and_update)
            end

            it "rejects additional create attempts" do
              organization.send(:lock_create_and_update)
              assert_raise Organization::WriteInProgressError do
                # save new record with the same name as an existing record
                organization_different_name.name = "minimum organization"
                organization_different_name.save!
              end
              organization.send(:unlock_create_and_update)
            end
          end

          describe "soft-deleted organizations" do
            before { organization.destroy }

            it "allows creation of organizations with the original deleted name" do
              recreated_organization = account.organizations.create!(
                name: "minimum organization",
                notes: "notes",
                domain_names: "TEST.com",
                details: "details",
                is_shared_comments: true
              )
              # assert no error raised
              assert recreated_organization.valid?
            end

            it "allows soft-deleting organizations when an already delete-encoded name exists" do
              recreated_organization = account.organizations.create!(
                name: "minimum organization",
                notes: "notes",
                domain_names: "TEST.com",
                details: "details",
                is_shared_comments: true
              )
              # assert no error raised
              recreated_organization.destroy
              assert recreated_organization.valid?
            end

            it "does not allow creation of organizations with the encoded name of the soft-deleted record" do
              assert_raise ActiveRecord::RecordInvalid do
                account.organizations.create!(
                  name: organization.name,
                  notes: "notes",
                  domain_names: "TEST.com",
                  details: "details",
                  is_shared_comments: true
                )
              end
            end
          end
        end
      end
    end

    describe "on text fields other than name" do
      before { assert organization.valid? }

      it "strips trailing whitespace from the notes" do
        assert_equal "test", organization.notes
      end

      it "strips trailing whitespace from the details" do
        assert_equal "test", organization.details
      end

      it "overrides is_shared_comments based on is_shared?" do
        refute organization.is_shared_comments?
      end
    end

    it "is valid if the name has no pipe characters in it" do
      organization.name = "Valid Organization"
      assert organization.valid?
    end

    it "is invalid if the name has pipe characters in it" do
      organization.name = "Invalid | Organization"
      refute organization.valid?
      assert_equal ["must not contain the '|' character."], organization.errors[:name]
    end

    it "is invalid if the name has pipe characters after a newline" do
      organization.name = "Invalid \n line breaks \n | Organization"
      refute organization.valid?
      assert_equal ["must not contain the '|' character."], organization.errors[:name]
    end

    describe "column length validations" do
      it "validates length of name" do
        organization.name = "a" * 256
        refute_valid organization
        assert_includes organization.errors.full_messages, "Name:  is too long (maximum is 255 characters after encoding)"
      end

      it "validates length of details" do
        organization.details = "a" * 65_536
        refute_valid organization
        assert_includes organization.errors.full_messages, "Details is too long (maximum is 65535 characters after encoding)"
      end

      it "validates length of notes" do
        organization.notes = "a" * 65_536
        refute_valid organization
        assert_includes organization.errors.full_messages, "Notes is too long (maximum is 65535 characters after encoding)"
      end

      it "validates length of external_id" do
        organization.external_id = "a" * 256
        refute_valid organization
        assert_includes organization.errors.full_messages, "External is too long (maximum is 255 characters after encoding)"
      end
    end
  end

  describe ".with_name_like" do
    it "find organization like" do
      organization.name = "New organization name"
      organization.save
      assert_includes Organization.with_name_like("new"), organization
    end

    it "find organization with escape character" do
      organization.name = "Orga%nization"
      organization.save
      assert_includes Organization.with_name_like("orga%"), organization
    end
  end

  describe "#users" do
    it "only include active users" do
      inactive_user = users(:inactive_user)
      organization  = organizations(:minimum_organization1)
      inactive_user.organization_memberships.create!(organization: organization)

      refute_includes organization.reload.users, inactive_user
    end
  end

  describe "duplicates" do
    it "does not allow duplicate organizations" do
      organization = accounts(:minimum).organizations.new(name: "Original Organization ")
      dupe_organization = accounts(:minimum).organizations.new(name: "Original Organization ")

      organization.save!
      refute dupe_organization.save
      assert_match(/Name:  #{organization.name} has already been taken/, dupe_organization.errors.full_messages.join)
    end
  end

  describe "with an external id" do
    before do
      Organization.any_instance.stubs(:external_id?).returns(true)
      Organization.any_instance.stubs(:lock_create_and_update?).returns(false)
    end

    should validate_uniqueness_of(:external_id).scoped_to(:account_id)
  end

  describe ".find_by_external_id" do
    subject { organizations(:minimum_organization1) }
    before { subject.update_attribute(:external_id, "ABC  ") }

    it "returns nil if looking for 'ABC'" do
      assert_nil Organization.find_by_external_id("ABC")
    end

    it "does not return nil when looking for 'ABC  '" do
      assert_equal subject, Organization.find_by_external_id("ABC  ")
    end
  end

  describe "#domain_names" do
    def org_with_domains_and_emails(domain_names = nil)
      account.organizations.create!(
        name: "the-sound-of-philadelphia",
        domain_names: domain_names
      )
    end

    describe "when the organization has no domains or emails" do
      let(:organization) { org_with_domains_and_emails }

      it "returns an empty array" do
        assert_equal [], organization.domain_names
      end
    end

    describe "when the organization has a single domain" do
      let(:organization) { org_with_domains_and_emails("mfsb.soul") }

      it "returns an array contain the domain's value" do
        assert_equal ["mfsb.soul"], organization.domain_names
      end
    end

    describe "when the organization has multiple domains" do
      let(:organization) do
        org_with_domains_and_emails("mfsb.soul bluenotes.soul")
      end

      it "returns an alphabetized array of the domains' values" do
        assert_equal ["bluenotes.soul", "mfsb.soul"], organization.domain_names
      end
    end

    describe "when the organization has a single email" do
      let(:organization) { org_with_domains_and_emails("mother@mfsb.soul") }

      it "returns an array contain the email's value" do
        assert_equal ["mother@mfsb.soul"], organization.domain_names
      end
    end

    describe "when the organization has multiple emails" do
      let(:organization) do
        org_with_domains_and_emails("teddy@bluenotes.soul mother@mfsb.soul")
      end

      it "returns an alphabetized array of the emails' values" do
        assert_equal ["mother@mfsb.soul", "teddy@bluenotes.soul"], organization.domain_names
      end
    end

    describe "when the organization has multiple domains and emails" do
      let(:organization) do
        org_with_domains_and_emails(
          "teddy@bluenotes.soul mfsb.soul bluenotes.soul mother@mfsb.soul"
        )
      end

      it "returns an array with an alphabetized list of domains' values followed by an alphabetized list of the emails' values" do
        assert_equal ["bluenotes.soul", "mfsb.soul", "mother@mfsb.soul", "teddy@bluenotes.soul"], organization.domain_names
      end
    end

    describe "when the organization has unpersisted in-flight domains and emails to be added or removed" do
      let(:organization) do
        org_with_domains_and_emails(
          "bluenotes.soul mfsb.soul mother@mfsb.soul teddy@bluenotes.soul"
        )
      end

      before do
        organization.domain_names = "bluenotes.soul ojays.soul harold@bluenotes.soul mother@mfsb.soul"
      end

      it "returns values for the domains and emails to be persisted and omits the domains and emails marked for removal" do
        assert_equal 1, organization.instance_variable_get(:@domains_to_remove).size
        assert_equal 1, organization.instance_variable_get(:@emails_to_remove).size
        assert_equal 1, organization.send(:in_flight_organization_domains).count(&:new_record?)
        assert_equal 1, organization.send(:in_flight_organization_emails).count(&:new_record?)

        assert_equal ["bluenotes.soul", "ojays.soul", "harold@bluenotes.soul", "mother@mfsb.soul"], organization.domain_names
      end
    end

    describe "when the organization is deleted and has soft-deleted organization_domains and organization_emails" do
      let(:organization) do
        org_with_domains_and_emails(
          "mfsb.soul teddy@bluenotes.soul"
        )
      end

      before do
        organization.destroy
        organization.reload

        # Confirm that test setup has created soft-deleted organization_domains
        org_domains = OrganizationDomain.unscoped.where(organization_id: organization.id)
        assert_equal 1, org_domains.count
        assert org_domains.all?(&:deleted?)

        # Confirm that test setup has created soft-deleted organization_emails
        org_emails = OrganizationEmail.unscoped.where(organization_id: organization.id)
        assert_equal 1, org_emails.count
        assert org_emails.all?(&:deleted?)
      end

      it "returns values for the soft-deleted organization_domains and organization_emails" do
        assert_equal ["mfsb.soul", "teddy@bluenotes.soul"], organization.domain_names
      end
    end
  end

  describe "#domain_names=" do
    describe "when setting organization_domains and organization_emails assocations" do
      def create_new_org(domain_names = nil)
        options = { name: "new-org" }
        options[:domain_names] = domain_names if domain_names

        account.organizations.create(options)
      end

      # Does not trigger new SQL query
      def assert_has_domains_with_values(organization, domain_values)
        assert_equal domain_values.sort, organization.send(:organization_domains).map(&:value).sort
      end

      # Does not trigger new SQL query
      def assert_has_emails_with_values(organization, email_values)
        assert_equal email_values.sort, organization.send(:organization_emails).map(&:value).sort
      end

      # Triggers new SQL query
      def assert_has_persisted_domains_with_values(organization, domain_values)
        assert_equal domain_values.sort, organization.send(:organization_domains).pluck(:value).sort
      end

      # Triggers new SQL query
      def assert_has_persisted_emails_with_values(organization, email_values)
        assert_equal email_values.sort, organization.send(:organization_emails).pluck(:value).sort
      end

      # Does not trigger new SQL query
      def assert_has_no_domains(organization)
        assert_empty organization.send(:organization_domains)
      end

      # Does not trigger new SQL query
      def assert_has_no_emails(organization)
        assert_empty organization.send(:organization_emails)
      end

      # Triggers new SQL query
      def assert_has_no_persisted_domains(organization)
        assert_empty organization.send(:organization_domains).reload
      end

      # Triggers new SQL query
      def assert_has_no_persisted_emails(organization)
        assert_empty organization.send(:organization_emails).reload
      end

      def assert_has_no_domains_with_values(organization, values)
        # Use `unscoped` here to confirm that the record has been fully destroyed, not simply soft deleted
        assert_empty OrganizationDomain.unscoped.where(account: organization.account, organization: organization, value: values)
      end

      def assert_has_no_emails_with_values(organization, values)
        # Use `unscoped` here to confirm that the record has been fully destroyed, not simply soft deleted
        assert_empty OrganizationEmail.unscoped.where(account: organization.account, organization: organization, value: values)
      end

      def assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization)
        assert_nil organization.instance_variable_get(:@domains_to_remove)
        assert_nil organization.instance_variable_get(:@emails_to_remove)
      end

      describe "when creating a new organization" do
        describe "when passed no domain names or email addresses" do
          let(:organization) { create_new_org }

          it "does not create any organization_domains or organization_emails associations" do
            assert_has_no_persisted_domains organization
            assert_has_no_persisted_emails organization
          end
        end

        describe "when passed valid domain names" do
          let(:organization) { create_new_org("domain.com other-domain.com") }

          it "creates an organization_domains association for each domain name" do
            assert_has_persisted_domains_with_values organization, ["domain.com", "other-domain.com"]
            assert_has_no_persisted_emails organization
          end
        end

        describe "when passed valid email addresses" do
          let(:organization) { create_new_org("user@domain.com user@other-domain.com") }

          it "creates an organization_emails association for each email address" do
            assert_has_no_persisted_domains organization
            assert_has_persisted_emails_with_values organization, ["user@domain.com", "user@other-domain.com"]
          end
        end

        describe "when passed both valid domain names and valid email addresses" do
          let(:organization) { create_new_org("domain.com other-domain.com user@domain.com user@other-domain.com") }

          it "creates an organization_domains association for each domain name and an organization_emails association for each email address" do
            assert_has_persisted_domains_with_values organization, ["domain.com", "other-domain.com"]
            assert_has_persisted_emails_with_values organization, ["user@domain.com", "user@other-domain.com"]
          end
        end

        describe "when passed at least one value that is neither valid domain name nor a valid email address" do
          let(:invalid_value) { "some_invalid_value" }
          let(:organization) { create_new_org("domain.com user@domain.com #{invalid_value}") }

          before { Organization.any_instance.stubs(:lock_create_and_update?).returns(false) }

          it "adds an error to the organization" do
            refute_valid organization
            assert_equal ["'#{invalid_value}' is not properly formatted"], organization.errors[:domain_name]
          end

          it "does not save any record for valid domain names or email addresses on attempted save" do
            assert_has_domains_with_values organization, ["domain.com"]
            assert_has_emails_with_values organization, ["user@domain.com"]
            assert_has_no_persisted_domains organization
            assert_has_no_persisted_emails organization
          end
        end
      end

      describe "when updating an existing organization" do
        let(:existing_domain_value1) { "existing-domain1.com" }
        let(:existing_domain_value2) { "existing-domain2.com" }
        let(:existing_domain_values) { [existing_domain_value1, existing_domain_value2] }
        let(:new_domain_value1) { "new-domain1.com" }
        let(:new_domain_value2) { "new-domain2.com" }
        let(:new_domain_values) { [new_domain_value1, new_domain_value2] }

        let(:existing_email_value1) { "user@existing-email1.com" }
        let(:existing_email_value2) { "user@existing-email2.com" }
        let(:existing_email_values) { [existing_email_value1, existing_email_value2] }
        let(:new_email_value1) { "user@new-email1.com" }
        let(:new_email_value2) { "user@new-email2.com" }
        let(:new_email_values) { [new_email_value1, new_email_value2] }

        let(:invalid_value) { "some-invalid-value" }
        let(:invalid_values) { [invalid_value] }

        let(:existing_domain_and_email_values) { existing_domain_values + existing_email_values }
        let(:new_domain_and_email_values) { new_domain_values + new_email_values }

        describe "when the organization has no existing organization_domains or organization_emails" do
          let(:organization) { create_new_org }

          describe "when passed valid domain names" do
            before do
              organization.domain_names = new_domain_values
              organization.save
            end

            it "creates an organization_domains association for each domain name" do
              assert_has_persisted_domains_with_values organization, new_domain_values
              assert_has_no_persisted_emails organization
            end
          end

          describe "when passed valid email addresses" do
            before do
              organization.domain_names = new_email_values
              organization.save
            end

            it "creates an organization_emails association for each email address" do
              assert_has_no_persisted_domains organization
              assert_has_persisted_emails_with_values organization, new_email_values
            end
          end

          describe "when passed both valid domain names and valid email addresses" do
            before do
              organization.domain_names = new_domain_and_email_values
              organization.save
            end

            it "creates an organization_domains association for each domain name and an organization_emails association for each email address" do
              assert_has_persisted_domains_with_values organization, new_domain_values
              assert_has_persisted_emails_with_values organization, new_email_values
            end
          end

          describe "when passed at least one value that is neither valid domain name nor a valid email address" do
            let(:organization) { create_new_org }

            before do
              organization.domain_names = new_domain_values + new_email_values + invalid_values
              organization.save
            end

            it "adds an error to the organization" do
              refute_valid organization
              assert_equal ["'#{invalid_value}' is not properly formatted"], organization.errors[:domain_name]
            end

            it "creates objects for any valid domain names and email addresses in memory" do
              assert_has_domains_with_values organization, new_domain_values
              assert_has_emails_with_values organization, new_email_values
            end

            it "does not save any record for valid domain names or email addresses on attempted save" do
              assert_has_no_persisted_domains organization
              assert_has_no_persisted_emails organization
            end
          end
        end

        describe "when the organization has existing organization_domains" do
          let(:organization) { create_new_org(existing_domain_values) }

          before do
            organization.domain_names = updated_domain_values
            organization.save
          end

          describe "when passed a list of domain names that includes all of the existing organization_domains' domain names" do
            describe "when passed a list of domain names that includes new domain names" do
              let(:updated_domain_values) { existing_domain_values + new_domain_values }

              it "keeps the existing organization_domains associations and creates a new association for each new domain name" do
                assert_has_persisted_domains_with_values organization, updated_domain_values
              end
            end

            describe "when passed a list of domain names with no new domain names" do
              let(:updated_domain_values) { existing_domain_values }

              it "keeps the existing organizations_domains associations" do
                assert_has_persisted_domains_with_values organization, updated_domain_values
              end
            end
          end

          describe "when passed a list of domain names that includes some, but not all, of the existing organization_domains' domain names" do
            describe "when passed a list of domain names that includes new domain names" do
              let(:updated_domain_values) { [existing_domain_value1, new_domain_value1, new_domain_value2] }

              it "adds organization_domains associations for the new domain names and keeps only the existing organization_domains associations whose domain values were passed in" do
                assert_has_persisted_domains_with_values organization, updated_domain_values
              end

              it "hard deletes existing organization_domains whose domain values were not explicitly passed in" do
                assert_has_no_domains_with_values organization, existing_domain_value2
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end

            describe "when passed a list of domain names with no new domain names" do
              let(:updated_domain_values) { [existing_domain_value1] }

              it "keeps only the existing organization_domains associations whose domain values were passed in" do
                assert_has_persisted_domains_with_values organization, updated_domain_values
              end

              it "hard deletes existing organization_domains whose domain values were not explicitly passed in" do
                assert_has_no_domains_with_values organization, existing_domain_value2
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end
          end

          describe "when passed a list of domain names that includes none of the existing organization_domains' domain names" do
            describe "when passed a list of domain names that includes new domain names" do
              let(:updated_domain_values) { new_domain_values }

              it "adds organization_domains associations for the new domain names" do
                assert_has_persisted_domains_with_values organization, updated_domain_values
              end

              it "hard deletes the existing organization_domains associations" do
                assert_has_no_domains_with_values organization, existing_domain_values
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end

            describe "when passed a list of domain names with no new domain names" do
              let(:updated_domain_values) { [] }

              it "does not add any new organization_domains associations" do
                assert_has_no_persisted_domains organization
              end

              it "hard deletes the existing organization_domains associations" do
                assert_has_no_domains_with_values organization, existing_domain_values
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end
          end

          describe "when passed at least one value that is neither a valid domain name nor a valid email address" do
            let(:updated_domain_values) { new_domain_values + invalid_values }

            it "adds an error to the organization" do
              refute_valid organization
              assert_equal ["'#{invalid_value}' is not properly formatted"], organization.errors[:domain_name]
            end

            it "creates objects for any valid domain names in memory" do
              assert_has_domains_with_values organization, existing_domain_values + new_domain_values
              assert_has_no_emails organization
            end

            it "does not save records for new organization_domains and does not delete the previously existing organization_domains" do
              assert_has_persisted_domains_with_values organization, existing_domain_values
              assert_has_no_persisted_emails organization
            end
          end
        end

        describe "when the organization has existing organization_emails" do
          let(:organization) { create_new_org(existing_email_values) }

          before do
            organization.domain_names = updated_email_values
            organization.save
          end

          describe "when passed a list of email addresses that includes all of the existing organization_emails' email addresses" do
            describe "when passed a list of email addresses that includes new email addresses" do
              let(:updated_email_values) { existing_email_values + new_email_values }

              it "keeps the existing organization_emails associations and creates a new association for each new email address" do
                assert_has_persisted_emails_with_values organization, updated_email_values
              end
            end

            describe "when passed a list of email addresses with no new email addresses" do
              let(:updated_email_values) { existing_email_values }

              it "keeps the existing organizations_emails associations" do
                assert_has_persisted_emails_with_values organization, updated_email_values
              end
            end
          end

          describe "when passed a list of email addresses that includes some, but not all, of the existing organization_emails' email addresses" do
            describe "when passed a list of email addresses that includes new email addresses" do
              let(:updated_email_values) { [existing_email_value1] + new_email_values }

              it "adds organization_emails associations for the new email addresses and keeps only the existing organization_emails associations whose email values were passed in" do
                assert_has_persisted_emails_with_values organization, updated_email_values
              end

              it "hard deletes existing organization_emails whose email values were not explicitly passed in" do
                assert_has_no_emails_with_values organization, existing_email_value2
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end

            describe "when passed a list of email addresses with no new email addresses" do
              let(:updated_email_values) { [existing_email_value1] }

              it "keeps only the existing organization_emails associations whose email values were passed in" do
                assert_has_persisted_emails_with_values organization, updated_email_values
              end

              it "hard deletes existing organization_emails whose email values were not explicitly passed in" do
                assert_has_no_emails_with_values organization, existing_email_value2
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end
          end

          describe "when passed a list of email addresses that includes none of the existing organization_emails' email addresses" do
            describe "when passed a list of email addresses that includes new email addresses" do
              let(:updated_email_values) { new_email_values }

              it "adds organization_domains associations for the new email addresses" do
                assert_has_persisted_emails_with_values organization, updated_email_values
              end

              it "hard deletes the existing organization_emails associations" do
                assert_has_no_emails_with_values organization, existing_email_values
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end

            describe "when passed a list of email addresses with no new email addresses" do
              let(:updated_email_values) { [] }

              it "does not add any new organization_emails associations" do
                assert_has_no_persisted_emails organization
              end

              it "hard deletes the existing organization_emails associations" do
                assert_has_no_emails_with_values organization, existing_email_values
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end
          end

          describe "when passed at least one value that is neither a valid domain name nor a valid email address" do
            let(:updated_email_values) { new_email_values + invalid_values }

            it "adds an error to the organization" do
              refute_valid organization
              assert_equal ["'#{invalid_value}' is not properly formatted"], organization.errors[:domain_name]
            end

            it "creates objects for any valid email addresses in memory" do
              assert_has_no_domains organization
              assert_has_emails_with_values organization, existing_email_values + new_email_values
            end

            it "does not save records for new organization_emails and does not delete the previously existing organization_emails" do
              assert_has_no_persisted_domains organization
              assert_has_persisted_emails_with_values organization, existing_email_values
            end
          end
        end

        describe "when the organization has existing organization_domains and organization_emails" do
          let(:organization) { create_new_org(existing_domain_values + existing_email_values) }

          before do
            organization.domain_names = updated_domain_and_email_values
            organization.save
          end

          describe "when passed a list of domain names and email addresses that includes all of the existing associations' values" do
            describe "when passed a list of domain names and email addresses that includes new values" do
              let(:updated_domain_and_email_values) { existing_domain_values + new_domain_values + existing_email_values + new_email_values }

              it "keeps the existing associations and creates a new association for each new value" do
                assert_has_persisted_domains_with_values organization, existing_domain_values + new_domain_values
                assert_has_persisted_emails_with_values organization, existing_email_values + new_email_values
              end
            end

            describe "when passed a list of domain names and email addresses with no new values" do
              let(:updated_domain_and_email_values) { existing_domain_values + existing_email_values }

              it "keeps the existing associations" do
                assert_has_persisted_domains_with_values organization, existing_domain_values
                assert_has_persisted_emails_with_values organization, existing_email_values
              end
            end
          end

          describe "when passed a list of domain names and email addresses that includes some, but not all, of the existing associations' values" do
            describe "when passed a list of domain names and email addresses that includes new values" do
              let(:updated_domain_and_email_values) do
                [
                  existing_domain_value1,
                  new_domain_value1,
                  new_domain_value2,
                  existing_email_value1,
                  new_email_value1,
                  new_email_value2
                ]
              end

              it "adds associations for the new values and keeps only the existing associations whose values were passed in" do
                assert_has_persisted_domains_with_values organization, [existing_domain_value1] + new_domain_values
                assert_has_persisted_emails_with_values organization, [existing_email_value1] + new_email_values
              end

              it "hard deletes existing assocations whose values were not explicitly passed in" do
                assert_has_no_domains_with_values organization, existing_domain_value2
                assert_has_no_emails_with_values organization, existing_email_value2
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end

            describe "when passed a list of domain names and email addresses with no new values" do
              let(:updated_domain_and_email_values) { [existing_domain_value1, existing_email_value1] }

              it "keeps only the existing associations whose values were passed in" do
                assert_has_persisted_domains_with_values organization, [existing_domain_value1]
                assert_has_persisted_emails_with_values organization, [existing_email_value1]
              end

              it "hard deletes existing associations whose values were not explicitly passed in" do
                assert_has_no_domains_with_values organization, existing_domain_value2
                assert_has_no_emails_with_values organization, existing_email_value2
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end
          end

          describe "when passed a list of domain names and email addresses that includes none of the existing associations' values" do
            describe "when passed a list of domain names and email addresses that includes new values" do
              let(:updated_domain_and_email_values) { new_domain_values + new_email_values }

              it "adds associations for the new values" do
                assert_has_persisted_domains_with_values organization, new_domain_values
                assert_has_persisted_emails_with_values organization, new_email_values
              end

              it "hard deletes the existing associations" do
                assert_has_no_domains_with_values organization, existing_domain_values
                assert_has_no_emails_with_values organization, existing_email_values
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end

            describe "when passed a list of domain names and email addresses with no values" do
              let(:updated_domain_and_email_values) { [] }

              it "does not add any new organization_emails associations" do
                assert_has_no_persisted_domains organization
                assert_has_no_persisted_emails organization
              end

              it "hard deletes the existing associations" do
                assert_has_no_domains_with_values organization, existing_domain_values
                assert_has_no_emails_with_values organization, existing_email_values
              end

              it { assert_domains_and_emails_to_remove_instance_variables_cleared_after_save(organization) }
            end
          end

          describe "when passed at least one value that is neither a valid domain name nor a valid email address" do
            let(:updated_domain_and_email_values) { new_domain_values + new_email_values + invalid_values }

            it "adds an error to the organization" do
              refute_valid organization
              assert_equal ["'#{invalid_value}' is not properly formatted"], organization.errors[:domain_name]
            end

            it "creates objects for any valid domain names and email addresses in memory" do
              assert_has_domains_with_values organization, existing_domain_values + new_domain_values
              assert_has_emails_with_values organization, existing_email_values + new_email_values
            end

            it "does not save records for new associations and does not delete the previously existing associations" do
              assert_has_persisted_domains_with_values organization, existing_domain_values
              assert_has_persisted_emails_with_values organization, existing_email_values
            end
          end
        end
      end
    end
  end

  describe "#parse_new_domain_names" do
    def parse_new_domain_names
      organization.send(:parse_new_domain_names, new_domain_names)
    end

    describe "when passed an array" do
      describe "when the array is empty" do
        let(:new_domain_names) { [] }

        it "returns an empty array" do
          assert_empty parse_new_domain_names
        end
      end

      describe "when the array's elements are all strings" do
        describe "when the strings are all lowercase and contain no leading or trailing whitespace" do
          let(:new_domain_names) { ["test.com", "user@test.com"] }

          it "returns the original array" do
            assert_equal new_domain_names, parse_new_domain_names
          end
        end

        describe "when some of the strings have uppercase characters" do
          let(:new_domain_names) { ["TeSt.CoM", "USER@test.com"] }

          it "downcases all of the uppercase characters" do
            assert_equal ["test.com", "user@test.com"], parse_new_domain_names
          end
        end

        describe "when some of the strings have leading and/or trailing whitespace" do
          let(:new_domain_names) { ["  test.com  \n\n", "user@test.com "] }

          it "strips the the trailing whitespace" do
            assert_equal ["test.com", "user@test.com"], parse_new_domain_names
          end
        end

        describe "when the array contains a string with whitespace in between other characters" do
          let(:new_domain_names) { ["test.com user@test.com"] }

          it "splits the string into substrings on the whitespaces" do
            assert_equal ["test.com", "user@test.com"], parse_new_domain_names
          end
        end

        describe "when the array contains a string with whitespace in between other characters plus other strings" do
          let(:new_domain_names) { ["test.com user@test.com", "test.com", "test.com", "other.user@test.com"] }

          it "splits the string into substrings on the whitespaces and adds the values from other strings, removing any duplicates" do
            assert_equal ["test.com", "user@test.com", "other.user@test.com"], parse_new_domain_names
          end
        end
      end

      describe "when the array contains blank non-string objects" do
        let(:new_domain_names) { ["test.com", [], {}, false, Set.new, nil, "user@test.com"] }

        it "removes the blank objects from the array" do
          assert_equal ["test.com", "user@test.com"], parse_new_domain_names
        end
      end
    end

    describe "when passed a string" do
      describe "when the string is empty" do
        ["", " ", "\n"].each do |string|
          let(:new_domain_names) { string }

          it "returns an empty array" do
            assert_equal [], parse_new_domain_names
          end
        end
      end

      describe "when the string contains no commas, semicolons, or spaces" do
        let(:new_domain_names) { "test.com" }

        it "returns an array containing the original string" do
          assert_equal ["test.com"], parse_new_domain_names
        end
      end

      describe "when the string contains commas, semicolons, and/or spaces" do
        let(:new_domain_names) { " \n  test.com;;;,  ,; user@test.com;" }

        it "splits the string on the commas, semicolons, and/or spaces" do
          assert_equal ["test.com", "user@test.com"], parse_new_domain_names
        end
      end

      describe "when the string has uppercase characters" do
        let(:new_domain_names) { "test.com USER@TEST.com" }

        it "downcases the uppercase characters and splits the string" do
          assert_equal ["test.com", "user@test.com"], parse_new_domain_names
        end
      end
    end

    describe "when passed nil" do
      let(:new_domain_names) { nil }

      it "returns an empty array" do
        assert_equal [], parse_new_domain_names
      end
    end

    describe "when passed an instance of something other than an array, a string, or nil" do
      let(:new_domain_names) { {} }

      it "raises ArgumentError" do
        assert_raise ArgumentError do
          parse_new_domain_names
        end
      end
    end
  end

  describe "#to_liquid" do
    it "calls #name" do
      organization.expects(:name)
      organization.to_liquid
    end
  end

  describe "#to_s" do
    it "calls #name" do
      organization.expects(:name)
      organization.to_s
    end
  end

  describe "tags" do
    # The exact same test is in user_test.rb
    let(:organization) { organizations(:minimum_organization1) }

    describe "with user_and_organization_fields feature" do
      before do
        Account.any_instance.stubs(:has_user_and_organization_fields?).returns(true)
      end

      describe "and with user_and_organization_tags feature" do
        before do
          Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
        end

        it "has tags" do
          assert_equal %w[beta.test premium], organization.tags
        end

        it "sets tags" do
          organization.tags = 'hello there'
          organization.save!

          assert_equal %w[hello there], organization.tags
        end

        it "adds tags" do
          organization.additional_tags = 'added'
          organization.save!

          assert_equal %w[beta.test premium added], organization.tags
        end

        it "removes tags" do
          organization.remove_tags = 'premium'
          organization.save!

          assert_equal %w[beta.test], organization.tags
        end

        describe "and custom field values updated" do
          before do
            organization.custom_field_values.update_from_hash(
              org_checkbox1: true
            )
            organization.save!
          end

          it "updates tags" do
            assert_equal %w[beta.test premium org_checkbox1], organization.tags
          end
        end
      end

      describe "and without user_and_organization_tags feature" do
        let(:tags) { organization.taggings.map(&:tag_name) }

        before do
          Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
        end

        it "does not have tags" do
          assert_equal %w[beta.test premium], tags
          assert_equal [], organization.tags
        end

        it "does not set tags" do
          organization.tags = 'hello there'
          organization.save!

          assert_equal %w[beta.test premium], tags
          assert_equal [], organization.tags
        end

        it "does not add tags" do
          organization.additional_tags = 'added'
          organization.save!

          assert_equal %w[beta.test premium], tags
          assert_equal [], organization.tags
        end

        it "does not remove tags" do
          organization.remove_tags = 'premium'
          organization.save!

          assert_equal %w[beta.test premium], tags
          assert_equal [], organization.tags
        end

        describe "and custom field values updated" do
          before do
            organization.custom_field_values.update_from_hash(
              org_checkbox1: true
            )
            organization.save!
          end

          it "does not update tags" do
            assert_equal %w[beta.test premium], tags
            assert_equal [], organization.tags
          end
        end
      end
    end
  end

  describe "when queueing OrganizationReassignJobs after_save" do
    describe "when creating a new organization" do
      describe_with_arturo_disabled :org_reassign_domain_mapping_kill_switch do
        describe "when not passed any domain_names values" do
          it "does not enqueue an OrganizationReassignJob" do
            OrganizationReassignJob.expects(:enqueue).never

            account.organizations.create!(name: "new-org")
          end
        end

        describe "when passed domain_names values that are valid OrganizationDomain values" do
          it "enqueues an OrganizationReassignJob" do
            OrganizationReassignJob.expects(:enqueue).with(
              account.id,
              kind_of(Integer), # We can't know what the organization.id will be before we create the org!
              ["new.com"],
              []
            ).once

            account.organizations.create!(
              name: "new-org",
              domain_names: ["new.com"]
            )
          end
        end

        describe "when passed domain_names values that are valid OrganizationEmail values" do
          it "does not enqueue an OrganizationReassignJob" do
            OrganizationReassignJob.expects(:enqueue).never

            account.organizations.create!(
              name: "new-org",
              domain_names: ["user@email.com"]
            )
          end
        end
      end

      describe_with_arturo_enabled :org_reassign_domain_mapping_kill_switch do
        it "does not enqueue an OrganizationReassignJob, even if the organization is passed valid OrganizationDomain values" do
          OrganizationReassignJob.expects(:enqueue).never

          account.organizations.create!(
            name: "new-org",
            domain_names: ["new.com"]
          )
        end
      end
    end

    describe "when updating an existing organization" do
      before do
        account.organizations.create!(
          name: "existing-org",
          domain_names: [
            "old-domain.com",
            "old.user@email.com",
            "common-domain.com",
            "common.user@email.com"
          ]
        )
      end

      let(:organization) do
        account.organizations.find_by_name("existing-org")
      end

      describe_with_arturo_disabled :org_reassign_domain_mapping_kill_switch do
        describe "when not passed any domain_names values" do
          it "does not enqueue an OrganizationReassignJob" do
            OrganizationReassignJob.expects(:enqueue).never

            organization.name = "new-name-same-domains"
            organization.save!
          end
        end

        describe "when passed all of the existing domain_names values" do
          it "does not enqueue an OrganizationReassignJob" do
            OrganizationReassignJob.expects(:enqueue).never

            organization.name = "new-name-same-domains"
            organization.domain_names = organization.domain_names
            organization.save!
          end
        end

        describe "when domain_names values that are valid OrganizationDomain values change" do
          it "enqueues an OrganizationReassignJob if the domain values change" do
            OrganizationReassignJob.expects(:enqueue).with(
              organization.account_id,
              organization.id,
              ["new-domain.com"],
              ["old-domain.com"]
            ).once

            organization.domain_names = [
              "new-domain.com",
              "old.user@email.com",
              "common-domain.com",
              "common.user@email.com"
            ]
            organization.save!
          end
        end

        describe "when domain_names values that are valid OrganizationEmail values change" do
          it "does not enqueue an OrganizationReassignJob" do
            OrganizationReassignJob.expects(:enqueue).never

            organization.name = "new-name-same-domains"
            organization.domain_names = [
              "old-domain.com",
              "new.user@email.com",
              "common-domain.com",
              "common.user@email.com"
            ]
            organization.save!
          end
        end
      end

      describe_with_arturo_enabled :org_reassign_domain_mapping_kill_switch do
        it "does not enqueue an OrganizationReassignJob, even if domain_values that are valid OrganizationDomain values change" do
          OrganizationReassignJob.expects(:enqueue).never

          organization.domain_names = [
            "new-domain.com",
            "old.user@email.com",
            "common-domain.com",
            "common.user@email.com"
          ]
          organization.save!
        end
      end
    end
  end

  describe "#destroy" do
    let(:organization) { accounts(:minimum).organizations.create!(name: "test") }
    let(:long_org) { accounts(:minimum).organizations.create!(name: "a" * 255) }

    it "softs delete the record instead" do
      id = organization.id
      organization.destroy

      deleted_organization = Organization.with_deleted { Organization.find(id) }
      assert deleted_organization
      assert deleted_organization.deleted_at
    end

    it "enqueues an OrganizationUnsetJob" do
      Resque.expects(:enqueue).with(
        OrganizationUnsetJob, organization.account_id, organization.id, anything
      ).once

      organization.destroy
    end

    it "honors deleted_at in uniqueness checks" do
      organization.destroy

      assert accounts(:minimum).organizations.create!(name: "test")
    end

    it "does not bother us about deleted external_ids" do
      organization.update_attribute(:external_id, "bananas")
      organization.destroy

      assert accounts(:minimum).organizations.create!(name: "test", external_id: "bananas")
    end

    it "renames the organization on deletion" do
      name_before_delete = organization.name
      organization.destroy

      refute_equal name_before_delete, organization.name
      assert_match(/^\d+_deleted_#{name_before_delete}$/, organization.name)
    end

    it "trims long org names before renaming on deletion" do
      name_before_delete = long_org.name
      long_org.destroy

      assert_match(/^\d+_deleted_#{name_before_delete.slice(0, 215)}$/, long_org.name)
    end

    it "soft deletes all associations to organization domains" do
      org_domain1 = OrganizationDomain.create!(organization: organization, value: 'zendesk.com')
      org_domain2 = OrganizationDomain.create!(organization: organization, value: 'google.com')

      refute org_domain1.deleted?
      refute org_domain2.deleted?

      organization.destroy

      assert org_domain1.reload.deleted?
      assert org_domain2.reload.deleted?
    end

    it "allows soft undeleting associations to organization domains" do
      org_domain = OrganizationDomain.create!(organization: organization, value: 'zendesk.com')

      organization.destroy

      assert org_domain.reload.deleted?

      organization.soft_undelete!

      refute org_domain.reload.deleted?
    end

    it "soft deletes all associations to organization emails" do
      org_email1 = OrganizationEmail.create!(organization: organization, value: 'psamal@zendesk.com')
      org_email2 = OrganizationEmail.create!(organization: organization, value: 'pranthik@gmail.com')

      refute org_email1.deleted?
      refute org_email2.deleted?

      organization.destroy

      assert org_email1.reload.deleted?
      assert org_email2.reload.deleted?
    end

    it "allows soft undeleting associations to organization emails" do
      org_email = OrganizationEmail.create!(organization: organization, value: 'pranthik@gmail.com')

      organization.destroy

      assert org_email.reload.deleted?

      organization.soft_undelete!

      refute org_email.reload.deleted?
    end
  end

  describe '#add_domain_event' do
    let(:organization) { organizations(:minimum_organization1) }

    describe 'with a valid domain event' do
      let(:event) { OrganizationExternalIdChangedProtobufEncoder.new(organization).to_object }

      before do
        organization.external_id = 'ivnsdfnisdfg'
      end

      it 'adds the domain event to the organization' do
        organization.add_domain_event(event)
        assert_equal [event], organization.domain_events
      end
    end

    describe 'with nil' do
      let(:event) { nil }

      it 'does not add a domain event' do
        organization.add_domain_event(event)
        assert_equal [], organization.domain_events
      end
    end

    describe 'with some other object type' do
      let(:event) { OrganizationExternalIdChangedProtobufEncoder.new(organization) }

      it 'raises ArgumentError' do
        assert_raise ArgumentError do
          organization.add_domain_event(event)
        end
      end
    end
  end
end
