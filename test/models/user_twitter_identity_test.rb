require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe UserTwitterIdentity do
  fixtures :accounts, :users, :user_identities, :suspended_tickets, :channels_user_profiles

  before do
    @minimum_end_user = users(:minimum_end_user)
  end

  describe "validations" do
    describe "uniqueness" do
      before do
        @twitter_user_profile = channels_user_profiles(:minimum_twitter_user_profile_1)
        @twitter_user_profile.save!
        stub_request(:get, "https://api.twitter.com/1.1/users/show.json?screen_name=#{@twitter_user_profile.screen_name}").
          to_return(
            status: 200,
            body: read_test_file('twitter_users_show_110542029_1.1.json'),
            headers: { 'Content-Type' => 'application/json' }
          )
        @preexisting_user_identity = UserTwitterIdentity.create(
          account: @minimum_end_user.account,
          user: @minimum_end_user,
          screen_name: @twitter_user_profile.screen_name,
          value: '12345'
        )
      end

      it "is not valid when an identity with the same account and value already exists" do
        stub_request(:get, "https://api.twitter.com/1.1/users/show.json?screen_name=#{@twitter_user_profile.screen_name}").
          to_return(
            status: 200,
            body: read_test_file('twitter_users_show_110542029_1.1.json'),
            headers: {'Content-Type' => 'application/json'}
          )
        identity = UserTwitterIdentity.new(
          account: @preexisting_user_identity.account,
          user: @preexisting_user_identity.user,
          screen_name: @twitter_user_profile.screen_name,
          value: @preexisting_user_identity.value
        )
        refute identity.save
        assert_equal ["#{@preexisting_user_identity.value} is already being used by another user"], identity.errors[:value]
      end
    end

    it 'validates the twitter format is correct' do
      stub_request(:get, "https://api.twitter.com/1.1/users/show.json?screen_name=dr+henner").
        to_return(status: 400, headers: { 'Content-Type' => 'application/json' })
      identity = new_user_identity(screen_name: 'dr henner')
      identity.valid?
      assert_includes identity.errors[:value], "'dr henner' does not exist."
    end

    it 'validates that the twitter account exists' do
      stub_request(:get, "https://api.twitter.com/1.1/users/show.json?screen_name=nobody").
        to_return(status: 400, headers: { 'Content-Type' => 'application/json' })
      identity = new_user_identity(screen_name: 'nobody')
      identity.valid?
      assert_includes identity.errors[:value], "'nobody' does not exist."
    end
  end

  describe "creating a new user" do
    before do
      @user = accounts(:minimum).users.new(name: 'Mick')
    end

    it "creates and associate twitter handle" do
      @user.identities << UserTwitterIdentity.new(user: @user, value: '12345')
      @user.save!
    end

    it "validates the value of the twitter user identity" do
      identity = UserTwitterIdentity.new(user: @user, account: accounts(:minimum), value: '12345')
      assert identity.valid?
      identity.value = 'hello'
      refute identity.valid?
    end

    it "has the identity errors on the user" do
      @user.identities << UserTwitterIdentity.new(user: @user, value: @minimum_end_user.identities.twitter.first.value)
      assert(!@user.valid?)
      assert(@user.errors[:twitter].any?)
    end
  end

  describe "#profile" do
    before do
      @identity = @minimum_end_user.identities.twitter.first
    end

    before do
      @profile = channels_user_profiles(:minimum_twitter_user_profile_1)
      @identity.update_column(:value, @profile.external_id)
    end

    it 'returns the channels user profile' do
      assert_equal @profile, @identity.twitter_user_profile
    end
  end

  private

  def new_user_identity(options = {})
    options = { user: @minimum_end_user, account: @minimum_end_user.account }.merge(options)
    UserTwitterIdentity.new(options)
  end
end
