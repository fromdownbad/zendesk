require 'openssl'
require 'net/http'
require 'json'

# Skip SSL exchange overhead to let benchmark results be more correct
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

COUNT = 1_000

account_url = 'https://support.zd-dev.com'
api_user    = 'admin@zendesk.com'
api_token   = '123456'
verbose     = false

uri = URI("#{account_url}/api/v2/tickets/create_many")
req = Net::HTTP::Post.new(uri)

req.basic_auth api_user, api_token
req.content_type = 'application/json'

(COUNT / 100).times do |i|
  tickets = (1..100).map do |j|
    {
      subject: "Rails Upgrade Test Ticket #{i}-#{j}",
      requester: {
        name: "Test User #{i}-#{j}",
        email: "cday+rails4-#{i}-#{j}@zendesk.com"
      },
      comment: {
        body: "Fake ticket for rails upgrade test"
      },
      description: "Fake ticket for rails upgrade test",
      tags: (1..10).map { |t| "rails4-#{t}" }
    }
  end

  req.body = JSON.dump(tickets: tickets)

  res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
    http.request(req)
  end

  next if res.is_a?(Net::HTTPSuccess)
  if verbose
    puts res.code
    puts res.body
  end
  break
end

puts "Tickets have been created!"
