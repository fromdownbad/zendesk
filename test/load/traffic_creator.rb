require 'openssl'
require 'net/http'
require 'json'
require 'benchmark'

# Skip SSL exchange overhead to let benchmark results be more correct
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

account_url = 'https://support.zd-dev.com'
api_user    = 'admin@zendesk.com'
api_token   = '123456'
concurrency = 2
verbose     = false

endpoints = [
  {
    url: '/api/v2/users/%{user_id}.json?include=abilities,roles',
    var: 'user_id',
    weight: 6.72
  },
  {
    url: '/api/v2/tickets/%{ticket_id}.json?include=brands,permissions,users,groups,organizations,sharing_agreements,incident_counts,slas',
    var: 'ticket_id',
    weight: 4.57
  },
  {
    url: '/api/v2/users.json',
    var: nil,
    weight: 4.29
  },
  {
    url: '/api/v2/views/%{view_id}/execute.json?per_page=30&page=1&sort_by=id&sort_order=desc&group_by=+&include=via_id',
    var: 'view_id',
    weight: 3.11
  },
  {
    url: '/api/v2/users/me.json?include=abilities,roles',
    var: nil,
    weight: 2.87
  }
]

sampled_endpoints = endpoints.each_with_object([]) do |e, acc|
  (e[:weight] * 3).round.times { acc << [e[:url], e[:var]] }
end

# ticket ids to hit
ticket_id_range = 1..281707

# get view ids
uri = URI("#{account_url}/api/v2/views.json")
req = Net::HTTP::Get.new(uri)
req.basic_auth api_user, api_token
res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) { |http| http.request(req) }
view_ids = JSON.parse(res.body)['views'].map { |v| v['id'] }

# get some user ids
uri = URI("#{account_url}/api/v2/users.json")
req = Net::HTTP::Get.new(uri)
req.basic_auth api_user, api_token
res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) { |http| http.request(req) }
user_ids = JSON.parse(res.body)['users'].map { |v| v['id'] }

500.times do
  Array.new(concurrency).map do
    Thread.new do
      path, var = sampled_endpoints.sample

      full_path = case var
                  when 'user_id'
                    path % {user_id: user_ids.sample}
                  when 'ticket_id'
                    path % {ticket_id: rand(ticket_id_range)}
                  when 'view_id'
                    path % {view_id: view_ids.sample}
                  else
                    path
      end

      uri = URI(account_url + full_path)
      req = Net::HTTP::Get.new(uri)
      req.basic_auth api_user, api_token

      status = 999

      time = Benchmark.realtime do
        res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) { |http| http.request(req) } rescue nil
        status = res && res.code
      end

      if verbose
        puts "[#{status} #{'%3.4f' % time}s]  " + account_url + full_path
      end
    end
  end.map(&:join)
end
