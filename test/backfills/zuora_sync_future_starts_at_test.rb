require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe ZuoraSyncFutureStartsAt do
  include ZendeskBillingCore::ZuoraTestHelper
  fixtures :all

  def generate_zuora_subscriptions
    accounts(:minimum).subscription.build_zuora_subscription(
      zuora_account_id:       "ZD:#{SecureRandom.hex}#{Time.now.to_i}",
      plan_type:              Zendesk::Types::SubscriptionPlanType.Large,
      billing_cycle_type:     Zendesk::Types::BillingCycleType.Annually,
      max_agents:             191,
      pricing_model_revision: 2,
      is_dunning_enabled:     true,
      dunning_level:          ZendeskBillingCore::States::Dunning::State::OK.level,
      is_active:              true
    ).save!

    accounts(:billing).subscription.build_zuora_subscription(
      zuora_account_id:       "ZD:#{SecureRandom.hex}#{Time.now.to_i}",
      plan_type:              Zendesk::Types::SubscriptionPlanType.Large,
      billing_cycle_type:     Zendesk::Types::BillingCycleType.Annually,
      max_agents:             191,
      pricing_model_revision: 2,
      is_dunning_enabled:     true,
      dunning_level:          ZendeskBillingCore::States::Dunning::State::OK.level,
      is_active:              true,
      future_subscription_starts_at: Time.zone.now
    ).save!
  end

  before do
    Timecop.freeze
    ZendeskBillingCore::Zuora::RemoteSynchronizerClient.any_instance.stubs(:synchronize!)
    ZuoraSyncFutureStartsAt.any_instance.stubs(:sleep)
    ZuoraSyncFutureStartsAt.any_instance.stubs(:batch_size).returns(1)
    generate_zuora_subscriptions
  end

  it "makes no changes in dry run mode" do
    ZendeskBillingCore::Zuora::RemoteSynchronizerClient.any_instance.expects(:synchronize!).never
    ZuoraSyncFutureStartsAt.perform
  end

  it "#backfill calls #sync_account for every queried subscription" do
    subscription_to_ignore = accounts(:minimum).subscription.zuora_subscription
    subscription_to_fix = accounts(:billing).subscription.zuora_subscription
    ZuoraSyncFutureStartsAt.any_instance.expects(:sync_account).with(subscription_to_ignore).never
    ZuoraSyncFutureStartsAt.any_instance.expects(:sync_account).with(subscription_to_fix).once
    ZuoraSyncFutureStartsAt.perform(:dry_run => false)
  end

  it "#backfill sleeps after each batch" do
    zs = accounts(:minimum).subscription.zuora_subscription
    zs.future_subscription_starts_at = Time.zone.now
    zs.save!

    ZuoraSyncFutureStartsAt.any_instance.expects(:sleep).with(30).once
    ZuoraSyncFutureStartsAt.perform(:dry_run => false)
  end

  describe "max_accounts" do
    before do
      accounts(:payment).subscription.build_zuora_subscription(
        zuora_account_id:       "ZD:#{SecureRandom.hex}#{Time.now.to_i}",
        plan_type:              Zendesk::Types::SubscriptionPlanType.Large,
        billing_cycle_type:     Zendesk::Types::BillingCycleType.Annually,
        max_agents:             191,
        pricing_model_revision: 2,
        is_dunning_enabled:     true,
        dunning_level:          ZendeskBillingCore::States::Dunning::State::OK.level,
        is_active:              true,
        future_subscription_starts_at: Time.zone.now
      ).save!
    end

    it "if not specified, sync all query results" do
      ZuoraSyncFutureStartsAt.any_instance.expects(:sync_account).times(2)
      ZuoraSyncFutureStartsAt.perform(:dry_run => false)
    end

    it "if max_accounts is provided, stop after specified number of accounts are synced" do
      ZuoraSyncFutureStartsAt.any_instance.expects(:sync_account).once
      ZuoraSyncFutureStartsAt.perform(:dry_run => false, :max_accounts => 1)
    end
  end
end
