require_relative "../support/test_helper"

SingleCov.covered!

describe EndusersOrganizationCleanup do
  fixtures :organizations
  fixtures :organization_memberships
  fixtures :users
  let(:described_class) { EndusersOrganizationCleanup }

  describe "Cleaning enduser organization up" do
    let(:org1) { organizations(:minimum_organization1) }
    let(:user) { users(:minimum_end_user) }

    before do
      user.organization_memberships = []
      user.update_column(:organization_id, org1.id)
      user.save!
    end

    describe "and there were no errors" do
      it "sets organization_id to nil" do
        described_class.new(dry_run: false).perform
        assert user.reload.organization_id, nil
      end
    end

    describe "and there was an error when saving" do
      before do
        User.any_instance.expects(:save).returns(nil)
      end

      it "does not change organization_id" do
        described_class.new(dry_run: false).perform
        assert_equal user.reload.organization_id, org1.id
      end
    end
  end
end
