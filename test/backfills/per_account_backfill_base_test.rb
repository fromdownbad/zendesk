require_relative "../support/test_helper"

SingleCov.covered!

class TestPerAccountJob < PerAccountBackfillBase

  attr_accessor :accounts_backfilled

  def initialize(options = {})
    super
    @accounts_backfilled = []
    @account_id = nil
  end

  def backfill_account(account)
    @accounts_backfilled << account
  end

  def constrain_accounts(arel)
    return super unless @account_id
    arel.where(id: @account_id)
  end

  def summary
    'Test log summary'
  end

  def constrain_to_account(account_id)
    @account_id = account_id
  end
end

describe PerAccountBackfillBase do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
  end

  describe '#perform' do
    [true, false].each do |dry_run|
      before do
        TestPerAccountJob.any_instance.stubs(:sleep)
      end

      describe 'logging' do
        before do
          TestPerAccountJob.any_instance.stubs(:sleep)
          @log_periodicity = PerAccountBackfillBase::OCCASIONAL_LOG_PERIODICITY
          PerAccountBackfillBase.const_set('OCCASIONAL_LOG_PERIODICITY', 0) # always log
        end

        after do
          PerAccountBackfillBase.const_set('OCCASIONAL_LOG_PERIODICITY', @log_periodicity)
        end

        it 'logs whether it is a dry run, etc.' do
          job = TestPerAccountJob.new(dry_run: dry_run, shards: [@account.shard_id], verbose: true)
          job.expects(:log).with("*** #{ dry_run ? 'DRY' : 'LIVE' } RUN ***")
          unless dry_run
            (0...10).each do |i|
              job.expects(:log).with("Starting in #{10 - i} seconds")
            end
          end
          job.stubs(:perform_one_pass).returns(1, 0)
          job.expects(:log_if_verbose).with('First pass- backfilling all accounts')
          job.expects(:log_if_verbose).with('Followup pass- backfilling moved accounts')
          job.expects(:log).with('Test log summary')
          job.perform
        end
      end

      describe 'sleeping' do
        it 'sleeps when live run, but not when dry run' do
          job = TestPerAccountJob.new(dry_run: dry_run, shards: [])
          job.expects(:sleep).never if dry_run
          job.expects(:sleep).times(10) unless dry_run
          job.perform
        end
      end
    end
  end

  describe '#perform_one_pass' do
    [true, false].each do |dry_run|

      describe 'logging' do
        before do
          @log_periodicity = PerAccountBackfillBase::OCCASIONAL_LOG_PERIODICITY
          PerAccountBackfillBase.const_set('OCCASIONAL_LOG_PERIODICITY', 0) # always log
        end

        after do
          PerAccountBackfillBase.const_set('OCCASIONAL_LOG_PERIODICITY', @log_periodicity)
        end

        it 'logs status, etc.' do
          job = TestPerAccountJob.new(dry_run: dry_run, shards: [@account.shard_id], verbose: true)
          num_accounts = Account.count
          Account.all.each_with_index do |acct, acct_index|
            job.expects(:log_occasionally).with("Backfilling account number #{acct_index + 1} of #{num_accounts} on " \
                                                "shard #{@account.shard_id} (shard  number 1 of 1)")
            job.expects(:log_if_verbose).with("Backfilling account #{acct.subdomain}")
          end
          job.perform_one_pass
        end
      end

      describe 'backfilling' do
        before do
          @job = TestPerAccountJob.new(dry_run: dry_run, shards: [@account.shard_id])
        end

        it 'calls "backfill_account" for each account (regardless of dry_run state)' do
          @job.perform_one_pass
          assert (Account.all - @job.accounts_backfilled).empty?
        end

        it 'calls "constrain_accounts" to constraint the accounts that are iterated' do
          @job.constrain_to_account(@account.id)
          @job.perform_one_pass
          assert_equal [@account], @job.accounts_backfilled
        end

        it 'skips accounts that were created after the job was created' do
          @account.update_attribute(:created_at, Time.now + 9999)
          @job.constrain_to_account(@account.id)
          @job.perform_one_pass
          assert_equal [], @job.accounts_backfilled
        end
      end
    end
  end

  describe '#log' do
    it 'calls puts' do
      job = TestPerAccountJob.new()
      job.expects(:puts).with('testing')
      job.log('testing')
    end
  end

  describe '#log_if_verbose' do
    describe 'when verbose' do
      it 'calls log' do
        job = TestPerAccountJob.new(verbose: true)
        job.expects(:log).with('testing')
        job.log_if_verbose('testing')
      end
    end

    describe 'when not verbose' do
      it 'calls log' do
        job = TestPerAccountJob.new(verbose: false)
        job.expects(:log).never
        job.log_if_verbose('testing')
      end
    end
  end

  describe '#log_occasionally' do
    before do
      Timecop.freeze
      @job = TestPerAccountJob.new
    end

    describe 'when called twice in quick succession' do
      it 'only logs first message' do
        @job.expects(:log).with('message 1')
        @job.expects(:log).with('message 2').never
        @job.log_occasionally('message 1')
        @job.log_occasionally('message 2')
      end
    end

    describe 'when called twice with a gap in time' do
      it 'logs both messages' do
        @job.expects(:log).with('message 1')
        @job.expects(:log).with('message 2')
        @job.log_occasionally('message 1')
        Timecop.travel(PerAccountBackfillBase::OCCASIONAL_LOG_PERIODICITY + 1)
        @job.log_occasionally('message 2')
      end
    end
  end

  describe '#backfill_account' do
    it 'raises exception (should be overridden in subclass)' do
      job = PerAccountBackfillBase.new

      assert_raises(RuntimeError) do
        job.backfill_account('foo')
      end
    end
  end

  describe '#constrain_accounts' do
    it 'returns the arel that was passed to it' do
      arel = stub('arel')
      assert_equal arel, PerAccountBackfillBase.new.constrain_accounts(arel)
    end
  end

  describe '#summary' do
    it 'returns a useful default message' do
      job = PerAccountBackfillBase.new(shards: ['test'])
      assert_equal 'Successfully completed backfill of 0 accounts in 1 shards',
                   job.summary
    end
  end
end
