require_relative "../support/test_helper"

SingleCov.covered!

describe TagAffectedTickets do
  fixtures :accounts, :tickets, :users, :custom_field_options, :ticket_fields

  describe "Tagging a list of tickets, including closed ones" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_admin) }
    let(:ticket) {tickets(:minimum_2)}

    before do
      ticket.will_be_saved_by(user)
      ticket.additional_tags = "asdf"
      ticket.save!
    end

    def perform_backfill(dry_run: true)
      TagAffectedTickets.new(
        dry_run: dry_run,
        account_id: account.id,
        ticket_ids:[ticket.nice_id],
        identification_tag: "backfill_tag_ticket"
      ).perform
    end

    describe "adding a tag to a ticket" do
      it "does not make an update unless dry_run: false is set" do
        event_count = ticket.events.count

        perform_backfill
        ticket.reload

        assert_equal event_count, ticket.events.count
        assert_equal "asdf", ticket.taggings.last.tag_name
      end

      it "does add the tag to a ticket when not doing a dry run" do
        event_count = ticket.events.count

        perform_backfill(dry_run: false)
        ticket.reload

        assert ticket.events.count > event_count
        assert_equal "backfill_tag_ticket", ticket.taggings.last.tag_name
      end

      describe "on a closed ticket" do
        before do
          ticket.will_be_saved_by(user)
          ticket.status_id = StatusType.CLOSED
          ticket.save!
        end

        it "does not make an update unless dry_run: false is set" do
          event_count = ticket.events.count

          perform_backfill
          ticket.reload

          assert_equal event_count, ticket.events.count
          assert_equal "asdf", ticket.taggings.last.tag_name
        end

        it "does add the tag to a ticket when not doing a dry run" do
          event_count = ticket.events.count

          perform_backfill(dry_run: false)
          ticket.reload

          assert ticket.events.count > event_count
          assert_equal "backfill_tag_ticket", ticket.taggings.last.tag_name
          assert_equal ticket.status_id, StatusType.CLOSED
        end
      end
    end
  end
end
