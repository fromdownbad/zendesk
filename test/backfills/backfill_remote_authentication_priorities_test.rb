require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillRemoteAuthenticationPriorities do
  fixtures :accounts

  let(:account1) { accounts(:minimum) }
  let(:account2) { accounts(:support) }
  let(:account3) { accounts(:minimum_jwt_saml) }

  describe 'remote auth priority 2' do
    before do
      account1.remote_authentications.first.update_attributes(
        priority: 2
      )
    end

    it 'correct remote auth when priority > 1' do
      rem_auth = account1.remote_authentications.first
      assert_equal 2, rem_auth.priority
      BackfillRemoteAuthenticationPriorities.perform(dry_run: false)
      rem_auth.reload
      assert_equal 1, rem_auth.priority
    end
  end

  describe 'remote auth priority 1' do
    before do
      account1.remote_authentications.first.update_attributes(
        priority: 1
      )
    end

    it 'do not correct remote auth when priority = 1' do
      rem_auth = account1.remote_authentications.first
      assert_equal 1, rem_auth.priority
      BackfillRemoteAuthenticationPriorities.perform(dry_run: false)
      rem_auth.reload
      assert_equal 1, rem_auth.priority
    end
  end

  describe 'remote auth with multiple accounts' do
    before do
      account1.remote_authentications.first.update_attributes(
        priority: 2
      )
      account2.remote_authentications.first.update_attributes(
        priority: 1
      )
      account3.remote_authentications.first.update_attributes(
        priority: 0
      )
    end

    it 'correct remote auth when priority = 2' do
      rem_auth1 = account1.remote_authentications.first
      rem_auth2 = account2.remote_authentications.first
      rem_auth3 = account3.remote_authentications.first
      assert_equal 2, rem_auth1.priority
      assert_equal 1, rem_auth2.priority
      assert_equal 0, rem_auth3.priority
      BackfillRemoteAuthenticationPriorities.perform(dry_run: false)
      rem_auth1.reload
      rem_auth2.reload
      rem_auth3.reload
      assert_equal 1, rem_auth1.priority
      assert_equal 1, rem_auth2.priority
      assert_equal 0, rem_auth3.priority
    end
  end
end
