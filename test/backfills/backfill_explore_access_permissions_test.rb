require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillExploreAccessPermissions do
  fixtures :accounts

  let!(:account) { accounts(:minimum) }

  before do
    account.permission_sets.new(
      name:        I18n.t('txt.default.roles.staff.name'),
      description: I18n.t('txt.default.roles.staff.description'),
      role_type:   ::PermissionSet::Type::CUSTOM
    ).save

    account.permission_sets.new(
      account:     account,
      name:        I18n.t('txt.default.roles.team_leader.name'),
      description: I18n.t('txt.default.roles.team_leader.description'),
      role_type:   ::PermissionSet::Type::CUSTOM
    ).save

    account.permission_sets.new(
      account:     account,
      name:        I18n.t('txt.default.roles.advisor.name'),
      description: I18n.t('txt.default.roles.advisor.description'),
      role_type:   ::PermissionSet::Type::CUSTOM
    ).save

    account.permission_sets.new(
      account:     account,
      name:           I18n.t('txt.default.roles.chat_agent.name'),
      description:    I18n.t('txt.default.roles.chat_agent.description'),
      role_type:   ::PermissionSet::Type::CUSTOM
    ).save

    account.permission_sets.new(
      account:     account,
      name:           I18n.t('txt.default.roles.contributor.name'),
      description:    I18n.t('txt.default.roles.contributor.description'),
      role_type:   ::PermissionSet::Type::CUSTOM
    ).save
  end

  describe 'dry run' do
    before do
      account.permission_sets.each do |ps|
        assert_empty ps.permissions.where(name: :explore_access)
      end

      BackfillExploreAccessPermissions.perform(dry_run: true)
    end

    it "does not add the explore_access permission to the permission sets" do
      account.permission_sets.each do |ps|
        assert_empty ps.permissions.where(name: :explore_access)
      end
    end
  end

  describe 'perform backfill' do
    before do
      account.permission_sets.each do |ps|
        assert_empty ps.permissions.where(name: :explore_access)
      end

      BackfillExploreAccessPermissions.perform(dry_run: false)
    end

    it 'adds the explore_access permission to the permission set' do
      account.permission_sets.each do |ps|
        assert_present ps.permissions.where(name: :explore_access)
      end
    end

    it 'adds explore_access edit to staff permission set' do
      explore = account.permission_sets.where(
          name: I18n.t('txt.default.roles.staff.name')
        ).first.permissions.where(name: :explore_access).first
      assert_equal 'edit', explore.value
    end

    it 'adds explore_access full to team_leader permission set' do
      explore = account.permission_sets.where(
          name: I18n.t('txt.default.roles.team_leader.name')
        ).first.permissions.where(name: :explore_access).first
      assert_equal 'full', explore.value
    end

    it 'adds explore_access none to advisor permission set' do
      explore = account.permission_sets.where(
          name: I18n.t('txt.default.roles.advisor.name')
        ).first.permissions.where(name: :explore_access).first
      assert_equal 'none', explore.value
    end

    it 'adds explore_access none to contributor permission set' do
      explore = account.permission_sets.where(
          name: I18n.t('txt.default.roles.contributor.name')
        ).first.permissions.where(name: :explore_access).first
      assert_equal 'none', explore.value
    end

    it 'adds explore_access none to chat_agent permission set' do
      explore = account.permission_sets.where(
          name: I18n.t('txt.default.roles.chat_agent.name')
        ).first.permissions.where(name: :explore_access).first
      assert_equal 'none', explore.value
    end
  end
end
