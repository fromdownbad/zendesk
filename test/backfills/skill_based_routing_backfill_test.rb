require_relative "../support/test_helper"

SingleCov.covered!

describe SkillBasedRoutingBackfill do
  fixtures :accounts, :subscriptions

  let!(:account) do
    accnt = accounts(:minimum)
    accnt.subscription.update_attributes!(:plan_type => SubscriptionPlanType.ExtraLarge)
    accnt
  end

  it "makes no changes in dry run mode" do
    SkillBasedRoutingBackfill.perform
    refute account.has_skill_based_ticket_routing?
    refute account.has_skill_based_attribute_ticket_mapping?
  end

  it "adds the expected subscription features to the account" do
    SkillBasedRoutingBackfill.perform(:dry_run => false)
    assert account.has_skill_based_ticket_routing?
    assert account.has_skill_based_attribute_ticket_mapping?
  end
end
