require_relative '../support/test_helper'

SingleCov.covered!

describe RemoveSubscriptionFeature do
  fixtures :accounts

  let(:deleted)     { 'deleted' }
  let(:not_deleted) { 'not_deleted' }
  let(:account_1)   { accounts(:minimum) }
  let(:account_2)   { accounts(:support) }

  before do
    [account_1, account_2].each do |account|
      account.subscription.features.create(name: deleted,     value: '0')
      account.subscription.features.create(name: not_deleted, value: '0')
    end
  end

  describe 'dry run' do
    it 'does not delete any features' do
      assert_difference 'SubscriptionFeature.count(:all)', 0 do
        RemoveSubscriptionFeature.perform(name: deleted)
      end
    end
  end

  describe 'actual run' do
    describe 'without name' do
      it 'raises error' do
        assert_raises RuntimeError do
          RemoveSubscriptionFeature.perform(dry_run: false)
        end
      end
    end

    describe 'with name' do
      it 'deletes all subscription features with the same name' do
        RemoveSubscriptionFeature.perform(dry_run: false, name: deleted)
        assert_equal 0, SubscriptionFeature.where(name: deleted).count(:all)
      end

      it 'does not delete subscription features with different names' do
        RemoveSubscriptionFeature.perform(dry_run: false, name: deleted)
        assert_equal 2, SubscriptionFeature.where(name: not_deleted).count(:all)
      end
    end
  end
end
