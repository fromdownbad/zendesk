require_relative '../support/test_helper'

SingleCov.covered!

describe ProductSyncReportExporter do
  fixtures :accounts

  let(:subject) { ProductSyncReportExporter }

  let(:account_one) { accounts(:support) }

  def stub_account_validation(account, result)
    mock = mock_validation_result(account,result)
    Zendesk::Accounts::ProductSyncValidator.expects(:validate).with(account, light_agents: nil).returns(mock)
  end

  def mock_validation_result(account = nil, result = nil)
    mock_comparison_results = {
      state: :pass,
      trial_expires_at: :pass,
      plan_type: :pass,
      max_agents: :pass,
      light_agents: :pass
    }
    mock_product_attributes = {
      state: 'trial',
      trial_expires_at: '2016-01-01T00:00Z',
      plan_type: 3,
      max_agents: 5,
      light_agents: false
    }
    mock_validation_result = {
      result: result || :pass,
      comparison_results: mock_comparison_results,
      expected_product_attributes: mock_product_attributes,
      actual_product_attributes: mock_product_attributes,
      support_product: stub(created_at: nil, updated_at: nil),
      account: account || accounts(:support)
    }
  end

  describe '.perform' do
    let(:target_dir) { '/tmp' }
    let(:datetime) { DateTime.parse('2016-01-08T00:00:00Z') }
    let(:expected_csv_data_path) { '/tmp/2016-01-08T00-00-00_product_sync_data.csv' }
    let(:string_buffer) { StringIO.new }

    before do
      Timecop.freeze(datetime)
      CSV.expects(:open).with(expected_csv_data_path, 'wb', subject::CSV_OPTIONS).yields(string_buffer)
    end

    it 'exports product sync validation results to csv' do
      stub_account_validation(account_one, :pass)
      subject.perform(target_dir: target_dir, account_ids: [account_one.id])
    end

    it 'handles an exception when calling zuora and nicely formats it for csv' do
      stub_account_validation(account_one, :pass)
      Account.any_instance.stubs(:billing_multi_product_participant?).raises(StandardError.new("an exception containing, comma"))
      subject.perform(target_dir: target_dir, account_ids: [account_one.id])
      assert_includes string_buffer.string, "an exception containing- comma"
    end

    describe 'pod option' do
      it 'queries for all accounts within a pod' do
        Zendesk::Accounts::ProductSyncValidator.expects(:validate).returns(mock_validation_result).at_least_once
        subject.perform(target_dir: target_dir, pod: 1)
      end
    end

    describe 'pods option' do
      it 'queries for all accounts within specified pods' do
        Zendesk::Accounts::ProductSyncValidator.expects(:validate).returns(mock_validation_result).at_least_once
        subject.perform(target_dir: target_dir, pods: [1, 2])
      end
    end
  end
end
