require_relative '../support/test_helper'

SingleCov.covered!

describe MonitorAssumeEventTicketRequiredBackfill do
  fixtures :all

  let!(:account1) { accounts(:minimum) }
  let!(:account2) { accounts(:multiproduct) }

  let!(:assume_event1) do
    MonitorAssumeEventTicketRequiredBackfill::MonitorAccountAssumeEvent.create!(
      account_id: account1.id,
      account_user_id: 1,
      monitor_user_id: 1,
      monitor_user_ip: '127.0.0.1',
      ticket_required: true
    )
  end

  let!(:assume_event2) do
    MonitorAssumeEventTicketRequiredBackfill::MonitorAccountAssumeEvent.create!(
      account_id: account2.id,
      account_user_id: 1,
      monitor_user_id: 1,
      monitor_user_ip: '127.0.0.1',
      ticket_required: true
    )
  end

  before do
    account1.owner.email = 'owner@zendesk.com'
    account1.owner.save!

    MonitorAssumeEventTicketRequiredBackfill.perform(dry_run: false)
  end

  it 'changes the record with a zendesk owner' do
    refute assume_event1.reload.ticket_required?
  end

  it 'does not change the record without a zendesk owner' do
    assert assume_event2.reload.ticket_required?
  end
end
