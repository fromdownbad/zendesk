require_relative "../support/test_helper"
require_relative "../support/rules_test_helper"

SingleCov.covered!

describe ConvertTriggersToAnswerBot do
  include RulesTestHelper

  fixtures :accounts

  let(:email_body_position) { 2 }
  let(:account) { accounts(:minimum) }
  let(:deflection_trigger) { account.triggers.reload.first }
  let(:deflection_triggers) { account.triggers.reload }
  let(:invalid_body) { '{{automatic_answers.articles_derp}} ' }
  let(:valid_body) do
    <<-HTML
      {% if automatic_answers.article_count > 0 %}
       Here are some great articles that may help:
         {{automatic_answers.article_list}}
         {{automatic_answers.first_article_body}}
      {% endif %}
    HTML
  end
  let(:expected_body) do
    <<-HTML
      {% if answer_bot.article_count > 0 %}
       Here are some great articles that may help:
         {{answer_bot.article_list}}
         {{answer_bot.first_article_body}}
      {% endif %}
    HTML
  end

  before do
    Account.any_instance.expects(:has_automatic_answers_enabled?).returns(true).at_least_once
    account.triggers.clear
  end

  describe 'perform' do
    describe 'when not on a dry run' do
      let(:backfill) { ConvertTriggersToAnswerBot.new(dry_run: false, account_ids: [account.id]) }

      describe 'given a trigger with a valid placeholder' do
        let(:body) { '{{automatic_answers.article_list}}' }
        let(:expected) { '{{answer_bot.article_list}}' }

        before do
          trigger = add_deflection_action_to_trigger(build_trigger, body)
          account.triggers << trigger
          backfill.perform
        end

        it 'converts the placeholder to answer_bot' do
          actual = deflection_trigger.definition.actions.first.value[email_body_position]

          assert_equal expected, actual
        end
      end

      describe 'given a trigger with an invalid placeholder' do
        let(:body) { '{{automatic_answers.thing}}' }

        before do
          trigger = add_deflection_action_to_trigger(build_trigger, body)
          account.triggers << trigger
          backfill.perform
        end

        it 'does not convert the placeholder to answer_bot' do
          actual = deflection_trigger.definition.actions.first.value[email_body_position]

          assert_equal body, actual
        end
      end

      describe 'given a trigger with no placeholder' do
        let(:body) { 'Other things are cool too.' }

        before do
          trigger = add_deflection_action_to_trigger(build_trigger, body)
          account.triggers << trigger
          backfill.perform
        end

        it 'does not update the email body' do
          actual = deflection_trigger.definition.actions.first.value[email_body_position]

          assert_equal body, actual
        end
      end

      describe 'given a trigger with valid and invalid placeholders' do
        let(:mixed_body) do
          <<-HTML
            {{automatic_answers.article_list}}
            {{automatic_answers.derp}}
          HTML
        end
        let(:expected) do
          <<-HTML
            {{answer_bot.article_list}}
            {{automatic_answers.derp}}
          HTML
        end

        before do
          trigger = add_deflection_action_to_trigger(build_trigger, mixed_body)
          account.triggers << trigger
          backfill.perform
        end

        it 'converts the valid and ignores the invalid' do
          actual = deflection_trigger.definition.actions.first.value[email_body_position]

          assert_equal expected, actual
        end
      end

      describe 'given an inactive deflection trigger' do
        before do
          trigger = add_deflection_action_to_trigger(build_trigger, valid_body)
          trigger.is_active = false
          account.triggers << trigger
          backfill.perform
        end

        it 'converts placeholders to answer_bot' do
          actual = deflection_trigger.definition.actions.first.value[email_body_position]

          assert_equal expected_body, actual
        end
      end

      describe 'given an account with multiple deflection triggers' do
        before do
          2.times do
            trigger = add_deflection_action_to_trigger(build_trigger, valid_body)
            account.triggers << trigger
          end
          backfill.perform
        end

        it 'converts placeholders to answer_bot' do
          deflection_triggers.each do |trigger|
            actual = trigger.definition.actions.first.value[email_body_position]

            assert_equal expected_body, actual
          end
        end
      end

      describe 'given a trigger with multiple deflection actions' do
        before do
          trigger = add_deflection_action_to_trigger(build_trigger, valid_body)
          trigger = add_deflection_action_to_trigger(trigger, invalid_body)
          account.triggers << trigger
          backfill.perform
        end

        it 'converts an action with valid placeholders' do
          actual = deflection_trigger.definition.actions.first.value[email_body_position]

          assert_equal expected_body, actual
        end

        it 'does not convert an action with invalid placeholders' do
          actual = deflection_trigger.definition.actions.last.value[email_body_position]

          assert_equal invalid_body, actual
        end
      end
    end

    describe 'when an exception is thrown' do
      let(:backfill) { ConvertTriggersToAnswerBot.new(dry_run: false, account_ids: [account.id]) }

      before do
        trigger = add_deflection_action_to_trigger(build_trigger, valid_body)
        account.triggers << trigger
        Trigger.any_instance.expects(:save!).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
      end

      it 'rescues and logs the exception' do
        expected_message = "[ConvertTriggersToAnswerBot | Pod: 1 | Dry Run: false]: Failed to convert answer_bot placeholder for account_id: #{account.id}, error: RecordNotSaved."
        Rails.logger.expects(:info).with(expected_message)

        backfill.perform
      end
    end

    describe 'when on a dry run' do
      let(:backfill) { ConvertTriggersToAnswerBot.new(dry_run: true, account_ids: [account.id]) }

      before do
        trigger = add_deflection_action_to_trigger(build_trigger, valid_body)
        account.triggers << trigger
        backfill.perform
      end

      it 'does not convert placeholders' do
        actual = deflection_trigger.definition.actions.first.value[email_body_position]

        assert_equal valid_body, actual
      end
    end
  end

  def build_trigger
    build_rule(type: "Trigger", conditions_all: ['update_type', 'is', ['Create']])
  end

  def add_deflection_action_to_trigger(trigger, body)
    action = DefinitionItem.new('deflection', 'is', ['requester_id', 'subject', body])
    trigger.definition.actions << action
    trigger
  end
end
