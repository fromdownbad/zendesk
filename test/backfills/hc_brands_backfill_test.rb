require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe HcBrandsBackfill do

  let(:described_class) { HcBrandsBackfill }
  let(:minimum_account) { accounts(:minimum) }

  before do
    BrandProtobufEncoder.any_instance.stubs(:to_proto)
  end

  describe "#perform_on_pod" do
    it "calls publish for account n times" do
      account_count = Account.count
      BrandPublisher.any_instance.expects(:publish_for_account).times(account_count)

      described_class.new(dry_run: false).perform_on_pod
    end
  end

  describe "#perform_for_account" do
    it "calls publish when dry_run is false" do
      BrandPublisher.any_instance.expects(:publish_for_account).once

      described_class.new(dry_run: false).perform_for_account(account_id: minimum_account.id)
    end

    it "does not call publish when dry_run is true" do
      BrandPublisher.any_instance.expects(:publish_for_account).never

      described_class.new(dry_run: true).perform_for_account(account_id: minimum_account.id)
    end
  end
end
