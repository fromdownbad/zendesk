require_relative '../support/test_helper'

SingleCov.covered!

describe FixNonBoostableSubscriptionFeatures do
  fixtures :accounts

  let(:boostable)     { 'boostable' }
  let(:non_boostable) { 'non_boostable' }
  let(:account)       { accounts(:support) }

  before do
    account.subscription.features.create(name: boostable,     value: '0')
    account.subscription.features.create(name: non_boostable, value: '0')
  end

  describe 'dry run' do
    it 'does not execute subscription feature service' do
      Zendesk::Features::SubscriptionFeatureService.any_instance.expects(:execute!).never
      FixNonBoostableSubscriptionFeatures.perform(account_ids: [account.id], names: [])
    end

    it 'does not update any subscription features' do
      ActiveRecord::Relation.any_instance.expects(:update_all).never
      FixNonBoostableSubscriptionFeatures.perform(account_ids: [account.id], names: [])
    end
  end

  describe 'actual run' do
    describe 'without names' do
      it 'raises error' do
        assert_raises RuntimeError do
          FixNonBoostableSubscriptionFeatures.perform(dry_run: false)
        end
      end
    end

    describe 'with names' do
      describe 'when account has use_feature_framework_persistence setting' do
        before do
          account.set_use_feature_framework_persistence(true)
        end

        it 'updates subscription features with the same names' do
          ActiveRecord::Relation.any_instance.expects(:update_all).at_least_once
          FixNonBoostableSubscriptionFeatures.perform(dry_run: false, account_ids: [account], names: [non_boostable])
        end

        it 'executes subscription feature service' do
          Zendesk::Features::SubscriptionFeatureService.any_instance.expects(:execute!).at_least_once
          FixNonBoostableSubscriptionFeatures.perform(dry_run: false, account_ids: [account], names: [non_boostable])
        end
      end

      describe 'when account does not have use_feature_framework_persistence setting' do
        before do
          account.set_use_feature_framework_persistence(false)
        end

        it 'does not update any subscription features' do
          ActiveRecord::Relation.any_instance.expects(:update_all).never
          FixNonBoostableSubscriptionFeatures.perform(dry_run: false, account_ids: [account], names: [non_boostable])
        end

        it 'does not execute subscription feature service' do
          Zendesk::Features::SubscriptionFeatureService.any_instance.expects(:execute!).never
          FixNonBoostableSubscriptionFeatures.perform(dry_run: false, account_ids: [account], names: [non_boostable])
        end
      end
    end
  end
end
