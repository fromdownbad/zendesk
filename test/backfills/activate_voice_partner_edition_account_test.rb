require_relative "../support/test_helper"

SingleCov.covered!

describe ActivateVoicePartnerEditionAccount do
  fixtures :accounts, :subscriptions
  let(:described_class) { ActivateVoicePartnerEditionAccount }

  let(:account) { accounts(:minimum) }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
    FactoryBot.create(:tpe_subscription, account: account, active: true)
    FactoryBot.create(:voice_partner_edition_account, account: account, trial: true)
  end

  describe "when no account_ids are passed" do
    it "doesn't perform any action" do
      Voice::PartnerEditionAccount.any_instance.expects(:subscribe!).never

      described_class.new(dry_run: false).perform
    end
  end

  describe "when dry_run is true" do
    it "doesn't perform any action" do
      Voice::PartnerEditionAccount.any_instance.expects(:subscribe!).never

      described_class.new(account_ids: [account.id], dry_run: true).perform
    end
  end

  it "updates trial to false" do
    Voice::PartnerEditionAccount.any_instance.expects(:subscribe!).once
    Rails.logger.expects(:info).with("Activating voice_partner_edition_account for account: #{account.id}").once

    described_class.new(account_ids: [account.id], dry_run: false).perform

    refute account.reload.voice_partner_edition_account.trial
  end
end
