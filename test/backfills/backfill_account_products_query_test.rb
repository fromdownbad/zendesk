require_relative '../support/test_helper'

SingleCov.covered!

describe BackfillAccountProductsQuery do
  fixtures :accounts, :translation_locales

  let(:account) { accounts(:support) }

  let!(:shell_account_without_support) { accounts(:shell_account_without_support) }

  let(:inactive_account) { accounts(:inactive) }

  let!(:sandbox_account) do
    sandbox = accounts(:minimum)
    sandbox.update_attribute(:sandbox_master_id, account.id)
    sandbox
  end

  let!(:inbox_account) do
    inbox = accounts(:inbox)
    inbox.subscription.update_attribute(:plan_type, SubscriptionPlanType.Inbox)
    inbox
  end

  let!(:other_account) do
    account = accounts(:minimum)
    account.update_attribute(:created_at, DateTime.tomorrow)
    account
  end

  let!(:soft_deleted_account) do
    account = accounts(:inactive)
    account.update_attribute(:deleted_at, DateTime.yesterday)
    account
  end

  describe '#account_query' do
    it 'excludes shell accounts without Support product' do
      assert_includes Account.unscoped, shell_account_without_support
      assert_not_includes BackfillAccountProducts.new().account_query, shell_account_without_support
    end

    describe 'with created_after_date and start_account_id option' do
      it 'raises error' do
        assert_raises ArgumentError do
          BackfillAccountProducts.new(start_account_id: account.id, created_after_date: account.created_at.to_date.to_s)
        end
      end
    end

    describe 'account_ids option' do
      let(:subject) { BackfillAccountProducts.new(account_ids: [account.id]) }

      it 'queries for specific accounts' do
        assert_equal subject.account_query, [account]
      end
    end

    describe 'start_account_id option' do
      let(:subject) { BackfillAccountProducts.new(start_account_id: other_account.id) }

      it 'queries for all accounts after and including start_account_id' do
        assert subject.account_query.all? { |acct| acct.id >= other_account.id }
        assert_not_includes subject.account_query, account
      end
    end

    describe 'created_after_date option' do
      let(:subject) { BackfillAccountProducts.new(created_after_date: other_account.created_at.to_date.to_s) }

      it 'queries for all accounts created on or after the created_after_date' do
        assert subject.account_query.all? { |acct| acct.id >= other_account.id }
        assert_not_includes subject.account_query, account
      end
    end

    describe 'include_inactive option' do
      describe 'when include_inactive is true' do
        let(:subject) { BackfillAccountProducts.new(include_inactive: true) }

        it 'includes inactive accounts' do
          assert_includes subject.account_query, inactive_account
        end
      end

      describe 'when include_inactive is false' do
        let(:subject) { BackfillAccountProducts.new(include_inactive: false) }

        it 'does not include inactive accounts' do
          assert_not_includes subject.account_query, inactive_account
        end
      end

      describe 'when include_inactive is not provided' do
        let(:subject) { BackfillAccountProducts.new() }

        it 'does not include inactive accounts by default' do
          assert_not_includes subject.account_query, inactive_account
        end
      end
    end

    describe 'is_deleted option' do
      describe 'when is_deleted is true' do
        let(:subject) { BackfillAccountProducts.new(is_deleted: true, include_inactive: true) }

        it 'queries for all deleted accounts' do
          assert_includes subject.account_query, soft_deleted_account
          assert_not_includes subject.account_query, account
        end
      end

      describe 'when is_deleted is false' do
        let(:subject) { BackfillAccountProducts.new(is_deleted: false, include_inactive: true) }

        it 'queries for all accounts that have not been deleted' do
          assert_includes subject.account_query, account
          assert_not_includes subject.account_query, soft_deleted_account
        end
      end
    end

    describe 'include_sandbox option' do
      describe 'when include_sandbox is true' do
        let(:subject) { BackfillAccountProducts.new(include_sandbox: true) }

        it 'includes sandbox accounts in query' do
          assert_includes subject.account_query, sandbox_account
        end
      end

      describe 'when include_sandbox is false' do
        let(:subject) { BackfillAccountProducts.new(include_sandbox: false) }

        it 'excludes sandbox accounts in query' do
          assert_includes Account.unscoped, sandbox_account
          assert_not_includes subject.account_query, sandbox_account
        end
      end

      describe 'when include_sandbox is not specified' do
        let(:subject) { BackfillAccountProducts.new() }

        it 'excludes sandbox accounts in query' do
          assert_includes Account.unscoped, sandbox_account
          assert_not_includes subject.account_query, sandbox_account
        end
      end
    end

    describe 'include_inbox option' do
      describe 'when include_inbox is true' do
        let(:subject) { BackfillAccountProducts.new(include_inbox: true) }

        it 'includes inbox accounts in query' do
          assert_includes subject.account_query, inbox_account
        end
      end

      describe 'when include_inbox is false' do
        let(:subject) { BackfillAccountProducts.new(include_inbox: false) }

        it 'excludes inbox accounts in query' do
          assert_includes Account.unscoped, inbox_account
          assert_not_includes subject.account_query, inbox_account
        end
      end

      describe 'when include_inbox is not specified' do
        let(:subject) { BackfillAccountProducts.new() }

        it 'excludes inbox accounts in query' do
          assert_includes Account.unscoped, inbox_account
          assert_not_includes subject.account_query, inbox_account
        end
      end
    end
  end

  describe '#account_query_for_exporter' do
    let(:subject) { BackfillAccountProducts.new() }

    it 'excludes accounts where the subscription was updated in the last 5 minutes' do
      account.subscription.update_column(:updated_at, 4.minutes.ago)
      assert_includes Account.unscoped, account
      assert_not_includes subject.account_query_for_exporter, account
    end

    it 'includes accounts where the subscription was updated over 5 minutes ago' do
      account.subscription.update_column(:updated_at, 6.minutes.ago)
      assert_includes subject.account_query_for_exporter, account
    end
  end
end
