require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe DisableGooddataProjects do
  fixtures :accounts

  let(:time_before) { Time.new(2016, 01, 01) }
  let(:start_time) { Time.new(2016, 01, 02).to_i }
  let(:time_after) { Time.new(2016, 01, 03) }
  let(:project_ids) { ['abc', 'def']}
  let(:gooddata_client) { stub('gooddata_client') }
  let(:integration) { stub('integration') }
  let(:some_accounts) { [accounts(:minimum), accounts(:support)] }
  let(:project) { stub('project', :integration => integration) }

  before do
    create_gooddata_integrations
  end

  describe 'perform' do
    describe 'when on a dry run' do
      let (:backfill) { DisableGooddataProjects.new(
        dry_run: true,
        project_ids: project_ids,
        start_time: start_time
      )}

      before do
        backfill.stubs(:gooddata_client).returns(gooddata_client)
      end

      it 'does not call gooddata client API' do
        gooddata_client.expects(:project).never

        backfill.perform
      end

      it 'does not disable any gooddata integration records' do
        backfill.perform

        assert_equal project_ids.length, GooddataIntegration.where(:project_id => project_ids).map(&:id).count
        GooddataIntegration.where(:project_id => project_ids).each  do |integration|
          assert_equal 'complete', integration.status
        end
      end
    end

    describe 'when not on a dry run' do
      let(:backfill) { DisableGooddataProjects.new(
        dry_run: false,
        project_ids: project_ids,
        start_time: start_time
      )}

      before do
        backfill.stubs(:gooddata_client).returns(gooddata_client)

        project_ids.each do |project_id|
          gooddata_client.stubs(:project).with(:id => project_id).returns(project)
          integration.expects(:update).with(
              :active => false,
              :template_name => Zendesk::Gooddata::IntegrationProvisioning::TEMPLATE_NAME,
              :template_version => 123
          )
        end
        Zendesk::Gooddata::IntegrationProvisioning.stubs(:integration_template_version).returns(123)
      end

      it 'disable gooddata integration' do
        backfill.perform

        assert_equal project_ids.length, GooddataIntegration.where(:project_id => project_ids).map(&:id).count
        GooddataIntegration.where(:project_id => project_ids).each  do |integration|
          assert_equal 'disabled', integration.status
        end
      end

      it 'does not remove any gooddata users' do
        GooddataUserDestroyJob.expects(:enqueue).never

        backfill.perform
      end
    end

    describe 'when given project IDs that does not exist in the shards for the given pod' do
      let (:backfill) { DisableGooddataProjects.new(
        dry_run: false,
        project_ids: ['some-non-existent-project'],
        start_time: start_time
      )}

      before do
        backfill.stubs(:gooddata_client).returns(gooddata_client)
      end

      it 'does not call gooddata client API' do
        gooddata_client.expects(:project).never

        backfill.perform
      end

      it 'does not disable any gooddata integration records' do
        backfill.perform

        assert_equal project_ids.length, GooddataIntegration.where(:project_id => project_ids).map(&:id).count
        GooddataIntegration.where(:project_id => project_ids).each  do |integration|
          assert_equal 'complete', integration.status
        end
      end
    end
  end

  def create_gooddata_integrations
    some_accounts.each_with_index do |test_account, index|
      test_account.reload.update_attribute(:is_serviceable, true)

      test_account.create_gooddata_integration(
               :version => 2,
               :project_id => project_ids[index],
               :status => 'complete'
      )
      create_gooddata_user(project_ids[index], test_account)
    end
  end

  def create_gooddata_user(project_id, account)
    GooddataUser.create! do |u|
      u.account = account
      u.user = account.users.first
      u.gooddata_project_id = project_id
      u.gooddata_user_id = "user_#{project_id}"
    end
  end
end
