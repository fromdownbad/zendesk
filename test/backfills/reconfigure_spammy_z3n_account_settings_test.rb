require_relative "../support/test_helper"

SingleCov.covered!

describe ReconfigureSpammyZ3nAccountSettings do
  fixtures :accounts

  let(:account) do
    account = accounts(:minimum)
    account.subdomain = 'z3ntest'
    account.save!
    account
  end

  describe 'run_on_account' do
    describe "when the account owner has a z3n subdomain" do
      it "modifies the trigger" do
        ReconfigureSpammyZ3nAccountSettings.new(account_id: account.id, dry_run: false).perform

        modified_trigger = account.triggers.find_by(title: ReconfigureSpammyZ3nAccountSettings::DEFAULT_NOTIFY_TRIGGER)
        assert_equal modified_trigger.definition.actions.first.value[1], I18n.t('txt.default.triggers.notify_requester_received.subject_v3')
        assert_equal modified_trigger.definition.actions.first.value[2], I18n.t('txt.default.triggers.notify_requester_received.body_v4')
      end

      it "does not modify the trigger in dry mode" do
        ReconfigureSpammyZ3nAccountSettings.new(account_id: account.id, dry_run: true).perform

        unmodified_trigger = account.triggers.find_by(title: ReconfigureSpammyZ3nAccountSettings::DEFAULT_NOTIFY_TRIGGER)
        refute_equal unmodified_trigger.definition.actions.first.value[1], I18n.t('txt.default.triggers.notify_requester_received.subject_v3')
        refute_equal unmodified_trigger.definition.actions.first.value[2], I18n.t('txt.default.triggers.notify_requester_received.body_v4')
      end

      describe 'when sender_authentication is disabled' do
        before { account.settings.email_sender_authentication = false }
        it "enables sender_authentication" do
          ReconfigureSpammyZ3nAccountSettings.new(account_id: account.id, dry_run: false).perform
          assert account.reload.has_email_sender_authentication?
        end

        it "does not enable sender_authentication in dry mode" do
          ReconfigureSpammyZ3nAccountSettings.new(account_id: account.id, dry_run: true).perform
          refute account.reload.has_email_sender_authentication?
        end
      end
    end

    describe "when it is not a z3n subdomain or account_name" do
      before do
        account.subdomain = 'notarealzendesk'
        account.name = 'notarealzendesk'
        account.save!
      end

      it "keeps the trigger unmodified" do
        ReconfigureSpammyZ3nAccountSettings.new(account_id: account.id, dry_run: false).perform
        trigger = account.triggers.find_by(title: ReconfigureSpammyZ3nAccountSettings::DEFAULT_NOTIFY_TRIGGER)
        refute_equal trigger.definition.actions.first.value[1], I18n.t('txt.default.triggers.notify_requester_received.subject_v3')
        refute_equal trigger.definition.actions.first.value[2], I18n.t('txt.default.triggers.notify_requester_received.body_v4')
      end

      describe 'when sender_authentication is disabled' do
        before { account.settings.email_sender_authentication = false }
        it "does not enable sender_authentication" do
          ReconfigureSpammyZ3nAccountSettings.new(account_id: account.id, dry_run: false).perform
          refute account.has_email_sender_authentication?
        end
      end
    end
  end

  describe 'on a POD' do
    it 'should run the migration for all account on all shards in the current POD' do
      Account.shard_local.active.serviceable.count.times do
        ReconfigureSpammyZ3nAccountSettings.any_instance.expects(:backfill_account)
      end
      ReconfigureSpammyZ3nAccountSettings.new.perform
    end

    describe 'when an exception is raised for an account' do
      it 'should skip account which raised exception and continue with the next' do
        Rails.logger.expects(:info).with("ReconfigureSpammyZ3nAccountSettings: Starting dry_run=true")
        expected_message = "backfill_shard failed for account_id: -1 with error: StandardError"
        Rails.logger.expects(:info).with(expected_message).once

        ReconfigureSpammyZ3nAccountSettings.any_instance.expects(:backfill_account).at_least_once
        ReconfigureSpammyZ3nAccountSettings.any_instance.stubs(:backfill_account).with(-1).raises(StandardError)

        ReconfigureSpammyZ3nAccountSettings.new.perform
      end
    end
  end
end
