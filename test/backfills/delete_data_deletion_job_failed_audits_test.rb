require_relative "../support/test_helper"

SingleCov.covered!

describe DeleteDataDeletionJobFailedAudits do
  fixtures :accounts
  before(:all) do
    @account = accounts(:minimum)
    @account.cancel!
    @account.subscription.credit_card = CreditCard.new
    @account.subscription.canceled_on = 130.days.ago
    @account.subscription.save!
    @account.soft_delete!
    @account.deleted_at = 40.days.ago
    @account.save!

    # Record created by soft_delete!
    @audit            = @account.data_deletion_audits.first
    @audit.created_at = 40.days.ago
    @audit.save!

    @job_audit1 = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
    @job_audit3 = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::RemoveShardDataJob).tap(&:save!)
    exception1  = -> { raise 'my message' }.must_raise RuntimeError
    @job_audit1.fail!(exception1)
  end

  describe 'when run in dry mode' do
    before do
      DeleteDataDeletionJobFailedAudits.perform
    end

    it 'makes no changes' do
      assert_equal @audit.job_audits.reload.count, 2
    end
  end

  describe 'when run in production mode' do
    before do
      DeleteDataDeletionJobFailedAudits.perform(dry_run: false)
    end

    it 'does delete failed audits' do
      assert DataDeletionAuditJob.find_by_id(@job_audit1.id).nil?
    end

    it "does not delete audits that aren't failed" do
      assert @job_audit3.reload.present?
    end
  end
end
