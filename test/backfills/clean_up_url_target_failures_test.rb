require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe CleanUpUrlTargetFailures do
  fixtures :accounts, :tickets, :events, :targets

  let(:account) { accounts(:minimum) }
  let(:event) { events(:create_external_for_minimum_ticket_1) }
  let(:ticket) { event.ticket }
  let(:target) { targets(:url_v2) }

  #To emulate the conditions which caused the need for this backfill:
  UrlTargetFailure.skip_callback(:create, :after, :clean_up_url_target_failures)

  describe "#perform" do

    it "doesn't destroy records if dry_run is true" do
      build_and_save_url_target_failures
      
      initial_count = UrlTargetFailure.where(url_target_v2_id: target.id).count
      CleanUpUrlTargetFailures.perform(dry_run: true)
      assert_equal initial_count, UrlTargetFailure.where(url_target_v2_id: target.id).count
    end

    it "destroys records if dry_run is false" do
      build_and_save_url_target_failures

      initial_count = UrlTargetFailure.where(url_target_v2_id: target.id).count
      CleanUpUrlTargetFailures.perform(dry_run: false)
      assert_operator initial_count, :>, UrlTargetFailure.where(url_target_v2_id: target.id).count
    end

    it "destroys all but 25 records for targets over limit" do
      initial_count = UrlTargetFailure.where(url_target_v2_id: target.id).count

      build_and_save_url_target_failures
      CleanUpUrlTargetFailures.perform(dry_run: false)
      assert_equal 25 + initial_count, UrlTargetFailure.where(url_target_v2_id: target.id).count
    end
  end

  private

  def new_url_target_failure
    UrlTargetFailure.new do |new_fail|
      new_fail.account_id = account.id
      new_fail.event = event
      new_fail.exception = Exception.to_s
      new_fail.message = "message"
      new_fail.target = target
      new_fail.consecutive_failure_count = 5
      new_fail.raw_http_capture = target.raw_http_capture
    end
  end

  def build_and_save_url_target_failures
    40.times do
      new_url_target_failure.save
    end
  end
end
