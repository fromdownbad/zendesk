require_relative "../support/test_helper"

SingleCov.covered!

describe DisableEndUserSocialLogin do
  fixtures :accounts

  def enable_end_user_remote_and_social_login!(account)
    account.role_settings.update_attributes!(end_user_remote_login: true, end_user_google_login: true, end_user_facebook_login: true, end_user_twitter_login: true, end_user_office_365_login: true)

    remote_and_social_logins_enabled?(account)
  end

  def remote_and_social_logins_enabled?(account)
    all_social_logins_enabled?(account)
    assert account.role_settings.end_user_remote_login
  end

  def all_social_logins_enabled?(account)
    assert account.role_settings.end_user_google_login
    assert account.role_settings.end_user_facebook_login
    assert account.role_settings.end_user_twitter_login
    assert account.role_settings.end_user_office_365_login
  end

  def all_social_logins_disabled?(account)
    refute account.role_settings.end_user_google_login
    refute account.role_settings.end_user_facebook_login
    refute account.role_settings.end_user_twitter_login
    refute account.role_settings.end_user_office_365_login
  end

  describe '#perform' do
    describe 'with 2 accounts having both end user remote login and all end user social logins enabled' do
      let(:account_1) { accounts(:minimum) }
      let(:account_2) { accounts(:multiproduct) }

      before do
        enable_end_user_remote_and_social_login!(account_1)
        enable_end_user_remote_and_social_login!(account_2)
      end

      describe 'with dry_run: false' do
        let(:dry_run) { false }

        before do
          DisableEndUserSocialLogin.perform(dry_run: dry_run)
          account_1.role_settings.reload
          account_2.role_settings.reload
        end

        it 'disables all social logins' do
          all_social_logins_disabled?(account_1)
          all_social_logins_disabled?(account_2)
        end

        it 'remote login stays enabled' do
          assert account_1.role_settings.end_user_remote_login
          assert account_2.role_settings.end_user_remote_login
        end
      end

      describe 'with dry_run: true' do
        let(:dry_run) { true }

        before do
          DisableEndUserSocialLogin.perform(dry_run: dry_run)
          account_1.role_settings.reload
          account_2.role_settings.reload
        end

        it 'all logins still enabled' do
          remote_and_social_logins_enabled?(account_1)
          remote_and_social_logins_enabled?(account_2)
        end
      end
    end

    describe 'with account having only social logins enabled' do
      let(:account) { accounts(:minimum) }

      before do
        account.role_settings.update_attributes!(end_user_remote_login: false, end_user_google_login: true, end_user_facebook_login: true, end_user_twitter_login: true, end_user_office_365_login: true)

        refute account.role_settings.end_user_remote_login
        all_social_logins_enabled?(account)
      end

      describe 'with dry_run: false' do
        let(:dry_run) { false }

        before do
          DisableEndUserSocialLogin.perform(dry_run: dry_run)
          account.role_settings.reload
          account.role_settings.reload
        end

        it 'all social logins still enabled' do
          all_social_logins_enabled?(account)
        end

        it 'remote login stays disabled' do
          refute account.role_settings.end_user_remote_login
        end
      end

      describe 'with dry_run: true' do
        let(:dry_run) { true }

        before do
          DisableEndUserSocialLogin.perform(dry_run: dry_run)
          account.role_settings.reload
          account.role_settings.reload
        end

        it 'all social logins still enabled' do
          all_social_logins_enabled?(account)
        end

        it 'remote login stays disabled' do
          refute account.role_settings.end_user_remote_login
        end
      end
    end
  end
end
