require_relative '../support/test_helper'

SingleCov.covered!

describe BackfillShopifyRedirect do
  fixtures :global_oauth_clients
  let(:client) { global_oauth_clients(:system_users) }

  before do
    silence_warnings {BackfillShopifyRedirect.send(:const_set, :CLIENT_ID, client.id)}
  end

  it "adds new uri to the redirect_uri" do
    backfill = BackfillShopifyRedirect.new({dry_run: false})
    backfill.perform

    BackfillShopifyRedirect::URLS.each do |url|
      assert client.reload.redirect_uri.include? url
    end
  end

  it "does not update redirect uri if dry run is true" do
    backfill = BackfillShopifyRedirect.new({dry_run: true})
    backfill.perform

    BackfillShopifyRedirect::URLS.each do |url|
      refute client.reload.redirect_uri.include? url
    end
  end
end
