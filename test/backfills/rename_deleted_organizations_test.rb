require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe RenameDeletedOrganizations do
  fixtures :organizations

  let(:org1) { organizations(:minimum_organization1) }
  let(:org2) { organizations(:minimum_organization2) }
  let(:org3) { organizations(:minimum_organization3) }
  let(:long_org) { Organization.create!(name: 'a'*255, account: accounts(:minimum)) }
  let(:old_name1) { org1.name }
  let(:old_name2) { org2.name }
  let(:old_name3) { org3.name }
  let(:long_name) { long_org.name }

  before do
    org1.soft_delete!
    long_org.soft_delete!
    org2.destroy
  end

  describe 'when run in dry mode' do
    before do
      RenameDeletedOrganizations.perform
    end

    it 'makes no changes' do
      assert_equal old_name1, org1.reload.name
      assert_equal old_name2, org2.reload.name
    end
  end

  describe 'when run in production mode' do
    before do
      RenameDeletedOrganizations.perform(:dry_run => false)
    end
    it 'does not rename organizations that are not deleted' do
      assert_equal old_name3, org3.reload.name
    end

    it 'does not rename if _deleted_ is in the name of a deleted organizations' do
      assert_equal old_name2, org2.reload.name
    end

    it 'renames if _deleted_ is not in the name of a deleted organization' do
      assert_equal "#{org1.id}_deleted_#{old_name1}", org1.reload.name
    end

    it 'renames deleted orgs with long names' do
      assert_equal "#{long_org.id}_deleted_#{long_name.slice(0,215)}", long_org.reload.name
    end
  end
end
