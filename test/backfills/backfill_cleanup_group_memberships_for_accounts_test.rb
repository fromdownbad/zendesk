require_relative "../support/test_helper"
SingleCov.covered!

describe BackfillCleanupGroupMembershipsForAccounts do
  fixtures :accounts
  fixtures :users
  fixtures :groups

  before do
    @agent = users(:minimum_agent)
    @group = groups(:minimum_group)
    @group_membership = @group.memberships.where(user_id: @agent.id).first
    @agent.update_column('roles', 0)
    @global_membership_count = Membership.count
  end

  describe "dry run mode" do
    describe "with dry_run param: #perform" do
      it "when no account ids are given, does not update records" do
        BackfillCleanupGroupMembershipsForAccounts.new(dry_run: true).perform
        m = accounts(:minimum).memberships.find(@group_membership.id)
        assert_not_nil m
      end

      it "when empty params are given, does not update records" do
        do_run
        m = accounts(:minimum).memberships.find(@group_membership.id)
        assert_not_nil m
      end

      it "does not update record out of account scope " do
        do_run(true, [@group.account.id + 1])
        m = accounts(:minimum).memberships.find(@group_membership.id)
        assert_not_nil m
      end

      it "when account given but dry run, it keeps record" do
        do_run(true, [@group.account_id])
        m = accounts(:minimum).memberships.find(@group_membership.id)
        assert_not_nil m
      end
    end
  end

  describe "run mode" do
    describe "with dry_run param: #perform" do
      it "when no account ids are given, does not update group_membershp record" do
        BackfillCleanupGroupMembershipsForAccounts.new(dry_run: false).perform
        m = accounts(:minimum).memberships.find(@group_membership.id)
        assert_not_nil m
        c = Membership.count
        assert_equal @global_membership_count, c
      end

      it "when empty params are given, does not update user_audit record" do
        do_run(false, [])
        m = accounts(:minimum).memberships.find(@group_membership.id)
        assert_not_nil m
        c = Membership.count
        assert_equal @global_membership_count, c
      end

      it "when non existant account id is given, does not update user_audit record" do
        do_run(false, [@group.account.id + 9657])
        m = accounts(:minimum).memberships.find(@group_membership.id)
        assert_not_nil m
        c = Membership.count
        assert_equal @global_membership_count, c
      end

      it "deletes the membership record" do
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_nil m
        c = Membership.count
        assert_equal @global_membership_count - 1, c
      end
      it "deletes the membership record when user SOFT deleted" do
        @agent.update_column('roles', Role::AGENT.id)
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_not_nil m
        @agent.update_column('is_active', false)
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_nil m
        c = Membership.count
        assert_equal @global_membership_count - 1, c
      end

      it "deletes the membership record when deleted and end user role" do
        @agent.update_column('roles', Role::END_USER.id)
        @agent.update_column('is_active', false)
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_nil m
        c = Membership.count
        assert_equal @global_membership_count - 1, c
      end

      it "deletes the membership record when user HARD deleted" do
        @agent.update_column('roles', Role::AGENT.id)
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_not_nil m
        User.connection.exec_delete("DELETE FROM `users` where account_id=#{@group.account_id} and id=#{@agent.id}", 'DELETE', [])
        u = accounts(:minimum).users.where(id: @agent.id).first
        assert_nil u
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_nil m
        c = Membership.count
        assert_equal @global_membership_count - 1, c
      end
      it "deletes the membership record when group SOFT deleted" do
        @agent.update_column('roles', Role::AGENT.id)
        @group.update_column('is_active', false)
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_nil m
        c = Membership.count
        assert_equal @global_membership_count - 2, c
      end
      it "deletes the membership record when group HARD deleted" do
        @agent.update_column('roles', Role::AGENT.id)
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_not_nil m
        Group.connection.exec_delete("DELETE FROM `groups` where account_id=#{@group.account_id} and id=#{@group.id}", 'DELETE', [])
        g = accounts(:minimum).groups.where(id: @group.id).first
        assert_nil g
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_nil m
        c = Membership.count
        assert_equal @global_membership_count - 2, c
      end
      it "deletes the membership record when group and user HARD deleted" do
        @agent.update_column('roles', Role::AGENT.id)
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_not_nil m
        Group.connection.exec_delete("DELETE FROM `groups` where account_id=#{@group.account_id} and id=#{@group.id}", 'DELETE', [])
        g = accounts(:minimum).groups.where(id: @group.id).first
        assert_nil g
        User.connection.exec_delete("DELETE FROM `users` where account_id=#{@group.account_id} and id=#{@agent.id}", 'DELETE', [])
        u = accounts(:minimum).groups.where(id: @group.id).first
        assert_nil u
        do_run(false, [@group.account.id])
        m = accounts(:minimum).memberships.where(id: @group_membership.id).first
        assert_nil m
        c = Membership.count
        assert_equal @global_membership_count - 2, c
      end
    end
  end

  def do_run(dry_run = true, account_ids = [])
    BackfillCleanupGroupMembershipsForAccounts.new(dry_run: dry_run, account_ids: account_ids).perform
  end
end
