require_relative "../support/test_helper"
SingleCov.covered! uncovered: 6

describe AddFacebookPageScopedId do
  fixtures :accounts, :users, :facebook_pages, :tickets

  let(:account) { accounts(:minimum) }
  let(:subject) { AddFacebookPageScopedId }
  let(:user1) { users(:minimum_end_user) }
  let(:user2) { users(:minimum_end_user2) }
  let(:app_scoped_id) { '123456789' }
  let(:payload) do
    {
        'name' => 'Face',
        'link' => 'http://www.facebook.com/12345678',
        'locale' => 'en_US',
        'token_for_business' => 'AbxorGA83CTwQ0pY',
        'id' => '123456789',
        'email' => 'face.person@gmail.com',
    }
  end
  let(:fb_url) { 'https://graph.facebook.com/v3.2/pages_id_mapping' }
  let(:write_to_file) { stub }

  describe '.perform ' do
    describe 'when there are multiple FB pages for an account' do
      before do
        User.any_instance.stubs(is_end_user?: true)
        @page1 = facebook_pages(:minimum_facebook_page_1)
        @page1.update_column(:state, Facebook::Page::State::ACTIVE)
        @page2 = facebook_pages(:minimum_facebook_page_3)
        @page2.update_column(:state, Facebook::Page::State::ACTIVE)
      end

      describe 'when a user has interacted with all the pages' do
        before do
          @facebook_identity = UserFacebookIdentity.where(user: user1, account: account).first
          @facebook_identity.value = app_scoped_id
          @facebook_identity.save!
          @profile = Channels::FacebookUserProfile.from_external_id(account, app_scoped_id, user1, payload: payload)

          @ticket1 = tickets(:minimum_1)
          @ticket1.will_be_saved_by(@ticket1.requester)
          @ticket1.via_id = ViaType.FACEBOOK_POST
          @ticket1.via_reference_id = @page1.id
          @ticket1.comment = Comment.new(ticket: @ticket1, author: user1, created_at: Time.now, account: account, body: "test", via_id: ViaType.FACEBOOK_POST)
          @ticket1.save!

          @ticket2 = tickets(:minimum_2)
          @ticket2.will_be_saved_by(@ticket2.requester)
          @ticket2.via_id = ViaType.FACEBOOK_POST
          @ticket2.via_reference_id = @page2.id
          @ticket2.comment = Comment.new(ticket: @ticket2, author: user1, created_at: Time.now, account: account, body: "test", via_id: ViaType.FACEBOOK_POST)
          @ticket2.save!

          @stubbed_headers = {
              'Accept' => '*/*',
              'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
              'Content-Type' => 'application/x-www-form-urlencoded',
              'Host' => 'graph.facebook.com',
              'User-Agent' => 'Ruby'
          }
          @response = {app_scoped_id => "1859875290756388"}

          [@page1, @page2].each do |page|
            form_data = { "access_token" => page.access_token,
                          "appsecret_proof" => OpenSSL::HMAC.hexdigest('SHA256', Zendesk::Configuration.dig!(:facebook_integration, :secret), page.access_token),
                          "user_ids" => "123456789"}
            stub_request(:post, fb_url).with(body: form_data, headers: @stubbed_headers).to_return(body: @response.to_json, headers: {"Content-Type" => "application/json"})
          end
        end

        it 'should create FacebookUserIdentity and FBUserProfile for each page for the given user' do
          AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
          [@page1, @page2].each do |page|
            new_asid = page.id.to_s + "-" + app_scoped_id
            assert_not_nil UserFacebookIdentity.where(account_id: account.id, user_id: user1.id, value: new_asid).first
            assert_not_nil Channels::FacebookUserProfile.where(account_id: account.id, user_id: user1.id, external_id: new_asid).first
            assert_equal Channels::FacebookUserProfile.where(account_id: account.id, user_id: user1.id, external_id: new_asid).first.metadata["page_scoped_id"], "1859875290756388"
          end
        end

        it 'should not create duplicate FacebookUserIdentity and FBUserProfile for the given user' do
          AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
          AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
          [@page1, @page2].each do |page|
            new_asid = page.id.to_s + "-" + app_scoped_id
            assert_equal UserFacebookIdentity.where(account_id: account.id, user_id: user1.id, value: new_asid).count, 1
            assert_equal Channels::FacebookUserProfile.where(account_id: account.id, user_id: user1.id, external_id: new_asid).count, 1
          end
        end

        describe 'when account is inactive' do
          before do
            account.update_attributes!(is_active: false)
          end

          it "doesn't process any pages" do
            AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
            page1_asid = @page1.id.to_s + "-" + app_scoped_id
            assert_equal UserFacebookIdentity.where(account_id: account.id, user_id: user1.id, value: page1_asid).count, 0
          end
        end

        describe "when account isn't serviceable" do
          before do
            account.update_attributes!(is_serviceable: false)
          end

          it "doesn't process any pages" do
            AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
            page1_asid = @page1.id.to_s + "-" + app_scoped_id
            assert_equal UserFacebookIdentity.where(account_id: account.id, user_id: user1.id, value: page1_asid).count, 0
          end
        end

        describe 'when the ticket date is limited' do
          describe 'and ticket_created_after is passed' do
            describe 'and the ticket is older than the specified date' do
              before do
                @ticket1.update_column(:created_at, 2.weeks.ago)
              end

              it "doesn't create any identities for Page 1" do
                AddFacebookPageScopedId.new(account_id: account.id, ticket_created_after: 1.week.ago.to_s, dry_run: false).perform
                page1_asid = @page1.id.to_s + "-" + app_scoped_id
                assert_equal UserFacebookIdentity.where(account_id: account.id, user_id: user1.id, value: page1_asid).count, 0
              end
            end
          end

          describe 'and ticket_created_after is not passed' do
            describe 'and the ticket is more than 1 year old' do
              before do
                @ticket1.update_column(:created_at, 2.years.ago)
              end

              it "doesn't create any identities for Page 1" do
                AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
                page1_asid = @page1.id.to_s + "-" + app_scoped_id
                assert_equal UserFacebookIdentity.where(account_id: account.id, user_id: user1.id, value: page1_asid).count, 0
              end
            end
          end
        end

        describe 'when there are active and inactive pages' do
          before do
            @page2.update_column(:state, Facebook::Page::State::INACTIVE)
            AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
          end

          it 'should only process active pages' do
            page1_asid = @page1.id.to_s + "-" + app_scoped_id
            page2_asid = @page2.id.to_s + "-" + app_scoped_id
            assert_equal UserFacebookIdentity.where(account_id: account.id, user_id: user1.id, value: page1_asid).count, 1
            assert_equal UserFacebookIdentity.where(account_id: account.id, user_id: user1.id, value: page2_asid).count, 0
          end
        end

        describe 'when there is an exception saving FBUserProfile record' do
          it 'should rollback changes to FacebookUserIdentity' do
            assert_raises (ActiveRecord::RecordInvalid) do
              AddFacebookPageScopedId.any_instance.expects(:create_or_update_fb_user_profile).raises(ActiveRecord::RecordInvalid.new(user1))
              AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
            end
            new_asid = @page1.id.to_s + "-" + app_scoped_id
            assert_nil UserFacebookIdentity.where(account_id: account.id, user_id: user1.id, value: new_asid).first
            assert_nil Channels::FacebookUserProfile.where(account_id: account.id, user_id: user1.id, external_id: new_asid).first
          end
        end

        describe 'when FB mapping endpoint returns an error' do
          before do
            @form_data1 = { "access_token" => @page1.access_token,
                            "appsecret_proof" => OpenSSL::HMAC.hexdigest('SHA256', Zendesk::Configuration.dig!(:facebook_integration, :secret), @page1.access_token),
                            "user_ids" => "123456789"}
            @form_data2 = { "access_token" => @page2.access_token,
                            "appsecret_proof" => OpenSSL::HMAC.hexdigest('SHA256', Zendesk::Configuration.dig!(:facebook_integration, :secret), @page2.access_token),
                            "user_ids" => "123456789"}
            @response = {"error" => {"message" => "(#100) There are one or more invalid App Scoped User IDs for current App.", "type" => "OAuthException", "code" => 100, "error_data" => {"0" => 1396182463845601}, "fbtrace_id" => "DPbYtwpO+GU"}}
            @request1 = stub_request(:post, fb_url).with(body: @form_data1, headers: @stubbed_headers).to_return(body: @response.to_json, headers: {"Content-Type" => "application/json"}, status: 400)
            @request2 = stub_request(:post, fb_url).with(body: @form_data2, headers: @stubbed_headers).to_return(body: @response.to_json, headers: {"Content-Type" => "application/json"}, status: 400)
          end

          it 'should retry twice to fetch the mapping' do
            logger = mock('logger')
            logger.expects(:info).at_least_once
            logger.expects(:error).with("Facebook Error. Code: 400, Body: #{@response.to_json}.").at_least_once
            Rails.expects(:logger).returns(logger).at_least_once
            AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
            assert_requested @request1, times: 3
            assert_requested @request2, times: 3
          end
        end
      end

      describe 'when user identity exists for a page' do
        before do
          @facebook_identity = UserFacebookIdentity.create(user: user2, account: account, value: @page1.graph_object_id)
          @profile = Channels::FacebookUserProfile.from_external_id(account, @page1.graph_object_id, user2, payload: payload)
          @ticket = tickets(:minimum_1)
          @ticket.will_be_saved_by(@ticket.requester)
          @ticket.via_id = ViaType.FACEBOOK_POST
          @ticket.via_reference_id = @page1.id
          @ticket.comment = Comment.new(ticket: @ticket, author: user2, created_at: Time.now, account: account, body: "test", via_id: ViaType.FACEBOOK_POST)
          @ticket.save!
        end

        it 'should not update the FacebookUserIdentity and FBUserProfile of the page as the user' do
          AddFacebookPageScopedId.new(account_id: account.id, dry_run: false).perform
          new_asid = @page1.id.to_s + "-" + @page1.graph_object_id
          assert_nil UserFacebookIdentity.where(account_id: account.id, user_id: user2.id, value: new_asid).first
          assert_not_nil UserFacebookIdentity.where(account_id: account.id, user_id: user2.id, value: @page1.graph_object_id).first
        end
      end
    end

    describe 'on a POD' do
      before do
        @second_account = accounts(:extra_large)
      end

      it 'should run the migration for all account on all shards in the current POD' do
        accounts_on_this_shard = Account.shard_local.pluck(:id)
        identities = UserFacebookIdentity.where(account_id: accounts_on_this_shard)
        accounts_with_fb_identity = accounts_on_this_shard & identities
        accounts_with_fb_identity.count.times do
          AddFacebookPageScopedId.any_instance.expects(:backfill_account)
        end
        AddFacebookPageScopedId.new.perform
      end

      describe 'when an exception is raised for an account' do
        before do
          @page3 = facebook_pages(:minimum_facebook_page_4)
          @user = users(:extra_large_end_user)
          User.any_instance.stubs(is_end_user?: true)
          @facebook_identity = UserFacebookIdentity.create(user: @user, account: @second_account, value: app_scoped_id)
        end

        it 'should skip account which raised exception and continue with the next' do
          AddFacebookPageScopedId.any_instance.expects(:backfill_account).with(@second_account.id).raises(ActiveRecord::RecordInvalid.new(user1))
          AddFacebookPageScopedId.any_instance.expects(:backfill_account).with(account.id)
          AddFacebookPageScopedId.new.perform
        end
      end
    end
  end
end
