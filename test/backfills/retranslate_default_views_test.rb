require_relative "../support/test_helper"

SingleCov.covered!

describe RetranslateDefaultViews do
  fixtures :accounts, :rules

  let(:account1) { accounts(:minimum) }
  let(:account2) { accounts(:support) }

  describe 'updating view titles' do
    let(:placeholder_match) { /{{zd.\w+}}/ }

    let(:a1_v1) do
      v = account1.views.first
      v.update_attribute(:title, I18n.t('txt.default.views.your_unsolved.title'))
      v
    end

    let(:a1_v2) do
      v = account1.views.last
      v.update_attribute(:title, I18n.t('txt.default.views.group_unsolved.title'))
      v
    end

    let(:a2_v1) do
      v = account2.views.first
      v.update_attribute(:title, I18n.t('txt.default.views.your_unsolved.title'))
      v
    end

    let(:a2_v2) do
      v = account2.views.last
      v.update_attribute(:title, I18n.t('txt.default.views.group_unsolved.title'))
      v
    end

    let(:all_views) { [a1_v1, a1_v2, a2_v1, a2_v2] }

    before do
      account1.views.each do |v|
        new_view = v.dup
        new_view.account_id = account2.id
        account2.views << new_view
      end
    end

    it 'makes no changes to either account in dry_run' do
      original_titles = all_views.map(&:title).sort

      RetranslateDefaultViews.perform(dry_run: true)

      new_titles = all_views.map(&:reload).map(&:title).sort

      new_titles.must_equal original_titles
      all_views.each do |view|
        view.title.wont_match placeholder_match
      end
    end

    it 'updates only the specified account if account_id option is used' do
      a1_original = [a1_v1.title, a1_v2.title].sort
      a2_original = [a2_v1.title, a2_v2.title].sort

      RetranslateDefaultViews.perform(dry_run: false, account_id: account1.id)

      all_views.map(&:reload)
      a1_new = [a1_v1.title, a1_v2.title].sort
      a2_new = [a2_v1.title, a2_v2.title].sort

      a1_new.wont_equal a1_original
      a2_new.must_equal a2_original
    end

    it 'updates all accounts if no account_id is passed in' do
      original_titles = all_views.map(&:title).sort

      RetranslateDefaultViews.perform(dry_run: false)

      new_titles = all_views.map(&:reload).map(&:title).sort

      new_titles.wont_equal original_titles
      all_views.each do |view|
        view.title.must_match placeholder_match
      end
    end

    it 'updates deleted views' do
      a1_v1.update_attribute(:deleted_at, Time.now)
      a2_v1.update_attribute(:deleted_at, Time.now)

      original_titles = all_views.map(&:title).sort

      RetranslateDefaultViews.perform(dry_run: false)

      new_titles = all_views.map(&:reload).map(&:title).sort

      new_titles.wont_equal original_titles
      all_views.each do |view|
        view.title.must_match placeholder_match
      end
    end

    it 'only updates views that match a default string' do
      a1_v1.update_attribute(:title, 'foo bar')

      v1_original = a1_v1.title
      v2_original = a1_v2.title

      RetranslateDefaultViews.perform(dry_run: false)

      a1_v1.reload.title.must_equal v1_original
      a1_v2.reload.title.wont_equal v2_original
      a1_v2.title.must_match placeholder_match
    end
  end
end
