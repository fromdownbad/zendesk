require_relative "../support/test_helper"

SingleCov.covered!

describe RecordDisplacer do
  fixtures :users, :accounts, :tickets
  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }

  describe "spreading out events based on limit per timestamp" do
    let(:limit_per_timestamp) { 2 }
    let(:dry_run) { false }
    let(:timestamp) { Time.now.to_i }
    let(:params) { { account: account, ticket: ticket, time: Time.at(timestamp), metric: 'agent_work_time', type: 'TicketMetric::Breach' } }
    let!(:ticket_metric_event01) { TicketMetric::Breach.create(params) }
    let!(:ticket_metric_event02) { TicketMetric::Breach.create(params) }
    let!(:ticket_metric_event03) { TicketMetric::Breach.create(params) }
    let!(:ticket_metric_event04) { TicketMetric::Breach.create(params) }
    let!(:ticket_metric_event05) { TicketMetric::Breach.create(params) }
    let!(:ticket_metric_event06) { TicketMetric::Breach.create(params) }
    let!(:ticket_metric_event07) { TicketMetric::Breach.create(params.merge(time: Time.at(timestamp + 1))) }
    let!(:ticket_metric_event08) { TicketMetric::Breach.create(params.merge(time: Time.at(timestamp + 1))) }
    let!(:ticket_metric_event09) { TicketMetric::Breach.create(params.merge(time: Time.at(timestamp + 1))) }
    let!(:ticket_metric_event10) { TicketMetric::Breach.create(params.merge(time: Time.at(timestamp + 2))) }
    let!(:ticket_metric_event11) { TicketMetric::Breach.create(params.merge(time: Time.at(timestamp + 2))) }
    let!(:ticket_metric_event12) { TicketMetric::Breach.create(params.merge(time: Time.at(timestamp + 3))) }

    it "moves events to the following timestamps" do
      RecordDisplacer.new(dry_run: dry_run, account_id: account.id, timestamp: timestamp, limit_per_timestamp: limit_per_timestamp, klass: TicketMetric::Event, time_attribute: :time).perform
      time_range = Time.at(timestamp)..Time.at(timestamp + 6)
      count = TicketMetric::Event.where(account_id: account.id, time: time_range).group(:time).count
      assert_equal [2, 3, 2, 2, 2, 1], count.values
    end

    describe "dry run" do
      let(:dry_run) { true }

      it "doesn't change events" do
        RecordDisplacer.new(dry_run: dry_run, account_id: account.id, timestamp: timestamp, limit_per_timestamp: limit_per_timestamp, klass: TicketMetric::Event, time_attribute: :time).perform
        time_range = Time.at(timestamp)..Time.at(timestamp + 3)
        count = TicketMetric::Event.where(account_id: account.id, time: time_range).group(:time).count
        assert_equal [6, 3, 2, 1], count.values
      end
    end

    describe "#too_klassy" do
      it "finds timestamps with too many entries" do
        response = RecordDisplacer.new(account_id: account.id, timestamp: timestamp, limit_per_timestamp: limit_per_timestamp, klass: TicketMetric::Event, time_attribute: :time).too_klassy
        assert_same_elements [timestamp, timestamp + 1], response.keys
        assert_same_elements [6, 3], response.values
      end
    end
  end
end
