require_relative "../support/test_helper"

SingleCov.covered!

describe DeleteOrphanEntries do
  fixtures :forums, :entries

  let(:solutions) { forums(:solutions) }
  let(:announcements) { forums(:announcements) }

  before do
    solutions.update_column :deleted_at, Time.now
  end

  it "deletes the orphan entries if dry run is false" do
    refute_empty Entry.where(forum_id: solutions.id)

    DeleteOrphanEntries.perform(dry_run: false)

    assert_empty Entry.where(forum_id: solutions.id)
  end

  it "doesn't delete the orphan entries if dry run is true" do
    refute_empty Entry.where(forum_id: solutions.id)

    DeleteOrphanEntries.perform(dry_run: true)

    refute_empty Entry.where(forum_id: solutions.id)
  end

  it "only soft_deletes the orphan entries" do
    refute_empty Entry.unscoped.where(forum_id: solutions.id)

    DeleteOrphanEntries.perform(dry_run: false)

    refute_empty Entry.unscoped.where(forum_id: solutions.id)
  end

  it "doesn't delete entries for a non-deleted forum" do
    refute_empty Entry.where(forum_id: announcements.id)

    DeleteOrphanEntries.perform(dry_run: false)

    refute_empty Entry.where(forum_id: announcements.id)
  end
end
