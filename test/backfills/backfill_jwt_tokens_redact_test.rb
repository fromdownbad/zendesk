require_relative '../support/test_helper'

SingleCov.covered!

describe BackfillJwtTokensRedact do
  fixtures :accounts
  fixtures :users
  fixtures :remote_authentications

  let!(:account) { accounts(:minimum_jwt_saml) }
  let!(:actor) { users(:minimum_jwt_saml_admin) }
  let!(:remote_auth) { remote_authentications(:jwt_remote_authentication) }
  let!(:saml_auth) { remote_authentications(:saml_remote_authentication) }

  describe 'nothing to update if dry run is true' do
    it 'returns how many to redact if dry run is true' do
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Starting dry_run = true, pod = 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: backfill shard 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 0')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Finished dry_run = true, pod = 1')
      BackfillJwtTokensRedact.perform(dry_run: true)
    end

    it 'returns how many to redact if dry run is false' do
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Starting dry_run = false, pod = 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: backfill shard 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 0')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Finished dry_run = false, pod = 1')
      BackfillJwtTokensRedact.perform(dry_run: false)
    end
  end

  describe 'redact all tokens that are not redacted' do
    before do
      event = CIA::Event.create!(account: account, actor: actor, action: 'update', source: remote_auth)
      event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: 'tkfjdksjfkdsafold', new_value: 'DFJSDHFJKDHSJFDSFHDJKS'
      event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: '', new_value: 'AFDFDDFJSDHFJKDHSJFDSFHDJKS'
      event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: 'AFDKET*************************', new_value: 'MFDFDDFJSDHFJKDHSJFDSFHDJKS'
      event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: 'QUEIEOTUEIRTEOUREI', new_value: ''
    end

    it 'returns how many to redact if dry run is true' do
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Starting dry_run = true, pod = 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: backfill shard 1')
      Rails.logger.expects(:info).with('Redacted token: old_value to tkfjdk***********')
      Rails.logger.expects(:info).with('Redacted token: new_value to DFJSDH****************')
      Rails.logger.expects(:info).with('Redacted token: new_value to AFDFDD*********************')
      Rails.logger.expects(:info).with('Redacted token: new_value to MFDFDD*********************')
      Rails.logger.expects(:info).with('Redacted token: old_value to QUEIEO************')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 4')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Successfully corrected 5 tokens')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Finished dry_run = true, pod = 1')
      BackfillJwtTokensRedact.perform(dry_run: true)
    end

    it 'redacts old jwt tokens' do
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Starting dry_run = false, pod = 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: backfill shard 1')
      Rails.logger.expects(:info).with('Redacted token: old_value to tkfjdk***********')
      Rails.logger.expects(:info).with('Redacted token: new_value to DFJSDH****************')
      Rails.logger.expects(:info).with('Redacted token: new_value to AFDFDD*********************')
      Rails.logger.expects(:info).with('Redacted token: new_value to MFDFDD*********************')
      Rails.logger.expects(:info).with('Redacted token: old_value to QUEIEO************')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 4')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Successfully corrected 5 tokens')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Finished dry_run = false, pod = 1')
      BackfillJwtTokensRedact.perform(dry_run: false)
    end
  end

  describe 'redact only jwt tokens' do
    before do
      event = CIA::Event.create!(account: account, actor: actor, action: 'update', source: remote_auth)
      event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: 'tkfjdksjfkdsafold', new_value: 'DFJSDHFJKDHSJFDSFHDJKS'
      saml_event = CIA::Event.create!(account: account, actor: actor, action: 'update', source: saml_auth)
      saml_event.attribute_changes.create account: account, source: saml_auth, attribute_name: 'token', old_value: 'AFDKET*************************', new_value: 'MFDFDDFJSDHFJKDHSJFDSFHDJKS'
    end
    it 'returns how many to redact if dry run is true' do
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Starting dry_run = true, pod = 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: backfill shard 1')
      Rails.logger.expects(:info).with('Redacted token: old_value to tkfjdk***********')
      Rails.logger.expects(:info).with('Redacted token: new_value to DFJSDH****************')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Successfully corrected 2 tokens')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Finished dry_run = true, pod = 1')
      BackfillJwtTokensRedact.perform(dry_run: true)
    end

    it 'does not redact saml tokens' do
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Starting dry_run = false, pod = 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: backfill shard 1')
      Rails.logger.expects(:info).with('Redacted token: old_value to tkfjdk***********')
      Rails.logger.expects(:info).with('Redacted token: new_value to DFJSDH****************')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 1')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Successfully corrected 2 tokens')
      Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Finished dry_run = false, pod = 1')
      BackfillJwtTokensRedact.perform(dry_run: false)
    end
  end
end
