require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillExpireOldOauthTokens do
  fixtures :accounts
  fixtures :users

  let(:account) { accounts(:minimum) }
  let!(:old_token1) { Zendesk::OAuth::Token.where(account_id: account.id, client_id: 1).create!(created_at: Time.parse('2016-09-01')) }
  let!(:old_token2) { Zendesk::OAuth::Token.where(account_id: account.id, client_id: 1).create!(created_at: Time.parse('2016-10-01')) }
  let!(:new_token) { Zendesk::OAuth::Token.where(account_id: account.id, client_id: 1).create! }

  it 'expires old oauth tokens' do
    BackfillExpireOldOauthTokens.perform(dry_run: false)
    assert old_token1.reload.expires_at <= Time.now
    assert old_token2.reload.expires_at <= Time.now
    assert_nil new_token.reload.expires_at
  end
end
