require_relative '../support/test_helper'

SingleCov.covered!

describe BackfillAccountMultiproductField do
  fixtures :accounts

  let(:account) { accounts(:trial) }
  let(:shell_account) { accounts(:shell_account_without_support) }
  let(:legacy_multiproduct_account) { accounts(:multiproduct) }
  let(:soft_deleted_legacy_multiproduct_account) do
    account = accounts(:multiproduct)
    account.update_attributes(deleted_at: DateTime.yesterday, multiproduct: true)
    account
  end

  describe '#perform' do
    describe 'dry run' do
      it 'skips backfill calls to Product API' do
        BackfillAccountMultiproductField.perform(account_ids: [legacy_multiproduct_account.id])
        assert legacy_multiproduct_account.reload.multiproduct
      end
    end

    describe 'actual run' do
      describe 'when account is a shell account' do
        it 'skips the account' do
          assert shell_account.multiproduct
          BackfillAccountMultiproductField.perform(account_ids: [shell_account.id], dry_run: false)
          assert shell_account.reload.multiproduct, "should have skipped account #{shell_account} - #{shell_account.multiproduct}"
        end
      end

      describe 'when account not a shell account' do
        it 'updates flag to false when multiproduct flag is true' do
          assert legacy_multiproduct_account.multiproduct
          BackfillAccountMultiproductField.perform(account_ids: [legacy_multiproduct_account.id], dry_run: false)
          refute legacy_multiproduct_account.reload.multiproduct
        end

        it 'does not affect account if multiproduct flag is already false' do
          refute account.multiproduct
          BackfillAccountMultiproductField.perform(account_ids: [account.id], dry_run: false)
          refute account.reload.multiproduct
        end
      end

      describe 'when legacy multiproduct account is soft deleted' do
        it 'updates flag to false' do
          assert soft_deleted_legacy_multiproduct_account.multiproduct
          BackfillAccountMultiproductField.perform(account_ids: [soft_deleted_legacy_multiproduct_account.id], dry_run: false)
          refute soft_deleted_legacy_multiproduct_account.reload.multiproduct
        end
      end
    end


    describe 'pod option' do
      it 'queries for all accounts within a pod' do
        assert soft_deleted_legacy_multiproduct_account.multiproduct
        BackfillAccountMultiproductField.perform(pod: 1, dry_run: false)
        refute soft_deleted_legacy_multiproduct_account.reload.multiproduct
      end
    end

    describe 'pods option' do
      it 'queries for all accounts within multiple pods' do
        assert soft_deleted_legacy_multiproduct_account.multiproduct
        BackfillAccountMultiproductField.perform(pods: [1, 2], dry_run: false)
        refute soft_deleted_legacy_multiproduct_account.reload.multiproduct
      end
    end
  end
end
