require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillSandboxType do
  fixtures :accounts

  describe '#perform' do
    before do
      sandbox_master = accounts(:minimum)
      @minimum_sandbox = sandbox_master.reset_sandbox
    end

    describe 'dry run is false' do
      let(:dry_run) { false }

      it 'updates sandbox_type' do
        assert_equal 'standard', @minimum_sandbox.settings.sandbox_type
        BackfillSandboxType.perform(account_id: @minimum_sandbox.id, sandbox_type: 'metadata', dry_run: dry_run)
        @minimum_sandbox.reload

        assert_equal 'metadata',  @minimum_sandbox.settings.sandbox_type
      end

      it 'raises when passed a non-sandbox account' do
        assert_raises RuntimeError do
          BackfillSandboxType.perform(account_id: accounts(:minimum).id, sandbox_type: 'metadata', dry_run: dry_run)
        end
      end

      it 'raises when passed an invalid sandbox_type' do
        assert_raises RuntimeError do
          BackfillSandboxType.perform(account_id: @minimum_sandbox.id, sandbox_type: 'foo', dry_run: dry_run)
        end
      end
    end

    describe 'dry run is true' do
      let(:dry_run) { true }
      let(:sandbox_type) {'metadata'}

      it 'does not update sandbox_type' do
        BackfillSandboxType.perform(account_id: @minimum_sandbox.id, sandbox_type: sandbox_type, dry_run: dry_run)
        @minimum_sandbox.reload

        assert_equal 'standard',  @minimum_sandbox.settings.sandbox_type
      end

      it 'logs correctly' do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with("Sandbox Account: #{@minimum_sandbox.id} setting sandbox type to #{sandbox_type}")

        BackfillSandboxType.perform(account_id: @minimum_sandbox.id, sandbox_type: sandbox_type, dry_run: dry_run)
      end
    end
  end
end
