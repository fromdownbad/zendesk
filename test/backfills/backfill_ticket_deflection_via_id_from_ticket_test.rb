require_relative '../support/test_helper'
require_relative '../support/ticket_deflection_test_helper'

SingleCov.covered!

describe BackfillTicketDeflectionViaIdFromTicket do
  fixtures :accounts, :tickets, :ticket_deflections, :brands, :sequences

  include TicketDeflectionTestHelper

  let(:backfill_account) { accounts(:multiproduct) }
  let(:exclude_account) { accounts(:minimum) }
  let(:account_ids) { [backfill_account.id] }
  let(:backfill) { BackfillTicketDeflectionViaIdFromTicket.new(account_ids: account_ids) }
  let(:ticket_via_id) { ViaType.WEB_FORM }

  before do
    backfill_account.tickets.destroy_all
    exclude_account.tickets.delete_all
    Prop.stubs(:throttle).with(:ticket_deflection_account, backfill_account.id.to_s).returns(true)
    Prop.stubs(:throttle).with(:ticket_deflection_account, exclude_account.id.to_s).returns(true)
    backfill.stubs(:puts)
  end

  after do
    TicketDeflection.destroy_all
  end

  describe '#perform' do
    describe 'all existing via_id is nil' do
      before do
        5.times do
          ticket = create_ticket_with_account(account: backfill_account, options: { via_id: ticket_via_id })
          create_ticket_deflection(ticket: ticket, deflection_channel_id: ticket_via_id)
        end

        5.times do
          ticket = create_ticket_with_account(account: exclude_account, options: { via_id: ticket_via_id })
          create_ticket_deflection(ticket: ticket, deflection_channel_id: ticket_via_id)
        end

        backfill.perform
      end

      it 'sets ticket_deflection.via_id from ticket' do
        TicketDeflection.where(account_id: backfill_account.id).map do |deflection|
          assert_equal deflection.ticket.via_id, deflection.via_id
        end
      end

      it 'only process deflection of the given account id' do
        via_id_change = -> { TicketDeflection.where(account_id: exclude_account.id, via_id: nil).count }

        assert_no_difference via_id_change do
          backfill.perform
        end
      end
    end

    it 'does not overwrite existing via_id' do
      existing_deflection_via_type = [nil, 99, nil, 98, nil, 97]
      existing_deflection_via_type.each do |via_id|
        ticket = create_ticket_with_account(account: backfill_account, options: { via_id: ticket_via_id })
        create_ticket_deflection(ticket: ticket, via_id: via_id, deflection_channel_id: ticket_via_id)
      end

      backfill.perform

      expected_deflection_via_type = [ticket_via_id, 99, ticket_via_id, 98, ticket_via_id, 97]
      assert_equal expected_deflection_via_type, TicketDeflection.where(account_id: backfill_account.id).pluck(:via_id)
    end

    describe 'when update ticket deflection throw exception' do
      let!(:ticket) { create_ticket_with_account(account: backfill_account, options: { via_id: ticket_via_id }) }
      let!(:ticket_deflection) { create_ticket_deflection(ticket: ticket, deflection_channel_id: ticket_via_id) }

      it 'rescues and log error' do
        TicketDeflection.any_instance.stubs(:save!).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
        expected_message = "Failed to set ticket_deflection via_id for ticket_deflection_id: #{ticket_deflection.id} account_id: #{backfill_account.id}, error: RecordNotSaved."
        Rails.logger.expects(:info).with(expected_message)

        backfill.perform
      end
    end

    describe 'when exception on perform' do
      it 'rescues and log error' do
        Account.any_instance.stubs(:on_shard).raises(RuntimeError)

        expected_message = "Failed to backfill via_id for account_id: #{backfill_account.id}, error: RuntimeError."
        Rails.logger.expects(:info).with(expected_message)

        backfill.perform
      end
    end
  end
end
