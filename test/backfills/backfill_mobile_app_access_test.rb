require_relative "../support/test_helper"

SingleCov.covered!
describe BackfillMobileAppAccess do
  fixtures :accounts

  let!(:account) { accounts(:minimum) }

  it "makes no changes in dry run mode" do
    account.settings.api_password_access = "0"
    account.settings.save!

    BackfillMobileAppAccess.perform
    assert account.reload.settings.mobile_app_access
  end

  it "disables the mobile_app_access setting if api_password_access is disabled" do
    account.settings.api_password_access = "0"
    account.settings.save!

    BackfillMobileAppAccess.perform(:dry_run => false)
    refute account.reload.settings.mobile_app_access
  end

  it "does nothing if api_password_access is enabled" do
    account.settings.api_password_access = "1"
    account.settings.save!

    BackfillMobileAppAccess.perform(:dry_run => false)
    assert account.reload.settings.mobile_app_access
  end
end
