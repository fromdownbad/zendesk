require_relative "../support/test_helper"

SingleCov.covered!

describe OrganizationMembershipDestroy do
  fixtures :organizations
  fixtures :organization_memberships
  fixtures :users

  let(:account) { accounts(:minimum) }
  let(:described_class) { OrganizationMembershipDestroy }

  describe "destroying memberships for deleted organizations" do
    let(:org1) { organizations(:minimum_organization1) }
    let(:org2) { organizations(:minimum_organization2) }
    let!(:organization_membership) { organization_memberships(:minimum) }
    let(:user) { users(:minimum_end_user) }

    let(:non_default) do
      user.organization_memberships.create!(
        account: account,
        organization: org2,
        created_at: 1.month.ago
      )
    end

    before do
      non_default
      org1.touch_without_callbacks :deleted_at
      org2.touch_without_callbacks :deleted_at
    end

    describe "dry run" do
      it "doesn't destroy the membership" do
        described_class.new.perform
        assert organization_membership.reload
      end
    end

    describe "for one account" do
      it "deletes and destroys the memberships" do
        described_class.new(account_id: account.id, dry_run: false).perform
        assert_raise(ActiveRecord::RecordNotFound) { organization_membership.reload }
        assert_raise(ActiveRecord::RecordNotFound) { non_default.reload }
      end

      it "doesn't destroy the membership of another account" do
        described_class.new(account_id: 12345, dry_run: false).perform
        assert organization_membership.reload
      end
    end
  end
end
