require_relative '../support/test_helper'

SingleCov.covered!

describe BackfillInboundMailRateLimits do
  let(:backfill) { BackfillInboundMailRateLimits.new(dry_run: dry_run) }

  before { InboundMailRateLimit.destroy_all }

  after { InboundMailRateLimit.destroy_all }

  describe "when dry_run is true" do
    let(:dry_run) { true }

    it "does not create inbound mail rate limits" do
      InboundMailRateLimit.any_instance.expects(:new).never
      backfill.perform
    end
  end

  describe "when dry_run is false" do
    let(:dry_run) { false }

    it "creates inbound mail rate limits" do
      InboundMailRateLimit.any_instance.expects(:save!).at_least_once
      backfill.perform
    end

    describe "and there is an exception" do
      it "logs the exception" do
        InboundMailRateLimit.any_instance.stubs(:save!).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
        Rails.logger.expects(:info).with("Backfill will create 30 new entries").once
        Rails.logger.expects(:info).with(regexp_matches(/There was an error backfilling inbound mail rate limit/)).at_least_once
        backfill.perform
      end
    end
  end
end
