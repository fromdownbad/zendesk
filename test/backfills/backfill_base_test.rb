require_relative '../support/test_helper'

SingleCov.covered! uncovered: 1

describe BackfillBase do
  let(:backfill_class) {
    Class.new(BackfillBase) do
      def perform
        "dry_run #{dry_run}, verbose #{verbose}, pod #{pod}, shards #{shards}"
      end
    end
  }

  describe '.perform' do
    it "calls the defined 'perform' method with the provided options" do
      assert_equal(
        'dry_run false, verbose true, pod 1, shards [201]',
        backfill_class.perform(dry_run: false, verbose: true, shards: [201])
      )
    end
  end

  describe '#on_shards' do
    describe 'when shards are specified' do
      let(:backfill) { backfill_class.new(shards: []) }

      it 'iterates through the specified shards' do
        assert_equal [], backfill.on_shards {}
      end
    end

    describe 'when shards are not specified' do
      let(:backfill) { backfill_class.new }

      it 'iterates through all shards on the pod' do
        assert_equal [1], backfill.on_shards {}
      end
    end
  end
end
