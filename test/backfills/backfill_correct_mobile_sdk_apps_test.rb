require_relative "../support/test_helper"
SingleCov.covered!

describe BackfillCorrectMobileSdkApps do
  fixtures :accounts, :users, :user_identities, :mobile_sdk_apps

  before do
    @account = accounts(:minimum_sdk)
    @mobile_sdk_app = @account.mobile_sdk_apps.first
  end

  describe "dry run mode" do
    before do
      @backfill = BackfillCorrectMobileSdkApps.new
    end

    describe "#perform" do
      it "does not update any MobileSdkApp" do
        # Preventing tests output to be cluttered with meaningless output
        Kernel.stubs(:puts).with(any_parameters).returns(true)
        MobileSdkApp.any_instance.expects(:update_attribute).with(any_parameters).never
        @backfill.perform
      end

      it "does print a nice resume of the changes that are gonna be performed" do
        expected_messages = dry_run_messages_for_accounts_corrections(@backfill.impacted_accounts_with_corrections)

        Kernel.expects(:puts).at_most(expected_messages.size).with { |message| message == expected_messages.shift }
        @backfill.perform
      end
    end

    describe "#correct_mobile_sdk_app" do
      it "does not update any MobileSdkApp" do
        current_mobile_app_id = @mobile_sdk_app.identifier
        assert_not_equal "new_mobile_app_identifier", current_mobile_app_id
        @backfill.correct_mobile_sdk_app(@account.id, @mobile_sdk_app.identifier, "new_mobile_app_identifier")
        assert_equal current_mobile_app_id, @mobile_sdk_app.reload.identifier
      end
    end
  end

  describe "run mode" do
    before do
      @backfill = BackfillCorrectMobileSdkApps.new(dry_run: false)
      Kernel.stubs(:puts).with(any_parameters).returns(true)
    end

    describe "#perform" do
      it "updates the mobile_sdk_apps listed on #impacted_accounts_with_corrections" do
        @backfill.stubs(:impacted_accounts_with_corrections).returns(
          @account => { @mobile_sdk_app.identifier => "new_mobile_app_identifier" }
        )

        assert_not_equal "new_mobile_app_identifier", @mobile_sdk_app.identifier
        @backfill.perform
        assert_equal "new_mobile_app_identifier", @mobile_sdk_app.reload.identifier
      end

      # This is because we are gonna run this backfillll on each pod separedly
      it "does not try to process the unexistent accounts listed on #impacted_accounts_with_corrections" do
        assert_nil Account.where(id: 100000).first
        @backfill.stubs(:impacted_accounts_with_corrections).returns(
          100000 => { "current_mobile_app_identifier" => "new_mobile_app_identifier" }
        )

        MobileSdkApp.any_instance.expects(:update_attribute).with(any_parameters).never
        @backfill.perform
      end

      # This is because we are gonna run this backfillll on each pod separedly
      it "does not try to process the unexistent mobile_sdk_apps on existent accounts on #impacted_accounts_with_corrections" do
        assert_nil MobileSdkApp.where(identifier: "unexistent_app_identifier").first
        @backfill.stubs(:impacted_accounts_with_corrections).returns(
          @account => { "unexistent_app_identifier" => "new_mobile_app_identifier" }
        )

        MobileSdkApp.any_instance.expects(:update_attribute).with(any_parameters).never
        @backfill.perform
      end
    end

    describe "#correct_mobile_sdk_app" do
      it "updates the MobileSdkApp" do
        assert_not_equal "new_mobile_app_identifier", @mobile_sdk_app.identifier
        @backfill.correct_mobile_sdk_app(@account.id, @mobile_sdk_app.identifier, "new_mobile_app_identifier")
        assert_equal "new_mobile_app_identifier", @mobile_sdk_app.reload.identifier
      end
    end
  end

  def dry_run_messages_for_accounts_corrections(accounts_with_corrections)
    messages = []
    accounts_with_corrections.each do |account_id, corrections|
      corrections.each do |current_mobile_app_id, new_mobile_app_id|
        messages << "Account #{account_id} MobileSdkApp identifier: '#{current_mobile_app_id}' gonna be corrected with: '#{new_mobile_app_id}'"
      end
    end

    messages
  end
end
