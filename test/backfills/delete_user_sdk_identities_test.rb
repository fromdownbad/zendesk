require_relative "../support/test_helper"

SingleCov.covered!

describe DeleteUserSdkIdentities do
  fixtures :accounts, :users, :user_identities, :device_identifiers
  let(:account) { accounts(:minimum_sdk) }
  let(:user) {users(:minimum_sdk_end_user)}

  it "deletes the user sdk indentities if dry run is false" do
    refute_empty account.users.where(id: user.id).first.identities.sdk
    backfill = DeleteUserSdkIdentities.new({ :dry_run => false, :account_id => account.id, :user_id => user.id })
    backfill.perform

    assert_empty account.users.where(id: user.id).first.identities.sdk
  end

  it "doesn't delete the user sdk indentities if dry run is true" do
    refute_empty account.users.where(id: user.id).first.identities.sdk

    backfill = DeleteUserSdkIdentities.new({ :dry_run => true, :account_id => account.id, :user_id => user.id })
    backfill.perform

    assert account.users.where(id: user.id).first.identities.sdk.any?
  end
end
