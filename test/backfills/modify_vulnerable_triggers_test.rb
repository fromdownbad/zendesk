require_relative "../support/test_helper"

SingleCov.covered!

describe ModifyVulnerableTriggers do
  fixtures :accounts

  let(:account) do
    accounts(:minimum)
  end

  let(:account2) do
    accounts(:multiproduct)
  end

  let(:account_ids) do
    Set[account.id, account2.id]
  end

  describe "modify_vulnerable_triggers" do
    before { Account.any_instance.stubs(:shard).returns(true) }
    describe "when an account has ccs disabled" do
      before { Account.any_instance.stubs(:has_follower_and_email_cc_collaborations_enabled?).returns(false) }
      describe "when dry_run is turned off" do
        before { ModifyVulnerableTriggers.new(account_ids: account_ids, dry_run: false).perform }
        it "modifies the old trigger" do
          modified_trigger = account.reload.triggers.find_by(title: I18n.t("#{ModifyVulnerableTriggers::DEFAULT_NOTIFY_REQUESTER_TRIGGER_KEY}.title"), is_active: true)
          new_notify_requester_trigger_item = DefinitionItem.new("role", "is", ["end_user"])

          assert_equal modified_trigger.definition.conditions_all.last, new_notify_requester_trigger_item
          assert_equal modified_trigger.definition.actions.first.value[1], I18n.t('txt.default.triggers.notify_requester_received.subject_v3')
          assert_equal modified_trigger.definition.actions.first.value[2], I18n.t('txt.default.triggers.notify_requester_received.body_v4').gsub("<br />", "\n")
        end

        it "adds the new proactive trigger" do
          proactive_trigger = account.reload.triggers.find_by(title: I18n.t("#{ModifyVulnerableTriggers::NEW_PROACTIVE_AGENT_TRIGGER_KEY}.title"), is_active: false)

          assert proactive_trigger.definition.conditions_all.include?(DefinitionItem.new("update_type", "is", "Create"))
          assert proactive_trigger.definition.conditions_all.include?(DefinitionItem.new("ticket_is_public", "is", ["public"]))
          assert proactive_trigger.definition.conditions_all.include?(DefinitionItem.new("role", "is", ["agent"]))

          assert_equal proactive_trigger.definition.actions.first.value[0], "requester_id"
          assert_equal proactive_trigger.definition.actions.first.source, "notification_user"
          assert_equal proactive_trigger.definition.actions.first.value[1], I18n.t("#{ModifyVulnerableTriggers::NEW_PROACTIVE_AGENT_TRIGGER_KEY}.subject")
          assert_equal proactive_trigger.definition.actions.first.value[2], I18n.t("#{ModifyVulnerableTriggers::NEW_PROACTIVE_AGENT_TRIGGER_KEY}.body").gsub("<br />", "\n")
        end
      end

      describe 'when an exception is raised for an account' do
        it 'should skip account which raised exception and continue with the next' do
          Rails.logger.expects(:info).with("ModifyVulnerableTriggers: Starting dry_run=false")
          expected_message = "ModifyVulnerableTriggers_Error failed for account_id: #{account.id} with error: StandardError"
          Rails.logger.expects(:info).with(expected_message).once

          ModifyVulnerableTriggers.any_instance.expects(:modify_vulnerable_triggers).at_least_once
          ModifyVulnerableTriggers.any_instance.stubs(:modify_vulnerable_triggers).with(account.id).raises(StandardError)

          ModifyVulnerableTriggers.new(account_ids: account_ids, dry_run: false).perform
        end
      end
    end

    describe "when an account has ccs enabled" do
      before { Account.any_instance.stubs(:has_follower_and_email_cc_collaborations_enabled?).returns(true) }
      describe "when dry_run is turned off" do
        before { ModifyVulnerableTriggers.new(account_ids: account_ids, dry_run: false).perform }
        it "adds the new proactive trigger with cc's option for requester" do
          proactive_trigger = account.reload.triggers.find_by(title: I18n.t("#{ModifyVulnerableTriggers::NEW_PROACTIVE_AGENT_TRIGGER_KEY}.title"), is_active: false)
          assert_equal proactive_trigger.definition.actions.first.value[0], "requester_and_ccs"
        end
      end
    end

    describe "when dry_run is turned on" do
      before { ModifyVulnerableTriggers.new(account_ids: account_ids, dry_run: true).perform }
      it "does not create any changes to the account" do
        notify_trigger = account.triggers.find_by(title: I18n.t("#{ModifyVulnerableTriggers::DEFAULT_NOTIFY_REQUESTER_TRIGGER_KEY}.title"), is_active: true)
        refute_equal notify_trigger.definition.actions.first.value[1], I18n.t('txt.default.triggers.notify_requester_received.subject_v3')
        refute_equal notify_trigger.definition.actions.first.value[2], I18n.t('txt.default.triggers.notify_requester_received.body_v4')

        proactive_trigger = account.triggers.find_by(title: I18n.t("#{ModifyVulnerableTriggers::NEW_PROACTIVE_AGENT_TRIGGER_KEY}.title"), is_active: false)
        assert_nil proactive_trigger
      end
    end
  end
end
