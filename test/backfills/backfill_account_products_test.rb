require_relative '../support/test_helper'

SingleCov.covered!

describe BackfillAccountProducts do
  fixtures :accounts, :translation_locales

  let(:account) { accounts(:trial) }

  let(:system_account) { accounts(:systemaccount) }

  let(:shell_account_without_support) { accounts(:shell_account_without_support) }

  let(:locale) { translation_locales(:english_by_zendesk) }

  let(:precreated_account) do
    account = accounts(:extra_large)
    account.update_column(:name, PreAccountCreation::DEFAULT_ACCOUNT_NAME)
    account.update_column(:subdomain, "z3nprecreated-1215180805")
    account.create_pre_account_creation!(
      pod_id: 1,
      locale_id: locale.id,
      region: 'us',
      source: 'Test Account Creation',
      status: PreAccountCreation::ProvisionStatus::INIT
    )
    account
  end

  let(:soft_deleted_account) do
    account = accounts(:inactive)
    account.update_column(:deleted_at, DateTime.yesterday)
    account
  end

  let(:unknown_state_account) do
    account = accounts(:minimum)
    account.subscription.update_column(:is_trial, false)
    account.subscription.update_column(:payment_method_type, 100)
    account
  end

  let(:subscribed_account) do
    account = accounts(:support)
    account.update_column(:billing_id, "someBillingId")
    account
  end

  describe '#perform' do
    describe 'dry run' do
      it 'skips backfill calls to Product API' do
        Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).never
        BackfillAccountProducts.perform(account_ids: [account.id])
      end
    end

    describe 'actual run' do
      it 'calls out to Product API to backfill accounts' do
        Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).once
        BackfillAccountProducts.perform(account_ids: [account.id], dry_run: false)
      end

      describe 'when account is in precreated state' do
        it 'skips the account' do
          Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).never
          Rails.logger.expects(:info).with("Skipping backfill for account #{precreated_account.idsub}. No product is required.").once
          BackfillAccountProducts.perform(account_ids: [precreated_account.id], dry_run: false)
        end
      end

      describe 'when account is system account' do
        it 'skips the account' do
          Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).never
          BackfillAccountProducts.perform(account_ids: [system_account.id], dry_run: false)
        end
      end

      describe 'when subscription is missing' do
        it 'skips the account' do
          Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).never
          BackfillAccountProducts.perform(account_ids: [shell_account_without_support.id], dry_run: false)
        end
      end

      describe 'when account state is unknown' do
        it 'skips the account' do
          Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).never
          Rails.logger.expects(:info).with("Skipping backfill for account #{unknown_state_account.idsub}. Derived product state is unknown.")
          BackfillAccountProducts.perform(account_ids: [unknown_state_account.id], dry_run: false)
        end
      end

      describe 'when trial_expires_on is nil' do
        it 'calls out to Product API to backfill account' do
          account.subscription.stubs(trial_expires_on: nil)
          Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).with(anything, anything, include_deleted_account: true, context: "backfill_account_products").once
          BackfillAccountProducts.perform(account_ids: [account.id], dry_run: false)
        end
      end

      describe 'when account is soft deleted' do
        it 'calls out to Product API to backfill account' do
          Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).with(anything, anything, include_deleted_account: true, context: "backfill_account_products").once
          BackfillAccountProducts.perform(account_ids: [soft_deleted_account.id], dry_run: false)
        end
      end

      describe 'skip_subscribed option' do
        describe 'when subscribed account' do
          it 'skips account when skip_subscribed is true' do
            assert subscribed_account.zuora_account_id.present?
            Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).never
            BackfillAccountProducts.perform(account_ids: [subscribed_account.id], dry_run: false, skip_subscribed: true)
          end

          it 'does NOT skip account when skip_subscribed is false' do
            assert subscribed_account.billing_id.present?
            Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).once
            BackfillAccountProducts.perform(account_ids: [subscribed_account.id], dry_run: false, skip_subscribed: false)
          end
        end

        describe 'when trial account' do
          it 'does NOT skip account' do
            refute account.zuora_account_id.present?
            Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).once
            BackfillAccountProducts.perform(account_ids: [account.id], dry_run: false, skip_subscribed: true)
          end
        end
      end
    end

    describe 'pod option' do
      it 'queries for all accounts within a pod' do
        Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).at_least_once
        BackfillAccountProducts.perform(pod: 1, dry_run: false)
      end
    end

    describe 'pods option' do
      it 'queries for all accounts within multiple pods' do
        Zendesk::Accounts::Client.any_instance.expects(:update_or_create_product!).at_least_once
        BackfillAccountProducts.perform(pods: [1, 2], dry_run: false)
      end
    end
  end
end
