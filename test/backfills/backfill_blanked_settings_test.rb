require_relative '../support/test_helper'

SingleCov.covered!

describe BackfillBlankedSettings do
  describe '#work' do
    fixtures :accounts, :users

    let!(:account) { accounts(:minimum) }
    let(:affected_account_ids) { [account.id] }
    let(:account_client) { stub }
    let(:work) { BackfillBlankedSettings.perform(account_ids: [account.id]) }

    before do
      Account.stubs(:find).returns(account)
      account.stubs(:shard)
      Zendesk::Accounts::Client.stubs(:new).returns(account_client)
    end

    describe 'inactive account' do
      before do
        account.is_active = false
      end

      it 'skips the account' do
        account_client.expects(:update_product!).never
        work
      end
    end

    describe 'to fix' do
      let(:event_message) { 'Update Explore account product via Account Service' }
      let(:cia_event) { CIA::Event.new(message: event_message) }
      let(:cia_event_collection) { stub }
      let(:changes) { CIA::AttributeChange.new(attribute_name: 'billing_cycle_day', old_value: 'wednesday', new_value: '') }
      let(:changes_collection) { stub }

      before do
        account.stubs(:cia_events).returns(cia_event_collection)
        cia_event_collection.stubs(:where).returns([cia_event])
        cia_event.stubs(:attribute_changes).returns(changes_collection)
        changes_collection.stubs(:where).returns([changes])
      end

      it 'updates product in account service' do
        account_client.expects(:update_product!)
        work
      end

      describe 'blank message' do
        let(:event_message) { nil }

        it 'skips event' do
          account_client.expects(:update_product!).never
          work
        end
      end
    end
  end
end
