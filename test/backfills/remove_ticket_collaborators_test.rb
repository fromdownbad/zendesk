require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe RemoveTicketCollaborators do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket) { account.tickets.first }
  let(:ticket_2) { account.tickets.second }
  let(:user) { account.agents.first }
  let(:time_before) { Time.new(2016, 01, 01) }
  let(:start_time) { Time.new(2016, 01, 02).to_i }
  let(:time_after) { Time.new(2016, 01, 03) }

  before do
    account.tickets.delete_all
    2.times { |i| build_ticket }
    50.times { |i| create_user }
    account.tickets.each { |t| populate_collaborators(t) }
  end

  describe 'perform' do

    describe 'when on a dry run' do
      it 'does not delete any collaboration records' do
        backfill = RemoveTicketCollaborators.new(
        dry_run: true,
        subdomain: account.subdomain,
        start_time: start_time
        )
        assert_equal 59, ticket.collaborators.where(:roles => Role::END_USER.id).map(&:id).count

        backfill.perform
        ticket.collaborators.reload

        assert_equal 59, account.tickets.first.collaborators.where(:roles => Role::END_USER.id).map(&:id).count
      end
    end

    describe 'when not on a dry run' do
      it 'removes excess end user collaborators for tickets generated after the given start_time' do
        backfill = RemoveTicketCollaborators.new(
        dry_run: false,
        subdomain: account.subdomain,
        start_time: start_time
        )
        assert_equal 59, ticket.collaborators.where(:roles => Role::END_USER.id).map(&:id).count

        backfill.perform
        ticket.collaborators.reload

        assert_equal 0, account.tickets.first.collaborators.where(:roles => Role::END_USER.id).map(&:id).count
      end

      it 'does not remove collaborators that are not end users' do
        backfill = RemoveTicketCollaborators.new(
        dry_run: false,
        subdomain: account.subdomain,
        start_time: start_time
        )
        assert_equal 63, ticket.collaborator_ids.count

        backfill.perform
        ticket.collaborators.reload

        assert_equal 4, ticket.collaborator_ids.count
      end

      it 'ignores tickets before the given start_time' do
        backfill = RemoveTicketCollaborators.new(
        dry_run: false,
        subdomain: account.subdomain,
        start_time: start_time
        )
        assert_equal 59, ticket.collaborators.where(:roles => Role::END_USER.id).map(&:id).count

        reset_ticket_time ticket_2, time_before
        backfill.perform
        ticket.collaborators.reload

        assert_equal 59, ticket_2.collaborators.where(:roles => Role::END_USER.id).map(&:id).count
      end

      it 'ignores tickets with an end user collaborator count less than the given threshold' do
        backfill = RemoveTicketCollaborators.new(
        dry_run: false,
        subdomain: account.subdomain,
        start_time: start_time
        )
        account.end_users.where(:name => 'John Doe').destroy_all
        assert_equal 9, ticket.collaborators.where(:roles => Role::END_USER.id).map(&:id).count

        reset_ticket_time ticket, time_after
        backfill.perform
        ticket.collaborators.reload

        assert_equal 9, ticket.collaborators.where(:roles => Role::END_USER.id).map(&:id).count
      end
    end
  end

  def create_user
    user = User.new(
    account: account,
    name: 'John Doe'
    ).save
    user
  end

  def populate_collaborators(ticket)
    agent_ids = account.agents.map(&:id)
    end_user_ids = account.end_users.map(&:id)
    ticket.collaborator_ids = agent_ids + end_user_ids
  end

  def reset_ticket_time(ticket, time)
    ticket.generated_timestamp = time
    ticket.will_be_saved_by user
    ticket.save!
  end

  def next_nice_id
    @nice_id ||= 0
    @nice_id += 1
  end

  def build_ticket(options = {})
    ticket = account.tickets.new
    ticket.assignee = user
    ticket.nice_id = next_nice_id

    attributes = options.merge({
      :subject       => "Thing!",
      :description   => "Boom.",
      :requester     => users(:minimum_end_user),
      :priority_id   => PriorityType.NORMAL
    })
    ticket.attributes = attributes
    ticket.will_be_saved_by user
    ticket.save!

    ticket.reload
  end
end
