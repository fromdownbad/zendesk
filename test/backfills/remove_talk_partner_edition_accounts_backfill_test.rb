require_relative "../support/test_helper"

SingleCov.covered!

describe RemoveTalkPartnerEditionAccountsBackfill do
  let(:account) { accounts(:minimum) }
  let(:vpea) { Voice::PartnerEditionAccount.find_or_create_by_account(account) }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
    Account.any_instance.stubs(:has_talk_cti_partner?)
  end

  it 'deletes regular account without add-on' do
    vpea.update_attributes(plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:regular])
    Account.any_instance.expects(:has_talk_cti_partner?).returns(false)

    RemoveTalkPartnerEditionAccountsBackfill.perform(dry_run: false)

    assert_nil account.reload.voice_partner_edition_account
  end

  it 'does not delete regular account with add-on' do
    vpea.update_attributes(plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:regular])
    Account.any_instance.expects(:has_talk_cti_partner?).returns(true)

    RemoveTalkPartnerEditionAccountsBackfill.perform(dry_run: false)

    refute_nil account.reload.voice_partner_edition_account
  end

  it 'does not delete legacy account without add-on' do
    vpea.update_attributes(plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:legacy])

    RemoveTalkPartnerEditionAccountsBackfill.perform(dry_run: false)

    refute_nil account.reload.voice_partner_edition_account
  end
end
