require_relative '../support/test_helper'

SingleCov.covered!

describe EnableSniCertificates do
  fixtures :accounts, :certificates

  let(:account) { accounts(:minimum) }
  let(:certificate) do
    cert = certificates(:active1)
    cert.account = account

    cert.tap(&:save!)
  end

  describe 'when account has active certificate' do
    describe 'with sni enabled false' do
      before do
        certificate.update_attribute(:sni_enabled, false)
      end

      it 'decreases non sni certificates by 1 when limit is 1' do
        assert_difference 'current_non_sni_certificate_count', -1 do
          EnableSniCertificates.perform(dry_run: false, limit: 1)
          certificate.reload
        end
      end

      it 'does nothing if dry run is true' do
        Certificate.any_instance.expects(:convert_to_sni!).never
        EnableSniCertificates.perform(dry_run: true, limit: 1)
        certificate.reload

        assert_equal false, certificate.sni_enabled
      end
    end

    describe 'with sni enabled true' do
      before do
        certificate.update_attribute(:sni_enabled, true)
      end

      it 'does nothing if dry run is false' do
        EnableSniCertificates.perform(dry_run: false, limit: 1)
        certificate.reload

        assert_equal true, certificate.sni_enabled
      end

      it 'does nothing if dry run is true' do
        Certificate.any_instance.expects(:convert_to_sni!).never
        EnableSniCertificates.perform(dry_run: true, limit: 1)
        certificate.reload

        assert_equal true, certificate.sni_enabled
      end
    end
  end

  describe 'when account has inactive certificate' do
    before do
      certificate.update_attribute(:state, 'pending')
    end

    describe 'with sni enabled false' do
      before do
        certificate.update_attribute(:sni_enabled, false)
      end

      it 'does not convert to sni if dry run is false' do
        EnableSniCertificates.perform(dry_run: false, limit: 1)
        certificate.reload

        assert_equal false, certificate.sni_enabled
      end

      it 'does nothing if dry run is true' do
        Certificate.any_instance.expects(:convert_to_sni!).never
        EnableSniCertificates.perform(dry_run: true, limit: 1)
        certificate.reload

        assert_equal false, certificate.sni_enabled
      end
    end
  end

  def current_non_sni_certificate_count
    Certificate.active.where(sni_enabled: false).count
  end
end
