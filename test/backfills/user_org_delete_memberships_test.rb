require_relative "../support/test_helper"

SingleCov.covered!

describe UserOrgDeleteMemberships do
  fixtures :organizations
  fixtures :users
  let(:account) { accounts(:minimum) }
  let(:described_class) { UserOrgDeleteMemberships }

  describe "destroying memberships for deleted organizations" do
    let(:org1) { organizations(:minimum_organization1) }
    let(:org2) { organizations(:minimum_organization2) }
    let(:user) { users(:minimum_end_user) }

    describe "when organization_memberships contain deleted organization" do
      before do
        org1.update_column(:deleted_at, Time.now)
        org2.update_column(:deleted_at, Time.now)
      end

      describe "when running on the account" do
        before do
          Organization.with_deleted do
            user.organization_memberships.where(default: true).first.
              update_column(:default, nil)
          end
        end

        it "deletes the memberships" do
          described_class.new(account_id: account.id, dry_run: false).perform
          user.reload
          assert_empty user.organization_memberships
        end
      end

      describe "when running on the shard" do
        before do
          Organization.with_deleted do
            user.organization_memberships.where(default: true).first.
              update_column(:default, nil)
          end
        end

        it "deletes the memberships" do
          described_class.new(shard_id: account.shard_id, dry_run: false).perform
          user.reload
          assert_empty user.organization_memberships
        end
      end

      describe "when running on the pod" do
        before do
          Organization.with_deleted do
            user.organization_memberships.where(default: true).first.
              update_column(:default, nil)
          end
        end

        it "deletes the memberships" do
          described_class.new(dry_run: false).perform
          user.reload
          assert_empty user.organization_memberships
        end
      end

      describe "when the account is deleted" do
        before do
          account.is_active = false
          account.is_serviceable = false
          account.save!
          account.soft_delete!
        end

        it "doesn't blow up" do
          described_class.new(dry_run: false).perform
        end
      end
    end
  end
end
