require_relative "../support/test_helper"

SingleCov.covered!

describe SetTrialExpirationDateForOpenEndedTpeTrials do
  let(:account) { accounts(:minimum) }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
  end

  describe 'when talk partner edition is Legacy' do
    before do
      FactoryBot.create(:voice_partner_edition_account,
        account_id: account.id,
        plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:legacy],
        trial_expires_at: nil
      )
    end

    it 'sets talk partner edition account trial to false but it does not update talk partner edition trial_expires_at value' do
      SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: false)

      assert_equal false, account.voice_partner_edition_account.trial
      assert_nil account.voice_partner_edition_account.trial_expires_at
    end
  end

  describe 'when talk partner edition is Regular' do
    before do
      FactoryBot.create(:voice_partner_edition_account,
        account_id: account.id,
        plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:regular],
        trial_expires_at: nil,
        trial: true
      )
    end

    describe 'when account has talk partner edition addon' do
      before do
        account.subscription_feature_addons.create!(
          name: 'talk_cti_partner',
          zuora_rate_plan_id: '123456'
        )
        Zendesk::Features::SubscriptionFeatureService.new(account).execute!
      end

      it 'does not change trial nor trial_expires_at values when run in dry mode' do
        SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: true)

        assert_equal true, account.voice_partner_edition_account.trial
        assert_nil account.voice_partner_edition_account.trial_expires_at
      end

      it 'sets talk partner edition account trial to false and makes no changes to trial_expires \
        at value when run without dry run mode' do
        SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: false)

        assert_equal false, account.voice_partner_edition_account.trial
        assert_nil account.voice_partner_edition_account.trial_expires_at
      end
    end

    describe 'when account has talk partner edition subscription' do
      before do
        FactoryBot.create(:tpe_subscription, account_id: account.id)
      end

      it 'does not change trial nor trial_expires_at values when run in dry mode' do
        trial = account.voice_partner_edition_account.trial
        SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: true)

        assert_equal trial, account.voice_partner_edition_account.trial
        assert_nil account.voice_partner_edition_account.trial_expires_at
      end

      it 'sets talk partner edition account trial to false and makes no changes to trial_expires at \
        value when run without dry run mode' do
        SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: false)

        assert_equal false, account.voice_partner_edition_account.trial
        assert_nil account.voice_partner_edition_account.trial_expires_at
      end
    end

    describe 'when account has no talk partner edition addon nor subscription' do
      before { account.stubs(:subscription_feature_addons).returns([]) }

      describe 'when talk partner edition is active' do

        describe 'when talk partner edition account trial_expires value is nil' do
          it 'does not change trial nor trial_expires_at values when run in dry mode' do
            SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: true)

            assert_equal true, account.voice_partner_edition_account.trial
            assert_nil account.voice_partner_edition_account.trial_expires_at
          end

          it 'sets talk partner edition account trial to true and sets trial_expires_at date to be \
            30 days from now when run without dry mode' do
            SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: false)

            assert_equal true, account.voice_partner_edition_account.trial
            assert_equal account.voice_partner_edition_account.trial_expires_at, 30.days.from_now.to_date
          end
        end

        describe 'when talk partner edition account has trial_expires value already set' do
          let(:expiration_date) { 15.days.from_now }
          before do
            travel_to Time.now
            account.voice_partner_edition_account.update_attributes(trial_expires_at: expiration_date)
          end

          after do
            travel_back
          end

          it 'does not change trial nor trial_expires_at values when run in dry mode' do
            SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: true)

            assert_equal true, account.voice_partner_edition_account.trial
            assert_equal expiration_date, account.voice_partner_edition_account.trial_expires_at
          end

          it 'sets trial to true but does not change the trial_expires_at value when run without dry mode' do
            SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: false)

            assert_equal true, account.voice_partner_edition_account.trial
            assert_equal expiration_date, account.voice_partner_edition_account.trial_expires_at
          end
        end
      end

      describe 'when talk partner edition is inactive' do
        before do
          account.voice_partner_edition_account.update_attributes(active: false)
        end

        it 'does not change trial nor trial_expires_at values when run in dry mode' do
          SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: true)

          assert_equal true, account.voice_partner_edition_account.trial
          assert_nil account.voice_partner_edition_account.trial_expires_at
        end

        it 'sets trial to true and trial_expires_at value to nil when run without dry mode' do
          SetTrialExpirationDateForOpenEndedTpeTrials.perform(dry_run: false)

          assert_equal true, account.voice_partner_edition_account.trial
          assert_nil account.voice_partner_edition_account.trial_expires_at
        end
      end
    end
  end
end
