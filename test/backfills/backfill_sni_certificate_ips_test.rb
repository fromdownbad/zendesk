require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillSniCertificateIps do
  fixtures :accounts, :certificates, :certificate_ips

  let(:account) { accounts(:minimum) }
  let(:certificate) do
    cert = certificates(:active1)
    cert.account = account
    cert.tap(&:save!)
  end

  describe "with a sni cert" do
    before do
      certificate.certificate_ips.destroy_all
      certificate.update_attribute(:sni_enabled, true)
    end

    it "doesn't create an sni ip record in dry_run mode" do
      BackfillSniCertificateIps.perform(dry_run: true)
      assert_empty certificate.certificate_ips
    end

    it "creates a sni ip record if the certificate doesn't have one" do
      BackfillSniCertificateIps.perform(dry_run: false)
      certificate.reload

      assert_equal 1, certificate.certificate_ips.size
      assert_equal 1, certificate.certificate_ips[0].pod_id
      assert_equal true, certificate.certificate_ips[0].sni
      assert_nil certificate.certificate_ips[0].ip
    end

    it "does nothing if the certificate alreay has a sni ip record" do
      certificate.tap(&:build_sni_pod_record).tap(&:save!)
      BackfillSniCertificateIps.perform(dry_run: false)
      certificate.reload

      assert_equal 1, certificate.certificate_ips.size
      assert_equal 1, certificate.certificate_ips[0].pod_id
      assert_equal true, certificate.certificate_ips[0].sni
      assert_nil certificate.certificate_ips[0].ip
    end
  end

  it "doesn't touch ip based certificates" do
    assert_equal false, certificate.sni_enabled
    assert_equal 1, certificate.certificate_ips.size
    refute_nil certificate.certificate_ips[0].ip
    ip = certificate.certificate_ips[0].ip

    BackfillSniCertificateIps.perform(dry_run: false)
    certificate.reload

    assert_equal 1, certificate.certificate_ips.size
    assert_equal ip, certificate.certificate_ips[0].ip
  end
end
