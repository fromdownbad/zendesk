require_relative '../support/test_helper'

SingleCov.covered! uncovered: 1

describe BackfillRemoveVoiceSeatsOffInactiveUsers do
  fixtures :accounts, :users

  let(:account_1) { accounts(:minimum) }
  let(:account_2) { accounts(:support) }
  let(:user_1)    { account_1.users.last }
  let(:user_2)    { account_2.users.last }
  let(:voice_subscription) { stub( is_active?: false, max_agents: 2, plan_type:  3) }

  before do
    Account.any_instance.stubs(:voice_subscription).returns(voice_subscription)

    stub_request(:post, %r{/voice_seat_lost.json})

    user_1.user_seats.create(account: account_1, seat_type: 'voice').save!
    user_2.user_seats.create(account: account_2, seat_type: 'voice').save!

    user_1.update_column(:is_active, false)
    user_2.update_column(:is_active, false)
  end

  it 'deletes voice seats for inactive users when dry run is false' do
    BackfillRemoveVoiceSeatsOffInactiveUsers.perform(dry_run: false)
    user_1.user_seats.count.must_equal 0
    user_2.user_seats.count.must_equal 0
  end

  it 'does not delete voice seats for active users when dry run is false' do
    user_2.update_column(:is_active, true)
    BackfillRemoveVoiceSeatsOffInactiveUsers.perform(dry_run: false)
    user_1.user_seats.count.must_equal 0
    user_2.user_seats.count.must_equal 1
  end

  it 'does not delete non-voice user seats for inactive users when dry run is false' do
    seat = user_1.user_seats.voice.first
    seat.seat_type = 'explore'
    seat.save!
    BackfillRemoveVoiceSeatsOffInactiveUsers.perform(dry_run: false)
    user_1.user_seats.count.must_equal 1
    user_2.user_seats.count.must_equal 0
  end

  it 'does not delete voice seats for inactive users when dry run is true' do
    BackfillRemoveVoiceSeatsOffInactiveUsers.perform(dry_run: true)
    user_1.user_seats.count.must_equal 1
    user_2.user_seats.count.must_equal 1
  end

  it 'does not attempt to remove voice seat from inactive accounts' do
    account_2.update_column(:deleted_at, Time.now)
    BackfillRemoveVoiceSeatsOffInactiveUsers.perform(dry_run: false)
    user_1.user_seats.count.must_equal 0
    user_2.user_seats.count.must_equal 1
  end
end
