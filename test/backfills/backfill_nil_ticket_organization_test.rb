require_relative '../support/test_helper'

SingleCov.covered!

describe BackfillNilTicketOrganization do
  fixtures :accounts, :tickets, :organizations, :organization_memberships
  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:user) { users(:minimum_end_user) }
  let(:ticket) { user.tickets.not_closed.first }
  let(:ticket2) { user.tickets.not_closed.last }
  let(:backfill) { BackfillNilTicketOrganization }

  describe_with_arturo_enabled :unset_ticket_org_when_removing_default_user_org do
    describe "when tickets have nil organization" do
      before do
        ticket.organization_id = nil
        ticket.will_be_saved_by agent
        ticket.save!

        ticket2.organization_id = nil
        ticket2.will_be_saved_by agent
        ticket2.save!
      end

      describe "with account_id" do
        it "updates to the requester's org" do
          backfill.new(account_id: account.id, dry_run: false).perform

          assert_equal ticket.requester.organization_id, ticket.reload.organization_id
        end

        describe "with closed tickets" do
          before do
            ticket2.status_id = 4
            ticket2.will_be_saved_by agent
            ticket2.save!
          end

          describe "and update_closed = true" do
            it "also updates Closed tickets" do
              backfill.new(account_id: account.id, update_closed: true, dry_run: false).perform

              assert_equal ticket.requester.organization_id, ticket.reload.organization_id
              assert_equal ticket2.requester.organization_id, ticket2.reload.organization_id
            end
          end

          describe "and update_closed = false" do
            it "doesn't update Closed tickets" do
              backfill.new(account_id: account.id, dry_run: false).perform

              assert_equal ticket.requester.organization_id, ticket.reload.organization_id
              assert_nil ticket2.reload.organization_id
            end
          end
        end

        describe "with tickets created at different times" do
          before do
            ticket.created_at = 1.hour.ago
            ticket.save!

            ticket2.created_at = 1.week.ago
            ticket2.save!
          end

          describe "and a start_time before both" do
            it "updates both" do
              backfill.new(account_id: account.id, start_time: 2.weeks.ago, dry_run: false).perform

              assert_equal ticket.requester.organization_id, ticket.reload.organization_id
              assert_equal ticket2.requester.organization_id, ticket2.reload.organization_id
            end
          end

          describe "and a start_time between them" do
            it "only updates the later one" do
              backfill.new(account_id: account.id, start_time: 1.day.ago, dry_run: false).perform

              assert_equal ticket.requester.organization_id, ticket.reload.organization_id
              assert_nil ticket2.reload.organization_id
            end
          end
        end
      end

      describe "with shard_id" do
        it "updates to the requester's org" do
          backfill.new(shard_id: account.shard_id, dry_run: false).perform

          assert_equal ticket.requester.organization_id, ticket.reload.organization_id
        end
      end

      describe "by whole pod" do
        it "updates to the requester's org" do
          backfill.new(dry_run: false).perform

          assert_equal ticket.requester.organization_id, ticket.reload.organization_id
        end
      end
    end
  end
end
