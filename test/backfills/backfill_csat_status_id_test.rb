require_relative "../support/test_helper"
require_relative "../support/satisfaction_test_helper"

SingleCov.covered!

describe BackfillCsatStatusId do
  include SatisfactionTestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:ticket) { build_ticket_with_satisfaction_rating }
  let(:ticket_with_status) { build_ticket_with_satisfaction_rating }

  before do
    ticket.update_column(:status_id, StatusType.PENDING)
    ticket.satisfaction_rating.update_column(:status_id, nil)
    ticket_with_status.satisfaction_rating.update_column(:status_id, StatusType.OPEN)
  end

  it "makes no changes in dry run mode" do
    BackfillCsatStatusId.perform
    assert_nil ticket.satisfaction_rating.status_id
  end

  it "set status of rating to status of ticket" do
    BackfillCsatStatusId.perform(:dry_run => false)
    refute_nil ticket.satisfaction_rating.reload.status_id
    assert ticket.status_id == ticket.satisfaction_rating.status_id
  end

  it "does not change existing status" do
    BackfillCsatStatusId.perform(:dry_run => false)
    assert ticket_with_status.satisfaction_rating.reload.status_id == StatusType.OPEN
  end

  describe "when ratings have no ticket" do
    let(:rating) { Satisfaction::Rating.create(:score => 0) }

    it "does not fail and does not set status" do
      assert_nil rating.ticket
      BackfillCsatStatusId.perform(:dry_run => false)
      assert_nil rating.status_id
    end
  end
end
