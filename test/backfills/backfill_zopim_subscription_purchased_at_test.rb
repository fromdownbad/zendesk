require_relative "../support/test_helper"
require_relative "../support/zopim_test_helper"

SingleCov.covered!

describe BackfillZopimSubscriptionPurchasedAt do
  include ZopimTestHelper
  fixtures :all

  let(:account) { accounts(:minimum) }

  let(:zuora_subscription) { stub(:zuora_account_id => '123456') }

  before do
    Account.any_instance.stubs(:zuora_managed? => true)

    Zopim::Reseller::client.stubs(
      :create_account! => Hashie::Mash.new(id: 1),
      :account_agents! => [Hashie::Mash.new(id: 1, owner: true)],
      :find_accounts!  => []
    )

    Subscription.any_instance.stubs(:zuora_subscription => zuora_subscription)

    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)

    setup_zopim_stubs
    setup_zopim_subscription(account)
  end

  describe "when the purchased_at column is nil" do
    describe "and the account is self-service" do
      it "makes no change in dry run mode" do
        ZendeskBillingCore::Zuora::Synchronizer.any_instance.expects(:synchronize!).never
        BackfillZopimSubscriptionPurchasedAt.perform
      end

      it "starts a Zuora sync to fill the purchased_at date" do
        ZendeskBillingCore::Zuora::Synchronizer.any_instance.expects(:synchronize!)
          .at_least(1)
        BackfillZopimSubscriptionPurchasedAt.perform(:dry_run => false)
      end
    end
  end

  describe "when the 'purchased at' column contains a value" do
    describe "and the account is self-service" do
      before do
        ZendeskBillingCore::Zopim::Subscription.where(:purchased_at => nil).each do |s|
          s.update_column(:purchased_at, Time.zone.now)
        end
      end

      it "makes no changes" do
        ZendeskBillingCore::Zuora::Synchronizer.any_instance.expects(:synchronize!).never
        BackfillZopimSubscriptionPurchasedAt.perform(:dry_run => false)
      end
    end
  end
end
