require_relative '../support/test_helper.rb'

SingleCov.covered!

describe BackfillBaseAgents do
  fixtures :subscriptions

  let(:subscription)   { subscriptions(:minimum) }
  let(:expected_value) { 42 }
  let(:invalid_value)  { 6 }

  describe 'when the values are different' do
    before do
      subscription.update_column(:max_agents, expected_value)
      subscription.update_column(:base_agents, invalid_value)
    end

    it 'should change the base_agents to equal the max_agents' do
      assert subscription.max_agents != subscription.base_agents
      BackfillBaseAgents.perform(dry_run: false)
      assert_equal subscription.max_agents, subscription.reload.base_agents
    end
  end

  describe 'when base_agents is NULL' do
    before do
      subscription.update_column(:base_agents, nil)
    end

    it 'should change the base_agents to equal the max_agents' do
      assert_nil subscription.base_agents
      BackfillBaseAgents.perform(dry_run: false)
      assert_equal subscription.max_agents, subscription.reload.base_agents
    end
  end
end
