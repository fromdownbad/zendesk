require_relative "../support/test_helper"

SingleCov.covered! uncovered: 40

describe BackfillTicketTags do
  fixtures :accounts, :tickets, :users, :custom_field_options, :ticket_fields

  let(:described_class) { BackfillTicketTags }
  let(:account_one) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_2) }
  let(:uuid) { UUIDTools::UUID.random_create.to_s }

  describe "#perform" do
    before do
      Account.new
    end
    it "Only runs for the given account if account_id is supplied" do
      described_class.any_instance.expects(:update_ticket_for_account).once
      described_class.new({account_id: account_one.id}).perform
    end
    it "Runs for all accounts" do
      described_class.any_instance.expects(:accounts_to_backfill).once
      described_class.new.perform
    end
  end
end
