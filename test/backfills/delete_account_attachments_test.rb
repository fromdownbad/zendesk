require_relative "../support/test_helper"

SingleCov.covered! uncovered: 12

describe DeleteAccountAttachments do
  fixtures :accounts, :attachments, :tickets

  let(:targeted_account) { accounts(:minimum) }
  let(:job_params) do 
    {
      account: targeted_account,
      excluded_tickets: [2, 3],
      start_time: Time.parse("2014-10-7"),
      end_time: Time.parse("2019-8-31")
    }
  end

  let(:included_ticket) { tickets(:minimum_1) }
  let(:excluded_ticket) { tickets(:minimum_2) }
  let!(:archived_excluded_ticket) { tickets(:minimum_3) }

  let!(:valid_attachment) do
    FactoryBot.create(:attachment,
      account: targeted_account,
      created_at: Time.parse("2016-1-1"),
      ticket_id: included_ticket.id
    )
  end
  let!(:excluded_invalid_attachment) do
    FactoryBot.create(:attachment,
      account: targeted_account,
      created_at: Time.parse("2016-1-1"),
      ticket_id: excluded_ticket.id
    )
  end
  let!(:archived_excluded_invalid_attachment) do
    FactoryBot.create(:attachment,
      account: targeted_account,
      created_at: Time.parse("2016-1-1"),
      ticket_id: archived_excluded_ticket.id
    )
  end
  let!(:time_invalid_attachment) do
    FactoryBot.create(:attachment,
      account: targeted_account,
      created_at: Time.parse("2012-1-1"),
      ticket_id: included_ticket.id
    )
  end
  
  before { archived_excluded_ticket.archive! }
  
  describe 'when run in dry mode' do
    before(:all) { DeleteAccountAttachments.new(job_params).perform }

    describe 'attachment inside the time range' do
      describe 'without a ticket in the exclusion list' do
        it 'is not deleted' do
          assert Attachment.exists?(valid_attachment.id)
        end 
      end
    end
  end

  describe 'when run in production mode' do
    before(:all) { DeleteAccountAttachments.new(job_params.merge(dry_run: false)).perform }
    
    describe 'attachment inside the time range' do
      describe 'without a ticket in the exclusion list' do
        it 'is deleted' do
          refute Attachment.exists?(valid_attachment.id)
        end        
      end
    end
    
    describe 'with a ticket in the exclusion list' do
      it 'is not deleted' do
        assert Attachment.exists?(excluded_invalid_attachment.id)
      end 
    end
    
    describe 'with an archived ticket in the exclusion list' do
      it 'is not deleted' do
        assert Attachment.exists?(archived_excluded_invalid_attachment.id)
      end 
    end

    describe 'attachment outside the time range' do
      it 'is not deleted' do
        assert Attachment.exists?(time_invalid_attachment.id)
      end
    end
  end

  describe '#excluded_ticket_ids' do
    describe 'when CSV file and Array is specified' do
      let(:csv_file) { Rails.root.join('test', 'backfills', 'excluded_ticket_ids.csv') }
      let(:expected_ids) { Set[excluded_ticket.id, archived_excluded_ticket.id] }
      let(:converted_ids) { DeleteAccountAttachments.new(job_params.merge(exclusion_csv: csv_file)).send(:excluded_ticket_ids) }

      it 'converts nice_ids to ticket ids from csv' do
        assert_equal expected_ids, converted_ids
      end
    end
    
    describe 'when only an exclusion array is specified' do
      let(:expected_ids) { Set[excluded_ticket.id, archived_excluded_ticket.id] }
      let(:converted_ids) { DeleteAccountAttachments.new(job_params).send(:excluded_ticket_ids) }

      it 'converts nice_ids to ticket ids from given array' do
        assert_equal expected_ids, converted_ids
      end
    end

    describe 'when no exclusion array is specified' do
      let(:expected_ids) { Set[] }
      let(:converted_ids) { DeleteAccountAttachments.new(job_params.except(:excluded_tickets)).send(:excluded_ticket_ids) }

      it 'returns an empty set' do
        assert_equal expected_ids, converted_ids
      end
    end
  end
end
