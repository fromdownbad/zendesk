require_relative '../support/test_helper'
SingleCov.covered!

describe FacebookPsidMigrateToExternalId do
  fixtures :accounts, :facebook_pages

  let(:account) { accounts(:minimum) }
  let(:subject) { FacebookPsidMigrateToExternalId }
  let(:user) { users(:minimum_end_user) }
  let(:external_id) { '1-123123' }
  let(:psid) { 'psid123' }

  let(:payload) do
    {
      'name' => 'Face',
      'link' => 'http://www.facebook.com/12345678',
      'locale' => 'en_US',
      'token_for_business' => 'AbxorGA83CTwQ0pY',
      'id' => '123',
      'email' => 'face.person@gmail.com',
      'page_scoped_id' => psid
    }
  end

  describe '.perform' do
    before do
      @page1 = facebook_pages(:minimum_facebook_page_1)
      @page1.update_column(:state, Facebook::Page::State::ACTIVE)
      @page2 = facebook_pages(:minimum_facebook_page_3)
      @page2.update_column(:state, Facebook::Page::State::ACTIVE)

      # create an expected profile with psid on the external id field and a user identity for it
      Channels::FacebookUserProfile.from_external_id(account, external_id, user, payload: payload)
      UserFacebookIdentity.create(user: user, account: account, value: external_id)
      @subject = FacebookPsidMigrateToExternalId.new(dry_run: false)
    end

    it 'fixes user profile external_id' do
      @subject.perform
      updated_profile = Channels::FacebookUserProfile.where(account_id: account.id).first
      assert_equal psid, updated_profile.external_id
    end

    it 'fix user identity value' do
      @subject.perform
      updated_profile = Channels::FacebookUserProfile.where(account_id: account.id).first
      updated_identity = UserFacebookIdentity.where(user: user, account: account, value: updated_profile.external_id).first
      assert_not_nil updated_identity
      assert_equal psid, updated_identity.value
    end

    describe 'when there is a duplicate FacebookUserProfile' do
      before do
        Channels::FacebookUserProfile.from_external_id(account, psid, user, payload: payload)
      end

      it 'deletes the duplicate FacebookUserProfile' do
        duplicate_profile_id = Channels::FacebookUserProfile.where(account_id: account.id, external_id: psid).pluck(&:id).first
        @subject.perform
        duplicate_profile = Channels::FacebookUserProfile.where(id: duplicate_profile_id)
        assert_equal duplicate_profile.any?, false
      end
    end

    describe 'with account_id' do
      before do
        @subject = FacebookPsidMigrateToExternalId.new(account_id: account.id, dry_run: false)
        @subject.perform
        @updated_profile = Channels::FacebookUserProfile.where(account_id: account.id).first
      end

      it 'fixes user profile external_id' do
        assert_equal psid, @updated_profile.external_id
      end

      it 'fixes user identity value' do
        updated_identity = UserFacebookIdentity.where(user: user, account: account, value: @updated_profile.external_id).first
        assert_not_nil updated_identity
        assert_equal psid, updated_identity.value
      end
    end

    describe 'when metadata[\'page_scoped_id\'] is nil' do
      before do
        payload.delete('page_scoped_id')
        @profile = Channels::FacebookUserProfile.from_external_id(account, external_id, user, payload: payload)
        @subject = FacebookPsidMigrateToExternalId.new(account_id: account.id, dry_run: false)
        @subject.perform
        @updated_profile = Channels::FacebookUserProfile.find(@profile.id)
      end

      it 'skips the profile and identity update' do
        assert_equal external_id, @updated_profile.external_id
        facebook_identity = UserFacebookIdentity.where(user: user, account: account, value: @updated_profile.external_id).first
        assert_not_nil facebook_identity
        assert_equal external_id, facebook_identity.value
      end
    end

    describe 'with multiple pages' do
      let(:external_id2) { '3-321312' }
      let(:psid2) { 'PSID2222' }
      let(:payload2) do
        {
          'name' => 'FacePage2',
          'link' => 'http://www.facebook.com/87654321',
          'locale' => 'en_US',
          'token_for_business' => 'AbxorGA83CTwTtAa',
          'id' => '456',
          'email' => 'facepage2.person@gmail.com',
          'page_scoped_id' => psid2
        }
      end

      before do
        Channels::FacebookUserProfile.from_external_id(account, external_id2, user, payload: payload2)
        UserFacebookIdentity.create(user: user, account: account, value: external_id2)
      end

      it 'updates all identities' do
        @subject.perform
        updated_profile = Channels::FacebookUserProfile.where(account_id: account.id).first
        updated_profile2 = Channels::FacebookUserProfile.where(account_id: account.id).second

        first_identity = UserFacebookIdentity.where(user: user, account: account, value: updated_profile.external_id).first
        second_identity = UserFacebookIdentity.where(user: user, account: account, value: updated_profile2.external_id).first

        assert_equal psid, first_identity.value
        assert_equal psid2, second_identity.value
      end

      describe 'when there is an exception updating FacebookUserProfile' do
        before do
          Channels::FacebookUserProfile.any_instance.stubs(:update_attribute).raises(ActiveRecord::RecordInvalid.new(user))
        end

        it 'logs the exception' do
          FacebookPsidMigrateToExternalId.any_instance.expects(:log_update_failure).times(2)
          @subject.perform
        end

        it 'rollsback changes to FacebookUserIdentity' do
          assert_nil UserFacebookIdentity.where(user: user, account: account, value: psid).first
          assert_nil UserFacebookIdentity.where(user: user, account: account, value: psid2).first
          @subject.perform
        end
      end
    end
  end
end
