require_relative '../support/test_helper'

SingleCov.covered!

describe FixSandboxesActingAsSpoke do
  fixtures :all

  let!(:z3ndinosaur) { accounts(:minimum) }

  before do
    z3ndinosaur.update_attribute(:subdomain, 'z3ndinosaur1465413090')
    z3ndinosaur.reload
    z3ndinosaur.subscription.update_attributes!(hub_account_id: 3)
  end

  describe 'dry_run' do
    it 'logs what it would do' do
      Rails.logger.expects(:info).with('FixSandboxesActingAsSpoke: Starting dry_run=true')
      Rails.logger.expects(:info).with('This backfill would change hub_account_id from 3 to nil for account z3ndinosaur1465413090')

      FixSandboxesActingAsSpoke.perform
    end
  end

  describe 'real run' do
    it 'changes the hub_account_id to nil' do
      FixSandboxesActingAsSpoke.perform dry_run: false

      assert_nil z3ndinosaur.subscription.reload.hub_account_id
    end
  end
end
