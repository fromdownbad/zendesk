require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 10

describe DurableBackfill::BackfillAudit do
  let(:accounts) { Account.last(4) }
  let(:task_title) { 'test_backfill' }
  let(:current_time) { '2019-02-01 12:30' }
  let(:audits) do
    accounts.map { |account| DurableBackfill::BackfillAudit.create!(task: task_title, account: account) }
  end
  let(:audit) { audits.first }

  describe '#start!' do
    it 'sets audit started_at with current time' do
      assert_nil audit.started_at
      audit.expects(:save!).once

      Timecop.freeze(current_time) do
        audit.start!
        assert_equal audit.started_at, Time.parse(current_time)
      end
    end
  end

  describe '#complete!' do
    it 'sets audit completed_at with current time' do
      assert_nil audit.completed_at
      audit.expects(:save!).once

      Timecop.freeze(current_time) do
        audit.complete!
        assert_equal audit.completed_at, Time.parse(current_time)
      end
    end
  end

  describe '.mark_as_shard_completed' do
    let(:waterline_audit) { DurableBackfill::BackfillAudit.unscoped.where(account_id: -10, task: task_title).first }
    let(:mark_as_shard_completed) { DurableBackfill::BackfillAudit.mark_as_shard_completed(task_title) }

    # create waterline audit first
    before { DurableBackfill::BackfillAudit.waterline_date(task_title) }

    it 'marks waterline audit as completed' do
      mark_as_shard_completed
      refute_nil waterline_audit.completed_at
    end
  end

  describe '.check_backfill_progress' do
    let(:progress) { DurableBackfill::BackfillAudit.for_task(task_title).check_backfill_progress }

    it 'returns a hash with backfill progress hash as value' do
      audits
      assert_equal progress, { :completed=>0, :in_progress=>0, :left=>4 }
    end
  end

  describe '.complete_percentage' do
    let(:percentage) { DurableBackfill::BackfillAudit.for_task(task_title).complete_percentage }

    describe 'when no audits are created' do
      it 'returns 0' do
        assert_equal percentage, 0
      end
    end

    describe 'when audits are created but nothing is completed' do
      it 'returns 0' do
        audits
        assert_equal percentage, 0
      end
    end

    describe 'when one audit is completed' do
      before { audits.first.complete! }

      it 'returns 25' do
        assert_equal percentage, 25
      end
    end

    describe 'when all audits are completed' do
      before { audits.each(&:complete!) }

      it 'returns 100' do
        assert_equal percentage, 100
      end
    end
  end
end
