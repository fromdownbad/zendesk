require_relative '../../support/test_helper'

SingleCov.covered!

class DurableBackfill::RunnerTestTask < DurableBackfill::Task
  self.title = 'test task'

  def work(*)
    Rails.cache.write('durable-backfill-runner-test', self.class.title)
  end
end

describe DurableBackfill::Runner do
  let(:shards) { [1] }
  let(:statsd_client) { stub_for_statsd }
  let(:runner) { DurableBackfill::Runner.new(shards) }

  before do
    # Reconnecting causes a transation commit, which will flush our audits, so fake the
    # reconnection during these tests.
    ActiveRecord::Base.connection.stubs(disconnect!: true)
    ActiveRecord::Base.connection.stubs(reconnect!: true)

    DurableBackfill::Manager.stubs(:tasks).returns([DurableBackfill::RunnerTestTask])
    DurableBackfill::BackfillAudit.without_arsi.delete_all
    DurableBackfill::RunnerTestTask.create_audits!
    DurableBackfill::Manager.stubs(:statsd).returns(statsd_client)
  end

  describe '#run' do
    describe 'with multiple shards' do
      let(:shards) { [1, 2, 3] }

      it 'works on every db shard' do
        ActiveRecord::Base.expects(:on_shard).with(1)
        ActiveRecord::Base.expects(:on_shard).with(2)
        ActiveRecord::Base.expects(:on_shard).with(3)

        runner.run
      end
    end

    it 'reports task complete progress for each shard' do
      statsd_client.expects(:gauge).with('completed', 0, tags: ['task:test task', 'shard_id:1'])
      statsd_client.expects(:gauge).with('left', instance_of(Integer), tags: ['task:test task', 'shard_id:1'])

      runner.run
    end

    it 'performs each task' do
      # ensure we execute the audit, maybe more than once depending on the number of accounts
      DurableBackfill::RunnerTestTask.any_instance.expects(:work_audit).at_least_once

      runner.run
    end

    describe 'when task execution raises errors' do
      before do
        DurableBackfill::RunnerTestTask.any_instance.stubs(:work_audit).raises(StandardError, 'something is wrong')
        Kernel.stubs(:puts)
      end

      after do
        Kernel.unstub(:puts)
      end

      it 'reports error to Rollbar and continue' do
        ZendeskExceptions::Logger.expects(:record).with(instance_of(StandardError), anything).at_least_once
        runner.run
      end
    end
  end
end
