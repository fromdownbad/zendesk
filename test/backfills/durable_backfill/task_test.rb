require_relative '../../support/test_helper'

SingleCov.covered!

describe DurableBackfill::Task do
  module DurableBackfill
    class TaskTestSample < Task
      self.title = 'task_test_sample'

      attr_accessor :worked

      def initialize(audit)
        super
        @worked = false
      end

      def work(_account)
        @worked = true
      end
    end

    class TaskWithoutWork < Task; end

    class AbstractTask < Task
      self.abstract = true
    end

    class RescheduledTask < Task
      self.title = 'rescheduled_task'

      def work(_)
        reschedule!
      end
    end
  end

  fixtures(:all)

  def prep_acct_for_create_audits(account, override_atts = {})
    account.update_columns({
      created_at: Time.now - 10.years,
      lock_state: ::Account::LockState::OPEN,
      is_active: 1,
      is_serviceable: 1
    }.merge(override_atts))
    account
  end

  let(:account) { accounts(:minimum) }
  let(:logger) { DurableBackfill::Manager.logger }

  before do
    logger.stubs(:info)
    logger.stubs(:error)
    DurableBackfill::BackfillAudit.without_arsi.delete_all
  end

  describe '.audits' do
    it 'returns audits for the task' do
      prep_acct_for_create_audits(account)
      ::DurableBackfill::TaskTestSample.create_audits!
      audits = ::DurableBackfill::TaskTestSample.audits
      refute audits.empty?
      audits.each { |audit| assert_equal 'task_test_sample', audit.task }
    end

    it 'returns no audits for an abstract task' do
      assert_equal [], DurableBackfill::AbstractTask.audits
    end
  end

  describe '.create_audits!' do
    it 'creates a work audit for each account' do
      ::DurableBackfill::TaskTestSample.create_audits!
      assert Account.count > 0
      assert_equal Account.where('id != -1').count, DurableBackfill::TaskTestSample.audits.count
    end

    it 'does not create audits for abstract tasks' do
      DurableBackfill::BackfillAudit.expects(:create!).never

      DurableBackfill::AbstractTask.create_audits!
    end

    describe "account is created later than waterline date" do
      it 'does not create audit' do
        prep_acct_for_create_audits(account, created_at: Time.now + 2.days )
        ::DurableBackfill::TaskTestSample.create_audits!
        refute DurableBackfill::BackfillAudit.where(account_id: account.id, task: ::DurableBackfill::TaskTestSample.title).exists?
      end
    end

    describe 'and account is system account' do
      let(:system_account) { accounts(:systemaccount) }

      it 'does not create audit' do
        assert_equal system_account.id, -1
        ::DurableBackfill::TaskTestSample.create_audits!
        assert_equal 0, DurableBackfill::TaskTestSample.audits.where(account_id: -1).count
      end
    end

    describe 'when create_audits! is called multiple times' do
      it 'does not create duplicated audits' do
        ::DurableBackfill::TaskTestSample.create_audits!
        ::DurableBackfill::TaskTestSample.create_audits!
        assert_equal Account.where('id != -1').count, DurableBackfill::TaskTestSample.audits.count
      end

      describe 'when account is moved to the shard between create_audits! calls' do
        let(:account) do
          FactoryBot.create(:account, name: 'linyaos happy account', subdomain: 'linlinlin').tap do |account|
            account.created_at = Time.now - 1.day
            account.save!
          end
        end

        before do
          ::DurableBackfill::TaskTestSample.create_audits!
          account # execute the let now to force account creation
          ::DurableBackfill::TaskTestSample.create_audits!
        end

        it 'creates audit for moved account' do
          assert_equal 1, DurableBackfill::TaskTestSample.audits.where(account_id: account.id).count
        end
      end
    end
  end

  describe '.abstract?' do
    it 'returns true for a task declaring it is abstract' do
      assert_equal true, DurableBackfill::AbstractTask.abstract?
    end
  end

  describe 'instance methods' do
    let(:audit) { DurableBackfill::BackfillAudit.where(account_id: account.id).first }
    let(:task) { ::DurableBackfill::TaskTestSample.new(audit) }

    before do
      prep_acct_for_create_audits(account)
      ::DurableBackfill::TaskTestSample.create_audits!
    end

    describe '#work_audit' do
      before do
        Timecop.freeze
      end

      it 'returns true on success' do
        task.work_audit
      end

      it 'records execution time to StatsD' do
        Zendesk::StatsD::Client.any_instance.expects(:time).with('execute.time', tags: ['task:task_test_sample'])
        task.work_audit
      end

      it 'sets started_at' do
        task.work_audit
        assert_equal Time.now.to_i, audit.reload.started_at.to_i
      end

      it 'sets completed_at' do
        task.work_audit
        assert_equal Time.now.to_i, audit.reload.completed_at.to_i
      end

      it 'calls #work' do
        task.work_audit
        assert task.worked
      end

      it 'logs completion' do
        logger.expects(:info).with("completed task_test_sample for account_id: #{account.id}")
        task.work_audit
      end

      it 'records completion to StatsD' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('execute.success', tags: ['task:task_test_sample'])
        task.work_audit
      end

      it 'raises an error if no work is defined' do
        task = ::DurableBackfill::TaskWithoutWork.new(audit)
        assert_raises(DurableBackfill::Task::MissingWorkImplementation) do
          task.work_audit
        end
      end

      describe 'audit is rescheduled' do
        let(:task) { ::DurableBackfill::RescheduledTask.new(audit) }

        it 'does not set completed_date for audit' do
          refute task.work_audit
          assert_nil audit.reload.completed_at
        end

        it 'logs the rescheduling' do
          logger.expects(:info).with("rescheduling rescheduled_task for account_id: #{account.id}")
          refute task.work_audit
        end
      end

      describe 'backfil job raises exception' do
        before do
          task.expects(:work).raises(Kragle::ServerError)
        end

        it 'raises ExecutionFailed error' do
          assert_raises(::DurableBackfill::Task::ExecutionFailed) do
            task.work_audit
          end
        end

        it 'does not set completed_date for audit' do
          assert_raises(::DurableBackfill::Task::ExecutionFailed) do
            task.work_audit
          end

          assert_nil audit.reload.completed_at
        end

        it 'records error to StatsD' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('execute.error', tags: ['task:task_test_sample', 'error:Kragle::ServerError'])

          assert_raises(::DurableBackfill::Task::ExecutionFailed) do
            task.work_audit
          end
        end

        it 'logs an error' do
          logger.expects(:error).with("failed task_test_sample for account_id: #{account.id}")

          assert_raises(::DurableBackfill::Task::ExecutionFailed) do
            task.work_audit
          end
        end
      end

      describe 'account for audit does not exist' do
        before do
          audit.update_attribute(:account_id, -123456)
        end

        it 'does not perform work' do
          task.expects(:work).never
          task.work_audit
        end

        it 'destroys audit' do
          task.work_audit
          assert audit.destroyed?
        end
      end

      describe 'account for audit is not on shard' do
        before do
          ActiveRecord::Base.stubs(:current_shard_id).returns(account.shard_id + 1)
        end

        it 'does not perform work' do
          task.expects(:work).never
          task.work_audit
        end
      end
    end

    describe '#reschedule!' do
      let(:task) { ::DurableBackfill::RescheduledTask.new(audit) }

      it 'raises RescheduleTask exception' do
        assert_raises(::DurableBackfill::Task::RescheduleTask) do
          task.reschedule!
        end
      end

      it 'records to StatsD' do
        Zendesk::StatsD::Client.
          any_instance.
          expects(:increment).
          with('execute.reschedule', tags: ['task:rescheduled_task'])

        task.work_audit
      end
    end

    describe '#title' do
      it 'returns the classes title attribute' do
        assert_equal 'task_test_sample', DurableBackfill::TaskTestSample.new(audit).title
      end
    end

    describe '#abstract?' do
      let(:task) { ::DurableBackfill::AbstractTask.new(audit) }

      it 'returns true for an abstract task' do
        assert_equal true, task.abstract?
      end
    end
  end
end
