require_relative '../../support/test_helper'

# We stub the results of zendesk_configuration, so the hash building lines aren't tested
SingleCov.covered! uncovered: 2

class DurableBackfill::OverAbstraction < DurableBackfill::Task
  self.abstract = true
end

class DurableBackfill::TestTask < DurableBackfill::Task
  self.title = 'test task'

  def run
    # Do nothing
  end
end

describe DurableBackfill::Manager do
  let(:manager) { DurableBackfill::Manager }
  let(:db_shards) { [13001, 13002, 13003] }

  before do
    # avoid Consul lookup
    manager.stubs(:db_shards).returns(db_shards)

    # we don't actually want to waste time sleeping in test
    manager.stubs(:sleep)

    # avoid noisy STDOUT logging
    manager.stubs(:logger).returns(Logger.new("/dev/null"))
  end

  describe '.run' do
    it 'calls run_once then sleeps for 5 seconds' do
      manager.expects(:run_once).once
      manager.expects(:sleep).with(5)
      manager.run(true)
    end
  end

  describe '.run_once' do
    let(:runner) { stub('DurableBackfill::Runner') }

    before do
      DurableBackfill::Runner.stubs(:new).with(db_shards).returns(runner)
    end

    describe 'audit generation' do
      before do
        manager.stubs(:run_tasks)
      end

      it 'generates audit records for each task for each account' do
        manager.run_once

        # The -1 is to exclude the system account, which we never backfill
        assert_equal(
          (Account.count - 1) * manager.tasks.count,
          DurableBackfill::BackfillAudit.count
        )
      end

      describe 'progress reporting' do
        let(:statsd_client) { stub_for_statsd }
        before { DurableBackfill::Manager.stubs(:statsd).returns(statsd_client) }

        it 'reports task completion progress' do
          statsd_client.expects(:gauge).with('complete_percentage', instance_of(Integer), tags: ['task:test task'])

          manager.run_once
        end
      end
    end

    describe 'passes shard list to runner to run tasks' do
      before do
        manager.stubs(:generate_audits)
      end

      it 'starts one runner with all db shards' do
        runner.expects(:run)
        manager.run_once
      end
    end
  end

  describe '.tasks' do
    let(:tasks) { manager.tasks }

    it 'returns a list of tasks with work to perform' do
      assert_includes tasks, DurableBackfill::TestTask
      refute_includes tasks, DurableBackfill::OverAbstraction
    end
  end

  describe '.statsd' do
    it 'returns a statsd client' do
      assert_equal Zendesk::StatsD::Client, manager.statsd.class
    end
  end
end
