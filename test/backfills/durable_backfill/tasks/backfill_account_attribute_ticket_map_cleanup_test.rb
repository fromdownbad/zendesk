require_relative '../../../support/test_helper'
require_relative '../../../support/routing'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_account_attribute_ticket_map_cleanup'

SingleCov.covered!

describe DurableBackfill::BackfillAccountAttributeTicketMapCleanup do
  include TestSupport::Routing::Helper

  let(:account) { accounts(:support) }
  let(:attribute_fixtures) { YAML.load_file("#{Rails.root}/test/files/deco/attributes.yml") }
  let(:attribute_value_fixtures) { YAML.load_file("#{Rails.root}/test/files/deco/attribute_values.yml") }
  let(:task) do
    DurableBackfill::BackfillAccountAttributeTicketMapCleanup.
      new(DurableBackfill::BackfillAudit.new)
  end

  before do
    client = Zendesk::Deco::Client.new(account)

    client.stubs(:get).with("attributes").returns(stub('response', body: attribute_fixtures))

    attribute_fixtures["attributes"].map { |a| a["id"] }.each do |attribute_id|
      attribute_value_fixtures["attribute_values"].each { |av| av["attribute_id"] = attribute_id }
      client.stubs(:get).with("attributes/#{attribute_id}/values").returns(stub('response', body: attribute_value_fixtures))
    end

    task.stubs(:deco_client).returns(client)

    DurableBackfill::Manager.logger.stubs(:info)
    DurableBackfill::Manager.logger.stubs(:error)
  end

  describe '#work' do
    let(:valid_attribute_map) do
      new_attribute_ticket_map(
        account: account,
        attribute_id: attribute_fixtures["attributes"].first["id"],
        attribute_value_id: attribute_value_fixtures["attribute_values"].first["id"]
      )
    end
    let(:invalid_attribute_map) { new_attribute_ticket_map(account: account, attribute_id: 1000, attribute_value_id: 400) }

    before do
      valid_attribute_map
      invalid_attribute_map
      account
    end

    describe_with_arturo_enabled :backfill_account_attribute_ticket_map_cleanup do
      it "removes invalid attribute ticket map entries" do
        DurableBackfill::Manager.logger.expects(:info).with("Attempting to delete invalid"\
          " AttributeTicketMap(s) for account: #{account.id}.").at_least_once

        task.work(account)

        account.reload
        refute account.attribute_ticket_map_ids.include? invalid_attribute_map.id
      end

      it "keeps valid attribute ticket map entries" do
        task.work(account)

        account.reload
        assert account.attribute_ticket_map_ids.include? valid_attribute_map.id
      end

      describe "when `ActiveRecord::RecordInvalid` occurs" do
        before do
          AttributeTicketMap.stubs(:soft_delete_all!).raises(ActiveRecord::RecordInvalid.new(valid_attribute_map), "Invalid attribute")
        end

        it 'logs RecordInvalid exception' do
          DurableBackfill::Manager.logger.expects(:error).with("Failed to delete invalid attribute values"\
            " for account: #{account.id}. RecordInvalid: Invalid attribute.").at_least_once

          task.work(account)
        end
      end

      describe "when an unknown exception occurs" do
        let(:title) { DurableBackfill::BackfillAccountAttributeTicketMapCleanup.title }

        before do
          AttributeTicketMap.stubs(:soft_delete_all!).raises(StandardError, "Unknown error")
        end

        it 'logs exception' do
          DurableBackfill::Manager.logger.expects(:error).with("Exception while running #{title}"\
            " for account #{account.id}: Unknown error.").at_least_once

          task.work(account)
        end
      end
    end

    describe_with_arturo_disabled :backfill_account_attribute_ticket_map_cleanup do
      it 'reschedules the task' do
        assert_raises(DurableBackfill::Task::RescheduleTask) do
          task.work(account)
        end
      end
    end
  end
end
