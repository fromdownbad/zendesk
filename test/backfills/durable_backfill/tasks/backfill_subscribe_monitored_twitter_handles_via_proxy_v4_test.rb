require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_subscribe_monitored_twitter_handles_via_proxy_v4'

SingleCov.covered!

describe DurableBackfill::BackfillSubscribeMonitoredTwitterHandlesViaProxyV4 do
  subject { DurableBackfill::BackfillSubscribeMonitoredTwitterHandlesViaProxyV4 }

  fixtures :accounts, :monitored_twitter_handles

  let(:account) { accounts(:minimum) }
  let!(:handle_1) { monitored_twitter_handles(:minimum_monitored_twitter_handle_1) }
  let!(:handle_2) { monitored_twitter_handles(:minimum_monitored_twitter_handle_2) }
  let!(:handle_3) { monitored_twitter_handles(:minimum_monitored_twitter_handle_1) }

  let(:task) do
    subject.new(DurableBackfill::BackfillAudit.new)
  end

  before do
    handles = MonitoredTwitterHandle.where(account_id: account.id)
    handles.update_all(state: 4)

    handles.first.state = 3
    handles.first.save!

    handles.second.state = 3
    handles.second.save!
  end

  describe '.work' do
    describe_with_arturo_disabled :twitter_backfill_monitored_twitter_handles do
      it { assert(task.work(account))  }
    end

    it 'works a SubscribeMonitoredTwitterHandleJob for active handles with mention autoconversion enabled' do
      handles = MonitoredTwitterHandle.where(account_id: account.id).active
      handle = handles.first
      handle.mention_autoconversion_enabled_at = Time.now
      handle.save!

      Channels::Maintenance::SubscribeMonitoredTwitterHandleJob.expects(:work).with(handle.id)
      Channels::Maintenance::SubscribeMonitoredTwitterHandleJob.expects(:work).with(handles.last.id).never
      task.work(account)
    end
  end
end
