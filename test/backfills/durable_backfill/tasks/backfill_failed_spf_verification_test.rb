require_relative "../../../support/test_helper"
require_relative "../../../../app/backfills/durable_backfill/tasks/backfill_failed_spf_verification"

SingleCov.covered!

describe DurableBackfill::BackfillFailedSpfVerification do
  describe "#work" do
    fixtures :accounts

    let(:spf_status_id) { RecipientAddress::SPF_STATUS.fetch(:failed) }
    let(:account) { accounts(:minimum) }

    let(:task) do
      DurableBackfill::BackfillFailedSpfVerification.new(DurableBackfill::BackfillAudit.new)
    end

    describe "#work" do
      before do
        account.recipient_addresses.update_all(updated_at: updated_at, spf_status_id: spf_status_id)
      end

      describe "when account recipient addresses were updated within incident timeframe" do
        let(:updated_at) { DurableBackfill::BackfillFailedSpfVerification::INCIDENT_START_TIME + 1.day }

        it "verifies spf records that have failed verification" do
          assert_not_empty account.recipient_addresses.reload.where(spf_status_id: spf_status_id)
          task.work(account)
          assert_empty account.recipient_addresses.reload.where(spf_status_id: spf_status_id)
        end
      end

      describe "when account recipient addresses were updated outside incident timeframe" do
        let(:updated_at) { DurableBackfill::BackfillFailedSpfVerification::INCIDENT_START_TIME - 1.day }

        it "does not verify spf records that have failed verification" do
          assert_not_empty account.recipient_addresses.reload.where(spf_status_id: spf_status_id)
          task.work(account)
          assert_not_empty account.recipient_addresses.reload.where(spf_status_id: spf_status_id)
        end
      end
    end
  end
end
