require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_update_cfc_ris_manifest_v3'

SingleCov.covered!

describe DurableBackfill::BackfillUpdateCfcRisManifestV3 do
  subject { DurableBackfill::BackfillUpdateCfcRisManifestV3.new(DurableBackfill::BackfillAudit.new) }

  let(:account) { accounts(:minimum) }
  let(:ris_model) { Channels::AnyChannel::RegisteredIntegrationService }

  let!(:cfc_ris) do
    ris = ris_model.new(
        account: account,
        external_id: 'whatsapp',
        name: 'whatsapp',
        search_name: 'whatsapp',
        admin_ui_url: 'https://connector.zendesk.ext.smooch.io/zendesk/whatsapp/admin',
        manifest_url: 'https://connector.zendesk.ext.smooch.io/zendesk/whatsapp/manifest.json'
      )
    ris.account_id = account.id
    ris.save!
    ris
  end

  let!(:other_ris) do
    ris = ris_model.new(
        account: account,
        external_id: 'foo',
        name: 'foo',
        search_name: 'foo',
        admin_ui_url: 'https://foo.example.com/admin',
        manifest_url: 'https://foo.example.com/manifest.json'
      )
    ris.account_id = account.id
    ris.save!
    ris
  end

  let(:manifest_data) do
    [
      {
        'id' => 'social_messaging',
        'version' => '7',
        'name' => 'SocialMessaging',
        'author' => 'Zendesk',
        'push_client_id' => 'smooch',
        'urls' => {
          'admin_ui' => 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/admin',
          'channelback_url' => 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/channelback',
          'event_callback_url' => 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/callback'
        },
        'channelback_files' => true,
        'create_followup_tickets' => false,
      },
      'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/manifest.json'
    ]
  end

  before do
    Channels::AnyChannelApi::Client.stubs(get_manifest: manifest_data)
  end

  describe '#work' do
    it 'uses Any Channel API client to get the manifest' do
      Channels::AnyChannelApi::Client.
        expects(:get_manifest).
        with('https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/manifest.json').
        returns(manifest_data).
        once

      subject.work(account)
    end

    it 'updates the attributes of the ris' do
      subject.work(account)
      cfc_ris.reload

      assert_equal cfc_ris.external_id, 'social_messaging'
      assert_equal cfc_ris.version, '7'
      assert_equal cfc_ris.name, 'SocialMessaging'
      assert_equal cfc_ris.search_name, 'socialmessaging'
      assert_equal cfc_ris.admin_ui_url, 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/admin'
      assert_equal cfc_ris.channelback_url, 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/channelback'
      assert_equal cfc_ris.event_callback_url, 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/callback'
    end

    describe 'when there is an error' do
      before do
        Channels::AnyChannel::RegisteredIntegrationService.any_instance.stubs(:update_attributes!).raises(StandardError, 'No good')
      end

      it 'logs the failure' do
        DurableBackfill::Manager.logger.expects(:info).with(regexp_matches(/Could not update manifest for account/))
        subject.work(account)
      end
    end
  end
end
