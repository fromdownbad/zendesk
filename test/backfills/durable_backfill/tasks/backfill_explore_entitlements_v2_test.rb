require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_explore_entitlements_v2'

SingleCov.covered!

describe DurableBackfill::BackfillExploreEntitlementsV2 do
  describe '#work' do
    fixtures :accounts, :users

    let!(:account) { accounts(:minimum) }
    let(:explore_product) { stub }
    let(:account_client) { stub }

    let(:task) do
      DurableBackfill::BackfillExploreEntitlementsV2.new(DurableBackfill::BackfillAudit.new)
    end

    before do
      Zendesk::Accounts::Client.stubs(:new).returns(account_client)
    end

    describe_with_arturo_disabled :ocp_explore_entitlements_backfill do
      it 'reschedules the task' do
        assert_raises(DurableBackfill::Task::RescheduleTask) { task.work(account) }
      end
    end

    describe_with_arturo_enabled :ocp_explore_entitlements_backfill do
      describe 'without explore product' do
        before do
          account_client.stubs(:product!).raises(Kragle::ResourceNotFound)
        end

        it 'skips the account' do
          Omnichannel::ExploreAccountSyncJob.expects(:perform).never
          task.work(account)
        end
      end

      describe 'with explore product' do
        before do
          account_client.stubs(:product!).with('explore').returns(explore_product)
          explore_product.stubs(:nil?).returns(false)
        end

        it 'performs explore account sync job' do
          Omnichannel::ExploreAccountSyncJob.expects(:perform).with(account_id: account.id)
          task.work(account)
        end
      end
    end
  end
end
