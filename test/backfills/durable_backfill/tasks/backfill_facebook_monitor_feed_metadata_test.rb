require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_facebook_monitor_feed_metadata'

SingleCov.covered!

describe DurableBackfill::BackfillFacebookMonitorFeedMetadata do
  describe '#work' do
    let(:account) { accounts(:minimum) }
    let(:channels_resource) { channels_resources(:minimum) }
    let(:incoming_channels_conversion) { incoming_channels_conversions(:minimum) }
    
    let(:task) do
      DurableBackfill::BackfillFacebookMonitorFeedMetadata.new(DurableBackfill::BackfillAudit.new)
    end

    describe_with_arturo_disabled :facebook_metadata_backfill do
      it 'reschedules the task' do
        assert_raises(DurableBackfill::Task::RescheduleTask) { task.work(account) }
      end
    end

    describe_with_arturo_enabled :facebook_metadata_backfill do
      describe 'when there are no Channels resources that match the criteria' do
        it 'does not perform any action' do
          Channels::Resource.any_instance.expects(:update_attribute).never

          task.work(account)
        end
  
        it 'logs the fact' do
          DurableBackfill::Manager.logger.expects(:info).with(regexp_matches(/No Channels resources to backfill in account #{account.id}/))
  
          task.work(account)
        end
      end

      describe 'when there are Channels resources that match the criteria' do
        before do
          @ic = Channels::IncomingConversion.new(
            account_id: account.id,
            source_id: 2,
            state: 10,
            resource_type: Channels::Constants::FACEBOOK_POST,
            thread_id: 123,
            payload: {},
            metadata: {},
            external_id: 1
          )
          @ic.metadata.merge!({ 'monitor' => :feed })
          @ic.save!

          @res = Channels::Resource.new(
            account_id: account.id,
            source_id: 2,
            state: 10,
            ticket_id: 2,
            comment_id: 2,
            ticket_nice_id: 22,
            resource_type: Channels::Constants::FACEBOOK_POST,
            thread_id: @ic.external_id,
            external_id: @ic.external_id,
            metadata: {}
          )
          @res.save!
        end

        it 'updates the resource metadata with the `monitor` attribute' do
          task.work(account)
          @res.reload
  
          assert_equal @res.metadata.key?('monitor'), true
        end

        describe 'when there is an error' do
          before do
            Channels::Resource.any_instance.stubs(:update_attributes!).raises(ActiveRecord::ActiveRecordError)
          end

          it 'logs the failure' do
            DurableBackfill::Manager.logger.expects(:info).with(regexp_matches(/ActiveRecordError: Could not add { 'monitor' => :feed } to metadata attribute in Channels resource #{@res.id} in account #{@res.account_id}:/))

            task.work(account)
          end
        end
      end
    end
  end
end
