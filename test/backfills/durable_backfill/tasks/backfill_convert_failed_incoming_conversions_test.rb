require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_convert_failed_incoming_conversions.rb'

SingleCov.covered!

describe DurableBackfill::BackfillConvertFailedIncomingConversions do
  subject { DurableBackfill::BackfillConvertFailedIncomingConversions.new(DurableBackfill::BackfillAudit.new) }
  fixtures :accounts, :monitored_twitter_handles

  let(:account) { accounts(:minimum) }
  let(:mth) { monitored_twitter_handles(:minimum_monitored_twitter_handle_1) }
  let(:payload) do
    {
      'id'=>'m_s3neRm-t8ZhzaQXA9OgLLHqv5HZrqhs522YlNT53ciSFirOFCDZNxUrT-YDtm4jL1itWOFRNkxvvja-fhH1C1w',
      'from'=>{
        'name'=>'Motorcentral Sales Corp.',
        'email'=>'663551493762661@facebook.com',
        'id'=>'663551493762661'
      },
      'message'=>'Hi user! Please let us know how we can help you.',
      'created_time'=>'2020-08-05T03:01:04+0000'
    }
  end

  describe '#work' do
    before do
      @icc = Channels::IncomingConversion.create!(
        account_id: account.id,
        external_id: 'abc123',
        thread_id: 'abc123',
        source_id: mth.id,
        resource_type: 'twitter_status',
        state: 30,
        payload: payload,
        metadata: {'conversion_attempt_number': 0}
      )
    end

    describe 'for incoming conversions affected by the incident' do
      before do
        # set the incident time
        @icc.update_attribute(:created_at, Time.new(2020, 8, 5, 03, 01, 01))
        Channels::IncomingConversion.any_instance.expects(:enqueue_job).with('BackfillConvertFailedIncomingConversions#work')
      end

      it 'enqueue an incoming conversion job' do
        subject.work(account)
        assert_equal 0, @icc.reload.metadata['conversion_attempt_number']
      end
    end

    describe 'for failed incoming conversions outside the incident' do
      it 'dont enqueue an incoming conversion job after the incident' do
        Channels::IncomingConversion.any_instance.expects(:enqueue_job).never
        subject.work(account)
      end
    end
  end
end
