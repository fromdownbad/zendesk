require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_custom_security_policy'

SingleCov.covered!

describe DurableBackfill::BackfillCustomSecurityPolicy do
  describe '#work' do
    fixtures :accounts, :role_settings

    let!(:account) { accounts(:minimum) }

    let(:task) do
      DurableBackfill::BackfillCustomSecurityPolicy.new(DurableBackfill::BackfillAudit.new)
    end

    describe_with_arturo_disabled :custom_security_policy_backfill do
      it 'reschedules the task' do
        assert_raises(DurableBackfill::Task::RescheduleTask) { task.work(account) }
      end
    end

    describe_with_arturo_enabled :custom_security_policy_backfill do
      describe 'account with custom agent security policy id and no custom security policy record' do
        before do
          account.role_settings.agent_security_policy_id = Zendesk::SecurityPolicy::Custom.id
          account.role_settings.save!
          assert_nil account.custom_security_policy
        end

        it 'creates a custom security policy record from High settings' do
          Rails.logger.expects(:info).with("Attempting to create a custom_security_policy for #{account.id}")
          task.work(account)
          
          assert_equal Zendesk::SecurityPolicy::Custom.id, account.role_settings.reload.agent_security_policy_id
          assert_not_nil account.reload.custom_security_policy
          assert_equal Zendesk::SecurityPolicy::High.password_duration, account.custom_security_policy.password_duration
          assert_equal Zendesk::SecurityPolicy::High.password_length, account.custom_security_policy.password_length
        end

        describe 'when an exception occurs' do
          before do
            task.stubs(:update_custom_security_policy_record?).raises(StandardError, "Unknown error")
          end

          it 'should log it' do
            ZendeskExceptions::Logger.expects(:record).at_least_once

            task.work(account)
          end
        end
      end

      describe 'account with high agent security policy' do
        before do
          account.role_settings.agent_security_policy_id = Zendesk::SecurityPolicy::High.id
          account.role_settings.save!
          assert_nil account.custom_security_policy
        end

        it 'does not create a custom security policy record' do
          Rails.logger.expects(:info).with("Attempting to create a custom_security_policy for #{account.id}").never
          task.work(account)
          
          assert_equal Zendesk::SecurityPolicy::High.id, account.role_settings.reload.agent_security_policy_id
          assert_nil account.reload.custom_security_policy
        end
      end

      describe 'account with custom agent security policy id and a custom security policy record' do
        before do
          account.role_settings.update_attribute(:agent_security_policy_id, Zendesk::SecurityPolicy::Custom.id)
          CustomSecurityPolicy.build_from_current_policy(account).save!
        end

        it 'does not attempt the backfill' do
          Rails.logger.expects(:info).with("Attempting to create a custom_security_policy for #{account.id}").never

          task.work(account)
        end
      end
    end
  end
end
