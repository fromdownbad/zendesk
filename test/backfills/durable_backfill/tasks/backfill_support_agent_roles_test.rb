require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_support_agent_roles'

SingleCov.covered!

describe DurableBackfill::BackfillSupportAgentRoles do
  describe '#work' do
    let(:account) { Account.new }
    let(:staff_client) { stub }
    let(:has_permission_sets) { true }

    let(:task) do
      DurableBackfill::BackfillSupportAgentRoles.new(DurableBackfill::BackfillAudit.new)
    end

    before do
      Zendesk::StaffClient.stubs(:new).returns(staff_client)
      account.stubs(:has_permission_sets?).returns(has_permission_sets)
    end

    describe_with_arturo_disabled :ocp_support_agent_roles_backfill do
      it 'reschedules the task' do
        assert_raises(DurableBackfill::Task::RescheduleTask) { task.work(account) }
      end
    end

    describe_with_arturo_enabled :ocp_support_agent_roles_backfill do
      describe 'without permission_set' do
        let(:has_permission_sets) { false }

        it 'skips the account' do
          staff_client.expects(:toggle_role!).never
          task.work(account)
        end
      end

      describe 'with permission_set' do
        it 'changes support agent role to assignable' do
          staff_client.expects(:toggle_role!).with(
            name: 'agent', 
            assignable: true
          )
          task.work(account)
        end
      end
    end
  end
end
