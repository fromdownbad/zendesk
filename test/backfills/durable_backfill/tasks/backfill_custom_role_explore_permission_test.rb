require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_custom_role_explore_permission'

SingleCov.covered!

describe DurableBackfill::BackfillCustomRoleExplorePermission do
  describe '#work' do
    let(:account) { accounts(:multiproduct) }
    let(:has_permission_sets) { true }
    let!(:explore_access) do
      account.permission_sets.last.permissions.create(name: 'explore_access', value: permission_value)
    end
    let(:permission_value) { 'edit' }

    let(:task) do
      DurableBackfill::BackfillCustomRoleExplorePermission.new(DurableBackfill::BackfillAudit.new)
    end

    before do
      account.stubs(:has_permission_sets?).returns(has_permission_sets)
    end

    describe_with_arturo_disabled :ocp_custom_role_explore_permission_backfill do
      it 'reschedules the task' do
        assert_raises(DurableBackfill::Task::RescheduleTask) { task.work(account) }
      end
    end

    describe_with_arturo_enabled :ocp_custom_role_explore_permission_backfill do
      describe 'without permission_set' do
        let(:has_permission_sets) { false }
        let(:permission_value) { 'none' }

        it 'skips the account' do
          task.work(account)
          assert_equal explore_access.reload.value, 'none'
        end
      end

      describe 'with permission_set' do
        let(:has_permission_sets) { true }

        it 'does not change explore access' do
          task.work(account)
          assert_equal explore_access.reload.value, 'edit'
        end

        describe 'when permission value is none' do
          let(:permission_value) { 'none' }

          it 'changes explore permission to readonly' do
            task.work(account)
            assert_equal explore_access.reload.value, 'readonly'
          end
        end
      end
    end
  end
end
