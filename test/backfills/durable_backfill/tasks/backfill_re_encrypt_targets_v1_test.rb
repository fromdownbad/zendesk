require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_re_encrypt_targets_v1'

SingleCov.covered!

describe DurableBackfill::BackfillReEncryptTargetsV1 do
  describe '#work' do
    let(:account) { accounts(:minimum) }
    let(:unencrypted_value) { 'password1234' }
    let(:old_encryption_key_name) { 'TARGET_CREDENTIALS_ENCRYPTION_TEST_KEY_B' }
    let(:new_encryption_key_name) { ENV.fetch('TARGET_CREDENTIALS_ENCRYPTION_KEY') }
    let(:cipher) { ENV.fetch('TARGET_CREDENTIALS_ENCRYPTION_CIPHER_NAME') }
    let(:title) { 'title' }
    let(:target) { Target.create(account: account, title: title) }
    let(:task) do
      DurableBackfill::BackfillReEncryptTargetsV1.new(DurableBackfill::BackfillAudit.new)
    end

    describe 'for targets with an encrypted password' do
      before do
        target.encryption_key_name = old_encryption_key_name
        target.encryption_cipher_name = cipher
        target.encrypted_password = unencrypted_value
        target.encrypt!
        target.save!
      end

      it 'should re-encrypt the encrypted password' do
        encrypted_value_before_re_encrypt = target.encrypted_password

        task.work(account)

        encrypted_value_after_re_encrypt = target.reload.encrypted_password

        refute_equal encrypted_value_before_re_encrypt, encrypted_value_after_re_encrypt
      end

      it 'should set the encryption key name to the new key' do
        task.work(account)

        assert target.encryption_key_name, new_encryption_key_name
      end

      it 'should be decryptable with the new key' do
        task.work(account)

        assert target.decrypted_value_for(:encrypted_password), unencrypted_value
      end
    end

    describe 'for targets with an encrypted token' do
      before do
        target.encryption_key_name = old_encryption_key_name
        target.encryption_cipher_name = cipher
        target.encrypted_token = unencrypted_value
        target.encrypt!
        target.save!
      end

      it 'should re-encrypt the encrypted token' do
        encrypted_value_before_re_encrypt = target.encrypted_token

        task.work(account)

        encrypted_value_after_re_encrypt = target.reload.encrypted_token

        refute_equal encrypted_value_before_re_encrypt, encrypted_value_after_re_encrypt
      end

      it 'should set the encryption key name to the new key' do
        task.work(account)

        assert target.encryption_key_name, new_encryption_key_name
      end

      it 'should be decryptable with the new key' do
        task.work(account)

        assert target.decrypted_value_for(:encrypted_token), unencrypted_value
      end
    end

    describe 'for targets with an encrypted secret value' do
      before do
        target.encryption_key_name = old_encryption_key_name
        target.encryption_cipher_name = cipher
        target.encrypted_secret_value = unencrypted_value
        target.encrypt!
        target.save!
      end

      it 'should re-encrypt the encrypted secret value' do
        encrypted_value_before_re_encrypt = target.encrypted_secret_value

        task.work(account)

        encrypted_value_after_re_encrypt = target.reload.encrypted_secret_value

        refute_equal encrypted_value_before_re_encrypt, encrypted_value_after_re_encrypt
      end

      it 'should set the encryption key name to the new key' do
        task.work(account)

        assert target.encryption_key_name, new_encryption_key_name
      end

      it 'should be decryptable with the new key' do
        task.work(account)

        assert target.decrypted_value_for(:encrypted_secret_value), unencrypted_value
      end
    end

    describe_with_arturo_disabled :backfill_rotate_target_encrypted_values do
      it 'reschedules the task' do
        assert_raises(DurableBackfill::Task::RescheduleTask) { task.work(account) }
      end
    end
  end
end
