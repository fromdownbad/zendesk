require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_ticket_deflection_updated_at'

SingleCov.covered!

describe DurableBackfill::BackfillTicketDeflectionUpdatedAt do
  describe '#work' do
    fixtures :accounts, :ticket_deflections, :ticket_deflection_articles

    let(:account) { accounts(:minimum) }
    let(:deflection) { ticket_deflections(:updated_a_month_ago) }
    let(:recently_updated_article) { ticket_deflection_articles(:updated_1_day_ago) }
    let(:recent_article_updated_at) { recently_updated_article.updated_at }
    let(:deflection_articles) do
      [
        ticket_deflection_articles(:updated_3_days_ago),
        ticket_deflection_articles(:updated_2_days_ago),
        recently_updated_article
      ]
    end

    let(:task) do
      DurableBackfill::BackfillTicketDeflectionUpdatedAt.new(
        DurableBackfill::BackfillAudit.new
      )
    end

    before do
      account
      deflection
      deflection_articles

      DurableBackfill::Manager.logger.stubs(:info)
      DurableBackfill::Manager.logger.stubs(:error)
    end

    describe 'when deflection updated_at is earlier than recent article updated_at' do
      it 'updates ticket_deflection updated_at' do
        task.work(account)

        assert_equal recent_article_updated_at, deflection.reload.updated_at
      end

      it 'logs updated records' do
        DurableBackfill::Manager.logger.expects(:info)
          .with(regexp_matches(%r{Succeeded.*#{deflection.id}}))

        task.work(account)
      end
    end

    describe 'when deflection updated_at is later than recent article updated_at' do
      it 'does not update ticket_deflection updated_at' do
        # update deflection updated_at = Time.now
        deflection.touch
        deflection_updated_at = deflection.reload.updated_at

        task.work(account)

        assert_equal deflection_updated_at, deflection.reload.updated_at
      end
    end

    describe 'when error is encountered' do
      it 'logs exception' do
        DurableBackfill::Manager.logger.expects(:error)
          .with(regexp_matches(%r{Failed.*#{deflection.id}.*Fail to save}))
          .at_least_once

        TicketDeflection.any_instance
          .stubs(:update_column)
          .raises(StandardError, "Fail to save")

        task.work(account)
      end
    end
  end
end
