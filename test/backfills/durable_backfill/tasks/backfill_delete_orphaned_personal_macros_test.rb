require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_delete_orphaned_personal_macros'
require_relative '../../../support/rule'

SingleCov.covered!

describe DurableBackfill::BackfillDeleteOrphanedPersonalMacros do
  include TestSupport::Rule::Helper

  fixtures :accounts, :users

  let(:active_agent)   { users(:minimum_agent) }
  let(:account)        { active_agent.account }
  let(:inactive_agent) { users(:inactive_agent) }
  let(:task) do
    DurableBackfill::BackfillDeleteOrphanedPersonalMacros.new(DurableBackfill::BackfillAudit.new)
  end

  describe '#backfill_delete_orphaned_personal_macros' do
    before do
      Macro.destroy_all

      create_macro(title: 'GA', owner: account, active: true, position: 5)
      create_macro(title: 'OA', owner: active_agent, active: true, position: 3)
    end

    describe_with_arturo_disabled :backfill_delete_orphaned_personal_macros do
      it "should reschedule task" do
        assert_raises(DurableBackfill::Task::RescheduleTask) do
          task.work(account)
        end
      end
    end

    describe_with_arturo_enabled :backfill_delete_orphaned_personal_macros do
      describe "when there are no orphaned macros" do
        it "should not call delete method" do
          Macro.any_instance.expects(:soft_delete_all!).never
          task.work(account)
        end
      end

      describe "when there are orphaned macros" do
        before do
          create_macro(title: 'ZB', owner: inactive_agent, active: true, position: 2)
          create_macro(title: 'ZA', owner: inactive_agent, active: true, position: 1)
          create_macro(title: 'CB', owner: inactive_agent, active: true, position: 6)
          create_macro(title: 'CA', owner: inactive_agent, active: true, position: 4)

          task.work(account)
        end

        it "should delete all orphaned macros" do
          assert_equal 0, inactive_agent.macros.count
        end

        it "should not affect non-orphaned macros" do
          assert_equal 1, account.macros.count
          assert_equal 1, active_agent.macros.count
        end
      end

      describe "when an exception occurs" do
        before do
          create_macro(title: 'ZY', owner: inactive_agent, active: true, position: 2)
          create_macro(title: 'CX', owner: inactive_agent, active: true, position: 3)

          Macro.stubs(:soft_delete_all!).raises(StandardError)
        end

        it 'should log exception' do
          task.expects(:log_exception)
          task.work(account)
        end
      end
    end
  end
end
