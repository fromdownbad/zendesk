require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_ticket_public_if_comment_public'

SingleCov.covered!

describe DurableBackfill::BackfillTicketPublicIfCommentPublic do

  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:account_id) { 9277979 }
  let(:owner) { account.owner }
  let(:ticket) { tickets(:minimum_1) }
  let(:ticket2) { tickets(:minimum_2) }

  let(:task) do
    DurableBackfill::BackfillTicketPublicIfCommentPublic.new(DurableBackfill::BackfillAudit.new)
  end

  describe 'test backfill' do
    it 'updates private tickets to public if there are public comments' do
      ticket.add_comment(body: 'this is a comment', is_public: true)
      ticket.will_be_saved_by(owner)
      ticket.save!

      ticket.update_column(:is_public, false)

      account.stubs(:id).returns(account_id)
      assert ticket.is_private?
      task.work(account)
      ticket.reload
      assert ticket.is_public?
    end

    it 'does not update tickets correctly marked as private' do
      ticket.public_comments.update_all(is_public: false)
      ticket.update_column(:is_public, false)

      account.stubs(:id).returns(account_id)
      assert ticket.is_private?
      task.work(account)
      ticket.reload
      ticket.events.reload
      assert ticket.is_private?
    end

    it 'does not update tickets for any other account' do
      ticket.add_comment(body: 'this is a comment', is_public: true)
      ticket.will_be_saved_by(owner)
      ticket.save!

      ticket.update_column(:is_public, false)

      assert ticket.is_private?
      task.work(account)
      ticket.reload
      assert ticket.is_private?
    end
  end
end
