require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_satisfaction_rating_comment_event_ids'

SingleCov.covered!

describe DurableBackfill::BackfillSatisfactionRatingCommentEventIds do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }

  let(:task) do
    DurableBackfill::BackfillSatisfactionRatingCommentEventIds.new(DurableBackfill::BackfillAudit.new)
  end

  before do
    ticket.satisfaction_score = SatisfactionType.GOODWITHCOMMENT
    ticket.satisfaction_comment = "Awesome!"
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.save!
  end

  describe_with_arturo_disabled :backfill_satisfaction_rating_comment_event_ids do
    it "reschedules task" do
      assert_raises DurableBackfill::Task::RescheduleTask do
        task.work(account)
      end
    end
  end

  describe_with_arturo_enabled :backfill_satisfaction_rating_comment_event_ids do
    describe "when the account was created after the fix date" do
      before do
        account.update_attribute(:created_at, DurableBackfill::BackfillSatisfactionRatingCommentEventIds::COMMENT_EVENT_ID_FIX_DATE + 1.day)
      end

      it "does nothing" do
        task.expects(:find_satisfaction_ratings).never
        task.work(account)
      end
    end

    describe "when the account was created before the fix date" do
      before do
        account.update_attribute(:created_at, DurableBackfill::BackfillSatisfactionRatingCommentEventIds::COMMENT_EVENT_ID_FIX_DATE - 1.day)
      end

      describe "when the satisfaction rating has a comment_event_id that points to a satisfaction comment event" do
        it "does not update the comment_event_id" do
          comment_event_id = ticket.satisfaction_rating.comment_event_id
          task.work(account)
          assert_equal comment_event_id, ticket.satisfaction_rating.reload.comment_event_id
        end
      end

      describe "when the satisfaction rating has a comment_event_id that points to a non-existent satisfaction comment event" do
        before do
          ticket.satisfaction_rating.update_column(:comment_event_id, 3287345232)
        end

        it "does not update the comment_event_id" do
          comment_event_id = ticket.satisfaction_rating.comment_event_id
          task.work(account)
          assert_equal comment_event_id, ticket.satisfaction_rating.reload.comment_event_id
        end
      end

      describe "when the satisfaction rating does not have a comment_event_id" do
        describe "when the satisfaction rating was created after the fix date" do
          before do
            ticket.satisfaction_rating.update_column(:created_at, DurableBackfill::BackfillSatisfactionRatingCommentEventIds::COMMENT_EVENT_ID_FIX_DATE + 1.day)
          end

          it "does not update the comment_event_id" do
            comment_event_id = ticket.satisfaction_rating.comment_event_id
            task.work(account)
            assert_equal comment_event_id, ticket.satisfaction_rating.reload.comment_event_id
          end
        end

        describe "when the satisfaction rating was created before the fix date" do
          before do
            ticket.satisfaction_rating.update_column(:created_at, DurableBackfill::BackfillSatisfactionRatingCommentEventIds::COMMENT_EVENT_ID_FIX_DATE - 1.day)
          end

          describe "when the satisfaction rating belongs to an archived ticket" do
            before do
              ticket.archive!
            end

            describe "when a satisfaction comment event can be found with the same parent_id as the event" do
              it "updates the comment_event_id" do
                comment_event_id = ticket.satisfaction_rating.comment_event_id
                ticket.satisfaction_rating.update_column(:comment_event_id, nil)
                task.work(account)
                assert_equal comment_event_id, ticket.satisfaction_rating.reload.comment_event_id
              end
            end
          end

          describe "when the satisfaction rating belongs to an event" do
            before do
              assert_not_nil ticket.satisfaction_rating.event
            end

            describe "when a satisfaction comment event can be found with the same parent_id as the event" do
              it "updates the comment_event_id" do
                comment_event_id = ticket.satisfaction_rating.comment_event_id
                ticket.satisfaction_rating.update_column(:comment_event_id, nil)
                task.work(account)
                assert_equal comment_event_id, ticket.satisfaction_rating.reload.comment_event_id
              end
            end
          end

          describe "when the satisfaction rating does not belong to a ticket or an event" do
            before do
              ticket.satisfaction_rating.update_column(:ticket_id, nil)
              ticket.satisfaction_rating.update_column(:event_id, nil)
            end

            it "does not update the comment_event_id" do
              comment_event_id = ticket.satisfaction_rating.comment_event_id
              task.work(account)
              assert_equal comment_event_id, ticket.satisfaction_rating.reload.comment_event_id
            end
          end
        end
      end
    end
  end
end
