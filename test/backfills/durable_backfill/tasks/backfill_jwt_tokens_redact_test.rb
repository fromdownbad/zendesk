require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_jwt_tokens_redact'

SingleCov.covered!

describe DurableBackfill::BackfillJwtTokensRedact do
  describe '#work' do
    fixtures :accounts
    fixtures :users
    fixtures :remote_authentications

    let!(:account) { accounts(:minimum_jwt_saml) }
    let!(:actor) { users(:minimum_jwt_saml_admin) }
    let!(:remote_auth) { remote_authentications(:jwt_remote_authentication) }
    let!(:saml_auth) { remote_authentications(:saml_remote_authentication) }
    let(:task) do
      DurableBackfill::BackfillJwtTokensRedact.new(DurableBackfill::BackfillAudit.new)
    end

    describe_with_arturo_disabled :jwt_redact_backfill do
      it 'reschedules the task' do
        assert_raises(DurableBackfill::Task::RescheduleTask) { task.work(account) }
      end
    end

    describe_with_arturo_enabled :jwt_redact_backfill do
      before do
        account.stubs(has_jwt_redact_backfill?: true)
      end

      describe 'nothing to update' do
        it 'returns how many to redact' do
          Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 0')
          task.work(account)
        end
      end

      describe 'redact all tokens that are not redacted' do
        before do
          event = CIA::Event.create!(account: account, actor: actor, action: 'update', source: remote_auth)
          event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: 'tkfjdksjfkdsafold', new_value: 'DFJSDHFJKDHSJFDSFHDJKS'
          event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: '', new_value: 'AFDFDDFJSDHFJKDHSJFDSFHDJKS'
          event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: 'AFDKET*************************', new_value: 'MFDFDDFJSDHFJKDHSJFDSFHDJKS'
          event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: 'QUEIEOTUEIRTEOUREI', new_value: ''
        end

        it 'returns how many to redact' do
          Rails.logger.expects(:info).with('Redacted token: old_value to tkfjdk***********')
          Rails.logger.expects(:info).with('Redacted token: new_value to DFJSDH****************')
          Rails.logger.expects(:info).with('Redacted token: new_value to AFDFDD*********************')
          Rails.logger.expects(:info).with('Redacted token: new_value to MFDFDD*********************')
          Rails.logger.expects(:info).with('Redacted token: old_value to QUEIEO************')
          Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 4')
          Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Successfully corrected 5 tokens')
          task.work(account)
        end

        it 'redacts old jwt tokens' do
          Rails.logger.expects(:info).with('Redacted token: old_value to tkfjdk***********')
          Rails.logger.expects(:info).with('Redacted token: new_value to DFJSDH****************')
          Rails.logger.expects(:info).with('Redacted token: new_value to AFDFDD*********************')
          Rails.logger.expects(:info).with('Redacted token: new_value to MFDFDD*********************')
          Rails.logger.expects(:info).with('Redacted token: old_value to QUEIEO************')
          Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 4')
          Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Successfully corrected 5 tokens')
          task.work(account)
        end
      end

      describe 'redact only jwt tokens' do
        before do
          event = CIA::Event.create!(account: account, actor: actor, action: 'update', source: remote_auth)
          event.attribute_changes.create account: account, source: remote_auth, attribute_name: 'token', old_value: 'tkfjdksjfkdsafold', new_value: 'DFJSDHFJKDHSJFDSFHDJKS'
          saml_event = CIA::Event.create!(account: account, actor: actor, action: 'update', source: saml_auth)
          saml_event.attribute_changes.create account: account, source: saml_auth, attribute_name: 'token', old_value: 'AFDKET*************************', new_value: 'MFDFDDFJSDHFJKDHSJFDSFHDJKS'
        end

        it 'does not redact saml tokens' do
          Rails.logger.expects(:info).with('Redacted token: old_value to tkfjdk***********')
          Rails.logger.expects(:info).with('Redacted token: new_value to DFJSDH****************')
          Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Total records for JWT tokens = 1')
          Rails.logger.expects(:info).with('BackfillJwtTokensRedact: Successfully corrected 2 tokens')
          task.work(account)
        end
      end
    end
  end
end
