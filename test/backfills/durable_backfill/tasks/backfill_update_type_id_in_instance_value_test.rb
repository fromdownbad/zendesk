require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_update_type_id_in_instance_value'

SingleCov.covered!(uncovered: 5)

describe DurableBackfill::BackfillUpdateTypeIdInInstanceValue do
  fixtures :tickets, :accounts
  let(:task) do
    DurableBackfill::BackfillUpdateTypeIdInInstanceValue.new(DurableBackfill::BackfillAudit.new)
  end
  let(:ticket) { tickets(:minimum_2) }
  let(:account) { ticket.account }
  let(:ticket_deleted) { tickets(:minimum_deleted) }
  let(:ticket_archived) { tickets(:minimum_archived) }
  let(:ticket_closed) do
    ticket = tickets(:minimum_1)
    ticket.update_column :status_id, StatusType.CLOSED
    ticket
  end
  let(:ticket_from_other_account) do
    ticket = tickets(:minimum_3)
    account = accounts(:support)
    ticket.update_column :account_id, account.id
    ticket
  end
  let(:migrated_ticket) do
    migrated_ticket = tickets(:minimum_3)
    migrated_ticket.update_column :status_id, StatusType.CLOSED
    migrated_ticket
  end

  describe_with_arturo_enabled :sbrv2_instance_value_durable_backfill do
    before { account.stubs(:has_skill_based_ticket_routing?).returns true }

    describe "replacing subtype_id in closed, archived or deleted tickets" do
      before do
        [ticket].each do |current_ticket|
          ['11EA74D156FA6A7BBB9F479DAD4B02E8', '11EA74D15CD98F2CBB9F7FE966B25807'].each do |attribute_value_id|
            DurableBackfill::BackfillUpdateTypeIdInInstanceValue::InstanceValue.create(
              account_id: current_ticket.account_id,
              attribute_value_id: Array(attribute_value_id).pack('H*'),
              type_id: 10,
              instance_id: current_ticket.id,
              subtype_id: 0
            )
          end
        end

        [migrated_ticket].each do |current_ticket|
          ['11EA74D156FA6A7BBB9F479DAD4B02E8', '11EA74D15CD98F2CBB9F7FE966B25807'].each do |attribute_value_id|
            DurableBackfill::BackfillUpdateTypeIdInInstanceValue::InstanceValue.create(
              account_id: current_ticket.account_id,
              attribute_value_id: Array(attribute_value_id).pack('H*'),
              type_id: 10,
              instance_id: current_ticket.id,
              subtype_id: 1
            )
          end
        end

        [ticket_closed, ticket_deleted, ticket_archived, ticket_from_other_account].each do |current_ticket|
          ['11EA74D156FA6A7BBB9F479DAD4B02E8', '11EA74D15CD98F2CBB9F7FE966B25807'].each do |attribute_value_id|
            DurableBackfill::BackfillUpdateTypeIdInInstanceValue::InstanceValue.create(
              account_id: current_ticket.account_id,
              attribute_value_id: Array(attribute_value_id).pack('H*'),
              type_id: 11,
              instance_id: current_ticket.id,
              subtype_id: 0
            )
          end
        end

        archive_and_delete ticket_archived
      end

      describe "#work" do
        before do
          DurableBackfill::BackfillUpdateTypeIdInInstanceValue.any_instance.expects(:killswitch_enabled).with(account).returns(false)
          Zendesk::StatsD::Client.any_instance.expects(:increment).with("durable_backfill_completed", tags: ["subdomain:#{ticket.account.subdomain}", "total_registers:6"]).once
          Zendesk::StatsD::Client.any_instance.expects(:increment).with("database_backoff", tags: ["subdomain:#{ticket.account.subdomain}", "duration:0.1"]).once
          task.work(account)
        end

        it "changes subtype_id to 1 and restore type_id to 10 on closed, deleted and archived tickets skills" do
          [ticket_closed, ticket_deleted, ticket_archived].each do |current_ticket|
            assert_equal 2, DurableBackfill::BackfillUpdateTypeIdInInstanceValue::InstanceValue.where(
              account_id: current_ticket.account_id,
              type_id: DurableBackfill::BackfillUpdateTypeIdInInstanceValue::TICKET_TYPE,
              instance_id: current_ticket.id
            ).count
            assert_equal 2, DurableBackfill::BackfillUpdateTypeIdInInstanceValue::InstanceValue.where(
              account_id: current_ticket.account_id,
              type_id: DurableBackfill::BackfillUpdateTypeIdInInstanceValue::TICKET_TYPE,
              subtype_id: DurableBackfill::BackfillUpdateTypeIdInInstanceValue::TICKET_CLOSED_SUBTYPE,
              instance_id: current_ticket.id
            ).count
          end
        end

        it "doesn't change subtype_id on unclosed ticket" do
          assert_equal 0, DurableBackfill::BackfillUpdateTypeIdInInstanceValue::InstanceValue.where(
            account_id: ticket.account_id,
            type_id: DurableBackfill::BackfillUpdateTypeIdInInstanceValue::TICKET_TYPE,
            subtype_id: DurableBackfill::BackfillUpdateTypeIdInInstanceValue::TICKET_CLOSED_SUBTYPE,
            instance_id: ticket.id
          ).count
        end

        it "doesn't change subtype_id on tickets from other accounts" do
          assert_equal 0, DurableBackfill::BackfillUpdateTypeIdInInstanceValue::InstanceValue.where(
            account_id: ticket_from_other_account.account_id,
            type_id: DurableBackfill::BackfillUpdateTypeIdInInstanceValue::TICKET_SKILLS_CLOSED_TYPE,
            subtype_id: DurableBackfill::BackfillUpdateTypeIdInInstanceValue::TICKET_CLOSED_SUBTYPE,
            instance_id: ticket_from_other_account.id
          ).count
        end
      end

      describe "killswitch is enabled" do
        before do
          DurableBackfill::BackfillUpdateTypeIdInInstanceValue.any_instance.expects(:killswitch_enabled).with(account).returns(true)
        end

        it "reschedules the task" do
          assert_raises(DurableBackfill::Task::RescheduleTask) do
            task.work(account)
          end
        end
      end
    end

    describe "without skill_based_ticket_routing feature" do
      before do
        account.stubs(:has_skill_based_ticket_routing?)
      end

      it "completes successfully" do
        ActiveRecord::Base.expects(:on_shard).never
        task.work(account)
        assert_nil task.work(account)
      end
    end

    describe "for an inactive account" do
      before do
        account.stubs(:is_active?)
      end

      it "completes successfully" do
        ActiveRecord::Base.expects(:on_shard).never
        task.work(account)
        assert_nil task.work(account)
      end
    end
  end

  describe_with_arturo_disabled :sbrv2_instance_value_durable_backfill do
    it "reschedules the task" do
      assert_raises(DurableBackfill::Task::RescheduleTask) do
        task.work(account)
      end
    end
  end

  describe "#killswitch_enabled" do
    describe "arturo is enabled" do
      before do
        Arturo.stubs(:feature_enabled_for?).with(:sbrv2_instance_value_durable_backfill, account).returns(true)
      end

      it "returns false" do
        assert !task.killswitch_enabled(account)
      end
    end

    describe "arturo is disabled" do
      before do
        Arturo.stubs(:feature_enabled_for?).with(:sbrv2_instance_value_durable_backfill, account).returns(false)
      end

      it "returns true" do
        assert task.killswitch_enabled(account)
      end
    end
  end
end
