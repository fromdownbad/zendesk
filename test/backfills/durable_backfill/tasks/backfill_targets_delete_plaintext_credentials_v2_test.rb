require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_targets_delete_plaintext_credentials_v2'

SingleCov.covered!

describe DurableBackfill::BackfillTargetsDeletePlaintextCredentialsV2 do
  describe "#work!" do
    fixtures :accounts, :targets

    let(:support) { accounts(:support) }
    let(:minimum) { accounts(:minimum) }

    let(:task) do
      DurableBackfill::BackfillTargetsDeletePlaintextCredentialsV2.
        new(DurableBackfill::BackfillAudit.new)
    end

    let(:twitter_target)  { targets(:twitter_valid) }
    let(:url_v2_target)   { targets(:url_v2_valid) }

    describe 'when plaintext credentials are present' do
      it 'deletes the plaintext credential from data' do
        # Must have plaintext credentials before the backfill job has executed
        assert_equal twitter_target.data, {
          "username" => "username",
          "password" => "password",
          "token" => "token",
          "secret" => "secret"
        }

        assert_equal url_v2_target.data, {
          "url" => "http://www.google.com/search",
          "method" => "get",
          "password" => "password",
        }

        # Execute the backfill job now
        task.work(minimum)

        # Plaintext data should be deleted now
        twitter_target.reload
        assert_equal twitter_target.data, {
          "username" => "username"
        }

        url_v2_target.reload
        assert_equal url_v2_target.data, {
          "url" => "http://www.google.com/search",
          "method" => "get"
        }
      end
    end

    describe 'when there are no credentials present' do
      before do
        EmailTarget.create!(
          account: support, title: 'email target', subject: 'subject', email: 'user@example.com'
        )
      end

      it 'does not update target' do
        Target.any_instance.expects(:save!).never
        task.work(support)
      end
    end
  end
end
