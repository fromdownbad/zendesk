require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_rescrub_archived_records_two'

SingleCov.covered! uncovered: 4

describe DurableBackfill::BackfillRescrubArchivedRecordsTwo do
  subject { DurableBackfill::BackfillRescrubArchivedRecordsTwo }
  fixtures :all

  let(:account) { accounts(:minimum) }

  before do
    Timecop.travel('2017-05-01'.to_date) do
      @end_user_ticket = create_and_close_ticket(users(:minimum_end_user))
      @end_user_ticket.status_id = StatusType.DELETED
      @end_user_ticket.archive!
      Zendesk::Scrub.scrub_ticket_and_associations(@end_user_ticket)
      @end_user_ticket.update_column(:generated_timestamp, Time.parse('20181213'))
      @end_user_ticket.ticket_archive_stub.update_column(:generated_timestamp, Time.parse('20181213'))
    end

    @end_user_ticket.reload
  end

  describe '#work' do
    let(:worker) { DurableBackfill::BackfillRescrubArchivedRecordsTwo.new(DurableBackfill::BackfillAudit.new) }

    def work(account)
      worker.work(account)
    end

    describe "with rescrub_archived_records" do
      it 'calls scrub_ticket_and_specific_association on the archived ticket' do
        Arturo.enable_feature! :rescrub_archived_records

        ticket = Ticket.with_deleted { Ticket.find @end_user_ticket.id }
        Zendesk::Scrub.expects(:scrub_ticket_and_specific_association).with(ticket, TicketFieldEntry)
        work(account)
      end
    end

    describe "without rescrub_archived_records" do
      it 'calls scrub_ticket_and_specific_association on the archived ticket' do
        Arturo.disable_feature! :rescrub_archived_records

        Zendesk::Scrub.expects(:scrub_ticket_and_specific_association).never
        assert_raises(DurableBackfill::Task::RescheduleTask) do
          work(account)
        end
      end
    end
  end

  protected

  def create_and_close_ticket(user)
    ticket = user.account.tickets.new
    ticket.description = "hello i need help yes please thanks thanks!"
    ticket.requester = user
    ticket.add_comment(body: 'public comment', is_public: false)
    ticket.will_be_saved_by(user)
    ticket.save!

    ticket.reload
    ticket.status_id = StatusType.CLOSED
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.add_comment(body: 'private comment', is_public: false)
    ticket.save!

    ticket
  end
end
