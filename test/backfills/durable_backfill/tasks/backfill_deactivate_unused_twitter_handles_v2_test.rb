require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_deactivate_unused_twitter_handles_v2'

SingleCov.covered!

describe DurableBackfill::BackfillDeactivateUnusedTwitterHandlesV2 do
  fixtures :accounts, :monitored_twitter_handles, :channels_brands

  describe '#work' do
    let(:account) { accounts(:minimum) }
    let(:minimum_handle) { monitored_twitter_handles(:minimum_monitored_twitter_handle_1) }
    let(:task) do
      DurableBackfill::BackfillDeactivateUnusedTwitterHandlesV2.new(DurableBackfill::BackfillAudit.new)
    end

    describe 'for an account that is inside the CSV' do
      before do
        Channels::Maintenance::UnsubscribeMonitoredTwitterHandleJob.expects(:work).returns(true)
        task.stubs(:accounts_and_handles).returns(
          [
            { account_id: minimum_handle.account_id.to_s, twitter_user_id: minimum_handle.twitter_user_id.to_s },
          ]
        )
      end

      it 'deactivated the twitter handle' do
        task.work(minimum_handle.account)
        assert_equal MonitoredTwitterHandle::State::UNAUTHORIZED, 
          minimum_handle.reload.state
      end
    end

    describe 'for an account that is not inside the CSV' do
      before do
        task.stubs(:accounts_and_handles).returns(
          [
            { account_id: 12345678, twitter_user_id: 12345678 },
          ]
        )
      end
      
      it 'did not deactivate the twitter handle' do
        task.work(minimum_handle.account)
        assert_not_equal MonitoredTwitterHandle::State::UNAUTHORIZED, 
          minimum_handle.reload.state
      end
    end
  end
end
