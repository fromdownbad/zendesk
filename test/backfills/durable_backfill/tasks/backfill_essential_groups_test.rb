require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_essential_groups'

SingleCov.covered!

describe DurableBackfill::BackfillEssentialGroups do
  describe '#work' do
    let(:account) do
      accounts(:minimum).tap do |account|
        account.subscription.pricing_model_revision = ZBC::Zendesk::PricingModelRevision::PATAGONIA
        account.subscription.plan_type = SubscriptionPlanType.Large
        account.subscription.features.groups = false
        account.subscription.features.save!
      end
    end

    let(:task) do
      DurableBackfill::BackfillEssentialGroups.new(DurableBackfill::BackfillAudit.new)
    end

    describe_with_arturo_enabled :backfill_essential_groups do
      describe 'deleted account' do
        before do
          account.stubs(:is_active?).returns(false)
        end

        it 'does not backfill' do
          Zendesk::Features::SubscriptionFeatureService.expects(:new).never
          task.work(account)
        end
      end

      describe 'non patagonia account' do
        before do
          account.subscription.update_attributes!(
            pricing_model_revision: ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE
          )
        end

        it 'does not backfill' do
          Zendesk::Features::SubscriptionFeatureService.expects(:new).never
          task.work(account)
        end
      end

      describe 'patagonia account' do
        describe 'small plan type' do
          before do
            account.subscription.update_attributes!(
              plan_type: SubscriptionPlanType.Small
            )
          end

          it 'enabled group features to the account' do
            Zendesk::Features::SubscriptionFeatureService.any_instance.expects(:execute!).once
            task.work(account)
          end
        end

        describe 'plan type other than small' do
          it 'does not backfill' do
            Zendesk::Features::SubscriptionFeatureService.expects(:new).never
            task.work(account)
          end
        end
      end
    end

    describe_with_arturo_disabled :backfill_essential_groups do
      it 'reschedules' do
        assert_raises(DurableBackfill::Task::RescheduleTask) do
          task.work(account)
        end
      end
    end
  end
end
