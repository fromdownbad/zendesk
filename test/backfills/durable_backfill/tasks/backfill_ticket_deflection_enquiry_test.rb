require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_ticket_deflection_enquiry'

SingleCov.covered!

describe DurableBackfill::BackfillTicketDeflectionEnquiry do
  describe '#work' do
    fixtures :accounts, :ticket_deflections

    let(:account) { accounts(:with_groups) }
    let(:ticket) { account.tickets.new(description: "I need some help!", id: 123) }

    let(:task) do
      DurableBackfill::BackfillTicketDeflectionEnquiry.new(
        DurableBackfill::BackfillAudit.new
      )
    end

    before do
      account
    end

    describe 'when deflection has a ticket associated which has already been deleted' do
      before do
        Ticket.stubs(:find_by_id).returns(nil)
      end

      it 'updates the enquiry text to SCRUBBED' do
        task.work(account)

        assert_equal account.ticket_deflections.pluck(:enquiry).uniq, ['SCRUBBED']
      end
    end

    describe 'when deflection has an active ticket associated' do
      before do
        Ticket.stubs(:find_by_id).returns(ticket)
      end

      it 'does not update the enquiry text to SCRUBBED' do
        task.work(account)

        assert_not_includes account.ticket_deflections.pluck(:enquiry), 'SCRUBBED'
      end
    end

    describe 'when deflection doesnt have a associated ticket' do
      let(:account) { accounts(:minimum_answer_bot) }

      it 'returns without proceeding' do
        Ticket.expects(:find_by_id).never

        task.work(account)
      end
    end
  end
end
