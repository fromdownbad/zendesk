require_relative '../../../support/test_helper'
require_relative '../../../support/brand_test_helper.rb'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_reactivate_facebook_pages.rb'

SingleCov.covered!

describe DurableBackfill::BackfillReactivateFacebookPages do
  include BrandTestHelper
  subject {DurableBackfill::BackfillReactivateFacebookPages }

  fixtures :accounts
  let(:account) { accounts(:minimum) }

  let(:task) do
    subject.new(DurableBackfill::BackfillAudit.new)
  end

  before do
    pages = Facebook::Page.where(account_id: account.id)
    channel_brands = ChannelsBrand.where(brandable: pages.first)

    # Unauthorized
    associate_brand_with(pages.first)
    pages.first.state = 5
    pages.first.last_polled_at = Time.parse('2020-01-10 18:38:00')
    pages.first.save!

    # Active
    associate_brand_with(pages.second)
    pages.second.state = 3
    pages.second.last_polled_at = Time.parse('2020-01-10 18:38:00')
    pages.second.save!
  end

  describe '.work' do
    it 'works a FacebookAccountReactivateJob for unauthorized pages' do
      active_page = Facebook::Page.where(account_id: account.id).active.first

      pages = Facebook::Page.where(account_id: account.id, state: Facebook::Page::State::UNAUTHORIZED)
      page = pages.first

      Channels::Maintenance::FacebookAccountReactivateJob.expects(:work).with(page.id)
      Channels::Maintenance::FacebookAccountReactivateJob.expects(:work).with(active_page.id).never

      task.work(account)
    end
  end
end
