require_relative '../../../support/test_helper'
require_relative '../../../../app/backfills/durable_backfill/tasks/backfill_channels_twitter_oauth2_refresh_token_v2'

SingleCov.covered!

describe DurableBackfill::BackfillChannelsTwitterOauth2RefreshTokenV2 do
  fixtures :accounts, :monitored_twitter_handles, :channels_brands

  describe '#work' do
    let(:unencrypted_token) { 'helloworld!' }
    let(:unencrypted_secret) { 'mysecretvalue' }
    let(:account) { accounts(:minimum) }
    let(:handle) { monitored_twitter_handles(:minimum_monitored_twitter_handle_1) }
    let(:task) do
      DurableBackfill::BackfillChannelsTwitterOauth2RefreshTokenV2.new(DurableBackfill::BackfillAudit.new)
    end

    describe 'for an account without the encrypted values' do
      before do
        handle.update_columns(
          secret: unencrypted_secret,
          access_token: unencrypted_token,
          encrypted_access_token_value: nil,
          encrypted_secret_value: nil,
          secret_updated_at: nil,
          encryption_cipher_name: nil,
          encryption_key_name: nil
        )
      end

      it 'encrypts the values' do
        task.work(account)
        handle.reload

        assert_equal ENV['CHANNELS_TWITTER_ENCRYPTION_KEY_NAME'], handle.encryption_key_name
        assert_equal ENV['CHANNELS_TWITTER_ENCRYPTION_CIPHER_NAME'], handle.encryption_cipher_name
        assert_equal handle.decrypted_secret_value, unencrypted_secret
        assert_equal handle.decrypted_access_token, unencrypted_token
        assert_not_nil handle.secret_updated_at
      end

      it 'logs' do
        DurableBackfill::Manager.logger.expects(:info).with("Starting backfill task to encrypt Twitter credentials for: #{account.id}")
        MonitoredTwitterHandle.where(account_id: account.id).each do |twitter_handle|
          DurableBackfill::Manager.logger.expects(:info).with("Encrypting Twitter credentials for handle: #{twitter_handle.twitter_user_id}, account_id: #{account.id}")
        end
        DurableBackfill::Manager.logger.expects(:info).with("Finished backfill task to encrypt Twitter credentials for: #{account.id}")
        
        task.work(account)
      end

      describe 'for an existing account which already has encrypted values' do
        before do
          Timecop.freeze(Time.now)
          handle.encryption_key_name = ENV['CHANNELS_TWITTER_ENCRYPTION_KEY_NAME']
          handle.secret_updated_at = 1.day.ago
          handle.save!
        end

        after { Timecop.return }

        it 'backfill job should be skipped for that account' do
          task.work(account)
          handle.reload

          assert_equal 1.day.ago.to_i, handle.secret_updated_at.to_i
        end
      end
    end
  end
end
