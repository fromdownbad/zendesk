require_relative '../support/test_helper'

SingleCov.covered!

describe TouchAccounts do
  let(:minimum_account) { accounts(:minimum) }
  let(:support_account) { accounts(:support) }

  before do
    @timestamp = Timecop.freeze(5.minutes.from_now)
    TouchAccounts.any_instance.stubs(:puts)
  end

  it 'updates updated_at timestamp on all accounts' do
    TouchAccounts.perform(dry_run: false)
    assert_equal @timestamp.to_i, minimum_account.updated_at.to_i
    assert_equal @timestamp.to_i, support_account.updated_at.to_i
  end

  it 'does not update updated_at timestamp on dry run' do
    TouchAccounts.perform
    assert_not_equal @timestamp.to_i, minimum_account.updated_at.to_i
    assert_not_equal @timestamp.to_i, support_account.updated_at.to_i
  end
end
