require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillRemoveOldApiTokens do
  fixtures :accounts
  fixtures :users

  let(:account) { accounts(:minimum) }

  before do
    time = Time.parse('2016-09-01')
    Timecop.freeze(time) do
      account.api_tokens.create!
      account.api_tokens.create!
    end
  end

  it 'removes old api tokens' do
    assert_equal 2, account.api_tokens.count
    BackfillRemoveOldApiTokens.perform(dry_run: false)
    assert_equal 0, account.api_tokens.count
  end
end
