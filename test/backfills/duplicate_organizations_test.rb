require_relative "../support/test_helper"

SingleCov.covered!

describe DuplicateOrganizations do
  fixtures :organizations
  fixtures :tickets
  fixtures :users
  let(:account) { accounts(:minimum) }
  let(:org1) { organizations(:minimum_organization1) }
  let(:org2) { organizations(:minimum_organization2) }
  let(:org3) { organizations(:minimum_organization3) }
  let(:described_class) { DuplicateOrganizations }

  describe "sorting duplicates" do
    let(:klass) { described_class::DuplicateOrganization }
    let(:org1) { klass.new }
    let(:org2) { klass.new }
    let(:org3) { klass.new }

    before do
      org1.stubs(:tickets_count).returns(1)
      org1.stubs(:users_count).returns(2)
      org2.stubs(:tickets_count).returns(10)
      org2.stubs(:users_count).returns(10)
      org3.stubs(:tickets_count).returns(0)
      org3.stubs(:users_count).returns(1)
    end

    it "sorts organization with most records first" do
      sorted = [org1, org2, org3].sort
      assert_equal([org2, org1, org3], sorted)
    end
  end

  describe "processing duplicates" do
    let(:backfill) { described_class.new(dry_run: dry_run) }
    let(:ticket1) { tickets(:minimum_1) }
    let(:ticket2) { tickets(:minimum_2) }
    let(:ticket3) { tickets(:minimum_3) }
    let(:user1) { users(:minimum_end_user) }
    let(:user2) { users(:minimum_end_user2) }

    before do
      org1.update_column :name, 'Name'
      org2.update_column :name, 'Name'
      org3.update_column :name, 'Name'
      ticket1.update_column :organization_id, org1.id
      ticket2.update_column :organization_id, org1.id
      ticket3.update_column :organization_id, org2.id
      user1.update_column :organization_id, org1.id
    end

    describe "dry run" do
      let(:dry_run) { true }

      it "does not alter data" do
        backfill.perform
        assert_equal('Name', org1.reload.name)
        assert_equal('Name', org2.reload.name)
        assert_equal('Name', org3.reload.name)
      end
    end

    describe "dry run false" do
      let(:dry_run) { false }

      it "renames used duplicates with zero width space Unicode character \u200b" do
        backfill.perform
        assert_equal("Name\u200b", org2.reload.name)
      end
    end
  end
end
