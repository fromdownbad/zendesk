require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe BackfillRoleSettings do
  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum) }
  let(:role_settings) { account.role_settings }

  def work(account, dry_run: false)
    BackfillRoleSettings.new(dry_run: dry_run).perform

    role_settings.reload
  end

  def enable_agent_remote_login!
    role_settings.update_attributes!(agent_remote_login: true, agent_zendesk_login: false)
  end

  def enable_end_user_remote_login!
    role_settings.update_attributes!(end_user_remote_login: true, end_user_zendesk_login: false)
  end

  describe "#work" do
    before do
      Arturo.enable_feature! :access_token_sso_bypass
      feature = Arturo::Feature.where(symbol: 'access_token_sso_bypass').first
      feature.external_beta_subdomains.push(account.subdomain)
      feature.save!

      enable_agent_remote_login!

      work account
    end

    it "disables agent passwords" do
      assert_equal false, role_settings.agent_password_allowed
    end

    it "allows end user passwords" do
      assert_equal true, role_settings.end_user_password_allowed
    end

    it "doesn't disable sso bypass" do
      assert_equal RoleSettings::REMOTE_BYPASS_ADMINS, role_settings.agent_remote_bypass
    end

    describe "when end users can't login with zendesk" do
      before do
        enable_end_user_remote_login!
        work account
      end

      it "disables end user passwords" do
        assert_equal false, role_settings.end_user_password_allowed
      end
    end

    describe "when sso bypass is disabled" do
      before do
        account.settings.sso_bypass = false
        account.settings.save!

        work account
      end

      it "disables sso bypass" do
        assert_equal RoleSettings::REMOTE_BYPASS_DISABLED, role_settings.agent_remote_bypass
      end
    end
  end
end
