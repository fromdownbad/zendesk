require_relative "../support/test_helper"

SingleCov.covered!

describe UserOrgFixMemberships do
  fixtures :accounts
  fixtures :organizations
  fixtures :organization_memberships
  fixtures :organization_domains
  fixtures :users
  let(:account) { accounts(:minimum) }
  let(:described_class) { UserOrgFixMemberships }

  # 1. organization_id present, organization memberships empty
  describe "when memberships is empty" do
    let(:org1) { organizations(:minimum_organization1) }
    let(:org2) { organizations(:minimum_organization2) }
    let!(:organization_membership) { organization_memberships(:minimum) }
    let(:user) { users(:minimum_end_user) }

    describe "with an active user" do
      before do
        user.organization_memberships.map(&:delete)
      end

      it "sets organization_id to nil" do
        Organization.destroy_all
        described_class.new(account_id: account.id, dry_run: false).perform
        assert_equal 0, user.reload.organization_memberships.count
        assert_nil user.reload.organization_id
      end

      describe "when user has an email domain that matches an organization" do
        it "adds the user to the matching organization" do
          described_class.new(account_id: account.id, dry_run: false).perform
          assert_equal 1, user.reload.organization_memberships.count
          assert_equal user.reload.organization_memberships.first.organization_id, user.reload.organization_id
        end
      end
    end

    describe "when the user save fails" do
      before do
        user.organization_memberships.map(&:delete)
        user.update_column :name, ""
      end

      it "reports that it could not save the user" do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(regexp_matches(/Could not save end user/))
        described_class.new(account_id: account.id, dry_run: false).perform
      end

      describe "with the :force flag" do
        it "updates the invalid user" do
          described_class.new(account_id: account.id, dry_run: false, force: true).perform

          assert_nil user.reload.organization_id
        end
      end
    end

    describe "with an inactive user" do
      let(:inactive_user) { users(:inactive_user) }

      before do
        inactive_user.update_column(:organization_id, org1.id)
      end

      it "fix_users_without_memberships skips the users" do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(regexp_matches(/fix_users_without_memberships: 0 users found with an inconsistent state/))

        described_class.new.perform
      end
    end

    describe "with an inactive account" do
      let(:inactive_account) { accounts(:inactive) }
      let(:inactive_account_end_user) { users(:inactive_account_end_user) }

      before do
        inactive_account_end_user.organization_memberships.map(&:delete)
      end

      it "skips the users in the inactive account" do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(regexp_matches(/fix_users_without_memberships: 0 users found with an inconsistent state/))

        described_class.new.perform
      end
    end
  end

  # 2. organization_id present, but not in organization memberships
  describe "when memberships exist and organization_id is invalid" do
    let(:org1) { organizations(:minimum_organization1) }
    let(:org2) { organizations(:minimum_organization2) }
    let!(:organization_membership) { organization_memberships(:minimum) }
    let(:user) { users(:minimum_end_user) }

    before do
      user.update_column(:organization_id, 12345)
    end

    it "updates organization_id" do
      described_class.new(account_id: account.id, dry_run: false).perform
      organization = user.organization_memberships.where(default: true).first.organization
      assert_equal organization.id, user.reload.organization_id
    end
  end

  # 3. organization_id nil, organization exists
  describe "when memberships exist and organization_id is nil" do
    let(:org1) { organizations(:minimum_organization1) }
    let(:org2) { organizations(:minimum_organization2) }
    let!(:organization_membership) { organization_memberships(:minimum) }
    let(:user) { users(:minimum_end_user) }

    before do
      user.update_column(:organization_id, nil)
    end

    it "updates organization_id" do
      described_class.new(account_id: account.id, dry_run: false).perform
      organization = user.organization_memberships.where(default: true).first.organization
      assert_equal organization.id, user.reload.organization_id
    end
  end

  # 4. organization_id points to a deleted organization, organization memberships empty
  describe "destroying memberships for deleted organizations" do
    let(:org1) { organizations(:minimum_organization1) }
    let(:org2) { organizations(:minimum_organization2) }
    let(:user) { users(:minimum_end_user) }

    describe "when organization_memberships empty" do
      before do
        org1.touch :deleted_at
        org2.touch :deleted_at
      end

      it "deletes and destroys the memberships" do
        described_class.new(account_id: account.id, dry_run: false).perform
        assert user.organization_memberships.empty?
        assert_nil user.reload.organization_id
      end
    end

    # 5. organization_id deleted organization, organization memberships exist
    describe "when organization_memberships exist" do
      let!(:organization_membership) { organization_memberships(:minimum) }

      before do
        org2.touch :deleted_at
        user.update_column(:organization_id, org2.id)
      end

      it "updates user.organization_id to point to a real membership" do
        described_class.new(account_id: account.id, dry_run: false).perform
        assert_equal organization_membership.organization_id, user.reload.organization_id
      end
    end
  end

  # 6. Organization Memberships have no default
  describe "organization memberships have no default" do
    let(:org1) { organizations(:minimum_organization1) }
    let(:org2) { organizations(:minimum_organization2) }
    let!(:organization_membership) { organization_memberships(:minimum) }
    let(:user) { users(:minimum_end_user) }

    before do
      user.update_column(:organization_id, nil)
      user.organization_memberships.first.update_column(:default, nil)
      described_class.new(account_id: account.id, dry_run: false).perform
    end

    it "sets the user organization_id" do
      organization = organization_membership.reload.organization
      assert_equal organization.id, user.reload.organization_id
    end

    it "sets the organization_membership default to true" do
      assert_equal true, organization_membership.reload.default
    end
  end

  # 7. Testing shard and pod level execution
  describe "running on a shard" do
    describe "when memberships exist and organization_id is invalid" do
      let(:org1) { organizations(:minimum_organization1) }
      let(:org2) { organizations(:minimum_organization2) }
      let!(:organization_membership) { organization_memberships(:minimum) }
      let(:user) { users(:minimum_end_user) }

      before do
        user.update_column(:organization_id, 12345)
      end

      it "updates organization_id" do
        described_class.new(shard_id: account.shard_id, dry_run: false).perform
        organization = user.organization_memberships.where(default: true).first.organization
        assert_equal organization.id, user.reload.organization_id
      end
    end
  end

  describe "running on a pod" do
    describe "when memberships exist and organization_id is invalid" do
      let(:org1) { organizations(:minimum_organization1) }
      let(:org2) { organizations(:minimum_organization2) }
      let!(:organization_membership) { organization_memberships(:minimum) }
      let(:user) { users(:minimum_end_user) }

      before do
        user.update_column(:organization_id, 12345)
      end

      it "updates organization_id" do
        described_class.new(dry_run: false).perform
        organization = user.organization_memberships.where(default: true).first.organization
        assert_equal organization.id, user.reload.organization_id
      end
    end
  end
end
