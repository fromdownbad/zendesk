require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillUpdateCfcRisManifest do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:ris_model) { Channels::AnyChannel::RegisteredIntegrationService }
  let(:backfill_dry_run) { BackfillUpdateCfcRisManifest.new }
  let(:backfill) { BackfillUpdateCfcRisManifest.new(dry_run: false) }

  let!(:cfc_ris_old_manifest) do
    ris = ris_model.new(
      account: account,
      external_id: 'whatsapp',
      name: 'whatsapp',
      search_name: 'whatsapp',
      admin_ui_url: 'https://connector.zendesk.ext.smooch.io/zendesk/whatsapp/admin',
      manifest_url: 'https://connector.zendesk.ext.smooch.io/zendesk/whatsapp/manifest.json',
      push_client_id: 'smooch'
    )
    ris.account_id = account.id
    ris.save!
    ris
  end

  let!(:other_ris) do
    ris = ris_model.new(
      account: account,
      external_id: 'foo',
      name: 'foo',
      search_name: 'foo',
      admin_ui_url: 'https://foo.example.com/admin',
      manifest_url: 'https://foo.example.com/manifest.json'
    )
    ris.account_id = account.id
    ris.save!
    ris
  end

  let(:manifest_data) do
    [
      {
        'id' => 'social_messaging',
        'version' => '7',
        'name' => 'SocialMessaging',
        'author' => 'Zendesk',
        'push_client_id' => 'smooch',
        'urls' => {
          'admin_ui' => 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/admin',
          'channelback_url' => 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/channelback',
          'event_callback_url' => 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/callback'
        },
        'channelback_files' => true,
        'create_followup_tickets' => false,
      },
      'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/manifest.json'
    ]
  end

  before do
    Channels::AnyChannelApi::Client.stubs(get_manifest: manifest_data)
  end

  describe "when run in dry mode" do
    it "does not perform any action" do
      Channels::AnyChannel::RegisteredIntegrationService.any_instance.expects(:update_attributes!).never

      backfill_dry_run.perform
    end
  end

  describe "on a shard" do
    it "calls update attributes on the RIS" do
      Channels::AnyChannel::RegisteredIntegrationService.any_instance.expects(:update_attributes!).once

      backfill.perform
    end

    it "should update the RIS attributes" do
      backfill.perform

      cfc_ris_old_manifest.reload
      assert_equal cfc_ris_old_manifest.external_id, 'social_messaging'
      assert_equal cfc_ris_old_manifest.version, '7'
      assert_equal cfc_ris_old_manifest.name, 'SocialMessaging'
      assert_equal cfc_ris_old_manifest.search_name, 'socialmessaging'
      assert_equal cfc_ris_old_manifest.admin_ui_url, 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/admin'
      assert_equal cfc_ris_old_manifest.channelback_url, 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/channelback'
      assert_equal cfc_ris_old_manifest.event_callback_url, 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/callback'
    end

    it "uses the AnyChannel API client to get the manifest" do
      Channels::AnyChannelApi::Client.
        expects(:get_manifest).
        with('https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/manifest.json').
        returns(manifest_data)

      backfill.perform
    end

    describe "when there is an error" do
      before do
        Channels::AnyChannel::RegisteredIntegrationService.any_instance.stubs(:update_attributes!).raises(StandardError)
      end

      it 'logs the failure' do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(regexp_matches(/Could not update manifest for CFC RIS in account/))

        backfill.perform
      end
    end
  end
end
