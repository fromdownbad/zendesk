require_relative "../support/test_helper"

SingleCov.covered!

describe TruncateCsTables do

  describe "when running after GA date" do
    before do
      Timecop.freeze(2020, 10, 5)
    end

    it "it fails with runtime error" do
      assert_raises(RuntimeError) do
        TruncateCsTables.perform
      end
    end
  end

  describe "when dry run is true" do
    before do
      Timecop.freeze(2020, 8, 5)
    end

    it "makes no change in collections service tables" do
      TruncateCsTables.any_instance.stubs(:get_num_records).returns(10_000)
      TruncateCsTables.any_instance.expects(:remove_records).never
      TruncateCsTables.perform
    end
  end

end
