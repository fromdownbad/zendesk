require_relative "../support/test_helper"
require 'timecop'

SingleCov.covered!

describe DeleteSessions do
  fixtures :users

  let(:user) { users(:minimum_end_user) }

  it "deletes the sessions if dry run is false" do
    Zendesk::SharedSession::SessionRecord.create(user: user, account: user.account, session_id: 1, data: "", expire_at: Time.now - 1000.days)
    refute_equal Zendesk::SharedSession::SessionRecord.count, 0

    DeleteSessions.perform(dry_run: false)

    assert_equal Zendesk::SharedSession::SessionRecord.count, 0
  end

  it "deletes multiple sessions if dry run is false" do
    i = 1
    501.times do
      Zendesk::SharedSession::SessionRecord.create(user: user, account: user.account, session_id: i, data: "", expire_at: Time.now - 1000.days)
      i += 1
    end
    refute_equal Zendesk::SharedSession::SessionRecord.count, 0

    DeleteSessions.perform(dry_run: false)

    assert_equal Zendesk::SharedSession::SessionRecord.count, 0
  end

  it "deletes the sessions I want using options" do
    Timecop.travel(2.hours.ago) do
      Zendesk::SharedSession::SessionRecord.create(user: user, account: user.account, session_id: 1, data: "", expire_at: Time.now - 1000.days)
    end

    Zendesk::SharedSession::SessionRecord.create(user: user, account: user.account, session_id: 2, data: "", expire_at: Time.now - 1000.days)

    assert_equal Zendesk::SharedSession::SessionRecord.count, 2

    DeleteSessions.new(period_before_now: 1.hours, dry_run: false).perform

    assert_equal Zendesk::SharedSession::SessionRecord.count, 1
  end

  it "doesn't delete the sessions if dry run is true" do
    Zendesk::SharedSession::SessionRecord.create(user: user, account: user.account, session_id: 1, data: "", expire_at: Time.now - 1000.days)
    refute_equal Zendesk::SharedSession::SessionRecord.count, 0

    DeleteSessions.perform(dry_run: true)

    refute_equal Zendesk::SharedSession::SessionRecord.count, 0
  end
end
