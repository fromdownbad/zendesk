require_relative '../support/test_helper.rb'

SingleCov.covered! uncovered: 1

describe UpdateExpiredTrialsToPatagonia do
  fixtures :accounts, :subscriptions

  let(:expired_trial_account) do
    account = accounts(:trial)
    account.update_attributes!(
      is_active: true,
      is_serviceable: false
    )
    account.subscription.update_attributes!(pricing_model_revision: 5, is_trial: false, payment_method_type: 0)
    account
  end

  let(:servicable_account) do
    account = accounts(:minimum)
    account.update_attributes!(
      is_active: true
    )
    account.subscription.update_attributes!(pricing_model_revision: 5, is_trial: false)
    account.update_column('is_serviceable', true)
    account.reload
  end

  let(:dunning_account) do
    account = accounts(:billing)
    account.update_attributes!(
      is_active: true,
      is_serviceable: false
    )
    account.subscription.update_attributes!(pricing_model_revision: 5, is_trial: false, payment_method_type: 1)
    account
  end

  before do
    expired_trial_account.subscription.pricing_model_revision.must_equal(5)
    servicable_account.subscription.pricing_model_revision.must_equal(5)
    dunning_account.subscription.pricing_model_revision.must_equal(5)
    UpdateExpiredTrialsToPatagonia.perform(dry_run: false)
  end

  it 'should change the pricing model on expired trials to patagonia' do
    expired_trial_account.subscription.reload.pricing_model_revision.must_equal(7)
  end

  it 'should not affect servicable accounts' do
    servicable_account.subscription.reload.pricing_model_revision.must_equal(5)
  end

  it 'should not affect accounts that are dunning' do
    dunning_account.subscription.reload.pricing_model_revision.must_equal(5)
  end

  it 'should not affect accounts that have pod_id == nil 122346549874641165' do
    expired_trial_account.subscription.reload.update_attribute(:pricing_model_revision, 5)
    Account.any_instance.stubs(:pod_id).returns(nil)
    UpdateExpiredTrialsToPatagonia.perform(dry_run: false)
    expired_trial_account.subscription.reload.pricing_model_revision.must_equal(5)
  end
end
