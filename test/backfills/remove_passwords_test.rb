require_relative "../support/test_helper"

SingleCov.covered!

describe RemovePasswords do
  let(:account) { accounts(:minimum) }
  let(:remove_passwords) { RemovePasswords.new(account.id) }


  it "deletes end_user_passwords" do
    refute nil_passwords?(account.end_users)

    remove_passwords.reset_end_users_password

    assert nil_passwords?(account.end_users)
  end

  it "deletes agents_passwords" do
    refute nil_passwords?(account.agents)

    remove_passwords.reset_agents_password

    assert nil_passwords?(account.agents)
  end

  def nil_passwords?(users)
    with_passwords = users.reject { |u| u.salt == nil && u.crypted_password == nil}
    with_passwords.size == 0
  end
end

