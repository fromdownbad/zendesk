require_relative "../support/test_helper"

SingleCov.covered!

describe DeleteAccountGoogleIdentities do
  fixtures :accounts
  fixtures :users
  fixtures :user_identities
  let(:account) { accounts(:minimum) }
  let(:user)    { users(:minimum_end_user) }
  let(:user2)   { users(:minimum_agent) }

  before do
    GoogleProfile.create!(user_identity: user.identities.email.first)
    GoogleProfile.create!(user_identity: user2.identities.email.first)
  end

  it "deletes the account's google identities if dry run is false" do
    refute_empty GoogleProfile.where(account_id: account.id)
    backfill = DeleteAccountGoogleIdentities.new(dry_run: false, account_id: account.id)
    backfill.perform

    assert_empty GoogleProfile.where(account_id: account.id)
  end

  it "doesn't delete the google indentities if dry run is true" do
    refute_empty GoogleProfile.where(account_id: account.id)
    backfill = DeleteAccountGoogleIdentities.new(dry_run: true, account_id: account.id)
    backfill.perform

    refute_empty GoogleProfile.where(account_id: account.id)
  end

  it "doesn't delete the google indentities if google logins are still allowed" do
    refute_empty GoogleProfile.where(account_id: account.id)
    role_settings = stub(agent_google_login: true)
    Account.any_instance.stubs(:role_settings).returns(role_settings)

    backfill = DeleteAccountGoogleIdentities.new(dry_run: false, account_id: account.id)
    backfill.perform

    refute_empty GoogleProfile.where(account_id: account.id)
  end
end
