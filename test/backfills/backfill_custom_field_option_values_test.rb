require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe BackfillCustomFieldOptionValues do
  fixtures :all

  fixtures :accounts, :custom_field_options

  describe '#perform' do
    let(:account) { accounts(:minimum) }
    let(:cfo1) { custom_field_options(:field_tagger_custom_option_1) }
    let(:cfo2) { custom_field_options(:field_tagger_custom_option_2) }

    before do
      cfo1.update_columns(value: nil, enhanced_value: 'some_value')
      cfo2.update_columns(value: nil, enhanced_value: 'some_value2', deleted_at: 1.day.ago)
    end

    describe "and there were no errors" do
      before do
        BackfillCustomFieldOptionValues.new(account_id: account.id, dry_run: false).perform
        cfo1.reload
        cfo2.reload
      end

      it 'sets enhanced value to equal value' do
        assert_equal(cfo1[:value], cfo1[:enhanced_value])
      end

      it 'updates deleted options' do
        assert_equal(cfo2[:value], cfo2[:enhanced_value])
      end
    end
  end
end

