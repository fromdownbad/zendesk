require_relative '../support/test_helper'

SingleCov.covered!

describe BackfillZendeskInternal do
  fixtures :global_oauth_clients

  let(:client) { global_oauth_clients(:system_users) }

  before do
    BackfillZendeskInternal.any_instance.stubs(:data).returns([{id: client.id, identifier: client.identifier}])
  end

  it "updates zendesk_internal to be true" do
    backfill = BackfillZendeskInternal.new({:dry_run => false})
    backfill.perform

    assert client.reload.zendesk_internal
  end

  it "doesn't update zendesk_internal if dry run is true" do
    backfill = BackfillZendeskInternal.new({:dry_run => true})
    backfill.perform

    refute client.reload.zendesk_internal
  end
end
