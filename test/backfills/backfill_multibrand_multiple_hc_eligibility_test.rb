require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillMultibrandMultipleHcEligibility do
  fixtures :all

  let(:backfill_instance) { BackfillMultibrandMultipleHcEligibility.new }

  let(:account) { accounts(:minimum) }
  let(:backfill_multiple_hc) { BackfillMultibrandMultipleHcEligibility }
  let(:zuora_session) { stub }
  let(:zuora_account_id) { stub }
  let(:logger) { stub(info: stub, error: stub) }
  let(:valid_accounts) { Account.where(subdomain: 'minimum') }

  describe '.perform' do
    let(:accounts_with_subscription) { stub }
    let(:account_stub) { stub }
    let(:expected_subscription_options) do
      {
        subscriptions: {
          is_trial: false,
          payment_method_type: 4
        }
      }
    end

    let(:expected_account_options) do
      {
        accounts: {
          is_active: true,
          is_serviceable: true
        }
      }
    end

    let(:shard_accounts) { stub }

    before do
      Account.stubs(:shard_local).returns(shard_accounts)
      shard_accounts.expects(:joins).with(:subscription).returns(accounts_with_subscription)
      accounts_with_subscription.expects(:where).with(expected_subscription_options).returns(account_stub)
      account_stub.expects(:where).with(expected_account_options).returns(valid_accounts)
      backfill_multiple_hc.any_instance.stubs(session: stub(account: zuora_session))
    end

    describe 'with dry_run parameter as false' do
      describe 'for all professional and enterprise accounts' do
        describe 'when there is no exception' do
          before do
            backfill_multiple_hc.any_instance.stubs(zuora_account_id: zuora_account_id)
            backfill_multiple_hc.any_instance.stubs(needs_grandfathering?: true)
          end

          it 'updates custom field and sets property to true' do
            zuora_session.expects(:update).with(
              zuora_account_id,
              multi_brand_includes_multi_hc__c: true
            ).returns(true)
            backfill_multiple_hc.perform(dry_run: false)
            assert_equal true, account.settings.multibrand_includes_help_centers
          end
        end

        describe 'when zuora client raises an error for update' do
          before do
            zuora_session.stubs(:update).raises(ZuoraClient::UpdateFailure)
            backfill_multiple_hc.any_instance.stubs(zuora_account_id: zuora_account_id)
            backfill_multiple_hc.any_instance.stubs(needs_grandfathering?: true)
            backfill_multiple_hc.any_instance.stubs(logger: logger)
          end

          it 'fails to update custom field and dosent set any property in account setting' do
            account.settings.expects(:where).never
            logger.expects(:error).with(regexp_matches(/error_obtained=ZuoraClient::UpdateFailure/))
            backfill_multiple_hc.perform(dry_run: false)
            assert_equal false, account.settings.multibrand_includes_help_centers
          end
        end

        describe 'when there is no zuora subscripiton for an account' do
          before do
            backfill_multiple_hc.any_instance.stubs(needs_grandfathering?: true)
            backfill_multiple_hc.any_instance.stubs(logger: logger)
          end

          it 'fails to update custom field and dosent set any property in account setting' do
            zuora_session.expects(:update).never
            logger.expects(:error).with(regexp_matches(/error_obtained=ZuoraSubscriptionNotPresent/))
            backfill_multiple_hc.perform(dry_run: false)
            assert_equal false, account.settings.multibrand_includes_help_centers
          end
        end
      end
    end

    describe 'for all old accounts other than professional and enterprise' do
      before do
        backfill_multiple_hc.any_instance.stubs(zuora_account_id: zuora_account_id)
        backfill_multiple_hc.any_instance.stubs(:needs_grandfathering?).returns(false)
        backfill_multiple_hc.any_instance.stubs(logger: logger)
      end

      it 'logs saying account is aligned and dosent update account settings' do
        logger.expects(:info).with("dry_run=false account_id=90538 in pod=1 is being skipped since it's already up-to-date")
        backfill_instance.expects(:update_zuora_customfield_and_account_setting).never
        backfill_multiple_hc.perform(dry_run: false)
        assert_equal false, account.settings.multibrand_includes_help_centers
      end
    end

    describe 'with dry_run parameter as true' do
      before do
        backfill_instance.stubs(logger: logger)
      end

      describe 'for all accounts other than professional and enterprise' do
        it 'logs saying account is aligned' do
          logger.expects(:info).with("dry_run=true account_id=90538 in pod=1 is being skipped since it's already up-to-date")
          backfill_instance.expects(:update_zuora_customfield_and_account_setting).never
          backfill_instance.perform
          assert_equal false, account.settings.multibrand_includes_help_centers
        end
      end

      describe 'for all professional and enterprise accounts' do
        before do
          backfill_instance.stubs(needs_grandfathering?: true)
        end

        it 'logs saying account is updated with dry_run true' do
          logger.expects(:info).with('dry_run=true account_id=90538 in pod=1 needs updating but is being skipped since this is a dry run')
          backfill_instance.expects(:update_zuora_customfield_and_account_setting).never
          backfill_instance.perform
          assert_equal false, account.settings.multibrand_includes_help_centers
        end
      end
    end
  end

  describe 'for all downgraded accounts which has multi hc property set as true' do
    let(:active_purchased_accounts) { Account.where(subdomain: 'support') }
    let(:support_account) { active_purchased_accounts.first }

    before do
      backfill_multiple_hc.any_instance.stubs(active_purchased_accounts: active_purchased_accounts)
      backfill_multiple_hc.any_instance.stubs(zuora_account_id: zuora_account_id)
      backfill_multiple_hc.any_instance.stubs(session: stub(account: zuora_session))
    end

    it 'updates custom field and sets property to false' do
      zuora_session.expects(:update).with(
        zuora_account_id,
        multi_brand_includes_multi_hc__c: false
      ).returns(true)
      backfill_multiple_hc.perform(dry_run: false)
      assert_equal false, support_account.settings.multibrand_includes_help_centers
    end
  end

  describe '#zuora_account_id' do
    it 'raises ZuoraSubscriptionNotPresent error' do
      assert_raises ZuoraSubscriptionNotPresent do
        backfill_instance.zuora_account_id(account)
      end
    end
  end

  describe 'while running on pods and shards with dry_run=true' do
    before do
      backfill_multiple_hc.any_instance.stubs(active_purchased_accounts: valid_accounts)
      backfill_multiple_hc.any_instance.stubs(logger: logger)
    end

    describe 'with correct shard_number' do
      before do
        backfill_multiple_hc.any_instance.stubs(:validate_and_update_accounts).returns(true)
      end

      it 'logs saying dry_run is true and multihc flag is not updated' do
        logger.expects(:info).with(regexp_matches(/Dry run?: true|On shard?|On pod?/))
        backfill_multiple_hc.perform(shard_number: 1, dry_run: true)
        assert_equal false, account.settings.multibrand_includes_help_centers
      end
    end

    describe 'with incorrect shard number' do
      it 'raises ActiveRecord::AdapterNotSpecified error' do
        assert_raises ActiveRecord::AdapterNotSpecified do
          backfill_multiple_hc.perform(shard_number: 100, dry_run: true)
        end
      end
    end

    describe 'with no shard number i.e running on all shards in pod' do
      before do
        backfill_multiple_hc.any_instance.stubs(:validate_and_update_accounts).returns(true)
      end

      it 'logs saying dry_run is true and multihc flag is not updated' do
        logger.expects(:info).with(regexp_matches(/Dry run?: false|On shard?|On pod?/))
        backfill_multiple_hc.perform(dry_run: true)
        assert_equal false, account.settings.multibrand_includes_help_centers
      end
    end
  end

  describe 'while running on pods and shards with dry_run=false' do
    let(:session) { stub }

    before do
      backfill_multiple_hc.any_instance.stubs(active_purchased_accounts: valid_accounts)
      backfill_multiple_hc.any_instance.stubs(zuora_account_id: zuora_account_id)
      backfill_multiple_hc.any_instance.stubs(:zuora_session).returns(session)
      session.stubs(:update).with(zuora_account_id, multi_brand_includes_multi_hc__c: true)
      backfill_multiple_hc.any_instance.stubs(needs_grandfathering?: true)
    end

    describe 'with correct shard_number' do
      it 'updates the account_setting flag' do
        backfill_multiple_hc.perform(shard_number: 1, dry_run: false)
        assert_equal true, account.settings.multibrand_includes_help_centers
      end
    end

    describe 'with incorrect shard number' do
      it 'raises ActiveRecord::AdapterNotSpecified error' do
        assert_raises ActiveRecord::AdapterNotSpecified do
          backfill_multiple_hc.perform(shard_number: 100, dry_run: true)
        end
      end
    end

    describe 'without no shard number i.e running on all shards on pod' do
      it 'loops through all accounts in pods and updates the settings' do
        backfill_multiple_hc.perform(dry_run: false)
        assert_equal true, account.settings.multibrand_includes_help_centers
      end
    end
  end
end
