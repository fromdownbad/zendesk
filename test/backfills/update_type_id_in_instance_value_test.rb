require_relative "../support/test_helper"

SingleCov.covered!

describe UpdateTypeIdInInstanceValue do
  fixtures :tickets, :accounts

  let(:ticket) { tickets(:minimum_2) }
  let(:ticket_deleted) { tickets(:minimum_deleted) }
  let(:ticket_archived) { tickets(:minimum_archived) }
  let(:ticket_closed) do
    ticket = tickets(:minimum_1)
    ticket.update_column :status_id, StatusType.CLOSED
    ticket
  end
  let(:ticket_from_other_account) do
    ticket = tickets(:minimum_3)
    account = accounts(:support)
    ticket.update_column :account_id, account.id
    ticket
  end
  let(:migrated_ticket) do
    migrated_ticket = tickets(:minimum_3)
    migrated_ticket.update_column :status_id, StatusType.CLOSED
    migrated_ticket
  end

  describe "replacing type_id in closed, archived or deleted tickets" do
    before do
      [ticket, ticket_closed, ticket_deleted, ticket_archived, ticket_from_other_account].each do |current_ticket|
        ['11EA74D156FA6A7BBB9F479DAD4B02E8', '11EA74D15CD98F2CBB9F7FE966B25807'].each do |attribute_value_id|
          UpdateTypeIdInInstanceValue::InstanceValue.create(
            account_id: current_ticket.account_id,
            attribute_value_id: Array(attribute_value_id).pack('H*'),
            type_id: 10,
            instance_id: current_ticket.id
          )
        end
      end

      archive_and_delete ticket_archived
    end

    describe "#perform" do
      describe "dry_run :false" do
        let(:backfill) { UpdateTypeIdInInstanceValue.new(account_id: ticket_closed.account_id, dry_run: false) }
        before do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with("backfill_completed", tags: ["subdomain:#{ticket.account.subdomain}, backfill_type:type_id, total_registers:6"]).once
          backfill.perform
        end

        it "changes type_id to 11 on closed, deleted and archived tickets skills" do
          [ticket_closed, ticket_deleted, ticket_archived].each do |current_ticket|
            assert_equal 2, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: current_ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_SKILLS_CLOSED_TYPE, instance_id: current_ticket.id).count
          end
        end

        it "doesn't change type_id on unclosed ticket and ticket from other accounts" do
          [ticket, ticket_from_other_account].each do |current_ticket|
            assert_equal 0, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: current_ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_SKILLS_CLOSED_TYPE, instance_id: current_ticket.id).count
          end
        end
      end

      describe "dry_run :true" do
        let(:backfill) { UpdateTypeIdInInstanceValue.new(account_id: ticket_closed.account_id, dry_run: true) }
        before do
          Zendesk::StatsD::Client.any_instance.expects(:increment).never
          backfill.perform
        end

        it "doesn't change type_id" do
          [ticket_closed, ticket_deleted, ticket_archived, ticket, ticket_from_other_account].each do |current_ticket|
            assert_equal 0, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: current_ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_SKILLS_CLOSED_TYPE, instance_id: current_ticket.id).count
          end
        end
      end
    end
  end

  describe "Rollback" do
    before do
      ['11EA74D156FA6A7BBB9F479DAD4B02E8', '11EA74D15CD98F2CBB9F7FE966B25807'].each do |attribute_value_id|
        UpdateTypeIdInInstanceValue::InstanceValue.create(
          account_id: ticket.account_id,
          attribute_value_id: Array(attribute_value_id).pack('H*'),
          type_id: 10,
          instance_id: ticket.id
        )
      end

      [ticket_closed, ticket_deleted, ticket_archived, ticket_from_other_account].each do |current_ticket|
        ['11EA74D156FA6A7BBB9F479DAD4B02E8', '11EA74D15CD98F2CBB9F7FE966B25807'].each do |attribute_value_id|
          UpdateTypeIdInInstanceValue::InstanceValue.create(
            account_id: current_ticket.account_id,
            attribute_value_id: Array(attribute_value_id).pack('H*'),
            type_id: 11,
            instance_id: current_ticket.id
          )
        end
      end

      archive_and_delete ticket_archived
    end

    describe "#perform" do
      describe "dry_run :false" do
        let(:backfill) { UpdateTypeIdInInstanceValue.new(account_id: ticket_closed.account_id, dry_run: false, rollback: true) }
        before do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with("backfill_completed", tags: ["subdomain:#{ticket.account.subdomain}, backfill_type:rollback, total_registers:6"]).once
          backfill.perform
        end

        it "restores type_id to 10" do
          [ticket_closed, ticket_deleted, ticket_archived].each do |current_ticket|
            assert_equal 2, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: current_ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_TYPE, instance_id: current_ticket.id).count
          end
        end

        it "doesn't change type_id on unclosed ticket and ticket from other accounts" do
          assert_equal 2, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_TYPE, instance_id: ticket.id).count
          assert_equal 0, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: ticket_from_other_account.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_TYPE, instance_id: ticket_from_other_account.id).count
        end
      end

      describe "dry_run :true" do
        let(:backfill) { UpdateTypeIdInInstanceValue.new(account_id: ticket_closed.account_id, dry_run: true, rollback: true) }
        before do
          Zendesk::StatsD::Client.any_instance.expects(:increment).never
          backfill.perform
        end

        it "doesn't change type_id" do
          [ticket_closed, ticket_deleted, ticket_archived, ticket_from_other_account].each do |current_ticket|
            assert_equal 0, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: current_ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_TYPE, instance_id: current_ticket.id).count
          end

          assert_equal 0, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_SKILLS_CLOSED_TYPE, instance_id: ticket.id).count
        end
      end
    end
  end

  describe "replacing subtype_id in closed, archived or deleted tickets" do
    before do
      [ticket].each do |current_ticket|
        ['11EA74D156FA6A7BBB9F479DAD4B02E8', '11EA74D15CD98F2CBB9F7FE966B25807'].each do |attribute_value_id|
          UpdateTypeIdInInstanceValue::InstanceValue.create(
            account_id: current_ticket.account_id,
            attribute_value_id: Array(attribute_value_id).pack('H*'),
            type_id: 10,
            instance_id: current_ticket.id,
            subtype_id: 0
          )
        end
      end

      [migrated_ticket].each do |current_ticket|
        ['11EA74D156FA6A7BBB9F479DAD4B02E8', '11EA74D15CD98F2CBB9F7FE966B25807'].each do |attribute_value_id|
          UpdateTypeIdInInstanceValue::InstanceValue.create(
            account_id: current_ticket.account_id,
            attribute_value_id: Array(attribute_value_id).pack('H*'),
            type_id: 10,
            instance_id: current_ticket.id,
            subtype_id: 1
          )
        end
      end

      [ticket_closed, ticket_deleted, ticket_archived, ticket_from_other_account].each do |current_ticket|
        ['11EA74D156FA6A7BBB9F479DAD4B02E8', '11EA74D15CD98F2CBB9F7FE966B25807'].each do |attribute_value_id|
          UpdateTypeIdInInstanceValue::InstanceValue.create(
            account_id: current_ticket.account_id,
            attribute_value_id: Array(attribute_value_id).pack('H*'),
            type_id: 11,
            instance_id: current_ticket.id,
            subtype_id: 0
          )
        end
      end

      archive_and_delete ticket_archived
    end

    describe "#perform" do
      describe "dry_run :false" do
        let(:backfill) { UpdateTypeIdInInstanceValue.new(account_id: ticket_closed.account_id, dry_run: false, use_subtype_id: true) }
        before do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with("backfill_completed", tags: ["subdomain:#{ticket.account.subdomain}, backfill_type:subtype_id, total_registers:6"]).once
          backfill.perform
        end

        it "changes subtype_id to 1 and restore type_id to 10 on closed, deleted and archived tickets skills" do
          [ticket_closed, ticket_deleted, ticket_archived].each do |current_ticket|
            assert_equal 2, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: current_ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_TYPE, instance_id: current_ticket.id).count
            assert_equal 2, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: current_ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_TYPE, subtype_id: UpdateTypeIdInInstanceValue::TICKET_CLOSED_SUBTYPE, instance_id: current_ticket.id).count
          end
        end

        it "doesn't change subtype_id on unclosed ticket" do
          assert_equal 0, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_TYPE, subtype_id: UpdateTypeIdInInstanceValue::TICKET_CLOSED_SUBTYPE, instance_id: ticket.id).count
        end

        it "doesn't change subtype_id on tickets from other accounts" do
          assert_equal 0, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: ticket_from_other_account.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_SKILLS_CLOSED_TYPE, subtype_id: UpdateTypeIdInInstanceValue::TICKET_CLOSED_SUBTYPE, instance_id: ticket_from_other_account.id).count
        end
      end

      describe "dry_run :true" do
        let(:backfill) { UpdateTypeIdInInstanceValue.new(account_id: ticket_closed.account_id, dry_run: true) }
        before do
          Zendesk::StatsD::Client.any_instance.expects(:increment).never
          backfill.perform
        end

        it "doesn't change subtype_id" do
          [ticket_closed, ticket_deleted, ticket_archived, ticket, ticket_from_other_account].each do |current_ticket|
            assert_equal 0, UpdateTypeIdInInstanceValue::InstanceValue.where(account_id: current_ticket.account_id, type_id: UpdateTypeIdInInstanceValue::TICKET_SKILLS_CLOSED_TYPE, subtype_id: UpdateTypeIdInInstanceValue::TICKET_CLOSED_SUBTYPE, instance_id: current_ticket.id).count
          end
        end
      end

      describe "doesn't modify migrated, unclosed and tickets from other accounts" do
        let(:backfill) { UpdateTypeIdInInstanceValue.new(account_id: ticket_closed.account_id, dry_run: false, use_subtype_id: true) }

        it "modifies only the subtype_id  only in closed, archived and deleted tickets" do
          assert_sql_queries 6, /`instance_values`.`subtype_id` = 1/ do
            backfill.perform
          end
        end
      end
    end
  end
end
