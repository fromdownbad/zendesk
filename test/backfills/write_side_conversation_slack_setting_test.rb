require_relative '../support/test_helper'
require_relative '../../app/backfills/write_side_conversation_slack_setting'

SingleCov.covered!

describe WriteSideConversationSlackSetting do
  fixtures :accounts, :account_settings

  describe '#perform' do
    let(:agent)   { users(:minimum_agent) }
    let(:account) { agent.account }

    let(:task) do
      WriteSideConversationSlackSetting.new(account_id: account.id, dry_run: false)
    end

    describe 'when collaboration_slack arturo is enabled' do
      before do
        Arturo.enable_feature!(:collaboration_slack)
      end

      describe 'and the ticket_threads setting is set to true' do
        before do
          account.settings.ticket_threads = true
          account.save!
        end

        describe 'with no existing setting value' do
          before do
            AccountSetting.where(name: 'side_conversations_slack').destroy_all
            task.perform
            account.reload
          end

          it 'sets the side_conversations_slack setting to true' do
            assert account.settings.side_conversations_slack
          end
        end

        describe 'with no existing setting value and dry_run = true' do
          let(:task) do
            WriteSideConversationSlackSetting.new(account_id: account.id, dry_run: true)
          end

          before do
            AccountSetting.where(name: 'side_conversations_slack').destroy_all
            task.perform
            account.reload
          end

          it 'does not set the side_conversations_slack setting to true' do
            refute account.settings.side_conversations_slack
          end
        end

        describe 'with an existing setting value of false' do
          before do
            account.settings.side_conversations_slack = false
            account.save!
            task.perform
            account.reload
          end

          it 'keeps side_conversations_slack setting at original value' do
            refute account.settings.side_conversations_slack
          end
        end
      end

      describe 'and the ticket_threads setting is set to false' do
        before do
          account.settings.ticket_threads = false
          account.save!
        end

        describe 'with no existing setting value' do
          before do
            AccountSetting.where(name: 'side_conversations_slack').destroy_all
            task.perform
            account.reload
          end

          it 'keeps the side_conversations_slack setting set to false' do
            refute account.settings.side_conversations_slack
          end
        end

        describe 'with an existing setting value' do
          before do
            account.settings.ticket_threads = false
            account.settings.side_conversations_slack = true
            account.save
            task.perform
            account.reload
          end

          it 'keeps side_conversations_slack setting at original value' do
            assert account.settings.side_conversations_slack
          end
        end
      end
    end

    describe 'when collaboration_slack arturo is disabled' do
      before do
        Arturo.disable_feature!(:collaboration_slack)
        task.perform
        account.reload
      end

      it 'does not set the side_conversations_slack setting' do
        refute account.settings.side_conversations_slack
      end
    end
  end
end
