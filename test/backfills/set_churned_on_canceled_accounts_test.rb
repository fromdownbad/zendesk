require_relative '../support/test_helper'

SingleCov.covered!

describe SetChurnedOnCanceledAccounts do

  let(:backfill) { SetChurnedOnCanceledAccounts.new({dry_run: false}) }
  let(:backfill_dry_run) { SetChurnedOnCanceledAccounts.new({dry_run: true}) }
  let(:account) { accounts(:minimum) }

  setup do
    Zendesk::Accounts::DataCenter.stubs(current_pod_shard_ids: [1])
  end

  describe "#set_churned_on" do

    it "modifies the column" do
      assert account.subscription.canceled_on.nil?
      backfill.send(:set_churned_on,account.subscription)
      account.subscription.reload
      refute account.subscription.canceled_on.nil?
    end

    it "modifies nothing on dry-run" do
      assert account.subscription.canceled_on.nil?
      backfill_dry_run.send(:set_churned_on,account.subscription)
      account.subscription.reload
      assert account.subscription.canceled_on.nil?
    end
  end

  describe "#canceled_accounts" do

    it "doesn't include an active account" do
      refute backfill.send(:canceled_accounts).include?(account)
    end

    it "includes a canceled account" do
      account.update_column(:is_active,false)
      account.update_column(:is_serviceable,false)
      assert backfill.send(:canceled_accounts).include?(account)
    end

  end

  describe "#perform" do

    it "changes nothing on dry-run" do
      account.update_column(:is_active,false)
      account.update_column(:is_serviceable,false)
      assert account.subscription.canceled_on.nil?

      backfill_dry_run.perform

      account.reload
      assert account.subscription.canceled_on.nil?
    end

    it "modifies a canceled account" do
      account.update_column(:is_active,false)
      account.update_column(:is_serviceable,false)
      assert account.subscription.canceled_on.nil?

      backfill.perform

      account.reload
      refute account.subscription.canceled_on.nil?

    end

    it "ignores an active account" do
      assert account.subscription.canceled_on.nil?

      backfill.perform

      account.reload
      assert account.subscription.canceled_on.nil?
    end

  end

end
