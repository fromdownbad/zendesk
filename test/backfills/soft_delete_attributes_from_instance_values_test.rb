require_relative "../support/test_helper"

SingleCov.covered!

describe SoftDeleteAttributesFromInstanceValues do
  fixtures :tickets, :accounts
  let(:account) { accounts(:minimum) }
  let(:ticket1) { tickets(:minimum_1) }
  let(:ticket2) { tickets(:minimum_2) }
  let(:ticket3) { tickets(:minimum_3) }

  let(:language_attribute_id) { "11EABAD266D0F54880FC5B5B263CE15D" }
  let(:language_attribute_uuid) { "66d0f548-bad2-11ea-80fc-5b5b263ce15d" }
  let(:second_language_attribute_id) { "11EABD0AAAD8D104BE5DF5038699C8CE" }
  let(:second_language_attribute_uuid) { "aad8d104-bd0a-11ea-be5d-f5038699c8ce" }
  let(:location_attribute_id) { "11EABAD44116FD7C80FCDBBD211344AF" }
  let(:location_attribute_uuid) { "4116fd7c-bad4-11ea-80fc-dbbd211344af" }

  let(:english_attribute_value_id) { "11EABAD31ADAE4B980FC295E5DE4BD62" }
  let(:english_attribute_value_uuid) { "1adae4b9-bad3-11ea-80fc-295e5de4bd62" }
  let(:spanish_attribute_value_id) { "11EABAD323ABD22A80FCAD84ABD52C32" }
  let(:spanish_attribute_value_uuid) { "23abd22a-bad3-11ea-80fc-ad84abd52c32" }
  let(:japanese_attribute_value_id) { "11EABAD32EA5BA1B80FC3B8D7561CB55" }
  let(:japanese_attribute_value_uuid) { "2ea5ba1b-bad3-11ea-80fc-3b8d7561cb55" }

  let(:english_second_attribute_value_id) { "11EABD0AB32991F5BE5DE5345F69FF58" }
  let(:english_second_attribute_value_uuid) { "b32991f5-bd0a-11ea-be5d-e5345f69ff58" }
  let(:spanish_second_attribute_value_id) { "11EABD0AB9CDB456BE5DB98041B96F57" }
  let(:spanish_second_attribute_value_uuid) { "b9cdb456-bd0a-11ea-be5d-b98041b96f57" }

  let(:europe_attribute_value_id) { "11EABAD447F7DABD80FC9BC80E01272D" }
  let(:europe_attribute_value_uuid) { "47f7dabd-bad4-11ea-80fc-9bc80e01272d" }
  let(:asia_attribute_value_id) { "11EABAD44EC704BE80FC97BD3E305D3A" }
  let(:asia_attribute_value_uuid) { "4ec704be-bad4-11ea-80fc-97bd3e305d3a" }
  let(:america_attribute_value_id) { "11EABAD45304AE6F80FC7D463B7D4736" }
  let(:america_attribute_value_uuid) { "5304ae6f-bad4-11ea-80fc-7d463b7d4736" }

  let!(:timestamp) { 10.minutes.ago.at_beginning_of_minute.strftime("%F %T") }
  let!(:timestamp_deleted_at) { 10.minute.since.at_beginning_of_minute.strftime("%F %T") }
  let!(:previusly_deleted_time) { 1.week.ago.at_beginning_of_minute.strftime("%F %T") }

  before do
    sql = "INSERT INTO `attributes` (`id`, `account_id`, `name`, `created_at`, `updated_at`, `description`, `deleted_at`)"\
    "VALUES"\
      "(X'#{language_attribute_id}',#{account.id},'Language','#{timestamp}','#{timestamp}','',NULL),"\
      "(X'#{location_attribute_id}',#{account.id},'Location','#{timestamp}','#{timestamp}','',NULL);"
    ActiveRecord::Base.connection.execute(sql)

    sql = "INSERT INTO `attribute_values` (`id`, `account_id`, `attribute_id`, `name`, `created_at`, `updated_at`, `description`, `deleted_at`)" \
        "VALUES" \
          "(X'#{english_attribute_value_id}',#{account.id},X'#{language_attribute_id}','English','#{timestamp}','#{timestamp}','',NULL),"\
          "(X'#{spanish_attribute_value_id}',#{account.id},X'#{language_attribute_id}','Spanish','#{timestamp}','#{timestamp}','',NULL),"\
          "(X'#{japanese_attribute_value_id}',#{account.id},X'#{language_attribute_id}','Japanese','#{timestamp}','#{timestamp}','',NULL),"\
          "(X'#{english_second_attribute_value_id}',#{account.id},X'#{language_attribute_id}','English','#{timestamp}','#{timestamp}','',NULL),"\
          "(X'#{spanish_second_attribute_value_id}',#{account.id},X'#{language_attribute_id}','Spanish','#{timestamp}','#{timestamp}','',NULL),"\
          "(X'#{europe_attribute_value_id}',#{account.id},X'#{location_attribute_id}','Europe','#{timestamp}','#{timestamp}','',NULL),"\
          "(X'#{asia_attribute_value_id}',#{account.id},X'#{location_attribute_id}','Asia','#{timestamp}','#{timestamp}','',NULL),"\
          "(X'#{america_attribute_value_id}',#{account.id},X'#{location_attribute_id}','America','#{timestamp}','#{timestamp}','',NULL);"
    ActiveRecord::Base.connection.execute(sql)

    sql = "INSERT INTO `instance_values` (`id`, `account_id`, `attribute_value_id`, `type_id`, `instance_id`, `created_at`, `updated_at`, `deleted_at`, `subtype_id`)"\
        "VALUES"\
          "(X'11EABC706F7355D1BE5D11A308FAE173',#{account.id},X'#{english_attribute_value_id}',10,'#{ticket1.id}','#{timestamp}','#{timestamp}',NULL,0),"\
          "(X'11EABC706F7355D2BE5D83F51F268CE7',#{account.id},X'#{spanish_attribute_value_id}',10,'#{ticket1.id}','#{timestamp}','#{timestamp}',NULL,0),"\
          "(X'11EABC706F7355D3BE5D25A73C33EFFA',#{account.id},X'#{japanese_attribute_value_id}',10,'#{ticket1.id}','#{timestamp}','#{timestamp}',NULL,0),"\
          "(X'11EABC70277A0ACEBE5DD5650D5AF819',#{account.id},X'#{europe_attribute_value_id}',10,'#{ticket2.id}','#{timestamp}','#{timestamp}',NULL,0),"\
          "(X'11EABC70277A0ACFBE5DE5513E666F13',#{account.id},X'#{asia_attribute_value_id}',10,'#{ticket2.id}','#{timestamp}','#{timestamp}',NULL,0),"\
          "(X'11EABC70277A0AD0BE5D27F48BC6197E',#{account.id},X'#{america_attribute_value_id}',10,'#{ticket2.id}','#{timestamp}','#{timestamp}',NULL,0),"\
          "(X'11EABAD44EC704BE80FC97BD3E305D3A',#{account.id},X'#{japanese_attribute_value_id}',10,'#{ticket3.id}','#{previusly_deleted_time}','#{previusly_deleted_time}','#{previusly_deleted_time}',0);"
    ActiveRecord::Base.connection.execute(sql)

    sql = "INSERT INTO `attribute_ticket_maps` (`id`, `account_id`, `attribute_id`,`attribute_value_id`, `definition`, `created_at`, `updated_at`, `deleted_at`)"\
        "VALUES"\
          "(1,#{account.id},'#{language_attribute_uuid}','#{english_attribute_value_uuid}','','#{timestamp}','#{timestamp}',NULL),"\
          "(2,#{account.id},'#{language_attribute_uuid}','#{spanish_attribute_value_uuid}','','#{timestamp}','#{timestamp}',NULL),"\
          "(3,#{account.id},'#{language_attribute_uuid}','#{japanese_attribute_value_uuid}','','#{timestamp}','#{timestamp}',NULL),"\
          "(4,#{account.id},'#{location_attribute_uuid}','#{europe_attribute_value_uuid}','','#{timestamp}','#{timestamp}',NULL),"\
          "(5,#{account.id},'#{location_attribute_uuid}','#{asia_attribute_value_uuid}','','#{timestamp}','#{timestamp}',NULL),"\
          "(6,#{account.id},'#{location_attribute_uuid}','#{america_attribute_value_uuid}','','#{timestamp}','#{timestamp}',NULL),"\
          "(7,#{account.id},'#{second_language_attribute_uuid}','#{english_second_attribute_value_uuid}','','#{timestamp}','#{timestamp}',NULL),"\
          "(8,#{account.id},'#{second_language_attribute_uuid}','#{spanish_second_attribute_value_uuid}','','#{timestamp}','#{timestamp}',NULL);"

    ActiveRecord::Base.connection.execute(sql)
  end

  describe "with dry_run false" do
    describe "soft delete attribute" do
      describe "search by id" do
        before do
          SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, deleted_at: timestamp_deleted_at, dry_run: false).perform
        end
        it "deletes attribute" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::Attribute.not_deleted.where("id = UNHEX('#{language_attribute_id}')").count
        end

        it "deletes attribute_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.not_deleted.where("attribute_id = UNHEX('#{language_attribute_id}')").count
        end

        it "deletes instance_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.not_deleted.where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
        end

        it "deletes attribute_ticket_maps" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.not_deleted.where(account_id: account.id, attribute_id: language_attribute_uuid).count
        end

        it "doesn't delete attribute_ticket_maps from different attribute" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.deleted.where(account_id: account.id, attribute_id: location_attribute_uuid).count
        end

        describe "rollback" do
          before do
            SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, rollback: true, deleted_at: timestamp_deleted_at, dry_run: false).perform
          end

          it "restores attribute" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::Attribute.deleted.where("id = UNHEX('#{language_attribute_id}')").count
          end

          it "restores attribute_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.deleted.where("attribute_id = UNHEX('#{language_attribute_id}')").count
          end

          it "restores instance_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.deleted.where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
          end

          it "restores attribute_ticket_maps" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.deleted.where(account_id: account.id, attribute_id: language_attribute_uuid).count
          end

          it "doesn't restore instance_values deleted previusly" do
            assert_equal 1, SoftDeleteAttributesFromInstanceValues::InstanceValue.deleted.where(account_id: account.id, instance_id: ticket3.id, type_id: 10).count
          end
        end
      end

      describe "search by name" do
        before do
          SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_name: "Location", deleted_at: timestamp_deleted_at, dry_run: false).perform
        end
        it "deletes attribute" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::Attribute.not_deleted.where("id = UNHEX('#{location_attribute_id}')").count
        end

        it "deletes attribute_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.not_deleted.where("attribute_id = UNHEX('#{location_attribute_id}')").count
        end

        it "deletes instance_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.not_deleted.where(account_id: account.id, instance_id: ticket2.id, type_id: 10).count
        end

        it "deletes attribute_ticket_maps" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.not_deleted.where(account_id: account.id, attribute_id: location_attribute_uuid).count
        end

        it "doesn't delete attribute_ticket_maps from different attribute" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.deleted.where(account_id: account.id, attribute_id: language_attribute_uuid).count
        end

        describe "rollback" do
          before do
            SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_name: "Location", rollback: true, deleted_at: timestamp_deleted_at, dry_run: false).perform
          end

          it "restores attribute" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::Attribute.deleted.where("id = UNHEX('#{location_attribute_id}')").count
          end

          it "restores attribute_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.deleted.where("attribute_id = UNHEX('#{location_attribute_id}')").count
          end

          it "restores instance_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.deleted.where(account_id: account.id, instance_id: ticket2.id, type_id: 10).count
          end

          it "restores attribute_ticket_maps" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.deleted.where(account_id: account.id, attribute_id: location_attribute_uuid).count
          end
        end
      end

      describe "search by uuid" do
        before do
          SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', uuid: language_attribute_uuid, deleted_at: timestamp_deleted_at, dry_run: false).perform
        end
        it "deletes attribute" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::Attribute.not_deleted.where("id = UNHEX('#{language_attribute_id}')").count
        end

        it "deletes attribute_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.not_deleted.where("attribute_id = UNHEX('#{language_attribute_id}')").count
        end

        it "deletes instance_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.not_deleted.where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
        end

        it "deletes attribute_ticket_maps" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.not_deleted.where(account_id: account.id, attribute_id: language_attribute_uuid).count
        end

        it "doesn't delete attribute_ticket_maps from different attribute" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.deleted.where(account_id: account.id, attribute_id: location_attribute_uuid).count
        end

        describe "rollback" do
          before do
            SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', uuid: language_attribute_uuid, rollback: true, deleted_at: timestamp_deleted_at, dry_run: false).perform
          end

          it "restores attribute" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::Attribute.deleted.where("id = UNHEX('#{language_attribute_id}')").count
          end

          it "restores attribute_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.deleted.where("attribute_id = UNHEX('#{language_attribute_id}')").count
          end

          it "restores instance_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.deleted.where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
          end

          it "restores attribute_ticket_maps" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.deleted.where(account_id: account.id, attribute_id: language_attribute_uuid).count
          end

          it "doesn't restore instance_values deleted previusly" do
            assert_equal 1, SoftDeleteAttributesFromInstanceValues::InstanceValue.deleted.where(account_id: account.id, instance_id: ticket3.id, type_id: 10).count
          end
        end
      end
    end

    describe "soft delete attribute value" do
      describe "search by id" do
        before do
          assert_sql_queries(1, /UPDATE `attribute_ticket_maps`/) do
            SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attribute_values', attribute_value_id: english_attribute_value_id, deleted_at: timestamp_deleted_at, dry_run: false).perform
          end
        end

        it "deletes attribute_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.not_deleted.where("id = UNHEX('#{english_attribute_value_id}')").count
        end

        it "deletes instance_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.not_deleted.where("attribute_value_id = UNHEX('#{english_attribute_value_id}')").where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
        end

        it "deletes attribute_ticket_maps" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.not_deleted.where(account_id: account.id, attribute_value_id: english_attribute_value_uuid).count
        end

        describe "rollback" do
          before do
            assert_sql_queries(1, /UPDATE `attribute_ticket_maps`/) do
              SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attribute_values', attribute_value_id: english_attribute_value_id, rollback: true, deleted_at: timestamp_deleted_at, dry_run: false).perform
            end
          end

          it "restores attribute_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.deleted.where("id = UNHEX('#{english_attribute_value_id}')").count
          end

          it "restores instance_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.deleted.where("attribute_value_id = UNHEX('#{english_attribute_value_id}')").where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
          end

          it "restores attribute_ticket_maps" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.deleted.where(account_id: account.id, attribute_value_id: english_attribute_value_uuid).count
          end
        end
      end

      describe "search by name" do
        describe "getting a single result" do
          before do
            assert_sql_queries(1, /UPDATE `attribute_ticket_maps`/) do
              SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attribute_values', attribute_value_name: "Japanese", deleted_at: timestamp_deleted_at, dry_run: false).perform
            end
          end

          it "deletes attribute_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.not_deleted.where("id = UNHEX('#{japanese_attribute_value_id}')").count
          end

          it "deletes instance_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.not_deleted.where("attribute_value_id = UNHEX('#{japanese_attribute_value_id}')").where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
          end

          it "deletes attribute_ticket_maps" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.not_deleted.where(account_id: account.id, attribute_value_id: japanese_attribute_value_uuid).count
          end

          describe "rollback" do
            before do
              assert_sql_queries(1, /UPDATE `attribute_ticket_maps`/) do
                SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attribute_values', attribute_value_name: "Japanese", rollback: true, deleted_at: timestamp_deleted_at, dry_run: false).perform
              end
            end

            it "restores attribute_values" do
              assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.deleted.where("id = UNHEX('#{japanese_attribute_value_id}')").count
            end

            it "restores instance_values" do
              assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.deleted.where("attribute_value_id = UNHEX('#{japanese_attribute_value_id}')").where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
            end

            it "restores attribute_ticket_maps" do
              assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.deleted.where(account_id: account.id, attribute_value_id: japanese_attribute_value_uuid).count
            end
          end
        end

        describe "getting more than one result" do
          it "doesn't delete attribute_values" do
            Rails.logger.expects(:info).once
            Rails.logger.expects(:info).with("UpdateTypeIdInInstanceValue: more than one result").once
            SoftDeleteAttributesFromInstanceValues::AttributeValue.any_instance.expects(:soft_delete).never
            SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attribute_values', attribute_value_name: "Spanish", deleted_at: timestamp_deleted_at, dry_run: false).perform
          end
        end
      end

      describe "search by uuid" do
        before do
          assert_sql_queries(1, /UPDATE `attribute_ticket_maps`/) do
            SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attribute_values', uuid: english_attribute_value_uuid, deleted_at: timestamp_deleted_at, dry_run: false).perform
          end
        end

        it "deletes attribute_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.not_deleted.where("id = UNHEX('#{english_attribute_value_id}')").count
        end

        it "deletes instance_values" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.not_deleted.where("attribute_value_id = UNHEX('#{english_attribute_value_id}')").where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
        end

        it "deletes attribute_ticket_maps" do
          assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.not_deleted.where(account_id: account.id, attribute_value_id: english_attribute_value_uuid).count
        end

        describe "rollback" do
          before do
            assert_sql_queries(1, /UPDATE `attribute_ticket_maps`/) do
              SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attribute_values', uuid: english_attribute_value_uuid, rollback: true, deleted_at: timestamp_deleted_at, dry_run: false).perform
            end
          end

          it "restores attribute_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeValue.deleted.where("id = UNHEX('#{english_attribute_value_id}')").count
          end

          it "restores instance_values" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.deleted.where("attribute_value_id = UNHEX('#{english_attribute_value_id}')").where(account_id: account.id, instance_id: ticket1.id, type_id: 10).count
          end

          it "restores attribute_ticket_maps" do
            assert_equal 0, SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.deleted.where(account_id: account.id, attribute_value_id: english_attribute_value_uuid).count
          end
        end
      end
    end

    describe "resume attribute soft deletion" do
      before do
        SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, deleted_at: timestamp_deleted_at, dry_run: false).perform

        sql = "INSERT INTO `instance_values` (`id`, `account_id`, `attribute_value_id`, `type_id`, `instance_id`, `created_at`, `updated_at`, `deleted_at`, `subtype_id`)"\
        "VALUES"\
          "(X'11EABD0AAAD8D104BE5DF5038699C8CE',#{account.id},X'#{spanish_attribute_value_id}',10,'#{ticket3.id}','#{timestamp}','#{timestamp}', NULL, 0);"
        ActiveRecord::Base.connection.execute(sql)
        SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, deleted_at: timestamp_deleted_at, dry_run: false).perform
      end

      it "deletes attribute_values" do
        assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.not_deleted.where("id = UNHEX('11EABD0AAAD8D104BE5DF5038699C8CE')").where(account_id: account.id, instance_id: ticket3.id, type_id: 10).count
        attribute = SoftDeleteAttributesFromInstanceValues::Attribute.where("id = UNHEX('#{language_attribute_id}')").first
        instance_value = SoftDeleteAttributesFromInstanceValues::InstanceValue.where("id = UNHEX('11EABD0AAAD8D104BE5DF5038699C8CE')").first
        assert attribute.deleted_at == instance_value.deleted_at
      end
    end

    describe "resume attribute soft deletion rollback" do
      before do
        SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, deleted_at: timestamp_deleted_at, dry_run: false).perform
        SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, deleted_at: timestamp_deleted_at, rollback: true, dry_run: false).perform

        sql = "INSERT INTO `instance_values` (`id`, `account_id`, `attribute_value_id`, `type_id`, `instance_id`, `created_at`, `updated_at`, `deleted_at`, `subtype_id`)"\
        "VALUES"\
          "(X'11EABD0AAAD8D104BE5DF5038699C8CE',#{account.id},X'#{spanish_attribute_value_id}',10,'#{ticket3.id}','#{timestamp}','#{timestamp}', '#{timestamp_deleted_at}', 0);"
        ActiveRecord::Base.connection.execute(sql)

        assert_sql_queries(1, /UPDATE `instance_values`/) do
          SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, deleted_at: timestamp_deleted_at, rollback: true, dry_run: false).perform
        end
      end

      it "restores remaning instance_values" do
        assert_equal 0, SoftDeleteAttributesFromInstanceValues::InstanceValue.deleted.where("attribute_value_id = UNHEX('#{spanish_attribute_value_id}')").where(account_id: account.id, instance_id: ticket3.id, type_id: 10).count
      end
    end

    describe "invalid deleted_at" do
      let(:invalid_deleted_at) { 1.week.ago.at_beginning_of_minute.strftime("%F %T") }
      it "doesn't soft delete attribute" do
        SoftDeleteAttributesFromInstanceValues::Attribute.any_instance.expects(:soft_delete).never
        SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, deleted_at: invalid_deleted_at, dry_run: false).perform
      end
    end
  end

  describe "with dry_run true" do
    it "doesn't delete attribute" do
      SoftDeleteAttributesFromInstanceValues::Attribute.any_instance.expects(:soft_delete).never
      SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, deleted_at: timestamp_deleted_at, dry_run: true).perform
    end
  end

  describe "transform id to uuid" do
    let!(:example_data) do
      [["11EA597F7AE30B50AE318F34CBA8F9EA", "7ae30b50-597f-11ea-ae31-8f34cba8f9ea"],
       ["11EA597FE577BED095AC5B4CE0A962AB", "e577bed0-597f-11ea-95ac-5b4ce0a962ab"],
       ["11EA8AD3409636BD96688F4DB46C2CEE", "409636bd-8ad3-11ea-9668-8f4db46c2cee"]]
    end

    it "tranforms model id to uuid" do
      example_data.each do |data|
        attribute = SoftDeleteAttributesFromInstanceValues::Attribute.new
        attribute.id = [data[0]].pack('H*')
        assert attribute.uuid == data[1]
      end
    end

    it "tranfoms uuid to model hex id" do
      backfill = SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes', attribute_id: language_attribute_id, deleted_at: timestamp_deleted_at, dry_run: true)
      example_data.each do |data|
        assert (backfill.uuid_to_hex data[1]).upcase, data[0]
      end
    end
  end
end
