require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe RevertTicketFieldUpdates do
  fixtures :accounts, :tickets, :users, :custom_field_options, :ticket_fields

  describe "Reverting a rule-caused ticket field update" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_admin) }
    let(:ticket) {tickets(:minimum_2)}

    before do
      @tagger = FieldTagger.create!(
        title: "Test tagger",
        account: account,
        is_active: true,
        custom_field_options: [
          CustomFieldOption.new(name: "Foo", value: "foo"),
          CustomFieldOption.new(name: "Bar", value: "bar"),
          CustomFieldOption.new(name: "Bar", value: "baz")
        ]
      )
    end

    def perform_backfill(dry_run: true)
      RevertTicketFieldUpdates.new(
        dry_run: dry_run,
        account_id: account.id,
        ticket_ids:[ticket.nice_id],
        field_ids: [@tagger.id],
        identification_tag: "backfill_reset_ticket_updates"
      ).perform
    end

    describe "reverting a ticket field to its initial value" do
      it "does not make an update dry_run: false is set" do
        ticket.will_be_saved_by(user)
        ticket.fields = {@tagger.id => "foo"}
        ticket.save!
        ticket.will_be_saved_by(user)
        ticket.fields = {@tagger.id => "baz"}
        ticket.save!

        event_count = ticket.events.count
        perform_backfill

        assert_equal event_count, ticket.events.count
        assert_equal "baz", ticket.ticket_field_entries.where(ticket_field_id: @tagger.id).first.value
      end

      it "does not fail when there are no updates to the field" do
        event_count = ticket.events.count
        perform_backfill(dry_run: false)
        assert_equal event_count, ticket.events.count
        assert_nil ticket.ticket_field_entries.where(ticket_field_id: @tagger.id).first
      end

      it "does revert to the earliest value of the field" do
        ticket.will_be_saved_by(user)
        ticket.fields = {@tagger.id => "foo"}
        ticket.save!
        ticket.will_be_saved_by(user)
        ticket.fields = {@tagger.id => "baz"}
        ticket.save!

        event_count = ticket.events.count
        perform_backfill(dry_run: false)

        assert ticket.events.count > event_count
        assert_equal "foo", ticket.ticket_field_entries.where(ticket_field_id: @tagger.id).first.value
      end
    end
  end
end
