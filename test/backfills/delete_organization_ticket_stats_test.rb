require_relative "../support/test_helper"

SingleCov.covered!

describe DeleteOrganizationTicketStats do

  let(:account) { accounts(:minimum) }
  let(:connection) { ActiveRecord::Base.connection }
  before do
    org_stat = "INSERT INTO ticket_stats(id, account_id, duration, organization_id, ts, type, value)"\
               " VALUES(123, #{account.id}, 86400, 1, '#{Time.now.to_s(:db)}', 'org', 10)"
    non_org_stat = "INSERT INTO ticket_stats(id, account_id, duration, organization_id, ts, type, value)"\
                   " VALUES(456, #{account.id}, 86400, 0, '#{Time.now.to_s(:db)}', 'non_org', 10)"

    [org_stat, non_org_stat].each do |stat|
      connection.execute(stat)
    end
  end

  it "does not delete organization ticket stat when dry_run is true" do
    Kernel.expects(:sleep).with(1)
    DeleteOrganizationTicketStats.perform(dry_run: true)
    assert connection.select_values('select * from ticket_stats where id = 123').any?
    assert connection.select_values('select * from ticket_stats where id = 456').any?
  end

  it "deletes organization ticket stats when dry_run is false" do
    Kernel.expects(:sleep).with(1)
    DeleteOrganizationTicketStats.perform(dry_run: false)
    refute connection.select_values('select * from ticket_stats where id = 123').any?
    assert connection.select_values('select * from ticket_stats where id = 456').any?
  end

  describe "when shards are specified for the backfill" do
    let(:backfill_shard_201) { DeleteOrganizationTicketStats.new(dry_run: false, pod: 2, shards: [201]) }

    before do
      ActiveRecord::Base.expects(:on_shard).with(201)
      ActiveRecord::Base.expects(:on_shard).with(202).never
    end

    it "tries to delete organization ticket stat only on specified shards" do
      backfill_shard_201.perform
    end
  end

  describe "when batch_minimum_sleep is specified for the backfill" do
    let(:backfill_batch_minimum_sleep_5) { DeleteOrganizationTicketStats.new(batch_minimum_sleep: 5) }

    before { Kernel.expects(:sleep).with(5) }

    it "sleeps for the minimum time" do
      backfill_batch_minimum_sleep_5.perform
    end
  end
end
