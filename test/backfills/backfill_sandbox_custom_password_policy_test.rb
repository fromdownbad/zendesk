require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillSandboxCustomPasswordPolicy do
  fixtures :accounts

  def setup_sandbox(account_type)
    sandbox_master = accounts(account_type)
    sandbox = sandbox_master.reset_sandbox

    sandbox.role_settings.update_attribute(:agent_security_policy_id, 400)
    CustomSecurityPolicy.build_from_current_policy(sandbox_master).save

    sandbox.reload
    sandbox_master.reload
    assert sandbox_master.custom_security_policy
    refute sandbox.custom_security_policy

    sandbox
  end

  describe '#perform' do
    before do
      @minimum_sandbox = setup_sandbox(:minimum)
      @multiproduct_sandbox = setup_sandbox(:multiproduct)
    end

    describe 'dry run is false' do
      let(:dry_run) { false }

      it 'creates sandbox custom security policy' do
        BackfillSandboxCustomPasswordPolicy.perform(dry_run: dry_run)
        @minimum_sandbox.reload
        @multiproduct_sandbox.reload

        assert @minimum_sandbox.custom_security_policy
        assert @multiproduct_sandbox.custom_security_policy
      end
    end

    describe 'dry run is true' do
      let(:dry_run) { true }

      it 'does not create sandbox custom security policy' do
        BackfillSandboxCustomPasswordPolicy.perform(dry_run: dry_run)
        @minimum_sandbox.reload
        @multiproduct_sandbox.reload

        refute @minimum_sandbox.custom_security_policy
        refute @multiproduct_sandbox.custom_security_policy
      end

      it 'logs correctly' do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with("Sandox Account: #{@minimum_sandbox.id} custom password to be created from its sandbox master")
        Rails.logger.expects(:info).with("Sandox Account: #{@multiproduct_sandbox.id} custom password to be created from its sandbox master")

        BackfillSandboxCustomPasswordPolicy.perform(dry_run: dry_run)
      end
    end
  end
end
