require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillSandboxEndUserSecurityPolicyId do
  fixtures :accounts

  describe '#perform' do
    describe 'with accounts;' do
      before do
        @sandbox_master_1_account = accounts(:minimum)
        @sandbox_1_account = @sandbox_master_1_account.reset_sandbox

        @sandbox_master_2_account = accounts(:multiproduct)
        @sandbox_2_account = @sandbox_master_2_account.reset_sandbox

        @not_sandbox_account = accounts(:support)
        refute @not_sandbox_account.is_sandbox?
      end

      describe 'end user security policy id for sandbox_1:400, sandbox_2:100, not_sandbox_account:400' do
        before do
          @sandbox_1_account.role_settings.update_attribute(:end_user_security_policy_id, 400)
          @sandbox_master_1_account.role_settings.update_attribute(:end_user_security_policy_id, 200)

          assert_equal 100, @sandbox_master_2_account.role_settings.end_user_security_policy_id
          assert_equal 100, @sandbox_2_account.role_settings.end_user_security_policy_id

          @not_sandbox_account.role_settings.update_attribute(:end_user_security_policy_id, 400)
        end

        describe 'dry_run is false' do
          let(:dry_run) { false }

          before do
            Rails.logger.stubs(:info)
            Rails.logger.expects(:info).with("Total number of sandbox end user security policy ids updated = 1")

            BackfillSandboxEndUserSecurityPolicyId.perform(dry_run: dry_run)
          end

          it 'updates sandbox_1 end user security policy id to sandbox master' do
            @sandbox_1_account.role_settings.reload

            assert_equal @sandbox_master_1_account.role_settings.end_user_security_policy_id, @sandbox_1_account.role_settings.end_user_security_policy_id
          end

          it 'does not do anything on sandbox_2' do
            @sandbox_2_account.role_settings.reload

            assert_equal 100, @sandbox_2_account.role_settings.end_user_security_policy_id
          end

          it 'does not do anything on not_sandbox_account' do
            @not_sandbox_account.role_settings.reload

            assert_equal 400, @not_sandbox_account.role_settings.end_user_security_policy_id
          end
        end

        describe 'dry_run is true' do
          let(:dry_run) { true }

          it 'does not do anything' do
            BackfillSandboxEndUserSecurityPolicyId.perform(dry_run: dry_run)
            @sandbox_1_account.role_settings.reload
            @sandbox_master_1_account.role_settings.reload

            assert_equal 400, @sandbox_1_account.role_settings.end_user_security_policy_id
          end

          it 'logs correctly' do
            Rails.logger.stubs(:info)
            Rails.logger.expects(:info).with("Account #{@sandbox_1_account.id} end_user_security_policy_id to be updated from sandbox_master")

            BackfillSandboxEndUserSecurityPolicyId.perform(dry_run: dry_run)
          end
        end
      end
    end
  end
end
