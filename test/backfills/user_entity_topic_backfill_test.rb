require_relative "../support/test_helper"

SingleCov.covered!

describe UserEntityTopicBackfill do
  let(:minimum_account) { accounts(:minimum) }

  before do
    UserProtobufEncoder.any_instance.stubs(:to_proto)
  end

  describe "#perform" do
    it "calls publish for account when dry_run is false" do
      account_count = Account.count
      UserEntityPublisher.any_instance.expects(:publish_for_account).times(account_count)

      UserEntityTopicBackfill.new(dry_run: false, tag: "1").perform
    end

    it "does not call publish for account when dry_run is true" do
      UserEntityPublisher.any_instance.expects(:publish_for_account).never

      UserEntityTopicBackfill.new.perform
    end

    it "skips the account" do
      account_count = Account.count
      UserEntityPublisher.any_instance.expects(:publish_for_account).times(account_count - 1)

      UserEntityTopicBackfill.new(dry_run: false, tag: "1").perform(account_ids_to_skip: [minimum_account.id])
    end
  end

  describe "#perform_for_accounts" do
    it "does not call publish for account when dry_run is true" do
      UserEntityPublisher.any_instance.expects(:publish_for_account).never

      UserEntityTopicBackfill.new.perform_for_accounts(account_ids: [minimum_account])
    end

    it "calls publish for account when dry_run is false" do
      UserEntityPublisher.any_instance.expects(:publish_for_account).times(1)

      UserEntityTopicBackfill.new(dry_run: false).perform_for_accounts(account_id: accounts(:minimum).id)
    end

    it "does not call publish for account when dry_run is true" do
      UserEntityPublisher.any_instance.expects(:publish_for_account).never

      UserEntityTopicBackfill.new.perform_for_accounts(account_id: accounts(:minimum).id)
    end

    it "marks shard as processed" do
      backfill = UserEntityTopicBackfill.new(dry_run: false, tag: "1")
      backfill.perform

      assert_equal backfill.processed_shards, [1, 0]
    end

    it "marks accounts as skipped if an exception is raised" do
      account_ids = [accounts(:minimum), accounts(:minimum)].map(&:id)
      UserEntityPublisher.stubs(:new)
      UserEntityPublisher.stubs(:new).with(account_id: account_ids.first).
        raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::LostConnection, "connection lost")
      backfill = UserEntityTopicBackfill.new(dry_run: false, tag: "1")
      backfill.perform_for_accounts(account_ids: account_ids)

      assert_equal backfill.skipped_accounts, [account_ids.first.to_s, "0"]
    end

    it "marks the processed accounts" do
      UserEntityPublisher.any_instance.expects(:publish_for_account).once

      backfill = UserEntityTopicBackfill.new(dry_run: false, tag: "1")
      backfill.perform_for_accounts(account_ids: [minimum_account.id])

      assert_equal backfill.processed_accounts, [minimum_account.id.to_s, "0"]
    end
  end
end
