require_relative "../support/test_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe DeleteOrphanOauthTokens do
  before do
    Zendesk::OAuth::Client.all.each { |c| c.destroy }

    @client = FactoryBot.create(:client)
    @token  = FactoryBot.create(:token, :client => @client, :user => @client.user)

    @client.update_attribute(:deleted_at, Time.zone.now)
    assert_empty Zendesk::OAuth::Client.all
    assert_equal 1, Zendesk::OAuth::Token.count
  end

  it "deletes the orphan token if dry run is false" do
     DeleteOrphanOauthTokens.perform(:dry_run => false)

     assert_empty Zendesk::OAuth::Token.all
  end

  it "doesn't delete the orphan token if dry run is true" do
     DeleteOrphanOauthTokens.perform(:dry_run => true)

     assert_equal 1, Zendesk::OAuth::Token.count
  end
end
