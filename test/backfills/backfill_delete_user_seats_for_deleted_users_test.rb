require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillDeleteUserSeatsForDeletedUsers do
  fixtures :accounts
  fixtures :users

  let(:account) { accounts(:minimum) }
  let(:other_account) { accounts(:support) }
  let(:user)    { account.users.last }
  let(:user_of_other_account) { other_account.users.last }
  let(:voice_subscription) do
    stub(
      is_active?: false,
      max_agents: 2,
      plan_type:  3
    )
  end

  before do
    Account.any_instance.stubs(:voice_subscription).returns(voice_subscription)

    stub_request(:post, %r{/voice_seat_lost.json})

    user.user_seats.create(account: account, seat_type: 'voice').save!
    user_of_other_account.user_seats.create(account: other_account, seat_type: 'voice').save!

    user.update_column(:is_active, false)
    user_of_other_account.update_column(:is_active, false)
  end

  it 'deletes user seats for deleted users of the given account when dry run is false' do
    BackfillDeleteUserSeatsForDeletedUsers.perform(account_id: account.id, dry_run: false)
    user.user_seats.count.must_equal 0
    user_of_other_account.user_seats.count.must_equal 1
  end

  it 'does not delete user seats for deleted users of the given account when dry run is true' do
    BackfillDeleteUserSeatsForDeletedUsers.perform(account_id: account.id, dry_run: true)
    user.user_seats.count.must_equal 1
  end

  it 'does not delete user seats for active users of the given account' do
    user.update_column(:is_active, true)
    BackfillDeleteUserSeatsForDeletedUsers.perform(account_id: account.id, dry_run: false)
    user.user_seats.count.must_equal 1
  end
end
