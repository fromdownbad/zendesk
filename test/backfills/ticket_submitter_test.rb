require_relative "../support/test_helper"

SingleCov.covered!

describe TicketSubmitter do
  fixtures :users, :tickets

  let(:account) { accounts(:minimum) }
  let(:described_class) { TicketSubmitter }

  describe "updating ticket.submitter" do
    let(:ticket) { tickets(:minimum_1) }
    let(:submitter) { users(:minimum_end_user) }
    let(:invalid_submitter) { users(:minimum_sdk_end_user) }
    let(:submitter_id) { users(:minimum_end_user).id }

    describe "with an invalid submitter" do
      before do
        ticket.update_column(:submitter_id, invalid_submitter)
      end

      it "updates the ticket" do
        described_class.new(account_id: account.id, submitter_id: submitter_id, dry_run: false).perform
        assert_equal ticket.reload.submitter_id, submitter_id
      end

      describe "dry run" do
        it "doesn't update the ticket" do
          described_class.new(account_id: account.id, submitter_id: submitter_id).perform
          assert_equal ticket.reload.submitter_id, invalid_submitter.id
        end
      end
    end

    describe "when ticket submitter belongs to the account" do
      let(:valid_submitter) { users(:minimum_end_user2) }

      before do
        ticket.update_column(:submitter_id, valid_submitter)
      end

      it "doesn't update the ticket" do
        backfill = described_class.new(account_id: account.id, submitter_id: submitter_id, dry_run: false)
        backfill.send(:process, Ticket.where(id: ticket.id), submitter)

        assert_equal ticket.reload.submitter_id, valid_submitter.id
      end
    end
  end
end
