require_relative "../support/test_helper"

SingleCov.covered!

describe SoftDeleteDuplicateTalkPartnerEditionAccounts do
  let(:account) { accounts(:minimum) }
  let(:second_account) { accounts(:multiproduct) }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
  end

  describe 'when no all accounts have duplicates' do
    it 'runs only on accounts with duplicate' do
      vp = FactoryBot.create(:voice_partner_edition_account,
        account_id: second_account.id,
        plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:legacy],
        trial_expires_at: nil,
        deleted_at: nil)

      vp2 = FactoryBot.create(:voice_partner_edition_account,
        account_id: account.id,
        plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:legacy],
        trial_expires_at: nil,
        deleted_at: nil)

      vp3 = FactoryBot.create(:voice_partner_edition_account,
        account_id: account.id,
        plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:legacy],
        trial_expires_at: nil,
        deleted_at: nil)

      SoftDeleteDuplicateTalkPartnerEditionAccounts.any_instance.expects(:soft_delete_duplicate_voice_partner_edition_account).with(vp).never
      SoftDeleteDuplicateTalkPartnerEditionAccounts.any_instance.expects(:soft_delete_duplicate_voice_partner_edition_account).with(vp2)
      SoftDeleteDuplicateTalkPartnerEditionAccounts.any_instance.expects(:soft_delete_duplicate_voice_partner_edition_account).with(vp3)
      SoftDeleteDuplicateTalkPartnerEditionAccounts.perform(dry_run: false)
    end
  end

  describe 'when there are duplicates' do
    before do
      3.times do
        FactoryBot.create(:voice_partner_edition_account,
          account_id: account.id,
          plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:legacy],
          trial_expires_at: nil,
          deleted_at: nil)
      end
    end

    it 'soft deletes duplicates' do
      SoftDeleteDuplicateTalkPartnerEditionAccounts.perform(dry_run: false)

      assert_equal 1, Voice::PartnerEditionAccount.where(account_id: account.id).count
      assert_nil Voice::PartnerEditionAccount.
        where(account_id: account.id).
        where(active: true).
        order('created_at desc').last.deleted_at
    end
  end

  describe 'when there are no duplicates' do
    before do
      FactoryBot.create(:voice_partner_edition_account,
        account_id: account.id,
        plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:regular],
        trial_expires_at: 2.days.ago,
        deleted_at: 2.days.ago)

      FactoryBot.create(:voice_partner_edition_account,
        account_id: account.id,
        plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:regular],
        trial_expires_at: 30.days.from_now,
        deleted_at: nil)
    end

    it 'does not delete the active one' do
      SoftDeleteDuplicateTalkPartnerEditionAccounts.perform(dry_run: false)

      assert_equal 1, Voice::PartnerEditionAccount.where(account_id: account.id).count
    end
  end

  describe 'when there are no active accounts' do
    it 'does not call soft delete' do
      vp = FactoryBot.create(:voice_partner_edition_account,
        account_id: account.id,
        plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:regular],
        trial_expires_at: 2.days.ago,
        deleted_at: 2.days.ago,
        active: false)

      SoftDeleteDuplicateTalkPartnerEditionAccounts.perform(dry_run: false)

      vp.expects(:soft_delete!).never
    end
  end
end
