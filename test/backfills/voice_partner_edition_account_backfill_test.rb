require_relative "../support/test_helper"

SingleCov.covered!

describe VoicePartnerEditionAccountBackfill do
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:path) { '/path/to/file'  }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
    File.expects(:open).with(path, 'r').yields("#{account.id}")
  end

  it 'creates new active partner edition voice account' do
    VoicePartnerEditionAccountBackfill.perform(path: path)

    assert_equal account.voice_partner_edition_account.active, true
    assert_equal account.voice_partner_edition_account.plan_type, Voice::PartnerEditionAccount::PLAN_TYPE[:legacy]
  end

  it 'updates existing account to active account' do
    account.create_voice_partner_edition_account(active: false)

    VoicePartnerEditionAccountBackfill.perform(path: path)

    account.reload
    assert_equal account.voice_partner_edition_account.active, true
  end
end
