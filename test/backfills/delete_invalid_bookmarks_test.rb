require_relative "../support/test_helper"

SingleCov.covered!

describe DeleteInvalidBookmarks do
  fixtures :accounts, :users, :tickets

  let(:agent) { users(:minimum_agent) }
  let(:deleted_archived_ticket) { tickets(:minimum_1) }
  let(:normal_ticket) { tickets(:minimum_2) }

  before do
    @valid_bookmark = Ticket::Bookmark.create!(:ticket => normal_ticket, :user => agent, :account => normal_ticket.account)

    @invalid_bookmark = Ticket::Bookmark.create!(:ticket => deleted_archived_ticket, :user => agent, :account => deleted_archived_ticket.account)
    archive_and_delete(deleted_archived_ticket)
    archived_ticket = Ticket.find(deleted_archived_ticket.id)
    archived_ticket.will_be_saved_by(agent)
    archived_ticket.soft_delete!
  end

  describe 'when run in dry mode' do
    before do
      DeleteInvalidBookmarks.perform()
    end

    it 'makes no changes' do
      vb = Ticket::Bookmark.without_arsi.find_by_id(@valid_bookmark.id)
      assert vb, "Invalid Bookmark #{vb.inspect} not found"
    end
  end

  describe 'when run in production mode' do
    before do
      DeleteInvalidBookmarks.perform(:dry_run => false)
    end
    it 'does not delete bookmarks with valid tickets' do
      assert Ticket::Bookmark.find_by_id(@valid_bookmark.id)
    end

    it 'deletes invalid bookmark if associated ticket is nil' do
      refute Ticket::Bookmark.find_by_id(@invalid_bookmark.id)
    end
  end
end
