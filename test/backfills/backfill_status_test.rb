require_relative "../support/test_helper"

SingleCov.covered!

describe BackfillStatus do
  let(:status) { BackfillStatus.new("some_backfill", Zendesk::RedisStore.client) }

  describe "#shard_processed?" do
    it "should return false when a shard has not been processed" do
      status.mark_shard_as_processed(1)
      assert_equal status.shard_processed?(1), true
      assert_equal status.shard_processed?(2), false
    end
  end

  describe "#skipped_shards" do
    it "should return the shard when has not been processed" do
      status.mark_shard_as_skipped(1)

      assert(status.skipped_shards.include?("1"))
    end
  end

  describe "#skipped_accounts" do
    it "should return the account when has not been processed" do
      status.mark_account_as_skipped(42)

      assert status.skipped_accounts.include?("42")
    end
  end
end
