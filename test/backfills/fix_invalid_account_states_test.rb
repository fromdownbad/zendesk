require_relative '../support/test_helper'

SingleCov.covered!

describe FixInvalidAccountStates do
  fixtures :accounts

  subject { FixInvalidAccountStates.new dry_run: false }
  let(:account) { accounts(:minimum) }

  def soft_delete_account(account)
    account.is_active = false
    account.is_serviceable = false
    account.save!
    account.soft_delete!
  end

  describe 'perform' do
    before { Arturo::Feature.create!(symbol: :prevent_deletion_if_churned) }
    it 'covers the whole file' do
      subject.perform
    end
  end

  describe 'soft deleted account in prevent_deletion_if_churned' do
    before do
      soft_delete_account(account)
      Arturo::Feature.create!(symbol: :prevent_deletion_if_churned, external_beta_subdomains: account.subdomain)
    end

    it 'undoes soft deletion but leaves account inactive' do
      subject.undo_soft_delete_for_prevent_deletion_if_churned
      account.reload
      account.deleted_at.must_be_nil
      account.is_active.must_equal false
    end
  end

  describe 'active but soft deleted account' do
    before do
      account.update_attribute(:deleted_at, Time.now)
    end

    it 'drops the active flag' do
      account.is_active.must_equal true
      subject.drop_active_on_deleted_accounts
      account.reload
      account.is_active.must_equal false
    end
  end

  describe 'inactive unbilled account but serviceable' do
    before do
      account.update_attribute(:is_active, false)
    end

    it 'drops the serviceable flag' do
      subject.drop_serviceable_on_inactive_unbilled_accounts
      account.reload
      account.is_serviceable.must_equal false
    end
  end

  describe 'active sandbox with deleted master' do
    fixtures :role_settings
    let!(:sandbox) { account.reset_sandbox }

    before do
      soft_delete_account(account)
    end

    it 'soft deletes the sandbox' do
      sandbox.deleted_at.must_be_nil
      subject.soft_delete_active_sandboxes_with_deleted_masters
      account.reload
      sandbox.reload
      sandbox.deleted_at.wont_be_nil
      sandbox.deleted_at.must_equal account.deleted_at
    end
  end

  describe 'soft deleted without an audit' do
    let(:deleted_account) { accounts(:support) }
    before do
      # account.soft_delete! normally creates the audit via callback
      account.update_attribute(:deleted_at, Time.now)
      deleted_account.cancel!
      deleted_account.soft_delete!
    end

    it 'creates the missing DataDeletionAudit record' do
      account.data_deletion_audits.count.must_equal 0
      deleted_account.data_deletion_audits.count.must_equal 1
      subject.create_missing_deletion_audits
      account.data_deletion_audits.count.must_equal 1
      deleted_account.data_deletion_audits.count.must_equal 1
    end
  end
end
