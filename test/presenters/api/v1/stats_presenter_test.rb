require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'StatsPresenter' do
  it "returns zero if no records are selected in a summation" do
    presenter = Api::V1::StatsPresenter.new('touches', StatRollup::Ticket.where('false'))
    assert_equal 0, presenter.to_json
  end
end
