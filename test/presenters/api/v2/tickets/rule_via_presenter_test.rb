require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Tickets::RuleViaPresenter do
  include TestSupport::Rule::Helper

  fixtures :all

  let(:account)   { accounts(:minimum) }
  let(:active)    { true }
  let(:user)      { users(:minimum_admin_not_owner) }
  let(:rule)      { create_trigger(active: active) }
  let(:presenter) { Api::V2::Tickets::RuleViaPresenter.new }

  let(:rule_reference) do
    Zendesk::Rules::RuleViaReference.new(rule: rule, user: user)
  end

  describe '#model_json' do
    it 'presents the `to` key' do
      assert_equal(
        {},
        presenter.model_json(rule_reference)[:to]
      )
    end

    it 'presents the rule id' do
      assert_equal(
        rule.id,
        presenter.model_json(rule_reference)[:from][:id]
      )
    end

    it 'presents the rule title' do
      assert_equal(
        rule_reference.title,
        presenter.model_json(rule_reference)[:from][:title]
      )
    end

    it 'presents whether the rule is deleted' do
      assert_equal(
        rule_reference.deleted?,
        presenter.model_json(rule_reference)[:from][:deleted]
      )
    end

    it 'presents the rule type as `rel`' do
      assert_equal(
        rule_reference.rule_type,
        presenter.model_json(rule_reference)[:rel]
      )
    end

    describe 'and the rule is deleted' do
      let(:rule) { nil }

      it 'presents the proper title' do
        assert_equal(
          'Unknown rule (deleted)',
          presenter.model_json(rule_reference)[:from][:title]
        )
      end
    end
  end

  describe 'when the rule is not viewable' do
    let(:user)   { users(:minimum_agent) }
    let(:active) { false }

    it 'does not present the rule ID' do
      refute presenter.model_json(rule_reference)[:from].key?(:id)
    end
  end
end
