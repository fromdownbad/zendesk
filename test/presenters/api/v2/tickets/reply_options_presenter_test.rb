require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::ReplyOptionsPresenter do
  fixtures :tickets, :users

  let(:ticket) { tickets(:minimum_1) }
  let(:presenter) do
    Api::V2::Tickets::ReplyOptionsPresenter.new(users(:minimum_end_user), url_builder: stub('url_builder'))
  end

  describe '#model_json' do
    it 'presents the event properly' do
      result = presenter.model_json(ticket)
      assert !result.empty?, 'Result does not have multiple items'
      result.each do |item|
        [:text, :description, :via_id, :public, :available, :supports_rich_text, :option_id].each do |key|
          assert item.key?(key), "Item does not have key #{key}"
        end
      end
    end
  end
end
