require_relative '../../../../support/test_helper'
require_relative '../../../../support/ticket_metric_helper'
require_relative '../../../../support/ticket_forms_helper'
require_relative '../../../../support/arturo_test_helper'
require_relative '../../../../support/collaboration_test_helper'
require_relative '../../../../support/collaboration_settings_test_helper'

SingleCov.covered! uncovered: 9 # the custom_fields_cache is tested via a `no_n_plus_ones` test, not each method

describe Api::V2::Tickets::TicketPresenter do
  include TicketMetricHelper
  include TicketFormsHelper
  include CollaborationTestHelper

  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint
  extend ArturoTestHelper

  fixtures :all

  let(:model_ticket) { tickets(:minimum_2) }

  def self.should_present_url
    it "presents the url using api_v2_ticket_url using nice_id" do
      expected_url = "stub_api_v2_organization_url/#{@model.nice_id}.json"
      @presenter.url_builder.expects(:api_v2_ticket_url).with(@model.nice_id, has_entry(format: :json)).returns(expected_url)
      assert_equal expected_url, @presenter.model_json(@model)[:url]
    end
  end

  before do
    @account = accounts(:minimum)
    @model = model_ticket
    ticket_metric_sets(:minimum_set_2)
    @presenter = Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_pass_lint_tests

  should_present_keys :allow_attachments,
    :allow_channelback,
    :assignee_id,
    :brand_id,
    :collaborator_ids,
    :created_at,
    :custom_fields,
    :description,
    :due_at,
    :email_cc_ids,
    :external_id,
    :fields,
    :follower_ids,
    :followup_ids,
    :forum_topic_id,
    :group_id,
    :has_incidents,
    :id,
    :is_public,
    :organization_id,
    :priority,
    :problem_id,
    :raw_subject,
    :recipient,
    :requester_id,
    :satisfaction_rating,
    :sharing_agreement_ids,
    :status,
    :subject,
    :submitter_id,
    :tags,
    :type,
    :updated_at,
    :url,
    :via

  should_present_method :nice_id, as: :id
  should_present_method :external_id
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :subject
  should_present_method :requester_id
  should_present_method :submitter_id
  should_present_method :assignee_id
  should_present_method :organization_id
  should_present_method :group_id
  should_present_method :entry_id, as: :forum_topic_id
  should_present_method :due_date, as: :due_at

  should_present_url

  describe "via type" do
    let(:apresenter) do
      Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder, expanded_via_types: voyager)
    end

    describe "when not requested by voyager" do
      let(:voyager) { false }

      it "presents via" do
        via = mock
        apresenter.expects(:serialized_via_object).with(@model).returns(via)
        assert_equal via, apresenter.model_json(@model)[:via]
      end
    end

    describe "from voyager api client" do
      let(:voyager) { true }

      it "returns the raw via id too" do
        via = mock
        apresenter.expects(:serialized_via_object).with(@model).returns(via)
        via.expects(:[]=).with(:id, @model.via_id)
        assert_equal via, apresenter.model_json(@model)[:via]
      end
    end
  end

  describe 'with option use_raw_ticket_field_entries' do
    let(:apresenter) do
      Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder, use_raw_ticket_field_entries: true)
    end

    it 'works' do
      result = apresenter.present(@account.tickets)[:tickets]
      nice_id = @account.tickets.joins(:ticket_field_entries).first!.nice_id

      json = result.find { |j| j[:id] == nice_id }
      refute_nil json[:custom_fields][0][:id]
    end

    describe 'abc' do
      before do
        @nice_id = @account.tickets.joins(:ticket_field_entries).first!.nice_id
        @account.tickets.map { |t| archive_and_delete(t) }
      end

      it 'works' do
        result = apresenter.present(@account.tickets.all_with_archived)[:tickets]
        refute result.empty?

        json = result.find { |j| j[:id] == @nice_id }
        refute_nil json[:custom_fields][0][:id]
      end
    end

    it 'works with deleted tickets' do
      ticket = @account.tickets.joins(:ticket_field_entries).first!
      nice_id = ticket.nice_id
      ticket.will_be_saved_by(@account.owner)
      ticket.soft_delete!

      tickets = Ticket.with_deleted { @account.tickets.where("status_id != #{StatusType.ARCHIVED}").to_a }
      tickets.map(&:nice_id).must_include nice_id
      result = apresenter.present(tickets)[:tickets]

      json = result.find { |j| j[:id] == nice_id }
      refute_nil json[:custom_fields][0][:id]
    end
  end

  describe "current user is an agent" do
    before do
      @presenter.user.stubs(:is_agent?).returns(true)
    end

    it "renders description" do
      @model.stubs(:description).returns("{{ticket.account}}")
      assert_equal "Minimum account", @presenter.model_json(@model)[:description]
    end

    it "renders the value for subject and the placeholder for raw subject " do
      @model.stubs(:subject).returns("{{ticket.account}}")
      assert_equal "Minimum account", @presenter.model_json(@model)[:subject]
      assert_equal "{{ticket.account}}", @presenter.model_json(@model)[:raw_subject]
    end
  end

  describe "current user is not an agent" do
    before do
      @placeholder = "{{ticket.account}}"

      @presenter.user.stubs(:is_agent?).returns(false)
      @model.stubs(:description).returns(@placeholder)
      @model.stubs(:subject).returns("{{ticket.account}}")
    end

    it "doesn't render placeholders if the submitter was an end user" do
      @model.stubs(:submitter).returns(users(:minimum_end_user))

      assert_equal @placeholder, @presenter.model_json(@model)[:description]
      assert_equal @placeholder, @presenter.model_json(@model)[:raw_subject]
      assert_equal @placeholder, @presenter.model_json(@model)[:subject]
    end

    it "doesn't render placeholder if submitted by an agent on behalf of an end user" do
      @model.stubs(:submitter).returns(users(:minimum_agent))

      assert_equal @placeholder, @presenter.model_json(@model)[:description]
      assert_equal @placeholder, @presenter.model_json(@model)[:raw_subject]
      assert_equal @placeholder, @presenter.model_json(@model)[:subject]
    end
  end

  describe 'with unfetched stub_only archived tickets' do
    before do
      archive_and_delete(@model)
      @model = Ticket.find(@model.id, stub_only: true)
    end

    it 'eagerly preloads the tickets' do
      Rails.logger.expects(:append_attributes).with(
        ticket_presenter_preload_archived_tickets: true
      )
      @presenter.present(@model)
    end
  end

  describe "#association_preloads" do
    let(:collaboration_preload) { {} }

    before do
      @preloads = {
        ticket_field_entries: {},
        collaborations:       collaboration_preload,
        problem:              {},
        shared_tickets:       {},
        requester:            {},
        event_decorations:    {},
        assignee:             {},
        organization:         {}
      }
    end

    describe_with_arturo_disabled :email_ccs do
      it "includes preloads" do
        assert_equal @preloads, @presenter.association_preloads
      end
    end

    describe_with_arturo_enabled :email_ccs do
      let(:collaboration_preload) { {user: {}} }

      it "includes collaboration user association in the preloads" do
        assert_equal @preloads, @presenter.association_preloads
      end
    end

    describe_with_and_without_arturo_enabled :email_ccs do
      before do
        if @account.has_email_ccs?
          collaboration_preload.merge!(user: {})
        end
      end

      it "preloads required associations" do
        assert_equal @preloads, @presenter.association_preloads
      end

      describe_with_arturo_setting_enabled :customer_satisfaction do
        it "preloads satisfaction" do
          satisfaction_preloads = @preloads.merge(
            satisfaction_rating:          {},
            satisfaction_rating_scores:   {},
            satisfaction_rating_comments: {}
          )

          assert_equal satisfaction_preloads, @presenter.association_preloads
        end

        describe_with_arturo_enabled :ticket_presenter_no_satisfaction_score_preloads do
          it "preloads without satisfaction_rating_scores" do
            satisfaction_preloads = @preloads.merge(
              satisfaction_rating:          {},
              satisfaction_rating_comments: {}
            )

            assert_equal satisfaction_preloads, @presenter.association_preloads
          end
        end

        describe_with_arturo_enabled :satisfaction_rating_disable_comment_fallback do
          it "preloads with satisfaction_rating comment and event, but without satisfaction_rating_scores or satisfaction_rating_comments" do
            satisfaction_preloads = @preloads.merge(
              satisfaction_rating: {comment: {}, event: {}}
            )

            assert_equal satisfaction_preloads, @presenter.association_preloads
          end
        end

        describe_with_arturo_enabled :ticket_presenter_no_satisfaction_preloads do
          it "preloads without satisfaction_rating_scores or satisfaction_rating_comments" do
            satisfaction_preloads = @preloads.merge(
              satisfaction_rating: {}
            )

            assert_equal satisfaction_preloads, @presenter.association_preloads
          end
        end
      end

      it "preloads metrics if sideloaded" do
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:metric_sets).returns(true)
        assert_equal @preloads.merge(ticket_metric_set: @presenter.metric_set_presenter.association_preloads), @presenter.association_preloads
      end

      describe "when audits are sideloaded" do
        before do
          @presenter.stubs(:side_load?).returns(false)
          @presenter.stubs(:side_load?).with(:last_audits).returns(true)
        end

        it "preloads audit associations" do
          assert_equal @preloads.merge(audits: @presenter.audit_presenter.association_preloads), @presenter.association_preloads
        end

        describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
          it "includes the notifications_with_ccs association" do
            assert @presenter.association_preloads[:audits].key?(:notifications_with_ccs)
          end
        end

        describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
          it "does not include the notifications_with_ccs association" do
            refute @presenter.association_preloads[:audits].key?(:notifications_with_ccs)
          end
        end
      end

      it "preloads ticket_forms if sideloaded" do
        @presenter.account.stubs(:has_ticket_forms?).returns(true)
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:ticket_forms).returns(true)
        assert_equal @preloads.merge(ticket_form: @presenter.ticket_form_presenter.association_preloads), @presenter.association_preloads
      end

      it "preloads with organization" do
        @preloads[:organization] = {}
        assert_equal @preloads, @presenter.association_preloads
      end
    end
  end

  it "renders description in the correct timezone" do
    @model.requester.stubs(:is_agent?).returns(true)
    @model.created_at = Time.now # avoid bugs when year changes

    @presenter.user.stubs(:time_zone).returns("Ulaan Bataar")
    @model.stubs(:description).returns("Foo {{ticket.created_at_with_time}} bar")
    json = @presenter.model_json(@model)

    # confirm that account's time zone is different from stubbed user
    assert_equal "Copenhagen", @account.time_zone

    expected = "Foo #{@model.created_at.to_format(with_time: true, time_format: @account.time_format_string, time_zone: "Ulaan Bataar")} bar"

    assert_equal expected, json[:description]
  end

  describe "#ticket_forms" do
    before do
      setup_ticket_forms
      @ticket_form = @non_active_form
      @model.update_column(:ticket_form_id, @non_active_form.id)
    end

    describe 'if account has ticket_forms enabled' do
      before do
        Account.any_instance.stubs(:has_ticket_forms?).returns(true)
      end

      it 'presents ticket_form_id' do
        assert_equal @ticket_form.id, @presenter.model_json(@model)[:ticket_form_id]
      end

      it 'presents deleted_ticket_form_id' do
        assert_equal @ticket_form.id, @presenter.model_json(@model)[:deleted_ticket_form_id]
      end

      it 'presents deleted_ticket_form_id if not nil and were previously loaded' do
        assert_equal @ticket_form.id, @presenter.present(@model)[:ticket][:deleted_ticket_form_id]
      end

      it 'does not present deleted_ticket_form_id if nil' do
        @model.stubs(:deleted_ticket_form).returns(nil)
        refute @presenter.model_json(@model).key?('deleted_ticket_form_id')
      end

      it 'sideloads deleted_ticket_forms' do
        json = @presenter.present(@model)[:deleted_ticket_forms] || []
        assert_equal [@ticket_form.id], json.map { |e| e[:id] }
      end

      it 'sideloads deleted_ticket_forms and does not do a sql request with duplicates' do
        assert_no_n_plus_one do
          @presenter.present([@model, @model])
        end
      end
    end

    describe 'if account does not have ticket forms enabled' do
      before do
        Account.any_instance.expects(:has_ticket_forms?).returns(false)
      end

      it 'does not present ticket_form_id' do
        refute @presenter.model_json(@model).key?('ticket_form_id')
      end

      it 'does not present deleted_ticket_form_id' do
        refute @presenter.model_json(@model).key?('deleted_ticket_form_id')
      end
    end
  end

  describe "#custom_status_id" do
    describe_with_arturo_enabled :custom_statuses do
      before { @model.stubs(:custom_status_id).returns(5) }

      it "presents custom_status_id" do
        assert_equal 5, @presenter.model_json(@model)[:custom_status_id]
      end
    end

    describe_with_arturo_disabled :custom_statuses do
      it "does not present custom_status_id" do
        refute @presenter.model_json(@model).key?(:custom_status_id)
      end
    end
  end

  describe "#brands" do
    before { @model.stubs(:brand_id).returns(5) }

    it "presents brand_id" do
      assert_equal 5, @presenter.model_json(@model)[:brand_id]
    end
  end

  describe "satisfaction ratings" do
    describe_with_arturo_setting_enabled :customer_satisfaction do
      describe "without a rating" do
        before do
          @model.will_be_saved_by(users(:minimum_agent))
          @model.update_attribute(:satisfaction_score, SatisfactionType.UNOFFERED)
          @json = @presenter.model_json(@model)
        end

        it "presents status" do
          assert_equal "unoffered", @json[:satisfaction_rating][:score]
        end
      end

      describe "with a rating" do
        before do
          @model.satisfaction_score = SatisfactionType.GOODWITHCOMMENT
          @model.satisfaction_comment = "Awesome!"
          @model.will_be_saved_by(users(:minimum_agent))
          @model.save!

          @json = @presenter.model_json(@model.reload)
        end

        it "presents score" do
          assert_equal "good", @json[:satisfaction_rating][:score]
        end

        it "presents comment" do
          assert_equal "Awesome!", @json[:satisfaction_rating][:comment]
        end

        it "presents id" do
          assert_equal @model.satisfaction_rating.id, @json[:satisfaction_rating][:id]
        end
      end

      describe "presenting multiple satisfaction ratings" do
        before do
          @ticket_1 = tickets(:minimum_1)
          @ticket_2 = tickets(:minimum_2)

          @tickets = [@ticket_1, @ticket_2]

          @ticket_1.satisfaction_score = SatisfactionType.GOODWITHCOMMENT
          @ticket_1.satisfaction_comment = "Awesome!"
          @ticket_1.will_be_saved_by(users(:minimum_agent))
          @ticket_1.save!

          @ticket_2.satisfaction_score = SatisfactionType.BAD
          @ticket_2.will_be_saved_by(users(:minimum_agent))
          @ticket_2.save!
        end

        describe_with_and_without_arturo_enabled :ticket_presenter_no_satisfaction_score_preloads do
          describe "with a rating with comment" do
            it "does not n+1" do
              assert_no_n_plus_one do
                @presenter.present(@tickets)
              end
            end
          end
        end

        describe_with_and_without_arturo_enabled :satisfaction_rating_disable_comment_fallback do
          describe "with a rating with comment" do
            it "does not n+1" do
              assert_no_n_plus_one do
                @presenter.present(@tickets)
              end
            end

            describe "without comment_event_id" do
              before do
                assert_not_nil @ticket_1.satisfaction_rating.comment_event_id
                @ticket_1.satisfaction_rating.update_column(:comment_event_id, nil)
              end

              it "does not n+1" do
                assert_no_n_plus_one do
                  @presenter.present(@tickets)
                end
              end
            end
          end
        end
      end
    end

    describe_with_arturo_setting_disabled :customer_satisfaction do
      before do
        @json = @presenter.model_json(@model)
      end

      it "is nil" do
        assert_nil @json[:satisfaction_rating]
      end
    end
  end

  describe "macro application" do
    before do
      @model.additional_tags = %w[itworks]
      @json = @presenter.model_json(@model)
    end

    it "presents current tags" do
      assert @json[:tags].is_a?(Array)
      assert @json[:tags].include?("itworks")
    end
  end

  describe "audit count" do
    before do
      @model.audits.create!(account: @account, author: users(:minimum_agent), via_id: 0, created_at: 1.year.ago)
      2.times { @model.audits.create!(account: @account, author: users(:minimum_agent), via_id: 0) }
      @last_viewed = Date.yesterday
    end

    describe "when 'audit_count_since' is provided" do
      before do
        @presenter = Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder, audit_count_since: @last_viewed)
        @json = @presenter.model_json(@model)
      end

      it "presents a count of the audits happened after the date provided by 'audit_count_since'" do
        assert @json[:audit_count_since]
        assert_equal @model.audits.where("created_at > ?", @last_viewed).count(:all), 2
        assert_equal @json[:audit_count_since], @model.audits.where("created_at > ?", @last_viewed).count(:all)
      end
    end

    describe "when 'audit_count_since' is not provided" do
      before do
        @presenter = Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder)
        @json = @presenter.model_json(@model)
      end

      it "does not present a count of the audits happened after the date provided by 'audit_count_since'" do
        assert_nil @json[:audit_count_since]
      end
    end
  end

  it "presents type" do
    @model.ticket_type_id = nil
    assert_nil @presenter.model_json(@model)[:type]

    @model.ticket_type_id = Zendesk::Types::TicketType.QUESTION
    assert_equal 'question', @presenter.model_json(@model)[:type]

    @model.ticket_type_id = Zendesk::Types::TicketType.INCIDENT
    assert_equal 'incident', @presenter.model_json(@model)[:type]

    @model.ticket_type_id = Zendesk::Types::TicketType.PROBLEM
    assert_equal 'problem', @presenter.model_json(@model)[:type]

    @model.ticket_type_id = Zendesk::Types::TicketType.TASK
    assert_equal 'task', @presenter.model_json(@model)[:type]
  end

  it "presents priority" do
    @model.priority_id = nil
    assert_nil @presenter.model_json(@model)[:priority]

    @model.priority_id = Zendesk::Types::PriorityType.LOW
    assert_equal 'low', @presenter.model_json(@model)[:priority]

    @model.priority_id = Zendesk::Types::PriorityType.NORMAL
    assert_equal 'normal', @presenter.model_json(@model)[:priority]

    @model.priority_id = Zendesk::Types::PriorityType.HIGH
    assert_equal 'high', @presenter.model_json(@model)[:priority]

    @model.priority_id = Zendesk::Types::PriorityType.URGENT
    assert_equal 'urgent', @presenter.model_json(@model)[:priority]
  end

  it "presents status" do
    @model.status_id = nil
    assert_nil @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.NEW
    assert_equal 'new', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.OPEN
    assert_equal 'open', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.PENDING
    assert_equal 'pending', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.HOLD
    assert_equal 'hold', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.SOLVED
    assert_equal 'solved', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.CLOSED
    json = @presenter.model_json(@model)
    assert_equal 'closed', json[:status]
    assert_includes json.keys, :followup_ids

    @model.status_id = Zendesk::Types::StatusType.DELETED
    assert_equal 'deleted', @presenter.model_json(@model)[:status]
  end

  describe "ticket fields" do
    before do
      @fields = @presenter.model_json(@model)[:custom_fields]
    end

    it "presents all ticket fields" do
      assert_equal @account.ticket_fields.custom.active.count(:all), @fields.size
    end

    it "presents field entries" do
      assert @model.ticket_field_entries.any?

      @model.ticket_field_entries.each do |entry|
        assert_includes @fields, id: entry.ticket_field_id, value: entry.value
      end
    end

    describe "with custom fields" do
      before do
        @ticket_field = ticket_fields(:field_tagger_custom)
        @custom_field_option = custom_field_options(:field_tagger_custom_option_1)

        @model.ticket_field_entries.build(
          ticket_field: @ticket_field,
          value:        @custom_field_option.value
        )
      end

      it "presents option value" do
        @fields = @presenter.model_json(@model)[:custom_fields]
        assert_equal @custom_field_option.value, @fields.detect { |field| field[:id] == @ticket_field.id }[:value]
      end

      describe "in rendered description" do
        it "does not N+1 ticket field lookups" do
          model_json = {}
          input_subject = "{{ticket.ticket_field_#{@ticket_field.id}}}, {{ticket.ticket_field_#{@ticket_field.id}}}, {{ticket.ticket_field_#{@ticket_field.id}}}"

          assert_no_n_plus_one do
            @model.stubs(:subject).returns(input_subject)
            model_json = @presenter.model_json(@model)
          end

          assert_equal "booring, booring, booring", model_json[:subject]
          assert_equal input_subject, model_json[:raw_subject]
        end
      end

      describe "exporting fields" do
        before do
          @ticket_field.is_exportable = false
          @ticket_field.save!

          @presenter = Api::V2::Tickets::TicketPresenter.new(@account.owner, exclude_non_exportable_fields: true, url_builder: mock_url_builder)
          @fields = @presenter.model_json(@model)[:custom_fields]
        end

        it "excludes fields not allowed for export" do
          refute_includes @fields, @ticket_field.id
        end
      end
    end

    describe "with custom multiselect fields" do
      let(:presenter) do
        Api::V2::Tickets::TicketPresenter.new(@account.owner, exclude_non_exportable_fields: true, url_builder: mock_url_builder)
      end

      before do
        @ticket_field = FactoryBot.create(:field_multiselect, account: @account)
        @model.ticket_field_entries.build(
          ticket_field: @ticket_field,
          value:        "red green"
        )
      end

      it "presents option value as array" do
        @fields = presenter.model_json(@model)[:custom_fields]
        assert_equal ["red", "green"], @fields.detect { |field| field[:id] == @ticket_field.id }[:value]
      end
    end

    describe 'with voice insights custom fields' do
      describe 'with include_voice_insights_fields true' do
        it 'includes the voice_insights fields' do
          Account.any_instance.expects(:ticket_fields).with(include_voice_insights: true).returns(TicketField.unscoped)
          Api::V2::Tickets::TicketPresenter.new(@account.owner, include_voice_insights_fields: true, url_builder: mock_url_builder)
        end
      end
      describe 'with include_voice_insights_fields not passed' do
        it 'excludes the voice_insights fields' do
          Account.any_instance.expects(:ticket_fields).with(include_voice_insights: nil).returns(TicketField.unscoped)
          Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder)
        end
      end
    end

    describe "with boolean fields" do
      before do
        @ticket_field = ticket_fields(:field_checkbox_custom)
        @model.ticket_field_entries.build(ticket_field: @ticket_field, value: "1")
        @fields = @presenter.model_json(@model)[:custom_fields]
      end

      it "presents true/false" do
        assert(@fields.detect { |field| field[:id] == @ticket_field.id }[:value])
      end
    end
  end

  describe 'followup_ids' do
    before do
      @ticket = tickets(:minimum_1)
      @followup = tickets(:minimum_2)
      @ticket.followups << @followup
    end

    describe 'for non-closed tickets' do
      it 'does not present ids' do
        fields = @presenter.present(@ticket)[:ticket][:followup_ids]

        assert_equal @ticket.closed?, false
        assert_equal fields, []
      end
    end

    describe 'for closed tickets' do
      before do
        @ticket.update_column :status_id, StatusType.CLOSED
        assert @ticket.closed?
      end

      it 'presents ids' do
        fields = @presenter.present(@ticket)[:ticket][:followup_ids]
        assert_not_nil fields

        assert_equal [@followup.nice_id], fields
      end

      it 'presents ids for archived tickets' do
        archive_and_delete(@ticket)

        fields = @presenter.present(@ticket)[:ticket][:followup_ids]
        assert_not_nil fields

        assert_equal [@followup.nice_id], fields
      end

      it 'does not present ids for archived tickets when the followup has been deleted' do
        archive_and_delete(@ticket)
        @followup.delete

        fields = @presenter.present(@ticket)[:ticket][:followup_ids]
        assert_empty fields
      end
    end
  end

  describe "satisfaction_probability" do
    describe "when the feature is available" do
      before do
        Account.any_instance.stubs(has_satisfaction_prediction?: true)
        @model.create_ticket_prediction(satisfaction_probability: 0.91)

        @presenter.preload_associations(@model)
      end

      it "adds satisfaction_probability" do
        assert_equal 0.91, @presenter.model_json(@model)[:satisfaction_probability]
      end
    end

    describe "when the feature is not available" do
      before do
        Arturo.disable_feature!(:satisfaction_prediction)
      end

      it "doesn't add satisfaction_condfidence when the feature is unavailable" do
        refute @presenter.model_json(@model).key?(:satisfaction_probability)
      end
    end
  end

  describe 'comment_count side-load' do
    let(:presenter) { Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder, includes: [:comment_count]) }

    describe 'with archived tickets' do
      before do
        archive_and_delete(@model)
        @model = Ticket.find(@model.id)
      end

      it 'does fetch count from SQL' do
        assert_sql_queries(0, /COUNT\(\*\)/) do
          presenter.present(@model).dig(:ticket, :comment_count).must_equal 1
        end
      end
    end

    describe 'with unarchived tickets' do
      it 'does not n+1 by using COUNT .. GROUP BY query' do
        assert_no_n_plus_one do
          presenter.present(@model).dig(:ticket, :comment_count).must_equal 1
        end
      end

      describe 'when preload_associations is not called' do
        it 'runs an n+1 and logs' do
          # Note, `assert_n_plus_ones` fails here due
          # assert_sql_queries /COUNT\(\*\) FROM `events`.*`ticket_id` =/, 1 do
          assert_n_plus_ones(1) do
            # model_json skips the preload call
            2.times do
              presenter.model_json(@model).fetch(:comment_count).must_equal 1
            end
          end
        end
      end
    end
  end

  describe "has_incidents" do
    let(:presenter) { Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder, includes: [:comment_count]) }

    before do
      @tickets = [tickets(:minimum_1), tickets(:minimum_2)]
      @tickets.each do |t|
        t.ticket_type_id = TicketType.Problem
      end
      presenter.preload_associations(@tickets)
    end

    it 'does not n+1' do
      assert_no_n_plus_one do
        presenter.present(@tickets)
      end
    end
  end

  it "presents recipient" do
    @model.original_recipient_address = @model.recipient = nil
    assert_nil @presenter.model_json(@model)[:recipient]

    @model.original_recipient_address = 'foo@test.com'
    @model.recipient = nil
    assert_equal "foo@test.com", @presenter.model_json(@model)[:recipient]

    @model.original_recipient_address = nil
    @model.recipient = "bar"
    assert_equal "bar", @presenter.model_json(@model)[:recipient]

    @model.original_recipient_address = "abc@test.com"
    @model.recipient = "bar"
    assert_equal "abc@test.com", @presenter.model_json(@model)[:recipient]
  end

  describe "collaborations" do
    before do
      @end_user_1 = users(:minimum_end_user)
      @end_user_2 = users(:minimum_end_user2)
      @agent_1 = users(:minimum_agent)
      @agent_2 = FactoryBot.create(:agent, account: @model.account, name: "Agent 2")

      build_collaborations_with_collaborator_type(@model, [@end_user_1, @agent_1], CollaboratorType.LEGACY_CC)
      build_collaborations_with_collaborator_type(@model, [@end_user_2, @agent_2], CollaboratorType.EMAIL_CC)
      build_collaborations_with_collaborator_type(@model, [@agent_2], CollaboratorType.FOLLOWER)
    end

    describe "follower_and_email_cc_collaborations setting not enabled" do
      let(:expected_collaborator_ids) { [@end_user_1.id, @end_user_2.id, @agent_1.id, @agent_2.id] }
      let(:expected_email_cc_ids) { [] }

      [
        CollaborationSettingsTestHelper.new(
          feature_arturo: false,
          feature_setting: false,
          followers_setting: false,
          email_ccs_setting: false
        ),
        CollaborationSettingsTestHelper.new(
          feature_arturo: true,
          feature_setting: false,
          followers_setting: false,
          email_ccs_setting: false
        )
      ].each do |scenario|
        before { scenario.apply }

        describe "collaborators" do
          it "presents collaborator_ids" do
            assert_equal expected_collaborator_ids.sort, @presenter.model_json(@model)[:collaborator_ids].sort
          end

          it "deduplicates collaborator_ids" do
            @model.ccs << @agent_1
            assert_equal expected_collaborator_ids.sort, @presenter.model_json(@model)[:collaborator_ids].sort
          end

          describe "of an archived ticket" do
            before do
              archive_and_delete(@model)
              @model = Ticket.find(@model.id)
              @presenter.preload_associations(@model)
            end

            it "uses collaborators_from_archived_ticket" do
              Ticket.any_instance.expects(:collaborations).never
              @presenter.model_json(@model)
            end
          end
        end

        describe "followers" do
          it "presents follower_ids" do
            assert_equal expected_collaborator_ids.sort, @presenter.model_json(@model)[:follower_ids].sort
          end

          it "deduplicates follower_ids" do
            @model.followers << @agent_1
            presented_follower_ids = @presenter.model_json(@model)[:follower_ids].sort
            assert_equal presented_follower_ids.uniq, presented_follower_ids
          end
        end

        describe "email_ccs" do
          it "presents empty email_ccs_ids" do
            assert_equal expected_email_cc_ids, @presenter.model_json(@model)[:email_cc_ids].sort
          end
        end
      end
    end

    describe "follower_and_email_cc_collaborations setting enabled" do
      describe "ticket_followers_allowed and comment_email_ccs_allowed enabled" do
        let(:expected_collaborator_ids) { [@end_user_1.id, @end_user_2.id, @agent_1.id, @agent_2.id] }
        let(:expected_follower_ids) { [@agent_1.id, @agent_2.id] }
        let(:expected_email_cc_ids) { [@end_user_1.id, @end_user_2.id, @agent_2.id] }

        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: true,
            email_ccs_setting: true
          ).apply
        end

        describe "collaborators" do
          it "presents collaborator_ids" do
            assert_equal expected_collaborator_ids.sort, @presenter.model_json(@model)[:collaborator_ids].sort
          end

          it "uses follower_ids and email_cc_ids instead of Ticket#collaborators" do
            Ticket.any_instance.expects(:collaborators).never
            @presenter.model_json(@model)
          end

          describe "of an archived ticket" do
            before do
              archive_and_delete(@model)
              @model = Ticket.find(@model.id)
              @presenter.preload_associations(@model)
            end

            it "uses collaborators_from_archived_ticket" do
              @presenter.expects(:collaborators_from_archived_ticket)
              @presenter.model_json(@model)
            end
          end
        end

        describe "followers" do
          it "presents follower_ids" do
            assert_equal expected_follower_ids.sort, @presenter.model_json(@model)[:follower_ids].sort
          end

          describe "of an archived ticket" do
            before do
              archive_and_delete(@model)
              @model = Ticket.find(@model.id)
              @presenter.preload_associations(@model)
            end

            it "uses followers_from_archived_ticket" do
              @presenter.expects(:followers_from_archived_ticket)
              @presenter.model_json(@model)[:follower_ids]
            end

            it "includes IDs for users in collaborations with the FOLLOWER collaborator_type" do
              presented_follower_ids = @presenter.model_json(@model)[:follower_ids].sort
              presented_follower_ids.must_include @agent_2.id
            end

            it "does not include IDs for agent users in collaborations with the LEGACY_CC collaborator_type" do
              presented_follower_ids = @presenter.model_json(@model)[:follower_ids].sort
              presented_follower_ids.wont_include @agent_1.id
            end
          end
        end

        describe "email_ccs" do
          it "presents email_cc_ids" do
            assert_equal expected_email_cc_ids.sort, @presenter.model_json(@model)[:email_cc_ids].sort
          end

          describe "of an archived ticket" do
            before do
              archive_and_delete(@model)
              @model = Ticket.find(@model.id)
              @presenter.preload_associations(@model)
            end

            it "uses email_ccs_from_archived_ticket" do
              @presenter.expects(:email_ccs_from_archived_ticket)
              @presenter.model_json(@model)[:email_cc_ids]
            end

            it "includes IDs for users in collaborations with the EMAIL_CC collaborator_type" do
              presented_email_cc_ids = @presenter.model_json(@model)[:email_cc_ids].sort
              presented_email_cc_ids.must_include @end_user_2.id
              presented_email_cc_ids.must_include @agent_2.id
            end

            it "does not include IDs for end users in collaborations with the LEGACY_CC collaborator_type" do
              presented_email_cc_ids = @presenter.model_json(@model)[:email_cc_ids].sort
              presented_email_cc_ids.wont_include @end_user_1.id
            end
          end
        end
      end

      describe "ticket_followers_allowed not enabled; comment_email_ccs_allowed enabled" do
        let(:expected_collaborator_ids) { [@end_user_1.id, @end_user_2.id, @agent_2.id] }
        let(:expected_follower_ids) { [] }
        let(:expected_email_cc_ids) { [@end_user_1.id, @end_user_2.id, @agent_2.id] }

        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: false,
            email_ccs_setting: true
          ).apply
        end

        describe "collaborators" do
          it "presents collaborator_ids" do
            assert_equal expected_collaborator_ids.sort, @presenter.model_json(@model)[:collaborator_ids].sort
          end
        end

        describe "followers" do
          it "presents empty follower_ids" do
            assert_equal [], @presenter.model_json(@model)[:follower_ids]
          end
        end

        describe "email_ccs" do
          it "presents email_cc_ids" do
            assert_equal expected_email_cc_ids.sort, @presenter.model_json(@model)[:email_cc_ids].sort
          end
        end
      end

      describe "ticket_followers_allowed enabled; comment_email_ccs_allowed not enabled" do
        let(:expected_collaborator_ids) { [@agent_1.id, @agent_2.id] }
        let(:expected_follower_ids) { [@agent_1.id, @agent_2.id] }
        let(:expected_email_cc_ids) { [] }

        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: true,
            email_ccs_setting: false
          ).apply
        end

        describe "collaborators" do
          it "presents collaborator_ids" do
            assert_equal expected_collaborator_ids.sort, @presenter.model_json(@model)[:collaborator_ids].sort
          end
        end

        describe "followers" do
          it "presents follower_ids" do
            assert_equal expected_follower_ids.sort, @presenter.model_json(@model)[:follower_ids].sort
          end
        end

        describe "email_ccs" do
          it "presents empty email_cc_ids" do
            assert_equal expected_email_cc_ids, @presenter.model_json(@model)[:email_cc_ids]
          end
        end
      end

      describe "ticket_followers_allowed and comment_email_ccs_allowed not enabled" do
        let(:expected_collaborator_ids) { [] }
        let(:expected_follower_ids) { [] }
        let(:expected_email_cc_ids) { [] }

        before do
          CollaborationSettingsTestHelper.new(
            feature_arturo: true,
            feature_setting: true,
            followers_setting: false,
            email_ccs_setting: false
          ).apply
        end

        describe "collaborators" do
          it "presents empty collaborator_ids" do
            assert_equal expected_collaborator_ids, @presenter.model_json(@model)[:collaborator_ids]
          end
        end

        describe "followers" do
          it "presents empty follower_ids" do
            assert_equal expected_follower_ids, @presenter.model_json(@model)[:follower_ids]
          end
        end

        describe "email_ccs" do
          it "presents empty email_cc_ids" do
            assert_equal expected_email_cc_ids, @presenter.model_json(@model)[:email_cc_ids]
          end
        end
      end
    end

    describe "#collaborators_from_archived_ticket" do
      let(:collaborators_from_archived_tickets) { {} }

      subject { @presenter.send(:collaborators_from_archived_ticket, @model) }

      before do
        @presenter.instance_variable_set(:@collaborators_from_archived_tickets, collaborators_from_archived_tickets)
      end

      describe "when @collaborators_from_archived_tickets is empty" do
        it { subject.must_be_empty }
      end

      describe "when @collaborators_from_archived_tickets is not empty" do
        let(:collaborators_from_archived_tickets) { { @model.id => @model.collaborations.to_a } }

        before do
          # Remove agent_2's EMAIL_CC collaboration so it only has a followers one
          collaborators_from_archived_tickets[@model.id].delete_if do |collaboration|
            collaboration.collaborator_type == CollaboratorType.EMAIL_CC && collaboration.user_id == @agent_2.id
          end
        end

        describe "and it contains EMAIL_CC collaborations" do
          it { subject.must_include @end_user_2.id }
        end

        describe "and it contains LEGACY_CC collaborations" do
          it { subject.must_include @end_user_1.id }
          it { subject.must_include @agent_1.id }
        end

        describe "and it contains FOLLOWER collaborations" do
          it { subject.must_include @agent_2.id }
        end
      end
    end

    describe "#email_ccs_from_archived_ticket" do
      let(:collaborators_from_archived_tickets) { {} }

      subject { @presenter.send(:email_ccs_from_archived_ticket, @model) }

      before do
        @presenter.instance_variable_set(:@collaborators_from_archived_tickets, collaborators_from_archived_tickets)
      end

      describe "when @collaborators_from_archived_tickets is empty" do
        it { subject.must_be_empty }
      end

      describe "when @collaborators_from_archived_tickets is not empty" do
        let(:collaborators_from_archived_tickets) { { @model.id => @model.collaborations.to_a } }

        describe "and it contains EMAIL_CC collaborations" do
          it { subject.must_include @end_user_2.id }
          it { subject.must_include @agent_2.id }
        end

        describe "and it contains LEGACY_CC collaborations" do
          it { subject.wont_include @end_user_1.id }
          it { subject.wont_include @agent_1.id }
        end

        describe "and it contains FOLLOWER collaborations" do
          before do
            # Remove agent_2's EMAIL_CC collaboration so it only has a followers one
            collaborators_from_archived_tickets[@model.id].delete_if do |collaboration|
              collaboration.collaborator_type == CollaboratorType.EMAIL_CC && collaboration.user_id == @agent_2.id
            end
          end

          it { subject.wont_include @agent_2.id }
        end
      end
    end

    describe "#followers_from_archived_ticket" do
      let(:collaborators_from_archived_tickets) { {} }

      subject { @presenter.send(:followers_from_archived_ticket, @model) }

      before do
        @presenter.instance_variable_set(:@collaborators_from_archived_tickets, collaborators_from_archived_tickets)
      end

      describe "when @collaborators_from_archived_tickets is empty" do
        it { subject.must_be_empty }
      end

      describe "when @collaborators_from_archived_tickets is not empty" do
        let(:collaborators_from_archived_tickets) { { @model.id => @model.collaborations.to_a } }

        before do
          # Remove agent_2's EMAIL_CC collaboration so it only has a followers one
          collaborators_from_archived_tickets[@model.id].delete_if do |collaboration|
            collaboration.collaborator_type == CollaboratorType.EMAIL_CC && collaboration.user_id == @agent_2.id
          end
        end

        describe "and it contains EMAIL_CC collaborations" do
          it { subject.wont_include @end_user_2.id }
        end

        describe "and it contains LEGACY_CC collaborations" do
          it { subject.wont_include @end_user_1.id }
          it { subject.wont_include @agent_1.id }
        end

        describe "and it contains FOLLOWER collaborations" do
          it { subject.must_include @agent_2.id }
        end
      end
    end
  end

  it "presents deleted_ticket_form" do
    setup_ticket_forms
    Account.any_instance.stubs(:has_ticket_forms?).returns(true)
    @model.update_column(:ticket_form_id, @non_active_form.id)

    presenter = Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder)
    presenter.preload_associations([@model])

    assert_equal @non_active_form.id,           presenter.side_loads(@model)[:deleted_ticket_forms].first[:id]
    assert_equal @non_active_form.display_name, presenter.side_loads(@model)[:deleted_ticket_forms].first[:display_name]
  end

  it "does not present deleted_ticket_form when it is active" do
    setup_ticket_forms
    Account.any_instance.stubs(:has_ticket_forms?).returns(true)
    @model.update_column(:ticket_form_id, @default_form.id)

    presenter = Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder, includes: [:deleted_ticket_form])
    presenter.preload_associations([@model])

    assert_nil presenter.side_loads(@model)[:deleted_ticket_forms]
  end

  describe 'presenting problem id' do
    it "presents problem ticket's nice id as problem_id" do
      @model.problem = tickets(:minimum_1)
      assert_equal tickets(:minimum_1).nice_id, @presenter.model_json(@model)[:problem_id]
    end

    it 'does not continuously fetch problems if they are archived' do
      problem = tickets(:minimum_1)
      problem.stubs(:archived?).returns(true)

      @model.expects(:problem).once.returns(problem)

      @presenter.problem_id(@model)
      @presenter.problem_id(@model)
      assert_equal tickets(:minimum_1).nice_id, @presenter.model_json(@model)[:problem_id]
    end
  end

  describe "with permissions" do
    before do
      @presenter = Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder, includes: [:permissions])
      @presenter.preload_associations([@model])
      Account.any_instance.stubs(has_ticket_forms?: false)
    end

    should_present_keys :allow_attachments,
      :allow_channelback,
      :assignee_id,
      :brand_id,
      :collaborator_ids,
      :created_at,
      :custom_fields,
      :description,
      :due_at,
      :email_cc_ids,
      :external_id,
      :fields,
      :follower_ids,
      :followup_ids,
      :forum_topic_id,
      :group_id,
      :has_incidents,
      :id,
      :is_public,
      :organization_id,
      :permissions,
      :priority,
      :problem_id,
      :raw_subject,
      :recipient,
      :requester_id,
      :satisfaction_rating,
      :sharing_agreement_ids,
      :status,
      :subject,
      :submitter_id,
      :tags,
      :type,
      :updated_at,
      :url,
      :via

    it "presents permissions" do
      @presenter.expects(:permissions).returns("stub_permissions")
      assert_equal "stub_permissions", @presenter.model_json(@model)[:permissions]
    end
  end

  describe "with permission to sideload everything" do
    describe_with_arturo_setting_enabled :customer_satisfaction do
      should_side_load :users, :organizations, :groups, :audit, :last_audits,
        :metrics, :metric_sets, :sharing_agreements, :followups, :satisfaction_ratings,
        :brands, :commenters
    end
  end

  describe "side-loading organizations" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:organizations).returns(true)
    end

    it "uses the OrganizationPresenter" do
      tickets = [@model]
      side_loaded_orgs = [stub]

      presenter.preload_associations(tickets)
      @presenter.expects(:side_load_association).with(tickets, :organization, @presenter.organization_presenter).returns(side_loaded_orgs)

      assert_equal side_loaded_orgs, @presenter.side_loads(tickets)[:organizations]
    end

    it "does not N+1 Organization side loads for archived tickets" do
      archive_and_delete(@model)
      # `Ticket#archived?` requires a full re-instantiation. reload is insufficient.
      @model = Ticket.find(@model.id)
      assert_no_n_plus_one do
        @presenter.present(@model)
      end
    end
  end

  describe "side-loading audit" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:audit).returns(true)
    end

    describe_with_and_without_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      describe_with_and_without_arturo_enabled :preload_audit_events_in_ticket_presenter do
        it "uses an AuditPresenter" do
          ticket = tickets(:minimum_1)
          audit = [stub(id: 1, ticket_id: ticket.id)]

          ticket.stubs(:archived?).returns(false)
          expects_chain(ticket, :audits, :latest).returns(audit)

          serialized_audit = { ticket_id: ticket.nice_id }

          Api::V2::Tickets::AuditPresenter.any_instance.expects(model_json: serialized_audit).with(audit)

          assert_equal serialized_audit, @presenter.side_loads(ticket)[:audit]
        end

        it "uses audits.last archived audits" do
          ticket = tickets(:minimum_1)

          archive_and_delete(ticket)
          ticket = Ticket.find(ticket.id)

          audits = stub
          ticket.expects(:audits).returns(audits)
          audits.expects(:last).returns

          @presenter.audit_presenter.expects(:model_json)

          @presenter.side_loads(ticket)[:audit]
        end
      end
    end
  end

  describe "side-loading last_audits" do
    let(:ticket) { tickets(:minimum_1) }

    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:last_audits).returns(true)
    end

    describe_with_and_without_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      describe_with_and_without_arturo_enabled :preload_audit_events_in_ticket_presenter do
        let(:serialized_audit) { { ticket_id: ticket.nice_id } }

        before do
          Api::V2::Tickets::AuditPresenter.any_instance.stubs(model_json: serialized_audit).with(ticket.audits.last)
        end

        it "presents the last_audit as part of the JSON" do
          @presenter.model_json(ticket)[:last_audit].must_equal serialized_audit
        end

        it "presents the last_audit as part of the side loads" do
          side_loads = @presenter.side_loads(ticket)
          side_loads[:last_audits][0].must_equal serialized_audit
        end
      end
    end
  end

  describe "side-loading audit and last_audits" do
    let(:ticket) { tickets(:minimum_1) }

    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:audit).returns(true)
      @presenter.stubs(:side_load?).with(:last_audits).returns(true)
    end

    [:email_ccs_preload_notifications_with_ccs, :preload_audit_events_in_ticket_presenter].each do |arturo|
      describe_with_arturo_enabled arturo do
        it "uses last_audits_side_load method" do
          @presenter.expects(:last_audits_side_load).at_least_once
          @presenter.side_loads(ticket)
        end

        it "generates JSON for the last audit of the ticket only once" do
          serialized_audit = { ticket_id: ticket.nice_id }
          Api::V2::Tickets::AuditPresenter.any_instance.expects(model_json: serialized_audit).with(ticket.audits.last).once
          @presenter.side_loads(ticket)
        end

        it "results in the same JSON for both" do
          side_loads = @presenter.side_loads(ticket)
          side_loads[:audit].must_equal side_loads[:last_audits][0]
        end
      end
    end

    describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
      describe_with_arturo_disabled :preload_audit_events_in_ticket_presenter do
        it "does not use the last_audits_side_load method" do
          @presenter.expects(:last_audits_side_load).never
          @presenter.side_loads(ticket)
        end

        it "generates JSON for the last audit of the ticket multiple times" do
          serialized_audit = { ticket_id: ticket.nice_id }
          Api::V2::Tickets::AuditPresenter.any_instance.expects(model_json: serialized_audit).with(ticket.audits.last).at_least(2)
          @presenter.side_loads(ticket)
        end

        it "results in the same JSON for both" do
          side_loads = @presenter.side_loads(ticket)
          side_loads[:audit].must_equal side_loads[:last_audits][0]
        end
      end
    end
  end

  describe "side-loading groups" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:groups).returns(true)
    end

    it "uses the GroupPresenter" do
      tickets = [@model]
      side_loaded_groups = [stub]

      presenter.preload_associations(tickets)
      @presenter.expects(:side_load_association).with(tickets, :group, @presenter.group_presenter).returns(side_loaded_groups)

      assert_equal side_loaded_groups, @presenter.side_loads(tickets)[:groups]
    end
  end

  describe "side-loading users" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:users).returns(true)
    end

    it "uses the UserPresenter" do
      users   = [stub(id: 1), stub(id: 2), stub(id: 3), stub(id: 4)]
      tickets = [
        stub(id: 1, requester_id: 1, assignee_id: nil, submitter_id: nil, collaborations: [], archived?: false),
        stub(id: 2, requester_id: 1, assignee_id: 2,   submitter_id: nil, collaborations: [], archived?: false),
        stub(id: 3, requester_id: 1, assignee_id: 2,   submitter_id: 3,   collaborations: [], archived?: false),
        stub(id: 4, requester_id: 1, assignee_id: 2,   submitter_id: 3,   collaborations: [stub(user_id: 4)], archived?: false)
      ]

      relation = stub(to_a: users)
      @presenter.account.all_users.expects(:where).with(id: [1, 2, 3, 4]).returns(relation)

      serialized_users = []

      users.each do |user|
        user_stub = stub
        serialized_users << user_stub
        @presenter.user_presenter.expects(:model_json).with(user).returns(user_stub)
      end

      assert_equal serialized_users, @presenter.side_loads(tickets)[:users]
    end

    describe "email ccs" do
      let(:recipients) { [users(:minimum_end_user), users(:minimum_search_user)] }
      let(:account) { accounts(:minimum) }
      let(:user) { users(:minimum_agent) }

      def save_ticket
        @ticket = Ticket.new(account: account, description: "testing recipient sideloads")
        [ViaType.WEB_SERVICE, ViaType.MAIL].each do |via_type|
          recipients.each do |recipient|
            @ticket.collaborations.build(ticket: @ticket, user: recipient, collaborator_type: CollaboratorType.EMAIL_CC)
            @ticket.recipients_for_latest_mail = recipient.email if via_type == ViaType.MAIL

            @ticket.will_be_saved_by(user, via_id: via_type)
            @ticket.add_comment(body: "cc #{recipient.name}", is_public: true, author: user)
            @ticket.save!
            @ticket.reload
          end
        end

        @presenter = Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder)
        @presenter.stubs(follower_ids: [])
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:users).returns(true)
      end

      describe "when comment_email_ccs_allowed setting is enabled" do
        before do
          Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: true)
          Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
          Audit.any_instance.stubs(recipients_notification_ccs: recipients)
        end

        it "side loads recipients" do
          save_ticket
          sideloaded_users = @presenter.present(@ticket.reload)[:users]
          assert_equal 3, sideloaded_users.size
          recipients.each do |recipient|
            assert_includes sideloaded_users, @presenter.user_presenter.model_json(recipient)
          end
        end

        it "only calls ticket.email_ccs once for both presentation and users side load" do
          save_ticket
          Ticket.any_instance.expects(email_ccs: recipients).once
          @presenter.present(@ticket.reload)
        end

        it "doesn't N+1 comment_email_ccs" do
          save_ticket
          assert_no_n_plus_one do
            @presenter.present(@ticket)
          end
        end
      end

      describe "when comment_email_ccs_allowed setting is disabled" do
        before do
          Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
        end

        it "does not side load recipients" do
          save_ticket

          sideloaded_users = @presenter.present(@ticket)[:users]
          assert_equal 1, sideloaded_users.size
          refute_includes sideloaded_users, @presenter.user_presenter.model_json(users(:minimum_end_user))
        end
      end
    end

    describe "followers" do
      let(:followers) { [users(:minimum_agent), users(:minimum_admin)] }
      let(:account) { accounts(:minimum) }
      let(:user) { users(:minimum_agent) }

      def save_ticket
        @ticket = Ticket.new(account: account, description: "testing user sideloads")
        followers.each do |follower|
          @ticket.collaborations.build(ticket: @ticket, user: follower, collaborator_type: CollaboratorType.FOLLOWER)
          @ticket.will_be_saved_by(user, via_id: ViaType.WEB_SERVICE)
          @ticket.add_comment(body: "follower #{follower.name}", is_public: true, author: user)
          @ticket.save!
          @ticket.reload
        end

        @presenter = Api::V2::Tickets::TicketPresenter.new(@account.owner, url_builder: mock_url_builder)
        @presenter.stubs(email_cc_ids: [])
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:users).returns(true)
      end

      describe "when ticket_followers_allowed setting is enabled" do
        before do
          Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: true)
          Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true)
        end

        it "side loads followers" do
          save_ticket
          sideloaded_users = @presenter.present(@ticket.reload)[:users]
          assert_equal 2, sideloaded_users.size

          # This is to workaround the "default" field in the group memberships
          # not loading correctly for some reason using the fixtures. The ticket
          # presenter does end up with "default" being true for these agents'
          # group memberships, but when we are looking at the users' "memberships"
          # associations here, that field is "false". If we load the users directly
          # from the database here, then it works as expected.
          follower_users = account.all_users.where(id: followers.map(&:id)).to_a
          follower_users.each do |follower|
            assert_includes sideloaded_users, @presenter.user_presenter.model_json(follower)
          end
        end

        it "only calls ticket.followers once for both presentation and users side load" do
          save_ticket
          Ticket.any_instance.expects(followers: followers).once
          @presenter.present(@ticket.reload)
        end
      end
    end
  end

  describe "side-loading sharing agreements" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:sharing_agreements).returns(true)
    end

    it "uses the SharingAgreementPresenter" do
      agreements = [stub(id: 1), stub(id: 2)]
      tickets = [
        stub(id: 1, shared_tickets: [stub(agreement_id: 1)], archived?: false),
        stub(id: 2, shared_tickets: [stub(agreement_id: 1)], archived?: false),
        stub(id: 3, shared_tickets: [stub(agreement_id: 2)], archived?: false)
      ]

      relation = stub(to_a: agreements)
      @presenter.account.sharing_agreements.expects(:where).with(id: [1, 2]).returns(relation)

      serialized_agreement1 = stub
      serialized_agreement2 = stub

      @presenter.sharing_agreement_presenter.expects(:model_json).with(agreements[0]).returns(serialized_agreement1)
      @presenter.sharing_agreement_presenter.expects(:model_json).with(agreements[1]).returns(serialized_agreement2)

      assert_equal [serialized_agreement1, serialized_agreement2], @presenter.side_loads(tickets)[:sharing_agreements]
    end
  end

  describe "side-loading followups" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:followups).returns(true)
    end

    it "side loads the followup association" do
      ticket = tickets(:minimum_1)
      followup = tickets(:minimum_2)
      ticket.followups << followup

      assert_equal [followup.nice_id], @presenter.side_loads([ticket])[:followups].map { |h| h[:id] }
    end
  end

  describe "side-loading metrics" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:metrics).returns(true)
    end

    it "uses the MetricSetPresenter" do
      metric = stub(id: 1, ticket_id: 1)
      ticket = stub(id: 1, to_ary: nil, to_a: [])

      ticket.expects(:ticket_metric_set).returns(metric)
      serialized_metric = stub

      @presenter.metric_set_presenter.expects(:model_json).with(metric).returns(serialized_metric)

      assert_equal serialized_metric, @presenter.side_loads(ticket)[:metrics]
    end
  end

  describe "side-loading metric_sets" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:metric_sets).returns(true)
    end

    it "does not explode when no metric is available" do
      @ticket = tickets(:minimum_1)

      @ticket.expects(:ticket_metric_set).returns(nil)
      @presenter.metric_set_presenter.expects(:model_json).never

      assert_nil @presenter.model_json(@ticket)[:metrics]
    end

    it "associates archived tickets and metric_sets" do
      @ticket = tickets(:minimum_1)
      archive_and_delete(@ticket)
      @ticket = Ticket.find(@ticket.id)
      @presenter.preload_associations(@ticket)
      metric_set = ticket_metric_sets(:minimum_set_1)

      @ticket.stubs(:ticket_metric_set).returns(metric_set)

      assert presenter.model_json(@ticket)[:metric_set]
    end
  end

  describe "side-loading dates" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:dates).returns(true)
    end

    it "returns the ticket dates" do
      @ticket = tickets(:minimum_1)

      dates = @presenter.model_json(@ticket)[:dates]
      assert_equal @ticket.assignee_updated_at, dates[:assignee_updated_at]
      assert_equal @ticket.requester_updated_at, dates[:requester_updated_at]
      assert_equal @ticket.status_updated_at, dates[:status_updated_at]
      assert_equal @ticket.initially_assigned_at, dates[:initially_assigned_at]
      assert_equal @ticket.assigned_at, dates[:assigned_at]
      assert_nil dates[:solved_at]
      assert_nil dates[:latest_comment_added_at]
    end
  end

  describe "side-loading brands" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:brands).returns(true)

      @ticket = tickets(:minimum_1)
      @brand  = FactoryBot.create(:brand)
      @ticket.brand = @brand
      @ticket.will_be_saved_by(@account.owner)
      @ticket.save!
    end

    it "uses the BrandPresenter" do
      serialized_brand = stub

      @presenter.brand_presenter.expects(:model_json).with(@brand).returns(serialized_brand)

      assert_equal [serialized_brand], @presenter.side_loads(@ticket)[:brands]
    end

    it "also loads deleted brands for closed tickets" do
      @ticket.status_id = StatusType.CLOSED
      @ticket.will_be_saved_by(@account.owner)
      @ticket.save!

      assert_equal @brand, @ticket.brand
      @brand.soft_delete

      assert_not_nil @brand.deleted_at
      @ticket.reload

      serialized_brand = stub
      @presenter.brand_presenter.expects(:model_json).with(@brand).returns(serialized_brand)
      assert_equal [serialized_brand], @presenter.side_loads(@ticket)[:brands]
    end
  end

  describe "side-loading ticket forms" do
    let(:ticket_form) { ticket_forms(:default_ticket_form) }

    before do
      ticket_form.account_id = @account.id
      ticket_form.save!
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:ticket_forms).returns(true)
      @presenter.account.expects(:has_ticket_forms?).returns(true).at_least_once

      @model.update_column(:ticket_form_id, ticket_form.id)
    end

    it "uses the TicketFormPresenter" do
      serialized_ticket_form = stub
      @presenter.ticket_form_presenter.expects(:model_json).with(ticket_form).returns(serialized_ticket_form)
      assert_equal [serialized_ticket_form], @presenter.side_loads(@model)[:ticket_forms]
    end

    it "does not N+1 TicketForm side loads for archived tickets" do
      archive_and_delete(@model)
      # `Ticket#archived?` requires a full re-instantiation. reload is insufficient.
      @model = Ticket.find(@model.id)
      assert_no_n_plus_one do
        @presenter.present(@model)
      end
    end
  end

  describe "side-loading satisfaction_ratings" do
    describe_with_arturo_setting_enabled :customer_satisfaction do
      before do
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:satisfaction_ratings).returns(true)
      end

      it "uses the SatisfactionRatingPresenter" do
        tickets = [@model]
        side_loaded_ratings = [stub]

        presenter.preload_associations(tickets)
        @presenter.expects(:side_load_association).with(tickets, :satisfaction_ratings, @presenter.satisfaction_rating_presenter).returns(side_loaded_ratings)

        assert_equal side_loaded_ratings, @presenter.side_loads(tickets)[:satisfaction_ratings]
      end
    end
  end

  describe "side-loading incident_counts" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:incident_counts).returns(true)
      tickets(:minimum_2).update_column(:linked_id, @model.id)
    end

    it "side-loads for problems" do
      @model.ticket_type_id = TicketType.PROBLEM
      @presenter.preload_associations(@model)
      assert_equal 1, @presenter.model_json(@model)[:incident_count]
    end

    it "includes archived tickets in the count" do
      @model.ticket_type_id = TicketType.PROBLEM
      archive_and_delete tickets(:minimum_2)
      @presenter.preload_associations(@model)
      assert_equal 1, @presenter.model_json(@model)[:incident_count]
    end

    it "does not side-load for non-problems" do
      refute @presenter.model_json(@model)[:incident_count]
    end
  end

  describe 'side-loading SLA information' do
    describe 'when there are SLA statuses' do
      let(:policy) { new_sla_policy(account: @account, tags: 'match_sla') }
      let(:end_user) { users(:minimum_end_user) }
      let(:ticket) { @model }

      before do
        @presenter.stubs(:side_load?).returns(false)

        new_sla_policy_metric(
          account:     @account,
          policy:      policy,
          metric_id:   Zendesk::Sla::TicketMetric::FirstReplyTime.id,
          priority_id: @model.priority_id,
          target:      50
        )
        new_sla_policy_metric(
          account:     @account,
          policy:      policy,
          metric_id:   Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
          priority_id: @model.priority_id,
          target:      100
        )

        ticket.events.delete_all
        ticket.metric_events.delete_all

        Account.any_instance.stubs(has_service_level_agreements?: true)

        ticket.update_column(:created_at, Time.utc(2020))

        # Record the measure metric events
        measure_metrics(
          :agent_work_time,
          :pausable_update_time,
          :periodic_update_time,
          :reply_time,
          :requester_wait_time
        )

        tags_updated(after: 0.minutes, to: 'match_sla')
        status_updated(after: 0.minutes, to: StatusType.OPEN)
        comments_added([{after: 0.minutes, by: end_user}])
        priority_updated(after: 6.minutes, to: PriorityType.NORMAL)
        status_updated(after: 10.minutes, to: StatusType.PENDING)
        status_updated(after: 29.minutes, to: StatusType.OPEN)
      end

      describe 'when side-loading' do
        before { @presenter.stubs(:side_load?).with(:slas).returns(true) }

        describe 'and the account does not have the feature' do
          before do
            Account.any_instance.stubs(has_service_level_agreements?: false)
          end

          it 'does not include the side-load' do
            refute @presenter.model_json(@model).key?(:slas)
          end
        end

        describe 'and the account has the feature' do
          let(:metrics_json) do
            @presenter.model_json(@model)[:slas][:policy_metrics]
          end

          it 'includes the side-load' do
            assert @presenter.model_json(@model).key?(:slas)
          end

          describe_with_arturo_enabled :sla_ticket_metrics_reader do
            it 'includes the correct number of statuses' do
              assert_equal 2, metrics_json.length
            end
          end

          describe_with_arturo_disabled :sla_ticket_metrics_reader do
            it 'includes the correct number of statuses' do
              assert_equal 2, metrics_json.length
            end

            describe_with_arturo_enabled :sla_sideload_first_reply_time do
              before { metrics_json }
              before_should 'include first_reply_time' do
                Zendesk::Sla::MetricSelection.any_instance.expects(all: []).with(true)
              end
            end

            describe_with_arturo_disabled :sla_sideload_first_reply_time do
              before { metrics_json }
              before_should 'not include first_reply_time' do
                Zendesk::Sla::MetricSelection.any_instance.expects(all: []).with(false)
              end
            end
          end

          describe 'when monitoring performance' do
            let(:span) { stub(:span) }

            before { metrics_json }

            before_should 'trace sla sideloads' do
              span.expects(:set_tag).with(
                Datadog::Ext::Analytics::TAG_ENABLED,
                true
              )

              span.expects(:set_tag).with('zendesk.ticket.id', @model.id)

              span.expects(:set_tag).with(
                'zendesk.account_id',
                @model.account_id
              )

              span.expects(:set_tag).with(
                'zendesk.account_subdomain',
                @model.account.subdomain
              )

              ZendeskAPM.
                expects(:trace).
                with(
                  'ticket_presenter.slas',
                  service: Zendesk::Sla::APM_SERVICE_NAME
                ).
                yields(span)
            end
          end
        end
      end

      describe 'when not side-loading' do
        before { @presenter.stubs(:side_load?).with(:slas).returns(false) }

        it 'does not include the side-load' do
          refute @presenter.model_json(@model).key?(:slas)
        end
      end
    end
  end

  describe "side-loading sla_policy" do
    before do
      @presenter.account.stubs(:has_service_level_agreements?).returns(true)

      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:sla_policy).returns(true)
    end

    describe "when the ticket does not have an associated SLA policy" do
      it "side-loads nil" do
        assert_nil @presenter.model_json(@model)[:sla_policy]
      end
    end

    describe "when the ticket has an associated SLA policy" do
      before do
        @model.build_sla_ticket_policy(
          policy: new_sla_policy(account: @account)
        )
      end

      it "side-loads the sla_policy" do
        assert_not_nil @presenter.model_json(@model)[:sla_policy]
      end
    end
  end

  describe "side-loading metric events" do
    let(:presented_events) { @presenter.model_json(@model)[:metric_events] }

    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:metric_events).returns(true)

      @model.metric_events.reply_time.activate.create!(
        account: @model.account,
        time:    @model.created_at
      )
      @model.metric_events.reply_time.fulfill.create!(
        account: @model.account,
        time:    @model.created_at + 10.minutes
      )
      @model.metric_events.reply_time.activate.create!(
        account:     @model.account,
        instance_id: 2,
        time:        @model.created_at + 15.minutes
      )
      @model.metric_events.requester_wait_time.activate.create!(
        account: @model.account,
        time:    @model.created_at
      )
      @model.metric_events.agent_work_time.activate.create!(
        account: @model.account,
        time:    @model.created_at
      )
      @model.metric_events.periodic_update_time.activate.create!(
        account:     @model.account,
        instance_id: 2,
        time:        @model.created_at + 15.minutes
      )
      TicketMetric::UpdateStatus.create!(
        account: @model.account,
        ticket:  @model,
        metric:  'periodic_update_time',
        time:    @model.created_at + 15.minutes
      )
      @model.metric_events.periodic_update_time.fulfill.create!(
        account: @model.account,
        time:    @model.created_at + 15.minutes
      )
      @model.metric_events.reset
    end

    it "includes the correct number of grouped 'agent work time' events" do
      assert_equal 1, presented_events[:agent_work_time].count
    end

    it "includes the correct number of grouped 'reply time' events" do
      assert_equal 3, presented_events[:reply_time].count
    end

    it "includes the correct number of grouped 'requester wait time' events" do
      assert_equal 1, presented_events[:requester_wait_time].count
    end

    it "includes the correct number of grouped 'periodic update time' events" do
      assert_equal 3, presented_events[:periodic_update_time].count
    end

    it "orders the events correctly" do
      assert_equal(
        %w[fulfill update_status activate],
        presented_events[:periodic_update_time].map { |event| event[:type] }
      )
    end
  end

  describe "side-loading public_updated_at" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:public_updated_at).returns(true)
    end

    it "side-loads public_updated_at" do
      assert_not_nil @presenter.model_json(@model)[:public_updated_at]
    end
  end

  describe "side-loading commenters" do
    before do
      @user_1 = users(:minimum_end_user)
      @user_2 = users(:minimum_end_user2)

      @ticket_1 = FactoryBot.create(:ticket, account: @account)
      @ticket_2 = FactoryBot.create(:ticket, account: @account)
      ticket_3 = FactoryBot.create(:ticket, account: @account)

      @ticket_1.comments.delete_all
      @ticket_2.comments.delete_all
      ticket_3.comments.delete_all

      Comment.create!(ticket: @ticket_1, audit: @ticket_1.audits.first, author: @user_1, created_at: 10.minutes.ago)
      Comment.create!(ticket: @ticket_1, audit: @ticket_1.audits.first, author: @user_2, created_at: 11.minutes.ago)

      Comment.create!(ticket: @ticket_2, audit: @ticket_2.audits.first, author: @user_1, created_at: 12.minutes.ago)
      Comment.create!(ticket: @ticket_2, audit: @ticket_2.audits.first, author: @user_1, created_at: 13.minutes.ago)
      Comment.create!(ticket: @ticket_2, audit: @ticket_2.audits.first, author: @user_2, created_at: 14.minutes.ago)

      Comment.create!(ticket: ticket_3, audit: ticket_3.audits.first, author: @user_1, created_at: 15.minutes.ago)

      archive_and_delete(ticket_3)
      # After archiving and deleting the ticket, its old reference points to an
      # object that does not exists anymore, that's why, to keep a reference to
      # this ticket, it's needed to search it again.
      @archived_ticket = Ticket.find(ticket_3.id)

      @tickets = [@ticket_1.reload, @ticket_2.reload, @archived_ticket.reload]
    end

    subject do
      @presenter.side_loads(@tickets)
    end

    describe "sideload 'commenters'" do
      before do
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:commenters).returns(true)
      end

      it "contains an object with each ticket's nice_id as keys" do
        assert_equal @tickets.size, subject[:commenters].keys.size
        subject[:commenters].keys.must_equal @tickets.map(&:nice_id)
      end

      it "contains an array with the correct quantity of commenters per each ticket" do
        assert_equal 2, subject[:commenters][@ticket_1.nice_id].size
        assert_equal 2, subject[:commenters][@ticket_2.nice_id].size
        assert_equal 1, subject[:commenters][@archived_ticket.nice_id].size
      end

      it "contains an array with the correct commenters per each ticket preserving the order of comments" do
        ticket_1_commenters_id = subject[:commenters][@ticket_1.nice_id].map { |commenter| commenter[:id] }
        ticket_2_commenters_id = subject[:commenters][@ticket_2.nice_id].map { |commenter| commenter[:id] }
        archived_commenters_id = subject[:commenters][@archived_ticket.nice_id].map { |commenter| commenter[:id] }

        assert_equal [@user_2.id, @user_1.id], ticket_1_commenters_id
        assert_equal [@user_2.id, @user_1.id], ticket_2_commenters_id
        assert_equal [@user_1.id], archived_commenters_id
      end

      it "does not run N+1 queries" do
        assert_no_n_plus_one { subject }
      end
    end

    describe "sideload 'commenters' not requested" do
      before do
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:commenters).returns(false)
      end

      it "does not contains a 'commenters' object" do
        assert_nil subject[:commenters]
      end
    end
  end

  it "presents mail tickets without n+1s" do
    Ticket.update_all(via_id: ViaType.MAIL)

    assert_no_n_plus_one do
      @presenter.present(@account.tickets)
    end
  end

  describe "side-loading 'last_comment'" do
    subject { @presenter.model_json(@model) }

    before do
      Comment.create!(
        audit: @model.audits.first,
        author_id: users(:minimum_agent).id,
        created_at: Date.yesterday
      )
      @presenter.stubs(:side_load?).returns(false)
    end

    describe "sideload 'last_comment' requested" do
      before do
        @presenter.stubs(:side_load?).with(:last_comment).returns(true)
      end

      it "includes last comment made on the ticket" do
        last_comment = Comment.create!(
          audit: @model.audits.first,
          author_id: users(:minimum_agent).id,
          created_at: Date.today
        )
        assert subject[:last_comment], "there should be a comment"
        assert_equal last_comment.id, subject[:last_comment][:id]
      end

      it "does not include last comment if the first_comment is the same as the last one" do
        @model.comments.delete_all
        Comment.create!(
          audit: @model.audits.first,
          author_id: users(:minimum_agent).id,
          created_at: Date.today
        )
        assert_nil subject[:last_comment]
      end
    end

    describe "sideload 'last_comment' not requested" do
      before do
        @presenter.stubs(:side_load?).with(:last_comment).returns(false)
      end

      it "does not include the last comment" do
        assert_nil subject[:last_comment]
      end
    end
  end

  describe "side-loading 'first_comment'" do
    subject { @presenter.model_json(@model) }

    before do
      Comment.create!(
        audit: @model.audits.first,
        author_id: users(:minimum_agent).id
      )
      @presenter.stubs(:side_load?).returns(false)
    end

    describe "sideload 'first_comment' requested" do
      before do
        @presenter.stubs(:side_load?).with(:first_comment).returns(true)
      end

      it "includes first comment made on the ticket" do
        assert subject[:first_comment], "there should be a comment"
        assert_equal @model.comments.first.body, subject[:first_comment][:body]
      end
    end

    describe "sideload 'first_comment' not requested" do
      before do
        @presenter.stubs(:side_load?).with(:first_comment).returns(false)
      end

      it "does not include the last comment" do
        assert_nil subject[:first_comment]
      end
    end
  end

  describe 'first and last voice comments' do
    let(:user) { @account.users.sample }
    let(:via_type) { ViaType.VOICEMAIL }
    let(:data) do
      {
        from: "+14045555404",
        to: "+18005555800",
        recording_url: "/voice/calls/12345/voice_recordings",
        call_duration: 50,
        answered_by_id: user.id,
        transcription_text: "a transcription",
        started_at: "2013-06-24 15:31:44 +0000",
        location: "location"
      }
    end
    let(:subject) { @presenter.model_json(@ticket) }

    before do
      @ticket = Zendesk::Tickets::V2::Initializer.new(
        @account,
        user,
        ticket: {
          via_id: via_type,
          description: "Deu RUIM",
          subject: "Mesmo",
          organization_id: @account.organizations.first.id,
          brand_id: @account.default_brand.id,
          voice_comment: data
        }
      ).ticket
      @ticket.save!

      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:first_comment).returns(true)
      @presenter.stubs(:side_load?).with(:last_comment).returns(true)
    end

    it 'present `first_comment` as voice comments' do
      comment = @ticket.comments.first
      payload = subject[:first_comment]

      assert_voice_comment_payload comment, payload
    end

    it 'present `last_comment` as voice comments' do
      comment = VoiceApiComment.create(
        account: @account,
        ticket: @ticket,
        data: data,
        author_id: user,
        audit: @ticket.audits.first,
        via_id: via_type,
        via_reference_id: via_type
      )

      payload = subject[:last_comment]

      assert_voice_comment_payload comment, payload
    end
  end

  describe 'allow_channelback' do
    describe 'when the ticket is not from channels' do
      it 'presents the `allow_channelback` attribute as false' do
        @model.stubs(:via_channel?).returns(false)

        assert_equal false, @presenter.model_json(@model)[:allow_channelback]
      end
    end

    describe 'when the ticket is from a social channel' do
      it 'calls into the channels code base and presents the `allow_channelback` attribute as the result' do
        @model.stubs(:via_channel?).returns(true)

        channelback_result = stub(:channelback_result)
        Channels::TicketPresenterHelper.expects(:allow_channelback?).with(@model).returns(channelback_result)

        assert_equal channelback_result, @presenter.model_json(@model)[:allow_channelback]
      end
    end
  end

  describe 'reply_options' do
    before do
      @presenter.stubs(:side_load?).returns(false)
    end

    describe 'Arturo disabled' do
      before do
        Arturo.disable_feature!(:reply_options)
      end

      it 'does not include reply_options in result' do
        subject = @presenter.model_json(@model)
        refute subject.key?(:reply_options)
      end
    end

    describe 'Arturo enabled' do
      before do
        Arturo.enable_feature!(:reply_options)
      end

      describe 'side load not specified' do
        before do
          @presenter.stubs(:side_load?).with(:reply_options).returns(false)
        end

        it 'does not include reply_options in result' do
          subject = @presenter.model_json(@model)
          refute subject.key?(:reply_options)
        end
      end

      describe 'side load specified' do
        before do
          @presenter.stubs(:side_load?).with(:reply_options).returns(true)
        end

        it 'include reply_options in result based on a call to ReplyOptionsPresenter' do
          expected = { foo: 'bar' }
          ::Api::V2::Tickets::ReplyOptionsPresenter.any_instance.expects(:model_json).with(@model).returns(expected)
          subject = @presenter.model_json(@model)
          assert_equal expected, subject[:reply_options]
        end
      end
    end
  end

  describe 'is_aeu' do
    describe_with_arturo_enabled(:agent_as_end_user) do
      before do
        Arturo.enable_feature!(:agent_as_end_user)
      end

      describe 'ticket is an aeu ticket' do
        before do
          @model.stubs(:agent_as_end_user?).returns(true)
        end

        it 'presents the value of ticket.agent_as_end_user?' do
          assert @presenter.model_json(@model)[:is_aeu]
        end

        it 'presents the name of the ticket submitter' do
          assert_equal @model.submitter.name, @presenter.model_json(@model)[:aeu_agent_name]
        end
      end

      describe 'ticket is not an aeu ticket' do
        before do
          @model.stubs(:agent_as_end_user?).returns(false)
        end

        it 'presents the value of ticket.agent_as_end_user?' do
          refute @presenter.model_json(@model)[:is_aeu]
        end

        it 'does not present the name of the ticket submitter' do
          refute @presenter.model_json(@model).key?(:aeu_agent_name)
        end
      end
    end

    describe_with_arturo_disabled(:agent_as_end_user) do
      before do
        Arturo.disable_feature!(:agent_as_end_user)
      end

      it 'does not present is_aeu' do
        refute @presenter.model_json(@model).key?(:is_aeu)
      end

      it 'does not present the name of the ticket submitter' do
        refute @presenter.model_json(@model).key?(:aeu_agent_name)
      end
    end
  end

  describe "side-loading 'workspace'" do
    let(:workspace) { workspaces(:refund_workspace) }

    subject { @presenter.model_json(@model) }

    before do
      Account.any_instance.stubs(:has_contextual_workspaces_in_js?).returns(false)
      Account.any_instance.stubs(:has_contextual_workspaces?).returns(true)
      @model.workspace = workspace
      @model.save
      @presenter.stubs(:side_load?).returns(false)
    end

    describe "when requested" do
      before do
        @presenter.stubs(:side_load?).with(:tde_workspace).returns(true)
      end

      it "includes workspace details" do
        assert_equal subject[:tde_workspace][:id], workspace.id
      end

      it "does not include workspace details if the account has contextual_workspace_in_js arturo" do
        Account.any_instance.stubs(:has_contextual_workspaces_in_js?).returns(true)
        refute subject[:tde_workspace]
      end
    end

    describe "when not requested" do
      before do
        @presenter.stubs(:side_load?).with(:tde_workspace).returns(false)
      end

      it "does not include workspace details" do
        assert_nil subject[:tde_workspace]
      end
    end
  end

  describe '#present_errors' do
    it 'includes ticket fields id and type' do
      ticket = users(:minimum_agent).tickets.new
      ticket.valid?

      result = @presenter.present_errors(ticket)
      keys = result[:details][:base].first.keys

      assert_includes keys, :ticket_field_id
      assert_includes keys, :ticket_field_type
    end
  end

  def assert_voice_comment_payload(comment, payload)
    assert_equal comment.id, payload[:id]
    assert_equal "VoiceComment", payload[:type]
    assert_equal comment.data[:from], payload[:data][:from]
    assert_equal comment.data[:to], payload[:data][:to]
    assert_equal comment.data[:recording_url], payload[:data][:recording_url]
    assert_equal comment.data[:call_duration], payload[:data][:call_duration]
    assert_equal comment.data[:answered_by_id], payload[:data][:answered_by_id]
    assert_equal comment.data[:transcription_text], payload[:data][:transcription_text]
    assert_equal comment.data[:started_at], payload[:data][:started_at]
    assert_equal comment.data[:location], payload[:data][:location]
    assert_equal_with_nil comment.data[:answerd_by_name], payload[:data][:answerd_by_name] # TODO: both nil
    assert_equal_with_nil comment.data[:transcript_status], payload[:data][:transcript_status] # TODO: both nil
    assert_equal comment.formatted_from, payload[:formatted_from]
    assert_equal comment.formatted_to, payload[:formatted_to]
    assert_equal comment.transcription_visible, payload[:transcription_visible]
    assert_equal comment.author_id, payload[:author_id]
    assert_equal comment.body, payload[:body]
    assert_equal comment.trusted, payload[:trusted]
    assert_equal comment.attachments, payload[:attachments]
    assert_equal comment.created_at.to_s(:db), payload[:created_at].to_s(:db)
  end

  describe '#reduced_payload_size?' do
    before do
      @presenter.stubs(:reduced_payload_size?).returns(true)
    end

    it 'does not include the description in the result' do
      subject = @presenter.model_json(@model)
      refute subject[:description]
    end

    it 'does not include fields as a duplicate for custom_fields' do
      subject = @presenter.model_json(@model)
      refute_includes subject.keys, :fields
    end

    it 'does not include null custom fields in the result' do
      subject = @presenter.model_json(@model)
      assert_equal 1, subject[:custom_fields].length
    end
  end
end
