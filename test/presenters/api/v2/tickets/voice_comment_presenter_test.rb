require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::VoiceCommentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @recording_url = "/voice/calls/12345/voice_recordings"
    @data = {
      from: "+14045555404",
      to: "+18005555800",
      recording_url: @recording_url,
      call_duration: 50,
      answered_by_id: 6,
      transcription_text: "a transcription",
      started_at: "2013-06-24 15:31:44 +0000",
      location: "location",
      call_id: 1
    }
    @model = VoiceApiComment.create!(account: @account,
                                     data: @data,
                                     ticket: @account.tickets.last,
                                     audit: @account.tickets.last.audits.last,
                                     author: @account.owner)

    @attachment = attachments(:serialization_comment_attachment)
    @model.attachments << @attachment

    @presenter = Api::V2::Tickets::VoiceCommentPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :data, :public, :formatted_from,
    :formatted_to, :transcription_visible, :body, :html_body, :attachments,
    :author_id, :trusted, :created_at

  should_present_method :id
  should_present_method :data, value: {from: '15551234567'}
  should_present_method :is_public?, as: :public
  should_present_method :formatted_from
  should_present_method :formatted_to
  should_present_method :transcription_visible
  should_present_method :body
  should_present_method :html_body
  should_present_method :trusted?, as: :trusted
  should_present_method :author_id
  should_present_collection :attachments, with: Api::V2::AttachmentPresenter

  should_present "VoiceComment", as: :type

  it 'presents an absolute url to a greeting when voice_recording_rework is on' do
    json = @presenter.model_json(@model)
    assert_equal "#{@account.url(ssl: true)}#{@recording_url}", json[:data][:recording_url]
  end
end
