require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::PushPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @model = Push.create!(
      account: @account,
      ticket: @ticket,
      audit: @ticket.audits.last,
      author: @account.owner,
      value: 'value',
      value_previous: 'value_previous',
      value_reference: 'value_reference'
    )

    @presenter = Api::V2::Tickets::PushPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :value, :value_reference

  should_present_method :id
  should_present_method :value
  should_present_method :value_reference
  should_present "Push", as: :type
end
