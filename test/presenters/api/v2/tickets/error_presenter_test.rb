require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::ErrorPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @model = Error.create!(account: @account,
                           ticket: @ticket,
                           audit: @ticket.audits.first,
                           value: "something went wrong")

    @presenter = Api::V2::Tickets::ErrorPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :message

  should_present_method :id
  should_present_method :value, as: :message
  should_present "Error", as: :type
end
