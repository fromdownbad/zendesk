require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::CommentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  let(:options) { {} }

  before do
    @model = events(:serialization_comment)
    @attachment = attachments(:serialization_comment_attachment)
    @model.attachments << @attachment
    @presenter = Api::V2::Tickets::CommentPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
  end

  should_present_keys :id, :type, :body, :html_body, :plain_body, :public, :attachments, :author_id, :audit_id

  should_present_method :id
  should_present_method :author_id
  should_present_method :body
  should_present_method :html_body
  should_present_method :is_public?, as: :public
  should_present_method :type, as: :type

  should_present_collection :attachments, with: Api::V2::AttachmentPresenter

  describe "collection presenter" do
    let(:options) { { includes: [:test] } }

    it "has the same side-loads" do
      assert_equal [:test], @presenter.collection_presenter.includes
    end
  end

  describe "if comment passed in is nil" do
    it "should not raise error" do
      refute @presenter.model_json(nil)
    end
  end
end
