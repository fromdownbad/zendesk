require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::EmailCcChangePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  let(:email_ccs) { [users(:minimum_end_user).email] }

  before do
    @account = accounts(:minimum)
    @model = EmailCcChange.new.tap do |change|
      change.current_email_ccs = email_ccs
      change.previous_email_ccs = []
    end

    @presenter = Api::V2::Tickets::EmailCcChangePresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :current_email_ccs, :previous_email_ccs, :via

  should_present_method :id
  should_present "EmailCcChange", as: :type

  it "presents current_email_ccs" do
    json = @presenter.model_json(@model)
    assert_equal email_ccs, json[:current_email_ccs]
  end

  it "presents previous_email_ccs" do
    json = @presenter.model_json(@model)
    assert_equal [], json[:previous_email_ccs]
  end
end
