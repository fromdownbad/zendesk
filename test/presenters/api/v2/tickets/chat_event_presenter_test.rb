require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::ChatEventPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users, :tickets, :attachments

  let(:user) { users(:minimum_agent) }

  def self.new_chat_message(chat_message)
    {
      'type' => 'ChatMessage',
      'actor_id' => '123',
      'actor_type' => 'end_user',
      'actor_name' => 'jack',
      'timestamp' => 456,
      'message' => chat_message
    }
  end

  def self.new_web_path(web_path)
    {
      'url' => web_path,
      'timestamp' => 456
    }
  end

  delegate :new_chat_message, :new_web_path, to: :class

  def new_chat_event
    ticket = tickets(:minimum_1)
    ChatStartedEvent.new(ticket: ticket, value: { chat_id: 'moo', visitor_id: 'cow' }).tap do |event|
      ticket.will_be_saved_by(user)
      ticket.audit.events << event
      ticket.save!
    end
  end

  def create_transcript(event, chat_messages:, web_urls:)
    ChatTranscript.create!(chat_started_event: event, account: event.account, ticket: event.ticket) do |t|
      t.chat_history = chat_messages.map { |chat_message| new_chat_message(chat_message) }
      t.web_paths = web_urls.map { |web_url| new_web_path(web_url) }
    end
  end

  describe 'for event without transcript' do
    before do
      @model = new_chat_event
      @attachment = attachments(:serialization_comment_attachment)
      @model.attachments << @attachment
      @presenter = Api::V2::Tickets::ChatEventPresenter.new(@model.account.owner, url_builder: mock_url_builder)
    end

    should_present_keys :id, :type, :value, :attachments

    should_present_method :id
    should_present 'ChatStartedEvent', as: :type
    should_present({ 'chat_id' => 'moo', 'visitor_id' => 'cow', 'history' => [], 'webpath' => [] }, as: :value)

    should_present_collection :attachments, with: Api::V2::AttachmentPresenter
  end

  describe 'with history and path item' do
    before do
      @model = new_chat_event
      create_transcript(@model, chat_messages: ['hello'], web_urls: ['http://example.org'])
      @presenter = Api::V2::Tickets::ChatEventPresenter.new(@model.account.owner, url_builder: mock_url_builder)
    end

    should_present({ 'chat_id' => 'moo', 'visitor_id' => 'cow', 'history' => [new_chat_message('hello')], 'webpath' => [new_web_path('http://example.org')] }, as: :value)
  end
end
