require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Tickets::AuditEventPresenter do
  extend Api::Presentation::TestHelper

  let(:include_ticket_id) { false }
  let(:included) { [] }
  let(:model) { events(:serialization_comment) }

  fixtures :all

  before do
    @account   = accounts(:minimum)
    @ticket    = tickets(:minimum_1)
    @model     = model
    @presenter = Api::V2::Tickets::AuditEventPresenter.new(@account.owner,
      url_builder: mock_url_builder,
      include_ticket_id: include_ticket_id,
      includes: included)
  end

  should_present_keys :id, :type

  should_present_method :id
  should_present_method :type

  it "presents a via object if present" do
    via = stub
    @presenter.expects(:serialized_via_object).with(model).returns(via)
    assert_equal via, @presenter.model_json(model)[:via]
  end

  describe "with include_ticket_id => true" do
    let(:include_ticket_id) { true }

    should_present_keys :id, :type, :ticket_id

    it "includes ticket nice_id" do
      assert_equal @ticket.nice_id, @presenter.model_json(model)[:ticket_id]
    end
  end

  describe "collection_presenter" do
    let(:included) { [:test] }
    it "uses AuditEventCollectionPresenter as it's collection_presenter" do
      assert_instance_of Api::V2::Tickets::AuditEventCollectionPresenter, @presenter.collection_presenter
    end

    it "pass includes to the collection presenter" do
      assert_equal [:test], @presenter.collection_presenter.includes
    end
  end

  describe "with CommentRedactionEvent" do
    let(:model) do
      CommentRedactionEvent.create!(audit: events(:serialization_audit), comment_id: 1)
    end

    should_present_keys :id, :type, :comment_id

    should_present_method :id
    should_present_method :type
    should_present_method :comment_id
  end

  describe "with TicketFieldRedactEvent" do
    let(:model) do
      ticket_field_entries(:field_text_custom_entry).redaction_events.create(audit: events(:serialization_audit))
    end

    should_present_keys :id, :type, :ticket_field_id

    should_present_method :id
    should_present_method :type
  end

  describe "with TicketFieldRedactEvent pointing at deleted ticket field entry" do
    let(:model) do
      tfe = ticket_field_entries(:field_text_custom_entry).redaction_events.create(audit: events(:serialization_audit))
      tfe.ticket_field_entry = nil
      tfe
    end

    should_present_keys :id, :type, :ticket_field_id

    should_present_method :id
    should_present_method :type
  end

  describe "with AttachmentRedactionEvent" do
    let(:model) do
      AttachmentRedactionEvent.create!(audit: events(:create_audit_for_minimum_ticket_1), attachment_id: attachments(:serialization_comment_attachment).id)
    end

    should_present_keys :id, :type, :attachment_id, :comment_id

    should_present_method :id
    should_present_method :type
    should_present_method :attachment_id
    should_present_method :comment_id
  end

  describe "with ScheduleAssignment" do
    let(:model) do
      ScheduleAssignment.create!(
        audit: events(:create_audit_for_minimum_ticket_1),
        value_previous: 1,
        value: 2
      )
    end

    should_present_keys :id, :type, :previous_schedule_id, :new_schedule_id

    should_present_method :id
    should_present_method :type
    should_present_method :previous_schedule_id
    should_present_method :new_schedule_id
  end

  describe "with ChannelBackFailedEvent" do
    let(:model) do
      ChannelBackFailedEvent.create!(
        audit: events(:create_audit_for_minimum_ticket_1),
        value: { description: 'test desc '}
      )
    end

    should_present_keys :id, :type, :description

    should_present_method :id
    should_present_method :type
  end

  describe "with arturo flag for plain_body_on_all_comment_types" do
    before do
      @agent = users(:minimum_agent)
      @model = FacebookComment.create(
        body: "hang tight bro", is_public: true, channel_back: "1",
        ticket: @ticket, author: @agent, via_id: 0,
        via_reference_id: 3, account: @account,
        value_reference: {via_zendesk: true}
      )
    end

    describe "when arturo flag true" do
      before do
        Arturo.enable_feature!(:plain_body_on_all_comment_types)
      end

      should_present_keys :id, :plain_body, :type, :via
    end

    describe "when arturo flag false" do
      before do
        Arturo.disable_feature!(:plain_body_on_all_comment_types)
      end

      should_present_keys :id, :type, :via
    end
  end

  describe "with AssociateAttValsEvent" do
    let(:model) do
      AssociateAttValsEvent.create!(
        audit: events(:create_audit_for_minimum_ticket_1),
        attribute_values: [{id: '1', name: '2'}]
      )
    end

    should_present_keys :id, :type, :attribute_values

    should_present_method :id
    should_present_method :type
    should_present_method :attribute_values
  end
end
