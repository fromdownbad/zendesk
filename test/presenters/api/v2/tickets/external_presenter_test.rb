require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::ExternalPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    External.any_instance.expects(:enqueue_target) # do not enqueue any jobs
    @model = External.create!(
      account: @account,
      ticket: @ticket,
      audit: @ticket.audits.last,
      author: @account.owner,
      value: 'value',
      value_previous: 'value_previous',
      value_reference: 'value_reference'
    )

    @presenter = Api::V2::Tickets::ExternalPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :resource, :body
  should_present_method :id
  should_present_method :value, as: :resource
  should_present_method :value_reference, as: :body
  should_present "External", as: :type
end
