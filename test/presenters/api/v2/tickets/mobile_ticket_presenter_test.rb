require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Tickets::MobileTicketPresenter do
  fixtures :accounts

  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)
  end

  describe "initialize with generic comment presenter" do
    let(:presenter) do
      Api::V2::Tickets::MobileTicketPresenter.new(@account.owner, url_builder: mock_url_builder)
    end

    describe "sets comment presenter on call" do
      it "should set default comment presenter to GenericCommentPresenter" do
        assert_instance_of Api::V2::Tickets::GenericCommentPresenter, presenter.send(:comment_presenter)
      end
    end
  end
end
