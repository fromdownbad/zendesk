require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::MessagingCsatEventPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @trigger = @account.triggers.active.first

    @model = MessagingCsatEvent.new.tap do |event|
      event.value = { chat_started_id: 1 }
      event.audit = @ticket.audits.last
      event.via_id = Zendesk::Types::ViaType.Rule
      event.via_reference_id = @trigger.id
    end

    @presenter = Api::V2::Tickets::MessagingCsatEventPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :via

  should_present_method :id
  should_present "MessagingCSATEvent", as: :type

  it "presents via object" do
    json = @presenter.model_json(@model)
    assert_equal({
      channel: :rule,
      source: {
        from: {
          id: @trigger.id,
          title: @trigger.title,
          deleted: false
        },
        to: {},
        rel: 'trigger'
      }
    }, json[:via])
  end
end
