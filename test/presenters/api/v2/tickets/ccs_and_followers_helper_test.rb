require_relative '../../../../support/collaboration_settings_test_helper'
require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Tickets::CcsAndFollowersHelper do
  include Api::V2::Tickets::CcsAndFollowersHelper
  include ArturoTestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }

  describe "#audit_recipients_ids" do
    let(:non_ticket_objects) { [] }
    let(:ticket_objects) { [] }

    subject { audit_recipients_ids(account, non_ticket_objects, ticket_objects) }

    describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
      it { subject.must_equal [] }
    end

    describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
        it { subject.must_equal [] }
      end

      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        describe "when non_tickets and tickets are both empty" do
          it { subject.must_equal [] }
        end

        describe "when non_tickets contains an Event that's not an Audit" do
          let(:ticket) { tickets(:minimum_1) }
          let(:non_ticket_objects) { [ticket.comments.first] }

          it { subject.must_equal [] }
        end

        describe "when non_tickets contains an Audit" do
          let(:ticket) { tickets(:minimum_1) }
          let(:to_recipient) { users(:minimum_end_user) }
          let(:cc_recipients) { [users(:minimum_search_user)] }
          let(:notification_with_ccs) { NotificationWithCcs.new(to_recipient_ids: [to_recipient.id], ccs: cc_recipients) }
          let(:non_ticket_objects) { [audit] }

          let(:audit) do
            Audit.new do |audit|
              audit.account = account
              audit.ticket = ticket
              audit.author = ticket.requester
              audit.via_id = ViaType.WEB_FORM
            end
          end

          describe "and it does not have a NotificationWithCcs event" do
            it { subject.must_equal [] }
          end

          describe "and it has a NotificationWithCcs event" do
            before do
              audit.events << notification_with_ccs
              audit.save!
            end

            it { subject.sort.must_equal [to_recipient.id, cc_recipients.map(&:id)].flatten.sort }
          end
        end

        describe "when tickets contains a Ticket" do
          let(:ticket) { tickets(:minimum_1) }
          let(:ticket_objects) { [ticket] }

          describe "and it does not have any audits with NotificationWithCcs events" do
            it { subject.must_equal [] }
          end

          describe "and it has an audit with a NotificationWithCcs event" do
            let(:to_recipient) { users(:minimum_end_user) }
            let(:cc_recipients) { [users(:minimum_search_user)] }
            let(:notification_with_ccs) { NotificationWithCcs.new(to_recipient_ids: [to_recipient.id], ccs: cc_recipients) }
            let(:audit) do
              Audit.new do |audit|
                audit.account = account
                audit.ticket = ticket
                audit.author = ticket.requester
                audit.via_id = ViaType.WEB_FORM
              end
            end

            before do
              audit.events << notification_with_ccs
              audit.save!
            end

            it { subject.sort.must_equal [to_recipient.id, cc_recipients.map(&:id)].flatten.sort }
          end
        end
      end
    end
  end

  describe "#audit_recipients_users" do
    let(:non_ticket_objects) { [] }
    let(:ticket_objects) { [] }
    let(:audit_recipient_ids_results) { [] }

    subject { audit_recipients_users(account, non_ticket_objects, ticket_objects) }

    before do
      stubs(audit_recipients_ids: audit_recipient_ids_results).with(account, non_ticket_objects, ticket_objects)
    end

    describe "when audit_recipients_ids returns an empty array" do
      it { subject.must_be_empty }
    end

    describe "when audit_recipients_ids returns an array of user IDs" do
      let(:recipient_1) { users(:minimum_end_user) }
      let(:recipient_2) { users(:minimum_search_user) }
      let(:audit_recipient_ids_results) { [recipient_1.id, recipient_2.id] }

      it { subject.must_equal [recipient_1, recipient_2].index_by(&:id) }

      describe "when one of the user IDs belongs to a soft-deleted user" do
        before do
          recipient_2.current_user = users(:minimum_admin)
          recipient_2.delete!
        end

        it { subject.must_equal [recipient_1, recipient_2].index_by(&:id) }
      end
    end
  end

  describe "#user_ids_or_addresses" do
    let(:user) { users(:minimum_agent) }
    let(:user_objects) { [] }

    subject { user_ids_or_addresses(user_objects) }

    describe "when given array of users is empty" do
      it { subject.must_be_empty }
    end

    describe "when given array of users contains an active user" do
      let(:user_objects) { [user] }

      it { subject.must_equal [user.id] }
    end

    describe "when given array of users contains an inactive user" do
      let(:user_objects) { [user] }

      before do
        user.current_user = user
        user.delete!
      end

      it { subject.must_equal [user.name] }
    end

    describe "when given array of users contains an active user and inactive user" do
      let(:user_2) { users(:minimum_admin) }
      let(:user_objects) { [user, user_2] }

      before do
        user_2.current_user = user_2
        user_2.delete!
      end

      it { subject.must_equal [user.id, user_2.name] }
    end
  end

  describe "#recipients_ids_or_identifiers" do
    let(:ticket) { tickets(:minimum_1) }
    let(:audit) { ticket.audits.first }
    let(:user_objects) { nil }

    subject { recipients_ids_or_identifiers(account, audit, user_objects) }

    describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
      it "calls recipients_ids_or_identifiers on the supplied audit" do
        audit.expects(:recipients_ids_or_identifiers)
        subject
      end
    end

    describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      let(:recipients) { "minimum_end_user@aghassipour.com minimum_search_user@aghassipour.com" }
      let(:via_id) { ViaType.MAIL }
      let(:audit) { Audit.new { |a| a.account = account; a.ticket = ticket; a.recipients = recipients; a.via_id = via_id } }
      let(:minimum_end_user_id) { users(:minimum_end_user).id }

      it "does not call recipients_ids_or_identifiers on the supplied audit" do
        audit.expects(:recipients_ids_or_identifiers).never
        subject
      end

      describe "when via type is MAIL" do
        describe "when all recipients are active Users" do
          it "returns an Array of the User IDs" do
            assert_equal 2, subject.length
            subject.must_include minimum_end_user_id
            subject.must_include users(:minimum_search_user).id
          end
        end

        describe "when one recipient is the author of the event" do
          before do
            audit.author = users(:minimum_end_user)
            audit.save!
          end

          it "does not include the author's ID" do
            assert_equal 1, subject.length
            subject.wont_include minimum_end_user_id
            subject.must_include users(:minimum_search_user).id
          end
        end

        describe "when one recipient is an inactive User" do
          let(:recipients) { "minimum_end_user@aghassipour.com inactive_user@zendesk.com" }

          it "returns an Array of the active User IDs and inactive email addresses" do
            assert_equal 2, subject.length
            subject.must_include minimum_end_user_id
            subject.must_include "inactive_user@zendesk.com"
          end
        end

        describe "when one recipient is a support address for the account" do
          let(:recipients) { "minimum_end_user@aghassipour.com not_default@example.net" }

          it "does not include the support address" do
            assert_equal 1, subject.length
            subject.must_equal [minimum_end_user_id]
          end
        end
      end

      [ViaType.WEB_FORM, ViaType.WEB_SERVICE, ViaType.CLOSED_TICKET].each do |via_type|
        describe "when via type is #{via_type}" do
          let(:via_id) { via_type }

          let(:recipients_notification_cc_users) { [] }

          before do
            Audit.any_instance.stubs(recipients_notification_ccs: recipients_notification_cc_users.map(&:id))
          end

          it "does not use the recipients_list" do
            Audit.any_instance.expects(:recipients_list).never
            subject
          end

          describe "when there are no notification CCs" do
            it { subject.must_be_empty }
          end

          describe "when there are notification CCs" do
            let(:ccd_user) { users(:minimum_search_user) }
            let(:recipients_notification_cc_users) { [ccd_user] }

            describe "when supplied users map is empty" do
              it "queries the database for the users" do
                # queries are account, ticket, users
                assert_sql_queries 3 do
                  subject
                end
              end

              it "includes the CC'd user's ID" do
                subject.must_include ccd_user.id
              end

              it "does not log" do
                Rails.logger.expects(:info).never
                subject
              end

              describe "when the CC'd user has been soft deleted" do
                before do
                  ccd_user.current_user = users(:minimum_admin)
                  ccd_user.delete!
                end

                it "includes the CC'd user's name and not the ID" do
                  subject.must_include ccd_user.name
                end
              end
            end

            describe "when the supplied users map contains the CCd users" do
              let(:user_objects) { recipients_notification_cc_users.index_by(&:id) }

              it "does not query the database for the users" do
                # queries are account and ticket
                assert_sql_queries 2 do
                  subject
                end
              end

              it "includes the CC'd user's ID" do
                subject.must_include ccd_user.id
              end

              it "does not log" do
                Rails.logger.expects(:info).never
                subject
              end

              describe "when the CC'd user has been soft deleted" do
                before do
                  ccd_user.current_user = users(:minimum_admin)
                  ccd_user.delete!
                end

                it "includes the CC'd user's name and not the ID" do
                  subject.must_include ccd_user.name
                end
              end
            end

            describe "when the supplied users map contains users but not the CC'd ones" do
              # let! to avoid the query on this "other" user
              let!(:user_objects) { [users(:minimum_author)].index_by(&:id) }

              it "does not query the database for the users" do
                # queries are account and ticket
                assert_sql_queries 2 do
                  subject
                end
              end

              it "does not include the CC'd user's ID" do
                subject.wont_include ccd_user.id
              end

              it "logs about the error condition" do
                Rails.logger.expects(:info).with("recipients_ids_or_identifiers: nil user found for account #{account.subdomain} and audit #{audit.id}")
                subject
              end
            end
          end
        end
      end

      describe "when via type is FACEBOOK_POST" do
        let(:via_id) { ViaType.FACEBOOK_POST }

        it { subject.must_be_empty }
      end
    end
  end
end
