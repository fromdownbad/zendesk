require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Tickets::TicketPermissionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    @account = accounts(:minimum)
    @model = tickets(:minimum_2)
    ticket_metric_sets(:minimum_set_2)
    @presenter = Api::V2::Tickets::TicketPermissionsPresenter.new(@account.owner, url_builder: mock_url_builder)

    @presenter.user.stubs(:can?).returns(true)
  end

  should_present_keys :can_update_ticket, :can_edit_ticket_properties, :can_delete_ticket, :can_merge_ticket,
    :can_edit_ticket_tags, :can_make_comments, :can_make_public_comments, :can_mark_as_spam, :can_create_followup_ticket,
    :can_read_problem, :can_view_ticket_satisfaction_prediction

  describe 'sideloads extended_permissions' do
    let(:options) do
      {
        url_builder: mock_url_builder,
        includes: [:extended_permissions]
      }
    end
    let(:apresenter) { Api::V2::Tickets::TicketPermissionsPresenter.new(@account.owner, options) }

    it 'renders extended permissions' do
      assert apresenter.model_json(@model)[:can_view_ticket]
    end
  end

  describe 'when ticket can be updated and' do
    before do
      @model.stubs(:can_be_updated?).returns(true)
    end

    describe 'current user can edit properties' do
      before do
        @presenter.user.stubs(:can?).with(:edit_properties, @model).returns(true)
      end

      it 'is true' do
        assert @presenter.model_json(@model)[:can_update_ticket]
        assert @presenter.model_json(@model)[:can_edit_ticket_properties]
      end
    end

    describe 'current user can not edit properties' do
      before do
        @presenter.user.stubs(:can?).with(:edit_properties, @model).returns(false)
      end

      it 'is true' do
        refute @presenter.model_json(@model)[:can_update_ticket]
        refute @presenter.model_json(@model)[:can_edit_ticket_properties]
      end
    end
  end

  describe "can_read_problem permission" do
    describe "when the ticket has a problem" do
      before do
        @problem = tickets(:minimum_1)
        @model.stubs(problem: @problem)
      end

      describe "when the user can view the problem ticket" do
        before do
          @presenter.user.stubs(:can?).with(:view, @problem).returns(true)
        end

        it "is true" do
          assert @presenter.model_json(@model)[:can_read_problem]
        end
      end

      describe "when the user cannot view the ticket" do
        before do
          @presenter.user.stubs(:can?).with(:view, @problem).returns(false)
        end

        it "is false" do
          refute @presenter.model_json(@model)[:can_read_problem]
        end
      end
    end

    describe "when the ticket does not have a problem" do
      before do
        assert_nil @model.problem
      end

      it "is nil" do
        assert_nil @presenter.model_json(@model)[:can_read_problem]
      end
    end
  end

  describe "can_view_ticket_satisfaction_prediction permission" do
    describe "when the user can view ticket satisfaction prediction" do
      before do
        @presenter.user.stubs(:can?).with(:view_satisfaction_prediction, Ticket).returns(true)
      end

      it "is true" do
        assert @presenter.model_json(@model)[:can_view_ticket_satisfaction_prediction]
      end
    end

    describe "when the user cannot view ticket satisfaction prediction" do
      before do
        @presenter.user.stubs(:can?).with(:view_satisfaction_prediction, Ticket).returns(false)
      end

      it "is false" do
        refute @presenter.model_json(@model)[:can_view_ticket_satisfaction_prediction]
      end
    end
  end

  describe 'when ticket can not be updated and' do
    before do
      @model.stubs(:can_be_updated?).returns(false)
    end

    describe 'current user can edit properties' do
      before do
        @presenter.user.stubs(:can?).with(:edit_properties, @model).returns(true)
      end

      it 'is true' do
        refute @presenter.model_json(@model)[:can_update_ticket]
        refute @presenter.model_json(@model)[:can_edit_ticket_properties]
      end
    end

    describe 'current user can not edit properties' do
      before do
        @presenter.user.stubs(:can?).with(:edit_properties, @model).returns(false)
      end

      it 'is true' do
        refute @presenter.model_json(@model)[:can_update_ticket]
        refute @presenter.model_json(@model)[:can_edit_ticket_properties]
      end
    end
  end
end
