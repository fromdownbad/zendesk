require_relative "../../../../support/test_helper"
require_relative "../../../../support/brand_test_helper"

SingleCov.covered!

describe Api::V2::Tickets::AuditPresenter do
  include BrandTestHelper
  include Api::V2::Tickets::EventViaPresenter
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @model = events(:serialization_audit)
    @url_builder = mock_url_builder
    @presenter = Api::V2::Tickets::AuditPresenter.new(@model.account.owner, url_builder: @url_builder)

    @url_builder.stubs(:zendesk_business_hours).returns(mock_url_builder)
  end

  should_present_keys :id, :ticket_id, :created_at, :author_id, :via, :metadata, :events

  should_present_method :id
  should_present_method :created_at
  should_present_method :author_id
  should_present_method :metadata
  should_present_collection :events, with: Api::V2::Tickets::AuditEventPresenter

  it "presents via object" do
    json = @presenter.model_json(@model)
    assert_equal({ channel: :web, source: { from: {}, to: {}, rel: nil } }, json[:via])
  end

  it "presents the ticket's nice_id as ticket_id" do
    assert_equal @model.ticket.nice_id, @presenter.model_json(@model)[:ticket_id]
  end

  should_side_load :users, :groups, :organizations, :tickets, :ticket_forms, :sharing_agreements, :brands, :schedules, :satisfaction_reasons

  describe "Presenting mixed collection data" do
    before do
      @audit = events(:create_audit_for_minimum_ticket_1)
      @presenter = Api::V2::Tickets::AuditPresenter.new(@audit.account.owner, url_builder: @url_builder)
      @rule1 = @audit.account.triggers[0]
      @rule2 = @audit.account.triggers[1]
      External.any_instance.stubs(:via_reference_id).returns(@rule1.id)
      Comment.any_instance.stubs(:via_reference_id).returns(@rule2.id)
      Comment.any_instance.stubs(:via_id).returns(ViaType.RULE)
    end

    it "presents all rule sources for mixed classes" do
      @json = @presenter.present(@audit)
      external_events = @json[:audit][:events].select { |e| e[:type].casecmp("external").zero? }
      assert external_events.any?
      expected_via = {channel: :rule, source: {to: {}, from: {id: @rule1.id, title: @rule1.title, deleted: false}, rel: @rule1.type.downcase}}
      external_events.each do |e|
        assert_equal expected_via, e[:via]
      end

      comment_events = @json[:audit][:events].select { |e| e[:type].casecmp("comment").zero? }
      assert comment_events.any?
      expected_via = {channel: :rule, source: {to: {}, from: {id: @rule2.id, title: @rule2.title, deleted: false}, rel: @rule2.type.downcase}}
      comment_events.each do |e|
        assert_equal expected_via, e[:via]
      end
    end

    it 'doesnt produce N+1' do
      assert_no_n_plus_one do
        @presenter.present(@audit)
      end
    end

    it "presents event decoration if present" do
      @json = @presenter.present(@audit)
      assert_equal @audit.comment.event_decoration.data.as_json, @json[:audit][:metadata]["decoration"]
    end
  end

  describe "events filtering" do
    before do
      @audit = events(:create_audit_for_minimum_ticket_1)
      Comment.create!(audit: @audit, author_id: users(:serialization_user).id)
      Comment.create!(audit: @audit, author_id: users(:serialization_user).id)
    end

    it "presents all the events when no 'filter_event_types' is passed" do
      presenter = Api::V2::Tickets::AuditPresenter.new(@audit.account.owner, url_builder: @url_builder)
      assert_equal @audit.events.count(:all), presenter.present(@audit)[:audit][:events].size
    end

    it "presents only the events with the type specified in 'filter_event_types'" do
      presenter = Api::V2::Tickets::AuditPresenter.new(@audit.account.owner, url_builder: @url_builder, filter_event_types: ["Comment"])
      events_json = presenter.present(@audit)[:audit][:events]

      assert_equal @audit.events.where(type: "Comment").count(:all), events_json.size
      assert_equal "Comment", events_json.map { |e| e[:type] }.uniq.first
    end

    it "presents no events when a non-existent 'filter_event_types' is passed" do
      presenter = Api::V2::Tickets::AuditPresenter.new(@audit.account.owner, url_builder: @url_builder, filter_event_types: ["Foo"])
      assert_equal presenter.present(@audit)[:audit][:events].size, 0
    end
  end

  describe "#association_preloads" do
    let(:preloads) do
      {
        events: {
          audit: {},
          event_decoration: {}
        },
        ticket: {}
      }
    end

    describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
      it "includes preloads" do
        assert_equal preloads, @presenter.association_preloads
      end
    end

    describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      before do
        preloads.merge!(notifications_with_ccs: {})
      end

      it "includes preloads" do
        assert_equal preloads, @presenter.association_preloads
      end
    end
  end

  describe "side-loading" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @model.events.clear
    end

    describe "users" do
      before do
        @presenter.stubs(:side_load?).with(:users).returns(true)
        @model.author = nil
        @user = users(:serialization_user)
        @presented_user = @presenter.user_presenter.model_json(@user)
      end

      describe "when audit author_id is different than comment author_id" do
        before do
          @model.author = users(:serialization_agent)
          comment = Comment.new(audit: @model, author_id: users(:serialization_user).id, account: @model.account)
          @model.stubs(events: [comment])
          assert_not_equal comment.author_id, @model.author_id
        end

        it "side-loads the comment author_id" do
          assert_includes @presenter.present(@model)[:users], @presenter.user_presenter.model_json(users(:serialization_user))
        end
      end

      it "includes the audit author" do
        @model.author = @user
        assert_equal [@presented_user], @presenter.present(@model)[:users]
      end

      ['requester', 'submitter', 'assignee'].each do |association|
        it "includes #{association.pluralize}" do
          @model.events << Create.new(field_name: "#{association}_id", value: @user.id, author: users(:minimum_end_user))
          assert_equal [@presented_user], @presenter.present(@model)[:users]

          @model.events.clear
          other_user = users(:serialization_admin)
          other_presented_user = @presenter.user_presenter.model_json(other_user)
          @model.events << Change.new(field_name: "#{association}_id", value: @user.id, value_previous: other_user.id, author: users(:minimum_end_user))
          assert_same_elements [@presented_user, other_presented_user], @presenter.present(@model)[:users]
        end
      end

      [Notification, Cc, Tweet].each do |klass|
        it "includes #{klass.name} recipients" do
          @model.events << klass.new(recipients: [@user.id], author: users(:serialization_user))
          assert_equal [@presented_user], @presenter.present(@model)[:users]
        end
      end

      it "includes phone answerers" do
        @model.events << VoiceComment.new(account: @model.account, data: {answered_by_id: @user.id}, author: users(:minimum_end_user), ticket: tickets(:serialization_ticket))
        assert_equal [@presented_user], @presenter.present(@model)[:users]
      end

      describe "email ccs" do
        let(:recipients) { "minimum_end_user@aghassipour.com bar@example.com" }
        let(:account) { accounts(:minimum) }
        let(:user) { users(:minimum_agent) }

        def save_ticket
          ticket = Ticket.new(account: account, description: "testing recipient sideloads")
          ticket.stubs(:email_cc_addresses).returns(recipients.split(" "))
          ticket.will_be_saved_by(user, via_id: ViaType.WEB_SERVICE)
          ticket.save!

          @audit = ticket.audits.last
          @presenter = Api::V2::Tickets::AuditPresenter.new(@audit.account.owner, url_builder: @url_builder)
          @presenter.stubs(:side_load?).returns(false)
          @presenter.stubs(:side_load?).with(:users).returns(true)
        end

        before do
          Audit.any_instance.stubs(active_recipients_ids: [users(:minimum_end_user).id])
        end

        describe "when comment_email_ccs_allowed setting is enabled" do
          before do
            Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
          end

          it "side loads recipients" do
            save_ticket
            sideloaded_users = @presenter.present(@audit)[:users]
            assert_equal 3, sideloaded_users.size
            assert_includes sideloaded_users, @presenter.user_presenter.model_json(users(:minimum_end_user))
          end
        end

        describe "when comment_email_ccs_allowed setting is disabled" do
          before do
            Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
          end

          it "does not side load recipients" do
            save_ticket
            sideloaded_users = @presenter.present(@audit)[:users]
            assert_equal 2, sideloaded_users.size
            refute_includes sideloaded_users, @presenter.user_presenter.model_json(users(:minimum_end_user))
          end
        end
      end
    end

    describe "organizations" do
      before do
        @presenter.stubs(:side_load?).with(:organizations).returns(true)
        @organization = organizations(:serialization_organization)
        @presented_organization = @presenter.organization_presenter.model_json(@organization)
      end

      it "includes organizations from Create events" do
        @model.events << Create.new(field_name: "organization_id", value: @organization.id)
        assert_equal [@presented_organization], @presenter.present(@model)[:organizations]
      end

      it "includes organizations from Change events" do
        other_organization = @model.account.organizations.create!(name: 'other org').reload
        other_presented_organization = @presenter.organization_presenter.model_json(other_organization)

        @model.events << Change.new(field_name: "organization_id", value: @organization.id, value_previous: other_organization.id)
        assert_same_elements [@presented_organization, other_presented_organization], @presenter.present(@model)[:organizations]
      end
    end

    describe "groups" do
      before do
        @presenter.stubs(:side_load?).with(:groups).returns(true)
        @group = groups(:serialization_group)
        @presented_group = @presenter.group_presenter.model_json(@group)
      end

      it "includes groups from Create events" do
        @model.events << Create.new(field_name: "group_id", value: @group.id)
        assert_equal [@presented_group], @presenter.present(@model)[:groups]
      end

      it "includes groups from Change events" do
        other_group = @model.account.groups.create!(name: 'other org').reload
        other_presented_group = @presenter.group_presenter.model_json(other_group)

        @model.events << Change.new(field_name: "group_id", value: @group.id, value_previous: other_group.id)
        assert_same_elements [@presented_group, other_presented_group], @presenter.present(@model)[:groups]
      end
    end

    describe "tickets" do
      before do
        @presenter.stubs(:side_load?).with(:tickets).returns(true)
        @ticket = tickets(:serialization_ticket)
        @presented_ticket = @presenter.ticket_presenter.model_json(@ticket)
        # Create an unrelated ticket so we know they don't get selected
        FactoryBot.create(:ticket, account: @ticket.account, via_id: @ticket.via_id, requester: @ticket.requester)
      end

      it "includes tickets" do
        assert_equal [@presented_ticket], @presenter.present(@model)[:tickets]
      end

      it "includes linked problems from Create events" do
        @model.events << Create.new(field_name: "linked_id", value: @ticket.id)
        assert_equal [@presented_ticket], @presenter.present(@model)[:tickets]
      end

      it "includes linked problems from Change events" do
        other_ticket = @model.account.tickets.new(
          requester: users(:serialization_admin),
          submitter: users(:serialization_admin)
        )
        other_ticket.will_be_saved_by(users(:serialization_admin))
        other_ticket.save(validate: false)
        other_ticket.reload
        other_presented_ticket = @presenter.ticket_presenter.model_json(other_ticket)

        @model.events << Change.new(field_name: "linked_id", value: @ticket.id, value_previous: other_ticket.id)
        assert_same_elements [@presented_ticket, other_presented_ticket], @presenter.present(@model)[:tickets]
      end

      it "grabs tickets from the archive when needed" do
        archive_and_delete(@ticket)
        # refetch ticket from archive so that audit is setup properly
        @ticket = @ticket.account.tickets.find_by_nice_id(@ticket.nice_id)
        assert_equal 1, @presenter.present(@ticket.audits.first)[:tickets].size
      end

      it "loads an empty set when no models are found" do
        @presenter.stubs(:side_load?).with(:groups).returns(true)
        @presenter.stubs(:group_ids).returns([-999])
        assert_empty @presenter.present(@ticket.audits.first)[:groups]
      end
    end

    describe "ticket forms" do
      before do
        @presenter.stubs(:side_load?).with(:ticket_forms).returns(true)
        @ticket_form = Zendesk::TicketForms::Initializer.new(@model.account, ticket_form: {name: "Default ticket form", display_name: "wombat", active: true, default: true, end_user_visible: true, position: 1}).ticket_form
        @ticket_form.save!
        @ticket_form.reload
        @presented_ticket_form = @presenter.ticket_form_presenter.model_json(@ticket_form)
      end

      it "includes ticket forms from Create events" do
        @model.events << Create.new(field_name: "ticket_form_id", value: @ticket_form.id)
        assert_equal [@presented_ticket_form], @presenter.present(@model)[:ticket_forms]
      end

      it "includes ticket forms from Change events" do
        other_form = Zendesk::TicketForms::Initializer.new(@model.account, ticket_form: {name: "Wombat ticket form", display_name: "stuff", active: true, default: false, end_user_visible: true, position: 2}).ticket_form
        other_form.save!
        other_form.reload
        other_presented_form = @presenter.ticket_form_presenter.model_json(other_form)

        @model.events << Change.new(field_name: "ticket_form_id", value: @ticket_form.id, value_previous: other_form.id)
        assert_same_elements [@presented_ticket_form, other_presented_form], @presenter.present(@model)[:ticket_forms]
      end
    end

    describe "sharing agreements" do
      before do
        @presenter.stubs(:side_load?).with(:sharing_agreements).returns(true)
        @account = accounts(:minimum)
        @ticket = tickets(:serialization_ticket)
        @sharing_agreement = FactoryBot.create(:agreement, account: @model.account, name: 'foo @ Zendesk', created_at: "May 16th")

        @presented_sharing_agreement = @presenter.sharing_agreement_presenter.model_json(@sharing_agreement)
      end

      it "includes sharing agreements from ticket sharing and unshare events" do
        @model.events << TicketSharingEvent.new(account: @account,
                                                ticket: @ticket,
                                                audit: @ticket.audits.last,
                                                author: @account.owner,
                                                agreement_id: @sharing_agreement.id)
        assert_equal [@presented_sharing_agreement], @presenter.present(@model)[:sharing_agreements]
      end
    end

    describe "brands" do
      before do
        @presenter.stubs(:side_load?).with(:brands).returns(true)
        create_brand(@model.account, name: "Wombat Brand", subdomain: "wombat", active: true)
        @brand = @model.account.brands(true).find_by_name("Wombat Brand")
      end

      let (:presented_brand) { @presenter.brand_presenter.model_json(@brand) }

      describe "for non deleted brands" do
        it "includes brands from Create events" do
          @model.events << Create.new(field_name: "brand_id", value: @brand.id)
          assert_equal [presented_brand], @presenter.present(@model)[:brands]
        end

        it "includes brands from Change events" do
          other_brand = create_brand(@model.account, name: "Giraffe Brand", subdomain: "giraffe").reload
          other_presented_brand = @presenter.brand_presenter.model_json(other_brand)

          @model.events << Change.new(field_name: "brand_id", value: @brand.id, value_previous: other_brand.id)
          assert_same_elements [presented_brand, other_presented_brand], @presenter.present(@model)[:brands]
        end
      end

      describe "for deleted brands" do
        before do
          @brand.soft_delete
          @brand.reload
          @presented_brand = @presenter.brand_presenter.model_json(@brand)
        end
        it "includes brands from Create events" do
          @model.events << Create.new(field_name: "brand_id", value: @brand.id)
          assert_equal [@presented_brand], @presenter.present(@model)[:brands]
        end

        it "includes brands from Change events" do
          other_brand = create_brand(@model.account, name: "Giraffe Brand", subdomain: "giraffe").reload
          other_presented_brand = @presenter.brand_presenter.model_json(other_brand)

          @model.events << Change.new(field_name: "brand_id", value: @brand.id, value_previous: other_brand.id)
          assert_same_elements [@presented_brand, other_presented_brand], @presenter.present(@model)[:brands]
        end
      end
    end

    describe "schedules" do
      let(:schedule_1) do
        @model.account.schedules.create(
          name:      'schedule_1',
          time_zone: @model.account.time_zone
        )
      end
      let(:schedule_2) do
        @model.account.schedules.create(
          name:      'schedule_2',
          time_zone: @model.account.time_zone
        )
      end

      before do
        Account.any_instance.stubs(:has_multiple_schedules?).returns(true)
        @presenter.stubs(:side_load?).with(:schedules).returns(true)

        @model.events << ScheduleAssignment.new(
          value_previous: nil,
          value: schedule_1.id
        )
        @model.events << ScheduleAssignment.new(
          value_previous: schedule_1.id,
          value: schedule_2.id
        )
      end

      it 'side-loads schedules for the related events' do
        assert_equal(
          [schedule_1, schedule_2].map(&:id).to_set,
          @presenter.present(@model)[:schedules].map do |presented_schedule|
            presented_schedule[:id]
          end.to_set
        )
      end

      describe 'when there are deleted schedules for related events' do
        let(:deleted_schedule) do
          @model.account.schedules.create(
            name:      'Deleted schedule',
            time_zone: @model.account.time_zone
          ).tap(&:soft_delete)
        end

        before do
          @model.events << ScheduleAssignment.new(
            value_previous: schedule_2.id,
            value: deleted_schedule.id
          )
        end

        it 'side-loads the deleted schedules' do
          assert_equal(
            [schedule_1, schedule_2, deleted_schedule].map(&:id).to_set,
            @presenter.present(@model)[:schedules].map do |presented_schedule|
              presented_schedule[:id]
            end.to_set
          )
        end
      end
    end

    describe "satisfaction reasons" do
      let(:satisfaction_reason_1) { @model.account.satisfaction_reasons.create(value: "foo") }
      let(:satisfaction_reason_2) { @model.account.satisfaction_reasons.create(value: "bar") }

      before do
        @presenter.stubs(:side_load?).with(:satisfaction_reasons).returns(true)

        @model.events << Create.new(
          field_name: "satisfaction_reason_code",
          value_previous: nil,
          value: satisfaction_reason_1.reason_code
        )
        @model.events << Change.new(
          field_name: "satisfaction_reason_code",
          value_previous: satisfaction_reason_1.reason_code,
          value: satisfaction_reason_2.reason_code
        )
      end

      it "side-loads satisfaction_reasons for the related events" do
        presented_reason_codes = @presenter.present(@model)[:satisfaction_reasons].map { |r| r[:reason_code] }
        events_reason_codes = [satisfaction_reason_1, satisfaction_reason_2].map(&:reason_code)
        assert_equal(presented_reason_codes, events_reason_codes)
      end
    end
  end
end
