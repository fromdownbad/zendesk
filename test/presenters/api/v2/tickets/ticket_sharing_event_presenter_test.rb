require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::TicketSharingEventPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @presenter = Api::V2::Tickets::TicketSharingEventPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  describe "for TicketSharingEvent" do
    before do
      @model = TicketSharingEvent.create!(
        account: @account,
        ticket: @ticket,
        audit: @ticket.audits.last,
        author: @account.owner,
        agreement_id: 1
      )
    end

    should_present_keys :id, :type, :agreement_id, :action

    should_present_method :id
    should_present_method :agreement_id
    should_present "TicketSharingEvent", as: :type
    should_present "shared", as: :action
  end

  describe "for TicketUnshareEvent" do
    before do
      @model = TicketUnshareEvent.create!(
        account: @account,
        ticket: @ticket,
        audit: @ticket.audits.last,
        author: @account.owner,
        agreement_id: 1
      )
    end

    should_present_keys :id, :type, :agreement_id, :action

    should_present_method :id
    should_present_method :agreement_id
    should_present "TicketSharingEvent", as: :type
    should_present "unshared", as: :action
  end
end
