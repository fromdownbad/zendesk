require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Tickets::MacroReferencePresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }
  let(:macro)   { rules(:macro_incident_escalation) }

  let(:reference_class)  { AgentMacroReference }
  let(:preloaded_macros) { {} }

  let!(:reference_value) { macro.id.to_s }

  let(:macro_reference) do
    reference_class.create!(
      account:         account,
      ticket:          ticket,
      audit:           ticket.audits.last,
      author:          account.owner,
      value:           reference_value,
      value_previous:  'value_previous',
      value_reference: 'value_reference'
    )
  end

  let(:presenter) do
    Api::V2::Tickets::MacroReferencePresenter.new(
      account.owner,
      url_builder: mock_url_builder,
      event_via_associations: {macros: preloaded_macros}
    )
  end

  before do
    @model     = macro_reference
    @presenter = presenter
  end

  describe '#model_json' do
    describe "when presenting a macro reference" do
      let(:reference_class) { MacroReference }

      should_present_keys :id,
        :type,
        :macro_id,
        :macro_title,
        :macro_deleted

      should_present_method :id
      should_present 'MacroReference', as: :type

      it 'presents the macro title' do
        assert_equal(
          macro.title,
          presenter.present(@model)[:macro_reference][:macro_title]
        )
      end

      it 'presents whether the macro is deleted' do
        assert_equal(
          macro.deleted?,
          presenter.present(@model)[:macro_reference][:macro_deleted]
        )
      end
    end

    describe 'when presenting an agent macro reference' do
      let(:reference_class) { AgentMacroReference }

      should_present_keys :id,
        :type,
        :macro_id,
        :macro_title,
        :macro_deleted

      should_present_method :id
      should_present 'AgentMacroReference', as: :type

      it 'presents the macro title' do
        assert_equal(
          macro.title,
          presenter.present(@model)[:macro_reference][:macro_title]
        )
      end

      it 'presents whether the macro is deleted' do
        assert_equal(
          macro.deleted?,
          presenter.present(@model)[:macro_reference][:macro_deleted]
        )
      end
    end

    describe 'when macro is preloaded' do
      let(:preloaded_macros) { { macro.id => macro } }

      it 'does not load it from DB' do
        assert_sql_queries 0, /rules/ do
          presenter.present(@model)
        end
      end
    end

    describe 'when macro is not preloaded' do
      it 'loads it from DB' do
        assert_sql_queries 1, /rules/ do
          presenter.present(@model)
        end
      end
    end
  end
end
