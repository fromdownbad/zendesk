require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::FacebookEventPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    @account = accounts(:minimum)
    @page = facebook_pages(:minimum_facebook_page_1)
    @ticket = tickets(:minimum_1)
    @presenter = Api::V2::Tickets::FacebookEventPresenter.new(@account.owner, url_builder: mock_url_builder)
    @model = FacebookEvent.create!(
      account: @account,
      ticket: @ticket,
      audit: @ticket.audits.last,
      author: @account.owner,
      value: 'value',
      graph_object_id: 'graph_object_id'
    )
  end

  should_present_keys :id, :type, :ticket_via, :body

  should_present_method :id
  should_present "FacebookEvent", as: :type
  should_present "value", as: :body

  describe "for wall posts" do
    before do
      Ticket.any_instance.stubs(:via_id).returns(ViaType.FACEBOOK_POST)
    end

    should_present "post", as: :ticket_via
  end

  describe "for private messages" do
    before do
      Ticket.any_instance.stubs(:via_id).returns(ViaType.FACEBOOK_MESSAGE)
    end

    should_present "message", as: :ticket_via
  end
end
