require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 7

describe Api::V2::Tickets::AttributeMappings do
  fixtures :accounts,
    :cf_fields,
    :ticket_fields

  let(:account) { accounts(:minimum) }

  describe '.ticket_attribute_name' do
    describe 'when the attribute is `via_id`' do
      let(:attribute) { 'via_id' }

      it 'returns `via_type`' do
        assert_equal(
          :via_type,
          Api::V2::Tickets::AttributeMappings.ticket_attribute_name(attribute)
        )
      end
    end

    describe 'when it is a ticket field attribute' do
      let(:field)     { cf_fields(:dropdown1) }
      let(:attribute) { "ticket_fields_#{field.id}" }

      it 'returns custom field attribute name' do
        assert_equal(
          "custom_fields_#{field.id}",
          Api::V2::Tickets::AttributeMappings.ticket_attribute_name(attribute)
        )
      end
    end

    describe 'when there is a mapping' do
      let(:attribute) { 'priority_id' }

      it 'returns the mapped attribute name' do
        assert_equal(
          :priority,
          Api::V2::Tickets::AttributeMappings.ticket_attribute_name(attribute)
        )
      end
    end

    describe 'when there is no mapping' do
      let(:attribute) { 'satisfaction_score' }

      it 'returns the attribute name' do
        assert_equal(
          attribute,
          Api::V2::Tickets::AttributeMappings.ticket_attribute_name(attribute)
        )
      end
    end

    describe 'when the attribute is `cc`' do
      let(:attribute) { 'cc' }

      describe 'when an account is provided as an argument' do
        let(:subject) do
          Api::V2::Tickets::AttributeMappings.ticket_attribute_name(
            attribute,
            accounts(:minimum)
          )
        end

        describe_with_arturo_enabled(:email_ccs) do
          describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
            it 'returns `follower`' do
              assert_equal 'follower', subject
            end
          end
        end

        describe_with_arturo_setting_disabled(:follower_and_email_cc_collaborations) do
          it 'returns `cc`' do
            assert_equal 'cc', subject
          end
        end

        describe_with_arturo_setting_disabled(:email_ccs) do
          it 'returns `cc`' do
            assert_equal 'cc', subject
          end
        end
      end

      describe 'when an account is not provided as an argument' do
        let(:subject) do
          Api::V2::Tickets::AttributeMappings.ticket_attribute_name(attribute)
        end

        it 'returns `cc`' do
          assert_equal 'cc', subject
        end
      end
    end
  end

  describe '.ticket_attribute_value' do
    describe "when there are mappings for the attribute's values" do
      let(:attribute) { 'priority_id' }

      it 'returns the mapped attribute value' do
        assert_equal(
          'urgent',
          Api::V2::Tickets::AttributeMappings.
            ticket_attribute_value(attribute, PriorityType.URGENT.to_s, account)
        )
      end
    end

    describe 'when the attribute is a ticket field' do
      let(:field)     { ticket_fields(:field_tagger_custom) }
      let(:attribute) { "ticket_fields_#{field.id}" }

      let(:custom_field_option) { field.custom_field_options.first }

      it 'returns the custom field option value' do
        assert_equal(
          custom_field_option.value,
          Api::V2::Tickets::AttributeMappings.ticket_attribute_value(
            attribute,
            custom_field_option.id,
            account
          )
        )
      end
    end

    describe 'when there are no mappings' do
      let(:attribute) { 'assignee_id' }

      it 'returns the attribute value' do
        assert_equal(
          'current_user',
          Api::V2::Tickets::AttributeMappings.
            ticket_attribute_value(attribute, 'current_user', account)
        )
      end
    end

    describe 'when the attribute is `satisfaction_score`' do
      let(:attribute) { 'satisfaction_score' }

      it 'returns the mapped attribute value' do
        assert_equal(
          'good',
          Api::V2::Tickets::AttributeMappings.ticket_attribute_value(
            attribute,
            SatisfactionType.GOODWITHCOMMENT.to_s,
            account
          )
        )
      end
    end

    describe 'when `full_fidelity` is true' do
      describe 'and the attribute is `satisfaction_score`' do
        let(:attribute) { 'satisfaction_score' }

        it 'returns the mapped attribute value' do
          assert_equal(
            'good_with_comment',
            Api::V2::Tickets::AttributeMappings.ticket_attribute_value(
              attribute,
              SatisfactionType.GOODWITHCOMMENT.to_s,
              account,
              full_fidelity: true
            )
          )
        end
      end

      describe "and there are mappings for the attribute's values" do
        let(:attribute) { 'priority_id' }

        it 'returns the mapped attribute value' do
          assert_equal(
            'urgent',
            Api::V2::Tickets::AttributeMappings.ticket_attribute_value(
              attribute,
              PriorityType.URGENT.to_s,
              account,
              full_fidelity: true
            )
          )
        end
      end

      describe 'and the attribute is a ticket field' do
        let(:field)     { ticket_fields(:field_tagger_custom) }
        let(:attribute) { "ticket_fields_#{field.id}" }

        let(:custom_field_option) { field.custom_field_options.first }

        it 'returns the custom field option value' do
          assert_equal(
            custom_field_option.value,
            Api::V2::Tickets::AttributeMappings.ticket_attribute_value(
              attribute,
              custom_field_option.id,
              account,
              full_fidelity: true
            )
          )
        end
      end

      describe 'and there are no mappings' do
        let(:attribute) { 'assignee_id' }

        it 'returns the attribute value' do
          assert_equal(
            'current_user',
            Api::V2::Tickets::AttributeMappings.ticket_attribute_value(
              attribute,
              'current_user',
              account,
              full_fidelity: true
            )
          )
        end
      end
    end
  end
end
