require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Tickets::RuleRevisionViaPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account)  { accounts(:minimum) }
  let(:rule)     { create_trigger }
  let(:revision) { rule.revisions.last }
  let(:user)     { users(:minimum_admin_not_owner) }

  let(:presenter) do
    Api::V2::Tickets::RuleRevisionViaPresenter.new(user)
  end

  before do
    Subscription.any_instance.stubs(has_trigger_revision_history?: true)
  end

  describe '#model_json' do
    describe 'when presenting a rule' do
      it 'presents the rule id' do
        assert_equal(
          rule.id,
          presenter.model_json(revision)[:from][:id]
        )
      end

      it 'presents the rule title' do
        assert_equal(
          rule.title,
          presenter.model_json(revision)[:from][:title]
        )
      end

      it 'presents whether the rule is deleted' do
        assert_equal(
          rule.deleted?,
          presenter.model_json(revision)[:from][:deleted]
        )
      end

      it 'presents the associated rule revision ID' do
        assert_equal(
          revision.nice_id,
          presenter.model_json(revision)[:from][:revision_id]
        )
      end

      it 'presents the rule type as `rel`' do
        assert_equal(
          'trigger',
          presenter.model_json(revision)[:rel]
        )
      end

      describe 'and rule is deleted' do
        before { revision.trigger = nil }

        it 'presents proper title' do
          assert_equal(
            'Unknown rule (deleted)',
            presenter.model_json(revision)[:from][:title]
          )
        end
      end
    end

    describe 'when account does not have `trigger_revision_history`' do
      before do
        Subscription.any_instance.stubs(has_trigger_revision_history?: false)
      end

      it 'does not present a revision ID' do
        refute presenter.model_json(revision)[:from].key?(:revision_id)
      end
    end

    describe 'when user is not an admin' do
      let(:user) { users(:minimum_agent) }

      it 'presents the associated rule revision ID' do
        assert_equal(
          revision.nice_id,
          presenter.model_json(revision)[:from][:revision_id]
        )
      end

      describe 'and the trigger is inactive' do
        before { revision.trigger.is_active = false }

        it 'does not present a revision ID' do
          refute presenter.model_json(revision)[:from].key?(:revision_id)
        end

        it 'does not present a rule ID' do
          refute presenter.model_json(revision)[:from].key?(:id)
        end
      end
    end

    describe 'when there is no revision' do
      let(:revision) { nil }

      it 'returns an empty hash' do
        assert_equal({}, presenter.model_json(revision))
      end
    end
  end
end
