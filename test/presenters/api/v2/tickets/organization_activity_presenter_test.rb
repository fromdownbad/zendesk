require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::OrganizationActivityPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @recipients = @account.end_users
    @trigger = @account.triggers.active.first
    @model = OrganizationActivity.create!(
      account: @account,
      ticket: @account.tickets.last,
      audit: @account.tickets.last.audits.last,
      author: @account.owner,
      via_id: Zendesk::Types::ViaType.Rule,
      via_reference_id: @trigger.id,
      value: @recipients.map(&:id),
      value_previous: 'value_previous',
      value_reference: 'value_reference'
    )

    @presenter = Api::V2::Tickets::OrganizationActivityPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :subject, :body, :recipients, :via

  should_present_method :id
  should_present_method :subject
  should_present_method :body
  should_present "OrganizationActivity", as: :type

  it "presents recipients" do
    json = @presenter.model_json(@model)
    expected_recipients = @recipients.map(&:id)
    assert_equal expected_recipients, json[:recipients]
  end

  it "presents via object" do
    json = @presenter.model_json(@model)
    assert_equal({
      channel: :rule,
      source: {
        from: {
          id: @trigger.id,
          title: @trigger.title,
          deleted: false
        },
        to: {},
        rel: 'trigger'
      }
    }, json[:via])
  end
end
