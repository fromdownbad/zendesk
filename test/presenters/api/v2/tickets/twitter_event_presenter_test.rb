require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::TwitterEventPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @recipients = @account.end_users
    @presenter = Api::V2::Tickets::TwitterEventPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  describe "for a Tweet event" do
    before do
      @model = Tweet.new(
        account: @account,
        ticket: @ticket,
        via_id: @ticket.audits.last.via_id,
        audit: @ticket.audits.last,
        author: @account.owner,
        value: @recipients.pluck(:id),
        value_previous: 'value_previous',
        value_reference: "@twitter_screen_name value_reference http://minimum.zendesk-test.com/twickets/#{@ticket.nice_id}"
      )
    end

    should_present_keys :id, :type, :body, :direct_message, :recipients

    should_present_method :id
    should_present_method :body

    should_present "Tweet", as: :type
    should_present false, as: :direct_message

    it "presents recipients" do
      json = @presenter.model_json(@model)
      expected_recipients = @recipients.pluck(:id)
      assert_equal expected_recipients, json[:recipients]
    end
  end

  describe "for a TwitterDmEvent" do
    before do
      @model = TwitterDmEvent.new(
        account: @account,
        ticket: @ticket,
        via_id: @ticket.audits.last.via_id,
        audit: @ticket.audits.last,
        author: @account.owner,
        value: @recipients.pluck(:id),
        value_previous: 'value_previous',
        value_reference: "@twitter_screen_name value_reference http://minimum.zendesk-test.com/twickets/#{@ticket.nice_id}"
      )
    end

    should_present_keys :id, :type, :body, :direct_message, :recipients

    should_present_method :id
    should_present_method :body

    should_present "Tweet", as: :type
    should_present true, as: :direct_message

    it "presents recipients" do
      json = @presenter.model_json(@model)
      expected_recipients = @recipients.pluck(:id)
      assert_equal expected_recipients, json[:recipients]
    end
  end
end
