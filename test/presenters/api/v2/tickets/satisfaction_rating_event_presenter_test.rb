require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::SatisfactionRatingEventPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @model = SatisfactionRatingEvent.create!(
      account: @account,
      ticket: @ticket,
      audit: @ticket.audits.last,
      author: @account.owner,
      value: 'value',
      value_previous: 'value_previous',
      value_reference: SatisfactionType.GOOD
    )

    @presenter = Api::V2::Tickets::SatisfactionRatingEventPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :body, :score, :assignee_id

  should_present_method :id
  should_present_method :comment, as: :body
  should_present_method :assignee_id
  should_present "SatisfactionRating", as: :type
  should_present "good", as: :score
end
