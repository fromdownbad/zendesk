require_relative '../../../../support/collaboration_settings_test_helper'
require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered! uncovered: 8

describe Api::V2::Tickets::EventViaPresenter do
  include Api::V2::Tickets::EventViaPresenter
  include TestSupport::Rule::Helper
  include ArturoTestHelper

  fixtures :all
  attr_accessor :account, :options

  def self.should_map_via_id(via_id, options)
    it "maps via_id #{via_id} to channel #{options[:to]}" do
      @model.stubs(:via_id).returns(via_id)
      stubs(:source_object).returns(nil)
      assert_equal options[:to], serialized_via_object(@model)[:channel]
    end
  end

  def self.voice_tests
    describe "inbound calls" do
      before do
        @model.stubs(:via_id).returns(Zendesk::Types::ViaType.PHONE_CALL_INBOUND)
        @model.stubs(:comment).returns(foo: :bar)
        @output = serialized_via_object(@model)[:source]
      end

      it "outputs detailed from information" do
        assert_equal({ phone: "456123456", formatted_phone: "+45 61 23 45 6", name: @user.name }, @output[:from])
      end

      it "outputs details to information" do
        assert_equal({ phone: "11234561234", formatted_phone: "+1 (123) 456-1234", name: @account.name }, @output[:to])
      end

      describe "with multibrand enabled" do
        before do
          @account.stubs(:has_multibrand?).returns(true)
          @output = serialized_via_object(@model)[:source]
        end

        it "outputs detailed to information" do
          assert_equal({ phone: "11234561234", formatted_phone: "+1 (123) 456-1234", name: @account.name, brand_id: 2 }, @output[:to])
        end
      end
    end

    describe "outbound calls" do
      before do
        @model.stubs(:via_id).returns(Zendesk::Types::ViaType.PHONE_CALL_OUTBOUND)
        @output = serialized_via_object(@model)[:source]
      end

      it "outputs detailed from information" do
        assert_equal({ phone: "456123456", formatted_phone: "+45 61 23 45 6", name: @account.name }, @output[:from])
      end

      it "outputs details to information" do
        assert_equal({ phone: "11234561234", formatted_phone: "+1 (123) 456-1234", name: @user.name }, @output[:to])
      end

      describe "with multibrand enabled" do
        before do
          @account.stubs(:has_multibrand?).returns(true)
          @output = serialized_via_object(@model)[:source]
        end

        it "outputs detailed from information" do
          assert_equal({ phone: "456123456", formatted_phone: "+45 61 23 45 6", name: @account.name, brand_id: 2 }, @output[:from])
        end
      end
    end
  end

  before do
    @account = accounts(:minimum)
    @model   = stub("event", via_id: nil, via_reference_id: nil, recipient: nil, ticket: nil)
    self.options = {}
  end

  should_map_via_id Zendesk::Types::ViaType.WEB_FORM,            to: :web
  should_map_via_id Zendesk::Types::ViaType.DROPBOX,             to: :web
  should_map_via_id Zendesk::Types::ViaType.BATCH,               to: :web
  should_map_via_id Zendesk::Types::ViaType.MAIL,                to: :email
  should_map_via_id Zendesk::Types::ViaType.CLOSED_TICKET,       to: :web
  should_map_via_id Zendesk::Types::ViaType.WEB_SERVICE,         to: :api
  should_map_via_id Zendesk::Types::ViaType.TICKET_SHARING,      to: :api
  should_map_via_id Zendesk::Types::ViaType.BLOG,                to: :api
  should_map_via_id Zendesk::Types::ViaType.IMPORT,              to: :api
  should_map_via_id Zendesk::Types::ViaType.GITHUB,              to: :api
  should_map_via_id Zendesk::Types::ViaType.TWITTER_FAVORITE,    to: :twitter
  should_map_via_id Zendesk::Types::ViaType.TWITTER_DM,          to: :twitter
  should_map_via_id Zendesk::Types::ViaType.TWITTER,             to: :twitter
  should_map_via_id Zendesk::Types::ViaType.TOPIC,               to: :forum
  should_map_via_id Zendesk::Types::ViaType.CHAT,                to: :chat
  should_map_via_id Zendesk::Types::ViaType.VOICEMAIL,           to: :voice
  should_map_via_id Zendesk::Types::ViaType.PHONE_CALL_INBOUND,  to: :voice
  should_map_via_id Zendesk::Types::ViaType.PHONE_CALL_OUTBOUND, to: :voice
  should_map_via_id Zendesk::Types::ViaType.FACEBOOK_POST,       to: :facebook
  should_map_via_id Zendesk::Types::ViaType.FACEBOOK_MESSAGE,    to: :facebook
  should_map_via_id Zendesk::Types::ViaType.RULE,                to: :rule
  should_map_via_id Zendesk::Types::ViaType.RULE_REVISION,       to: :rule
  should_map_via_id Zendesk::Types::ViaType.LINKED_PROBLEM,      to: :system
  should_map_via_id Zendesk::Types::ViaType.GROUP_DELETION,      to: :system
  should_map_via_id Zendesk::Types::ViaType.USER_DELETION,       to: :system
  should_map_via_id Zendesk::Types::ViaType.GROUP_CHANGE,        to: :system
  should_map_via_id Zendesk::Types::ViaType.RESOURCE_PUSH,       to: :system
  should_map_via_id Zendesk::Types::ViaType.IPHONE,              to: :system
  should_map_via_id Zendesk::Types::ViaType.GET_SATISFACTION,    to: :system
  should_map_via_id Zendesk::Types::ViaType.MERGE,               to: :system
  should_map_via_id Zendesk::Types::ViaType.USER_MERGE,          to: :system
  should_map_via_id Zendesk::Types::ViaType.LOGMEIN_RESCUE,      to: :system
  should_map_via_id Zendesk::Types::ViaType.MACRO_REFERENCE,     to: :system
  should_map_via_id Zendesk::Types::ViaType.ADMIN_SETTING,       to: :system
  should_map_via_id Zendesk::Types::ViaType.API_VOICEMAIL,       to: :api
  should_map_via_id Zendesk::Types::ViaType.API_PHONE_CALL_INBOUND, to: :api
  should_map_via_id Zendesk::Types::ViaType.API_PHONE_CALL_OUTBOUND, to: :api
  should_map_via_id Zendesk::Types::ViaType.HELPCENTER,          to: :help_center
  should_map_via_id Zendesk::Types::ViaType.SAMPLE_TICKET,       to: :sample_ticket
  should_map_via_id Zendesk::Types::ViaType.SAMPLE_INTERACTIVE_TICKET, to: :sample_ticket
  should_map_via_id Zendesk::Types::ViaType.SIDE_CONVERSATION, to: :side_conversation
  should_map_via_id Zendesk::Types::ViaType.SMS, to: :sms
  should_map_via_id Zendesk::Types::ViaType.ANSWER_BOT_API,            to: :answer_bot_api
  should_map_via_id Zendesk::Types::ViaType.ANSWER_BOT_FOR_WEB_WIDGET, to: :answer_bot_for_web_widget
  should_map_via_id Zendesk::Types::ViaType.ANSWER_BOT_FOR_AGENTS,     to: :answer_bot_for_agents
  should_map_via_id Zendesk::Types::ViaType.ANSWER_BOT_FOR_SLACK,      to: :answer_bot_for_slack
  should_map_via_id Zendesk::Types::ViaType.ANSWER_BOT_FOR_SDK,        to: :answer_bot_for_sdk
  should_map_via_id Zendesk::Types::ViaType.LOTUS,               to: :web
  should_map_via_id Zendesk::Types::ViaType.ANY_CHANNEL,         to: :any_channel
  should_map_via_id Zendesk::Types::ViaType.LINE,                to: :line
  should_map_via_id Zendesk::Types::ViaType.WECHAT,              to: :wechat
  should_map_via_id Zendesk::Types::ViaType.WHATSAPP,            to: :whatsapp
  should_map_via_id Zendesk::Types::ViaType.NATIVE_MESSAGING,    to: :native_messaging
  should_map_via_id Zendesk::Types::ViaType.MAILGUN,             to: :mailgun
  should_map_via_id Zendesk::Types::ViaType.MESSAGEBIRD_SMS,     to: :messagebird_sms
  should_map_via_id Zendesk::Types::ViaType.SUNSHINE_CONVERSATIONS_FACEBOOK_MESSENGER, to: :sunshine_conversations_facebook_messenger
  should_map_via_id Zendesk::Types::ViaType.TELEGRAM,   to: :telegram
  should_map_via_id Zendesk::Types::ViaType.TWILIO_SMS, to: :twilio_sms
  should_map_via_id Zendesk::Types::ViaType.SUNSHINE_CONVERSATIONS_TWITTER_DM, to: :sunshine_conversations_twitter_dm
  should_map_via_id Zendesk::Types::ViaType.VIBER,      to: :viber
  should_map_via_id Zendesk::Types::ViaType.GOOGLE_RCS, to: :google_rcs
  should_map_via_id Zendesk::Types::ViaType.APPLE_BUSINESS_CHAT,        to: :apple_business_chat
  should_map_via_id Zendesk::Types::ViaType.GOOGLE_BUSINESS_MESSAGES,   to: :google_business_messages
  should_map_via_id Zendesk::Types::ViaType.KAKAOTALK,      to: :kakaotalk
  should_map_via_id Zendesk::Types::ViaType.INSTAGRAM_DM,   to: :instagram_dm
  should_map_via_id Zendesk::Types::ViaType.SUNSHINE_CONVERSATIONS_API, to: :sunshine_conversations_api

  describe '#serialized_via_object' do
    it 'should hide the via object if this event has the same via as the audit' do
      event = events(:create_comment_for_minimum_ticket_1)
      assert_nil serialized_via_object(event)
    end

    it 'should not hide the via object if this event is a ChannelBackEvent' do
      event = events(:channel_back_event)
      expected_result = {
        channel: :web,
        source: {
          from: {},
          to: {},
          rel: nil
        }
      }.to_s
      assert_equal expected_result, serialized_via_object(event).to_s
    end
  end

  describe "on an ticket with a recipient" do
    before { @model.expects(:recipient).returns("support") }
    should_map_via_id Zendesk::Types::ViaType.RECOVERED_FROM_SUSPENDED_TICKETS, to: :email
  end

  describe "on an ticket without a recipient" do
    before { @model.expects(:recipient).returns(nil) }
    should_map_via_id Zendesk::Types::ViaType.RECOVERED_FROM_SUSPENDED_TICKETS, to: :web
  end

  describe "on a followup ticket without a via_reference_id" do
    before do
      @closed_ticket = tickets(:minimum_5)

      @follow_up_ticket = Ticket.new(account: @closed_ticket.account, requester: @closed_ticket.account.owner)
      @follow_up_ticket.via_id = ViaType.CLOSED_TICKET
      @follow_up_ticket.description = "This is my follow-up."
      @follow_up_ticket.instance_variable_set(:@current_via_reference_id, @closed_ticket.id)
      @follow_up_ticket.via_followup_source_id = @closed_ticket.nice_id
      @follow_up_ticket.set_followup_source(@closed_ticket.account.owner)
      @follow_up_ticket.will_be_saved_by(@closed_ticket.account.owner)

      @follow_up_ticket.save!

      @follow_up_ticket.instance_variable_set(:@current_via_reference_id, nil)
    end

    it "outputs the ticket with the correct follow-up details" do
      expected = {
        rel: :follow_up,
        from: {
          subject: @closed_ticket.subject,
          ticket_id: @closed_ticket.nice_id,
          channel: :web
        },
        to: {}
      }
      assert_equal(expected, serialized_via_object(@follow_up_ticket)[:source])
    end

    describe "with empty preload optimizations" do
      before do
        options[:event_via_associations] = {
          rules: {},
          tickets: {},
          nice_tickets: {}
        }
      end

      it "outputs the ticket with the correct follow-up details" do
        expected = {
          rel: :follow_up,
          from: {
            subject: @closed_ticket.subject,
            ticket_id: @closed_ticket.nice_id,
            channel: :web
          },
          to: {}
        }
        assert_equal(expected, serialized_via_object(@follow_up_ticket)[:source])
      end
    end
  end

  describe 'via RULE' do
    let(:rule) { create_trigger(title: 'Via Rule') }
    let(:user) { users(:minimum_admin) }

    before do
      stubs(user: user)

      @model.stubs(via_id: Zendesk::Types::ViaType.RULE)
      @model.stubs(via_reference_id: 1)

      stubs(:lookup_via_rule_association).with(@model.via_reference_id).returns(rule)
    end

    it 'outputs the rule as the source' do
      assert_equal(
        {rel: 'trigger', from: { title: 'Via Rule', id: rule.id, deleted: false }, to: {}},
        serialized_via_object(@model)[:source]
      )
    end
  end

  describe 'via RULE_REVISION' do
    let(:rule)           { create_trigger(title: 'Via Rule') }
    let(:account)        { accounts(:minimum) }
    let(:revision)       { rule.revisions.last }
    let(:rule_revisions) { {rule_revisions: {revision.id => revision}} }
    let(:user)           { users(:minimum_admin) }

    before do
      stubs(user: user)

      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.RULE_REVISION)
      @model.stubs(:via_reference_id).returns(revision.id)

      Subscription.any_instance.stubs(has_trigger_revision_history?: true)

      revision.stubs(includes: [revision])

      options[:event_via_associations] = rule_revisions
    end

    describe 'without preload optimizations' do
      let(:rule_revisions) { nil }

      before { serialized_via_object(@model) }

      before_should 'lookup rule revision in db' do
        TriggerRevision.expects(:where).returns(revision)
      end
    end

    describe 'with preload optimizations' do
      let(:rule_revisions) { {rule_revisions: {revision.id => revision}} }

      before { serialized_via_object(@model) }

      before_should 'does not lookup rule revision in db' do
        TriggerRevision.expects(:where).never
      end
    end

    it 'outputs the rule as the source' do
      assert_equal(
        {
          from: {
            id:          rule.id,
            title:       'Via Rule',
            deleted:     false,
            revision_id: revision.nice_id
          },
          rel: 'trigger'
        },
        serialized_via_object(@model)[:source]
      )
    end

    describe 'when the revision does not exist' do
      before { @model.stubs(via_reference_id: nil) }

      it 'returns an empty source' do
        assert_equal({}, serialized_via_object(@model)[:source])
      end
    end
  end

  describe "with garbage as the via_reference_id" do
    it "returns nil" do
      via_reference_id = HashWithIndifferentAccess.new("from" => {}, "to" => {}, "rel" => "follow_up")
      @model.stubs(:via_reference_id).returns(via_reference_id)
      assert_nil lookup_via_nice_ticket_association(@model.via_reference_id)
      assert_nil lookup_via_ticket_association(@model.via_reference_id)
    end
  end

  describe "with a ticket as via_reference" do
    before do
      via_reference_id = 999
      @model.stubs(:via_reference_id).returns(via_reference_id)
      expects(:lookup_via_ticket_association).with(via_reference_id).returns(subject)
    end

    describe "valid via_reference" do
      subject { stub(nice_id: 1, subject: "Mock Ticket Subject") }

      describe "via MERGE" do
        before { @model.stubs(:via_id).returns(Zendesk::Types::ViaType.MERGE) }

        it "outputs the ticket as the source" do
          assert_equal({ rel: :merge, from: { subject: "Mock Ticket Subject", ticket_id: 1 }, to: {} }, serialized_via_object(@model)[:source])
        end
      end

      describe "via LINKED_PROBLEM" do
        before { @model.stubs(:via_id).returns(Zendesk::Types::ViaType.LINKED_PROBLEM) }

        it "outputs the ticket as the source" do
          assert_equal({ rel: :problem, from: { subject: "Mock Ticket Subject", ticket_id: 1 }, to: {} }, serialized_via_object(@model)[:source])
        end
      end

      describe "via CLOSED_TICKET" do
        before do
          @model.stubs(:via_id).returns(Zendesk::Types::ViaType.CLOSED_TICKET)
          subject.stubs(:via_id).returns(Zendesk::Types::ViaType.MAIL)
        end

        it "outputs the ticket as the source" do
          expected = {
            rel: :follow_up,
            from: {
              subject: "Mock Ticket Subject",
              ticket_id: 1,
              channel: :email
            },
            to: {}
          }
          assert_equal(expected, serialized_via_object(@model)[:source])
        end
      end
    end

    describe "nil via_reference" do
      subject { nil }
      before { @model.stubs(value_reference: nil, merge_type: nil, source_ids: nil) }

      describe "via MERGE" do
        before { @model.stubs(via_id: Zendesk::Types::ViaType.MERGE) }

        it "outputs the ticket as the source" do
          assert_equal({ rel: :merge, from: { subject: nil, ticket_id: nil }, to: {} }, serialized_via_object(@model)[:source])
        end

        describe "with a value_reference of target and value with ids of source tickets" do
          before { @model.stubs(source_ids: [1, 2, 3, 4], merge_type: "target") }

          it "outputs the ticket_ids as the source" do
            assert_equal({ rel: :merge, from: { ticket_ids: [1, 2, 3, 4], ticket_id: nil, subject: nil }, to: {} }, serialized_via_object(@model)[:source])
          end
        end
      end

      describe "via LINKED_PROBLEM" do
        before { @model.stubs(via_id: Zendesk::Types::ViaType.LINKED_PROBLEM) }

        it "outputs the ticket as the source" do
          assert_equal({ rel: :problem, from: { subject: nil, ticket_id: nil }, to: {} }, serialized_via_object(@model)[:source])
        end
      end

      describe "via CLOSED_TICKET" do
        before { @model.stubs(:via_id).returns(Zendesk::Types::ViaType.CLOSED_TICKET) }

        it "outputs the ticket as the source" do
          assert_equal({ rel: :follow_up, from: { subject: nil, ticket_id: nil }, to: {} }, serialized_via_object(@model)[:source])
        end
      end
    end
  end

  describe "#lookup_via_rule_association" do
    before { @rule = stub(id: 1, has_attribute?: true) }

    describe "without preload optimizations" do
      describe "for a non-deleted rule" do
        it "lookups rule in db" do
          Rule.expects(:find_by_sql).returns([@rule])
          assert_equal @rule, lookup_via_rule_association(@rule.id)
        end
      end

      describe "for a soft-deleted rule" do
        before { @rule = stub(id: 2, has_attribute?: true, deleted?: true) }

        it "lookups rule with deleted scope" do
          Rule.expects(:with_deleted).returns(@rule)
          assert_equal @rule, lookup_via_rule_association(@rule.id)
        end
      end
    end

    describe "with preload optimizations" do
      before do
        options[:event_via_associations] = {
          rules: { @rule.id => @rule }
        }
      end

      it "does not lookup rule in db" do
        Rule.expects(:find_by_sql).never
        assert_equal @rule, lookup_via_rule_association(@rule.id)
      end
    end
  end

  describe "#lookup_via_ticket_association" do
    before { @ticket = tickets(:minimum_1) }

    describe "without preload optimizations" do
      it "lookups ticket in db" do
        Ticket.expects(:find_by_sql).returns([@ticket])
        assert_equal @ticket, lookup_via_ticket_association(@ticket.id)
      end
    end

    describe "with preload optimizations" do
      before do
        options[:event_via_associations] = {
          tickets: { @ticket.id => @ticket }
        }
      end

      it "does not lookup ticket in db" do
        Ticket.expects(:find_by_sql).never
        assert_equal @ticket, lookup_via_ticket_association(@ticket.id)
      end
    end
  end

  describe "via TWITTER" do
    describe 'with Ticket' do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.requester.identities << UserTwitterIdentity.first
        @ticket.stubs(:via_id).returns(Zendesk::Types::ViaType.TWITTER)
      end

      it 'returns the correct rel' do
        profile = mock('TwitterUserProfile')
        Channels::TwitterUserProfile.stubs(:find_by).returns profile
        assert_equal :mention, serialized_via_object(@ticket)[:source][:rel]
      end

      it 'returns an empty hash for from' do
        @output = serialized_via_object(@ticket)[:source]
        assert_equal({}, @output[:from])
      end

      it 'returns an empty hash for to' do
        @output = serialized_via_object(@ticket)[:source]
        assert_equal({}, @output[:to])
      end

      describe 'with twitter_via_source_attributes Arturo enabled' do
        before do
          Arturo.enable_feature!(:twitter_via_source_attributes)
          profile = mock('TwitterUserProfile')
          profile.stubs(:metadata).returns(display_name: 'Author Minimum')
          profile.stubs(:name).returns('wombat')
          Channels::TwitterUserProfile.stubs(:find_by).returns profile
        end

        it 'returns the correct rel' do
          assert_equal :mention, serialized_via_object(@ticket)[:source][:rel]
        end

        it 'returns correct values for from' do
          @output = serialized_via_object(@ticket)[:source]
          assert_equal({ name: 'Author Minimum', username: 'wombat', profile_url: 'https://www.twitter.com/wombat', twitter_id: '1236' }, @output[:from])
        end

        it 'returns correct values for to' do
          @output = serialized_via_object(@ticket)[:source]
          assert_equal({ name: nil, username: 'wombat', profile_url: 'https://www.twitter.com/wombat', twitter_id: '1235' }, @output[:to])
        end

        describe 'when no Twitter handle is found' do
          before do
            stubs(:ticket_handle_source).returns(nil)
          end

          it 'returns an empty hash for to' do
            @output = serialized_via_object(@ticket)[:source]
            assert_equal({}, @output[:to])
          end

          it 'returns correct values for from' do
            @output = serialized_via_object(@ticket)[:source]
            assert_equal({ name: 'Author Minimum', username: 'wombat', profile_url: 'https://www.twitter.com/wombat', twitter_id: '1236' }, @output[:from])
          end
        end
      end
    end

    describe 'with an Event' do
      describe 'with a Comment' do
        before do
          @ticket = tickets(:minimum_1)
          comment = @ticket.comments.first
          comment.author.identities << UserTwitterIdentity.first
          @comment_audit = comment.audit
          @comment_audit.stubs(:via_id).returns(Zendesk::Types::ViaType.TWITTER)
        end

        it 'returns the correct rel' do
          profile = mock('TwitterUserProfile')
          Channels::TwitterUserProfile.stubs(:find_by).returns profile
          assert_equal :mention, serialized_via_object(@comment_audit)[:source][:rel]
        end

        it 'returns an empty hash for from' do
          @output = serialized_via_object(@comment_audit)[:source]
          assert_equal({}, @output[:from])
        end

        it 'returns an empty hash for to' do
          @output = serialized_via_object(@comment_audit)[:source]
          assert_equal({}, @output[:to])
        end

        describe 'with twitter_via_source_attributes Arturo enabled' do
          before do
            Arturo.enable_feature!(:twitter_via_source_attributes)
            profile = mock('TwitterUserProfile')
            profile.stubs(:metadata).returns(display_name: 'Author Minimum')
            profile.stubs(:name).returns('wombat')
            Channels::TwitterUserProfile.stubs(:find_by).returns profile
          end

          it 'returns the correct rel' do
            assert_equal :mention, serialized_via_object(@comment_audit)[:source][:rel]
          end

          it 'returns correct values for from' do
            @output = serialized_via_object(@comment_audit)[:source]
            assert_equal({ name: 'Author Minimum', username: 'wombat', profile_url: 'https://www.twitter.com/wombat', twitter_id: '1236' }, @output[:from])
          end

          it 'returns correct values for to' do
            @output = serialized_via_object(@comment_audit)[:source]
            assert_equal({ name: nil, username: 'wombat', profile_url: 'https://www.twitter.com/wombat', twitter_id: '1235' }, @output[:to])
          end

          describe 'with soft deleted ticket' do
            before do
              @ticket.will_be_saved_by @ticket.account.owner
              @ticket.soft_delete!
            end

            it 'still returns the correct values for `to`' do
              refute @comment_audit.ticket # ZD4950420 - with_deleted scope fails without explicit find/reload
              @output = serialized_via_object(@comment_audit)[:source]
              assert_equal({ name: nil, username: 'wombat', profile_url: 'https://www.twitter.com/wombat', twitter_id: '1235' }, @output[:to])
            end
          end

          describe 'when no Twitter handle is found' do
            before do
              stubs(:ticket_handle_source).returns(nil)
            end

            it 'returns an empty hash for to' do
              @output = serialized_via_object(@comment_audit)[:source]
              assert_equal({}, @output[:to])
            end

            it 'returns correct values for from' do
              @output = serialized_via_object(@comment_audit)[:source]
              assert_equal({ name: 'Author Minimum', username: 'wombat', profile_url: 'https://www.twitter.com/wombat', twitter_id: '1236' }, @output[:from])
            end
          end
        end
      end

      describe 'with a non-Comment Event' do
        before do
          @event = KnowledgeLinked.new(ticket: tickets(:minimum_1))
          @event.stubs(:via_id).returns(Zendesk::Types::ViaType.TWITTER)
        end

        it 'returns the correct rel' do
          profile = mock('TwitterUserProfile')
          Channels::TwitterUserProfile.stubs(:find_by).returns profile
          assert_equal :mention, serialized_via_object(@event)[:source][:rel]
        end

        it 'returns an empty hash for from' do
          @output = serialized_via_object(@event)[:source]
          assert_equal({}, @output[:from])
        end

        it 'returns an empty hash for to' do
          @output = serialized_via_object(@event)[:source]
          assert_equal({}, @output[:to])
        end

        describe 'with twitter_via_source_attributes Arturo enabled' do
          before do
            Arturo.enable_feature!(:twitter_via_source_attributes)
          end

          it 'returns an empty hash for from' do
            assert_equal({}, serialized_via_object(@event)[:source][:from])
          end
        end
      end
    end

    describe 'with a TwitterEvent' do
      before do
        @event = TwitterDmEvent.new
        @event.stubs(:via_id).returns(Zendesk::Types::ViaType.TWITTER)
      end

      it 'returns the correct rel' do
        profile = mock('TwitterUserProfile')
        Channels::TwitterUserProfile.stubs(:find_by).returns profile
        assert_equal :mention, serialized_via_object(@event)[:source][:rel]
      end

      it 'returns an empty hash for from' do
        @output = serialized_via_object(@event)[:source]
        assert_equal({}, @output[:from])
      end

      it 'returns an empty hash for to' do
        @output = serialized_via_object(@event)[:source]
        assert_equal({}, @output[:to])
      end

      describe 'with twitter_via_source_attributes Arturo enabled' do
        before do
          Arturo.enable_feature!(:twitter_via_source_attributes)
        end

        it 'returns an empty hash' do
          assert_equal({}, serialized_via_object(@event)[:source])
        end
      end
    end
  end

  # This could go in the describe below but facebook_to_page is stubbed...
  describe 'facebook_to_page' do
    it 'returns nil if no ticket given' do
      res = facebook_to_page(nil)
      assert_nil res
    end

    it 'returns nil if the ticket has no decorations' do
      ticket = mock("ticket")
      ticket.stubs(:event_decorations).returns([])
      assert_nil facebook_to_page(ticket)
    end

    describe 'when the ticket has decorations' do
      before do
        @ticket = tickets(:minimum_1)
        assert @ticket.event_decorations.count > 0
      end

      it 'returns a facebook page' do
        assert_equal facebook_pages(:minimum_facebook_page_2), facebook_to_page(@ticket)
      end

      describe 'and none of them have the source attribute' do
        before do
          @ticket.event_decorations.each do |ed|
            ed.data = {}
            ed.save!
          end
        end

        it 'returns nil' do
          assert_nil facebook_to_page(@ticket)
        end
      end
    end
  end

  describe "via FACEBOOK" do
    before do
      @ticket = mock("ticket")
      page = facebook_pages(:minimum_facebook_page_1)
      stubs(:facebook_to_page).returns(page)
      @ticket.stubs(:requester).returns(users(:minimum_end_user))
      @model.stubs(:author).returns(users(:minimum_end_user))
      @model.stubs(:association).returns(stub(loaded?: true))
      @model.stubs(:ticket).returns(@ticket)
      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.FACEBOOK_POST)
    end

    describe "when no wall post is present" do
      before do
        stubs(:facebook_to_page).returns(nil)
      end

      it "returns nil values" do
        @output = serialized_via_object(@model)[:source]
        assert_equal({ profile_url: "http://www.facebook.com/1", facebook_id: "1", name: "minimum_end_user" }, @output[:from])
        assert_equal({ profile_url: nil, facebook_id: nil, name: nil }, @output[:to])
      end
    end

    describe "when event ticket cannot be found" do
      before do
        @model.stubs(:ticket).returns(nil)
        unstub(:facebook_to_page)
      end

      it "returns nil values" do
        @output = serialized_via_object(@model)[:source]
        assert_equal({ profile_url: "http://www.facebook.com/1", facebook_id: "1", name: "minimum_end_user" }, @output[:from])
        assert_equal({ profile_url: nil, facebook_id: nil, name: nil }, @output[:to])
      end
    end

    describe "for a ticket requester" do
      before do
        @output = serialized_via_object(@model)[:source]
      end

      it "outputs detailed from information" do
        assert_equal({ profile_url: "http://www.facebook.com/1", facebook_id: "1", name: "minimum_end_user" }, @output[:from])
      end

      it "outputs details to information" do
        assert_equal({ profile_url: "pagelink", facebook_id: "33452", name: "Evaluate" }, @output[:to])
      end
    end

    describe "for a ticket requester with no facebook id" do
      before do
        @model.stubs(:author).returns(users(:minimum_search_user))
        @output = serialized_via_object(@model)[:source]
      end

      it "outputs detailed from information" do
        assert_equal({ profile_url: "http://www.facebook.com/33452", facebook_id: "33452", name: "minimum_search_user" }, @output[:from])
      end
    end

    describe "for a facebook commenter" do
      before do
        @model.stubs(:is_a?).with(AuditEvent).returns(false)
        @model.stubs(:is_a?).with(Ticket).returns(false)
        @model.stubs(:is_a?).with(FacebookEvent).returns(false)
      end

      describe "for audits" do
        before do
          @model.stubs(:is_a?).with(Audit).returns(true)
          @model.stubs(:is_a?).with(FacebookComment).returns(false)
          comment = mock("comment", data: {requester: {name: "fb_user", id: "4234"}})
          comment.stubs(:is_a?).with(FacebookComment).returns(true)
          @model.stubs(:comment).returns(comment)
          @output = serialized_via_object(@model)[:source]
        end

        it "outputs detailed from information" do
          assert_equal({ profile_url: "http://www.facebook.com/4234", facebook_id: "4234", name: "fb_user" }, @output[:from])
        end
      end

      describe "for facebook comments" do
        before do
          @model.stubs(:is_a?).with(Audit).returns(false)
          @model.stubs(:is_a?).with(FacebookComment).returns(true)
          @model.stubs(:data).returns(requester: {name: "fb_user", id: "4234"})
          @output = serialized_via_object(@model)[:source]
        end

        it "outputs detailed from information" do
          assert_equal({ profile_url: "http://www.facebook.com/4234", facebook_id: "4234", name: "fb_user" }, @output[:from])
        end
      end
    end
  end

  describe "via ANY_CHANNEL" do
    describe "when event ticket object exists" do
      before do
        @ticket = stub("ticket", id: 123)
        @model.stubs(:association).returns(stub(loaded?: true))
        @model.stubs(:ticket).returns(@ticket)
        @model.stubs(:via_id).returns(Zendesk::Types::ViaType.ANY_CHANNEL)
      end

      it "returns info from Channels code" do
        Channels::AnyChannel::RegisteredIntegrationService.
          expects(:service_info_for_ticket).
          with(@account.id, @ticket.id).
          returns(test_info: 'test value')
        expected_result = {
          from: {
            service_info: { test_info: 'test value' }
          },
          to: {},
          rel: nil
        }

        assert_equal expected_result, serialized_via_object(@model)[:source]
      end
    end

    describe "when event ticket object cannot be found" do
      before do
        @model.stubs(:via_id).returns(Zendesk::Types::ViaType.ANY_CHANNEL)
        stubs(:find_ticket).returns(nil)
      end

      it "returns empty hash" do
        assert_equal({}, source_object(@model))
      end
    end
  end

  describe_with_and_without_arturo_enabled :email_ccs do
    describe "via MAIL" do
      let(:output) { serialized_via_object(@model)[:source] }

      before do
        @model = tickets(:minimum_1)
        @model.via_id = Zendesk::Types::ViaType.MAIL
        @user = @model.requester
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        describe 'an audit' do
          describe 'when `recipients_support_address` is present' do
            before do
              @model = Audit.new do |audit|
                audit.account = @account
                audit.ticket = @model
                audit.recipients = "test1 test2 support+id92pd0w-qg02@minimum.zendesk-test.com"
                audit.via_id = @model.via_id
              end
            end

            it "outputs detailed `from` information" do
              assert_equal({
                address: @user.email,
                name: @user.name,
                original_recipients: %w[test1 test2 support+id92pd0w-qg02@minimum.zendesk-test.com],
              },
                output[:from])
            end

            it "outputs detailed `to` information" do
              assert_equal({
                name: 'Minimum account',
                address: 'support+id92pd0w-qg02@minimum.zendesk-test.com',
                email_ccs: %w[test1 test2]
              },
                output[:to])
            end
          end

          describe 'when `recipients_support_address` is absent' do
            before do
              @model = Audit.new do |audit|
                audit.account = @account
                audit.ticket = @model
                audit.recipients = "test1 test2"
                audit.via_id = @model.via_id
              end
            end

            it "outputs detailed `from` information" do
              assert_equal({
                address: @user.email,
                name: @user.name,
                original_recipients: %w[test1 test2],
              },
                output[:from])
            end

            it "outputs detailed `to` information" do
              assert_equal({
                name: 'Minimum account',
                address: 'support@minimum.test5.localhost:3008',
                email_ccs: %w[test1 test2]
              },
                output[:to])
            end
          end
        end
      end

      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        describe "an audit" do
          before do
            @model = Audit.new do |audit|
              audit.account = @account
              audit.ticket = @model
              audit.recipients = "test1 test2"
              audit.via_id = @model.via_id
            end
          end

          it "outputs detailed `from` information" do
            assert_equal({
              address: @user.email,
              name: @user.name,
              original_recipients: %w[test1 test2]
},
              output[:from])
          end

          it "outputs detailed `to` information" do
            assert_equal({
              name: 'Minimum account',
              address: 'support@minimum.test5.localhost:3008'
},
              output[:to])
          end
        end
      end

      describe "a suspended ticket" do
        before do
          @model = suspended_tickets(:minimum_spam)
          @user = @model.author
        end

        it "outputs detailed from information" do
          assert_equal({ address: @user.email, name: @user.name }, output[:from])
        end

        it "outputs detailed to information" do
          assert_equal({ address: @model.recipient, name: @account.name }, output[:to])
        end
      end

      describe "anything else" do
        it "outputs detailed from information" do
          assert_equal({ address: @user.email, name: @user.name }, output[:from])
        end

        it "outputs details to information" do
          assert_equal({ address: @model.recipient, name: @account.name }, output[:to])
        end
      end

      describe "when the requester is missing" do
        before do
          @model.requester = nil
        end

        it "does not include requester information" do
          assert_equal({}, output[:from])
        end
      end
    end
  end

  [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
    describe "via #{ViaType.to_s(via_id)}" do
      let(:output) { serialized_via_object(@model)[:source] }

      before do
        @model = tickets(:minimum_1)
        @model.via_id = via_id
        @user = @model.requester
      end

      describe "an audit" do
        let(:requester) { users(:minimum_author) }
        let(:recipient_1) { users(:minimum_end_user) }
        let(:recipient_2) { users(:minimum_search_user) }

        describe_with_and_without_arturo_enabled :email_ccs_preload_notifications_with_ccs do
          before do
            @model = Audit.new { |a| a.account = @account; a.ticket = @model; a.via_id = @model.via_id }
            if @account.has_email_ccs_preload_notifications_with_ccs?
              stubs(recipients_ids_or_identifiers: [recipient_1.id, recipient_2.id])
              stubs(audit_recipients_users_with_ids: [requester])
            else
              @model.stubs(recipients_ids_or_identifiers: [recipient_1.id, recipient_2.id])
              @model.stubs(recipients_notification_to: requester)
            end
          end

          describe "when comment_email_ccs_allowed setting is enabled" do
            before do
              Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
            end

            it "outputs email_ccs in to object" do
              output[:to][:email_ccs].must_equal [recipient_1.id, recipient_2.id]
            end

            it "does not include the requester name in to object" do
              output[:to].wont_include :name
            end

            it "does not include the requester email address in to object" do
              output[:to].wont_include :address
            end

            describe "when the audit has a comment" do
              before(:each) do
                @model.comment = events(:create_comment_for_minimum_ticket_1)
              end

              describe "when the comment is public" do
                before(:each) do
                  @model.comment.stubs(:is_public?).returns(true)
                end

                it "includes the requester name in to object" do
                  output[:to][:name].must_equal "Author Minimum"
                end

                it "includes the requester email address in to object" do
                  output[:to][:address].must_equal "minimum_author@aghassipour.com"
                end
              end

              describe "when the comment is private" do
                before(:each) do
                  @model.comment.stubs(:is_public?).returns(false)
                end

                it "does not include the requester name in to object" do
                  refute output[:to][:name]
                end

                it "does not include the requester email address in to object" do
                  refute output[:to][:address]
                end
              end
            end
          end

          describe "when comment_email_ccs_allowed setting is disabled" do
            before do
              Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
            end

            it "does not output email_ccs in to object" do
              output[:to].wont_include :email_ccs
            end

            it "does not include the requester name in to object" do
              output[:to].wont_include :name
            end

            it "does not include the requester email address in to object" do
              output[:to].wont_include :address
            end
          end
        end
      end

      describe "anything else" do
        describe "when comment_email_ccs_allowed setting is enabled" do
          before do
            Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
          end

          it "does not output email_ccs in to object" do
            output[:to].wont_include :email_ccs
          end

          it "does not include the requester name in to object" do
            output[:to].wont_include :name
          end

          it "does not include the requester email address in to object" do
            output[:to].wont_include :address
          end
        end

        describe "when comment_email_ccs_allowed setting is disabled" do
          before do
            Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
          end

          it "does not output email_ccs in to object" do
            output[:to].wont_include :email_ccs
          end

          it "does not include the requester name in to object" do
            output[:to].wont_include :name
          end

          it "does not include the requester email address in to object" do
            output[:to].wont_include :address
          end
        end

        describe "rel" do
          describe "via_reference_id is not set" do
            it "is nil" do
              assert_nil output[:rel]
            end
          end

          describe "via_reference_id is set to a value in VIA_ID_MAP" do
            before do
              @model.via_reference_id = ViaType.HELPCENTER
            end

            it "returns the value from VIA_ID_MAP" do
              assert_equal :help_center, output[:rel]
            end
          end

          describe "via_reference_id is set to a value not in VIA_ID_MAP" do
            before do
              @model.via_reference_id = -1 * ViaType.HELPCENTER
            end

            it "is nil" do
              assert_nil output[:rel]
            end
          end
        end
      end
    end
  end

  describe "via CHAT" do
    let(:output) { serialized_via_object(@model)[:source] }

    before do
      @model = tickets(:minimum_1)
      @model.via_id = ViaType.CHAT
      @user = @model.requester
    end

    describe "rel" do
      describe "via_reference_id is not set" do
        it "is nil" do
          assert_nil output[:rel]
        end
      end

      describe "via_reference_id is set to a value in REL_MAP" do
        before do
          @model.via_reference_id = ViaType.CHAT_OFFLINE_MESSAGE
        end

        it "returns the value from REL_MAP" do
          assert_equal :chat_offline_message, output[:rel]
        end
      end

      describe "via_reference_id is set to a value not in REL_MAP" do
        before do
          @model.via_reference_id = -1 * ViaType.CHAT_OFFLINE_MESSAGE
        end

        it "is nil" do
          assert_nil output[:rel]
        end
      end
    end
  end

  describe "via LINE" do
    before do
      @model = tickets(:minimum_1)
      @model.via_id = ViaType.LINE
      @user = @model.requester
      @output = serialized_via_object(@model)
    end

    it "has the correct 'via' data structure" do
      assert_equal({ channel: :line, source: { from: {}, to: {}, rel: nil } }, @output)
    end
  end

  describe "via WECHAT" do
    before do
      @model = tickets(:minimum_1)
      @model.via_id = ViaType.WECHAT
      @user = @model.requester
      @output = serialized_via_object(@model)
    end

    it "has the correct 'via' data structure" do
      assert_equal({ channel: :wechat, source: { from: {}, to: {}, rel: nil } }, @output)
    end
  end

  describe "via WHATSAPP" do
    before do
      @model = tickets(:minimum_1)
      @model.via_id = ViaType.WHATSAPP
      @user = @model.requester
      @output = serialized_via_object(@model)
    end

    it "has the correct 'via' data structure" do
      assert_equal({ channel: :whatsapp, source: { from: {}, to: {}, rel: nil } }, @output)
    end
  end

  describe "via NATIVE_MESSAGING" do
    before do
      @model = tickets(:minimum_1)
      @model.via_id = ViaType.NATIVE_MESSAGING
      @user = @model.requester
      @output = serialized_via_object(@model)
    end

    it "has the correct 'via' data structure" do
      assert_equal({ channel: :native_messaging, source: { from: {}, to: {}, rel: nil } }, @output)
    end
  end

  # Additional Sunshine's social types
  [
    {via_id: ViaType.MAILGUN, name: 'MAILGUN', channel: :mailgun},
    {via_id: ViaType.MESSAGEBIRD_SMS, name: 'MESSAGEBIRD_SMS', channel: :messagebird_sms},
    {via_id: ViaType.TELEGRAM, name: 'TELEGRAM', channel: :telegram},
    {via_id: ViaType.SUNSHINE_CONVERSATIONS_FACEBOOK_MESSENGER, name: 'SUNSHINE_CONVERSATIONS_FACEBOOK_MESSENGER', channel: :sunshine_conversations_facebook_messenger},
    {via_id: ViaType.TWILIO_SMS, name: 'TWILIO_SMS', channel: :twilio_sms},
    {via_id: ViaType.SUNSHINE_CONVERSATIONS_TWITTER_DM, name: 'SUNSHINE_CONVERSATIONS_TWITTER_DM', channel: :sunshine_conversations_twitter_dm},
    {via_id: ViaType.VIBER, name: 'VIBER', channel: :viber},
    {via_id: ViaType.GOOGLE_RCS, name: 'GOOGLE_RCS', channel: :google_rcs},
    {via_id: ViaType.APPLE_BUSINESS_CHAT, name: 'APPLE_BUSINESS_CHAT', channel: :apple_business_chat},
    {via_id: ViaType.GOOGLE_BUSINESS_MESSAGES, name: 'GOOGLE_BUSINESS_MESSAGES', channel: :google_business_messages},
    {via_id: ViaType.KAKAOTALK, name: 'KAKAOTALK', channel: :kakaotalk},
    {via_id: ViaType.INSTAGRAM_DM, name: 'INSTAGRAM_DM', channel: :instagram_dm},
    {via_id: ViaType.SUNSHINE_CONVERSATIONS_API, name: 'SUNSHINE_CONVERSATIONS_API', channel: :sunshine_conversations_api}
  ].each do |setting|
    describe "via #{setting[:name]}" do
      before do
        @model = tickets(:minimum_1)
        @model.via_id = setting[:via_id]
        @user = @model.requester
        @output = serialized_via_object(@model)
      end

      it "has the correct 'via' data structure" do
        assert_equal({ channel: setting[:channel], source: { from: {}, to: {}, rel: nil } }, @output)
      end
    end
  end

  describe 'via CLOSED_TICKET' do
    let(:ticket) { tickets(:minimum_1) }
    let(:output) { serialized_via_object(@model)[:source] }
    let(:requester) { users(:minimum_author) }
    let(:recipient_1) { users(:minimum_end_user) }
    let(:recipient_2) { users(:minimum_search_user) }

    describe_with_and_without_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      before do
        @model = Audit.new do |audit|
          audit.account = @account
          audit.ticket = ticket;
          audit.via_id = ViaType.CLOSED_TICKET
        end

        if @account.has_email_ccs_preload_notifications_with_ccs?
          stubs(recipients_ids_or_identifiers: [recipient_1.id, recipient_2.id])
          stubs(audit_recipients_users_with_ids: [requester])
        else
          @model.stubs(recipients_ids_or_identifiers: [recipient_1.id, recipient_2.id])
          @model.stubs(recipients_notification_to: requester)
        end

        @user = ticket.requester
      end

      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        it 'presents `email_ccs` in `to`' do
          output[:to][:email_ccs].must_equal [recipient_1.id, recipient_2.id]
        end

        it 'does not present the requester name in `to`' do
          output[:to].wont_include :name
        end

        it 'does not present the requester email address in `to`' do
          output[:to].wont_include :address
        end

        describe 'and the audit has a comment' do
          before do
            @model.comment = events(:create_comment_for_minimum_ticket_1)
          end

          describe 'and the comment is public' do
            before do
              @model.comment.stubs(:is_public?).returns(true)
            end

            it 'includes the requester name in `to`' do
              output[:to][:name].must_equal 'Author Minimum'
            end

            it 'includes the requester email address in `to`' do
              output[:to][:address].must_equal 'minimum_author@aghassipour.com'
            end
          end

          describe 'and the comment is private' do
            before do
              @model.comment.stubs(:is_public?).returns(false)
            end

            it 'does not include the requester name in `to`' do
              refute output[:to][:name]
            end

            it 'does not include the requester email address in `to`' do
              refute output[:to][:address]
            end
          end
        end
      end

      describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
        it 'does not present `email_ccs`' do
          output[:to].wont_include :email_ccs
        end

        it 'does not present `name` in `to`' do
          output[:to].wont_include :name
        end

        it 'does not present `address` in `to`' do
          output[:to].wont_include :address
        end
      end
    end
  end

  describe "via VOICE" do
    before do
      @user = users(:minimum_end_user)
      @ticket = mock("ticket")
      @ticket.stubs(:requester).returns(@user)
      @model.stubs(:association).returns(stub(loaded?: true))
      @model.stubs(:ticket).returns(@ticket)
      @model.stubs(:is_a?).with(AuditEvent).returns(false)
      @model.stubs(:is_a?).with(Ticket).returns(false)
      @model.stubs(:is_a?).with(Audit).returns(false)
      @model.stubs(:account).returns(@account)
      @model.stubs(:is_a?).with(VoiceComment).returns(true)
      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.VOICEMAIL)
      @model.stubs(:formatted_from).returns("+45 61 23 45 6")
      @model.stubs(:formatted_to).returns("+1 (123) 456-1234")
      @model.stubs(:data).returns(brand_id: 2, from: "456123456", to: "11234561234")
    end

    describe "only voice comments have via.source info" do
      before do
        @account.stubs(:has_voice_via_only_voice_comments?).returns(true)
        @output = serialized_via_object(@model)[:source]
      end

      it "returns only :rel when comment is not a voice comment" do
        @model.stubs(:is_a?).with(VoiceComment).returns(false)
        @output = serialized_via_object(@model)[:source]
        assert_equal({ rel: :voicemail }, @output)
      end

      voice_tests
    end

    describe "all comments have via.source info" do
      before do
        @account.stubs(:has_voice_via_only_voice_comments?).returns(false)
        @model.stubs(:ticket).returns(@ticket)
        @output = serialized_via_object(@model)[:source]
      end

      voice_tests
    end
  end

  describe "via TOPIC" do
    before do
      @entry = subject

      ticket = mock("ticket")
      ticket.stubs(:entry).returns(@entry)

      @model.stubs(:association).returns(stub(loaded?: true))
      @model.stubs(:ticket).returns(ticket)
      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.TOPIC)

      @output = serialized_via_object(@model)[:source]
    end

    describe "with a valid entry" do
      subject { entries(:ponies) }
      it "outputs detailed from information" do
        assert_equal({ topic_id: @entry.id, topic_name: @entry.name }, @output[:from])
      end

      it "outputs details to information" do
        assert_equal({}, @output[:to])
      end
    end

    describe "with a deleted entry" do
      subject { nil }

      it "outputs nothing" do
        assert_equal({ topic_id: nil, topic_name: nil }, @output[:from])
      end
    end
  end

  describe "via HELPCENTER" do
    before do
      ticket = mock("ticket")
      hc_ticket = mock("help_center_ticket",
        post_id: 42,
        post_name: "Converting post to tickets",
        post_url: "url_to_post_42")
      HelpCenterTicket.stubs(:new).with(ticket).returns(hc_ticket)
      @model.stubs(:association).returns(stub(loaded?: true))
      @model.stubs(:ticket).returns(ticket)
      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.HELPCENTER)
    end

    it "builds :from based on the help_center ticket" do
      output = serialized_via_object(@model)[:source][:from]
      expected_output = {
        post_id: 42,
        post_name: "Converting post to tickets",
        post_url: "url_to_post_42"
      }

      assert_equal expected_output, output
    end
  end

  describe "via API_VOICEMAIL" do
    before do
      @model = tickets("minimum_1")

      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.API_VOICEMAIL)
      @output = serialized_via_object(@model)
    end

    it "has the correct 'via data structure" do
      assert_equal({channel: :api, source: {rel: :voicemail}}, @output)
    end
  end

  describe "via API_PHONE_CALL_INBOUND" do
    before do
      @model = tickets("minimum_1")

      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.API_PHONE_CALL_INBOUND)
      @output = serialized_via_object(@model)
    end

    it "has the correct 'via data structure" do
      assert_equal({channel: :api, source: {rel: :inbound}}, @output)
    end
  end

  describe "via API_PHONE_CALL_OUTBOUND" do
    before do
      @model = tickets("minimum_1")

      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.API_PHONE_CALL_OUTBOUND)
      @output = serialized_via_object(@model)
    end

    it "has the correct 'via data structure" do
      assert_equal({channel: :api, source: {rel: :outbound}}, @output)
    end
  end

  describe "via ADMIN_SETTING" do
    before do
      setting_id = 10051
      setting_name = 'assign_tickets_upon_solve'
      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.ADMIN_SETTING)
      @model.stubs(:via_reference_id).returns(setting_id)
      name_stub = stub
      name_stub.expects(:name).returns(setting_name)
      AccountSetting.stubs(:find).with(setting_id).returns(name_stub)
      @output = serialized_via_object(@model)
    end

    it "has the correct 'via data structure" do
      assert_equal({channel: :system, source: {to: {}, from: {id: 10051, title: 'Assign Tickets Upon Solve'}, rel: :admin_setting}}, @output)
    end
  end

  describe "via USER_MERGE" do
    it 'returns the correct rel' do
      Zendesk::Types::ViaType.USER_MERGE
      @model = tickets(:minimum_1)
      @model.stubs(:via_id).returns(Zendesk::Types::ViaType.USER_MERGE)
      expected = { from: {}, to: {}, rel: :user_merge }
      assert_equal expected, serialized_via_object(@model)[:source]
    end
  end

  describe "comment audit does not exist" do
    describe "with a null audit" do
      before do
        @model = tickets("minimum_1").comments.first
        @model.stubs(:via_id).returns(Zendesk::Types::ViaType.API_PHONE_CALL_OUTBOUND)
        @model.stubs(:audit).returns(nil)
      end

      it "handles a null audit" do
        output = serialized_via_object(@model)
        assert_equal({ channel: :api, source: { rel: :outbound } }, output)
      end
    end

    describe "when event is null" do
      it "returns nil if no event is provided" do
        assert_nil serialized_via_object(nil)
      end
    end
  end

  describe 'find_ticket' do
    describe 'on a ticket' do
      it 'returns the ticket' do
        ticket = tickets("minimum_1")
        assert_equal ticket, find_ticket(ticket)
      end
    end

    describe 'on an audit' do
      before { @ticket = tickets("minimum_1") }

      it 'returns a loaded event.ticket' do
        audit = @ticket.events.last.audit
        audit.ticket
        assert audit.association(:ticket).loaded?

        assert_sql_queries 0 do
          res = find_ticket(audit)
          assert_equal @ticket, res
        end
      end
    end

    describe 'on an event' do
      before { @ticket = tickets("minimum_1") }

      describe "with event_via_ticket_cache arturo enabled" do
        before { Arturo.enable_feature!(:event_via_ticket_cache) }
        it 'retrieves ticket from the cache if ticket association not loaded' do
          Arturo.enable_feature!(:event_via_ticket_cache)
          ticket_audits = Audit.where(ticket_id: @ticket.id)
          assert ticket_audits.count > 1
          audit_1 = ticket_audits.first
          audit_2 = ticket_audits.last
          ticket_audits.map { |a| refute a.association(:ticket).loaded? }

          assert_sql_queries 2 do
            find_ticket(audit_1) # all_with_archived issues 2 sql queries to get the ticket(tickets/ticket_archive_stubs)
            find_ticket(audit_2) # reads from ticket cache
          end.join("\n")
        end
      end

      describe "with event_via_ticket_cache arturo disabled" do
        before { Arturo.disable_feature!(:event_via_ticket_cache) }
        it 'does not get ticket from the cache if ticket association not loaded' do
          ticket_audits = Audit.where(ticket_id: @ticket.id)
          assert ticket_audits.count > 1
          audit_1 = ticket_audits.first
          audit_2 = ticket_audits.last
          ticket_audits.map { |a| refute a.association(:ticket).loaded? }

          assert_sql_queries 4 do
            find_ticket(audit_1) # all_with_archived issues 2 sql queries to get the ticket(tickets/ticket_archive_stubs)
            find_ticket(audit_2)
          end.join("\n")
        end
      end

      it 'returns a loaded event.ticket' do
        event = @ticket.events.last
        event.ticket
        assert event.association(:ticket).loaded?
        res = nil

        assert_sql_queries 0 do
          res = find_ticket(event)
          assert_equal @ticket, res
        end.join("\n")
      end

      it 'returns soft deleted ticket even if event.ticket is nil and ticket association is loaded' do
        event = @ticket.events.last
        event.ticket
        assert event.association(:ticket).loaded?

        options[:deleted_tickets_lookup] = true
        @ticket.will_be_saved_by @ticket.account.owner
        @ticket.soft_delete!
        event.ticket = nil
        event.ticket_id = @ticket.id

        res = find_ticket(event)
        assert_equal @ticket, res
      end

      it 'loads the ticket from archive & deleted tickets if ticket is not loaded and deleted_tickets_lookup is true' do
        options[:deleted_tickets_lookup] = true
        @ticket.will_be_saved_by @ticket.account.owner
        @ticket.soft_delete!

        event = @ticket.events.last
        refute event.association(:ticket).loaded?
        res = nil

        queries = sql_queries { res = find_ticket(event) }

        queries.must_include "SELECT `tickets`.* FROM `tickets` WHERE `tickets`.`account_id` = #{@ticket.account_id} AND `tickets`.`id` = #{@ticket.id}"
        queries.must_include "SELECT `ticket_archive_stubs`.* FROM `ticket_archive_stubs` WHERE (`ticket_archive_stubs`.`account_id` = #{@ticket.account_id} AND `ticket_archive_stubs`.`id` = #{@ticket.id})"

        assert_equal @ticket, res
      end

      it 'attempts to load the ticket from archive & deleted tickets if ticket is not loaded and deleted_tickets_lookup is true \
          using audit.ticket_id if the ticket could not be found by id and
          feature flag attempt_to_find_ticket_by_ticket_id_from_audit is enabled' do
        options[:deleted_tickets_lookup] = true
        @ticket.will_be_saved_by @ticket.account.owner
        @ticket.soft_delete!

        event = @ticket.events.last
        refute event.association(:ticket).loaded?
        res = nil

        event.ticket_id = @ticket.nice_id         # setting event's ticket_id to ticket's nice_id
        event.audit.ticket_id = @ticket.id        # setting event's audit's ticket_id to ticket's id

        # enabling the feature flag
        account.stubs(:has_attempt_to_find_ticket_by_ticket_id_from_audit?).returns(true)

        queries = sql_queries { res = find_ticket(event) }

        queries.must_include "SELECT `tickets`.* FROM `tickets` WHERE `tickets`.`account_id` = #{@ticket.account_id} AND `tickets`.`id` = #{event.ticket_id}"
        queries.must_include "SELECT `ticket_archive_stubs`.* FROM `ticket_archive_stubs` WHERE (`ticket_archive_stubs`.`account_id` = #{@ticket.account_id} AND `ticket_archive_stubs`.`id` = #{event.ticket_id})"

        queries.must_include "SELECT `tickets`.* FROM `tickets` WHERE `tickets`.`account_id` = #{@ticket.account_id} AND `tickets`.`id` = #{event.audit.ticket_id}"
        queries.must_include "SELECT `ticket_archive_stubs`.* FROM `ticket_archive_stubs` WHERE (`ticket_archive_stubs`.`account_id` = #{@ticket.account_id} AND `ticket_archive_stubs`.`id` = #{event.audit.ticket_id})"

        assert_equal @ticket, res
      end

      it 'if ticekt is not loaded and deleted_ticket_lookup is true \
          and ticket could not be found using event.ticekt_id \
          increase Datadog counter' do
        options[:deleted_tickets_lookup] = true
        @ticket.will_be_saved_by @ticket.account.owner
        @ticket.soft_delete!

        event = @ticket.events.last
        refute event.association(:ticket).loaded?

        event.ticket_id = @ticket.nice_id         # setting event's ticket_id to ticket's nice_id
        event.audit.ticket_id = @ticket.id        # setting event's audit's ticket_id to ticket's id

        # enabling the feature flag
        account.stubs(:has_attempt_to_find_ticket_by_ticket_id_from_audit?).returns(true)

        # stubbing and setting expections on statsd_client
        statsd_client = stub_for_statsd
        Zendesk::StatsD::Client.stubs(:new).with(namespace: %w[presenter Api_V2_Tickets_EventViaPresenter]).returns(statsd_client)
        statsd_client.expects(:increment).with(
          "find_ticket_by_ticket_id.fail", tags: ["account_id:#{account.id}"]
        ).once

        find_ticket(event)
      end

      it 'loads the ticket from archive if ticket is not loaded and deleted_tickets_lookup is not set' do
        refute options[:deleted_tickets_lookup]

        event = @ticket.events.last
        refute event.association(:ticket).loaded?
        res = nil

        queries = assert_sql_queries(3) { res = find_ticket(event) }

        queries.must_include "SELECT `tickets`.* FROM `tickets` WHERE (tickets.status_id NOT IN (5,7)) AND `tickets`.`account_id` = #{@ticket.account_id} AND `tickets`.`id` = #{@ticket.id}"
        queries.must_include "SELECT `ticket_archive_stubs`.* FROM `ticket_archive_stubs` WHERE ((`ticket_archive_stubs`.status_id NOT IN (5,7)) AND `ticket_archive_stubs`.`account_id` = #{@ticket.account_id} AND `ticket_archive_stubs`.`id` = #{@ticket.id})"

        assert_equal @ticket, res
      end
    end
  end

  describe 'get_voice_comment_from_resource' do
    before do
      @ticket = tickets("minimum_1")

      recording_sid = "RE5d4706b2a7ce9c6085119352bc3f96c"
      recording_url = "http://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/#{recording_sid}"
      call = mock('Call')
      call_id = 7890
      agent = users(:minimum_agent)

      call.stubs(:voice_comment_data).returns(
        recording_url: recording_url,
        recording_duration: "7",
        transcription_text: "foo: hi\nbar: bye\n",
        transcription_status: "completed",
        answered_by_id: agent.id,
        call_id: call_id,
        from: '+11234567890',
        to: '+11234567890',
        started_at: 'June 04, 2014 05:10:27 AM',
        outbound: false
      )
      call.stubs(:account_id).returns(@account.id)
      @ticket.add_voice_comment(call,
        author_id: agent.id,
        body: "Call recording")
      @ticket.will_be_saved_by(agent)

      @ticket.save!

      @voice_comment = @ticket.comments.find { |c| c.is_a?(VoiceComment) }
      refute_nil @voice_comment
    end

    describe 'without has_voice_via_only_voice_comments' do
      before { @account.stubs(:has_voice_via_only_voice_comments?).returns(false) }

      describe 'with archived ticket' do
        before do
          archive_and_delete(@ticket)
          @ticket = Ticket.find(@ticket.id)
          assert @ticket.archived?
        end

        it 'works with archived tickets' do
          assert_equal @voice_comment, get_voice_comment_from_resource(@ticket)
        end

        it 'works with audit from archived ticket' do
          assert_equal @voice_comment, get_voice_comment_from_resource(@ticket.audits.last)
        end
      end

      it 'works with tickets' do
        assert_equal @voice_comment, get_voice_comment_from_resource(@ticket)
      end

      it 'works with audit' do
        assert_equal @voice_comment, get_voice_comment_from_resource(@ticket.audits.last)
      end
    end
  end

  describe '#event_via_presenter_preload_associations preloads all required associations for each item to be presented' do
    let(:num_items) { 2 }
    let(:requester) { users(:minimum_end_user) }

    def assert_associations_are_loaded(items, assoc_key)
      items.each do |item|
        assert item.association(assoc_key).loaded?
      end
    end

    def assert_tickets_are_cached_by_ids(tickets)
      assert @tickets_cache_by_id

      # Assert key exists and value is non-nil.
      assert tickets.all? do |ticket|
        @tickets_cache_by_id[ticket.id].presence
      end
    end

    before do
      @source_ticket1 = tickets(:minimum_1)
      @target_ticket1 = tickets(:minimum_2)
      ticket_merger = Zendesk::Tickets::Merger.new(user: (admin = account.admins.first))
      ticket_merger.merge(
        target:         @target_ticket1,
        sources:        [@source_ticket1],
        target_comment: 'Merged some tickets into this one',
        source_comment: 'This a duplicate'
      )

      @source_ticket2 = tickets(:minimum_3)
      @target_ticket2 = tickets(:minimum_4)
      ticket_merger = Zendesk::Tickets::Merger.new(user: admin)
      ticket_merger.merge(
        target:         @target_ticket2,
        sources:        [@source_ticket2],
        target_comment: 'Merged some tickets into this one',
        source_comment: 'This a duplicate'
      )

      @tickets = [@source_ticket1, @target_ticket1, @source_ticket2, @target_ticket2]
    end

    describe 'via MERGE' do
      it 'when items are Tickets' do
        event_via_presenter_preload_associations(@tickets)
        assert_tickets_are_cached_by_ids(@tickets)
      end

      it 'when items are Events' do
        events = @tickets.map(&:events).flatten.uniq
        event_via_presenter_preload_associations(events)
        assert_tickets_are_cached_by_ids(@tickets)
      end

      it 'when items are Audits' do
        audits = @tickets.map(&:audits).flatten.uniq
        event_via_presenter_preload_associations(audits)
        assert_tickets_are_cached_by_ids(@tickets)
      end
    end

    describe 'via MAIL' do
      def create_tickets(via_id, count)
        tickets_array = FactoryBot.create_list(
          :ticket,
          count,
          account: account,
          requester: requester,
          via_id: via_id
        )

        # Re-select to reset eagerly/lazily fetched associations.
        Ticket.where(id: tickets_array.map(&:id))
      end

      # The name must be different from the fixture method name #tickets
      let(:new_tickets) { create_tickets(ViaType.MAIL, num_items) }

      def assert_associations_from_tickets_are_loaded(tickets)
        assert_associations_are_loaded(tickets, :requester)

        requesters = tickets.map(&:requester)
        assert_associations_are_loaded(requesters, :identities)
      end

      describe 'when items are Tickets' do
        it 'preloads associations' do
          event_via_presenter_preload_associations(new_tickets)

          assert_tickets_are_cached_by_ids(new_tickets)
          assert_associations_from_tickets_are_loaded(new_tickets)
        end

        describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it 'does not preload audit associations' do
              expects(:load_audit_recipients_users).never
              event_via_presenter_preload_associations(new_tickets)
            end
          end

          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it 'preloads audit associations' do
              expects(:load_audit_recipients_users)
              event_via_presenter_preload_associations(new_tickets)
            end
          end
        end

        describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
          it 'does not preload audit associations' do
            expects(:load_audit_recipients_users).never
            event_via_presenter_preload_associations(new_tickets)
          end
        end
      end

      describe 'when items are Events' do
        it 'preloads associations' do
          events = new_tickets.map do |ticket|
            comment = ticket.add_comment(body: 'Foo', via_id: ViaType.MAIL, via_reference_id: ticket.id, author: requester)
            ticket.will_be_saved_by(requester)
            ticket.save!
            comment
          end

          event_via_presenter_preload_associations(events)

          assert_tickets_are_cached_by_ids(new_tickets)
          assert_associations_are_loaded(events, :ticket)
          assert_associations_from_tickets_are_loaded(events.map(&:ticket))
        end
      end

      describe 'when items are Audits' do
        let(:audits) do
          new_tickets.map do |ticket|
            comment = ticket.add_comment(body: 'Foo', via_id: ViaType.MAIL, via_reference_id: ticket.id, author: requester)
            ticket.will_be_saved_by(requester)
            ticket.save!
            comment.audit
          end
        end

        it 'preloads associations' do
          event_via_presenter_preload_associations(audits)

          assert_tickets_are_cached_by_ids(new_tickets)
          assert_associations_are_loaded(audits, :ticket)
          assert_associations_from_tickets_are_loaded(audits.map(&:ticket))
        end

        describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it 'does not preload NotificationWithCcs associations' do
              expects(:load_audit_recipients_users).never
              event_via_presenter_preload_associations(audits)
            end
          end

          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it 'preloads NotificationWithCcs associations' do
              expects(:load_audit_recipients_users)
              event_via_presenter_preload_associations(audits)
            end
          end
        end

        describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
          it 'does not preload NotificationWithCcs associations' do
            expects(:load_audit_recipients_users).never
            event_via_presenter_preload_associations(audits)
          end
        end
      end
    end
  end

  describe '#lookup_via_ticket_association' do
    it 'return key from @tickets_cache_by_id' do
      stub = stub(id: 123)
      cache_tickets_by_ids([stub])
      assert_equal stub, lookup_via_ticket_association(123)
    end
  end

  describe "#audit_recipients" do
    let(:ticket) { tickets(:minimum_1) }
    let(:event) { Audit.new { |a| a.account = @account; a.ticket = ticket; a.recipients = "test1 test2"; a.via_id = ticket.via_id } }
    let(:event_via_presenter) { Api::V2::Tickets::AuditPresenter.new(event, url_builder: '') }
    let(:audit_recipients) { event_via_presenter.send(:audit_recipients, event) }

    it "calls Audit#recipients_list" do
      Audit.any_instance.expects(:recipients_list)
      audit_recipients
    end

    describe "when event is not an Audit" do
      let(:event) { ticket }

      it { refute audit_recipients }
    end
  end

  describe "#audit_recipients_preload" do
    let(:non_ticket_objects) { [] }
    let(:ticket_objects) { [] }

    subject { audit_recipients_preload(non_ticket_objects, ticket_objects) }

    describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
      it { subject.must_be_nil }
    end

    describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
        it { subject.must_be_nil }
      end

      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        it "calls load_audit_recipients_users" do
          expects(:load_audit_recipients_users)
          subject
        end

        describe "when load_audit_recipients_users returns a map of users" do
          let(:recipient) { users(:minimum_end_user) }

          before do
            stubs(load_audit_recipients_users: { recipient.id => recipient })
          end

          it { subject.keys.must_include recipient.id }
        end
      end
    end
  end

  describe "#load_audit_recipients_users" do
    let(:account) { accounts(:minimum) }
    let(:non_ticket_objects) { nil }
    let(:ticket_objects) { nil }
    let(:audit) { nil }
    let(:ticket) { tickets(:minimum_1) }

    subject { load_audit_recipients_users(account, non_tickets: non_ticket_objects, tickets: ticket_objects, audit: audit) }

    describe "with multiple non_tickets/audits supplied" do
      let(:non_ticket_objects) { ticket.audits }
      let(:audit_recipients_users_output) { [users(:minimum_end_user), users(:minimum_search_user), users(:minimum_agent)].index_by(&:id) }

      it "loads all of the users associated with the supplied audits" do
        expects(audit_recipients_users: audit_recipients_users_output).with(account, non_ticket_objects, nil)
        loaded_recipient_users = subject
        audit_recipients_users_output.keys.each do |user_id|
          assert loaded_recipient_users.key?(user_id)
        end
      end
    end

    describe "with multiple tickets supplied" do
      let(:ticket_objects) { [ticket, tickets(:minimum_2)] }
      let(:audit_recipients_users_output) { [users(:minimum_end_user), users(:minimum_search_user), users(:minimum_agent)].index_by(&:id) }

      it "loads all of the users associated with the supplied audits" do
        expects(audit_recipients_users: audit_recipients_users_output).with(account, nil, ticket_objects)
        loaded_recipient_users = subject
        audit_recipients_users_output.keys.each do |user_id|
          assert loaded_recipient_users.key?(user_id)
        end
      end
    end

    describe "with an audit supplied" do
      let(:audit) { ticket.audits[0] }
      let(:audit_recipients_users_output) { [users(:minimum_end_user), users(:minimum_search_user)].index_by(&:id) }
      let(:audit_recipients_ids_output) { audit_recipients_users_output.keys }

      before do
        expects(audit_recipients_ids: audit_recipients_ids_output).with(account, [audit], nil)
        expects(audit_recipients_users: audit_recipients_users_output).with(account, [audit], [])
        @loaded_recipient_users = subject
      end

      it "loads all of the users associated with the supplied audits" do
        audit_recipients_users_output.keys.each do |user_id|
          assert @loaded_recipient_users.key?(user_id)
        end
      end

      describe "and then another audit is supplied" do
        let(:another_recipient) { users(:minimum_agent) }
        let(:another_audit) { Audit.new { |a| a.account = @account; a.ticket = ticket; a.recipients = another_recipient.email; a.via_id = ticket.via_id } }

        before do
          unstub(:audit_recipients_ids)
          unstub(:audit_recipients_users)
          expects(audit_recipients_ids: [another_recipient.id]).with(account, [another_audit], nil)
          expects(audit_recipients_users: [another_recipient].index_by(&:id)).with(account, [another_audit], [])
          load_audit_recipients_users(account, non_tickets: nil, tickets: nil, audit: another_audit)
        end

        it "contains previously loaded users" do
          assert @loaded_recipient_users.key?(audit_recipients_users_output.keys.first)
        end

        it "contains newly loaded users from the supplied audit" do
          assert @loaded_recipient_users.key?(another_recipient.id)
        end
      end

      describe "and then the same audit is supplied" do
        it "does not query for the users again" do
          unstub(:audit_recipients_users)
          expects(:audit_recipients_users).never
          subject
        end
      end
    end
  end

  describe "#audit_recipients_users_with_ids" do
    let(:user) { users(:minimum_end_user) }
    let(:ids) { [user.id] }

    subject { audit_recipients_users_with_ids(ids) }

    describe "when the user with a requested ID has already been loaded" do
      before do
        @audit_recipients_users = [user].index_by(&:id)
      end

      it "does not query for the user" do
        assert_sql_queries 0 do
          subject
        end
      end

      it "returns the user" do
        subject.must_include user
      end
    end

    describe "when the user with a requested ID has not already been loaded" do
      it "queries for the user" do
        # trigger initial query for the local user variable
        ids

        assert_sql_queries 1 do
          subject
        end
      end

      it "returns the user" do
        subject.must_include user
      end
    end
  end

  describe "#add_email_ccs" do
    let(:account) { accounts(:minimum) }
    let(:to) { {} }
    let(:recipient) { users(:minimum_end_user) }
    let(:ticket) { tickets(:minimum_1) }
    let(:event) { Audit.new { |a| a.account = @account; a.ticket = ticket; a.recipients = recipient.email; a.via_id = ticket.via_id } }

    subject { add_email_ccs(to, event) }

    describe "when event is a Ticket" do
      let(:event) { tickets(:minimum_1) }

      it { subject.must_be_nil }
    end

    describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
      it "loads the users if needed" do
        expects(:load_audit_recipients_users)
        subject
      end

      it "uses the CcsAndFollowersHelper's recipients_ids_or_identifiers instead of the Audit's" do
        expects(:recipients_ids_or_identifiers)
        Audit.any_instance.expects(:recipients_ids_or_identifiers).never
        subject
      end

      describe "when recipients_ids_or_identifiers contains values" do
        before do
          stubs(recipients_ids_or_identifiers: [recipient.id]).with(account, event, {})
        end

        it "populates the to object's email_ccs with the recipient user IDs" do
          subject
          to[:email_ccs].must_include recipient.id
        end
      end
    end

    describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
      it "uses the Audit's recipients_ids_or_identifiers instead of the CcsAndFollowersHelper's" do
        expects(:recipients_ids_or_identifiers).never
        Audit.any_instance.expects(:recipients_ids_or_identifiers)
        subject
      end

      describe "when the Audit's recipients_ids_or_identifiers contains values" do
        before do
          Audit.any_instance.stubs(recipients_ids_or_identifiers: [recipient.id])
        end

        it "populates the to object's email_ccs with the recipient user IDs" do
          subject
          to[:email_ccs].must_include recipient.id
        end
      end
    end
  end

  describe "#event_requester" do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }
    let(:recipients_notification_to) { users(:minimum_end_user) }
    let(:recipients_notification_to_email) { recipients_notification_to.email }
    let(:event) { Audit.new { |a| a.account = @account; a.ticket = ticket; a.recipients = recipients_notification_to_email; a.via_id = ticket.via_id } }

    subject { event_requester(event) }

    let(:audit_recipients_notification_to) { recipients_notification_to }

    before do
      Audit.any_instance.stubs(recipients_notification_to: audit_recipients_notification_to)
    end

    describe "when event is a ticket" do
      let(:event) { ticket }

      it { subject.must_be_nil }
    end

    describe "when event is an audit" do
      describe_with_arturo_disabled :email_ccs_preload_notifications_with_ccs do
        describe "when the audit's recipients_notification_to returns a user" do
          it { subject.must_equal recipients_notification_to }
        end

        describe "when the audit's recipients_notification_to returns nil" do
          let(:recipients_notification_to) { nil }
          let(:recipients_notification_to_email) { "" }

          it { subject.must_be_nil }
        end
      end

      describe_with_arturo_enabled :email_ccs_preload_notifications_with_ccs do
        let(:audit_recipients_notification_to) { recipients_notification_to.id }

        it "loads the users if needed" do
          expects(:load_audit_recipients_users)
          subject
        end

        it "uses the CcsAndFollowersHelper's audit_recipients_users_with_ids" do
          expects(audit_recipients_users_with_ids: [])
          subject
        end

        describe "when audit_recipients_users_with_ids returns a user ID" do
          before do
            stubs(audit_recipients_users_with_ids: [recipients_notification_to]).with([recipients_notification_to.id])
          end

          it { subject.must_equal recipients_notification_to }
        end

        describe "when audit_recipients_users_with_ids returns an empty list" do
          before do
            stubs(audit_recipients_users_with_ids: []).with([recipients_notification_to.id])
          end

          it { subject.must_be_nil }
        end
      end
    end

    describe "when event is a suspended ticket" do
      let(:event) { suspended_tickets(:minimum_spam) }

      it { subject.must_be_nil }
    end
  end
end
