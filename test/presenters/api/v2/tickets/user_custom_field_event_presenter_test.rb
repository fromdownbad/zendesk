require_relative "../../../../support/test_helper"
require_relative "../../../../support/rules_test_helper"

SingleCov.covered!

describe Api::V2::Tickets::UserCustomFieldEventPresenter do
  extend Api::Presentation::TestHelper
  include CustomFieldsTestHelper
  include RulesTestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @requester = User.find(@ticket.requester.id)

    create_user_custom_field!("field_decimal", "decimal field", "Decimal")
    new_rule(DefinitionItem.new("requester.custom_fields.field_decimal", nil, -10.0.to_s)).apply_actions(@ticket)
    @ticket.save!

    cfv = @requester.custom_field_values.value_for_key("field_decimal")
    ref_hash = {
      object_id: cfv.id, object_type: "CustomField::Value",
      info: { cf_field_id: cfv.field.id, owner_id: cfv.owner_id }
    }

    @model = UserCustomFieldEvent.create!(
      account: @account,
      ticket: @ticket,
      audit: @ticket.audits.last,
      author: @account.owner,
      value: -10.0,
      value_previous: 123.45,
      value_reference: ref_hash
    )

    @presenter = Api::V2::Tickets::UserCustomFieldEventPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :field_name, :owner_type, :owner_name, :action_type, :value, :value_previous

  should_present_method :id

  it "presents correctly" do
    json = {
      id: @ticket.reload.audits.last.events.last.id, type: "UserCustomFieldEvent",
      field_name: "decimal field", owner_type: "User", owner_name: "Author Minimum",
      action_type: "Change", value: -10.0, value_previous: 123.45
    }
    assert_equal json, @presenter.model_json(@model)
  end
end
