require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::CommentPrivacyChangePresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @comment = @ticket.comments.first
    @model = CommentPrivacyChange.create!(
      account: @account,
      ticket: @ticket,
      audit: @comment.audit,
      value: '1',
      value_reference: @comment.id
    )

    @presenter = Api::V2::Tickets::CommentPrivacyChangePresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :comment_id, :public

  should_present_method :id
  should_present_method :event_id, as: :comment_id
  should_present "CommentPrivacyChange", as: :type

  it "presents value as a boolean" do
    @model.value = '0'
    json = @presenter.model_json(@model)
    assert_equal false, json[:public]

    @model.value = '1'
    json = @presenter.model_json(@model)
    assert(json[:public])
  end
end
