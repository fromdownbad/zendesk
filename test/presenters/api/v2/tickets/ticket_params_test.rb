require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::TicketParams do
  def self.should_map(name, options)
    from_name = name
    to_name   = options.delete(:to)

    from_value = options.keys.first
    to_value   = options.values.first

    if from_value.nil?
      from_value = to_value = :value
    end

    it "maps #{from_name.inspect} #{from_value.inspect} to #{to_name.inspect} #{to_value.inspect}" do
      params = Api::V2::Tickets::TicketParams.new(ticket: {from_name => from_value}).to_hash
      assert_equal to_value, params[:ticket][to_name]
    end
  end

  should_map :status, :to => :status_id, 'new'     => Zendesk::Types::StatusType.NEW.to_s
  should_map :status, :to => :status_id, 'open'    => Zendesk::Types::StatusType.OPEN.to_s
  should_map :status, :to => :status_id, 'pending' => Zendesk::Types::StatusType.PENDING.to_s
  should_map :status, :to => :status_id, 'solved'  => Zendesk::Types::StatusType.SOLVED.to_s
  should_map :status, :to => :status_id, 'closed'  => Zendesk::Types::StatusType.CLOSED.to_s
  should_map :status, :to => :status_id, 'deleted' => Zendesk::Types::StatusType.DELETED.to_s

  should_map :priority, :to => :priority_id, 'low'    => Zendesk::Types::PriorityType.LOW.to_s
  should_map :priority, :to => :priority_id, 'normal' => Zendesk::Types::PriorityType.NORMAL.to_s
  should_map :priority, :to => :priority_id, 'high'   => Zendesk::Types::PriorityType.HIGH.to_s
  should_map :priority, :to => :priority_id, 'urgent' => Zendesk::Types::PriorityType.URGENT.to_s

  should_map :type, :to => :ticket_type_id, 'question' => Zendesk::Types::TicketType.QUESTION.to_s
  should_map :type, :to => :ticket_type_id, 'incident' => Zendesk::Types::TicketType.INCIDENT.to_s
  should_map :type, :to => :ticket_type_id, 'problem'  => Zendesk::Types::TicketType.PROBLEM.to_s
  should_map :type, :to => :ticket_type_id, 'task'     => Zendesk::Types::TicketType.TASK.to_s

  it "maps `priority => nil` to the default ticket priority" do
    params = Api::V2::Tickets::TicketParams.new(ticket: { priority: nil }).to_hash
    assert_equal Zendesk::Types::PriorityType.find!("-").to_s, params[:ticket][:priority_id]
  end

  it "maps `type => nil` to the default ticket type" do
    params = Api::V2::Tickets::TicketParams.new(ticket: { type: nil }).to_hash
    assert_equal Zendesk::Types::TicketType.find!("-").to_s, params[:ticket][:ticket_type_id]
  end

  should_map :via_type,       to: :via_id
  should_map :forum_topic_id, to: :entry_id
  should_map :due_at,         to: :due_date
  should_map :custom_fields,  to: :fields
  should_map :recipient,      to: :original_recipient_address
end
