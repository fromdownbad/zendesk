require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::FacebookCommentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @model = FacebookComment.create!(account: @account,
                                     ticket: @account.tickets.last,
                                     audit: @account.tickets.last.audits.last,
                                     author: @account.owner,
                                     body: 'value',
                                     data: {content: '15551234567'},
                                     graph_object_id: "abc")

    @presenter = Api::V2::Tickets::FacebookCommentPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :data, :public, :body, :html_body, :graph_object_id, :author_id, :trusted, :attachments, :via

  should_present_method :id
  should_present_method :data, value: {content: '15551234567'}
  should_present_method :is_public?, as: :public
  should_present_method :body
  should_present_method :html_body
  should_present_method :trusted?, as: :trusted
  should_present_method :author_id
  should_present_method :graph_object_id
  should_present_collection :attachments, with: Api::V2::AttachmentPresenter

  should_present "FacebookComment", as: :type
end
