require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::LogMeInTranscriptPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @model = LogMeInTranscript.create!(
      account: @account,
      ticket: @ticket,
      audit: @ticket.audits.last,
      author: @account.owner,
      value: 'value',
      value_previous: 'value_previous',
      value_reference: 'value_reference'
    )

    @presenter = Api::V2::Tickets::LogMeInTranscriptPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :body

  should_present_method :id
  should_present_method :chat_transcript, as: :body
  should_present "LogMeInTranscript", as: :type
end
