require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::TranslatableErrorPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account   = accounts(:minimum)
    @ticket    = tickets(:minimum_1)
    @presenter = Api::V2::Tickets::TranslatableErrorPresenter.new(@account.owner, url_builder: mock_url_builder)
    options =  {
      key: "activerecord.errors.rule_failed_to_apply",
      error_field: 'assignee',
      error_type: 'unauthorized',
      rule_id: rules(:trigger_notify_blank_email).id,
      field: :assignee,
      value: users(:minimum_agent).id
    }
    @ticket.will_be_saved_by(users(:minimum_agent))
    @ticket.add_translatable_error_event(options)
    @ticket.save
    @model = @ticket.events.find_by_type('TranslatableError')
  end

  should_present_keys :id, :type, :message

  should_present_method :id
  should_present "Error", as: :type
  should_present I18n.t("activerecord.errors.rule_failed_to_apply", rule_id: '69108', field: 'assignee', value: '57888', error: 'Assignee: unauthorized'), as: :message
end
