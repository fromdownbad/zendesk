require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::FollowerChangePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  let(:followers) { [users(:minimum_admin).email] }

  before do
    @account = accounts(:minimum)
    @model = FollowerChange.new.tap do |change|
      change.current_followers = followers
      change.previous_followers = []
    end

    @presenter = Api::V2::Tickets::FollowerChangePresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :current_followers, :previous_followers, :via

  should_present_method :id
  should_present "FollowerChange", as: :type

  it "presents current_followers" do
    json = @presenter.model_json(@model)
    assert_equal followers, json[:current_followers]
  end

  it "presents previous_followers" do
    json = @presenter.model_json(@model)
    assert_equal [], json[:previous_followers]
  end
end
