require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::PredictionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @ticket = tickets(:minimum_1)
    @model = @ticket.create_ticket_prediction(satisfaction_probability: 0.1)
    @account = accounts(:minimum)
    @presenter = Api::V2::Tickets::PredictionPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :satisfaction_probability, :ticket_id, :account_id, :created_at, :updated_at
end
