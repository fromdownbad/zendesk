require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered! uncovered: 1

describe Api::V2::Tickets::AuditEventCollectionPresenter do
  extend Api::Presentation::TestHelper
  include TestSupport::Rule::Helper
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }
  let(:admin)   { users(:minimum_admin) }

  describe '#item_presenter' do
    let(:rule) { create_trigger }

    before do
      @presenter = Api::V2::Tickets::AuditEventCollectionPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
    end

    def self.should_return(presenter, options)
      it "returns a #{presenter.name} for a #{options[:for].name}" do
        model = options[:for].new
        assert_instance_of presenter, @presenter.item_presenter(model)
      end
    end

    should_return Api::V2::Tickets::CommentPresenter,                 for: Comment
    should_return Api::V2::Tickets::VoiceCommentPresenter,            for: VoiceComment
    should_return Api::V2::Tickets::NotificationPresenter,            for: Notification
    should_return Api::V2::Tickets::OrganizationActivityPresenter,    for: OrganizationActivity
    should_return Api::V2::Tickets::CcPresenter,                      for: Cc
    should_return Api::V2::Tickets::ChangePresenter,                  for: Change
    should_return Api::V2::Tickets::CreatePresenter,                  for: Create
    should_return Api::V2::Tickets::AuditEventPresenter,              for: AuditTrustChange
    should_return Api::V2::Tickets::AuditEventPresenter,              for: CommentRedactionEvent
    should_return Api::V2::Tickets::AuditEventPresenter,              for: AttachmentRedactionEvent
    should_return Api::V2::Tickets::CommentPrivacyChangePresenter,    for: CommentPrivacyChange
    should_return Api::V2::Tickets::ErrorPresenter,                   for: Error
    should_return Api::V2::Tickets::ExternalPresenter,                for: External
    should_return Api::V2::Tickets::LogMeInTranscriptPresenter,       for: LogMeInTranscript
    should_return Api::V2::Tickets::MacroReferencePresenter,          for: MacroReference
    should_return Api::V2::Tickets::PushPresenter,                    for: Push
    should_return Api::V2::Tickets::TwitterEventPresenter,            for: Tweet
    should_return Api::V2::Tickets::TwitterEventPresenter,            for: TwitterDmEvent
    should_return Api::V2::Tickets::TicketSharingEventPresenter,      for: TicketSharingEvent
    should_return Api::V2::Tickets::TicketSharingEventPresenter,      for: TicketUnshareEvent
    should_return Api::V2::Tickets::SatisfactionRatingEventPresenter, for: SatisfactionRatingEvent
    should_return Api::V2::Tickets::FacebookEventPresenter,           for: FacebookEvent
    should_return Api::V2::Tickets::SlaTargetChangePresenter,         for: SlaTargetChange
    should_return Api::V2::WorkspacePresenter,                        for: Workspace
    should_return Api::Lotus::ConversationItemPresenter,              for: AutomaticAnswerSend
    should_return Api::Lotus::ConversationItemPresenter,              for: AutomaticAnswerSend
    should_return Api::Lotus::ConversationItemPresenter,              for: AutomaticAnswerReject
    should_return Api::Lotus::KnowledgeEventPresenter,                for: KnowledgeLinked
    should_return Api::Lotus::KnowledgeEventPresenter,                for: KnowledgeCaptured
    should_return Api::Lotus::KnowledgeEventPresenter,                for: KnowledgeFlagged
    should_return Api::Lotus::KnowledgeEventPresenter,                for: KnowledgeLinkAccepted
    should_return Api::Lotus::KnowledgeEventPresenter,                for: KnowledgeLinkRejected
    should_return Api::V2::Tickets::EmailCcChangePresenter,           for: EmailCcChange
    should_return Api::V2::Tickets::FollowerChangePresenter,          for: FollowerChange
    should_return Api::V2::Tickets::FollowerNotificationPresenter,    for: FollowerNotification
    should_return Api::Lotus::WorkspaceChangedPresenter,              for: WorkspaceChanged
    should_return Api::V2::Tickets::ChatEventPresenter,               for: ChatStartedEvent
    should_return Api::V2::Tickets::ChatEventPresenter,               for: ChatEndedEvent
    should_return Api::V2::Tickets::MessagingCsatEventPresenter,      for: MessagingCsatEvent

    describe '#preload_associations' do
      let(:macro) { create_macro }

      before do
        @events = ticket.events

        @events[0].via_id = Zendesk::Types::ViaType.RULE
        @events[0].via_reference_id = rule.id

        @rule_revision = rule.revisions.last
        @events[1].via_id = Zendesk::Types::ViaType.RULE_REVISION
        @events[1].via_reference_id = @rule_revision.id

        @events[2].via_id = Zendesk::Types::ViaType.MERGE
        @events[2].via_reference_id = ticket.id

        # To create audit or else inserting new event fails
        ticket.will_be_saved_by(admin)

        @events << AgentMacroReference.new(
          via_id:           Zendesk::Types::ViaType.WEB_SERVICE,
          via_reference_id: '',
          value:            macro.id,
          audit:            ticket.audit
        )

        create = @events.select { |e| e.is_a?(Create) }[0]
        create.field_name = 'linked_id'
        create.value = ticket.id.to_s

        change = @events.select { |e| e.is_a?(Change) }[0]
        change.field_name = 'linked_id'
        change.value = tickets(:minimum_2).id.to_s
        change.value_previous = tickets(:minimum_1).id.to_s

        @presenter.preload_associations(@events)
      end

      it 'preloads attachments on comments' do
        comments = @events.select { |e| e.is_a?(Comment) }
        assert comments.any?
        assert comments.all? { |comment| comment.attachments.loaded? }
      end

      it 'preloads via associations' do
        assert @presenter.options[:event_via_associations]
      end

      it 'preloads rules' do
        assert_equal({ rule.id => rule }, @presenter.options[:event_via_associations][:rules])
      end

      it 'preloads rule revisions' do
        assert_equal(
          {@rule_revision.id => @rule_revision},
          @presenter.options[:event_via_associations][:rule_revisions]
        )
      end

      it 'preloads macros' do
        assert_equal(
          { macro.id => macro },
          @presenter.options[:event_via_associations][:macros]
        )
      end

      it 'preloads tickets' do
        assert_equal({ ticket.id => ticket }, @presenter.options[:event_via_associations][:tickets])
      end

      it 'preloads problem ticket associations' do
        assert_equal({
          tickets(:minimum_1).id => tickets(:minimum_1),
          tickets(:minimum_2).id => tickets(:minimum_2)
        }, @presenter.options[:problem_ticket_associations])
      end
    end
  end
end
