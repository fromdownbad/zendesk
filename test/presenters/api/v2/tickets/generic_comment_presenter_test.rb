require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::GenericCommentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :events, :users, :accounts, :tickets

  let(:options) { { includes: [:test] } }

  describe "present with Comment presenter" do
    before do
      @model = events(:serialization_comment)
      @presenter = Api::V2::Tickets::GenericCommentPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :attachments, :audit_id, :author_id, :body, :created_at, :html_body, :id, :plain_body, :public, :type, :via

    it "has the same side-loads" do
      assert_equal [:test], @presenter.collection_presenter.includes
    end
  end

  describe "present with VoiceComment presenter" do
    before do
      @recording_sid = "RE5d4706b2a7ce9c6085119352bc3f96c"
      @recording_url = "http://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/#{@recording_sid}"
      @call = mock('Call')
      @call_id = 7890
      @agent = users(:minimum_agent)

      @now = 'June 04, 2014 05:10:27 AM'
      @ticket = tickets(:minimum_1)
      @call.stubs(:voice_comment_data).returns(recording_url: @recording_url,
                                               recording_duration: "7",
                                               transcription_text: "foo: hi\nbar: bye\n",
                                               transcription_status: "completed",
                                               call_duration: 126,
                                               answered_by_id: @agent.id,
                                               call_id: @call_id,
                                               from: '+14155556666',
                                               to: '+14155557777',
                                               started_at: @now,
                                               outbound: false)
      @call.stubs(:account_id).returns(accounts(:minimum).id)
      @ticket.add_voice_comment(@call, author_id: accounts(:minimum).agents.last.id,
                                       body: "Call recording")
      @ticket.will_be_saved_by(accounts(:minimum).agents.last)
      @ticket.save!
      @voice_comment = @ticket.reload.voice_comments.last
      @external_recording_url = "#{accounts(:minimum).url}/v2/recordings/#{@call_id}/#{@recording_sid}"

      @model = @voice_comment
      @presenter = Api::V2::Tickets::GenericCommentPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :attachments, :author_id, :body, :created_at, :data, :formatted_from, :formatted_to, :html_body, :id, :public, :transcription_visible, :trusted, :type, :via

    it "has the same side-loads" do
      assert_equal [:test], @presenter.collection_presenter.includes
    end
  end
end
