require_relative '../../../../support/test_helper'
require_relative '../../../../support/collection_presenter_helper'

SingleCov.covered! uncovered: 7

describe Api::V2::Tickets::CommentCollectionPresenter do
  extend Api::Presentation::TestHelper

  include CollectionPresenterHelper

  fixtures :all

  before do
    @model = events(:serialization_comment)
    @collection = [@model]
    @model_key  = :comments
    @user = users(:minimum_agent)
    @item_presenter = Api::V2::Tickets::CommentPresenter.new(@user, url_builder: mock_url_builder)
    @presenter = Api::V2::Tickets::CommentCollectionPresenter.new(@user, url_builder: mock_url_builder,
                                                                         item_presenter: @item_presenter)
  end

  should_side_load :users
  should_present_collection_keys :metadata, :via, :created_at

  describe 'VoiceComment' do
    before do
      recording_sid = "RE5d4706b2a7ce9c6085119352bc3f96c"
      recording_url = "http://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/#{recording_sid}"
      call = mock('Call')
      call_id = 7890
      agent = users(:minimum_agent)

      ticket = accounts(:minimum).tickets.first
      call.stubs(:voice_comment_data).returns(
        recording_url: recording_url,
        recording_duration: "7",
        transcription_text: "foo: hi\nbar: bye\n",
        transcription_status: "completed",
        answered_by_id: agent.id,
        call_id: call_id,
        from: '+11234567890',
        to: '+11234567890',
        started_at: 'June 04, 2014 05:10:27 AM',
        outbound: false
      )
      call.stubs(:account_id).returns(accounts(:minimum).id)
      ticket.add_voice_comment(call,
        author_id: accounts(:minimum).agents.last.id,
        body: "Call recording")
      ticket.will_be_saved_by(accounts(:minimum).agents.last)
      ticket.save!
      @model = ticket.reload.voice_comments.last
      @collection = [@model]
    end

    should_present_collection_keys :data, :formatted_from, :formatted_to, :transcription_visible
  end

  describe "comment decoration data exists" do
    it "includes decoration in metadata" do
      payload = @presenter.present([events(:create_comment_for_minimum_ticket_1)])[:comments].first
      assert payload[:metadata][:decoration]
    end
  end

  describe "comment decoration data exists but no metadata" do
    before do
      @presenter = Api::V2::Tickets::CommentCollectionPresenter.new(@user,       url_builder: mock_url_builder,
                                                                                 item_presenter: @item_presenter,
                                                                                 without_metadata: true)
    end
    it "should not merge on null JSON" do
      payload = @presenter.present([events(:create_comment_for_minimum_ticket_1)])[:comments].first
      refute payload[:metadata]
    end
  end

  describe "comment decoration data does not exist" do
    it "does not include decoration in metadata" do
      payload = @presenter.present([@model])[:comments].first
      refute payload[:metadata].key?(:decoration)
    end
  end

  describe "comment is missing an event" do
    before { @model.stubs(audit: nil) }

    it 'presents comments' do
      assert @presenter.present([@model])[:comments].present?
    end
  end

  describe "via" do
    describe "a missing ticket for a comment" do
      before do
        @model.update_column :via_id, Zendesk::Types::ViaType.MAIL
        @model.ticket.delete
        @model.reload
      end

      it "does not blow up" do
        assert_not_nil @presenter.model_json([@model])[0][:via]
      end
    end
  end

  describe "side-loading users" do
    describe "email ccs" do
      let(:recipients) { "minimum_end_user@aghassipour.com bar@example.com" }
      let(:account) { accounts(:minimum) }
      let(:user) { users(:minimum_agent) }

      def save_ticket
        @ticket = Ticket.new(account: account, description: "testing recipient sideloads")
        @ticket.stubs(:email_cc_addresses).returns(recipients.split(" "))
        @ticket.will_be_saved_by(user, via_id: ViaType.WEB_SERVICE)
        @ticket.save!
        @ticket.reload

        @presenter = Api::V2::Tickets::CommentCollectionPresenter.new(@user, url_builder: mock_url_builder,
                                                                             item_presenter: @item_presenter)
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:users).returns(true)
      end

      before do
        Audit.any_instance.stubs(active_recipients_ids: [users(:minimum_end_user).id])
      end

      describe "when comment_email_ccs_allowed setting is enabled" do
        before do
          Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
        end

        it "side loads recipients" do
          save_ticket
          sideloaded_users = @presenter.present([@ticket.comments.first])[:users]
          assert_equal 2, sideloaded_users.size
          assert_includes sideloaded_users, @presenter.user_presenter.model_json(users(:minimum_end_user))
        end
      end

      describe "when comment_email_ccs_allowed setting is disabled" do
        before do
          Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
        end

        it "does not side load recipients" do
          save_ticket
          sideloaded_users = @presenter.present([@ticket.comments.first])[:users]
          assert_equal 1, sideloaded_users.size
          refute_includes sideloaded_users, @presenter.user_presenter.model_json(users(:minimum_end_user))
        end
      end
    end
  end
end
