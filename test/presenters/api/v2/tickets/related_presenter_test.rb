require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::RelatedPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @model = tickets(:minimum_1)
    @presenter = Api::V2::Tickets::RelatedPresenter.new(@account.owner, url_builder: mock_url_builder)
    @ticket = tickets(:minimum_1)
  end

  should_present_keys :topic_id, :jira_issue_ids, :followup_source_ids, :from_archive, :incidents, :url
end
