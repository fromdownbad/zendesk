require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Tickets::SlaTargetChangePresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :events

  before do
    @account = accounts(:minimum)

    @presenter = Api::V2::Tickets::SlaTargetChangePresenter.new(@account.owner,
      url_builder: mock_url_builder)

    @model = SlaTargetChange.new.tap do |change|
      change.target_info = {
        metric_id:          Zendesk::Sla::TicketMetric::FirstReplyTime.id,
        initial_target:     {minutes: 2, in_business_hours: false},
        final_target:       {minutes: 3, in_business_hours: false},
        current_sla_policy: Hashie::Mash.new(title: 'test policy')
      }

      change.audit = events(:create_audit_for_minimum_ticket_1)
    end
  end

  should_present_keys :id, :type, :previous_value, :value, :field_name, :via

  should_present_method :id

  should_present 'Change', as: :type

  should_present({minutes: 2, in_business_hours: false}, as: :previous_value)
  should_present({minutes: 3, in_business_hours: false}, as: :value)

  should_present Zendesk::Sla::TicketMetric::FirstReplyTime.name,
    as: :field_name

  it 'presents via' do
    assert_equal(
      {source: {rel: 'sla_target_change'}, current_sla_policy: 'test policy'},
      @presenter.model_json(@model)[:via]
    )
  end
end
