require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::ChangePresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account   = accounts(:minimum)
    @ticket    = tickets(:minimum_1)
    @presenter = Api::V2::Tickets::ChangePresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  describe "for a change of the linked problem ticket" do
    before do
      @model = Change.create!(
        account: @account,
        ticket: @ticket,
        audit: @ticket.audits.last,
        author: @account.owner,
        value: tickets(:minimum_2).id,
        value_previous: tickets(:minimum_3).id,
        value_reference: "linked_id"
      )
      @json = @presenter.model_json(@model)
    end

    should_present_keys :id, :type, :field_name, :value, :previous_value

    should_present_method :id
    should_present "Change", as: :type
    should_present "problem_id", as: :field_name

    it "presents the previous problem's nice_id" do
      assert_equal tickets(:minimum_3).nice_id.to_s, @json[:previous_value]
    end

    it "presents the new problem's nice_id" do
      assert_equal tickets(:minimum_2).nice_id.to_s, @json[:value]
    end
  end

  describe "for a change of other ticket attributes" do
    before do
      @model = Change.create!(
        account: @account,
        ticket: @ticket,
        audit: @ticket.audits.last,
        author: @account.owner,
        value: "new subject",
        value_previous: "old subject",
        value_reference: "subject"
      )
    end

    should_present_keys :id, :type, :field_name, :value, :previous_value

    should_present_method :id
    should_present_method :field_name
    should_present_method :value
    should_present_method :value_previous, as: :previous_value
    should_present "Change", as: :type
  end

  # cuz Rails 4 decided to get weird and start storing t/f instead of 1/0 text columns
  describe "with Rails4 ChangeEvent for ticket.is_public" do
    before do
      @model = Change.create!(
        account: @account,
        ticket: @ticket,
        audit: @ticket.audits.last,
        value: "t",
        value_previous: "f",
        value_reference: "is_public",
        author: @account.owner
      )
    end

    it "should present t/f as 1/0" do
      assert_equal "1", @presenter.model_json(@model)[:value]
      assert_equal "0", @presenter.model_json(@model)[:previous_value]
    end
  end
end
