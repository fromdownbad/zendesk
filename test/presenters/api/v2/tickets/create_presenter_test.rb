require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::CreatePresenter do
  extend Api::Presentation::TestHelper

  def self.should_map_value(field_name, from, to)
    describe "mapping #{field_name}" do
      before { @event = stub(field_name: field_name.to_s, value: from.to_s) }

      it "maps #{from} to #{to}" do
        assert_equal_with_nil to, @presenter.mapped_value(@event)
      end
    end
  end

  def self.should_map_field_name(from, to)
    describe "mapping field_name #{from} to #{to}" do
      before { @event = stub(field_name: from.to_s) }

      it "maps #{from} to #{to}" do
        assert_equal to.to_s, @presenter.mapped_field_name(@event)
      end
    end
  end

  fixtures :all

  before do
    @account   = accounts(:minimum)
    @ticket    = tickets(:minimum_1)
    @presenter = Api::V2::Tickets::CreatePresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_map_value :satisfaction_score, SatisfactionType.GOOD,             "good"
  should_map_value :satisfaction_score, SatisfactionType.GOODWITHCOMMENT,  "good"
  should_map_value :satisfaction_score, SatisfactionType.BAD,              "bad"
  should_map_value :satisfaction_score, SatisfactionType.BADWITHCOMMENT,   "bad"
  should_map_value :satisfaction_score, SatisfactionType.OFFERED,          "offered"
  should_map_value :satisfaction_score, SatisfactionType.UNOFFERED,        "unoffered"
  should_map_value :satisfaction_score, "unknown", nil

  should_map_value :ticket_type_id, TicketType.QUESTION, "question"
  should_map_value :ticket_type_id, TicketType.INCIDENT, "incident"
  should_map_value :ticket_type_id, TicketType.PROBLEM, "problem"
  should_map_value :ticket_type_id, TicketType.TASK, "task"
  should_map_value :ticket_type_id, "-", nil

  should_map_value :priority_id, PriorityType.LOW, "low"
  should_map_value :priority_id, PriorityType.NORMAL, "normal"
  should_map_value :priority_id, PriorityType.HIGH, "high"
  should_map_value :priority_id, PriorityType.URGENT, "urgent"
  should_map_value :priority_id, "-", nil

  should_map_value :status_id, StatusType.NEW, "new"
  should_map_value :status_id, StatusType.OPEN, "open"
  should_map_value :status_id, StatusType.PENDING, "pending"
  should_map_value :status_id, StatusType.HOLD, "hold"
  should_map_value :status_id, StatusType.SOLVED, "solved"
  should_map_value :status_id, StatusType.CLOSED, "closed"
  should_map_value :status_id, StatusType.DELETED, "deleted"

  should_map_value :current_tags, "abc def", ["abc", "def"]
  should_map_value :current_tags, "abc", ["abc"]
  should_map_value :current_tags, nil, []

  should_map_value :other, "unknown", "unknown"

  should_map_field_name :satisfaction_score, :satisfaction_score
  should_map_field_name :ticket_type_id, :type
  should_map_field_name :status_id, :status
  should_map_field_name :priority_id, :priority
  should_map_field_name :current_tags, :tags
  should_map_field_name :other_field, :other_field
  should_map_field_name :via_id, :via_type

  should_map_value :via_id, ViaType.WEB_FORM, :web
  should_map_value :via_id, ViaType.RULE, :rule
  should_map_value :via_id, ViaType.GITHUB, :api
  should_map_value :via_id, "400", "400"

  describe "for the linked problem ticket" do
    before do
      @model = Create.create!(
        account: @account,
        ticket: @ticket,
        audit: @ticket.audits.last,
        author: @account.owner,
        value: 1,
        value_reference: "linked_id"
      )

      @presenter.stubs(:problem_ticket_nice_id).with(@model).returns(2)

      @json = @presenter.model_json(@model)
    end

    should_present_keys :id, :type, :field_name, :value

    should_present_method :id
    should_present "Create", as: :type
    should_present "problem_id", as: :field_name

    it "presents the problem's nice_id" do
      assert_equal "2", @json[:value]
    end
  end

  describe "for other ticket attributes" do
    before do
      @model = Change.create!(
        account: @account,
        ticket: @ticket,
        audit: @ticket.audits.last,
        author: @account.owner,
        value: "new subject",
        value_reference: "subject"
      )
    end

    should_present_keys :id, :type, :field_name, :value

    should_present_method :id
    should_present_method :field_name
    should_present_method :value
    should_present "Create", as: :type
  end

  describe "#problem_ticket_nice_id" do
    before do
      @problem_ticket = stub(id: 1, nice_id: 2)
      @model = stub(value: 1)
    end

    describe "without preload optimizations" do
      it "lookups the ticket in the database" do
        Ticket.expects(:find_by_id).with(@problem_ticket.id).returns(@problem_ticket)
        assert_equal @problem_ticket.nice_id, @presenter.problem_ticket_nice_id(@model)
      end
    end

    describe "with preload optimizations" do
      before do
        @presenter.options[:problem_ticket_associations] = {
          @problem_ticket.id => @problem_ticket
        }
      end

      it "lookups the ticket in the database" do
        Ticket.expects(:find_by_id).never
        assert_equal @problem_ticket.nice_id, @presenter.problem_ticket_nice_id(@model)
      end
    end
  end

  describe "with Rails4 CreateEvent for ticket.is_public" do
    before do
      @model = Create.create!(
        account: @account,
        ticket: @ticket,
        audit: @ticket.audits.last,
        value: "t",
        value_previous: nil,
        value_reference: "is_public",
        author: @account.owner
      )
    end

    it "should show t/f as 1/0" do
      assert_equal "1", @presenter.model_json(@model)[:value]
    end
  end
end
