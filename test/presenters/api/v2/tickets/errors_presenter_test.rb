require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Tickets::ErrorsPresenter do
  fixtures :users

  let(:ticket) { users(:minimum_agent).tickets.new }
  let(:presenter) { Api::V2::Tickets::ErrorsPresenter.new }

  describe '#present_attribute_error' do
    it 'details which ticket field is with error and its type' do
      ticket.valid?
      description = ticket.account.field_description

      errors_expected = [
        {
          description: 'Enter your request: cannot be blank',
          error: 'BlankValue',
          ticket_field_id: description.id,
          ticket_field_type: description.type
        }
      ]
      assert_equal errors_expected, presenter.present(ticket)[:details][:base]
    end
  end
end
