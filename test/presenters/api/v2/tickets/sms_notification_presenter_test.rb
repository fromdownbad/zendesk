require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tickets::SmsNotificationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @recipients = @account.end_users
    @trigger = @account.triggers.active.first
    stub_request(:post, %r{/api/v2/channels/sms_app/internal/triggers/messages})
    @model = SmsNotification.create!(
      account: @account,
      ticket: @account.tickets.last,
      audit: @account.tickets.last.audits.last,
      author: @account.owner,
      via_id: Zendesk::Types::ViaType.Rule,
      via_reference_id: @trigger.id,
      value: @recipients.map(&:id),
      value_previous: 'value_previous',
      value_reference: 'value_reference'
    )

    @presenter = Api::V2::Tickets::SmsNotificationPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :type, :body, :recipients

  should_present_method :id
  should_present_method :body
  should_present "SmsNotification", as: :type

  it "presents recipients" do
    json = @presenter.model_json(@model)
    expected_recipients = @recipients.map(&:id)
    assert_equal expected_recipients, json[:recipients]
  end
end
