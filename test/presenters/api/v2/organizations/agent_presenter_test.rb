require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Api::V2::Organizations::AgentPresenter do
  extend Api::Presentation::TestHelper

  def self.should_present_url
    it "presents url using api_v2_organization_url" do
      expected_url = "stub_api_v2_organization_url/#{@model.id}.json"
      @presenter.url_builder.expects(:api_v2_organization_url).with(@model, has_entry(format: :json)).returns(expected_url)
      assert_equal expected_url, @presenter.model_json(@model)[:url]
    end
  end

  fixtures :organizations, :accounts, :users

  before do
    @model     = organizations(:serialization_organization)
    @presenter = Api::V2::Organizations::AgentPresenter.new(@model.account.owner, url_builder: mock_url_builder)
    @presenter.account.stubs(:has_user_and_organization_fields?).returns(false)
  end

  should_present_keys :id, :external_id, :name, :created_at, :updated_at, :domain_names, :details, :notes, :group_id, :shared_tickets, :shared_comments, :organization_fields, :tags, :url

  should_present_method :id
  should_present_method :external_id
  should_present_method :name
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :domain_names
  should_present_method :details
  should_present_method :notes
  should_present_method :group_id
  should_present_method :is_shared?, as: :shared_tickets
  should_present_method :is_shared_comments?, as: :shared_comments

  should_present_url

  describe "when the account has user_and_organization_tags" do
    before { @presenter.account.stubs(:has_user_and_organization_tags?).returns(true) }

    it "includes the organization tags" do
      tagging = Struct.new(:tag_name).new("stub_tag")
      @model.stubs(:taggings).returns([tagging])

      assert_equal [tagging.tag_name], @presenter.model_json(@model)[:tags]
    end

    it "includes taggings, domains, and emails in #association_preloads" do
      assert_equal(
        {
          custom_field_values: {field: {dropdown_choices: {}}},
          taggings: {},
          organization_domains: {},
          organization_emails: {}
        },
        @presenter.association_preloads
      )
    end
  end

  describe "when the account does not have user_and_organization_tags" do
    before { @presenter.account.stubs(:has_user_and_organization_tags?).returns(false) }

    it "presents an empty array of tags regardless of the tags on the organization" do
      @model.stubs(:tags).returns(['tag1', 'tag2'])

      assert_equal [], @presenter.model_json(@model)[:tags]
    end

    it "includes domains and emails, but not taggings, in #association_preloads" do
      assert_equal(
        {
          custom_field_values: {field: {dropdown_choices: {}}},
          organization_domains: {},
          organization_emails: {}
        },
        @presenter.association_preloads
      )
    end
  end

  describe "when the account has user_and_organization_fields" do
    before { @presenter.account.stubs(:has_user_and_organization_fields?).returns(true) }

    it "presents custom organization fields" do
      assert_includes @presenter.model_json(@model), :organization_fields
    end
  end

  describe "when the account does not have user_and_organization_fields" do
    before { @presenter.account.stubs(:has_user_and_organization_fields?).returns(false) }

    it "presents an empty hash of fields regardless of the fields on the organization" do
      assert_equal({}, @presenter.model_json(@model)[:organization_fields])
    end
  end

  describe "organization custom fields" do
    before do
      @presenter.account.stubs(:has_user_and_organization_fields?).returns(true)

      field = @model.account.custom_fields.for_organization.build(key: "field_1", title: "Field 1")
      field.type = "Checkbox"
      field.save!

      field = @model.account.custom_fields.for_organization.build(key: "field_2", title: "Field 2")
      field.type = "Integer"
      field.save!

      @json = @presenter.model_json(@model)
    end

    it "is included" do
      assert @json[:organization_fields]
    end

    describe "with a custom field or two" do
      before do
        @model.custom_field_values.update_from_hash("field_1" => true, "field_2" => 12345)
      end

      it "models the organization_fields properly" do
        assert_equal({"field_1" => true, "field_2" => 12345}, @presenter.model_json(@model)[:organization_fields])
      end
    end

    describe "exporting organizations" do
      before do
        @field_1 = @model.account.custom_fields.for_organization.first
        @field_2 = @model.account.custom_fields.for_organization.last
      end

      it "excludes fields marked as not exportable" do
        @field_1.is_exportable = false
        @field_1.save!
        owner_account = @model.account.owner.account
        owner_account.stubs(:custom_fields_for_owner).with('Organization').returns(owner_account.custom_fields.active.for_owner('Organization').reload)

        presenter = Api::V2::Organizations::AgentPresenter.new(@model.account.owner, url_builder: mock_url_builder, exclude_non_exportable_fields: true)

        exported_field_keys = presenter.model_json(@model)[:organization_fields].keys
        refute_includes exported_field_keys, @field_1.key
        assert_equal ['field_2'], exported_field_keys
      end

      it "includes fields not specifically excluded from export" do
        presenter = Api::V2::Organizations::AgentPresenter.new(@model.account.owner, url_builder: mock_url_builder, exclude_non_exportable_fields: true)

        exported_field_keys = presenter.model_json(@model)[:organization_fields].keys
        assert_equal ['field_1', 'field_2'], exported_field_keys
      end

      it "includes fields explicitly marked as allowed for export" do
        @field_1.is_exportable = true
        @field_1.save!
        owner_account = @model.account.owner.account
        owner_account.stubs(:custom_fields_for_owner).with('Organization').returns(owner_account.custom_fields.active.for_owner('Organization').reload)

        presenter = Api::V2::Organizations::AgentPresenter.new(@model.account.owner, url_builder: mock_url_builder, exclude_non_exportable_fields: true)

        exported_field_keys = presenter.model_json(@model)[:organization_fields].keys
        assert_equal ['field_1', 'field_2'], exported_field_keys
      end
    end
  end

  describe 'detecting n+1 queries' do
    before do
      field = @model.account.custom_fields.for_organization.build(key: "field_1", title: "Field 1")
      field.type = "Checkbox"
      field.save!

      field = @model.account.custom_fields.for_organization.build(key: "field_2", title: "Field 2")
      field.type = "Integer"
      field.save!

      @one_model = Array(@model)
      Organization.transaction do
        @ten_models = Array.new(10) do
          m = @model.dup
          m.save(validate: false)
          Organization.find(m.id)
        end
      end
      @presenter = ::Api::V2::Organizations::AgentPresenter.new(@model,
        redacted_fields: [],
        url_builder: mock_url_builder)
      @presenter.account.stubs(:has_user_and_organization_fields?).returns(false)
    end

    describe "presenting the model once" do
      should_do_sql_queries(3) { @presenter.present(@one_model) }
    end

    # IMPORTANT: Presenting the same model duplicated should not require extra queries, if it does
    # it's almost certainly an N+1
    describe "presenting the model ten times" do
      should_do_sql_queries(3) { @presenter.present(@ten_models) }
    end
  end
end
