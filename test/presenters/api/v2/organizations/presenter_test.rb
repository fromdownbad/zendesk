require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Organizations::Presenter do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
  end

  it "builds EndUserPresenter for end_users" do
    presenter = Api::V2::Organizations::Presenter.new(@account.users.new { |u| u.roles = Role::END_USER.id }, url_builder: "")
    assert_equal Api::V2::Organizations::EndUserPresenter, presenter.class
  end

  it "builds AgentPresenter for agents" do
    presenter = Api::V2::Organizations::Presenter.new(@account.users.new { |u| u.roles = Role::AGENT.id }, url_builder: "")
    assert_equal Api::V2::Organizations::AgentPresenter, presenter.class
  end

  it "builds AgentPresenter for admins" do
    presenter = Api::V2::Organizations::Presenter.new(@account.users.new { |u| u.roles = Role::ADMIN.id }, url_builder: "")
    assert_equal Api::V2::Organizations::AgentPresenter, presenter.class
  end
end
